/**
 * 
 */
package com.cognizant.insurance.omnichannel.component.repository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.omnichannel.domain.CheckinData;
import com.cognizant.insurance.omnichannel.domain.CheckinData.Status;
import com.cognizant.insurance.omnichannel.jpa.component.CheckedinDetailsComponent;
import com.cognizant.insurance.omnichannel.vo.CheckinResponse;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * @author 397850
 *
 */
public class CheckinRepository {
	
	@PersistenceContext(unitName="LE_Platform")
	private EntityManager entityManager;
	
	@Autowired
	private CheckedinDetailsComponent checkinDetailsComp;
	
	 public static final Logger LOGGER = LoggerFactory.getLogger(CheckinRepository.class);
	
	 @Transactional
	public String checkinDetailsSave(CheckinData checkinData) throws ParseException{
		
		if (checkinData.getAgentId().isEmpty()) {
			throw new IllegalArgumentException("Agent is missing");
		}
	
		String response="Success";
		try{	
			final Request<CheckinData> checkinRequest =
	            new JPARequestImpl<CheckinData>(entityManager, ContextTypeCodeList.AGENT, "");			
		
				checkinRequest.setType(checkinData);
				checkinDetailsComp.save(checkinRequest);
		
		}catch (DaoException e) {
			  LOGGER.error("DaoException", e);
			  response="Failure";
		}catch (Exception e) {
            LOGGER.error("Exception", e);
            response="Failure";
		}
		
		return response;
		
	}

	 @Transactional
	public void retriveAndUpdateCheckedinDetails(CheckinData checkinData) throws ParseException {
		final Request<CheckinData> checkedinRequest =
            new JPARequestImpl<CheckinData>(entityManager, ContextTypeCodeList.AGENT, "");
		
		try{
		CheckinData data= new CheckinData();
		data=checkinDetailsComp.retriveCheckedinDetails(checkedinRequest,checkinData.getAgentId(),Status.checkedin);
		
			if(data.getAgentId()!=null){
			
				data.setCheckoutDateTime(LifeEngageComponentHelper.getCurrentdate());
				data.setStatus(Status.checkedout);
				checkinDetailsComp.update(checkedinRequest,data);
		
			}
		}catch(DaoException e) {
			  LOGGER.error("DaoException", e);
			  //response="Failure";
		}catch (Exception e) {
            LOGGER.error("Exception", e);
            //response="Failure";
		}
		
		
	}

	public List<CheckinData> retireveCheckinList(RequestInfo requestInfo, List<String> agentList, List<CheckinResponse> checkinResponseList) {
		final Request<CheckinData> checkedinRequest =
            new JPARequestImpl<CheckinData>(entityManager, ContextTypeCodeList.AGENT, "");
		
		List<CheckinData> checkinList=new ArrayList<CheckinData>();
		
		 final Request<List<String>> statusRequest =
             new JPARequestImpl<List<String>>(entityManager, ContextTypeCodeList.AGENT,
                     "");
		 statusRequest.setType(agentList);
		
		checkinList=checkinDetailsComp.retireveCheckinList(checkedinRequest,Status.checkedin,statusRequest);
		
		return checkinList;
	}
	
	
	@Transactional
	public void updateCheckinDetails(Date date) {
		
		final Request<CheckinData> checkedinRequest =
            new JPARequestImpl<CheckinData>(entityManager, ContextTypeCodeList.AGENT, "");
		
		checkinDetailsComp.updateCheckinDetails(checkedinRequest,date);
		
		
	}



}
