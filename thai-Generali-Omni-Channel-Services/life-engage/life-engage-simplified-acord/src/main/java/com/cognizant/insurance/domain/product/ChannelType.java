/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Feb 22, 2013
 */
package com.cognizant.insurance.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class class ChannelType.
 */

@Entity
@DiscriminatorValue("Channel")
public class ChannelType extends Type {

    /**
     *
     */
    private static final long serialVersionUID = 2647407107470054408L;

    /** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "channelType")
    private Set<ProductSpecification> productSpecifications;
    
    /** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "channelType")
    private Set<ProductChannel> productChannels;

    /**
     * @param productSpecifications
     *            the productSpecifications to set
     */
    public void setProductSpecifications(Set<ProductSpecification> productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    /**
     * @return the productSpecifications
     */
    public Set<ProductSpecification> getProductSpecifications() {
        return productSpecifications;
    }

    /**
     * @param productChannels
     *            the productChannels to set
     */
    public void setProductChannels(Set<ProductChannel> productChannels) {
        this.productChannels = productChannels;
    }

    /**
     * @return the productChannels
     */
    public Set<ProductChannel> getProductChannels() {
        return productChannels;
    }

}
