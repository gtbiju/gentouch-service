/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * A code list indicating the use of a feature on a manufactured item (e.g.
 * optional, required).
 * 
 * @author 301350
 * 
 * 
 */
public enum SpecificationUseCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Required,
    /**
     * 
     * 
     * 
     * 
     */
    Optional
}
