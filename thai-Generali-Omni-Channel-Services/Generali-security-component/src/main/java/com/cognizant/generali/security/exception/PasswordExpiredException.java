/**
 *
 *
 * @author : 594889
 * 
 */

package com.cognizant.generali.security.exception;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * The Class class PasswordExpiredException.
 */
public class PasswordExpiredException extends UsernameNotFoundException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6683837323771587383L;

    /** The error code. */
    private String errorCode;

    /**
     * Instantiates a new password expired exception.
     * 
     * @param msg
     *            the msg
     */
    public PasswordExpiredException(String msg) {
        super(msg);
    }

    /**
     * Instantiates a new password expired exception.
     * 
     * @param msg
     *            the msg
     * @param t
     *            the t
     */
    public PasswordExpiredException(String msg, Throwable t) {
        super(msg, t);
    }

    /**
     * Instantiates a new password expired exception.
     * 
     * @param errorCode
     *            the error code
     * @param msg
     *            the msg
     */
    public PasswordExpiredException(final String errorCode, final String msg) {
        super(msg);
        this.setErrorCode(errorCode);
    }

    /**
     * Instantiates a new password expired exception.
     * 
     * @param errorCode
     *            the error code
     * @param msg
     *            the msg
     * @param cause
     *            the cause
     */
    public PasswordExpiredException(final String errorCode, final String msg, final Throwable cause) {
        super(msg, cause);
        this.setErrorCode(errorCode);
    }

    /**
     * Sets the error code.
     * 
     * @param errorCode
     *            the error code to set.
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Gets the error code.
     * 
     * @return the error code
     */
    public String getErrorCode() {
        return errorCode;
    }
}
