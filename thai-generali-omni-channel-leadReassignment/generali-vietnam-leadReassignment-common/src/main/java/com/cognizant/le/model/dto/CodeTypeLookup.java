package com.cognizant.le.model.dto;

/**
 * The Class CodeTypes.
 */
public class CodeTypeLookup extends MasterDBEntry {

	/** The id. */
	private int id;
	
	/** The carrier_id. */
	private int carrierId;
	
	/** The type name. */
	private String typeName;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the carrier_id.
	 *
	 * @return the carrier_id
	 */
	public int getCarrier_id() {
		return carrierId;
	}

	/**
	 * Sets the carrier_id.
	 *
	 * @param carrier_id the new carrier_id
	 */
	public void setCarrier_id(int carrier_id) {
		this.carrierId = carrier_id;
	}

	/**
	 * Gets the type name.
	 *
	 * @return the type name
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * Sets the type name.
	 *
	 * @param typeName the new type name
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
