package com.cognizant.insurance.agent.dao;

import com.cognizant.insurance.agent.domain.agent.Agent;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.request.Request;

/**
 * The Interface AgentDao.
 */
public interface AgentDao {

	/**
	 * Retrieve agent by code.
	 *
	 * @param request the request
	 * @return the agent
	 */
	GeneraliAgent retrieveAgentByCode(Request<GeneraliAgent> request);
}
