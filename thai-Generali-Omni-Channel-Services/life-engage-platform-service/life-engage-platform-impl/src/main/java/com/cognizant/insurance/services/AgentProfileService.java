package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * The Interface AgentProfileService.
 */
public interface AgentProfileService {
	
	/**
	 * Retrieve agent by code.
	 *
	 * @param json the json
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String retrieveAgentByCode(final String json) throws BusinessException;
	
	String retrieveLeads(final String json) throws BusinessException;
	
	String validateReassignLogin(final String json) throws BusinessException;
	
	String retrieveAgentsForReassign(final String json) throws BusinessException;
	

}
