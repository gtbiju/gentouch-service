package com.cognizant.insurance.omnichannel.component.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.cognizant.generali.encryption.util.EncryptionUtil;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.component.LifeAsiaRequestSubmitter;
import com.google.gson.Gson;

@Component
public class LifeAsiaRequestSubmitterImpl implements LifeAsiaRequestSubmitter {

    public static final Logger LOGGER = LoggerFactory.getLogger(LifeAsiaRequestSubmitterImpl.class);

    private static final String AUTHORIZATION = "Authorization";

    @Autowired
    RestTemplate restTemplate;

    @Value("${generali.platform.lifeasia.newApplicationURL}")
    private String newApplicationURL;

    @Value("${generali.platform.bpm.retrieveStatusURL}")
    private String retrieveStatusURL;

    @Value("${generali.platform.bpm.retrieveLAStatusURL}")
    private String retrieveLAStatusURL;
    
    @Value("${generali.platform.bpm.authKey}")
    private String authorizationKey;

    @Autowired
    EncryptionUtil encryptionUtil;

    @Autowired
    @Qualifier("eappSubmissionME")
    WebServiceTemplate eappSubmissionME;

    @Override
    public String submit(String object) throws BusinessException {
        return postJSONToBPM(object, newApplicationURL);

    }

    @Override
    public String submit(Source object) throws BusinessException {
        return submit1(object);

    }

    /**
     * Method to Push Data to BPM System
     * 
     * @param soapBody
     * @param url
     * @return
     * @throws BusinessException
     */
    @Override
    public String postJSONToBPM(String jsonString, String url) throws BusinessException {
        LOGGER.info("Sending data to BPM....Start");
        LOGGER.info("BPM Request.."+jsonString);
        HttpHeaders headers = new HttpHeaders();
        //headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Content-Type", "application/json;charset=UTF-8");
        List<MediaType> mediaTypes = new ArrayList<MediaType>();
        
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        mediaTypes.add(mediaType);
        headers.setAccept(mediaTypes);

        headers.add(AUTHORIZATION, authorizationKey);
        HttpEntity<String> httpRequest = new HttpEntity<String>(jsonString, headers);
        ResponseEntity<String> response = null;
        try {

            response = restTemplate.postForEntity(url, httpRequest, String.class);

        } catch (RestClientException e) {
            throw new BusinessException(e.getMessage());
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
        String responseBody = response.getBody();    
        LOGGER.info("BPM Response.."+responseBody);
        LOGGER.info("Sending data to BPM....End");
        return responseBody;
    }

    private String postSOAPXML(String soapBody, String url) throws BusinessException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_XML);
        List<MediaType> mediaTypes = new ArrayList<MediaType>();
        mediaTypes.add(MediaType.TEXT_XML);
        headers.setAccept(mediaTypes);

        headers.add(AUTHORIZATION, generateAuthorizationKey());
        HttpEntity<String> httpRequest = new HttpEntity<String>(soapBody, headers);
        ResponseEntity<String> response = null;
        try {

            response = restTemplate.postForEntity(url, httpRequest, String.class);

        } catch (RestClientException e) {
            throw new BusinessException(e.getMessage());
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
        String responseBody = response.getBody();
        int f = responseBody.lastIndexOf(">");
        int l = responseBody.indexOf("<");
        System.out.println("");
        // Extracting response from SOAP PArt data
        responseBody = responseBody.substring(l, f + 1);
        return responseBody;
    }

    private String generateAuthorizationKey() {
        String authorizationKey = "Basic " + Base64.encodeBase64String(("userName" + ":" + "Password").getBytes());
        return authorizationKey;
    }

    /**
     * WHY WE ARE USING THIS APPROCH </br> - We tried both SOAPClient implementation and RESTtemplate we had experienced
     * some exception because one request contain almost 115MB including Documents if we using SOAPClient we got Out Of
     * memory exception if we are using Simple REST client 'String Object' can't able to handle this much data. So we
     * are using Stream (File) Data it will not in case of data Size
     * 
     * @param urlString
     * @param source
     * @return
     * @throws IOException
     * @throws TransformerException
     */
    @SuppressWarnings("unused")
    private String normalHttpCall(String urlString, StreamSource source) throws IOException, TransformerException {

        String response = "";
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "text/xml");
        connection.setRequestProperty(AUTHORIZATION, generateAuthorizationKey());

        OutputStream os = connection.getOutputStream();
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();

            StreamResult result = new StreamResult(os);

            transformer.transform(source, result);

            os.flush();

            if (200 <= connection.getResponseCode() && connection.getResponseCode() <= 299) {
                response = IOUtils.toString(connection.getInputStream(), "UTF-8");
            } else {
                response = IOUtils.toString(connection.getErrorStream(), "UTF-8");
            }
        } finally {
            os.flush();
            connection.disconnect();
        }
        ;
        int f = response.lastIndexOf(">");
        int l = response.indexOf("<");
        // Extracting response from SOAP PArt data
        response = response.substring(l, f + 1);
        return response;
    }

    public String submit1(Source request) {
        ByteArrayOutputStream streamResult = new ByteArrayOutputStream();
        StreamResult res = new StreamResult(streamResult);
        Writer writer = new StringWriter();
        res.setWriter(writer);
        // eappSubmissionME.setDefaultUri(newApplicationURL);
        // eappSubmissionME.afterPropertiesSet();
        @SuppressWarnings("unused")
        boolean _responseRecevied = eappSubmissionME.sendSourceAndReceiveToResult(request, res);
        String result1 = writer.toString();
        return result1;
    }

    public String fetchStatusChanges(String request) throws BusinessException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        List<MediaType> mediaTypes = new ArrayList<MediaType>();
        mediaTypes.add(MediaType.APPLICATION_JSON);
        headers.setAccept(mediaTypes);
        String encryptedString = encryptionUtil.encryptString(request);
        String responseBodyString = "";
        HttpEntity<String> httpRequest = new HttpEntity<String>(encryptedString, headers);
        ResponseEntity<String> response = null;

        try {

            response = restTemplate.postForEntity(retrieveStatusURL, httpRequest, String.class);
            ;
            responseBodyString = encryptionUtil.decryptString(new Gson().fromJson(response.getBody(), String.class));

        } catch (RestClientException e) {
            throw new BusinessException(e.getMessage());
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
        return responseBodyString;
    }

    /**
     * Method to get response from getLAStatus web service 0- up 1- down 4- time out
     * 
     */
    public String getLAStatus() throws BusinessException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        List<MediaType> mediaTypes = new ArrayList<MediaType>();
        mediaTypes.add(MediaType.APPLICATION_JSON);
        headers.setAccept(mediaTypes);
        String responseBodyString = "";
        HttpEntity<String> httpRequest = new HttpEntity<String>(headers);
        ResponseEntity<String> response = null;

        try {
            response = restTemplate.exchange(retrieveLAStatusURL, HttpMethod.GET, httpRequest, String.class);
            ;
            responseBodyString = encryptionUtil.decryptString(response.getBody());

        } catch (RestClientException e) {
            throw new BusinessException(e.getMessage());
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
        return responseBodyString;
    }

}
