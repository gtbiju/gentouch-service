/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.component;

import com.cognizant.insurance.lookup.CountryDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.vo.CountryLookUpResponse;

/**
 * @author 300797
 *
 */
public interface LookUpComponent {

    /**
     * gets the country details.
     *
     * @param request the request
     */
    Response<CountryLookUpResponse> getCountrySpecificDetails(Request<CountryDetails> request);

    /**
     * Gets the country or country sub division list.
     *
     * @param request the request
     * @return the country or country sub division list
     */
    Response<CountryLookUpResponse> getCountryOrCountrySubDivisionList(Request<CountryDetails> request);
}
