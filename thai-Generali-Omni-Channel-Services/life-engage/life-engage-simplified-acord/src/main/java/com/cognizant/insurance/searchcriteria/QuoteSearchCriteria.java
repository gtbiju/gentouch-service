/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 305688
 * @version       : 0.1, Mar 4, 2013
 */

package com.cognizant.insurance.searchcriteria;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class class QuoteSearchCriteria.
 */
public class QuoteSearchCriteria implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6751871510847211534L;

    /** The id. */
    private Long id;

    /** The code. */
    private String code;

    /** The first name. */
    private String firstName;

    /** The last name. */
    private String lastName;

    /** The start date. */
    private Date startDate;
    
    /** The end date. */
    private Date endDate;
    
    /** The last name. */
    private String typeName;

      /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the code to set.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name.
     *
     * @param firstName the first name to set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name.
     *
     * @param lastName the last name to set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Sets the start date.
     *
     * @param startDate the start date to set.
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Sets the end date.
     *
     * @param endDate the end date to set.
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Sets The typeName.
     *
     * @param typeName The typeName to set.
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * Gets the typeName.
     *
     * @return Returns the typeName.
     */
    public String getTypeName() {
        return typeName;
    }

}
