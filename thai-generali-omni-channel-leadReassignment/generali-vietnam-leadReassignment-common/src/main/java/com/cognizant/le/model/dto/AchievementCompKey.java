package com.cognizant.le.model.dto;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class AchievementCompKey implements Serializable {
	private String agentId;
	private String title;

	public AchievementCompKey(String agentId, String title) {

		this.agentId = agentId;

		this.title = title;
 }

	public AchievementCompKey() {

	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
