/**
 * 
 */
package com.cognizant.insurance.domain.finance.financecodelists;

/**
 * This code list defines the possible ways of counting an entry (for example,
 * an accounting entry). Values are debit or credit.
 * 
 * @author 301350
 * 
 * 
 */
public enum EntryCategoryCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    debit,
    /**
     * 
     * 
     * 
     * 
     */
    credit
}
