/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.generali.encryption.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Utility class to read the property file.
 * 
 */
public final class PropertyFileReader {

    /** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertyFileReader.class);

    /**
     * Fetch property.
     * 
     * @param key
     *            , the key to the property file.
     * @param fileName
     *            the file name
     * @return the property value.
     */
    public static String fetchProperty(final String key, final String fileName) {
        LOGGER.debug("Method Entry : PropertyFileReader-> fetchProperty");
        String value = null;
        try {
            Properties prop = new Properties();
            /*
             * URL url = prop.getClass().getResource("/queries.properties"); FileInputStream fileStream = new
             * FileInputStream(url.getFile());
             */
           // prop.load(PropertyFileReader.class.getResourceAsStream(fileName));
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("/" + fileName);
			if(null == stream) {
				stream = classLoader.getResourceAsStream(fileName);
			}
			prop.load(stream);
            value = prop.getProperty(key);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.debug("Method Exit : PropertyFileReader-> fetchProperty");
        return value;
    }

    /**
     * Instantiates a new property file reader.
     */
    private PropertyFileReader() {

    }

}
