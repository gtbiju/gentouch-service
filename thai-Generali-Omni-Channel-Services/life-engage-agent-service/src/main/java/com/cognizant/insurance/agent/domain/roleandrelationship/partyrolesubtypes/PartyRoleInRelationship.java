/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes;



import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.cognizant.insurance.agent.domain.roleandrelationship.PartyRole;

/**
 * A role played by a party in the context of a relationship.
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PARTY_ROLE_IN_RELATIONSHIP", discriminatorType = DiscriminatorType.STRING)
public class PartyRoleInRelationship extends PartyRole {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -666869047275043448L;
}
