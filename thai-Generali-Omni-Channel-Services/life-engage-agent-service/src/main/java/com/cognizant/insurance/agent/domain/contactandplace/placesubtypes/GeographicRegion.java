/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.placesubtypes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.cognizant.insurance.agent.domain.contactandplace.Place;

/**
 * Region is most commonly found as a term used in terrestrial and astrophysics
 * sciences also an area, notably among the different sub-disciplines of
 * geography, studied by regional geographers.
 * 
 * SOURCE: http://en.wikipedia.org/wiki/Region
 * 
 * ACORD Note: As used here, this represents a geographic place with no
 * recognized/formal government. A region may be known formally, with agreed
 * upon understanding and/or be defined as needed by a given party (e.g. New
 * England, Scandinavia, The Tropics, a flood zone, a wind zone, etc.).
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("GEOGRAPHIC_REGION")
public class GeographicRegion extends Place {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2611850312183629635L;

    /**
     * This relationship links a country element to the country element group to
     * which it belongs.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany (cascade=CascadeType.ALL)
    @JoinColumn(name="relatedid")
    private Set<CountryElement> includesElement;

    /**
     * This relationship links a country element group to its related country.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne (cascade=CascadeType.PERSIST)
    private Country relatedCountry;

    /**
     * @return the includesElement
     * 
     * 
     */
    public Set<CountryElement> getIncludesElement() {
        return includesElement;
    }

    /**
     * @return the relatedCountry
     * 
     * 
     */
    public Country getRelatedCountry() {
        return relatedCountry;
    }

    /**
     * @param includesElement
     *            the includesElement to set
     * 
     * 
     */
    public void setIncludesElement(
            final Set<CountryElement> includesElement) {
        this.includesElement = includesElement;
    }

    /**
     * @param relatedCountry
     *            the relatedCountry to set
     * 
     * 
     */
    public void setRelatedCountry(final Country relatedCountry) {
        this.relatedCountry = relatedCountry;
    }
}
