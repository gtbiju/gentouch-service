/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 301350
 * @version       : 0.1, Aug 27, 2013
 */
package com.cognizant.insurance.services.vo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * The Class class FileUploadForm.
 */
public class FileUploadForm {

    /** The files. */
    private CommonsMultipartFile [] files;
    
    /** The proposal number. */
    private String proposalNumber;

    /**
     * Gets the proposal number.
     *
     * @return the proposal number
     */
    public final String getProposalNumber() {
        return proposalNumber;
    }

    /**
     * Sets the proposal number.
     *
     * @param proposalNumber the proposal number to set.
     */
    public final void setProposalNumber(final String proposalNumber) {
        this.proposalNumber = proposalNumber;
    }

    /**
     * Gets the files.
     *
     * @return the files
     */
    public final CommonsMultipartFile[] getFiles() {
        CommonsMultipartFile [] files = null;
        if (this.files != null) {
            files = this.files.clone();
        }
        return files;
    }

    /**
     * Sets the files.
     *
     * @param files the files to set.
     */
    public final void setFiles(final CommonsMultipartFile[] files) {
        this.files = new CommonsMultipartFile[files.length];
        System.arraycopy(files, 0, this.files, 0, files.length);
    }
}