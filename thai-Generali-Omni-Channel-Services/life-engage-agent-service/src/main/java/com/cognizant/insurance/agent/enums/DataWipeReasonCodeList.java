/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Nov 22, 2016
 */

package com.cognizant.insurance.agent.enums;

/**
 * The Enum enum DataWipeReasonCodeList.
 */
public enum DataWipeReasonCodeList {

    /** The Rooted device. */
    RootedDevice,

    /** The Un authorized user. */
    UnAuthorizedUser,

    /** The Brute force login. */
    BruteForceLogin,

    /** The Other. */
    Other
}
