package com.cognizant.insurance.omnichannel.utils;

import java.util.List;

/**
 * @author 420118
 * 
 *         The Class ExclusionsTableData
 * 
 *         This class is a POJO for Memo details in Memo PDF..
 * 
 */
public class ExclusionsTableData {

	
	private String description;

	private String index;
	
	private String countOfRiders;
	
	public String getCountOfRiders() {
		return countOfRiders;
	}

	public void setCountOfRiders(String countOfRiders) {
		this.countOfRiders = countOfRiders;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
