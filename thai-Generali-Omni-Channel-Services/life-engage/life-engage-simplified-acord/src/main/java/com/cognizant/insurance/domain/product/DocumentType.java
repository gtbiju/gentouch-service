/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Dec 13, 2013
 */

package com.cognizant.insurance.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class class DocumentType.
 */

@Entity
@DiscriminatorValue("Document_Types")
public class DocumentType extends Type {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2182594528943276604L;

    /** The product collaterals. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "documentType")
    private Set<ProductCollaterals> productCollaterals;

    /**
     * The Constructor.
     */
    public DocumentType() {
        super();
    }

    /**
     * The Constructor.
     * 
     * @param id
     *            the id
     */
    public DocumentType(final String id) {
        super(id);
    }

    /**
     * Sets the product collaterals.
     * 
     * @param productCollaterals
     *            the productCollaterals to set
     */
    public final void setProductCollaterals(Set<ProductCollaterals> productCollaterals) {
        this.productCollaterals = productCollaterals;
    }

    /**
     * Gets the product collaterals.
     * 
     * @return the productCollaterals
     */
    public final Set<ProductCollaterals> getProductCollaterals() {
        return productCollaterals;
    }

}
