/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.agent.request.vo;

import java.util.List;

/**
 * The Class class RequestPayload.
 *
 * @author 345166
 * 
 * Class RequestPayload - Created for EApp. Used to map RequestPayload details.
 */
public class RequestPayload {
    
	/** The transactions list. */
	private List<Transactions> transactions;

	public List<Transactions> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transactions> transactions) {
		this.transactions = transactions;
	}
	
	
}
