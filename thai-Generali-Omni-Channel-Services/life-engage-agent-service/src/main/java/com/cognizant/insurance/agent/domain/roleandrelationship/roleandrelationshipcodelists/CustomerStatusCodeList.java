/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists;

/**
 * Allowed life cycle states for a customer.
 * 
 * @author 300797
 * 
 * 
 */
public enum CustomerStatusCodeList {

    /** The Initial. */
    Initial,

    /** The Active. */
    Active,

    /** The Disqualified. */
    Disqualified,

    /** The Dormant. */
    Dormant,

    /** The Final. */
    Final,

    /** The Former. */
    Former,

    /** The Not interested. */
    NotInterested,

    /** The Potential. */
    Potential,

    /** The Prospect. */
    Prospect,

    /** Not Mentioned. */
    NotMentioned,

    /** Open. */
    Open,

    /** Dropped. */
    Dropped,

    /** Login. */
    Login,

    /** Fresh. */
    Fresh,

    /** Interested. */
    Interested,

    /** DocumentationPending. */
    DocumentationPending,

    /** FollowUp. */
    FollowUp,

    /** Recyclable. */
    Recyclable,

    /** Suspended. */
    Suspended,
    
    /** Cancelled. */
    Cancelled,
    
    // Added for generali Vietnam
    /** New. */
    New,
    /** Closed or Submitted. */ 
    SuccessfulSale,
    
    Non_Contactable,
	
	Meeting_Fixed,
	
	Successful_Sale,
	
	ToBeCalled,
	
	ContactedAndNotInterested,
	
	App_Submitted,
	
	Pending_Documents,
	
	Others
	
    //End
}
