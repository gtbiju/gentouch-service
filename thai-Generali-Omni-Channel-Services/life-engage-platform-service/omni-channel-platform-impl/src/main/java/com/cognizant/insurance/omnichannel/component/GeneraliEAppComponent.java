/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 356551
 * @version       : 0.1, Apr 11, 2018
 */
package com.cognizant.insurance.omnichannel.component;

import org.json.JSONObject;

import com.cognizant.insurance.component.LifeEngageEAppComponent;
import com.cognizant.insurance.request.vo.RequestInfo;

public interface GeneraliEAppComponent extends LifeEngageEAppComponent{
    
    String lockOrUnlock(RequestInfo requestInfo, JSONObject jsonObj);
    
}
