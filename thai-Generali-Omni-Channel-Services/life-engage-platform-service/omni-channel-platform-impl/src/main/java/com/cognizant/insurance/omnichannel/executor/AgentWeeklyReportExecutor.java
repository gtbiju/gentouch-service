package com.cognizant.insurance.omnichannel.executor;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.omnichannel.component.GeneraliEmailComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliSalesActivityComponent;
import com.cognizant.insurance.omnichannel.utils.GeneraliExcelSheetUtil;
import com.cognizant.insurance.omnichannel.vo.AgentReportVo;

/**
 * Class : - AgentReportExecutor - Scheduler to create agent report
 * 
 * @author 481774
 * 
 */
@Component
public class AgentWeeklyReportExecutor {
    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(AgentWeeklyReportExecutor.class);

    @Autowired
    private GeneraliEmailComponent generaliEmailComponent;

    @Autowired
    private GeneraliExcelSheetUtil generaliExcelSheetUtil;

    @Autowired
    private GeneraliSalesActivityComponent generaliSalesActivityComponent;

    private static final String WEEKLY = "Weekly";

    public void run() {
        List<AgentReportVo> agentData = null;
        String fileName = null;
        try {
            LOGGER.info("Agent Report Weekly Data Load-> Start");
            agentData = generaliSalesActivityComponent.getAgentReport(WEEKLY);
            LOGGER.info("Agent Report Weekly Data Load-> End");
            LOGGER.info("Agent Report Weekly Excel generation-> Start");
            fileName = generaliExcelSheetUtil.generateExcelReport(agentData, WEEKLY);
            LOGGER.info("Agent Report Weekly Excel generation-> End");
            LOGGER.info("Agent Report Weekly mail-> Start");
            generaliEmailComponent.sendAgentReport(fileName, WEEKLY);
            LOGGER.info("Agent Report Weekly mail-> End");
        } catch (Exception e) {
            LOGGER.error("Error in Agent Weekly Report scheduler {}", e.getMessage());
        }
    }
}
