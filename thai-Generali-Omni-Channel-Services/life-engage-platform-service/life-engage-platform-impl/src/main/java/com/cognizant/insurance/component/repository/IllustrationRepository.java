/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 25, 2013
 */
package com.cognizant.insurance.component.repository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.agreement.component.AgreementComponent;
import com.cognizant.insurance.component.repository.helper.LifeEngageComponentRepositoryHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.finance.FinancialProvision;
import com.cognizant.insurance.domain.finance.agreementloan.AgreementLoan;
import com.cognizant.insurance.domain.finance.agreementloan.AgreementWithdrawal;
import com.cognizant.insurance.domain.goalandneed.Agreement_GoalAndNeed;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.party.component.PartyComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

/**
 * The Class class IllustrationRepository.
 * 
 * @author 300797
 */
// @Repository
public class IllustrationRepository {

    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;

    /** The agreement component. */
    @Autowired
    private AgreementComponent agreementComponent;

    /** The party component. */
    @Autowired
    private PartyComponent partyComponent;

    /** The conflict resolution. */
    @Value("${le.platform.service.conflictResolution}")
    private String conflictResolution;

    /** The source of truth. */
    @Value("${le.platform.service.sourceOfTruth}")
    private String sourceOfTruth;

    /** The illustration limit. */
    @Value("${le.platform.service.illustration.fetchLimit}")
    private Integer illustrationLimit;
    
    /** File Upload Path */
    @Value("${le.platform.service.fileUploadPath}")
    private String fileUploadPath;
    
    /** The slash. */
    @Value("${le.platform.service.slash}")
    private String slash; 

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(IllustrationRepository.class);

    /**
     * Save illustration.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the string
     * @throws ParseException
     *             the parse exception
     */
    public String saveIllustration(final Transactions transactions, final RequestInfo requestInfo, InsuranceAgreement savedIllustration)
            throws ParseException {
        LOGGER.trace("Inside IllustrationRepository saveIllustration : requestInfo" + requestInfo);
        final Request<Agreement> request =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) transactions.getProposal();
        insuranceAgreement.setContextId(ContextTypeCodeList.ILLUSTRATION);
        if (Constants.UPDATE.equals(transactions.getMode())) {
            String status = onAgreementUpdate(transactions, requestInfo, savedIllustration);
            if (!Constants.SUCCESS.equals(status)) {
                return status;
            }
        } else {
            insuranceAgreement.setCreationDateTime(transactions.getCreationDateTime());
        }
        onBeforeSave(transactions, requestInfo);
        //Changes related to save requirement in Agreement.
        insuranceAgreement.setRequirements(transactions.getRequirements());
        request.setType(insuranceAgreement);
        agreementComponent.saveAgreement(request);

        createAgreementLoans(transactions.getAgreementLoans(), insuranceAgreement, requestInfo);
        createAgreementWithdrawals(transactions.getAgreementWithdrawals(), insuranceAgreement, requestInfo);

        LifeEngageComponentRepositoryHelper.saveInsured(transactions.getInsured(), requestInfo, insuranceAgreement,
                entityManager, ContextTypeCodeList.ILLUSTRATION, partyComponent);

        saveBeneficiary(transactions.getBeneficiaries(), requestInfo, insuranceAgreement);

        LifeEngageComponentRepositoryHelper.savePayer(transactions.getPayer(), requestInfo, insuranceAgreement,
                entityManager, ContextTypeCodeList.ILLUSTRATION, partyComponent);

        return insuranceAgreement.getStatusCode().toString();
    }

    /**
     * On agreement update.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the string
     * @throws ParseException
     *             the parse exception
     */
    protected String onAgreementUpdate(Transactions transactions, RequestInfo requestInfo, InsuranceAgreement savedIllustration) throws ParseException {
        Set<Agreement> replacingAgeements = null;
        final List<AgreementStatusCodeList> statusList =getAgreementStatusCodesFor(Constants.UPDATE_OP);
        InsuranceAgreement agreement = null;
        if (null != savedIllustration) // We already have agreement with transTracking Id
            agreement = savedIllustration;
        else {
            agreement =
                    LifeEngageComponentRepositoryHelper.createReplacingAgreement(transactions, requestInfo,
                            entityManager, agreementComponent, ContextTypeCodeList.ILLUSTRATION, statusList);
        }
        // Checking whether the status code is cancelled, if so return directly.
        if (agreement.getStatusCode() != null && agreement.getStatusCode().equals(AgreementStatusCodeList.Cancelled)) {
            return AgreementStatusCodeList.Cancelled.toString();
        }
        // Checking whether the server data is latest when source of truth is server, if so reject the client data and
        // send rejected status
        if (Constants.TRUE.equals(conflictResolution) && Constants.SERVER.equals(sourceOfTruth)) {

            LOGGER.trace("Inside conflict resolution true...");
            Date modifiedTimeStamp = agreement.getModifiedDateTime();

            if (!(transactions.getLastSyncDate().toString().equals(Constants.DEFAULT_DATE))
                    && ((null != modifiedTimeStamp) && (modifiedTimeStamp.after(transactions.getLastSyncDate())))) {

                return Constants.REJECTED;

            }
        }
        replacingAgeements =
                LifeEngageComponentRepositoryHelper.updateAgreementStatus(agreement, agreementComponent, requestInfo,
                        entityManager, ContextTypeCodeList.ILLUSTRATION);

        transactions.getProposal().setCreationDateTime(agreement.getCreationDateTime());

        Set<AgreementDocument> agreementDocumentsSet = new HashSet<AgreementDocument>();
        if (replacingAgeements != null) {
            agreementDocumentsSet = LifeEngageComponentRepositoryHelper.createReplaceDocument(replacingAgeements);
            transactions.getProposal().setRelatedDocument(agreementDocumentsSet);
            transactions.getProposal().setModifiedDateTime(LifeEngageComponentRepositoryHelper.getCurrentdate());
        }
        if (replacingAgeements != null) {
            LifeEngageComponentRepositoryHelper.createReplacingRequirementSet(transactions, replacingAgeements,
                    fileUploadPath + slash);
            // While copying all the Previous requirements will be set in the current agreement
        }
        transactions.getProposal().setReplacesAgreements(replacingAgeements);

        return Constants.SUCCESS;
    }

    /**
     * On before save.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     */
    protected void onBeforeSave(Transactions transactions, RequestInfo requestInfo) {
        InsuranceAgreement insuranceAgreement = transactions.getProposal();
        // Since ModifiedTime is using for Chunk retrieve and SourceOfTruth functionality
        insuranceAgreement.setModifiedDateTime(transactions.getModifiedTimeStamp());

        insuranceAgreement.setTransactionId(requestInfo.getTransactionId());
        if (StringUtils.isNotEmpty(transactions.getIllustrationId())) {
        insuranceAgreement.setIdentifier(transactions.getIllustrationId());
        }

        if (null != transactions.getStatus()
                && transactions.getStatus().equals(AgreementStatusCodeList.Cancelled.toString())) {

            insuranceAgreement.setStatusCode(AgreementStatusCodeList.Cancelled);
        } else {
            insuranceAgreement.setStatusCode(AgreementStatusCodeList.Final);
        }

        if (transactions.getAgentId() != null) {
            insuranceAgreement.setAgentId(transactions.getAgentId());
        }

    }

    /**
     * Retrieve illustration.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the transactions
     */
    public Transactions retrieveIllustration(final Transactions transactions, final RequestInfo requestInfo) {
        String transactionId = requestInfo.getTransactionId();
        Transactions transaction = new Transactions();
        transaction = getIllustration(transactions, transactionId);
        return transaction;
    }

    /**
     * Retrieve illustration for pdf generation.
     * 
     * @param illustrationId
     *            the illustrationId
     * @return the transactions
     */
    public Transactions retrieveIllustrationforPdfGeneration(String illustrationId) {
        Transactions transactions = new Transactions();
        transactions.setIllustrationId(illustrationId);
        transactions = getIllustration(transactions, "");
        return transactions;
    }

    /**
     * Retrieve illustration.
     * 
     * @param transactions
     *            the transactions
     * @param transactionId
     *            the transaction id
     * @return the transactions
     */
    public Transactions getIllustration(final Transactions transactions, String transactionId) {
        final Transactions transaction = new Transactions();
        transaction.setIllustrationId(transactions.getIllustrationId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE);

        final Request<Agreement> request =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.ILLUSTRATION, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(transactions.getIllustrationId());
        request.setType(insuranceAgreement);

        final String illustrationID = transactions.getIllustrationId();

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementResponse =
                agreementComponent.retrieveAgreementForIdentifier(request, statusRequest);
        final InsuranceAgreement agreement = (InsuranceAgreement) agreementResponse.getType();
        if (agreement != null) {
            transaction.setProposal(agreement);
            request.setType(agreement);

            getAgreementDetails(transaction, request);
            transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
        }

        final Request<Agreement_GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<Agreement_GoalAndNeed>(entityManager, ContextTypeCodeList.FNA, transactionId);

        final String getFNAId = agreementComponent.retrieveFNAId(goalAndNeedRequest, illustrationID);
        if (StringUtils.isNotEmpty(getFNAId)) {
            transaction.setFnaId(getFNAId);
        }
        return transaction;
    }

    /**
     * Gets the agreement details such as Insured details, payer details, agreement loan and agreement withdrawals.
     * 
     * @param transaction
     *            the transaction
     * @param request
     *            the request
     * @return the agreement details
     */
    protected void getAgreementDetails(final Transactions transaction, final Request<Agreement> request) {
        final Response<Insured> insuredResponse = partyComponent.retrieveInsured(request);
        transaction.setInsured(insuredResponse.getType());

        final Response<PremiumPayer> payerResponse = partyComponent.retrievePayer(request);
        transaction.setPayer(payerResponse.getType());

        final Response<Set<Beneficiary>> beneficiariesResponse = partyComponent.retrieveBeneficiaries(request);
        transaction.setBeneficiaries(beneficiariesResponse.getType());

        final Response<Set<AgreementLoan>> agreementLoanResponse = agreementComponent.getAgreementLoans(request);
        transaction.setAgreementLoans(agreementLoanResponse.getType());

        final Response<Set<AgreementWithdrawal>> agreementWithdrawalResponse =
                agreementComponent.getAgreementWithdrawals(request);
        transaction.setAgreementWithdrawals(agreementWithdrawalResponse.getType());
    }

    /**
     * Retrieve all.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param isFullDetailsRequired
     *            the is full details required
     * @return the list
     */
    public List<Transactions> retrieveAll(final Transactions transactions, final RequestInfo requestInfo,
            final boolean isFullDetailsRequired) {
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;

         List<AgreementStatusCodeList> statusList = null;

        if (requestInfo.getFirstTimeSync()) {
        	statusList=getAgreementStatusCodesFor(Constants.FIRST_TIME_SYNC);
        } else {
            statusList=getAgreementStatusCodesFor(Constants.RETRIEVE_ALL);
           
        }

        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setAgentId(transactions.getAgentId());
        insuranceAgreement.setLastSyncDate(requestInfo.getLastSyncDate());
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        Request<Integer> limitRequest = null;

        if (isFullDetailsRequired) {
            if (illustrationLimit != null) {
                limitRequest =
                        new JPARequestImpl<Integer>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                                requestInfo.getTransactionId());
                limitRequest.setType(illustrationLimit);
            }
        }
        final Response<List<Agreement>> agreementsResponse =
                agreementComponent.getAgreements(agreementRequest, statusRequest, limitRequest);

        final List<Agreement> agreements = agreementsResponse.getType();
        if (CollectionUtils.isNotEmpty(agreements)) {
            for (Agreement agreement : agreements) {
                transaction = new Transactions();
                transaction.setProposal((InsuranceAgreement) agreement);

                agreementRequest.setType(agreement);

                final String illustrationID = agreement.getIdentifier().toString();

                if (isFullDetailsRequired) {
                    getAgreementDetails(transaction, agreementRequest);
                } else {
                    final Response<Insured> insuredResponse = partyComponent.retrieveInsured(agreementRequest);
                    transaction.setInsured(insuredResponse.getType());
                    final Response<PremiumPayer> payerResponse = partyComponent.retrievePayer(agreementRequest);
                    transaction.setPayer(payerResponse.getType());
                }

                final Request<Agreement_GoalAndNeed> goalAndNeedRequest =
                        new JPARequestImpl<Agreement_GoalAndNeed>(entityManager, ContextTypeCodeList.FNA,
                                requestInfo.getTransactionId());

                final String getFNAId = agreementComponent.retrieveFNAId(goalAndNeedRequest, illustrationID);
                if (StringUtils.isNotEmpty(getFNAId)) {
                    transaction.setFnaId(getFNAId);
                }
                if (agreement.getModifiedDateTime() != null && !(agreement.getModifiedDateTime().toString().equals(""))) {

                    transaction.setLastSyncDate(agreement.getModifiedDateTime());
                } else if (agreement.getCreationDateTime() != null
                        && !("".equals(agreement.getCreationDateTime().toString()))) {

                    transaction.setLastSyncDate(agreement.getCreationDateTime());
                }

                transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }

        return transactionsList;
    }

    /**
     * Save fna id.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     */
    @SuppressWarnings("null")
    public void saveFNAId(Transactions transactions, RequestInfo requestInfo) {

        final Request<Agreement_GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<Agreement_GoalAndNeed>(entityManager, ContextTypeCodeList.FNA,
                        requestInfo.getTransactionId());
        final Agreement_GoalAndNeed agreementgoalAndNeed = new Agreement_GoalAndNeed();
        final String fnaId = transactions.getFnaId();
        final String illustratuinId = transactions.getIllustrationId();
        agreementgoalAndNeed.setFnaID(fnaId);
        agreementgoalAndNeed.setIllustrationID(illustratuinId);
        goalAndNeedRequest.setType(agreementgoalAndNeed);
        agreementComponent.saveFNAId(goalAndNeedRequest);
    }

    /**
     * Creates the agreement loans.
     * 
     * @param agreementLoans
     *            the agreement loans
     * @param insuranceAgreement
     *            the insurance agreement
     * @param requestInfo
     *            the request info
     */
    protected void createAgreementLoans(final Set<AgreementLoan> agreementLoans,
            final InsuranceAgreement insuranceAgreement, final RequestInfo requestInfo) {
        if (CollectionUtils.isNotEmpty(agreementLoans)) {
            final Request<FinancialProvision> financialProvisionRequest =
                    new JPARequestImpl<FinancialProvision>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                            requestInfo.getTransactionId());
            for (AgreementLoan agreementLoan : agreementLoans) {
                agreementLoan.setRelatedAgreement(insuranceAgreement);
                financialProvisionRequest.setType(agreementLoan);
                agreementComponent.saveFinancialProvision(financialProvisionRequest);
            }
        }
    }

    /**
     * Creates the agreement withdrawals.
     * 
     * @param agreementWithdrawals
     *            the agreement withdrawals
     * @param insuranceAgreement
     *            the insurance agreement
     * @param requestInfo
     *            the request info
     */
    protected void createAgreementWithdrawals(final Set<AgreementWithdrawal> agreementWithdrawals,
            final InsuranceAgreement insuranceAgreement, final RequestInfo requestInfo) {
        if (CollectionUtils.isNotEmpty(agreementWithdrawals)) {
            final Request<FinancialProvision> financialProvisionRequest =
                    new JPARequestImpl<FinancialProvision>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                            requestInfo.getTransactionId());
            for (AgreementWithdrawal agreementWithdrawal : agreementWithdrawals) {
                agreementWithdrawal.setRelatedAgreement(insuranceAgreement);
                financialProvisionRequest.setType(agreementWithdrawal);
                agreementComponent.saveFinancialProvision(financialProvisionRequest);
            }

        }
    }

    /**
     * Save beneficiary.
     * 
     * @param beneficiaries
     *            the beneficiaries
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    protected void saveBeneficiary(final Set<Beneficiary> beneficiaries, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement) {

        if (CollectionUtils.isNotEmpty(beneficiaries)) {
            final Request<PartyRoleInAgreement> beneficiaryRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                            requestInfo.getTransactionId());
            for (Beneficiary beneficiary : beneficiaries) {
                beneficiary.setIsPartyRoleIn(insuranceAgreement);
                beneficiary.setTransactionId(requestInfo.getTransactionId());
                beneficiaryRequest.setType(beneficiary);
                partyComponent.savePartyRoleInAgreement(beneficiaryRequest);
            }
        }
    }

    /**
     * Get illustration agreement.
     * 
     * @param transactions
     *            the transactions
     * @param transactionId
     *            the transaction id
     * @return the illustration agreement
     */

    public final Agreement getIllustrationAgreement(final Transactions transactions, String transactionId) {

        final Transactions transaction = new Transactions();
        final List<AgreementStatusCodeList> statusList =getAgreementStatusCodesFor(Constants.RETRIEVE);
        final Request<Agreement> request =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.ILLUSTRATION, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setTransTrackingId(transactions.getIllustrationId());
        insuranceAgreement.setAgentId(transactions.getAgentId());
        request.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementResponse =
                agreementComponent.retrieveAgreementForTransTrackingId(request, statusRequest);
        final InsuranceAgreement agreement = (InsuranceAgreement) agreementResponse.getType();
        if (agreement != null) {
            transaction.setProposal(agreement);
            request.setType(agreement);

            getAgreementDetails(transaction, request);
        }

        return agreement;
    }

    /**
     * Retrieve by count illustration.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the search count result
     */
    public SearchCountResult retrieveByCountIllustration(final RequestInfo requestInfo,
            final SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_COUNT);
        statusRequest.setType(statusList);

        final Response<SearchCountResult> searchCountResponse =
                agreementComponent.getAgreementCount(searchCriteriatRequest, statusRequest);

        SearchCountResult searchCountResult = null;
        if (searchCountResponse.getType() != null) {
            searchCountResult = (SearchCountResult) searchCountResponse.getType();

        }
        return searchCountResult;

    }

    /**
     * Retrieve by filter illustration.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the list
     */
    public List<Transactions> retrieveByFilterIllustration(RequestInfo requestInfo, SearchCriteria searchCriteria) {

        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_FILTER);
       
        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Response<List<Agreement>> searchCriteriatResponse =
                agreementComponent.getAgreementByFilter(searchCriteriatRequest, statusRequest);

        final List<Agreement> agreements = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(agreements)) {
            for (Agreement agreement : agreements) {
                transaction = new Transactions();
                transaction.setProposal((InsuranceAgreement) agreement);
                transaction.setIllustrationId(agreement.getIdentifier().toString());
                transaction.setType(searchCriteria.getType());
                transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }
        LOGGER.trace("Inside EAppRepository retrieveByFilterIllustration : transactionsList" + transactionsList.size());
        return transactionsList;

    }
    
    /**
     * Retrieve ids by email illustration.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     */
    public List<Transactions> retrieveIdsByEmailIllustration(RequestInfo requestInfo, SearchCriteria searchCriteria) {

        LOGGER.trace("Inside IllustrationRepository retrieveIdsByEmailIllustration : requestInfo" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = new Transactions();
        String illustrationID = null;
        ArrayList<String> idsList = new ArrayList<String>();
        idsList = searchCriteria.getSelectedIds();
        for (int i = 0; i < idsList.size(); i++) {
            illustrationID = idsList.get(i);
            transaction.setIllustrationId(illustrationID);
            transaction = getIllustration(transaction, "");
            transactionsList.add(transaction);
        }

        LOGGER.trace("Inside IllustrationRepository retrieveIdsByEmailIllustration : transactionsList"
                + transactionsList.size());
        return transactionsList;

    }

    /**
     * Gets the entity manager.
     * 
     * @return the entityManager
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Sets the entity manager.
     * 
     * @param entityManager
     *            the entityManager to set
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Gets the agreement component.
     * 
     * @return the agreementComponent
     */
    public final AgreementComponent getAgreementComponent() {
        return agreementComponent;
    }

    /**
     * Sets the agreement component.
     * 
     * @param agreementComponent
     *            the agreementComponent to set
     */
    public final void setAgreementComponent(final AgreementComponent agreementComponent) {
        this.agreementComponent = agreementComponent;
    }

    /**
     * Gets the party component.
     * 
     * @return the partyComponent
     */
    public final PartyComponent getPartyComponent() {
        return partyComponent;
    }

    /**
     * Sets the party component.
     * 
     * @param partyComponent
     *            the partyComponent to set
     */
    public final void setPartyComponent(final PartyComponent partyComponent) {
        this.partyComponent = partyComponent;
    }
    
    protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(String operation){
    	List<AgreementStatusCodeList> statusCodeLists=new ArrayList<AgreementStatusCodeList>();
    	if(Constants.FIRST_TIME_SYNC.equals(operation)||Constants.RETRIEVE.equals(operation)){    		
    		statusCodeLists.add(AgreementStatusCodeList.Final);
    	}else if(Constants.RETRIEVE_ALL.equals(operation)||
    			Constants.UPDATE_OP.equals(operation)){
    		statusCodeLists.add(AgreementStatusCodeList.Final);
    		statusCodeLists.add(AgreementStatusCodeList.Cancelled);
    	}else if(Constants.RETRIEVE_BY_FILTER.equals(operation)||Constants.RETRIEVE_BY_COUNT.equals(operation)){
    		statusCodeLists.add(AgreementStatusCodeList.Initial);
    		statusCodeLists.add(AgreementStatusCodeList.Final);
    		statusCodeLists.add(AgreementStatusCodeList.Proposed);
    	}
    	
    	return statusCodeLists;
    }
    /* Get illustration agreement.
    * 
    * @param transactions
    *            the transactions
    * @param transactionId
    *            the transaction id
    * @return the illustration agreement
    */

   public InsuranceAgreement getIllustrationAgreementByTransTrackingId(final Transactions transactions,
           String transactionId) {
       final List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();
       statusList.add(AgreementStatusCodeList.Final);
       statusList.add(AgreementStatusCodeList.InProgress);
       final Request<Agreement> request =
               new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.ILLUSTRATION, transactionId);
       final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
       insuranceAgreement.setTransTrackingId(transactions.getTransTrackingId());
       insuranceAgreement.setAgentId(transactions.getAgentId());
       request.setType(insuranceAgreement);

       final Request<List<AgreementStatusCodeList>> statusRequest =
               new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                       transactionId);
       statusRequest.setType(statusList);

       final Response<Agreement> agreementResponse =
               agreementComponent.retrieveAgreementForTransTrackingId(request, statusRequest);
       final InsuranceAgreement agreement = (InsuranceAgreement) agreementResponse.getType();
       return agreement;
   }
   
   /* Get latest illustration agreement.
    * 
    * @param transactions
    *            the transactions
    * @param transactionId
    *            the transaction id
    * @return the latest illustration agreement
    */
    public InsuranceAgreement getLatestIllustrationAgreement(final Transactions transactions,
           String transactionId) {
       
       final Request<Agreement> request =
               new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.ILLUSTRATION, transactionId);
       final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
       insuranceAgreement.setTransTrackingId(transactions.getTransTrackingId());
       insuranceAgreement.setTransactionId(transactionId);
       request.setType(insuranceAgreement);    

       final Response<Agreement> agreementResponse =
               agreementComponent.retrieveLatestAgreement(request);
       final InsuranceAgreement agreement = (InsuranceAgreement) agreementResponse.getType();
       return agreement;
   } 
   
}
