package com.cognizant.insurance.executor;

import org.json.JSONObject;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.component.LifeEngageEmailComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.EmailRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.request.vo.StatusData;

/**
 * @Class LifeEngageMailManager
 * @Desc To execute email notification in an async manner
 *
 */

	public class LifeEngageMailExecutor implements Tasklet, StepExecutionListener{

		@Autowired
		private LifeEngageEmailComponent lifeEngageEmailComponent;
		
		@Autowired
		private EmailRepository emailRepository;
		
		/** The id. */
		private Long id;
		
		/** The email json. */
		private String emailJson = null;
		
		/** The attempt Number. */
		private String attemptNo;
		
		/** The e app pdf name. */
	    @Value("${le.platform.service.email.pdfName.eApp}")
	    private String eAppPDFName;

	    /** The illustration pdf name. */
	    @Value("${le.platform.service.email.pdfName.illustration}")
	    private String illustrationPDFName;

	    /** The e app template name. */
	    @Value("${le.platform.service.email.templateName.eApp}")
	    private String eAppTemplateName;

	    /** The illustration template name. */
	    @Value("${le.platform.service.email.templateName.illustration}")
	    private String illustrationTemplateName;

	    /** The fna template name. */
	    @Value("${le.platform.service.email.templateName.fna}")
	    private String fnaTemplateName;
		
		/** The Constant EMAIL_TEMPLATE. */
	    private static final String EMAIL_TEMPLATE = "emailTemplate";
	    
	    /** The Constant AGENT ID. */
	    private static final String AGENT_ID = "agentId";
		
		/**
	     * Gets the id.
	     *
	     * @return the id
	     */
		public Long getId() {
			return id;
		}

		/**
	     * Sets the id.
	     *
	     * @param id the id to set
	     */
		public void setId(Long id) {
			this.id = id;
		}

		/**
	     * Gets the email json.
	     *
	     * @return the email json
	     */
		public String getEmailJson() {
			return emailJson;
		}
		
		/**
	     * Sets the email json.
	     *
	     * @param email values the email json to set
	     */
		public void setEmailJson(String emailJson) {
			this.emailJson = emailJson;
		}

		/**
	     * Gets the attempt number.
	     *
	     * @return the attempt number
	     */
		public String getAttemptNo() {
			return attemptNo;
		}
		
		/**
	     * Sets the attempt number.
	     *
	     * @param attempt number the attempt number to set
	     */
		public void setAttemptNo(String attemptNo) {
			this.attemptNo = attemptNo;
		}

		@Override
		public void beforeStep(StepExecution stepExecution) {
			
		}
	
		@Override
		public ExitStatus afterStep(StepExecution stepExecution) {
			return null;
		}
		
		/**
		 * executes the batch job
		 * 
		 * @param arg0
		 *            the Step Contribution
		 * @param chunkContext
		 *            the chunk Context
		 */
		@Override
		public RepeatStatus execute(StepContribution contribution,
				ChunkContext chunkContext) throws Exception {
			String statusFlag;
			try {
				JobExecution jobExecution = chunkContext.getStepContext().getStepExecution().getJobExecution();
				Long jobId = jobExecution.getJobId();
	            String templateName = null;
	            String pdfName = null;
	            final JSONObject emailJSONObj = new JSONObject(emailJson);
	            final JSONObject templateObj = emailJSONObj.getJSONObject(EMAIL_TEMPLATE);
	            final String type = emailJSONObj.getString(Constants.TYPE);				
	            String agentId = new JSONObject(emailJson).getString(AGENT_ID);
	            if (Constants.EAPP.equals(type)) {
	                templateName = eAppTemplateName;
	                pdfName = eAppPDFName;
	            } else if (Constants.ILLUSTRATION.equals(type)) {
	                templateName = illustrationTemplateName;
	                pdfName = illustrationPDFName;
	            } else if (Constants.FNA.equals(type)) {
	                templateName = fnaTemplateName;
	                pdfName = emailJSONObj.getString(Constants.PDF_NAME);
	            }
				StatusData statusData= lifeEngageEmailComponent.sendEmail(emailJson,templateName,templateObj.toString(),pdfName);
                statusFlag = statusData.getStatus();
                final LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
                lifeEngageEmail.setSendTime(LifeEngageComponentHelper.getCurrentdate());
                lifeEngageEmail.setId(id);
                lifeEngageEmail.setEmailValues(emailJson);
                lifeEngageEmail.setStatus(statusFlag);
                lifeEngageEmail.setAttemptNumber(attemptNo);
                lifeEngageEmail.setStatusMessage(statusData.getStatusMessage());
                lifeEngageEmail.setBatchJobId(jobId);
                lifeEngageEmail.setType(type);
                lifeEngageEmail.setAgentId(agentId);		
				if (Constants.SUCCESS.equals(statusFlag))
				{
					statusFlag = Constants.SUCCESS;
					emailRepository.updatelifeEngageEmail(lifeEngageEmail);
				}else{
					statusFlag = Constants.FAILURE;
					emailRepository.updatelifeEngageEmail(lifeEngageEmail);
				}
			}catch (Exception e) {
			}
			return null;
		}
}
