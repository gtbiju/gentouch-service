/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of ContactPreferences according to their
 * preferred medium.
 * 
 * @author 301350
 * 
 * 
 */
public enum CommunicationMediumCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    ClaimForm,
    /**
     * 
     * 
     * 
     * 
     */
    Email,
    /**
     * 
     * 
     * 
     * 
     */
    FaceToFace,
    /**
     * 
     * 
     * 
     * 
     */
    Fax,
    /**
     * 
     * 
     * 
     * 
     */
    Internet,
    /**
     * 
     * 
     * 
     * 
     */
    Mail,
    /**
     * Short message service.
     * 
     * 
     * 
     */
    SMS,
    /**
     * 
     * 
     * 
     * 
     */
    Telephone,
    /**
     * 
     * 
     * 
     * 
     */
    TelephoneDirect,
    /**
     * Wireless access protocol - such as a WAP-enabled phone.
     * 
     * 
     * 
     */
    WAP
}
