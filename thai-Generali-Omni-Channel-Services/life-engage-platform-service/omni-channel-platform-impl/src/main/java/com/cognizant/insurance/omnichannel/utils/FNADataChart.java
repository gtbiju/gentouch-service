package com.cognizant.insurance.omnichannel.utils;

import java.util.List;

public class FNADataChart {
	
	private Integer valueIDR;	

	private Long savingsArrayValue;
	
	private String goalName;
	
	private String productName;
	
	private Integer number;	
	
	private String selectedGoalName;
	
	private String unSelectedGoal;
	
	private String priority;
	
	private String goalNamePrint;
	
	private String productCodeValue;
	
	private String productRecomAdd;
	
	private String imageIcon;
	
	private Long xaxisValue;
	
	public String getGoalNamePrint() {
		return goalNamePrint;
	}

	public void setGoalNamePrint(String goalNamePrint) {
		this.goalNamePrint = goalNamePrint;
	}

	public String getUnSelectedGoal() {
		return unSelectedGoal;
	}

	public void setUnSelectedGoal(String unSelectedGoal) {
		this.unSelectedGoal = unSelectedGoal;
	}

	public String getSelectedGoalName() {
		return selectedGoalName;
	}

	public void setSelectedGoalName(String selectedGoalName) {
		this.selectedGoalName = selectedGoalName;
	}

	private List<String> productCodeMain;
	
	private List<String> productCodeAdd;
	
	private boolean iPLANImage;
	
	private boolean uBRichImage;
	
	public boolean isiPLANImage() {
		return iPLANImage;
	}

	public void setiPLANImage(boolean iPLANImage) {
		this.iPLANImage = iPLANImage;
	}

	public boolean isuBRichImage() {
		return uBRichImage;
	}

	public void setuBRichImage(boolean uBRichImage) {
		this.uBRichImage = uBRichImage;
	}

	public Long getSavingsArrayValue() {
		return savingsArrayValue;
	}

	public void setSavingsArrayValue(Long savingsArrayValue) {
		this.savingsArrayValue = savingsArrayValue;
	}

	public Integer getValueIDR() {
		return valueIDR;
	}

	public void setValueIDR(Integer valueIDR) {
		this.valueIDR = valueIDR;
	}
	

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<String> getProductCodeMain() {
		return productCodeMain;
	}

	public void setProductCodeMain(List<String> productCodeMain) {
		this.productCodeMain = productCodeMain;
	}

	public List<String> getProductCodeAdd() {
		return productCodeAdd;
	}

	public void setProductCodeAdd(List<String> productCodeAdd) {
		this.productCodeAdd = productCodeAdd;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getGoalName() {
		return goalName;
	}

	public void setGoalName(String goalName) {
		this.goalName = goalName;
	}

	public String getProductCodeValue() {
		return productCodeValue;
	}

	public void setProductCodeValue(String productCodeValue) {
		this.productCodeValue = productCodeValue;
	}

	public String getProductRecomAdd() {
		return productRecomAdd;
	}

	public void setProductRecomAdd(String productRecomAdd) {
		this.productRecomAdd = productRecomAdd;
	}

	public String getImageIcon() {
		return imageIcon;
	}

	public void setImageIcon(String imageIcon) {
		this.imageIcon = imageIcon;
	}

    public void setXaxisValue(Long xaxisValue) {
        this.xaxisValue = xaxisValue;
    }

    public Long getXaxisValue() {
        return xaxisValue;
    }


	

		
}
