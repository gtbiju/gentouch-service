/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists;

/**
 * This list defines types of documents.
 * 
 * @author 301350
 * 
 * 
 */
public enum DocumentTypeCodeList {

    /** The Electronic. */
    Electronic,

    /** The Generated. */
    Generated,

    /** The Physical. */
    Physical,

    /** The Photograph. */
    Photograph,

    /** The Audio recording. */
    AudioRecording,

    /** The Video recording. */
    VideoRecording,

    /** The Not mentioned. */
    NotMentioned,

    /** Added for eapp -start. */
    IncomeProof,

    /** The Age proof. */
    AgeProof,

    /** The Signature. */
    Signature,

    /** Address proof */
    AddressProof,

    /** Id proof. */
    IdentityProof,

    /** Medical Records */
    MedicalReports,

    NRIQuestionnaire,

    OccupationQuestionnaire,

    DiscrepencyResolution,

    AgentConfidentialityReport,

    CreditOrDebitCardMandate,

    HighSumAssuredQuestionnaire,

    PANDecalration,
    
    MedicalRecords,
    
    FATCA,
    
    MedicalDocument,
    
    FATCAForms,
    
    PayorRecords,
    
    DeclarationDocuments,
    
    Financialstatement,
    
    FinancialQuestionnaire,
    
    Others,
    
    FinancialRecords,
    
    FinancialRecord,
    
    PaymentRecord,
    
    Declaration,
	
	BPMPdf,
	SPAJ,
	Illustrasi,
	
	/**
	 * Added for generali vietnam
	 */
	Quest9AgentFin,
	Quest10ClientFin,
	InsuranceCoverages,
	MedicalRecord,
	Quest1Hypertension,
	Quest2chestpain,
	Quest3Hyperthyroidism,
	Quest4Diabetes,
	Quest5Cardiovascular,
	Quest6Respiratory,
	Quest7Digestive,
	Quest8Tumor,
	AdditionalInsuredPdf,
	MainInsuredPdf,
	ACRPdf,
	EappConfirmation

    

}