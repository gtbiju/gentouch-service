package com.cognizant.insurance.agent.component;

import java.util.List;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.generali.security.entity.PasswordHistory;


public interface ODSUserComponent {
	
	public Response<GeneraliAgent> validateAgentProfile(Request<Transactions> request);
	
	public void resetPassword(Request<GeneraliTHUser> request);
	
	public void saveLoginDetails(Request<GeneraliTHUser> request);
	
	public void saveHistory(Request<PasswordHistory> request);
	
	public Response<GeneraliTHUser> validateLoggedInUser(Request<Transactions> request);
	
	public Response<List<PasswordHistory>> getRecentPasswords(Request<String> userRequest, Request<Integer> historyRequest);

	public Response<PasswordHistory> getOldPasswordHistory(Request<String> userRequest);

	public void removeOldHistory(Request<PasswordHistory> request);
}
