/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.agent.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;

import com.cognizant.insurance.agent.audit.LifeEngageAudit;
import com.cognizant.insurance.agent.audit.LifeEngagePayloadAudit;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.agent.component.AuditComponent;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.request.impl.JPARequestImpl;
import com.cognizant.insurance.agent.request.vo.RequestInfo;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.agent.audit.LEDataWipeAudit;

import org.apache.commons.collections.CollectionUtils;

import java.util.Date;

/**
 * The Class class AuditRepository.
 * 
 * @author 300797
 */
@Repository
public class AuditRepository {
	
	private static final String LE_TXN_MANAGER = "le_txn_manager";
	
    /** The entity manager. */
	@PersistenceContext (unitName = "LE_Platform")
    private EntityManager entityManager;
    
    @PersistenceContext (unitName = "ODS_unit")
    private EntityManager odsEntityManager;

    /** The party component. */
    @Autowired
    private AuditComponent auditComponent;

    /**
     * Savelife engage audit.
     * 
     * @param requestInfo
     *            the request info
     * @param json
     *            the json
     * @return the life engage audit
     */
    @Transactional(LE_TXN_MANAGER)
    public final LifeEngageAudit savelifeEngageAudit(final RequestInfo requestInfo, final String json) {
        final Request<LifeEngageAudit> request =
                new JPARequestImpl<LifeEngageAudit>(entityManager, ContextTypeCodeList.LIFE_ENGAGE,
                        requestInfo.getTransactionId());

        final LifeEngageAudit lifeEngageAudit = new LifeEngageAudit();
        lifeEngageAudit.setCreationDateandTime(requestInfo.getCreationDateTime());
        lifeEngageAudit.setRequestJson(json);
        lifeEngageAudit.setSourceInfoName(requestInfo.getSourceInfoName());
        lifeEngageAudit.setTransactionId(requestInfo.getTransactionId());
        lifeEngageAudit.setUserName(requestInfo.getUserName());

        request.setType(lifeEngageAudit);

        //auditComponent.save(request);
        return lifeEngageAudit;
    }

    /**
     * Save life engage payload audit.
     * 
     * @param transactions
     *            the transactions
     * @param transactionId
     *            the transaction id
     * @param json
     *            the json
     * @param lifeEngageAudit
     *            the life engage audit
     */
    @Transactional(LE_TXN_MANAGER)
    public final void savelifeEngagePayloadAudit(final Transactions transactions, final String transactionId,
            final String json, final LifeEngageAudit lifeEngageAudit) {
        final Request<LifeEngagePayloadAudit> request =
                new JPARequestImpl<LifeEngagePayloadAudit>(entityManager, ContextTypeCodeList.LIFE_ENGAGE,
                        transactionId);

        final Request<LifeEngageAudit> lifeEngaeAuditRequest =
                new JPARequestImpl<LifeEngageAudit>(entityManager, ContextTypeCodeList.LIFE_ENGAGE, transactionId);
        lifeEngaeAuditRequest.setType(lifeEngageAudit);
        final Response<LifeEngageAudit> response = auditComponent.getLifeEngageAudit(lifeEngaeAuditRequest);

        final LifeEngagePayloadAudit lifeEngagePayloadAudit = new LifeEngagePayloadAudit();
        if ("LMS".equals(transactions.getType())) {
            lifeEngagePayloadAudit.setKey1(transactions.getKey1());
            lifeEngagePayloadAudit.setKey2(transactions.getKey2());
            lifeEngagePayloadAudit.setKey3(transactions.getKey3());
            lifeEngagePayloadAudit.setKey4(transactions.getKey4());
            lifeEngagePayloadAudit.setKey5(transactions.getKey5());
        } else {
            lifeEngagePayloadAudit.setKey1(transactions.getCustomerId());
            lifeEngagePayloadAudit.setKey2(transactions.getFnaId());
            lifeEngagePayloadAudit.setKey3(transactions.getIllustrationId());
            lifeEngagePayloadAudit.setKey4(transactions.getProposalNumber());
            lifeEngagePayloadAudit.setKey5(transactions.getProductId());

        }

        lifeEngagePayloadAudit.setOfflineIdentifier(transactions.getOfflineIdentifier());
        lifeEngagePayloadAudit.setKey6(transactions.getKey6());
        lifeEngagePayloadAudit.setKey7(transactions.getKey7());
        lifeEngagePayloadAudit.setKey8(transactions.getKey8());

        lifeEngagePayloadAudit.setKey11(transactions.getAgentId());
        lifeEngagePayloadAudit.setKey12(transactions.getLastSyncDate());
        lifeEngagePayloadAudit.setKey13(transactions.getCreationDateTime());
        lifeEngagePayloadAudit.setKey14(transactions.getModifiedTimeStamp());
        lifeEngagePayloadAudit.setKey15(transactions.getStatus());
        lifeEngagePayloadAudit.setKey16(transactions.getSyncStatus());
        lifeEngagePayloadAudit.setKey17(transactions.getSyncError());
        lifeEngagePayloadAudit.setTransTrackingID(transactions.getTransTrackingId());
        lifeEngagePayloadAudit.setType(transactions.getType());
        lifeEngagePayloadAudit.setRequestHashCode(json.hashCode());
        lifeEngagePayloadAudit.setLifeEngageAudit(response.getType());

		lifeEngagePayloadAudit.setKey21(transactions.getKey21());
        lifeEngagePayloadAudit.setKey22(transactions.getKey22());
        lifeEngagePayloadAudit.setKey23(transactions.getKey23());
        lifeEngagePayloadAudit.setKey24(transactions.getKey24());
        lifeEngagePayloadAudit.setKey25(transactions.getKey25());
        request.setType(lifeEngagePayloadAudit);

        auditComponent.save(request);
    }

    /**
     * Savelife engage payload audit without keys.
     * 
     * @param transactions
     *            the transactions
     * @param transactionId
     *            the transaction id
     * @param json
     *            the json
     * @param lifeEngageAudit
     *            the life engage audit
     */
    @Transactional(LE_TXN_MANAGER)
    public final void savelifeEngagePayloadAuditWithoutKeys(final Transactions transactions,
            final String transactionId, final String json, final LifeEngageAudit lifeEngageAudit) {
        final Request<LifeEngagePayloadAudit> request =
                new JPARequestImpl<LifeEngagePayloadAudit>(entityManager, ContextTypeCodeList.LIFE_ENGAGE,
                        transactionId);

        final Request<LifeEngageAudit> lifeEngaeAuditRequest =
                new JPARequestImpl<LifeEngageAudit>(entityManager, ContextTypeCodeList.LIFE_ENGAGE, transactionId);
        lifeEngaeAuditRequest.setType(lifeEngageAudit);
        final Response<LifeEngageAudit> response = auditComponent.getLifeEngageAudit(lifeEngaeAuditRequest);

        final LifeEngagePayloadAudit lifeEngagePayloadAudit = new LifeEngagePayloadAudit();
        lifeEngagePayloadAudit.setTransTrackingID(transactions.getTransTrackingId());
        lifeEngagePayloadAudit.setType(transactions.getType());
        lifeEngagePayloadAudit.setRequestHashCode(json.hashCode());
        lifeEngagePayloadAudit.setLifeEngageAudit(response.getType());

        request.setType(lifeEngagePayloadAudit);

        auditComponent.save(request);
    }

    /**
     * Checks if is transaction already processed.
     * 
     * @param transactions
     *            the transactions
     * @param transactionId
     *            the transaction id
     * @param json
     *            the json
     * @return true, if is transaction already processed
     */
    @Transactional(LE_TXN_MANAGER)
    public final boolean isTransactionAlreadyProcessed(final Transactions transactions, final String transactionId,
            final String json) {
        boolean result;
        final Request<LifeEngagePayloadAudit> request =
                new JPARequestImpl<LifeEngagePayloadAudit>(entityManager, ContextTypeCodeList.LIFE_ENGAGE,
                        transactionId);

        final LifeEngagePayloadAudit lifeEngagePayloadAudit = new LifeEngagePayloadAudit();
        final Response<List<LifeEngagePayloadAudit>> response;

        if ("LMS".equals(transactions.getType())) {
            lifeEngagePayloadAudit.setKey1(transactions.getKey1());
            lifeEngagePayloadAudit.setKey2(transactions.getKey2());
            lifeEngagePayloadAudit.setKey4(transactions.getKey4());
            lifeEngagePayloadAudit.setTransTrackingID(transactions.getTransTrackingId());
            lifeEngagePayloadAudit.setType(transactions.getType());
            lifeEngagePayloadAudit.setRequestHashCode(json.hashCode());
            request.setType(lifeEngagePayloadAudit);
            response = auditComponent.retrieveLMSPayloadAudit(request);

        } else {
            lifeEngagePayloadAudit.setKey1(transactions.getCustomerId());
            lifeEngagePayloadAudit.setKey2(transactions.getFnaId());

            lifeEngagePayloadAudit.setKey3(transactions.getIllustrationId());
            lifeEngagePayloadAudit.setKey4(transactions.getProposalNumber());
            lifeEngagePayloadAudit.setKey5(transactions.getProductId());
            lifeEngagePayloadAudit.setKey6(transactions.getKey6());
            lifeEngagePayloadAudit.setKey7(transactions.getKey7());
            lifeEngagePayloadAudit.setKey8(transactions.getKey8());

            lifeEngagePayloadAudit.setKey11(transactions.getAgentId());
            lifeEngagePayloadAudit.setKey13(transactions.getCreationDateTime());
            lifeEngagePayloadAudit.setKey14(transactions.getModifiedTimeStamp());
            lifeEngagePayloadAudit.setTransTrackingID(transactions.getTransTrackingId());
            lifeEngagePayloadAudit.setType(transactions.getType());
            lifeEngagePayloadAudit.setRequestHashCode(json.hashCode());
            request.setType(lifeEngagePayloadAudit);
            response = auditComponent.retrieveLifeEngagePayloadAudit(request);

        }

        if (response.getType() != null && response.getType().size() > 0) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Gets the last life engage payload audit.
     * 
     * @param proposalNumber
     *            the proposal number
     * @return the last life engage payload audit
     */
    @Transactional(LE_TXN_MANAGER)
    public LifeEngagePayloadAudit getLastLifeEngagePayloadAudit(String proposalNumber) {
        final Request<LifeEngagePayloadAudit> request =
                new JPARequestImpl<LifeEngagePayloadAudit>(entityManager, ContextTypeCodeList.LIFE_ENGAGE, "");
        LifeEngagePayloadAudit lifeEngagePayloadAudit = new LifeEngagePayloadAudit();
        lifeEngagePayloadAudit.setKey6(proposalNumber);
        request.setType(lifeEngagePayloadAudit);
        Response<LifeEngagePayloadAudit> response = auditComponent.retrieveLastLifeEngagePayloadAudit(request);

        return response.getType();
    }

    /**
     * Gets the entity manager.
     * 
     * @return the entityManager
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Sets the entity manager.
     * 
     * @param entityManager
     *            the entityManager to set
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Gets the audit component.
     * 
     * @return the auditComponent
     */
    public final AuditComponent getAuditComponent() {
        return auditComponent;
    }

    /**
     * Sets the audit component.
     * 
     * @param auditComponent
     *            the auditComponent to set
     */
    public final void setAuditComponent(final AuditComponent auditComponent) {
        this.auditComponent = auditComponent;
    }
    
    /* Added for LE_datawipe functionality */ 
    /**
     * Save data wipe audit.
     * 
     * @param transactions
     *            the transactions
     * @throws ParseException
     *             the parse exception
     */
    @Transactional(LE_TXN_MANAGER)
    public void saveDataWipeAudit(final Transactions transactions) throws ParseException {
        final Request<LEDataWipeAudit> request =
                new JPARequestImpl<LEDataWipeAudit>(entityManager, ContextTypeCodeList.LIFE_ENGAGE, "");
        LEDataWipeAudit auditForSave;
        Date currentDate = LifeEngageComponentHelper.getCurrentdate();
        if (CollectionUtils.isNotEmpty(transactions.getUserIds())) {
            for (String userId : transactions.getUserIds()) {
                auditForSave = new LEDataWipeAudit();
                auditForSave.setDeviceID(transactions.getDataWipeAudit().getDeviceID());
                auditForSave.setDeviceModel(transactions.getDataWipeAudit().getDeviceModel());
                auditForSave.setDeviceType(transactions.getDataWipeAudit().getDeviceType());
                auditForSave.setOsVersion(transactions.getDataWipeAudit().getOsVersion());
                auditForSave.setReason(transactions.getDataWipeAudit().getReason());
                auditForSave.setDataWipeDateAndTime(currentDate);
                // audit.setId(null);
                auditForSave.setUserName(userId);

                request.setType(auditForSave);
                auditComponent.save(request);
            }
        }
    }
    /* Added for LE_datawipe functionality */

}
