/**
*
* Copyright 2012, Cognizant 
*
* @author        : 300797
* @version       : 0.1, Aug 8, 2013
*/
package com.cognizant.insurance.component.repository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.AutoEtrGenerateAudit;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.generic.component.AutoEtrGenerateAuditComponent;
import com.cognizant.insurance.pushnotification.CompositeKey;
import com.cognizant.insurance.pushnotification.DeviceRegister;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.service.impl.LifeEngageSyncServiceImpl;

/**
* The Class class AuditRepository.
* 
* @author 300797
*/

public class AutoEtrGenerateAuditRepository {
  
	/** The Constant KEY9. */
   private static final String KEY9 = "Key9";
   /** The Constant KEY15. */
   private static final String KEY15 = "Key15";
   /** The Constant KEY18. */
   private static final String KEY18 = "Key18";
   /** The Constant KEY20. */
   private static final String KEY20 = "Key20";
   
   /** The Constant LOGGER. */
   public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageSyncServiceImpl.class);
	
	/** The entity manager. */
   @PersistenceContext(unitName="LE_Platform")
	private EntityManager entityManager;

   /** The party component. */
  
   @Autowired
   private AutoEtrGenerateAuditComponent autoEtrGenerateAuditComponent; 

  
  public final AutoEtrGenerateAudit savePaymentDetails(AutoEtrGenerateAudit autoEtrGenerateAudit) {
	   final Request<AutoEtrGenerateAudit> request =
               new JPARequestImpl<AutoEtrGenerateAudit>(entityManager, null, "");
	

       request.setType(autoEtrGenerateAudit);

       autoEtrGenerateAuditComponent.save(request);
       return autoEtrGenerateAudit;
   }
  
 	public List<AutoEtrGenerateAudit> retrieveAutoEtrGenerateAudit(String eappNo) {
 		List<AutoEtrGenerateAudit> autoEtrGenerateAudit = autoEtrGenerateAuditComponent.retrieveAutoEtrGenerateAudit(eappNo);
 		
 		return autoEtrGenerateAudit;
 	/*	List<AutoEtrGenerateAudit> autoEtrGenerateAudit = autoEtrGenerateAuditComponent.retrieveAutoEtrGenerateAudit(eappNo);
 		return autoEtrGenerateAudit;*/ 		
 	}
  
 	public void updateAutoETROnSave(final RequestInfo requestInfo, final Transactions transactions,AutoEtrGenerateAudit autoEtrGenerateAudit) throws ParseException {

        final Request<AutoEtrGenerateAudit> autoEtrGenerateAuditRQ =
                new JPARequestImpl<AutoEtrGenerateAudit>(entityManager, null,
                        "");
     
        autoEtrGenerateAuditRQ.setType(autoEtrGenerateAudit);
        autoEtrGenerateAuditComponent.updateAutoETROnSave(autoEtrGenerateAuditRQ);

    }
 	
}
