package com.cognizant.insurance.omnichannel.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngageAuditComponent;
import com.cognizant.insurance.component.LifeEngageEAppComponent;
import com.cognizant.insurance.component.LifeEngageFNAComponent;
import com.cognizant.insurance.component.LifeEngageIllustrationComponent;
import com.cognizant.insurance.component.LifeEngageLMSComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.EmailRepository;
import com.cognizant.insurance.component.repository.ProductRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliEAppComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliPushNotificationsComponent;
import com.cognizant.insurance.omnichannel.component.LifeAsiaComponent;
import com.cognizant.insurance.omnichannel.component.impl.GeneraliLifeEngageFNAComponentImpl;
import com.cognizant.insurance.omnichannel.component.impl.LifeAsiaProcessEngine;
import com.cognizant.insurance.omnichannel.service.GeneraliSyncService;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.service.impl.LifeEngageSyncServiceImpl;

/**
 * The Class GeneraliSyncServiceImpl.
 */
public class GeneraliSyncServiceImpl extends LifeEngageSyncServiceImpl implements GeneraliSyncService{

    /** The Constant KEY11. */
    private static final String KEY11 = "Key11";

    /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = "TransactionData";

    /** The Constant EMAIL. */
    private static final String EMAIL = "Email";

    /** The Constant EMAIL_ID. */
    private static final String EMAIL_ID = "toMailIds";

    /** The Constant CC_EMAIL_ID. */
    private static final String CC_EMAIL_ID = "ccMailIds";

    /** The Constant EMAIL_FROM_ADDRESS. */
    private static final String EMAIL_FROM_ADDRESS = "fromMailId";

    /** The Constant PRODUCT. */
    private static final String PRODUCT = "Product";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant TEMPLATES. */
    private static final String TEMPLATES = "templates";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";

    private static final String GENERALI_THAILAND = "Generali Thailand-";
    
    private static final String EMAILSUBJECT = "mailSubject";

    private static final String QUOTATION = "ใบเสนอราคา  ";

    private static final String FORYOU = "สำหรับคุณ ";

    /** The Constant PRODUCT_DETAILS. */
    private static final String PRODUCT_DETAILS = "ProductDetails";

    /** The Constant PRODUCT_NAME. */
    private static final String PRODUCT_NAME = "productName";

    /** The Constant MAIL_SUBJECT. */
    private static final String MAIL_SUBJECT = "mailSubject";

    /** The Constant AGENT ID. */
    private static final String AGENT_ID = "agentId";
   
    /** The Constant Key4. */
    private static final String KEY16 = "Key16";
    
    /** The Constant KEY3. */
    private static final String KEY3 = "Key3";

    /** The Constant ID. */
    private static final String ID = "id";
    
    /** The Constant ILLUSTRATION_ID. */
    private static final String ILLUSTRATION_ID = "illustrationId";
    
    /** The Constant SPAJ_NUMBER. */
    private static final String SPAJ_NUMBER = "spajNumber";
    
    /** The Constant AdditionalQuestionnaire_case. */
    private static final String IS_ADDITIONAL_QUESTIONNAIRE_CASE = "isAdditionalQuestionnaireCase";
    
    /** The Constant SUCCESS_MESSAGE_SAVE. */
    private static final String SUCCESS_MESSAGE_SAVE = "Lead Saved";
   
    /** The Constant UPDATE_MESSAGE_SAVE. */
    private static final String UPDATE_MESSAGE_SAVE = "Lead Updated";

    /** The Constant EMAIL_TEMPLATE. */
    private static final String EMAIL_TEMPLATE = "emailTemplate";

    /** The Constant INITIALISED. */
    private static final String INITIALISED = "initialised";

    /** The Constant VT. */
    private static final String VT = "vt";
    
    private static final String TH = "th";

    /** The Constant EN. */
    private static final String EN = "en";

    /** The Constant LANGUAGE **/
    private static final String LANGUAGE = "language";

    /** The Constant PREMIUMSUMMARY **/
    private static final String PREMIUMSUMMARY = "premiumSummary";

    /** The Constant TOTALPREMIUM **/
    private static final String TOTALPREMIUM = "totalAnnualPremium";

    /** The Constant TEMPLATE_IN **/
    private static final String TEMPLATE_VT = "template_vt";
    
    private static final String TEMPLATE_TH = "template_th";
    
    /** The Constant TEMPLATE_ILLUSTRATION_COPY to attch with **/
    private static final String TEMPLATE_ILLUSTRATION_COPY = "template_illustration_copy";

    /** The Constant TEMPLATE_EN **/
    private static final String TEMPLATE_EN = "template_en";
    
    /** The Constant LEAD_NAME **/
    private static final String LEAD_NAME = "leadName";

    /** The Constant AGENT_NAME **/
    private static final String AGENT_NAME = "agentName";

    /** The Constant AGENT_EMAIL_ID **/
    private static final String AGENT_EMAIL_ID = "agentEmailId";

    /** The Constant DAY **/
    private static final String DAY = "day";

    /** The Constant DATE **/
    private static final String DATE = "date";

    /** The Constant TIME **/
    private static final String TIME = "time";

    /** The Constant daysEN **/
    private static final String[] daysEN = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
            "Saturday" };

    /** The Constant daysIN **/
    private static final String[] daysIN = { "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu" };

    /** The Constant DATE_FORMAT **/
    private static final String DATE_FORMAT = "dd/MM/yyyy";

    /** The Constant TIME_FORMAT **/
    private static final String TIME_FORMAT = "HH:mm:ss";
    
    /** The Constant FNA. */
    private static final String FNA = "FNA";
    
    /** The Constant FNA_PDF_TEMPLATE. */
    private static final String FNA_PDF_TEMPLATE = "fnaPdfTemplate";
    
    /** The Constant KEY2. */
    private static final String KEY2 = "Key2";

    /** The Constant PRODUCTCODE */
    private static final String PRODUCT_CODE = "productCode"; 
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";
    
    private static final String INSURED = "Insured";
    
    private static final String BASICDETAILS = "BasicDetails";


    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";
    private static final String ILLUSTRATION_OUTPUT = "IllustrationOutput";
    private static final String KEY18 = "Key18";
    private static final String LMS = "LMS";
    private static final String KEY15 = "Key15";
    

    /** The Constant OBSERVATIONS. */
    private static final String OBSERVATION = "Observation";
    
    /** The Constant RDS_ILLUS_ID */
    private static final String RDS_ILLUS_ID = "RDS_ILLUS_ID";
    
    //Added for Thailand
    private static final String FULL_NAME = "fullName";

    @Autowired
    private GeneraliEAppComponent lifeEngageEAppComponent;

    /** The life engage illustration component. */
    @Autowired
    private LifeEngageIllustrationComponent lifeEngageIllustrationComponent;

    /** The life engage FNA component. */
    @Autowired
    @Qualifier("generaliLifeEngageFNAComponent")
    private LifeEngageFNAComponent lifeEngageFNAComponent;

    /** The life engage e LMS component. */
    @Autowired
    private LifeEngageLMSComponent lifeEngageLMSComponent;

    /** The life engage audit component. */
    @Autowired
    private LifeEngageAuditComponent lifeEngageAuditComponent;

    @Autowired
    private EmailRepository emailRepository;
    
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private GeneraliPushNotificationsComponent pushNotificationsComponent;
    
    @Autowired
	private TaskExecutor pushNotificationThreadPool;
   
    /** The Constant Key4. */
    private static final String KEY4 = "Key4";
    @Autowired
	LifeAsiaProcessEngine engine;
    
    @Autowired
    private LifeAsiaComponent lifeAsiaComponent;
    
    /** pushNotification Message. */
    @Value("${generali.platform.leadReassignment.pushNotification.message}")
    private String pushNotificationMessage;

    @Value("${generali.platform.service.fnaEmail.subject}")
    private String fnaMailSubject;
    
    @Override
    public LifeEngageEmail savelifeEngageEmail(JSONObject jsonObj, String response) throws ParseException {

        LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
        String syncStatus = (String) new JSONObject(response).get(KEY16);
        if (LifeEngageComponentHelper.SUCCESS.equals(syncStatus)) {

            final JSONObject emailObject = new JSONObject();
            final JSONObject emailTemplateObj = new JSONObject();

            String agentId = (String) jsonObj.getString(KEY11);
            String ccMailIds = "";
            String language = "";
            final JSONObject emailObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(EMAIL);
            String toMailIds = (String) emailObj.getString(EMAIL_ID);
            if("undefined".equals(toMailIds)){
    			toMailIds="";
    		}
            if (emailObj.has(CC_EMAIL_ID)) {
                ccMailIds = (String) emailObj.getString(CC_EMAIL_ID);
            }
            if("undefined".equals(ccMailIds)){
            	ccMailIds="";
    		}
            // String fromMailId = (String) emailObj.getString(EMAIL_FROM_ADDRESS);
            String fromMailId = "";
            if (emailObj.has(LANGUAGE)) {
                language = (String) emailObj.getString(LANGUAGE);
            }
            String agentName = "";
            String agentEmailId = "";
            String leadName = "";
            String fullName = "";
           
            String type = "";
            String id = "";
            String illustrationId = "";
            String spajNumber = "";
            String additionalQuestionnaireCase = "false";

            String template_en = "";
            String template_vt = "";
            String template_th = "";
            String RDS_IllusID = "";
            String template_illustration_copy = "";
            type = jsonObj.getString(TYPE);

            Response<List<ProductTemplateMapping>> templateResponse = null;
            Response<List<ProductTemplateMapping>> templateResponseIllusCopy = null;
            List<ProductTemplateMapping> templateMappings = null;

            if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {

                id = (String) new JSONObject(response).get(KEY3);

                // Getting template list from product service - based on the language and product
                templateResponse = productRepository.getProductTemplatesByFilter(buildProductSearchCriteria(jsonObj));

                agentName = (String) emailObj.getString(AGENT_NAME);
                agentEmailId = (String) emailObj.getString(AGENT_EMAIL_ID);
                leadName = (String) emailObj.getString(LEAD_NAME);
                                
            } else if (FNA.equals(jsonObj.getString(TYPE))) {

                template_vt =
                        jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(TEMPLATES).getString(FNA_PDF_TEMPLATE);

                id = (String) new JSONObject(response).get(KEY2);
                emailObject.put(Constants.PDF_NAME, FNA);
                leadName = (String) emailObj.getString(LEAD_NAME);

            }

            if (null != templateResponse) {
                templateMappings = templateResponse.getType();
            }
            if (templateMappings != null && !templateMappings.isEmpty()) {
                for (ProductTemplateMapping productTemplateMapping : templateMappings) {
                    if (EN.equals(productTemplateMapping.getProductTemplates().getLanguage())) {
                        template_en = productTemplateMapping.getProductTemplates().getTemplateId();
                    } else if (TH.equals(productTemplateMapping.getProductTemplates().getLanguage())) {
                        template_th = productTemplateMapping.getProductTemplates().getTemplateId();
                    }

                }

            }

            if (null != templateResponseIllusCopy) {
                templateMappings = templateResponseIllusCopy.getType();
            }
            if (templateMappings != null && !templateMappings.isEmpty()) {
                for (ProductTemplateMapping productTemplateMapping : templateMappings) {
                    if (TH.equals(productTemplateMapping.getProductTemplates().getLanguage())) {
                        template_illustration_copy = productTemplateMapping.getProductTemplates().getTemplateId();
                    }
                }

            }

           // String premium = "";
            String productName = "";
            String mailSubject = "";
            // Mail Subject for thailand (Illustration_(Insured full  name)_(product name))
            if (ILLUSTRATION.equals(type)) {
                final JSONObject productObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(PRODUCT);
                final JSONObject insuredObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(INSURED).getJSONObject(BASICDETAILS);
                fullName = insuredObj.getString(FULL_NAME);
                productName = productObj.getJSONObject(PRODUCT_DETAILS).getString(PRODUCT_NAME);
                 //Framing mail subject from UI, if not present in UI then from service side
                mailSubject = StringUtils.isEmpty(emailObj.getString(EMAILSUBJECT))? 
                        GENERALI_THAILAND + "_" + QUOTATION +productName + FORYOU  + fullName : emailObj.getString(EMAILSUBJECT);
            
            } else if (FNA.equals(type)) {
                if (null != leadName) {
                	fullName = leadName;
                }
                mailSubject = fnaMailSubject;
                
            }

            emailObject.put(MAIL_SUBJECT, mailSubject);
            emailObject.put(TEMPLATE_TH, template_th);
            emailObject.put(TEMPLATE_EN, template_en);
            emailObject.put(TEMPLATE_ILLUSTRATION_COPY, template_illustration_copy);
            emailObject.put(IS_ADDITIONAL_QUESTIONNAIRE_CASE, additionalQuestionnaireCase);
            emailObject.put(TYPE, type);
            emailObject.put(EMAIL_ID, toMailIds);
            emailObject.put(CC_EMAIL_ID, ccMailIds);
            emailObject.put(EMAIL_FROM_ADDRESS, fromMailId);
            emailObject.put(AGENT_ID, agentId);
            emailObject.put(LANGUAGE, language);
            emailObject.put(RDS_ILLUS_ID, RDS_IllusID);

            if (!id.isEmpty()) {
                emailTemplateObj.put(LEAD_NAME, fullName);
                emailTemplateObj.put(AGENT_NAME, agentName);
                emailTemplateObj.put(AGENT_EMAIL_ID, agentEmailId);
                emailTemplateObj.put(SPAJ_NUMBER, spajNumber);

                SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
                SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT);
                Date date = new Date();
                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(date);
                int day = cal.get(Calendar.DAY_OF_WEEK);

                emailTemplateObj.put(DATE, dateFormat.format(date));
                emailTemplateObj.put(TIME, timeFormat.format(date));

               // TODO : Thailand changes
                if (VT.equals(language)) {
                    emailTemplateObj.put(DAY, daysIN[day - 1]);

                } else if (EN.equals(language)) {
                    emailTemplateObj.put(DAY, daysEN[day - 1]);
                }

                emailObject.put(ID, id);
                emailObject.put(ILLUSTRATION_ID, illustrationId);

                emailObject.put(EMAIL_TEMPLATE, emailTemplateObj);
                lifeEngageEmail.setSendTime(LifeEngageComponentHelper.getCurrentdate());
                lifeEngageEmail.setEmailValues(emailObject.toString());
                lifeEngageEmail.setAttemptNumber("1");
                lifeEngageEmail.setStatus(INITIALISED);
                lifeEngageEmail.setAgentId(agentId);
                lifeEngageEmail.setType(type);
                lifeEngageEmail = emailRepository.savelifeEngageEmail(lifeEngageEmail);
            }

        }
        return lifeEngageEmail;

    }
    
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public String save(final String json) throws BusinessException ,CookieTheftException {
		LOGGER.trace("Inside GeneraliSyncServiceImpl.Save :json " + json);
		JSONObject jsonObject;
		String typeVariableObj = "";
		final JSONObject jsonObjectRs = new JSONObject();
		try {
			jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent
					.savelifeEngageAudit(requestInfo, json);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonRqArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final JSONArray jsonRsArray = new JSONArray();

			//Added a check to bypass this code if allocation type is not available in request;
			if (jsonRqArray.length() > 0) {
				if (jsonRqArray.getJSONObject(0).has("TransactionData")) {
					if (jsonRqArray.getJSONObject(0)
							.getJSONObject("TransactionData")
							.has("AllocationType")) {
						if (jsonRqArray.getJSONObject(0)
								.getJSONObject("TransactionData")
								.getString("AllocationType") != null
								&& !"".equals(jsonRqArray.getJSONObject(0)
										.getJSONObject("TransactionData")
										.getString("AllocationType"))) {
							typeVariableObj = jsonRqArray.getJSONObject(0)
									.getJSONObject("TransactionData")
									.getString("AllocationType");
						}
					}
				}
				HashMap<String, Integer> pushNotificationAgentMap = new HashMap<String, Integer>();
				for (int i = 0; i < jsonRqArray.length(); i++) {

					final JSONObject jsonObj = jsonRqArray.getJSONObject(i);

					String emailFlag = (String) jsonObj.getString(KEY18);
					String eAppFinalSave = (String) jsonObj.getString(KEY15);
					//String eAppFinalSave ="";
					String response = null;
					try {
						if (E_APP.equals(jsonObj.getString(TYPE))) {
							response = lifeEngageEAppComponent.saveEApp(
									jsonObj.toString(), requestInfo,
									lifeEngageAudit);
							if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
								savelifeEngageEmail(jsonObj, response);
							}

							LOGGER.trace("Inside LifeEngageSyncServiceImpl.SaveEApp response :response "
									+ response);
							
						} else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
							/* Getting IllustartionOutput object from json */
							JSONObject jsonTransactionData = null;
							String jsonIllustrationOutput = null;
							if (!(jsonObj.isNull(TRANSACTIONDATA))) {
								jsonTransactionData = jsonObj
										.getJSONObject(TRANSACTIONDATA);
								if (!(jsonTransactionData
										.isNull(ILLUSTRATION_OUTPUT))) {
									jsonIllustrationOutput = jsonTransactionData
											.getJSONObject(ILLUSTRATION_OUTPUT)
											.toString();
								}
							}
							/* EndOf Getting IllustartionOutput object from json */
							response = lifeEngageIllustrationComponent
									.saveIllustration(jsonObj.toString(),
											jsonIllustrationOutput,
											requestInfo, lifeEngageAudit);
							if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
								savelifeEngageEmail(jsonObj, response);
							}
							LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveIllustration response :response "
									+ response);
						} else if (FNA.equals(jsonObj.getString(TYPE))) {
							response = lifeEngageFNAComponent.saveFNA(
									jsonObj.toString(), requestInfo,
									lifeEngageAudit);
							if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
								savelifeEngageEmail(jsonObj, response);
							}
							LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveFNA response :response "
									+ response);
						} else if (OBSERVATION.equals(jsonObj.getString(TYPE))) {
							response = lifeEngageEAppComponent
									.saveToSyncObservation(jsonObj.toString(),
											requestInfo, lifeEngageAudit);
							LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveToSyncObservation response :response "
									+ response);
						} else if (LMS.equals(jsonObj.getString(TYPE))) {
							response = lifeEngageLMSComponent.saveLMS(jsonObj.toString(), requestInfo,lifeEngageAudit);
								// Adding count of Leads assigned to each
								// agentId in order to send push notification
							   
							   if(GeneraliConstants.REASSIGNKEY.equals(typeVariableObj)) {
								   addtoMap(pushNotificationAgentMap, jsonObj,
											response, typeVariableObj);
							   } else {
								   addtoMap(pushNotificationAgentMap, jsonObj,
											response, "");
							   }							
								
							LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveToSyncObservation response :response "
									+ response);
						}
						jsonRsArray.put(new JSONObject(response));
					} catch (SystemException e) {
						jsonRsArray.put(new JSONObject(e.getMessage()));
					} catch (CookieTheftException e) {
                        LOGGER.error("CookieTheftException", e);
                        throw new CookieTheftException(Constants.PROCESSED_TRANSACTION);
                    } catch (Exception e) {
						e.printStackTrace();
					}

				}
				pushNotificationThreadPool.execute(new CreatePushNotificationTask(pushNotificationAgentMap,requestInfo,typeVariableObj));
			}

			LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo,
					jsonRsArray, jsonObjectRs, null);

		} catch (ParseException e) {
			LOGGER.trace("LifeEngageSyncServiceImpl.Save : Parse Exception"
					+ e.toString());
			LOGGER.error("LifeEngageSyncServiceImpl.Save : Parse Exception"
					+ e.toString());
			throwBusinessException(true, e.getMessage());
		}

		return jsonObjectRs.toString();
	}

    /**
     * Building search Criteria for the PDF templates
     * 
     * @param object
     * @return
     */
    private ProductSearchCriteria buildProductSearchCriteria(JSONObject object) {
        ProductSearchCriteria criteria = new ProductSearchCriteria();
        String type = object.getString(TYPE);
        final JSONObject productObj = object.getJSONObject(TRANSACTIONDATA).getJSONObject(PRODUCT);
        String productCode = productObj.getJSONObject(PRODUCT_DETAILS).getString(PRODUCT_CODE);
        String templateTypeCode = "";
        if (ILLUSTRATION.equals(type)) {
            templateTypeCode = GeneraliConstants.ILLUSTRATION_PDF_TEMPLATE_TYPE;
        } else if (E_APP.equals(type)) {
            templateTypeCode = GeneraliConstants.EAPP_PDF_TEMPLATE_TYPE;
        } else if (FNA.equals(type)) {
            templateTypeCode = GeneraliConstants.FNA_TEMPLATE_TYPE;
        }
        criteria.setId(Long.parseLong(productCode));
        criteria.setCarrierCode(GeneraliConstants.CARRIER_CODE);
        criteria.setTemplateType(templateTypeCode);
        return criteria;
    } 
    
    private ProductSearchCriteria buildProductSearchCriteriaForEapp(JSONObject object, String type) {
        ProductSearchCriteria criteria = new ProductSearchCriteria();
        final JSONObject productObj = object.getJSONObject(TRANSACTIONDATA).getJSONObject(PRODUCT);
        String productCode = productObj.getJSONObject(PRODUCT_DETAILS).getString(PRODUCT_CODE);
        String templateTypeCode = "";
        if (ILLUSTRATION.equals(type)) {
            templateTypeCode = GeneraliConstants.ILLUSTRATION_PDF_TEMPLATE_TYPE;
        } else if (E_APP.equals(type)) {
            templateTypeCode = GeneraliConstants.EAPP_PDF_TEMPLATE_TYPE;
        } else if (FNA.equals(type)) {
            templateTypeCode = GeneraliConstants.FNA_TEMPLATE_TYPE;
        }
        criteria.setId(Long.parseLong(productCode));
        criteria.setCarrierCode(GeneraliConstants.CARRIER_CODE);
        criteria.setTemplateType(templateTypeCode);
        return criteria;
    } 
    
	
/**
 * Method to allocate no of leads assigned to each agent.
 * @param agentMap
 * @param jsonObject
 * @param response
 */
	public void addtoMap(HashMap<String, Integer> agentMap,
			JSONObject jsonObject, String response, String reassignVariable) {
		try {
			Integer count = 0;
			String syncStatus = (String) new JSONObject(response)
					.get(Constants.KEY16);
			
			JSONObject	responseObj= new JSONObject(response);
			JSONObject statusObj=responseObj.getJSONObject("StatusData");
			String statusMessage=statusObj.getString("StatusMessage");
			String branchId=jsonObject.getString(Constants.KEY25);
			String agentId = jsonObject.getString(Constants.KEY11);
			if (SUCCESS_MESSAGE_SAVE.equals(statusMessage)
					&& !GeneraliConstants.JSON_EMPTY.equals(branchId)) {
				if (LifeEngageComponentHelper.SUCCESS.equals(syncStatus)) {
					if (agentMap.containsKey(agentId)) {
						count = (Integer) agentMap.get(agentId);
						if (count != null) {
							count++;
						} else {
							count = 1;
						}
						agentMap.put(agentId, count);
					} else {
						agentMap.put(agentId, 1);
					}

				}
			} else if(UPDATE_MESSAGE_SAVE.equals(statusMessage)
					&& GeneraliConstants.REASSIGNKEY.equals(reassignVariable)) {
				if (LifeEngageComponentHelper.SUCCESS.equals(syncStatus)) {
					if (agentMap.containsKey(agentId)) {
						count = (Integer) agentMap.get(agentId);
						if (count != null) {
							count++;
						} else {
							count = 1;
						}
						agentMap.put(agentId, count);
					} else {
						agentMap.put(agentId, 1);
					}

				}
			}
		} catch (NoSuchElementException e) {

			throw new SystemException(e.getMessage());

		} catch (ParseException e) {
			throw new SystemException(e.getMessage());

		}
	}
	private class CreatePushNotificationTask implements Runnable {
		private HashMap<String, Integer> agentPushNotificationMapMap;
		private RequestInfo requestInfo;
		private String typeConstant;
		private String message;
		public CreatePushNotificationTask(HashMap<String, Integer> agentMap,
				RequestInfo requestInfo, String type) {
			super();
			this.requestInfo=requestInfo;
			this.agentPushNotificationMapMap=agentMap;
			this.typeConstant=type;
		}

		@Override
		public void run() {
			if(GeneraliConstants.REASSIGNKEY.equals(typeConstant)) {
				pushNotificationsComponent.pushNotifications(agentPushNotificationMapMap,requestInfo,GeneraliConstants.TYPE_REASSIGN,pushNotificationMessage);
			} else {
				pushNotificationsComponent.pushNotifications(agentPushNotificationMapMap,requestInfo,GeneraliConstants.TYPE_BANCA,message);
			}
		}
	}
	
	protected String retrieveAll(final RequestInfo requestInfo, final JSONArray jsonRqArray) throws BusinessException,
    ParseException {
		LOGGER.trace("Inside LifeEngageSyncImpl RetrieveAll : requestInfo" + requestInfo);
		JSONObject jsonObject;
		JSONArray jsonRsArray = new JSONArray();
		final JSONObject jsonObjectRs = new JSONObject();
		Integer chunkSize = null;
		if (jsonRqArray.length() > 0) {
		    final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
		    if (jsonObj.has(TYPE) && jsonObj.getString(TYPE) != null && !"".equals(jsonObj.getString(TYPE))
		            && !jsonObj.getString(TYPE).equals("null")) {
		        String transactionArray = "{\"Transactions\":[]}";
		        if (E_APP.equals(jsonObj.getString(TYPE))) {
		        	String agentId=jsonObj.getString(KEY11);
		        	/** Life Asia Status update   Start    	 */
		        	
		        	/** Life Asia Status update   END      	 */
		            transactionArray = lifeEngageEAppComponent.retrieveAllEApp(requestInfo, jsonObj);
		            chunkSize = eAppLimit;
		        } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
		            transactionArray = lifeEngageIllustrationComponent.retrieveAllIllustration(requestInfo, jsonObj);
		            chunkSize = illustrationLimit;
		        } else if (FNA.equals(jsonObj.getString(TYPE))) {
		            transactionArray = lifeEngageFNAComponent.retrieveAllFNA(requestInfo, jsonObj);
		            chunkSize = fnaLimit;
		        } else if (LMS.equals(jsonObj.getString(TYPE))) {
		            transactionArray = lifeEngageLMSComponent.retrieveAllLMS(requestInfo, jsonObj);
		            chunkSize = lmsLimit;
		        }
		        LOGGER.trace("Inside LifeEngageSyncImpl RetrieveAll : transactionArray" + transactionArray);
		        jsonObject = new JSONObject(transactionArray);
		        jsonRsArray = jsonObject.getJSONArray(TRANSACTIONS);
		    }
		}
		LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, chunkSize);
		LOGGER.trace("Inside LifeEngageSyncImpl RetrieveAll : jsonObjectRs" + jsonObjectRs.toString());
		return jsonObjectRs.toString();
}
	
	
	
	/**
	 * Method used to trigger life asia request
	 */
	private void triggerLifeAsiaSendRequest(String identifier){
		engine.createAndSentLifeAsiaRequest(identifier);
	}
}
