/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.domain.product;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;

/**
 * The Class class PurchasedProduct.
 * 
 * @author 300797
 */

@Entity
public class PurchasedProduct extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5207011824648657049L;

    /** The product code. */
    private String productCode;

    /** The proposal number. */
    private String proposalNumber;
    
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount premium;
    
    @Temporal(TemporalType.DATE)
    private Date submissionDate;

    /**
     * Gets the product code.
     * 
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the product code.
     * 
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * Gets the proposal number.
     * 
     * @return the proposalNumber
     */
    public String getProposalNumber() {
        return proposalNumber;
    }

    /**
     * Sets the proposal number.
     * 
     * @param proposalNumber
     *            the proposalNumber to set
     */
    public void setProposalNumber(String proposalNumber) {
        this.proposalNumber = proposalNumber;
    }

	public CurrencyAmount getPremium() {
		return premium;
	}

	public void setPremium(CurrencyAmount premium) {
		this.premium = premium;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}
    

}
