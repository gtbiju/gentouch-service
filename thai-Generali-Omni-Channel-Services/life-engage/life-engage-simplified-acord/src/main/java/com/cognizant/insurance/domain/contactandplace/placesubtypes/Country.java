/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.placesubtypes;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.cognizant.insurance.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.domain.contactandplace.Place;

/**
 * A country is a region legally identified as a distinct entity in political
 * geography.
 * 
 * SOURCE: http://en.wikipedia.org/wiki/Country
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("COUNTRY")
public class Country extends Place {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8318503563705782945L;

    /**
     * See ISO codes definition (ISO 3166-1: A2).
     */
    @OneToOne(cascade=CascadeType.ALL)
    private ExternalCode alphaISOCode;

    /**
     * See ISO codes definition (ISO 3166-1: A3).
     * 
     * 
     * 
     */
    @OneToOne(cascade=CascadeType.ALL)
    private ExternalCode extendedISOCode;

    /**
     * States the number one has to dial prior to the domestic number.
     * 
     * e.g. United States: 1; France: 33;
     * 
     */
    @OneToOne(cascade=CascadeType.ALL)
    private ExternalCode telephonePrefixCode;

    /**
     * Gets the alphaISOCode.
     * 
     * @return Returns the alphaISOCode.
     */
    public ExternalCode getAlphaISOCode() {
        return alphaISOCode;
    }

    /**
     * Gets the extendedISOCode.
     * 
     * @return Returns the extendedISOCode.
     */
    public ExternalCode getExtendedISOCode() {
        return extendedISOCode;
    }

    /**
     * Gets the telephonePrefixCode.
     * 
     * @return Returns the telephonePrefixCode.
     */
    public ExternalCode getTelephonePrefixCode() {
        return telephonePrefixCode;
    }

    /**
     * Sets The alphaISOCode.
     * 
     * @param alphaISOCode
     *            The alphaISOCode to set.
     */
    public void setAlphaISOCode(final ExternalCode alphaISOCode) {
        this.alphaISOCode = alphaISOCode;
    }

    /**
     * Sets The extendedISOCode.
     * 
     * @param extendedISOCode
     *            The extendedISOCode to set.
     */
    public void setExtendedISOCode(final ExternalCode extendedISOCode) {
        this.extendedISOCode = extendedISOCode;
    }

    /**
     * Sets The telephonePrefixCode.
     * 
     * @param telephonePrefixCode
     *            The telephonePrefixCode to set.
     */
    public void setTelephonePrefixCode(final ExternalCode telephonePrefixCode) {
        this.telephonePrefixCode = telephonePrefixCode;
    }

}
