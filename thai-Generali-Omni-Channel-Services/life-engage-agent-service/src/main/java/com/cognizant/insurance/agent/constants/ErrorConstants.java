/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Feb 15, 2013
 */
package com.cognizant.insurance.agent.constants;

// TODO: Auto-generated Javadoc
/**
 * The Class class ErrorConstants.
 */
public final class ErrorConstants {

    /** The Constant PARTY_NOT_FOUND_EXCEPTION. */
    public static final String PARTY_MISSING_EXCEPTION = "PartyMissingException";

    /** Error codes */

    public static final String LE_SYNC_ERR_100 = "LE_SYNC_ERR_100";

    public static final String LE_SYNC_ERR_101 = "LE_SYNC_ERR_101 ";

    public static final String LE_SYNC_ERR_102 = "LE_SYNC_ERR_102";

    public static final String LE_SYNC_ERR_103 = "LE_SYNC_ERR_103 ";

    public static final String LE_SYNC_ERR_104 = "LE_SYNC_ERR_104";

    public static final String LE_SYNC_ERR_105 = "LE_SYNC_ERR_105";

    public static final String LE_SYNC_ERR_106 = "LE_SYNC_ERR_106";

    public static final String LE_SYNC_ERR_107 = "LE_SYNC_ERR_107";

    /**
     * Instantiates a new error constants.
     */
    private ErrorConstants() {
    };
}
