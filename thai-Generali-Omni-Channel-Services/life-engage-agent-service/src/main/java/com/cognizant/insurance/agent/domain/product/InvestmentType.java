/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Feb 22, 2013
 */
package com.cognizant.insurance.agent.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class class InvestmentType.
 */

@Entity
@DiscriminatorValue("Investment")
public class InvestmentType extends Type {

    /**
     *
     */
    private static final long serialVersionUID = 2699700617583661447L;
    
    /** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "investmentType")
    private Set<ProductSpecification> productSpecifications;

    /**
     * @param productSpecifications
     *            the productSpecifications to set
     */
    public void setProductSpecifications(Set<ProductSpecification> productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    /**
     * @return the productSpecifications
     */
    public Set<ProductSpecification> getProductSpecifications() {
        return productSpecifications;
    }

}
