/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304000
 * @version       : 0.1, Feb 21, 2014
 */
package com.cognizant.insurance.goalandneed.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;

import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.goalandneed.dao.GoalAndNeedDao;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

/**
 * The Class class GoalAndNeedDaoImpl.
 */
public class GoalAndNeedDaoImpl extends DaoImpl implements GoalAndNeedDao {

    /** The Constant RETRIEVE_GOAL_FNA. */
    private static final String RETRIEVE_GOAL_FNA =
            "select goalAndNeed from GoalAndNeed goalAndNeed "
                    + "WHERE goalAndNeed.contextId=?1 and goalAndNeed.agentId=?2 "
                    + "and (goalAndNeed.creationDateTime > ?3 or goalAndNeed.modifiedDateTime > ?3) and goalAndNeed.status !='Cancelled' order by goalAndNeed.creationDateTime asc";

    /** The Constant GET_FNA_BY_IDENTIFIER. */
    private static final String GET_FNA_BY_IDENTIFIER = "select goalAndNeed from GoalAndNeed goalAndNeed "
            + "WHERE goalAndNeed.identifier =?1 and goalAndNeed.contextId=?2";

    /** The Constant GET_FNA_BY_TRANSTRACKING_ID. */
    private static final String GET_FNA_BY_TRANSTRACKING_ID = "select goalAndNeed from GoalAndNeed goalAndNeed "
            + "WHERE goalAndNeed.transTrackingId =?1 and goalAndNeed.contextId=?2 and goalAndNeed.agentId=?3 ";
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.goalandneed.dao.GoalAndNeedDao#retrieveAll(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<GoalAndNeed>> retrieveAll(final Request<GoalAndNeed> goalAndNeedRequest,
            final Request<Integer> limitRequest) {
        final Response<List<GoalAndNeed>> goalAndNeedResponse = new ResponseImpl<List<GoalAndNeed>>();
        if (goalAndNeedRequest.getType() != null) {
            final GoalAndNeed goalAndNeed = (GoalAndNeed) goalAndNeedRequest.getType();
            List<GoalAndNeed> goalAndNeedObjList = null;
            if (limitRequest != null && limitRequest.getType() != null && limitRequest.getType() > 0) {
                goalAndNeedObjList =
                        findUsingQueryWithLimit(goalAndNeedRequest, RETRIEVE_GOAL_FNA, limitRequest.getType(),
                                goalAndNeedRequest.getContextId(), goalAndNeed.getAgentId(),
                                goalAndNeed.getCreationDateTime());
            } else {
                goalAndNeedObjList =
                        findByQuery(goalAndNeedRequest, RETRIEVE_GOAL_FNA, goalAndNeedRequest.getContextId(),
                                goalAndNeed.getAgentId(), goalAndNeed.getCreationDateTime());
            }

            final List<GoalAndNeed> goalAndNeeds = new ArrayList<GoalAndNeed>();
            if (CollectionUtils.isNotEmpty(goalAndNeedObjList)) {
                for (Object goalObj : goalAndNeedObjList) {
                    goalAndNeeds.add((GoalAndNeed) goalObj);
                }
            }
            goalAndNeedResponse.setType(goalAndNeeds);
        }
        return goalAndNeedResponse;
    }

    /**
     * To get fna using identifier so as to update the entity.
     * 
     * @param goalAndNeedRequest
     *            the goal and need request
     * @return the goal and need by identifier
     */
    @Override
	public Response<List<GoalAndNeed>> getGoalAndNeedByIdentifier(
			final Request<GoalAndNeed> goalAndNeedRequest) {
		final Response<List<GoalAndNeed>> goalAndNeedResponse = new ResponseImpl<List<GoalAndNeed>>();
		if (goalAndNeedRequest.getType() != null) {
			final GoalAndNeed goalAndNeed = (GoalAndNeed) goalAndNeedRequest
					.getType();
			if (goalAndNeed.getIdentifier() != null
					&& !("".equals(goalAndNeed.getIdentifier()))) {
				final List<GoalAndNeed> goalAndNeedObjList = findByQuery(
						goalAndNeedRequest, GET_FNA_BY_IDENTIFIER,
						goalAndNeed.getIdentifier(),
						goalAndNeedRequest.getContextId());
				final List<GoalAndNeed> goalAndNeeds = new ArrayList<GoalAndNeed>();
				if (CollectionUtils.isNotEmpty(goalAndNeedObjList)) {
					for (Object goalObj : goalAndNeedObjList) {
						goalAndNeeds.add((GoalAndNeed) goalObj);
					}
				} else {
					throw new DaoException("No FNA with identifier : "
							+ goalAndNeed.getIdentifier());
				}
				goalAndNeedResponse.setType(goalAndNeeds);
			} else {
				final List<GoalAndNeed> goalAndNeedObjList = findByQuery(
						goalAndNeedRequest, GET_FNA_BY_TRANSTRACKING_ID,
						goalAndNeed.getTransTrackingId(),
						goalAndNeedRequest.getContextId(),
						goalAndNeed.getAgentId());
				final List<GoalAndNeed> goalAndNeeds = new ArrayList<GoalAndNeed>();
				if (CollectionUtils.isNotEmpty(goalAndNeedObjList)) {
					for (Object goalObj : goalAndNeedObjList) {
						goalAndNeeds.add((GoalAndNeed) goalObj);
					}
				}
				goalAndNeedResponse.setType(goalAndNeeds);
			}
		}

		return goalAndNeedResponse;
	}

    /**
     * Find using query with limit.
     * 
     * @param <T>
     *            the generic type
     * @param request
     *            the request
     * @param queryString
     *            the query string
     * @param limit
     *            the limit
     * @param params
     *            the params
     * @return the list
     */
    protected <T> List<T> findUsingQueryWithLimit(final Request<T> request, final String queryString, Integer limit,
            Object... params) {
        try {
            final JPARequestImpl<T> jpaRequestImpl = (JPARequestImpl<T>) request;
            final Query query =
                    jpaRequestImpl.getEntityManager().createQuery(queryString).setMaxResults(limit.intValue());
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<T>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * Find using query.
     * 
     * @param request
     *            the request
     * @param queryString
     *            the query string
     * @param params
     *            the params
     * @return the list
     */
    protected List<Object> findUsingQuery(final Request request, final String queryString, Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<Object>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.goalandneed.dao.GoalAndNeedDao#getFNACount(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<SearchCountResult> getGoalAndNeedCount(Request<SearchCriteria> searchCriteriatRequest,List<String> fnaSearchParamsList) {
        final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
        List<Object> searchCriteriaList = null;
        String type = null;
        String query;
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            type = searchCriteria.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select count(goalAndNeed.identifier),goalAndNeed.status from GoalAndNeed goalAndNeed where goalAndNeed.agentId =?1 "
                                + "and goalAndNeed.contextId =?2 and (";
                               
                   for(int index=0;index<fnaSearchParamsList.size();index++){
                    
                    if(index>0){
                        query=query+" or";
                    }
                    query=query + " goalAndNeed.transactionKeys." + fnaSearchParamsList.get(index)+ " like ?3";
                  }
                   
                query=query + " and goalAndNeed.status != 'Cancelled') group by goalAndNeed.status";
                
                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                searchCriteriatRequest.getContextId(), "%" + searchCriteria.getValue() + "%");

            } else {
                query =
                        "select count(goalAndNeed.identifier) ,goalAndNeed.status from GoalAndNeed goalAndNeed where goalAndNeed.agentId =?1 "
                                + "and goalAndNeed.contextId =?2 and goalAndNeed.status != 'Cancelled' group by goalAndNeed.status";

                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                searchCriteriatRequest.getContextId());
            }
        }
        final List<SearchCountResult> subModeResults = new ArrayList<SearchCountResult>();
        final SearchCountResult searchCountResult = new SearchCountResult();
        SearchCountResult submode;
        Integer totalModCount = 0;
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object searchCriteria : searchCriteriaList) {
                Object[] values = (Object[]) searchCriteria;
                Long count = (Long) values[0];
                String mode = (String) values[1];
                submode = new SearchCountResult();
                submode.setCount(count.toString());
                submode.setMode(mode.toString());

                subModeResults.add(submode);
                totalModCount += Integer.valueOf(submode.getCount());
            }
            searchCountResult.setSubModeResults(subModeResults);
        }
        searchCountResult.setCount(String.valueOf(totalModCount));
        searchCountResult.setMode(type);
        searchCountResponse.setType(searchCountResult);
        return searchCountResponse;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.goalandneed.dao.GoalAndNeedDao#getFNAByFilter(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<GoalAndNeed>> getFNAByFilter(Request<SearchCriteria> searchCriteriatRequest) {
        final Response<List<GoalAndNeed>> goalAndNeedResponse = new ResponseImpl<List<GoalAndNeed>>();
        List<Object> goalAndNeedObjList = null;
        String query;
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select goalAndNeed from GoalAndNeed goalAndNeed "
                                + "WHERE goalAndNeed.contextId=?1 and goalAndNeed.agentId=?2 and"
                                + "(goalAndNeed.transactionKeys.key6 like ?3 or goalAndNeed.transactionKeys.key8 like ?3 and goalAndNeed.status != 'Cancelled')";
                goalAndNeedObjList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteriatRequest.getContextId(),
                                searchCriteria.getAgentId(), "%" + searchCriteria.getValue() + "%");

            } else {
                query =
                        "select goalAndNeed from GoalAndNeed goalAndNeed "
                                + "WHERE goalAndNeed.contextId=?1 and goalAndNeed.agentId=?2 and goalAndNeed.status !='Cancelled'";
                goalAndNeedObjList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteriatRequest.getContextId(),
                                searchCriteria.getAgentId());
            }

            final List<GoalAndNeed> goalAndNeeds = new ArrayList<GoalAndNeed>();
            if (CollectionUtils.isNotEmpty(goalAndNeedObjList)) {
                for (Object goalObj : goalAndNeedObjList) {
                    goalAndNeeds.add((GoalAndNeed) goalObj);
                }
            }
            goalAndNeedResponse.setType(goalAndNeeds);
        }
        return goalAndNeedResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.goalandneed.dao.GoalAndNeedDao#updateGoalAndNeedStatusForLead(com.cognizant.insurance
     * .request.Request)
     */
    public void updateGoalAndNeedStatusForLead(Request<GoalAndNeed> goalAndNeedRequest) {
        String query;
        if (goalAndNeedRequest.getType() != null) {
            final GoalAndNeed goalAndNeed = (GoalAndNeed) goalAndNeedRequest.getType();
            query =
                    "select goalAndNeed from GoalAndNeed goalAndNeed where goalAndNeed.contextId =?1 and goalAndNeed.transactionKeys.key1 =?2 and goalAndNeed.status !=?3";
            final List<Object> goalAndNeedObjList =
                    findUsingQuery(goalAndNeedRequest, query, goalAndNeedRequest.getContextId(),
                            goalAndNeed.getLeadId(), goalAndNeed.getStatus());
            if (CollectionUtils.isNotEmpty(goalAndNeedObjList)) {
                for (Object goalAndNeedObj : goalAndNeedObjList) {
                    GoalAndNeed goalAndNeedData = ((GoalAndNeed) goalAndNeedObj);
                    goalAndNeedData.setStatus(goalAndNeed.getStatus());
                    goalAndNeedRequest.setType(goalAndNeedData);
                    merge(goalAndNeedRequest);

                }
            }
        }
    }
}
