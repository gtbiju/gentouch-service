package com.cognizant.le.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jettison.json.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cognizant.le.manager.FileUploadService;
import com.cognizant.le.model.dto.AgentAchievmentDTO;
import com.cognizant.le.model.dto.AgentDTO;
import com.cognizant.le.model.dto.PropertyFileSource;
import com.cognizant.le.util.LoggerUtil;
import com.cognizant.le.util.Constants;
import com.cognizant.le.model.dto.ExcelStatus;

/**
 * Servlet implementation class FileAction
 */
public class FileAction extends HttpServlet {
    private static final long serialVersionUID = 1L;

    static Logger logger = Logger.getLogger(FileAction.class);
    
    @Autowired
    FileUploadService fileUploadService;
    
    @Autowired
    PropertyFileSource PropertyFileSource;
    
	private WebApplicationContext springContext;

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // logger.debug(LoggerUtil.getLogMessage("Method Entry :FileAction.Browsw  for Agent:", ""));
        response.setContentType("text/html");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        String fileName = "";
        ExcelStatus profile = new ExcelStatus();
        PrintWriter writer;
        JSONObject js = new JSONObject();
        boolean fileType = false;
        boolean agentSheetValid = false;
        boolean bancaSheetValid = false;
        boolean emptyExcel = false;
        boolean validUserType = false;
    	String username = "";
    	String userType= "";
        String folderPath=PropertyFileSource.getSharedFolderPath();
        try {
        	 HttpSession session = request.getSession(true);
        	 if(session != null){
        		username = session.getAttribute("username").toString();
        		userType = session.getAttribute("userType").toString();
        	 }
            fileName = uploadFile(request);
            boolean isValidTemplate = false;
            String type="";

            if (fileName != null && !fileName.isEmpty()) {
            	 String fullfileName=folderPath+fileName;
                String fileExtension = getFileExtension(fileName);
              
                if ((fileExtension.contains("xls")) || (fileExtension.contains("xlsx"))) {
                    fileType = true;
                    agentSheetValid = validateAgentSheet(fullfileName);
                    bancaSheetValid = validateAchvSheet(fullfileName);
                    emptyExcel = false;
                    if ((agentSheetValid) || (bancaSheetValid)) {
                        isValidTemplate = true;
                        emptyExcel=validateEmptyExcel(fullfileName);
                        if(agentSheetValid){
                        	type="Agency";
                        }
                        else if(bancaSheetValid){
                        	type="Banca";
                        }
                        validUserType = validateUserType(type,userType);
                    }
                
                
                profile.setValidTemplate(isValidTemplate);
                profile.setValidFileType(fileType);
                profile.setEmptyExcel(emptyExcel);
                logger.debug(LoggerUtil.getLogMessage("*Log Entry Agent Profile and Agent Datas :", ""));
                if (profile != null) {
                    logger.debug(LoggerUtil.getLogMessage("Agent Profile Datas :", profile + ""));
                }
            }
            }
            js.put("isValidTemplate",isValidTemplate);
            js.put("fileType",fileType);
            js.put("emptyExcel", emptyExcel);
            js.put("validUserType", validUserType);
            response.getWriter().print(js);
            if(isValidTemplate&&fileType&&!emptyExcel &&validUserType){
            	fileUploadService.fileUploadService(fileName, type ,username);
            }
            logger.debug(LoggerUtil.getLogMessage("Method End :FileAction.Browse  for Agent:", ""));

        }

        catch (Exception e) {
            logger.error(LoggerUtil.getLogMessage("Error while FileAction.Browse:", "public exception"), e);
            //RequestDispatcher view = request.getRequestDispatcher("/jsp/agentprofileupload.jsp");
            //view.include(request, response);
            e.printStackTrace();

        }

    }
    
    private Boolean validateUserType(String sheetType, String validUserType) {
		// TODO Auto-generated method stub
    	boolean userType = false;
    	if("Agency".equals(sheetType) &&  "agency_upload_admin".equals(validUserType))
    		userType = true;
    	else if("Banca".equals(sheetType) &&"banca_upload_admin".equals(validUserType))
    		userType = true;
		return userType;
	}
    private void moveFiles(String fileName) {
        String fName = new File(fileName).getName();
        Properties mailProperties = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream("/" + Constants.PROPERTY_FILE);
        if (stream == null) {
            stream = loader.getResourceAsStream(Constants.PROPERTY_FILE);
        }
        try {
            mailProperties.load(stream);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String source = mailProperties.getProperty(Constants.AGENT_PROFILE_PATH);
        String archiveSource = mailProperties.getProperty(Constants.AGENT_PROFILE_BACKUP);
        String tarFileName = source + File.separator + fName;
        String archivefile = archiveSource + File.separator + fName;
        File sourcefile = new File(tarFileName);
        sourcefile.renameTo(new File(archivefile));
        sourcefile.delete();
    }

    private String uploadFile(HttpServletRequest request) throws ServletException, IOException {

        String fullFileName = null;
        String fileName = null;
        // check if the request actually contains a file
        if (!ServletFileUpload.isMultipartContent(request)) {
            // if not, we stop here
            return "error";
        }
        // configures some settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(Constants.THRESHOLD_SIZE);
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(Constants.MAX_FILE_SIZE);
        upload.setSizeMax(Constants.REQUEST_SIZE);
    
       
        // Set the PDF path
        String serverPath = PropertyFileSource.getSharedFolderPath();
        // creates the directory if it does not exist
        File uploadDir = new File(serverPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
        try {
            // parses the request's content to extract file data
            List formItems = upload.parseRequest(request);
            Iterator iter = formItems.iterator();

            // iterates over form's fields
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                // processes only fields that are not form fields

                if (!item.isFormField()) {
                    fileName = new File(item.getName()).getName();
					if (fileName != null || !"".equals(fileName)) {
						Calendar lCDateTime = Calendar.getInstance();
						Long timeInMS = lCDateTime.getTimeInMillis();
						fileName = timeInMS + "_" + fileName;
					}
                    fullFileName = serverPath+fileName;

                    File storeFile = new File(fullFileName);
                    // saves the file on disk
                    item.write(storeFile);
                }
            }

            // AgentFileSyncUtility.refreshDataBase();
        } catch (Exception ex) {
            request.setAttribute("message", "There was an error: " + ex.getMessage());
        }
        return fileName;

    }

    private Map<String, List<AgentDTO>> readAgent(String fileName, HttpServletRequest request) throws Exception {
        AgentDTO agent = null;
        String fileExactName = null;
        Map<String, List<AgentDTO>> agentMap = new HashMap<String, List<AgentDTO>>();
        List<AgentDTO> agentList = new ArrayList<AgentDTO>();
        List<AgentDTO> vFailAgentList = new ArrayList<AgentDTO>();
        HttpSession session = request.getSession(false);
        String splitFileName = null;
        int indxFirstUnderScore;
        try {
            String fileExtn = getFileExtension(fileName);
            Workbook wb_xssf; // Declare XSSF WorkBook
            Workbook wb_hssf; // Declare HSSF WorkBook
            Sheet sheet = null; // sheet can be used as common for XSSF and HSSF

            if (fileExtn.equalsIgnoreCase("xlsx")) {
                wb_xssf = new XSSFWorkbook(fileName);
                sheet = wb_xssf.getSheetAt(0);
            }
            if (fileExtn.equalsIgnoreCase("xls")) {
                FileInputStream myInput = new FileInputStream(fileName);
                POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
                wb_hssf = new HSSFWorkbook(myFileSystem);

                sheet = wb_hssf.getSheetAt(0);
            }

            Iterator rows = sheet.rowIterator();

            while (rows.hasNext()) {
                agent = new AgentDTO();
                Row myRow = (Row) rows.next();

                if (myRow.getRowNum() == 0) {
                    continue;
                }

                if (myRow.getRowNum() > 0) {
                    Iterator cellIter = myRow.cellIterator();

                    while (cellIter.hasNext()) {
                        Cell myCell = (Cell) cellIter.next();

                        if (myCell.getColumnIndex() == 0) {
                            String userId = null;
                            if (myCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                userId = String.valueOf((int) myCell.getNumericCellValue()).trim();

                            } else if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {

                                userId = myCell.getStringCellValue().trim();
                            }
                            // vFailAgentList.add(agent);

                            // vFailAgentList.add(agent);

                            agent.setUserid(userId);
                        }

                        else if (myCell.getColumnIndex() == 1) {
                            String empCode = null;
                            if (myCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                empCode = String.valueOf((int) myCell.getNumericCellValue());

                            } else if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {

                                empCode = myCell.getStringCellValue();
                            }
                            agent.setAgentCode(empCode);
                        } else if (myCell.getColumnIndex() == 2) {
                            String fullname = null;
                            if (myCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                fullname = String.valueOf((int) myCell.getNumericCellValue());

                            } else if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {

                                fullname = myCell.getStringCellValue();
                            }
                            agent.setFullname(fullname);
                        }

                        else if (myCell.getColumnIndex() == 3) {
                            String empType = null;
                            if (myCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                empType = String.valueOf((int) myCell.getNumericCellValue());

                            } else if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {

                                empType = myCell.getStringCellValue();
                            }
                            agent.setEmployeeType(empType);
                        }

                        else if (myCell.getColumnIndex() == 4) {
                            String role = null;
                            if (myCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                role = String.valueOf((int) myCell.getNumericCellValue());

                            } else if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {

                                role = myCell.getStringCellValue();
                            }
                            agent.setRole(role);
                        }

                        else if (myCell.getColumnIndex() == 5) {
                            String superVisorCode = null;
                            if (myCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                superVisorCode = String.valueOf((int) myCell.getNumericCellValue());

                            } else if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {

                                superVisorCode = myCell.getStringCellValue();
                            }

                            agent.setSupervisorCode(superVisorCode);
                        } else if (myCell.getColumnIndex() == 6) {
                            agent.setOffice(returnStringCell(myCell));

                        } else if (myCell.getColumnIndex() == 7) {
                            agent.setUnit(returnStringCell(myCell));

                        } else if (myCell.getColumnIndex() == 8) {
                            agent.setGroup(returnStringCell(myCell));

                        } else if (myCell.getColumnIndex() == 9) {
                            String briefWriteup = null;
                            if (myCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                briefWriteup = String.valueOf((int) myCell.getNumericCellValue());

                            } else if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {

                                briefWriteup = myCell.getStringCellValue();
                            }
                            agent.setBriefWriteUp(briefWriteup);
                        } else if (myCell.getColumnIndex() == 10) {
                            agent.setYearsOfExperience(returnNumericCell(myCell) != null ? returnNumericCell(myCell).intValue()
                                    : 0);

                        } else if (myCell.getColumnIndex() == 11) {
                            agent.setBusinessSourced(returnNumericCell(myCell));

                        } else if (myCell.getColumnIndex() == 12) {
                            agent.setNumOfCustServiced(returnNumericCell(myCell));

                        } else if (myCell.getColumnIndex() == 13) {
                            agent.setLicenseNumber(returnStringCell(myCell));

                        } else if (myCell.getColumnIndex() == 14) {
                            agent.setLicenseIssueDate(returnDateCell(myCell));

                        } else if (myCell.getColumnIndex() == 15) {
                            agent.setLicenseExpiryDate(returnDateCell(myCell));

                        } else if (myCell.getColumnIndex() == 16) {
                            agent.setEmailId(returnStringCell(myCell));

                        } else if (myCell.getColumnIndex() == 17) {
                            agent.setMobileNumber(returnStringCell(myCell));

                        } else if (myCell.getColumnIndex() > 17) {
                            break;
                        }

                    }// while

                }// IF

                if (agent.getAgentCode() != null && agent.getUserid() != null) {

                    if (fileName != null) {
                        fileExactName = new File(fileName).getName();
                        if (fileExactName != null) {
                            if (fileExactName.contains("_")) {
                                indxFirstUnderScore = fileExactName.indexOf("_");
                                splitFileName = fileExactName.substring(indxFirstUnderScore + 1);
                                agent.setName_of_excelsheet(splitFileName);
                            }
                        }
                    }
                    /*
                     * if(session!=null){ agent.setUserid(session.getAttribute("username").toString()); } else{
                     * agent.setUserid(""); }
                     */
                    agentList.add(agent);

                } else {
                    if (agent.getAgentCode() == null && agent.getUserid() == null) {
                        // ignore
                    } else {
                        if (agent.getUserid() == null
                                || (null != agent.getUserid() && agent.getUserid().trim().equals(""))) {
                            agent.setStatus(Constants.AGENT_STATUS_INVALID);
                            agent.setMessage("User id field is mandatory!");
                        }

                        if (agent.getAgentCode() == null
                                || (null != agent.getAgentCode() && agent.getAgentCode().trim().equals(""))) {
                            agent.setStatus(Constants.AGENT_STATUS_INVALID);
                            if (agent.getMessage() != null) {
                                agent.setMessage(agent.getMessage().concat("\n Employee Code field is mandatory!"));
                            } else {
                                agent.setMessage("Employee Code field is mandatory!");
                            }
                            // vFailAgentList.add(agent);
                        }

                        /*
                         * if(agent.getFullname() == null || (null!=agent.getFullname() &&
                         * agent.getFullname().trim().equals(""))){ agent.setStatus(Constants.AGENT_STATUS_INVALID);
                         * if(agent.getMessage()!=null){
                         * agent.setMessage(agent.getMessage().concat("\n Full Name field is mandatory!")); }else{
                         * agent.setMessage("Full Name field is mandatory!"); } //vFailAgentList.add(agent); }
                         */

                        vFailAgentList.add(agent);
                    }
                }
            }// WHILE OUTER
            agentMap.put("VALID", agentList);
            agentMap.put("INVALID", vFailAgentList);
        } catch (Exception e) {
            logger.error(LoggerUtil.getLogMessage("Error while FileAction.writeintoFile:", "public exception"), e);
            e.printStackTrace();
        }
        logger.debug(LoggerUtil.getLogMessage("Method Entry : FileAction.writeintoFile  for Agent:", agentList.size()
                + ""));

        return agentMap;
    }

    private List<AgentAchievmentDTO> readAchievment(String fileName) throws Exception {
        AgentDTO agent = null;

        List<AgentAchievmentDTO> achivementList = new ArrayList<AgentAchievmentDTO>();
        try {
            String fileExtn = getFileExtension(fileName);
            Workbook wb_xssf; // Declare XSSF WorkBook
            Workbook wb_hssf; // Declare HSSF WorkBook
            Sheet sheet1 = null; // sheet can be used as common for XSSF and
                                 // HSSF

            if (fileExtn.equalsIgnoreCase("xlsx")) {
                wb_xssf = new XSSFWorkbook(fileName);
                sheet1 = wb_xssf.getSheetAt(1);
            }
            if (fileExtn.equalsIgnoreCase("xls")) {
                FileInputStream myInput = new FileInputStream(fileName);
                POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
                wb_hssf = new HSSFWorkbook(myFileSystem);

                sheet1 = wb_hssf.getSheetAt(1);
            }

            AgentAchievmentDTO agentAchievment;

            Iterator rows_achiev = sheet1.rowIterator();
            while (rows_achiev.hasNext()) {
                agentAchievment = new AgentAchievmentDTO();
                Row myRow = (Row) rows_achiev.next();

                if (myRow.getRowNum() == 0) {
                    continue;
                }
                if (myRow.getRowNum() > 0) {
                    Iterator cellIter = myRow.cellIterator();

                    while (cellIter.hasNext()) {
                        Cell myCell = (Cell) cellIter.next();
                        if (myCell.getColumnIndex() == 0) {
                            String agentCode = null;
                            if (myCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                agentCode = String.valueOf((int) myCell.getNumericCellValue()).trim();

                            } else if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {

                                agentCode = myCell.getStringCellValue().trim();

                            }

                            if (null == agentCode) {
                                break;
                            }

                            if (agentCode.equals("")) {
                                System.out.println("agent Code in achievement===" + agentCode);
                                agentAchievment.setAgentCode(null);
                            } else {
                                agentAchievment.setAgentCode(agentCode);
                            }

                        }

                        if (myCell.getColumnIndex() == 1) {
                            String title = null;
                            if (myCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                title = String.valueOf((int) myCell.getNumericCellValue());

                            } else if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {

                                title = myCell.getStringCellValue();
                            }

                            agentAchievment.setAchievementId(title);
                        } else if (myCell.getColumnIndex() == 2) {
                            agentAchievment.setTitle(returnStringCell(myCell));

                        } else if (myCell.getColumnIndex() == 3) {
                            agentAchievment.setText(returnStringCell(myCell));

                        } else if (myCell.getColumnIndex() > 3) {
                            break;
                        }

                    }

                }// IF
                if (agentAchievment.getAgentCode() != null) {
                    achivementList.add(agentAchievment);
                }
            }// WHILE OUT

        } catch (Exception e) {
            logger.error(LoggerUtil.getLogMessage("Error while FileAction.writeintoFile:", "public exception"), e);
            e.printStackTrace();
        }
        logger.debug(LoggerUtil.getLogMessage("Method Entry : FileAction.writeintoFile  for Agent:",
                achivementList.size() + ""));

        return achivementList;
    }

    /*
     * private AgentProfile uploadAgentToDB(List<AgentDTO> dataList1, List<AgentAchievmentDTO> dataList2) throws
     * Exception { AgentProfile results = null; AgentServiceManager service = new AgentServiceManagerImpl(); try {
     * 
     * results = service.saveAgentProfile(dataList1, dataList2); } catch (Exception e) {
     * 
     * logger.error(LoggerUtil.getLogMessage("Error while FileAction.uploadAgentToDB", "public exception"), e); }
     * 
     * return results; }
     */

    private String getFileExtension(String fname2) {
        String fileName = fname2;
        String fname = "";
        String ext = "";
        int mid = fileName.lastIndexOf(".");
        fname = fileName.substring(0, mid);
        ext = fileName.substring(mid + 1, fileName.length());
        return ext;
    }

    private boolean validateAgentSheet(String filename) throws Exception {
        boolean isValidSheet = true;
        int flag=0;
        try {
            String fileExtn = getFileExtension(filename);
            Workbook wb_xssf; // Declare XSSF WorkBook
            Workbook wb_hssf; // Declare HSSF WorkBook
            Sheet sheet = null; // sheet can be used as common for XSSF and HSSF

            if (fileExtn.equalsIgnoreCase("xlsx")) {
                wb_xssf = new XSSFWorkbook(filename);
                sheet = wb_xssf.getSheetAt(0);
            }
            if (fileExtn.equalsIgnoreCase("xls")) {
                FileInputStream myInput = new FileInputStream(filename);
                POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
                wb_hssf = new HSSFWorkbook(myFileSystem);

                sheet = wb_hssf.getSheetAt(0);
            }
           
            Iterator rows = sheet.rowIterator();
            while (rows.hasNext()) {
                Row myRow = (Row) rows.next();
                if (myRow.getRowNum() == 0) {
                    Iterator cellIter = myRow.cellIterator();
                    while (cellIter.hasNext()) {
                        Cell myCell = (Cell) cellIter.next();

                        if (myCell.getColumnIndex() == 0) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NAME) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NAME_VN)) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S FULL NAME HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 1) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_GENDER) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_GENDER_VN)) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S GENDER HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 2) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_MOBILE) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_MOBILE_VN)) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S MOBILE NUMBER HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 3) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NATIONAL_ID) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NATIONAL_ID_VN)) {
                                isValidSheet = false;
                               // System.out.println("CUSTOMER'S NATIONAL ID HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 4) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_EMAIL) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_EMAIL_VN)) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S EMAIL NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 5) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_ADDRESS) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_ADDRESS_VN)) {
                                isValidSheet = false;
                               // System.out.println("CUSTOMER'S ADDRESS CODE HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 6) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_STATE) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_STATE_VN)) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S STATE HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 7) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NATIONALITY) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NATIONALITY_VN)) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S NATIONALITY HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 8) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_DATE_OF_BIRTH) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_DATE_OF_BIRTH_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S DATE OF BIRTH HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 9) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.POTENTIAL) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.POTENTIAL_VN)) {
                                isValidSheet = false;
                                //System.out.println("POTENTIAL HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 10) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.SOURCE) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.SOURCE_VN)) {
                                isValidSheet = false;
                                //System.out.println("SOURCE HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 11) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_OCCUPATION) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_OCCUPATION_VN)) {
                                isValidSheet = false;
                               // System.out.println("CUSTOMER'S OCCUPATION HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 12) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.ACTION) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.ACTION_VN)) {
                                isValidSheet = false;
                                //System.out.println("ACTION HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 13) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.NEXT_MEETING_DATE) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.NEXT_MEETING_DATE_VN)) {
                                isValidSheet = false;
                                //System.out.println("NEXT_MEETING_DATE HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 14) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.NEXT_MEETING_TIME) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.NEXT_MEETING_TIME_VN)) {
                                isValidSheet = false;
                                //System.out.println("NEXT_MEETING_TIME HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 15) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.AGENT_CODE) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.AGENT_CODE_VN)) {
                                isValidSheet = false;
                                //System.out.println("AGENT_CODE HEADER NOT VALID");
                                return isValidSheet;
                            } else if (myCell.getColumnIndex() > 15) {
                                break;
                            }
                        }

                    }// column while
                }// row while
            }

        } catch (Exception e) {
            logger.error(LoggerUtil.getLogMessage("Error while FileAction.uploadAgentToDB", "public exception"), e);
            isValidSheet=false;
        }
        return isValidSheet;

    }

    private boolean validateAchvSheet(String filename) throws Exception {
        boolean isValidSheet = true;
        try {
            String fileExtn = getFileExtension(filename);
            Workbook wb_xssf; // Declare XSSF WorkBook
            Workbook wb_hssf; // Declare HSSF WorkBook
            Sheet sheet1 = null; // sheet can be used as common for XSSF and
                                 // HSSF

            if (fileExtn.equalsIgnoreCase("xlsx")) {
                wb_xssf = new XSSFWorkbook(filename);
                sheet1 = wb_xssf.getSheetAt(0);
            }
            if (fileExtn.equalsIgnoreCase("xls")) {
                FileInputStream myInput = new FileInputStream(filename);
                POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
                wb_hssf = new HSSFWorkbook(myFileSystem);

                sheet1 = wb_hssf.getSheetAt(0);
            }

            Iterator rows = sheet1.rowIterator();

            while (rows.hasNext()) {
                Row myRow = (Row) rows.next();

                if (myRow.getRowNum() == 0) {

                    Iterator cellIter = myRow.cellIterator();

                    while (cellIter.hasNext()) {
                        Cell myCell = (Cell) cellIter.next();

                        if (myCell.getColumnIndex() == 0) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NAME) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NAME_VN)) {
                                isValidSheet = false;
                               // System.out.println("CUSTOMER'S FULL NAME HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 1) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_GENDER) &&
                            		!((returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_GENDER_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S GENDER HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 2) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NATIONAL_ID) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NATIONAL_ID_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S NATIONAL ID HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 3) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_MOBILE) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_MOBILE_VN))) {
                                isValidSheet = false;
                               // System.out.println("CUSTOMER'S MOBILE NUMBER HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 4) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_EMAIL) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_EMAIL_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S EMAIL NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 5) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_ADDRESS) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_ADDRESS_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S ADDRESS HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 6) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_DATE_OF_BIRTH) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_DATE_OF_BIRTH_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S DATE OF BIRTH HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 7) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.POTENTIAL) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.POTENTIAL_VN))) {
                                isValidSheet = false;
                                //System.out.println("POTENTIAL HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 8) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.BANCA_CUSTOMER_STATE) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.BANCA_CUSTOMER_STATE_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S STATE HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 9) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NATIONALITY)&&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NATIONALITY_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S NATIONALITY HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 10) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_ANNUAL_INCOME) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_ANNUAL_INCOME_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S ANNUAL INCOME HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 11) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NEED) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_NEED_VN))) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S NEED HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 12) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.SOURCE) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.SOURCE_VN))) {
                                isValidSheet = false;
                                //System.out.println("SOURCE HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 13) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_OCCUPATION) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_OCCUPATION_VN)) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER'S OCCUPATION HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 14) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.ACTION) &&
                            		(!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.ACTION_VN))) {
                                isValidSheet = false;
                                //System.out.println("ACTION HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 15) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.NEXT_MEETING_DATE) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.NEXT_MEETING_DATE_VN)) {
                                isValidSheet = false;
                                //System.out.println("NEXT_MEETING_DATE HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 16) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.NEXT_MEETING_TIME) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.NEXT_MEETING_TIME_VN)) {
                                isValidSheet = false;
                                //System.out.println("NEXT_MEETING_TIME HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 17) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_BANK_BRANCH) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_BANK_BRANCH_VN)) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER_BANK_BRANCH HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 18) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_BANK) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.CUSTOMER_BANK_VN)) {
                                isValidSheet = false;
                                //System.out.println("CUSTOMER_BANK HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 19) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.REFERRER_NAME) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.REFERRER_NAME_VN)) {
                                isValidSheet = false;
                                //System.out.println("REFERRER_NAME HEADER NOT VALID");
                                return isValidSheet;
                            }
                        } else if (myCell.getColumnIndex() == 20) {
                            if (!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.MOBILE_NUMBER) &&
                            		!(returnStringCell(myCell)).trim().equalsIgnoreCase(Constants.MOBILE_NUMBER_VN)) {
                                isValidSheet = false;
                                //System.out.println("MOBILE_NUMBER HEADER NOT VALID");
                                return isValidSheet;
                            } else if (myCell.getColumnIndex() > 20) {
                                break;
                            }
                        }

                    }// column while
                }// row while
            }

        } catch (Exception e) {
            logger.error(LoggerUtil.getLogMessage("Error while FileAction.uploadAgentToDB", "public exception"), e);
            isValidSheet=false;
        }
        return isValidSheet;

    }
    private boolean validateEmptyExcel(String filename) throws Exception {
        boolean emptyExcel = false;
        int flag=1;
        try {
            String fileExtn = getFileExtension(filename);
            Workbook wb_xssf; // Declare XSSF WorkBook
            Workbook wb_hssf; // Declare HSSF WorkBook
            Sheet sheet2 = null; // sheet can be used as common for XSSF and
                                 // HSSF
           if (fileExtn.equalsIgnoreCase("xlsx")) {
                wb_xssf = new XSSFWorkbook(filename);
                sheet2 = wb_xssf.getSheetAt(0);
            }
            if (fileExtn.equalsIgnoreCase("xls")) {
                FileInputStream myInput = new FileInputStream(filename);
                POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
                wb_hssf = new HSSFWorkbook(myFileSystem);
                sheet2 = wb_hssf.getSheetAt(0);
            }
           for (int rowNum = 1; rowNum <=sheet2.getLastRowNum(); rowNum++) {
        	   		Row r = sheet2.getRow(rowNum);
        	   		if(r==null)
        	   			continue;
        	   		int lastColumn = r.getLastCellNum();
        	   		for (int cn = 0; cn <lastColumn; cn++) {
        	   				Cell c = r.getCell(cn, Row.RETURN_BLANK_AS_NULL);
        	   					if (c == null) {
        	   						flag=1;
        	         // The spreadsheet is empty in this cell
        	   						} 
        	   					else 
        	   						{
        	   							flag=0;
        	   							break;
        	   						}
        	   			}
        	   			if(flag==0)
        	   				break;
        	}
           if(flag==1)
           		emptyExcel=true;
        }
        catch (Exception e) {
            logger.error(LoggerUtil.getLogMessage("Error while FileAction.uploadAgentToDB", "public exception"), e);
            emptyExcel=false;
        }
        return emptyExcel;

    }
    public String returnStringCell(Cell dataCell) {
        String value = null;
        if (dataCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            value = String.valueOf((long) dataCell.getNumericCellValue());

        } else if (dataCell.getCellType() == Cell.CELL_TYPE_STRING) {

            value = dataCell.getStringCellValue();
        }
        return value;
    }

    public Long returnNumericCell(Cell dataCell) {
        Double value = null;
        if (dataCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            value = dataCell.getNumericCellValue();

        } else if (dataCell.getCellType() == Cell.CELL_TYPE_STRING) {

            value = Double.parseDouble(dataCell.getStringCellValue());
        }
        return value != null ? value.longValue() : null;
    }

    public Date returnDateCell(Cell dataCell) {
        Date value = null;
        if (DateUtil.isCellDateFormatted(dataCell)) {
            value = dataCell.getDateCellValue();
        }
        return value;
    }
    
    public void init(ServletConfig config)throws ServletException {
		super.init(config);
		 springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
	        final AutowireCapableBeanFactory beanFactory = springContext.getAutowireCapableBeanFactory();
	        beanFactory.autowireBean(this);
    //SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
      //config.getServletContext());
  }

}
