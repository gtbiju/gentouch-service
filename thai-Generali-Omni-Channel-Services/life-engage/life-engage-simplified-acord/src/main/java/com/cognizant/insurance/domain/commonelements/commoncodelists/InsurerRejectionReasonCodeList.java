/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of Insurers according to their rejection reason.
 * 
 * @author 301350
 * 
 * 
 */
public enum InsurerRejectionReasonCodeList {
    /**
     * Identifies a Insurer with rejection reason 'High risk'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    HighRisk,
    /**
     * Identifies a Insurer with rejection reason 'History of fraud'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    HistoryOfFraud,
    /**
     * 
     * 
     * 
     * 
     */
    Unspecified
}
