package com.cognizant.insurance.omnichannel.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.agent.AgentAchievement;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliAgentAddress;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.component.impl.LifeEngageAgentComponentImpl;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.contactandplace.ContactPreference;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.TelephoneCallContact;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.party.partyname.PersonName;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.omnichannel.component.GeneraliLifeEngageAgentComponent;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliAgentProfileRepository;
import com.cognizant.insurance.omnichannel.component.repository.UserRepository;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.vo.AgentProfileResponse;
import com.cognizant.insurance.response.vo.GeneraliAgentProfileResponse;
import com.cognizant.insurance.response.vo.ResponseInfo;
import com.google.common.base.Strings;

/**
 * The Class LifeEngageAgentComponentImpl.
 */
@Service("generaliLifeEngageAgentComponent")
public class GeneraliLifeEngageAgentComponentImpl extends LifeEngageAgentComponentImpl
		implements GeneraliLifeEngageAgentComponent {

	@Autowired
	GeneraliAgentProfileRepository agentRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	@Qualifier("agentProfileMapping")
	LifeEngageSmooksHolder agentSmooksHolder;

	@Autowired
	@Qualifier("leadReassignAgentProfileMapping")
	LifeEngageSmooksHolder leadReassignSmooksHolder;

	@Autowired
	@Qualifier("leadReassignCustomerProfileMapping")
	LifeEngageSmooksHolder leadReassignCustomerSmooksHolder;

	private static final String AGENT_ID = "Key11";

	private static final String KEY1 = "Key1";

	public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliLifeEngageAgentComponentImpl.class);

	/**
	 * Retrieve agent profile.
	 * 
	 * @param agentID
	 *            the agent id
	 * @return the string
	 * @throws BusinessException
	 *             the business exception
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
	public String retrieveAgentProfileByCode(RequestInfo requestInfo, JSONArray jsonArray) throws BusinessException {

		String response = null;
		ProductSpecification prodTypeSpecification = new ProductSpecification();

		if (jsonArray.length() > 0) {
			final JSONObject jsonObj = jsonArray.getJSONObject(0);
			final String agentID = getStringKeyValue(AGENT_ID, jsonObj);

			if (Strings.isNullOrEmpty(agentID)) {
				throwBusinessException(true, "Key11 is null or empty!");
			}

			GeneraliGAOAgent gaoAgent = null;
    		GeneraliAgentAddress address = null;    		
			final GeneraliAgent agent = agentRepository.retrieveAgentODSDetailsByCode(requestInfo, agentID);

			if(agent == null){
    			gaoAgent = agentRepository.retrieveGAOAgentODSDetailsByCode(requestInfo, agentID);
    			address = agentRepository.retrieveGAOAddress(requestInfo, gaoAgent.getGaoCode());
    		}
			final List<AgentAchievement> agentAchievementResponse = agentRepository
					.retrieveAgentAchievementDetails(requestInfo, agentID);

			// Added for retrieving agents emailID
			Transactions transactions = new Transactions();
			transactions.setKey1(agentID);
			Response<GeneraliTHUser> generaliTHUser = userRepository.validateLoggedInUser(transactions);

			GeneraliTHUser thUser = new GeneraliTHUser();
			if (generaliTHUser.getType() != null) {
				thUser = generaliTHUser.getType();
			}
			// Added for SalesIllustration - Agents have permission to access
			// all OL products,if SO_LIC_EXPIRE_DATE is
			// valid in ODS table
			String licenseExpDate = StringUtils.EMPTY;
    		
    		if(agent != null){
    			licenseExpDate = agent.getLicenseExpireDate();
    		} else if(gaoAgent != null){
    			licenseExpDate = gaoAgent.getLicenseExpireDate();
    		}
    		
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(new Date());
			Map<String, String> agentProductList = new HashMap<String, String>();

			if (licenseExpDate.compareTo(date) >= 0) {
				final List<ProductSpecification> agentProductResponse = agentRepository
						.retrieveAgentODSDetailsByProductCode(requestInfo, prodTypeSpecification);
				for (ProductSpecification prodSpecification : agentProductResponse) {
					// 51001 - Traditional (OL Type)
					if (!agentProductList.containsKey(prodSpecification.getProductCode())
							&& !agentProductList.containsValue(prodSpecification.getName())) {
						agentProductList.put(prodSpecification.getProductCode(), prodSpecification.getName());
					}
				}
			}
			final AgentProfileResponse agentResponse = new AgentProfileResponse();
			final Agent agentMail = new Agent();
			agentMail.setEmailId(thUser.geteMail());
			agentResponse.setAgentProductList(agentProductList);
			agentResponse.setAgentAchievementList(agentAchievementResponse);
			agentResponse.setResponseInfo(buildResponseInfo(requestInfo));
			if(agent != null){
    			agentResponse.setGeneraliAgent(agent);
    		} else if(gaoAgent != null){
    			agentResponse.setGaoAgent(gaoAgent);
    		}
    		
    		if(address != null){
    			agentResponse.setAddress(address);
    		}
    		agentResponse.setAgent(agentMail);
			response = agentSmooksHolder.parseBO(agentResponse);
		}
		return response;
	}

	@Override
	@Transactional
	public String retrieveAgentsForReassign(RequestInfo requestInfo, JSONArray jsonArray) throws BusinessException {
		List<GeneraliAgent> terminatedAgents = new ArrayList<GeneraliAgent>();
		List<GeneraliAgent> activeAgents = new ArrayList<GeneraliAgent>();
		String result = StringUtils.EMPTY;
		
		if (jsonArray.length() > 0) {
			final JSONObject jsonObj = jsonArray.getJSONObject(0);
			final String agentID = getStringKeyValue(AGENT_ID, jsonObj);
			String userAction = getStringKeyValue(KEY1, jsonObj);
			String agentStatus = StringUtils.EMPTY;

			if (userAction.equalsIgnoreCase("activeAgents")) {
				agentStatus = "inforce";
				activeAgents = agentRepository.retrieveActiveAgentCodeToReassign(requestInfo, agentID, agentStatus);
				
			} else {
				agentStatus = "terminate";
				terminatedAgents = agentRepository.retrieveAgentODSDetailsForReassign(requestInfo, agentID, agentStatus);
			}

			if (Strings.isNullOrEmpty(agentID)) {
				throwBusinessException(true, "Key11 is null or empty!");
			}
			
			List<GeneraliAgent> activeAgentsList = new ArrayList<GeneraliAgent>();
			List<GeneraliAgent> terminatedAgentsList = new ArrayList<GeneraliAgent>();
			
			for (GeneraliAgent agentDetails : terminatedAgents) {				
				if(agentDetails.getAgentStatus().equalsIgnoreCase("terminate")) {					
					terminatedAgentsList.add(agentDetails);
				}				
			}
			
			for(GeneraliAgent agentDetails : activeAgents) {	
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String date = sdf.format(new Date());
				
				if(agentDetails.getAgentStatus().equalsIgnoreCase("inforce") 
						&& agentDetails.getLicenseExpireDate().compareTo(date) >= 0) {
					activeAgentsList.add(agentDetails);
				}
			}

			final GeneraliAgentProfileResponse agentResponse = new GeneraliAgentProfileResponse();
			agentResponse.setResponseInfo(buildResponseInfo(requestInfo));
			agentResponse.setTerminatedAgentList(terminatedAgentsList);
			agentResponse.setActiveAgentList(activeAgentsList);
			result = leadReassignSmooksHolder.parseBO(agentResponse);
		}
		return result;
	}

	@Override
	public String validateReassignLogin(String agentId, String password) {
		String status = Constants.FAILURE;
		Transactions transactions = new Transactions();
		transactions.setKey1(agentId);
		Transactions validateAgentProfile = userRepository.validateAgentProfile(transactions);
		GeneraliAgent agent = validateAgentProfile.getGeneraligent();
		List<String> positionList = Arrays.asList("D1", "D2", "D3", "E1", "E2", "E3", "SM");
		if (agent.getAgentStatus().equalsIgnoreCase("INFORCE") && positionList.contains(agent.getAgentPositionCode())) {
			Response<GeneraliTHUser> generaliTHUser = userRepository.validateLoggedInUser(transactions);
			StandardPasswordEncoder encoder = new StandardPasswordEncoder();
			GeneraliTHUser user = generaliTHUser.getType();
			if (user != null) {

				if (isPasswordMatched(encoder, user.getPassword(), password)) {
					status = Constants.SUCCESS;
					
				} else {
					status = Constants.INVALID_AGENTCODE_PWD_ERROR_MSG;
				} 
			}
		}

		return status;
	}

	private boolean isPasswordMatched(StandardPasswordEncoder encoder, String passwordResponse,
			String currentPassword) {

		if (encoder.matches(currentPassword, passwordResponse)) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public String retrieveLeads(RequestInfo requestInfo, JSONArray jsonArray) throws BusinessException {
		final List<Customer> customers;
		String result = StringUtils.EMPTY;
		if (jsonArray.length() > 0) {
			final JSONObject jsonObj = jsonArray.getJSONObject(0);
			final String agentID = getStringKeyValue(AGENT_ID, jsonObj);

			if (Strings.isNullOrEmpty(agentID)) {
				throwBusinessException(true, "Key11 is null or empty!");
			}

			customers = agentRepository.retrieveLeads(requestInfo, agentID);
			for (Customer customer : customers) {

				Date creationDate = customer.getCreationDateTime();
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
				String creationDateString = dateFormat.format(creationDate);
				Person person = (Person) customer.getPlayerParty();
				PersonName name = person.getName().iterator().next();
				String givenName = name.getGivenName();
				String lastName = name.getSurname();
				String identifier = customer.getIdentifier();
				customer.getRelatedCustomers();
				String fullName = givenName + " "+ lastName;
				customer.getTransactionKeys().setKey31(fullName);
				customer.getTransactionKeys().setKey32(creationDateString);
				customer.getTransactionKeys().setKey33(identifier);
				String transTrackingID = customer.getTransTrackingId();
				String mobileNumber = getMobileNumber(person);
				customer.getTransactionKeys().setKey34(mobileNumber);
				Integer illustrationCount = agentRepository.retrieveIllustrationCount(requestInfo, transTrackingID);
				Integer eAppCount = agentRepository.retrieveEappCount(requestInfo, transTrackingID);
				String fnaCount = agentRepository.retrieveFnaCount(requestInfo, transTrackingID)>0?"Yes":"No";
				customer.getTransactionKeys().setKey36(illustrationCount.toString());
				customer.getTransactionKeys().setKey37(eAppCount.toString());
				customer.getTransactionKeys().setKey35(fnaCount); 

			}

			final GeneraliAgentProfileResponse agentResponse = new GeneraliAgentProfileResponse();
			agentResponse.setResponseInfo(buildResponseInfo(requestInfo));
			agentResponse.setCustomerList(customers);
			result = leadReassignCustomerSmooksHolder.parseBO(agentResponse);
		}
		return result;
	}

	/**
	 * @param person
	 * @return mobile number of the customer
	 */
	private String getMobileNumber(Person person) {
		try {
			Iterator<ContactPreference> ltr = (Iterator<ContactPreference>) person.getPreferredContact().iterator();
			while (ltr.hasNext()) {
				ContactPreference cp = (ContactPreference) ltr.next();
				if (cp.getPreferredContactPoint() instanceof TelephoneCallContact) {
					TelephoneCallContact teleContact = (TelephoneCallContact) cp.getPreferredContactPoint();
					return teleContact.getFullNumber();
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occurred: " + e.getMessage(), e);
		}
		return StringUtils.EMPTY;
	}

	/**
	 * Builds the response info.
	 * 
	 * @param requestInfo
	 *            the request info
	 * @return the response info
	 */
	private ResponseInfo buildResponseInfo(final RequestInfo requestInfo) {
		final ResponseInfo responseInfo = new ResponseInfo();
		responseInfo.setCreationDateTime(requestInfo.getCreationDateTime());
		responseInfo.setRequestorToken(requestInfo.getRequestorToken());
		responseInfo.setSourceInfoName(requestInfo.getSourceInfoName());
		responseInfo.setTransactionId(requestInfo.getTransactionId());
		responseInfo.setUserName(requestInfo.getUserName());
		return responseInfo;
	}

	/**
	 * Gets the string key value.
	 * 
	 * @param key
	 *            the key
	 * @param jsonObj
	 *            the json obj
	 * @return the string key value
	 */
	private static String getStringKeyValue(final String key, final JSONObject jsonObj) {
		String result = null;
		if (jsonObj.has(key)) {
			result = jsonObj.getString(key);
		}

		return result;
	}

	public GeneraliAgentProfileRepository getAgentProfileRepository() {
		return agentRepository;
	}

	public void setAgentProfileRepository(GeneraliAgentProfileRepository agentRepository) {
		this.agentRepository = agentRepository;
	}

}
