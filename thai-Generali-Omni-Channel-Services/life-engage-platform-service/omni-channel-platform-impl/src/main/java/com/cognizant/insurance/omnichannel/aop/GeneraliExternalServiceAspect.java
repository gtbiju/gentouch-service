package com.cognizant.insurance.omnichannel.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.AfterThrowing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * @author 481774 The Class class GeneraliExternalServiceAspect
 */

@Aspect
@Component
public class GeneraliExternalServiceAspect {

    /** The Constant FAILURE. */
    public static final String FAILURE = "FAILURE";

    /** The Constant SUCCESS. */
    public static final String SUCCESS = "SUCCESS";

    /** The Constant AUTHENTICATE_USER. */
    public static final String AUTHENTICATE = "authenticate";

    /** The Constant RETRIEVE_AGENT_PROFILE_BYCODE. */
    public static final String RETRIEVE_AGENT_PROFILE_BYCODE = "retrieveAgentProfileByCode";
    
    /** The Constant SEND_EMAIL. */
    public static final String SEND_EMAIL = "sendEmail";

    /** The Constant ICONNECT. */
    public static final String ICONNECT = "iConnect";    


    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliExternalServiceAspect.class);

    @Pointcut("execution(* com.cognizant.generali.security.provider..*.*(..)) || execution(* com.cognizant.insurance.omnichannel.component.impl..*.*(..))")
    public
            void externalServiceMethodExecution() {
    }

    @AfterThrowing(pointcut = "externalServiceMethodExecution()", throwing = "ex")
    public void AfterThrowingAdvice(JoinPoint joinPoint, Throwable ex) {

        String serviceName = joinPoint.getSignature().getName();

        if (AUTHENTICATE.equals(serviceName)) {
            LOGGER.error("{} - {} - {}", ICONNECT, serviceName, FAILURE);
        } else if (RETRIEVE_AGENT_PROFILE_BYCODE.equals(serviceName)) {
            LOGGER.error("{} - {} - {}", ICONNECT, serviceName, FAILURE);
        } 

    }

    @AfterReturning(pointcut = "externalServiceMethodExecution()", returning = "retVal")
    public void afterAdvice(JoinPoint joinPoint, Object retVal) {
        String serviceName = joinPoint.getSignature().getName();

        if (AUTHENTICATE.equals(serviceName)) {
            LOGGER.info("{} - {} - {}", ICONNECT, serviceName, SUCCESS);
        } else if (RETRIEVE_AGENT_PROFILE_BYCODE.equals(serviceName)) {
            LOGGER.info("{} - {} - {}", ICONNECT, serviceName, SUCCESS);
        } 
    }
}
