/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 356551
 * @version       : 0.1, Mar 10, 2016
 */
package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * The Interface interface EncryptionKeyService.
 */
public interface EncryptionKeyService {

	/**
     * Fetch key.
     *
     * @param json the json
     * @return the string
     * @throws BusinessException the business exception
     */
    String fetchKey(final String json) throws BusinessException;
}
