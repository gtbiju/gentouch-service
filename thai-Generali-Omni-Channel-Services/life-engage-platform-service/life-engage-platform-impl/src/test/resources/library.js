function abs(arg){
	return Math.abs(arg);
}

function ceil(arg){
	return Math.ceil(arg);
}

function floor(arg){
	return Math.floor(arg);
}

function sqrt(arg){
	return Math.sqrt(arg);
}

function exp(arg){
	return Math.exp(arg);
}
function cbrt(arg) {
    var sign = arg === 0 ? 0 : arg > 0 ? 1 : -1;
    var result =  sign * Math.pow(Math.abs(arg), 1 / 3);
	return result;
}
function formatDecimal(){
	var l = arguments.length,result;
	if(l == 1){
		result = arguments[0].toFixed();
	} else if(l == 2){
		result = arguments[0].toFixed(arguments[1]);
	}  
	return result;
}

function maxOfNValues(){
	var l = arguments.length, i, result;
	var valueList = [];
	for (i = 0; i < l; i++) {
		valueList.push(arguments[i]);
	}
	result = Math.max.apply( null, valueList);
	return result;
}

function minOfNValues(){
	var l = arguments.length, i, result;
	var valueList = [];
	for (i = 0; i < l; i++) {
		valueList.push(arguments[i]);
	}
	result = Math.min.apply( null, valueList);
	return result;
}

//////String functions//////
function toUpperCase(arg){
	return arg.toUpperCase();
}

function toLowerCase(arg){
	return arg.toLowerCase();
}

function trim(arg){
	return arg.replace(/^\s+|\s+$/g,'');
}

function ltrim(arg) {
	return arg.replace(/^\s+/,"");
}
function rtrim(arg) {
	return arg.replace(/\s+$/,"");
}

//arguments[0]-string value,arguments[1]-begin index,arguments[2]- end index
function substring(){
	var l = arguments.length,result;
	if(l == 2){
		result = arguments[0].substring(arguments[1]);
	} else if(l == 3){
		result = arguments[0].substring(arguments[1],arguments[2]);
	}  
	return result;
}

//arguments[0]-string value,arguments[1]-regex,arguments[2]- limit
function splitByRegex(){
	var l = arguments.length,result;

	if(l == 2){
		result = arguments[0].split(arguments[1]);
	} else if(l == 3){
		result = arguments[0].split(arguments[1],arguments[2]);
	} 
	return result;
}

//arguments[0]-array,arguments[1]-start,arguments[2]- end
function slice(){
var l = arguments.length,result;

	if(l == 2){
		result = arguments[0].slice(arguments[1]);
	} else if(l == 3){
		result = arguments[0].slice(arguments[1],arguments[2]);
	} 
	return result;
}

function indexOf(){
	var l = arguments.length,result;
	if(l == 2){
		result = arguments[0].indexOf(arguments[1]);
	} else if(l == 3){
		result = arguments[0].indexOf(arguments[1],arguments[2]);
	} 
	return result;
}

function lastIndexOf(){
	var l = arguments.length,result;
	if(l == 2){
		result = arguments[0].lastIndexOf(arguments[1]);
	} else if(l == 3){
		result = arguments[0].lastIndexOf(arguments[1],arguments[2]);
	} 
	return result;
}

function equlasIgnoreCase(arg1,arg2){
	return (arg1.toLowerCase() == arg2.toLowerCase());
}

function isEmpty(arg){
	return (arg == "" || !arg.length);

}

function isUndefined(arg){
	if(typeof arg == 'undefined'){
		return true;
	} else {
	return false;
	}
}


function pushArray(arg1,arg2){
	arg1.push.apply(arg1,arg2);
	return arg1;

}


/////////Newly added functions end///////////

function add() {
	var l = arguments.length, i, sum = 0;
	for (i = 0; i < l; i++) {
		sum = parseFloat(sum, 10) + parseFloat(arguments[i], 10);
	}
	return sum;
}

function sum() {
	var l = arguments.length, i, sum = 0;
	for (i = 0; i < l; i++) {
		sum = parseFloat(sum, 10) + parseFloat(arguments[i], 10);
	}
	return sum;
}


function sub() {
	var l = arguments.length, i, diff = arguments[0];
	for (i = 1; i < l; i++) {
		diff -= arguments[i];
	}
	return diff;
}

function mul() {
	var l = arguments.length, i, product = 1;
	for (i = 0; i < l; i++) {
		product *= arguments[i];
	}
	return product;
}

function div() {
	var l = arguments.length, i, quotient = arguments[0];
	for (i = 1; i < l; i++) {
		quotient /= arguments[i];
	}
	return quotient;
}

function pow(arg1, arg2) {
	return Math.pow(arg1, arg2);
}

function round(arg) {
	return Math.round(arg);
}

function getAge(childDOB) {
	birthDate = new Date(childDOB);
	today = new Date();
	calculateAge = Math.floor((today - birthDate)
			/ (365.25 * 24 * 60 * 60 * 1000));
	age = parseInt(calculateAge, 10)
	age = calculateAge.toFixed(0);
	return age;
}

function getDurationOfYear(yearsFromNowFundRequired) {
	today = new Date();
	year = today.getFullYear();
	duringYear = parseInt(year, 10) + parseInt(yearsFromNowFundRequired, 10);
	duringYear = duringYear.toFixed(0);
	return duringYear;
}

function split(value) {
	returnvalue = value.split(',');
	return returnvalue;
}

function format(val) {
	returnVal = val.toString();
	return returnVal;
}

function max(value1, value2) {
	if (value1 >= value2) {
		result = value1;
	} else if (value1 < value2) {
		result = value2;
	}
	return result;
}

function min(value1, value2) {
	if (value1 <= value2) {
		result = value1;
	} else if (value1 > value2) {
		result = value2;
	}
	return result;
}

function length(value) {
	result = value.length;
	return result;
}

function lookup() {
	var params = [];
	for ( var i = 1; i < arguments.length; i++) {
		params[i - 1] = arguments[i];
	}
	tableName = arguments[0];
	response = LookUpDB.lookup(tableName, params);
	jsonResponse = JSON.parse(response);
	return jsonResponse;
}

function lookupWhere(tableName, condition) {
	response = LookUpDB.lookupWhere(tableName, condition);
	jsonResponse = JSON.parse(response);
	return jsonResponse;
}

function lookupAsJson() {

	var params = [];
	for ( var i = 2; i < arguments.length; i++) {
		params[i - 2] = arguments[i];
	}
	field = arguments[0];
	tableName = arguments[1];
	response = LookUpDB.lookupAsJson(field, tableName, params);
	jsonResponse = JSON.parse(response);
	return jsonResponse;
}

function searchData(obj, key, val) {
	var objects = [];
	for ( var i in obj) {
		if (!obj.hasOwnProperty(i))
			continue;
		if (typeof obj[i] == 'object') {
			objects = objects.concat(searchData(obj[i], key, val));
		} else if (i == key && obj[key] == val) {
			return true;
		}
	}
	return objects;
}

function updateJSON(jsondata, key, value) {
	try {
		if (jsondata === null) {
			return "null";
		} else if (jsondata === undefined) {
			return "undefined";
		} else if (typeof jsondata == 'string') {
			jsondata = JSON.parse(jsondata);

		}

		jsondata[key] = value;
	} catch (exception) {
		jsondata = null;
	}
	return jsondata;
}

function containsInArray(sourceArray, targetArrayString) {
	if(sourceArray ==null ||sourceArray == 'undefined'){return false;}
		var isContains = checkArray(sourceArray, targetArrayString);
		return (isContains == 0 ? true : false);
	 
}
function containsNotInArray(sourceArray, targetArrayString) {
		
	if(sourceArray ==null ||sourceArray == 'undefined'){return false;}
	 
		var isContains = checkArray(sourceArray, targetArrayString);
		return (isContains == -1 ? true : false);
	 
		
}
function checkArray(sourceArray, targetArrayString) {
	var targetArray = targetArrayString.split(',');
	var sourceArray = sourceArray;
	var isContains=-1;
	for (cntSourceArray = 0; cntSourceArray < sourceArray.length; cntSourceArray++) {
		var value = sourceArray[cntSourceArray];
		for(var innerCount = 0 ;innerCount< targetArray.length; innerCount++  ){
			if(value==targetArray[innerCount]){
				isContains = 0;
				break;
			}
		} 
	}
	return isContains;
}

function removeItemFromArray(array, key, item) { 
	for ( var i in array[key]) {
		if (array[key][i] === item[0]) {
			array[key].splice(i, 1);
			break;
		}
	}
}

function addToArray(inoutMap,sourceKey,targetKey) {

	if (inoutMap[sourceKey]) {		
		if(inoutMap[sourceKey]===inoutMap[targetKey][0])
			inoutMap[sourceKey].push(inoutMap[targetKey][0]);
	} else {
		updateJSON(inoutMap, sourceKey, inoutMap.formName);
	}

}

function removeFromArray(inoutMap,sourceKey,targetKey) {
	removeItemFromArray(inoutMap, sourceKey, inoutMap[targetKey]);
}


function createTable(inoutmap, arrayname,index, key,value) {
    
    if (inoutmap != null) {
        if (typeof inoutmap[arrayname] == 'undefined') {
            var arr = new Array();
            var obj = {};
            obj[key] = value;
            arr.push(obj);
            inoutmap[arrayname] = arr;
        } else {
            if (typeof inoutmap[arrayname][index] != 'undefined') {
                inoutmap[arrayname][index][key] = value;
        }
        else {
            var obj = {};
            obj[key] = value;
            //inoutmap
            inoutmap[arrayname][index] = obj;
        }
           
         }
        
    }
}

function createObject(inoutmap,objname, key, value) {

    if (inoutmap != null) {
        if (typeof inoutmap[objname] == 'undefined') {
            var obj = {};
            obj[key] = value;
            inoutmap[objname] = obj;
        } else {
            inoutmap[objname][key] = value;
        }

    }
}


//Aneesh

function getBandId(sumAssured,productCode)	{
	var band_id;
	var max_MIN_SA = lookupAsJson("MAX(MIN_SA)", "EXTN_BAND_TBL", "PROD_ID='"
			+ productCode + "'");
	if (sumAssured >= max_MIN_SA) {
		band_id =  lookupAsJson("BAND_ID", "EXTN_BAND_TBL", "MIN_SA <= '" + sumAssured + "'", "PROD_ID='"
				+ productCode + "'");
	}else{
		band_id =  lookupAsJson("BAND_ID", "EXTN_BAND_TBL", "MIN_SA <='" + sumAssured + "'", 
				"MAX_SA>='" + sumAssured + "'", "PROD_ID='"+ productCode + "'");
	}
	return 	band_id;	
}




function getPremiumRate(age,PPT,bandID,productCode,policyTerm)	{
	var rate;
	if(policyTerm!=""){
	rate = lookupAsJson("PREMIUM_VALUE", "EXTN_PREMIUM_SPEC_TBL", "ISSUE_AGE ='" + age + "'", "PREMIUM_PAYING_TERM ='" + PPT + "'", "BAND_VALUE ='" + bandID + "'", "PROD_ID ='" + productCode + "'", "POLICY_TERM ='" + policyTerm + "'");
	}else{
	rate = lookupAsJson("PREMIUM_VALUE", "EXTN_PREMIUM_SPEC_TBL", "ISSUE_AGE ='" + age + "'", "PREMIUM_PAYING_TERM ='" + PPT + "'", "BAND_VALUE ='" + bandID + "'", "PROD_ID ='" + productCode + "'");
	}
	return rate;
}

//Aneesh