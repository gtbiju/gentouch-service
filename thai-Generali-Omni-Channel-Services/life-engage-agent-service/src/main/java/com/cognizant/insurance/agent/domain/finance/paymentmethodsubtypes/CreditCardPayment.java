/**
 * 
 */
package com.cognizant.insurance.agent.domain.finance.paymentmethodsubtypes;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.CreditCardTypeCodeList;
import com.cognizant.insurance.agent.domain.finance.bankaccount.BankAccount;
import com.cognizant.insurance.agent.domain.finance.financialactivitysubtypes.PaymentMethod;
import com.cognizant.insurance.agent.domain.party.partyname.PartyName;

/**
 * This concept states a payment by credit card. 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("CREDITCARD")
public class CreditCardPayment extends PaymentMethod {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4398003747175655476L;

    /**
     * The credit card identification number.
     * 
     * 
     * 
     */
    private Long cardIdentifier;

    /**
     * Identifies the type of credit card .
     * 
     * 
     * 
     */
    @Enumerated
    private CreditCardTypeCodeList creditCardTypeCode;

    /**
     * This relationship links a credit card with its bank account.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private BankAccount relatedBankAccount;

    /**
     * The brand of the financial transaction card.
     * 
     * e.g: American Express
     * 
     * e.g: Diner's Club
     * 
     * e.g: Master Card
     * 
     * e.g: Visa
     * 
     * 
     * 
     */
    @Enumerated
    private CreditCardTypeCodeList brandCode;

    /**
     * The expiry year of the credit card.
     * 
     * 
     * 
     * 
     * 
     */
    private Integer expiryYear;

    /**
     * The expiry month of the credit card.
     * 
     * 
     * 
     */
    private Integer expiryMonth;

    /**
     * The credit card authentication number (for validation purposes).
     * 
     * 
     * 
     */
    private Integer cvv2;

    /**
     * The personal identification number used for authorizing access or usage.
     * 
     * 
     * 
     */
    private String pin;

    /**
     * This relationship links a credit card payment with the party name
     * physically listed on the credit card (e.g. the card-holder's name).
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private PartyName cardholderName;

    /**
     * Added for generali
     */
    @Temporal(TemporalType.DATE)
    private Date expiryDate;

    /**
     * Added for generali
     */
    @Temporal(TemporalType.DATE)
    private Date effectiveDate;
    
    
    
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * Gets the cardIdentifier.
     * 
     * @return Returns the cardIdentifier.
     */
    public Long getCardIdentifier() {
        return cardIdentifier;
    }

    /**
     * Gets the creditCardTypeCode.
     * 
     * @return Returns the creditCardTypeCode.
     */
    public CreditCardTypeCodeList getCreditCardTypeCode() {
        return creditCardTypeCode;
    }

    /**
     * Gets the relatedBankAccount.
     * 
     * @return Returns the relatedBankAccount.
     */
    public BankAccount getRelatedBankAccount() {
        return relatedBankAccount;
    }

    /**
     * Gets the brandCode.
     * 
     * @return Returns the brandCode.
     */
    public CreditCardTypeCodeList getBrandCode() {
        return brandCode;
    }

    /**
     * Gets the expiryYear.
     * 
     * @return Returns the expiryYear.
     */
    public Integer getExpiryYear() {
        return expiryYear;
    }

    /**
     * Gets the expiryMonth.
     * 
     * @return Returns the expiryMonth.
     */
    public Integer getExpiryMonth() {
        return expiryMonth;
    }

    /**
     * Gets the cvv2.
     * 
     * @return Returns the cvv2.
     */
    public Integer getCvv2() {
        return cvv2;
    }

    /**
     * Gets the pin.
     * 
     * @return Returns the pin.
     */
    public String getPin() {
        return pin;
    }

    /**
     * Gets the cardholderName.
     * 
     * @return Returns the cardholderName.
     */
    public PartyName getCardholderName() {
        return cardholderName;
    }

    /**
     * Sets The cardIdentifier.
     * 
     * @param cardIdentifier
     *            The cardIdentifier to set.
     */
    public void setCardIdentifier(final Long cardIdentifier) {
        this.cardIdentifier = cardIdentifier;
    }

    /**
     * Sets The creditCardTypeCode.
     * 
     * @param creditCardTypeCode
     *            The creditCardTypeCode to set.
     */
    public void setCreditCardTypeCode(
            final CreditCardTypeCodeList creditCardTypeCode) {
        this.creditCardTypeCode = creditCardTypeCode;
    }

    /**
     * Sets The relatedBankAccount.
     * 
     * @param relatedBankAccount
     *            The relatedBankAccount to set.
     */
    public void
            setRelatedBankAccount(final BankAccount relatedBankAccount) {
        this.relatedBankAccount = relatedBankAccount;
    }

    /**
     * Sets The brandCode.
     * 
     * @param brandCode
     *            The brandCode to set.
     */
    public void setBrandCode(final CreditCardTypeCodeList brandCode) {
        this.brandCode = brandCode;
    }

    /**
     * Sets The expiryYear.
     * 
     * @param expiryYear
     *            The expiryYear to set.
     */
    public void setExpiryYear(final Integer expiryYear) {
        this.expiryYear = expiryYear;
    }

    /**
     * Sets The expiryMonth.
     * 
     * @param expiryMonth
     *            The expiryMonth to set.
     */
    public void setExpiryMonth(final Integer expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    /**
     * Sets The cvv2.
     * 
     * @param cvv2
     *            The cvv2 to set.
     */
    public void setCvv2(final Integer cvv2) {
        this.cvv2 = cvv2;
    }

    /**
     * Sets The pin.
     * 
     * @param pin
     *            The pin to set.
     */
    public void setPin(final String pin) {
        this.pin = pin;
    }

    /**
     * Sets The cardholderName.
     * 
     * @param cardholderName
     *            The cardholderName to set.
     */
    public void setCardholderName(final PartyName cardholderName) {
        this.cardholderName = cardholderName;
    }

}
