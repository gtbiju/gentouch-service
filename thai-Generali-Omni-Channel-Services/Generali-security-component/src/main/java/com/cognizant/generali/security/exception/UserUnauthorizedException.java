package com.cognizant.generali.security.exception;

import com.cognizant.le.security.exception.UserNotFoundException;

public class UserUnauthorizedException extends UserNotFoundException {

	public UserUnauthorizedException(String msg) {
		super(msg);
	}
}
