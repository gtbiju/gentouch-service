package com.cognizant.le.model.dto;

public class PropertyFileSource {

	private String sharedFolderPath;
	private String serverUrl;
	private String agentprofile;
	private String defaultLanguage;

	private String authRequestTemplate;
	private String authUlrl;
	
	private String uploadRequestTemplate;
	private String uploadUlrl;
	
	private String leadRetrieveUrl;
	private String leadRetrieveRqUrl;
	private String leadUpdateUrl;

	public String getUploadRequestTemplate() {
		return uploadRequestTemplate;
	}

	public void setUploadRequestTemplate(String uploadRequestTemplate) {
		this.uploadRequestTemplate = uploadRequestTemplate;
	}

	public String getUploadUlrl() {
		return uploadUlrl;
	}

	public void setUploadUlrl(String uploadUlrl) {
		this.uploadUlrl = uploadUlrl;
	}

	public String getSharedFolderPath() {
		return sharedFolderPath;
	}

	public void setSharedFolderPath(String sharedFolderPath) {
		this.sharedFolderPath = sharedFolderPath;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getAgentprofile() {
		return agentprofile;
	}

	public void setAgentprofile(String agentprofile) {
		this.agentprofile = agentprofile;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public String getAuthRequestTemplate() {
		return authRequestTemplate;
	}

	public void setAuthRequestTemplate(String authRequestTemplate) {
		this.authRequestTemplate = authRequestTemplate;
	}

	public String getAuthUlrl() {
		return authUlrl;
	}

	public void setAuthUlrl(String authUlrl) {
		this.authUlrl = authUlrl;
	}

	/**
	 * @return the leadRetrieveUrl
	 */
	public String getLeadRetrieveUrl() {
		return leadRetrieveUrl;
	}

	/**
	 * @param leadRetrieveUrl the leadRetrieveUrl to set
	 */
	public void setLeadRetrieveUrl(String leadRetrieveUrl) {
		this.leadRetrieveUrl = leadRetrieveUrl;
	}
	
	public String getLeadUpdateUrl() {
		return leadUpdateUrl;
	}

	public void setLeadUpdateUrl(String leadUpdateUrl) {
		this.leadUpdateUrl = leadUpdateUrl;
	}

	/**
	 * @return the leadRetrieveRqUrl
	 */
	public String getLeadRetrieveRqUrl() {
		return leadRetrieveRqUrl;
	}

	/**
	 * @param leadRetrieveRqUrl the leadRetrieveRqUrl to set
	 */
	public void setLeadRetrieveRqUrl(String leadRetrieveRqUrl) {
		this.leadRetrieveRqUrl = leadRetrieveRqUrl;
	}

}
