/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */
package com.cognizant.insurance.searchcriteria;

import java.io.Serializable;

/**
 * The Class class AgreementSearchCriteria.
 */
public class AgreementSearchCriteria implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6187534571587134660L;

    /** The Id. */
    private Long id;
    
    /** The policy number. */
    private Long policyNumber;

    /**
     * Gets the id.
     * 
     * @return Returns the id.
     */
    public final Long getId() {
        return id;
    }

    /**
     * Sets The id.
     * 
     * @param id
     *            The id to set.
     */
    public final void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets The policyNumber.
     *
     * @param policyNumber The policyNumber to set.
     */
    public final void setPolicyNumber(final Long policyNumber) {
        this.policyNumber = policyNumber;
    }

    /**
     * Gets the policyNumber.
     *
     * @return Returns the policyNumber.
     */
    public final Long getPolicyNumber() {
        return policyNumber;
    }
}
