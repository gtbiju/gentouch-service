package com.cognizant.insurance.omnichannel.utils;

public class SavingsData {
	
	private Long savingsAvailCost;
	
	private Long savingsAvailSavings;

	private Long shortFall;
	
	private String categoryAxisSavings;
    
    public String getCategoryAxisSavings() {
        return categoryAxisSavings;
    }

    public void setCategoryAxisSavings(String categoryAxisSavings) {
        this.categoryAxisSavings = categoryAxisSavings;
    }

    public String getCategoryAxisFund() {
        return categoryAxisFund;
    }

    public void setCategoryAxisFund(String categoryAxisFund) {
        this.categoryAxisFund = categoryAxisFund;
    }

    private String categoryAxisFund;
	
	public Long getSavingsAvailCost() {
		return savingsAvailCost;
	}

	public void setSavingsAvailCost(Long savingsAvailCost) {
		this.savingsAvailCost = savingsAvailCost;
	}

	public Long getSavingsAvailSavings() {
		return savingsAvailSavings;
	}

	public void setSavingsAvailSavings(Long savingsAvailSavings) {
		this.savingsAvailSavings = savingsAvailSavings;
	}

    public void setShortFall(Long shortFall) {
        this.shortFall = shortFall;
    }

    public Long getShortFall() {
        return shortFall;
    }

}
