/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.DataCompleteCodeList;

/**
 * This concept is a generalizing concept for ALL concepts in the Information Model. By having a common super-type, the
 * Information Model can now use techniques like Categorization (via the Category sub-package and concept) and can help
 * support the common interfaces used in the Component Model.
 * 
 * Conceptually, InformationModelObject represents a thing or a concept that is meaningful to the industry.
 * 
 * @author 301350
 * 
 * 
 */

@MappedSuperclass
public abstract class InformationModelObject implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1006525425566984465L;

    /** The id. */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    /** The context id. */
    @Enumerated
    private ContextTypeCodeList contextId;

    /** The transaction id. */
    private String transactionId;
    
    /** ****Newly added for EApp - Start *******. */
    
    /** The transaction Tracking Id used to identify individual eAppdata. */
    private String transTrackingId;
    
    /******Newly added for EApp - End ********/
    
    /** ****Newly added for Search - Start *******. */
    
    /** The transactionKeys. */
    @OneToOne(cascade={CascadeType.ALL})    
    private TransactionKeys transactionKeys;
    
    /******Newly added for Search - End ********/

    /**
     * A code indicating the degree to which data for the object in question is considered complete. Having this
     * attribute at this level makes all objects in the model capable of noting their level of completeness.
     * 
     * 
     * 
     */
    @Enumerated
    private DataCompleteCodeList basicDataCompleteCode;

    /**
     * Indicates the name of the type the object belongs to. The "typeName" can be used to reduce inheritance structures
     * for things that have the same attributes (to the organization) but different names. For example, instead of
     * creating sub-types for every vehicle manufacturer, you can use typeName as a simple String to differentiate them.
     * It's important to differentiate between typeName and Category. The former is a very simple means of identifying
     * the class, while the latter is used to group classes of the same (or different) types.
     * 
     * 
     * 
     */
    private String typeName;

    /**
     * The point in time when the business concept was created. Note that this is different from when the concept is
     * considered effective or available. For example, a creation date on a Fund or Account would indicate when the Fund
     * or Account was created. This would be a different point in time from when the Fund was available for purchase or
     * when the Account could be used.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDateTime;
    
    /**
     * The date time stamp on which the data is updated
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDateTime;
    
    

    /**
     * Gets the basicDataCompleteCode.
     * 
     * @return Returns the basicDataCompleteCode.
     */
    public DataCompleteCodeList getBasicDataCompleteCode() {
        return basicDataCompleteCode;
    }

    /**
     * Gets the contextId.
     * 
     * @return Returns the contextId.
     */
    public ContextTypeCodeList getContextId() {
        return contextId;
    }

    /**
     * Gets the transactionId.
     * 
     * @return Returns the transactionId.
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Gets the creationDateTime.
     * 
     * @return Returns the creationDateTime.
     */
    public Date getCreationDateTime() {
        return creationDateTime;
    }

    /**
     * Gets the id.
     * 
     * @return Returns the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the typeName.
     * 
     * @return Returns the typeName.
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets The basicDataCompleteCode.
     * 
     * @param basicDataCompleteCode
     *            The basicDataCompleteCode to set.
     */
    public void setBasicDataCompleteCode(final DataCompleteCodeList basicDataCompleteCode) {
        this.basicDataCompleteCode = basicDataCompleteCode;
    }

    /**
     * Sets The contextId.
     * 
     * @param contextId
     *            The contextId to set.
     */
    public void setContextId(final ContextTypeCodeList contextId) {
        this.contextId = contextId;
    }

    /**
     * Sets The transactionId.
     * 
     * @param transactionId
     *            The transactionId to set.
     */
    public void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Sets The creationDateTime.
     * 
     * @param creationDateTime
     *            The creationDateTime to set.
     */
    public void setCreationDateTime(final Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
    
    /**
     * Gets the modifiedDateTime.
     *
     * @return Returns the modifiedDateTime.
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    /**
     * Sets The modifiedDateTime.
     *
     * @param modifiedDateTime The modifiedDateTime to set.
     */
    public void setModifiedDateTime(final Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    /**
     * Sets The id.
     * 
     * @param id
     *            The id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets The typeName.
     * 
     * @param typeName
     *            The typeName to set.
     */
    public void setTypeName(final String typeName) {
        this.typeName = typeName;
    }



	public String getTransTrackingId() {
		return transTrackingId;
	}

	public void setTransTrackingId(String transTrackingId) {
		this.transTrackingId = transTrackingId;
	}

	public TransactionKeys getTransactionKeys() {
		return transactionKeys;
	}

	public void setTransactionKeys(TransactionKeys transactionKeys) {
		this.transactionKeys = transactionKeys;
	}
}
