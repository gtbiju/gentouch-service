package com.cognizant.insurance.omnichannel.jpa.component;

import java.util.List;

import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.omnichannel.domain.GeneraliSPAJNo;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;

public interface SpajNoComponent {

	public Response<GeneraliSPAJNo> save(Request<GeneraliSPAJNo> request);

	public Boolean isSpajExisting(Request<InsuranceAgreement> spajRequest,
			String spajNo,
			Request<List<AgreementStatusCodeList>> statusRequest,
			String identifier);
}
