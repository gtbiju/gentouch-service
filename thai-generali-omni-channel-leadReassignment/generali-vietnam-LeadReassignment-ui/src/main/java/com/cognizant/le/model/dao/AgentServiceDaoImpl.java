package com.cognizant.le.model.dao;

import java.text.ParseException;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.cognizant.generali.encryption.util.EncryptionUtil;
import com.cognizant.le.model.dto.PropertyFileSource;

@Repository
public class AgentServiceDaoImpl implements AgentServiceDAO {
	static Logger logger = Logger.getLogger(AgentServiceDaoImpl.class);

	public static final String INVALID_AGENTCODE_PWD_ERROR_MSG = "Username/Password entered is invalid. Please re-enter the correct one.";
	
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	PropertyFileSource propertyFileSource;
	
	@Autowired
	EncryptionUtil encryptionUtil;

	@Override
	public String isValidUser(String userid, String password) throws Exception {
		String userType = "";
		
		String AUTH_REQUEST = propertyFileSource.getAuthRequestTemplate();
		String AUTH_URL = propertyFileSource.getAuthUlrl();

		String OMNI_AUTH_REQUEST = AUTH_REQUEST.replace("{AgentName}", userid)
				.replace("{pwd}", password);
		if (AUTH_URL.equals("authenticate.dev")) {
			userType = mockAuthenticateService(OMNI_AUTH_REQUEST);
		} else {
			String encryptedString="";
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Content-Type", "application/json; charset=utf-8");
			headers.add("Accept","application/json;charset=utf-8");
			encryptedString=encryptionUtil.encryptString(OMNI_AUTH_REQUEST);
			HttpEntity<String> httpRequest = new HttpEntity<String>(
					OMNI_AUTH_REQUEST, headers);
			ResponseEntity<String> response = restTemplate.postForEntity(
					AUTH_URL, httpRequest, String.class);
			
			String responseBodyJson = response.getBody();
			//String responseBodyString = new Gson().fromJson(responseBodyJson,String.class);
			//String authResponse = encryptionUtil.decryptString(responseBodyString);
			String status = "";
			
			JSONObject object = new JSONObject(responseBodyJson);
			status = object.getString("status").toLowerCase();
			
			if ("success".equals(status)) {
				  if (object.has("userType")) {
					  userType = object.getString("userType").toLowerCase();
				  }
				
			}  else if("failure".equals(status)){
				userType = "User Terminated or Suspended";
				
			} else if(INVALID_AGENTCODE_PWD_ERROR_MSG.equalsIgnoreCase(status)) { 
				userType = "Invalid Password";
			}
		}		
		return userType;
	}

	public String mockAuthenticateService(String requestJson)
			throws ParseException {
		String authResponse = "";
		String userId = "";
		String password = "";

		JSONObject object = new JSONObject(requestJson);
		userId = object.getString("agentCode").toLowerCase();
		password = object.getString("password").toLowerCase();
		if ("admin".equals(userId) && "agency".equals(password)) {
			authResponse = "agency_upload_admin";
		}
		else if("admin".equals(userId) && "banca".equals(password)){
			authResponse = "banca_upload_admin";
		}

		return authResponse;
	}
}
