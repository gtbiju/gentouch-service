package com.cognizant.insurance.omnichannel.component.repository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.repository.IllustrationRepository;
import com.cognizant.insurance.component.repository.helper.LifeEngageComponentRepositoryHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.AgreementExtension;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.finance.agreementloan.AgreementLoan;
import com.cognizant.insurance.domain.finance.agreementloan.AgreementWithdrawal;
import com.cognizant.insurance.domain.goalandneed.Agreement_GoalAndNeed;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliAgreementComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliPartyComponent;
import com.cognizant.insurance.omnichannel.component.UserSelectionComponent;
import com.cognizant.insurance.omnichannel.component.repository.helper.GeneraliComponentRepositoryHelper;
import com.cognizant.insurance.omnichannel.utils.GeneraliDateUtil;
import com.cognizant.insurance.product.component.ProductComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public class GeneraliIllustrationRepository extends IllustrationRepository {

    private static final String RM_REASSIGN = "rm_reassign";

	private static final String SUCCESS = "Success";

	private static final String ILLUSTRATION = "Illustration";

	/** The agreement component. */
    @Autowired
    private GeneraliAgreementComponent agreementComponent;

    /** LE Entity Manager*/
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager leEntityManager;
    
    /** ODS Entity Manager*/
    @PersistenceContext(unitName = "ODS_unit")
    private EntityManager odsEntityManager;
    
    @Autowired
    private GeneraliPartyComponent partyComponent;
    
    @Autowired
    private UserSelectionComponent userSelectionComponent;

    @Autowired
    private GeneraliComponentRepositoryHelper generaliComponentRepositoryHelper;
    
    /** The id generator. */
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;
    
    /** The agreement component. */
    @Autowired
    private ProductComponent productComponent;
    
    /** The source of truth. */
    @Value("${le.platform.service.sourceOfTruth}")
    private String sourceOfTruth;
    
    /** The conflict resolution. */
    @Value("${le.platform.service.conflictResolution}")
    private String conflictResolution;

    /** File Upload Path */
    @Value("${le.platform.service.fileUploadPath}")
    private String fileUploadPath;
    
    /** The slash. */
    @Value("${le.platform.service.slash}")
    private String slash; 
    
    /** The illustration limit. */
    @Value("${le.platform.service.illustration.fetchLimit}")
    private Integer illustrationLimit;


    public String saveIllustration(final Transactions transactions, final RequestInfo requestInfo,InsuranceAgreement savedIllustration)
            throws ParseException {
        LOGGER.trace("Inside IllustrationRepository saveIllustration : requestInfo" + requestInfo);
        final Request<Agreement> request =
                new JPARequestImpl<Agreement>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        InsuranceAgreement insuranceAgreement = (InsuranceAgreement) transactions.getProposal();
        insuranceAgreement.setContextId(ContextTypeCodeList.ILLUSTRATION);
        onBeforeSave(transactions, requestInfo);
        if (Constants.UPDATE.equals(transactions.getMode())) {
            String status = onAgreementUpdate(transactions, requestInfo, savedIllustration);
            if (!Constants.SUCCESS.equals(status)) {
                return status;
            }
        } else {
            insuranceAgreement.setCreationDateTime(transactions.getCreationDateTime());
        }
        //Saving agreement Requirements to insurance agreement. Changes from LE
        insuranceAgreement.setRequirements(transactions.getRequirements());
        String typeVariableObj = transactions.getAllocationType()!=null?transactions.getAllocationType():StringUtils.EMPTY;
        if(typeVariableObj.equalsIgnoreCase(RM_REASSIGN)) {
        	String newAgentId = transactions.getKey39();
        	String status = onAgreementUpdate(transactions, requestInfo, savedIllustration);
        	if (!Constants.SUCCESS.equals(status)) {
                return status;
            }
        	transactions.setProductId(savedIllustration.getTransactionKeys().getKey5());
        	insuranceAgreement.setAgentId(newAgentId);
        	insuranceAgreement.setIdentifier(transactions.getIllustrationId());
        	insuranceAgreement.setModifiedDateTime(new Date());
        	insuranceAgreement.setContextId(ContextTypeCodeList.ILLUSTRATION);
        	insuranceAgreement.setTransTrackingId(savedIllustration.getTransTrackingId());
        	insuranceAgreement.setStatusCode(AgreementStatusCodeList.Confirmed);
        	TransactionKeys newTransactionKeys = savedIllustration.getTransactionKeys();
        	newTransactionKeys.setKey11(newAgentId);
        	newTransactionKeys.setKey40("rm_reassign");
        	newTransactionKeys.setKey1(transactions.getKey22());
        	insuranceAgreement.setTransactionKeys(newTransactionKeys);
        	saveAgreementProductSpecification(transactions, requestInfo, insuranceAgreement);
        	
        	request.setType(insuranceAgreement);
        	
            agreementComponent.saveAgreement(request);
            
            final Request<Agreement> agreementRequest =
                    new JPARequestImpl<Agreement>(leEntityManager, ContextTypeCodeList.ILLUSTRATION, savedIllustration.getTransactionId());
            agreementRequest.setType(savedIllustration);
            getAgreementDetails(transactions, agreementRequest);
        }
        else {
        	request.setType(insuranceAgreement);
            
            agreementComponent.saveAgreement(request);
        }

        createAgreementLoans(transactions.getAgreementLoans(), insuranceAgreement, requestInfo);
        createAgreementWithdrawals(transactions.getAgreementWithdrawals(), insuranceAgreement, requestInfo);

        LifeEngageComponentRepositoryHelper.saveInsured(transactions.getInsured(), requestInfo, insuranceAgreement,
                getEntityManager(), ContextTypeCodeList.ILLUSTRATION, partyComponent);
        generaliComponentRepositoryHelper.saveAdditionalInsuredes(transactions.getAdditionalInsuredes(), requestInfo,
                insuranceAgreement, getEntityManager(), ContextTypeCodeList.ILLUSTRATION, partyComponent,userSelectionComponent);
        saveBeneficiary(transactions.getBeneficiaries(), requestInfo, insuranceAgreement);
        
		generaliComponentRepositoryHelper.saveAgent(
				transactions.getAgreementProducer(), requestInfo,
				insuranceAgreement, partyComponent, getEntityManager(),
				ContextTypeCodeList.ILLUSTRATION);

        LifeEngageComponentRepositoryHelper.savePayer(transactions.getPayer(), requestInfo, insuranceAgreement,
                getEntityManager(), ContextTypeCodeList.ILLUSTRATION, partyComponent);

        return insuranceAgreement.getStatusCode() != null ? insuranceAgreement.getStatusCode().toString() : StringUtils.EMPTY;
    }
    
    /**
     * Save Product specification related to the agreement.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    protected void saveAgreementProductSpecification(final Transactions transactions, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement) {
        if (null != transactions.getProductId()) {
            final Request<ProductSearchCriteria> requestProductSpec =
                    new JPARequestImpl<ProductSearchCriteria>(leEntityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
            ProductSearchCriteria searchCriteria = new ProductSearchCriteria();
            searchCriteria.setId(Long.parseLong(transactions.getProductId()));
            requestProductSpec.setType(searchCriteria);
            final Response<ProductSpecification> productSpec = productComponent.getProductById(requestProductSpec, "1");
            insuranceAgreement.setIsBasedOn(productSpec.getType());
        }
    }

    /**
     * Gets the agreement details such as Insured details, payer details, agreement loan and agreement withdrawals.
     * 
     * @param transaction
     *            the transaction
     * @param request
     *            the request
     * @return the agreement details
     */
    @Override
    protected void getAgreementDetails(final Transactions transaction, final Request<Agreement> request) {

        // final Response<Insured> insuredResponse = partyComponent.retrieveInsured(request);
        // transaction.setInsured(insuredResponse.getType());

        final Response<PremiumPayer> payerResponse = partyComponent.retrievePayer(request);
        transaction.setPayer(payerResponse.getType());
        
        final Response<AgreementProducer> agentResponse = partyComponent.retrieveAgent(request);
        transaction.setAgreementProducer(agentResponse.getType());

        final Response<Set<Beneficiary>> beneficiariesResponse = partyComponent.retrieveBeneficiaries(request);
        transaction.setBeneficiaries(beneficiariesResponse.getType());

        final Response<Set<Insured>> additionalInsuredResponse = partyComponent.retrieveAdditionalInsureds(request);
        Set<Insured> insuredSet = additionalInsuredResponse.getType();
        Set<Insured> additionalInsuredSet = new HashSet<Insured>();
        if (CollectionUtils.isNotEmpty(insuredSet)) {
            for (Insured insured : insuredSet) {
                if ((insured.getTypeCode() != null) && ("Additional").equalsIgnoreCase(insured.getTypeCode().name())) {
                    additionalInsuredSet.add(insured);
                } else if ((insured.getTypeCode() != null)
                        && ("Primary").equalsIgnoreCase(insured.getTypeCode().name())) {
                    {
                        transaction.setInsured(insured);
                    }
                }
            }
            transaction.setAdditionalInsuredes(additionalInsuredSet);
            final Response<Set<AgreementLoan>> agreementLoanResponse = agreementComponent.getAgreementLoans(request);
            transaction.setAgreementLoans(agreementLoanResponse.getType());

            final Response<Set<AgreementWithdrawal>> agreementWithdrawalResponse =
                    agreementComponent.getAgreementWithdrawals(request);
            transaction.setAgreementWithdrawals(agreementWithdrawalResponse.getType());
        }
    }

    /**
     * On before save.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     */
    @Override
    protected void onBeforeSave(Transactions transactions, RequestInfo requestInfo) {
        InsuranceAgreement insuranceAgreement = transactions.getProposal();
        // Since ModifiedTime is using for Chunk retrieve and SourceOfTruth functionality
        insuranceAgreement.setModifiedDateTime(transactions.getModifiedTimeStamp());

        insuranceAgreement.setTransactionId(requestInfo.getTransactionId());
        if (StringUtils.isNotEmpty(transactions.getIllustrationId())) {
        insuranceAgreement.setIdentifier(transactions.getIllustrationId());
        }
        if (null != transactions.getStatus()) {

            if (transactions.getStatus().equals(AgreementStatusCodeList.Draft.toString())) {

                insuranceAgreement.setStatusCode(AgreementStatusCodeList.Draft);
            } else if (transactions.getStatus().equals(AgreementStatusCodeList.Completed.toString())) {
                insuranceAgreement.setStatusCode(AgreementStatusCodeList.Completed);
            } else if (transactions.getStatus().equals(AgreementStatusCodeList.Confirmed.toString())) {
                insuranceAgreement.setStatusCode(AgreementStatusCodeList.Confirmed);
            } else if (transactions.getStatus().equals(AgreementStatusCodeList.Cancelled.toString())) {
                insuranceAgreement.setStatusCode(AgreementStatusCodeList.Cancelled);
            }
        }

        if (transactions.getAgentId() != null) {
            insuranceAgreement.setAgentId(transactions.getAgentId());
        }

    }
   
    /**
     * On agreement update.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the string
     * @throws ParseException
     *             the parse exception
     */
    @Override
    protected String onAgreementUpdate(Transactions transactions, RequestInfo requestInfo,InsuranceAgreement savedIllustration) throws ParseException {
        Set<Agreement> replacingAgeements = null;
        final List<AgreementStatusCodeList> statusList =getAgreementStatusCodesFor(Constants.UPDATE_OP);
        InsuranceAgreement agreement = null;
        if (null != savedIllustration) // We already have agreement with transTracking Id
            agreement = savedIllustration;
        else {
            agreement =
                    LifeEngageComponentRepositoryHelper.createReplacingAgreement(transactions, requestInfo,
                            getEntityManager(), agreementComponent, ContextTypeCodeList.ILLUSTRATION, statusList);
        }
        // Checking whether the status code is cancelled, if so return directly.
        if (agreement.getStatusCode() != null && agreement.getStatusCode().equals(AgreementStatusCodeList.Cancelled)) {
            return AgreementStatusCodeList.Cancelled.toString();
        }
        // Checking whether the server data is latest when source of truth is server, if so reject the client data and
        // send rejected status
             
        if (Constants.TRUE.equals(conflictResolution) && Constants.SERVER.equals(sourceOfTruth)) {
        	
        	InsuranceAgreement agreementToSave= transactions.getProposal();
        	Boolean agreementTosaveStatusLatestthanExistingStaus=false;
        	
        	AgreementStatusCodeList statusToSave = agreementToSave.getStatusCode();        	
        	AgreementStatusCodeList statusExisting = agreement.getStatusCode();
        	// Modified the logic
        	//If the new agreement has latest or same  Status (Confirmed /completed) than the  status of the existing illustration, will proceed with the save.
        	// Will not route to Reject
        	
        	if(statusToSave!=null && statusToSave.equals(AgreementStatusCodeList.Confirmed)){
        		if(statusExisting.equals(AgreementStatusCodeList.Draft)||statusExisting.equals(AgreementStatusCodeList.Completed)||statusExisting.equals(AgreementStatusCodeList.Confirmed)){
        			agreementTosaveStatusLatestthanExistingStaus=true;
        		}
        	}
        	
        	else if(statusToSave!=null && statusToSave.equals(AgreementStatusCodeList.Completed)){
        		if(statusExisting.equals(AgreementStatusCodeList.Draft)||statusExisting.equals(AgreementStatusCodeList.Completed)){
        			agreementTosaveStatusLatestthanExistingStaus=true;
        		}
        	}
        	
        	
			LOGGER.trace("Inside conflict resolution true...");
			Date modifiedTimeStamp = agreement.getModifiedDateTime();
			
			if (!(transactions.getLastSyncDate().toString()
					.equals(GeneraliDateUtil.getDefaultDate())) &&!agreementTosaveStatusLatestthanExistingStaus
					&& ((null != modifiedTimeStamp) && (modifiedTimeStamp
							.after(transactions.getLastSyncDate())))) {

				return Constants.REJECTED;

			}
        }
       
        
        replacingAgeements =
            LifeEngageComponentRepositoryHelper.updateAgreementStatus(agreement, agreementComponent, requestInfo,
                    getEntityManager(), ContextTypeCodeList.ILLUSTRATION);

        transactions.getProposal().setCreationDateTime(agreement.getCreationDateTime());

        Set<AgreementDocument> agreementDocumentsSet = new HashSet<AgreementDocument>();
        if (replacingAgeements != null) {
            agreementDocumentsSet = generaliComponentRepositoryHelper.createReplaceDocument(replacingAgeements);
            transactions.getProposal().setRelatedDocument(agreementDocumentsSet);
            transactions.getProposal().setModifiedDateTime(LifeEngageComponentRepositoryHelper.getCurrentdate());
        }
        if (replacingAgeements != null) {
            LifeEngageComponentRepositoryHelper.createReplacingRequirementSet(transactions, replacingAgeements,
                    fileUploadPath + slash);
            // While copying all the Previous requirements will be set in the current agreement
        } 
        transactions.getProposal().setReplacesAgreements(replacingAgeements);

        return Constants.SUCCESS;
    }
    @Override
    protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(String operation) {
        List<AgreementStatusCodeList> statusCodeLists = new ArrayList<AgreementStatusCodeList>();
        if (Constants.FIRST_TIME_SYNC.equals(operation) || Constants.RETRIEVE.equals(operation)) {
            statusCodeLists.add(AgreementStatusCodeList.Draft);
            statusCodeLists.add(AgreementStatusCodeList.Completed);
            statusCodeLists.add(AgreementStatusCodeList.Confirmed);
        } else if (Constants.RETRIEVE_ALL.equals(operation) || Constants.UPDATE_OP.equals(operation)) {
            // statusCodeLists.add(AgreementStatusCodeList.Final);
            statusCodeLists.add(AgreementStatusCodeList.Draft);
            statusCodeLists.add(AgreementStatusCodeList.Completed);
            statusCodeLists.add(AgreementStatusCodeList.Confirmed);
            statusCodeLists.add(AgreementStatusCodeList.Cancelled);
        } else if (Constants.RETRIEVE_BY_FILTER.equals(operation) || Constants.RETRIEVE_BY_COUNT.equals(operation)) {
            /*statusCodeLists.add(AgreementStatusCodeList.Initial);
            statusCodeLists.add(AgreementStatusCodeList.New);
            statusCodeLists.add(AgreementStatusCodeList.Submitted);*/
            statusCodeLists.add(AgreementStatusCodeList.Draft);
            statusCodeLists.add(AgreementStatusCodeList.Completed);
            statusCodeLists.add(AgreementStatusCodeList.Confirmed);
            //statusCodeLists.add(AgreementStatusCodeList.Proposed);
        } else if (GeneraliConstants.RETRIEVE_BY_COUNT_ILLUSTRATION.equals(operation)) {
            statusCodeLists.add(AgreementStatusCodeList.Completed);
            statusCodeLists.add(AgreementStatusCodeList.Confirmed);
        } else if (GeneraliConstants.RETRIEVE_BY_COUNT_ILLUSTRATION_DRAFT.equals(operation)){
            statusCodeLists.add(AgreementStatusCodeList.Draft);
        }

        return statusCodeLists;
    }
    
    /**
     * Retrieve by count illustration.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the search count result
     */
    public SearchCountResult retrieveIllustrationCountForLMS(final RequestInfo requestInfo,
            final SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_COUNT);
        statusRequest.setType(statusList);

        final Response<SearchCountResult> searchCountResponse =
                agreementComponent.getAgreementCountForLMS(searchCriteriatRequest, statusRequest);

        SearchCountResult searchCountResult = null;
        if (searchCountResponse.getType() != null) {
            searchCountResult = (SearchCountResult) searchCountResponse.getType();

        }
        return searchCountResult;

    }

	/**
	 * Method to get illustration identifier .
	 * 
	 * @param transTrackingId
	 * @return illustrationId
	 */
	public String getIllustrationId(String transTrackingId, String agentId) {
		String illustrationId = null;
		final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE);
		final Request<Agreement> request = new JPARequestImpl<Agreement>(
				getEntityManager(), ContextTypeCodeList.ILLUSTRATION, null);
		final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
		insuranceAgreement.setTransTrackingId(transTrackingId);
		insuranceAgreement.setAgentId(agentId);
		request.setType(insuranceAgreement);

		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				getEntityManager(), ContextTypeCodeList.ILLUSTRATION, null);
		statusRequest.setType(statusList);

		final Response<Agreement> agreementResponse = agreementComponent
				.retrieveAgreementForTransTrackingId(request, statusRequest);
		final InsuranceAgreement agreement = (InsuranceAgreement) agreementResponse
				.getType();
		illustrationId = agreement.getIdentifier().toString();
		return illustrationId;
	}
	
	 /* Get illustration agreement.
	    * 
	    * @param transactions
	    *            the transactions
	    * @param transactionId
	    *            the transaction id
	    * @return the illustration agreement
	    */

	   public InsuranceAgreement getIllustrationAgreementByTransTrackingId(final Transactions transactions,
	           String transactionId) {
	       final List<AgreementStatusCodeList> statusList =getAgreementStatusCodesFor(Constants.RETRIEVE);
	       
	       final Request<Agreement> request =
	               new JPARequestImpl<Agreement>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION, transactionId);
	       final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
	       insuranceAgreement.setTransTrackingId(transactions.getTransTrackingId());
	       insuranceAgreement.setAgentId(transactions.getAgentId());
	       request.setType(insuranceAgreement);

	       final Request<List<AgreementStatusCodeList>> statusRequest =
	               new JPARequestImpl<List<AgreementStatusCodeList>>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION,
	                       transactionId);
	       statusRequest.setType(statusList);

	       final Response<Agreement> agreementResponse =
	               agreementComponent.retrieveAgreementForTransTrackingId(request, statusRequest);
	       final InsuranceAgreement agreement = (InsuranceAgreement) agreementResponse.getType();
	       return agreement;
	   }
	/**
	 * Method to get the count of illustration on daily basis   
	 * @param requestInfo
	 * @param searchCriteria
	 * @return
	 */
    public List<Object> getIllustrationCountForSalesActivity(final RequestInfo requestInfo,
            final SearchCriteria searchCriteria) {
        // TODO Auto-generated method stub

        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION, null);
        searchCriteriatRequest.setType(searchCriteria);

        final Response<List<Object>> searchCountResponse =
                agreementComponent.getCountForSalesActivity(searchCriteriatRequest);

        final List<Object> illustrationCountList = (List<Object>) searchCountResponse.getType();

        return illustrationCountList;
    }

    public Transactions getIllustration(final Transactions transactions, String transactionId) {
        final Transactions transaction = new Transactions();
        transaction.setIllustrationId(transactions.getIllustrationId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE);

        final Request<Agreement> request =
                new JPARequestImpl<Agreement>(leEntityManager, ContextTypeCodeList.ILLUSTRATION, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(transactions.getIllustrationId());
        
        request.setType(insuranceAgreement);

        final String illustrationID = transactions.getIllustrationId();

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(leEntityManager, ContextTypeCodeList.ILLUSTRATION,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementResponse =
                agreementComponent.retrieveAgreementForIdentifier(request, statusRequest);
        final InsuranceAgreement agreement = (InsuranceAgreement) agreementResponse.getType();
        String agentID=agreement.getAgentId();
        if (agreement != null) {
            transaction.setProposal(agreement);
            request.setType(agreement);

            getAgreementDetails(transaction, request);
            transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
        }
        
        final Request<GeneraliAgent> agentRequest=new JPARequestImpl<GeneraliAgent>(odsEntityManager,ContextTypeCodeList.AGENT,transactionId);
        final Response<GeneraliAgent> generaliAgentResponse = agreementComponent.retrieveGeneraliAgent(agentRequest, agentID);
        transaction.setGeneraligent(generaliAgentResponse.getType());
        
        final Request<Agreement_GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<Agreement_GoalAndNeed>(leEntityManager, ContextTypeCodeList.FNA, transactionId);

        final String getFNAId = agreementComponent.retrieveFNAId(goalAndNeedRequest, illustrationID);
        if (StringUtils.isNotEmpty(getFNAId)) {
            transaction.setFnaId(getFNAId);
        }
        
        return transaction;
    }

	public EntityManager getOdsEentityManager() {
		return odsEntityManager;
	}

	public void setOdsEentityManager(EntityManager odsEentityManager) {
		this.odsEntityManager = odsEentityManager;
	}

    public Map<String, Integer> retrieveMonthwiseCount(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION, null);
        searchCriteriatRequest.setType(searchCriteria);
        final Map<String, Integer> searchCountResponse =
                agreementComponent.retrieveMonthwiseCount(searchCriteriatRequest);

        return searchCountResponse;
    }

    public Map<String, Integer> retrieveWeeklyCount(RequestInfo requestInfo, SearchCriteria searchCriteria) {

        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION, null);
        searchCriteriatRequest.setType(searchCriteria);
        final Map<String, Integer> searchCountResponse =
                agreementComponent.retrieveWeeklyCount(searchCriteriatRequest);

        return searchCountResponse;
    }
    
    public List<Transactions> retrieveMonthlyPresentationData(final RequestInfo requestInfo,
            final SearchCriteria searchCriteria) {
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        List<AgreementStatusCodeList> statusCodeLists = new ArrayList<AgreementStatusCodeList>();
        statusCodeLists.add(AgreementStatusCodeList.Draft);
        statusCodeLists.add(AgreementStatusCodeList.Completed);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusCodeLists);

        final Response<List<Agreement>> searchCriteriatResponse =
                agreementComponent.getMonthlyData(searchCriteriatRequest, statusRequest);

        final List<Agreement> agreements = searchCriteriatResponse.getType();

        if (CollectionUtils.isNotEmpty(agreements)) {
            for (Agreement agreement : agreements) {
                transaction = new Transactions();
                transaction.setProposal((InsuranceAgreement) agreement);
                transactionsList.add(transaction);
            }
        }
        return transactionsList;

    }
    
    public SearchCountResult retrieveByCountIllustration(final RequestInfo requestInfo,
            final SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(leEntityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);
        
        SearchCountResult searchCountResult = null;
        final Response<SearchCountResult> searchCountResponse;

        if(searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))){
            
            final Request<List<AgreementStatusCodeList>> statusRequest =
            new JPARequestImpl<List<AgreementStatusCodeList>>(leEntityManager, ContextTypeCodeList.ILLUSTRATION,
                    requestInfo.getTransactionId());
            final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_COUNT);
            statusRequest.setType(statusList);
            
            searchCountResponse = agreementComponent.getAgreementCountForIllustration(searchCriteriatRequest, statusRequest, null);
        } else {
        
            final Request<List<AgreementStatusCodeList>> statusRequest =
                    new JPARequestImpl<List<AgreementStatusCodeList>>(leEntityManager, ContextTypeCodeList.ILLUSTRATION,
                            requestInfo.getTransactionId());
            final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(GeneraliConstants.RETRIEVE_BY_COUNT_ILLUSTRATION);
            statusRequest.setType(statusList);
            
            final Request<List<AgreementStatusCodeList>> statusRequestForDraft =
                    new JPARequestImpl<List<AgreementStatusCodeList>>(leEntityManager, ContextTypeCodeList.ILLUSTRATION,
                            requestInfo.getTransactionId());
            final List<AgreementStatusCodeList> statusListForDraft = getAgreementStatusCodesFor(GeneraliConstants.RETRIEVE_BY_COUNT_ILLUSTRATION_DRAFT);
            statusRequestForDraft.setType(statusListForDraft);
            
            searchCountResponse = agreementComponent.getAgreementCountForIllustration(searchCriteriatRequest, statusRequest, statusRequestForDraft);
        }
        
        if (searchCountResponse.getType() != null) {
            searchCountResult = (SearchCountResult) searchCountResponse.getType();
        }  
        return searchCountResult;
    }
    
    @Override
    public List<Transactions> retrieveAll(Transactions transactions, RequestInfo requestInfo,
            boolean isFullDetailsRequired) {
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;

         List<AgreementStatusCodeList> statusList = null;

        if (requestInfo.getFirstTimeSync()) {
            statusList=getAgreementStatusCodesFor(Constants.FIRST_TIME_SYNC);
        } else {
            statusList=getAgreementStatusCodesFor(Constants.RETRIEVE_ALL);
           
        }

        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(leEntityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setAgentId(transactions.getAgentId());
        insuranceAgreement.setLastSyncDate(requestInfo.getLastSyncDate());
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(leEntityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        Request<Integer> limitRequest = null;

        if (isFullDetailsRequired) {
            if (illustrationLimit != null) {
                limitRequest =
                        new JPARequestImpl<Integer>(leEntityManager, ContextTypeCodeList.ILLUSTRATION,
                                requestInfo.getTransactionId());
                limitRequest.setType(illustrationLimit);
            }
        }
        final Response<List<Agreement>> agreementsResponse =
                agreementComponent.getAgreementsList(agreementRequest, statusRequest, limitRequest);

        final List<Agreement> agreements = agreementsResponse.getType();
        if (CollectionUtils.isNotEmpty(agreements)) {
            for (Agreement agreement : agreements) {
                transaction = new Transactions();
                transaction.setProposal((InsuranceAgreement) agreement);

                agreementRequest.setType(agreement);

                final String illustrationID = agreement.getIdentifier().toString();

                if (isFullDetailsRequired) {
                    getAgreementDetails(transaction, agreementRequest);
                } else {
                    final Response<Insured> insuredResponse = partyComponent.retrieveInsured(agreementRequest);
                    transaction.setInsured(insuredResponse.getType());
                    final Response<PremiumPayer> payerResponse = partyComponent.retrievePayer(agreementRequest);
                    transaction.setPayer(payerResponse.getType());
                }

                final Request<Agreement_GoalAndNeed> goalAndNeedRequest =
                        new JPARequestImpl<Agreement_GoalAndNeed>(leEntityManager, ContextTypeCodeList.FNA,
                                requestInfo.getTransactionId());

                final String getFNAId = agreementComponent.retrieveFNAId(goalAndNeedRequest, illustrationID);
                if (StringUtils.isNotEmpty(getFNAId)) {
                    transaction.setFnaId(getFNAId);
                }
                if (agreement.getModifiedDateTime() != null && !(agreement.getModifiedDateTime().toString().equals(""))) {

                    transaction.setLastSyncDate(agreement.getModifiedDateTime());
                } else if (agreement.getCreationDateTime() != null
                        && !("".equals(agreement.getCreationDateTime().toString()))) {

                    transaction.setLastSyncDate(agreement.getCreationDateTime());
                }

                transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }

        return transactionsList;
    }

    public Map<String, String>
            getEAppIllustrationCountForReport(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.ILLUSTRATION, null);
        searchCriteriatRequest.setType(searchCriteria);
        final Map<String, String> searchCountResponse =
                agreementComponent.getEAppIllustrationCountForReport(searchCriteriatRequest);
        return searchCountResponse;
    }
}
