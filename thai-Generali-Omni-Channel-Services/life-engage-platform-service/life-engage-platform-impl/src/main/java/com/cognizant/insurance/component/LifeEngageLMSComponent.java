/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.component;

import java.text.ParseException;

import org.json.JSONObject;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * The Interface interface LifeEngageEAppComponent.
 * 
 * @author 300797
 */
public interface LifeEngageLMSComponent {

    /**
     * Save LMS.
     * 
     * @param json
     *            the json
     * @param requestInfo
     *            the request info
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     */
    String saveLMS(String json, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit);

    /**
     * Retrieve lms.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonObj
     *            the json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveLMS(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
            ParseException;

    /**
     * Retrieve all lms.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonObj
     *            the json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveAllLMS(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
            ParseException;
}
