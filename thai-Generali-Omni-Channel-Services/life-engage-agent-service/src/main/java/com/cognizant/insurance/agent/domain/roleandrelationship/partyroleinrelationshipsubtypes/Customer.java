/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.agent.domain.documentandcommunication.communicationcontentspecificationsubtypes.Questionnaire;
import com.cognizant.insurance.agent.domain.extension.Extension;
import com.cognizant.insurance.agent.domain.product.PurchasedProduct;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolerelationshipsubtypes.CustomerRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.ActivityCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CustomerImportanceLevelCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CustomerNeedCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CustomerSourceCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;

/**
 * A party (e.g. person or organization) to whom services, goods or benefits are currently, have been previously or will
 * be supplied.
 * 
 * A party to whom another party has sold or wants to sell products and/or services.
 * 
 * e.g: Jane Doe as a prospective customer of Acme Insurance Company. John Smith as a current customer of Acme Services
 * Company.
 * 
 * 
 * @author 300797
 * 
 */

@Entity
@DiscriminatorValue("Customer")
public class Customer extends PartyRoleInRelationship {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6470084883396613685L;

    /**
     * The level of importance the customer has to the modeled organization.
     * 
     * e.g: 1 (Highest priority/ VIP)
     * 
     * e.g: 2 (High priority)
     * 
     * e.g: 3 (Normal priority)
     * 
     * e.g: 4 (Low priority)
     * 
     * 
     * 
     */
    @Enumerated
    private CustomerImportanceLevelCodeList importanceLevelCode;

    /**
     * The current status of the customer within the life-cycle model.
     * 
     * e.g: Active
     * 
     * e.g: Ex-Customer
     * 
     * e.g: Prospect
     * 
     * e.g: Suspect
     * 
     * 
     * 
     */
    @Enumerated
    private CustomerStatusCodeList statusCode;

    /*
     * Fields added for LMS : Start
     */

    /** The source. */
    @Enumerated
    private CustomerSourceCodeList source;

    /** The sub source. */
    @Enumerated
    private CustomerSourceCodeList subSource;

    /** The sub status code. */
    @Enumerated
    private CustomerStatusCodeList subStatusCode;

    /** The reason. */
    private String reason;

    /** The sub reason. */
    private String subReason;

    /** The includes extension. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private List<Extension> includesExtension;

    /** The related customers. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private Set<Customer> relatedCustomers;

    /** The questionnaire. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private Set<Questionnaire> questionnaire;

    /** The suggestions. */
    private String suggestions;

    /** The purchased products. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private Set<PurchasedProduct> purchasedProducts;

    /** The proposer. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY)
    private AgreementHolder proposer;
     
    /** The is proposer same as customer. */
    private Boolean isProposerSameAsCustomer;

    /** The identifier. */
    private String identifier;

    /** The agent id. */
    private String agentId;

    /** The last sync date. */
    private Date lastSyncDate;
    
    /** The parent lead id. */
    private String parentLeadId;
    
    private Boolean hasDependent;
    
    private String claimCount;
    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    private CustomerRelationship customerRelationship;
    
  

    /*
     * Fields added for LMS : End
     */
    
    //Added for Generali Vietnam.
    /** The branch id. */
    private String branchId;
    
    /**Source from which lead is allocated */
    private String allocationType;
    

    
    @Enumerated
    private CustomerNeedCodeList need;
    
    @Enumerated
    private ActivityCodeList action;
    
    @Enumerated
    private ActivityCodeList nextAction;
    
    private String branchName;
    

    @Temporal(TemporalType.DATE)
    private Date subDate;
    
    private String stateValue;
    
    private String cityValue;
    
    private String wardValue;
    
    private String countryValue;
    
    /**
     * Gets the importanceLevelCode.
     * 
     * @return Returns the importanceLevelCode.
     */
    public CustomerImportanceLevelCodeList getImportanceLevelCode() {
        return importanceLevelCode;
    }

    /**
     * Gets the statusCode.
     * 
     * @return Returns the statusCode.
     */
    public CustomerStatusCodeList getStatusCode() {
        return statusCode;
    }

    /**
     * Sets The importanceLevelCode.
     * 
     * @param importanceLevelCode
     *            The importanceLevelCode to set.
     */
    public void setImportanceLevelCode(final CustomerImportanceLevelCodeList importanceLevelCode) {
        this.importanceLevelCode = importanceLevelCode;
    }

    /**
     * Sets The statusCode.
     * 
     * @param statusCode
     *            The statusCode to set.
     */
    public void setStatusCode(final CustomerStatusCodeList statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Gets the source.
     * 
     * @return the source
     */
    public CustomerSourceCodeList getSource() {
        return source;
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the source to set
     */
    public void setSource(CustomerSourceCodeList source) {
        this.source = source;
    }

    /**
     * Gets the sub source.
     * 
     * @return the subSource
     */
    public CustomerSourceCodeList getSubSource() {
        return subSource;
    }

    /**
     * Sets the sub source.
     * 
     * @param subSource
     *            the subSource to set
     */
    public void setSubSource(CustomerSourceCodeList subSource) {
        this.subSource = subSource;
    }

    /**
     * Gets the sub status code.
     * 
     * @return the subStatusCode
     */
    public CustomerStatusCodeList getSubStatusCode() {
        return subStatusCode;
    }

    /**
     * Sets the sub status code.
     * 
     * @param subStatusCode
     *            the sub status code to set.
     */
    public void setSubStatusCode(CustomerStatusCodeList subStatusCode) {
        this.subStatusCode = subStatusCode;
    }

    /**
     * Gets the reason.
     * 
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the reason.
     * 
     * @param reason
     *            the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * Gets the sub reason.
     * 
     * @return the subReason
     */
    public String getSubReason() {
        return subReason;
    }

    /**
     * Sets the sub reason.
     * 
     * @param subReason
     *            the subReason to set
     */
    public void setSubReason(String subReason) {
        this.subReason = subReason;
    }

    /**
     * Gets the includes extension.
     * 
     * @return the includesExtension
     */
    public List<Extension> getIncludesExtension() {
        return includesExtension;
    }

    /**
     * Sets the includes extension.
     * 
     * @param includesExtension
     *            the includesExtension to set
     */
    public void setIncludesExtension(List<Extension> includesExtension) {
        this.includesExtension = includesExtension;
    }

    /**
     * Gets the related customers.
     * 
     * @return the relatedCustomers
     */
    public Set<Customer> getRelatedCustomers() {
        return relatedCustomers;
    }

    /**
     * Sets the related customers.
     * 
     * @param relatedCustomers
     *            the relatedCustomers to set
     */
    public void setRelatedCustomers(Set<Customer> relatedCustomers) {
        this.relatedCustomers = relatedCustomers;
    }

    /**
     * Gets the questionnaire.
     * 
     * @return the questionnaire
     */
    public Set<Questionnaire> getQuestionnaire() {
        return questionnaire;
    }

    /**
     * Sets the questionnaire.
     * 
     * @param questionnaire
     *            the questionnaire to set
     */
    public void setQuestionnaire(Set<Questionnaire> questionnaire) {
        this.questionnaire = questionnaire;
    }

    /**
     * Gets the suggestions.
     * 
     * @return the suggestions
     */
    public String getSuggestions() {
        return suggestions;
    }

    /**
     * Sets the suggestions.
     * 
     * @param suggestions
     *            the suggestions to set
     */
    public void setSuggestions(String suggestions) {
        this.suggestions = suggestions;
    }

    /**
     * Gets the purchased products.
     * 
     * @return the purchasedProducts
     */
    public Set<PurchasedProduct> getPurchasedProducts() {
        return purchasedProducts;
    }

    /**
     * Sets the purchased products.
     * 
     * @param purchasedProducts
     *            the purchasedProducts to set
     */
    public void setPurchasedProducts(Set<PurchasedProduct> purchasedProducts) {
        this.purchasedProducts = purchasedProducts;
    }
   
    /**
     * Gets the proposer.
     * 
     * @return the proposer
     */
    public AgreementHolder getProposer() {
        return proposer;
    }  

	/**
     * Sets the proposer.
     * 
     * @param proposer
     *            the proposer to set
     */
    public void setProposer(AgreementHolder proposer) {
        this.proposer = proposer;
    }

    /**
     * Gets the checks if is proposer same as customer.
     * 
     * @return the isProposerSameAsCustomer
     */
    public Boolean getIsProposerSameAsCustomer() {
        return isProposerSameAsCustomer;
    }

    /**
     * Sets the checks if is proposer same as customer.
     * 
     * @param isProposerSameAsCustomer
     *            the isProposerSameAsCustomer to set
     */
    public void setIsProposerSameAsCustomer(Boolean isProposerSameAsCustomer) {
        this.isProposerSameAsCustomer = isProposerSameAsCustomer;
    }

    /**
     * Gets the identifier.
     * 
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Sets the identifier.
     * 
     * @param identifier
     *            the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets the agent id.
     * 
     * @return the agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * Sets the agent id.
     * 
     * @param agentId
     *            the agentId to set
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * Gets the last sync date.
     * 
     * @return the lastSyncDate
     */
    public Date getLastSyncDate() {
        return lastSyncDate;
    }

    /**
     * Sets the last sync date.
     * 
     * @param lastSyncDate
     *            the lastSyncDate to set
     */
    public void setLastSyncDate(Date lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

	/**
	 * Sets the parent lead id.
	 *
	 * @param parentLeadId the new parent lead id
	 */
	public void setParentLeadId(String parentLeadId) {
		this.parentLeadId = parentLeadId;
	}

	/**
	 * Gets the parent lead id.
	 *
	 * @return the parent lead id
	 */
	public String getParentLeadId() {
		return parentLeadId;
	}

	public Boolean getHasDependent() {
		return hasDependent;
	}

	public void setHasDependent(Boolean hasDependent) {
		this.hasDependent = hasDependent;
	}

	public String getClaimCount() {
		return claimCount;
	}

	public void setClaimCount(String claimCount) {
		this.claimCount = claimCount;
	}

	public CustomerRelationship getCustomerRelationship() {
		return customerRelationship;
	}

	public void setCustomerRelationship(CustomerRelationship customerRelationship) {
		this.customerRelationship = customerRelationship;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getBranchId() {
		return branchId;
	}

	public CustomerNeedCodeList getNeed() {
		return need;
	}

	public void setNeed(CustomerNeedCodeList need) {
		this.need = need;
	}

	public ActivityCodeList getAction() {
		return action;
	}

	public void setAction(ActivityCodeList action) {
		this.action = action;
	}

	public ActivityCodeList getNextAction() {
		return nextAction;
	}

	public void setNextAction(ActivityCodeList nextAction) {
		this.nextAction = nextAction;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}	

	/**
	 * @param allocationType the allocationType to set
	 */
	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}

	/**
	 * @return the allocationType
	 */
	public String getAllocationType() {
		return allocationType;
	}

	/**
	 * @return the subDate
	 */
	public Date getSubDate() {
		return subDate;
	}

	/**
	 * @param subDate the subDate to set
	 */
	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public String getStateValue() {
		return stateValue;
	}

	public void setStateValue(String stateValue) {
		this.stateValue = stateValue;
	}

	public String getCityValue() {
		return cityValue;
	}

	public void setCityValue(String cityValue) {
		this.cityValue = cityValue;
	}

	public String getWardValue() {
		return wardValue;
	}

	public void setWardValue(String wardValue) {
		this.wardValue = wardValue;
	}

	public String getCountryValue() {
		return countryValue;
	}

	public void setCountryValue(String countryValue) {
		this.countryValue = countryValue;
	}

}
