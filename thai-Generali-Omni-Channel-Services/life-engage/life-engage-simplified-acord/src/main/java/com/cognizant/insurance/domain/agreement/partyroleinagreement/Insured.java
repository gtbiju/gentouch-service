/**
 * 
 */
package com.cognizant.insurance.domain.agreement.partyroleinagreement;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;

import com.cognizant.insurance.domain.agreement.agreementcodelists.InsurableInterestCodeList;
import com.cognizant.insurance.domain.agreement.agreementcodelists.InsuredTypeCodeList;
import com.cognizant.insurance.domain.commonelements.commonclasses.FamilyHealthHistory;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;

/**
 * Any person or organization whose interest in the subject matter of the
 * insurance is protected under the insurance contract. The insured does not
 * necessarily need to be the contractholder or the beneficiary. (Alternate
 * definition: A person or organization covered as defined in the insurance
 * contract. In life insurance policies there is one designated insured, the
 * person so named; or a contract can be issued to numerous insured on a group
 * basis. The insured persons in property and casualty policies may also include
 * residents of the insured's household, such as a spouse, relatives of either,
 * and other individuals under their care, custody, and control if under age 21.
 * See also "Named Insured".
 * 
 * SOURCE: Barron's
 * 
 * e.g: Barbara Wire, as insured in a dangerous sports disability coverage. <!--
 * end-UML-doc -->
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("INSURED")
public class Insured extends PartyRoleInAgreement {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1970841447838916871L;

    /**
     * The exact, legal, insured name/description. 
     * 
     * 
     * 
     */
    private String legalWording;

    /**
     * A code indicating the type of insured. 
     * 
     * 
     * 
     */
    @Enumerated
    private InsuredTypeCodeList typeCode;

    /**
     * A code indicating the insurable interest of the insured party.
     * 
     * 
     * 
     */
    @Enumerated
    private InsurableInterestCodeList insurableInterestCode;
    
    /******Newly added for EApp - Start********/    
    /**
     * Indicates that the user does not require a medical checkup prior to policy issuance. 
     */   
    private Boolean isNonMedicalCase;
    
   
    /**
     * Indicates the relationship of the insured with proposer. 
     */    
    private String relationshipWithProposer;
    
    private String relationshipWithPO;
    
    /**
	 * @return the relationshipWithPO
	 */
	public String getRelationshipWithPO() {
		return relationshipWithPO;
	}

	/**
	 * @param relationshipWithPO the relationshipWithPO to set
	 */
	public void setRelationshipWithPO(String relationshipWithPO) {
		this.relationshipWithPO = relationshipWithPO;
	}

	private String actualRelationship;
    
    private String relationType;
    
    private String reasonForInsurance;
    
    private Boolean disabledStatus;
    
    private String isInsuredSameAsPayer;
    
    private String occupationClassCode;
    
	/**
	 * Indicates if the premium payer is different from the Insured.
	 */
	private boolean payorDifferentFromInsuredIndicator;
    
  /*  *//**
     * Indicates the set of UserSelections made as part of a questionnaire. 
     *//* 
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    private Set<UserSelection> userSelectedOptions;*/
    
    
    public Boolean getIsNonMedicalCase() {
        return isNonMedicalCase;
    }

    public void setIsNonMedicalCase(Boolean isNonMedicalCase) {
        this.isNonMedicalCase = isNonMedicalCase;
    }

    /** The family health history. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    private Set<FamilyHealthHistory> familyHealthHistory;
    
    /******Newly added for EApp - End ********/
   
    /**
     * Gets the legalWording.
     * 
     * @return Returns the legalWording.
     */
    public String getLegalWording() {
        return legalWording;
    }

    /**
     * Gets the typeCode.
     * 
     * @return Returns the typeCode.
     */
    public InsuredTypeCodeList getTypeCode() {
        return typeCode;
    }

    /**
     * Gets the insurableInterestCode.
     * 
     * @return Returns the insurableInterestCode.
     */
    public InsurableInterestCodeList getInsurableInterestCode() {
        return insurableInterestCode;
    }

    /**
     * Sets The legalWording.
     * 
     * @param legalWording
     *            The legalWording to set.
     */
    public void setLegalWording(final String legalWording) {
        this.legalWording = legalWording;
    }

    /**
     * Sets The typeCode.
     * 
     * @param typeCode
     *            The typeCode to set.
     */
    public void setTypeCode(final InsuredTypeCodeList typeCode) {
        this.typeCode = typeCode;
    }

    /**
     * Sets The insurableInterestCode.
     * 
     * @param insurableInterestCode
     *            The insurableInterestCode to set.
     */
    public void setInsurableInterestCode(
            final InsurableInterestCodeList insurableInterestCode) {
        this.insurableInterestCode = insurableInterestCode;
    }

    /**
     * Gets the relationshipWithProposer.
     *
     * @return Returns the relationshipWithProposer.
     */
    public final String getRelationshipWithProposer() {
        return relationshipWithProposer;
    }

    /**
     * Sets The relationshipWithProposer.
     *
     * @param relationshipWithProposer The relationshipWithProposer to set.
     */
    public final void setRelationshipWithProposer(String relationshipWithProposer) {
        this.relationshipWithProposer = relationshipWithProposer;
    }

  /*  *//**
     * Gets the userSelectedOptions.
     *
     * @return Returns the userSelectedOptions.
     *//*
    public final Set<UserSelection> getUserSelectedOptions() {
        return userSelectedOptions;
    }

    *//**
     * Sets The userSelectedOptions.
     *
     * @param userSelectedOptions The userSelectedOptions to set.
     *//*
    public final void
            setUserSelectedOptions(Set<UserSelection> userSelectedOptions) {
        this.userSelectedOptions = userSelectedOptions;
    }*/

    /**
     * Gets the familyHealthHistory.
     *
     * @return Returns the familyHealthHistory.
     */
    public final Set<FamilyHealthHistory> getFamilyHealthHistory() {
        return familyHealthHistory;
    }

    /**
     * Sets The familyHealthHistory.
     *
     * @param familyHealthHistory The familyHealthHistory to set.
     */
    public final void
            setFamilyHealthHistory(Set<FamilyHealthHistory> familyHealthHistory) {
        this.familyHealthHistory = familyHealthHistory;
    }

	public String getReasonForInsurance() {
		return reasonForInsurance;
	}

	public void setReasonForInsurance(String reasonForInsurance) {
		this.reasonForInsurance = reasonForInsurance;
	}

	/**
	 * @param payorDifferentFromInsuredIndicator the payorDifferentFromInsuredIndicator to set
	 */
	public void setPayorDifferentFromInsuredIndicator(
			boolean payorDifferentFromInsuredIndicator) {
		this.payorDifferentFromInsuredIndicator = payorDifferentFromInsuredIndicator;
	}

	/**
	 * @return the payorDifferentFromInsuredIndicator
	 */
	public boolean isPayorDifferentFromInsuredIndicator() {
		return payorDifferentFromInsuredIndicator;
	}

	/**
	 * @param actualRelationship the actualRelationship to set
	 */
	public void setActualRelationship(String actualRelationship) {
		this.actualRelationship = actualRelationship;
	}

	/**
	 * @return the actualRelationship
	 */
	public String getActualRelationship() {
		return actualRelationship;
	}

	/**
	 * @param relationType the relationType to set
	 */
	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	/**
	 * @return the relationType
	 */
	public String getRelationType() {
		return relationType;
	}

	/**
	 * @param disabledStatus the disabledStatus to set
	 */
	public void setDisabledStatus(Boolean disabledStatus) {
		this.disabledStatus = disabledStatus;
	}

	/**
	 * @return the disabledStatus
	 */
	public Boolean getDisabledStatus() {
		return disabledStatus;
	}

	public String getIsInsuredSameAsPayer() {
		return isInsuredSameAsPayer;
	}

	public void setIsInsuredSameAsPayer(String isInsuredSameAsPayer) {
		this.isInsuredSameAsPayer = isInsuredSameAsPayer;
	}

	public String getOccupationClassCode() {
		return occupationClassCode;
	}

	public void setOccupationClassCode(String occupationClassCode) {
		this.occupationClassCode = occupationClassCode;
	}

    
}
