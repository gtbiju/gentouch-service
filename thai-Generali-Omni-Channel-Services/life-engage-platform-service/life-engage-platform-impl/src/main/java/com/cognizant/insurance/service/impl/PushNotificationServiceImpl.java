/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 22, 2015
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngageAuditComponent;
import com.cognizant.insurance.component.LifeEngagePushNotificationComponent;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.services.PushNotificationService;

/**
 * The Class class PushNotificationServiceImpl.
 */
@Service("pushNotificationService")
public class PushNotificationServiceImpl implements PushNotificationService {

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The life engage push notification component. */
    @Autowired
    private LifeEngagePushNotificationComponent lifeEngagePushNotificationComponent;

    /** The life engage audit component. */
    @Autowired
    private LifeEngageAuditComponent lifeEngageAuditComponent;

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(PushNotificationServiceImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.PushNotificationService#registerDevice(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String registerDevice(final String json) throws BusinessException {
        JSONObject jsonObject;
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        String registerDeviceRs = null;
        try {
            jsonObject = new JSONObject(json);

            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonTransactionArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonTransactionArray.length() > 0) {
                final JSONObject jsonTransactonObj = jsonTransactionArray.getJSONObject(0);
                registerDeviceRs =
                        lifeEngagePushNotificationComponent.registerDevice(requestInfo, jsonTransactonObj.toString(),
                                lifeEngageAudit);
                jsonRsArray.put(new JSONObject(registerDeviceRs));
            }
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.error("Parse exception while registering device" + e);
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.PushNotificationService#unRegisterDevice(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String unRegisterDevice(final String json) throws BusinessException {
        JSONObject jsonObject;
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        String registerDeviceRs = null;
        try {
            jsonObject = new JSONObject(json);

            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonTransactionArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonTransactionArray.length() > 0) {
                final JSONObject jsonTransactonObj = jsonTransactionArray.getJSONObject(0);
                registerDeviceRs =
                        lifeEngagePushNotificationComponent.unRegisterDevice(requestInfo, jsonTransactonObj.toString(),
                                lifeEngageAudit);
                jsonRsArray.put(new JSONObject(registerDeviceRs));
            }
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.error("Parse exception while unregistering device" + e);
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.PushNotificationService#pushNotifications(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String pushNotifications(final String json) throws BusinessException {
        JSONObject jsonObject;
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        String pushNotificationRs = null;
        try {
            jsonObject = new JSONObject(json);

            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonTransactionArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonTransactionArray.length() > 0) {
                for (int i = 0; i < jsonTransactionArray.length(); i++) {
                    final JSONObject jsonTransactonObj = jsonTransactionArray.getJSONObject(i);
                    pushNotificationRs =
                            lifeEngagePushNotificationComponent.pushNotifications(requestInfo,
                                    jsonTransactonObj.toString(), lifeEngageAudit);
                    jsonRsArray.put(new JSONObject(pushNotificationRs));
                }
            }
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("Parse exception while push notifications" + e);
            throwBusinessException(true, e.getMessage());
        }
        return jsonObjectRs.toString();

    }

}
