/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Estimate Type - A list of the various types of estimates that can be
 * provided.
 * 
 * @author 301350
 * 
 * 
 */
public enum EstimateTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * The initial estimated cost. Initial estimates are often kept to determine
     * the success of estimating procedures 
     * 
     * 
     * 
     */
    InitialEstimatedCost,
    /**
     * The initial estimated duration. 
     * 
     * 
     * 
     */
    InitialEstimatedDuration,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * The revised estimated cost. This should be able to be added multiple
     * times
     * 
     * 
     * 
     */
    RevisedEstimatedCost,
    /**
     * The revised estimated duration. This should be able to be added multiple
     * times.
     * 
     * 
     * 
     */
    RevisedEstimatedDuration
}
