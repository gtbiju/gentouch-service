/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Nov 13, 2012
 */
package com.cognizant.insurance.agent.party.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;

import com.cognizant.insurance.agent.dao.exception.DaoException;
import com.cognizant.insurance.agent.dao.impl.DaoImpl;
import com.cognizant.insurance.agent.domain.agreement.Agreement;
import com.cognizant.insurance.agent.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.agent.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Appointee;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationInteractionTypeCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
import com.cognizant.insurance.agent.party.dao.PartyDao;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.request.impl.JPARequestImpl;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.response.impl.ResponseImpl;
import com.cognizant.insurance.agent.searchcriteria.SearchCountResult;
import com.cognizant.insurance.agent.searchcriteria.SearchCriteria;

/**
 * The Class class PartyDaoImpl.
 */
/**
 * @author 300797
 * 
 */
public class PartyDaoImpl extends DaoImpl implements PartyDao {

   
	
	/** The Constant GET_LMS_BY_IDENTIFIER. */
    private static final String GET_LMS_BY_IDENTIFIER = "select cus from Customer cus "
            + "WHERE cus.identifier =?1 and cus.statusCode in ?2";
	
	/** The Constant GET_LMS_BY_TRANSTRACKING_ID. */
    private static final String GET_LMS_BY_TRANSTRACKING_ID = "select cus from Customer cus "
            + "WHERE cus.transTrackingId =?1 and cus.statusCode in ?2 and cus.agentId =?3 ";
    
    /**
     * Builds the status list.
     * 
     * @return the list
     */
    protected List<AgreementStatusCodeList> buildStatusList() {
        List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();
        statusList.add(AgreementStatusCodeList.Initial);
        statusList.add(AgreementStatusCodeList.Final);
        statusList.add(AgreementStatusCodeList.Proposed);
        return statusList;
    }

    /**
     * Find using query.
     * 
     * @param request
     *            the request
     * @param queryString
     *            the query string
     * @param params
     *            the params
     * @return the list
     */
    protected List<Object> findUsingQuery(Request request, String queryString, Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<Object>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * Find using query.
     * 
     * @param request
     *            the request
     * @param queryString
     *            the query string
     * @param params
     *            the params
     * @return the list
     */
    protected List findUsingQueryForInteger(Request request, String queryString, Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i+1 , params[i]);
                }
            }
            return   query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
    
    /**
     * Find using query with limit.
     * 
     * @param request
     *            the request
     * @param queryString
     *            the query string
     * @param limit
     *            the limit
     * @param params
     *            the params
     * @return the list
     */
    protected List<Object> findUsingQueryWithLimit(final Request request, final String queryString, Integer limit,
            Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query =
                    jpaRequestImpl.getEntityManager().createQuery(queryString).setMaxResults(limit.intValue());
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<Object>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveAppointee(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Appointee> retrieveAppointee(Request<Agreement> agreementRequest) {
        Response<Appointee> response = new ResponseImpl<Appointee>();
        if (agreementRequest.getType() != null) {
            String query = "select appointee from Appointee appointee where appointee.isPartyRoleIn.id =?1";
            List<Object> appointeeList = findUsingQuery(agreementRequest, query, agreementRequest.getType().getId());
            if (CollectionUtils.isNotEmpty(appointeeList)) {
                response.setType((Appointee) appointeeList.get(0));
            }
        }

        return response;
    }

    /**
     * Gets the appointee.
     * 
     * @param request
     *            the request
     * @param iAgreement
     *            the i agreement
     * @param statusList
     *            the status list
     * @param transactions
     *            the transactions
     * @return the appointee
     */
    @Deprecated
    private void getAppointee(Request<InsuranceAgreement> request, InsuranceAgreement iAgreement,
            List<AgreementStatusCodeList> statusList, Transactions transactions) {
        String query;
        query =
                "select appointee from Appointee appointee where appointee.isPartyRoleIn.identifier =?1 and appointee.isPartyRoleIn.statusCode in ?2 ";
        List<Object> appointeeList = findUsingQuery(request, query, iAgreement.getIdentifier(), statusList);
        for (Object appointeeElement : appointeeList) {
            Appointee appointee = (Appointee) appointeeElement;
            transactions.setAppointee(appointee);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveBeneficiaries(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Set<Beneficiary>> retrieveBeneficiaries(Request<Agreement> agreementRequest) {
        Response<Set<Beneficiary>> response = new ResponseImpl<Set<Beneficiary>>();
        if (agreementRequest.getType() != null) {
            String query = "select beneficiary from Beneficiary beneficiary where beneficiary.isPartyRoleIn.id =?1";
            List<Object> beneficiaryList = findUsingQuery(agreementRequest, query, agreementRequest.getType().getId());
            if (CollectionUtils.isNotEmpty(beneficiaryList)) {
                Set<Beneficiary> beneficiaries = new HashSet<Beneficiary>();
                for (Object object : beneficiaryList) {
                    beneficiaries.add((Beneficiary) object);
                }
                response.setType(beneficiaries);
            }
        }
        return response;

    }

    /**
     * Gets the beneficiaries.
     * 
     * @param request
     *            the request
     * @param iAgreement
     *            the i agreement
     * @param statusList
     *            the status list
     * @param transactions
     *            the transactions
     * @return the beneficiaries
     */
    @Deprecated
    private void getBeneficiaries(Request<InsuranceAgreement> request, InsuranceAgreement iAgreement,
            List<AgreementStatusCodeList> statusList, Transactions transactions) {
        String query;
        query =
                "select beneficiary from Beneficiary beneficiary where beneficiary.isPartyRoleIn.identifier =?1 and beneficiary.isPartyRoleIn.statusCode in ?2 ";
        List<Object> beneficiaryList = findUsingQuery(request, query, iAgreement.getIdentifier(), statusList);
        if (CollectionUtils.isNotEmpty(beneficiaryList)) {
            Set<Beneficiary> beneficiaries = new HashSet<Beneficiary>();
            for (Object object : beneficiaryList) {
                beneficiaries.add((Beneficiary) object);
            }
            transactions.setBeneficiaries(beneficiaries);
        }
    }

    /**
     * Gets the insurance agreement.
     * 
     * @param request
     *            the request
     * @param iAgreement
     *            the i agreement
     * @param statusList
     *            the status list
     * @param transactions
     *            the transactions
     * @return the insurance agreement
     */
    @Deprecated
    private void getInsuranceAgreement(Request<InsuranceAgreement> request, InsuranceAgreement iAgreement,
            List<AgreementStatusCodeList> statusList, Transactions transactions) {
        String query;
        query =
                "select insAgr from InsuranceAgreement insAgr where insAgr.identifier =?1 and insAgr.statusCode in ?2 and insAgr.previousPolicyIndicator=?3";
        List<Object> insuranceAgreementList =
                findUsingQuery(request, query, iAgreement.getIdentifier(), statusList, Boolean.FALSE);
        if (CollectionUtils.isEmpty(insuranceAgreementList)) {
            throw new DaoException("No agreement with identifier : " + iAgreement.getIdentifier());
        }
        for (Object insuranceAgreementElement : insuranceAgreementList) {
            InsuranceAgreement insuranceAgreement = (InsuranceAgreement) insuranceAgreementElement;
            transactions.setProposal(insuranceAgreement);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveInsured(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Insured> retrieveInsured(Request<Agreement> agreementRequest) {
        Response<Insured> response = new ResponseImpl<Insured>();
        if (agreementRequest.getType() != null) {
            String query = "select ins from Insured ins where ins.isPartyRoleIn.id =?1";
            List<Object> insuredList = findUsingQuery(agreementRequest, query, agreementRequest.getType().getId());
            if (CollectionUtils.isNotEmpty(insuredList)) {
                response.setType((Insured) insuredList.get(0));
            }
        }
        return response;

    }

    /**
     * Gets the insured.
     * 
     * @param request
     *            the request
     * @param iAgreement
     *            the i agreement
     * @param statusList
     *            the status list
     * @param transactions
     *            the transactions
     * @return the insured
     */
    @Deprecated
    private Insured getInsured(Request<InsuranceAgreement> request, InsuranceAgreement iAgreement,
            List<AgreementStatusCodeList> statusList, Transactions transactions) {
        String query =
                "select ins from Insured ins where ins.isPartyRoleIn.identifier =?1 and ins.isPartyRoleIn.statusCode in ?2 ";
        List<Object> insuredList = findUsingQuery(request, query, iAgreement.getIdentifier(), statusList);
        Insured insured = null;
        for (Object insuredElement : insuredList) {
            insured = (Insured) insuredElement;
            transactions.setInsured(insured);
        }
        return insured;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrievePayer(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<PremiumPayer> retrievePayer(Request<Agreement> agreementRequest) {
        Response<PremiumPayer> response = new ResponseImpl<PremiumPayer>();
        if (agreementRequest.getType() != null) {
            String query = "select payer from PremiumPayer payer where payer.isPartyRoleIn.id =?1";
            List<Object> payerList = findUsingQuery(agreementRequest, query, agreementRequest.getType().getId());
            if (CollectionUtils.isNotEmpty(payerList)) {
                response.setType((PremiumPayer) payerList.get(0));
            }
        }
        return response;
    }

    /**
     * Gets the payer.
     * 
     * @param request
     *            the request
     * @param iAgreement
     *            the i agreement
     * @param statusList
     *            the status list
     * @param transactions
     *            the transactions
     * @return the payer
     */
    @Deprecated
    private void getPayer(Request<InsuranceAgreement> request, InsuranceAgreement iAgreement,
            List<AgreementStatusCodeList> statusList, Transactions transactions) {
        String query;
        query =
                "select payer from PremiumPayer payer where payer.isPartyRoleIn.identifier =?1 and payer.isPartyRoleIn.statusCode in ?2 ";
        List<Object> payerList = findUsingQuery(request, query, iAgreement.getIdentifier(), statusList);
        for (Object payerElement : payerList) {
            PremiumPayer payer = (PremiumPayer) payerElement;
            transactions.setPayer(payer);
        }
    }

    /**
     * Gets the previous agreement.
     * 
     * @param request
     *            the request
     * @param transactions
     *            the transactions
     * @param insured
     *            the insured
     * @return the previous agreement
     */
    @Deprecated
    private void getPreviousAgreement(Request<InsuranceAgreement> request, Transactions transactions, Insured insured) {
        String query;
        query =
                "select insAgr from InsuranceAgreement insAgr,Insured ins where ins.playerParty.id =?1 and insAgr.id=ins.isPartyRoleIn.id and insAgr.previousPolicyIndicator=?2";
        List<Object> prevAgreementList = findUsingQuery(request, query, insured.getPlayerParty().getId(), Boolean.TRUE);
        if (CollectionUtils.isNotEmpty(prevAgreementList)) {
            Set<InsuranceAgreement> prevAgreements = new HashSet<InsuranceAgreement>();
            for (Object object : prevAgreementList) {
                prevAgreements.add((InsuranceAgreement) object);
            }
            transactions.setPreviousAgreements(prevAgreements);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveProposer(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<AgreementHolder> retrieveProposer(Request<Agreement> agreementRequest) {
        Response<AgreementHolder> response = new ResponseImpl<AgreementHolder>();
        if (agreementRequest.getType() != null) {
            String query = "select proposer from AgreementHolder proposer where proposer.isPartyRoleIn.id =?1";
            List<Object> proposerList = findUsingQuery(agreementRequest, query, agreementRequest.getType().getId());
            if (CollectionUtils.isNotEmpty(proposerList)) {
                response.setType((AgreementHolder) proposerList.get(0));
            }
        }
        return response;
    }

    /**
     * Gets the proposer.
     * 
     * @param request
     *            the request
     * @param iAgreement
     *            the i agreement
     * @param statusList
     *            the status list
     * @param transactions
     *            the transactions
     * @return the proposer
     */
    @Deprecated
    private void getProposer(Request<InsuranceAgreement> request, InsuranceAgreement iAgreement,
            List<AgreementStatusCodeList> statusList, Transactions transactions) {
        String query;
        query =
                "select proposer from AgreementHolder proposer where proposer.isPartyRoleIn.identifier =?1 and proposer.isPartyRoleIn.statusCode in ?2 ";
        List<Object> proposerList = findUsingQuery(request, query, iAgreement.getIdentifier(), statusList);
        for (Object proposerElement : proposerList) {
            AgreementHolder proposer = (AgreementHolder) proposerElement;
            transactions.setProposer(proposer);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieve(com.cognizant.insurance .request.Request,
     * com.cognizant.insurance.request.vo.RequestPayload)
     */
    @Deprecated
    public Response<Transactions> retrieve(Request<InsuranceAgreement> request) {
        InsuranceAgreement iAgreement = request.getType();

        List<AgreementStatusCodeList> statusList = buildStatusList();

        final Response<Transactions> response = new ResponseImpl<Transactions>();
        Transactions transactions = new Transactions();

        getInsuranceAgreement(request, iAgreement, statusList, transactions);

        Insured insured = getInsured(request, iAgreement, statusList, transactions);

        if (insured != null) {
            getPreviousAgreement(request, transactions, insured);
        }

        getProposer(request, iAgreement, statusList, transactions);

        getBeneficiaries(request, iAgreement, statusList, transactions);

        getAppointee(request, iAgreement, statusList, transactions);

        getPayer(request, iAgreement, statusList, transactions);

        if (transactions.getProposal() != null) {
            transactions.setProposalNumber(iAgreement.getIdentifier().toString());
        }
        response.setType(transactions);
        return response;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveAll(com.cognizant.insurance.request.Request)
     */
    @Deprecated
    public Response<List<Transactions>> retrieveAll(Request<InsuranceAgreement> request) {

        final Response<List<Transactions>> response = new ResponseImpl<List<Transactions>>();
        List<Transactions> transactionsList = new ArrayList<Transactions>();

        InsuranceAgreement insuranceAgreement = request.getType();
        List<AgreementStatusCodeList> statusList = buildStatusList();
        // get all agreements for the agent based on the lastSyncDate. If no lastSyncDate is provides, select all data.
        List<Object> insuranceAgreementIds = getInsuranceAgreementIdList(request, insuranceAgreement, statusList);
        for (Object insAgreementId : insuranceAgreementIds) {
            // set the new identifier value. The AgentId will remain the same.
            insuranceAgreement.setIdentifier(insAgreementId.toString());
            // set the updated insAgr object back to request
            request.setType(insuranceAgreement);
            // populate all the details for each proposal no and set it back to response list.
            transactionsList.add(this.retrieve(request).getType());

        }
        response.setType(transactionsList);
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveBasicDetails(com.cognizant .insurance.request.Request)
     */
    @Deprecated
    public Response<List<Transactions>> retrieveBasicDetails(Request<InsuranceAgreement> request) {

        InsuranceAgreement iAgreement = request.getType();
        String query;
        List<AgreementStatusCodeList> statusList = buildStatusList();
        final Response<List<Transactions>> response = new ResponseImpl<List<Transactions>>();
        List<Transactions> trList = new ArrayList<Transactions>();

        List<Object> insuranceAgreementIdList = getInsuranceAgreementIdList(request, iAgreement, statusList);
        for (Object insuredIdentifierElement : insuranceAgreementIdList) {
            String identString = insuredIdentifierElement.toString();
            Transactions transactions = new Transactions();
            query =
                    "select proposer from AgreementHolder proposer where proposer.isPartyRoleIn.identifier =?1 and proposer.isPartyRoleIn.statusCode in ?2";

            List<Object> proposerList = findUsingQuery(request, query, Integer.parseInt(identString), statusList);
            for (Object proposerElement : proposerList) {
                AgreementHolder proposer = (AgreementHolder) proposerElement;
                transactions.setProposer(proposer);
            }

            query =
                    "select insAgr from InsuranceAgreement insAgr where insAgr.identifier =?1 and insAgr.statusCode in ?2";
            List<Object> insuranceAgreementList =
                    findUsingQuery(request, query, Integer.parseInt(identString), statusList);
            for (Object insuranceAgreementElement : insuranceAgreementList) {
                InsuranceAgreement insuranceAgreement = (InsuranceAgreement) insuranceAgreementElement;
                transactions.setProposal(insuranceAgreement);
            }
            transactions.setProposalNumber(identString);
            trList.add(transactions);
        }
        response.setType(trList);
        return response;

    }

    /**
     * Gets the insurance agreement id list.
     * 
     * @param request
     *            the request
     * @param iAgreement
     *            the i agreement
     * @param statusList
     *            the status list
     * @return the insurance agreement id list
     */
    @Deprecated
    private List<Object> getInsuranceAgreementIdList(Request<InsuranceAgreement> request,
            InsuranceAgreement iAgreement, List<AgreementStatusCodeList> statusList) {
        String query =
                "select insAgr.identifier from InsuranceAgreement insAgr where insAgr.agentId =?1 and insAgr.statusCode in ?2 and insAgr.creationDateTime>=?3 and insAgr.contextId =?4 order by insAgr.creationDateTime desc";
        List<Object> insuranceAgreementIdList =
                findUsingQuery(request, query, iAgreement.getAgentId(), statusList, iAgreement.getLastSyncDate(),
                        request.getContextId());
        return insuranceAgreementIdList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveCustomer(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Customer> retrieveCustomer(Request<Customer> identifierRequest,
           final Request<List<CustomerStatusCodeList>> statusRequest) {
            Response<Customer> response = new ResponseImpl<Customer>();
            if (identifierRequest.getType() != null) {
                final Customer customer = (Customer) identifierRequest.getType();
                if (customer.getIdentifier() != null && !("".equals(customer.getIdentifier()))) {
                    final List<Customer> customerObjList =
                            findByQuery(identifierRequest, GET_LMS_BY_IDENTIFIER, customer.getIdentifier(), statusRequest.getType());
                    final List<Customer> customers = new ArrayList<Customer>();
                    if (CollectionUtils.isNotEmpty(customerObjList)) {
                        for (Object customerObj : customerObjList) {
                            customers.add((Customer) customerObj);
                        }
                    } else {
                        throw new DaoException("No LMS with identifier : " + customer.getIdentifier());
                    }
                    response.setType((Customer) customers.get(0)); 

                } else {
                    final List<Customer> customerObjList =
                            findByQuery(identifierRequest, GET_LMS_BY_TRANSTRACKING_ID, customer.getTransTrackingId(),
                                    statusRequest.getType(),customer.getAgentId());
                    final List<Customer> customers = new ArrayList<Customer>();
                    if (CollectionUtils.isNotEmpty(customerObjList)) {
                        for (Object customerObj : customerObjList) {
                            customers.add((Customer) customerObj);
                        }
                    }
                    Customer customerRetrieved = new Customer();
                    if( customers != null && customers.size() > 0){
                    	customerRetrieved = (Customer) customers.get(0);
                    }
                    response.setType(customerRetrieved);
                }
                
            }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveCustomers(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public List<Customer> retrieveCustomers(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest, Request<Integer> limitRequest) {
        String query;
        final Customer customer = (Customer) customerRequest.getType();
        query =
                "select cus from Customer cus where cus.agentId =?1 "
                        + "and cus.statusCode in ?2 and cus.creationDateTime>=?3 "
                        + "order by cus.creationDateTime asc";
        List<Object> customerList;
        if (limitRequest != null && limitRequest.getType() != null && limitRequest.getType() > 0) {
            customerList =
                    findUsingQueryWithLimit(customerRequest, query, limitRequest.getType(), customer.getAgentId(),
                            statusRequest.getType(), customer.getLastSyncDate());
        } else {
            customerList =
                    findUsingQuery(customerRequest, query, customer.getAgentId(), statusRequest.getType(),
                            customer.getLastSyncDate());
        }
        final List<Customer> customers = new ArrayList<Customer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object customerObj : customerList) {
                customers.add((Customer) customerObj);
            }
        }
        return customers;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.dao.PartyDao#retrieveCustomerForIdentifier(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public List<Customer> retrieveCustomerForIdentifier(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        String query;
        query = "select cus from Customer cus where cus.identifier =?1 and statusCode in ?2";
        final Customer customer = (Customer) customerRequest.getType();
        return (List<Customer>) findByQuery(customerRequest, query, customer.getIdentifier(), statusRequest.getType());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.dao.PartyDao#retrieveLMSDuplicateCustomers(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.vo.Transactions, com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public List<Customer> retrieveLMSDuplicateCustomers(final Request<PartyRoleInRelationship> customerRequest,
            final Transactions transactions, final Request<List<CustomerStatusCodeList>> statusRequest,
            final Request<List<TelephoneTypeCodeList>> telephoneTypeRequest) {
        // TODO Auto-generated method stub
        String query;
        query =
                "select customer from Customer customer join fetch customer.playerParty party join fetch party.name name join fetch party.preferredContact pc join fetch pc.preferredContactPoint pcp where pcp.typeCode in ?1 and name.givenName=?2 and pcp.fullNumber=?3 and customer.statusCode in ?4";
        List<Object> customerList =
                findUsingQuery(customerRequest, query, telephoneTypeRequest.getType(), transactions.getKey2(),
                        transactions.getKey4(), statusRequest.getType());
        final List<Customer> customers = new ArrayList<Customer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object customerObj : customerList) {
                customers.add((Customer) customerObj);
            }
        }
        return customers;
    }

    
     /** (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#getCustomerCount(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)*/
     
    @Override
    public Response<SearchCountResult> getCustomerCount(Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
        List<Object> searchCriteriaList = null;
        List<Object>  noOfAppointment = null ;
        String type = null;
        String query;
        String queryForCommunication;
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            type = searchCriteria.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select count(customer.identifier),customer.statusCode from Customer customer where customer.agentId =?1 "
                                + "and customer.statusCode in ?2 and "
                                + "(customer.transactionKeys.key2 like ?3 or customer.transactionKeys.key4 like ?3) "
                                + "group by customer.statusCode";
                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType(), "%" + searchCriteria.getValue() + "%");

            } else {
                query =
                        "select count(customer.identifier), customer.statusCode from Customer customer where customer.agentId =?1 "
                                + "and customer.statusCode in ?2 and YEAR(customer.creationDateTime) = YEAR(GETDATE())"
                        		+ "and MONTH(customer.creationDateTime) = MONTH(GETDATE())" + "group by customer.statusCode";
                
                queryForCommunication = "select count(customer.identifier) from Customer customer join customer.receivesCommunication test where  test.interactionType= '1' " 
                						+ "and customer.agentId =?1 and YEAR(customer.creationDateTime) = YEAR(GETDATE())"
                						+ "and MONTH(customer.creationDateTime) = MONTH(GETDATE())";

                searchCriteriaList = findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),statusRequest.getType());
                noOfAppointment = findUsingQueryForInteger(searchCriteriatRequest, queryForCommunication,searchCriteria.getAgentId());
            }
        }
        final List<SearchCountResult> subModeResults = new ArrayList<SearchCountResult>();
        final SearchCountResult searchCountResult = new SearchCountResult();
        SearchCountResult submode;
        Integer totalModCount = 0;
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object searchCriteria : searchCriteriaList) {
                Object[] values = (Object[]) searchCriteria;
                Long count = (Long) values[0];
                CustomerStatusCodeList mode = (CustomerStatusCodeList) values[1];
                submode = new SearchCountResult();
                submode.setCount(count.toString());
                submode.setMode(mode.toString());

                subModeResults.add(submode);
                totalModCount += Integer.valueOf(submode.getCount());
            }
            searchCountResult.setSubModeResults(subModeResults);
        }
        Long countOfAppointment = (Long) noOfAppointment.get(0);
        searchCountResult.setCount(String.valueOf(totalModCount));
        searchCountResult.setMode(type);
        searchCountResult.setNoOfAppointments(countOfAppointment.toString());
        searchCountResponse.setType(searchCountResult);
        return searchCountResponse;
    }

	

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveFilterCustomers(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public List<Customer> retrieveFilterCustomers(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<CustomerStatusCodeList>> statusRequest) {
        List<Object> customerList = null;
        String query;
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select cus from Customer cus where cus.agentId =?1 and cus.statusCode in ?2 and "
                                + "(cus.transactionKeys.key2 like ?3 or cus.transactionKeys.key4 like ?3)";
                customerList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType(), "%" + searchCriteria.getValue() + "%");
            } else {
                query = "select cus from Customer cus where cus.agentId =?1 and cus.statusCode in ?2";
                customerList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType());
            }
        }

        final List<Customer> customers = new ArrayList<Customer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object customerObj : customerList) {
                customers.add((Customer) customerObj);
            }
        }
        return customers;
    } 

}
