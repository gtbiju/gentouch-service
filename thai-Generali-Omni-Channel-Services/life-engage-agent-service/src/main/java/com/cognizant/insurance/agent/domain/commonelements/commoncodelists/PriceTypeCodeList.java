/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of asset prices according to their price type.
 * 
 * @author 301350
 * 
 * 
 */
public enum PriceTypeCodeList {
    /**
     * Price at which you can buy the asset.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Bid,
    /**
     * Price at which you can sell the asset when withdrawn.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Cancellation,
    /**
     * Price at which you can buy the asset when launched (Initial Public
     * Offering).
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Creation,
    /**
     * Average price between the Bid and the Offer. This could be used for
     * transactions such as a fund switch.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Mid,
    /**
     * Price at which you can sell the asset.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Offer
}
