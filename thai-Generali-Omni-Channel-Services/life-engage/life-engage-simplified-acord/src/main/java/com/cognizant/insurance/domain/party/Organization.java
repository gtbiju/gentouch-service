/**
 * 
 */
package com.cognizant.insurance.domain.party;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.domain.party.partycodelists.OrganizationStatusCodeList;
import com.cognizant.insurance.domain.party.partydetailsubtypes.OrganizationDetail;
import com.cognizant.insurance.domain.party.partyname.OrganizationName;

/**
 * A business concern or a group of individuals that are systematically bound by
 * a common purpose, with or without a legal status. Organizations may be legal
 * entities in their own right.
 * 
 * Various types of organizations include and are not limited to: - government
 * (e.g. federal, state, regional, local, and various agencies) - commercial
 * organizations (limited companies, publicly quoted multinationals,
 * subsidiaries, etc.) - organizational units (e.g. branches, departments,
 * teams, etc.) - informal groups (e.g. clubs, societies, charities, interest
 * groups, etc.) - other similar entities
 * 
 * Note: Various details, such as name, legal status, nature of operations, are
 * not included within organization because organizations may have multiple
 * names, operations and legal details. These concepts are addressed via
 * associations to related classes (OrganizationName, OrganizationDetail). <!--
 * end-UML-doc -->
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Organization extends Party {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 866253048554042438L;

    /**
     * The date on which the organization was founded or created.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date foundationDate;

    /**
     * The date at which this organization ceases to legally exist.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date dissolutionDate;

    /**
     * The count of parties being members of this organization. This count often
     * is an approximate number, because the number of members may in general
     * change over time.
     * 
     * 
     * 
     */
    private BigDecimal memberCount;

    /**
     * The current status of the organization within the life cycle model.
     * 
     * 
     * 
     */
    @Enumerated
    private OrganizationStatusCodeList statusCode;

    /**
     * The date on which the organization's latest status became effective.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date statusDate;

    /**
     * The additional information to explain the latest status of the
     * organization.
     * 
     * e.g: Risky investment
     * 
     * 
     * 
     */
    private String statusReason;

    /**
     * Accounting period of the organization.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch=FetchType.LAZY)
    private TimePeriod accountingPeriod;

    /**
     * This relationship links an organization to its details (if any).
     * 
     * e.g. legal status, nature of operations This represents details (if any)
     * of an organization (e.g. legal status, nature of operations, etc.). <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    private Set<OrganizationDetail> detail;

    /**
     * This relationship links an organization to its name(s). This represents
     * the organization's name.
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    private Set<OrganizationName> name;

    /**
     * Gets the foundationDate.
     * 
     * @return Returns the foundationDate.
     */
    public Date getFoundationDate() {
        return foundationDate;
    }

    /**
     * Gets the dissolutionDate.
     * 
     * @return Returns the dissolutionDate.
     */
    public Date getDissolutionDate() {
        return dissolutionDate;
    }

    /**
     * Gets the memberCount.
     * 
     * @return Returns the memberCount.
     */
    public BigDecimal getMemberCount() {
        return memberCount;
    }

    /**
     * Gets the statusCode.
     * 
     * @return Returns the statusCode.
     */
    public OrganizationStatusCodeList getStatusCode() {
        return statusCode;
    }

    /**
     * Gets the statusDate.
     * 
     * @return Returns the statusDate.
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * Gets the statusReason.
     * 
     * @return Returns the statusReason.
     */
    public String getStatusReason() {
        return statusReason;
    }

    /**
     * Gets the accountingPeriod.
     * 
     * @return Returns the accountingPeriod.
     */
    public TimePeriod getAccountingPeriod() {
        return accountingPeriod;
    }

    /**
     * Gets the detail.
     * 
     * @return Returns the detail.
     */
    public Set<OrganizationDetail> getDetail() {
        return detail;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public Set<OrganizationName> getName() {
        return name;
    }

    /**
     * Sets The foundationDate.
     * 
     * @param foundationDate
     *            The foundationDate to set.
     */
    public void setFoundationDate(final Date foundationDate) {
        this.foundationDate = foundationDate;
    }

    /**
     * Sets The dissolutionDate.
     * 
     * @param dissolutionDate
     *            The dissolutionDate to set.
     */
    public void setDissolutionDate(final Date dissolutionDate) {
        this.dissolutionDate = dissolutionDate;
    }

    /**
     * Sets The memberCount.
     * 
     * @param memberCount
     *            The memberCount to set.
     */
    public void setMemberCount(final BigDecimal memberCount) {
        this.memberCount = memberCount;
    }

    /**
     * Sets The statusCode.
     * 
     * @param statusCode
     *            The statusCode to set.
     */
    public void
            setStatusCode(final OrganizationStatusCodeList statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Sets The statusDate.
     * 
     * @param statusDate
     *            The statusDate to set.
     */
    public void setStatusDate(final Date statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * Sets The statusReason.
     * 
     * @param statusReason
     *            The statusReason to set.
     */
    public void setStatusReason(final String statusReason) {
        this.statusReason = statusReason;
    }

    /**
     * Sets The accountingPeriod.
     * 
     * @param accountingPeriod
     *            The accountingPeriod to set.
     */
    public void setAccountingPeriod(final TimePeriod accountingPeriod) {
        this.accountingPeriod = accountingPeriod;
    }

    /**
     * Sets The detail.
     * 
     * @param detail
     *            The detail to set.
     */
    public void setDetail(final Set<OrganizationDetail> detail) {
        this.detail = detail;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final Set<OrganizationName> name) {
        this.name = name;
    }

}
