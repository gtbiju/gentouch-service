/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.domain.documentandcommunication.communicationcontentspecificationsubtypes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;

import com.cognizant.insurance.domain.commonelements.commoncodelists.QuestionnaireTypeCodeList;

/**
 * A kind of document template that can be sent out to a party with a number of asked questions that allow a limited set
 * of predefined answers or free text answers. In a marketing context, this is also known as a survey.
 * 
 * e.g: Appointment feedback questionnaire.
 * 
 * e.g: Car insurance questionnaire.
 * 
 * e.g: Customer satisfaction survey.
 * 
 * 
 * 
 * 
 * @author 300797
 * 
 * 
 */

@Entity
@DiscriminatorValue("Questionnaire")
public class Questionnaire extends CommunicationContentTemplate {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8640091902003778533L;

    /** The question id. */
    private Long questionId;

    /** The user selected option. */
    private Integer selectedOption;

    /** The detailed info. */
    private String detailedInfo;

    /** The unit. */
    private String unit;

    /** The Questionnaire type. */
    @Enumerated
    private QuestionnaireTypeCodeList questionnaireType;

    /**
     * Gets the question id.
     * 
     * @return the questionId
     */
    public Long getQuestionId() {
        return questionId;
    }

    /**
     * Sets the question id.
     * 
     * @param questionId
     *            the questionId to set
     */
    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    /**
     * Gets the selected option.
     * 
     * @return the selectedOption
     */
    public Integer getSelectedOption() {
        return selectedOption;
    }

    /**
     * Sets the selected option.
     * 
     * @param selectedOption
     *            the selectedOption to set
     */
    public void setSelectedOption(Integer selectedOption) {
        this.selectedOption = selectedOption;
    }

    /**
     * Gets the detailed info.
     * 
     * @return the detailedInfo
     */
    public String getDetailedInfo() {
        return detailedInfo;
    }

    /**
     * Sets the detailed info.
     * 
     * @param detailedInfo
     *            the detailedInfo to set
     */
    public void setDetailedInfo(String detailedInfo) {
        this.detailedInfo = detailedInfo;
    }

    /**
     * Gets the unit.
     * 
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the unit.
     * 
     * @param unit
     *            the unit to set
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Gets the questionnaire type.
     * 
     * @return the questionnaireType
     */
    public QuestionnaireTypeCodeList getQuestionnaireType() {
        return questionnaireType;
    }

    /**
     * Sets the questionnaire type.
     * 
     * @param questionnaireType
     *            the questionnaireType to set
     */
    public void setQuestionnaireType(QuestionnaireTypeCodeList questionnaireType) {
        this.questionnaireType = questionnaireType;
    }
}
