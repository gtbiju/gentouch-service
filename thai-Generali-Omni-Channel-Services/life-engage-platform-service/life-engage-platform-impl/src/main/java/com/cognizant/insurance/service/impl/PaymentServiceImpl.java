/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.PaymentService;
/* Added for security_optimizations */
import org.springframework.beans.factory.annotation.Value;
import com.cognizant.insurance.core.exception.InputValidationException;
import com.cognizant.insurance.service.helper.LifeEngageValidationHelper;
/* Added for security_optimizations */

/**
 * The Class class PaymentServiceImpl.
 * 
 * @author 300797
 */
@Service("paymentService")
public class PaymentServiceImpl implements PaymentService {

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";
    
    /* Added for security_optimizations */  
    @Value("${le.platform.security.validationPattern}")
    private String validationPattern;
    /* Added for security_optimizations */

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#save(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String makePayment(final String json) throws BusinessException {

        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(json);
            /* Added for security_optimizations */
            LifeEngageValidationHelper.validateInputString(json.toString(),validationPattern);
            /* Added for security_optimizations */
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonTransactionArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);

            if (jsonTransactionArray.length() > 0) {
                for (int i = 0; i < jsonTransactionArray.length(); i++) {
                    final JSONObject jsonObj = jsonTransactionArray.getJSONObject(i);
                    jsonObj.getJSONObject(TRANSACTION_DATA);

                    // @ToDo - Connect to PaymentGateway, Setting the status of the message
                }
            }

        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
        } catch (InputValidationException e) {
        	JSONObject jsonObjectRs = new JSONObject();
        	JSONObject jsonResponse= new JSONObject();
         	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
         	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
         	jsonResponse.put("statusMessage", e.getMessage());                    	
         	jsonObjectRs.put("StatusData", jsonResponse);	
         	return jsonObjectRs.toString();
		}

        return "Success";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.PaymentService#getPaymentOptions(java.lang.String)
     */
    @Override
    public final String getPaymentOptions(final String json) throws BusinessException {
        // @ToDo - Getting the payment Options
        return null;
    }

}
