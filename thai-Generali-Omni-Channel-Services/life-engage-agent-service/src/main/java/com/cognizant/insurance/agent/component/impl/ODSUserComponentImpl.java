package com.cognizant.insurance.agent.component.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.agent.component.ODSUserComponent;
import com.cognizant.insurance.agent.dao.ODSUserDao;
import com.cognizant.insurance.agent.dao.impl.ODSUserDaoImpl;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.generali.security.entity.PasswordHistory;

@Component
public class ODSUserComponentImpl implements ODSUserComponent {

	@Override
	public Response<GeneraliAgent> validateAgentProfile(Request<Transactions> request) {
		ODSUserDao dao=new ODSUserDaoImpl();
		return dao.validateAgentProfile(request);
	}
	
	@Override
    public void resetPassword(Request<GeneraliTHUser> request) {
		ODSUserDao dao = new ODSUserDaoImpl();
        dao.merge(request);
    }
	
	@Override
    public void saveLoginDetails(Request<GeneraliTHUser> request) {
		ODSUserDao dao = new ODSUserDaoImpl();
        dao.save(request);
    }
	
	@Override
    public void saveHistory(Request<PasswordHistory> request) {
		ODSUserDao dao = new ODSUserDaoImpl();
        dao.save(request);
    }
	
	@Override
    public void removeOldHistory(Request<PasswordHistory> request) {
		ODSUserDao dao = new ODSUserDaoImpl();
        dao.remove(request);
    }
	
	@Override
	public Response<GeneraliTHUser> validateLoggedInUser(Request<Transactions> request) {
		ODSUserDao dao=new ODSUserDaoImpl();
		return dao.validateLoggedInUser(request);
	}
	
	@Override
	public Response<List<PasswordHistory>> getRecentPasswords(Request<String> userRequest, Request<Integer> historyRequest) {
		ODSUserDao dao = new ODSUserDaoImpl();
		return dao.getRecentPasswords(userRequest, historyRequest);
	}
	
	@Override
	public Response<PasswordHistory> getOldPasswordHistory(Request<String> userRequest) {
		ODSUserDao dao = new ODSUserDaoImpl();
		return dao.getOldPasswordHistory(userRequest);
	}
}
