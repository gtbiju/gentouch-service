package com.cognizant.insurance.agent.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.service.CodeLookUpService;


/**
 * The Class class CodeLookUpServiceEndPoint.
 */
@Controller
@RequestMapping("/codeLookUpService")
public class CodeLookUpServiceEndPoint {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(CodeLookUpServiceEndPoint.class);

    /** The code look up service. */
    @Autowired
    private CodeLookUpService codeLookUpService;

    /**
     * Gets the look up data.
     * 
     * @param json
     *            the json
     * @return the look up data
     */
    @RequestMapping(value = "/getLookUpData", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String getLookUpData(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return codeLookUpService.getLookUpData(json);
            }
        }.execute(json);
    }

}
