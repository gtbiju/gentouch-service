package com.cognizant.insurance.omnichannel.dao;

import com.cognizant.insurance.omnichannel.domain.TinyUrlGenerator;
import com.cognizant.insurance.request.Request;

public interface GeneraliPaymentDao{
	
	Long retrieveIdForPaymentUrl(Request<TinyUrlGenerator> searchCriteriatRequest);
	
	TinyUrlGenerator retrieveOriginalUrlForPayment(Request<TinyUrlGenerator> searchCriteriatRequest);

}
