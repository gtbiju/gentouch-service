/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;

/**
 * This concept is the generalizing concept of all types of roles.
 * 
 * e.g. Jane Doe is a person who, within the context of insurance, may play the role of "insured" and "driver" on a
 * policy.
 * 
 * @author 301350
 * 
 * 
 */
@MappedSuperclass
public abstract class Role extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 609123057490759250L;

    /**
     * Validity period of the role.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch=FetchType.LAZY)
    private TimePeriod effectivePeriod;

    /**
     * Free text describing the role.
     * 
     * 
     * 
     */
    private String description;

    /**
     * This is the time period from when the role was created (i.e. start date) until it was terminated (i.e. end date).
     * This period is not intended to represent when the party or physical object is actively playing the role.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch=FetchType.LAZY)
    private TimePeriod rolePeriod;

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the effectivePeriod.
     * 
     * @return Returns the effectivePeriod.
     */
    public TimePeriod getEffectivePeriod() {
        return effectivePeriod;
    }

    /**
     * Gets the playerRole.
     * 
     * @return Returns the playerRole.
     */

    /**
     * Gets the rolePeriod.
     * 
     * @return Returns the rolePeriod.
     */
    public TimePeriod getRolePeriod() {
        return rolePeriod;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The effectivePeriod.
     * 
     * @param effectivePeriod
     *            The effectivePeriod to set.
     */
    public void setEffectivePeriod(final TimePeriod effectivePeriod) {
        this.effectivePeriod = effectivePeriod;
    }

    /**
     * Sets The playerRole.
     * 
     * @param playerRole
     *            The playerRole to set.
     */

    /**
     * Sets The rolePeriod.
     * 
     * @param rolePeriod
     *            The rolePeriod to set.
     */
    public void setRolePeriod(final TimePeriod rolePeriod) {
        this.rolePeriod = rolePeriod;
    }

}
