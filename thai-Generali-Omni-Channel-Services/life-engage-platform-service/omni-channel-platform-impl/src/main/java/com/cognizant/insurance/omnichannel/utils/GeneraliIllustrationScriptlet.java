/**
 * 
 */
package com.cognizant.insurance.omnichannel.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;

import net.sf.jasperreports.engine.JRScriptletException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.audit.IllustrationResponsePayLoad;
import com.cognizant.insurance.domain.agreement.Coverage;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.commoncodelists.PaymentFrequencyCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;
import com.cognizant.insurance.domain.party.partyname.PersonName;
import com.cognizant.insurance.domain.party.persondetailsubtypes.OccupationDetail;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.utils.IllustrationScriptlet;


/**
 * @author 471501
 * 
 *         The Class GeneraliIllustrationScriptlet.
 */
/**
 * @author 471501
 * 
 */
public class GeneraliIllustrationScriptlet extends IllustrationScriptlet {
    /** The json object. */
    private JSONObject jsonObject;

    /** The agreement. */
    private InsuranceAgreement agreement;

    /** The agreementId. */
    private String agreementId;

    /** The payer. */
    private PremiumPayer payer;

    /** The insured. */
    private Insured insured;

    /** The payer person. */
    private Person payerPerson;

    /** The insured person. */
    private Person insuredPerson;
    
    /** The Generali Agent*/
    private GeneraliAgent generaliAgent;
    
    /** The appointee agreement Documents. */
    private AgreementDocument agreementDocument;

    /** The agreementProducer agent. */
    private AgreementProducer agent;

    /** The first additional insured. */
    Insured firstAddInsured;

    /** The second additional insured. */
    Insured secondAddInsured;

    /** The third additional insured. */
    Insured thirdAddInsured;

    /** The fourth additional insured. */
    Insured fourthAddInsured;

    List<Insured> addInsured = null;

    public static String pdfReqInput;

    private JSONObject jsonObjectPdfInput;

    private Set<Insured> additionalInsureds;

    private Person additionalInsPerson;

    public String languageSelected = "vn";

    public String EN = "en";
    public String VN = "vn";
    
    public String annual="6001";
    public String semiAnnual="6002";
    public String quarterly="6003";
    public String monthly="6004";
    public String male="Male";
    public String female="Female";
    
    public boolean flag=true;

	/** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(GeneraliIllustrationScriptlet.class);

    public void beforeReportInit() throws JRScriptletException {

        if (agreement == null) {
            agreement = (InsuranceAgreement) this.getFieldValue("proposal");
        }
        agreementId = agreement.getIdentifier().toString();
        
        generaliAgent=(GeneraliAgent)this.getFieldValue("generaligent");

        payer = (PremiumPayer) this.getFieldValue("payer");
        insured = (Insured) this.getFieldValue("insured");

        /*additionalInsureds = (Set<Insured>)this.getFieldValue("additionalInsuredes");

        for (Insured additionalInsured : additionalInsureds) {
            int count = 0;
            if (additionalInsured.getPartyId() != null
                    && additionalInsured.getPartyId().equalsIgnoreCase("1")) {
                firstAddInsured = additionalInsured;
            } else if (additionalInsured.getPartyId() != null
                    && additionalInsured.getPartyId().equalsIgnoreCase("2")) {
                secondAddInsured = additionalInsured;
            } else if (additionalInsured.getPartyId() != null
                    && additionalInsured.getPartyId().equalsIgnoreCase("3")) {
                thirdAddInsured = additionalInsured;
            } else if (additionalInsured.getPartyId() != null
                    && additionalInsured.getPartyId().equalsIgnoreCase("4")) {
                fourthAddInsured = additionalInsured;
            }
            count++;
        }
        if (additionalInsureds != null) {
            addInsured = new ArrayList<Insured>();
            addInsured.add(firstAddInsured);
            addInsured.add(secondAddInsured);
            addInsured.add(thirdAddInsured);
            addInsured.add(fourthAddInsured);
        }*/

        agreementDocument = (AgreementDocument) this.getFieldValue("document");
        if (null != agreementDocument) {
            appionteeDocuments = agreementDocument.getPages();
        }

        payerPerson = (Person) payer.getPlayerParty();
        insuredPerson = (Person) insured.getPlayerParty();

        Set<IllustrationResponsePayLoad> respSet = agreement.getIllustrationResponse();
        String json = null;
    
        for (IllustrationResponsePayLoad illustrationResponsePayLoad : respSet) {
            json = illustrationResponsePayLoad.getResponseJson();
        }

      /*  try {
            if (pdfReqInput == null)
                pdfReqInput = GeneraliPDFContentReader
                        .getPDFContent("pdfTemplates/PDFResourceBI.json");
        } catch (Exception e) {
            LOGGER.error("Out of beforeDetailEval pdfReqInput...."
                    + e.getMessage());
        }*/

        JSONParser jsonParser = new JSONParser();
        try {
            Object obj = jsonParser.parse(json);
            jsonObject = (JSONObject) obj;
            /*Object obj1 = jsonParser.parse(pdfReqInput);
            jsonObjectPdfInput = (JSONObject) obj1;*/
        } catch (ParseException e) {
            LOGGER.error("Out of beforeDetailEval " + e.getMessage() + " "
                    + agreementId);
        } catch (NullPointerException e) {
            LOGGER.error("Out of beforeDetailEval " + e.getMessage() + " "
                    + agreementId);
        }
    }

    /**
     * Gets the Additional insured names.
     * 
     * @return the Additional insured names
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public List<String> getAdditionalInsuredNames(String language)
            throws JRScriptletException {
        String name = null;
        String lname = null;
        List<String> additionalInsDataList = new ArrayList<String>();
        try {
            for (Insured additionalInsured : addInsured) {
                additionalInsPerson = (Person) additionalInsured
                        .getPlayerParty();

                for (PersonName pName : additionalInsPerson.getName()) {
                    name = pName.getGivenName();
                    lname = pName.getSurname();
                    name = createFullName(name, lname, language);
                }
                additionalInsDataList.add(name);
            }

        } catch (Exception e) {
            LOGGER.error("getAdditionalInsuredNames .... " + e.getMessage()
                    + " " + agreementId);
        }
        return additionalInsDataList;
    }

    /**
     * Gets the first additional insured name.
     * 
     * @return the first additional insured name
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getFirstAdditionalInsuredName(String language)
            throws JRScriptletException {
        String name = null;
        try {
            name = getAdditionalInsuredNames(language).get(0);
        } catch (Exception e) {
            LOGGER.error("getFirstAdditionalInsuredName .... " + e.getMessage()
                    + " " + agreementId);
        }
        return name;
    }

    /**
     * Gets the second additional insured name.
     * 
     * @return the second additional insured name
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getSecondAdditionalInsuredName(String language)
            throws JRScriptletException {
        String name = null;
        try {
            name = getAdditionalInsuredNames(language).get(1);
        } catch (Exception e) {
            LOGGER.error("getSecondAdditionalInsuredName .... "
                    + e.getMessage() + " " + agreementId);
        }
        return name;
    }

    /**
     * Gets the third additional insured name.
     * 
     * @return the third additional insured name
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getThirdAdditionalInsuredName(String language)
            throws JRScriptletException {
        String name = null;
        try {
            name = getAdditionalInsuredNames(language).get(2);
        } catch (Exception e) {
            LOGGER.error("getThirdAdditionalInsuredName .... " + e.getMessage()
                    + " " + agreementId);
        }
        return name;
    }

    /**
     * Gets the fourth additional insured name.
     * 
     * @return the fourth additional insured name
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getFourthAdditionalInsuredName(String language)
            throws JRScriptletException {
        String name = null;
        try {
            name = getAdditionalInsuredNames(language).get(3);
        } catch (Exception e) {
            LOGGER.error("getFourthAdditionalInsuredName .... "
                    + e.getMessage() + " " + agreementId);
        }
        return name;
    }

    /**
     * Gets the ph name.
     * 
     * @return the PH name
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPHName(String language) throws JRScriptletException {
        String name = null;
        String lname = null;
        try {
            for (PersonName pName : payerPerson.getName()) {
                name = pName.getGivenName();
                lname = pName.getSurname();
                name = createFullName(name, lname, language);
            }
        } catch (Exception e) {
            LOGGER.error("getPHName .... " + e.getMessage() + " " + agreementId);
        }
        return name;
    }
    
    
        
    /**
     * Gets the ph age.
     * 
     * @return the PH age
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPHAge() throws JRScriptletException {
        Integer age = null;
        try {
            Calendar dob = Calendar.getInstance();
            dob.setTime(payerPerson.getBirthDate());
            Calendar today = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            formatter.format(payerPerson.getBirthDate());
            age = payerPerson.getAge();
        } catch (Exception e) {
            LOGGER.error("getPHAge .... " + e.getMessage() + " " + agreementId);
        }
        return age.toString();
    }

    /**
     * Gets the ph sex.
     * 
     * @return the PH sex
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPHSex(String language) throws JRScriptletException {
        String gender = " ";
        String genderCode = " ";
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                .get(language);
        try {
            genderCode = payerPerson.getGenderCode().toString();
            if (productJsonObject.containsKey(genderCode)) {
                gender = productJsonObject.get(genderCode).toString();
            }

        } catch (Exception e) {
            LOGGER.error("getPHSex .... " + e.getMessage() + " " + agreementId);
        }
        return gender;
    }

    /**
     * Gets the ph dob.
     * 
     * @return the PH dob.
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPHDob() throws JRScriptletException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateOfBirth = null;
        try {
            Calendar dob = Calendar.getInstance();
            dob.setTime(payerPerson.getBirthDate());
            dateOfBirth = formatter.format(payerPerson.getBirthDate());
        } catch (Exception e) {
            LOGGER.error("getINDob .... " + e.getMessage() + " " + agreementId);
        }
        return dateOfBirth;
    }

    /**
     * Gets the in jobClass.
     * 
     * @return the PH jobClass
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPHjobClass() throws JRScriptletException {
        String jobClass = "";
        try {
            if (jsonObject.containsKey("PayorDetails")) {
                jobClass = checkForValue(getValue(jsonObject, "PayorDetails"),
                        "occupationClass");
            }
        } catch (Exception e) {
            LOGGER.error("getPHjobClass .... " + e.getMessage() + " "
                    + agreementId);
        }
        return jobClass;
    }

    /**
     * Gets the in name.
     * 
     * @return the IN name
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getINName(String language) throws JRScriptletException {
        String name = null;
        String lname = null;
        try {
            for (PersonName pName : insuredPerson.getName()) {
                name = pName.getGivenName();
                lname = pName.getSurname();
                name = createFullName(name, lname, language);
            }
        } catch (Exception e) {
            LOGGER.error("getINName .... " + e.getMessage() + " " + agreementId);
        }
        return name;
    }

    /**
     * Gets the in age.
     * 
     * @return the IN age
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getINAge() throws JRScriptletException {
        Integer age = null;
        try {
            Calendar dob = Calendar.getInstance();
            dob.setTime(insuredPerson.getBirthDate());
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            formatter.format(insuredPerson.getBirthDate());
            age = insuredPerson.getAge();
        } catch (Exception e) {
            LOGGER.error("getINAge .... " + e.getMessage() + " " + agreementId);
        }
        return age.toString();
    }

    /**
     * Gets the in sex.
     * 
     * @return the IN sex
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getINSex(String language) throws JRScriptletException {
        String gender = " ";
        String genderCode = " ";
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                .get(language);
        try {
            genderCode = insuredPerson.getGenderCode().toString();
            if (productJsonObject.containsKey(genderCode)) {
                gender = productJsonObject.get(genderCode).toString();
            }

        } catch (Exception e) {
            LOGGER.error("getINSex.... " + e.getMessage() + " " + agreementId);
        }
        return gender;
    }

    /**
     * Gets the in dob.
     * 
     * @return the IN dob
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getINDob() throws JRScriptletException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateOfBirth = null;
        try {
            Calendar dob = Calendar.getInstance();
            dob.setTime(insuredPerson.getBirthDate());
            dateOfBirth = formatter.format(insuredPerson.getBirthDate());
        } catch (Exception e) {
            LOGGER.error("getINDob .... " + e.getMessage() + " " + agreementId);
        }
        return dateOfBirth;
    }

    /**
     * Gets the in jobClass.
     * 
     * @return the IN jobClass
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getINjobClass() throws JRScriptletException {
        String jobClass = "";
        try {
            if (jsonObject.containsKey("InsuredDetails")) {
                jobClass = checkForValue(
                        getValue(jsonObject, "InsuredDetails"),
                        "occupationClass");
            }
        } catch (Exception e) {
            LOGGER.error("getINjobClass .... " + e.getMessage() + " "
                    + agreementId);
        }
        return jobClass;
    }

    /**
     * Gets the Additional insured data.
     * 
     * @return the Additional insured data
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public List<AdditionalInsuredDataGli> getAdditionalInsData(String language)
            throws JRScriptletException {
        String name = null;
        String lname = null;
        String age = "";
        String gender = "";
        String genderCode = "";
        String dob = "";
        String jobClass = "";

        List<AdditionalInsuredDataGli> additionalInsDataList = new ArrayList<AdditionalInsuredDataGli>();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        try {
            JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                    .get(language);
            for (Insured additionalInsured : addInsured) {
                AdditionalInsuredDataGli additionalTable = new AdditionalInsuredDataGli();
                additionalInsPerson = (Person) additionalInsured
                        .getPlayerParty();

                for (PersonName pName : additionalInsPerson.getName()) {
                    name = pName.getGivenName();
                    lname = pName.getSurname();
                    name = createFullName(name, lname, language);
                }
                additionalTable.setName(name);

                age = additionalInsPerson.getAge().toString();
                additionalTable.setAge(age);

                genderCode = additionalInsPerson.getGenderCode().toString();
                if (productJsonObject.containsKey(genderCode)) {
                    gender = productJsonObject.get(genderCode).toString();
                }
                additionalTable.setGender(gender);

                dob = formatter.format(additionalInsPerson.getBirthDate());
                additionalTable.setDob(dob);

                for (PersonDetail pDetail : additionalInsPerson.getDetail()) {
                    if (pDetail instanceof OccupationDetail) {
                        jobClass = ((OccupationDetail) pDetail)
                                .getOccupationClass();
                    }
                }
                additionalTable.setOccupationClass(jobClass);

                additionalInsDataList.add(additionalTable);
            }
        } catch (Exception e) {
            LOGGER.error("getAdditionalInsData .... " + e.getMessage() + " "
                    + agreementId);
        }
        return additionalInsDataList;
    }

    /**
     * Gets the basic product name.
     * 
     * @return the basic product name
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getProductName() throws JRScriptletException {
        String productName = "";
        JSONArray jsonArray = null;
        try {
            if (jsonObject.containsKey("policyDetailsTbl")) {
                jsonArray = (JSONArray) jsonObject.get("policyDetailsTbl");
                for (Object obj1 : jsonArray) {
                    JSONObject jsonObj1 = (JSONObject) obj1;
                    if (jsonObj1.containsKey("productName")) {
                        productName = checkForValue(jsonObj1, "productName");
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getProductName .... " + e.getMessage() + " "
                    + agreementId);
        }
        return productName;
    }

    /**
     * Gets the sum assured.
     * 
     * @return the sum assured in policy details table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPDSumAssured(String language) throws JRScriptletException {
        String sumAssured = "";
        JSONArray jsonArray = null;
        try {
            if (jsonObject.containsKey("policyDetailsTbl")) {
                jsonArray = (JSONArray) jsonObject.get("policyDetailsTbl");
                for (Object obj1 : jsonArray) {
                    JSONObject jsonObj1 = (JSONObject) obj1;
                    if (jsonObj1.containsKey("sumAssured")) {
                        sumAssured = formatter(
                                checkForValue(jsonObj1, "sumAssured"), language);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getPDSumAssured .... " + e.getMessage() + " "
                    + agreementId);
        }
        return sumAssured;
    }

    /**
     * Gets the premium and policy term.
     * 
     * @return the premium and policy term in "premiumTerm/policyTerm" format
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPremiumAndPolicyTerm() throws JRScriptletException {
        String term = "";
        JSONArray jsonArray = null;
        try {
            if (jsonObject.containsKey("policyDetailsTbl")) {
                jsonArray = (JSONArray) jsonObject.get("policyDetailsTbl");
                for (Object obj1 : jsonArray) {
                    JSONObject jsonObj1 = (JSONObject) obj1;
                    if (jsonObj1.containsKey("pptpolicyYr")) {
                        term = checkForValue(jsonObj1, "pptpolicyYr");
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getPremiumAndPolicyTerm .... " + e.getMessage() + " "
                    + agreementId);
        }
        return term;
    }

    /**
     * Gets the premium term.
     * 
     * @return the premium term
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPremiumTerm() throws JRScriptletException {
        String premiumTerm = "";
        try {
            String term = getPremiumAndPolicyTerm();
            StringTokenizer st = new StringTokenizer(term, "/");
            premiumTerm = st.nextToken();
        } catch (Exception e) {
            LOGGER.error("getPremiumTerm .... " + e.getMessage() + " "
                    + agreementId);
        }
        return premiumTerm;
    }

    /**
     * Gets the yearly premium.
     * 
     * @return the yearly premium in policy details table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getYearlyPremium(String language) throws JRScriptletException {
        String yearlyPremium = "";
        JSONArray jsonArray = null;
        try {
            if (jsonObject.containsKey("policyDetailsTbl")) {
                jsonArray = (JSONArray) jsonObject.get("policyDetailsTbl");
                for (Object obj1 : jsonArray) {
                    JSONObject jsonObj1 = (JSONObject) obj1;
                    if (jsonObj1.containsKey("yearlyPremium")) {
                        yearlyPremium = formatter(
                                checkForValue(jsonObj1, "yearlyPremium"),
                                language);
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getYearlyPremium .... " + e.getMessage() + " "
                    + agreementId);
        }
        return yearlyPremium;
    }

    /**
     * Gets the large size discount.
     * 
     * @return the large size discount in policy details table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getLargeSizeDiscount(String language)
            throws JRScriptletException {
        String largeSizeDiscount = "";
        try {
            largeSizeDiscount = formatter(
                    checkForValue(jsonObject, "largeSizeDiscount"), language);
        } catch (Exception e) {
            LOGGER.error("getLargeSizeDiscount .... " + e.getMessage() + " "
                    + agreementId);
        }
        return largeSizeDiscount;
    }

    /**
     * Gets the premium after discount.
     * 
     * @return the premium after discount in policy details table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPremiumAfterDiscount(String language)
            throws JRScriptletException {
        String premiumAfterDiscount = "";
        try {
            premiumAfterDiscount = formatter(
                    checkForValue(jsonObject, "premiumAfterDiscount"), language);
        } catch (Exception e) {
            LOGGER.error("getPremiumAfterDiscount .... " + e.getMessage() + " "
                    + agreementId);
        }
        return premiumAfterDiscount;
    }

    /**
     * Gets the rider table details.
     * 
     * @return the rider table details
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public List<RiderTableData> getRiderTable(String language) {
        JSONArray jsonArray = null;

        List<RiderTableData> riderData = new ArrayList<RiderTableData>();
        RiderTableData riderTable = new RiderTableData();
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput.get(language);

        String riderName = null;
        String lifeAssured = null;
        String sumAssured = null;
        String term = null;
        String yearlyPremium = null;
        String totalPremium = null;
        String yearlyPremiumTotal = null;
        String halfYearlyPremiumTotal = null;
        String quarterlyPremiumTotal = "-";
        String ppt = null;
        String policyYr = "";
        Boolean riderSubHeading = false;
        String riderNameValue = "";

        try {
            if (jsonObject != null) {

                jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
                for (Object obj1 : jsonArray) {
                    riderSubHeading = false;
                    JSONObject jsonObj1 = (JSONObject) obj1;
                    RiderTableData riderDetails = new RiderTableData();
                    String riderNameInput = "";
                    if (jsonObj1 != null) {
                       if (jsonObj1.containsKey("productName")) {
                            riderNameInput = checkForValue(jsonObj1, "productName");
                        }
                        riderName = riderNameInput.replaceAll("[^a-zA-Z]+", "").replaceAll("\\s", "").trim();
                        if ("OutPatient".equals(riderName) || ("Dental").equals(riderName)) {
                            riderSubHeading = true;

                        }
                        riderDetails.setRiderSingle(riderSubHeading);

                        if (riderName.equals("FinancialSupport")) {
                            if (getProductCode().equals("1")) {
                                riderNameValue = checkForValue(productJsonObject, "FinancialSupportHead");
                            } else if (getProductCode().equals("9")) {
                                riderNameValue = checkForValue(productJsonObject, "FinancialSupportRiderHead");
                            }
                        } else {
                            riderNameValue = checkForValue(productJsonObject, riderName + "Head");
                        }
                        if (!GeneraliConstants.JSON_EMPTY.equals(riderNameValue)) {
                            riderDetails.setRiderName(riderNameValue);
                        } else {
                            riderDetails.setRiderName(riderNameInput);
                        }

                        if (jsonObj1.containsKey("LifeAssured")) {
                            lifeAssured = checkForValue(jsonObj1, "LifeAssured");
                            riderDetails.setLifeAssured(lifeAssured);
                        }

                        if (jsonObj1.containsKey("sumAssured")) {
                            sumAssured = checkForValue(jsonObj1, "sumAssured");
                            if (!"0".equals(sumAssured) && !(" ").equals(sumAssured)
                                    && !GeneraliConstants.JSON_EMPTY.equals(sumAssured)) {
                                riderDetails.setSumAssured(formatter(sumAssured, language));
                            } else {
                                riderDetails.setSumAssured("-");
                            }
                        }

                        if (jsonObj1.containsKey("pptpolicyYr")) {
                            term = checkForValue(jsonObj1, "pptpolicyYr");
                            riderDetails.setPremiumAndPolicyTerm(term);
                        }

                        // Added for VITA
                        if (jsonObj1.containsKey("ppt")) {
                            ppt = checkForValue(jsonObj1, "ppt");
                            riderDetails.setPpt(ppt);
                        }
                        if (jsonObj1.containsKey("policyYrActual")) {
                            policyYr = checkForValue(jsonObj1, "policyYrActual");
                        } else if (jsonObj1.containsKey("policyYr")) {
                            policyYr = checkForValue(jsonObj1, "policyYr");
                        }
                        if (!GeneraliConstants.JSON_EMPTY.equals(policyYr)) {
                            String plicyYearKey = policyYr.replaceAll("[^a-zA-Z]+", "").replaceAll("\\s", "").trim();
                            String policyYearVlaue = checkForValue(productJsonObject, plicyYearKey);
                            if (!GeneraliConstants.JSON_EMPTY.equals(policyYearVlaue)) {
                                policyYr = policyYearVlaue;
                            }
                            riderDetails.setPolicyYr(policyYr);
                        }

                        if (jsonObj1.containsKey("yearlyPremium")) {
                            yearlyPremium = checkForValue(jsonObj1, "yearlyPremium");

                            riderDetails.setYearlyPremium(formatter(yearlyPremium, language));
                        }
                    }
                    riderData.add(riderDetails);
                }
                totalPremium = formatter(checkForValue(jsonObject, "totalPremium"), language);
                riderTable.setTotalPremium(totalPremium);
                yearlyPremiumTotal = formatter(checkForValue(jsonObject, "yearlyPremium"), language);
                riderTable.setTotalPremiumYearly(yearlyPremiumTotal);
                halfYearlyPremiumTotal = formatter(checkForValue(jsonObject, "halfYearlyPremium"), language);
                riderTable.setTotalPremiumHalfYearly(halfYearlyPremiumTotal);
                if (!getHeading1()) {
                    quarterlyPremiumTotal = formatter(checkForValue(jsonObject, "quarterlyPremium"), language);
                    if ("0".equals(quarterlyPremiumTotal) || (" ").equals(quarterlyPremiumTotal)
                            || GeneraliConstants.JSON_EMPTY.equals(quarterlyPremiumTotal)) {
                        quarterlyPremiumTotal = "-";
                    }
                } else {
                    quarterlyPremiumTotal = "-";
                }
                riderTable.setTotalPremiumQuaterly(quarterlyPremiumTotal);

                riderData.add(riderTable);
            }

        } catch (Exception e) {
            LOGGER.error("getRiderTable .... " + e.getMessage() + " " + agreementId);
        }
        return riderData;
    }

    /**
     * Gets the death benefit before 10 years.
     * 
     * @return the death benefit before 10 years
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getDeathBenefitBeforeTenYears(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "dbBefore10Yrs");
        } catch (Exception e) {
            LOGGER.error("getDeathBenefitBeforeTenYears .... " + e.getMessage()
                    + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the death benefit after 10 years.
     * 
     * @return the death benefit after 10 years
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getDeathBenefitAfterTenYears(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "dbAfter10Yrs");
        } catch (Exception e) {
            LOGGER.error("getDeathBenefitAfterTenYears .... " + e.getMessage()
                    + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the TPD benefit before 10 years.
     * 
     * @return the TPD benefit before 10 years
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getTPDBenefitBeforeTenYears(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "tpdBefore10Yrs");
        } catch (Exception e) {
            LOGGER.error("getTPDBenefitBeforeTenYears .... " + e.getMessage()
                    + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the TPD benefit after 10 years.
     * 
     * @return the TPD benefit after 10 years
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getTPDBenefitAfterTenYears(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "tpdAfter10Yrs");
        } catch (Exception e) {
            LOGGER.error("getTPDBenefitAfterTenYears .... " + e.getMessage()
                    + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the CI benefit before 10 years.
     * 
     * @return the CI benefit before 10 years
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCIBenefitBeforeTenYears(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "ciBefore10Yrs");
        } catch (Exception e) {
            LOGGER.error("getCIBenefitBeforeTenYears .... " + e.getMessage()
                    + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the CI benefit after 10 years.
     * 
     * @return the CI benefit after 10 years
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCIBenefitAfterTenYears(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "ciAfter10Yrs");
        } catch (Exception e) {
            LOGGER.error("getCIBenefitAfterTenYears .... " + e.getMessage()
                    + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the additional gender specific benefit.
     * 
     * @return the additional gender specific benefit
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getAdditionalGenderSpecificBenefit(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "addtionalGenderSpecBen");
        } catch (Exception e) {
            LOGGER.error("getAdditionalGenderSpecificBenefit .... "
                    + e.getMessage() + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the coronary diseases benefit.
     * 
     * @return the coronary diseases benefit
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCoronaryDiseasesBenefit(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "coronaryDisBenefit");
        } catch (Exception e) {
            LOGGER.error("getCoronaryDiseasesBenefit .... " + e.getMessage()
                    + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the daily hospital cash.
     * 
     * @return the daily hospital cash
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getDailyHospitalCash(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "dlyHosCash");
            benefit = formatter(benefit, language);
        } catch (Exception e) {
            LOGGER.error("getDailyHospitalCash .... " + e.getMessage() + " "
                    + agreementId);
        }
        /*
         * if (!benefit.equals("") && benefit != null) { benefit =
         * formatter(benefit).concat("  75 days max"); }
         */
        return benefit;
    }

    /**
     * Gets the additional daily hospital cash.
     * 
     * @return the additional daily hospital cash
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getAdditionalDailyHospitalCash(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "dlyHosCashFrom6Days");
            benefit = formatter(benefit, language);
        } catch (Exception e) {
            LOGGER.error("getAdditionalDailyHospitalCash .... "
                    + e.getMessage() + " " + agreementId);
        }
        /*
         * if (!benefit.equals("") && benefit != null) { benefit =
         * formatter(benefit).concat("  50 days max"); }
         */
        return benefit;
    }

    /**
     * Gets the ICU hospital cash.
     * 
     * @return the ICU hospital cash
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getICUHospitalCash(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "icuHosCash");
            benefit = formatter(benefit, language);
        } catch (Exception e) {
            LOGGER.error("getICUHospitalCash .... " + e.getMessage() + " "
                    + agreementId);
        }
        /*
         * if (!benefit.equals("") && benefit != null) { benefit =
         * formatter(benefit).concat("  30 days max"); }
         */
        return benefit;
    }

    /**
     * Gets the surgery hospital cash.
     * 
     * @return the ICU hospital cash
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getSurgeyHospitalCash(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "surgeryHosCash");
            // benefit=formatter(benefit);
        } catch (Exception e) {
            LOGGER.error("getSurgeyHospitalCash .... " + e.getMessage() + " "
                    + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the out-patient per visit hospital cash.
     * 
     * @return the out-patient per visit hospital cash
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getOutPatientHospitalCash(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "optHosCash");
            benefit = formatter(benefit, language);
        } catch (Exception e) {
            LOGGER.error("getOutPatientHospitalCash .... " + e.getMessage()
                    + " " + agreementId);
        }
        /*
         * if (!benefit.equals("") && benefit != null) { benefit =
         * formatter(benefit).concat("  max 2 visits/year"); }
         */
        return benefit;
    }

    /**
     * Gets the total anticipated cash benefit every 3 years.
     * 
     * @return the total anticipated cash benefit every 3 years
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getTotalAnticipatedCashBenefit(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "totalAnticipatedCashBen");
        } catch (Exception e) {
            LOGGER.error("getTotalAnticipatedCashBenefit .... "
                    + e.getMessage() + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the guaranteed maturity benefit.
     * 
     * @return the guaranteed maturity benefit
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getGuaranteedMaturityBenefit(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "guaranteedMaturityBen");
        } catch (Exception e) {
            LOGGER.error("getGuaranteedMaturityBenefit .... " + e.getMessage()
                    + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    /**
     * Gets the Total benefits at maturity if not receiving Accumulated Cash
     * Benefit before (interest rate 8%).
     * 
     * @return the total benefits at maturity if not receiving Accumulated Cash
     *         Benefit before (interest rate 8%)
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getTotalBenefitsAtMaturity(String language)
            throws JRScriptletException {
        String benefit = "";
        try {
            benefit = checkForValue(jsonObject, "totalBenAtMaturity");

        } catch (Exception e) {
            LOGGER.error("getTotalBenefitsAtMaturity .... " + e.getMessage()
                    + " " + agreementId);
        }
        return formatter(benefit, language);
    }

    
   

    private BigInteger checkForBigIntValue(JSONObject obj, String key) {
        BigInteger value = null;
        if (obj.containsKey(key)) {
            Object jsonValue = obj.get(key);
            if (jsonValue instanceof Double) {
                value = ((BigInteger) jsonValue);
            } else if (jsonValue instanceof String) {
                value = new BigInteger((String) jsonValue);
            } else {
                value = (BigInteger) jsonValue;
            }
        }
        return value;

    }

    /**
     * Gets the benefit illustration for rider table details.
     * 
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public List<RiderTableData> getBenefitIllusRiderSingle(String language,
            String lifeAssured) {
        JSONArray jsonArray = null;
        List<RiderTableData> riderData = new ArrayList<RiderTableData>();
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                .get(language);
        jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");

        String riderName = "";
        String sumAssured = "";

        try {
            for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                RiderTableData riderTable = new RiderTableData();
                if (jsonObj != null) {
                    if (jsonObj.containsKey("productName")
                            && jsonObj.containsKey("laNum")
                            && checkForValue(jsonObj, "laNum").equals(
                                    lifeAssured)) {
                        riderName = checkForValue(jsonObj, "productName");

                        if (riderName.equals("Level Term Rider")) {
                            riderTable.setRiderName(productJsonObject.get(
                                    "LTRiderHead").toString());
                            if (productJsonObject.containsKey("LTRiderText")) {
                                riderTable.setDescription(productJsonObject
                                        .get("LTRiderText").toString());
                                if (jsonObj.containsKey("sumAssured")) {
                                    sumAssured = checkForValue(jsonObj,
                                            "sumAssured");
                                }
                                riderTable.setSumAssured(formatter(sumAssured,
                                        language));
                            }
                            riderData.add(riderTable);
                        } else if (riderName
                                .equals("Critical Illness Rider (*)")) {
                            riderTable.setRiderName(productJsonObject.get(
                                    "CIRiderHead").toString());
                            if (productJsonObject.containsKey("CIRiderText")) {
                                riderTable.setDescription(productJsonObject
                                        .get("CIRiderText").toString());
                                if (jsonObj.containsKey("sumAssured")) {
                                    sumAssured = checkForValue(jsonObj,
                                            "sumAssured");
                                }
                                riderTable.setSumAssured(formatter(sumAssured,
                                        language));
                            }
                            riderData.add(riderTable);
                        } else if (riderName
                                .equals("Critical Illness Waiver Rider (*)")) {
                            riderTable.setRiderName(productJsonObject.get(
                                    "CIWRiderHead").toString());
                            if (productJsonObject.containsKey("CIWRiderText")) {
                                riderTable.setDescription(productJsonObject
                                        .get("CIWRiderText").toString());
                                if (jsonObj.containsKey("sumAssured")) {
                                    sumAssured = checkForValue(jsonObj,
                                            "sumAssured");
                                }
                                riderTable.setSumAssured(formatter(sumAssured,
                                        language));
                            }
                            riderData.add(riderTable);
                        } else if (riderName.equals("Financial Support")) {
                            riderTable.setRiderName(productJsonObject.get(
                                    "FinancialSupportRiderHead").toString());
                            if (productJsonObject
                                    .containsKey("FinancialSupportRiderText")) {
                                riderTable.setDescription(productJsonObject
                                        .get("FinancialSupportRiderText")
                                        .toString());
                                if (jsonObj.containsKey("sumAssured")) {
                                    sumAssured = checkForValue(jsonObj,
                                            "sumAssured");
                                }
                                riderTable.setSumAssured(formatter(sumAssured,
                                        language));
                            }
                            riderData.add(riderTable);
                        } else if (riderName.equals("Waiver of Premium")) {
                            riderTable.setRiderName(productJsonObject.get(
                                    "WPRiderHead").toString());
                            if (productJsonObject.containsKey("WPRiderText")) {
                                riderTable.setDescription(productJsonObject
                                        .get("WPRiderText").toString());
                                if (jsonObj.containsKey("sumAssured")) {
                                    sumAssured = checkForValue(jsonObj,
                                            "sumAssured");
                                }
                                riderTable.setSumAssured(formatter(sumAssured,
                                        language));
                            }
                            riderData.add(riderTable);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getBenefitIllusRiderSingle .... " + e.getMessage()
                    + " " + agreementId);
        }
        return riderData;
    }

    public List<RiderTableData> getBenefitIllusRiderTable(String language,
            String lifeAssured) {
        JSONArray jsonArray = null;
        List<RiderTableData> riderData = new ArrayList<RiderTableData>();
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                .get(language);
        jsonArray = (JSONArray) jsonObject.get("pdfRiderTbl");
        boolean riderHeading = false;
        String riderNameValue = "";
        try {
            for (Object obj : jsonArray) {
                RiderTableData riderTable = new RiderTableData();
                riderHeading = false;
                String riderName = "";
                JSONObject jsonObj = (JSONObject) obj;
                if (jsonObj != null) {
                    if (lifeAssured.equals(checkForValue(jsonObj, "partyName"))) {
                        riderTable
                                .setSumAssured(formatter(
                                        checkForValue(jsonObj, "sumAssured"),
                                        language));
                        String riderNameInput = checkForValue(jsonObj,
                                "productName");
                        riderName = riderNameInput.replaceAll("[^a-zA-Z]+", "")
                                .replaceAll("\\s", "").trim();
                        if (riderName.equals("FinancialSupport")|| riderName.equals("FinancialSupportRider") ) {
                            if (getProductCode().equals("1")) {
                                riderNameValue = checkForValue(productJsonObject, "FinancialSupportHeadBI");
                            } else if (getProductCode().equals("9")) {
                                riderNameValue = checkForValue(productJsonObject, "FinancialSupportRiderHeadBI");
                            }
                        } 
                        else if (riderName.equals("WaiverofPremium")|| riderName.equals("WaiverofPremiumRider") ) {
                            if (getProductCode().equals("1")) {
                                riderNameValue = checkForValue(productJsonObject, "WaiverofPremiumHeadBI");
                            } else if (getProductCode().equals("9")) {
                                riderNameValue = checkForValue(productJsonObject, "WaiverofPremiumRiderHeadBI");
                            }
                        } 

                        else{
                        	  riderNameValue = checkForValue(productJsonObject, riderName+"HeadBI");
                        }
                        riderTable
                                .setSumAssured(formatter(
                                        checkForValue(jsonObj, "sumAssured"),
                                        language));
                        if (!GeneraliConstants.JSON_EMPTY
                                .equals(riderNameValue)) {
                            riderTable.setRiderName(riderNameValue);
                        } else {
                            riderTable.setRiderName(riderNameInput);
                        }
                        riderTable.setDescription(checkForValue(
                                productJsonObject,
                                checkForValue(jsonObj, "riderCode")));
                        if (!GeneraliConstants.JSON_EMPTY.equals(checkForValue(
                                jsonObj, "productName"))) {
                            riderHeading = true;
                        }
                        riderTable.setRiderSingle(riderHeading);
                        riderData.add(riderTable);
                    }
                }

            }
        } catch (Exception e) {
            LOGGER.error("getBenefitIllusRiderTable .... " + e.getMessage()
                    + " " + agreementId);
        }
        return riderData;

    }

    public boolean isBenefitIllustrationNeeded() {
        boolean flag = false;
        JSONArray jsonArray = null;
        jsonArray = (JSONArray) jsonObject.get("pdfRiderTbl");
        if (jsonArray.size() > 0) {
            flag = true;
        }
        return flag;

    }

    public boolean isBenefitIllustrationInsuredNeeded(String insured) {
        boolean flag = false;
        JSONArray jsonArray = null;
        jsonArray = (JSONArray) jsonObject.get("pdfRiderTbl");
        for (Object obj : jsonArray) {
            JSONObject jsonObj = (JSONObject) obj;
            if (jsonObj != null) {
                if (insured.equals(checkForValue(jsonObj, "partyName"))) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;

    }

    /**
     * Gets the benefit illustration for rider table details.
     * 
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public List<RiderBenefitIllustrationTable> getBenefitIllusRiderMultiple(
            String language, String lifeAssured) {
        JSONArray jsonArray = null;
        List<RiderBenefitIllustrationTable> illusData = new ArrayList<RiderBenefitIllustrationTable>();
        jsonArray = (JSONArray) jsonObject.get("pdfRiderTbl");
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                .get(language);

        String riderName = "";
        String sumAssured = "";
        Long firstSumAssured = null;
        Long secondSumAssured = null;
        Long thirdSumAssured = null;
        Long fourthSumAssured = null;

        try {
            for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                RiderBenefitIllustrationTable illustrationTableData = new RiderBenefitIllustrationTable();
                if (jsonObj != null) {
                    if (jsonObj.containsKey("productName")
                            && jsonObj.containsKey("partyName")
                            && checkForValue(jsonObj, "partyName").equals(
                                    lifeAssured)) {
                        riderName = checkForValue(jsonObj, "productName");

                        if (riderName
                                .equals("Accidental Death & Dismemberment Rider (*)")) {
                            illustrationTableData
                                    .setFirstRiderName(productJsonObject.get(
                                            "ADDRiderHead").toString());
                            illustrationTableData.setSecondRiderName("");
                            illustrationTableData.setThirdRiderName("");

                            if (productJsonObject.containsKey("ADDRiderText1")) {
                                illustrationTableData
                                        .setFirstDescription(productJsonObject
                                                .get("ADDRiderText1")
                                                .toString());
                            }
                            if (productJsonObject.containsKey("ADDRiderText2")) {
                                illustrationTableData
                                        .setSecondDescription(productJsonObject
                                                .get("ADDRiderText2")
                                                .toString());
                            }
                            if (productJsonObject.containsKey("ADDRiderText3")) {
                                illustrationTableData
                                        .setThirdDescription(productJsonObject
                                                .get("ADDRiderText3")
                                                .toString());
                            }
                            if (jsonObj.containsKey("sumAssured")) {
                                sumAssured = checkForValue(jsonObj,
                                        "sumAssured");
                                firstSumAssured = Long.parseLong(sumAssured);
                                secondSumAssured = Long.parseLong(sumAssured) * 2;
                                thirdSumAssured = Long.parseLong(sumAssured) * 3;
                            }
                            illustrationTableData.setFirstSumAssured(formatter(
                                    String.valueOf(firstSumAssured), language));
                            illustrationTableData
                                    .setSecondSumAssured(formatter(
                                            String.valueOf(secondSumAssured),
                                            language));
                            illustrationTableData.setThirdSumAssured(formatter(
                                    String.valueOf(thirdSumAssured), language));

                            illusData.add(illustrationTableData);
                        } else if (riderName.equals("Hospital Cash Rider (*)")) {
                            illustrationTableData
                                    .setFirstRiderName(productJsonObject.get(
                                            "HCRiderHead").toString());
                            illustrationTableData.setSecondRiderName("");
                            illustrationTableData.setThirdRiderName("");
                            illustrationTableData.setFourthRiderName("");

                            if (productJsonObject.containsKey("HCRiderText1")) {
                                illustrationTableData
                                        .setFirstDescription(productJsonObject
                                                .get("HCRiderText1").toString());
                            }
                            if (productJsonObject.containsKey("HCRiderText2")) {
                                illustrationTableData
                                        .setSecondDescription(productJsonObject
                                                .get("HCRiderText2").toString());
                            }
                            if (productJsonObject.containsKey("HCRiderText3")) {
                                illustrationTableData
                                        .setThirdDescription(productJsonObject
                                                .get("HCRiderText3").toString());
                            }
                            if (productJsonObject.containsKey("HCRiderText4")) {
                                illustrationTableData
                                        .setFourthDescription(productJsonObject
                                                .get("HCRiderText4").toString());
                            }
                            if (jsonObj.containsKey("sumAssured")) {
                                sumAssured = checkForValue(jsonObj,
                                        "sumAssured");
                                firstSumAssured = Long.parseLong(sumAssured);
                                secondSumAssured = Long.parseLong(sumAssured) * 2;
                                thirdSumAssured = Long.parseLong(sumAssured) * 3;
                                fourthSumAssured = (long) (Long
                                        .parseLong(sumAssured) * 0.5);
                            }
                            illustrationTableData.setFirstSumAssured(formatter(
                                    String.valueOf(firstSumAssured), language));
                            illustrationTableData
                                    .setSecondSumAssured(formatter(
                                            String.valueOf(secondSumAssured),
                                            language));
                            illustrationTableData.setThirdSumAssured(formatter(
                                    String.valueOf(thirdSumAssured), language));
                            illustrationTableData
                                    .setFourthSumAssured(formatter(
                                            String.valueOf(fourthSumAssured),
                                            language));
                            illusData.add(illustrationTableData);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getBenefitIllusRiderMultiple .... " + e.getMessage()
                    + " " + agreementId);
        }
        return illusData;
    }

    /**
     * Gets the guaranteed benefit in guaranteed rate table.
     * 
     * @return the guaranteed benefit in guaranteed rate table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getGRGuaranteedBenefit(String language)
            throws JRScriptletException {
        String guaranteedBenefit = "";
        try {
            if (jsonObject.containsKey("guaranteedRateTbl")) {
                guaranteedBenefit = checkForValue(
                        getValue(jsonObject, "guaranteedRateTbl"),
                        "guaranteedBenefit");

                if (guaranteedBenefit == null || guaranteedBenefit.equals("")
                        || "0".equals(guaranteedBenefit)) {
                    guaranteedBenefit = "-";

                } else {
                    BigDecimal bd = new BigDecimal(guaranteedBenefit);
                    guaranteedBenefit = formatter(bd.toString(), language);
                }

            }
        } catch (Exception e) {
            LOGGER.error("getGRGuaranteedBenefit .... " + e.getMessage() + " "
                    + agreementId);
        }
        return guaranteedBenefit;
    }

    /**
     * Gets the anticipated cash benefit in guaranteed rate table.
     * 
     * @return the anticipated cash benefit in guaranteed rate table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getGRAnticipatedCashBenefit(String language)
            throws JRScriptletException {
        String anticipatedCashBenefit = "";
        try {
            if (jsonObject.containsKey("guaranteedRateTbl")) {

                anticipatedCashBenefit = checkForValue(
                        getValue(jsonObject, "guaranteedRateTbl"),
                        "anticipatedCashBenefit");

                if (anticipatedCashBenefit == null
                        || anticipatedCashBenefit.equals("")
                        || "0".equals(anticipatedCashBenefit)) {
                    anticipatedCashBenefit = "-";

                } else {
                    BigDecimal bd = new BigDecimal(anticipatedCashBenefit);
                    anticipatedCashBenefit = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getGRAnticipatedCashBenefit .... " + e.getMessage()
                    + " " + agreementId);
        }
        return anticipatedCashBenefit;
    }

    /**
     * Gets the accumulated interest for cash coupons in guaranteed rate table.
     * 
     * @return the accumulated interest for cash coupons in guaranteed rate
     *         table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getGRAccumulatedInterestForCashCoupons(String language)
            throws JRScriptletException {
        String accumulatedInterestForCashCoupons = "";
        try {
            if (jsonObject.containsKey("guaranteedRateTbl")) {

                accumulatedInterestForCashCoupons = checkForValue(
                        getValue(jsonObject, "guaranteedRateTbl"),
                        "accumulatedInterest4CashCoupons");

                if (accumulatedInterestForCashCoupons == null
                        || accumulatedInterestForCashCoupons.equals("")
                        || accumulatedInterestForCashCoupons.equals("0")) {
                    accumulatedInterestForCashCoupons = "-";

                } else {
                    BigDecimal bd = new BigDecimal(
                            accumulatedInterestForCashCoupons);
                    accumulatedInterestForCashCoupons = formatter(
                            bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getGRAccumulatedInterestForCashCoupons .... "
                    + e.getMessage() + " " + agreementId);
        }
        return accumulatedInterestForCashCoupons;
    }

    /**
     * Gets the loyalty addition in guaranteed rate table.
     * 
     * @return the loyalty addition in guaranteed rate table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getGRLoyaltyAddition(String language)
            throws JRScriptletException {
        String loyaltyAddition = "";
        try {
            if (jsonObject.containsKey("guaranteedRateTbl")) {
                loyaltyAddition = checkForValue(
                        getValue(jsonObject, "guaranteedRateTbl"),
                        "loyaltyAddition");

                if (loyaltyAddition == null || loyaltyAddition.equals("")
                        || "0".equals(loyaltyAddition)) {
                    loyaltyAddition = "-";

                } else {
                    BigDecimal bd = new BigDecimal(loyaltyAddition);
                    loyaltyAddition = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getGRLoyaltyAddition .... " + e.getMessage() + " "
                    + agreementId);
        }
        return loyaltyAddition;
    }

    /**
     * Gets the total benefits at maturity in guaranteed rate table.
     * 
     * @return the total benefits at maturity in guaranteed rate table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getGRTotalBenefitsAtMaturity(String language)
            throws JRScriptletException {
        String totalBenefitsAtMaturity = "";
        try {
            if (jsonObject.containsKey("guaranteedRateTbl")) {
                totalBenefitsAtMaturity = checkForValue(
                        getValue(jsonObject, "guaranteedRateTbl"),
                        "totalBenefitsAtMaturity");

                if (totalBenefitsAtMaturity == null
                        || totalBenefitsAtMaturity.equals("")
                        || "0".equals(totalBenefitsAtMaturity)) {
                    totalBenefitsAtMaturity = "-";

                } else {
                    BigDecimal bd = new BigDecimal(totalBenefitsAtMaturity);
                    totalBenefitsAtMaturity = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getGRTotalBenefitsAtMaturity .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalBenefitsAtMaturity;
    }

    /**
     * Gets the guaranteed benefit in cash coupons received table.
     * 
     * @return the guaranteed benefit in cash coupons received table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCRGuaranteedBenefit(String language)
            throws JRScriptletException {
        String guaranteedBenefit = "";
        try {
            if (jsonObject.containsKey("cashCouponsRecvd8Pct")) {
                guaranteedBenefit = checkForValue(
                        getValue(jsonObject, "cashCouponsRecvd8Pct"),
                        "guaranteedBenefit");

                if (guaranteedBenefit == null || guaranteedBenefit.equals("")
                        || "0".equals(guaranteedBenefit)) {
                    guaranteedBenefit = "-";

                } else {
                    BigDecimal bd = new BigDecimal(guaranteedBenefit);
                    guaranteedBenefit = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCRGuaranteedBenefit .... " + e.getMessage() + " "
                    + agreementId);
        }
        return guaranteedBenefit;
    }

    /**
     * Gets the anticipated cash benefit in cash coupons received table.
     * 
     * @return the anticipated cash benefit in cash coupons received table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCRAnticipatedCashBenefit(String language)
            throws JRScriptletException {
        String anticipatedCashBenefit = "";
        try {
            if (jsonObject.containsKey("cashCouponsRecvd8Pct")) {
                anticipatedCashBenefit = checkForValue(
                        getValue(jsonObject, "cashCouponsRecvd8Pct"),
                        "anticipatedCashBenefit");

                if (anticipatedCashBenefit == null
                        || anticipatedCashBenefit.equals("")
                        || "0".equals(anticipatedCashBenefit)) {
                    anticipatedCashBenefit = "-";

                } else {
                    BigDecimal bd = new BigDecimal(anticipatedCashBenefit);
                    anticipatedCashBenefit = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCRAnticipatedCashBenefit .... " + e.getMessage()
                    + " " + agreementId);
        }
        return anticipatedCashBenefit;
    }

    /**
     * Gets the accumulated interest for cash coupons in cash coupons received
     * table.
     * 
     * @return the accumulated interest for cash coupons in cash coupons
     *         received table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCRAccumulatedInterestForCashCoupons(String language)
            throws JRScriptletException {
        String accumulatedInterestForCashCoupons = "";
        try {
            if (jsonObject.containsKey("cashCouponsRecvd8Pct")) {
                accumulatedInterestForCashCoupons = checkForValue(
                        getValue(jsonObject, "cashCouponsRecvd8Pct"),
                        "accumulatedInterest4CashCoupons");

                if (accumulatedInterestForCashCoupons == null
                        || accumulatedInterestForCashCoupons.equals("")
                        || accumulatedInterestForCashCoupons.equals("0")) {
                    accumulatedInterestForCashCoupons = "-";

                } else {
                    BigDecimal bd = new BigDecimal(
                            accumulatedInterestForCashCoupons);
                    accumulatedInterestForCashCoupons = formatter(
                            bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCRAccumulatedInterestForCashCoupons .... "
                    + e.getMessage() + " " + agreementId);
        }
        return accumulatedInterestForCashCoupons;
    }

    /**
     * Gets the loyalty addition in cash coupons received table.
     * 
     * @return the loyalty addition in cash coupons received table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCRLoyaltyAddition(String language)
            throws JRScriptletException {
        String loyaltyAddition = "";
        try {
            if (jsonObject.containsKey("cashCouponsRecvd8Pct")) {
                loyaltyAddition = checkForValue(
                        getValue(jsonObject, "cashCouponsRecvd8Pct"),
                        "loyaltyAddition");

                if (loyaltyAddition == null || loyaltyAddition.equals("")
                        || "0".equals(loyaltyAddition)) {
                    loyaltyAddition = "-";

                } else {
                    BigDecimal bd = new BigDecimal(loyaltyAddition);
                    loyaltyAddition = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCRLoyaltyAddition .... " + e.getMessage() + " "
                    + agreementId);
        }
        return loyaltyAddition;
    }

    /**
     * Gets the total benefits at maturity in cash coupons received table.
     * 
     * @return the total benefits at maturity in cash coupons received table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCRTotalBenefitsAtMaturity(String language)
            throws JRScriptletException {
        String totalBenefitsAtMaturity = "";
        try {
            if (jsonObject.containsKey("cashCouponsRecvd8Pct")) {
                totalBenefitsAtMaturity = checkForValue(
                        getValue(jsonObject, "cashCouponsRecvd8Pct"),
                        "totalBenefitsAtMaturity");

                if (totalBenefitsAtMaturity == null
                        || totalBenefitsAtMaturity.equals("")
                        || "0".equals(totalBenefitsAtMaturity)) {
                    totalBenefitsAtMaturity = "-";

                } else {
                    BigDecimal bd = new BigDecimal(totalBenefitsAtMaturity);
                    totalBenefitsAtMaturity = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCRTotalBenefitsAtMaturity .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalBenefitsAtMaturity;
    }

    /**
     * Gets the guaranteed benefit in cash coupons accumulated table.
     * 
     * @return the guaranteed benefit in cash coupons accumulated table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCAGuaranteedBenefit(String language)
            throws JRScriptletException {
        String guaranteedBenefit = "";
        try {
            if (jsonObject.containsKey("cashCouponsAccumulated8Pct")) {
                Object obj = jsonObject.get("cashCouponsAccumulated8Pct");
                JSONObject jsonObj = (JSONObject) obj;
                guaranteedBenefit = checkForValue(jsonObj, "guaranteedBenefit");

                if (guaranteedBenefit == null || guaranteedBenefit.equals("")
                        || "0".equals(guaranteedBenefit)) {
                    guaranteedBenefit = "-";

                } else {
                    BigDecimal bd = new BigDecimal(guaranteedBenefit);
                    guaranteedBenefit = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCAGuaranteedBenefit .... " + e.getMessage() + " "
                    + agreementId);
        }
        return guaranteedBenefit;
    }

    /**
     * Gets the anticipated cash benefit in cash coupons accumulated table.
     * 
     * @return the anticipated cash benefit in cash coupons accumulated table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCAAnticipatedCashBenefit(String language)
            throws JRScriptletException {
        String anticipatedCashBenefit = "";
        try {
            if (jsonObject.containsKey("cashCouponsAccumulated8Pct")) {
                anticipatedCashBenefit = checkForValue(
                        getValue(jsonObject, "cashCouponsAccumulated8Pct"),
                        "anticipatedCashBenefit");

                if (anticipatedCashBenefit == null
                        || anticipatedCashBenefit.equals("")
                        || "0".equals(anticipatedCashBenefit)) {
                    anticipatedCashBenefit = "-";

                } else {
                    BigDecimal bd = new BigDecimal(anticipatedCashBenefit);
                    anticipatedCashBenefit = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCAAnticipatedCashBenefit .... " + e.getMessage()
                    + " " + agreementId);
        }
        return anticipatedCashBenefit;
    }

    /**
     * Gets the accumulated interest for cash coupons in cash coupons
     * accumulated table.
     * 
     * @return the accumulated interest for cash coupons in cash coupons
     *         accumulated table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCAAccumulatedInterestForCashCoupons(String language)
            throws JRScriptletException {
        String accumulatedInterestForCashCoupons = "";
        try {
            if (jsonObject.containsKey("cashCouponsAccumulated8Pct")) {
                accumulatedInterestForCashCoupons = checkForValue(
                        getValue(jsonObject, "cashCouponsAccumulated8Pct"),
                        "accumulatedInterest4CashCoupons");

                if (accumulatedInterestForCashCoupons == null
                        || accumulatedInterestForCashCoupons.equals("")
                        || "0".equals(accumulatedInterestForCashCoupons)) {
                    accumulatedInterestForCashCoupons = "-";

                } else {
                    BigDecimal bd = new BigDecimal(
                            accumulatedInterestForCashCoupons);
                    accumulatedInterestForCashCoupons = formatter(
                            bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCAAccumulatedInterestForCashCoupons .... "
                    + e.getMessage() + " " + agreementId);
        }
        return accumulatedInterestForCashCoupons;
    }

    /**
     * Gets the loyalty addition in cash coupons accumulated table.
     * 
     * @return the loyalty addition in cash coupons accumulated table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCALoyaltyAddition(String language)
            throws JRScriptletException {
        String loyaltyAddition = "";
        try {
            if (jsonObject.containsKey("cashCouponsAccumulated8Pct")) {
                loyaltyAddition = checkForValue(
                        getValue(jsonObject, "cashCouponsAccumulated8Pct"),
                        "loyaltyAddition");

                if (loyaltyAddition == null || loyaltyAddition.equals("")
                        || "0".equals(loyaltyAddition)) {
                    loyaltyAddition = "-";

                } else {
                    BigDecimal bd = new BigDecimal(loyaltyAddition);
                    loyaltyAddition = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCALoyaltyAddition .... " + e.getMessage() + " "
                    + agreementId);
        }
        return loyaltyAddition;
    }

    /**
     * Gets the total benefits at maturity in cash coupons accumulated table.
     * 
     * @return the total benefits at maturity in cash coupons accumulated table
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCCATotalBenefitsAtMaturity(String language)
            throws JRScriptletException {
        String totalBenefitsAtMaturity = "";
        try {
            if (jsonObject.containsKey("cashCouponsAccumulated8Pct")) {
                totalBenefitsAtMaturity = checkForValue(
                        getValue(jsonObject, "cashCouponsAccumulated8Pct"),
                        "totalBenefitsAtMaturity");

                if (totalBenefitsAtMaturity == null
                        || totalBenefitsAtMaturity.equals("")
                        || "0".equals(totalBenefitsAtMaturity)) {
                    totalBenefitsAtMaturity = "-";

                } else {
                    BigDecimal bd = new BigDecimal(totalBenefitsAtMaturity);
                    totalBenefitsAtMaturity = formatter(bd.toString(), language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getCCATotalBenefitsAtMaturity .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalBenefitsAtMaturity;
    }
    
    // New sections added for VITA

    //Page 1 
    //Line 1
    /**
     * Gets the comprehensive protection Fund.
     * 
     * @return comprehensive protection Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getCompProtectFund(String language)
            throws JRScriptletException {
        String compProtectFund = "";
        try {

            compProtectFund = getMoneyValues(jsonObject, "compProtectFund",
                    language);

        } catch (Exception e) {
            LOGGER.error("getCompProtectFund .... " + e.getMessage() + " "
                    + agreementId);
        }
        return compProtectFund;
    }
    //Line 2
    /**
     * Gets the cash payable Fund.
     * 
     * @return cash payable Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getcashBenPayableFund(String language)
            throws JRScriptletException {
        String cashBenPayableFund = "";
        try {

            cashBenPayableFund = getMoneyValues(jsonObject,
                    "cashBenPayableFund", language);
        } catch (Exception e) {
            LOGGER.error("getcashBenPayableFund .... " + e.getMessage() + " "
                    + agreementId);
        }
        return cashBenPayableFund;
    }
    
    //Line 3
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getoldAgeSplCashBenFund(String language)
            throws JRScriptletException {
        String oldAgeSplCashBenFund= "";
        try {
            oldAgeSplCashBenFund=getMoneyValues(jsonObject, "oldAgeSplCashBenFund",
                    language);

        } catch (Exception e) {
            LOGGER.error("getoldAgeSplCashBenFund .... "
                    + e.getMessage() + " " + agreementId);
        }
        return oldAgeSplCashBenFund;
    }
    
    //Line 4
    /**
     * Gets the family Legacy Fund.
     * 
     * @return family Legacy Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getfamilyLegacyFund(String language)
            throws JRScriptletException {
        String familyLegacyFund = "";
        try {
            familyLegacyFund = getMoneyValues(jsonObject, "familyLegacyFund",
                    language);

        } catch (Exception e) {
            LOGGER.error("getfamilyLegacyFund .... " + e.getMessage() + " "
                    + agreementId);
        }
        return familyLegacyFund;
    }

    //End Page 1
    
    //Page 2
    //POLICY INFORMATION
    //Policy Term (year)
    /**
     * Gets the Policy Term Year.
     * 
     * @return Policy Term Year
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPolicyTermYr() throws JRScriptletException {
        String policyTermYr = "";
        JSONArray jsonArray = null;
        try {
            if (jsonObject.containsKey("policyDetailsTbl")) {
                jsonArray = (JSONArray) jsonObject.get("policyDetailsTbl");
                for (Object obj1 : jsonArray) {
                    JSONObject jsonObj1 = (JSONObject) obj1;
                        policyTermYr = checkForValue(jsonObj1, "policyTermYr");
                }
            }
        } catch (Exception e) {
            LOGGER.error("getPolicyTermYr .... " + e.getMessage() + " "
                    + agreementId);
        }
        return policyTermYr;
    }
    
    //Premium Term (year)
    /**
     * Gets the Premium Term Year.
     * 
     * @return Premium Term Year
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPremiumTermYr() throws JRScriptletException {
        String premiumTermYr = "";
        JSONArray jsonArray = null;
        try {
            if (jsonObject.containsKey("policyDetailsTbl")) {
                jsonArray = (JSONArray) jsonObject.get("policyDetailsTbl");
                for (Object obj1 : jsonArray) {
                    JSONObject jsonObj1 = (JSONObject) obj1;
                    premiumTermYr = checkForValue(jsonObj1, "premiumTermYr");
                }
            }
        } catch (Exception e) {
            LOGGER.error("getPremiumTermYr .... " + e.getMessage() + " "
                    + agreementId);
        }
        return premiumTermYr;
    }
    // Premium Value
    /**
     * Gets the Chosen Premium Value.
     * 
     * @return Chosen Premium Value
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getChosenPremValue(String language)
            throws JRScriptletException {
        String chosenPremValue = "";
        JSONArray jsonArray = null;
        try {
            if (jsonObject.containsKey("policyDetailsTbl")) {
                jsonArray = (JSONArray) jsonObject.get("policyDetailsTbl");
                for (Object obj1 : jsonArray) {
                    JSONObject jsonObj1 = (JSONObject) obj1;
                    chosenPremValue = getMoneyValues(jsonObj1,"chosenPremValue",language);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getChosenPremValue .... " + e.getMessage() + " "
                    + agreementId);
        }
        return chosenPremValue;
    }
    //End of Policy information table- Basic section
    
    //Notes under Policy Information Table
    
    public List<IDDescData> getNotes(String language) {
        List<IDDescData> notesList = new ArrayList<IDDescData>();
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                .get(language);
        if (ifRiderExist("ADDRider") || ifRiderExist("HCRider")
                || ifRiderExist("CIRider") || ifRiderExist("CIWOPRider")) {
            IDDescData iDDescData1 = new IDDescData();
            iDDescData1.setId("(*)");
            iDDescData1.setDescription(checkForValue(productJsonObject,
                    "NoteOneStar"));
            notesList.add(iDDescData1);
        }  if (ifRiderExist("VGHRider")) {
            IDDescData iDDescData2 = new IDDescData();
            iDDescData2.setId("(**)");
            iDDescData2.setDescription(checkForValue(productJsonObject,
                    "NoteTwoStar"));
            notesList.add(iDDescData2);
        }  if (ifRiderExist("VGHRider")
                && (ifRiderExist("CIWOPRider") || ifRiderExist("WOPRider"))) {
            IDDescData iDDescData3 = new IDDescData();
            iDDescData3.setId("2.");
            iDDescData3.setDescription(checkForValue(productJsonObject,
                    "NoteTwo"));
            notesList.add(iDDescData3);
        }
        return notesList;

    }
    //End Page 2
    
    //Page 3
    //Value 1
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getFirstEarlyStageCI(String language)
            throws JRScriptletException {
        String firstEarlyStageCI= "";
        try {
            firstEarlyStageCI=getMoneyValues(jsonObject,"firstEarlyStageCI",language);

        } catch (Exception e) {
            LOGGER.error("getFirstEarlyStageCI .... "
                    + e.getMessage() + " " + agreementId);
        }
        return firstEarlyStageCI;
    }
    
    //Value 2
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getsecondEarlyStageCI(String language)
            throws JRScriptletException {
        String secondEarlyStageCI= "";
        try {
            secondEarlyStageCI=getMoneyValues(jsonObject,"secondEarlyStageCI",language);

        } catch (Exception e) {
            LOGGER.error("getsecondEarlyStageCI .... "
                    + e.getMessage() + " " + agreementId);
        }
        return secondEarlyStageCI;
    }
    
    //Value 3
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getdiabeticComplications(String language)
            throws JRScriptletException {
        String diabeticComplications= "";
        try {
            diabeticComplications=getMoneyValues(jsonObject,"diabeticComplications",language);

        } catch (Exception e) {
            LOGGER.error("getdiabeticComplications .... "
                    + e.getMessage() + " " + agreementId);
        }
        return diabeticComplications;
    }
    
    //Value 4
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getFirstLateStageCI(String language)
            throws JRScriptletException {
        String firstLateStageCI= "";
        try {
            firstLateStageCI=getMoneyValues(jsonObject,"firstLateStageCI",language);

        } catch (Exception e) {
            LOGGER.error("getFirstLateStageCI .... "
                    + e.getMessage() + " " + agreementId);
        }
        return firstLateStageCI;
    }
    
    //Value 5
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getSecondLateStageCI(String language)
            throws JRScriptletException {
        String secondLateStageCI= "";
        try {
            secondLateStageCI=getMoneyValues(jsonObject,"secondLateStageCI",language);

        } catch (Exception e) {
            LOGGER.error("getSecondLateStageCI .... "
                    + e.getMessage() + " " + agreementId);
        }
        return secondLateStageCI;
    }

    //Value 6
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getDbB475NotRecvLSCIBen(String language)
            throws JRScriptletException {
        String dbB475NotRecvLSCIBen= "";
        try {
            dbB475NotRecvLSCIBen=getMoneyValues(jsonObject,"dbB475NotRecvLSCIBen",language);

        } catch (Exception e) {
            LOGGER.error("getDbB475NotRecvLSCIBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return dbB475NotRecvLSCIBen;
    }
    
    //Value 7
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getDbB475RecvFLSCIBen(String language)
            throws JRScriptletException {
        String dbB475RecvFLSCIBen= "";
        try {
            dbB475RecvFLSCIBen=getMoneyValues(jsonObject,"dbB475RecvFLSCIBen",language);

        } catch (Exception e) {
            LOGGER.error("getDbB475RecvFLSCIBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return dbB475RecvFLSCIBen;
    }
    
    //Value 8
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getDrecvdCashBen(String language)
            throws JRScriptletException {
        String recvdCashBen= "";
        try {
            recvdCashBen=getMoneyValues(jsonObject,"recvdCashBen",language);

        } catch (Exception e) {
            LOGGER.error("getDrecvdCashBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return recvdCashBen;
    }
 
    //Value 9
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getAccumulatedCashBen(String language)
            throws JRScriptletException {
        String accumulatedCashBen= "";
        try {
            accumulatedCashBen=getMoneyValues(jsonObject,"accumulatedCashBen",language);

        } catch (Exception e) {
            LOGGER.error("getAccumulatedCashBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return accumulatedCashBen;
    }

    //Value 10
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getLoyaltyAddition7Pct(String language)
            throws JRScriptletException {
        String loyaltyAddition7Pct= "";
        try {
            loyaltyAddition7Pct=getMoneyValues(jsonObject,"loyaltyAddition7Pct",language);

        } catch (Exception e) {
            LOGGER.error("getLoyaltyAddition7Pct .... "
                    + e.getMessage() + " " + agreementId);
        }
        return loyaltyAddition7Pct;
    }
    
    //Value 11
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getSplCashBenAt75(String language)
            throws JRScriptletException {
        String splCashBenAt75= "";
        try {
            splCashBenAt75=getMoneyValues(jsonObject,"splCashBenAt75",language);

        } catch (Exception e) {
            LOGGER.error("getSplCashBenAt75 .... "
                    + e.getMessage() + " " + agreementId);
        }
        return splCashBenAt75;
    }
    
    //Value 12
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getDbFrom75(String language)
            throws JRScriptletException {
        String dbFrom75= "";
        try {
            dbFrom75=getMoneyValues(jsonObject,"dbFrom75",language);

        } catch (Exception e) {
            LOGGER.error("getDbFrom75 .... "
                    + e.getMessage() + " " + agreementId);
        }
        return dbFrom75;
    }


    //Value 13
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getJuvenileCancerBen(String language)
            throws JRScriptletException {
        String juvenileCancerBen= "";
        try {
            juvenileCancerBen=getMoneyValues(jsonObject,"juvenileCancerBen",language);

        } catch (Exception e) {
            LOGGER.error("getJuvenileCancerBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return juvenileCancerBen;
    }

    //End page 3
    
    //Page 5
    
    /**
     * Gets the benefit illustration table details 
     * 
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public List<BenefitIllustrationDataVITA> getBenefitIllusDataVITA(String position,
            String language) {
        JSONArray jsonArray = null;
        List<BenefitIllustrationDataVITA> illusData = new ArrayList<BenefitIllustrationDataVITA>();
        String benefitArrayTag="illustrationTable1Data";
        if("tail".equals(position)){
            benefitArrayTag="illustrationTable2Data";
        }
        jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);

        String annualizedPremium = "-";
        String accumulatedPremiums = "-";
        String guarEarlyStgCI = "-";
        String guarFirstLateStgCI = "-";
        String guarSecondLateStgCI = "-";
        String guarDBAreadyClaimed = "-";
        String guarDBNotClaimed = "-";
        String guarSurrenderValue = "-";
        String guarCashBenefit = "-";
        String accumulatedCashBenefit = "-";
        String accumLABonus4Pct = "-";
        String maxTotalInsuranceBenefit4Pct = "-";
        String surrenderValue4Pct = "-";
        String accumLABonus7Pct = "-";
        String maxTotalInsuranceBenefit7Pct = "-";
        String surrenderValue7Pct = "-";
        try {
            for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                BenefitIllustrationDataVITA illustrationTableData = new BenefitIllustrationDataVITA();
                annualizedPremium = "-";
                accumulatedPremiums = "-";
                guarEarlyStgCI = "-";
                guarFirstLateStgCI = "-";
                guarSecondLateStgCI = "-";
                guarDBAreadyClaimed = "-";
                guarDBNotClaimed = "-";
                guarSurrenderValue = "-";
                guarCashBenefit = "-";
                accumulatedCashBenefit = "-";
                accumLABonus4Pct = "-";
                maxTotalInsuranceBenefit4Pct = "-";
                surrenderValue4Pct = "-";
                accumLABonus7Pct = "-";
                maxTotalInsuranceBenefit7Pct = "-";
                surrenderValue7Pct = "-";
                BigInteger zero= new BigInteger("0");
                if (jsonObj != null) {
                    if (checkForValue(jsonObj, "policyYear").toString() != null
                            && checkForValue(jsonObj, "policyYear").toString() != ""
                                && checkForValue(jsonObj, "policyYear").toString() != "0") {
                        illustrationTableData
                                .setPolicyYear(checkForValue(jsonObj,
                                        "policyYear").toString());
                    }

                    if (checkForBigIntValue(jsonObj, "annualizedPremium") != null
                            && !zero.equals(checkForBigIntValue(jsonObj, "annualizedPremium"))) {
                        annualizedPremium = formatter(
                                checkForBigIntValue(jsonObj, "annualizedPremium")
                                        .toString(), language);
                    }
                    illustrationTableData.setAnnualizedPremium(annualizedPremium);

                    if (checkForBigIntValue(jsonObj, "accumulatedPremiums") != null
                            &&!zero.equals(checkForBigIntValue(jsonObj, "accumulatedPremiums"))) {
                        accumulatedPremiums = formatter(
                                checkForBigIntValue(jsonObj, "accumulatedPremiums")
                                        .toString(), language);
                    }
                    illustrationTableData.setAccumulatedPremiums(accumulatedPremiums);

                    if (checkForBigIntValue(jsonObj, "guarEarlyStgCI") != null
                            && !zero.equals(checkForBigIntValue(jsonObj, "guarEarlyStgCI"))) {
                        guarEarlyStgCI = formatter(
                                checkForBigIntValue(jsonObj, "guarEarlyStgCI")
                                        .toString(), language);
                    }
                    illustrationTableData.setGuarEarlyStgCI(guarEarlyStgCI);

                    if (checkForBigIntValue(jsonObj, "guarFirstLateStgCI") != null
                            && !zero.equals(checkForBigIntValue(jsonObj, "guarFirstLateStgCI"))) {
                        guarFirstLateStgCI = formatter(
                                checkForBigIntValue(jsonObj,
                                        "guarFirstLateStgCI").toString(),
                                language);
                    }
                    illustrationTableData
                            .setGuarFirstLateStgCI(guarFirstLateStgCI);

                    if (checkForBigIntValue(jsonObj, "guarSecondLateStgCI") != null
                            &&!zero.equals(checkForBigIntValue(jsonObj, "guarSecondLateStgCI")) ) {
                        guarSecondLateStgCI = formatter(
                                checkForBigIntValue(jsonObj,
                                        "guarSecondLateStgCI").toString(),
                                language);
                    }
                    illustrationTableData
                            .setGuarSecondLateStgCI(guarSecondLateStgCI);

                    if (checkForBigIntValue(jsonObj, "guarDBAreadyClaimed") != null
                            && !zero.equals(checkForBigIntValue(jsonObj, "guarDBAreadyClaimed"))) {
                        guarDBAreadyClaimed = formatter(
                                checkForBigIntValue(jsonObj,
                                        "guarDBAreadyClaimed").toString(),
                                language);
                    }
                    illustrationTableData.setGuarDBAreadyClaimed(guarDBAreadyClaimed);

                    if (checkForBigIntValue(jsonObj,
                            "guarDBNotClaimed") != null
                            &&!zero.equals(checkForBigIntValue(jsonObj, "guarDBNotClaimed"))) {
                        guarDBNotClaimed = formatter(
                                checkForBigIntValue(jsonObj,
                                        "guarDBNotClaimed")
                                        .toString(), language);
                    }
                    illustrationTableData
                            .setGuarDBNotClaimed(guarDBNotClaimed);

                    if (checkForBigIntValue(jsonObj, "guarSurrenderValue") != null
                            &&!zero.equals(checkForBigIntValue(jsonObj, "guarSurrenderValue"))) {
                        guarSurrenderValue = formatter(
                                checkForBigIntValue(jsonObj,
                                        "guarSurrenderValue").toString(),
                                language);
                    }
                    illustrationTableData
                            .setGuarSurrenderValue(guarSurrenderValue);

                    if (checkForBigIntValue(jsonObj, "guarCashBenefit") != null
                            &&!zero.equals(checkForBigIntValue(jsonObj, "guarCashBenefit"))) {
                        guarCashBenefit = formatter(
                                checkForBigIntValue(jsonObj, "guarCashBenefit")
                                        .toString(), language);
                    }
                    illustrationTableData.setGuarCashBenefit(guarCashBenefit);

                    if (checkForBigIntValue(jsonObj, "accumulatedCashBenefit") != null
                            && !zero.equals(checkForBigIntValue(jsonObj, "accumulatedCashBenefit"))) {
                        accumulatedCashBenefit = formatter(
                                checkForBigIntValue(jsonObj,
                                        "accumulatedCashBenefit").toString(),
                                language);
                    }
                    illustrationTableData.setAccumulatedCashBenefit(accumulatedCashBenefit);
                    
                    if (checkForBigIntValue(jsonObj, "accumLABonus4Pct") != null
                            && !zero.equals(checkForBigIntValue(jsonObj, "accumLABonus4Pct"))) {
                        accumLABonus4Pct = formatter(
                                checkForBigIntValue(jsonObj,
                                        "accumLABonus4Pct").toString(),
                                language);
                    }
                    illustrationTableData.setAccumLABonus4Pct(accumLABonus4Pct);
                    

                    if (checkForBigIntValue(jsonObj, "maxTotalInsuranceBenefit4Pct") != null
                            &&!zero.equals(checkForBigIntValue(jsonObj, "maxTotalInsuranceBenefit4Pct")) ) {
                        maxTotalInsuranceBenefit4Pct = formatter(
                                checkForBigIntValue(jsonObj,
                                        "maxTotalInsuranceBenefit4Pct").toString(),
                                language);
                    }
                    illustrationTableData
                            .setMaxTotalInsuranceBenefit4Pct(maxTotalInsuranceBenefit4Pct);

                    if (checkForBigIntValue(jsonObj, "surrenderValue4Pct") != null
                            && !zero.equals(checkForBigIntValue(jsonObj, "surrenderValue4Pct"))) {
                        surrenderValue4Pct = formatter(
                                checkForBigIntValue(jsonObj,
                                        "surrenderValue4Pct").toString(),
                                language);
                    }
                    illustrationTableData.setSurrenderValue4Pct(surrenderValue4Pct);

                    if (checkForBigIntValue(jsonObj,
                            "accumLABonus7Pct") != null
                            &&!zero.equals(checkForBigIntValue(jsonObj, "accumLABonus7Pct"))) {
                        accumLABonus7Pct = formatter(
                                checkForBigIntValue(jsonObj,
                                        "accumLABonus7Pct")
                                        .toString(), language);
                    }
                    illustrationTableData
                            .setAccumLABonus7Pct(accumLABonus7Pct);

                    if (checkForBigIntValue(jsonObj, "maxTotalInsuranceBenefit7Pct") != null
                            &&!zero.equals(checkForBigIntValue(jsonObj, "maxTotalInsuranceBenefit7Pct"))) {
                        maxTotalInsuranceBenefit7Pct = formatter(
                                checkForBigIntValue(jsonObj,
                                        "maxTotalInsuranceBenefit7Pct").toString(),
                                language);
                    }
                    illustrationTableData
                            .setMaxTotalInsuranceBenefit7Pct(maxTotalInsuranceBenefit7Pct);

                    if (checkForBigIntValue(jsonObj, "surrenderValue7Pct") != null
                            &&!zero.equals(checkForBigIntValue(jsonObj, "surrenderValue7Pct"))) {
                        surrenderValue7Pct = formatter(
                                checkForBigIntValue(jsonObj, "surrenderValue7Pct")
                                        .toString(), language);
                    }
                    illustrationTableData.setSurrenderValue7Pct(surrenderValue7Pct);
                }
                illusData.add(illustrationTableData);
            }
        } catch (Exception e) {
            LOGGER.error("getBenefitIllusData1VITA .... " + e.getMessage() + " "
                    + agreementId);
        }
                return illusData;

    }
    //End page 5
    
    //page 6
    // If not yet claim Late Stage CI Benefit
    //Guaranteed
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getGuarNoClaimLateStgCIBen(String language)
            throws JRScriptletException {
        String guarNoClaimLateStgCIBen= "";
        try {
            guarNoClaimLateStgCIBen=getMoneyValues(jsonObject,"guarNoClaimLateStgCIBen",language);

        } catch (Exception e) {
            LOGGER.error("getGuarNoClaimLateStgCIBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return guarNoClaimLateStgCIBen;
    }

    //Illustrated at 4%/year LA rate
    
    /**
     * guar Claim First Late Stg CI Ben.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getL4PctNoClaimLateStgCIBen(String language)
            throws JRScriptletException {
        String l4PctNoClaimLateStgCIBen= "";
        try {
            l4PctNoClaimLateStgCIBen=getMoneyValues(jsonObject,"l4PctNoClaimLateStgCIBen",language);

        } catch (Exception e) {
            LOGGER.error("getL4PctNoClaimLateStgCIBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return l4PctNoClaimLateStgCIBen;
    }
    
    // Illustrated at 7%/year LA rate
    
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getH7PctNoClaimLateStgCIBen(String language)
            throws JRScriptletException {
        String h7PctNoClaimLateStgCIBen= "";
        try {
            h7PctNoClaimLateStgCIBen=getMoneyValues(jsonObject,"h7PctNoClaimLateStgCIBen",language);

        } catch (Exception e) {
            LOGGER.error("getH7PctNoClaimLateStgCIBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return h7PctNoClaimLateStgCIBen;
    }
    
    // If already claim First Late Stage CI Benefit
    //Guaranteed
    /**
     * guar Claim First Late Stg CI Ben.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getGuarClaimFirstLateStgCIBen(String language)
            throws JRScriptletException {
        String guarClaimFirstLateStgCIBen= "";
        try {
            guarClaimFirstLateStgCIBen=getMoneyValues(jsonObject,"guarClaimFirstLateStgCIBen",language);

        } catch (Exception e) {
            LOGGER.error("getGuarClaimFirstLateStgCIBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return guarClaimFirstLateStgCIBen;
    }

    // Illustrated at 4%/year LA rate
    
    /**
     * Gets the old age special cash benefit Fund.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getL4ClaimFirstLateStgCIBen(String language)
            throws JRScriptletException {
        String l4ClaimFirstLateStgCIBen= "";
        try {
            l4ClaimFirstLateStgCIBen=getMoneyValues(jsonObject,"l4ClaimFirstLateStgCIBen",language);

        } catch (Exception e) {
            LOGGER.error("getL4ClaimFirstLateStgCIBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return l4ClaimFirstLateStgCIBen;
    }
    
    // Illustrated at 7%/year LA rate
    
    /**
     * guar Claim First Late Stg CI Ben.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getH7ClaimFirstLateStgCIBen(String language)
            throws JRScriptletException {
        String h7ClaimFirstLateStgCIBen= "";
        try {
            h7ClaimFirstLateStgCIBen=getMoneyValues(jsonObject,"h7ClaimFirstLateStgCIBen",language);

        } catch (Exception e) {
            LOGGER.error("getH7ClaimFirstLateStgCIBen .... "
                    + e.getMessage() + " " + agreementId);
        }
        return h7ClaimFirstLateStgCIBen;
    }

    //End page 6
    
    //Page 8
    
    //Plan
    public String getPlan(String language) {
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                .get(language);
        String planKey = checkForValue(jsonObject, "hrr11TblHeaderName");// Standard" or "Executive" or "Premier"
        String planValue = checkForValue(productJsonObject, planKey);

        return planValue;

    }
    
    //Plan SumAssured
    public String getHRRSAStandard(String language) {
        String hrr11TblChosenSA = "";
        try {
            hrr11TblChosenSA =getMoneyValues(jsonObject,"hrr11TblChosenSAStd",language);
        } catch (Exception e) {
            LOGGER.error("getHRRSAStandard .... "
                    + e.getMessage() + " " + agreementId);
        }
        return hrr11TblChosenSA;

    }
    public String getHRRSAExecutive(String language) {
        String hrr11TblChosenSA = "";
        try {
            hrr11TblChosenSA =getMoneyValues(jsonObject,"hrr11TblChosenSAExec",language);
        } catch (Exception e) {
            LOGGER.error("getHRRSAExecutive .... "
                    + e.getMessage() + " " + agreementId);
        }
        return hrr11TblChosenSA;

    }
    public String getHRRSAPremier(String language) {
        String hrr11TblChosenSA = "";
        try {
            hrr11TblChosenSA =getMoneyValues(jsonObject,"hrr11TblChosenSAPrem",language);
        } catch (Exception e) {
            LOGGER.error("getHRRSAPremier .... "
                    + e.getMessage() + " " + agreementId);
        }
        return hrr11TblChosenSA;

    }
    //Territory
    public String getTerritory(String language) {
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
        .get(language);
        String hrr11TerritoryDisplayKey = checkForValue(jsonObject, "hrr11TerritoryDisplay");
        String hrr11TerritoryDisplay = checkForValue(productJsonObject, hrr11TerritoryDisplayKey);
        return hrr11TerritoryDisplay;

    }
    
    public String getTerritoryVietnam(String language) {
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
        .get(language);
        String hrr11TerritoryDisplayKey = checkForValue(jsonObject, "Vietnam");
        String hrr11TerritoryDisplay = checkForValue(productJsonObject, hrr11TerritoryDisplayKey);
        return hrr11TerritoryDisplay;
    }
    
    public String getTerritoryAsia(String language) {
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
        .get(language);
        String hrr11TerritoryDisplayKey = checkForValue(jsonObject, "Asia");
        String hrr11TerritoryDisplay = checkForValue(productJsonObject, hrr11TerritoryDisplayKey);
        return hrr11TerritoryDisplay;
    }
    
    
    public boolean isHRRTableNeeded(String position) {
        boolean flag = false;
        String benefitArrayTag = "vitaBenPg11TableData";
        if ("tail".equals(position)) {
            benefitArrayTag = "vitaBenPg12TableData";
        }
        if (jsonObject.containsKey(benefitArrayTag)) {
            JSONArray jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);
            if (jsonArray.size() > 0) {
                flag = true;
            }
        }
        return flag;
    }
    /**
     * get RiderBenefit Illustration HRR
     * @param language
     * @param lifeAssured
     * @return
     */
    
    
    public boolean isBenefitTablesNeeded() {
        boolean flag = false;
        List<String> tableNames = new ArrayList<String>();
        tableNames.add("vitaStandard11TableData");
        tableNames.add("vitaStdOutput11TableData");
        tableNames.add("vitaExecutive11TableData");
        tableNames.add("vitaExecutiveOP11TableData");
        tableNames.add("vitaPremier11TableData");
        tableNames.add("vitaPremierOP11TableData");
        tableNames.add("vitaPremierOPDental11TableData");

        try {
        	for(String tableName:tableNames){
        		if (jsonObject.containsKey(tableName)) {
    			    JSONArray jsonArray = (JSONArray) jsonObject.get(tableName);
    			    if (jsonArray.size() > 0) {
    			        flag = true;
    			    }
    			}
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
        return flag;
    }
    
    
    
    public List<HRRBenfitIllustrationTableData> getRiderBenefitIllustrationHRR(
            String language, String position) {
        JSONArray jsonArray = null;
        List<HRRBenfitIllustrationTableData> riderData = new ArrayList<HRRBenfitIllustrationTableData>();
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                .get(language);
        String benefitArrayTag="vitaBenPg11TableData";
        if("tail".equals(position)){
            benefitArrayTag="vitaBenPg12TableData";
        }
        jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);
        boolean isColumn1BorderNeeded = false;
        boolean isColumn2BorderNeeded = false;
        try {
            for (Object obj : jsonArray) {
                isColumn1BorderNeeded = false;
                 isColumn2BorderNeeded = false;
                HRRBenfitIllustrationTableData riderTable = new HRRBenfitIllustrationTableData();
                isColumn1BorderNeeded = false;
                JSONObject jsonObj = (JSONObject) obj;
                if (jsonObj != null) {
                    
                    String benefitValue = createBenefitValue(productJsonObject,
                            jsonObj, language);
                    String description = checkForValue(productJsonObject, checkForValue(jsonObj,"actualLabel"));
                    if(GeneraliConstants.JSON_EMPTY.equals(description)){
                        description = checkForValue(productJsonObject, checkForValue(jsonObj,"label"));
                    }
                    String styleType=checkForValue(jsonObj,"type");
                    //Border needs only if there is data available.
                    if (!GeneraliConstants.JSON_EMPTY.equals(benefitValue)) {
                        isColumn2BorderNeeded = true;
                    }
                    //Border needs only if there is data available.
                    if (!GeneraliConstants.JSON_EMPTY.equals(description)) {
                        isColumn1BorderNeeded = true;
                    }
                    
                    riderTable.setBenefitValue(benefitValue);
                    riderTable.setBenefitDescription(description);
                    riderTable.setTypeStyle(styleType);
                    riderTable.setIsColumn1BorderNeeded(isColumn1BorderNeeded);
                    riderTable.setIsColumn2BorderNeeded(isColumn2BorderNeeded);
                    riderData.add(riderTable);

                }

            }
        } catch (Exception e) {
            LOGGER.error("getRiderBenefitIllustrationHRR .... " + e.getMessage()
                    + " " + agreementId);
        }
        return riderData;
    }
    
    public boolean isRiderTableNeeded(String tableName,String position) {
        boolean flag = false;
       /* String benefitArrayTag = "vitaBenPg11TableData";
        if ("tail".equals(position)) {
            benefitArrayTag = "vitaBenPg12TableData";
        }*/
        String benefitArrayTag = "";
        benefitArrayTag = getBenefitArrayTag(tableName,position);
      
        if (jsonObject.containsKey(benefitArrayTag)) {
            JSONArray jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);
            if (jsonArray.size() > 0) {
                flag = true;
            }
        }
        return flag;
    }
    
    public String getBenefitArrayTag(String tableName,String position){
    	  String benefitArrayTag = "";
    	  
    	  if(tableName != null && tableName.equalsIgnoreCase("Standard")){
          	benefitArrayTag="vitaStandard11TableData";
              if("tail".equals(position)){
                  benefitArrayTag="vitaStandard12TableData";
              }
          }else if(tableName != null && tableName.equalsIgnoreCase("StandardOutpatient")){
          	benefitArrayTag="vitaStdOutput11TableData";
              if("tail".equals(position)){
                  benefitArrayTag="vitaStdOutput12TableData";
              }
          }else if(tableName != null && tableName.equalsIgnoreCase("Executive")){
          	benefitArrayTag="vitaExecutive11TableData";
              if("tail".equals(position)){
                  benefitArrayTag="vitaExecutive12TableData";
              }
          }else if(tableName != null && tableName.equalsIgnoreCase("ExecutiveOutpatient")){
          	benefitArrayTag="vitaExecutiveOP11TableData";
              if("tail".equals(position)){
                  benefitArrayTag="vitaExecutiveOP12TableData";
              }
          }else if(tableName != null && tableName.equalsIgnoreCase("Premier")){
          	benefitArrayTag="vitaPremier11TableData";
              if("tail".equals(position)){
                  benefitArrayTag="vitaPremier12TableData";
              }
          }else if(tableName != null && tableName.equalsIgnoreCase("PremierOutpatient")){
          	benefitArrayTag="vitaPremierOP11TableData";
              if("tail".equals(position)){
                  benefitArrayTag="vitaPremierOP12TableData";
              }
          }else if(tableName != null && tableName.equalsIgnoreCase("PremierOutpatientDental")){
          	benefitArrayTag="vitaPremierOPDental11TableData";
              if("tail".equals(position)){
                  benefitArrayTag="vitaPremierOPDental12TableData";
              }
          }
		return benefitArrayTag;
    }
    /**
     * common data source for all the rider tables
     * 
     * 
     */
    public List<HRRBenfitIllustrationTableData> getRiderBenefitIllustration(
            String language, String position,String arrayName) {
        JSONArray jsonArray = null;
        List<HRRBenfitIllustrationTableData> riderData = new ArrayList<HRRBenfitIllustrationTableData>();
        JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput.get(language);
        
        String planName = arrayName;
        String benefitArrayTag= "";
        
        benefitArrayTag = getBenefitArrayTag(planName,position);
        
        jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);
        boolean isColumn1BorderNeeded = false;
        boolean isColumn2BorderNeeded = false;
        try {
            for (Object obj : jsonArray) {
                isColumn1BorderNeeded = false;
                 isColumn2BorderNeeded = false;
                HRRBenfitIllustrationTableData riderTable = new HRRBenfitIllustrationTableData();
                isColumn1BorderNeeded = false;
                JSONObject jsonObj = (JSONObject) obj;
                if (jsonObj != null) {
                    
                    String benefitValue = createBenefitValue(productJsonObject,
                            jsonObj, language);
                    String description = checkForValue(productJsonObject, checkForValue(jsonObj,"actualLabel"));
                    if(GeneraliConstants.JSON_EMPTY.equals(description)){
                        description = checkForValue(productJsonObject, checkForValue(jsonObj,"label"));
                    }
                    String styleType=checkForValue(jsonObj,"type");
                    //Border needs only if there is data available.
                    if (!GeneraliConstants.JSON_EMPTY.equals(benefitValue)) {
                        isColumn2BorderNeeded = true;
                    }
                    //Border needs only if there is data available.
                    if (!GeneraliConstants.JSON_EMPTY.equals(description)) {
                        isColumn1BorderNeeded = true;
                    }
                    
                    riderTable.setBenefitValue(benefitValue);
                    riderTable.setBenefitDescription(description);
                    riderTable.setTypeStyle(styleType);
                    riderTable.setIsColumn1BorderNeeded(isColumn1BorderNeeded);
                    riderTable.setIsColumn2BorderNeeded(isColumn2BorderNeeded);
                    riderData.add(riderTable);

                }

            }
        } catch (Exception e) {
            LOGGER.error("getRiderBenefitIllustrationHRR .... " + e.getMessage()
                    + " " + agreementId);
        }
        return riderData;
    }
    
    
    
    // End page 9
    
    //Page 10
    
    //Print when conditions
    
    public boolean isPremiumTbaleNeeded(String position) {
        Boolean tableNeeded = false;
        JSONArray jsonArray = null;
        String benefitArrayTag = "";
        if ("head".equals(position)) {
            benefitArrayTag = "riderTable1Data";
        } else if ("middle".equals(position)) {
            benefitArrayTag = "riderTable2Data";
        } else if ("tail".equals(position)) {
            benefitArrayTag = "riderTable3Data";
        }
        jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);
        if (jsonArray.size() > 0) {
            tableNeeded = true;
        }
        return tableNeeded;

    }
    /**
     * Gets the benefit illustration table details 
     * 
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public List<RiderTableData> getRiderPremiumTable(String position,
            String language) {
        JSONArray jsonArray = null;
        List<RiderTableData> illusData = new ArrayList<RiderTableData>();
        String benefitArrayTag = "riderTable1Data";
        if ("middle".equals(position)) {
            benefitArrayTag = "riderTable2Data";
        } else if ("tail".equals(position)) {
            benefitArrayTag = "riderTable3Data";
        }
        jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);
        String yearlyPremium = "-";
        String vitaHalfYrlyPrem = "-";

        try {
            for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                RiderTableData illustrationTableData = new RiderTableData();
                yearlyPremium = "-";
                vitaHalfYrlyPrem = "-";
                String zero = new String("0");
                if (jsonObj != null) {
                    if (checkForValue(jsonObj, "vitaPolicyYear").toString() != null
                            && checkForValue(jsonObj, "vitaPolicyYear")
                                    .toString() != ""
                            && checkForValue(jsonObj, "vitaPolicyYear")
                                    .toString() != "0") {
                        illustrationTableData.setPolicyYr(checkForValue(
                                jsonObj, "vitaPolicyYear").toString());
                    }

                    if (checkForValue(jsonObj, "vitaYrlyPrem") != ""
                            && !zero.equals(checkForValue(jsonObj,
                                    "vitaYrlyPrem"))) {
                        yearlyPremium = getMoneyValues(jsonObj, "vitaYrlyPrem",language);
                    }
                    illustrationTableData.setYearlyPremium(yearlyPremium);

                    if (checkForValue(jsonObj, "vitaHalfYrlyPrem") != null
                            && !zero.equals(checkForValue(jsonObj,
                                    "vitaHalfYrlyPrem"))) {
                        vitaHalfYrlyPrem = getMoneyValues(jsonObj, "vitaHalfYrlyPrem", language);
                    }
                    illustrationTableData
                            .setTotalPremiumHalfYearly(vitaHalfYrlyPrem);

                }
                illusData.add(illustrationTableData);
            }
        } catch (Exception e) {
            LOGGER.error("getRiderPremiumTable .... " + e.getMessage()
                    + " " + agreementId);
        }
        return illusData;

    }

    //Total paid premium
    //Yearly
    /**
     * guar Claim First Late Stg CI Ben.
     * 
     * @return old age special cash benefit Fund
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getTotalAnnualPremium_VITA(String language)
            throws JRScriptletException {
        String totalAnnualPremium_VITA= "";
        try {
            totalAnnualPremium_VITA=getMoneyValues(jsonObject,"totalAnnualPremium_VITA",language);
       	
        } catch (Exception e) {
            LOGGER.error("getTotalAnnualPremium_VITA .... "
                    + e.getMessage() + " " + agreementId);
        }
        return totalAnnualPremium_VITA;
    }
    
    //Half Yearly
    /**
     * @return totalSemiAnnualPremium_VITA
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getTotalSemiAnnualPremium_VITA(String language)
            throws JRScriptletException {
        String totalSemiAnnualPremium_VITA= "";
        try {
            totalSemiAnnualPremium_VITA=getMoneyValues(jsonObject,"totalSemiAnnualPremium_VITA",language);
        	
        } catch (Exception e) {
            LOGGER.error("getTotalSemiAnnualPremium_VITA .... "
                    + e.getMessage() + " " + agreementId);
        }
        return totalSemiAnnualPremium_VITA;
    }

    //end

    /**
     * Gets the policy holder signature.
     * 
     * @return the policy holder signature
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPHSignature() {
        String sign = null;
        String temptoken = null;
        Document documentDetail = null;
        try {
            if (appionteeDocuments != null) {
                for (Document agreementDocument : appionteeDocuments) {
                    if (agreementDocument != null) {
                        if (agreementDocument.getSignatureType().equals(
                                "ClientSignature")) {
                            documentDetail = agreementDocument;
                            sign = documentDetail.getBase64string();
                            StringTokenizer token = new StringTokenizer(sign,
                                    ",");
                            while (token.hasMoreTokens()) {
                                temptoken = token.nextToken();
                            }
                            if (temptoken != null) {
                                sign = temptoken;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getLeadSignature for " + agreementId + " "
                    + e.getMessage());
        }
        return sign;
    }

    /**
     * Gets the agent signature.
     * 
     * @return the agent signature
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getAgentSignature() {
        String sign = null;
        String temptoken = null;
        Document documentDetail = null;
        try {
            if (appionteeDocuments != null) {
                for (Document agreementDocument : appionteeDocuments) {
                    if (agreementDocument != null) {
                        documentDetail = agreementDocument;
                        if (agreementDocument.getSignatureType().equals(
                                "AgentSignature")) {
                            //documentDetail = agreementDocument;
                            sign = documentDetail.getBase64string();
                            StringTokenizer token = new StringTokenizer(sign,
                                    ",");
                            while (token.hasMoreTokens()) {
                                temptoken = token.nextToken();
                            }
                            if (temptoken != null) {
                                sign = temptoken;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getAgentSignature for " + agreementId
                    + e.getMessage());
        }
        return sign;
    }

    /**
     * Gets the Main Insured signature.
     * 
     * @return the policy holder signature
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getMISignature() {
        String sign = null;
        String temptoken = null;
        Document documentDetail = null;
        try {
            if (appionteeDocuments != null) {
                for (Document agreementDocument : appionteeDocuments) {
                    if (agreementDocument != null) {
                        if (agreementDocument.getSignatureType().equals(
                                "lifeAssuredSig")) {
                            documentDetail = agreementDocument;
                            sign = documentDetail.getBase64string();
                            StringTokenizer token = new StringTokenizer(sign,
                                    ",");
                            while (token.hasMoreTokens()) {
                                temptoken = token.nextToken();
                            }
                            if (temptoken != null) {
                                sign = temptoken;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getMISignature for " + agreementId + " "
                    + e.getMessage());
        }
        return sign;
    }

    /**
     * Gets the illustrated date.
     * 
     * @return the illustrated date
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getIllustratedDate() {
        String illustratedDate = "";
        try {
            illustratedDate = checkForValue(jsonObject, "illustratedDate");
        	        } catch (Exception e) {
            LOGGER.error("Error in getting the illustrated date for "
                    + e.getMessage() + " " + agreementId);
        }

        return illustratedDate;
    }

    /**
     * Gets the agent name.
     * 
     * @return the agent name
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getAgentName() throws JRScriptletException {
        String agentName = "";
        try {
            agentName = checkForValue(jsonObject, "agentName");
       	
        } catch (Exception e) {
            LOGGER.error("Error in getting the agent name for "
                    + e.getMessage() + " " + agreementId);
        }

        return agentName;
    }

    /**
     * Gets the agent id.
     * 
     * @return the agent id
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getAgentId() {
        String agentId = "";
        try {
            agentId = checkForValue(jsonObject, "agentCode");
       	
        } catch (Exception e) {
            LOGGER.error("Error in getting the agent code for "
                    + e.getMessage() + " " + agreementId);
        }

        return agentId;
    }

    /**
     * getHeading1
     * 
     * @param language
     * @return
     */
    public Boolean getHeading1() {
        Boolean channelTcb = false;
        String channelType = "";
        if (jsonObject.containsKey("Channel")) {
            JSONObject channelJSONObject = (JSONObject) jsonObject
                    .get("Channel");
            channelType = checkForValue(channelJSONObject, "id");
            if ("9085".equals(channelType)) {
                channelTcb = true;
            }
        }

        return channelTcb;

    }

    /**
     * Gets the premium frequency.
     * 
     * @return the premium frequency
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getPremiumFrequency(String language) {
        String premiumFrequency = "";
        String premiumFrequncyCode = "6001";
        try {
            JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput.get(language);

            JSONObject policyDetailsJsonObject = getValue(jsonObject, "PolicyDetails");
            if (policyDetailsJsonObject != null) {
                premiumFrequncyCode = checkForValue(policyDetailsJsonObject, "premiumMode");
            }
            premiumFrequency = checkForValue(productJsonObject, "PF" + premiumFrequncyCode);
        } catch (Exception e) {
            LOGGER.error("Error in getting the premiumFrequency for " + agreementId, e);
        }
        return premiumFrequency;

    }

    public String getPremiumFrequencyForProductTable(String language) {
        String premiumFrequency = "";
        String premiumFrequncyCode = "6001";
        try {
            JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput.get(language);

            JSONObject policyDetailsJsonObject = getValue(jsonObject, "PolicyDetails");
            if (policyDetailsJsonObject != null) {
                premiumFrequncyCode = checkForValue(policyDetailsJsonObject, "premiumMode");
            }
            premiumFrequency = checkForValue(productJsonObject, "P" + premiumFrequncyCode);
        } catch (Exception e) {
            LOGGER.error("Error in getting the premiumFrequency for " + agreementId, e);
        }
        return premiumFrequency;

    }
    /**
     * Gets the product code.
     * 
     * @return the product code
     * @throws JRScriptletException
     *             the JR scriptlet exception
     */
    public String getProductCode() {
        String productCode = "";
        try {
            JSONObject productDetailsJsonObject = getValue(jsonObject, "PolicyDetails");
            if (productDetailsJsonObject != null) {
                productCode = checkForValue(productDetailsJsonObject, "productCode");
            }
        } catch (Exception e) {
            LOGGER.error("Error in getting the productCode for " + agreementId, e);
        }
        return productCode;

    }
    
    /**
     * getAgeCalculatedDate
     * 
     * @return ageCalculatedDateString
     */
    public String getAgeCalculatedDate() {
        String ageCalculatedDateString = "";
        try {
            Date ageCalculatedDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            if (payerPerson != null) {
                ageCalculatedDate = payerPerson.getIdentityDate();
                ageCalculatedDateString = sdf.format(ageCalculatedDate);
            }
        } catch (Exception e) {
            LOGGER.error(
                    "Error in getting the age calculated date of policyHolder for "
                            + agreementId, e);
        }
        return ageCalculatedDateString;

    }

    /**
     * getPolicyHolderJobClass
     * 
     * @return jobClass
     */
    public String getPolicyHolderJobClass() {
        String jobClass = "";
        try {
            if (payerPerson != null) {
                Set<PersonDetail> payerPersonDetails = payerPerson.getDetail();
                if (payerPersonDetails != null) {
                    for (PersonDetail personDetail : payerPersonDetails) {
                        if (personDetail instanceof OccupationDetail) {
                            OccupationDetail payerOccupationDetail = (OccupationDetail) personDetail;
                            if (payerOccupationDetail != null) {
                                jobClass = payerOccupationDetail
                                        .getOccupationClass();
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            LOGGER.error(
                    "Error in getting the occupation class of policyHolder for "
                            + agreementId, e);
        }
        return jobClass;

    }

    /**
     * get INName ForBenefitIllus Table
     * 
     * @return
     */
    public String getINNameForBenefitIllus(String language) {
        String inName = "";
        try {
            JSONObject productJsonObject = (JSONObject) jsonObjectPdfInput
                    .get(language);
            if (isBenefitIllustrationInsuredNeeded("LA1")) {
                inName = getINName(language);
                if (!GeneraliConstants.JSON_EMPTY.equals(inName)) {
                    String header = checkForValue(productJsonObject,
                            "BenefitIllusMIHeader");
                    inName = header + " " + inName;

                }
            }
        } catch (Exception e) {
            LOGGER.error(
                    "Error in getting the insured name for " + agreementId, e);
        }
        return inName;

    }
    
    /**
     * Method to create Benefit value;
     * @param productJsonObject
     * @param jsonObj
     * @param language
     * @return
     */
    public String createBenefitValue(JSONObject productJsonObject,
            JSONObject jsonObj, String language) {
        String benefitValueString = "";

        try {
            String valueFromRule = formatter(checkForValue(jsonObj, "benefitValue"),
                    language);
            String valuekey=valueFromRule.replaceAll("[^a-zA-Z]+", "")
            .replaceAll("\\s", "").trim();
            String value=checkForValue(productJsonObject,valuekey
            );
            if(GeneraliConstants.JSON_EMPTY.equals(value)){
                value=valueFromRule;
            }
            String prefixFromRule=checkForValue(jsonObj, "prefix");
            String prefixKey = prefixFromRule.replaceAll("[^a-zA-Z]+", "")
            .replaceAll("\\s", "").trim();
            String prefix = checkForValue(productJsonObject,prefixKey
                    );
            String suffixFromRule=checkForValue(jsonObj, "suffix");
            String suffixKey = suffixFromRule.replaceAll("[^a-zA-Z]+", "")
            .replaceAll("\\s", "").trim();
            String suffix = checkForValue(productJsonObject,suffixKey
                    );
            benefitValueString = prefix.concat (" ").concat(value).concat(" ").concat(suffix).trim();
        } catch (Exception e) {
            LOGGER.error("createBenefitValue .... " + e.getMessage() + " "
                    + agreementId);
        }
        return benefitValueString;
    }
    /**
     * Adds comma or dot to separate the number.
     * 
     * @return the number with comma as string
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String formatter(String number, String language) {
        double amount = 0;
        BigDecimal bd = new BigDecimal("0");
        String result = number;

        try {
            bd = new BigDecimal(number);
            amount = Double.parseDouble(number);
        } catch (NumberFormatException e) {
            LOGGER.error("Error in formatting value " + number
                    + " For agreement" + agreementId, e.getMessage());
        } finally {

            DecimalFormat formatter = (DecimalFormat) NumberFormat
                    .getInstance(Locale.US);
            DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
            if ("vn".equals(language)) {
                symbols.setGroupingSeparator('.');
            } else {
                symbols.setGroupingSeparator(',');
            }
            formatter.setDecimalFormatSymbols(symbols);
            if (amount >0) {
                result = formatter.format(bd);
            }
        }
        return result;
    }

    /**
     * Gets the value from JSON.
     * 
     * @return the value;
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String checkForValue(JSONObject obj, String key) {
        String value = "";
        if (obj.containsKey(key)) {
            Object jsonValue = obj.get(key);
            if (jsonValue != null) {
                value = jsonValue.toString();
            }
        }
        return value;
    }

    /**
     * Gets the value from JSON.
     * 
     * @return the value
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public JSONObject getValue(JSONObject obj, String key) {
        JSONObject value = null;
        if (obj.containsKey(key)) {
            Object jsonValue = obj.get(key);
            value = (JSONObject) jsonValue;
        }
        return value;
    }

    /**
     * getRiderFooter
     * 
     * @return boolean Returns a boolean to print different disclaimers
     */
    public boolean getRiderFooter() {
        boolean riderFooterwithAsterik = false;
        JSONArray jsonArray = null;
        try {
            if (jsonObject != null) {

                jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
                for (Object obj1 : jsonArray) {
                    JSONObject jsonObj1 = (JSONObject) obj1;
                    String riderNameInput = "";
                    if (jsonObj1 != null) {
                        if (jsonObj1.containsKey("productName")) {
                            riderNameInput = checkForValue(jsonObj1,
                                    "productName");
                            if (riderNameInput.contains("*")) {
                                riderFooterwithAsterik = true;
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(
                    "Error in getting Rider Footer disclaimer flag For agreement"
                            + agreementId, e.getMessage());
        }
        return riderFooterwithAsterik;

    }

    public String createFullName(String firstName, String lastName,
            String language) {
        String fullname;
        StringBuffer sbFullname = new StringBuffer(firstName);
        try {
            if (lastName != null) {
                /*
                 * if (VN.equals(language)) { sbFullname = new
                 * StringBuffer(lastName); sbFullname = sbFullname.append(" ");
                 * sbFullname = sbFullname.append(firstName); } else {
                 */
                sbFullname = sbFullname.append(" ");
                sbFullname = sbFullname.append(lastName);
                // }
            }
        } catch (Exception e) {
            LOGGER.error("Error in concatinating full name, First Name: "
                    + firstName + "Last Name : " + lastName + "AgreementId : "
                    + agreementId, e.getMessage());
        }
        fullname = sbFullname.toString();
        return fullname;
    }
    
    public String getMoneyValues(JSONObject jsonObject, String key, String language)throws Exception{
        String moneyValue="";
        moneyValue= checkForValue(jsonObject, key);

        if (!GeneraliConstants.JSON_EMPTY.equals(moneyValue)){
            BigDecimal bd = new BigDecimal(moneyValue);
            moneyValue= formatter(bd.toString(), language);
        }
        return moneyValue;
        
    }

    public boolean ifRiderExist(String riderName) {
        boolean isRequiredFlag = false;

        if (jsonObject.containsKey(riderName)) {
            JSONObject riderObject = (JSONObject) jsonObject.get(riderName);

            if (isRiderExistForinsured(riderObject, "LA1")) {
                isRequiredFlag = true;
            } else if (isRiderExistForinsured(riderObject, "LA2")) {
                isRequiredFlag = true;
            } else if (isRiderExistForinsured(riderObject, "LA3")) {
                isRequiredFlag = true;
            }
        }
        return isRequiredFlag;
    }

    public boolean isRiderExistForinsured(JSONObject riderObject,
            String insuredType) {
        boolean isRequiredFlag = false;
        String isRequiredValue = "";
        if (riderObject.containsKey(insuredType)) {
            JSONObject laObject = (JSONObject) riderObject.get(insuredType);
            if (laObject != null) {
                isRequiredValue = checkForValue(laObject, "isRequired");
                if ("Yes".equalsIgnoreCase(isRequiredValue)) {
                    isRequiredFlag = true;
                }
            }

        }
        return isRequiredFlag;
    }
    
    public int getAdditionalInsuredCount(){
    	int count = 0;
    	
    	 JSONArray jsonArray = null;
    	 String benefitArrayTag = "vitaRiderTable1Data";
    	 jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);
    	 if(jsonArray != null && jsonArray.size() > 0){
    		 Object obj = jsonArray.get(0);
        	 JSONObject jsonObj = (JSONObject) obj;
        	 if(jsonObj.containsKey("fifthPremium")){
        		 count = 5;
        	 }else if(jsonObj.containsKey("fourthPremium")){
        		 count = 4;
        	 }else if(jsonObj.containsKey("thirdPremium")){
        		 count = 3;
        	 }else if(jsonObj.containsKey("secondPremium")){
        		 count = 2;
        	 }else if(jsonObj.containsKey("firstPremium")){
        		 count = 1;
        	 }
    	 }
		return count;
    }
    
    public List<RiderTableData> getRiderPremiumTableForVita(String position,
            String language) {
        JSONArray jsonArray = null;
        List<RiderTableData> illusData = new ArrayList<RiderTableData>();
        String benefitArrayTag = "vitaRiderTable1Data";
        if ("middle".equals(position)) {
            benefitArrayTag = "vitaRiderTable2Data";
        } else if ("tail".equals(position)) {
            benefitArrayTag = "vitaRiderTable3Data";
        }
        jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);
        String yearlyPremium = "-";
        String mainInsuredPremium = "-";
        String addInsured1Premium = "-";
        String addInsured2Premium = "-";
        String addInsured3Premium = "-";
        String addInsured4Premium = "-";

        try {
            for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                RiderTableData illustrationTableData = new RiderTableData();
                mainInsuredPremium = "-";
                addInsured1Premium = "-";
                addInsured2Premium = "-";
                addInsured3Premium = "-";
                addInsured4Premium = "-";
                String zero = new String("0");
                if (jsonObj != null) {
                    if (checkForValue(jsonObj, "vitaPolicyYear").toString() != null
                            && checkForValue(jsonObj, "vitaPolicyYear")
                                    .toString() != ""
                            && checkForValue(jsonObj, "vitaPolicyYear")
                                    .toString() != "0") {
                        illustrationTableData.setPolicyYr(checkForValue(
                                jsonObj, "vitaPolicyYear").toString());
                    }
                    
                    if (checkForValue(jsonObj, "firstPremium") != ""
                        && !zero.equals(checkForValue(jsonObj,
                                "firstPremium"))) {
                    	mainInsuredPremium = getMoneyValues(jsonObj, "firstPremium",language);
                    }
                    illustrationTableData.setMainInsuredPremium(mainInsuredPremium);

                    if (checkForValue(jsonObj, "secondPremium") != ""
                        && !zero.equals(checkForValue(jsonObj,
                                "secondPremium"))) {
                    	addInsured1Premium = getMoneyValues(jsonObj, "secondPremium",language);
                    }
                    illustrationTableData.setAddIns1Premium(addInsured1Premium);
                    
                    if (checkForValue(jsonObj, "thirdPremium") != ""
                        && !zero.equals(checkForValue(jsonObj,
                                "thirdPremium"))) {
                    	addInsured2Premium = getMoneyValues(jsonObj, "thirdPremium",language);
                    }
                    illustrationTableData.setAddIns2Premium(addInsured2Premium);
                    
                    if (checkForValue(jsonObj, "fourthPremium") != ""
                        && !zero.equals(checkForValue(jsonObj,
                                "fourthPremium"))) {
                    	addInsured3Premium = getMoneyValues(jsonObj, "fourthPremium",language);
                    }
                    illustrationTableData.setAddIns3Premium(addInsured3Premium);
                    
                    if (checkForValue(jsonObj, "fifthPremium") != ""
                        && !zero.equals(checkForValue(jsonObj,
                                "fifthPremium"))) {
                    	addInsured4Premium = getMoneyValues(jsonObj, "fifthPremium",language);
                    }
                    illustrationTableData.setAddIns4Premium(addInsured4Premium);
                    
                    illustrationTableData.setLifeAssured(checkForValue(jsonObj, "firstName"));
                    
                    illustrationTableData.setFirstAddInsuredName(checkForValue(jsonObj, "secondName"));
                    illustrationTableData.setSecondAddInsuredName(checkForValue(jsonObj, "thirdName"));
                    illustrationTableData.setThirdAddInsuredName(checkForValue(jsonObj, "fourthName"));
                    illustrationTableData.setFourthAddInsuredName(checkForValue(jsonObj, "fifthName"));
                    
                   /* if (checkForValue(jsonObj, "vitaYrlyPrem") != ""
                            && !zero.equals(checkForValue(jsonObj,
                                    "vitaYrlyPrem"))) {
                        yearlyPremium = getMoneyValues(jsonObj, "vitaYrlyPrem",language);
                    }
                    illustrationTableData.setYearlyPremium(yearlyPremium);

                    if (checkForValue(jsonObj, "vitaHalfYrlyPrem") != null
                            && !zero.equals(checkForValue(jsonObj,
                                    "vitaHalfYrlyPrem"))) {
                        vitaHalfYrlyPrem = getMoneyValues(jsonObj, "vitaHalfYrlyPrem", language);
                    }
                    illustrationTableData
                            .setTotalPremiumHalfYearly(vitaHalfYrlyPrem);*/

                }
                illusData.add(illustrationTableData);
            }
        } catch (Exception e) {
            LOGGER.error("getRiderPremiumTable .... " + e.getMessage()
                    + " " + agreementId);
        }
        return illusData;
    }
    
    public boolean isVitaPremiumTbaleNeeded(String position) {
        Boolean tableNeeded = false;
        JSONArray jsonArray = null;
        String benefitArrayTag = "";
        if ("head".equals(position)) {
            benefitArrayTag = "vitaRiderTable1Data";
        } else if ("middle".equals(position)) {
            benefitArrayTag = "vitaRiderTable2Data";
        } else if ("tail".equals(position)) {
            benefitArrayTag = "vitaRiderTable3Data";
        }
        jsonArray = (JSONArray) jsonObject.get(benefitArrayTag);
        if (jsonArray.size() > 0) {
            tableNeeded = true;
        }
        return tableNeeded;
    }
    
    /*** Changes for Generali Thailand*/
    public String getGeneraliAgentName() throws JRScriptletException {
		String name = "";
		name = generaliAgent.getAgentName();
		return name;
	}
    
    public String getGeneraliAgentLicense() throws JRScriptletException{
    	String licNumber="";
    	licNumber=generaliAgent.getLicenseNumber();
    	return licNumber;
    }
    
    public String getPDFPrintDate()throws JRScriptletException{
    	Calendar calendar =Calendar.getInstance();
    	calendar.add(Calendar.YEAR, 543);
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    	String date = sdf.format(calendar.getTime());
    	return date;
    }
    
    public int getCustomerAge() throws JRScriptletException {
    	int customerAge;
    	customerAge = insuredPerson.getAge();
    	return customerAge;
    }
    
    public String getCustomerName() throws JRScriptletException {
		String fullname = "";
		for (PersonName pName : insuredPerson.getName()) {
			fullname = pName.getFullName();
		}
		return fullname;
	}
    
    public String getAgentContactNumber() throws JRScriptletException{
    	String agentContactNo="";
    	agentContactNo =generaliAgent.getZmPhone();
    	return agentContactNo;
    }
    
    public String getCustomerGender() throws JRScriptletException {
    	  	String gender;
    	  	gender = insuredPerson.getGenderCode().toString();
    	  	if(gender.equals(male)){
    	  		gender=GeneraliConstants.MALE;
    	  	}else if(gender.equals(female)){
    	  		gender=GeneraliConstants.FEMALE;
    	  	}
    	    return gender;
    }
    
    public String getPremiumFrequency() throws JRScriptletException{
    	String premiumFrequency= "";
    	String premiumFrequencyCode="";
    	try {
    		JSONObject policyDetailsJsonObject = getValue(jsonObject, "PolicyDetails");
    		if (policyDetailsJsonObject != null){
    			premiumFrequency = checkForValue(policyDetailsJsonObject, "premiumMode");
    		}
        } catch (Exception e) {
            LOGGER.error("getPremiumFrequency .... " + e.getMessage()
                    + " " + agreementId);
        }
    		if(premiumFrequency.equals(annual)){
    			 premiumFrequencyCode=GeneraliConstants.ANNUAL;
    		}else if(premiumFrequency.equals(semiAnnual)){
    			premiumFrequencyCode=GeneraliConstants.SEMI_ANNUAL;
    		}else if(premiumFrequency.equals(quarterly)){
    			premiumFrequencyCode=GeneraliConstants.QUARTERLY;
    		}else if(premiumFrequency.equals(monthly)){
    			premiumFrequencyCode=GeneraliConstants.MONTHLY;
    		}
    		return premiumFrequencyCode;
    }
          
    public String getProtectionPeriodFinalYears()throws JRScriptletException {
    	String protectionPeriodFinal="" ;
        try {
        	if(jsonObject!=null)
        	{protectionPeriodFinal = checkForValue(jsonObject, "protectionPeriodFinal");}
        } catch (Exception e) {
            LOGGER.error("getprotectionPeriodFinalYears .... " + e.getMessage()
                    + " " + agreementId);
        }
        return protectionPeriodFinal;
    }
    
    public String getPremiumPeriodFinalYears()throws JRScriptletException {
    	String premiumPeriodFinal="" ;
        try {
        	if(jsonObject!=null)
        	{premiumPeriodFinal = checkForValue(jsonObject, "premiumPeriodFinal");
        	}
        } catch (Exception e) {
            LOGGER.error("getpremiumPeriodFinalYears .... " + e.getMessage()
                    + " " + agreementId);
        }
        return premiumPeriodFinal;
    }
    
    public String getSumInsured() throws JRScriptletException {
        String sumAssured = "";
        try {
        	if(jsonObject!=null)
            {
        		if (jsonObject.containsKey("PolicyDetails")) {
            	sumAssured = checkForValue(getValue(jsonObject, "PolicyDetails"),"sumAssured");
            	BigDecimal amt = new BigDecimal(sumAssured);
            	sumAssured = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        		}
            }
        } catch (Exception e) {
            LOGGER.error("getSumInsured .... " + e.getMessage() + " "
                    + agreementId);
        }
        return sumAssured;	
    }
    
    public String getAnnualPremium()throws JRScriptletException {
    	String annualPremium="" ;
        try {
        	if(jsonObject!=null)
        	{
        		if (jsonObject.containsKey("PolicyDetails")){
        		annualPremium = checkForValue(getValue(jsonObject, "PolicyDetails"), "premium");
        		BigDecimal amt = new BigDecimal(annualPremium);
        		annualPremium = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        		}
        	}
        } catch (Exception e) {
            LOGGER.error("getannualPremium .... " + e.getMessage()
                    + " " + agreementId);
        }
        return annualPremium;
    }
    
    public String getTotalPremium()throws JRScriptletException {
    	String totalPremium="" ;
        try {
        	if(jsonObject!=null)
        	{
        		totalPremium = checkForValue(jsonObject,  "totalPremiumFinal");
        		BigDecimal amt = new BigDecimal(totalPremium);
        		totalPremium = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getTotalPremium .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalPremium;
    }
    
    public String getSumInsuredField()throws JRScriptletException{
    	String sumInsuredField="";
    	try {
    		if(jsonObject!=null)
    		{	
    		sumInsuredField = checkForValue(jsonObject, "sumAssuredField");
    		BigDecimal amt = new BigDecimal(sumInsuredField);
    		sumInsuredField = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
    		}
        } catch (Exception e) {
            LOGGER.error("getsumInsuredField .... " + e.getMessage()
                    + " " + agreementId);
        }
        return sumInsuredField;
    	
    }
    public String getPremiumModeField()throws JRScriptletException{
    	String premiumModeField="";
    	try {
    		if(jsonObject!=null)
    		{
    			premiumModeField = checkForValue(jsonObject, "premiumModeField");
    			BigDecimal amt = new BigDecimal(premiumModeField);
    			premiumModeField = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
    		}
        } catch (Exception e) {
            LOGGER.error("getpremiumModeField .... " + e.getMessage()
                    + " " + agreementId);
        }
        return premiumModeField;
    	
    }
    public String getPremiumModeFieldforCC()throws JRScriptletException{
    	String premiumModeField="";
    	try {
    		if(jsonObject!=null)
    		{
    			premiumModeField = checkForValue(jsonObject, "premiumModeField");
    			BigDecimal amt = new BigDecimal(premiumModeField);
    			premiumModeField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
    		}
        } catch (Exception e) {
            LOGGER.error("getpremiumModeField .... " + e.getMessage()
                    + " " + agreementId);
        }
        return premiumModeField;
    	
    }
    public String getDeathBenefitMinField()throws JRScriptletException{
    	String deathBenefitMinField="";
    	try {
    		if(jsonObject!=null)
    		{
    			deathBenefitMinField = checkForValue(jsonObject, "deathBenefitMinField");
    			BigDecimal amt = new BigDecimal(deathBenefitMinField);
    			deathBenefitMinField = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
    		}
    		
        } catch (Exception e) {
            LOGGER.error("getdeathBenefitMinField .... " + e.getMessage()
                    + " " + agreementId);
        }
        return deathBenefitMinField;
    	
    }
    public String getDeathBenefitMaxField()throws JRScriptletException{
    	String deathBenefitMaxField="";
    	try {
    		if(jsonObject!=null)
    		{
    			deathBenefitMaxField = checkForValue(jsonObject, "deathBenefitMaxField");
    			BigDecimal amt = new BigDecimal(deathBenefitMaxField);
    			deathBenefitMaxField = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
    		}
        } catch (Exception e) {
            LOGGER.error("getdeathBenefitMaxField .... " + e.getMessage()
                    + " " + agreementId);
        }
        return deathBenefitMaxField;
    	
    }
    public String getInsuredAgeField()throws JRScriptletException{
    	String insuredAge="";
    	try {
    		if(jsonObject!=null)
    		{
    			insuredAge = checkForValue(jsonObject, "insuredAge");
    		}
        } catch (Exception e) {
            LOGGER.error("getinsuredAge .... " + e.getMessage()
                    + " " + agreementId);
        }
        return insuredAge;
    	
    }
    
    public String getInsuredAgeAfterEightYears()throws JRScriptletException{
    	String insuredAgeAfterEightYears="";
    	try {
    		if(jsonObject!=null)
    		{
    			insuredAgeAfterEightYears = checkForValue(jsonObject, "insuredAgeAfterEightYears");
    		}
        } catch (Exception e) {
            LOGGER.error("getinsuredAgeAfterEightYears .... " + e.getMessage()
                    + " " + agreementId);
        }
        return insuredAgeAfterEightYears;
    	
    }
   
    
    public String getFifteenPerSA()throws JRScriptletException {
    	String fifteenPerSA="" ;
        try {
        	if(jsonObject!=null)
        	{
        		fifteenPerSA = checkForValue(jsonObject, "fifteenPerSA");
        		BigDecimal amt = new BigDecimal(fifteenPerSA);
        		fifteenPerSA = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getfifteenPerSA .... " + e.getMessage()
                    + " " + agreementId);
        }
        return fifteenPerSA;
    }
    
    public String getTwentyPerSA()throws JRScriptletException {
    	String twentyPerSA="" ;
        try {
        	if(jsonObject!=null)
        	{
        		twentyPerSA = checkForValue(jsonObject, "twentyPerSA");
        		BigDecimal amt = new BigDecimal(twentyPerSA);
        		twentyPerSA = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("gettwentyPerSA .... " + e.getMessage()
                    + " " + agreementId);
        }
        return twentyPerSA;
    }
    
    public String getTwentyFivePerSA()throws JRScriptletException {
    	String twentyFivePerSA="" ;
        try {
        	if(jsonObject!=null)
        	{
        		twentyFivePerSA = checkForValue(jsonObject, "twentyFivePerSA");
        		BigDecimal amt = new BigDecimal(twentyFivePerSA);
        		twentyFivePerSA = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("gettwentyFivePerSA .... " + e.getMessage()
                    + " " + agreementId);
        }
        return twentyFivePerSA;
    }
    
    public String getTotalBenefitAmountField()throws JRScriptletException{
    	String totalBenefitAmountField="";
    	try {
    		if(jsonObject!=null)
    		{
    			totalBenefitAmountField = checkForValue(jsonObject, "totalBenefitAmountField");
    			BigDecimal amt = new BigDecimal(totalBenefitAmountField);
    			totalBenefitAmountField = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
    		}
        } catch (Exception e) {
            LOGGER.error("gettotalBenefitAmountField .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalBenefitAmountField;
    	
    }
    
    public String getTotalLivingBenefit()throws JRScriptletException {
    	String totalLivingBenefit="" ;
        try {
        	if(jsonObject!=null)
        	{
        		totalLivingBenefit = checkForValue(jsonObject, "totalLivingBenefit");
        		BigDecimal amt = new BigDecimal(totalLivingBenefit);
        		totalLivingBenefit = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getTotalLivingBenefit .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalLivingBenefit;
    }
    
    public String getTotalLivingBenefitForCC()throws JRScriptletException {
    	String totalLivingBenefit="" ;
        try {
        	if(jsonObject!=null)
        	{
        		totalLivingBenefit = checkForValue(jsonObject, "totalLivingBenefit");
        	}
        } catch (Exception e) {
            LOGGER.error("getTotalLivingBenefitForCC .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalLivingBenefit;
    }
    
    public String getTotalTaxationBenefit()throws JRScriptletException {
    	String totalTaxationBenefit="" ;
        try {
        	if(jsonObject!=null)
        	{
        		totalTaxationBenefit = checkForValue(jsonObject, "totalTaxationBenefit");
        		BigDecimal amt = new BigDecimal(totalTaxationBenefit);
        		totalTaxationBenefit = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getTotalTaxationBenefit .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalTaxationBenefit;
    }
    
    public String getTotalTaxationBenefitForCC()throws JRScriptletException {
    	String totalTaxationBenefit="" ;
        try {
        	if(jsonObject!=null)
        	{
        		totalTaxationBenefit = checkForValue(jsonObject, "totalTaxationBenefit");
        	}
        } catch (Exception e) {
            LOGGER.error("getTotalTaxationBenefitForCC .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalTaxationBenefit;
    }
    
    public String getTotalPremiumPaid()throws JRScriptletException {
    	String totalPremiumPaid="" ;
        try {
        	if(jsonObject!=null){
        		totalPremiumPaid = checkForValue(jsonObject, "totalPremiumPaid");
        		BigDecimal amt = new BigDecimal(totalPremiumPaid);
        		totalPremiumPaid = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
        		}
        } catch (Exception e) {
            LOGGER.error("getTotalPremiumPaid .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalPremiumPaid;
    }
    
    public String getTotalPremiumPaidForCC()throws JRScriptletException {
    	String totalPremiumPaid="" ;
        try {
        	if(jsonObject!=null){
        		totalPremiumPaid = checkForValue(jsonObject, "totalPremiumPaid");
        		}
        } catch (Exception e) {
            LOGGER.error("getTotalPremiumPaidForCC .... " + e.getMessage()
                    + " " + agreementId);
        }
        return totalPremiumPaid;
    }
    
    public String getGrandTotalBenefit()throws JRScriptletException {
    	String grandTotalBenefit="" ;
        try {
        	if(jsonObject!=null)
        	{
        		grandTotalBenefit = checkForValue(jsonObject, "grandTotalBenefit");
        		BigDecimal amt = new BigDecimal(grandTotalBenefit);
        		grandTotalBenefit = String.format("%,.2f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getGrandTotalBenefit .... " + e.getMessage()
                    + " " + agreementId);
        }
    	
    	return grandTotalBenefit;
    }
    
    public String getGrandTotalBenefitForCC()throws JRScriptletException {
    	String grandTotalBenefit="" ;
        try {
        	if(jsonObject!=null)
        	{
        		grandTotalBenefit = checkForValue(jsonObject, "grandTotalBenefit");
        	}
        } catch (Exception e) {
            LOGGER.error("getGrandTotalBenefit .... " + e.getMessage()
                    + " " + agreementId);
        }
    	
    	return grandTotalBenefit;
    }
    
    public String getADDSumAssuredCover()throws JRScriptletException {
    	String addSumAssuredCover="" ;
        try {
        	if(jsonObject!=null)
        	{
        		addSumAssuredCover = checkForValue(jsonObject, "ADDSumAssuredCover");
        		BigDecimal amt = new BigDecimal(addSumAssuredCover);
        		addSumAssuredCover = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getADDSumAssuredCover .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return addSumAssuredCover;
    }
    
    public String getCISumAssuredCover()throws JRScriptletException {
    	String cISumAssuredCover="" ;
        try {
        	if(jsonObject!=null)
        	{
        		cISumAssuredCover = checkForValue(jsonObject, "CISumAssuredCover");
        		BigDecimal amt = new BigDecimal(cISumAssuredCover);
        		cISumAssuredCover = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getCISumAssuredCover .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return cISumAssuredCover;
    }
    
    public String getHBSumAssuredCover()throws JRScriptletException {
    	String hBSumAssuredCover="" ;
        try {
        	if(jsonObject!=null)
        	{
        		hBSumAssuredCover = checkForValue(jsonObject, "HBSumAssuredCover");
        		BigDecimal amt = new BigDecimal(hBSumAssuredCover);
        		hBSumAssuredCover = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getHBSumAssuredCover .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return hBSumAssuredCover;
    }
    
    public String getHSSumAssuredCover()throws JRScriptletException {
    	String hSSumAssuredCover="" ;
        try {
        	if(jsonObject!=null)
        	{
        		hSSumAssuredCover = checkForValue(jsonObject, "HSSumAssuredCover");
        		BigDecimal amt = new BigDecimal(hSSumAssuredCover);
        		hSSumAssuredCover = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getHSSumAssuredCover .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return hSSumAssuredCover;
    }
    
    public String getADDPremiumCover()throws JRScriptletException {
    	String addPremiumCover="" ;
        try {
        	if(jsonObject!=null)
        	{
        		addPremiumCover = checkForValue(jsonObject, "ADDPremiumCover");
        		BigDecimal amt = new BigDecimal(addPremiumCover);
        		addPremiumCover = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getADDPremiumCover .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return addPremiumCover;
    }
    
    public String getCIPremiumCover()throws JRScriptletException {
    	String cIPremiumCover="" ;
        try {
        	if(jsonObject!=null)
        	{
        		cIPremiumCover = checkForValue(jsonObject, "CIPremiumCover");
        		BigDecimal amt = new BigDecimal(cIPremiumCover);
        		cIPremiumCover = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getCIPremiumCover .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return cIPremiumCover;
    }
    
    public String getHSPremiumCover()throws JRScriptletException {
    	String hSPremiumCover="" ;
        try {
        	if(jsonObject!=null)
        	{
        		hSPremiumCover = checkForValue(jsonObject, "HSPremiumCover");
        		BigDecimal amt = new BigDecimal(hSPremiumCover);
        		hSPremiumCover = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getHSPremiumCover .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return hSPremiumCover;
    }
    
    public String getHBPremiumCover()throws JRScriptletException {
    	String hBPremiumCover="" ;
        try {
        	if(jsonObject!=null)
        	{
        		hBPremiumCover = checkForValue(jsonObject, "HBPremiumCover");
        		BigDecimal amt = new BigDecimal(hBPremiumCover);
        		hBPremiumCover = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getHBPremiumCover .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return hBPremiumCover;
    }
    
    public String gettotalPolicyPremium()throws JRScriptletException {
    	String totalPolicyPremium="" ;
        try {
        	if(jsonObject!=null)
        	{
        		totalPolicyPremium = checkForValue(jsonObject, "totalPolicyPremium");
        		BigDecimal amt = new BigDecimal(totalPolicyPremium);
        		totalPolicyPremium = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("gettotalPolicyPremium .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return totalPolicyPremium;
    }
    
    public String getAddRiderSumAssured()throws JRScriptletException{
    	String sumAssured="" ;
        try {
        	if (jsonObject.containsKey("ADDRider")) {
            	sumAssured = checkForValue(getValue(jsonObject, "ADDRider"),"sumAssured");
            	BigDecimal amt = new BigDecimal(sumAssured);
            	sumAssured = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        }catch (Exception e) {
            LOGGER.error("getAddRiderSumAssured .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return sumAssured;
    }
    
    public String getAddSumAssured60Percent()throws JRScriptletException {
    	String addSumAssured60Percent="" ;
        try {
        	if(jsonObject!=null)
        	{
        		addSumAssured60Percent = checkForValue(jsonObject, "ADDSumAssured60Percent");
        		BigDecimal amt = new BigDecimal(addSumAssured60Percent);
        		addSumAssured60Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getAddSumAssured60Percent .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return addSumAssured60Percent;
    }
    
    public String getAddSumAssured25Percent()throws JRScriptletException {
    	String addSumAssured25Percent="" ;
        try {
        	if(jsonObject!=null)
        	{
        		addSumAssured25Percent = checkForValue(jsonObject, "ADDSumAssured25Percent");
        		BigDecimal amt = new BigDecimal(addSumAssured25Percent);
        		addSumAssured25Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getAddSumAssured25Percent .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return addSumAssured25Percent;
    }
    
    public String getAddSumAssured200Percent()throws JRScriptletException {
    	String addSumAssured200Percent="" ;
        try {
        	if(jsonObject!=null)
        	{
        		addSumAssured200Percent = checkForValue(jsonObject, "ADDSumAssured200Percent");
        		BigDecimal amt = new BigDecimal(addSumAssured200Percent);
        		addSumAssured200Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        	}
        } catch (Exception e) {
            LOGGER.error("getAddSumAssured200Percent .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return addSumAssured200Percent;
    }
    
    public String getHS1Details()throws JRScriptletException{
    	String hsDetails=null;
    	JSONArray jsonArray = null;
    	try{
    		if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("HSDetails");
        		hsDetails=jsonArray.get(0).toString();
        		BigDecimal amt = new BigDecimal(hsDetails);
        		hsDetails = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
    		}
    	}catch(Exception e){
            LOGGER.error("getHS1Details .... " + e.getMessage()
                    + " " + agreementId);
    	}
    	return hsDetails;
    }
    
    public String getHS2Details()throws JRScriptletException{
    	String hsDetails=null;
    	JSONArray jsonArray = null;
    	try{
    		if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("HSDetails");
        		hsDetails=jsonArray.get(1).toString();
        		BigDecimal amt = new BigDecimal(hsDetails);
        		hsDetails = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        		
    		}
    	}catch(Exception e){
            LOGGER.error("getHS2Details .... " + e.getMessage()
                    + " " + agreementId);
    	}
    	return hsDetails;
    }
    
    public String getHS3Details()throws JRScriptletException{
    	String hsDetails=null;
    	JSONArray jsonArray = null;
    	try{
    		if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("HSDetails");
        		hsDetails=jsonArray.get(2).toString();
        		BigDecimal amt = new BigDecimal(hsDetails);
        		hsDetails = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
    		}
    	}catch(Exception e){
            LOGGER.error("getHS3Details .... " + e.getMessage()
                    + " " + agreementId);
    	}
    	return hsDetails;
    }
    
    public String getHS4Details()throws JRScriptletException{
    	String hsDetails=null;
    	JSONArray jsonArray = null;
    	try{
    		if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("HSDetails");
        		hsDetails=jsonArray.get(3).toString();
        		BigDecimal amt = new BigDecimal(hsDetails);
        		hsDetails = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
    		}
    	}catch(Exception e){
            LOGGER.error("getHS4Details .... " + e.getMessage()
                    + " " + agreementId);
    	}
    	return hsDetails;
    }
    
    public String getHS5Details()throws JRScriptletException{
    	String hsDetails=null;
    	JSONArray jsonArray = null;
    	try{
    		if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("HSDetails");
        		hsDetails=jsonArray.get(4).toString();
        		BigDecimal amt = new BigDecimal(hsDetails);
        		hsDetails = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
    		}
    	}catch(Exception e){
            LOGGER.error("getHS5Details .... " + e.getMessage()
                    + " " + agreementId);
    	}
    	return hsDetails;
    }
    
    public String getHS6Details()throws JRScriptletException{
    	String hsDetails=null;
    	JSONArray jsonArray = null;
    	try{
    		if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("HSDetails");
        		hsDetails=jsonArray.get(5).toString();
        		BigDecimal amt = new BigDecimal(hsDetails);
        		hsDetails = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
    		}
    	}catch(Exception e){
            LOGGER.error("getHS6Details .... " + e.getMessage()
                    + " " + agreementId);
    	}
    	return hsDetails;
    }
    
    public String getHS7Details()throws JRScriptletException{
    	String hsDetails=null;
    	JSONArray jsonArray = null;
    	try{
    		if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("HSDetails");
        		hsDetails=jsonArray.get(6).toString();
        		BigDecimal amt = new BigDecimal(hsDetails);
        		hsDetails = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
    		}
    	}catch(Exception e){
            LOGGER.error("getHS7Details .... " + e.getMessage()
                    + " " + agreementId);
    	}
    	return hsDetails;
    }
    
    public String getHS8Details()throws JRScriptletException{
    	String hsDetails=null;
    	JSONArray jsonArray = null;
    	try{
    		if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("HSDetails");
        		hsDetails=jsonArray.get(7).toString();
        		BigDecimal amt = new BigDecimal(hsDetails);
        		hsDetails = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
    		}
    	}catch(Exception e){
            LOGGER.error("getHS8Details .... " + e.getMessage()
                    + " " + agreementId);
    	}
    	return hsDetails;
    }
    
    public String getHS9Details()throws JRScriptletException{
    	String hsDetails=null;
    	JSONArray jsonArray = null;
    	try{
    		if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("HSDetails");
        		hsDetails=jsonArray.get(8).toString();
        		BigDecimal amt = new BigDecimal(hsDetails);
        		hsDetails = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
    		}
    	}catch(Exception e){
            LOGGER.error("getAddSumAssured200Percent .... " + e.getMessage()
                    + " " + agreementId);
    	}
    	return hsDetails;
    }
    
    public String getPackagePlan() throws JRScriptletException{
    	String packageNameField="";
    	try {
        	if(jsonObject!=null)
        	{
        		packageNameField = checkForValue(jsonObject, "packageNameField");
        	}
        	if (packageNameField.equals(GeneraliConstants.PACAKGE_PLAN_A)){
        		packageNameField=GeneraliConstants.PACAKGE_PLAN_A_CODE;
        	}else if(packageNameField.equals(GeneraliConstants.PACAKGE_PLAN_B)){
            	packageNameField=GeneraliConstants.PACAKGE_PLAN_B_CODE;
        	}
        } catch (Exception e) {
            LOGGER.error("getPackagePlan .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return packageNameField;
    }
    
    public String getPackagePlanCode() throws JRScriptletException{
    	String packageNameField="";
    	try {
        	if(jsonObject!=null)
        	{
        		packageNameField = checkForValue(jsonObject, "packageNameField");
        	}
        } catch (Exception e) {
            LOGGER.error("getPackagePlanCode .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return packageNameField;
    }
    
    public String getInsuredAgeFieldCC() throws JRScriptletException{
    	String insuredAgeField="";
    	try {
        	if(jsonObject!=null)
        	{
        		insuredAgeField = checkForValue(jsonObject, "insuredAgeField");
        	}
        } catch (Exception e) {
            LOGGER.error("getInsuredAgeFieldCC .... " + e.getMessage()
                    + " " + agreementId);
        }
    	return insuredAgeField;
    }
    
     public List<BenefitIllustrationTable> getBenefitIllusData() {
        JSONArray jsonArray = null;
        List<BenefitIllustrationTable> illusData = new ArrayList<BenefitIllustrationTable>();
        
        String policyYear="";
        String age="";
        String annualisedPremium="";
        String benefitRate="";
        String benefitAmount="";
        String taxDeducAmount="";
        String beforeAnnuityAmount="";
        String afterAnnuityAmount="";
        String totalLivingBenefit="";
        String totalTaxationBenefit="";
        String totalPremiumPaid="";
        String totalRatePercent="";
        String taxRate="";
        String benefitTaxAmount = "";
        String benefitLifeRate="";
        String benefitLifeAmount="";
        
        boolean isGenProLife20=false;
        boolean isGenProLife25=false;
        
        try {
        	if(jsonObject != null) {
        		jsonArray = (JSONArray) jsonObject.get("benefitTableData");
        		//jsonArray.remove(jsonArray.size()-1);
            for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                BenefitIllustrationTable illustrationTableData = new BenefitIllustrationTable();
                                
                if (jsonObj != null) {
                	if (jsonObj.containsKey("policyYear")) {
                		policyYear = checkForValue(jsonObj, "policyYear");
                        illustrationTableData.setPolicyYear(policyYear);
                    }
                    if (jsonObj.containsKey("age")) {
                        age = checkForValue(jsonObj, "age");
                        illustrationTableData.setAge(age);
                    }
                    if(jsonObj.containsKey("annualisedPremium")){
                    	annualisedPremium=checkForValue(jsonObj, "annualisedPremium");
                    	BigDecimal amt = new BigDecimal(annualisedPremium);
                    	annualisedPremium = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	illustrationTableData.setAnnualisedPremium(annualisedPremium);
                    }
                    if(jsonObj.containsKey("benefitRate")){
                    	benefitRate=checkForValue(jsonObj, "benefitRate");
                    	illustrationTableData.setBenefitRate(benefitRate);
                    }
                    if(jsonObj.containsKey("benefitAmount")){
                    	benefitAmount=checkForValue(jsonObj, "benefitAmount");
                    	BigDecimal amt = new BigDecimal(benefitAmount);
                    	benefitAmount = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	illustrationTableData.setBenefitAmount(benefitAmount);
                    }    
                    if(jsonObj.containsKey("taxDeducAmount")){
                    	taxDeducAmount=checkForValue(jsonObj, "taxDeducAmount");
                    	BigDecimal amt = new BigDecimal(taxDeducAmount);
                    	taxDeducAmount = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	illustrationTableData.setTaxDeducAmount(taxDeducAmount);
                    }
                    if(jsonObj.containsKey("beforeAnnuityAmount")){
                    	beforeAnnuityAmount=checkForValue(jsonObj, "beforeAnnuityAmount");
                    	BigDecimal amt = new BigDecimal(beforeAnnuityAmount);
                    	beforeAnnuityAmount = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	illustrationTableData.setBeforeAnnuityAmount(beforeAnnuityAmount);
                    }
                    if(jsonObj.containsKey("afterAnnuityAmount")){
                    	afterAnnuityAmount=checkForValue(jsonObj, "afterAnnuityAmount");
                    	BigDecimal amt = new BigDecimal(afterAnnuityAmount);
                    	afterAnnuityAmount = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	illustrationTableData.setAfterAnnuityAmount(afterAnnuityAmount);
                    }
                    if(jsonObj.containsKey("benefitTaxAmount")){
                    	benefitTaxAmount= checkForValue(jsonObj, "benefitTaxAmount");
                    	illustrationTableData.setBenefitTaxAmount(benefitTaxAmount);
                    }
                    if(jsonObj.containsKey("benefitLifeRate")){
                    	benefitLifeRate= checkForValue(jsonObj, "benefitLifeRate");
                    	illustrationTableData.setBenefitLifeRate(benefitLifeRate);
                    }
                    if(jsonObj.containsKey("benefitLifeAmount")){
                    	benefitLifeAmount= checkForValue(jsonObj, "benefitLifeAmount");
                    	BigDecimal amt = new BigDecimal(benefitLifeAmount);
                    	benefitLifeAmount = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	illustrationTableData.setBenefitLifeAmount(benefitLifeAmount);
                    }
                    if(getPremiumPayingTerm().equals("20")){
                    	isGenProLife20=true;
                    }
                    illustrationTableData.setIsGenProLife20(isGenProLife20);
                    if(getPremiumPayingTerm().equals("25")){
                    	isGenProLife25=true;
                    }
                    illustrationTableData.setIsGenProLife25(isGenProLife25);
                    
                    totalLivingBenefit = checkForValue(jsonObject, "totalLivingBenefit");
                    BigDecimal livingBenefitAmount = new BigDecimal(totalLivingBenefit);
                    totalLivingBenefit = String.format("%,.0f", livingBenefitAmount.setScale(2, RoundingMode.DOWN));
                    illustrationTableData.setTotalLivingBenefit(totalLivingBenefit);
                    
                    totalTaxationBenefit = checkForValue(jsonObject, "totalTaxationBenefit");
                    BigDecimal taxationAmount = new BigDecimal(totalTaxationBenefit);
                    totalTaxationBenefit = String.format("%,.0f", taxationAmount.setScale(2, RoundingMode.DOWN));
                    illustrationTableData.setTotalTaxationBenefit(totalTaxationBenefit);
                    
                    totalPremiumPaid= checkForValue(jsonObject, "totalPremiumPaid");
                    BigDecimal premiumPaidAmount = new BigDecimal(totalPremiumPaid);
                    totalPremiumPaid = String.format("%,.0f", premiumPaidAmount.setScale(2, RoundingMode.DOWN));
                    illustrationTableData.setTotalPremiumPaid(totalPremiumPaid);
                    
                    totalRatePercent= checkForValue(jsonObject, "totalRatePercent");
                    /*String percentSymbol="%";
                    String newRatePercent= totalRatePercent.concat(percentSymbol);*/
                    illustrationTableData.setTotalRatePercent(totalRatePercent);
                    JSONObject policyDetailsJsonObject = getValue(jsonObject, "PolicyDetails");
                    taxRate = checkForValue(policyDetailsJsonObject, "taxRate");
                    illustrationTableData.setTaxRate(taxRate);
                    
                	}
                illusData.add(illustrationTableData);
            	}
        	}
        } catch (Exception e) {
            LOGGER.error("getRiderTable .... " + e.getMessage() + " "
                    + agreementId);
        }
        return illusData;
    }
    
    public List<PolicyValueTableData> getPolicyValueData() {
        JSONArray jsonArray = null;
        List<PolicyValueTableData> policyValueData = new ArrayList<PolicyValueTableData>();
        jsonArray = (JSONArray) jsonObject.get("illustrationTableData");
        String policyYear="";
        String age="";
        String cashValue="";
        String rpuCash="";
        String rpuSA="";
        String extPeriodYear="";
        String extPeriodDay="";
        String etiCash="";
        String etiSA="";
        
        boolean isGenProLife20=false;
        boolean isGenProLife25=false;
        
        try {
            for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                PolicyValueTableData policyValueTableData = new PolicyValueTableData();
                                
                if (jsonObj != null) {
                	if (jsonObj.containsKey("policyYear")) {
                		policyYear = checkForValue(jsonObj, "policyYear");
                        policyValueTableData.setPolicyYear(policyYear);
                    }
                    if (jsonObj.containsKey("age")) {
                        age = checkForValue(jsonObj, "age");
                        policyValueTableData.setAge(age);
                    }
                    if(jsonObj.containsKey("cashValue")){
                    	cashValue=checkForValue(jsonObj, "cashValue");
                    	BigDecimal amt = new BigDecimal(cashValue);
                    	cashValue = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	policyValueTableData.setCashValue(cashValue);
                    }
                    if(jsonObj.containsKey("rpuCash")){
                    	rpuCash=checkForValue(jsonObj, "rpuCash");
                    	BigDecimal amt = new BigDecimal(rpuCash);
                    	rpuCash = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	policyValueTableData.setRpuCash(rpuCash);
                    }
                    if(jsonObj.containsKey("rpuSA")){
                    	rpuSA=checkForValue(jsonObj, "rpuSA");
                    	BigDecimal amt = new BigDecimal(rpuSA);
                    	rpuSA = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	policyValueTableData.setRpuSA(rpuSA);
                    }
                    if(jsonObj.containsKey("extPeriodYear")){
                    	extPeriodYear= checkForValue(jsonObj, "extPeriodYear");
                    	policyValueTableData.setExtPeriodYear(extPeriodYear);
                    }
                    if(jsonObj.containsKey("extPeriodDay")){
                    	extPeriodDay= checkForValue(jsonObj, "extPeriodDay");
                    	policyValueTableData.setExtPeriodDay(extPeriodDay);
                    }
                    if(jsonObj.containsKey("etiCash")){
                    	etiCash= checkForValue(jsonObj, "etiCash");
                    	BigDecimal amt = new BigDecimal(etiCash);
                    	etiCash = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	policyValueTableData.setEtiCash(etiCash);
                    }
                    if(jsonObj.containsKey("etiSA")){
                    	etiSA= checkForValue(jsonObj, "etiSA");
                    	BigDecimal amt = new BigDecimal(etiSA);
                    	etiSA = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	policyValueTableData.setEtiSA(etiSA);
                    }
                    if(getPremiumPayingTerm().equals("20")){
                    	isGenProLife20=true;
                    }
                    policyValueTableData.setIsGenProLife20(isGenProLife20);
                    if(getPremiumPayingTerm().equals("25")){
                    	isGenProLife25=true;
                    }
                    policyValueTableData.setIsGenProLife25(isGenProLife25);
                }
                policyValueData.add(policyValueTableData);
            }
        } catch (Exception e) {
            LOGGER.error("getPolicyValueData .... " + e.getMessage() + " "
                    + agreementId);
        }
        return policyValueData;
    }
    public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	public String indicatorFlag() {
		String test="";
		setFlag(false);
		return test;
	}
	
	public String resetIndicatorFlag() {
		String test="";
		setFlag(true);
		return test;
	}
	
	public String getPremiumPayingTerm()throws JRScriptletException {
		String premiumPayingTerm = "";
        try {
        	if(jsonObject!=null)
            {
        		if (jsonObject.containsKey("PolicyDetails")) {
        			premiumPayingTerm = checkForValue(getValue(jsonObject, "PolicyDetails"),"premiumPayingTerm");
        		}
            }
        } catch (Exception e) {
            LOGGER.error("getPremiumPayingTerm .... " + e.getMessage() + " "
                    + agreementId);
        }
        return premiumPayingTerm;	
	}
	
	public String getRiderPremiumFinal() throws JRScriptletException{
	   String riderPremiumFinal="";
	    	try {
	        	if(jsonObject!=null)
	        	{
	        		riderPremiumFinal = checkForValue(jsonObject, "riderPremiumFinal");
	        		BigDecimal amt = new BigDecimal(riderPremiumFinal);
	        		riderPremiumFinal = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
	        	}
	        } catch (Exception e) {
	            LOGGER.error("getRiderPremiumFinal .... " + e.getMessage()
	                    + " " + agreementId);
	        }
	    return riderPremiumFinal;
	 }
	
	public String getSumAssured150PerField() throws JRScriptletException{
		String sumAssured150PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured150PerField = checkForValue(jsonObject, "sumAssured150PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured150PerField);
		        		sumAssured150PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured150PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured150PerField;
	}
	
	public String getinsuredAge0Field() throws JRScriptletException{
		String insuredAge0Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge0Field = checkForValue(jsonObject, "insuredAge0Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge0Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge0Field;
	}
	
	public String getinsuredAge1Field() throws JRScriptletException{
		String insuredAge1Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge1Field = checkForValue(jsonObject, "insuredAge1Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge1Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge1Field;
	}
	
	public String getinsuredAge10Field() throws JRScriptletException{
		String insuredAge10Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge10Field = checkForValue(jsonObject, "insuredAge10Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge10Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge10Field;
	}

	public String getinsuredAge20Field() throws JRScriptletException{
		String insuredAge20Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge20Field = checkForValue(jsonObject, "insuredAge20Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge20Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge20Field;
	}
	
	public String getSumAssured30PerField() throws JRScriptletException{
		String sumAssured30PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured30PerField = checkForValue(jsonObject, "sumAssured30PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured30PerField);
		        		sumAssured30PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured30PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured30PerField;
	}
	
	public String getSumAssured130PerField() throws JRScriptletException{
		String sumAssured130PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured130PerField = checkForValue(jsonObject, "sumAssured130PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured130PerField);
		        		sumAssured130PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured130PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured130PerField;
	}
	
	public String getBasePremiumFinal() throws JRScriptletException{
		String basePremiumFinal="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		basePremiumFinal = checkForValue(jsonObject, "basePremiumFinal");
		        		BigDecimal amt = new BigDecimal(basePremiumFinal);
		        		basePremiumFinal = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getBasePremiumFinal .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return basePremiumFinal;
	}
	public String getinsuredAge5Field() throws JRScriptletException{
		String insuredAge5Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge5Field = checkForValue(jsonObject, "insuredAge5Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge5Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge5Field;
	}
	
	public String getinsuredAge15Field() throws JRScriptletException{
		String insuredAge15Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge15Field = checkForValue(jsonObject, "insuredAge15Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge15Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge15Field;
	}
	
	public String getInsuredAge25Field() throws JRScriptletException{
		String insuredAge25Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge25Field = checkForValue(jsonObject, "insuredAge25Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getInsuredAge25Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge25Field;
	}
	
	public String getSumAssured4PerField() throws JRScriptletException{
		String sumAssured4PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured4PerField = checkForValue(jsonObject, "sumAssured4PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured4PerField);
		        		sumAssured4PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured4PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured4PerField;
	}
	
	public String getSumAssured8PerField() throws JRScriptletException{
		String sumAssured8PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured8PerField = checkForValue(jsonObject, "sumAssured8PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured8PerField);
		        		sumAssured8PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured8PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured8PerField;
	}
	
	public String getSumAssured13PerField() throws JRScriptletException{
		String sumAssured13PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured13PerField = checkForValue(jsonObject, "sumAssured13PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured13PerField);
		        		sumAssured13PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured13PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured13PerField;
	}

	public String getSumAssured16PerField() throws JRScriptletException{
		String sumAssured16PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured16PerField = checkForValue(jsonObject, "sumAssured16PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured16PerField);
		        		sumAssured16PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured16PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured16PerField;
	}
	
	public String getSumAssuredADBRider()throws JRScriptletException{
		String sumAssured = "";
        try {
        	if(jsonObject!=null)
            {
        		if (jsonObject.containsKey("ADBRider")) {
        			sumAssured = checkForValue(getValue(jsonObject, "ADBRider"),"sumAssured");
        			BigDecimal amt = new BigDecimal(sumAssured);
        			sumAssured = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        		}
            }
        } catch (Exception e) {
            LOGGER.error("getSumAssuredADBRider .... " + e.getMessage() + " "
                    + agreementId);
        }
        return sumAssured;	
	}
	
	public String ADBSumAssured200Percent()throws JRScriptletException{
		String ADBSumAssured200Percent = "";
        try {
        	if(jsonObject!=null)
            {
        			ADBSumAssured200Percent = checkForValue(jsonObject,"ADBSumAssured200Percent");
        			BigDecimal amt = new BigDecimal(ADBSumAssured200Percent);
        			ADBSumAssured200Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
            }
        } catch (Exception e) {
            LOGGER.error("ADBSumAssured200Percent .... " + e.getMessage() + " "
                    + agreementId);
        }
        return ADBSumAssured200Percent;	
	}
	
	public String getSumAssuredADDRider()throws JRScriptletException{
		String sumAssured = "";
        try {
        	if(jsonObject!=null)
            {
        		if (jsonObject.containsKey("ADDRiders")) {
        			sumAssured = checkForValue(getValue(jsonObject, "ADDRiders"),"sumAssured");
        			BigDecimal amt = new BigDecimal(sumAssured);
        			sumAssured = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        		}
            }
        } catch (Exception e) {
            LOGGER.error("getSumAssuredADDRider .... " + e.getMessage() + " "
                    + agreementId);
        }
        return sumAssured;	
	}
	
	public String getSumAssuredAIRider()throws JRScriptletException{
		String sumAssured = "";
        try {
        	if(jsonObject!=null)
            {
        		if (jsonObject.containsKey("AIRider")) {
        			sumAssured = checkForValue(getValue(jsonObject, "AIRider"),"sumAssured");
        			BigDecimal amt = new BigDecimal(sumAssured);
        			sumAssured = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
        		}
            }
        } catch (Exception e) {
            LOGGER.error("getSumAssuredAIRider .... " + e.getMessage() + " "
                    + agreementId);
        }
        return sumAssured;	
	}
	
	public String getAISumAssured60Percent()throws JRScriptletException{
		String aISumAssured60Percent = "";
        try {
        	if(jsonObject!=null)
            {
        			aISumAssured60Percent = checkForValue(jsonObject,"AISumAssured60Percent");
        			BigDecimal amt = new BigDecimal(aISumAssured60Percent);
        			aISumAssured60Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
            }
        } catch (Exception e) {
            LOGGER.error("getAISumAssured60Percent .... " + e.getMessage() + " "
                    + agreementId);
        }
        return aISumAssured60Percent;	
	}
	
	public String getAISumAssured50Percent()throws JRScriptletException{
		String aISumAssured50Percent = "";
        try {
        	if(jsonObject!=null)
            {
        			aISumAssured50Percent = checkForValue(jsonObject,"AISumAssured50Percent");
        			BigDecimal amt = new BigDecimal(aISumAssured50Percent);
        			aISumAssured50Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
            }
        } catch (Exception e) {
            LOGGER.error("getAISumAssured50Percent .... " + e.getMessage() + " "
                    + agreementId);
        }
        return aISumAssured50Percent;	
	}
	
	public String getAISumAssured6Percent()throws JRScriptletException{
		String aISumAssured6Percent = "";
        try {
        	if(jsonObject!=null)
            {
        			aISumAssured6Percent = checkForValue(jsonObject,"AISumAssured6Percent");
        			BigDecimal amt = new BigDecimal(aISumAssured6Percent);
        			aISumAssured6Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
            }
        } catch (Exception e) {
            LOGGER.error("getAISumAssured6Percent .... " + e.getMessage() + " "
                    + agreementId);
        }
        return aISumAssured6Percent;	
	}
	
	public String getAISumAssured2Percent()throws JRScriptletException{
		String aISumAssured2Percent = "";
        try {
        	if(jsonObject!=null)
            {
        			aISumAssured2Percent = checkForValue(jsonObject,"AISumAssured2Percent");
        			BigDecimal amt = new BigDecimal(aISumAssured2Percent);
        			aISumAssured2Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
            }
        } catch (Exception e) {
            LOGGER.error("getAISumAssured2Percent .... " + e.getMessage() + " "
                    + agreementId);
        }
        return aISumAssured2Percent;	
	}
	
	public String getAISumAssured10Percent()throws JRScriptletException{
		String aISumAssured10Percent = "";
        try {
        	if(jsonObject!=null)
            {
        			aISumAssured10Percent = checkForValue(jsonObject,"AISumAssured10Percent");
        			BigDecimal amt = new BigDecimal(aISumAssured10Percent);
        			aISumAssured10Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
            }
        } catch (Exception e) {
            LOGGER.error("getAISumAssured10Percent .... " + e.getMessage() + " "
                    + agreementId);
        }
        return aISumAssured10Percent;	
	}
	
	public String getAISumAssured3Percent()throws JRScriptletException{
		String aISumAssured3Percent = "";
        try {
        	if(jsonObject!=null)
            {
        			aISumAssured3Percent = checkForValue(jsonObject,"AISumAssured3Percent");
        			BigDecimal amt = new BigDecimal(aISumAssured3Percent);
        			aISumAssured3Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
            }
        } catch (Exception e) {
            LOGGER.error("getAISumAssured3Percent .... " + e.getMessage() + " "
                    + agreementId);
        }
        return aISumAssured3Percent;	
	}
	
	public String getAISumAssured200Percent()throws JRScriptletException{
		String aISumAssured200Percent = "";
        try {
        	if(jsonObject!=null)
            {
        			aISumAssured200Percent = checkForValue(jsonObject,"AISumAssured200Percent");
        			BigDecimal amt = new BigDecimal(aISumAssured200Percent);
        			aISumAssured200Percent = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
            }
        } catch (Exception e) {
            LOGGER.error("getAISumAssured200Percent .... " + e.getMessage() + " "
                    + agreementId);
        }
        return aISumAssured200Percent;	
	}
	
	public List<SelectedRiderTableData> getSelectedRiderValues() {
        JSONArray jsonArray = null;
        List<SelectedRiderTableData> selectedRidersTable = new ArrayList<SelectedRiderTableData>();
        jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
        String riderNameForPdf="";
        String translatedRiderName="";
        String riderSumAssured="";
        String riderPremium="";
        
        try {
            for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                SelectedRiderTableData selectedRiders = new SelectedRiderTableData();
                                
                if (jsonObj != null) {
                	if (jsonObj.containsKey("riderNameForPdf")) {
                		riderNameForPdf = checkForValue(jsonObj, "riderNameForPdf");
                		translatedRiderName= translateRiderName(riderNameForPdf);
                		selectedRiders.setRiderNameForPdf(translatedRiderName);
                    }
                	if (jsonObj.containsKey("riderSumAssured")) {
                    	riderSumAssured = checkForValue(jsonObj, "riderSumAssured");
                    	if(!riderSumAssured.isEmpty()){
                    		BigDecimal amt = new BigDecimal(riderSumAssured);
                    		riderSumAssured = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                        	selectedRiders.setRiderSumAssured(riderSumAssured);
                    	}
                    	else{
                    		selectedRiders.setRiderSumAssured(riderSumAssured);
                    	}
                    } 

                    if(jsonObj.containsKey("riderPremium")){
                    	riderPremium= checkForValue(jsonObj, "riderPremium");
                   
                    	
                    	if(riderNameForPdf.equals(GeneraliConstants.WP_Rider))
                    		{
                    		riderSumAssured = checkForValue(jsonObj, "riderSumAssured");
                    		int sumInsured=Integer.parseInt(riderSumAssured);
                    		if(sumInsured<4000000){
                    		riderPremium=GeneraliConstants.WP_RiderPremium_Free;
                    		}
                    		else{
                    				BigDecimal amt = new BigDecimal(riderPremium);
                    				riderPremium = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN)).concat("**");
                    			}
                    	}
                    	else{
                        	BigDecimal amt = new BigDecimal(riderPremium);
                        	riderPremium = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
                    	}
                    	selectedRiders.setRiderPremium(riderPremium);
                    }
               }
                	selectedRidersTable.add(selectedRiders);
            }
        } catch (Exception e) {
            LOGGER.error("getSelectedRiderValues .... " + e.getMessage() + " "
                    + agreementId);
        }
        return selectedRidersTable;
    }
	
	public String translateRiderName(String riderNameForPdf)throws JRScriptletException{
		String name="";
		if(riderNameForPdf.equals(GeneraliConstants.WP_Rider)){
			name = GeneraliConstants.WP_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.ADB_Rider)){
			name = GeneraliConstants.ADB_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.ADBRCC_Rider)){
			name = GeneraliConstants.ADBRCC_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.ADD_Rider)){
			name = GeneraliConstants.ADD_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.ADDRCC_Rider)){
			name = GeneraliConstants.ADDRCC_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.AI_Rider)){
			name = GeneraliConstants.AI_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.AIRCC_Rider)){
			name = GeneraliConstants.AIRCC_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.HB_Rider)){
			name = GeneraliConstants.HB_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.HS_Extra_Rider)){
			name = GeneraliConstants.HS_Extra_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.DD_2551_Rider)){
			name = GeneraliConstants.DD_2551_Rider_Thai;
		}else if(riderNameForPdf.equals(GeneraliConstants.PB_Rider)){
			name = GeneraliConstants.PB_Rider_Thai;
		}
		return name;
	}
	
	public boolean isSelectedRidersAppicable() throws JRScriptletException {
		boolean flag=false;
		JSONArray jsonArray = null;
		jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
		try{
                if(!jsonArray.isEmpty()){
                	flag=true;	
			}
		}catch (Exception e) {
            LOGGER.error("isSelectedRidersAppicable .... " + e.getMessage() + " "
                    + agreementId);
        }
		return flag;
	}
	
	
	public boolean isADBRiderAppicable() throws JRScriptletException {
		boolean flag=false;
		JSONArray jsonArray = null;
		String planName="";
		jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
		try{
			for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                if(jsonObj!=null && jsonObj.containsKey("planName")){
                		planName=checkForValue(jsonObj, "planName");
                		if(planName.equals(GeneraliConstants.ADBName)){
                			flag=true;
                			break;
                	}
                }
			}
		}catch (Exception e) {
            LOGGER.error("isADBRiderAppicable .... " + e.getMessage() + " "
                    + agreementId);
        }
		return flag;
	}
	
	public boolean isADDRiderAppicable() throws JRScriptletException {
		boolean flag=false;
		JSONArray jsonArray = null;
		String planName="";
		jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
		try{
			for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                if(jsonObj!=null && jsonObj.containsKey("planName")){
                		planName=checkForValue(jsonObj, "planName");
                		if(planName.equals(GeneraliConstants.ADDName)){
                			flag=true;
                			break;
                	}
                }
			}
		}catch (Exception e) {
            LOGGER.error("isADDRiderAppicable .... " + e.getMessage() + " "
                    + agreementId);
        }
		return flag;
	}
	
	public boolean isAIRiderAppicable() throws JRScriptletException {
		boolean flag=false;
		JSONArray jsonArray = null;
		String planName="";
		jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
		try{
			for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                if(jsonObj!=null && jsonObj.containsKey("planName")){
                		planName=checkForValue(jsonObj, "planName");
                		if(planName.equals(GeneraliConstants.AIName)){
                			flag=true;
                			break;
                	}
                }
			}
		}catch (Exception e) {
            LOGGER.error("isAIRiderAppicable .... " + e.getMessage() + " "
                    + agreementId);
        }
		return flag;
	}
	
	public boolean isWPRiderAppicable() throws JRScriptletException {
		boolean flag=false;
		JSONArray jsonArray = null;
		String planName="";
		jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
		try{
			for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                if(jsonObj!=null && jsonObj.containsKey("planName")){
                		planName=checkForValue(jsonObj, "planName");
                		if(planName.equals(GeneraliConstants.WPName)){
                			flag=true;
                			break;
                	}
                }
			}
		}catch (Exception e) {
            LOGGER.error("isWPRiderAppicable .... " + e.getMessage() + " "
                    + agreementId);
        }
		return flag;
	}
	
	public boolean isHBRiderAppicable() throws JRScriptletException {
		boolean flag=false;
		JSONArray jsonArray = null;
		String planName="";
		jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
		try{
			for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                if(jsonObj!=null && jsonObj.containsKey("planName")){
                		planName=checkForValue(jsonObj, "planName");
                		if(planName.equalsIgnoreCase(GeneraliConstants.HB_AName)){
                			flag=true;
                			break;
                	}
                }
			}
		}catch (Exception e) {
            LOGGER.error("isHBRiderAppicable .... " + e.getMessage() + " "
                    + agreementId);
        }
		return flag;
	}
	
	public boolean isHSExtraRiderAppicable() throws JRScriptletException {
		boolean flag=false;
		JSONArray jsonArray = null;
		String planName="";
		jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
		try{
			for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                if(jsonObj!=null && jsonObj.containsKey("planName")){
                		planName=checkForValue(jsonObj, "planName");
                		if(planName.equalsIgnoreCase(GeneraliConstants.HS_ExtraName)){
                			flag=true;
                			break;
                	}
                }
			}
		}catch (Exception e) {
            LOGGER.error("isHSExtraRiderAppicable .... " + e.getMessage() + " "
                    + agreementId);
        }
		return flag;
	}
	
	public boolean isDD2551ExtraRiderAppicable() throws JRScriptletException {
		boolean flag=false;
		JSONArray jsonArray = null;
		String planName="";
		jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
		try{
			for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                if(jsonObj!=null && jsonObj.containsKey("planName")){
                		planName=checkForValue(jsonObj, "planName");
                		if(planName.equalsIgnoreCase(GeneraliConstants.DD_2551Name)){
                			flag=true;
                			break;
                	}
                }
			}
		}catch (Exception e) {
            LOGGER.error("isDD2551ExtraRiderAppicable .... " + e.getMessage() + " "
                    + agreementId);
        }
		return flag;
	}
	
	public boolean isPBRiderAppicable() throws JRScriptletException {
		boolean flag=false;
		JSONArray jsonArray = null;
		String planName="";
		jsonArray = (JSONArray) jsonObject.get("selectedRiderTbl");
		try{
			for (Object obj : jsonArray) {
                JSONObject jsonObj = (JSONObject) obj;
                if(jsonObj!=null && jsonObj.containsKey("planName")){
                		planName=checkForValue(jsonObj, "planName");
                		if(planName.equalsIgnoreCase(GeneraliConstants.PBName)){
                			flag=true;
                			break;
                	}
                }
			}
		}catch (Exception e) {
            LOGGER.error("isPBRiderAppicable .... " + e.getMessage() + " "
                    + agreementId);
        }
		return flag;
	}
	
	
	public String getinsuredAge13Field() throws JRScriptletException{
		String insuredAge13Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge13Field = checkForValue(jsonObject, "insuredAge13Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge13Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge13Field;
	}
	
	public String getinsuredAge14Field() throws JRScriptletException{
		String insuredAge14Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge14Field = checkForValue(jsonObject, "insuredAge14Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge14Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge14Field;
	}
	
	public String getinsuredAge16Field() throws JRScriptletException{
		String insuredAge16Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge16Field = checkForValue(jsonObject, "insuredAge16Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge16Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge16Field;
	}
	
	public String getinsuredAge17Field() throws JRScriptletException{
		String insuredAge17Field="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		insuredAge17Field = checkForValue(jsonObject, "insuredAge17Field");
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getinsuredAge17Field .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return insuredAge17Field;
	}
	public String getSumAssured110PerField() throws JRScriptletException{
		String sumAssured110PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured110PerField = checkForValue(jsonObject, "sumAssured110PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured110PerField);
		        		sumAssured110PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured110PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured110PerField;
	}
	public String getSumAssured120PerField() throws JRScriptletException{
		String sumAssured120PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured120PerField = checkForValue(jsonObject, "sumAssured120PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured120PerField);
		        		sumAssured120PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured120PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured120PerField;
	}
	
	public String getSumAssured140PerField() throws JRScriptletException{
		String sumAssured140PerField="";
		    	try {
		        	if(jsonObject!=null)
		        	{
		        		sumAssured140PerField = checkForValue(jsonObject, "sumAssured140PerField");
		        		BigDecimal amt = new BigDecimal(sumAssured140PerField);
		        		sumAssured140PerField = String.format("%,.0f", amt.setScale(2, RoundingMode.DOWN));
		        	}
		        } catch (Exception e) {
		            LOGGER.error("getSumAssured140PerField .... " + e.getMessage()
		                    + " " + agreementId);
		        }
		    return sumAssured140PerField;
	}
	
	 public String getOccupationClass() throws JRScriptletException {
	        String occClass = "";
	        try {
	            if (insuredPerson != null) {
	                Set<PersonDetail> payerPersonDetails = insuredPerson.getDetail();
	                if (payerPersonDetails != null) {
	                    for (PersonDetail personDetail : payerPersonDetails) {
	                        if (personDetail instanceof OccupationDetail) {
	                            OccupationDetail payerOccupationDetail = (OccupationDetail) personDetail;
	                            if (payerOccupationDetail != null) {
	                                occClass = payerOccupationDetail
	                                        .getOccupationClass();
	                            }
	                        }
	                    }
	                }

	            }
	        } catch (Exception e) {
	            LOGGER.error(
	                    "Error in getting the occupation class of policyHolder for "
	                            + agreementId, e);
	        }
	        return occClass;

	    }
	 public String getSignatureModifiedDate() throws JRScriptletException	 {
		 String finalSignatureModifiedDate="";
		 try {
			 	String initialSignatureModifiedDate ="";
				initialSignatureModifiedDate = agreement.getTransactionKeys().getKey14();
			    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			    Date modifiedDate = sdf.parse(initialSignatureModifiedDate);
			    Calendar calendar = new GregorianCalendar();
			    calendar.setTime(modifiedDate);
			    calendar.add(Calendar.YEAR, 543);
			    SimpleDateFormat finalsdf = new SimpleDateFormat("dd/MM/yyyy");
		    	finalSignatureModifiedDate = finalsdf.format(calendar.getTime());
		 	}catch (Exception e) {
	            LOGGER.error(
	                    "Error in getting the signature date "
	                            + agreementId, e);
	        }
		return finalSignatureModifiedDate;
	 }
	 
	 public boolean isHBRiderApplicable() throws JRScriptletException
     {
		 	boolean flag=false;
            String isRequiredFlag="";
            try {
            	if (jsonObject.containsKey("HBRider")) {
            		 isRequiredFlag = checkForValue( getValue(jsonObject, "HBRider"),"isRequired");
                    }if(isRequiredFlag.equals("Yes")){ 
                          flag=true;
               }
          }
            catch (Exception e) {
                 LOGGER.error("Error in getting isRequired "+ agreementId, e);
             }
            return flag;
     }
}