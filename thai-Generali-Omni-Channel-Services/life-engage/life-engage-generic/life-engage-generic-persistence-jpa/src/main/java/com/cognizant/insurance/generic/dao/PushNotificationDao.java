/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 19, 2015
 */
package com.cognizant.insurance.generic.dao;

import java.util.List;

import com.cognizant.insurance.dao.Dao;
import com.cognizant.insurance.pushnotification.ApplicationDetails;
import com.cognizant.insurance.pushnotification.DeviceRegister;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;

/**
 * The Interface interface PushNotificationDao.
 */
public interface PushNotificationDao extends Dao {

    /**
     * Register device.
     * 
     * @param request
     *            the request
     * @return the string
     */
    void unRegisterDevice(Request<DeviceRegister> request);

    /**
     * Checks if is device id exists.
     * 
     * @param request
     *            the request
     * @return true, if is device id exists
     */
    boolean isDeviceIdExists(Request<DeviceRegister> request);

    /**
     * Gets the device details.
     * 
     * @param request
     *            the request
     * @return the device details
     */
    Response<List<DeviceRegister>> getDeviceDetails(Request<DeviceRegister> request);

    /**
     * Gets the application credentialss.
     * 
     * @param request
     *            the request
     * @return the application credentialss
     */
    Response<List<ApplicationDetails>> getApplicationDetails(Request<ApplicationDetails> request);

}
