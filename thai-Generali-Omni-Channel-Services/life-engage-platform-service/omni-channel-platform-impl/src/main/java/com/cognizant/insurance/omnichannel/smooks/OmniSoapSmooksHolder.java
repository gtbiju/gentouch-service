package com.cognizant.insurance.omnichannel.smooks;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;
import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.milyn.Smooks;
import org.milyn.container.ExecutionContext;
import org.milyn.payload.JavaResult;
import org.milyn.payload.JavaSource;
import org.milyn.validation.OnFailResult;
import org.milyn.validation.ValidationResult;
import org.xml.sax.SAXException;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.google.common.base.Strings;

public class OmniSoapSmooksHolder {

    /** The xml to bo smooks. */
    private Smooks xmlToBoSmooks;

    /** The bo to xml smooks. */
    private Smooks boToXmlSmooks;

    /**
     * Instantiates a new smooks holder.
     */
    public OmniSoapSmooksHolder() {

    }


    /**
     * Sets the xml to bo mapping file.
     *
     * @param xmlToBoMappingFile the xml to bo mapping file to set.
     */
    public final void setXmlToBoMappingFile(final String xmlToBoMappingFile) {
        try {
            xmlToBoSmooks = new Smooks(xmlToBoMappingFile);
        } catch (IOException e) {
            throw new SystemException(e);
        } catch (SAXException e) {
            throw new SystemException(e);
        }
    }

   
    /**
     * Sets the bo to xml mapping file.
     *
     * @param boToXmlMappingFile the bo to xml mapping file to set.
     */
    public final void setBoToXmlMappingFile(final String boToXmlMappingFile) {
        try {
            boToXmlSmooks = new Smooks(boToXmlMappingFile);
        } catch (IOException e) {
            throw new SystemException(e);
        } catch (SAXException e) {
            throw new SystemException(e);
        }

    }

    /**
     * Parses the bo.
     * 
     * @param o
     *            the o
     * @return the string
     */
    public final String parseBO(final Object o) {

        ExecutionContext executionContext = boToXmlSmooks.createExecutionContext();
        StringWriter writer = new StringWriter();
        /*try {
			executionContext.setEventListener(new HtmlReportGenerator("smooks-report.html"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
        executionContext.getBeanContext().addBean(Constants.RESULT_OBJECT, o);
        boToXmlSmooks.filterSource(executionContext, new JavaSource(o), new StreamResult(writer));
        return writer.toString();

    }

    /**
     * Parses the inputXML.
     * 
     * @param inputXML
     *            the input XML
     * @return the object
     * @throws BusinessException
     *             the business exception
     */
    public final Object parseXml(String inputXML) throws BusinessException {
        if (!Strings.isNullOrEmpty(inputXML)) {
            inputXML = inputXML.replaceAll(Constants.REG_EXPRESSION, Constants.REPLACEMENT);
        }
        JavaResult result = new JavaResult();
        ExecutionContext executionContext = xmlToBoSmooks.createExecutionContext();
        final ValidationResult validationResult = new ValidationResult();

        xmlToBoSmooks.filterSource(executionContext, new StreamSource(new ByteArrayInputStream(inputXML.getBytes())),
                validationResult, result);

        StringBuilder systemExBuilder = new StringBuilder();
        StringBuilder businessExBuilder = new StringBuilder();

        if (CollectionUtils.isNotEmpty(validationResult.getErrors())) {

            for (OnFailResult validationError : validationResult.getErrors()) {

                String[] errorMessage = validationError.getMessage().split(":");
                if (StringUtils.equals(Constants.ERROR_MESSAGE_S, StringUtils.trim(errorMessage[0]))) {
                    systemExBuilder.append(errorMessage[1]).append(":").append(errorMessage[2]).append(",");
                } else if (StringUtils.equals(Constants.ERROR_MESSAGE_C, StringUtils.trim(errorMessage[0]))) {
                    businessExBuilder.append(errorMessage[1]).append(":").append(errorMessage[2]).append(",");
                }
            }

            throwBusinessException(businessExBuilder.length() > 0, businessExBuilder.toString());
            throwSystemException(systemExBuilder.length() > 0, systemExBuilder.toString());
        }

        return result.getBean(Constants.RESULT);

    }
    public final void parseBO(final Object o,Result result) {

        ExecutionContext executionContext = boToXmlSmooks.createExecutionContext();
        executionContext.getBeanContext().addBean(Constants.RESULT_OBJECT, o);
        boToXmlSmooks.filterSource(executionContext, new JavaSource(o), result);
        
    }

}
