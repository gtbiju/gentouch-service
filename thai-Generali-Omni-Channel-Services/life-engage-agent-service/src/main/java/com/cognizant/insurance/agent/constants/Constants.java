/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Feb 3, 2014
 */
package com.cognizant.insurance.agent.constants;

/**
 * The Class class Constants.
 * 
 * @author 300797
 */
public final class Constants {
    /**
     * Make sure that utility classes (classes that contain only static methods) do not have a public constructor.
     */
    private Constants() {

    }
    /**
     * Mail constants
     */
    public static final String TYPE = "Type";
    
    public static final String EAPP = "eApp";
    
    public static final String ILLUSTRATION = "illustration";

    public static final String FNA = "FNA";
    
    public static final String PDF_NAME = "pdfName";


    /**
     * Status
     */
    public static final String SUCCESS = "Success";

    public static final String FAILURE = "Failure";

    public static final String STATUS_UPDATE = "update";

    public static final String STATUS_SAVE = "save";

    public static final String STPSTATUS = "STPStatus";

    public static final String VALIDATION_RESULTS = "ValidationResults";

    public static final String REJECTED = "Rejected";
    
    public static final String CANCELLED = "Cancelled";
	 
	 /** The Constant ERROR_CODE_402. */
    public static final String ERROR_CODE_402 = "402";
	
	 /** The Constant NOTIFICATION_ERROR. */
    public static final String NOTIFICATION_ERROR =
            "Error occured in the process of pushing notifications to Amazon SNS server";
			 /** The Constant MESSAGE_FAILED. */
    public static final String MESSAGE_FAILED = "Message sending failed";

    /** The Constant ERROR_CODE_400. */
    public static final String ERROR_CODE_400 = "400";
	  /** The Constant INVALID_INFORMATION. */
    public static final String INVALID_DEVICE_INFORMATION = "Unable to get device information";
    
    public static final String INVALID_APPLICATION_INFORMATION = "Unable to get application information";

    /**
     * Exception messages
     */
    public static final String PROCESSED_TRANSACTION = "This Trasaction Data is already processed";

    public static final String STPRULES_FAILED = "Failed to satisfy STPRules : ";

    public static final String OBSERVATION_FAILURE = "Error Saving Observation : ";

    public static final String FAILED_DIRECTORY_CREATION = "Failed to create folder : ";

    public static final String DOCUMENT_DETAILS_MISSING = "Document details is missing";

    public static final String NO_PROPOSAL_EXIST = "No proposal with given proposal number";

    public static final String OUTPUT_STREAM_IO_ERROR = "IOError writing file to output stream";

    public static final String DUPLICATE_LEAD = "Duplicate Lead";

    public static final String AGENTID_MISSING = "AgentId is missing";
    
    public static final String EMAIL_DETAILS_MISSING = "Email Details is missing";
    
    public static final String EMAIL_DETAILS_SAVED = "Email Details Saved";
    
    public static final String FILE_DETAILS_MISSING = "File details is missing";

    
    public static final String REQUIREMENT_DETAILS_MISSING = "Requirement details is missing";

    /**
     * STP Rules constants
     */
    public static final String RUN_STPRULES_RULE = "STPRules";

    public static final String CLIENT_ID_INS = "INS";

    public static final String RULE_CATEGORY = "STPRule";

    public static final String ILLUSTRATION_RULE_NAME = "illustrationRuleName";

    public static final String VALIDATION_RULE_INPUT = "validationRuleName";

    public static final String ILLUSTRATION_RULE_GROUP = "illustrationRuleGroup";

    public static final String VALIDATION_RULE_GROUP = "validationRuleGroup";

    public static final String RULE_INPUT = "ruleInput";

    public static final String RULE_NAME = "ruleName";

    public static final String RULE_GROUP = "ruleGroup";

    public static final String RESULT_OBJECT = "resultObject";

    public static final String RESULT = "Result";

    public static final String REG_EXPRESSION = "&";

    public static final String REPLACEMENT = "&amp;";

    public static final String ERROR_MESSAGE_S = "S";

    public static final String ERROR_MESSAGE_C = "C";

    public static final String NULL = "null";

    public static final String UPDATE = "update";

    public static final String TRANSACTION_DATA = "TransactionData";

    public static final String TEMPLATE_ID = "templateId";

    public static final String TEMPLATE_ID_DEFAULT = "321";

    public static final String TEMPLATE_TYPE_DEFAULT = "EAPP";

    public static final String TEMPLATE_TYPE = "templateType";

    public static final String KEY6 = "Key6";

    public static final String KEY3 = "Key3";

    public static final String TRANS_TRACKING_ID = "TransTrackingID";

    public static final String ILLUSTRATION_CLIENTID_DEFAULT = "LE";

    /**
     * Search criteria constants
     */
    public static final String LANDING_DASHBOARD_COUNT = "LandingDashBoardCount";

    public static final String SEARCHCRITERIAREQUEST = "searchCriteriaRequest";

    public static final String KEY11 = "Key11";

    public static final String MODES = "modes";
    
    public static final String IDS = "selectedIds";

    public static final String COMMAND = "command";

    public static final String VALUE = "value";

    public static final String LISTING_DASHBOARD = "ListingDashBoard";
    
    public static final String EMAIL_TRIGGERING = "EmailTriggering";

    /**
     * Request and Response types
     */
    public static final String TYPE_APPLICATION_JSON = "application/json";

    public static final String TYPE_APPLICATION_JSON_UTF8 = "application/json;charset=utf-8";

    public static final String TYPE_APPLICATION_PDF = "application/pdf";

    public static final String TRUE = "true";

    public static final String SERVER = "server";

    public static final String DEFAULT_DATE = "Mon Jan 01 01:01:01 IST 1900";
    
    /*Operations*/
    
    public static final String SAVE="save";
    public static final String SYNC="sync";
    public static final String FIRST_TIME_SYNC="firstTimeSync";
    public static final String GET_STATUS="getStatus";
    public static final String DELETE="delete";
    public static final String DUPLICATE_CHECK="duplicateCheck";
    public static final String UPDATE_OP="update";
    public static final String RETRIEVE="retrieve";
    public static final String RETRIEVE_ALL="retrieveAll";
    public static final String RETRIEVE_BY_FILTER="retrieveByFilter";
    public static final String RETRIEVE_BY_COUNT="retrieveByCount";
    
	/** Push notification **/
	/** The Constant ENABLE. */
    public static final String ENABLE = "E";

    /** The Constant DISABLE. */
    public static final String DISABLE = "D";

    /** The Constant GCM. */
    public static final String GCM = "GCM";

    /** The Constant APNS. */
    public static final String APNS = "APNS";

    /** The Constant APNS_SANDBOX. */
    public static final String APNS_SANDBOX = "APNS_SANDBOX";
    
    /** The Constant NOTIFICATION_FAILED. */
    public static final String NOTIFICATION_FAILED = "Notification sending failed";
    
    /** The Constant NOTIFICATION_SUCCESS. */
    public static final String NOTIFICATION_SUCCESS = "Notification(s) Dispatched Successfully";
    
    /** The Constant DATEFORMAT_YYYY_MM. */
    public static final String DATEFORMAT_YYYY_MM = "yyyy-MM-dd";
    
    /** Push notification flag**/
    public static final String KEY1 = "Key1";
    
    public static final String KEY16 = "Key16";
    
    public static final String KEY25 = "Key25";
    
    public static final String TRANSACTIONDATA = "TransactionData";
    
    public static final String APPLICATION_NAME = "ApplicationName";
    
    /*Lead reassignment*/
    
    public static final String RM_REASSIGN = "rm_reassign";
    
    /*Payment Validation*/
    
    public static final String CASH = "Cash";
    
    public static final String CARD = "Card";
    
    public static final String MANUALBANKING = "Manual bank deposit";
    
    public static final String POS = "POS";
    
    
    
   /* Newly Added for Generali Thailand */
    public static final String SUCCESS_CODE = "100";
    public static final String FAILURE_CODE = "105";
    public static final String USER_DETAILS = "userDetails";
    public static final String CREATE_PASSWORD = "createPassword";
    public static final String FORGET_PASSWORD = "forgetPassword";
    public static final String VALIDATE_USER= "validateUser";
    
    public static final String KEY5 = "Key5";
    public static final String KEY2 = "Key2";
    public static final String KEY4 = "Key4";
    public static final String KEY8 = "Key8";
    
    public static final String AGENT_CODE = "agentCode";
    public static final String LICENSE_NUMBER = "licenseNumber";
    public static final String IDENTIFICATION_ID = "identificationId";
    public static final String AGENT_DOB = "agentDob";
    
    public static final String USER_TYPE_AGENT="agent";    
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";  
    public static final String AGENT_STATUS_INFORCE = "INFORCE";
    public static final String RESET = "reset";
    
    /* The Constant ERROR_INVALID_CODE. */
    public static final String INVALID_ERROR_CODE = "400";
    public static final String USER_ALREADY_EXIST_ERROR_CODE = "501";      
    
    /*User registration related error messages*/
    public static final String INVALID_REGISTRATION_ERROR_CODE = "201"; 
    public static final String USER_NOT_FOUND = "User not found";
    public static final String INVALID_AGENT_CODE_ERROR_MSG = "Incorrect agent code. Please input valid agent number";
    public static final String INVALID_LICENSE_NUMBER_ERROR_MSG = "Incorrect License No., Please input correct License No.";
    public static final String INVALID_IDENTIFICATION_ID_ERROR_MSG = "Incorrect ID No., Please input correct ID No.";
    public static final String INVALID_AGENT_DOB_ERROR_MSG = "Incorrect Date of birth, Please input correct Date of birth.";
    
    public static final String LICENSE_DATE_EXPIRED_ERROR_MSG = "Your agent license expired . Please contact Licensing Team at 02 XXX XXXX";
    public static final String AGENT_NOTACTIVE_EXPIRED_ERROR_MSG = "Agent status not active. Please contact Sales support team at 02 XXX XXXX";
    
    /*User login related error messages*/
    public static final String INVALID_USER_ERROR_CODE = "502"; 
    public static final String INVALID_AGENTCODE_PWD_ERROR_MSG = "Username/Password entered is invalid. Please re-enter the correct one.";
    public static final String LICENSE_EXPIRED_ERROR_MSG = "Your license has expired. Kindly contact your office for details";
    public static final String AGENT_STATUS_EXPIRED_ERROR_MSG = "You are not authorize to access. Kindly contact your office for details";
}
