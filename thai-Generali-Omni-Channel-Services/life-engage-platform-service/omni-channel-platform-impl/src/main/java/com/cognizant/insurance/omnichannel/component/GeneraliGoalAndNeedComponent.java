package com.cognizant.insurance.omnichannel.component;

import java.util.List;

import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.goalandneed.component.GoalAndNeedComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public interface GeneraliGoalAndNeedComponent extends GoalAndNeedComponent {

    /**
     * Gets RelatedTransactions.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @return the fNA by filter
     */
    public Response<List<GoalAndNeed>> retrieveRelatedTransactions(Request<SearchCriteria> searchCriteriatRequest);
    /**
     * Gets the goal and need count.
     * 
     * @param searchCriteriatRequest
     *            
     * @return the goal and need count
     */
    Response<SearchCountResult> getGoalAndNeedCountforLMS(Request<SearchCriteria> searchCriteriatRequest);

}
