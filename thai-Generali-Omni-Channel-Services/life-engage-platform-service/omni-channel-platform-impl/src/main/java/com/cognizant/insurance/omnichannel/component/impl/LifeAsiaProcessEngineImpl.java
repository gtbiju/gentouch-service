package com.cognizant.insurance.omnichannel.component.impl;

import java.text.ParseException;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.core.task.TaskRejectedException;
import org.springframework.scheduling.annotation.Async;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.component.LifeAsiaComponent;
import com.cognizant.insurance.omnichannel.component.repository.LifeAsiaRepository;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaRequest;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;


public class LifeAsiaProcessEngineImpl implements LifeAsiaProcessEngine {

	@Autowired
	private LifeAsiaComponent lifeAsiaComponent;

	@Autowired
	/**
	 * Only for non transactional  Activities 
	 */
	private LifeAsiaRepository bpmRepository;

	@Autowired
	private TaskExecutor lifeAsiaThreadPool;

	public static final Logger LOGGER = LoggerFactory
			.getLogger(LifeAsiaProcessEngineImpl.class);

	@Async
	public void createAndSentLifeAsiaRequest(String proposalNo) {
		LOGGER.info("BPM Request Creation request received for identifier : "
				+ proposalNo);
		LifeAsiaRequest lifeAsiaRequest;
		try {
			lifeAsiaRequest = lifeAsiaComponent
					.createLifeAsiaRequest(proposalNo);
			if (lifeAsiaRequest != null && lifeAsiaRequest.getId() != null) {
				LOGGER.info("BPM Request created for proposal : "
						+ proposalNo + " With LifeAsia ID "
						+ lifeAsiaRequest.getId() + "With SPAJ NO : "
						+ lifeAsiaRequest.getSpajNo());
				lifeAsiaThreadPool.execute(new CreateApplicationTask(
						lifeAsiaRequest.getId(), true));
			}
		} catch (BusinessException e) {
			LOGGER.error(e.getMessage());
		}

	}

	@Override
	public String getLifeAsiaMsg(String json) {
		String result = null;
		try {
			JSONObject jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			 final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
	         final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
			result = lifeAsiaComponent.createLifeAsiaRequestString(requestInfo, jsonRqArray.getJSONObject(0));
		} catch (BusinessException e) {

		} catch (ParseException e) {
			
		}
		return result;
	}

	/**
	 * This method is not async and it requires and existing transaction
	 */

	public void createAndSentBPMRequest(Transactions transactions) {
		LOGGER.debug("On Life-Asia Engine : createAndSentBPMRequest");
		LifeAsiaRequest bpmRequest;
		try {
			bpmRequest = lifeAsiaComponent.createLifeAsiaRequest(transactions);
			lifeAsiaComponent.submit(bpmRequest);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}

	}

	@Override
    public void reSubmissionJob() {
        try {
            LOGGER.info("BPM failed cases Starts");
            /*
             * if(!lifeAsiaComponent.isLASAvailable()){ Temporarily commented for Thailand if (true) {
             * LOGGER.info("LAS is not Live, abort resubmission"); return; }
             */

            List<Number> failedIds = bpmRepository.getFailedTransactionIds();
            if (failedIds == null || failedIds.size() < 1) {
                return;
            }
            for (Number number : failedIds) {
                try {
                    lifeAsiaThreadPool.execute(new CreateApplicationTask(number, true));
                } catch (TaskRejectedException e) {
                    LOGGER.error("BPM Executer rejected the task" + e.getMessage());
                    // Assuming queue is full, will trigger the remaining
                    // transaction on next Job
                    return;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                }
            }
        } catch (Exception e) {
            LOGGER.error("Unable to resubmit failed cases" + e.getMessage());
        }

    }

	@SuppressWarnings("unused")
	private void handShakeWithBPM() {

	}

    private class CreateApplicationTask implements Runnable {

        private Number lifeAsiaRequestId;

        private boolean resubmission;

        public CreateApplicationTask(Number bpmRequestId, boolean resubmission) {
            super();
            this.lifeAsiaRequestId = bpmRequestId;
            this.resubmission = resubmission;
        }

        @Override
        public void run() {
            LOGGER.info("LifeAsia Thread Initiated for ID  : " + lifeAsiaRequestId);
            if (resubmission) {
                lifeAsiaComponent.submit(lifeAsiaRequestId);
            } else {
                try {
                    lifeAsiaComponent.createLifeAsiaRequest(String.valueOf(lifeAsiaRequestId));
                } catch (BusinessException e) {
                    LOGGER.error(e.getMessage());
                }
            }

        }

    }

    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    private static final String REQUEST_PAYLOAD = "RequestPayload";

    private static final String TRANSACTIONS = "Transactions";

}
