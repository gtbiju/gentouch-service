package com.cognizant.insurance.component.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.generic.component.AgentComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;

/**
 * The Class AgentProfileRepository.
 */
public class AgentProfileRepository {

	/** The entity manager. */
	@PersistenceContext(unitName="LE_Platform")
	private EntityManager entityManager;

	/** The entity manager. */
	@PersistenceContext(unitName="ODS_unit")
	private EntityManager odsEntityManager;
	
	/** The agent component. */
	@Autowired
	private AgentComponent agentComponent;
	

	/**
	 * Retrieve agent details by code.
	 * 
	 * @param requestInfo
	 *            the request info
	 * @param agentID
	 *            the agent id
	 * @return the agent
	 */
	public final Agent retrieveAgentDetailsByCode(
			final RequestInfo requestInfo, final String agentID) {
		final Request<Agent> agentRequest = new JPARequestImpl<Agent>(
				entityManager, ContextTypeCodeList.AGENT,
				requestInfo.getTransactionId());
		final Agent agent = new Agent();
		agent.setAgentCode(agentID);
		agentRequest.setType(agent);
		Response<Agent> response = agentComponent.retrieveAgentByCode(agentRequest);
		return response.getType();
	}
	
	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the new entity manager
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Gets the agent component.
	 * 
	 * @return the agent component
	 */
	public AgentComponent getAgentComponent() {
		return agentComponent;
	}

	/**
	 * Sets the agent component.
	 * 
	 * @param agentComponent
	 *            the new agent component
	 */
	public void setAgentComponent(AgentComponent agentComponent) {
		this.agentComponent = agentComponent;
	}

}
