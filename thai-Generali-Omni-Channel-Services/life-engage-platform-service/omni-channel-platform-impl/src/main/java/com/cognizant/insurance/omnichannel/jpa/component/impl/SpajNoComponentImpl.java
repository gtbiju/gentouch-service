package com.cognizant.insurance.omnichannel.jpa.component.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.omnichannel.dao.GeneraliAgreementDao;
import com.cognizant.insurance.omnichannel.dao.GeneraliSPAJNoDao;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliAgreementDaoImpl;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliSPAJNoDaoImpl;
import com.cognizant.insurance.omnichannel.domain.GeneraliSPAJNo;
import com.cognizant.insurance.omnichannel.jpa.component.SpajNoComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;

@Component
public class SpajNoComponentImpl implements SpajNoComponent {

	@Override
	public Response<GeneraliSPAJNo> save(Request<GeneraliSPAJNo> request) {
		GeneraliSPAJNoDao dao = new GeneraliSPAJNoDaoImpl();
		dao.save(request);
		Response<GeneraliSPAJNo> response = new ResponseImpl<GeneraliSPAJNo>();
		response.setType(request.getType());
		return response;
	}

	@Override
	public Boolean isSpajExisting(Request<InsuranceAgreement> spajRequest,
			String spajNo,
			Request<List<AgreementStatusCodeList>> statusRequest,
			String identifier) {
		GeneraliAgreementDao dao = new GeneraliAgreementDaoImpl();
		Boolean isSpajExists = dao.isSpajExisting(spajRequest, spajNo,
				statusRequest, identifier);
		return isSpajExists;
	}

}
