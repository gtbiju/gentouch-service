/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jan 13, 2015
 */
package com.cognizant.insurance.life.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LifeEngageSyncService;

/**
 * The Class class TestLifeEngageLMSService.
 * 
 * @author 300797
 * 
 */
@ContextConfiguration("/service-context-test.xml")
public class TestLifeEngageLMSService extends AbstractJUnit4SpringContextTests {

    /** The life engage sync service. */
    @Autowired
    private LifeEngageSyncService lifeEngageSyncService;

    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant KEY1. */
    private static final String KEY1 = "Key1";

    /** The Constant KEY2. */
    private static final String KEY2 = "Key2";

    /** The Constant KEY11. */
    private static final String KEY11 = "Key11";

    /** The Constant KEY15. */
    private static final String KEY15 = "Key15";

    /** The Constant KEY16. */
    private static final String KEY16 = "Key16";

    /** The Constant STATUS. */
    private static final String TYPE = "Type";

    /** The Constant STATUS. */
    private static final String STATUS = "Status";

    /** The Constant STATUS_CODE. */
    private static final String STATUS_CODE = "StatusCode";

    /** The Constant STATUS_DATA. */
    private static final String STATUS_DATA = "StatusData";

    /** The Constant STATUS_MESSAGE. */
    private static final String STATUS_MESSAGE = "StatusMessage";

    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";

    /** The Constant INSURED. */
    private static final String LEAD = "Lead";

    /** The Constant BASIC_DETAILS. */
    private static final String BASIC_DETAILS = "BasicDetails";

    /** The Constant BASIC_DETAILS. */
    private static final String FULL_DETAILS = "FullDetails";

    /** The Constant FIRST_NAME. */
    private static final String FIRST_NAME = "firstName";
    
    /** The Constant FULL_NAME. */
    private static final String FULL_NAME = "fullName";

    /** The Constant PROPOSER. */
    private static final String PROPOSER = "Proposer";

    /** The Constant REJECTED. */
    private static final String REJECTED = "Rejected";

    /**
     * This Method Tests the lms Save, retrieve, retreiveAll(basic details and full details) methods.
     * 
     * @throws BusinessException
     *             the business exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public final void lmsTest() throws BusinessException, IOException, ParseException {
        String agentId = "Agent101";
        final SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Calendar calendar = new GregorianCalendar();
        final String currentDate = newDt.format(calendar.getTime());
        final String leadFirstNameFirst = "Nems";
        final String leadFirstNameSecond = "Sam";
        final String proposalFirstNameFirst = "John Smith";
        final String proposalFirstNameSecond = "John Smith";
        String leadStatusOpen = "Open";
        String statusCode100 = "100";
        String statusSuccess = "SUCCESS";
        String statusCode400 = "400";
        String statusFailure = "FAILURE";
        String statusMessage = "Lead Saved";
        String status = "SUCCESS";
        String statusMessageUpdate = "Lead Updated";
        String rejectedCode102 = "102";
        String statusRejected = "REJECTED";
        String rejectedMessage = "Lead Rejected";

        // creating the first lms save request
        String saveRequest = getRequest("lmsRequest/lmsSaveRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", leadFirstNameFirst)
                    .replace("{4}", proposalFirstNameFirst)
                    .replace("{5}", "")
                    .replace("{6}", "");

        String responseJson = lifeEngageSyncService.save(saveRequest);

        // System.out.println(responseJson);
        assertNotNull(responseJson);
        JSONObject jsonObject = new JSONObject(responseJson);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(leadStatusOpen, jsonObj.getString(KEY15));
        String leadIdFirst = jsonObj.getString(KEY1);
        assertNotNull(leadIdFirst);
        JSONObject statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));
        assertEquals(status, jsonObj.getString(KEY16));

        // Trying to save same lms Request
        responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals("", jsonObj.getString(KEY15));
        assertEquals("", jsonObj.getString(KEY1));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode400, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusFailure, statusDataObj.getString(STATUS));
        assertTrue(statusDataObj.getString(STATUS_MESSAGE).contains("This Trasaction Data is already processed"));

        // Retrieving the first saved eapp
        String retrieveRequest = getRequest("lmsRequest/lmsServiceRq_Retrieve.json");
        retrieveRequest =
                retrieveRequest.replace("{1}", currentDate).replace("{2}", leadIdFirst).replace("{3}", agentId);
        // retrieveRequest = retrieveRequest.replace("{1}", currentDate).replace("{2}", "546071");
        // System.out.println(retrieveRequest);
        String retrieveResponseJson = lifeEngageSyncService.retrieve(retrieveRequest);
        // System.out.println(retrieveResponseJson);

        assertNotNull(retrieveResponseJson);
        jsonObject = new JSONObject(retrieveResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(0).getString(KEY11));
        // assertEquals(currentDate, jsonRsTransactionsArray.getJSONObject(0).getString(KEY13));
        assertEquals(leadIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY1));
        assertEquals("LMS", jsonRsTransactionsArray.getJSONObject(0).getString(TYPE));
        assertEquals(leadStatusOpen, jsonRsTransactionsArray.getJSONObject(0).getString(KEY15));

        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(LEAD));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(LEAD)
            .getJSONObject(BASIC_DETAILS));
        assertEquals(leadFirstNameFirst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(LEAD)
            .getJSONObject(BASIC_DETAILS)
            .getString(FULL_NAME));

        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(PROPOSER));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PROPOSER)
            .getJSONObject(BASIC_DETAILS));
        assertEquals(proposalFirstNameFirst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PROPOSER)
            .getJSONObject(BASIC_DETAILS)
            .getString(FIRST_NAME));

        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Communication"));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Communication")
            .getJSONArray("followUps"));
        assertEquals(1,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Communication")
                    .getJSONArray("followUps")
                    .length());

        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("SystemRecordDetails"));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Feedback"));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Feedback")
            .getJSONArray("questions"));
        assertEquals(4,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Feedback")
                    .getJSONArray("questions")
                    .length());
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject("Policies"));


        // creating the second lms save request
        saveRequest = getRequest("lmsRequest/lmsSaveRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", leadFirstNameSecond)
                    .replace("{4}", proposalFirstNameSecond)
                    .replace("{5}", "")
                    .replace("{6}", "");
        responseJson = lifeEngageSyncService.save(saveRequest);

        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(leadStatusOpen, jsonObj.getString(KEY15));
        String leadIdSecond = jsonObj.getString(KEY1);
        assertNotNull(leadIdFirst);
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));
        assertEquals(status, jsonObj.getString(KEY16));

        // Retrieving the lms list - basic details
        String retrieveListRequest = getRequest("lmsRequest/lmsServiceRq_Retrieve.json");
        retrieveListRequest =
                retrieveListRequest.replace("{1}", currentDate)
                    .replace("{2}", "")
                    .replace("{3}", agentId)
                    .replace("{4}", BASIC_DETAILS);
        // retrieveRequest = retrieveRequest.replace("{1}", currentDate).replace("{2}", "546071");
        // System.out.println(retrieveRequest);
        String retrieveListResponseJson = lifeEngageSyncService.retrieveAll(retrieveListRequest);
        // System.out.println(retrieveListResponseJson);
        jsonObject = new JSONObject(retrieveListResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(2, jsonRsTransactionsArray.length());
        for (int i = 0; i < jsonRsTransactionsArray.length(); i++) {
            assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(i).getString(KEY11));
            // assertEquals(currentDate, jsonRsTransactionsArray.getJSONObject(i).getString(KEY13));
            assertEquals("LMS", jsonRsTransactionsArray.getJSONObject(i).getString(TYPE));
            assertEquals(leadStatusOpen, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
            assertFalse(jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).has(LEAD));
            if (leadIdFirst.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY1))) {
                assertEquals(leadFirstNameFirst, jsonRsTransactionsArray.getJSONObject(i).getString(KEY2));
            } else if (leadIdSecond.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY1))) {
                assertEquals(leadFirstNameSecond, jsonRsTransactionsArray.getJSONObject(i).getString(KEY2));
            }
        }

        // Retrieving the lms list - full details
        retrieveListRequest = getRequest("lmsRequest/lmsServiceRq_Retrieve.json");
        retrieveListRequest =
                retrieveListRequest.replace("{1}", currentDate)
                    .replace("{2}", "")
                    .replace("{3}", agentId)
                    .replace("{4}", FULL_DETAILS);
        // retrieveRequest = retrieveRequest.replace("{1}", currentDate).replace("{2}", "546071");
        // System.out.println(retrieveRequest);
        retrieveListResponseJson = lifeEngageSyncService.retrieveAll(retrieveListRequest);
        // System.out.println(retrieveListResponseJson);
        jsonObject = new JSONObject(retrieveListResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(2, jsonRsTransactionsArray.length());
        for (int i = 0; i < jsonRsTransactionsArray.length(); i++) {
            assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(i).getString(KEY11));
            // assertEquals(currentDate, jsonRsTransactionsArray.getJSONObject(i).getString(KEY13));
            assertEquals("LMS", jsonRsTransactionsArray.getJSONObject(i).getString(TYPE));
            assertEquals(leadStatusOpen, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONObject(LEAD));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(LEAD)
                .getJSONObject(BASIC_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PROPOSER));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PROPOSER)
                .getJSONObject(BASIC_DETAILS));
            if (leadIdFirst.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY1))) {
                assertEquals(leadFirstNameFirst, jsonRsTransactionsArray.getJSONObject(i).getString(KEY2));
                assertEquals(proposalFirstNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(leadFirstNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(LEAD)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FULL_NAME));
            } else if (leadIdSecond.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY1))) {
                assertEquals(leadFirstNameSecond, jsonRsTransactionsArray.getJSONObject(i).getString(KEY2));
                assertEquals(proposalFirstNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(leadFirstNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(LEAD)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FULL_NAME));
            }
        }

        // Updating the first lms save request
        String updateRequest = getRequest("lmsRequest/lmsSaveRq.json");
        updateRequest =
                updateRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", leadFirstNameFirst)
                    .replace("{4}", proposalFirstNameFirst)
                    .replace("{5}", leadIdFirst)
                    .replace("{6}", "");
        responseJson = lifeEngageSyncService.save(updateRequest);

        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(leadStatusOpen, jsonObj.getString(KEY15));
        assertEquals(leadIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY1));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessageUpdate, statusDataObj.getString(STATUS_MESSAGE));
        assertEquals(status, jsonObj.getString(KEY16));

        // creating the first lms update request again with ConflictResolution true and sourceOfTruth as server
        updateRequest = getRequest("lmsRequest/lmsSaveRq.json");
        calendar.add(Calendar.DATE, -1);
        final String previousDate = newDt.format(calendar.getTime());
        updateRequest =
                updateRequest.replace("{1}", agentId)
                    .replace("{2}", previousDate)
                    .replace("{3}", leadFirstNameFirst)
                    .replace("{4}", proposalFirstNameFirst)
                    .replace("{5}", leadIdFirst)
                    .replace("{6}", previousDate);
        // updating first eapp Request with ConflictResolution true and sourceOfTruth as server
        responseJson = lifeEngageSyncService.save(updateRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(REJECTED, jsonObj.getString(KEY15));
        assertEquals(leadIdFirst, jsonObj.getString(KEY1));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(rejectedCode102, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusRejected, statusDataObj.getString(STATUS));
        assertEquals(rejectedMessage, statusDataObj.getString(STATUS_MESSAGE));
    }

    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(final String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(new String(line.getBytes(), "UTF-8"));
        }

        return sb.toString();
    }

}
