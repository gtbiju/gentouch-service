package com.cognizant.insurance.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class BarChart.
 */
public class BarChart {
	
	/** The policy year. */
	private Long policyYear;
	
	/** The death benefit. */
	private Long deathBenefit;

	/**
	 * Gets the policy year.
	 *
	 * @return the policy year
	 */
	public Long getPolicyYear() {
		return policyYear;
	}

	/**
	 * Sets the policy year.
	 *
	 * @param policyYear the new policy year
	 */
	public void setPolicyYear(Long policyYear) {
		this.policyYear = policyYear;
	}

	/**
	 * Gets the death benefit.
	 *
	 * @return the death benefit
	 */
	public Long getDeathBenefit() {
		return deathBenefit;
	}

	/**
	 * Sets the death benefit.
	 *
	 * @param deathBenefit the new death benefit
	 */
	public void setDeathBenefit(Long deathBenefit) {
		this.deathBenefit = deathBenefit;
	}
}
