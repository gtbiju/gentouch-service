/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304000
 * @version       : 0.1, Feb 12, 2014
 */
package com.cognizant.insurance.life.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import java.text.ParseException;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LifeEngageSyncService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@ContextConfiguration("/service-context-test.xml")
public class TestLifeEngageFNAService extends AbstractJUnit4SpringContextTests {
    
    /** The life engage sync service. */
    @Autowired
    LifeEngageSyncService lifeEngageSyncService;
    
    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";
    
    /** The Constant KEY2. */
    private static final String KEY2 = "Key2";
    
    /** The Constant KEY11. */
    private static final String KEY11 = "Key11";
    
    /** The Constant KEY15. */
    private static final String KEY15 = "Key15";
    
    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant STATUS_DATA. */
    private static final String STATUS_DATA = "StatusData";

    /** The Constant STATUS_CODE. */
    private static final String STATUS_CODE = "StatusCode";

    /** The Constant STATUS. */
    private static final String STATUS = "Status";

    /** The Constant STATUS_MESSAGE. */
    private static final String STATUS_MESSAGE = "StatusMessage";
    
    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";
    
    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";
    
    /** The Constant INSURED. */
    private static final String INSURED = "Insured";
    
    /** The Constant BASIC_DETAILS. */
    private static final String BASIC_DETAILS = "BasicDetails";
    
    /** The Constant BASIC_DETAILS. */
    private static final String FULL_DETAILS = "FullDetails";

    /** The Constant FIRST_NAME. */
    private static final String FIRST_NAME = "firstName";

    /** The Constant LAST_NAME. */
    private static final String LAST_NAME = "lastName";
    
    /** The Constant CONTACT_DETAILS. */
    private static final String CONTACT_DETAILS = "ContactDetails";
    
    /** The Constant EMAIL_ID. */
    private static final String EMAIL_ID = "emailId";
    
    /** The Constant HOME_NUMBER. */
    private static final String HOME_NUMBER = "homeNumber1";
    
    /** The Constant BENEFICIARIES. */
    private static final String BENEFICIARIES = "Beneficiaries";
    
    /** The Constant GOALS. */
    private static final String GOALS = "goals";
    
    /** The Constant SELECTED_PRODUCT. */
    private static final String SELECTED_PRODUCT = "selectedProduct";
    
    private static final String PARTIES="parties";
    
    public static final Logger LOGGER = LoggerFactory.getLogger(TestLifeEngageFNAService.class);
    
    /**
     * This Method Tests the FNA Save, retreiveAll(basic details and full details) methods.
     * 
     * @throws BusinessException
     *             the business exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public void fnaTest() throws BusinessException, IOException, ParseException {

    	final SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    	final Calendar calendar = new GregorianCalendar();
    	final  String currentDate = newDt.format(calendar.getTime());
        
        String agentId = "Agent1";
        String agentId2 = "Agent2";
        String insuredFirstName = "John";
        String insuredLastName = "Neman";
        String insuredFirstName2 = "Jack";
        String insuredLastName2 = "Smith";
        String statusCode100 = "100";
        String statusCodeRejected102 = "102";
        String statusSuccess = "SUCCESS";
        String statusCode400 = "400";
        String statusFailure = "FAILURE";
        String statusRejected = "REJECTED";
        String statusMessage = "FNA Saved";
        String statusMessageUpdate = "FNA Updated";
        String statusMessageRejected = "FNA Rejected";
        String type = "FNA";
        String fnaStatusInitial = "Initial";
        String fnaStatusFinal = "Final";
        String fnaStatusRejected = "Rejected"; 

        /** creating the first fna save request **/
        String saveRequest = getRequest("fnaRequest/fnaSaveRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", insuredFirstName)
                    .replace("{4}", insuredLastName)
                    .replace("{5}", "")
                    .replace("{6}", fnaStatusInitial).replace("{7}", "");
       
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Save FNA : saveRequest >>"+ saveRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Saving first fna Request with Initial Status(==> Work in Progress) **/
        String responseJson = lifeEngageSyncService.save(saveRequest);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Save FNA : responseJson 1 >>"+ responseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Verify responsejson **/
        assertNotNull(responseJson);
        
        /** Verify TransactionArray length **/
        JSONObject jsonObject = new JSONObject(responseJson);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        
        /** Verify fnaIdentifier **/
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        String fnaIdentifier = jsonObj.getString(KEY2);
        assertNotNull(fnaIdentifier);
        
        /** Verify Status Data >> Success status expected**/
        JSONObject statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));

        /** Saving the same fna Request **/
        responseJson = lifeEngageSyncService.save(saveRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Save FNA : responseJson 2 >>"+ responseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Verify responsejson **/
        assertNotNull(responseJson);
        
        /** Verify TransactionArray length **/
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        
        /** Verify fnaIdentifier **/
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals("", jsonObj.getString(KEY2));
        
        /** Verify Status Data >> Failure Status expected**/
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode400, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusFailure, statusDataObj.getString(STATUS));
        assertTrue(statusDataObj.getString(STATUS_MESSAGE).contains("This Trasaction Data is already processed"));

        /** Creating request for retrieving basic details of the first saved fna **/
        String retrieveRequest = getRequest("fnaRequest/fnaRetrieveRq.json");
        retrieveRequest = retrieveRequest.replace("{1}", agentId)
                            .replace("{2}", "")
                            .replace("{3}", BASIC_DETAILS)
                            .replace("{5}", fnaIdentifier);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retrieve FNA basic dtls : retrieveRequest >>"+ retrieveRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Retrieving the first saved fna with only basic details**/
        String retrieveResponseJson = lifeEngageSyncService.retrieve(retrieveRequest);

        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retrieve FNA  basic dtls : retrieveResponseJson >>"+ retrieveResponseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Verify retrieveResponseJson **/
        assertNotNull(retrieveResponseJson);
        
        /** Verify TransactionArray length **/
        jsonObject = new JSONObject(retrieveResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        
        /** Verify agent id **/
        assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(0).getString(KEY11));
                
        /** Verify fna identifier**/
        assertEquals(fnaIdentifier, jsonRsTransactionsArray.getJSONObject(0).getString(KEY2));
        
        /** Verify fna type**/
        assertEquals(type, jsonRsTransactionsArray.getJSONObject(0).getString(TYPE));
        
        /** Verify fna status**/
        assertEquals(fnaStatusInitial, jsonRsTransactionsArray.getJSONObject(0).getString(KEY15));
        
        /** Verify  Insured**/
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES));
        JSONObject insured=null;
        for(int i=0;i<jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).length();i++){
        	JSONObject object=jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).getJSONObject(i);
        	if(object.getBoolean("isInsured")==true){
        		insured=object;
        		break;
        	}
        }
        assertNotNull(insured
            .getJSONObject(BASIC_DETAILS));
        assertEquals(insuredFirstName,insured
            .getJSONObject(BASIC_DETAILS)
            .getString(FIRST_NAME));
        assertEquals(insuredLastName, insured
            .getJSONObject(BASIC_DETAILS)
            .getString(LAST_NAME));
        
        /** Creating request for retrieving full details of the first saved fna **/
        String retrieveFullDtlsRequest = getRequest("fnaRequest/fnaRetrieveRq.json");
        retrieveFullDtlsRequest = retrieveFullDtlsRequest.replace("{1}", agentId)
                            .replace("{2}", "")
                            .replace("{3}", FULL_DETAILS)
                            .replace("{5}", fnaIdentifier);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retrieve FNA full dtls : retrieveFullDtlsRequest >>"+ retrieveFullDtlsRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Retrieving the first saved fna with only basic details**/
        String retrieveFullDtlsResponseJson = lifeEngageSyncService.retrieveAll(retrieveFullDtlsRequest);

        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retrieve FNA full dtls : retrieveFullDtlsResponseJson >>"+ retrieveFullDtlsResponseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Verify retrieveFullDtlsResponseJson **/
        assertNotNull(retrieveFullDtlsResponseJson);
        
        /** Verify TransactionArray length **/
        jsonObject = new JSONObject(retrieveFullDtlsResponseJson);
        JSONObject jsonFullResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonFullDtlsResponsePayloadObj = jsonFullResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonFullDtlsRsTransactionsArray = jsonFullDtlsResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonFullDtlsRsTransactionsArray.length());
        
        /** Verify agent id **/
        assertEquals(agentId, jsonFullDtlsRsTransactionsArray.getJSONObject(0).getString(KEY11));
        
        /** Verify fna identifier**/
        assertEquals(fnaIdentifier, jsonFullDtlsRsTransactionsArray.getJSONObject(0).getString(KEY2));
        
        /** Verify fna type**/
        assertEquals(type, jsonFullDtlsRsTransactionsArray.getJSONObject(0).getString(TYPE));
        
        /** Verify fna status**/
        assertEquals(fnaStatusInitial, jsonFullDtlsRsTransactionsArray.getJSONObject(0).getString(KEY15));
        
        /** Verify  Insured**/
        assertNotNull(jsonFullDtlsRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES));
        for(int i=0;i<jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).length();i++){
        	JSONObject object=jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).getJSONObject(i);
        	if(object.getBoolean("isInsured")==true){
        		insured=object;
        		break;
        	}
        }
        assertNotNull(insured
            .getJSONObject(BASIC_DETAILS));
        assertEquals(insuredFirstName,insured
            .getJSONObject(BASIC_DETAILS)
            .getString(FIRST_NAME));
        assertEquals(insuredLastName, insured
            .getJSONObject(BASIC_DETAILS)
            .getString(LAST_NAME));
        
       
        
        /** Verify  Beneficiaries**/
        JSONObject beneficiary=null;
        for(int i=0;i<jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).length();i++){
        	JSONObject object=jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).getJSONObject(i);
        	if(object.getBoolean("isBeneficiary")==true){
        		beneficiary=object;
        		break;
        	}
        }
        assertNotNull(beneficiary);
        
        /** Verify  Goals**/
        assertNotNull(jsonFullDtlsRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONArray(GOALS));
        
        /** Verify  Selected Product**/
        assertNotNull(jsonFullDtlsRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(SELECTED_PRODUCT));
        
        /** creating muliple fna save request **/
        saveRequest = getRequest("fnaRequest/multiplefnaSaveRq.json");
        saveRequest =
            saveRequest.replace("{1}", agentId)
                .replace("{2}", currentDate)
                .replace("{3}", insuredFirstName2)
                .replace("{4}", insuredLastName2)
                .replace("{5}", "")
                .replace("{6}", fnaStatusFinal)
                .replace("{7}", agentId2)
                .replace("{8}", currentDate)
                .replace("{9}", "")
                .replace("{10}", fnaStatusFinal);

        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Save Muliple FNA : saveRequest >>"+ saveRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Saving muliple fna request **/
        responseJson = lifeEngageSyncService.save(saveRequest);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Save Muliple FNA  : responseJson >>"+ responseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Verify  responseJson**/
        assertNotNull(responseJson);
        
        /** Verify TransactionArray length **/
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(2, jsonRsTransactionsArray.length());
        
        for (int i = 0; i < jsonRsTransactionsArray.length(); i++) {
            /** Verify fna identifier **/
            jsonObj = jsonRsTransactionsArray.getJSONObject(i);
            String fnaIdentifier2 = jsonObj.getString(KEY2);
            assertNotNull(fnaIdentifier2);
            
            /** Verify status **/
            statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
            assertNotNull(statusDataObj);
            assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
            assertEquals(statusSuccess, statusDataObj.getString(STATUS));
            assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));
        }
        
        /** Retrieving the fna list - basic details**/
        String retrieveListRequest = getRequest("fnaRequest/fnaRetrieveRq.json");
        retrieveListRequest = retrieveRequest.replace("{1}", agentId)
                        .replace("{2}", currentDate)
                        .replace("{3}", BASIC_DETAILS);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retreive Muliple FNA Basic Dtls : retrieveListRequest >>"+ retrieveListRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        String retrieveListResponseJson = lifeEngageSyncService.retrieveAll(retrieveListRequest);

        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retreive Muliple FNA Basic Dtls  : retrieveListResponseJson >>"+ retrieveListResponseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** verify retrieveListResponseJson **/
        assertNotNull(retrieveListResponseJson);
        
        /** verify TransactionsArray length  **/
        jsonObject = new JSONObject(retrieveListResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(3, jsonRsTransactionsArray.length());
        
        for (int i = 0; i < jsonRsTransactionsArray.length(); i++) {
            
            /** Verify agent id **/
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getString(KEY11));
            
            /** Verify fna identifier**/
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getString(KEY2));
            
            /** Verify fna type**/
            assertEquals(type, jsonRsTransactionsArray.getJSONObject(i).getString(TYPE));
            
            /** Verify  Insured**/
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES));
           insured=null;
            for(int ii=0;ii<jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).length();ii++){
            	JSONObject object=jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).getJSONObject(ii);
            	if(object.getBoolean("isInsured")==true){
            		insured=object;
            		break;
            	}
            }
            assertNotNull(insured
                .getJSONObject(BASIC_DETAILS));
                        
            
            if (fnaIdentifier.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY2))) {
                /** Verify fna status**/
                assertEquals(fnaStatusInitial, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
                
                /** Verify Insured names**/
                assertEquals(insuredFirstName, insured
                        .getJSONObject(BASIC_DETAILS)
                        .getString(FIRST_NAME));
                assertEquals(insuredLastName, insured
                    .getJSONObject(BASIC_DETAILS)
                    .getString(LAST_NAME));
            } else {
                /** Verify fna status**/
                assertEquals(fnaStatusFinal, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
                
                /** Verify Insured names**/
                assertEquals(insuredFirstName2, insured
                        .getJSONObject(BASIC_DETAILS)
                        .getString(FIRST_NAME));
                assertEquals(insuredLastName2,insured
                    .getJSONObject(BASIC_DETAILS)
                    .getString(LAST_NAME));
            }

        }

        /** Retrieving multiple fna list - Full details**/
        retrieveListRequest = getRequest("fnaRequest/fnaRetrieveRq.json");
        retrieveListRequest = retrieveListRequest.replace("{1}", agentId)
                        .replace("{2}", "")
                        .replace("{3}", FULL_DETAILS);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retreive Muliple FNA Full Dtls : retrieveListRequest >>"+ retrieveListRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        retrieveListResponseJson = lifeEngageSyncService.retrieveAll(retrieveListRequest);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retreive Muliple FNA Full Dtls : retrieveListResponseJson >>"+ retrieveListResponseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** verify retrieveListResponseJson **/
        assertNotNull(retrieveListResponseJson);
        
        /** verify TransactionsArray length **/
        jsonObject = new JSONObject(retrieveListResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(3, jsonRsTransactionsArray.length());
        
        for (int i = 0; i < jsonRsTransactionsArray.length(); i++) {
            
            /** Verify agent id **/
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getString(KEY11));
            
            /** Verify fna identifier**/
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getString(KEY2));
            
            /** Verify fna type**/
            assertEquals(type, jsonRsTransactionsArray.getJSONObject(0).getString(TYPE));
            
            insured=null;
            for(int ii=0;ii<jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).length();ii++){
            	JSONObject object=jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).getJSONObject(ii);
            	if(object.getBoolean("isInsured")==true){
            		insured=object;
            		break;
            	}
            }
            
            if (fnaIdentifier.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY2))) {
                
                /** Verify fna status**/
                assertEquals(fnaStatusInitial, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
                
                /** Verify Insured names**/
                assertEquals(insuredFirstName, insured
                        .getJSONObject(BASIC_DETAILS)
                        .getString(FIRST_NAME));
                assertEquals(insuredLastName, insured
                    .getJSONObject(BASIC_DETAILS)
                    .getString(LAST_NAME));
                
            }else{
                /** Verify fna status**/
                assertEquals(fnaStatusFinal, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
                
                /** Verify Insured names**/
                assertEquals(insuredFirstName2, insured
                        .getJSONObject(BASIC_DETAILS)
                        .getString(FIRST_NAME));
                assertEquals(insuredLastName2, insured
                    .getJSONObject(BASIC_DETAILS)
                    .getString(LAST_NAME));
            }
            
            assertNotNull(insured
                    .getJSONObject(CONTACT_DETAILS));
            assertNotNull(insured
                    .getJSONObject(CONTACT_DETAILS)
                    .getString(EMAIL_ID));
            assertNotNull(insured
                    .getJSONObject(CONTACT_DETAILS)
                    .getString(HOME_NUMBER));
            
            /** Verify  Beneficiaries**/
            beneficiary=null;
            for(int ii=0;ii<jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).length();ii++){
            	JSONObject object=jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONArray(PARTIES).getJSONObject(ii);
            	if(object.getBoolean("isBeneficiary")==true){
            		beneficiary=object;
            		break;
            	}
            }
            assertNotNull(beneficiary);
            
            /** Verify  Goals**/
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONArray(GOALS));
            
            /** Verify  Selected Product**/
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONObject(SELECTED_PRODUCT));
            
        }
        
        
        /** Retrieving blank fna list**/
        retrieveListRequest = getRequest("fnaRequest/fnaRetrieveRq.json");
        retrieveListRequest = retrieveListRequest.replace("{1}", "Agent5")
                        .replace("{2}", currentDate)
                        .replace("{3}", FULL_DETAILS);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retreive Blank FNA Full Dtls : retrieveListRequest >>"+ retrieveListRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        retrieveListResponseJson = lifeEngageSyncService.retrieveAll(retrieveListRequest);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Retreive Blank FNA Full Dtls : retrieveListResponseJson >>"+ retrieveListResponseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** verify retrieveListResponseJson **/
        assertNotNull(retrieveListResponseJson);
        
        /** verify TransactionsArray length **/
        jsonObject = new JSONObject(retrieveListResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(0, jsonRsTransactionsArray.length());

        /** Creating the first fna update request **/
        saveRequest = getRequest("fnaRequest/fnaSaveRq.json");
        
        saveRequest =
            saveRequest.replace("{1}", agentId)
                .replace("{2}", currentDate)
                .replace("{3}", insuredFirstName)
                .replace("{4}", insuredLastName)
                .replace("{5}", fnaIdentifier)
                .replace("{6}", fnaStatusFinal)
                .replace("{7}", "");
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Update FNA  : saveRequest >>"+ saveRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Updating first fna Request **/
        responseJson = lifeEngageSyncService.save(saveRequest);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Update FNA  : responseJson >>"+ responseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Verify responseJSON **/
        assertNotNull(responseJson);
        
        /** Verify TransactionsArray length **/
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        
        /** Verify fna status **/
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(fnaStatusFinal, jsonObj.getString(KEY15));
        
        /** Verify fna identifier **/
        assertEquals(fnaIdentifier, jsonObj.getString(KEY2));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        
        /** Verify fna status data **/
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessageUpdate, statusDataObj.getString(STATUS_MESSAGE));
        
        
        /** Creating the first fna update request again with ConflictResolution true and sourceOfTruth as server **/
        saveRequest = getRequest("fnaRequest/fnaSaveRq.json");
        calendar.add(Calendar.DATE, -1); 
        final String previousDate = newDt.format(calendar.getTime());
        saveRequest =
            saveRequest.replace("{1}", agentId)
                .replace("{2}", currentDate)
                .replace("{3}", insuredFirstName)
                .replace("{4}", insuredLastName)
                .replace("{5}", fnaIdentifier)
                .replace("{6}", fnaStatusFinal).replace("{7}", previousDate);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Update FNA  : saveRequest >>"+ saveRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Updating first fna Request again with ConflictResolution true and sourceOfTruth as server**/
        responseJson = lifeEngageSyncService.save(saveRequest);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Update FNA  : responseJson >>"+ responseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Verify responseJSON **/
        assertNotNull(responseJson);
        
        /** Verify TransactionsArray length **/
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        
        /** Verify fna status **/
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(fnaStatusRejected, jsonObj.getString(KEY15));
        
        /** Verify fna identifier **/
        assertEquals(fnaIdentifier, jsonObj.getString(KEY2));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        
        /** Verify fna status data **/
        assertNotNull(statusDataObj);
        assertEquals(statusCodeRejected102, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusRejected, statusDataObj.getString(STATUS));
        assertEquals(statusMessageRejected, statusDataObj.getString(STATUS_MESSAGE));
    }
    
    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(new String(line.getBytes(), "UTF-8"));
        }

        return sb.toString();
    }
    
}
