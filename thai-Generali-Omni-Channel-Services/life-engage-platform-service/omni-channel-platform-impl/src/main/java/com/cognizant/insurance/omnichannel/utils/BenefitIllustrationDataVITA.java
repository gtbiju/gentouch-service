package com.cognizant.insurance.omnichannel.utils;
/**
 * @author 390229
 * 
 *         The Class BenefitIllustrationDataVITA.
 * 
 *         This class is a POJO for Benefit illustration table in illustration PDF.
 */

public class BenefitIllustrationDataVITA {
	
	private String policyYear;
	
	private String annualizedPremium;
	
	private String accumulatedPremiums;
	
	private String guarEarlyStgCI;
	
	private String guarFirstLateStgCI;
	
	private String guarSecondLateStgCI;
	
	private String guarDBAreadyClaimed;
	
	private String guarDBNotClaimed;
	
	private String guarSurrenderValue;
	
	private String guarCashBenefit;
	
	private String accumulatedCashBenefit;
	
	private String accumLABonus4Pct;
	
	private String maxTotalInsuranceBenefit4Pct;
	
	private String surrenderValue4Pct;
	
	private String accumLABonus7Pct;
	
	private String maxTotalInsuranceBenefit7Pct;
	
	private String surrenderValue7Pct;

	/**
	 * @return the policyYear
	 */
	public String getPolicyYear() {
		return policyYear;
	}

	/**
	 * @param policyYear the policyYear to set
	 */
	public void setPolicyYear(String policyYear) {
		this.policyYear = policyYear;
	}

	/**
	 * @return the annualizedPremium
	 */
	public String getAnnualizedPremium() {
		return annualizedPremium;
	}

	/**
	 * @param annualizedPremium the annualizedPremium to set
	 */
	public void setAnnualizedPremium(String annualizedPremium) {
		this.annualizedPremium = annualizedPremium;
	}

	/**
	 * @return the accumulatedPremiums
	 */
	public String getAccumulatedPremiums() {
		return accumulatedPremiums;
	}

	/**
	 * @param accumulatedPremiums the accumulatedPremiums to set
	 */
	public void setAccumulatedPremiums(String accumulatedPremiums) {
		this.accumulatedPremiums = accumulatedPremiums;
	}

	/**
	 * @return the guarEarlyStgCI
	 */
	public String getGuarEarlyStgCI() {
		return guarEarlyStgCI;
	}

	/**
	 * @param guarEarlyStgCI the guarEarlyStgCI to set
	 */
	public void setGuarEarlyStgCI(String guarEarlyStgCI) {
		this.guarEarlyStgCI = guarEarlyStgCI;
	}

	/**
	 * @return the guarFirstLateStgCI
	 */
	public String getGuarFirstLateStgCI() {
		return guarFirstLateStgCI;
	}

	/**
	 * @param guarFirstLateStgCI the guarFirstLateStgCI to set
	 */
	public void setGuarFirstLateStgCI(String guarFirstLateStgCI) {
		this.guarFirstLateStgCI = guarFirstLateStgCI;
	}

	/**
	 * @return the guarSecondLateStgCI
	 */
	public String getGuarSecondLateStgCI() {
		return guarSecondLateStgCI;
	}

	/**
	 * @param guarSecondLateStgCI the guarSecondLateStgCI to set
	 */
	public void setGuarSecondLateStgCI(String guarSecondLateStgCI) {
		this.guarSecondLateStgCI = guarSecondLateStgCI;
	}

	/**
	 * @return the guarDBAreadyClaimed
	 */
	public String getGuarDBAreadyClaimed() {
		return guarDBAreadyClaimed;
	}

	/**
	 * @param guarDBAreadyClaimed the guarDBAreadyClaimed to set
	 */
	public void setGuarDBAreadyClaimed(String guarDBAreadyClaimed) {
		this.guarDBAreadyClaimed = guarDBAreadyClaimed;
	}

	/**
	 * @return the guarDBNotClaimed
	 */
	public String getGuarDBNotClaimed() {
		return guarDBNotClaimed;
	}

	/**
	 * @param guarDBNotClaimed the guarDBNotClaimed to set
	 */
	public void setGuarDBNotClaimed(String guarDBNotClaimed) {
		this.guarDBNotClaimed = guarDBNotClaimed;
	}

	/**
	 * @return the guarSurrenderValue
	 */
	public String getGuarSurrenderValue() {
		return guarSurrenderValue;
	}

	/**
	 * @param guarSurrenderValue the guarSurrenderValue to set
	 */
	public void setGuarSurrenderValue(String guarSurrenderValue) {
		this.guarSurrenderValue = guarSurrenderValue;
	}

	/**
	 * @return the guarCashBenefit
	 */
	public String getGuarCashBenefit() {
		return guarCashBenefit;
	}

	/**
	 * @param guarCashBenefit the guarCashBenefit to set
	 */
	public void setGuarCashBenefit(String guarCashBenefit) {
		this.guarCashBenefit = guarCashBenefit;
	}

	/**
	 * @return the accumulatedCashBenefit
	 */
	public String getAccumulatedCashBenefit() {
		return accumulatedCashBenefit;
	}

	/**
	 * @param accumulatedCashBenefit the accumulatedCashBenefit to set
	 */
	public void setAccumulatedCashBenefit(String accumulatedCashBenefit) {
		this.accumulatedCashBenefit = accumulatedCashBenefit;
	}

	/**
	 * @return the accumLABonus4Pct
	 */
	public String getAccumLABonus4Pct() {
		return accumLABonus4Pct;
	}

	/**
	 * @param accumLABonus4Pct the accumLABonus4Pct to set
	 */
	public void setAccumLABonus4Pct(String accumLABonus4Pct) {
		this.accumLABonus4Pct = accumLABonus4Pct;
	}

	/**
	 * @return the maxTotalInsuranceBenefit4Pct
	 */
	public String getMaxTotalInsuranceBenefit4Pct() {
		return maxTotalInsuranceBenefit4Pct;
	}

	/**
	 * @param maxTotalInsuranceBenefit4Pct the maxTotalInsuranceBenefit4Pct to set
	 */
	public void setMaxTotalInsuranceBenefit4Pct(String maxTotalInsuranceBenefit4Pct) {
		this.maxTotalInsuranceBenefit4Pct = maxTotalInsuranceBenefit4Pct;
	}

	/**
	 * @return the surrenderValue4Pct
	 */
	public String getSurrenderValue4Pct() {
		return surrenderValue4Pct;
	}

	/**
	 * @param surrenderValue4Pct the surrenderValue4Pct to set
	 */
	public void setSurrenderValue4Pct(String surrenderValue4Pct) {
		this.surrenderValue4Pct = surrenderValue4Pct;
	}

	/**
	 * @return the accumLABonus7Pct
	 */
	public String getAccumLABonus7Pct() {
		return accumLABonus7Pct;
	}

	/**
	 * @param accumLABonus7Pct the accumLABonus7Pct to set
	 */
	public void setAccumLABonus7Pct(String accumLABonus7Pct) {
		this.accumLABonus7Pct = accumLABonus7Pct;
	}

	/**
	 * @return the maxTotalInsuranceBenefit7Pct
	 */
	public String getMaxTotalInsuranceBenefit7Pct() {
		return maxTotalInsuranceBenefit7Pct;
	}

	/**
	 * @param maxTotalInsuranceBenefit7Pct the maxTotalInsuranceBenefit7Pct to set
	 */
	public void setMaxTotalInsuranceBenefit7Pct(String maxTotalInsuranceBenefit7Pct) {
		this.maxTotalInsuranceBenefit7Pct = maxTotalInsuranceBenefit7Pct;
	}

	/**
	 * @return the surrenderValue7Pct
	 */
	public String getSurrenderValue7Pct() {
		return surrenderValue7Pct;
	}

	/**
	 * @param surrenderValue7Pct the surrenderValue7Pct to set
	 */
	public void setSurrenderValue7Pct(String surrenderValue7Pct) {
		this.surrenderValue7Pct = surrenderValue7Pct;
	}


}
