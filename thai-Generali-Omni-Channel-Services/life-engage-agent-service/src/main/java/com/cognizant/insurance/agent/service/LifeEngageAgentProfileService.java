package com.cognizant.insurance.agent.service;

import com.cognizant.insurance.agent.core.exception.BusinessException;

/**
 * The Interface AgentProfileService.
 */
public interface LifeEngageAgentProfileService {
	
	/**
	 * Retrieve agent by code.
	 *
	 * @param json the json
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String retrieveAgentByCode(final String json) throws BusinessException;

}
