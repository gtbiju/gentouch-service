/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolerelationshipsubtypes.EmploymentRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;

/**
 * An employer is a party role where the party (e.g. organization or person) is
 * utilizing the skills or services of an employee or contractor usually for
 * some form of compensation.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Employer extends PartyRoleInRelationship {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 290874455162981551L;
    
    /**
     * This relationship links an employment to the related employer.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<EmploymentRelationship> employment;

    /**
     * Gets the employment.
     * 
     * @return Returns the employment.
     */
    public Set<EmploymentRelationship> getEmployment() {
        return employment;
    }

    /**
     * Sets The employment.
     * 
     * @param employment
     *            The employment to set.
     */
    public void
            setEmployment(final Set<EmploymentRelationship> employment) {
        this.employment = employment;
    }

}
