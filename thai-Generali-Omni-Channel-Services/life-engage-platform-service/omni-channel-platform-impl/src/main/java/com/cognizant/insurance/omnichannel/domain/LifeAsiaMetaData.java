package com.cognizant.insurance.omnichannel.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class LifeAsiaMetaData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastStatusFetch;
	@Column(unique=true)
	private String typeCode;
	
	private String value;
	
	private String msg_en;
	@Column(columnDefinition="NVARCHAR(MAX)")
	private String msg_vn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getLastStatusFetch() {
		return lastStatusFetch;
	}

	public void setLastStatusFetch(Date lastStatusFetch) {
		this.lastStatusFetch = lastStatusFetch;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMsg_en() {
		return msg_en;
	}

	public void setMsg_en(String msg_en) {
		this.msg_en = msg_en;
	}

	public String getMsg_vn() {
		return msg_vn;
	}

	public void setMsg_vn(String msg_vn) {
		this.msg_vn = msg_vn;
	}
	
	

}
