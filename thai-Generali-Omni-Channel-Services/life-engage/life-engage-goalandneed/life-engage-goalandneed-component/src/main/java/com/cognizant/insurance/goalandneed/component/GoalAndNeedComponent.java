/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304000
 * @version       : 0.1, Feb 21, 2014
 */
package com.cognizant.insurance.goalandneed.component;

import java.util.List;

import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

/**
 * The Interface GoalAndNeedComponent.
 */
public interface GoalAndNeedComponent {

    /**
     * Save goal and need.
     * 
     * @param goalRequest
     *            the goal request
     */
    void saveGoalAndNeed(Request<GoalAndNeed> goalRequest);

    /**
     * Update goal and need.
     * 
     * @param goalRequest
     *            the goal request
     * @return the goal and need by identifier
     */
    Response<List<GoalAndNeed>> getGoalAndNeedByIdentifier(Request<GoalAndNeed> goalRequest);

    /**
     * Retrieve All Goal And Need.
     * 
     * @param request
     *            the request
     * @param limitRequest
     *            the limit request
     * @return the response
     */
    Response<List<GoalAndNeed>> retrieveAll(Request<GoalAndNeed> request, Request<Integer> limitRequest);


    /**
     * Gets the goal and need count.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @return the goal and need count
     */
    Response<SearchCountResult> getGoalAndNeedCount(Request<SearchCriteria> searchCriteriatRequest,List<String> fnaSearchParamsList);

    /**
     * Gets the fNA by filter.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @return the fNA by filter
     */
    Response<List<GoalAndNeed>> getFNAByFilter(Request<SearchCriteria> searchCriteriatRequest);

    /**
     * Update goal and need status for lead.
     * 
     * @param goalAndNeedRequest
     *            the goal and need request
     */
    void updateGoalAndNeedStatusForLead(Request<GoalAndNeed> goalAndNeedRequest);

}
