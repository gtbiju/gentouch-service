/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304000
 * @version       : 0.1, Feb 10, 2014
 */
package com.cognizant.insurance.component.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.helper.LifeEngageComponentRepositoryHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.goalandneed.component.GoalAndNeedComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

/**
 * The Class class FNARepository.
 */
public class FNARepository {

    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;

    /** The goalAndNeed component. */
    @Autowired
    private GoalAndNeedComponent goalAndNeedComponent;

    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";

    /** The Constant PARTIES. */
    private static final String PARTIES = "parties";

    /** The Constant PARTIES. */
    private static final String ISINSURED = "isInsured";

    /** The Constant BASICDETAILS. */
    private static final String BASICDETAILS = "BasicDetails";

    /** The Constant CONTACTDETAILS. */
    private static final String CONTACTDETAILS = "ContactDetails";

    /** The Constant FIRSTNAME. */
    private static final String FIRSTNAME = "firstName";

    /** The Constant LASTNAME. */
    private static final String LASTNAME = "lastName";

    /** The Constant DOB. */
    private static final String DOB = "dob";

    /** The Constant EMAILID. */
    private static final String EMAILID = "emailId";

    /** The Constant CONTACTNUMBER. */
    private static final String CONTACTNUMBER = "homeNumber1";

    /** The date format. */
    public static final String DATEFORMAT_MM_DD_YYYY = "MM-dd-yyyy";

    /** The date format yyyy-mm-dd. */
    public static final String DATEFORMAT_YYYY_MM_DD = "yyyy-MM-dd";

    /** The party type. */
    private static final String partyType = "FNAMyself";

    /** The conflict resolution. */
    @Value("${le.platform.service.conflictResolution}")
    private String conflictResolution;

    /** The source of truth. */
    @Value("${le.platform.service.sourceOfTruth}")
    private String sourceOfTruth;

    /** The fna limit. */
    @Value("${le.platform.service.fna.fetchLimit}")
    private Integer fnaLimit;

    @Value("${le.platform.service.searchCiteria.fna}")
    private String fnaSearchParams;

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(FNARepository.class);

    /**
     * Save FNA.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param json
     *            the json
     * @return the string
     * @throws ParseException
     *             the parse exception
     */
    public Transactions saveFNA(final Transactions transactions, final RequestInfo requestInfo, final String json, final GoalAndNeed responseGoalAndNeed)
            throws ParseException {

        LOGGER.info(" :::: Entering saveFNA in FNARepository 2.....");
        final Request<GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<GoalAndNeed>(entityManager, ContextTypeCodeList.FNA, requestInfo.getTransactionId());
        final GoalAndNeed goalAndNeed = new GoalAndNeed();

        goalAndNeed.setIdentifier(transactions.getFnaId());
        goalAndNeed.setTransTrackingId(transactions.getTransTrackingId());
        goalAndNeedRequest.setType(goalAndNeed);

        final JSONObject transactionJsonObject = new JSONObject(json);
        final JSONObject jsonObject = transactionJsonObject.getJSONObject(TRANSACTION_DATA);

        if (Constants.UPDATE.equals(transactions.getMode())) {
            String status = onFnaUpdate(transactions, requestInfo, jsonObject, responseGoalAndNeed);
            if (!Constants.SUCCESS.equals(status)) {
                transactions.setStatus(Constants.REJECTED);
                return transactions;
            }
        } else {
            setGoalAndNeed(transactions, requestInfo, goalAndNeed, jsonObject);
           // goalAndNeed.setCreationDateTime(currentDate);
            goalAndNeed.getTransactionKeys().setKey13(
                    LifeEngageComponentRepositoryHelper.formatDate(goalAndNeed.getCreationDateTime()));
            goalAndNeedComponent.saveGoalAndNeed(goalAndNeedRequest);
        }

       // if (transactions.getMode().equals("update")) {
           // transactions.setLastSyncDate(goalAndNeed.getModifiedDateTime());
      //  } else if (transactions.getMode().equals("save")) {
         //   transactions.setLastSyncDate(goalAndNeed.getCreationDateTime());
      //  }
        
        LOGGER.trace(" :::: Exiting saveFNA in FNARepository 2 : status >> " + transactions.getStatus());
        LOGGER.trace(" :::: Exiting saveFNA in FNARepository 2 : getLastSyncDate >> " + transactions.getLastSyncDate());
        return transactions;
    }

    /**
     * On fna update.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param trnasactionData
     *            the trnasaction data
     * @return the string
     * @throws ParseException
     *             the parse exception
     */
    protected String onFnaUpdate(Transactions transactions, RequestInfo requestInfo, JSONObject trnasactionData, GoalAndNeed responseGoalAndNeed)
            throws ParseException {
        final Request<GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<GoalAndNeed>(entityManager, ContextTypeCodeList.FNA, requestInfo.getTransactionId());
        final GoalAndNeed goalAndNeed = new GoalAndNeed();

        goalAndNeed.setIdentifier(transactions.getFnaId());
        goalAndNeedRequest.setType(goalAndNeed);
        List<GoalAndNeed> goalAndNeeds = new ArrayList<GoalAndNeed>();
        if( responseGoalAndNeed != null ){
        	 goalAndNeeds.add((GoalAndNeed) responseGoalAndNeed);
        }else{
        final Response<List<GoalAndNeed>> goalAndNeedResponse =
                goalAndNeedComponent.getGoalAndNeedByIdentifier(goalAndNeedRequest);
        goalAndNeeds = goalAndNeedResponse.getType();
        }
     
        for (GoalAndNeed goalAndneed : goalAndNeeds) {
            // Checking whether the server data is latest when source of
            // truth is server, if so reject the client data and send
            // rejected status
            if (Constants.TRUE.equals(conflictResolution) && Constants.SERVER.equals(sourceOfTruth)) {

                if (transactions.getLastSyncDate() != null
                        && !(transactions.getLastSyncDate().toString().equals(Constants.DEFAULT_DATE))
                        && ((null != goalAndneed.getModifiedDateTime()) && (goalAndneed.getModifiedDateTime().after(transactions.getLastSyncDate())))) {

                    return Constants.REJECTED;

                }

            }
            setGoalAndNeed(transactions, requestInfo, goalAndneed, trnasactionData);
        }
        return Constants.SUCCESS;
    }

    /**
     * Retrieve FNA.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the transactions
     */
    public Transactions retrieveFNA(final Transactions transactions, final RequestInfo requestInfo) {

        LOGGER.info(" :::: Entering retrieve in FNARepository 2 ");
        String transactionId = requestInfo.getTransactionId();
        Transactions transaction = new Transactions();
        final Request<GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<GoalAndNeed>(entityManager, ContextTypeCodeList.FNA, transactionId);
        final GoalAndNeed goalAndNeed = new GoalAndNeed();
        goalAndNeed.setIdentifier(transactions.getFnaId());
        goalAndNeed.setTransTrackingId(transactions.getTransTrackingId());
        goalAndNeed.setAgentId(transactions.getAgentId());
        goalAndNeedRequest.setType(goalAndNeed);

        final Response<List<GoalAndNeed>> goalAndNeedResponse =
                goalAndNeedComponent.getGoalAndNeedByIdentifier(goalAndNeedRequest);

        final List<GoalAndNeed> goalAndNeeds = goalAndNeedResponse.getType();
        if (CollectionUtils.isNotEmpty(goalAndNeeds)) {
            for (GoalAndNeed goalAndneed : goalAndNeeds) {
                transaction.setKey1(goalAndneed.getLeadId());
                transaction.setAgentId(goalAndneed.getAgentId());
                transaction.setModifiedTimeStamp(goalAndneed.getModifiedDateTime());
                transaction.setGoalAndNeed((GoalAndNeed) goalAndneed);
             }
        }
        LOGGER.info(" :::: Exiting retrieve in FNARepository 2 ");
        return transaction;
    }

    /**
     * Retrieve all FNA.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param isFullDetailsRequired
     *            the is full details required
     * @return the list
     */
    public List<Transactions> retrieveAll(final Transactions transactions, final RequestInfo requestInfo,
            final boolean isFullDetailsRequired) {

        LOGGER.info(" :::: Entering retrieveAll in FNARepository 2 ");

        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        final JSONObject jsonObjectRs = new JSONObject();
        Transactions transaction;

        final Request<GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<GoalAndNeed>(entityManager, ContextTypeCodeList.FNA, requestInfo.getTransactionId());
        final GoalAndNeed goalAndNeed = new GoalAndNeed();
        goalAndNeed.setAgentId(transactions.getAgentId());
        goalAndNeed.setCreationDateTime(requestInfo.getLastSyncDate());
        goalAndNeedRequest.setType(goalAndNeed);
        Request<Integer> limitRequest = null;

        if (isFullDetailsRequired) {
            if (fnaLimit != null) {
                limitRequest =
                        new JPARequestImpl<Integer>(entityManager, ContextTypeCodeList.FNA,
                                requestInfo.getTransactionId());
                limitRequest.setType(fnaLimit);
            }
        }

        final Response<List<GoalAndNeed>> goalAndNeedResponse =
                goalAndNeedComponent.retrieveAll(goalAndNeedRequest, limitRequest);

        final List<GoalAndNeed> goalAndNeeds = goalAndNeedResponse.getType();
        if (CollectionUtils.isNotEmpty(goalAndNeeds)) {
            for (GoalAndNeed goalAndneed : goalAndNeeds) {
                transaction = new Transactions();
                if (!isFullDetailsRequired) {
                    getBasicDetailsRequestJSON(goalAndneed, jsonObjectRs);
                    transaction.setGoalAndNeedBasicDtls(jsonObjectRs.toString());
                }
                transaction.setGoalAndNeed((GoalAndNeed) goalAndneed);
                transaction.setCustomerId(goalAndneed.getLeadId());
                SimpleDateFormat simpleFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD);
                String birthDate = "";
                if (goalAndneed.getBirthDate() != null) {
                    birthDate = simpleFormat.format(goalAndneed.getBirthDate());
                }
                transaction.setKey7(birthDate);
                if (goalAndneed.getModifiedDateTime() != null
                        && !(goalAndneed.getModifiedDateTime().toString().equals(""))) {
                    transaction.setLastSyncDate(goalAndneed.getModifiedDateTime());
                } else if (goalAndneed.getCreationDateTime() != null
                        && !("".equals(goalAndneed.getCreationDateTime().toString()))) {
                    transaction.setLastSyncDate(goalAndneed.getCreationDateTime());
                }
                transaction.setModifiedTimeStamp(goalAndneed.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }
        LOGGER.info(" :::: Exiting retrieveAll in FNARepository 2 : transactionsList >> " + transactionsList);
        return transactionsList;
    }

    /**
     * Create basic details json objects with details from db.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param goalAndNeed
     *            the goal and need
     * @param jsonObject
     *            the json object
     * @throws ParseException
     *             the parse exception
     */
    protected static void setGoalAndNeed(final Transactions transactions, final RequestInfo requestInfo,
            final GoalAndNeed goalAndNeed, final JSONObject jsonObject) throws ParseException {
        try {
            TransactionKeys transactionKeys = new TransactionKeys();
            boolean isInsured = false;
            final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD, Locale.getDefault());
            goalAndNeed.setTransactionId(requestInfo.getTransactionId());

            Date currentDate = LifeEngageComponentRepositoryHelper.getCurrentdate();
            
            goalAndNeed.setAgentId(transactions.getAgentId());
            goalAndNeed.setUserEmail(requestInfo.getUserEmail());
            goalAndNeed.setModifiedDateTime(currentDate);
           
            if (Constants.UPDATE.equals(transactions.getMode())) {
                transactions.setModifiedTimeStamp(currentDate);
                transactions.setCreationDateTime(goalAndNeed.getCreationDateTime());
                transactions.setLastSyncDate(currentDate);
           } else {
               goalAndNeed.setCreationDateTime(currentDate);
               transactions.setCreationDateTime(currentDate);
               transactions.setModifiedTimeStamp(currentDate);
               transactions.setLastSyncDate(currentDate);   
           }

            if (jsonObject.has(PARTIES)) {
                final JSONArray jsonpartiesObj = jsonObject.getJSONArray(PARTIES);
                JSONObject jsonInsuredObj = new JSONObject();
                if (jsonpartiesObj != null && jsonpartiesObj.length() > 0) {
                    for (int i = 0; i < jsonpartiesObj.length(); i++) {
                        JSONObject partyObj = (JSONObject) jsonpartiesObj.get(i);
                        if (partyObj.getString("type").equalsIgnoreCase(partyType)) {
                            isInsured = true;
                        }
                        if (isInsured) {
                            jsonInsuredObj = partyObj;
                            break;
                        }
                    }
                    if (jsonInsuredObj != null && jsonInsuredObj.length() > 0) {
                        final JSONObject jsonInsuredBasicDetailsObj = jsonInsuredObj.getJSONObject(BASICDETAILS);
                        final JSONObject jsonInsuredContactDetailsObj = jsonInsuredObj.getJSONObject(CONTACTDETAILS);

                        goalAndNeed.setGivenName(LifeEngageComponentHelper.getStringKeyValue(FIRSTNAME,
                                jsonInsuredBasicDetailsObj));
                        goalAndNeed.setSurName(LifeEngageComponentHelper.getStringKeyValue(LASTNAME,
                                jsonInsuredBasicDetailsObj));

                        goalAndNeed.setBirthDate(LifeEngageComponentHelper.getDateKeyValue(dateFormat, DOB,
                                jsonInsuredBasicDetailsObj));

                        goalAndNeed.setEmailAddress(LifeEngageComponentHelper.getStringKeyValue(EMAILID,
                                jsonInsuredContactDetailsObj));
                        goalAndNeed.setContactNumber(LifeEngageComponentHelper.getStringKeyValue(CONTACTNUMBER,
                                jsonInsuredContactDetailsObj));

                        // set values to transaction keys object
                        
                        transactionKeys.setKey6(goalAndNeed.getGivenName() + " " + goalAndNeed.getSurName());
                        transactionKeys.setKey7(jsonInsuredBasicDetailsObj.getString(DOB));
                        transactionKeys.setKey8(goalAndNeed.getContactNumber());
                        transactionKeys.setKey20(goalAndNeed.getEmailAddress());
                        transactionKeys.setKey14(LifeEngageComponentRepositoryHelper.formatDate(goalAndNeed.getModifiedDateTime()));
                    }
                }
            }
            goalAndNeed.setRequestJSON(jsonObject.toString());
            goalAndNeed.setStatus(AgreementStatusCodeList.valueOf(transactions.getStatus()).toString());
            goalAndNeed.setProductId(transactions.getProductId());
            goalAndNeed.setLeadId(transactions.getCustomerId());
            goalAndNeed.setContextId(ContextTypeCodeList.FNA);

            // set values to transaction keys object
            transactionKeys.setKey1(transactions.getCustomerId());
            transactionKeys.setKey2(transactions.getKey2());
            transactionKeys.setKey11(transactions.getAgentId());
            transactionKeys.setKey15(AgreementStatusCodeList.valueOf(transactions.getStatus()).toString());
            goalAndNeed.setTransactionKeys(transactionKeys);
        } catch (ParseException ex) {
            LOGGER.error(" :::: Error while setting values in goaland need for saving >> " + ex);
        }
    }

    /**
     * Sets all the values of GoalAndNeed.
     * 
     * @param goalAndNeed
     *            the goal and need
     * @param jsonObjectRs
     *            the json object rs
     * @return the basic details request json
     */
    protected static void getBasicDetailsRequestJSON(final GoalAndNeed goalAndNeed, final JSONObject jsonObjectRs) {
        final JSONObject insuredpartyObject = new JSONObject();
        final JSONObject insuredBasicDtlsObject = new JSONObject();
        setStringValueInJsonObject(FIRSTNAME, goalAndNeed.getGivenName(), insuredBasicDtlsObject);
        setStringValueInJsonObject(LASTNAME, goalAndNeed.getSurName(), insuredBasicDtlsObject);
        setStringValueInJsonObject(DOB,
                goalAndNeed.getBirthDate() != null ? goalAndNeed.getBirthDate().toString() : "", insuredBasicDtlsObject);
        insuredpartyObject.put(BASICDETAILS, insuredBasicDtlsObject);
        setStringValueInJsonObject(ISINSURED, "true", insuredpartyObject);
        final JSONArray partiesObj = new JSONArray();
        partiesObj.put(0, insuredpartyObject);
        jsonObjectRs.put(PARTIES, partiesObj);
    }

    /**
     * Retrieve by count fna.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the search count result
     */
    public SearchCountResult retrieveByCountFNA(final RequestInfo requestInfo, final SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.FNA,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        List<String> fnaSearchParamsList = new ArrayList<String>(Arrays.asList(fnaSearchParams.split(",")));

        final Response<SearchCountResult> searchCountResponse =
                goalAndNeedComponent.getGoalAndNeedCount(searchCriteriatRequest, fnaSearchParamsList);

        SearchCountResult searchCountResult = null;
        if (searchCountResponse.getType() != null) {
            searchCountResult = (SearchCountResult) searchCountResponse.getType();

        }
        return searchCountResult;

    }

    /**
     * Retrieve by filter fna.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the list
     */
    public List<Transactions> retrieveByFilterFNA(RequestInfo requestInfo, SearchCriteria searchCriteria) {

        LOGGER.info(" :::: Entering retrieveByFilter in FNARepository 3 ");
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.FNA,
                        requestInfo.getTransactionId());

        searchCriteriatRequest.setType(searchCriteria);
        final Response<List<GoalAndNeed>> searchCriteriatResponse =
                goalAndNeedComponent.getFNAByFilter(searchCriteriatRequest);
        final List<GoalAndNeed> goalAndNeeds = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(goalAndNeeds)) {
            for (GoalAndNeed goalAndneed : goalAndNeeds) {
                transaction = new Transactions();
                transaction.setGoalAndNeed((GoalAndNeed) goalAndneed);
                transaction.setKey2(goalAndneed.getIdentifier());
                transaction.setCustomerId(goalAndneed.getLeadId());
                transaction.setType(searchCriteria.getType());
                if (goalAndneed.getModifiedDateTime() != null
                        && !(goalAndneed.getModifiedDateTime().toString().equals(""))) {
                    transaction.setLastSyncDate(goalAndneed.getModifiedDateTime());
                } else if (goalAndneed.getCreationDateTime() != null
                        && !("".equals(goalAndneed.getCreationDateTime().toString()))) {
                    transaction.setLastSyncDate(goalAndneed.getCreationDateTime());
                }
                transaction.setModifiedTimeStamp(goalAndneed.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }
        LOGGER.info(" :::: Exiting retrieveByFilter in FNARepository 3 : transactionsList >> " + transactionsList);
        return transactionsList;

    }

    public Transactions retrieveFNAforPdfGeneration(final String fnaId, final RequestInfo requestInfo) {
        Transactions transactions = new Transactions();
        transactions.setFnaId(fnaId);
        transactions = retrieveFNA(transactions, requestInfo);
        return transactions;
    }

    /**
     * Sets the string value in json object.
     * 
     * @param key
     *            the key
     * @param value
     *            the value
     * @param jsonObjectRs
     *            the json object rs
     */
    protected static void
            setStringValueInJsonObject(final String key, final String value, final JSONObject jsonObjectRs) {
        if (StringUtils.isNotEmpty(value)) {
            jsonObjectRs.put(key, value);
        } else {
            jsonObjectRs.put(key, "");
        }
    }

    /**
     * Gets the entityManager.
     * 
     * @return Returns the entityManager.
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Sets The entityManager.
     * 
     * @param entityManager
     *            The entityManager to set.
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Gets the goalAndNeedComponent.
     * 
     * @return Returns the goalAndNeedComponent.
     */
    public GoalAndNeedComponent getGoalAndNeedComponent() {
        return goalAndNeedComponent;
    }

    /**
     * Sets The goalAndNeedComponent.
     * 
     * @param goalAndNeedComponent
     *            The goalAndNeedComponent to set.
     */
    public final void setGoalAndNeedComponent(final GoalAndNeedComponent goalAndNeedComponent) {
        this.goalAndNeedComponent = goalAndNeedComponent;
    }
    
    public List<Transactions> retrieveIdsByEmailFNA(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        return null;
    }

}
