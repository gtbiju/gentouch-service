/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304007
 * @version       : 0.1, Jan 30, 2013
 */
package com.cognizant.insurance.agent.domain.product;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The Class Type.
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.STRING)
@NamedQueries({@NamedQuery(name="type.getProductType",query="select p from ProductType p join fetch p.carriers carrier where carrier.id = :carrierCode "), 
			   @NamedQuery(name="type.getCategoryType",query="select c from CategoryType c join fetch c.carriers carrier where carrier.id = :carrierCode"),	
			   @NamedQuery(name="type.getLobType",query="select lob from LobType lob join fetch lob.carriers carrier where carrier.id = :carrierCode")
			  })     
@Table(name = "CORE_PROD_CODE_TYPE_LOOK_UP")

public class Type implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6143475804546781147L;

    /** The id. */
    @Id
    @Column(name="ID")
    private String id;
    
    /** The description. */
    @Column(name="DESCRIPTION")
    private String description;

    /** The CODE_MAPPING_KEY. */
    @Column(name="CODE_MAPPING_KEY")
    private String codeMappingKey;   

    /** The carriers. */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinTable( name = "CORE_PROD_CODE_TYPE_CARRIER",
    joinColumns = {@JoinColumn(name = "TYPE_LOOK_UP_ID", referencedColumnName = "ID")}, 
        inverseJoinColumns = {@JoinColumn(name = "CARRIER_ID", referencedColumnName = "id")}) 
    private Set<Carrier> carriers;
    
    /**
     * The Constructor.
     */
    public Type() {
        super();
    }
    
    /**
     * The Constructor.
     *
     * @param id the id
     */
    public Type(final String id) {
        this.id = id;
    }
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the description
     */
    public void setDescription(final String description) {
        this.description = description;
    }


	/**
	 * @return the codeMappingKey
	 */
	public String getCodeMappingKey() {
		return codeMappingKey;
	}

	/**
	 * @param codeMappingKey
	 *            the codeMappingKey to set
	 */
	public void setCodeMappingKey(String codeMappingKey) {
		this.codeMappingKey = codeMappingKey;
	}

	/**
     * Sets The carriers.
     *
     * @param carriers The carriers to set.
     */
    public void setCarriers(Set<Carrier> carriers) {
        this.carriers = carriers;
    }

    /**
     * Gets the carriers.
     *
     * @return Returns the carriers.
     */
    public Set<Carrier> getCarriers() {
        return carriers;
    }
}
