package com.cognizant.insurance.omnichannel.dao;

import java.util.List;

import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.goalandneed.dao.GoalAndNeedDao;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public interface GeneraliGoalAndNeedDao extends GoalAndNeedDao {

    /**
     * Gets RelatedTransactions.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @return the fNA by filter
     */
    Response<List<GoalAndNeed>> retrieveRelatedTransactions(Request<SearchCriteria> searchCriteriatRequest);
    /**
     * getGoalAndNeedCountforLMS
     * @param searchCriteriatRequest
     * @return
     */
    
    Response<SearchCountResult> getGoalAndNeedCountforLMS(Request<SearchCriteria> searchCriteriatRequest);
    
    public void updateGoalAndNeedStatusForLeadReassign(Request<GoalAndNeed> goalAndNeedRequest);

}
