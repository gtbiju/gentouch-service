package com.cognizant.insurance.agent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Agent details will be stored.
 * 
 * 
 */
@Entity
@Table(name = "STRUCTURE_FCA")
public class GeneraliAgent {	
	
	@Id
	@Column(name = "SALES_OFFICER_CODE")
	private String agentCode;
	
	@Column(name = "SALES_OFFICER_NAME", columnDefinition = "NVARCHAR(MAX)")
	private String agentName;
	
	@Column(name = "TEAM_Thai", columnDefinition = "NVARCHAR(MAX)")
	private String teamThai;
	
	@Column(name = "TEAM_Eng")
	private String teamEng;
	
	@Column(name = "SO_POSITION_CODE")
	private String agentPositionCode;
	
	@Column(name = "Position")
	private String agentPosition;
	
	@Column(name = "SO_POSITION_DESC", columnDefinition = "NVARCHAR(MAX)")
	private String agentPositionDesc;
	
	@Column(name = "APPOINT_DATE")
	private String appointmentDate;
	
	@Column(name = "SO_LIC_EXPIRE_DATE")
	private String licenseExpireDate;
	
	@Column(name = "fist_LIC_Start_date")
	private String licenseStartDate;
	
	@Column(name = "Start_date")
	private String joinDate;
	
	@Column(name = "STATUS")
	private String agentStatus;
	
	@Column(name = "TERMINATE_DATE")
	private String terminateDate;
	
	@Column(name = "upline1")
	private String nameOfUpline1;
	
	@Column(name = "upline2")
	private String nameOfUpline2;
	
	@Column(name = "SMCode")
	private String smCode;
	
	@Column(name = "SMName", columnDefinition = "NVARCHAR(MAX)")
	private String smName;
	
	@Column(name = "SMPosition")
	private String smPosition;
	
	@Column(name = "SMLIC")
	private String smLicenseStartDate;
	
	@Column(name = "GMCode")
	private String gmCode;
	
	@Column(name = "GMName", columnDefinition = "NVARCHAR(MAX)")
	private String gmName;
	
	@Column(name = "GMPosition")
	private String gmPosition;
	
	@Column(name = "GMLIC")
	private String gmLicenseStartDate;
	
	@Column(name = "working_Day", columnDefinition = "NVARCHAR(MAX)")
	private String workingDay;
	
	@Column(name = "Recruiter_Code")
	private String recruiterCode;
	
	@Column(name = "Zone")
	private String zone;
	
	@Column(name = "TSALESUNT")
	private String gaoCode;
	
	@Column(name = "GAO_Thai", columnDefinition = "NVARCHAR(MAX)")
	private String gaoNameThai;
	
	@Column(name = "GAO_Eng")
	private String gaoNameEng;
	
	@Column(name = "GAO_Name")
	private String headOfGAO;
	
	@Column(name = "SECURITY_NO")
	private String identificationId;
	
	@Column(name = "SALES_OFFICER_LICENSE")
	private String licenseNumber;
	
	@Column(name = "CLTDOB")
	private String agentDob;
	
	@Column(name = "SURNAME", columnDefinition = "NVARCHAR(MAX)")
	private String surName;
	
	@Column(name = "GIVNAME", columnDefinition = "NVARCHAR(MAX)")
	private String givenName;
	
	@Column(name = "TTarget")
	private float targetByMonth;

	@Column(name = "ZHPHONE")
	private String zhPhone;
	
	@Column(name = "ZMPHONE")
	private String zmPhone;
	
	@Column(name = "ZOPHONE")
	private String zoPhone;
	
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getTeamThai() {
		return teamThai;
	}

	public void setTeamThai(String teamThai) {
		this.teamThai = teamThai;
	}

	public String getTeamEng() {
		return teamEng;
	}

	public void setTeamEng(String teamEng) {
		this.teamEng = teamEng;
	}

	public String getAgentPositionCode() {
		return agentPositionCode;
	}

	public void setAgentPositionCode(String agentPositionCode) {
		this.agentPositionCode = agentPositionCode;
	}

	public String getAgentPosition() {
		return agentPosition;
	}

	public void setAgentPosition(String agentPosition) {
		this.agentPosition = agentPosition;
	}

	public String getAgentPositionDesc() {
		return agentPositionDesc;
	}

	public void setAgentPositionDesc(String agentPositionDesc) {
		this.agentPositionDesc = agentPositionDesc;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getLicenseExpireDate() {
		return licenseExpireDate;
	}

	public void setLicenseExpireDate(String licenseExpireDate) {
		this.licenseExpireDate = licenseExpireDate;
	}

	public String getLicenseStartDate() {
		return licenseStartDate;
	}

	public void setLicenseStartDate(String licenseStartDate) {
		this.licenseStartDate = licenseStartDate;
	}

	public String getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}

	public String getAgentStatus() {
		return agentStatus;
	}

	public void setAgentStatus(String agentStatus) {
		this.agentStatus = agentStatus;
	}

	public String getTerminateDate() {
		return terminateDate;
	}

	public void setTerminateDate(String terminateDate) {
		this.terminateDate = terminateDate;
	}

	public String getNameOfUpline1() {
		return nameOfUpline1;
	}

	public void setNameOfUpline1(String nameOfUpline1) {
		this.nameOfUpline1 = nameOfUpline1;
	}

	public String getNameOfUpline2() {
		return nameOfUpline2;
	}

	public void setNameOfUpline2(String nameOfUpline2) {
		this.nameOfUpline2 = nameOfUpline2;
	}

	public String getSmCode() {
		return smCode;
	}

	public void setSmCode(String smCode) {
		this.smCode = smCode;
	}

	public String getSmName() {
		return smName;
	}

	public void setSmName(String smName) {
		this.smName = smName;
	}

	public String getSmPosition() {
		return smPosition;
	}

	public void setSmPosition(String smPosition) {
		this.smPosition = smPosition;
	}

	public String getSmLicenseStartDate() {
		return smLicenseStartDate;
	}

	public void setSmLicenseStartDate(String smLicenseStartDate) {
		this.smLicenseStartDate = smLicenseStartDate;
	}

	public String getGmCode() {
		return gmCode;
	}

	public void setGmCode(String gmCode) {
		this.gmCode = gmCode;
	}

	public String getGmName() {
		return gmName;
	}

	public void setGmName(String gmName) {
		this.gmName = gmName;
	}

	public String getGmPosition() {
		return gmPosition;
	}

	public void setGmPosition(String gmPosition) {
		this.gmPosition = gmPosition;
	}

	public String getGmLicenseStartDate() {
		return gmLicenseStartDate;
	}

	public void setGmLicenseStartDate(String gmLicenseStartDate) {
		this.gmLicenseStartDate = gmLicenseStartDate;
	}

	public String getWorkingDay() {
		return workingDay;
	}

	public void setWorkingDay(String workingDay) {
		this.workingDay = workingDay;
	}

	public String getRecruiterCode() {
		return recruiterCode;
	}

	public void setRecruiterCode(String recruiterCode) {
		this.recruiterCode = recruiterCode;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getGaoCode() {
		return gaoCode;
	}

	public void setGaoCode(String gaoCode) {
		this.gaoCode = gaoCode;
	}

	public String getGaoNameThai() {
		return gaoNameThai;
	}

	public void setGaoNameThai(String gaoNameThai) {
		this.gaoNameThai = gaoNameThai;
	}

	public String getGaoNameEng() {
		return gaoNameEng;
	}

	public void setGaoNameEng(String gaoNameEng) {
		this.gaoNameEng = gaoNameEng;
	}

	public String getHeadOfGAO() {
		return headOfGAO;
	}

	public void setHeadOfGAO(String headOfGAO) {
		this.headOfGAO = headOfGAO;
	}

	public String getIdentificationId() {
		return identificationId;
	}

	public void setIdentificationId(String identificationId) {
		this.identificationId = identificationId;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getAgentDob() {
		return agentDob;
	}

	public void setAgentDob(String agentDob) {
		this.agentDob = agentDob;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public float getTargetByMonth() {
		return targetByMonth;
	}

	public void setTargetByMonth(float targetByMonth) {
		this.targetByMonth = targetByMonth;
	}

	public String getZhPhone() {
		return zhPhone;
	}

	public void setZhPhone(String zhPhone) {
		this.zhPhone = zhPhone;
	}

	public String getZmPhone() {
		return zmPhone;
	}

	public void setZmPhone(String zmPhone) {
		this.zmPhone = zmPhone;
	}

	public String getZoPhone() {
		return zoPhone;
	}

	public void setZoPhone(String zoPhone) {
		this.zoPhone = zoPhone;
	}
}
