/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 27, 2014
 */

package com.cognizant.insurance.endpoint;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.service.BulkUploadService;
import com.cognizant.insurance.constants.Constants;


/**
 * The Class class CodeLookUpServiceEndPoint.
 */
@Controller
@RequestMapping("/bulkUploadService")
public class GeneraliLeadBulkUploadEndPoint {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliLeadBulkUploadEndPoint.class);

    /** The code look up service. */
    @Autowired
    private BulkUploadService bulkUploadService;
  
    /**
     * Generate pdf.
     * 
     * @param inputJsonObj
     *            the input json obj     
     * @param response
     *            the response
     * @return 
     * @return the string           
     * @throws BusinessException
     *             the business exception
     * @throws ParseException 
     */
    @RequestMapping(value = "/validateAndAllocateLeads", method = RequestMethod.POST ,consumes=Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String validateAndAllocateLeads(@RequestBody final String inputJson) {
    		  return new ServiceTemplate() {
    	            @Override
    	            public String doExecute(final String json) throws BusinessException {
    	            	LOGGER.debug("Entered in service");
					return bulkUploadService.validateAndAllocateLeads(inputJson);
					
    	        }
				
    		  }.execute(inputJson);
    	  }
}
