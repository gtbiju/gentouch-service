package com.cognizant.insurance.agent.response.vo;

import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;

/**
 * The Class AgentResponse.
 */
public class AgentProfileResponse {
	
	/** The response info. */
	private ResponseInfo responseInfo;
	
	/** The agent. */
	private GeneraliAgent agent;

	/**
	 * Gets the response info.
	 *
	 * @return the response info
	 */
	public ResponseInfo getResponseInfo() {
		return responseInfo;
	}

	/**
	 * Sets the response info.
	 *
	 * @param responseInfo the new response info
	 */
	public void setResponseInfo(ResponseInfo responseInfo) {
		this.responseInfo = responseInfo;
	}

	/**
	 * Gets the agent.
	 *
	 * @return the agent
	 */
	public GeneraliAgent getAgent() {
		return agent;
	}

	/**
	 * Sets the agent.
	 *
	 * @param agent the new agent
	 */
	public void setAgent(GeneraliAgent agent) {
		this.agent = agent;
	}

}
