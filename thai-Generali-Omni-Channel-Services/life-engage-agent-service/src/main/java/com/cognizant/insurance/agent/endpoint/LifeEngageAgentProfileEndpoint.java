package com.cognizant.insurance.agent.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.service.LifeEngageAgentProfileService;

/**
 * The Class LifeEngageAgentProfileEndpoint.
 */
@Controller
@RequestMapping("/agentProfileService")
public class LifeEngageAgentProfileEndpoint{
	
	/** The agent profile service. */
	@Autowired
	LifeEngageAgentProfileService agentProfileService;
	
    /**
     * Retrieve agent.
     *
     * @param json the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveAgentProfile", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String retrieveAgent(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return agentProfileService.retrieveAgentByCode(json);
            }
        }.execute(json);
    }

}
