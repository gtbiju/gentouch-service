/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of Beneficiaries according to their benefit
 * distribution calculation.
 * 
 * @author 301350
 * 
 * 
 */
public enum BenefitDistributionCalculationCodeList {
    /**
     * Identifies a Beneficiary with benefit distribution calculation 'Balance'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Balance,
    /**
     * Identifies a Beneficiary with benefit distribution calculation 'Equal
     * parts'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    EqualParts,
    /**
     * Identifies a Beneficiary with benefit distribution calculation 'Flat
     * amount'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    FlatAmount,
    /**
     * Identifies a Beneficiary with benefit distribution calculation
     * 'Percentage of balance'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    PercentageOfBalance,
    /**
     * 
     * 
     * 
     * 
     */
    PercentageOfTotal
}
