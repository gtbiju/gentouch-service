/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.Measurement;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.TimePeriod;

/**
 * A bounded area defined by nature, by one or a series of country elements, by
 * an external authority (such as a government), used to identify a location in
 * space; for example, country, city, continent, postal area or risk area.
 * 
 * e.g. land parcel 5, section 12, lot 3
 * 
 * e.g. coastal waters
 * 
 * e.g. Hawthorne, NY Postal area, ZIP code 10532
 * 
 * e.g. Hurricane risk area
 * 
 * e.g. Manhattan, New York
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PLACE_TYPE", discriminatorType = DiscriminatorType.STRING)
public class Place extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 660233651007473824L;

    /**
     * General name given to the location in space (Place).
     * 
     * 
     * 
     */
    @Basic(fetch=FetchType.EAGER)
    @Column(columnDefinition="nvarchar(400)")
    private String name;

    /**
     * A textual statement used to help identify a place.
     * 
     * e.g: Belgium is a small country located on the European coast of the
     * North Sea, bordered by the Netherlands, Germany, France and Luxembourg.
     * 
     * 
     * 
     * 
     */
    private String description;

    /**
     * The measurement of the area of this place.
     * 
     * e.g. 400 square kilometers
     * 
     * 
     * 
     */
    @Transient
    private Measurement surfaceAreaMeasurement;

    /**
     * The time period at which the place is available for use.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private TimePeriod availablePeriod;

    /**
     * The standard abbreviation or code for the geographic area.
     * 
     * e.g: NY
     * 
     * 
     * 
     */
    private String abbreviation;

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the surfaceAreaMeasurement.
     * 
     * @return Returns the surfaceAreaMeasurement.
     */
    public Measurement getSurfaceAreaMeasurement() {
        return surfaceAreaMeasurement;
    }

    /**
     * Gets the availablePeriod.
     * 
     * @return Returns the availablePeriod.
     */
    public TimePeriod getAvailablePeriod() {
        return availablePeriod;
    }

    /**
     * Gets the abbreviation.
     * 
     * @return Returns the abbreviation.
     */
    public String getAbbreviation() {
        return abbreviation;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The surfaceAreaMeasurement.
     * 
     * @param surfaceAreaMeasurement
     *            The surfaceAreaMeasurement to set.
     */
    public void setSurfaceAreaMeasurement(
            final Measurement surfaceAreaMeasurement) {
        this.surfaceAreaMeasurement = surfaceAreaMeasurement;
    }

    /**
     * Sets The availablePeriod.
     * 
     * @param availablePeriod
     *            The availablePeriod to set.
     */
    public void setAvailablePeriod(final TimePeriod availablePeriod) {
        this.availablePeriod = availablePeriod;
    }

    /**
     * Sets The abbreviation.
     * 
     * @param abbreviation
     *            The abbreviation to set.
     */
    public void setAbbreviation(final String abbreviation) {
        this.abbreviation = abbreviation;
    }

}
