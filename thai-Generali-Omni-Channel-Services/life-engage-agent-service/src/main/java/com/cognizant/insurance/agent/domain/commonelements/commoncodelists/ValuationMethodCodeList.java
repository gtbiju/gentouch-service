/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * A code list identifying the method used to determine the item value.
 * 
 * @author 301350
 * 
 * 
 */
public enum ValuationMethodCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Appraisal,
    /**
     * 
     * 
     * 
     * 
     */
    ComparableSale,
    /**
     * 
     * 
     * 
     * 
     */
    SalesReceipt
}
