package com.cognizant.insurance.generic.dao;

import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.request.Request;

/**
 * The Interface AgentDao.
 */
public interface AgentDao {

	/**
	 * Retrieve agent by code.
	 *
	 * @param request the request
	 * @return the agent
	 */
	Agent retrieveAgentByCode(Request<Agent> request);
}
