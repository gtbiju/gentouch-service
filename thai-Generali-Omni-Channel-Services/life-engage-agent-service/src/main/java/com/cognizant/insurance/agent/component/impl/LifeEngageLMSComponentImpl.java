package com.cognizant.insurance.agent.component.impl;



import static com.cognizant.insurance.agent.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.agent.audit.LifeEngageAudit;
import com.cognizant.insurance.agent.component.LifeEngageLMSComponent;
import com.cognizant.insurance.agent.component.TrackingIdGenerator;
import com.cognizant.insurance.agent.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.constants.ErrorConstants;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.core.exception.SystemException;
import com.cognizant.insurance.agent.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.agent.dao.exception.DaoException;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.agent.repository.LMSRepository;
import com.cognizant.insurance.agent.request.vo.RequestInfo;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.searchcriteria.SearchCriteria;


@Component
public class LifeEngageLMSComponentImpl implements LifeEngageLMSComponent {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageLMSComponentImpl.class);
	
	@Autowired
    @Qualifier("lmsSaveMapping")
    private LifeEngageSmooksHolder saveLMSHolder;
	
	@Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;
	
	/** The retrieve LMS holder. */
    @Autowired
    @Qualifier("lmsRetrieveMapping")
    private LifeEngageSmooksHolder retrieveLMSHolder;
    
    /** The retrieve LMS holder. */
    @Autowired
    @Qualifier("lmsRetrieveListMapping")
    private LifeEngageSmooksHolder retrieveLMSListHolder;
    
    /** The e app repository. */
    @Autowired
    private LMSRepository lmsRepository;
    
    /** The retrieve by filter holder. */
    @Autowired
    @Qualifier("retrieveByFilterMapping")
    private LifeEngageSmooksHolder retrieveByFilterHolder;
    
    /** The Constant SUCCESS_MESSAGE_SAVE. */
    private static final String SUCCESS_MESSAGE_SAVE = "Lead Saved";

    /** The Constant SUCCESS_MESSAGE_UPDATE. */
    private static final String SUCCESS_MESSAGE_UPDATE = "Lead Updated";

    /** The Constant REJECTED_MESSAGE. */
    private static final String REJECTED_MESSAGE = "Lead Rejected";

    /** The Constant LMS. */
    private static final String LMS = "LMS";

    
    @Transactional("le_txn_manager")
    @Override
	public String retrieveLMS(RequestInfo requestInfo, JSONObject jsonObj) throws BusinessException, ParseException {
    	 String response = null;
         final List<Transactions> transactionsList = new ArrayList<Transactions>();
         
         if (jsonObj.has(LifeEngageComponentHelper.KEY_1) && jsonObj.getString(LifeEngageComponentHelper.KEY_1) != null
                 && !"".equals(jsonObj.getString(LifeEngageComponentHelper.KEY_1))
                 && !jsonObj.getString(LifeEngageComponentHelper.KEY_1).equals(Constants.NULL)) {
        	
        	 final Transactions requestPayload = (Transactions) retrieveLMSHolder.parseJson(jsonObj.toString());
        	 Transactions responseTransactions = null;
        	 
        	 try{
        		 
        		 responseTransactions = lmsRepository.retrieveLMS(requestPayload, requestInfo.getTransactionId(), requestInfo);
               		 
        	 }catch(Exception e){
        		 LOGGER.error("Unable to retrieve LMS :" + e.getMessage());
                 responseTransactions = requestPayload;
                 LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage()
                         + " : " + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
        	 }
        	 LOGGER.trace("Inside LifeEngageEApp component retrieveEApp : responseTransactions" + responseTransactions);
             LifeEngageComponentHelper.parseLMSTrasactionDetails(jsonObj.toString(), responseTransactions);
             transactionsList.add(responseTransactions);
             LOGGER.trace("LifeEngageEAppComponent.retrieveEApp: responseTransactions" + responseTransactions);
         }
         response = retrieveLMSHolder.parseBO(transactionsList);
         response = response.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
         return response;
	}
    
    @Transactional("le_txn_manager")
	@Override
	public String retrieveAllLMS(RequestInfo requestInfo, JSONObject jsonObj) throws BusinessException, ParseException {
		
		final Transactions transactions = (Transactions) retrieveLMSHolder.parseJson(jsonObj.toString());
	    boolean fullFetch = true;
	    List<Transactions> transactionsList = new ArrayList<Transactions>();
	    
	    if (jsonObj.has(LifeEngageComponentHelper.KEY_10) && jsonObj.getString(LifeEngageComponentHelper.KEY_10) != null) {
	    	 if (LifeEngageComponentHelper.FULL_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
	                // Retrieve All details for all proposals for an agent
	                transactionsList = lmsRepository.retrieveAll(transactions, requestInfo, true);
	                LOGGER.trace("Inside LifeEngageLMSComponentImpl RetrieveAllLMS - FullDetails: transactionsList"
	                        + transactionsList.size());
	            } else if (LifeEngageComponentHelper.BASIC_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
	                // Retrieve Only the basic details required for listing
	                transactions.setProposalNumber(null);
	                transactionsList = lmsRepository.retrieveAll(transactions, requestInfo, false);
	                LOGGER.trace("Inside LifeEngageLMSComponentImpl RetrieveAllLMS : transactionsList"
	                        + transactionsList.size());
	                fullFetch = false;
	            }
	        }
	    LifeEngageComponentHelper.buildTransactionsList(transactionsList, jsonObj);
        String eAppMapToReturn = "{\"Transactions\":[]}";
        if (fullFetch) {
            eAppMapToReturn = retrieveLMSHolder.parseBO(transactionsList);
        } else {
            eAppMapToReturn = retrieveLMSListHolder.parseBO(transactionsList);
        }
        eAppMapToReturn = eAppMapToReturn.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
        return eAppMapToReturn;
		
	}
	
	/**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }

    @Transactional("le_txn_manager")
	@Override
	public String saveLMS(String json, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit) {


        LOGGER.trace("Inside LifeEngageEApp component saveEApp json: json" + json);
        String lmsResponse;
        Transactions transactions = new Transactions();
        String successMessage;
        String offlineIdentifier = null;
        try {
            LifeEngageComponentHelper.parseLMSTrasactionDetails(json, transactions);
            // Store to a temp variable to set it back to response.
            offlineIdentifier = transactions.getOfflineIdentifier();
            // Do not process eApp data if already done
            /*if (auditRepository.isTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                throwSystemException(true, ErrorConstants.LE_SYNC_ERR_102, Constants.PROCESSED_TRANSACTION);
            }*/
            transactions = (Transactions) saveLMSHolder.parseJson(json);

			/**
			 * Update flow was depending on lmsid alone. Instead will verify
			 * whether the data exist for transTrackingId and AgentId
			 * combination. If so , considered as update flow. Bug fix when
			 * multple save hapenning for same record with same transTrackingId,
			 * in data gets saved in backend, but not getting updated in UI and
			 * hence the same record comes again withour fnaId
			 **/

			Transactions responseTransactions = new Transactions();
			Customer customer = new Customer();
			if (StringUtils.isNotEmpty(transactions.getKey1())
					|| StringUtils
							.isNotEmpty(transactions.getTransTrackingId())) {
				responseTransactions = lmsRepository.retrieveLMS(transactions,
						requestInfo.getTransactionId(), requestInfo);
			}
			if (responseTransactions != null
					&& responseTransactions.getCustomer() != null
					&& responseTransactions.getCustomer().getIdentifier() != null) {
				customer = responseTransactions.getCustomer();
				if (StringUtils.isEmpty(transactions.getKey1())) {
					transactions.setKey1(responseTransactions.getCustomer()
							.getIdentifier());
				}
				transactions.setMode(Constants.STATUS_UPDATE);
				successMessage = SUCCESS_MESSAGE_UPDATE;
			} else {
            	
                transactions.setKey1(transactions.getAgentId()+String.valueOf(idGenerator.generate(LMS)));
                transactions.setMode(Constants.STATUS_SAVE);
                successMessage = SUCCESS_MESSAGE_SAVE;
            }

            onBeforeSave(transactions);

            final String proposalstatus = lmsRepository.saveLMS(transactions, requestInfo,customer);
            if (Constants.REJECTED.equals(proposalstatus)) {
                successMessage = REJECTED_MESSAGE;
            }
            onAfterSave(transactions);

            LifeEngageComponentHelper.createResponseStatus(transactions, proposalstatus, successMessage, null);

            transactions.setOfflineIdentifier(offlineIdentifier);

            //auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,lifeEngageAudit);
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
			if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						Constants.FAILURE, e.getMessage() + " : "
								+ getExceptionMessage(e),
						ErrorConstants.LE_SYNC_ERR_105);
			} else if (ErrorConstants.LE_SYNC_ERR_102.equals(e.getErrorCode())) {
				LifeEngageComponentHelper.createResponseStatus(transactions, LifeEngageComponentHelper.SUCCESS, SUCCESS_MESSAGE_SAVE, null);

			} else {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						Constants.FAILURE, e.getMessage() + " : "
								+ getExceptionMessage(e), e.getErrorCode());
			}
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                        + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_103);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                        + getExceptionMessage(e), e.getErrorCode());
            }
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_104);
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (ParseException e) {
            LOGGER.error("ParseException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_101);
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
            lmsResponse = saveLMSHolder.parseBO(transactions);

        }
        LOGGER.trace("Inside LifeEngageEApp component eAppResponse : eAppResponse" + lmsResponse);
        return lmsResponse;
    
	}
    
    @Override
    public String retrieveByFilter(final RequestInfo requestInfo, final JSONArray jsonRqArray) {
        Transactions transactions = new Transactions();
        String response = null;
        String agentId = null;
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        try {
            if (jsonRqArray != null) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);
                    JSONObject jsonTransactionData = null;
                    SearchCriteria searchCriteria = new SearchCriteria();
                    agentId = jsonTransactionsObj.getString(Constants.KEY11);
                    if (agentId != null && !("").equals(agentId)) {
                        ArrayList<String> modesList = new ArrayList<String>();
                        if (!(jsonTransactionsObj.isNull(Constants.TRANSACTIONDATA))) {
                            jsonTransactionData = jsonTransactionsObj.getJSONObject(Constants.TRANSACTIONDATA);
                            final JSONObject searchCriteriaRqJson =
                                    jsonTransactionData.getJSONObject(Constants.SEARCHCRITERIAREQUEST);
                            JSONArray jsonModesArray = searchCriteriaRqJson.getJSONArray(Constants.MODES);
                            for (int j = 0; j < jsonModesArray.length(); j++) {
                                modesList.add((String) jsonModesArray.get(j));

                            }
                            String command = searchCriteriaRqJson.getString(Constants.COMMAND);
                            searchCriteria.setCommand(command);
                            searchCriteria.setValue(searchCriteriaRqJson.getString(Constants.VALUE));
                            searchCriteria.setAgentId(agentId);
                            searchCriteria.setModes(modesList);
                            if (Constants.LISTING_DASHBOARD.equals(command)) {
                                transactionsList = retrieveFilter(requestInfo, searchCriteria);
                            }
                        }
                    } else {
                         throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, Constants.AGENTID_MISSING);
                    }
                }
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (Exception e) {
            LOGGER.error("Unable to retrieve listing details for retrieve filter :" + e.getMessage());
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

        } finally {
            response = retrieveByFilterHolder.parseBO(transactionsList);

        }

        return response;
    }
	
	/**
     * On before save.
     * 
     * @param transactions
     *            the transactions
     * @throws ParseException
     *             the parse exception
     */
    protected void onBeforeSave(Transactions transactions) throws ParseException {
        transactions.setCreationDateTime(LifeEngageComponentHelper.getCurrentdate());
        transactions.setModifiedTimeStamp(LifeEngageComponentHelper.getCurrentdate());
    }

    /**
     * On after save.
     * 
     * @param transactions
     *            the transactions
     */
    protected void onAfterSave(Transactions transactions) {

    }
    
    /**
     * Retrieve filter.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    protected List<Transactions> retrieveFilter(final RequestInfo requestInfo, final SearchCriteria searchCriteria)
            throws BusinessException, ParseException {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
        ArrayList<String> modeList = searchCriteria.getModes();
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        if (modeList.size() > 0) {
            String mode = modeList.get(0);
           if (LMS.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = lmsRepository.retrieveByFilterLMS(requestInfo, searchCriteria);
            }
            LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter ");
        }
        return transactionsList;
    }

	

}
