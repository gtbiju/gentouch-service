/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 22, 2015
 */
package com.cognizant.insurance.component;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * The Interface interface LifeEngagePushNotificationComponent.
 */
public interface LifeEngagePushNotificationComponent {

    /**
     * Register device.
     * 
     * @param requestInfo
     *            the request info
     * @param json
     *            the json
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String registerDevice(RequestInfo requestInfo, String json, LifeEngageAudit lifeEngageAudit)
            throws BusinessException;

    /**
     * Un register device.
     * 
     * @param requestInfo
     *            the request info
     * @param json
     *            the json
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String unRegisterDevice(RequestInfo requestInfo, String json, LifeEngageAudit lifeEngageAudit)
            throws BusinessException;

    /**
     * Push notifications.
     * 
     * @param requestInfo
     *            the request info
     * @param json
     *            the json
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String pushNotifications(RequestInfo requestInfo, String json, LifeEngageAudit lifeEngageAudit)
            throws BusinessException;
}
