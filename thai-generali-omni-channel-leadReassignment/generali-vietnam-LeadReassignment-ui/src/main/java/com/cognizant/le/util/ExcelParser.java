package com.cognizant.le.util;

import java.math.BigDecimal;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;

public class ExcelParser {
	

	public boolean isRowEmpty(XSSFRow row) {
		for (int c = row.getFirstCellNum(); c <= row.getLastCellNum(); c++) {
			Cell cell = row.getCell(c);
			if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK
					&& StringUtils.isNotBlank(cell.toString()))
				return false;
		}
		return true;
	}

	public String returnString(final Cell cell) {
		if (cell.getCellType() == 0) {
			return (new BigDecimal(cell.getNumericCellValue())).toString();
		} else if (cell.getCellType() == 1) {
			return (cell.getStringCellValue());
		} else if (cell.getCellType() == 4) {
			return (new Boolean(cell.getBooleanCellValue())).toString();
		} else if (cell.getCellType() == 2) {
			return ((new BigDecimal(cell.getNumericCellValue()))).toString();
		}
		return null;
	}

	public Integer returnNumber(final Cell cell) throws Exception {
		if (cell.getCellType() == 0) {
			return (new BigDecimal(cell.getNumericCellValue()).intValue());
		} else if (cell.getCellType() == 1) {
			return (Integer.parseInt(cell.getStringCellValue()));
		} else if (cell.getCellType() == 4) {
			throw new Exception("Expected numeric value, encountered Boolean");
		} else if (cell.getCellType() == 2) {
			return ((new BigDecimal(cell.getNumericCellValue())).intValue());
		}
		return null;

	}

}
