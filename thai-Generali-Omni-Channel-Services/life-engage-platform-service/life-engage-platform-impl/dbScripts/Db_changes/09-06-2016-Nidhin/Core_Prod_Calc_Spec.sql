-- CORE_PROD_CALC_SPEC
-- UPDATE
UPDATE [dbo].[CORE_PROD_CALC_SPEC] SET [calculationType_ID]='50118' where [parentProduct_id]=1

-- INSERT
SET IDENTITY_INSERT [dbo].[CORE_PROD_CALC_SPEC] ON 
INSERT [dbo].[CORE_PROD_CALC_SPEC] ([id], [basicDataCompleteCode], [contextId], [creationDateTime], [transactionId], [typeName], [ACTUAL_RULE_REF], [INPUT_PARAMETERS], [NAME], [GROUP_NAME], [OUTPUT_PARAMETERS], [VERSION], [calculationType_ID], [parentProduct_id], [modifiedDateTime], [transTrackingId], [transactionKeys_id]) VALUES (2, NULL, NULL, NULL, NULL, NULL, N'CIBProduct', N'', N'CIB Product', N'', N'', N'1', N'50121', 1, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[CORE_PROD_CALC_SPEC] OFF

-- CORE_PROD_TEMPLATES
-- INSERT
INSERT [dbo].[CORE_PROD_TEMPLATES] ([TEMPLATE_ID], [TEMPLATE_NAME], [VERSION], [LANGUAGE], [COUNTRY_CODE]) VALUES (N'1003', N'CI_IllustrationOutput', CAST(1.000 AS Decimal(5, 3)), N'en', N'')


-- CORE_PROD_TEMPLATE_MAPPING
-- INSERT
INSERT [dbo].[CORE_PROD_TEMPLATE_MAPPING] ([ID], [PROD_ID], [TEMPLATE_CONTEXT], [TEMPLATE_TYPE], [TEMPLATE_ID] ) VALUES (3, 1, N'', N'22003', N'1003')