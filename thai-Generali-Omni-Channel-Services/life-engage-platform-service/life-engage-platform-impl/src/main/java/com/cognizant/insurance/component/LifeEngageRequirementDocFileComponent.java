/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 325754
 * @version       : 0.1, Oct 6, 2015
 */
package com.cognizant.insurance.component;

import java.io.IOException;

import org.json.JSONObject;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * @author 325754
 * 
 */
public interface LifeEngageRequirementDocFileComponent {

    /**
     * 
     * @param string
     * @param transactionId
     * @return
     * @throws BusinessException
     * @throws IOException
     */
    public String uploadRequirementDocFile(String string, String transactionId) throws BusinessException, IOException;

    /**
     * @param jsonObj
     * @param transactionId
     * @return
     * @throws IOException
     */
    public String getRequirementDocFile(JSONObject jsonObj, String transactionId);

    /**
     * @param jsonObj
     * @param transactionId
     * @return
     * @throws IOException
     */
    public String getRequirementDocFileList(JSONObject jsonObj, String transactionId);
    
    /**
     * 
     * @param string
     * @param transactionId
     * @return
     * @throws BusinessException
     * @throws IOException
     */
    public String saveRequirement(String string, String transactionId) throws BusinessException, IOException;

}
