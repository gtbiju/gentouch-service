/**
 * 
 */
package com.cognizant.insurance.domain.finance.bankaccount;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;

import com.cognizant.insurance.domain.commonelements.commoncodelists.BankAccountTypeCodeList;
import com.cognizant.insurance.domain.finance.Account;

/**
 * A physical account provided by a bank, which is used for a myriad of things
 * such as paying bills (for the insurance company), receiving premium payments
 * from insureds, and a physical place to transfer funds to and from.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("BANKACCOUNT")
public class BankAccount extends Account {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2895909434702706291L;

    /**
     * Indicates the type of bank account.
     * 
     * 
     * 
     */
    @Enumerated
    private BankAccountTypeCodeList bankAccountTypeCode;

    /**
     * The bank check number for an Contact bank account.
     * 
     * 
     * 
     * 
     * 
     */
    
    
    private Integer bankCheckIdentifier;

    /**
     * The name of the branch.
     * 
     * 
     * 
     */
	@Column(columnDefinition = "NVARCHAR(MAX)")
    private String branchName;

    /**
     * The branch of the bank for an European bank account.
     * 
     * 
     * 
     * 
     * 
     */
    private String branchCode;

    /**
     * The international bank account number (IBAN) for an European bank
     * account.
     * 
     * 
     * 
     */
    private Integer ibanCheckIdentifier;

    /**
     * The country code for an European bank account.
     * 
     * 
     * 
     * 
     * 
     */
    private String countryCode;

    /**
     * The code of the bank for an European bank account.
     * 
     * 
     * 
     * 
     * 
     */
    private String bankCode;
    
	/**
	 * The Banking object on another associated Holding is the preferred
	 * location for this information.
	 */
	@Column(columnDefinition = "NVARCHAR(MAX)")
    private String bankName;

	/**
    * The bank account number.
    * 
    * 
    * 
    */
	private String accNumber;
    /**
     * Gets the bankAccountTypeCode.
     * 
     * @return Returns the bankAccountTypeCode.
     */
    public BankAccountTypeCodeList getBankAccountTypeCode() {
        return bankAccountTypeCode;
    }

    /**
     * Gets the bankCheckIdentifier.
     * 
     * @return Returns the bankCheckIdentifier.
     */
    public Integer getBankCheckIdentifier() {
        return bankCheckIdentifier;
    }

    /**
     * Gets the branchName.
     * 
     * @return Returns the branchName.
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * Gets the branchCode.
     * 
     * @return Returns the branchCode.
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Gets the ibanCheckIdentifier.
     * 
     * @return Returns the ibanCheckIdentifier.
     */
    public Integer getIbanCheckIdentifier() {
        return ibanCheckIdentifier;
    }

    /**
     * Gets the countryCode.
     * 
     * @return Returns the countryCode.
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Gets the bankCode.
     * 
     * @return Returns the bankCode.
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * Sets The bankAccountTypeCode.
     * 
     * @param bankAccountTypeCode
     *            The bankAccountTypeCode to set.
     */
    public void setBankAccountTypeCode(
            final BankAccountTypeCodeList bankAccountTypeCode) {
        this.bankAccountTypeCode = bankAccountTypeCode;
    }

    /**
     * Sets The bankCheckIdentifier.
     * 
     * @param bankCheckIdentifier
     *            The bankCheckIdentifier to set.
     */
    public void setBankCheckIdentifier(final Integer bankCheckIdentifier) {
        this.bankCheckIdentifier = bankCheckIdentifier;
    }

    /**
     * Sets The branchName.
     * 
     * @param branchName
     *            The branchName to set.
     */
    public void setBranchName(final String branchName) {
        this.branchName = branchName;
    }

    /**
     * Sets The branchCode.
     * 
     * @param branchCode
     *            The branchCode to set.
     */
    public void setBranchCode(final String branchCode) {
        this.branchCode = branchCode;
    }

    /**
     * Sets The ibanCheckIdentifier.
     * 
     * @param ibanCheckIdentifier
     *            The ibanCheckIdentifier to set.
     */
    public void setIbanCheckIdentifier(final Integer ibanCheckIdentifier) {
        this.ibanCheckIdentifier = ibanCheckIdentifier;
    }

    /**
     * Sets The bankCode.
     * 
     * @param bankCode
     *            The bankCode to set.
     */
    public void setBankCode(final String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * Sets The countryCode.
     * 
     * @param countryCode
     *            The countryCode to set.
     */
    public void setCountryCode(final String countryCode) {
        this.countryCode = countryCode;
    }

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(final String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	public String getAccNumber() {
		return accNumber;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}
}
