/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.contactcodelists;

/**
 * Contains the allowed values for the result of a validation check on contact
 * preference.
 * 
 * @author 301350
 * 
 * 
 */
public enum ValidationResultCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Invalid,
    /**
     * 
     * 
     * 
     * 
     */
    InvalidButAccepted,
    /**
     * 
     * 
     * 
     * 
     */
    Valid
}
