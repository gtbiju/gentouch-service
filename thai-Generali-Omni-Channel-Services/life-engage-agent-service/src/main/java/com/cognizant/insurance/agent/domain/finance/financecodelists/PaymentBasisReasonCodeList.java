/**
 * 
 */
package com.cognizant.insurance.agent.domain.finance.financecodelists;

/**
 * Payment Basis Reason.
 * 
 * @author 301350
 * 
 * 
 */
public enum PaymentBasisReasonCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * (Payment not through debit.).
     * 
     * 
     * 
     */
    PaymentsNewbusiness1stpaymentfordirectnewbusinesswithtrailoption,
    /**
     * Asset-based (i.e., vs. premium based) commission paid to the broker -
     * paid only on certain products such as SVUL, VULII, etc. Payments usually
     * begin in year 2 of the policy and are typically paid for the lifetime of
     * the contract.
     * 
     * 
     * 
     * 
     * 
     */
    RenewalsTrailassetbased,
    /**
     * 
     * 
     * 
     * 
     */
    AddOnsAddonsthroughbrokerdealer,
    /**
     * 
     * 
     * 
     * 
     */
    AddOnsDirectaddons,
    /**
     * 
     * 
     * 
     * 
     */
    AddOnsAddonsemployee,
    /**
     * 
     * 
     * 
     * 
     */
    BonusesPersistencybonusassetbased,
    /**
     * 
     * 
     * 
     * 
     */
    BonusesPeriodicbonuspremiumbased,
    /**
     * 
     * 
     * 
     * 
     */
    BonusesBonusother,
    /**
     * 
     * 
     * 
     * 
     */
    AdvanceNetpaymentadjustmentforadvance,
    /**
     * 
     * 
     * 
     * 
     */
    AdvanceCommissionpaymentforanadvance,
    /**
     * 
     * 
     * 
     * 
     */
    AdvanceCommissionpaymentfora1035exchangeadvance,
    /**
     * (Payment not through debit.).
     * 
     * 
     * 
     */
    PaymentsNewbusiness1stpaymentfordirectnewbusinesswithouttrailoption,
    /**
     * 
     * 
     * 
     * 
     */
    CorrectionsMGAoverrides,
    /**
     * 
     * 
     * 
     * 
     */
    CorrectionsIncorrectnetting,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * 
     * 
     * 
     * 
     */
    MiscellaneousMarketing,
    /**
     * 
     * 
     * 
     * 
     */
    MiscellaneousSpecialpaymenttofirm,
    /**
     * 
     * 
     * 
     * 
     */
    MiscellaneousFeeforservice,
    /**
     * 
     * 
     * 
     * 
     */
    ChargebacksAdjustmentsAdvanceCBfornetadjustmentforanadvance,
    /**
     * 
     * 
     * 
     * 
     */
    ChargebacksAdjustmentsAdvanceCBforadjustmentfora1035advance,
    /**
     * 
     * 
     * 
     * 
     */
    ChargebacksAdjustmentsAdvanceCBforfulladvance,
    /**
     * 
     * 
     * 
     * 
     */
    CancellationsCBforcancelagreement,
    /**
     * 
     * 
     * 
     * 
     */
    CancellationsCBcanceledduringfreelook,
    /**
     * 
     * 
     * 
     * 
     */
    PaymentsNewbusinessDirectbusinessnobrokerdealerinvolvement,
    /**
     * 
     * 
     * 
     * 
     */
    SurrenderschangesCBduetopartialsurrender,
    /**
     * 
     * 
     * 
     * 
     */
    SurrenderschangesCBduetopartialsurrender1st6months,
    /**
     * 
     * 
     * 
     * 
     */
    SurrenderschangesCBduetopartialsurrender2nd6months,
    /**
     * 
     * 
     * 
     * 
     */
    SurrenderschangesCBduetosurrenderafter1styear,
    /**
     * 
     * 
     * 
     * 
     */
    SurrenderschangesCBduetofullsurrender,
    /**
     * 
     * 
     * 
     * 
     */
    SurrenderschangesCBfordecreaseinagreement,
    /**
     * 
     * 
     * 
     * 
     */
    DeathClaimCBduetodeathclaim,
    /**
     * 
     * 
     * 
     * 
     */
    DeathClaimCBduetodeathclaim1st6months,
    /**
     * 
     * 
     * 
     * 
     */
    DeathClaimCBduetodeathclaim2nd6months,
    /**
     * 
     * 
     * 
     * 
     */
    DeathClaimCBduetodeathclaimafter1styear,
    /**
     * 
     * 
     * 
     * 
     */
    PaymentsNewbusinessElectronicappnewbusinesstrail,
    /**
     * or agent/broker dealer change.
     * 
     * 
     * 
     */
    AgentBrokerDealerrelatedCBduetowrongagentbrokerdealer,
    /**
     * 
     * 
     * 
     * 
     */
    AgentBrokerDealerrelatedCBforinadequatelicensingofrepresentative,
    /**
     * 
     * 
     * 
     * 
     */
    CorrectionsCBforagreementnettedatincorrectrate,
    /**
     * 
     * 
     * 
     * 
     */
    CorrectionsCBduetoduplicatepayment,
    /**
     * 
     * 
     * 
     * 
     */
    CorrectionsCBfornettedincorrectlyduetoage,
    /**
     * 
     * 
     * 
     * 
     */
    CorrectionsCBforoverpaid,
    /**
     * 
     * 
     * 
     * 
     */
    MiscellaneousCBformarketing,
    /**
     * 
     * 
     * 
     * 
     */
    MiscellaneousCBforotheradjustment,
    /**
     * 
     * 
     * 
     * 
     */
    PaymentisfromanonTaxQualifiedrollover,
    /**
     * 
     * 
     * 
     * 
     */
    PaymentsNewbusinessElectronicappnewbusinessnotrail,
    /**
     * This is an amount that is paid to a manager (like General Agent or
     * District Manager) and not to the agent/producer/broker. It's a set %.
     * 
     * 
     * 
     * 
     * 
     */
    FirstYearOverride,
    /**
     * Amount of commission paid to the broker/agent/producer (only paid in
     * renewal years usually 2-10) depends on product.
     * 
     * Notes: This is actually a 'commission' in the truest sense of the word
     * but it's NOT trail as the current model indicates. Trail is another
     * animal and we'd like simple 'renewal' commissions (normally premium based
     * for us)
     * 
     * 
     * 
     * 
     * 
     * 
     */
    RenewalCommission,
    /**
     * This is an amount that is paid to a manager (like General Agent or
     * District Manager) and not to the agent/producer/broker on renewal
     * premiums (usually in years 2-10). It's a set %.
     * 
     * Notes: Emphasis is on 'override' - it's not considered a 'commission'
     * 
     * 
     * 
     * 
     * 
     * 
     */
    RenewalOverride,
    /**
     * Amount paid to the producer/broker/agent for which they must provide
     * tangible evidence of 'servicing' the policy- after renewal period -
     * usually years 11+.
     * 
     * 
     * 
     * 
     * 
     */
    ServiceFeeCommission,
    /**
     * Amount of override paid to a manager (like General Agent or District
     * Manager) for Trail commissions - Based on asset value vs. premium and
     * paid only on certain products such as SVUL, VULII, etc. Payments begin in
     * year 2 of the policy and are paid for the lifetime of the contract.
     * 
     * 
     * 
     * 
     * 
     */
    TrailOverride,
    /**
     * Producer receives additional set % of premium per policy (50% most often)
     * on First year business only. This amount is in lieu of any administrative
     * expense, rent, general office supplies, etc.
     * 
     * Notes: Typically used by New York Companies
     * 
     * 
     * 
     * 
     * 
     * 
     */
    ExpenseReimbursementAllowance,
    /**
     * A manager (like General Agent or District Manager) receives additional
     * set % of premium per policy either 0 or 100% on First year business only.
     * This amount is in lieu of any administrative expense, rent, general
     * office supplies, etc.
     * 
     * Notes: Typically used by New York Companies
     * 
     * 
     * 
     * 
     * 
     * 
     */
    ExpenseReimbursementAllowanceOverride,
    /**
     * Allows for split of the ERA to various parties to the policy and is based
     * on first year paid commissions; payable to both a manager (like General
     * Agent or District Manager) and/or Broker.
     * 
     * 
     * 
     * 
     * 
     */
    ExpenseReimbursementAllowanceSupplementalSplitOverride,
    /**
     * Amount paid per policy. Normal commission structure ignored. Represents
     * the GAs portion or split of the total comp payable on a policy in the
     * first year.
     * 
     * Notes: It's the total amount of $ available under all the commission
     * structures; it is split and allocated according to agreed parties
     * recommendation. Allows flexible payouts instead of the traditional
     * straight commission structure.
     * 
     * 
     * 
     * 
     * 
     */
    FirstYearGrossDealerCompensationforGA,
    /**
     * Amount paid per policy. Normal commission structure ignored. Represents
     * the GAs portion or split of the total comp payable on a policy generally
     * in years 2-10.
     * 
     * Notes: Represents the GAs portion or split of the total comp payable on
     * the policy in the first year.
     * 
     * 
     * 
     * 
     * 
     * 
     */
    RenewalGrossDealerCompensationforGA,
    /**
     * 
     * 
     * 
     * 
     */
    PaymentsNewbusinessNewbusinessemployee,
    /**
     * Amount paid per policy. Normal commission structure ignored. Represents
     * the GAs portion or split of the total asset-based comp payable on a
     * policy generally in years 2+.
     * 
     * 
     * 
     * 
     * 
     */
    TrailGrossDealerCompensationforGA,
    /**
     * Amount paid per policy. Normal commission structure ignored. Represents
     * the Broker's portion or split of the total comp payable on a policy in
     * the first year.
     * 
     * 
     * 
     * 
     * 
     */
    FirstYearGrossDealerCompensationforBroker,
    /**
     * Amount paid per policy. Normal commission structure ignored. Represents
     * the Broker's portion or split of the total comp payable on a policy
     * generally in years 2-10.
     * 
     * 
     * 
     * 
     * 
     */
    RenewalGrossDealerCompensationforBroker,
    /**
     * Amount paid per policy. Normal commission structure ignored. Represents
     * the Broker's portion or split of the total asset-based comp payable on a
     * policy generally in years 2+.
     * 
     * 
     * 
     */
    TrailGrossDealerCompensationforBroker,
    /**
     * Payable to wholesale and retail producers this amount does not have to be
     * 'earned' but the amount is written into the contract at the time of
     * appointment;.
     * 
     * Notes: Broker could get both Expense Reimbursement Allowance ERA and
     * Marketing allowance as it's producer specific; paid weekly and could be
     * paid to the GA or Broker
     * 
     * 
     * 
     * 
     * 
     * 
     */
    MarketingAllowanceforBroker,
    /**
     * Can be positive or negative meaning a debit or credit. Normally a
     * positive or 'advance'
     * 
     * Notes: Usually done to advance the entity $ especially in large $ cases;
     * used when the system calculated the commissions incorrectly, rates were
     * loaded incorrectly; transactions are hung up in the system;
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Adjustment,
    /**
     * Original commission code is returned with same $ paid to the producer but
     * shown as a negative; want a void indicator so that the transaction can be
     * identified as 'void'.
     * 
     * 
     * 
     * 
     * 
     */
    WillbereasoncodeoforiginaltransactionAmountsignwillbereversed,
    /**
     * Indicator so that these transactions can be aggregated and known as
     * replacements which are not paid as true 'first year business'
     * 
     * Notes: Commissions are NOT paid on full premium for replacements; only
     * paid on the 'new' premium or difference from old to new $.
     * 
     * 
     * 
     * 
     * 
     */
    Replacement,
    /**
     * Commission is paid on premium needed to reinstate the policy due to lapse
     * pending; premiums not being paid, etc. Not paid on full FY commission -
     * no double dipping per se.
     * 
     * 
     * 
     * 
     * 
     */
    Reinstatement,
    /**
     * 
     * 
     * 
     * 
     */
    CommissionpaymentorChargebackduetoannuitization,
    /**
     * 
     * 
     * 
     * 
     */
    PaymentsNewbusiness1styearpaymentfornewbusiness,
    /**
     * 
     * 
     * 
     * 
     */
    Commissionpaymentorchargebackforconversion,
    /**
     * 
     * 
     * 
     * 
     */
    Chargebackduetolapsedagreement,
    /**
     * Express commission is recalled manually.
     * 
     * 
     * 
     * 
     * 
     */
    ManualRecallofanadvancedpayment,
    /**
     * 
     * 
     * 
     * 
     */
    PaymentsNewbusiness1035orqualifiedtransfer,
    /**
     * Asset-based (i.e., vs. premium based) commission paid to the broker -
     * paid only on certain products such as SVUL, VULII, etc. Payments usually
     * begin in year 2 of the policy and are typically paid for the lifetime of
     * the contract.
     * 
     * 
     * 
     * 
     * 
     */
    RenewalsRenewaltrailforpremium
}
