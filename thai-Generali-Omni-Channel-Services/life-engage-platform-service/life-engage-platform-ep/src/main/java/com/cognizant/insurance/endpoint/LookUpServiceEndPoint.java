/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LookUpService;

/**
 * The Class class LookUpServiceEndPoint.
 * 
 * @author 300797
 */
@Controller
@RequestMapping("/lookUpService")
public class LookUpServiceEndPoint {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LookUpServiceEndPoint.class);

    /** The look up service. */
    @Autowired
    private LookUpService lookUpService;

    /**
     * Gets the country specific details.
     * 
     * @param json
     *            the json
     * @return the country specific details
     */
    @RequestMapping(value = "/getCountrySpecificDetails", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getCountrySpecificDetails(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lookUpService.getCountrySpecificDetails(json);
            }
        }.execute(json);
    }

    /**
     * Gets the country or country sub division list.
     * 
     * @param json
     *            the json
     * @return the country or country sub division list
     */
    @RequestMapping(value = "/getCountryOrCountrySubDivisionList", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getCountryOrCountrySubDivisionList(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lookUpService.getCountryOrCountrySubDivisionList(json);
            }
        }.execute(json);
    }

    /**
     * Gets the look up service.
     * 
     * @return the lookUpService
     */
    public final LookUpService getLookUpService() {
        return lookUpService;
    }

    /**
     * Sets the look up service.
     * 
     * @param lookUpService
     *            the lookUpService to set
     */
    public final void setLookUpService(final LookUpService lookUpService) {
        this.lookUpService = lookUpService;
    }
}
