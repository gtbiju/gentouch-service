/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Mar 14, 2013
 */
package com.cognizant.insurance.domain.codes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * The Class class CodeRelation.
 */

@Entity
@Table(name = "CODE_RELATION")
public class CodeRelation {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1165006850406283857L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /** The parent. */
    /** The code type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JoinColumn(name = "PARENT_ID")
    private CodeLookUp parent;

    /** The child. */
    /** The code type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JoinColumn(name = "CHILD_ID")
    private CodeLookUp child;

    /** The type. */
    /** The code type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_ID")
    private CodeTypeLookUp type;

    /**
     * Gets the id.
     * 
     * @return Returns the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets The id.
     * 
     * @param id
     *            The id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the parent.
     * 
     * @return Returns the parent.
     */
    public CodeLookUp getParent() {
        return parent;
    }

    /**
     * Sets The parent.
     * 
     * @param parent
     *            The parent to set.
     */
    public void setParent(final CodeLookUp parent) {
        this.parent = parent;
    }

    /**
     * Gets the child.
     * 
     * @return Returns the child.
     */
    public CodeLookUp getChild() {
        return child;
    }

    /**
     * Sets The child.
     * 
     * @param child
     *            The child to set.
     */
    public void setChild(final CodeLookUp child) {
        this.child = child;
    }

    /**
     * Gets the type.
     * 
     * @return Returns the type.
     */
    public CodeTypeLookUp getType() {
        return type;
    }

    /**
     * Sets The type.
     * 
     * @param type
     *            The type to set.
     */
    public void setType(final CodeTypeLookUp type) {
        this.type = type;
    }

}
