/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 28, 2013
 */
package com.cognizant.insurance.services;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.vo.FileUploadForm;

/**
 * The Interface interface DocumentService.
 * 
 * @author 300797
 */
public interface DocumentService {

    /**
     * Save files.
     * 
     * @param uploadForm
     *            the upload form
     * @return the string
     */
    String saveFiles(FileUploadForm uploadForm);

    /**
     * Gets the file.
     * 
     * @param fileName
     *            the file name
     * @param proposalNumber
     *            the proposal number
     * @param request
     *            the request
     * @param response
     *            the response
     */
    void getFile(String fileName, String proposalNumber, HttpServletRequest request, HttpServletResponse response);

    /**
     * Delete file.
     * 
     * @param inputJsonObj
     *            the input json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String deleteFile(String inputJsonObj) throws BusinessException;

    /**
     * Generate pdf.
     * 
     * @param proposalnumber
     *            the proposal number
     * @param type
     *            the type
     * @param response
     *            the response
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException 
     */
    void generatePdfGet(String proposalnumber, String type, String productCode, HttpServletResponse response) throws BusinessException, ParseException;
    
    /**
     * Generate base 64 string pdf.
     * 
     * @param json 
     *            the json objct
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String generateBase64StringPdf (final String json)  throws BusinessException;
    
    /**
     * Upload files.
     * 
     * @param inputJsonObj
     *            the input json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String uploadFiles(final String inputJsonObj) throws BusinessException;

    /**
     * Upload document.
     * 
     * @param inputJsonObj
     *            the input json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String uploadDocument(final String inputJsonObj) throws BusinessException;

    /**
     * Gets the document.
     * 
     * @param inputJsonObj
     *            the input json obj
     * @return the document
     * @throws BusinessException
     *             the business exception
     */
    String getDocument(final String inputJsonObj) throws BusinessException;

	
}
