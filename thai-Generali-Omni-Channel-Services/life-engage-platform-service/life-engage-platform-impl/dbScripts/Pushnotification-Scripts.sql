USE [generaliServiceWeb]
GO
/****** Object:  Table [dbo].[AWS_SNS_APPLICATION_DETAILS]    Script Date: 3/30/2016 4:04:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AWS_SNS_APPLICATION_DETAILS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[APP_CERTIFICATE_KEY] [varchar](max) NULL,
	[APP_PRIVATE_KEY] [varchar](max) NULL,
	[APPLICATION_NAME] [varchar](max) NULL,
	[PLATFORM_APPLICATION_ARN] [varchar](max) NULL,
	[PLATFORM_TYPE] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AWS_SNS_DEVICE_REGISTER]    Script Date: 3/30/2016 4:04:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AWS_SNS_DEVICE_REGISTER](
	[SNS_MOBILEAPP_BUNDLE_ID] [varchar](max) NOT NULL,
	[SNS_PUSH_MSG_REGISTERED_ID] [varchar](max) NOT NULL,
	[AGENT_ID] [varchar](max) NULL DEFAULT (NULL),
	[SNS_DEVICE_APP_NAME] [varchar](max) NULL DEFAULT (NULL),
	[SNS_DEVICE_ID] [varchar](max) NULL DEFAULT (NULL),
	[SNS_DEVICE_PLATFORM] [varchar](max) NULL DEFAULT (NULL),
	[SNS_REQUESTED_TIME] [datetime] NULL DEFAULT (NULL),
	[SNS_PLATFORM_ENDPOINT_ARN] [varchar](max) NULL DEFAULT (NULL),
	[SNS_DEVICE_ID_STATUS] [varchar](max) NULL DEFAULT (NULL),
	[SNS_USER_GROUP] [varchar](max) NULL DEFAULT (NULL)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AWS_SNS_SERVICES]    Script Date: 3/30/2016 4:04:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AWS_SNS_SERVICES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[APPLICATION_NAME] [varchar](max) NULL,
	[DEVICE_FILTER] [varchar](max) NULL,
	[SNS_DEVICE_ID] [varchar](max) NULL,
	[MESSAGE] [varchar](max) NULL,
	[NOTIFIER_GROUP_ID] [varchar](max) NULL,
	[NOTIFIER_ID] [varchar](max) NULL,
	[PLATFORM_FILTER] [varchar](max) NULL,
	[REQUEST_JSON] [varchar](max) NULL,
	[SNS_RECEIVED_TIME] [datetime] NULL,
	[SNS_REQUEST_TIME] [datetime] NULL,
	[SNS_STATUS_MESSAGE] [varchar](max) NULL,
	[SOURCE_APP_ID] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AWS_SNS_APPLICATION_DETAILS] ADD  DEFAULT (NULL) FOR [APPLICATION_NAME]
GO
ALTER TABLE [dbo].[AWS_SNS_APPLICATION_DETAILS] ADD  DEFAULT (NULL) FOR [PLATFORM_APPLICATION_ARN]
GO
ALTER TABLE [dbo].[AWS_SNS_APPLICATION_DETAILS] ADD  DEFAULT (NULL) FOR [PLATFORM_TYPE]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [APPLICATION_NAME]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [DEVICE_FILTER]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [SNS_DEVICE_ID]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [MESSAGE]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [NOTIFIER_GROUP_ID]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [NOTIFIER_ID]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [PLATFORM_FILTER]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [REQUEST_JSON]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [SNS_RECEIVED_TIME]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [SNS_REQUEST_TIME]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [SNS_STATUS_MESSAGE]
GO
ALTER TABLE [dbo].[AWS_SNS_SERVICES] ADD  DEFAULT (NULL) FOR [SOURCE_APP_ID]
GO
