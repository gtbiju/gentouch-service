/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Feb 4, 2014
 */
package com.cognizant.insurance.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

/**
 * The Class class Base64Utils.
 * 
 * @author 300797
 */
public final class Base64Utils {
	
	private static final  char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    /**
     * Instantiates a new base64 utils.
     */
    private Base64Utils() {
        // not called
    }

    /**
     * Encode file to base64 binary.
     * 
     * @param fileName
     *            the file name
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String encodeFileToBase64Binary(final String fileName) throws IOException {

        File file = new File(fileName);
        byte[] bytes = FileUtils.loadFile(file);
        byte[] encoded = Base64.encodeBase64(bytes);       
        return new String(encoded);
    }
    
    public static String encode(byte[] buf){
    	  int size = buf.length;
    	  char[] ar = new char[((size + 2) / 3) * 4];
    	  int a = 0;
    	  int i=0;
    	       while(i < size){
    	           byte b0 = buf[i++];
    	           byte b1 = (i < size) ? buf[i++] : 0;
    	           byte b2 = (i < size) ? buf[i++] : 0;
    	 
    	           int mask = 0x3F;
    	           ar[a++] = ALPHABET[(b0 >> 2) & mask];
    	           ar[a++] = ALPHABET[((b0 << 4) | ((b1 & 0xFF) >> 4)) & mask];
    	           ar[a++] = ALPHABET[((b1 << 2) | ((b2 & 0xFF) >> 6)) & mask];
    	           ar[a++] = ALPHABET[b2 & mask];
    	       }//while
    	 
    	       switch(size % 3){
    	          case 1: ar[--a]  = '=';
    	          case 2: ar[--a]  = '=';
    	       }//switch
    	    return new String(ar);
    	 }


   
}
