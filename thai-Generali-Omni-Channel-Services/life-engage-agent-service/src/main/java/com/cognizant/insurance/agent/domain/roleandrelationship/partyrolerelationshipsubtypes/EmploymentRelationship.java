/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.partyrolerelationshipsubtypes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.EmploymentTypeCodeList;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.agent.domain.roleandrelationship.PartyRoleRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.JobTitleCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.ProficiencyLevelCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.SalaryGradeCodeList;

/**
 * This concept defines the business relationship between an employee and
 * his/her employer.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class EmploymentRelationship extends PartyRoleRelationship {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -413598195669949779L;

    /**
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private EmploymentTypeCodeList occupationClassCode;

    /**
     * Job description of the employee (claims handler, sales, underwriter,
     * etc.).
     * 
     * 
     * 
     */
    private String jobDescription;

    /**
     * Main location of the employee (headquarter, agency, etc.).
     * 
     * 
     * 
     */
    private String location;

    /**
     * Business Unit of the employee.
     * 
     * 
     * 
     */
    private String businessUnit;

    /**
     * An indicator this is the current employment. 
     * 
     * 
     * 
     */
    private Boolean currentIndicator;

    /**
     * The job title of the position held by the employee.
     * 
     * e.g: Chief Executive Officer
     * 
     * e.g: Clerk
     * 
     * e.g: Managing Director
     * 
     * e.g: Marketing Manager
     * 
     * 
     * 
     */
    @Enumerated
    private JobTitleCodeList jobTitleCode;

    /**
     * The total taxable monetary amount that the employee receives annually in
     * the context of his employment agreement.
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount annualTaxableBenefitAmount;

    /**
     * The fixed salary an employee receives in the context of his employment
     * agreement.
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount baseSalaryAmount;

    /**
     * The bonus salary an employee receives over and above the base salary in
     * the context of his employment agreement. 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount bonusSalaryAmount;

    /**
     * The overall skill level of the employee.
     * 
     * e.g: Professional
     * 
     * e.g: Semi-skilled
     * 
     * e.g: Skilled
     * 
     * e.g: Unskilled
     * 
     * 
     * 
     */
    @Enumerated
    private ProficiencyLevelCodeList skillLevelCode;

    /**
     * Indicates whether the employee has a job that gives him rights to a
     * pension.
     * 
     * 
     * 
     */
    private Boolean pensionableIndicator;

    /**
     * Indicates the salary or wage level of an employee.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private SalaryGradeCodeList salaryGradeCode;

    /**
     * Indicates whether the employee is exempt or non-exempt.
     * 
     * In the USA, this is associated with being paid on a basis that is not
     * subject to the 40 hour work week, minimum wage, and overtime rules. Such
     * jobs are therefore considered "exempt". Conversely, jobs that are
     * non-exempt pay salaries subject to concepts such as time worked, overtime
     * rules, etc.
     * 
     * SOURCES:
     * http://blogs.payscale.com/ask_dr_salary/2007/01/hourly_wage_vs_.html
     * 
     * http://en.wikipedia.org/wiki/Salary
     * 
     * 
     * 
     * 
     * 
     * 
     */
    private Boolean exemptIndicator;

    
    /** The occupation type code. */
    private String occupationTypeCode;
    
    /**
     * Gets the annualTaxableBenefitAmount.
     * 
     * @return Returns the annualTaxableBenefitAmount.
     */
    public CurrencyAmount getAnnualTaxableBenefitAmount() {
        return annualTaxableBenefitAmount;
    }

    /**
     * Gets the baseSalaryAmount.
     * 
     * @return Returns the baseSalaryAmount.
     */
    public CurrencyAmount getBaseSalaryAmount() {
        return baseSalaryAmount;
    }

    /**
     * Gets the bonusSalaryAmount.
     * 
     * @return Returns the bonusSalaryAmount.
     */
    public CurrencyAmount getBonusSalaryAmount() {
        return bonusSalaryAmount;
    }

    /**
     * Gets the businessUnit.
     * 
     * @return Returns the businessUnit.
     */
    public String getBusinessUnit() {
        return businessUnit;
    }

    /**
     * Gets the currentIndicator.
     * 
     * @return Returns the currentIndicator.
     */
    public Boolean getCurrentIndicator() {
        return currentIndicator;
    }

    /**
     * Gets the exemptIndicator.
     * 
     * @return Returns the exemptIndicator.
     */
    public Boolean getExemptIndicator() {
        return exemptIndicator;
    }

    /**
     * Gets the jobDescription.
     * 
     * @return Returns the jobDescription.
     */
    public String getJobDescription() {
        return jobDescription;
    }

    /**
     * Gets the jobTitleCode.
     * 
     * @return Returns the jobTitleCode.
     */
    public JobTitleCodeList getJobTitleCode() {
        return jobTitleCode;
    }

    /**
     * Gets the location.
     * 
     * @return Returns the location.
     */
    public String getLocation() {
        return location;
    }

    /**
     * Gets the occupationClassCode.
     * 
     * @return Returns the occupationClassCode.
     */
    public EmploymentTypeCodeList getOccupationClassCode() {
        return occupationClassCode;
    }

    /**
     * Gets the occupationTypeCode.
     *
     * @return Returns the occupationTypeCode.
     */
    public String getOccupationTypeCode() {
        return occupationTypeCode;
    }

    /**
     * Gets the pensionableIndicator.
     * 
     * @return Returns the pensionableIndicator.
     */
    public Boolean getPensionableIndicator() {
        return pensionableIndicator;
    }

    /**
     * Gets the salaryGradeCode.
     * 
     * @return Returns the salaryGradeCode.
     */
    public SalaryGradeCodeList getSalaryGradeCode() {
        return salaryGradeCode;
    }

    /**
     * Gets the skillLevelCode.
     * 
     * @return Returns the skillLevelCode.
     */
    public ProficiencyLevelCodeList getSkillLevelCode() {
        return skillLevelCode;
    }

    /**
     * Sets The annualTaxableBenefitAmount.
     * 
     * @param annualTaxableBenefitAmount
     *            The annualTaxableBenefitAmount to set.
     */
    public void setAnnualTaxableBenefitAmount(
            final CurrencyAmount annualTaxableBenefitAmount) {
        this.annualTaxableBenefitAmount = annualTaxableBenefitAmount;
    }

    /**
     * Sets The baseSalaryAmount.
     * 
     * @param baseSalaryAmount
     *            The baseSalaryAmount to set.
     */
    public void setBaseSalaryAmount(final CurrencyAmount baseSalaryAmount) {
        this.baseSalaryAmount = baseSalaryAmount;
    }

    /**
     * Sets The bonusSalaryAmount.
     * 
     * @param bonusSalaryAmount
     *            The bonusSalaryAmount to set.
     */
    public void setBonusSalaryAmount(final CurrencyAmount bonusSalaryAmount) {
        this.bonusSalaryAmount = bonusSalaryAmount;
    }

    /**
     * Sets The businessUnit.
     * 
     * @param businessUnit
     *            The businessUnit to set.
     */
    public void setBusinessUnit(final String businessUnit) {
        this.businessUnit = businessUnit;
    }

    /**
     * Sets The currentIndicator.
     * 
     * @param currentIndicator
     *            The currentIndicator to set.
     */
    public void setCurrentIndicator(final Boolean currentIndicator) {
        this.currentIndicator = currentIndicator;
    }

    /**
     * Sets The exemptIndicator.
     * 
     * @param exemptIndicator
     *            The exemptIndicator to set.
     */
    public void setExemptIndicator(final Boolean exemptIndicator) {
        this.exemptIndicator = exemptIndicator;
    }

    /**
     * Sets The jobDescription.
     * 
     * @param jobDescription
     *            The jobDescription to set.
     */
    public void setJobDescription(final String jobDescription) {
        this.jobDescription = jobDescription;
    }

    /**
     * Sets The jobTitleCode.
     * 
     * @param jobTitleCode
     *            The jobTitleCode to set.
     */
    public void setJobTitleCode(final JobTitleCodeList jobTitleCode) {
        this.jobTitleCode = jobTitleCode;
    }

    /**
     * Sets The location.
     * 
     * @param location
     *            The location to set.
     */
    public void setLocation(final String location) {
        this.location = location;
    }

    /**
     * Sets The occupationClassCode.
     * 
     * @param occupationClassCode
     *            The occupationClassCode to set.
     */
    public void setOccupationClassCode(
            final EmploymentTypeCodeList occupationClassCode) {
        this.occupationClassCode = occupationClassCode;
    }

    /**
     * Sets The occupationTypeCode.
     *
     * @param occupationTypeCode The occupationTypeCode to set.
     */
    public void setOccupationTypeCode(final String occupationTypeCode) {
        this.occupationTypeCode = occupationTypeCode;
    }

    /**
     * Sets The pensionableIndicator.
     * 
     * @param pensionableIndicator
     *            The pensionableIndicator to set.
     */
    public void
            setPensionableIndicator(final Boolean pensionableIndicator) {
        this.pensionableIndicator = pensionableIndicator;
    }

    /**
     * Sets The salaryGradeCode.
     * 
     * @param salaryGradeCode
     *            The salaryGradeCode to set.
     */
    public void setSalaryGradeCode(
            final SalaryGradeCodeList salaryGradeCode) {
        this.salaryGradeCode = salaryGradeCode;
    }

    /**
     * Sets The skillLevelCode.
     * 
     * @param skillLevelCode
     *            The skillLevelCode to set.
     */
    public void setSkillLevelCode(
            final ProficiencyLevelCodeList skillLevelCode) {
        this.skillLevelCode = skillLevelCode;
    }

}
