/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304000
 * @version       : 0.1, Feb 12, 2014
 */
package com.cognizant.insurance.life.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import java.text.ParseException;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LifeEngageSyncService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@ContextConfiguration("/service-context-test.xml")
public class TestLifeEngageObservationService extends AbstractJUnit4SpringContextTests {
    
    /** The life engage sync service. */
    @Autowired
    LifeEngageSyncService lifeEngageSyncService;
    
    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";
    
    /** The Constant STATUS_DATA. */
    private static final String STATUS_DATA = "StatusData";

    /** The Constant STATUS_CODE. */
    private static final String STATUS_CODE = "StatusCode";

    /** The Constant STATUS. */
    private static final String STATUS = "Status";

    /** The Constant STATUS_MESSAGE. */
    private static final String STATUS_MESSAGE = "StatusMessage";
    
    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";
    
    public static final Logger LOGGER = LoggerFactory.getLogger(TestLifeEngageObservationService.class);
    
    /**
     * This Method Tests the Observation Save, retrieve, retreiveAll(basic details and full details) methods.
     * 
     * @throws BusinessException
     *             the business exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public void saveToSyncObservation() throws BusinessException, IOException, ParseException {

        SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar();
        String currentDate = newDt.format(calendar.getTime());
        String agentId = "Agent1";
        String statusCode100 = "100";
        String statusSuccess = "SUCCESS";
        String statusCode400 = "400";
        String statusFailure = "FAILURE";
        String statusMessage = "Observation Saved";

        /** creating the first fna save request **/
        String saveRequest = getRequest("observationRequest/observationSyncRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate);
       
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Save Observation : saveRequest >>"+ saveRequest);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Saving first fna Request with Initial Status(==> Work in Progress) **/
        String responseJson = lifeEngageSyncService.save(saveRequest);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Save Observation : responseJson 1 >>"+ responseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Verify responsejson **/
        assertNotNull(responseJson);
        
        /** Verify TransactionArray length **/
        JSONObject jsonObject = new JSONObject(responseJson);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        
        /** Verify Status Data >> Success status expected**/
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        JSONObject statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));

        /** Saving the same Observation Request **/
        responseJson = lifeEngageSyncService.save(saveRequest);
        
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        LOGGER.info("$$$$ Save Observation : responseJson 2 >>"+ responseJson);
        LOGGER.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        
        /** Verify responsejson **/
        assertNotNull(responseJson);
        
        /** Verify TransactionArray length **/
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        
        /** Verify Status Data >> Failure Status expected**/
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode400, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusFailure, statusDataObj.getString(STATUS));
        assertTrue(statusDataObj.getString(STATUS_MESSAGE).contains("This Trasaction Data is already processed"));

    }
    
    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(new String(line.getBytes(), "UTF-8"));
        }

        return sb.toString();
    }
    
}
