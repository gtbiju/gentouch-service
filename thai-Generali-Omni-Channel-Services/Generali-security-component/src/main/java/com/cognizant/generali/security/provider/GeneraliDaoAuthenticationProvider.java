package com.cognizant.generali.security.provider;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import com.cognizant.generali.security.component.GeneraliLoginComponent;
import com.cognizant.generali.security.constants.GeneraliExceptionCodes;
import com.cognizant.generali.security.exception.UserLicenseException;
import com.cognizant.generali.security.exception.UserTerminatedException;
import com.cognizant.generali.security.repository.GeneraliLoginRepository;
import com.cognizant.le.security.constants.ExceptionCodes;
import com.cognizant.le.security.constants.ExceptionMessages;
import com.cognizant.le.security.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.le.security.exception.PasswordEmptyException;
import com.cognizant.le.security.exception.PasswordIncorrectException;
import com.cognizant.le.security.request.Request;
import com.cognizant.le.security.request.impl.JPARequestImpl;

public class GeneraliDaoAuthenticationProvider extends DaoAuthenticationProvider {
	
	/*User login related error messages*/
    public static final String INVALID_USER_ERROR_CODE = "401"; 
    public static final String INVALID_AGENTCODE_PWD_ERROR_MSG = "Username/Password entered is invalid. Please re-enter the correct one.";
    public static final String LICENSE_EXPIRED_ERROR_MSG = "Your license has expired. Kindly contact your office for details";
    public static final String AGENT_STATUS_EXPIRED_ERROR_MSG = "You are not authorize to access. Kindly contact your office for details";
	
	@Autowired
    private GeneraliLoginComponent generaliLoginComponent;	
	
	@Autowired
	private GeneraliLoginRepository generaliLoginRepository;
	
	
	@PersistenceContext(unitName="LE_Platform")
	private EntityManager entityManager;
  
    /**
     * Instantiates a new life engage dao authentication provider.
     */
    public GeneraliDaoAuthenticationProvider() {
        super();
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.security.authentication.dao.DaoAuthenticationProvider#additionalAuthenticationChecks(org.
     * springframework.security.core.userdetails.UserDetails,
     * org.springframework.security.authentication.UsernamePasswordAuthenticationToken)
     */
	@SuppressWarnings("deprecation")
    protected void additionalAuthenticationChecks(UserDetails userDetails,
            UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        Object salt = null;   
        
      if(userDetails.getUsername() != null && userDetails.getPassword() != null) {
        	
        final Request<String> request =  new JPARequestImpl<String>(entityManager,ContextTypeCodeList.LOGIN,null);
        
           request.setType(userDetails.getUsername());

        if (getSaltSource() != null) {
            salt = getSaltSource().getSalt(userDetails);
        }

        if (authentication.getCredentials() == null || "".equals(authentication.getCredentials())) {
        	
            logger.debug("Authentication failed: no credentials provided. UserName : " + userDetails.getUsername());

            throw new PasswordEmptyException(ExceptionCodes.ERROR_473, ExceptionMessages.EMPTY_PASSWORD);
        }

        String presentedPassword = authentication.getCredentials().toString();

        if (!getPasswordEncoder().isPasswordValid(userDetails.getPassword(), presentedPassword, salt)) {
        	
            logger.debug("Authentication failed: password does not match stored value. UserName : "
                    + userDetails.getUsername());

            generaliLoginRepository.updateFailedAttempt(request);
            
            throw new PasswordIncorrectException(INVALID_USER_ERROR_CODE, INVALID_AGENTCODE_PWD_ERROR_MSG);
        }
        
        if(generaliLoginComponent.validateAgentLicenseExpiry(userDetails.getUsername())) {
        	
        	logger.debug("Authentication failed: Agent license has expired. UserName : "
                    + userDetails.getUsername());
        	
        	throw new UserLicenseException(GeneraliExceptionCodes.ERROR_481, LICENSE_EXPIRED_ERROR_MSG);        	
        }
        
        if(generaliLoginComponent.validateAgentStatus(userDetails.getUsername())) {
        	
        	logger.debug("Authentication failed: Agent status is Terminated or Suspended. UserName : "
                    + userDetails.getUsername());
        	
        	throw new UserTerminatedException(GeneraliExceptionCodes.ERROR_480, AGENT_STATUS_EXPIRED_ERROR_MSG);        	
        }
        
        generaliLoginRepository.resetAttempts(request);
      }
    }
}
