/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304000
 * @version       : 0.1, Feb 21, 2014
 */
package com.cognizant.insurance.goalandneed.component.impl;

import java.util.List;

import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.goalandneed.component.GoalAndNeedComponent;
import com.cognizant.insurance.goalandneed.dao.GoalAndNeedDao;
import com.cognizant.insurance.goalandneed.dao.impl.GoalAndNeedDaoImpl;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

/**
 * The Class GoalAndNeedComponentImpl.
 */
public class GoalAndNeedComponentImpl implements GoalAndNeedComponent {
    /**
     * Save goal and need.
     * 
     * @param goalRequest
     *            the goal request
     */
    @Override
    public final void saveGoalAndNeed(final Request<GoalAndNeed> goalRequest) {
        final GoalAndNeedDao goalAndNeedDao = new GoalAndNeedDaoImpl();
        goalAndNeedDao.save(goalRequest);
    }

    /**
     * Retrieve all.
     * 
     * @param request
     *            the request
     * @param limitRequest
     *            the limit request
     * @return the response
     */
    public final Response<List<GoalAndNeed>> retrieveAll(final Request<GoalAndNeed> request,
            Request<Integer> limitRequest) {
        final GoalAndNeedDao goalAndNeedDao = new GoalAndNeedDaoImpl();
        return goalAndNeedDao.retrieveAll(request, limitRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.goalandneed.component.GoalAndNeedComponent
     * #getGoalAndNeedByIdentifier(com.cognizant.insurance .request.Request)
     */
    @Override
    public final Response<List<GoalAndNeed>> getGoalAndNeedByIdentifier(final Request<GoalAndNeed> goalRequest) {
        final GoalAndNeedDao goalAndNeedDao = new GoalAndNeedDaoImpl();
        return goalAndNeedDao.getGoalAndNeedByIdentifier(goalRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.goalandneed.component.GoalAndNeedComponent#getGoalAndNeedCount(com.cognizant.insurance
     * .request.Request)
     */
    @Override
    public final Response<SearchCountResult> getGoalAndNeedCount(final Request<SearchCriteria> searchCriteriatRequest, List<String> fnaSearchParamsList) {
        final GoalAndNeedDao goalAndNeedDao = new GoalAndNeedDaoImpl();
        final Response<SearchCountResult> searchCountResponse =
                goalAndNeedDao.getGoalAndNeedCount(searchCriteriatRequest,fnaSearchParamsList);
        return searchCountResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.goalandneed.component.GoalAndNeedComponent#getFNAByFilter(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public final Response<List<GoalAndNeed>> getFNAByFilter(final Request<SearchCriteria> searchCriteriatRequest) {
        final GoalAndNeedDao goalAndNeedDao = new GoalAndNeedDaoImpl();
        return goalAndNeedDao.getFNAByFilter(searchCriteriatRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.goalandneed.component.GoalAndNeedComponent#updateGoalAndNeedStatusForLead(com.cognizant
     * .insurance.request.Request)
     */
    public void updateGoalAndNeedStatusForLead(final Request<GoalAndNeed> goalAndNeedRequest) {
        final GoalAndNeedDao goalAndNeedDao = new GoalAndNeedDaoImpl();
        goalAndNeedDao.updateGoalAndNeedStatusForLead(goalAndNeedRequest);
    }
}
