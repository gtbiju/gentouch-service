package com.cognizant.insurance.omnichannel.utils;

/**
 * @author 390229
 * 
 *         The Class HRRBenfitIllustrationTableData.
 * 
 *         This class is a POJO for Benefit illustration for HRR rider table in
 *         illustration PDF.
 */

public class HRRBenfitIllustrationTableData {
	
	/** BenefitDescription. */
	private String benefitDescription;
	
	/** BenefitValue. */
	private String benefitValue;
	
	/** Type to identify header style**/
	private String typeStyle;
	
	/** Flag to decide border style**/
	private Boolean isColumn1BorderNeeded;
	
	/** Flag to decide border style**/
	private Boolean isColumn2BorderNeeded;

	/**
	 * @return the benefitDescription
	 */
	public String getBenefitDescription() {
		return benefitDescription;
	}

	/**
	 * @param benefitDescription the benefitDescription to set
	 */
	public void setBenefitDescription(String benefitDescription) {
		this.benefitDescription = benefitDescription;
	}

	/**
	 * @return the benefitValue
	 */
	public String getBenefitValue() {
		return benefitValue;
	}

	/**
	 * @param benefitValue the benefitValue to set
	 */
	public void setBenefitValue(String benefitValue) {
		this.benefitValue = benefitValue;
	}

	/**
	 * @return the typeStyle
	 */
	public String getTypeStyle() {
		return typeStyle;
	}

	/**
	 * @param typeStyle the typeStyle to set
	 */
	public void setTypeStyle(String typeStyle) {
		this.typeStyle = typeStyle;
	}

	/**
	 * @param isBorderNeeded the isBorderNeeded to set
	 */
	public void setIsColumn1BorderNeeded(Boolean isColumn1BorderNeeded) {
		this.isColumn1BorderNeeded = isColumn1BorderNeeded;
	}

	/**
	 * @return the isBorderNeeded
	 */
	public Boolean getIsColumn1BorderNeeded() {
		return isColumn1BorderNeeded;
	}

	/**
	 * @param isColumn2BorderNeeded the isColumn2BorderNeeded to set
	 */
	public void setIsColumn2BorderNeeded(Boolean isColumn2BorderNeeded) {
		this.isColumn2BorderNeeded = isColumn2BorderNeeded;
	}

	/**
	 * @return the isColumn2BorderNeeded
	 */
	public Boolean getIsColumn2BorderNeeded() {
		return isColumn2BorderNeeded;
	}
	
	

}
