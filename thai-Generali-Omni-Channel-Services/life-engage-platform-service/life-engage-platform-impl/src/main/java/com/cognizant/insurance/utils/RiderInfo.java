package com.cognizant.insurance.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class RiderInfo.
 */
public class RiderInfo {

	/** The max age. */
	private String maxAge;
	
	/** The name. */
	private String name;
	
	/** The sum assured. */
	private String sumAssured;
	
	/** The insurance costs. */
	private String insuranceCosts;
	
	/** The total premium. */
	private String totalPremium;

	/**
	 * Gets the max age.
	 *
	 * @return the max age
	 */
	public String getMaxAge() {
		return maxAge;
	}

	/**
	 * Sets the max age.
	 *
	 * @param maxAge the new max age
	 */
	public void setMaxAge(String maxAge) {
		this.maxAge = maxAge;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the sum assured.
	 *
	 * @return the sum assured
	 */
	public String getSumAssured() {
		return sumAssured;
	}

	/**
	 * Sets the sum assured.
	 *
	 * @param sumAssured the new sum assured
	 */
	public void setSumAssured(String sumAssured) {
		this.sumAssured = sumAssured;
	}

	/**
	 * Gets the insurance costs.
	 *
	 * @return the insurance costs
	 */
	public String getInsuranceCosts() {
		return insuranceCosts;
	}

	/**
	 * Sets the insurance costs.
	 *
	 * @param insuranceCosts the new insurance costs
	 */
	public void setInsuranceCosts(String insuranceCosts) {
		this.insuranceCosts = insuranceCosts;
	}

	/**
	 * Gets the total premium.
	 *
	 * @return the total premium
	 */
	public String getTotalPremium() {
		return totalPremium;
	}

	/**
	 * Sets the total premium.
	 *
	 * @param totalPremium the new total premium
	 */
	public void setTotalPremium(String totalPremium) {
		this.totalPremium = totalPremium;
	}
}
