/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 29, 2013
 */
package com.cognizant.insurance.agent.component.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.agent.audit.LifeEngageAudit;
import com.cognizant.insurance.agent.component.LifeEngageAuditComponent;
import com.cognizant.insurance.agent.repository.AuditRepository;
import com.cognizant.insurance.agent.request.vo.RequestInfo;
import com.cognizant.insurance.agent.request.vo.Transactions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import com.cognizant.insurance.agent.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.constants.ErrorConstants;
import com.cognizant.insurance.agent.core.smooks.LifeEngageSmooksHolder;

/**
 * The Class class LifeEngageAuditComponentImpl.
 *
 * @author 300797
 */
@Component
public class LifeEngageAuditComponentImpl implements LifeEngageAuditComponent {

    /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;
    
    
    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageAuditComponentImpl.class);

    /** The save eApp holder. */
    @Autowired
    @Qualifier("dataWipeAuditMapping")
    private LifeEngageSmooksHolder dataWipeAuditHolder;


    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageAuditComponent#savelifeEngageAudit(com.cognizant.insurance.request
     * .vo.RequestInfo, java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public final LifeEngageAudit savelifeEngageAudit(final RequestInfo requestInfo, final String json) {
        return auditRepository.savelifeEngageAudit(requestInfo, json);
    }
    
    /* Added for LE_datawipe functionality */
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageAuditComponent#saveLifeEngageDataWipeAudit(java.lang.String,
     * com.cognizant.insurance.request.vo.RequestInfo, com.cognizant.insurance.audit.LifeEngageAudit)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String saveLifeEngageDataWipeAudit(String json, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit) {
        Transactions transactions = new Transactions();
        String dataWipeAuditResponse;
        try {
            LifeEngageComponentHelper.parseTrasactionDetails(json, transactions, false);
            transactions = (Transactions) dataWipeAuditHolder.parseJson(json);
            auditRepository.saveDataWipeAudit(transactions);
            auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.SUCCESS, Constants.SUCCESS, null);
            dataWipeAuditResponse = dataWipeAuditHolder.parseBO(transactions);
        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_100);
            dataWipeAuditResponse = dataWipeAuditHolder.parseBO(transactions);
        }

        return dataWipeAuditResponse;

    }
    /* Added for LE_datawipe functionality */
}
