/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists;

/**
 * Identifies a classification of Communications according to their purpose.
 * 
 * @author 300797
 * 
 * 
 */
public enum CommunicationInteractionTypeCodeList {

    /** Face to Face. */
    F2F,

    /** PhoneCall. */
    PhoneCall,
    
    Email,
    
    Others,
    
    Call,
        
    Visit,
       
    Appointment,

    /** Not Mentioned. */
    NotMentioned
}
