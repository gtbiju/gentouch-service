/**
 * 
 */
package com.cognizant.insurance.domain.party.partycodelists;

/**
 * Identifies a classification of Persons according to their ethnicity.
 * 
 * @author 301350
 * 
 * 
 */
public enum EthnicityCodeList {
    /**
     * Identifies a Person with ethnicity 'Afro-American'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    AfroAmerican,
    /**
     * Identifies a Person with ethnicity 'Caucasian'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Caucasian
}
