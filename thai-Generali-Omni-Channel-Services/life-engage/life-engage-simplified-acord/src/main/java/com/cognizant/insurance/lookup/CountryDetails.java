/**
 *
 * © Copyright 2013, Cognizant 
 *
 * @author        : 301350
 * @version       : 0.1, Aug 13, 2013
 */
package com.cognizant.insurance.lookup;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class class CountryLookUpResponse.
 * 
 * @author 301350 Added as part of eApp - 13Aug2013
 */
@Entity
@Table(name = "COUNTRY_LOOKUP")
public class CountryDetails {

    /** The identifier. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDENTIFIER")
    private Integer identifier;

    /** The name. */
    @Column(name = "Name")
    private String name;

    /** The subDivisionType. */
    @Column(name = "Type")
    private String subDivisionType;

    /** The parent id. */
    @Column(name = "ParentID")
    private Integer parentID;

    /**
     * Gets the identifier.
     * 
     * @return Returns the identifier.
     */
    public final Integer getIdentifier() {
        return identifier;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the subDivisionType.
     * 
     * @return Returns the subDivisionType.
     */
    public final String getType() {
        return subDivisionType;
    }

    /**
     * Gets the parentID.
     * 
     * @return Returns the parentID.
     */
    public final Integer getParentID() {
        return parentID;
    }

    /**
     * Sets The identifier.
     * 
     * @param identifier
     *            The identifier to set.
     */
    public final void setIdentifier(Integer identifier) {
        this.identifier = identifier;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * Sets The subDivisionType.
     * 
     * @param subDivisionType
     *            The subDivisionType to set.
     */
    public final void setType(String type) {
        this.subDivisionType = type;
    }

    /**
     * Sets The parentID.
     * 
     * @param parentID
     *            The parentID to set.
     */
    public final void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

}
