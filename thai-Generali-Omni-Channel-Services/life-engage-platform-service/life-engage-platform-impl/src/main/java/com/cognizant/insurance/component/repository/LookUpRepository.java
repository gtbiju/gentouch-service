/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.component.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.generic.component.LookUpComponent;
import com.cognizant.insurance.lookup.CountryDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.vo.CountryLookUpResponse;

/**
 * @author 300797
 * 
 */
//@Repository
public class LookUpRepository {
    /** The entity manager. */
    @PersistenceContext(unitName="LE_Platform")
    private EntityManager entityManager;

    /** The party component. */
    @Autowired
    private LookUpComponent lookUpComponent;

    /**
     * Gets the country specific details.
     * 
     * @param countryId
     *            the country id
     * @param countryName
     *            the country name
     * @param requestInfo
     *            the request info
     * @return the country specific details
     */
    public final Response<CountryLookUpResponse> getCountrySpecificDetails(final Integer countryId,
            final String countryName, final RequestInfo requestInfo) {
        final Request<CountryDetails> request =
                new JPARequestImpl<CountryDetails>(entityManager, ContextTypeCodeList.LIFE_ENGAGE,
                        requestInfo.getTransactionId());

        final CountryDetails countryDetails = new CountryDetails();
        countryDetails.setIdentifier(countryId);
        countryDetails.setName(countryName);
        request.setType(countryDetails);

        return lookUpComponent.getCountrySpecificDetails(request);
    }

    /**
     * Gets the country or country sub division list.
     * 
     * @param subDivisionType
     *            the sub division type
     * @param parentId
     *            the parent id
     * @param requestInfo
     *            the request info
     * @return the country or country sub division list
     */
    public final Response<CountryLookUpResponse> getCountryOrCountrySubDivisionList(final String subDivisionType,
            final Integer parentId, final RequestInfo requestInfo) {

        final Request<CountryDetails> request =
                new JPARequestImpl<CountryDetails>(entityManager, ContextTypeCodeList.LIFE_ENGAGE,
                        requestInfo.getTransactionId());

        final CountryDetails countryDetails = new CountryDetails();
        countryDetails.setType(subDivisionType);
        countryDetails.setParentID(parentId);

        request.setType(countryDetails);

        return lookUpComponent.getCountryOrCountrySubDivisionList(request);
    }

    /**
     * @return the entityManager
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * @return the lookUpComponent
     */
    public final LookUpComponent getLookUpComponent() {
        return lookUpComponent;
    }

    /**
     * @param lookUpComponent the lookUpComponent to set
     */
    public final void setLookUpComponent(final LookUpComponent lookUpComponent) {
        this.lookUpComponent = lookUpComponent;
    }
}
