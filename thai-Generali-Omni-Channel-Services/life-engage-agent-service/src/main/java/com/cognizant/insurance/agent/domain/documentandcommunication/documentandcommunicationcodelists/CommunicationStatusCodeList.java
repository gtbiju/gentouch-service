/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists;

/**
 * Allowed life cycle states for a communication.
 * 
 * @author 300797
 * 
 * 
 */
public enum CommunicationStatusCodeList {

    /** The Abandoned. */
    Abandoned,

    /** The Cancelled. */
    Cancelled,

    /** The Declined. */
    Declined,

    /** The Draft. */
    Draft,

    /** The End incoming communication. */
    EndIncomingCommunication,

    /** The End interactive communication. */
    EndInteractiveCommunication,

    /** The End outgoing communication. */
    EndOutgoingCommunication,

    /** The Ended. */
    Ended,

    /** The Failed. */
    Failed,

    /** The Initial. */
    Initial,

    /** The Invited. */
    Invited,

    /** The Missed. */
    Missed,

    /** The Ready. */
    Ready,

    /** The Receipt confirmed. */
    ReceiptConfirmed,

    /** The Received. */
    Received,

    /** The Receiving. */
    Receiving,

    /** The Receiving failed. */
    ReceivingFailed,

    /** The Scheduled. */
    Scheduled,

    /** The Sending. */
    Sending,

    /** The Sent. */
    Sent,

    /** The Started. */
    Started,
    
    //Added for Thailand
    /** The NotInterested. */
    NotInterested,
    
    /** The Interestedbutdontwanttomakeappointment. */
    Interestedbutdontwanttomakeappointment,
    
    /** The MakeAppointment. */
    MakeAppointment,
    
    /** The Cantreach. */
    Cantreach,
    
    /** The MetwithCustomer. */
    MetwithCustomer,
    
    /** The MeetingCancelled. */
    MeetingCancelled,
    
    /** The MeetingRescheduled. */
    MeetingRescheduled,
    
    /** The Other. */
    Other
    
}
