package com.cognizant.insurance.omnichannel.utils;

import java.math.BigDecimal;

public class RiderTableForEapp {
private String lifeAssuredName;
private String riderName;
private Integer riderTerm;
private Integer policyTermTerm;
private BigDecimal riderInitialPremium;
private BigDecimal riderSumAssured;


/**
 * @return the lifeAssuredName
 */
public String getLifeAssuredName() {
	return lifeAssuredName;
}
/**
 * @param lifeAssuredName the lifeAssuredName to set
 */
public void setLifeAssuredName(String lifeAssuredName) {
	this.lifeAssuredName = lifeAssuredName;
}
/**
 * @return the riderName
 */
public String getRiderName() {
	return riderName;
}
/**
 * @param riderName the riderName to set
 */
public void setRiderName(String riderName) {
	this.riderName = riderName;
}
/**
 * @return the riderTerm
 */
public Integer getRiderTerm() {
	return riderTerm;
}
/**
 * @param riderTerm the riderTerm to set
 */
public void setRiderTerm(Integer riderTerm) {
	this.riderTerm = riderTerm;
}
/**
 * @return the policyTermTerm
 */
public Integer getPolicyTermTerm() {
	return policyTermTerm;
}
/**
 * @param policyTermTerm the policyTermTerm to set
 */
public void setPolicyTermTerm(Integer policyTermTerm) {
	this.policyTermTerm = policyTermTerm;
}
/**
 * @return the riderInitialPremium
 */
public BigDecimal getRiderInitialPremium() {
	return riderInitialPremium;
}
/**
 * @param riderInitialPremium the riderInitialPremium to set
 */
public void setRiderInitialPremium(BigDecimal riderInitialPremium) {
	this.riderInitialPremium = riderInitialPremium;
}
/**
 * @return the riderSumAssured
 */
public BigDecimal getRiderSumAssured() {
	return riderSumAssured;
}
/**
 * @param riderSumAssured the riderSumAssured to set
 */
public void setRiderSumAssured(BigDecimal riderSumAssured) {
	this.riderSumAssured = riderSumAssured;
}
}
