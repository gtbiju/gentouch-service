package com.cognizant.insurance.omnichannel.service;

import com.cognizant.insurance.core.exception.BusinessException;

public interface GeneraliSalesActivityService {
    
    String retrieveSalesActivityDetails(String json) throws BusinessException;
}
