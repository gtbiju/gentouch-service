/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 22, 2013
 */
package com.cognizant.insurance.life.service;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.EmailService;

/**
 * @author 300797
 *
 */
@ContextConfiguration("/service-context-test.xml")
public class TestEmailService extends AbstractJUnit4SpringContextTests {


  @Autowired
  EmailService emailService;

 
  @Test
  public void sendEmail() throws IOException, BusinessException {

      String responseJSON = emailService.sendEmail(getRequest("eMailRequest/emailServiceRq.json"));
      // System.out.println(responseJSON);
      assertEquals("Failure", responseJSON);
     
  }

     /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(line);
        }

        return sb.toString();
    }
   
}
