/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.agreementcodelists;

/**
 * A code list indicating the insurable interest of the insured party.
 * 
 * @author 301350
 * 
 * 
 */
public enum InsurableInterestCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Child,
    /**
     * 
     * 
     * 
     * 
     */
    Dependent,
    /**
     * 
     * 
     * 
     * 
     */
    Designated,
    /**
     * This concept gives informations on a person's profile in a key person
     * insurance context. Key person insurance is a particular type of life
     * insurance policy taken out by a company on one of their employees, in
     * which the company is the beneficiary in the case of that employee's
     * untimely demise. Key person insurance is a relatively new phenomenon, but
     * has attracted much praise and is encouraged by many strategic advisors.
     * Life insurance is basically a system by which a fixed amount of money is
     * paid to a beneficiary in the event that the person being covered dies.
     * Most life insurance companies offer a type of key person insurance, as it
     * has become more and more necessary in the modern business world.
     * 
     * There are four categories of loss for which key person insurance can
     * provide compensation: 1. Losses related to the extended period when a key
     * person is unable to work, to provide temporary personnel and, if
     * necessary to finance the recruitment and training of a replacement.
     * 
     * 2. Insurance to protect profits. For example, offsetting lost income from
     * lost sales, losses resulting from the delay or cancellation of any
     * business project that the key person was involved in, loss of opportunity
     * to expand, loss of specialized skills or knowledge.
     * 
     * 3. Insurance to protect shareholders or partnership interests. Typically
     * this is insurance to enable shareholdings or partnership interests to be
     * purchased by existing shareholders or partners.
     * 
     * 4. Insurance for anyone involved in guaranteeing business loans or
     * banking facilities. The value of insurance coverage is arranged to equal
     * the value of the guarantee. A key person can be anyone directly
     * associated with the business whose loss can cause financial strain to the
     * business. For example, the person could be a director of the company, a
     * partner, a key sales person, key project manager, or someone with
     * specific skills or knowledge which is especially valuable to the company.
     * Syn: key man insurance or keyman insurance.
     * 
     * 
     * 
     */
    KeyPerson,
    /**
     * A party who grants a lease of property.
     * 
     * SOURCE: http://dictionary.reference.com/browse/lessor
     * 
     * A person or organization who provides property or workers via a lease. In
     * worker's compensation, this is a company who provides leased employees to
     * another company.
     * 
     * 
     * 
     */
    Lessor,
    /**
     * The party to a mortgage who makes the loan. The party who holds mortgaged
     * property as security for repayment of a loan.
     * 
     * SOURCE: http://dictionary.reference.com/browse/mortgagee 
     *
     * 
     * 
     * 
     */
    Mortgagee,
    /**
     * 
     * 
     * 
     * 
     */
    Spouse,
    /**
     * A party who has a lien on particular property. The lienholder is the
     * legal owner of the property and holds the title to it. A lien is a form
     * of security instrument that secures payment of a debt.
     * 
     * e.g. A lienholder is the lending institution that financed the purchase
     * of a vehicle for a person.
     * 
     * 
     * 
     */
    Lienholder,
    /**
     * 
     * 
     * 
     * 
     */
    LessorandLossPayee,
    /**
     * 
     * 
     * 
     * 
     */
    StateorPoliticalSubdivisionsPermits,
    /**
     * 
     * 
     * 
     * 
     */
    ExtensionOf,
    /**
     * The party to whom the claim from a loss is to be paid.
     * 
     * SOURCE: http://www.investopedia.com/terms/l/loss-payee.asp
     * 
     * A loss payee clause (or loss payable clause) is a clause in a contract of
     * insurance which provides that in the event of payment being made under
     * the policy in relation to the insured risk, payment will be made to a
     * third party rather than to the insured beneficiary of the policy. Such
     * clauses are common where the insured property is subject to a mortgage or
     * other security interest and the mortgagee (ie. usually a bank) requires
     * that the property be insured and that such a clause be included.
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Loss_payee_clause 
     *
     * 
     * 
     * 
     */
    LossPayee,
    /**
     * 
     * 
     * 
     * 
     */
    LossPayeeandAdditionalInterest,
    /**
     * 
     * 
     * 
     * 
     */
    Volunteers,
    /**
     * 
     * 
     * 
     * 
     */
    ControllingInterest,
    /**
     * 
     * 
     * 
     * 
     */
    CoOwnerofInsuredPremises,
    /**
     * 
     * 
     * 
     * 
     */
    ArchitectsEngineersorSurveyors,
    /**
     * 
     * 
     * 
     * 
     */
    Executor,
    /**
     * 
     * 
     * 
     * 
     */
    OwnersManagersorOperatorsofPremises,
    /**
     * 
     * 
     * 
     * 
     */
    MortgageeAssigneeorReceiver,
    /**
     * 
     * 
     * 
     * 
     */
    OwnersorContractors,
    /**
     * 
     * 
     * 
     * 
     */
    OwnerLesseeContractorFormA,
    /**
     * 
     * 
     * 
     * 
     */
    OwnerLesseeContractorFormB,
    /**
     * 
     * 
     * 
     * 
     */
    StateorPoliticalSubdivisionPermitsRelatingtoPremises,
    /**
     * 
     * 
     * 
     * 
     */
    TownhouseAssociation,
    /**
     * 
     * 
     * 
     * 
     */
    Vendor,
    /**
     * 
     * 
     * 
     * 
     */
    GrantorofFranchise,
    /**
     * 
     * 
     * 
     * 
     */
    StateorGovernmentalAgencyorSubdivision,
    /**
     * 
     * 
     * 
     * 
     */
    Owner,
    /**
     * 
     * 
     * 
     * 
     */
    StudentLivingAwayFromTheResidencePremises,
    /**
     * 
     * 
     * 
     * 
     */
    VendorIncludesRestrictionsorAbridgements
}
