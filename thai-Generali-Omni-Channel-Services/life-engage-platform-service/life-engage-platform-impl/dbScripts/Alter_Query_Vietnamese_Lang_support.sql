ALTER TABLE [dbo].[Agreement] ALTER COLUMN [identifier] varchar(255)
-- Queries to update tables for Vietnamese language support
 
ALTER TABLE PartyName ALTER COLUMN givenName nvarchar(100)
ALTER TABLE PostalAddressContact ALTER COLUMN addressLine1 nvarchar(400)
ALTER TABLE Communication ALTER COLUMN communicationDescription nvarchar(400)
ALTER TABLE ElectronicContact ALTER COLUMN emailAddress nvarchar(120)
ALTER TABLE Person ALTER COLUMN identityProof nvarchar(80)

Alter TABLE TransactionKeys alter column key2 NVARCHAR(100)

-- Newly added in 15-09-2016

ALTER TABLE PostalAddressContact ALTER COLUMN streetName nvarchar(400)
ALTER TABLE PostalAddressContact ALTER COLUMN addressLine2 nvarchar(400) 
ALTER TABLE Place ALTER COLUMN name nvarchar(400) 
ALTER TABLE Agreement ALTER COLUMN identifier varchar(255)

--Added for Bulk upload CR R3
Alter table [CODE_LOCALE_LOOKUP] alter column VALUE nvarchar(255)
Alter table [CODE_LOOKUP] alter column DESCRIPTION nvarchar(255)
ALTER TABLE PostalAddressContact ALTER COLUMN boxNumber nvarchar(50)
ALTER TABLE PostalAddressContact ALTER COLUMN addressLine3 nvarchar(100)

--Added for BI
alter table coverage alter column marketingName nvarchar (200)
alter table transactionkeys alter column key19 nvarchar (200)
alter table EXTN_BAND_TBL alter column MAX_SA decimal(38,0)
alter table EXTN_BAND_TBL alter column MIN_SA decimal(38,0)
alter table CurrencyAmount alter column amount numeric(38,2)

--eapp

alter table [AgreementExtension] alter column prevPolicyInsuredName nvarchar (510)
alter table [AgreementExtension] alter column spajSignPlace nvarchar (510)

alter table [AgreementExtension] alter column remark nvarchar (250)
alter table [UserSelection] alter column detailedInfo nvarchar (510)

alter table [InsuranceAgreement] alter column insurerName nvarchar (250)

ALTER TABLE PersonDetail ALTER COLUMN nameofInstitution nvarchar(255)
ALTER TABLE PersonDetail ALTER COLUMN natureofWork nvarchar(255)


alter table CheckinData alter column agentName nvarchar(255)
alter table CheckinData alter column branchName nvarchar(255)
alter table AWS_SNS_SERVICES alter column MESSAGE nvarchar(300)

ALTER TABLE Account ALTER COLUMN name nvarchar(510)