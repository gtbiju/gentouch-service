/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists;

/**
 * Allowed life cycle states for a customer.
 * 
 * @author 300797
 * 
 * 
 */
public enum CustomerSourceCodeList {

    /** Reference. */
    Reference,

    /** Activities. */
    Activities,

    /** Branch. */
    Branch,

    /** HO. */
    HO,

    /** Others. */
    Others,

    /** NaturalMarket. */
    NaturalMarket,
    
    /** Source1. */
    Source1,
    
    /** Source2. */
    Source2,
    
    /** Source3. */
    Source3,
    
    /** Source4. */
    Source4,
    
    /** Source5. */
    Source5,
    
    /** Subsource1. */
    Subsource1,
    
    /** Subsource2. */
    Subsource2,
    
    /** Subsource3. */
    Subsource3,
    
    /** Subsource4. */
    Subsource4,
    
    /** Subsource5. */
    Subsource5,

    /** Subsource6. */
    Subsource6,

    /** Subsource7. */
    Subsource7,

    /** RenewalConnection. */
    RenewalConnection,

    /** Servicing. */
    Servicing,

    /** Open. */
    Open,

    /** Closed. */
    Closed,

    /** Dropped. */
    Dropped,

    /** Xsell. */
    Xsell,

    /** CPCLeads. */
    CPCLeads,

    /** Nominee. */
    Nominee,

    /** Sampark. */
    Sampark,

    /** NotMentioned. */
    NotMentioned,
    ColdCallOrCanvasing,
    CentreofInfluence,
    Networking,
    PersonalObservation,
    Referral,
    
    /** added for vietnam. */
    Walkin,
    EventOrGP,
    MarketingCampaign,
    FriendOrFamily,
    ReferralFromBranch,
    
    /** Added for Thailand    */
    FriendsAndFamily,
    Events,
    Booths
}
