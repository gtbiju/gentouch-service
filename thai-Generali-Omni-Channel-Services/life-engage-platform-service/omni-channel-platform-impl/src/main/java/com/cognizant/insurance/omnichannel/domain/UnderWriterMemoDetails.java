package com.cognizant.insurance.omnichannel.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "UW_MEMO_DETAILS")
public class UnderWriterMemoDetails implements Serializable{
	
	/**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "DESCITEM",nullable = false, unique = true)
    @NaturalId(mutable = true)
    private String memoCode;
    
    @Column(name = "LONGDESC",columnDefinition="nvarchar(MAX)")
    private String memoDescription;
    
    @Column(name = "QUESTIONNAIRE_CODE", columnDefinition="nvarchar(MAX)")
    private String pdfName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMemoCode() {
		return memoCode;
	}

	public void setMemoCode(String memoCode) {
		this.memoCode = memoCode;
	}

	public String getMemoDescription() {
		return memoDescription;
	}

	public void setMemoDescription(String memoDescription) {
		this.memoDescription = memoDescription;
	}

	public String getPdfName() {
		return pdfName;
	}

	public void setPdfName(String pdfName) {
		this.pdfName = pdfName;
	}

}
