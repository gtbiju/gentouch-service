package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;

public class AgentReportVo implements Serializable {

    private String agentCode;

    private String agentName;

    private String position;

    private String smName;
    
    private String gmName;

    private String teamName;

    private String leadCount;

    private String callCount;

    private String appointmentcount;

    private String visitCount;

    private String illustrationCount;

    private String eAppCount;

    public String getGmName() {
        return gmName;
    }

    public void setGmName(String gmName) {
        this.gmName = gmName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSmName() {
        return smName;
    }

    public void setSmName(String smName) {
        this.smName = smName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getLeadCount() {
        return leadCount;
    }

    public void setLeadCount(String leadCount) {
        this.leadCount = leadCount;
    }

    public String getCallCount() {
        return callCount;
    }

    public void setCallCount(String callCount) {
        this.callCount = callCount;
    }

    public String getAppointmentcount() {
        return appointmentcount;
    }

    public void setAppointmentcount(String appointmentcount) {
        this.appointmentcount = appointmentcount;
    }

    public String getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(String visitCount) {
        this.visitCount = visitCount;
    }

    public String getIllustrationCount() {
        return illustrationCount;
    }

    public void setIllustrationCount(String illustrationCount) {
        this.illustrationCount = illustrationCount;
    }

    public String geteAppCount() {
        return eAppCount;
    }

    public void seteAppCount(String eAppCount) {
        this.eAppCount = eAppCount;
    }

}
