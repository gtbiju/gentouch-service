package com.cognizant.insurance.generic.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.domain.AutoEtrGenerateAudit;
import com.cognizant.insurance.generic.dao.AutoEtrGenerateAuditDao;
import com.cognizant.insurance.pushnotification.CompositeKey;
import com.cognizant.insurance.pushnotification.DeviceRegister;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;

public class AutoEtrGenerateAuditDaoImpl extends DaoImpl implements AutoEtrGenerateAuditDao {
	
	@Override
	public List<AutoEtrGenerateAudit> retrieveAutoEtrGenerateAudit(String eappNo,EntityManager entityManager) {

    	String query;
    	final Request<AutoEtrGenerateAudit> autoEtrGenerateAuditRequest =
                new JPARequestImpl<AutoEtrGenerateAudit>(entityManager, null, "");
    	/*query = "select id, application_no, paymentDate, product_id, premium, payor_role, application_date, cash_payment, source_of_data, payor_email,  policy_holder,  payor_name, pay_for, sum_assured ,agent_code ,mobile ,etrDate ,dob_policy_holder ,etrNumber ,bankName ,bankBranchName ,bankAccountNumber ,branchCode ,bankBranchCode ,splitPayment from AutoEtrGenerateAudit where application_no = ?1";
       List<AutoEtrGenerateAudit> autoEtrGenerateAuditObjectList = findByQuery(autoEtrGenerateAuditRequest, query, eappNo);*/
    	query = "select id, application_no, paymentDate, product_id, premium,payor_role, application_date, cash_payment, source_of_data, payor_email, policy_holder, payor_name, pay_for, sum_assured, agent_code, mobile, etrDate, dob_policy_holder, etrNumber, bankName, bankBranchName, bankAccountNumber, branchCode, bankBranchCode, splitPayment, ispaymentDone from AutoEtrGenerateAudit where application_no = ?1";
        List<AutoEtrGenerateAudit> autoEtrGenerateAuditObjectList = (List<AutoEtrGenerateAudit>) findByQuery(autoEtrGenerateAuditRequest, query, eappNo);
         
    	return autoEtrGenerateAuditObjectList;
    
	}
	
	@Override
    public void updateAutoETROnSave(Request<AutoEtrGenerateAudit> autoEtrGenerateAuditReq) {

        String query =
                "update AutoEtrGenerateAudit autoEtrGenerateAudit set autoEtrGenerateAudit.ispaymentDone = ?1, autoEtrGenerateAudit.splitNumber = ?4 where autoEtrGenerateAudit.etrNumber = ?2 and autoEtrGenerateAudit.application_no = ?3";
        final AutoEtrGenerateAudit autoEtrGenerateAudit = (AutoEtrGenerateAudit) autoEtrGenerateAuditReq.getType();

        int result =
                executeUpdate(autoEtrGenerateAuditReq, query, Boolean.TRUE, autoEtrGenerateAudit.getEtrNumber(), autoEtrGenerateAudit.getApplication_no(), autoEtrGenerateAudit.getSplitNumber());
    }

	
}
