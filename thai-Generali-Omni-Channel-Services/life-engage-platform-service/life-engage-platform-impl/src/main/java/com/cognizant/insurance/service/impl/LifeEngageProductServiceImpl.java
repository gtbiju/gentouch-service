/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Dec 9, 2013
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.LifeEngageAuditComponent;
import com.cognizant.insurance.component.LifeEngageProductComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.ProductRepository;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.vo.ResponseBo;
import com.cognizant.insurance.response.vo.ResponseInfo;
import com.cognizant.insurance.response.vo.ResponsePayload;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.services.LifeEngageProductService;

import com.cognizant.insurance.audit.LifeEngageAudit;

/**
 * The Class class LifeEngageProductServiceImpl.
 */
@Service("leProductService")
public class LifeEngageProductServiceImpl implements LifeEngageProductService {

    /** The e app repository. */
    @Autowired
    private ProductRepository productRepository;

    /** The get products holder. */
    @Autowired
    @Qualifier("getProductsServiceMapping")
    private LifeEngageSmooksHolder getProductsHolder;

    /** The get products holder. */
    @Autowired
    @Qualifier("getProductServiceMapping")
    private LifeEngageSmooksHolder getProductHolder;
    
    /** The get products holder. */
    @Autowired
    @Qualifier("getProductsByFilterMapping")
    private LifeEngageSmooksHolder getProductsByFilterHolder;

    /** The get products holder. */
    @Autowired
    @Qualifier("getProductsDetailsByFilterMapping")
    private LifeEngageSmooksHolder getProductsDetailsByFilterHolder;

    /** The getproduct templates by filter holder. */
    @Autowired
    @Qualifier("getproductTemplatesByFilterMapping")
    private LifeEngageSmooksHolder getproductTemplatesByFilterHolder;
    /**
     * Is Product Active Holder
     */
    @Autowired
    @Qualifier("isProductActiveServiceMapping")
    private LifeEngageSmooksHolder isProductActiveHolder;

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The life engage audit component. */
    @Autowired
    private LifeEngageAuditComponent lifeEngageAuditComponent;
    
    /** The life engage product component. */
    @Autowired
    private LifeEngageProductComponent lifeEngageProductComponent;
    
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageProductService#getProduct(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String getProduct(final String json, final String version) throws BusinessException {

    	 JSONObject jsonObject;
         String parseJson = "";
         try {
             jsonObject = new JSONObject(json);
             final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
             final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
             final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
             final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
                         
             parseJson = lifeEngageProductComponent.getProduct(requestInfo, json, version);
             Transactions transactions = new Transactions();
             final StatusData statusData = new StatusData();
             statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
             statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE);
             transactions.setStatusData(statusData);
             transactions.setTransTrackingId(requestInfo.getTransactionId());
             transactions.setType("product");
             lifeEngageAuditComponent.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json, lifeEngageAudit);
             

         } catch (Exception e) {
             throwBusinessException(true, e.getMessage());
         }

         return parseJson;
    }
    /* (non-Javadoc)
     * @see com.cognizant.insurance.services.LifeEngageProductService#isProductActive(java.lang.String)
     */
    @Override
    public final String isProductActive(final String json, final String version) throws BusinessException {

        JSONObject jsonObject;
        String parseJson = "";
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
           
            parseJson = lifeEngageProductComponent.isProductActive(requestInfo, json, version);
            Transactions transactions = new Transactions();
            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
            statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE);
            transactions.setStatusData(statusData);
            transactions.setTransTrackingId(requestInfo.getTransactionId());
            transactions.setType("isproductactive");
            lifeEngageAuditComponent.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json, lifeEngageAudit);

        } catch (Exception e) {
            throwBusinessException(true, e.getMessage());
        }

        return parseJson;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageProductService#getProducts(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String getProducts(final String json) throws BusinessException {

        return getProducts(json, false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageProductService#getActiveProducts(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String getActiveProducts(final String json) throws BusinessException {

        return getProducts(json, true);
    }

    /**
     * Gets the products.
     * 
     * @param json
     *            the json
     * @param activeProducts
     *            the active products
     * @return the products
     * @throws BusinessException
     *             the business exception
     */
    private String getProducts(final String json, final Boolean activeProducts) throws BusinessException {

        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        String parseJson = "";

        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final ProductSearchCriteria productSearchCriteria =
                    (ProductSearchCriteria) getProductsHolder.parseJson(json);
            Response<List<ProductSpecification>> response;
            if (activeProducts) {
                response = productRepository.getProducts(productSearchCriteria, true);
            } else {
                response = productRepository.getProducts(productSearchCriteria, false);
            }
            final List<ProductSpecification> prodSpecList = response.getType();

            final ResponseBo responseBo = new ResponseBo();
            final ResponseInfo responseInfo = createResponseInfo(requestInfo);
            responseBo.setResponseInfo(responseInfo);
            final ResponsePayload responsePayLoad = new ResponsePayload();
            responsePayLoad.setProductSpecification(prodSpecList);
            responseBo.setResponsePayload(responsePayLoad);
            parseJson = getProductsHolder.parseBO(responseBo);
            jsonObjectRs = new JSONObject(parseJson);

        } catch (Exception e) {
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }
    
    /**
     * Get all products by filter : carrierId & ChannelId
     * @param json
     * @return
     * @throws BusinessException
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String getAllProductsByFilter(final String json, final String version) throws BusinessException {

        return getAllOrActiveProductsByFilter(json, false, version);
    }

  /**
   * Get all Active products by filter : carrierId & ChannelId
   * @param json
   * @return
   * @throws BusinessException
   */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String getAllActiveProductsByFilter(final String json, final String version) throws BusinessException {

        return getAllOrActiveProductsByFilter(json, true, version);
    }
    
    /**
     * Get all products by filter : carrierId & ChannelId
     * 
     * @param json
     *            the json
     * @param activeProducts
     *            the active products
     * @return the products
     * @throws BusinessException
     *             the business exception
     */
    private String getAllOrActiveProductsByFilter(final String json, final Boolean activeProducts ,final String version) throws BusinessException {

        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        String parseJson = "";

        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            
            final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
            
            parseJson = lifeEngageProductComponent.getAllOrActiveProductsByFilter(requestInfo, json, activeProducts, version);
            jsonObjectRs = new JSONObject(parseJson);
            
            Transactions transactions = new Transactions();
            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
            statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE);
            transactions.setStatusData(statusData);
            transactions.setTransTrackingId(requestInfo.getTransactionId());
            transactions.setType("activeproducts");
            lifeEngageAuditComponent.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json, lifeEngageAudit);
           
        } catch (Exception e) {
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }

    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String getProductsByFilter(String json, String version) throws BusinessException {
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        String parseJson = "";
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            
            final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
            
            parseJson = lifeEngageProductComponent.getProductsByFilter(requestInfo, json, version);
            jsonObjectRs = new JSONObject(parseJson);
            
            Transactions transactions = new Transactions();
            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
            statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE);
            transactions.setStatusData(statusData);
            transactions.setTransTrackingId(requestInfo.getTransactionId());
            transactions.setType("products");
            lifeEngageAuditComponent.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json, lifeEngageAudit);
           
        } catch (Exception e) {
            throwBusinessException(true, e.getMessage());
        }
        
        return jsonObjectRs.toString();
    }

    /* (non-Javadoc)
     * @see com.cognizant.insurance.services.LifeEngageProductService#getProductTemplatesByFilter(java.lang.String)
     */
    @Override
    public String getProductTemplatesByFilter(String json) throws BusinessException {
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        String parseJson = "";
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final ProductSearchCriteria productSearchCriteria =
                (ProductSearchCriteria) getproductTemplatesByFilterHolder.parseJson(json);
            Response<List<ProductTemplateMapping>> response = productRepository.getProductTemplatesByFilter(productSearchCriteria);
            final List<ProductTemplateMapping> productTemplateMappings = response.getType();
            
            final ResponseBo responseBo = new ResponseBo();
            final ResponseInfo responseInfo = createResponseInfo(requestInfo);
            responseBo.setResponseInfo(responseInfo);
            
            final ResponsePayload responsePayLoad = new ResponsePayload();
            productSearchCriteria.setTemplatemapping(productTemplateMappings);
            responsePayLoad.setProductSearchCriteria(productSearchCriteria);
            responseBo.setResponsePayload(responsePayLoad);
            parseJson = getproductTemplatesByFilterHolder.parseBO(responseBo);
            jsonObjectRs = new JSONObject(parseJson);
            
        } catch (Exception e) {
            throwBusinessException(true, e.getMessage(),e);
        }
        return jsonObjectRs.toString();
    }
    /**
     * @return the getProductHolder
     */
    public final LifeEngageSmooksHolder getGetProductHolder() {
        return getProductHolder;
    }

    /**
     * @param getProductHolder
     *            the getProductHolder to set
     */
    public final void setGetProductHolder(final LifeEngageSmooksHolder getProductHolder) {
        this.getProductHolder = getProductHolder;
    }

    /**
     * @return the productRepository
     */
    public final ProductRepository getProductRepository() {
        return productRepository;
    }

    /**
     * @param productRepository
     *            the productRepository to set
     */
    public final void setProductRepository(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * @return the getProductsHolder
     */
    public final LifeEngageSmooksHolder getGetProductsHolder() {
        return getProductsHolder;
    }

    /**
     * @param getProductsHolder
     *            the getProductsHolder to set
     */
    public final void setGetProductsHolder(final LifeEngageSmooksHolder getProductsHolder) {
        this.getProductsHolder = getProductsHolder;
    }
	/**
	 * @return the isProductActiveHolder
	 */
	public final LifeEngageSmooksHolder getIsProductActiveHolder() {
		return isProductActiveHolder;
	}
	/**
	 * @param isProductActiveHolder the isProductActiveHolder to set
	 */
	public final void setIsProductActiveHolder(
			final LifeEngageSmooksHolder isProductActiveHolder) {
		this.isProductActiveHolder = isProductActiveHolder;
	}

	private ResponseInfo createResponseInfo(RequestInfo requestInfo) {
	    final ResponseInfo responseInfo = new ResponseInfo();
	    responseInfo.setCreationDateTime(requestInfo.getCreationDateTime());
        responseInfo.setRequestorToken(requestInfo.getRequestorToken());
        responseInfo.setSourceInfoName(requestInfo.getSourceInfoName());
        responseInfo.setTransactionId(requestInfo.getTransactionId());
        responseInfo.setUserName(requestInfo.getUserName());

        return responseInfo;
	}
	
}
