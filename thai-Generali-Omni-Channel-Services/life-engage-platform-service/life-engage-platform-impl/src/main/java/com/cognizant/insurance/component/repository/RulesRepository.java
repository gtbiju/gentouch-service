/**
 *
 * Copyright 2012, Cognizant
 *
 * @author : 262471
 * @version : 0.1, Feb 14, 2013
 */
package com.cognizant.insurance.component.repository;

import java.io.IOException;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.icr.exception.RuleException;
import com.cognizant.icr.rule.bom.RuleCriteria;
import com.cognizant.icr.rule.v2.RuleEngine;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;

/**
 * The Class class RuleRepository.
 */
//@Repository
public class RulesRepository {

    /** The Constant RULEEXCEPTION. */
    public static final String RULEEXCEPTION = "RULEEXCEPTION";

    /**
     * The rule engine.
     */
    @Autowired
    private RuleEngine ruleEngine;

    /**
     * Execute rule.
     * 
     * @param criteria
     *            the criteria
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    public final String executeRule(final RuleCriteria criteria, final String json) throws BusinessException {

        String result = null;
        try {
            result = ruleEngine.executeRuleWithJson(criteria, json);
        } catch (SQLException ex) {
            throw new SystemException(ex);
        } catch (IOException ex) {
            throw new SystemException(ex);
        } catch (RuleException ex) {
            final StringBuilder stringBuilder = new StringBuilder();
            if (ex.getFileName() != null) {
                stringBuilder.append(ex.getFileName());
            }
            if (ex.getLineNumber() > 0) {
                stringBuilder.append(ex.getLineNumber());
            }
            throw new BusinessException(RULEEXCEPTION, stringBuilder.toString(), ex);
        }
        return result;
    }

    /**
     * @return the ruleEngine
     */
    public final RuleEngine getRuleEngine() {
        return ruleEngine;
    }

    /**
     * @param ruleEngine the ruleEngine to set
     */
    public final void setRuleEngine(final RuleEngine ruleEngine) {
        this.ruleEngine = ruleEngine;
    }
}
