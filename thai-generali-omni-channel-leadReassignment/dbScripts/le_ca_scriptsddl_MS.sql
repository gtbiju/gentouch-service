IF OBJECT_ID('dbo.ACHIEVEMENT', 'U') IS NOT NULL
  DROP TABLE dbo.ACHIEVEMENT


CREATE TABLE ACHIEVEMENT (
	[agentId] varchar(255) NOT NULL,
    [title]	varchar(255) NOT NULL,
	[text] varchar(255) DEFAULT NULL,  
	PRIMARY KEY ([agentId],[title])
)  ;  
  
IF OBJECT_ID('dbo.AGENT', 'U') IS NOT NULL
  DROP TABLE dbo.AGENT
  
CREATE TABLE AGENT (
  [agentCode] varchar(255) NOT NULL,
  [admCode] varchar(255) DEFAULT NULL,
  [advisorCategory] varchar(255) DEFAULT NULL,
  [riefWriteUp] varchar(255) DEFAULT NULL,
  [mployeeId] varchar(255) DEFAULT NULL,
  [fullname] varchar(255) DEFAULT NULL,
  [go] varchar(255) DEFAULT NULL,
  [name_of_excelsheet] varchar(255) DEFAULT NULL,
  [role] varchar(255) DEFAULT NULL,
  [trustFactor] varchar(255) DEFAULT NULL,
  [uploaded_timestamp]  datetime2(0) DEFAULT NULL,
  [userid] varchar(255) DEFAULT NULL,
  PRIMARY KEY ([agentCode])
)  ;  
    
IF OBJECT_ID('dbo.AGENT_APP_LOGIN', 'U') IS NOT NULL
  DROP TABLE dbo.AGENT_APP_LOGIN

CREATE TABLE AGENT_APP_LOGIN (

  [userid] varchar(20) NOT NULL DEFAULT '',
  [password] varchar(256) DEFAULT NULL,
  [active] varchar(20) DEFAULT NULL,
  [agentcode] varchar(255) DEFAULT NULL,
  PRIMARY KEY ([userid])
)  ;  

IF OBJECT_ID('dbo.APPLICATION_CONTEXTS', 'U') IS NOT NULL
  DROP TABLE dbo.APPLICATION_CONTEXTS

CREATE TABLE APPLICATION_CONTEXTS (

  [Id] bigint NOT NULL , 
  [Context] varchar(255) DEFAULT NULL,
  PRIMARY KEY ([Id])
)  ;  

IF OBJECT_ID('dbo.CONTENT_TYPES', 'U') IS NOT NULL
  DROP TABLE dbo.CONTENT_TYPES

  
CREATE TABLE CONTENT_TYPES (
  [Id] bigint NOT NULL , 
  [Type] varchar(255) DEFAULT NULL,
  [priority] int DEFAULT NULL,
  [extensions] varchar(256) DEFAULT NULL,
  [optionality] TINYINT,
  [previewable] TINYINT,
  PRIMARY KEY ([Id])
) ;

IF OBJECT_ID('dbo.CONTENTFILES', 'U') IS NOT NULL
  DROP TABLE dbo.CONTENTFILES


CREATE TABLE CONTENTFILES (
  [fileName] varchar(255) NOT NULL,
  [lastUpdated] datetime2(0) DEFAULT NULL,
  [version] int DEFAULT NULL,
  [publicURL] varchar(255) DEFAULT NULL,
  [tabletFullPath] varchar(255) DEFAULT NULL,
  [Type_Id] bigint DEFAULT NULL,
  [comments] varchar(255) DEFAULT NULL,
  [lang_id] bigint,
  [is_mandatory] TINYINT DEFAULT '1',
  PRIMARY KEY ([fileName]), 
  CONSTRAINT [FKBC11519E360674B9] FOREIGN KEY ([Type_Id]) REFERENCES CONTENT_TYPES ([id])
) ;

IF OBJECT_ID('dbo.CONTENTFILES_APPCONTEXT_X', 'U') IS NOT NULL
  DROP TABLE dbo.CONTENTFILES_APPCONTEXT_X


CREATE TABLE CONTENTFILES_APPCONTEXT_X (
  [contentFile] varchar(255) NOT NULL,
  [appContextId] bigint NOT NULL
  
) ;

CREATE INDEX [FK9E3B01C826F5859] ON CONTENTFILES_APPCONTEXT_X ([contentFile]);
CREATE INDEX [FK9E3B01C8227E8B28] ON CONTENTFILES_APPCONTEXT_X ([appContextId]);

IF OBJECT_ID('dbo.NWS_USER', 'U') IS NOT NULL
  DROP TABLE dbo.NWS_USER


CREATE TABLE NWS_USER (
  [UID] varchar(255) NOT NULL,
  [givenName] varchar(255) DEFAULT NULL,
  [mnylAgentCode] varchar(255) DEFAULT NULL,
  [mnylAgentEmail] varchar(255) DEFAULT NULL,
  [mnylBossID] varchar(255) DEFAULT NULL,
  [mnylMyAgentRoleCode] varchar(255) DEFAULT NULL,
  [mnylStatus] varchar(255) DEFAULT NULL,
  [title] varchar(255) DEFAULT NULL,
  [UserMarkedAsDeleted] varchar(255) DEFAULT NULL,
  PRIMARY KEY ([UID])
);


IF OBJECT_ID('dbo.CONTENT_LANGUAGES', 'U') IS NOT NULL
  DROP TABLE dbo.CONTENT_LANGUAGES

CREATE TABLE CONTENT_LANGUAGES (
  [id] bigint NOT NULL ,
  [name] varchar(60) DEFAULT NULL,
  PRIMARY KEY ([id])
) ;




ALTER TABLE CONTENTFILES
 ADD CONSTRAINT FK_CONFILE_CONLANG FOREIGN KEY (lang_id) REFERENCES CONTENT_LANGUAGES (id) ;

 ALTER TABLE CONTENTFILES
  ADD description VARCHAR(255);

ALTER TABLE CONTENTFILES
  ADD size bigint;

 