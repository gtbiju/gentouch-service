/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.agent.agreement.component.impl;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.cognizant.insurance.agent.agreement.component.AgreementComponent;
import com.cognizant.insurance.agent.agreement.dao.AgreementDao;
import com.cognizant.insurance.agent.agreement.dao.impl.AgreementDaoImpl;
import com.cognizant.insurance.agent.domain.agreement.Agreement;
import com.cognizant.insurance.agent.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.agent.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.agent.domain.documentandcommunication.Document;
import com.cognizant.insurance.agent.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.agent.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.agent.domain.finance.FinancialProvision;
import com.cognizant.insurance.agent.domain.finance.agreementloan.AgreementLoan;
import com.cognizant.insurance.agent.domain.finance.agreementloan.AgreementWithdrawal;
import com.cognizant.insurance.agent.domain.goalandneed.Agreement_GoalAndNeed;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.response.impl.ResponseImpl;
import com.cognizant.insurance.agent.searchcriteria.SearchCountResult;
import com.cognizant.insurance.agent.searchcriteria.SearchCriteria;

/**
 * The Class class AgreementComponentImpl.
 * 
 * @author 300797
 */
@Component
public class AgreementComponentImpl implements AgreementComponent {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#getAgreementLoans(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public final Response<Set<AgreementLoan>> getAgreementLoans(final Request<Agreement> agreementRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        return agreementDao.getAgreementLoans(agreementRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#getAgreements(com.cognizant.insurance.request.
     * Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<List<Agreement>> getAgreements(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest, Request<Integer> limitRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        final List<Agreement> agreements = agreementDao.getAgreements(agreementRequest, statusRequest, limitRequest);

        final Response<List<Agreement>> response = new ResponseImpl<List<Agreement>>();
        if (!agreements.isEmpty()) {
            response.setType(agreements);
        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#getAgreementWithdrawals(com.cognizant.insurance
     * .request.Request)
     */
    @Override
    public final Response<Set<AgreementWithdrawal>> getAgreementWithdrawals(final Request<Agreement> agreementRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        return agreementDao.getAgreementWithdrawals(agreementRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#getInsuranceAgreement(com.cognizant.insurance.
     * request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<Agreement> getInsuranceAgreement(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        return agreementDao.getInsuranceAgreement(agreementRequest, statusRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#getPreviousAgreements(com.cognizant.insurance.
     * request.Request)
     */
    @Override
    public final Response<Set<InsuranceAgreement>> getPreviousAgreements(
            final Request<PartyRoleInAgreement> partyRoleInAgreementRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        return agreementDao.getPreviousAgreements(partyRoleInAgreementRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.agreement.component.AgreementComponent#
     * retrieveAgreementForIdentifier(com.cognizant.insurance .request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<Agreement> retrieveAgreementForIdentifier(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        final List<Agreement> agreements = agreementDao.retrieveAgreementForIdentifier(agreementRequest, statusRequest);
        final Response<Agreement> response = new ResponseImpl<Agreement>();
        if (!agreements.isEmpty()) {
            response.setType(agreements.get(0));
        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#retrieveAgreementForTransTrackingId(com.cognizant
     * .insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<Agreement> retrieveAgreementForTransTrackingId(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        final List<Agreement> agreements =
                agreementDao.retrieveAgreementForTransTrackingId(agreementRequest, statusRequest);
        final Response<Agreement> response = new ResponseImpl<Agreement>();
        if (!agreements.isEmpty()) {
            response.setType(agreements.get(0));
        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#saveAgreement(com.cognizant.insurance.request.
     * Request)
     */
    @Override
    public final Response<Agreement> saveAgreement(final Request<Agreement> agreementRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        agreementDao.save(agreementRequest);
        final Response<Agreement> agreementResponse = new ResponseImpl<Agreement>();
        agreementResponse.setType(agreementRequest.getType());
        return agreementResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#saveFNAId(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Agreement_GoalAndNeed> saveFNAId(final Request<Agreement_GoalAndNeed> agreementgoalAndNeed) {

        final AgreementDao agreementDao = new AgreementDaoImpl();
        agreementDao.save(agreementgoalAndNeed);
        final Response<Agreement_GoalAndNeed> agreementResponse = new ResponseImpl<Agreement_GoalAndNeed>();
        agreementResponse.setType(agreementgoalAndNeed.getType());
        return agreementResponse;

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#saveFinancialProvision(com.cognizant.insurance
     * .request.Request)
     */
    @Override
    public final Response<Long> saveFinancialProvision(final Request<FinancialProvision> finanProvReq) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        agreementDao.save(finanProvReq);

        final Response<Long> financProvResp = new ResponseImpl<Long>();
        financProvResp.setType(Long.valueOf(finanProvReq.getType().getId()));
        return financProvResp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#updateAgreement(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public Response<Agreement> updateAgreement(final Request<Agreement> agreementRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        agreementDao.merge(agreementRequest);
        final Response<Agreement> agreementResponse = new ResponseImpl<Agreement>();
        agreementResponse.setType(agreementRequest.getType());
        return agreementResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#mergeAgreement(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public void mergeAgreement(Request<Agreement> agreementRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        agreementDao.merge(agreementRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#getAgreementDocumentDetails(com.cognizant.insurance
     * .request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<AgreementDocument> getAgreementDocumentDetails(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        return agreementDao.getAgreementDocumentDetails(agreementRequest, statusRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.agreement.component.AgreementComponent#getAgreementDetails(com.cognizant.insurance
     * .request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Agreement> getStatus(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        return agreementDao.getStatus(agreementRequest, statusRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#retrieveFNAId(com.cognizant.insurance.request.
     * Request, java.lang.String)
     */
    @Override
    public String retrieveFNAId(final Request<Agreement_GoalAndNeed> request, final String illustrationID) {

        final AgreementDao agreementDao = new AgreementDaoImpl();
        return agreementDao.getFnaId(request, illustrationID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#getAgreementCount(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<SearchCountResult> getAgreementCount(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        Response<SearchCountResult> searchCountResponse =
                agreementDao.getAgreementCount(searchCriteriatRequest, statusRequest);
        return searchCountResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#getAgreementByFilter(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<Agreement>> getAgreementByFilter(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        final List<Agreement> agreements = agreementDao.getAgreementByFilter(searchCriteriatRequest, statusRequest);

        final Response<List<Agreement>> agreementFilterResponse = new ResponseImpl<List<Agreement>>();
        if (!agreements.isEmpty()) {
            agreementFilterResponse.setType(agreements);
        }
        return agreementFilterResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.component.AgreementComponent#updateAgreementStatusForLead(com.cognizant.insurance
     * .request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public void updateAgreementStatusForLead(final Request<String> leadTransTrackingIdRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        agreementDao.updateAgreementStatusForLead(leadTransTrackingIdRequest, statusRequest);

    }
    

    /* (non-Javadoc)
     * @see com.cognizant.insurance.agreement.component.AgreementComponent#getRequirementDocumentDetails(com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Requirement> getRequirementDocumentDetails(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        Response<Requirement> req = agreementDao.getRequirementDocumentDetails(agreementRequest, statusRequest);
        return req;
    }

    /* (non-Javadoc)
     * @see com.cognizant.insurance.agreement.component.AgreementComponent#insuranceAgreementgetInsuranceAgreementForReqDocUpdate(com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public void updateInsuranceAgreementWithRequirementDocUpdate(
            Request<Agreement> agreementRequest, Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
         agreementDao.getInsuranceAgreementForReqDocUpdate(agreementRequest, statusRequest);
    }

    /* (non-Javadoc)
     * @see com.cognizant.insurance.agreement.component.AgreementComponent#getInsuranceAgreementWithRequirementDocFile(com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Document> getRequirementDocFile(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        Response<Document> req = agreementDao.getRequirementDocFile(agreementRequest, statusRequest);
        return req;
    }

    /* (non-Javadoc)
     * @see com.cognizant.insurance.agreement.component.AgreementComponent#getRequirementDocFileList(com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<AgreementDocument> getRequirementDocFileList(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
        Response<AgreementDocument> req = agreementDao.getRequirementDocFileList(agreementRequest, statusRequest);
        return req;
    }
    
    
    /* (non-Javadoc)
     * @see com.cognizant.insurance.agreement.component.AgreementComponent#getInsuranceAgreementForSaveReq(com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public void updateInsuranceAgreementWithRequirement(
            Request<Agreement> agreementRequest, Request<List<AgreementStatusCodeList>> statusRequest) {
        final AgreementDao agreementDao = new AgreementDaoImpl();
         agreementDao.getInsuranceAgreementForSaveReq(agreementRequest, statusRequest);
    }
    

}
