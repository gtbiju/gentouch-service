/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of EducationCertificates according to their
 * level.
 * 
 * @author 301350
 * 
 * 
 */
public enum EducationCertificateLevelCodeList {
    /**
     * Identifies an education certificate with grade 'black belt'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    ProfessionalCertification,
    /**
     * Identifies an education certificate at level 'Mba'.
     * 
     * 
     * 
     */
    Mba
}
