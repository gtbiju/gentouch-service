/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Dec 13, 2013
 */
package com.cognizant.insurance.domain.product;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Class class ProductFundMapping.
 * 
 * @author 300797
 */

@Entity
@Table(name = "CORE_PROD_FUND_MAPPING")
public class ProductFundMapping extends Component {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7896221550321763251L;

    /** The product funds. */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    @JoinColumn(name = "FUND_ID")
    private ProductFunds productFunds;

    /** The product specification. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    @JoinColumn(name = "PROD_ID")
    private ProductSpecification productSpecification;
    

    /**
     * Gets the product funds.
     * 
     * @return the productFunds
     */
    public final ProductFunds getProductFunds() {
        return productFunds;
    }

    /**
     * Sets the product funds.
     * 
     * @param productFunds
     *            the productFunds to set
     */
    public final void setProductFunds(ProductFunds productFunds) {
        this.productFunds = productFunds;
    }

    /**
     * Gets the product specification.
     * 
     * @return the productSpecification
     */
    public final ProductSpecification getProductSpecification() {
        return productSpecification;
    }

    /**
     * Sets the product specification.
     * 
     * @param productSpecification
     *            the productSpecification to set
     */
    public final void setProductSpecification(ProductSpecification productSpecification) {
        this.productSpecification = productSpecification;
    }
}
