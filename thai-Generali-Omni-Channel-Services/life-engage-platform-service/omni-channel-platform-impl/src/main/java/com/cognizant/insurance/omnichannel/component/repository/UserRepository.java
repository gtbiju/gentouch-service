package com.cognizant.insurance.omnichannel.component.repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.omnichannel.component.ODSUserComponent;
import com.cognizant.insurance.omnichannel.dataresults.dto.TeamDetailsDto;
import com.cognizant.insurance.omnichannel.vo.AgentReportVo;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.agent.PasswordHistory;
import com.cognizant.le.security.exception.UserNotFoundException;

@Component
public class UserRepository {
	
	 
    private static final String LE_TXN_MANAGER = "le_txn_manager";
    
    private static final String ODS_TXN_MANAGER = "ods_txn_manager";
    /** The entity manager. */
    @PersistenceContext (unitName = "LE_Platform")
    private EntityManager entityManager;
    
    @PersistenceContext (unitName = "ODS_unit")
    private EntityManager odsEntityManager;
    
    @Value("${le.platform.service.passwordHistory.number}")
	private Integer noPreviousPasswordHistoryCheck;
    
    private ContextTypeCodeList contextId; //To Check
    
    @Autowired
    private ODSUserComponent odsUserComponent; // to check whether AgentDAO can be used
    
    @Value("${le.platform.service.security.resetPassword.expiryDuration}")
    private int passwordexpiryDate;
    
     /**
     * Validate user.
     * 
     * @param transactions
     *            the transactions
     * @param responseMsg 
     * @param responseCode 
     * @return the transactions
     * @throws BusinessException
     *             the business exception
     * @throws UserNotFoundException
     *             the user not found exception
     */
    @Transactional(value = ODS_TXN_MANAGER)
    public Transactions validateAgentProfile(Transactions transactions) {
    	
        Request<Transactions> request = new JPARequestImpl<Transactions>(odsEntityManager,contextId, "");
        request.setType(transactions);
        Response<GeneraliAgent> response=odsUserComponent.validateAgentProfile(request);
               
		if (response.getType() != null) {
			transactions.setGeneraligent(response.getType());
		}
        return transactions;
    }
    
    @Transactional(value = ODS_TXN_MANAGER)
    public Transactions validateGAOAgentProfile(Transactions transactions) {
    	
        Request<Transactions> request = new JPARequestImpl<Transactions>(odsEntityManager,contextId, "");
        request.setType(transactions);
        Response<GeneraliGAOAgent> response=odsUserComponent.validateGAOAgentProfile(request);
       
		if (response.getType() != null) {
			transactions.setGaoAgent(response.getType());
		}
        return transactions;
    }
    
    public GeneraliAgent agentProfileLoginValidation(String agentCode){
    	Transactions transactions = new Transactions();
    	transactions.setKey1(agentCode);
    	transactions  = validateAgentProfile(transactions);
    	GeneraliAgent agent  = transactions.getGeneraligent();
		return agent;
    }
    
    public GeneraliGAOAgent gaoAgentProfileLoginValidation(String agentCode){
    	Transactions transactions = new Transactions();
    	transactions.setKey1(agentCode);
    	transactions  = validateGAOAgentProfile(transactions);
    	GeneraliGAOAgent agent  = transactions.getGaoAgent();
		return agent;
    }
    
    @Transactional(value = LE_TXN_MANAGER)
   	public Response<GeneraliTHUser> validateLoggedInUser(Transactions transactions) {        
    	
        Request<Transactions> request = new JPARequestImpl<Transactions>(entityManager,contextId, "");
        
        request.setType(transactions);
        Response<GeneraliTHUser> response=odsUserComponent.validateLoggedInUser(request); 
        return response;
    }
	
    @Transactional(value = LE_TXN_MANAGER)
	public void createPassword(Transactions transactions) {
    	
    	Request<GeneraliTHUser> request = new JPARequestImpl<GeneraliTHUser>(entityManager,contextId, "");    	
    	GeneraliTHUser generaliThUser = new GeneraliTHUser();
    	generaliThUser.setUserId(transactions.getKey1());
    	generaliThUser.setPassword(transactions.getKey5());
    	generaliThUser.seteMail(transactions.getKey6());
    	// setting the orgin(source or OS form factor) of the request
    	generaliThUser.setOrgin(transactions.getSource());
    	
    	GeneraliAgent agent = agentProfileLoginValidation(transactions.getKey1());
    	
    	if(agent!=null){
    		generaliThUser.setIsActive(agent.getAgentStatus().equals("INFORCE"));
    	}else{
    		GeneraliGAOAgent gaoAgent =gaoAgentProfileLoginValidation(transactions.getKey1());
    		if(gaoAgent!=null){
    			generaliThUser.setIsActive(gaoAgent.getAgentStatus().equals("INFORCE"));
    		}
    	}
    	generaliThUser.setCreatedDate(new Date());
    	
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(new Date());
    	cal.add(cal.DATE, passwordexpiryDate);
    	Date expirydate = cal.getTime();
    	generaliThUser.setPasswordExpiryDate(expirydate);
    	
    	request.setType(generaliThUser);  	
    	odsUserComponent.saveLoginDetails(request);
    	
    	Request<PasswordHistory> historyRequest=new JPARequestImpl<PasswordHistory>(entityManager,contextId, "");    	
        PasswordHistory history=new PasswordHistory();
        history.setPassword(transactions.getKey5());
        history.setCreatedOn(new Date());
        history.setUserId(transactions.getKey1());
        
        historyRequest.setType(history);
        odsUserComponent.saveHistory(historyRequest); 
    }
    
    @Transactional(value = LE_TXN_MANAGER)
   	public void resetPassword(Transactions transactions) {
    	
   		Request<GeneraliTHUser> request = new JPARequestImpl<GeneraliTHUser>(entityManager,contextId, "");   		
   		GeneraliTHUser generaliThUser = transactions.getGeneraliThUser();
   		generaliThUser.setUpdatedDate(new Date());
   		
   		GeneraliAgent agent = agentProfileLoginValidation(transactions.getKey1());
   		if(agent!=null){
    		generaliThUser.setIsActive(agent.getAgentStatus().equals("INFORCE"));
    	}else{
    		GeneraliGAOAgent gaoAgent =gaoAgentProfileLoginValidation(transactions.getKey1());
    		if(gaoAgent!=null){
    			generaliThUser.setIsActive(gaoAgent.getAgentStatus().equals("INFORCE"));
    		}
    	}
       	generaliThUser.setPassword(transactions.getKey5());
       	
       	if(!transactions.getKey6().isEmpty()) {
    	    generaliThUser.seteMail(transactions.getKey6());
    	}
   		
   		Calendar cal = Calendar.getInstance();
       	cal.setTime(new Date());
       	cal.add(cal.DATE, passwordexpiryDate);
       	Date expirydate = cal.getTime();
       	generaliThUser.setPasswordExpiryDate(expirydate);
       	
       	generaliThUser.setAttempts(0);
		generaliThUser.setPasswordLocked(false);
       	
   		request.setType(generaliThUser);  	
       	odsUserComponent.resetPassword(request);
       	
       	Request<PasswordHistory> historyRequest=new JPARequestImpl<PasswordHistory>(entityManager,contextId, "");    	
        PasswordHistory history=new PasswordHistory();
        history.setPassword(transactions.getKey5());
        history.setCreatedOn(new Date());
        history.setUserId(transactions.getKey1());
         
        historyRequest.setType(history);
        odsUserComponent.saveHistory(historyRequest); 
        
        if(getOldPasswordHistory(generaliThUser).getType() != null) {
           Request<PasswordHistory> oldHistoryRequest=new JPARequestImpl<PasswordHistory>(entityManager,contextId, "");
           oldHistoryRequest.setType(getOldPasswordHistory(generaliThUser).getType());
           odsUserComponent.removeOldHistory(oldHistoryRequest);
        }
   	}
    
    @Transactional(value = LE_TXN_MANAGER)
    public Response<List<PasswordHistory>> isThisRecentPassword(GeneraliTHUser user){
    	
    	Request<String> userRequest=new JPARequestImpl<String>(entityManager,contextId, "");
    	userRequest.setType(user.getUserId());
    	Request<Integer> historyRequest=new JPARequestImpl<Integer>(entityManager,contextId, "");
    	historyRequest.setType(noPreviousPasswordHistoryCheck);
    	Response<List<PasswordHistory>> response=odsUserComponent.getRecentPasswords(userRequest, historyRequest);
    	return response;
    }
    
    @Transactional(value = LE_TXN_MANAGER)
    private Response<PasswordHistory> getOldPasswordHistory(GeneraliTHUser user){
    	
    	Request<String> userRequest=new JPARequestImpl<String>(entityManager,contextId, "");
    	userRequest.setType(user.getUserId());
    	Response<PasswordHistory> response=odsUserComponent.getOldPasswordHistory(userRequest);
    	return response;
    }    
    
    @Transactional(value = ODS_TXN_MANAGER)
    public Response<List<TeamDetailsDto>> getTeamDetails(Transactions userTransactions) {
        
        Request<Transactions> request = new JPARequestImpl<Transactions>(odsEntityManager,contextId, "");
        request.setType(userTransactions);
        Response<List<TeamDetailsDto>> response=odsUserComponent.getTeamDetails(request);
        return response;
    }
    
    @Transactional(value = ODS_TXN_MANAGER, readOnly = true)
    public List<AgentReportVo> getAllAgentDetails() {
        List<AgentReportVo> agentReportList = new ArrayList<AgentReportVo>();
        Request<Transactions> request = new JPARequestImpl<Transactions>(odsEntityManager, contextId, "");
        request.setType(new Transactions());
        Response<List<GeneraliAgent>> response = odsUserComponent.getAllAgentDetails(request);
        List<GeneraliAgent> agentList = response.getType();
        for (GeneraliAgent generaliAgent : agentList) {
            AgentReportVo agentReportVo = new AgentReportVo();
            agentReportVo.setAgentCode(generaliAgent.getAgentCode());
            agentReportVo.setAgentName(generaliAgent.getAgentName());
            agentReportVo.setPosition(generaliAgent.getAgentPosition());
            agentReportVo.setSmName(generaliAgent.getSmName());
            agentReportVo.setGmName(generaliAgent.getGmName());
            agentReportVo.setTeamName(generaliAgent.getTeamEng());
            agentReportList.add(agentReportVo);
        }

        return agentReportList;
    }
}
