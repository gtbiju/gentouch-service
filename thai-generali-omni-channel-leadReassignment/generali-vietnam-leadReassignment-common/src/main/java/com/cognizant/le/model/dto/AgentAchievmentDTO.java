package com.cognizant.le.model.dto;

public class AgentAchievmentDTO {
	
	private String status;
	
	private String title;
	
	private String text;
	
    private String message;
	
	private String agentCode;
	
	private String achievementId;
	
	private boolean orphanChild= false;
	
	
	public boolean isOrphanChild() {
		return orphanChild;
	}

	public void setOrphanChild(boolean orphanChild) {
		this.orphanChild = orphanChild;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	
	
	public  String toString()
	{
	    return "Agent Code:" + this.agentCode+", "+"Title :"
	                            +this.title +", "+"Text:"
	                            +this.text+","+"orphanChild:"
	                            +this.orphanChild+","+"Status:"
	                            +this.status+",, "+"Message:"
	                            +this.message;
	}

	public String getAchievementId() {
		return achievementId;
	}

	public void setAchievementId(String achievementId) {
		this.achievementId = achievementId;
	}
	

}
