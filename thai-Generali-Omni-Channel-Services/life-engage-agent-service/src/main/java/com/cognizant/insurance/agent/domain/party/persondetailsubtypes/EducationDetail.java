/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.persondetailsubtypes;

import javax.persistence.Basic;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;

import com.cognizant.insurance.agent.domain.party.partycodelists.EducationLevelCodeList;
import com.cognizant.insurance.agent.domain.party.partycodelists.PersonDesignationCodeList;
import com.cognizant.insurance.agent.domain.party.partydetailsubtypes.PersonDetail;

/**
 * This concept defines details about a person's education.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("EDUCATIONDETAIL")
public class EducationDetail extends PersonDetail {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 9027363608544067996L;

    /**
     * The highest level of degree obtained by the person.
     * 
     * e.g: Associate, Masters
     * 
     * 
     * 
     */
    @Enumerated
    @Basic(fetch=FetchType.EAGER)
    private EducationLevelCodeList educationLevelCode;

    /**
     * Indicates the person is a full-time student (as contrasted with a
     * part-time student).
     * 
     * 
     * 
     * 
     * 
     */
    private Boolean fullTimeStudentIndicator;

    /**
     * A code identifying a professional designation.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private PersonDesignationCodeList designationCode;

    /**
     * Gets the educationLevelCode.
     * 
     * @return Returns the educationLevelCode.
     */
    public EducationLevelCodeList getEducationLevelCode() {
        return educationLevelCode;
    }

    /**
     * Gets the fullTimeStudentIndicator.
     * 
     * @return Returns the fullTimeStudentIndicator.
     */
    public Boolean getFullTimeStudentIndicator() {
        return fullTimeStudentIndicator;
    }

    /**
     * Gets the designationCode.
     * 
     * @return Returns the designationCode.
     */
    public PersonDesignationCodeList getDesignationCode() {
        return designationCode;
    }

    /**
     * Sets The educationLevelCode.
     * 
     * @param educationLevelCode
     *            The educationLevelCode to set.
     */
    public void setEducationLevelCode(
            final EducationLevelCodeList educationLevelCode) {
        this.educationLevelCode = educationLevelCode;
    }

    /**
     * Sets The fullTimeStudentIndicator.
     * 
     * @param fullTimeStudentIndicator
     *            The fullTimeStudentIndicator to set.
     */
    public void setFullTimeStudentIndicator(
            final Boolean fullTimeStudentIndicator) {
        this.fullTimeStudentIndicator = fullTimeStudentIndicator;
    }

    /**
     * Sets The designationCode.
     * 
     * @param designationCode
     *            The designationCode to set.
     */
    public void setDesignationCode(
            final PersonDesignationCodeList designationCode) {
        this.designationCode = designationCode;
    }

}
