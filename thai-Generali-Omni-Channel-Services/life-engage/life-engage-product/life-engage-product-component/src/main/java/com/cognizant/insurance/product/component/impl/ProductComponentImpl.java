/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */
package com.cognizant.insurance.product.component.impl;

import java.util.List;

import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.product.component.ProductComponent;
import com.cognizant.insurance.product.dao.ProductDao;
import com.cognizant.insurance.product.dao.impl.ProductDaoImpl;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;

/**
 * The Class class ProductComponentImpl.
 */

public class ProductComponentImpl implements ProductComponent {

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.product.component.ProductComponent#getProducts (String careerId)
     */
    @Override
    public final Response<List<ProductSpecification>> getProducts(
            final Request<ProductSearchCriteria> criteriaRequest) {

        final ProductDao productDao = new ProductDaoImpl();
        return productDao.getProducts(criteriaRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.product.component.ProductComponent#getActiveProducts (String careerId)
     */
    @Override
    public final Response<List<ProductSpecification>> getActiveProducts(
            final Request<ProductSearchCriteria> criteriaRequest) {
        final ProductDao productDao = new ProductDaoImpl();
        return productDao.getActiveProducts(criteriaRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.product.component.ProductComponent#getProduct(com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<ProductSpecification> getProduct(final Request<ProductSearchCriteria> criteriaRequest, final String version) {
        final ProductDao productDao = new ProductDaoImpl();
        final ProductSpecification product = productDao.getProduct(criteriaRequest, version);

        final Response<ProductSpecification> prodtSpecifics = new ResponseImpl<ProductSpecification>();
        prodtSpecifics.setType(product);

        return prodtSpecifics;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.product.component.ProductComponent#getProductById(com.cognizant.insurance.request.Request
     * )
     */
    @Override
    public final Response<ProductSpecification> getProductById(final Request<ProductSearchCriteria> request, final String version) {

        final ProductDao productDao = new ProductDaoImpl();
        final ProductSpecification product = productDao.getProductById(request,version);

        final Response<ProductSpecification> response = new ResponseImpl<ProductSpecification>();
        response.setType(product);

        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.product.component.ProductComponent#isProductActive(com.cognizant.insurance.request.Request
     * )
     */
    @Override
    public final Response<Boolean> isProductActive(final Request<ProductSearchCriteria> criteriaRequest, final String version) {

        final ProductDao productDao = new ProductDaoImpl();
        final Boolean isProductActive = productDao.isProductActive(criteriaRequest,version);

        final Response<Boolean> response = new ResponseImpl<Boolean>();
        response.setType(isProductActive);

        return response;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.product.component.ProductComponent#getActiveProductsByFilter(com.cognizant.insurance.
     * request.Request)
     */
    @Override
    public final Response<List<ProductSpecification>> getActiveProductsByFilter(
            final Request<ProductSearchCriteria> criteriaRequest, final String version) {
        final ProductDao productDao = new ProductDaoImpl();
        return productDao.getActiveProductsByFilter(criteriaRequest, version);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.product.component.ProductComponent#getProductsByFilter(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public final Response<List<ProductSpecification>> getAllProductsByFilter(
            final Request<ProductSearchCriteria> criteriaRequest, final String version) {
        final ProductDao productDao = new ProductDaoImpl();
        return productDao.getAllProductsByFilter(criteriaRequest, version);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.product.component.ProductComponent#getProductsByIds(com.cognizant.insurance.request.Request
     * )
     */
    @Override
    public Response<List<ProductSpecification>> getProductsByFilter(Request<ProductSearchCriteria> criteriaRequest, String version) {
        final ProductDao productDao = new ProductDaoImpl();
        return productDao.getProductsByFilter(criteriaRequest, version);
    }
    /* (non-Javadoc)
     * @see com.cognizant.insurance.product.component.ProductComponent#getProductTemplatesByFilter(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<ProductTemplateMapping>> getProductTemplatesByFilter(
            Request<ProductSearchCriteria> criteriaRequest) {
        final ProductDao productDao = new ProductDaoImpl();
        return productDao.getProductTemplatesByFilter(criteriaRequest);
    }

}
