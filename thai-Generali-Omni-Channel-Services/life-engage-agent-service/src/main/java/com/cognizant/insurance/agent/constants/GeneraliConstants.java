package com.cognizant.insurance.agent.constants;

public final class GeneraliConstants {
	
    public static final String ACR_PDF_TEMPLATE_TYPE = "22007";

    public static final String EAPP_PDF_TEMPLATE_TYPE = "22004";

    public static final String ILLUSTRATION_PDF_TEMPLATE_TYPE = "22005";

    public static final String FNA_TEMPLATE_TYPE = "22006";
    
    public static final long CARRIER_CODE = 1L;
    
    public static final String RELATED_TRANSACTIONS = "RelatedTransactions";
    
	public static final String BPM_SPAJ = "bpm_spaj";

	public static final String BPM_QUESTIONNAIRE = "bpm_questionnaire";
	
	public static final String ADD_INSURED_PDF = "additionalInsured";
	public static final String MAIN_INSURED_PDF = "mainInsured";
	public static final String ACR_PDF = "acr";

	public static final String ADD_INSURED_PDF_ID = "1009";


	public static final String BPM_ILLUSTRATION = "bpm_illustration";
	
	public static final String JSON_EMPTY = "";
	
	public static final String JSON_EMPTY_ARRAY = "[]";
	
	public static final String JSON_EMPTY_OBJECT = "{}";
	
	public static final String KEY21 = "Key21";
	
	
	// Vietnam Release Two BulkUpload Related ValidationMeaages.
	
	public static final String EMPTY_CUSTOMER_NAME = "Vui lòng nhập họ và tên lót khách hàng";//Please enter full Name
	public static final String INVALID_CUSTOMER_NAME = "Họ và tên lót khách hàng chỉ được chứa chữ cái và khoảng trắng";//Last name can have only alphabets
	
	public static final String EMPTY_FIRST_NAME= "Vui lòng nhập Tên hợp lệ"; //Please enter First Name” 
	public static final String INVALID_FIRST_NAME="Tên chỉ được chứa các chữ cái";// First Name can only have alphabets
	public static final String INVALID_FIRST_NAME_LENGTH="Tên khách hàng không vượt quá 60 ký tự";//Maximum of 60 characters only allowed for Customer first name
	
	public static final String EMPTY_LAST_NAME= "Vui lòng nhập Họ hợp lệ"; //Please enter Last Name” 
	public static final String INVALID_LAST_NAME="Họ chỉ được chứa các chữ cái";// Last Name can only have alphabets
	public static final String INVALID_LAST_NAME_LENGTH="Họ khách hàng không vượt quá 60 ký tự";//Maximum of 60 characters only allowed for Customer last name
	
	public static final String EMPTY_GENDER = "Vui lòng nhập giới tính";//Please enter gender
	
	public static final String INVALID_MOBILE_NUMBER = "Vui lòng nhập số điện thoại hợp lệ"; //"Enter a valid mobile number".
	
	public static final String INVALID_EMAIL_ID = "Vui lòng nhập email hợp lệ";//“Enter a valid email id”
	
	public static final String INVALID_DOB = "Vui lòng nhập ngày sinh hợp lệ (dd/mm/yyyy)";//Please enter a valid date of birth
	
	public static final String EMPTY_POTENTIAL = "Vui lòng nhập tiềm năng của khách hàng";//Please enter potential
	
	public static final String EMPTY_STATE = "Vui lòng nhập Tỉnh/TP";//Please enter state

	
	public static final String EMPTY_SOURCE = "Vui lòng nhập nguồn";//Please enter source
	
	public static final String EMPTY_ACTION = "Vui lòng nhập hành động tiếp theo";//Please enter action
														//Please select a future date not more than 90 days
	public static final String INAVALID_MEETING_DATE = "Ngày giờ hẹn gặp tiếp theo phải là ngày tương lai nhưng không vượt quá 90 ngày kể từ ngày hôm nay";
	
	public static final String EMPTY_DATE = "Vui lòng nhập ngày (dd/mm/yyyy ) và giờ (hh:mm) hẹn gặp tiếp theo hợp lệ ";//Please enter a valid date- time.
	
	public static final String INAVALID_BANK_NAME = "Vui lòng nhập tên ngân hàng";//Please enter bank name
	
	public static final String INAVALID_REFFER_NAME = "Tên người giới thiệu chỉ được chứa chữ cái và khoảng trắng"; //Referrer Name can only have alphabets and  spaces 
	
	public static final String EMPTY_CITY = "Please enter city";
	
	public static final String INVALID_REF_NAME = "Tên người giới thiệu chỉ được chứa chữ cái và khoảng trắng";
	
	public static final String SUSPENDED_TERMINATED_AGENT = "Vui lòng nhập mã số đại lý hợp lệ";//Please enter a valid Agent code. 
	
	public static final String OTHER_CHANNEL_AGENTS = "Vui lòng nhập mã số đại lý hợp lệ";//Please enter a valid Agent code. 
	
	public static final String INVALID_AGENTCODE = "Vui lòng nhập mã số đại lý hợp lệ";//Please enter a valid Agent code. 
	
	public static final String INVALID_NATIONALID = "Số CMND tối đa 30 kí tự"; //Max 30 characters allowed for Customer's National ID
	
	public static final String INVALID_ADDRESS = "Địa chỉ tối đa 100 kí tự";//Max 100 characters allowed for Customer 's Address
	
	public static final String INVALID_STATE = "Vui lòng nhập Tỉnh/TP";
	
	public static final String INVALID_NATIONALITY = "Vui lòng nhập Quốc tịch"; //Max 50 characters allowed for Customer's Nationality
	
	public static final String INVALID_OCCUPATION = "Vui lòng nhập nghề nghiệp";//Max 50 characters allowed
	
	public static final String DUPLICATE_LEAD = "Khách hàng tiềm năng này đã tồn tại trên hệ thống";
	
	public static final String INVALID_TEMPLATE = "Invalid template; Syetem can't process Data. Please check the data";
	
	public static final String NO_ALLOCATION="Không tồn tại chuyên viên tư vấn để phân bổ thông tin khách hàng tiềm năng";//"No agents avaialble to allocate the lead";
	
	public static final String NEXT_MEETING_TIME_INVALID = "Vui lòng nhập giờ hẹn gặp kế tiếp";// TIME NOT THERE
	
	public static final String NEXT_MEETING_DATE_INVALID = "Vui lòng nhập ngày hẹn gặp kế tiếp"; // DATE NOT THERE
	
	public static final String NAME_INVALID_LENGTH = "Vui lòng nhập Họ tên khách hàng hợp lệ"; // Name invalid length
	
	public static final String NationalID_INVALID_CHARACTER="Vui lòng nhập đúng CMND";
	
	
	
	//Vietnam Relese 2
	
	public static final String BULKUPLOAD = "BulkUpload";
	public static final String BULKUPLOAD_PENDING = "BulkUploadPending";
	
	
	
	// Vietnam Excel type related Constants

	public static final String VALID = "VALID";
	public static final String INVALID = "INVALID";
	public static final String VALID_SUCCESS = "ValidSuccess";
	public static final String AGENT_FAILED = "AllocationFailed";
	
	//Vietnam 2 - General Constants
	
	public static final String TYPE_AGENCY = "Agency";
	public static final String TYPE_BANCA = "Banca";
	public static final String TYPE_BANCA_AGENT = "IOIS";
	public static final String TYPE_REASSIGN = "leadReassign";
	
	public static final String BULK_AGENCY="BulkUploadAgency";
	public static final String BULK_BANCA="BulkUploadBanca";
	
	public static final String STATUS_ACTIVE = "Active";
	
	public static final String KEY12 = "Key12";

	//Vietnam R3- General Constants

	public static final String bulkUploadPushNotificationMessageAgency="Danh sách Khách hàng tiềm năng của bạn vừa được cập nhật.";	

	public static final String pushNotificationMessage="khách hàng vừa được giới thiệu đến anh/chị.";
	
	public static final String MAIL_SUBJECT="Bulk Upload Results";
	
	public static final String LEADREASSIGN = "leadReAssign";
	
	public static final String DELETEBRNCHUSERASSIGNEDLEAD = "branchuserAssignedLeadDelete";

	public static final String DASHBOARDCRITERIAREQUEST = "dashboardCriteriaRequest";	

	public static final String AGENTIDS = "agentIds";
	
	public static final String LISTINGDASHBOARD = "ListingDashBoard";
	
	public static final String RETRIEVE_BY_HIERARCHYLEVEL = "HierarchyLevelRetrieve";
	
	public static final String RETRIEVE_BY_LEADREASSIGN = "LeadReassignRetrieve";
	
	public static final String REASSIGNKEY = "rm_reassign";
	
	public static final String RETRIEVE_BY_CONVERSIONRATE = "ConversionRateRetrieve";
	
	public static String CHOOSEPARTY="CHOOSEPARTY";
	
	public static String LMS="LMS";

	// Code lookup related Constants

	public static final String TYPE_STATE="STATE";	

	public static final String TYPE_NATIONALITY="NATIONALITY";
	
	public static final String TYPE_OCCUPATION="OCCUPATION_LIST";
	
	public static final String TYPE_NEED="NEED";
	
	public static final String TYPE_INCOME="INCOME";
	
	public static final String LANG_EN="en";
	
	public static final String LANG_VT="vt";
	
	public static final String LANG_BULK="bulk";
	
	public static final String COUNTRY_VT="VT";
	
	public static final String BULK="BULK";
	
	public static final String US="US";
	
	public static final String RETRIEVE_BY_NEW_STATUS="RetrieveByNew";
	
	public static final String RETRIEVE_BY_CLOSED_STATUS="RetrieveByClosed";
	
	public static final String RETRIEVE_BY_FOLLOWUP_STATUS="RetrieveByFollowUp";
	public static final String PDF_IMAGE_LOC = "le.pdf.image.location";
	public static final String CONFIG_PROPERTY_FILE = "omni-channel-vietnam-platform-service-qa";
	// codes for geofencing
	
	public static final String AGENTCHECKIN="agentCheckin";
	
	public static final String SUBMITTED="SUBMITTED";

	public static final String REQUEST = "Request";
    public static final String REQUEST_INFO = "RequestInfo";
    public static final String TRANSACTIONS = "Transactions";
    public static final String REQUEST_PAYLOAD = "RequestPayload";

	public static final String TRANSACTION_DATA = "TransactionData";

}
