/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.agreementcodelists;

/**
 * It indicates the relative degree of damageability of the vehicle.
 * 
 * @author 301350
 * 
 * 
 */
public enum VehicleDamageabilityCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Average,
    /**
     * 
     * 
     * 
     * 
     */
    High,
    /**
     * 
     * 
     * 
     * 
     */
    Low
}
