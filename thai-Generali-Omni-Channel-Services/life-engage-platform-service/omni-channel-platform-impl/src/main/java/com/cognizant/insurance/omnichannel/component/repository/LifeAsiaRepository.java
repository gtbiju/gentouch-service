package com.cognizant.insurance.omnichannel.component.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.agreement.component.AgreementComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.domain.agreement.AddressTranslation;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliAgreementComponent;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaMetaData;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaRequest;
import com.cognizant.insurance.omnichannel.domain.UnderWriterMemoDetails;
import com.cognizant.insurance.omnichannel.jpa.component.LifeAsiaRequestComponent;
import com.cognizant.insurance.omnichannel.lifeasia.searchcriteria.LifeAsiaSearchCriteria;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;

public class LifeAsiaRepository {

	/** The entity manager. */
	@PersistenceContext(unitName = "LE_Platform")
	private EntityManager entityManager;
	@Value("${generali.platform.service.lifeasia.failedRange.minute}")
	private Integer failedSpajMinute;

	@Autowired
	LifeAsiaRequestComponent lifeAsiaComponent;

	@Autowired
	private GeneraliAgreementComponent agreementComponent;
	
	private ContextTypeCodeList contextId; //To Check

	public static final Logger LOGGER = LoggerFactory
			.getLogger(LifeAsiaRepository.class);

	/**
	 * Persist BPMRequest
	 * 
	 * @param request
	 * @return
	 */
	public LifeAsiaRequest save(LifeAsiaRequest request) {

		Response<LifeAsiaRequest> response = null;
		LOGGER.debug("On Life-Asia Repo : saving request ");
		if (request.getCreationDate() == null) {
			request.setCreationDate(new Date());
		}

		Request<LifeAsiaRequest> lifeAsiaRequest = new JPARequestImpl<LifeAsiaRequest>(
				entityManager, null, null);
		lifeAsiaRequest.setType(request);
		if (request.getId() == null) {
			response = lifeAsiaComponent.save(lifeAsiaRequest);
		} else {
			response = lifeAsiaComponent.merge(lifeAsiaRequest);
		}
		entityManager.flush();
		return response.getType();
	}

	/**
	 * Get BPMRequest Data with spajNo
	 * 
	 * @param spajNo
	 * @return
	 */
	public LifeAsiaRequest getRequestData(String spajNo) {
		Request<String> bpmRequest = new JPARequestImpl<String>(entityManager,
				null, null);
		bpmRequest.setType(spajNo);
		Response<LifeAsiaRequest> response = lifeAsiaComponent.getLifeAsiaRequest(bpmRequest);
		return response.getType();
	}

	/**
	 * 
	 * @param request
	 * @return
	 */

	public List<LifeAsiaRequest> getTransaction(LifeAsiaSearchCriteria request) {
		Request<LifeAsiaSearchCriteria> bpmRequest = new JPARequestImpl<LifeAsiaSearchCriteria>(
				entityManager, null, null);
		bpmRequest.setType(request);
		Response<List<LifeAsiaRequest>> response = lifeAsiaComponent
				.getLifeAsiaRequests(bpmRequest);
		return response.getType();
	}

	public List<Number> getFailedTransactionIds() {
		LifeAsiaSearchCriteria criteria = new LifeAsiaSearchCriteria();
		List<String> statuses = new ArrayList<String>();
		statuses.add(GeneraliConstants.FAILED);
		criteria.setStatuses(statuses);
		Request<LifeAsiaSearchCriteria> bpmRequest = new JPARequestImpl<LifeAsiaSearchCriteria>(
				entityManager, null, null);
		bpmRequest.setType(criteria);
		Response<List<Number>> response = lifeAsiaComponent
				.getLifeAsiaRequestsIds(bpmRequest);
		return response.getType();
	}

	public boolean isExistingApplication(String eAppId) {
		boolean isExisitng = false;
		LifeAsiaSearchCriteria criteria = new LifeAsiaSearchCriteria();
		criteria.seteAppId(eAppId);
		Request<LifeAsiaSearchCriteria> lifeAsiaRequest = new JPARequestImpl<LifeAsiaSearchCriteria>(
				entityManager, null, null);
		lifeAsiaRequest.setType(criteria);
		Response<List<Number>> response = lifeAsiaComponent
				.getLifeAsiaRequestsIds(lifeAsiaRequest);
		if (response.getType() != null && !response.getType().isEmpty()) {
			isExisitng = true;
		}
		return isExisitng;
	}

	public LifeAsiaMetaData getLifeAsiaMetaData(String typeCode) {
		Request<String> bpmMetaRequest = new JPARequestImpl<String>(
				entityManager, null, null);
		bpmMetaRequest.setType(typeCode);
		Response<LifeAsiaMetaData> response = lifeAsiaComponent
				.getLifeAsiaMetaData(bpmMetaRequest);
		return response.getType();
	}

	public void saveMetaDate(LifeAsiaMetaData metaData) {
		Request<LifeAsiaMetaData> metaRequest = new JPARequestImpl<LifeAsiaMetaData>(
				entityManager, null, null);
		metaRequest.setType(metaData);
		lifeAsiaComponent.saveMetaData(metaRequest);
	}

	public String getAppIdfromDocId(String bpmDocId) {
		LifeAsiaSearchCriteria criteria = new LifeAsiaSearchCriteria();
		criteria.setSpajNo(bpmDocId);
		Request<LifeAsiaSearchCriteria> bpmRequest = new JPARequestImpl<LifeAsiaSearchCriteria>(
				entityManager, null, null);
		bpmRequest.setType(criteria);
		Response<String> response = lifeAsiaComponent.getEappId(bpmRequest);
		return response.getType();
	}

	public boolean updateAgreementWithStauts(String status, String identifier,
			String pendingInfo, String docId, String checkDevice) {
		List<AgreementStatusCodeList> statusCodeLists = new ArrayList<AgreementStatusCodeList>();
		statusCodeLists.add(AgreementStatusCodeList.Submitted);
		

		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				entityManager, ContextTypeCodeList.EAPP, "");
		statusRequest.setType(statusCodeLists);
		
		final Request<Agreement> agreementRequest = new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, "");
		final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
		insuranceAgreement.setIdentifier(identifier);
		agreementRequest.setType(insuranceAgreement);
		final Response<Agreement> agreementsResponse = agreementComponent.getInsuranceAgreement(agreementRequest, statusRequest);

		
		//Fix for Not showing the Branch Admin Records After Submit -- Start
		if(checkDevice != null && checkDevice.equals("byWeb")) {
		List<AgreementStatusCodeList> statusListPending = new ArrayList<AgreementStatusCodeList>();
		statusListPending.add(AgreementStatusCodeList.PendingSubmission);
		
		final Request<List<AgreementStatusCodeList>> statusRequestPending = new JPARequestImpl<List<AgreementStatusCodeList>>(
				entityManager, ContextTypeCodeList.EAPP, "");
		statusRequestPending.setType(statusListPending);
		
		final Response<Agreement> agreementsResponsePending = agreementComponent.getInsuranceAgreementPending(agreementRequest, statusRequestPending);
		
		if(agreementsResponsePending!=null && agreementsResponsePending.getType()!=null){
			Agreement agreement=agreementsResponsePending.getType();
			if((agreement.getTransactionKeys().getKey37()!=null && agreement.getTransactionKeys().getKey37()!="") || (agreement.getTransactionKeys().getKey36()!=null && agreement.getTransactionKeys().getKey36()!="") ||
					(agreement.getTransactionKeys().getKey35()!=null && agreement.getTransactionKeys().getKey35()!="")) {
				agreement.getTransactionKeys().setKey37("");
				agreement.getTransactionKeys().setKey36("");
				agreement.getTransactionKeys().setKey35("");
				agreementRequest.setType(agreement);
				agreementComponent.mergeAgreement(agreementRequest);
				LOGGER.info("Updated the Branch Admin Key37 as empty for Pending submission");
			}
		}
		}
		//Fix for Not showing the Branch Admin Records After Submit -- End
		
		
		
		if(agreementsResponse!=null && agreementsResponse.getType()!=null){
			boolean needToUpdateRecord=false;
			
			Agreement agreement=agreementsResponse.getType();
				//Check Status is same as DB
			if(agreement.getAgreementExtension().getBpmStatus()==null ||(agreement.getAgreementExtension().getBpmStatus()!=null && status!=null && 
					!status.trim().equalsIgnoreCase(agreement.getAgreementExtension().getBpmStatus().trim()))){
				needToUpdateRecord=true;
			}
			//Check remark same as DB
			if(agreement.getAgreementExtension().getBpmPendingInfo()==null ||(agreement.getAgreementExtension().getBpmPendingInfo()!=null && 
					pendingInfo!=null &&  !pendingInfo.trim().equals(agreement.getAgreementExtension().getBpmPendingInfo().trim()))){
				needToUpdateRecord=true;
			}
			if(needToUpdateRecord){
				agreement.getAgreementExtension().setBpmPendingInfo(pendingInfo);
				agreement.getAgreementExtension().setBpmStatus(status);
				try {
					Date date =LifeEngageComponentHelper.getCurrentdate();
					agreement.setModifiedDateTime(date);
					agreement.setCreationDateTime(date);
					SimpleDateFormat dateFormat = new SimpleDateFormat(LifeEngageComponentHelper.DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
					agreement.getTransactionKeys().setKey22(dateFormat.format(date));// Listing Screen date 
					agreement.getTransactionKeys().setKey30("0");// Setting default memo count as 0
				} catch (ParseException e) {
					LOGGER.error("Failed to parse date");
				}
				agreementRequest.setType(agreement);
				agreementComponent.mergeAgreement(agreementRequest);
				LOGGER.info("Updated application status of  "+identifier+" with  status, info  :- "
						+", "+status+", "+pendingInfo);
				return true;
		}
				
		}

		return false;
	}
	
	public List<String> getSubmittedProposals(String agentId,List<String> status){
		Request<String> agentRequest=new JPARequestImpl<String>(entityManager, null, null);
		Request<List<String>> statusRequest=new JPARequestImpl<List<String>>(entityManager, null, null);
		agentRequest.setType(agentId);
		statusRequest.setType(status);
		Response<List<String>> response=lifeAsiaComponent.getProposalsSubmitted(agentRequest, statusRequest);
		return response.getType();
	}
	
    public List<String> getFailedSPAJs() {
        LifeAsiaSearchCriteria criteria = new LifeAsiaSearchCriteria();
        Request<LifeAsiaSearchCriteria> bpmRequest = new JPARequestImpl<LifeAsiaSearchCriteria>(entityManager, null, null);
        bpmRequest.setType(criteria);
        Response<List<String>> response = lifeAsiaComponent.getFailedSPAJs(bpmRequest,failedSpajMinute);
        return response.getType();
    }
    public List<String> getFailedSPAJsFromOmni() {
        LifeAsiaSearchCriteria criteria = new LifeAsiaSearchCriteria();
        Request<LifeAsiaSearchCriteria> bpmRequest = new JPARequestImpl<LifeAsiaSearchCriteria>(entityManager, null, null);
        bpmRequest.setType(criteria);
        Response<List<String>> response = lifeAsiaComponent.getFailedSPAJsFromOmni(bpmRequest);
        return response.getType();
    }
    
	public Response<List<AppianMemoDetails>> retrieveMemoDetails(Transactions transactions, String type) {

		Request<Transactions> request = new JPARequestImpl<Transactions>(entityManager, contextId, "");
		request.setType(transactions);
		Response<List<AppianMemoDetails>> response = lifeAsiaComponent.retrieveMemoDetails(request,type);
		if (response != null && response.getType() != null) {
            for (AppianMemoDetails memo : response.getType()) {
                if (memo != null) {
                    String dynamicDesc = StringUtils.EMPTY;
                    String ststicDesc = StringUtils.EMPTY;
                    if(memo.getMemoDesc()!=null){
                        dynamicDesc = memo.getRemarks();
                    }
                    if(memo.getMemoDetails().getMemoDescription()!=null){
                        ststicDesc = memo.getMemoDetails().getMemoDescription();
                    }
                    memo.setMemoDesc(StringUtils.isEmpty(dynamicDesc)?ststicDesc:ststicDesc.concat(" + ").concat(dynamicDesc));
                }
            }
        }
        
		return response;
	}
    
	public void updateMemoDetails(AppianMemoDetails userMemoDetails, String type) {

		Request<AppianMemoDetails> request = new JPARequestImpl<AppianMemoDetails>(entityManager, contextId, "");
		request.setType(userMemoDetails);
		lifeAsiaComponent.updateMemoDetails(request, type);
	}

	public UnderWriterMemoDetails getMemoDescription(String memoCode) {
		// TODO Auto-generated method stub
		Request<String> request = new JPARequestImpl<String>(entityManager, contextId, "");
		request.setType(memoCode);
		return lifeAsiaComponent.getMemoDescription(request);
	}

    public void updateLifeAsisRqStatus(String appNumber, String appStatus) {
        LifeAsiaSearchCriteria lifeAsiaSearchCriteria = new LifeAsiaSearchCriteria();
        lifeAsiaSearchCriteria.setSpajNo(appNumber);
        Request<LifeAsiaSearchCriteria> request = new JPARequestImpl<LifeAsiaSearchCriteria>(entityManager, contextId, "");
        request.setType(lifeAsiaSearchCriteria);
        Response<LifeAsiaRequest> response = lifeAsiaComponent.getLifeAsiaRequestByCriteria(request);
        LifeAsiaRequest lifeAsiaRq = response.getType();
        if(lifeAsiaRq != null){
            lifeAsiaRq.setStatus(appStatus);
            save(lifeAsiaRq); 
        }
        
    }

    /*public Map<String, UnderWriterMemoDetails> getUnderWriterMemoDetails(Set<String> memoCodeSet) {
        Request<Set<String>> request = new JPARequestImpl<Set<String>>(entityManager, contextId, "");
        request.setType(memoCodeSet);
        return lifeAsiaComponent.getUnderWriterMemoDetails(request);
    }*/

	public void updateAppainStatus(LifeAsiaRequest lifeAsiaRequest) {
		// TODO Auto-generated method stub
		Request<LifeAsiaRequest> request = new JPARequestImpl<LifeAsiaRequest>(entityManager, contextId, "");
		request.setType(lifeAsiaRequest);
		lifeAsiaComponent.updateAppainStatus(request);
	}

    @Transactional(rollbackFor = { Exception.class }, readOnly = true)
    public List<AddressTranslation> getTranslatedAddress(AddressTranslation addressTransaction) {
        Request<AddressTranslation> request = new JPARequestImpl<AddressTranslation>(entityManager, contextId, "");
        request.setType(addressTransaction);
        Response<List<AddressTranslation>> response = lifeAsiaComponent.getTranslatedAddress(request);
        return response.getType();
    }

}
