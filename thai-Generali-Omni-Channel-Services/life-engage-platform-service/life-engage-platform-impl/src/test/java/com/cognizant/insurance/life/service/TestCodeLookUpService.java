/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Mar 14, 2013
 */
package com.cognizant.insurance.life.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.services.CodeLookUpService;



@ContextConfiguration("/service-context-test.xml")
public class TestCodeLookUpService  extends AbstractJUnit4SpringContextTests {
    
    /** The look upservice. */
    @Autowired
    private CodeLookUpService lookUpservice;
    
    /** The data source. */
    @Autowired
    private  DataSource dataSource;

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";

    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";

    
    @Test
    public void getLookUpDataWithoutCode() throws BusinessException, IOException, ParseException {
        String responseJSON =
            lookUpservice.getLookUpData((getRequest(
                    "codeLookUpRequest/codeLookUpServiceRq.json")));
        // System.out.println(responseJSON);
        assertNotNull(responseJSON);
        JSONObject jsonObject = new JSONObject(responseJSON);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals("IND", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(0).getString("code"));
        assertEquals("Indien", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(0).getString("value"));
        assertEquals("USA", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(1).getString("code"));
        assertEquals("USA", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(1).getString("value"));
        assertEquals("DUB", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(2).getString("code"));
        assertEquals("Dubai", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(2).getString("value"));
    }

    @Test
    public void getLookUpDataWithCode() throws BusinessException, IOException, ParseException {
        String responseJSON =
            lookUpservice.getLookUpData((getRequest(
                    "codeLookUpRequest/codeLookUpWithCodeServiceRq.json")));
        // System.out.println(responseJSON);
        assertNotNull(responseJSON);
        JSONObject jsonObject = new JSONObject(responseJSON);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals("KL", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(0).getString("code"));
        assertEquals("Kerala", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(0).getString("value"));
        assertEquals("MU", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(1).getString("code"));
        assertEquals("Mumbai", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(1).getString("value"));
        assertEquals("KA", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(2).getString("code"));
        assertEquals("Karnataka", jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").getJSONObject(2).getString("value"));
    }
    
    @Test (expected = BusinessException.class)  
    public void getLookUpDataWithoutDataFileds() throws IOException, SystemException, BusinessException {
        String responseJSON =
            lookUpservice.getLookUpData((getRequest(
                    "codeLookUpRequest/codeLookUpWithNoInputTagsServiceRq.json")));
        // System.out.println(responseJSON);
        assertNotNull(responseJSON);
    }
    
    @Test
    public void getNoLookUpData() throws BusinessException, IOException, ParseException {
        String responseJSON =
            lookUpservice.getLookUpData((getRequest(
                    "codeLookUpRequest/codeLookUpWithCodeNoDateServiceRq.json")));
        // System.out.println(responseJSON);
        assertNotNull(responseJSON);
        JSONObject jsonObject = new JSONObject(responseJSON);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(0, jsonObj.getJSONObject("TransactionData").getJSONArray("lookUps").length());
    }
    
    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(final String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(line);
        }

        return sb.toString();
    }
    
    @Before
    public final void setUp(){        
         try{
                    
                QueryRunner queryRunner = new QueryRunner(dataSource);
     
                queryRunner.update("DELETE FROM CODE_RELATION;");
                queryRunner.update("DELETE FROM CODE_LOCALE_LOOKUP;");
                queryRunner.update("DELETE FROM CODE_LOOKUP;");
                queryRunner.update("DELETE FROM CODE_TYPE_LOOKUP;");
                queryRunner.update("DELETE FROM CORE_CARRIER;");
                
                queryRunner.update("INSERT INTO CORE_CARRIER(id, DESCRIPTION)VALUES (1, 'ABC insurance Company');");
                queryRunner.update("insert into `CODE_TYPE_LOOKUP`(`id`,`NAME`,`CARRIER_ID`) values (1,'COUNTRY_LIST',1),(2,'STATE_LIST',1), (3,'COUNTRY_STATE',1);");
                
                queryRunner.update("insert into `CODE_LOOKUP`(`id`,`CODE`,`DESCRIPTION`,`CODETYPE_ID`) values (1,'IND','India',1), (2,'USA','USA',1), (3,'DUB','Dubai',1);");
                queryRunner.update("insert into `CODE_LOOKUP`(`id`,`CODE`,`DESCRIPTION`,`CODETYPE_ID`) values (4,'KL','Kerala',2), (5,'MU','Mumbai',2), (6,'KA','Karnataka',2);");
                queryRunner.update("insert into `CODE_LOCALE_LOOKUP`(`id`,`COUNTRY`,`LANGUAGE`,`VALUE`,`LOOKUPCODE_ID`) values (1,'US','en','India',1), (2,'US','en','USA',2), (3,'US','en','Dubai',3);");
                queryRunner.update("insert into `CODE_LOCALE_LOOKUP`(`id`,`COUNTRY`,`LANGUAGE`,`VALUE`,`LOOKUPCODE_ID`) values (4,'DE','de','Indien',1) , (5,'DE','de','USA',2), (6,'DE','de','Dubai',3), (8,'US','en','Kerala',4);");
                queryRunner.update("insert into `CODE_LOCALE_LOOKUP`(`id`,`COUNTRY`,`LANGUAGE`,`VALUE`,`LOOKUPCODE_ID`) values (9,'US','en','Mumbai',5), (10,'US','en','Karnataka',6);");
                
                queryRunner.update("insert into `CODE_RELATION`(`id`,`CHILD_ID`,`PARENT_ID`,`TYPE_ID`) values (1,4,1,3), (2,5,1,3), (3,6,1,3);");
                
         } catch (Exception e) {
             e.printStackTrace();
         }
    }
    
    @After
    public void cleanup() {
        try {
            QueryRunner queryRunner = new QueryRunner(dataSource);
            queryRunner.update("DELETE FROM CODE_RELATION;");
            queryRunner.update("DELETE FROM CODE_LOCALE_LOOKUP;");
            queryRunner.update("DELETE FROM CODE_LOOKUP;");
            queryRunner.update("DELETE FROM CODE_TYPE_LOOKUP;");
            queryRunner.update("DELETE FROM CORE_CARRIER;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
