/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.placesubtypes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;

import com.cognizant.insurance.domain.contactandplace.contactcodelists.AdministrativeSubDivisionCodeList;

/**
 * This concept represents a city.
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("CITY")
public class City extends CountryElement {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2598992660461050572L;
    /**
     * A code indicating the nature of the city with regards to government
     * administrative functions, if applicable.
     * 
     * 
     * 
     */
    @Enumerated
    private AdministrativeSubDivisionCodeList administrativeSubDivisionCode;

    /**
     * @return the administrativeSubDivisionCode
     * 
     * 
     */
    public AdministrativeSubDivisionCodeList
            getAdministrativeSubDivisionCode() {
        return administrativeSubDivisionCode;
    }

    /**
     * @param administrativeSubDivisionCode
     *            the administrativeSubDivisionCode to set
     * 
     * 
     */
    public
            void
            setAdministrativeSubDivisionCode(
                    final AdministrativeSubDivisionCodeList administrativeSubDivisionCode) {
        this.administrativeSubDivisionCode = administrativeSubDivisionCode;
    }
}
