--LMS Data Extraction Query . Version 1.1
select pr.id as ID ,pr.identifier,CONVERT(VARCHAR(10),pr.creationDateTime,103) as CreatedDate,
CONVERT(char(5), pr.creationDateTime, 108) as CreatedTime,
CONVERT(VARCHAR(10),pr.modifiedDateTime,103) as LastEditedDate,
CONVERT(char(5), pr.modifiedDateTime, 108) as LastEditTime,
pr.agentId as AgentCode,
ISNULL(pr.branchId,'') as BranchCode,
CASE WHEN pr.branchId IS NOT NULL and pr.branchId<>''
THEN 'Assigned By Branch'
ELSE 'Self Created'
END as TypeOfLead,
tk.key2 as FullName,
CASE WHEN per.genderCode=0 THEN 'Female'
	 WHEN per.genderCode=1 THEN 'Male'
	 WHEN per.genderCode=2 THEN 'Unknown'
	 WHEN per.genderCode=3 THEN 'NotMentioned'
END as Gender,
ISNULL(per.identityProof,'') as NationalId,
tk.key4 as MobileNumber,
temp_email.EmailAddress as EmailAddress,
temp_address.Address as Address,
ISNULL(per.birthDate,'') as DateOfBirth,
CASE WHEN pr.importanceLevelCode=5 THEN 'Warm'
	 WHEN pr.importanceLevelCode=6 THEN 'Hot'
	 WHEN pr.importanceLevelCode=7 THEN 'Cold'
	 WHEN pr.importanceLevelCode=4 THEN 'NotMentioned'
END as Potential ,
temp_AnualIncome.AnualIncome as AnualIncome,
CASE WHEN pr.Need=0 THEN 'Education'
	 WHEN pr.Need=1 THEN 'Retirement'
	 WHEN pr.Need=2 THEN 'Investment'
	 WHEN pr.Need=3 THEN 'Health'
	 WHEN pr.Need=4 THEN 'Other'
	 WHEN pr.Need=5 THEN 'NotMentioned'
END as Need,
temp_state.State as State,
temp_Nationality.Nationality as Nationality,
temp_Occupation.Occupation as Occupation,
CASE WHEN pr.source=33 THEN 'Walk in'
	 WHEN pr.source=34 THEN 'Event/GP'
	 WHEN pr.source=35 THEN 'Marketing Campaign'
	 WHEN pr.source=36 THEN 'Friend/Family'
	 WHEN pr.source=37 THEN 'Referral From Branch'
	 WHEN pr.source=27 THEN 'NotMentioned'
	 WHEN pr.source=5 THEN 'NaturalMarket'
END as Source,
temp_referrer.ReferrerBranch as ReferrerBranch,
temp_referrer.ReferrerName as ReferrerName,
temp_referrer.NationalIDReferrer as NationlaIdofReferrer,
ISNULL(temp_referrer_contact.fullNumber,'') as ReferrerMobileNumber,
ISNULL(CONVERT(VARCHAR(10),temp_meeting_Date.ActualDateAndTime,103),'') as ActualMeetingDate,
ISNULL(CONVERT(char(5), temp_meeting_Date.ActualDateAndTime, 108),'') as ActualMeetingTime,
ISNULL(temp_Contact_log.InteractionType,'') as InteractionType,
CASE WHEN pr.action=0 THEN 'Non Contactable'
	 WHEN pr.action=1 THEN 'Meeting Fixed'
	 WHEN pr.action=2 THEN 'Successful Sale'
	 WHEN pr.action=3 THEN 'To Be Called'
	 WHEN pr.action=4 THEN 'Contacted And Not Interested'
	 WHEN pr.action=5 THEN 'Interested'
	 WHEN pr.action=6 THEN 'App Submitted'
	 WHEN pr.action=7 THEN 'Pending Documents'
	 WHEN pr.action=8 THEN 'Others'
	 WHEN pr.action=9 THEN 'NotMentioned'
END as Action,
temp_Contact_log.Remarks as Remarks,
CASE WHEN pr.statusCode=22 THEN 'Non Contactable'
	 WHEN pr.statusCode=23 THEN 'Meeting Fixed'
	 WHEN pr.statusCode=24 THEN 'Successful Sale'
	 WHEN pr.statusCode=25 THEN 'To Be Called'
	 WHEN pr.statusCode=26 THEN 'Contacted And Not Interested'
	 WHEN pr.statusCode=14 THEN 'Interested'
	 WHEN pr.statusCode=27 THEN 'App Submitted'
	 WHEN pr.statusCode=28 THEN 'Pending Documents'
	 WHEN pr.statusCode=29 THEN 'Others'
	 WHEN pr.statusCode=18 THEN 'Suspended'
	 WHEN pr.statusCode=20 THEN 'New'
END as Status,

ISNULL(CONVERT(varchar(50),temp_vnd.VNDAPE),'') as VNDAPE,
ISNULL(temp_vnd.PolicyNo,'') as PolicyNo,
ISNULL(temp_vnd.SubmitDate,'') as ExpectedSubmittedDate,
ISNULL(CONVERT(VARCHAR(10),temp_meeting_Date.ActualDateAndTime,103),'') as NextMeetingDate,
ISNULL(CONVERT(char(5), temp_meeting_Date.ActualDateAndTime, 108),'') as NextMeetingTime,
temp_referrer.ReferrerName as AddRefFullName,
ISNULL(temp_referrer_contact.fullNumber,'') as AddRefMobileNumber,
ISNULL(temp_Q1.Q1,'') as FeedBackQ1,
CASE WHEN temp_Q2.Q2=0 THEN 'No'
	 WHEN temp_Q2.Q2=1 THEN 'Yes'
ELSE ''
END as FeedBackQ2,
ISNULL(temp_Q3.Q3,'') as FeedBackQ3,
temp_BranchDetails.BranchUserName as BranchUserName,
temp_BranchDetails.BranchUserNationalID as BranchUserNationalID,
temp_Branch_Number.BranchfullNumber as BranchMobileNumber

from PartyRoleInRelationship as pr with (nolock) INNER JOIN TransactionKeys as tk with (nolock) ON (pr.transactionKeys_id=tk.id) 
INNER JOIN Party as prt with (nolock) ON (pr.playerParty_id=prt.id) 
INNER JOIN Person as per with (nolock) ON (prt.id=per.id) 
LEFT JOIN 
( select pr.id as custId,ISNULL(ele_cnt.emailAddress,'') as EmailAddress from PartyRoleInRelationship as pr with (nolock)  
INNER JOIN Party as prt with (nolock) ON (pr.playerParty_id=prt.id) 
INNER JOIN Person as per with (nolock) ON (prt.id=per.id) 
INNER JOIN Party_ContactPreference as part_con with (nolock) ON (prt.id=part_con.Party_id) 
INNER JOIN ContactPreference as cont_prf with (nolock) ON (part_con.preferredContact_id=cont_prf.id)
INNER JOIN ContactPoint as cont_pnt with (nolock) ON (cont_prf.preferredContactPoint_id=cont_pnt.id) 
INNER JOIN ElectronicContact as ele_cnt with (nolock) ON (cont_pnt.id=ele_cnt.id) 
where pr.statusCode not in ('19')) temp_email ON (pr.id=temp_email.custId )
LEFT JOIN ( select distinct pr.id as custId,ISNULL(PostAddr.addressLine1,'') as Address from PartyRoleInRelationship as pr with (nolock)  
INNER JOIN Party as prt with (nolock) ON (pr.playerParty_id=prt.id) 
INNER JOIN Person as per with (nolock) ON (prt.id=per.id) 
INNER JOIN Party_ContactPreference as part_con with (nolock) ON (prt.id=part_con.Party_id) 
INNER JOIN ContactPreference as cont_prf with (nolock) ON (part_con.preferredContact_id=cont_prf.id)
INNER JOIN ContactPoint as cont_pnt with (nolock) ON (cont_prf.preferredContactPoint_id=cont_pnt.id) 
INNER JOIN PostalAddressContact as PostAddr with (nolock) ON (cont_pnt.id=PostAddr.id)
where pr.statusCode not in ('19') ) temp_address ON (pr.id=temp_address.custId )
LEFT JOIN
(select pr.id as custId,ISNULL(ped.description,'') as Occupation from PartyRoleInRelationship as pr with (nolock)  
INNER JOIN Party as prt with (nolock) ON (pr.playerParty_id=prt.id) 
INNER JOIN Person as per with (nolock) ON (prt.id=per.id) 
INNER JOIN Person_PersonDetail as Per_det with (nolock) ON (per.id=Per_det.Person_id)
INNER JOIN PersonDetail as ped with (nolock) ON (Per_det.detail_id=ped.id)
where pr.statusCode not in ('19') and ped.person_detail_type='OCCUPATIONDETAIL') temp_Occupation ON (pr.id=temp_Occupation.custId )
LEFT JOIN 
(select pr.id as custId,ISNULL(replace(replace(ped.grossIncome, '&lt;', '<'), '&gt;', '>'),'') as AnualIncome
from PartyRoleInRelationship as pr with (nolock)  
INNER JOIN Party as prt with (nolock) ON (pr.playerParty_id=prt.id) 
INNER JOIN Person as per with (nolock) ON (prt.id=per.id) 
INNER JOIN Person_PersonDetail as Per_det with (nolock) ON (per.id=Per_det.Person_id)
INNER JOIN PersonDetail as ped with (nolock) ON (Per_det.detail_id=ped.id)
where pr.statusCode not in ('19') and ped.person_detail_type='INCOMEDETAIL') temp_AnualIncome ON (pr.id=temp_AnualIncome.custId )
LEFT JOIN 
(select pr.id as custId,ISNULL(pl.name,'') as State
from PartyRoleInRelationship as pr with (nolock)  
INNER JOIN Party as prt with (nolock) ON (pr.playerParty_id=prt.id) 
INNER JOIN Party_ContactPreference as part_con with (nolock) ON (prt.id=part_con.Party_id) 
INNER JOIN ContactPreference as cont_prf with (nolock) ON (part_con.preferredContact_id=cont_prf.id)
INNER JOIN ContactPoint as cont_pnt with (nolock) ON (cont_prf.preferredContactPoint_id=cont_pnt.id) 
INNER JOIN PostalAddressContact as PostAddr with (nolock) ON (cont_pnt.id=PostAddr.id)
INNER JOIN Place as pl with (nolock) ON (PostAddr.postalCountrySubdivision_id=pl.id)
where pr.statusCode not in ('19') and pl.place_type='COUNTRY_SUBDIV')  temp_state ON (pr.id=temp_state.custId )
LEFT JOIN 
(select pr.id as custId,ISNULL(pl.name,'') as Nationality
from PartyRoleInRelationship as pr with (nolock) 
INNER JOIN Party as prt with (nolock) ON (pr.playerParty_id=prt.id) 
INNER JOIN Person as per with (nolock) ON (prt.id=per.id) 
INNER JOIN Person_NationalityCountry as pe_nat with (nolock) ON (pe_nat.person_Id=per.id)
INNER JOIN Place as pl with (nolock) ON (pe_nat.Country_Id=pl.id)
where pr.statusCode not in ('19') and  pl.place_type='COUNTRY') temp_Nationality ON (pr.id=temp_Nationality.custId )
LEFT JOIN
( select pr.id as custId,ISNULL(related.branchName,'') as ReferrerBranch,ISNULL(pname.givenName,'') as ReferrerName,
ISNULL(per_rel.identityProof,'') as NationalIDReferrer
from PartyRoleInRelationship as pr with (nolock)
INNER JOIN PartyRoleRelationship as PRR with (nolock) ON (pr.customerRelationship_id=PRR.id) 
INNER JOIN PartyRoleInRelationship as related with (nolock) ON (PRR.relatedCustomer_id=related.id)
INNER JOIN Party as prt_rel with (nolock) ON (related.playerParty_id=prt_rel.id) 
INNER JOIN Person as per_rel with (nolock) ON (prt_rel.id=per_rel.id)
INNER JOIN Person_PartyName as per_partn with (nolock) ON (per_partn.Person_id=per_rel.id)
INNER JOIN PartyName as pname with (nolock) ON (per_partn.name_id=pname.id)
where pr.statusCode not in ('19')	) temp_referrer ON (pr.id=temp_referrer.custId)
LEFT JOIN 
(select pr.id as custId,ISNULL(tc.fullNumber,'') as fullNumber
from PartyRoleInRelationship as pr with (nolock)
INNER JOIN PartyRoleRelationship as PRR with (nolock) ON (pr.customerRelationship_id=PRR.id) 
INNER JOIN PartyRoleInRelationship as related with (nolock) ON (PRR.relatedCustomer_id=related.id)
JOIN Party as prt_rel with (nolock) ON (related.playerParty_id=prt_rel.id) 
INNER JOIN Party_ContactPreference as part_con with (nolock) ON (prt_rel.id=part_con.Party_id) 
INNER JOIN ContactPreference as cont_prf with (nolock) ON (part_con.preferredContact_id=cont_prf.id)
INNER JOIN ContactPoint as cont_pnt with (nolock) ON (cont_prf.preferredContactPoint_id=cont_pnt.id)
INNER JOIN TelephoneCallContact as tc with (nolock) ON (cont_pnt.id=tc.id)
where pr.statusCode not in ('19')	) temp_referrer_contact ON (pr.id=temp_referrer_contact.custId)
LEFT JOIN 
(select pr.id as custId,
CASE WHEN com.interactionType=0 THEN 'Face To Face'
WHEN com.interactionType=1 THEN 'Phone Call'
WHEN com.interactionType=2 THEN 'Email'
WHEN com.interactionType=3 THEN 'Others'
WHEN com.interactionType=4 THEN 'NotMentioned'		 
END as InteractionType,
ISNULL(com.communicationDescription,'') as Remarks

from PartyRoleInRelationship as pr with (nolock) 
INNER JOIN PartyRoleInRelationship_Communication as pc with (nolock) ON (pr.id=pc.PartyRoleInRelationship_id)
INNER JOIN Communication as com with (nolock) ON (pc.receivesCommunication_id=com.id)
where pr.statusCode not in ('19')) temp_Contact_log ON (pr.id=temp_Contact_log.custId)
LEFT JOIN 
(select pr.id as custId,max(conf.actualDateAndTime) as ActualDateAndTime from PartyRoleInRelationship as pr with (nolock) 
INNER JOIN PartyRoleInRelationship_Communication as pc with (nolock) ON (pr.id=pc.PartyRoleInRelationship_id)
INNER JOIN Communication as com with (nolock) ON (pc.receivesCommunication_id=com.id)
INNER JOIN Communication_Communication as cc with (nolock) ON (com.id=cc.Communication_id)
INNER JOIN Communication as conf with (nolock) ON (cc.followsUp_id=conf.id)
where pr.statusCode not in ('19') group by pr.id ) temp_meeting_Date ON (pr.id=temp_meeting_Date.custId)
LEFT JOIN 
(select pr.id as custId,pp.proposalNumber as PolicyNo,pp.submissionDate as SubmitDate,ca.amount as VNDAPE from PartyRoleInRelationship as pr with (nolock) 
INNER JOIN PartyRoleInRelationship_PurchasedProduct as prpd with (nolock) ON (pr.id=prpd.PartyRoleInRelationship_id)
INNER JOIN PurchasedProduct as PP with (nolock) ON (prpd.purchasedProducts_id=PP.id)
INNER JOIN CurrencyAmount as ca with (nolock) ON (pp.premium_id=ca.id)
where pr.statusCode not in ('19')) temp_vnd ON (pr.id=temp_vnd.custId)
LEFT JOIN
(
select pr.id as custId, sp.questionId,ISNULL(sp.selectedOption,'') as Q1 from PartyRoleInRelationship as pr with (nolock) 
INNER JOIN PartyRoleInRelationship_Specification as ps with (nolock) ON (pr.id=ps.PartyRoleInRelationship_id)	
INNER JOIN Specification as sp with (nolock) ON (ps.questionnaire_id=sp.id)	
where pr.statusCode not in ('19') and sp.questionId=1 ) temp_Q1 ON (pr.id=temp_Q1.custId)
LEFT JOIN
(select pr.id as custId, sp.questionId,ISNULL(sp.selectedOption,'') as Q2 from PartyRoleInRelationship as pr with (nolock) 
INNER JOIN PartyRoleInRelationship_Specification as ps with (nolock) ON (pr.id=ps.PartyRoleInRelationship_id)	
INNER JOIN Specification as sp with (nolock) ON (ps.questionnaire_id=sp.id)	
where pr.statusCode not in ('19') and sp.questionId=2) temp_Q2 ON (pr.id=temp_Q2.custId)
LEFT JOIN
(select pr.id as custId, sp.questionId,ISNULL(sp.selectedOption,'') as Q3 from PartyRoleInRelationship as pr with (nolock) 
INNER JOIN PartyRoleInRelationship_Specification as ps with (nolock) ON (pr.id=ps.PartyRoleInRelationship_id)	
INNER JOIN Specification as sp with (nolock) ON (ps.questionnaire_id=sp.id)	
where pr.statusCode not in ('19') and sp.questionId=3) temp_Q3 ON (pr.id=temp_Q3.custId)

LEFT JOIN 
(select pr.id as custId,ISNULL(related.branchName,'') as temp_Branch,ISNULL(pname.givenName,'') as BranchUserName,
ISNULL(per_rel.identityProof,'') as BranchUserNationalID
from PartyRoleInRelationship as pr with (nolock)
INNER JOIN PartyRoleInRelationship_PartyRoleInRelationship as pp with (nolock) ON (pr.id=pp.PartyRoleInRelationship_id)
INNER JOIN PartyRoleInRelationship as related with (nolock) ON (pp.relatedCustomers_id=related.id)
INNER JOIN Party as prt_rel with (nolock) ON (related.playerParty_id=prt_rel.id) 
INNER JOIN Person as per_rel with (nolock) ON (prt_rel.id=per_rel.id)
INNER JOIN Person_PartyName as per_partn with (nolock) ON (per_partn.Person_id=per_rel.id)
INNER JOIN PartyName as pname with (nolock) ON (per_partn.name_id=pname.id) ) temp_BranchDetails ON (pr.id=temp_BranchDetails.custId)

LEFT JOIN 
(select pr.id as custId,ISNULL(tc.fullNumber,'') as BranchfullNumber
from PartyRoleInRelationship as pr with (nolock)
INNER JOIN PartyRoleInRelationship_PartyRoleInRelationship as pp with (nolock) ON (pr.id=pp.PartyRoleInRelationship_id)
INNER JOIN PartyRoleInRelationship as related with (nolock) ON (pp.relatedCustomers_id=related.id)
INNER JOIN Party as prt_rel with (nolock) ON (related.playerParty_id=prt_rel.id) 
INNER JOIN Party_ContactPreference as part_con with (nolock) ON (prt_rel.id=part_con.Party_id) 
INNER JOIN ContactPreference as cont_prf with (nolock) ON (part_con.preferredContact_id=cont_prf.id)
INNER JOIN ContactPoint as cont_pnt with (nolock) ON (cont_prf.preferredContactPoint_id=cont_pnt.id)
INNER JOIN TelephoneCallContact as tc with (nolock) ON (cont_pnt.id=tc.id)) temp_Branch_Number ON (pr.id=temp_Branch_Number.custId)

where pr.statusCode not in ('19') order by pr.creationDateTime



