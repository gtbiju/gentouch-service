/**
 *

 * Copyright 2012, Cognizant
 *
 * @author        : 304000
 * @version       : 0.1, Feb 21, 2014
 */
package com.cognizant.insurance.domain.goalandneed;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;

/**
 * The Class GoalAndNeed.
 */

@Entity
public class GoalAndNeed extends InformationModelObject {

    /** Generated Constant serialVersionUID. */
    private static final long serialVersionUID = -1942023383984821262L;

    /** The identifier. */
    private String identifier;

    /** The user name. */
    private String agentId;

    /** The user email. */
    @Basic(fetch = FetchType.LAZY)
    private String userEmail;

    /** The given name. */
    private String givenName;

    /** The sur name. */
    private String surName;

    /** The birth date. */
    @Basic(fetch = FetchType.LAZY)
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    /** The email address. */
    @Basic(fetch = FetchType.LAZY)
    private String emailAddress;

    /** The contact number. */
    @Basic(fetch = FetchType.LAZY)
    private String contactNumber;

    /** The request json - Transaction Data Contents. */
    @Basic(fetch = FetchType.LAZY)
    @Lob
    private String requestJSON;

    /** The status. */
    @Basic(fetch = FetchType.LAZY)
    private String status;

    /** The product. */
    @Basic(fetch = FetchType.LAZY)
    private String productId;
    
    private String leadId;

    /**
     * Gets the identifier.
     * 
     * @return the identifier
     */
    public final String getIdentifier() {
        return identifier;
    }

    /**
     * Sets the identifier.
     * 
     * @param identifier
     *            the new identifier
     */
    public final void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets the AgentId.
     * 
     * @return the AgentId
     */
    public final String getAgentId() {
        return agentId;
    }

    /**
     * Sets the AgentId.
     * 
     * @param agentId
     *            AgentId
     */
    public final void setAgentId(final String agentId) {
        this.agentId = agentId;
    }

    /**
     * Gets the user email.
     * 
     * @return the user email
     */
    public final String getUserEmail() {
        return userEmail;
    }

    /**
     * Sets the user email.
     * 
     * @param userEmail
     *            the new user email
     */
    public final void setUserEmail(final String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * Gets the given name.
     * 
     * @return the given name
     */
    public final String getGivenName() {
        return givenName;
    }

    /**
     * Sets the given name.
     * 
     * @param givenName
     *            the new given name
     */
    public final void setGivenName(final String givenName) {
        this.givenName = givenName;
    }

    /**
     * Gets the sur name.
     * 
     * @return the sur name
     */
    public final String getSurName() {
        return surName;
    }

    /**
     * Sets the sur name.
     * 
     * @param surName
     *            the new sur name
     */
    public final void setSurName(final String surName) {
        this.surName = surName;
    }

    /**
     * Gets the birth date.
     * 
     * @return the birth date
     */
    public final Date getBirthDate() {
        return birthDate;
    }

    /**
     * Sets the birth date.
     * 
     * @param birthDate
     *            the new birth date
     */
    public final void setBirthDate(final Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Gets the email address.
     * 
     * @return the email address
     */
    public final String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the email address.
     * 
     * @param emailAddress
     *            the new email address
     */
    public final void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Gets the contact number.
     * 
     * @return the contact number
     */
    public final String getContactNumber() {
        return contactNumber;
    }

    /**
     * Sets the contact number.
     * 
     * @param contactNumber
     *            the new contact number
     */
    public final void setContactNumber(final String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**
     * Gets the request json.
     * 
     * @return the request json
     */
    public final String getRequestJSON() {
        return requestJSON;
    }

    /**
     * Sets the request json.
     * 
     * @param requestJSON
     *            the new request json
     */
    public final void setRequestJSON(final String requestJSON) {
        this.requestJSON = requestJSON;
    }

    /**
     * Gets the status.
     * 
     * @return the status
     */
    public final String getStatus() {
        return status;
    }

    /**
     * Sets the status.
     * 
     * @param status
     *            the new status
     */
    public final void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Gets the product id.
     * 
     * @return the product id
     */
    public final String getProductId() {
        return productId;
    }

    /**
     * Sets the product id.
     * 
     * @param productId
     *            the new product id
     */
    public final void setProductId(final String productId) {
        this.productId = productId;
    }

	public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}
    

}
