/**
 * 
 */
package com.cognizant.insurance.omnichannel.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author 397850
 *
 */
@Entity
public class CheckinData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6331970611291658729L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date checkinDateTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date checkoutDateTime;
	
	private String agentId;
	
	private String branchId;
	
	private String agentName;
	
	private String agentType;
	
	private String branchName;
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	
	
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Date getCheckinDateTime() {
		return checkinDateTime;
	}



	public void setCheckinDateTime(Date checkinDateTime) {
		this.checkinDateTime = checkinDateTime;
	}



	public Date getCheckoutDateTime() {
		return checkoutDateTime;
	}



	public void setCheckoutDateTime(Date checkoutDateTime) {
		this.checkoutDateTime = checkoutDateTime;
	}



	public String getAgentId() {
		return agentId;
	}



	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}



	public String getBranchId() {
		return branchId;
	}



	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}



	public String getAgentName() {
		return agentName;
	}



	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}



	public String getAgentType() {
		return agentType;
	}



	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}



	public Status getStatus() {
		return status;
	}



	public void setStatus(Status status) {
		this.status = status;
	}



	public String getBranchName() {
		return branchName;
	}



	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}



	public enum Status{
		checkedin,
		checkedout
			
	}

}
