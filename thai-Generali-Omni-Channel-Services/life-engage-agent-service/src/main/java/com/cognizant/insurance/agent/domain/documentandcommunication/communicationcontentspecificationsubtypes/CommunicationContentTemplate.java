package com.cognizant.insurance.agent.domain.documentandcommunication.communicationcontentspecificationsubtypes;

import javax.persistence.Entity;
import com.cognizant.insurance.agent.domain.documentandcommunication.CommunicationContentSpecification;

@Entity
public class CommunicationContentTemplate extends CommunicationContentSpecification {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2601537824856028113L;
}
