package com.cognizant.insurance.omnichannel.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.component.GeneraliSalesActivityComponent;
import com.cognizant.insurance.omnichannel.service.GeneraliSalesActivityService;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;

/**
 * The class GeneraliSalesActivityServiceImpl
 * 
 * @author 481774
 * 
 */

@Service("gliSalesActivityService")
public class GeneraliSalesActivityServiceImpl implements GeneraliSalesActivityService {

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliSalesActivityServiceImpl.class);

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";

    @Autowired
    private GeneraliSalesActivityComponent generaliSalesActivityComponent;

    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly = true)
    public String retrieveSalesActivityDetails(String json) throws BusinessException {
        LOGGER.trace("Inside GeneraliSalesActivityServiceImpl.retrieveSalesActivityDetails :json " + json);
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        String transactionArray = "{\"Transactions\":[]}";
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);

            transactionArray = generaliSalesActivityComponent.retrieveSalesActivityDetails(requestInfo, jsonRqArray);

            JSONArray jsonRsArray = new JSONArray();
            jsonRsArray.put(new JSONObject(transactionArray));
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("GeneraliSalesActivityServiceImpl.retrieveSalesActivityDetails: Parse Exception"
                    + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        LOGGER.trace("Inside GeneraliSalesActivityServiceImpl retrieveSalesActivityDetails : jsonObjectRs"
                + jsonObjectRs.toString());
        return jsonObjectRs.toString();
    }

}
