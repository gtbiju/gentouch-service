package com.cognizant.insurance.omnichannel.component.repository;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.LMSRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Communication;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationInteractionTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationStatusCodeList;
import com.cognizant.insurance.domain.party.Party;
import com.cognizant.insurance.domain.product.PurchasedProduct;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliPartyComponent;
import com.cognizant.insurance.omnichannel.utils.GeneraliDateUtil;
import com.cognizant.insurance.omnichannel.vo.AgentData;
import com.cognizant.insurance.omnichannel.vo.ManagerialLevelUserDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public class GeneraliLMSRepository extends LMSRepository {

	@Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;
	
    /** The party component. */
    @Autowired
    private GeneraliPartyComponent partyComponent;

    /** The Constant FNA. */
    private static final String FNA = "FNA";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";
    
    /** The Constant illustration. */
    private static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
    
    /** The conflict resolution. */
    @Value("${le.platform.service.conflictResolution}")
    private String conflictResolution;
    
    /** The source of truth. */
    @Value("${le.platform.service.sourceOfTruth}")
    private String sourceOfTruth;
    

    /** The lms limit. */
    @Value("${le.platform.service.lms.fetchLimit}")
    private Integer lmsLimit;
    
    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;
    
    /** The Constant LMS. */
    private static final String LMS = "LMS";
    
    private static final String SUCCESSFUL_SALE="Successful Sale";

    protected boolean isExistingCustomer(Transactions transactions, RequestInfo requestInfo) {
        boolean isDuplicateLeadExists = false;
        
        if(!Constants.RM_REASSIGN.equalsIgnoreCase(transactions.getAllocationType())) {
        	
        	if(!Constants.CANCELLED.equalsIgnoreCase(transactions.getStatus())){
                final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.DUPLICATE_CHECK);
                final Request<List<CustomerStatusCodeList>> statusRequest =
                        new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.LMS,
                                requestInfo.getTransactionId());
                statusRequest.setType(statusList);

                final List<TelephoneTypeCodeList> telephoneTypeList = new ArrayList<TelephoneTypeCodeList>();
                telephoneTypeList.add(TelephoneTypeCodeList.Mobile);

                final Request<List<TelephoneTypeCodeList>> telephoneTypeRequest =
                        new JPARequestImpl<List<TelephoneTypeCodeList>>(getEntityManager(), ContextTypeCodeList.LMS,
                                requestInfo.getTransactionId());
                telephoneTypeRequest.setType(telephoneTypeList);

                final Request<PartyRoleInRelationship> customerRequest =
                        new JPARequestImpl<PartyRoleInRelationship>(getEntityManager(), ContextTypeCodeList.LMS,
                                requestInfo.getTransactionId());
                customerRequest.setType(transactions.getCustomer());

                final Response<List<Customer>> duplicateCustomerResponse =
                        getPartyComponent().retrieveDuplicateLeadList(customerRequest, transactions, statusRequest,
                                telephoneTypeRequest);

				final List<Customer> customers = duplicateCustomerResponse.getType();

				if (CollectionUtils.isEmpty(customers)) {
					isDuplicateLeadExists = false;
				} else {
					int size = customers.size();
					for (Customer customerObj : customers) {
						if (transactions.getKey1().equals(customerObj.getIdentifier())) {
							size = size - 1;
						}
					}
					if (size >= 5) {
						isDuplicateLeadExists = true;
					}
				}
            }        	
        }
        
        return isDuplicateLeadExists;
    }
    
    /**
     * Retrieve by filter lms.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     */
    @Override
    public List<Transactions> retrieveByFilterLMS(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList =getCustomerStatusCodesFor(Constants.RETRIEVE_BY_FILTER);

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Response<List<Customer>> searchCriteriatResponse =
                partyComponent.retrieveFilterCustomers(searchCriteriatRequest, statusRequest);
        final List<Customer> customers = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(customers)) {
            for (Customer customerObj : customers) {
                transaction = new Transactions();
                transaction.setCustomer(customerObj);
                transaction.setCustomerId(customerObj.getIdentifier());
                transaction.setType(searchCriteria.getType());
                if (customerObj.getModifiedDateTime() != null
                        && !(customerObj.getModifiedDateTime().toString().equals(""))) {
                    LOGGER.trace("Inside LMSRepository customerObj.getModifiedDateTime() != null"
                            + customerObj.getModifiedDateTime());
                    transaction.setLastSyncDate(customerObj.getModifiedDateTime());
                    transaction.setModifiedTimeStamp(customerObj.getModifiedDateTime());
                } else if (customerObj.getCreationDateTime() != null
                        && !("".equals(customerObj.getCreationDateTime().toString()))) {
                    LOGGER.trace("Inside LMSRepository getCreationDateTime() != null"
                            + customerObj.getCreationDateTime());
                    transaction.setLastSyncDate(customerObj.getCreationDateTime());
                }
                transactionsList.add(transaction);
            }
        }

        return transactionsList;
    }
    
    
    public Map<String,Integer> retrieveMonthlySalesActivity(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList =getCustomerStatusCodesFor(GeneraliConstants.CALL_OR_VISIT);
        final List<CommunicationStatusCodeList> CommunicationStatusList = getCommunicationStatusCodesForCallVisit();
        final List<CommunicationInteractionTypeCodeList> CommunicationInteractionList = getCommunicationInteractionCodesForCallVisit();

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);
        
        final Request<List<CommunicationStatusCodeList>> communicationStatusRequest =
                new JPARequestImpl<List<CommunicationStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        communicationStatusRequest.setType(CommunicationStatusList);
        
        final Request<List<CommunicationInteractionTypeCodeList>> communicationInteractionCodeRequest =
                new JPARequestImpl<List<CommunicationInteractionTypeCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        communicationInteractionCodeRequest.setType(CommunicationInteractionList);

        final Map<String,Integer> searchCriteriatResponse =
                partyComponent.retrieveMonthlySalesActivityCustomers(searchCriteriatRequest, statusRequest,
                        communicationStatusRequest, communicationInteractionCodeRequest);
        /*final List<Customer> customers = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(customers)) {
            for (Customer customerObj : customers) {
                transaction = new Transactions();
                transaction.setCustomer(customerObj);
                transaction.setCustomerId(customerObj.getIdentifier());
                transaction.setType(searchCriteria.getType());
                if (customerObj.getModifiedDateTime() != null
                        && !(customerObj.getModifiedDateTime().toString().equals(""))) {
                    LOGGER.trace("Inside LMSRepository customerObj.getModifiedDateTime() != null"
                            + customerObj.getModifiedDateTime());
                    transaction.setLastSyncDate(customerObj.getModifiedDateTime());
                    transaction.setModifiedTimeStamp(customerObj.getModifiedDateTime());
                } else if (customerObj.getCreationDateTime() != null
                        && !("".equals(customerObj.getCreationDateTime().toString()))) {
                    LOGGER.trace("Inside LMSRepository getCreationDateTime() != null"
                            + customerObj.getCreationDateTime());
                    transaction.setLastSyncDate(customerObj.getCreationDateTime());
                }
                transactionsList.add(transaction);
            }
        }*/

        return searchCriteriatResponse;
    }
    
    private List<CommunicationInteractionTypeCodeList> getCommunicationInteractionCodesForCallVisit() {
        List<CommunicationInteractionTypeCodeList> communicationInteractionCodeList = new ArrayList<CommunicationInteractionTypeCodeList>();
        communicationInteractionCodeList.add(CommunicationInteractionTypeCodeList.Call);
        communicationInteractionCodeList.add(CommunicationInteractionTypeCodeList.Visit);
        return communicationInteractionCodeList;
    }

    private List<CommunicationStatusCodeList> getCommunicationStatusCodesForCallVisit() {
        List<CommunicationStatusCodeList> communicationStatusCodeList = new ArrayList<CommunicationStatusCodeList>();
        communicationStatusCodeList.add(CommunicationStatusCodeList.NotInterested);
        communicationStatusCodeList.add(CommunicationStatusCodeList.Interestedbutdontwanttomakeappointment);
        communicationStatusCodeList.add(CommunicationStatusCodeList.MakeAppointment);
        communicationStatusCodeList.add(CommunicationStatusCodeList.MetwithCustomer);
        return communicationStatusCodeList;
    }

    public List<Transactions> retrieveMonthlyNewProspect(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList =getCustomerStatusCodesFor(Constants.RETRIEVE_BY_FILTER);

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Response<List<Customer>> searchCriteriatResponse =
                partyComponent.retrieveMonthlyNewProspect(searchCriteriatRequest, statusRequest);
        final List<Customer> customers = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(customers)) {
            for (Customer customerObj : customers) {
                transaction = new Transactions();
                transaction.setCustomer(customerObj);
                transaction.setCustomerId(customerObj.getIdentifier());
                transaction.setType(searchCriteria.getType());
                if (customerObj.getModifiedDateTime() != null
                        && !(customerObj.getModifiedDateTime().toString().equals(""))) {
                    LOGGER.trace("Inside LMSRepository customerObj.getModifiedDateTime() != null"
                            + customerObj.getModifiedDateTime());
                    transaction.setLastSyncDate(customerObj.getModifiedDateTime());
                    transaction.setModifiedTimeStamp(customerObj.getModifiedDateTime());
                } else if (customerObj.getCreationDateTime() != null
                        && !("".equals(customerObj.getCreationDateTime().toString()))) {
                    LOGGER.trace("Inside LMSRepository getCreationDateTime() != null"
                            + customerObj.getCreationDateTime());
                    transaction.setLastSyncDate(customerObj.getCreationDateTime());
                }
                transactionsList.add(transaction);
            }
        }

        return transactionsList;
    }
    
    public Map<String,Integer> retrieveMonthwiseSalesActivity(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList =getCustomerStatusCodesFor(Constants.RETRIEVE);

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Map<String,Integer> searchCriteriatResponse =
                partyComponent.retrieveMonthwiseSalesActivityCustomers(searchCriteriatRequest, statusRequest);

        return searchCriteriatResponse;
    }
    
    public Map<String,Integer> retrieveWeekwiseSalesActivity(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList =getCustomerStatusCodesFor(Constants.RETRIEVE);

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Map<String,Integer> searchCriteriatResponse =
                partyComponent.retrieveWeekwiseSalesActivityCustomers(searchCriteriatRequest, statusRequest);

        return searchCriteriatResponse;
    }
    
    public Map<String,Integer> retrieveWeekwiseNewProspect(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList =getCustomerStatusCodesFor(Constants.RETRIEVE_BY_FILTER);

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Map<String,Integer> searchCriteriatResponse =
                partyComponent.retrieveWeekwiseNewProspect(searchCriteriatRequest, statusRequest);

        return searchCriteriatResponse;
    }
    
    public Map<String,Integer> retrieveMonthwiseNewProspect(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList =getCustomerStatusCodesFor(Constants.RETRIEVE_BY_FILTER);

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Map<String,Integer> searchCriteriatResponse =
                partyComponent.retrieveMonthwiseNewProspect(searchCriteriatRequest, statusRequest);

        return searchCriteriatResponse;
    }

    /**
     * retrieves the Status for the particular operation.
     * 
     */
    @Override
    protected List<CustomerStatusCodeList> getCustomerStatusCodesFor(String operation) {
        List<CustomerStatusCodeList> statusCodeLists = new ArrayList<CustomerStatusCodeList>();
        if (Constants.RETRIEVE.equals(operation) || Constants.RETRIEVE_BY_FILTER.equals(operation) || Constants.RETRIEVE_BY_COUNT.equals(operation)
                || Constants.UPDATE_OP.equals(operation) || Constants.DELETE.equals(operation)) {
            statusCodeLists.add(CustomerStatusCodeList.New);
            statusCodeLists.add(CustomerStatusCodeList.SuccessfulSale);
            statusCodeLists.add(CustomerStatusCodeList.FollowUp);
            statusCodeLists.add(CustomerStatusCodeList.Non_Contactable);
            statusCodeLists.add(CustomerStatusCodeList.Meeting_Fixed);
            statusCodeLists.add(CustomerStatusCodeList.Successful_Sale);
            statusCodeLists.add(CustomerStatusCodeList.ToBeCalled);
            statusCodeLists.add(CustomerStatusCodeList.ContactedAndNotInterested);
            statusCodeLists.add(CustomerStatusCodeList.App_Submitted);
            statusCodeLists.add(CustomerStatusCodeList.Pending_Documents);
            statusCodeLists.add(CustomerStatusCodeList.Others);
        } else if (Constants.RETRIEVE_ALL.equals(operation)) {
        	 statusCodeLists.add(CustomerStatusCodeList.New);
             statusCodeLists.add(CustomerStatusCodeList.SuccessfulSale);
             statusCodeLists.add(CustomerStatusCodeList.FollowUp);
             statusCodeLists.add(CustomerStatusCodeList.Non_Contactable);
             statusCodeLists.add(CustomerStatusCodeList.Meeting_Fixed);
             statusCodeLists.add(CustomerStatusCodeList.Successful_Sale);
             statusCodeLists.add(CustomerStatusCodeList.ToBeCalled);
             statusCodeLists.add(CustomerStatusCodeList.ContactedAndNotInterested);
             statusCodeLists.add(CustomerStatusCodeList.App_Submitted);
             statusCodeLists.add(CustomerStatusCodeList.Pending_Documents);
             statusCodeLists.add(CustomerStatusCodeList.Others);
             statusCodeLists.add(CustomerStatusCodeList.Cancelled);
        } else if (Constants.DUPLICATE_CHECK.equals(operation)) {
            statusCodeLists.add(CustomerStatusCodeList.Cancelled);
            statusCodeLists.add(CustomerStatusCodeList.Suspended);
            statusCodeLists.add(CustomerStatusCodeList.Successful_Sale);
            statusCodeLists.add(CustomerStatusCodeList.SuccessfulSale);
        } else if(GeneraliConstants.BULKUPLOAD_PENDING.equals(operation)) {
            statusCodeLists.add(CustomerStatusCodeList.Meeting_Fixed);
            statusCodeLists.add(CustomerStatusCodeList.ToBeCalled);
            statusCodeLists.add(CustomerStatusCodeList.App_Submitted);
            statusCodeLists.add(CustomerStatusCodeList.Pending_Documents);
            statusCodeLists.add(CustomerStatusCodeList.Non_Contactable);
            statusCodeLists.add(CustomerStatusCodeList.Others);
        } else if(GeneraliConstants.RETRIEVE_BY_HIERARCHYLEVEL.equals(operation)) {
        	 statusCodeLists.add(CustomerStatusCodeList.New);            
             statusCodeLists.add(CustomerStatusCodeList.Successful_Sale);            
             statusCodeLists.add(CustomerStatusCodeList.Non_Contactable);
             statusCodeLists.add(CustomerStatusCodeList.Meeting_Fixed);             
             statusCodeLists.add(CustomerStatusCodeList.ToBeCalled);
             statusCodeLists.add(CustomerStatusCodeList.App_Submitted);
             statusCodeLists.add(CustomerStatusCodeList.Pending_Documents);
             statusCodeLists.add(CustomerStatusCodeList.Others);
             statusCodeLists.add(CustomerStatusCodeList.FollowUp);
             statusCodeLists.add(CustomerStatusCodeList.SuccessfulSale);
        } else if(GeneraliConstants.RETRIEVE_BY_LEADREASSIGN.equals(operation)) {
        	 statusCodeLists.add(CustomerStatusCodeList.New);            
             statusCodeLists.add(CustomerStatusCodeList.FollowUp);
             statusCodeLists.add(CustomerStatusCodeList.Non_Contactable);
             statusCodeLists.add(CustomerStatusCodeList.Meeting_Fixed);            
             statusCodeLists.add(CustomerStatusCodeList.ToBeCalled);
             statusCodeLists.add(CustomerStatusCodeList.ContactedAndNotInterested);
             statusCodeLists.add(CustomerStatusCodeList.App_Submitted);
             statusCodeLists.add(CustomerStatusCodeList.Pending_Documents);
             statusCodeLists.add(CustomerStatusCodeList.Others);
        } else if(GeneraliConstants.RETRIEVE_BY_CONVERSIONRATE.equals(operation)) {
	       	 statusCodeLists.add(CustomerStatusCodeList.Successful_Sale); 
	       	 statusCodeLists.add(CustomerStatusCodeList.SuccessfulSale);
        } else if(GeneraliConstants.RETRIEVE_BY_NEW_STATUS.equals(operation)) {
	       	 statusCodeLists.add(CustomerStatusCodeList.New);
        } else if(GeneraliConstants.RETRIEVE_BY_CLOSED_STATUS.equals(operation)) {
	       	 statusCodeLists.add(CustomerStatusCodeList.Successful_Sale);  
	       	 statusCodeLists.add(CustomerStatusCodeList.SuccessfulSale);
        } else if(GeneraliConstants.RETRIEVE_BY_FOLLOWUP_STATUS.equals(operation)) {
	       	 statusCodeLists.add(CustomerStatusCodeList.Meeting_Fixed);
	       	 statusCodeLists.add(CustomerStatusCodeList.ToBeCalled); 
	       	 statusCodeLists.add(CustomerStatusCodeList.App_Submitted); 
	       	 statusCodeLists.add(CustomerStatusCodeList.Pending_Documents); 
	       	 statusCodeLists.add(CustomerStatusCodeList.Non_Contactable); 
	         statusCodeLists.add(CustomerStatusCodeList.Others); 
        } else if (operation.equals(GeneraliConstants.CALL_OR_VISIT)){
            statusCodeLists.add(CustomerStatusCodeList.New);
        }
        
        return statusCodeLists;
    }

		
    @Override
    protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(String operation) {
        List<AgreementStatusCodeList> statusCodeLists = new ArrayList<AgreementStatusCodeList>();
        if (Constants.DELETE.equals(operation)) {
            statusCodeLists.add(AgreementStatusCodeList.Draft);
            statusCodeLists.add(AgreementStatusCodeList.Confirmed);
            statusCodeLists.add(AgreementStatusCodeList.Completed);
            statusCodeLists.add(AgreementStatusCodeList.New);
            
        } else if (Constants.RETRIEVE_BY_FILTER.equals(operation) || Constants.RETRIEVE_BY_COUNT.equals(operation)) {
            //statusCodeLists.add(AgreementStatusCodeList.Initial);
            statusCodeLists.add(AgreementStatusCodeList.New);
            statusCodeLists.add(AgreementStatusCodeList.Submitted);
            statusCodeLists.add(AgreementStatusCodeList.Draft);
            statusCodeLists.add(AgreementStatusCodeList.Completed);
            statusCodeLists.add(AgreementStatusCodeList.Confirmed);
            statusCodeLists.add(AgreementStatusCodeList.PendingSubmission);
           // statusCodeLists.add(AgreementStatusCodeList.Proposed);
        }
        return statusCodeLists;
    }

    /**
     * Retrieve by filter lms.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     */
    public List<Transactions> retrieveLMSRelatedTransCount(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside GeneraliLMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.RETRIEVE_BY_FILTER);

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Response<List<Customer>> searchCriteriatResponse =
                partyComponent.retrieveFilterCustomers(searchCriteriatRequest, statusRequest);
        final List<Customer> customers = searchCriteriatResponse.getType();
        List<String> transTrackingIdList = new ArrayList<String>();
        if (CollectionUtils.isNotEmpty(customers)) {
            for (Customer customerObj : customers) {
                transTrackingIdList.add(customerObj.getTransTrackingId());

                transaction = new Transactions();
                transaction.setCustomer(customerObj);
                transaction.setCustomerId(customerObj.getIdentifier());
                transaction.setType(searchCriteria.getType());
                if (customerObj.getModifiedDateTime() != null
                        && !(customerObj.getModifiedDateTime().toString().equals(""))) {
                    LOGGER.trace("Inside GeneraliLMSRepository customerObj.getModifiedDateTime() != null"
                            + customerObj.getModifiedDateTime());
                    transaction.setLastSyncDate(customerObj.getModifiedDateTime());
                    transaction.setModifiedTimeStamp(customerObj.getModifiedDateTime());
                } else if (customerObj.getCreationDateTime() != null
                        && !("".equals(customerObj.getCreationDateTime().toString()))) {
                    LOGGER.trace("Inside GeneraliLMSRepository getCreationDateTime() != null"
                            + customerObj.getCreationDateTime());
                    transaction.setLastSyncDate(customerObj.getCreationDateTime());
                }
                transactionsList.add(transaction);
            }
            // Get the count of FNA,illustration and eApp
            searchCriteria.setTransTrackingIdList(transTrackingIdList);
            searchCriteriatRequest.setType(searchCriteria);

            final Request<List<AgreementStatusCodeList>> statusRequestAgreement =
                    new JPARequestImpl<List<AgreementStatusCodeList>>(getEntityManager(), ContextTypeCodeList.LMS,
                            requestInfo.getTransactionId());
            final List<AgreementStatusCodeList> statusListAgreement =
                    getAgreementStatusCodesFor(Constants.RETRIEVE_BY_COUNT);
            statusRequestAgreement.setType(statusListAgreement);

            final Response<List<SearchCountResult>> searchCriteriaCountResponse =
                    partyComponent.getRelatedTransactionCountForLMS(searchCriteriatRequest, statusRequestAgreement);

            final List<SearchCountResult> searchCountResultList = searchCriteriaCountResponse.getType();

            if (CollectionUtils.isNotEmpty(searchCountResultList)) {

                for (SearchCountResult searchCountResult : searchCountResultList) {

                    for (Transactions transactions : transactionsList) {

                        if (searchCountResult.getTransTrackingId().equals(
                                transactions.getCustomer().getTransTrackingId())) {

                            if (FNA.equals(searchCountResult.getMode())) {
                                transactions.setFnaCount(searchCountResult.getCount());

                            } else if (ILLUSTRATION.equals(searchCountResult.getMode())) {

                                transactions.setIllustrationCount(searchCountResult.getCount());

                            } else if (E_APP.equals(searchCountResult.getMode())) {
                                transactions.seteAppCount(searchCountResult.getCount());
                            }

                        }
                    }
                }

            }

        }

        return transactionsList;
    }
    
 /**
     * On fna update.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the string
     */

    protected String onFnaUpdate(Transactions transactions, RequestInfo requestInfo) {

		final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.UPDATE_OP);
		Customer customer = createReplacingCustomer(transactions, requestInfo,
				ContextTypeCodeList.LMS, statusList);
		// Checking whether the server data is latest when source of
		// truth is server, if so reject the client data
		// and send rejected status
		if (Constants.TRUE.equals(conflictResolution)
				&& Constants.SERVER.equals(sourceOfTruth)) {
			Date modifiedTimeStamp = customer.getModifiedDateTime();

			if (!(transactions.getLastSyncDate().toString()
					.equals(GeneraliDateUtil.getDefaultDate()))
					&& ((null != modifiedTimeStamp) && (modifiedTimeStamp
							.after(transactions.getLastSyncDate())))) {

				LOGGER.trace("Inside if case and rejected the case..");
				return Constants.REJECTED;

			}

		}
		updateCustomerStatus(customer, requestInfo, ContextTypeCodeList.EAPP);
		transactions.getCustomer().setCreationDateTime(
				customer.getCreationDateTime());
		return Constants.SUCCESS;
    }
     
    /**
     * update the customer status for cancelled status leads once aftr reassigning.
     * 
     * @param customer
     *            the customer
     * @param requestInfo
     *            the request info
     * @param contextType
     *            the context type
     */
    protected void updateCustomerStatusREASSIGN(final Customer customer, final RequestInfo requestInfo,
            final ContextTypeCodeList contextType) {
        final Request<Customer> request =
                new JPARequestImpl<Customer>(entityManager, contextType, requestInfo.getTransactionId());
        customer.getTransactionKeys().setKey15(Constants.CANCELLED);
        try {
			customer.setModifiedDateTime(LifeEngageComponentHelper.getCurrentdate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        customer.setStatusCode(CustomerStatusCodeList.Cancelled);
        request.setType(customer);
        partyComponent.updateCustomer(request);
    }
    
    /**
     * update the customer status for cancelled status leads once aftr reassigning.
     * 
     * @param customer
     *            the customer
     * @param requestInfo
     *            the request info
     * @param contextType
     *            the context type
     */
    protected void deleteAssignedLead(final Customer customer, final RequestInfo requestInfo,
            final ContextTypeCodeList contextType) {
        final Request<Customer> request =
                new JPARequestImpl<Customer>(entityManager, contextType, requestInfo.getTransactionId());
     // A new entry is created for the Lead with a new transtrackingID
		customer.setTransTrackingId(
				GeneraliConstants.DELETEBRNCHUSERASSIGNEDLEAD + "-"
						+ customer.getBranchId() + "-"
						+ String.valueOf(idGenerator.generate()) + "-"
						+ String.valueOf(idGenerator.generate()));
		String deletedleadIdentifier = customer.getBranchId()
				+ String.valueOf(idGenerator.generate(LMS));
		int i=0;
		
		// Agent id is set to blank to remove this lead from agent.
		customer.getTransactionKeys().setKey11("");
		customer.setAgentId("");
		
		// New identifier
		customer.setIdentifier(deletedleadIdentifier);
		customer.getTransactionKeys()
				.setKey1(deletedleadIdentifier);
        try {
			customer.setModifiedDateTime(LifeEngageComponentHelper.getCurrentdate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //customer.setStatusCode(CustomerStatusCodeList.Cancelled);
        request.setType(customer);
        partyComponent.updateCustomer(request);
    }

	/**
     * On fna update.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the string
     */
    @Override
    protected String onLMSUpdate(Transactions transactions, RequestInfo requestInfo, Customer responseCustomer) {

        final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.UPDATE_OP);
        Customer customer = new Customer();
        if( responseCustomer != null ){
        	customer = responseCustomer;
        }else{
        	customer = createReplacingCustomer(transactions, requestInfo, ContextTypeCodeList.LMS, statusList);
        }
        // Checking whether the server data is latest when source of
        // truth is server, if so reject the client data
        // and send rejected status
        if (Constants.TRUE.equals(conflictResolution) && Constants.SERVER.equals(sourceOfTruth)) {
            Date modifiedTimeStamp = customer.getModifiedDateTime();
            if (!(transactions.getLastSyncDate().toString().equals(GeneraliDateUtil.getDefaultDate()))
                    && ((null != modifiedTimeStamp) && (modifiedTimeStamp.after(transactions.getLastSyncDate())))) {

                LOGGER.trace("Inside if case and rejected the case..");
                return Constants.REJECTED;

            }
            transactions.getCustomer().setCreationDateTime(customer.getCreationDateTime());
        }
        if(Constants.RM_REASSIGN.equals(transactions.getAllocationType())) {
        	CustomerStatusCodeList newCustomerStatus = customer.getStatusCode();
			updateCustomerStatusREASSIGN(customer, requestInfo, ContextTypeCodeList.LMS);
			transactions.getCustomer().setTransTrackingId(transactions.getTransTrackingId());
			String leadReassignIdentifier = transactions.getAgentId()+String.valueOf(idGenerator.generate(LMS));
			transactions.setKey1(leadReassignIdentifier);
			transactions.getCustomer().setIdentifier(leadReassignIdentifier);
			transactions.getCustomer().getTransactionKeys().setKey1(leadReassignIdentifier);
			transactions.getCustomer().getTransactionKeys().setKey2(responseCustomer.getTransactionKeys().getKey2());
			transactions.getCustomer().getTransactionKeys().setKey3(responseCustomer.getTransactionKeys().getKey3());
			transactions.getCustomer().getTransactionKeys().setKey4(responseCustomer.getTransactionKeys().getKey4());
			transactions.getCustomer().getTransactionKeys().setKey5(responseCustomer.getTransactionKeys().getKey5());
			transactions.getCustomer().getTransactionKeys().setKey20(responseCustomer.getTransactionKeys().getKey20());
			transactions.getCustomer().getTransactionKeys().setKey6(responseCustomer.getTransactionKeys().getKey6());
			SimpleDateFormat sf = new SimpleDateFormat(DATEFORMAT); 
			transactions.getCustomer().getTransactionKeys().setKey13(sf.format(new Date()));
			transactions.getCustomer().getTransactionKeys().setKey14(sf.format(new Date()));
			transactions.getCustomer().getTransactionKeys().setKey18(responseCustomer.getTransactionKeys().getKey18());
			transactions.getCustomer().getTransactionKeys().setKey19(responseCustomer.getTransactionKeys().getKey19());
			transactions.getCustomer().getTransactionKeys().setKey26(responseCustomer.getTransactionKeys().getKey26());
			transactions.getCustomer().getTransactionKeys().setKey15(newCustomerStatus.toString());
			transactions.getCustomer().setCreationDateTime(new Date());
			transactions.getCustomer().setStatusCode(newCustomerStatus);
			transactions.getCustomer().setCityValue(responseCustomer.getCityValue());
			transactions.getCustomer().setStateValue(responseCustomer.getStateValue());
			transactions.getCustomer().setWardValue(responseCustomer.getWardValue());
			Party playerParty = customer.getPlayerParty();
			transactions.getCustomer().setPlayerParty(playerParty);
			
			Set<Communication> newCommunication = new HashSet<Communication>();
			for(Communication comm : customer.getReceivesCommunication()){
				Communication newCom = new Communication();
				Set<Communication> newFollowup = new HashSet<Communication>();
				for(Communication followup : comm.getFollowsUp()){
				Communication followupCommunication = new Communication();
				followupCommunication.setInteractionType(followup.getInteractionType());
				followupCommunication.setActualDateAndTime(followup.getActualDateAndTime());
				followupCommunication.setPlannedTime(followup.getPlannedTime());
				followupCommunication.setPlannedTimePeriod(followup.getPlannedTimePeriod());
				followupCommunication.setCommunicationStatusDate(followup.getCommunicationStatusDate());
				followupCommunication.setCommunicationDescription(followup.getCommunicationDescription());
				followupCommunication.setCommunicationPurposeCode(followup.getCommunicationPurposeCode());
				followupCommunication.setCommunicationStatusCode(followup.getCommunicationStatusCode());
				newFollowup.add(followupCommunication);
				}
				newCom.setFollowsUp(newFollowup);
				newCommunication.add(newCom);
			}
			
			transactions.getCustomer().setReceivesCommunication(newCommunication);
			// This is to handle the when an agent deletes leads assigned by a branch user. 
			//In this case the lead has to be kept with branch user and need to be removed just from the agent.
		} else if (Constants.CANCELLED.equals(transactions.getStatus())
				&& customer.getBranchId() != null
				&& !GeneraliConstants.JSON_EMPTY.equals(customer.getBranchId())) {			
			// Modifying the existing lead entry.
			deleteAssignedLead(customer, requestInfo,
					ContextTypeCodeList.LMS);
			transactions.getCustomer().setCreationDateTime(customer.getCreationDateTime());
			 } 
        else {
	    	updateCustomerStatus(customer, requestInfo, ContextTypeCodeList.LMS);
	    	transactions.getCustomer().setCreationDateTime(customer.getCreationDateTime());
	    }        
        
        return Constants.SUCCESS;
    }
    
    /**
     * Retrieve all.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param isFullDetailsRequired
     *            the is full details required
     * @return the list
     */
    public List<Transactions> retrieveAll(final Transactions transactions, final RequestInfo requestInfo,
            final boolean isFullDetailsRequired) {
        LOGGER.trace("Inside LMSRepository retrieveAll : retrieveAll" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;

        final List<CustomerStatusCodeList> statusList;

        final Request<Customer> customerRequest =
                new JPARequestImpl<Customer>(getEntityManager(), ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
        final Customer customer = new Customer();
        customer.setAgentId(transactions.getAgentId());
        customer.setBranchId(transactions.getBranchId());
            if(transactions.getKey24().equals("ReAssign")) {
		       	 customer.setAllocationType(transactions.getKey24());
		       	 statusList = getCustomerStatusCodesFor(GeneraliConstants.RETRIEVE_BY_LEADREASSIGN);
            } else {
            	 statusList = getCustomerStatusCodesFor(Constants.RETRIEVE_ALL);
            }
        customer.setLastSyncDate(requestInfo.getLastSyncDate());
        customerRequest.setType(customer);

        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        Request<Integer> limitRequest = null;

        if ((isFullDetailsRequired) && (lmsLimit != null)) {

            limitRequest =
                    new JPARequestImpl<Integer>(getEntityManager(), ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
            limitRequest.setType(lmsLimit);

        }

        final Response<List<Customer>> customersResponse =
                partyComponent.retrieveCustomers(customerRequest, statusRequest, limitRequest);
        final List<Customer> customers = customersResponse.getType();
        if (CollectionUtils.isNotEmpty(customers)) {
            for (Customer customerObj : customers) {
                transaction = new Transactions();
                transaction.setCustomer(customerObj);
                if (customerObj.getModifiedDateTime() != null
                        && !(customerObj.getModifiedDateTime().toString().equals(""))) {
                    LOGGER.trace("Inside LMSRepository customerObj.getModifiedDateTime() != null"
                            + customerObj.getModifiedDateTime());
                    transaction.setLastSyncDate(customerObj.getModifiedDateTime());
                    transaction.setModifiedTimeStamp(customerObj.getModifiedDateTime());
                } else if (customerObj.getCreationDateTime() != null
                        && !("".equals(customerObj.getCreationDateTime().toString()))) {
                    LOGGER.trace("Inside LMSRepository getCreationDateTime() != null"
                            + customerObj.getCreationDateTime());
                    transaction.setLastSyncDate(customerObj.getCreationDateTime());
                }
                
                Set<Communication> communicationList = transaction.getCustomer().getReceivesCommunication();
            	Date appointmentDate = null;
            	int appointmentCount = 0;
            	for(Communication communication : communicationList){
            	    List<Date> appointmentDateList = new ArrayList<Date>();
            		if(communication.getInteractionType().toString().equals(Constants.APPOINTMENT)){
            			Set<Communication> followUps = communication.getFollowsUp();
            			appointmentCount = followUps.size();
            			for(Communication followUp : followUps){
            				String metTimeString = followUp.getPlannedTime();
            				SimpleDateFormat sf = new SimpleDateFormat("HH:mm");
            				String TIME24HOURS_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
            				Date metTime = null;
            				if(metTimeString != null && metTimeString.matches(TIME24HOURS_PATTERN)){
							try {
								metTime = sf.parse(metTimeString);
							} catch (ParseException e) {
								e.printStackTrace();
							}
            				Date combinedDate = getDateTime(followUp.getCommunicationStatusDate(),metTime);
            				appointmentDateList.add(combinedDate);
            				}
            			}
            			if(!appointmentDateList.isEmpty()){
            			Collections.sort(appointmentDateList);
            			for(Date appointment : appointmentDateList){
            			if(appointment.after(new Date())){
            			appointmentDate = appointment;
            			break;
            			}
            			}
            			}
            		}
            	}
//            	if(appointmentDate != null){
//            	SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//            	String appointmentDateString = sf.format(appointmentDate);
//            	transaction.getCustomer().getTransactionKeys().setKey19(appointmentDateString);
//            	}
            	transaction.getCustomer().getTransactionKeys().setKey27(Integer.toString(appointmentCount));
                transactionsList.add(transaction);
            }
        }

        return transactionsList;
    }
    
    private Date getDateTime(Date date, Date time) {
    	Calendar aDate = Calendar.getInstance();
        aDate.setTime(date);

        Calendar aTime = Calendar.getInstance();
        aTime.setTime(time);

        aDate.set(Calendar.HOUR_OF_DAY, aTime.get(Calendar.HOUR_OF_DAY));
        aDate.set(Calendar.MINUTE, aTime.get(Calendar.MINUTE));

        return aDate.getTime();
    }
    
    public List<String>retrieveAgentwithLessthan50Followups(List<String> agentCodeList,RequestInfo requestInfo){
    	
    	final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(GeneraliConstants.BULKUPLOAD_PENDING);
    	
    	 final Request<List<String>> customerRequest =
             new JPARequestImpl<List<String>>(getEntityManager(), ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
    	 customerRequest.setType(agentCodeList);

    	 final Request<List<CustomerStatusCodeList>> statusRequest =
             new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.EAPP,
                     requestInfo.getTransactionId());
    	   statusRequest.setType(statusList);
    	   List<String> agentCodes=new ArrayList<String>();
    	   final Response<List<String>> customersResponse=partyComponent.retrieveAgentwith50lessFollowUps(customerRequest, statusRequest);
    	   if(customersResponse!=null){
    		   agentCodes= customersResponse.getType();
    	   }
		return agentCodes;
		
	}
    
/* public List<String>retrieveAgentwithMaximumYTD(List<String> agentCodeList,RequestInfo requestInfo){
    	
    	final List<CustomerStatusCodeList> statusListClosed = new ArrayList<CustomerStatusCodeList>();
    	statusListClosed.add(CustomerStatusCodeList.Successful_Sale);
    	 final Request<List<String>> customerRequest =
             new JPARequestImpl<List<String>>(getEntityManager(), ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
    	 customerRequest.setType(agentCodeList);

    	 final Request<List<CustomerStatusCodeList>> statusRequest =
             new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.EAPP,
                     requestInfo.getTransactionId());
    	   statusRequest.setType(statusListClosed);
    	 
    	   final Response<List<String>> customersResponse=partyComponent.retrieveAgentwith50lessFollowUps(customerRequest, statusRequest);
    	    final List<String> agentCodes = customersResponse.getType();
		return agentCodes;
		
	}
 */
 public List<AgentData> retrieveAgentwithMaximumYTD(
			List<String> agentCodeList, RequestInfo requestInfo) {
	 List<AgentData> agentCodes=new ArrayList<AgentData>();
		List<String> agentDataList1 = new ArrayList<String>();
		final List<CustomerStatusCodeList> statusListClosed = new ArrayList<CustomerStatusCodeList>();
		statusListClosed.add(CustomerStatusCodeList.Successful_Sale);
		final JPARequestImpl<List<AgentData>> customerRequest = new JPARequestImpl<List<AgentData>>(
				getEntityManager(), ContextTypeCodeList.EAPP,
				requestInfo.getTransactionId());
		 AgentData agentData= new AgentData();
		if(agentCodeList!=null && agentCodeList.size()>0){
 		agentData.setAgentCode(agentCodeList.get(0));
 		agentData.setCurrentDate(new Date());
 		
 		Calendar cal = Calendar.getInstance();
 		int year=cal.get(Calendar.YEAR);
 		cal.set(Calendar.YEAR,year);
 		cal.set(Calendar.DAY_OF_YEAR, 1);    
 		Date start = cal.getTime();
 		agentData.setStartDate(start);
 	
		List<AgentData> agentDataList = new ArrayList<AgentData>();
		for(String agentCode: agentCodeList){
			AgentData agentData1= new AgentData();
			agentData1.setAgentCode(agentCode);
			agentData1.setCurrentDate(new Date());
			agentData1.setStartDate(start);
			agentDataList.add(agentData1);
		}
		customerRequest.setType(agentDataList);

		final Request<List<CustomerStatusCodeList>> statusRequestClosed = new JPARequestImpl<List<CustomerStatusCodeList>>(
				getEntityManager(), ContextTypeCodeList.EAPP,
				requestInfo.getTransactionId());
		statusRequestClosed.setType(statusListClosed);

		final List<CustomerStatusCodeList> statusList = new ArrayList<CustomerStatusCodeList>();
		statusList.add(CustomerStatusCodeList.Cancelled);
		statusList.add(CustomerStatusCodeList.Suspended);
		final Request<List<CustomerStatusCodeList>> statusRequest = new JPARequestImpl<List<CustomerStatusCodeList>>(
				getEntityManager(), ContextTypeCodeList.EAPP,
				requestInfo.getTransactionId());
		statusRequest.setType(statusList);

		final Response<List<AgentData>> customersResponse = partyComponent
				.retrieveAgentwithMaximumYTD(customerRequest,
						statusRequestClosed, statusRequest);
		agentCodes= customersResponse.getType();
		
	
		for(AgentData agentCode: agentCodes){
			agentDataList1.add(agentCode.getAgentCode());
		}
		}
		return agentCodes;

	}

 /**
  * Retrieve lms.
  * 
  * @param transactions
  *            the lead identifier
  * @param transactionId
  *            the transaction id
  * @param requestInfo
  *            the request info
  * @return the transactions
  */
 @Override
 public Transactions retrieveLMS(final Transactions leadIdentifier, final String transactionId,
         final RequestInfo requestInfo) {
     LOGGER.trace("LMSRepository.retrieveLMS: leadIdentifier" + leadIdentifier);
     LOGGER.trace("LMSRepository.retrieveLMS: transactionId" + transactionId);
     Transactions transaction = new Transactions();
     final Request<Customer> leadRequest =
             new JPARequestImpl<Customer>(getEntityManager(), ContextTypeCodeList.LMS, transactionId);
     final Customer customer = new Customer();
     customer.setIdentifier(leadIdentifier.getKey1());
     customer.setTransTrackingId(leadIdentifier.getTransTrackingId());
     customer.setAgentId(leadIdentifier.getAgentId());
     leadRequest.setType(customer);
     
     List<CustomerStatusCodeList> statusList = new ArrayList<CustomerStatusCodeList>();

     if(Constants.RM_REASSIGN.equals(leadIdentifier.getKey24())) {
    	 statusList = getCustomerStatusCodesFor(GeneraliConstants.RETRIEVE_BY_LEADREASSIGN);
     } else {
    	 statusList = getCustomerStatusCodesFor(Constants.RETRIEVE);
     }
     

     final Request<List<CustomerStatusCodeList>> statusRequest =
             new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.LMS,
                     requestInfo.getTransactionId());
     statusRequest.setType(statusList);

     final Response<Customer> customerResponse = partyComponent.retrieveCustomer(leadRequest, statusRequest);
     if (customerResponse.getType() != null) {
         transaction.setCustomer(customerResponse.getType());
         transaction.setModifiedTimeStamp(customerResponse.getType().getModifiedDateTime());
         //transaction = getYTDAPE(transaction, leadRequest, statusRequest);
     }

     return transaction;

 }
 
 public String notifySuccessfulSale(String lmsTrackKey,String proposalNo,BigDecimal amount,Date submissionDate,String productCode){
	 LOGGER.info("LifeAsiaBackflow: recived SuccessfulSale notification for LMS : "+lmsTrackKey+" Proposal No :"+proposalNo);
	 Request<String> transTrackReq=new JPARequestImpl<String>(entityManager, ContextTypeCodeList.LMS, "");
	 transTrackReq.setType(lmsTrackKey);
	 Request<List<CustomerStatusCodeList>> statusRequest=new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.LMS, "");
	 statusRequest.setType(getCustomerStatusCodesFor(Constants.RETRIEVE));
	 Response<Customer> response=partyComponent.retrieveCustomerWithTransTrackingId(transTrackReq, statusRequest);
	 Customer customer=response.getType();
	 if(customer==null){
		 LOGGER.error("Customer Not found with Id: "+lmsTrackKey+"Proposal No :"+proposalNo);
		 return "Customer Not found with Id "+lmsTrackKey;
	 }
	 PurchasedProduct  purchasedProduct=new PurchasedProduct();
	 purchasedProduct.setProposalNumber(proposalNo);
	 purchasedProduct.setProposalNumber(proposalNo);
	 CurrencyAmount currencyAmount=new  CurrencyAmount();
	 currencyAmount.setAmount(amount);
	 purchasedProduct.setPremium(currencyAmount);
	 purchasedProduct.setSubmissionDate(submissionDate); 
	 purchasedProduct.setProductCode(productCode);
	 if(customer.getPurchasedProducts()==null){
		 Set<PurchasedProduct> purchasedProducts=new HashSet<PurchasedProduct>();
		 customer.setPurchasedProducts(purchasedProducts);
	 }
	 customer.getPurchasedProducts().add(purchasedProduct);
	 customer.setStatusCode(CustomerStatusCodeList.Successful_Sale);
	 customer.getTransactionKeys().setKey15(SUCCESSFUL_SALE);
	 customer.setModifiedDateTime(new Date());
	 /*Updating the creationDateTime to fetch by retriveAll service */
	// customer.setCreationDateTime(new Date());
	 LOGGER.info("LifeAsiaBackflow: Completed  SuccessfulSale notification for LMS : "+lmsTrackKey+"Proposal No :"+proposalNo);
	 return "Updated Successfuly "+lmsTrackKey;
	 
 }

public ManagerialLevelUserDetails retrieveByCountForManagerialLevel(
		RequestInfo requestInfo,
		ManagerialLevelUserDetails managerialLevelUserDetails) {
	LOGGER.trace("GeneraliLMSRepository.retrieveByCountForManagerialLevel: selectedAgentIds" + managerialLevelUserDetails.getSelectedAgentIds());
	final Request<ManagerialLevelUserDetails> managerialLevelUserDetailsRequest = new JPARequestImpl<ManagerialLevelUserDetails>(getEntityManager(), ContextTypeCodeList.LMS,
            requestInfo.getTransactionId());
	managerialLevelUserDetailsRequest.setType(managerialLevelUserDetails);
	
	final Request<List<CustomerStatusCodeList>> statusRequestNew =
            new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.LMS,
                    requestInfo.getTransactionId());
	
	final Request<List<CustomerStatusCodeList>> statusRequestClosed =
            new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.LMS,
                    requestInfo.getTransactionId());
	
	final Request<List<CustomerStatusCodeList>> statusRequestFollowUp =
            new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.LMS,
                    requestInfo.getTransactionId());
	
	final List<CustomerStatusCodeList> statusListNew = getCustomerStatusCodesFor(GeneraliConstants.RETRIEVE_BY_NEW_STATUS);
	statusRequestNew.setType(statusListNew);
	
	final List<CustomerStatusCodeList> statusListClosed = getCustomerStatusCodesFor(GeneraliConstants.RETRIEVE_BY_CLOSED_STATUS);
	statusRequestClosed.setType(statusListClosed);
	
	final List<CustomerStatusCodeList> statusListFollowUp = getCustomerStatusCodesFor(GeneraliConstants.RETRIEVE_BY_FOLLOWUP_STATUS);
	statusRequestFollowUp.setType(statusListFollowUp);
	
	final Response<ManagerialLevelUserDetails> managerialLevelUserDetailsResp = partyComponent.retrieveByStatusForManagerialLevel(managerialLevelUserDetailsRequest, statusListNew, statusListClosed, statusListFollowUp);
	
	return managerialLevelUserDetailsResp.getType();
}
 
	/*@Override
	public SearchCountResult retrieveByCountLMS(final RequestInfo requestInfo, final SearchCriteria searchCriteria) {
        try {
            String pattern = "yyyy-MM-dd";
            DateFormat dateFormat = new SimpleDateFormat(pattern);

            Calendar calTomorrow = Calendar.getInstance();
            calTomorrow.setTime(new Date());
            calTomorrow.add(Calendar.DATE, 1);
            Date tommorwDate = calTomorrow.getTime();
            String strCurrentDate = dateFormat.format(tommorwDate);
            tommorwDate = dateFormat.parse(strCurrentDate);
            // Current Date
            searchCriteria.setCurrentDate(tommorwDate);

            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.DAY_OF_YEAR, 1);
            Date start = cal.getTime();
            String strStartDate = dateFormat.format(start);
            start = dateFormat.parse(strStartDate);
            
            //Year Start Date
            searchCriteria.setStartDate(start);

            Calendar calmoth = Calendar.getInstance();
            int month = calmoth.get(Calendar.MONTH);
            calmoth.set(Calendar.MONTH, month);
            calmoth.set(Calendar.DAY_OF_MONTH, 1);
            Date startmonth = calmoth.getTime();
            String strStartMonth = dateFormat.format(startmonth);
            startmonth = dateFormat.parse(strStartMonth);
            //Current Month Start Date
            searchCriteria.setCurrentMonth(startmonth);

        } catch (ParseException e) {
            LOGGER.error("Error while parsing date during retrieveByCountLMS" + e);
        }
		final Request<SearchCriteria> searchCriteriatRequest =
	            new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.LMS,
	                    requestInfo.getTransactionId());
	    searchCriteriatRequest.setType(searchCriteria);
	   
	    final Request<List<CustomerStatusCodeList>> statusRequest =
	            new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.LMS,
	                    requestInfo.getTransactionId());
	    final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.RETRIEVE_BY_COUNT);
	    statusRequest.setType(statusList);
	    
	    	
		final Request<List<CustomerStatusCodeList>> statusRequestClosed =
	            new JPARequestImpl<List<CustomerStatusCodeList>>(getEntityManager(), ContextTypeCodeList.LMS,
	                    requestInfo.getTransactionId());					
				
		final List<CustomerStatusCodeList> statusListClosed = getCustomerStatusCodesFor(GeneraliConstants.RETRIEVE_BY_CLOSED_STATUS);
		statusRequestClosed.setType(statusListClosed);		
		
	
	    final Response<SearchCountResult> searchCountResponse =
	            partyComponent.getCustomerCountRetrieveFilter(searchCriteriatRequest, statusList, statusListClosed);
	
	    SearchCountResult searchCountResult = null;
	    if (searchCountResponse.getType() != null) {
	        searchCountResult = (SearchCountResult) searchCountResponse.getType();
	
	    }
	    return searchCountResult;
	
	}*/
	/**
	 * Method to fetch Lead details of an Illustration for Web
	 * input Transtrackingid of the lead
	 * @param requestInfo
	 * @param searchCriteria
	 * @return
	 */
	public List<Transactions> retrieveRelatedLMSforIllustration(
			RequestInfo requestInfo, SearchCriteria searchCriteria) {
		LOGGER.info(" :::: Entering retrieveByFilter in GeneraliLMSRepository ");
		final List<Transactions> transactionsList = new ArrayList<Transactions>();
		Transactions transaction = null;
		String lmsTrackKey = searchCriteria.getValue();
		Request<String> transTrackReq = new JPARequestImpl<String>(
				entityManager, ContextTypeCodeList.LMS, "");
		//Transtrackingid is the input
		transTrackReq.setType(lmsTrackKey);
		Request<List<CustomerStatusCodeList>> statusRequest = new JPARequestImpl<List<CustomerStatusCodeList>>(
				entityManager, ContextTypeCodeList.LMS, "");
		statusRequest.setType(getCustomerStatusCodesFor(Constants.RETRIEVE));
		Response<Customer> response = partyComponent
				.retrieveCustomerWithTransTrackingId(transTrackReq,
						statusRequest);
		 Customer customer=response.getType();
		 if(customer!=null){
			  transaction = new Transactions();
			  transaction.setCustomer(customer);
			  transaction.setType(GeneraliConstants.LMS);
			  transaction.setRetrieveType(LifeEngageComponentHelper.FULL_DETAILS);
			  transactionsList.add(transaction);
		 }
		return transactionsList;

	}
	
    public Map<String, String> retrieveMonthlyNewCount(RequestInfo requestInfo, SearchCriteria searchCriteria) {
       

        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());

        searchCriteriatRequest.setType(searchCriteria);

        final Map<String, String> searchCriteriatResponse =
                partyComponent.retrieveMonthlyNewCount(searchCriteriatRequest);

        return searchCriteriatResponse;
    }

    public Map<String, Map<String, String>> retrieveLeadStatistics(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        // TODO Auto-generated method stub
        final Request<SearchCriteria> searchCriteriatRequest =
            new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.LMS,
                    requestInfo.getTransactionId());

    searchCriteriatRequest.setType(searchCriteria);

    final Map<String, Map<String, String>> searchCriteriatResponse =
            partyComponent.retrieveLeadStatistics(searchCriteriatRequest);

    return searchCriteriatResponse;
    }
}
