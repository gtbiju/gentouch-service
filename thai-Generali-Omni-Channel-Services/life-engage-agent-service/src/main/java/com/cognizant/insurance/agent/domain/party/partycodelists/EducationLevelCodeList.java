/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partycodelists;

/**
 * Identifies a classification of Persons according to their education level.
 * 
 * @author 301350
 * 
 * 
 */
public enum EducationLevelCodeList {
    /**
     * Identifies a Person with education level 'None'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    None,
    /**
     * General Education Diploma.
     * 
     * 
     * 
     * 
     * 
     * 
     */
    GED,
    /**
     * 
     * 
     * 
     * 
     */
    HighSchool,
    /**
     * An associate degree is an undergraduate academic degree awarded by
     * community colleges, junior colleges, technical colleges, and bachelor's
     * degree-granting colleges and universities upon completion of a course of
     * study usually lasting two years. In the United States, and to some extent
     * in Western Canada, an associate degree is equivalent to the first two
     * years of a four-year college or university degree. It is the lowest in
     * the hierarchy of post-secondary academic degrees offered in these
     * countries. Although an associate degree is not usually as lucrative as a
     * bachelor's degree, the resulting careers can still afford a respectable
     * income with greater job security and much less student debt.
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Associate_degree 
     *
     * 
     * 
     * 
     */
    Associate,
    /**
     * A bachelor's degree is usually an academic degree awarded for an
     * undergraduate course or major that generally lasts for four years, but
     * can range anywhere from three to six years depending on the region of the
     * world. In some exceptional cases, it may also be the name of a
     * postgraduate degree, such as a bachelor of civil law, the bachelor of
     * music, the bachelor of philosophy, or the bachelor of sacred theology
     * degree.
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Bachelor's_degree 
     *
     * 
     * 
     * 
     */
    Bachelors,
    /**
     * A master's degree is an academic degree granted to individuals who have
     * undergone study demonstrating a mastery or high-order overview of a
     * specific field of study or area of professional practice.[1] Within the
     * area studied, graduates are posited to possess advanced knowledge of a
     * specialized body of theoretical and applied topics; high order skills in
     * analysis, critical evaluation or professional application; and the
     * ability to solve complex problems and think rigorously and independently.
     * 
     * In some languages, a master's degree is called a magister, and magister
     * or a cognate can also be used for a person who has the degree. There are
     * various degrees of the same level, such as engineer's degrees, which have
     * different names for historical reasons.
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Masters_degree
     * 
     * 
     * 
     */
    Masters,
    /**
     * Post-graduate education (or graduate education in North America) involves
     * learning and studying for degrees or other qualifications for which a
     * first or Bachelor's degree generally is required, and is normally
     * considered to be part of higher education. In North America, this level
     * is generally referred to as graduate school.
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Post_graduate
     * 
     * 
     * 
     */
    PostGraduate,
    /**
     * Identifies a person with education level 'Professional certification'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    ProfessionalCertification,
    /**
     * Identifies a person with education level 'Second level'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    SecondLevel,
    /**
     * Not Mentioned.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    NotMentioned
}
