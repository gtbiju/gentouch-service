/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.request.vo;

import java.util.Date;

/**
 * The Class class ObservationInfo.
 *
 * @author 345166
 * 
 */
public class ObservationInfo {
    
	private Date creationDate;
	
	private String context;
	
	private String page;
	
	private String path;
	
	private String customerName;
	
	private String email;
	
	private String agent;
	
	private String customerDbId;
	
	private String customerDeviceId;
	
	private String customerId;

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
     * Gets the context.
     *
     * @return Returns the context.
     */
    public String getContext() {
        return context;
    }

    /**
     * Sets The context.
     *
     * @param context The context to set.
     */
    public void setContext(String context) {
        this.context = context;
    }

    /**
	 * @return the page
	 */
	public String getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(String page) {
		this.page = page;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * @param agent the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}

	/**
	 * @return the customerDbId
	 */
	public String getCustomerDbId() {
		return customerDbId;
	}

	/**
	 * @param customerDbId the customerDbId to set
	 */
	public void setCustomerDbId(String customerDbId) {
		this.customerDbId = customerDbId;
	}

	/**
	 * @return the customerDeviceId
	 */
	public String getCustomerDeviceId() {
		return customerDeviceId;
	}

	/**
	 * @param customerDeviceId the customerDeviceId to set
	 */
	public void setCustomerDeviceId(String customerDeviceId) {
		this.customerDeviceId = customerDeviceId;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

		
}
