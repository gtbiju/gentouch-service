/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.agent.domain.documentandcommunication;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.contactandplace.ContactPoint;
import com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationInteractionTypeCodeList;
import com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationPurposeCodeList;
import com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationStatusCodeList;
import com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists.DirectionCodeList;
import com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists.PriorityCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.PartyRole;

/**
 * A record of the receiving or sending (or the intention to send) of a communication between two parties. A
 * communication may be in any medium such as a telephone call, a letter, a fax, an email, a meeting, and so on.
 * 
 * It may have occurred or be planned to occur. In each case, a discrete occurrence of communication is identifiable.
 * Correspondence which takes place over a period of time, is not a single communication but is modeled as a series of
 * related communications.
 * 
 * e.g: ABC bank notifying ABC insurer of the payment made by John Doe into ABC insurer's bank account.
 * 
 * e.g: John Doe's quotation request dated 12/25/2004.
 * 
 * e.g: John Doe's quotation request dated 12/25/1990.
 * 
 * e.g: Product launch campaign mailing to John Doe on 01/01/1997
 * 
 * e.g: Tony Green's complaint faxed 08/11/2003 regarding discrepancy on premium.
 * 
 * e.g: Tony Green's complaint faxed 08/11/2010 regarding discrepancy on premium invoice.
 * 
 * @author 300797
 * 
 * 
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Communication extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4130888776177606829L;

    /**
     * A free-text statement describing the content of the communication.
     * 
     * 
     * 
     */
    @Column(columnDefinition="nvarchar(400)")
    private String communicationDescription;

    /**
     * The current status of the communication within the life cycle model.
     * 
     * e.g: Awaiting further information
     * 
     * e.g: Closed
     * 
     * e.g: Open
     * 
     * 
     * 
     */
    @Enumerated
    private CommunicationStatusCodeList communicationStatusCode;

    /**
     * The date on which the current communication status became valid.
     * 
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date communicationStatusDate;

    /**
     * Indicates a basic reason why a communication is initiated or is planned to be initiated.
     * 
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private CommunicationPurposeCodeList communicationPurposeCode;

    /**
     * The value that specifies the importance of this communication relative to other communications.
     * 
     * e.g: 1 (High Priority)
     * 
     * e.g: 5 (Low Priority)
     * 
     * 
     * 
     */
    @Enumerated
    private PriorityCodeList priorityCode;

    /**
     * The direction of a communication relative to the originator.
     * 
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private DirectionCodeList communicationDirectionTypeCode;

    /**
     * The time period on which the communication is planned.
     * 
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date plannedTimePeriod;

    /**
     * The actual time period(date and time) the communication.
     * 
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualTimePeriod;

    /**
     * The identification of a communication as the subject of a communication effectiveness score.
     */

    /**
     * The identification of a communication responding to another communication.
     * 
     * e.g: A written apology responds to a filed complaint.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Set<Communication> respondsTo;

    /**
     * The identification of a communication following up another communication.
     * 
     * e.g.: A phone call request for brochure about new auto insurance products.
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private Set<Communication> followsUp;

    /**
     * The identification of a communication following up another communication.
     * 
     * e.g.: A phone call request for brochure about new auto insurance products.
     */
    @Transient
    private Set<Communication> isFollowedUpBy;

    /**
     * /** The identification of the communication content of a communication.
     * 
     * e.g: John Doe's communication dated 12/25/2008 has for content a filled out questionnaire for subscribing to a
     * life insurance policy.
     * 
     * e.g: The communication acceptance of offer has for content a signed proposal and a copy of the customer's birth
     * certificate.
     * 
     * 
     * 
     * 
     * 
     */

    /**
     * This relationship represents the sending of a communication to a party playing a role.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Set<PartyRole> receiver;

    /**
     * Indicates who is responsible for sending the communication.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Set<PartyRole> sender;

    /**
     * This relationship links communication with a communication profile.
     * 
     * 
     * 
     * 
     * 
     */

    /**
     * The identification of a contact point as the destination of a communication.
     * 
     * e.g: The communication 'Customer Satisfaction Survey' has for destination contact point, jdoe@acord.org, the
     * email address of the person Mr. John Doe.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Set<ContactPoint> directedContactPoint;

    /**
     * The identification of a contact point as the origin of a communication.
     * 
     * e.g: A phone call, originates from telephone number 555 621 1435, telephone number of the person Mrs. Mary Jane
     * Smith.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private ContactPoint originatingContactPoint;

    /**
     * The identification of a contact point as the address to use for replying to a communication.
     * 
     * e.g: An email has return address of email address jdoe@acord.org.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Set<ContactPoint> repliedToContactPoint;

    /**
     * The identification of a communication sent via a contact point.
     * 
     * e.g: The communication marketing letter is sent via the contact point Insurance Agent's Postal address.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Set<ContactPoint> sendingContactPoint;

    /*
     * Fields added for LMS : Start
     */

    /** The interaction type. */
    private CommunicationInteractionTypeCodeList interactionType;

    /** The is join call. */
    private Boolean isJoinCall;

    /** The join call with. */
    private String joinCallWith;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date actualDateAndTime;
    
    /** Added for Time Format */
    private String plannedTime;

    /*
     * Fields added for LMS : End
     */

    /**
     * Gets the communicationDescription.
     * 
     * @return Returns the communicationDescription.
     */
    public String getCommunicationDescription() {
        return communicationDescription;
    }

    /**
     * Gets the communicationStatusCode.
     * 
     * @return Returns the communicationStatusCode.
     */
    public CommunicationStatusCodeList getCommunicationStatusCode() {
        return communicationStatusCode;
    }

    /**
     * Gets the communicationStatusDate.
     * 
     * @return Returns the communicationStatusDate.
     */
    public Date getCommunicationStatusDate() {
        return communicationStatusDate;
    }

    /**
     * Gets the communicationPurposeCode.
     * 
     * @return Returns the communicationPurposeCode.
     */
    public CommunicationPurposeCodeList getCommunicationPurposeCode() {
        return communicationPurposeCode;
    }

    /**
     * Gets the priorityCode.
     * 
     * @return Returns the priorityCode.
     */
    public PriorityCodeList getPriorityCode() {
        return priorityCode;
    }

    /**
     * Gets the communicationDirectionTypeCode.
     * 
     * @return Returns the communicationDirectionTypeCode.
     */
    public DirectionCodeList getCommunicationDirectionTypeCode() {
        return communicationDirectionTypeCode;
    }

    /**
     * Gets the plannedTimePeriod.
     * 
     * @return Returns the plannedTimePeriod.
     */
    public Date getPlannedTimePeriod() {
        return plannedTimePeriod;
    }

    /**
     * Gets the actualTimePeriod.
     * 
     * @return Returns the actualTimePeriod.
     */
    public Date getActualTimePeriod() {
        return actualTimePeriod;
    }

    /**
     * Gets the respondsTo.
     * 
     * @return Returns the respondsTo.
     */
    public Set<Communication> getRespondsTo() {
        return respondsTo;
    }

    /**
     * Gets the receiver.
     * 
     * @return Returns the receiver.
     */
    public Set<PartyRole> getReceiver() {
        return receiver;
    }

    /**
     * Gets the sender.
     * 
     * @return Returns the sender.
     */
    public Set<PartyRole> getSender() {
        return sender;
    }

    /**
     * Gets the directedContactPoint.
     * 
     * @return Returns the directedContactPoint.
     */
    public Set<ContactPoint> getDirectedContactPoint() {
        return directedContactPoint;
    }

    /**
     * Gets the originatingContactPoint.
     * 
     * @return Returns the originatingContactPoint.
     */
    public ContactPoint getOriginatingContactPoint() {
        return originatingContactPoint;
    }

    /**
     * Gets the repliedToContactPoint.
     * 
     * @return Returns the repliedToContactPoint.
     */
    public Set<ContactPoint> getRepliedToContactPoint() {
        return repliedToContactPoint;
    }

    /**
     * Gets the sendingContactPoint.
     * 
     * @return Returns the sendingContactPoint.
     */
    public Set<ContactPoint> getSendingContactPoint() {
        return sendingContactPoint;
    }

    /**
     * Gets the followsUp.
     * 
     * @return Returns the followsUp.
     */
    public Set<Communication> getFollowsUp() {
        return followsUp;
    }

    /**
     * Sets The communicationDescription.
     * 
     * @param communicationDescription
     *            The communicationDescription to set.
     */
    public void setCommunicationDescription(final String communicationDescription) {
        this.communicationDescription = communicationDescription;
    }

    /**
     * Sets The communicationStatusCode.
     * 
     * @param communicationStatusCode
     *            The communicationStatusCode to set.
     */
    public void setCommunicationStatusCode(final CommunicationStatusCodeList communicationStatusCode) {
        this.communicationStatusCode = communicationStatusCode;
    }

    /**
     * Sets The communicationStatusDate.
     * 
     * @param communicationStatusDate
     *            The communicationStatusDate to set.
     */
    public void setCommunicationStatusDate(final Date communicationStatusDate) {
        this.communicationStatusDate = communicationStatusDate;
    }

    /**
     * Sets The communicationPurposeCode.
     * 
     * @param communicationPurposeCode
     *            The communicationPurposeCode to set.
     */
    public void setCommunicationPurposeCode(final CommunicationPurposeCodeList communicationPurposeCode) {
        this.communicationPurposeCode = communicationPurposeCode;
    }

    /**
     * Sets The priorityCode.
     * 
     * @param priorityCode
     *            The priorityCode to set.
     */
    public void setPriorityCode(final PriorityCodeList priorityCode) {
        this.priorityCode = priorityCode;
    }

    /**
     * Sets The communicationDirectionTypeCode.
     * 
     * @param communicationDirectionTypeCode
     *            The communicationDirectionTypeCode to set.
     */
    public void setCommunicationDirectionTypeCode(final DirectionCodeList communicationDirectionTypeCode) {
        this.communicationDirectionTypeCode = communicationDirectionTypeCode;
    }

    /**
     * Sets The plannedTimePeriod.
     * 
     * @param plannedTimePeriod
     *            The plannedTimePeriod to set.
     */
    public void setPlannedTimePeriod(final Date plannedTimePeriod) {
        this.plannedTimePeriod = plannedTimePeriod;
    }

    /**
     * Sets The actualTimePeriod.
     * 
     * @param actualTimePeriod
     *            The actualTimePeriod to set.
     */
    public void setActualTimePeriod(final Date actualTimePeriod) {
        this.actualTimePeriod = actualTimePeriod;
    }

    /**
     * Sets The respondsTo.
     * 
     * @param respondsTo
     *            The respondsTo to set.
     */
    public void setRespondsTo(final Set<Communication> respondsTo) {
        this.respondsTo = respondsTo;
    }

    /**
     * Sets The receiver.
     * 
     * @param receiver
     *            The receiver to set.
     */
    public void setReceiver(final Set<PartyRole> receiver) {
        this.receiver = receiver;
    }

    /**
     * Sets The sender.
     * 
     * @param sender
     *            The sender to set.
     */
    public void setSender(final Set<PartyRole> sender) {
        this.sender = sender;
    }

    /**
     * Sets The directedContactPoint.
     * 
     * @param directedContactPoint
     *            The directedContactPoint to set.
     */
    public void setDirectedContactPoint(final Set<ContactPoint> directedContactPoint) {
        this.directedContactPoint = directedContactPoint;
    }

    /**
     * Sets The originatingContactPoint.
     * 
     * @param originatingContactPoint
     *            The originatingContactPoint to set.
     */
    public void setOriginatingContactPoint(final ContactPoint originatingContactPoint) {
        this.originatingContactPoint = originatingContactPoint;
    }

    /**
     * Sets The repliedToContactPoint.
     * 
     * @param repliedToContactPoint
     *            The repliedToContactPoint to set.
     */
    public void setRepliedToContactPoint(final Set<ContactPoint> repliedToContactPoint) {
        this.repliedToContactPoint = repliedToContactPoint;
    }

    /**
     * Sets The sendingContactPoint.
     * 
     * @param sendingContactPoint
     *            The sendingContactPoint to set.
     */
    public void setSendingContactPoint(final Set<ContactPoint> sendingContactPoint) {
        this.sendingContactPoint = sendingContactPoint;
    }

    /**
     * Sets The followsUp.
     * 
     * @param followsUp
     *            The followsUp to set.
     */
    public void setFollowsUp(final Set<Communication> followsUp) {
        this.followsUp = followsUp;
    }

    /**
     * Gets the checks if is followed up by.
     * 
     * @return the isFollowedUpBy
     */
    public Set<Communication> getIsFollowedUpBy() {
        return isFollowedUpBy;
    }

    /**
     * Sets the checks if is followed up by.
     * 
     * @param isFollowedUpBy
     *            the isFollowedUpBy to set
     */
    public void setIsFollowedUpBy(Set<Communication> isFollowedUpBy) {
        this.isFollowedUpBy = isFollowedUpBy;
    }

    /**
     * Gets the interaction type.
     * 
     * @return the interactionType
     */
    public CommunicationInteractionTypeCodeList getInteractionType() {
        return interactionType;
    }

    /**
     * Sets the interaction type.
     * 
     * @param interactionType
     *            the interactionType to set
     */
    public void setInteractionType(CommunicationInteractionTypeCodeList interactionType) {
        this.interactionType = interactionType;
    }

    /**
     * Gets the checks if is join call.
     * 
     * @return the isJoinCall
     */
    public Boolean getIsJoinCall() {
        return isJoinCall;
    }

    /**
     * Sets the checks if is join call.
     * 
     * @param isJoinCall
     *            the isJoinCall to set
     */
    public void setIsJoinCall(Boolean isJoinCall) {
        this.isJoinCall = isJoinCall;
    }

    /**
     * Gets the join call with.
     * 
     * @return the joinCallWith
     */
    public String getJoinCallWith() {
        return joinCallWith;
    }

    /**
     * Sets the join call with.
     * 
     * @param joinCallWith
     *            the joinCallWith to set
     */
    public void setJoinCallWith(String joinCallWith) {
        this.joinCallWith = joinCallWith;
    }

	public Date getActualDateAndTime() {
		return actualDateAndTime;
	}

	public void setActualDateAndTime(Date actualDateAndTime) {
		this.actualDateAndTime = actualDateAndTime;
	}

	public String getPlannedTime() {
		return plannedTime;
	}

	public void setPlannedTime(String plannedTime) {
		this.plannedTime = plannedTime;
	}
    
    

}
