/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;

/**
 * The Interface interface LifeEngageSyncService.
 */
public interface LifeEngageSyncService {

    /**
     * Saves the eApp details.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String save(String json) throws BusinessException,CookieTheftException;

    /**
     * Retrieves the eApp details based on criteria.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String retrieve(String json) throws BusinessException;

    /**
     * Retrieve customer list.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String retrieveCustomerList(String json) throws BusinessException;

    /**
     * Save Observations.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @Param json the json
     */
    String saveObservations(String json) throws BusinessException;

    /**
     * Get Status.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @Param json the json
     */
    String getStatus(String json) throws BusinessException;

    /**
     * Retrieve all.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String retrieveAll(String json) throws BusinessException;

    /**
     * Run illustration.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String runIllustration(final String json) throws BusinessException;

    /**
     * Execute Rule.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String executeRule(final String json) throws BusinessException;

    /**
     * Delete.
     * 
     * @param json
     *            the json
     * @return the String
     * @throws BusinessException
     *             the business exception
     */
    String delete(String json) throws BusinessException;

    /**
     * Generate illustration.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String generateIllustration(final String json) throws BusinessException;

    /**
     * Retrieve by filter count.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String retrieveByFilterCount(String json) throws BusinessException;

    /**
     * Retrieve By Filter.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String retrieveByFilter(String json) throws BusinessException;
    
    /**
     * Retrieve By Ids.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String retrieveByIds(String json) throws BusinessException;
}
