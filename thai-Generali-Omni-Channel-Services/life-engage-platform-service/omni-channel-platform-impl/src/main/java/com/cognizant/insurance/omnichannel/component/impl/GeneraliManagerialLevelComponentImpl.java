package com.cognizant.insurance.omnichannel.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.cognizant.insurance.component.impl.LifeEngageSearchCriteriaComponentImpl;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.omnichannel.component.GeneraliManagerialLevelComponent;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliLMSRepository;
import com.cognizant.insurance.omnichannel.vo.HierarchyLevelSearchCriteriaResponse;
import com.cognizant.insurance.omnichannel.vo.ManagerialLevelCriteria;
import com.cognizant.insurance.omnichannel.vo.ManagerialLevelResponse;
import com.cognizant.insurance.omnichannel.vo.ManagerialLevelUserDetails;
import com.cognizant.insurance.omnichannel.vo.SelectedAgentData;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GeneraliManagerialLevelComponentImpl implements GeneraliManagerialLevelComponent {

	@Autowired
	private GeneraliLMSRepository lmsRepository;
	
	 /** The retrieve by managerial holder. */
    @Autowired
    @Qualifier("retrieveByManagerialMapping")
    private LifeEngageSmooksHolder retrieveByManagerialHolder;
	
	/** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliManagerialLevelComponentImpl.class);

    /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = "TransactionData";
	
	@Override
	public String retrieveByManagerialLevelCount(RequestInfo requestInfo,
			JSONArray jsonRqArray) throws BusinessException, ParseException {
		//ManagerialLevelResponse managerialLevelResponse = new ManagerialLevelResponse();
		String response = null;
	    String agentId = null;	     
	    String agentIds = null;
	    String type = null;
	    ArrayList<String> selectedAgentIDs = null;
	    ManagerialLevelUserDetails managerialLevelResponse = new ManagerialLevelUserDetails();
	    ObjectMapper mapper= new ObjectMapper();
		try {
	        if(jsonRqArray != null) {
	            for(int i = 0; i < jsonRqArray.length(); i++) {
	                final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);
	                JSONObject jsonTransactionData = null;
	                ManagerialLevelUserDetails managerialLevelUserDetails = new ManagerialLevelUserDetails();	                
	               //JAXP readValue	                
	                agentId = jsonTransactionsObj.getString(Constants.KEY11);
	               // managerialLevelUserDetails = (ManagerialLevelUserDetails) retrieveByManagerialHolder.parseJson(jsonTransactionsObj.toString());	                              
	                if(!(jsonTransactionsObj.isNull(TRANSACTIONDATA))) {
	                	jsonTransactionData = jsonTransactionsObj.getJSONObject(TRANSACTIONDATA);
                    	final JSONObject managerialLevelCriteriaRqJson =
                                jsonTransactionData.getJSONObject(Constants.SEARCHCRITERIAREQUEST);
                    	JSONArray jsonUserDetailsArray = managerialLevelCriteriaRqJson.getJSONArray("userDetails");
                    	String command = managerialLevelCriteriaRqJson.getString(Constants.COMMAND);
                    	                   	
                    	if(jsonUserDetailsArray.length() > 0) {
                    		//JAXP readValue
                    		managerialLevelUserDetails = mapper.readValue(jsonUserDetailsArray.get(0).toString(), ManagerialLevelUserDetails.class);
                    	}                    	
                    	if("LeadManagerialLevelRequest".equals(command)) {                    			
                    		if(managerialLevelUserDetails != null ) {
                    			List<ManagerialLevelUserDetails> managerialLevelUserDetailslist  = managerialLevelUserDetails.getUserDetails();
                    			if(managerialLevelUserDetailslist != null && managerialLevelUserDetailslist.size() > 0){
                    				int userDetailsSize = managerialLevelUserDetailslist.size();
                    				for(int index = 0; index < userDetailsSize; index ++) {
                    					ManagerialLevelUserDetails managerialLevelUserDetailsInnner =  getInnerUserDetals(requestInfo, managerialLevelUserDetailslist.get(index));
                    					if(managerialLevelUserDetailslist.get(index).getUserType().equals("RM")) {
                    						managerialLevelUserDetailsInnner.setUserType(managerialLevelUserDetailslist.get(index).getUserType());
                    						managerialLevelUserDetailsInnner.setUserId(managerialLevelUserDetailslist.get(index).getUserId());
                    						managerialLevelUserDetailsInnner.setUserName(managerialLevelUserDetailslist.get(index).getUserName());
                    					}
                    					managerialLevelUserDetailslist.set(index, managerialLevelUserDetailsInnner);
                    					
                    					//seperate sum of ASM's data to each AM's
                    					managerialLevelUserDetails = generateCalculations(managerialLevelUserDetails, managerialLevelUserDetailsInnner);                    					
                    				}
                    				
                    				//calculate ratio calculations
                    				if((managerialLevelUserDetails.getAPE_MTD().compareTo(BigDecimal.ZERO) > 0)  && (managerialLevelUserDetails.getClosed_MTD() > 0)) {
                    					managerialLevelUserDetails.setCaseSize_MTD(managerialLevelUserDetails.getAPE_MTD().divide(new BigDecimal(managerialLevelUserDetails.getClosed_MTD()), 2, BigDecimal.ROUND_HALF_UP));
                    				}
                    				if((managerialLevelUserDetails.getAPE_YTD().compareTo(BigDecimal.ZERO) > 0) && (managerialLevelUserDetails.getClosed_YTD()>0)) {
                        				managerialLevelUserDetails.setCaseSize_YTD(managerialLevelUserDetails.getAPE_YTD().divide(new BigDecimal(managerialLevelUserDetails.getClosed_YTD()), 2, BigDecimal.ROUND_HALF_UP));
                    				}
                    				if(managerialLevelUserDetails.getActiveAgents_MTD()>0 && managerialLevelUserDetails.getTotalAgents_MTD()>0) {
                    					managerialLevelUserDetails.setActivityRatio_MTD(new BigDecimal((float)managerialLevelUserDetails.getActiveAgents_MTD()/managerialLevelUserDetails.getTotalAgents_MTD()*100).setScale(2,BigDecimal.ROUND_HALF_UP));
                    				}
                    				if(managerialLevelUserDetails.getActiveAgents_YTD()>0 && managerialLevelUserDetails.getTotalAgents_YTD()>0) {
                        				managerialLevelUserDetails.setActivityRatio_YTD(new BigDecimal((float)managerialLevelUserDetails.getActiveAgents_YTD()/managerialLevelUserDetails.getTotalAgents_YTD()*100).setScale(2,BigDecimal.ROUND_HALF_UP));
                    				}
                    				if((managerialLevelUserDetails.getAPE_MTD().compareTo(BigDecimal.ZERO) > 0) && (managerialLevelUserDetails.getTotalAgents_MTD()>0)) {
                        				managerialLevelUserDetails.setProductivity_MTD(managerialLevelUserDetails.getAPE_MTD().divide(new BigDecimal(managerialLevelUserDetails.getTotalAgents_MTD()), 2, BigDecimal.ROUND_HALF_UP));
                    				}
                    				if((managerialLevelUserDetails.getAPE_YTD().compareTo(BigDecimal.ZERO) > 0) && (managerialLevelUserDetails.getTotalAgents_YTD()>0)) {
                        				managerialLevelUserDetails.setProductivity_YTD(managerialLevelUserDetails.getAPE_YTD().divide(new BigDecimal(managerialLevelUserDetails.getTotalAgents_YTD()), 2, BigDecimal.ROUND_HALF_UP));
                    				}
                    				if(managerialLevelUserDetails.getClosed_MTD()>0 && managerialLevelUserDetails.getTotalLeads_MTD()>0) {
                    					managerialLevelUserDetails.setConversionRate_MTD(new BigDecimal((float)managerialLevelUserDetails.getClosed_MTD()/managerialLevelUserDetails.getTotalLeads_MTD()*100).setScale(2,BigDecimal.ROUND_HALF_UP));
                    				}
                    				if(managerialLevelUserDetails.getClosed_YTD()>0 && managerialLevelUserDetails.getTotalLeads_YTD()>0) {
                    					managerialLevelUserDetails.setConversionRate_YTD(new BigDecimal((float)managerialLevelUserDetails.getClosed_YTD()/managerialLevelUserDetails.getTotalLeads_YTD()*100).setScale(2,BigDecimal.ROUND_HALF_UP));
                    				}
                    				
                    				managerialLevelResponse = managerialLevelUserDetails;
                    				
                    			} else {
                    				managerialLevelResponse = retrieveByCountForManagerial(requestInfo, managerialLevelUserDetails);
   	                    		 	managerialLevelResponse.setUserType(managerialLevelUserDetails.getUserType());
   	                    		 	managerialLevelResponse.setUserId(managerialLevelUserDetails.getUserId());
   	                    		 	managerialLevelResponse.setUserName(managerialLevelUserDetails.getUserName()); 
   	                    		 	managerialLevelResponse.setMobileNumber(managerialLevelUserDetails.getMobileNumber());
                    				//RM
                    			}
	                	     }
	                    } else {
	                    	 throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, "JAXP Parse exception");
	                    }
	                } else {
                        throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, "Transaction Data is not complete");
                    }
	            }
	        }
		} catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
            	
            } else {
            	
            }
		} catch (Exception e) {
            LOGGER.error("Unable to retrieve count by each module :" + e.getMessage());
            
		} finally {				
			response = retrieveByManagerialHolder.parseBO(managerialLevelResponse);
		}
		
		LOGGER.info("responseresponseresponseresponseresponseresponse" + response);
        return response;      			 
	
	}


	private ManagerialLevelUserDetails getInnerUserDetals(RequestInfo requestInfo, ManagerialLevelUserDetails managerialLevelUserDtls) throws BusinessException, ParseException {
		
		 List<ManagerialLevelUserDetails> managerialLevelUserDtlslist  = managerialLevelUserDtls.getUserDetails();
		 if(managerialLevelUserDtlslist != null && managerialLevelUserDtlslist.size() > 0) {
			 List<ManagerialLevelUserDetails> managerialLevelUserDtlsInnnerList = getSecondInnerUserDetals(requestInfo, managerialLevelUserDtlslist);			 
			 int userDtlsSize = managerialLevelUserDtlsInnnerList.size();
			 for(int innerIndex = 0; innerIndex < userDtlsSize; innerIndex ++) {
				 managerialLevelUserDtls = generateCalculations(managerialLevelUserDtls, managerialLevelUserDtlsInnnerList.get(innerIndex));
			}	
			//Calculating MTD and YTD casesize, activity ratio, productivity, and conversionRate for ASM
			 
			    if((managerialLevelUserDtls.getAPE_MTD().compareTo(BigDecimal.ZERO) > 0)  && (managerialLevelUserDtls.getClosed_MTD() > 0)) {
			    	managerialLevelUserDtls.setCaseSize_MTD(managerialLevelUserDtls.getAPE_MTD().divide(new BigDecimal(managerialLevelUserDtls.getClosed_MTD()), 2, BigDecimal.ROUND_HALF_UP));
				}
				if((managerialLevelUserDtls.getAPE_YTD().compareTo(BigDecimal.ZERO) > 0) && (managerialLevelUserDtls.getClosed_YTD()>0)) {
					managerialLevelUserDtls.setCaseSize_YTD(managerialLevelUserDtls.getAPE_YTD().divide(new BigDecimal(managerialLevelUserDtls.getClosed_YTD()), 2, BigDecimal.ROUND_HALF_UP));
				}
				if(managerialLevelUserDtls.getActiveAgents_MTD()>0 && managerialLevelUserDtls.getTotalAgents_MTD()>0) {
					managerialLevelUserDtls.setActivityRatio_MTD(new BigDecimal((float)managerialLevelUserDtls.getActiveAgents_MTD()/managerialLevelUserDtls.getTotalAgents_MTD()*100).setScale(2,BigDecimal.ROUND_HALF_UP));
				}
				if(managerialLevelUserDtls.getActiveAgents_YTD()>0 && managerialLevelUserDtls.getTotalAgents_YTD()>0) {
					managerialLevelUserDtls.setActivityRatio_YTD(new BigDecimal((float)managerialLevelUserDtls.getActiveAgents_YTD()/managerialLevelUserDtls.getTotalAgents_YTD()*100).setScale(2,BigDecimal.ROUND_HALF_UP));
				}
				if((managerialLevelUserDtls.getAPE_MTD().compareTo(BigDecimal.ZERO) > 0) && (managerialLevelUserDtls.getTotalAgents_MTD()>0)) {
					managerialLevelUserDtls.setProductivity_MTD(managerialLevelUserDtls.getAPE_MTD().divide(new BigDecimal(managerialLevelUserDtls.getTotalAgents_MTD()), 2, BigDecimal.ROUND_HALF_UP));
				}
				if((managerialLevelUserDtls.getAPE_YTD().compareTo(BigDecimal.ZERO) > 0) && (managerialLevelUserDtls.getTotalAgents_YTD()>0)) {
					managerialLevelUserDtls.setProductivity_YTD(managerialLevelUserDtls.getAPE_YTD().divide(new BigDecimal(managerialLevelUserDtls.getTotalAgents_YTD()), 2, BigDecimal.ROUND_HALF_UP));
				}
				if(managerialLevelUserDtls.getClosed_MTD()>0 && managerialLevelUserDtls.getTotalLeads_MTD()>0) {
					managerialLevelUserDtls.setConversionRate_MTD(new BigDecimal((float)managerialLevelUserDtls.getClosed_MTD()/managerialLevelUserDtls.getTotalLeads_MTD()*100).setScale(2,BigDecimal.ROUND_HALF_UP));
				}
				if(managerialLevelUserDtls.getClosed_YTD()>0 && managerialLevelUserDtls.getTotalLeads_YTD()>0) {
					managerialLevelUserDtls.setConversionRate_YTD(new BigDecimal((float)managerialLevelUserDtls.getClosed_YTD()/managerialLevelUserDtls.getTotalLeads_YTD()*100).setScale(2,BigDecimal.ROUND_HALF_UP));
				}
				
		} else {
			managerialLevelUserDtls = retrieveByCountForManagerial(requestInfo, managerialLevelUserDtls);			
		}
		
		return managerialLevelUserDtls;
	}
	
	private List<ManagerialLevelUserDetails> getSecondInnerUserDetals(RequestInfo requestInfo, List<ManagerialLevelUserDetails> managerialLevelUserDtlsList) throws BusinessException, ParseException {
		
		int userDtlsSize = managerialLevelUserDtlsList.size();
		for(int innerIndex = 0; innerIndex < userDtlsSize; innerIndex ++) {	
			ManagerialLevelUserDetails managerialLevelUserDtls = managerialLevelUserDtlsList.get(innerIndex);
			managerialLevelUserDtls = retrieveByCountForManagerial(requestInfo, managerialLevelUserDtls);	
			managerialLevelUserDtls.setUserId(managerialLevelUserDtlsList.get(innerIndex).getUserId());
			managerialLevelUserDtls.setUserType(managerialLevelUserDtlsList.get(innerIndex).getUserType());
			managerialLevelUserDtls.setUserName(managerialLevelUserDtlsList.get(innerIndex).getUserName());
			managerialLevelUserDtlsList.set(innerIndex, managerialLevelUserDtls);	
		}		
		return managerialLevelUserDtlsList;
	}


	private ManagerialLevelUserDetails generateCalculations(
			ManagerialLevelUserDetails managerialLevelUserDetails,
			ManagerialLevelUserDetails managerialLevelUserDetailsInnner) {
		
		managerialLevelUserDetails.setActiveAgents_MTD(managerialLevelUserDetails.getActiveAgents_MTD() + managerialLevelUserDetailsInnner.getActiveAgents_MTD());
		managerialLevelUserDetails.setActiveAgents_YTD(managerialLevelUserDetails.getActiveAgents_YTD() + managerialLevelUserDetailsInnner.getActiveAgents_YTD());
		managerialLevelUserDetails.setClosed_MTD(managerialLevelUserDetails.getClosed_MTD() + managerialLevelUserDetailsInnner.getClosed_MTD());
		managerialLevelUserDetails.setClosed_YTD(managerialLevelUserDetails.getClosed_YTD() + managerialLevelUserDetailsInnner.getClosed_YTD());
		managerialLevelUserDetails.setFollowUp_MTD(managerialLevelUserDetails.getFollowUp_MTD() + managerialLevelUserDetailsInnner.getFollowUp_MTD());
		managerialLevelUserDetails.setFollowUp_YTD(managerialLevelUserDetails.getFollowUp_YTD() + managerialLevelUserDetailsInnner.getFollowUp_YTD());
		managerialLevelUserDetails.setNew_MTD(managerialLevelUserDetails.getNew_MTD() + managerialLevelUserDetailsInnner.getNew_MTD());
		managerialLevelUserDetails.setNew_YTD(managerialLevelUserDetails.getNew_YTD() + managerialLevelUserDetailsInnner.getNew_YTD());
		managerialLevelUserDetails.setTotalAgents_MTD(managerialLevelUserDetails.getTotalAgents_MTD() + managerialLevelUserDetailsInnner.getTotalAgents_MTD());
		managerialLevelUserDetails.setTotalAgents_YTD(managerialLevelUserDetails.getTotalAgents_YTD() + managerialLevelUserDetailsInnner.getTotalAgents_YTD());
		managerialLevelUserDetails.setAPE_MTD(managerialLevelUserDetails.getAPE_MTD().add(managerialLevelUserDetailsInnner.getAPE_MTD()));
		managerialLevelUserDetails.setAPE_YTD(managerialLevelUserDetails.getAPE_YTD().add(managerialLevelUserDetailsInnner.getAPE_YTD()));
		managerialLevelUserDetails.setTotalLeads_MTD(managerialLevelUserDetails.getTotalLeads_MTD() + managerialLevelUserDetailsInnner.getTotalLeads_MTD());
		managerialLevelUserDetails.setTotalLeads_YTD(managerialLevelUserDetails.getTotalLeads_YTD() + managerialLevelUserDetailsInnner.getTotalLeads_YTD());
		
		return managerialLevelUserDetails;
		
	}


	protected ManagerialLevelUserDetails retrieveByCountForManagerial(
			final RequestInfo requestInfo, final ManagerialLevelUserDetails managerialLevelUserDetails) throws BusinessException, ParseException {
		LOGGER.trace("Inside GeneraliManagerialLevelComponentImpl retrieveByCountForManagerial : requestInfo" + requestInfo);
		ManagerialLevelUserDetails managerialLevelUserDetailsResponse = new ManagerialLevelUserDetails();
		ArrayList<SelectedAgentData> selectedAgentIds = managerialLevelUserDetails.getSelectedAgentIds();
		
		String pattern = "yyyy-MM-dd";
		DateFormat dateFormat = null;
		dateFormat = new SimpleDateFormat(pattern);
		
		Calendar calTomorrow = Calendar.getInstance();		
		calTomorrow.setTime(new Date());
		calTomorrow.add(Calendar.DATE,1);		
		Date tommorwDate = calTomorrow.getTime();
		String strCurrentDate = dateFormat.format(tommorwDate);
		tommorwDate = dateFormat.parse(strCurrentDate);
		
		
		
	    managerialLevelUserDetails.setCurrentDate(tommorwDate);
		
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_YEAR, 1);
		Date start = cal.getTime();
		String startDate = dateFormat.format(start);
		start = dateFormat.parse(startDate);
		
		managerialLevelUserDetails.setStartDate(start);
		
		Calendar calmoth = Calendar.getInstance();
		int month = calmoth.get(Calendar.MONTH);
		calmoth.set(Calendar.MONTH, month);
		calmoth.set(Calendar.DAY_OF_MONTH, 1);
		Date startmonth = calmoth.getTime();		
		String startMonthDate = dateFormat.format(startmonth);
		startmonth = dateFormat.parse(startMonthDate);
		managerialLevelUserDetails.setCurrentMonth(startmonth);
		
		if(selectedAgentIds.size() > 0) {
			managerialLevelUserDetailsResponse = lmsRepository.retrieveByCountForManagerialLevel(requestInfo, managerialLevelUserDetails);
			
		}
		
		return managerialLevelUserDetailsResponse;
	}

	
}
