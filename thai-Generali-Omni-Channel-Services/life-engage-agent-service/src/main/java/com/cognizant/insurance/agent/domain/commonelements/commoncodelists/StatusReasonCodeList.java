/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Status Reason.
 * 
 * @author 301350
 * 
 * 
 */
public enum StatusReasonCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    InsuredDeath,
    /**
     * 
     * 
     * 
     * 
     */
    PayorDeath,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * 
     * 
     * 
     * 
     */
    PayorDisabled,
    /**
     * A complex change is used to correct fields that are not correctable
     * through normal change transactions. An "enforce record must exist, be
     * active, and suspended. Notes: Further defines OLI_POLSTAT_CTRCTCHANGE
     * (code 31)
     * 
     * 
     * 
     */
    ComplexAgreementAdditionHistoricalAddition,
    /**
     * Identifies a history addition or complex change record originally entered
     * to the file in a pending state and subsequently either released or
     * declined. Notes: Further defines OLI_POLSTAT_CTRCTCHANGE (code 31)
     * 
     * 
     * 
     */
    HistoricalAddition,
    /**
     * Identify a policy undergoing some kind of change, usually benefit
     * additions.
     * 
     * 
     * 
     */
    AgreementAdditionChangeinProgress,
    /**
     * The client did not respond to an offer. There are a specified number of
     * days that the Not Taken status will automatically be applied to the
     * application if there is no communicated response
     * 
     * 
     * 
     * 
     * 
     */
    Noresponsefromapplicant,
    /**
     * The applicant communicated the declination of the offer of a policy.
     * 
     * 
     * 
     * 
     * 
     */
    Applicantdeclinedoffer,
    /**
     * A requested change is denied by underwriter and client does not accept
     * denial of change.
     * 
     * Notes: Use in conjunction with a status reason of rejected <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    ChangeRequestDenied,
    /**
     * If a case is renumbered, the old case becomes inactive.
     * 
     * 
     * 
     */
    Renumbered,
    /**
     * 
     * 
     * 
     * 
     */
    Appeal,
    /**
     * 
     * 
     * 
     * 
     */
    Producernotapprovedtosell,
    /**
     * 
     * 
     * 
     * 
     */
    GroupExpired,
    /**
     * 
     * 
     * 
     * 
     */
    Unapprovedlicensingorappointment,
    /**
     * 
     * 
     * 
     * 
     */
    PVTWSLossofSightorLimbs,
    /**
     * 
     * 
     * 
     * 
     */
    AgreementChangeCausedFreeze,
    /**
     * 
     * 
     * 
     * 
     */
    ProcessingSystemProblemCausedFreeze,
    /**
     * 
     * 
     * 
     * 
     */
    AgreementHolderServiceFreeze
}
