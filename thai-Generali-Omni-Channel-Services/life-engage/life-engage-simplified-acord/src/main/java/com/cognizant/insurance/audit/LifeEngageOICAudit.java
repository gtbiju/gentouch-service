/**
 *
 * Copyright 2018, Cognizant 
 *
 * @author        : 420039
 * @version       : 0.1, Dec 13, 2018
 */
package com.cognizant.insurance.audit;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * The Class class LifeEngageOICAudit.
 *
 * @author 420039
 */
@Entity
public class LifeEngageOICAudit {
    /** The id. */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    /** The file Name. */
    private String fileName;
    
	/** The application number. */
    private String applicationNumber;
	
	/** The file Size. */
    private String fileSize;
	
	/** The file Transfer Rate. */
    private String fileTransferRate;
    
    /** The file transfer type. */
	private String fileTransferType;
	
	/** The file Type. */
	private String fileType;
	
	/** The file transfer completion percentage. */
	private Integer fileTransferCompletionPercentage;
	
	/** The creation dateand time. */
    private String creationDateandTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileTransferRate() {
		return fileTransferRate;
	}

	public void setFileTransferRate(String fileTransferRate) {
		this.fileTransferRate = fileTransferRate;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getCreationDateandTime() {
		return creationDateandTime;
	}

	public void setCreationDateandTime(String creationDateandTime) {
		this.creationDateandTime = creationDateandTime;
	}
	
	public Integer getFileTransferCompletionPercentage() {
		return fileTransferCompletionPercentage;
	}

	public void setFileTransferCompletionPercentage(Integer fileTransferCompletionPercentage) {
		this.fileTransferCompletionPercentage = fileTransferCompletionPercentage;
	}
	
	public String getFileTransferType() {
		return fileTransferType;
	}

	public void setFileTransferType(String fileTransferType) {
		this.fileTransferType = fileTransferType;
	}

	@Override
	public String toString() {
		return "LifeEngageOICAudit [id=" + id + ", fileName=" + fileName + ", applicationNumber=" + applicationNumber
				+ ", fileSize=" + fileSize + ", fileTransferRate=" + fileTransferRate + ", fileTransferType="
				+ fileTransferType + ", fileType=" + fileType + ", fileTransferCompletionPercentage="
				+ fileTransferCompletionPercentage + ", creationDateandTime=" + creationDateandTime + "]";
	}
	
}
