package com.cognizant.generali.security.entity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.cognizant.le.security.entity.UserToken;
@Entity
@NamedQueries({
    @NamedQuery(name = "GeneraliUserToken.fetchTokenDetails", query = "select userTocken from GeneraliUserToken userTocken where userTocken.seriesId = :seriesId") })
public class GeneraliUserToken extends UserToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	

}
