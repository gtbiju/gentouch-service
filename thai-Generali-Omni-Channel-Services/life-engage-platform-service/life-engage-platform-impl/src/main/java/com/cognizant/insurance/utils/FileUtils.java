/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Feb 7, 2014
 */
package com.cognizant.insurance.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * @author 300797
 *
 */
public final class FileUtils {

    private FileUtils() {
        // not called
    }

    /**
     * Encode file to base64 binary.
     * 
     * @param fileName
     *            the file name
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String getFileContent(final String fileName) throws IOException {

        File file = new File(fileName);
        byte[] bytes = FileUtils.loadFile(file);
        String encodedString = new String(bytes);
        encodedString = StringEscapeUtils.escapeJava(encodedString);
        return encodedString;
    }
    /**
     * Load file.
     * 
     * @param file
     *            the file
     * @return the byte[]
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static byte[] loadFile(final File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();

        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }
}
