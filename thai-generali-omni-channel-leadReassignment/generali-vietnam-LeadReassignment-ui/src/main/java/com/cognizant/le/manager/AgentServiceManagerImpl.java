package com.cognizant.le.manager;

import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.cognizant.generali.encryption.util.EncryptionUtil;
import com.cognizant.le.model.dao.AgentServiceDAO;
import com.cognizant.le.model.dto.AgentResponseDTO;
import com.cognizant.le.model.dto.PropertyFileSource;
import com.cognizant.le.util.LoggerUtil;

/**
 * Class AgentServiceManagerImpl
 * @author 481774
 *
 */

@Component
public class AgentServiceManagerImpl implements AgentServiceManager {
    static Logger logger = Logger.getLogger(AgentServiceManagerImpl.class);

    @Autowired
    AgentServiceDAO agentServicedao;

    @Autowired
    PropertyFileSource propertyFileSource;

    @Autowired
    RestTemplate restTemplate;
    
	@Autowired
	EncryptionUtil encryptionUtil;
    /**
     * Method to check User Role
     * @param userid
     * @param password
     * @return userType
     * @throws Exception
     */
    public String isValidUser(String userid, String password) throws Exception {
        String userType = "";
        logger.debug(LoggerUtil.getLogMessage("Method Entry : AgentServiceManagerImpl.isValidUser  for Agent:", userid));

        try {
            userType = agentServicedao.isValidUser(userid, password);

        } catch (Exception e) {
            logger.error(LoggerUtil.getLogMessage("Error while getting isValidUser Class for Agent:", userid), e);
            throw new Exception(e);
        }
        logger.debug(LoggerUtil.getLogMessage("Method : AgentServiceManagerImpl.isValidUsere  Agent:", userid));
        logger.debug(LoggerUtil.getLogMessage("Method : AgentServiceManagerImpl.checkUserFlag  Agent:", userType + ""));
        logger.debug(LoggerUtil.getLogMessage("Method Exit : AgentServiceManagerImpl.isValidUsere  Agent:", userid));

        return userType;
    }
    /**
     * Method to get Agent Details
     * @param userid
     * @param password
     * @return branchRMResp
     * @throws Exception
     */
    @Override
    public JSONObject getAgentDetails(String userid,String userAction) throws Exception {
        JSONObject branchRMResp = null;
        JSONObject vendorRMResp = null;
        String agentProfileURL = propertyFileSource.getAgentprofile();
        if (agentProfileURL != null && agentProfileURL.endsWith(".json")) {
            /* Mock Service */
            return getMockAgentDetails();
        } else {
            /* Third party service of RM to get agent details */
            JSONObject branchRequest = null;
            if (userid != null && !"".equalsIgnoreCase(userid)) {
                String rmReq = buildRMRequest(userid,userAction);
                if (rmReq != null) {
                    try {
                    	
                        vendorRMResp = getVendorResponse(rmReq, agentProfileURL);
                        
                    } catch (Exception e) {
                        logger.error("Error while getting the third party response " + e.getMessage());
                    }
                }

            }
        }
        return vendorRMResp;
    }
    
    /**
     * Method to build branch request
     * @param agentDetailsList
     * @return null
     */

    private JSONObject buildRetrieveBranchRequest(List<AgentResponseDTO> agentDetailsList) {
        try {
            // Getting request structure
            String url = "/agentProfile/Agent_Request.json";
            String request = new Scanner(this.getClass().getResourceAsStream(url)).useDelimiter("\\Z").next();
            JSONObject jsonObject = new JSONObject(request);
            JSONArray reqTransactionArray =
                    jsonObject.getJSONObject("Request").getJSONObject("RequestPayLoad").getJSONArray("Transactions");

            // Getting key structure
            String keysUrl = "/agentProfile/KeyStructure.json";
            String keysRequest = new Scanner(this.getClass().getResourceAsStream(keysUrl)).useDelimiter("\\Z").next();
            JSONObject keyJsonObject = null;
            keyJsonObject = new JSONObject(keysRequest);
            JSONObject txnObject = null;

            if (agentDetailsList != null && agentDetailsList.size() > 0) {
                for (AgentResponseDTO agentDTO : agentDetailsList) {
                    String transaction = keyJsonObject.toString();
                    transaction = transaction.replace("--AgentId--", agentDTO.getAgentId());
                    txnObject = new JSONObject(transaction);
                    reqTransactionArray.put(txnObject);
                }
            }
            return jsonObject;
        } catch (Exception ex) {
            logger.error("Error while building branch request");
        }
        return null;
    }
    /**
     * Method to read Branch Response
     * @return jsonObject
     * @throws JSONException
     */
    private JSONObject getMockAgentDetails() throws JSONException {
        String url = "/agentProfile/BranchResponse.json";
        String request = new Scanner(this.getClass().getResourceAsStream(url)).useDelimiter("\\Z").next();
        JSONObject jsonObject = new JSONObject(request);
        return jsonObject;
    }
    /**
     * Method to get Agent Details from GLINK
     * @param profileReq
     * @param agentProfileURL
     * @return respObj
     * @throws Exception
     */
    private JSONObject getVendorResponse(String profileReq, String agentProfileURL) throws Exception {
        JSONObject respObj = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Content-Type", "application/json; charset=utf-8");
		headers.add("Accept","application/json;charset=utf-8");
        //headers.add("source", "100");
        //headers.add("Token", "RHg0ZUhrWW9jVDV3cXkxK2tWb2tlUT09OmV5SmhiR2NpT2lKSVV6STFOaUo5LmV5SnpkV0lpT2lKVVQwdEZUam94TURBaUxDSmhkV1FpT2lJd01EQXdOemMyTmlJc0ltbGhkQ0k2TVRVeU5UTXlOVE0yTlN3aWFYTnpJam9pVEVVaWZRLllRSTBWQWk2cE9IT2VKWFBGM2ZqQmRFb3VqcWdJd3E0aHc2YktnTG5xTGc");
        String encryptedRmReq=encryptionUtil.encryptString(profileReq);
        HttpEntity<String> httpRequest = new HttpEntity<String>(profileReq, headers);
        ResponseEntity<String> response;
        String responseBodyString="";
        String responseBodyJson="";
        try {
        	UriComponentsBuilder builderInstance = UriComponentsBuilder.fromHttpUrl(agentProfileURL);

			//restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
            //response = restTemplate.postForEntity(builderInstance.build(), httpRequest, String.class);
			response = restTemplate.exchange(builderInstance.build().toUriString(),
					HttpMethod.POST, httpRequest, String.class);
            responseBodyJson=response.getBody();
            //responseBodyString= new Gson().fromJson(responseBodyJson, String.class);
        } catch (RestClientException e) {
            throw new Exception("Unable to complete Agent profile Service request - RestClient Exception", e);
        } catch (Exception e) {
            throw new Exception("Unable to complete Agent profile Service request", e);
        }
        if (response.getStatusCode().value() != 200) {
            logger.error("Vendor Service error : Status " + response.getStatusCode() + " - " + response.getBody());
            throw new Exception("Unable to complete Agent profile Service request : Failed to process the response");
        } else {
            //String resp = encryptionUtil.decryptString(responseBodyString);
            respObj = new JSONObject(responseBodyJson);
        }
        return respObj;
    }
    /**
     * Method to build request for RM user
     * @param userId
     * @return response
     */
    private String buildRMRequest(String userId,String userAction) {
        String response = "";
        try {
            String agentId = userId;
            String url = "/agentProfile/vendor_RM_Request.json";
            response = new Scanner(this.getClass().getResourceAsStream(url)).useDelimiter("\\Z").next();
            response = response.replaceAll("--AgentId--", agentId);
            response = response.replaceAll("--UserAction--", userAction);
        } catch (Exception e) {
            logger.error("Error while building RM request ");
        }
        return response;
    }

}
