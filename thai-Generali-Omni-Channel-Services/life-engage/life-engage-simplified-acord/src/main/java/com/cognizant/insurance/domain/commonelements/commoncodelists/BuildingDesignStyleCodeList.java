/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of Structures according to their design style.
 * 
 * @author 301350
 * 
 * 
 */
public enum BuildingDesignStyleCodeList {
    /**
     * Identifies a Structure with design style 'Duplex'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Duplex,
    /**
     * Identifies a Structure with design style 'High rise multiple units'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    HighRiseMultipleUnits,
    /**
     * Identifies a Structure with design style 'Low rise multiple units'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    LowRiseMultipleUnits,
    /**
     * Identifies a Structure with design style 'Single family detached four
     * square'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    SingleFamilyDetachedFourSquare,
    /**
     * Identifies a Structure with design style 'Single family detached ranch'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    SingleFamilyDetachedRanch,
    /**
     * Identifies a Structure with design style 'Single family row home'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    SingleFamilyRowHome
}
