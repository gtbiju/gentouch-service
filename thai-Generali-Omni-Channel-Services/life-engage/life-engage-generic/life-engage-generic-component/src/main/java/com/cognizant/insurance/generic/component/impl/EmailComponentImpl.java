package com.cognizant.insurance.generic.component.impl;

import java.util.List;

import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.generic.component.EmailComponent;
import com.cognizant.insurance.generic.dao.EmailDao;
import com.cognizant.insurance.generic.dao.impl.EmailDaoImpl;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;

/**
 * The Class class EmailComponentImpl.
 *
 */
public class EmailComponentImpl implements  EmailComponent {

    @Override
    public <T> Response<Object> save(Request<T> request) {
        final EmailDao emailDao = new EmailDaoImpl();
        emailDao.save(request);
        
        final Response<Object> response = new ResponseImpl<Object>();
        response.setType(request.getType());
        
        return response;
    }
    
    @Override
    public <T> void update(Request<T> request) {
        final EmailDao emailDao = new EmailDaoImpl();
        emailDao.merge(request);
    }
    
    @Override
    public Response<List<LifeEngageEmail>> retrieveFailedLifeEngageEmailDetails(Request<LifeEngageEmail> request, String maxAttemptNo) {
        Response<List<LifeEngageEmail>> response = new ResponseImpl<List<LifeEngageEmail>>();
        final EmailDao emailDao = new EmailDaoImpl();
        response.setType(emailDao.retrieveFailedLifeEngageEmailDetails(request,maxAttemptNo));
        return response;
    }
    
    @Override
    public Response<List<LifeEngageEmail>> retrieveInitialisedLifeEngageEmailDetails(Request<LifeEngageEmail> request) {
        Response<List<LifeEngageEmail>> response = new ResponseImpl<List<LifeEngageEmail>>();
        final EmailDao emailDao = new EmailDaoImpl();
        response.setType(emailDao.retrieveInitialisedLifeEngageEmailDetails(request));
        return response;
    }
    
}
