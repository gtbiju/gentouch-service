/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.familymembersubtypes;

import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.FamilyMember;

/**
 * This concept represents a parent's sibling (e.g. aunt, uncle).
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class ParentSibling extends FamilyMember {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7988866671247679609L;
}
