/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */
package com.cognizant.insurance.searchcriteria;

import java.io.Serializable;
import java.util.Date;

import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.roleandrelationship.PartyRole;

/**
 * The Class class PartySearchCriteria.
 */
public class PartySearchCriteria implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4231420976201640238L;

    /** The first name. */
    private String firstName;

    /** The last name. */
    private String lastName;

    /** The id. */
    private Long id;
    
    /** The code list name. */
    private String codeListName;
    
    /** The role to be found. */
    private PartyRole roleToBeFound;

    /** The context id. */
    private ContextTypeCodeList contextId;

    /** The transaction id. */
    private String transactionId;
 
    /** The type name. */
    private String typeName;
    
    /** The Authentication ID. */
    private String authenticationID;
    
    /** The dob date. */
    private Date birthDate;
    
    /** The policy no. */
    private String policyNo;

    /** The email. */
    private String email;

    /**
     * Gets the firstName.
     * 
     * @return Returns the firstName.
     */
    public final String getFirstName() {
        return firstName;
    }

    /**
     * Gets the id.
     * 
     * @return Returns the id.
     */
    public final Long getId() {
        return id;
    }

    /**
     * Gets the lastName.
     * 
     * @return Returns the lastName.
     */
    public final String getLastName() {
        return lastName;
    }

    /**
     * Sets The firstName.
     * 
     * @param firstName
     *            The firstName to set.
     */
    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Sets The id.
     * 
     * @param id
     *            The id to set.
     */
    public final void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets The lastName.
     * 
     * @param lastName
     *            The lastName to set.
     */
    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the codeListName.
     *
     * @return Returns the codeListName.
     */
    public final String getCodeListName() {
        return codeListName;
    }

    /**
     * Sets The codeListName.
     *
     * @param codeListName The codeListName to set.
     */
    public final void setCodeListName(final String codeListName) {
        this.codeListName = codeListName;
    }

    /**
     * Sets The roleToBeFound.
     *
     * @param roleToBeFound The roleToBeFound to set.
     */
    public final void setRoleToBeFound(final PartyRole roleToBeFound) {
        this.roleToBeFound = roleToBeFound;
    }

    /**
     * Gets the roleToBeFound.
     *
     * @return Returns the roleToBeFound.
     */
    public final PartyRole getRoleToBeFound() {
        return roleToBeFound;
    }

    /**
     * Gets the contextId.
     *
     * @return Returns the contextId.
     */
    public final ContextTypeCodeList getContextId() {
        return contextId;
    }

    /**
     * Sets The contextId.
     *
     * @param contextId The contextId to set.
     */
    public final void setContextId(final ContextTypeCodeList contextId) {
        this.contextId = contextId;
    }

    /**
     * Gets the transactionId.
     *
     * @return Returns the transactionId.
     */
    public final String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets The transactionId.
     *
     * @param transactionId The transactionId to set.
     */
    public final void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Sets The typeName.
     *
     * @param typeName The typeName to set.
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * Gets the typeName.
     *
     * @return Returns the typeName.
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Gets the authentication id.
     *
     * @return the authentication id
     */
    public String getAuthenticationID() {
        return authenticationID;
    }

    /**
     * Sets the authentication id.
     *
     * @param authenticationID the new authentication id
     */
    public void setAuthenticationID(String authenticationID) {
        this.authenticationID = authenticationID;
    }

    /**
     * Gets the policy no.
     *
     * @return the policy no
     */
    public String getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the policy no.
     *
     * @param policyNo the new policy no
     */
    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Sets The email.
     *
     * @param email The email to set.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the email.
     *
     * @return Returns the email.
     */
    public String getEmail() {
        return email;
    }

}
