package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ScoringDataVo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The scoreKey **/
    private String scoreKey;

    /** The columnHeader **/
    private String columnHeader;

    /** The total **/
    private String total;

    /** The call **/
    private String call;

    /** The appointment **/
    private String appointment;

    /** The visiting **/
    private String visiting;

    /** The presentation **/
    private String presentation;

    /** The newProspect **/
    private String newProspect;

    /** The closeCase **/
    private String closeCase;
    
    Map<String,String> appointmentCountMap = new LinkedHashMap<String,String>();
    
    Map<String,String> callCountMap = new LinkedHashMap<String,String>();
    
    Map<String,String> visitCountMap = new LinkedHashMap<String,String>();
    
    Map<String,String> presentationCountMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> newProspectCountMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> closeCountMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> totalPercentPerformanceMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> totalCountMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringCallColorMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringVisitColorMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringAppointmentColorMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringNewProspectColorMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringPresentationColorMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringCloseCaseColorMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringTotalColorMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringTotalPercentPerformanceColorMap =  new LinkedHashMap<String,String>();
    
    Map<String,String> appointmentCountMapInPercent = new LinkedHashMap<String,String>();
    
    Map<String,String> callCountMapInPercent = new LinkedHashMap<String,String>();
    
    Map<String,String> visitCountMapInPercent = new LinkedHashMap<String,String>();
    
    Map<String,String> presentationCountMapInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> closeCountMapInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> newProspectCountInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> totalCountInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringCallColorMapInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringVisitColorMapInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringAppointmentColorMapInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringNewProspectColorMapInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringPresentationColorMapInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringCloseCaseColorMapInPercent =  new LinkedHashMap<String,String>();
    
    Map<String,String> scoringTotalColorMapInPercent =  new LinkedHashMap<String,String>();

    public String getScoreKey() {
        return scoreKey;
    }

    public void setScoreKey(String scoreKey) {
        this.scoreKey = scoreKey;
    }

    public String getColumnHeader() {
        return columnHeader;
    }

    public void setColumnHeader(String columnHeader) {
        this.columnHeader = columnHeader;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    public String getVisiting() {
        return visiting;
    }

    public void setVisiting(String visiting) {
        this.visiting = visiting;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getNewProspect() {
        return newProspect;
    }

    public void setNewProspect(String newProspect) {
        this.newProspect = newProspect;
    }

    public String getCloseCase() {
        return closeCase;
    }

    public void setCloseCase(String closeCase) {
        this.closeCase = closeCase;
    }

    public Map<String, String> getAppointmentCountMap() {
        return appointmentCountMap;
    }

    public Map<String, String> getCallCountMap() {
        return callCountMap;
    }

    public Map<String, String> getVisitCountMap() {
        return visitCountMap;
    }

    public Map<String, String> getPresentationCountMap() {
        return presentationCountMap;
    }

    public Map<String, String> getCloseCountMap() {
        return closeCountMap;
    }

    public void setAppointmentCountMap(Map<String, String> appointmentCountMap) {
        this.appointmentCountMap = appointmentCountMap;
    }

    public void setCallCountMap(Map<String, String> callCountMap) {
        this.callCountMap = callCountMap;
    }

    public void setVisitCountMap(Map<String, String> visitCountMap) {
        this.visitCountMap = visitCountMap;
    }

    public void setPresentationCountMap(Map<String, String> presentationCountMap) {
        this.presentationCountMap = presentationCountMap;
    }

    public void setCloseCountMap(Map<String, String> closeCountMap) {
        this.closeCountMap = closeCountMap;
    }

    public Map<String, String> getTotalPercentPerformanceMap() {
        return totalPercentPerformanceMap;
    }

    public void setTotalPercentPerformanceMap(Map<String, String> totalPercentPerformanceMap) {
        this.totalPercentPerformanceMap = totalPercentPerformanceMap;
    }

	public Map<String, String> getNewProspectCountMap() {
		return newProspectCountMap;
	}

	public void setNewProspectCountMap(Map<String, String> newProspectCountMap) {
		this.newProspectCountMap = newProspectCountMap;
	}

	public Map<String, String> getTotalCountMap() {
		return totalCountMap;
	}

	public void setTotalCountMap(Map<String, String> totalCountMap) {
		this.totalCountMap = totalCountMap;
	}

	public Map<String, String> getScoringCallColorMap() {
		return scoringCallColorMap;
	}

	public void setScoringCallColorMap(Map<String, String> scoringCallColorMap) {
		this.scoringCallColorMap = scoringCallColorMap;
	}

	public Map<String, String> getScoringVisitColorMap() {
		return scoringVisitColorMap;
	}

	public void setScoringVisitColorMap(Map<String, String> scoringVisitColorMap) {
		this.scoringVisitColorMap = scoringVisitColorMap;
	}

	public Map<String, String> getScoringAppointmentColorMap() {
		return scoringAppointmentColorMap;
	}

	public void setScoringAppointmentColorMap(Map<String, String> scoringAppointmentColorMap) {
		this.scoringAppointmentColorMap = scoringAppointmentColorMap;
	}

	public Map<String, String> getScoringNewProspectColorMap() {
		return scoringNewProspectColorMap;
	}

	public void setScoringNewProspectColorMap(Map<String, String> scoringNewProspectColorMap) {
		this.scoringNewProspectColorMap = scoringNewProspectColorMap;
	}

	public Map<String, String> getScoringTotalColorMap() {
		return scoringTotalColorMap;
	}

	public void setScoringTotalColorMap(Map<String, String> scoringTotalColorMap) {
		this.scoringTotalColorMap = scoringTotalColorMap;
	}

    public Map<String, String> getCallCountMapInPercent() {
        return callCountMapInPercent;
    }

    public Map<String, String> getVisitCountMapInPercent() {
        return visitCountMapInPercent;
    }

    public Map<String, String> getPresentationCountMapInPercent() {
        return presentationCountMapInPercent;
    }

    public Map<String, String> getCloseCountMapInPercent() {
        return closeCountMapInPercent;
    }

    public void setCallCountMapInPercent(Map<String, String> callCountMapInPercent) {
        this.callCountMapInPercent = callCountMapInPercent;
    }

    public void setVisitCountMapInPercent(Map<String, String> visitCountMapInPercent) {
        this.visitCountMapInPercent = visitCountMapInPercent;
    }

    public void setPresentationCountMapInPercent(Map<String, String> presentationCountMapInPercent) {
        this.presentationCountMapInPercent = presentationCountMapInPercent;
    }

    public void setCloseCountMapInPercent(Map<String, String> closeCountMapInPercent) {
        this.closeCountMapInPercent = closeCountMapInPercent;
    }

    public Map<String, String> getAppointmentCountMapInPercent() {
        return appointmentCountMapInPercent;
    }

    public void setAppointmentCountMapInPercent(Map<String, String> appointmentCountMapInPercent) {
        this.appointmentCountMapInPercent = appointmentCountMapInPercent;
    }

	public Map<String, String> getScoringCallColorMapInPercent() {
		return scoringCallColorMapInPercent;
	}

	public void setScoringCallColorMapInPercent(Map<String, String> scoringCallColorMapInPercent) {
		this.scoringCallColorMapInPercent = scoringCallColorMapInPercent;
	}

	public Map<String, String> getScoringVisitColorMapInPercent() {
		return scoringVisitColorMapInPercent;
	}

	public void setScoringVisitColorMapInPercent(Map<String, String> scoringVisitColorMapInPercent) {
		this.scoringVisitColorMapInPercent = scoringVisitColorMapInPercent;
	}

	public Map<String, String> getScoringAppointmentColorMapInPercent() {
		return scoringAppointmentColorMapInPercent;
	}

	public void setScoringAppointmentColorMapInPercent(Map<String, String> scoringAppointmentColorMapInPercent) {
		this.scoringAppointmentColorMapInPercent = scoringAppointmentColorMapInPercent;
	}

	public Map<String, String> getScoringNewProspectColorMapInPercent() {
		return scoringNewProspectColorMapInPercent;
	}

	public void setScoringNewProspectColorMapInPercent(Map<String, String> scoringNewProspectColorMapInPercent) {
		this.scoringNewProspectColorMapInPercent = scoringNewProspectColorMapInPercent;
	}

	public Map<String, String> getScoringTotalColorMapInPercent() {
		return scoringTotalColorMapInPercent;
	}

	public void setScoringTotalColorMapInPercent(Map<String, String> scoringTotalColorMapInPercent) {
		this.scoringTotalColorMapInPercent = scoringTotalColorMapInPercent;
	}

	public Map<String, String> getNewProspectCountInPercent() {
		return newProspectCountInPercent;
	}

	public void setNewProspectCountInPercent(Map<String, String> newProspectCountInPercent) {
		this.newProspectCountInPercent = newProspectCountInPercent;
	}

	public Map<String, String> getTotalCountInPercent() {
		return totalCountInPercent;
	}

	public void setTotalCountInPercent(Map<String, String> totalCountInPercent) {
		this.totalCountInPercent = totalCountInPercent;
	}

	public Map<String, String> getScoringTotalPercentPerformanceColorMap() {
		return scoringTotalPercentPerformanceColorMap;
	}

	public void setScoringTotalPercentPerformanceColorMap(Map<String, String> scoringTotalPercentPerformanceColorMap) {
		this.scoringTotalPercentPerformanceColorMap = scoringTotalPercentPerformanceColorMap;
	}

	public Map<String, String> getScoringPresentationColorMap() {
		return scoringPresentationColorMap;
	}

	public void setScoringPresentationColorMap(Map<String, String> scoringPresentationColorMap) {
		this.scoringPresentationColorMap = scoringPresentationColorMap;
	}

	public Map<String, String> getScoringCloseCaseColorMap() {
		return scoringCloseCaseColorMap;
	}

	public void setScoringCloseCaseColorMap(Map<String, String> scoringCloseCaseColorMap) {
		this.scoringCloseCaseColorMap = scoringCloseCaseColorMap;
	}

	public Map<String, String> getScoringPresentationColorMapInPercent() {
		return scoringPresentationColorMapInPercent;
	}

	public void setScoringPresentationColorMapInPercent(Map<String, String> scoringPresentationColorMapInPercent) {
		this.scoringPresentationColorMapInPercent = scoringPresentationColorMapInPercent;
	}

	public Map<String, String> getScoringCloseCaseColorMapInPercent() {
		return scoringCloseCaseColorMapInPercent;
	}

	public void setScoringCloseCaseColorMapInPercent(Map<String, String> scoringCloseCaseColorMapInPercent) {
		this.scoringCloseCaseColorMapInPercent = scoringCloseCaseColorMapInPercent;
	}

}
