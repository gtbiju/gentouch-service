package com.cognizant.insurance.generic.component.impl;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.cognizant.insurance.domain.AutoEtrGenerateAudit;
import com.cognizant.insurance.generic.component.AutoEtrGenerateAuditComponent;
import  com.cognizant.insurance.generic.dao.AutoEtrGenerateAuditDao;
import com.cognizant.insurance.generic.dao.PushNotificationDao;
import com.cognizant.insurance.generic.dao.impl.AutoEtrGenerateAuditDaoImpl;
import com.cognizant.insurance.generic.dao.impl.PushNotificationDaoImpl;
import com.cognizant.insurance.pushnotification.DeviceRegister;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
@Component
public class AutoEtrGenerateAuditComponentImpl implements AutoEtrGenerateAuditComponent {	 

	@PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;	
	
	@Override
	public <T> Response<Object> save(Request<T> request) {
		 final AutoEtrGenerateAuditDao paymentDetails = new AutoEtrGenerateAuditDaoImpl();
	        paymentDetails.save(request);
	        
	        final Response<Object> response = new ResponseImpl<Object>();
	        response.setType(request.getType());
	       
	        return response;
	}	
	
	@Override
    public List<AutoEtrGenerateAudit> retrieveAutoEtrGenerateAudit(String eappNo) {
		 final AutoEtrGenerateAuditDao dao = new AutoEtrGenerateAuditDaoImpl();
	    	List<AutoEtrGenerateAudit> auditList = dao.retrieveAutoEtrGenerateAudit(eappNo, entityManager);
	    	return auditList;
    }
	 @Override
	    public void updateAutoETROnSave(Request<AutoEtrGenerateAudit> request) {

		 final AutoEtrGenerateAuditDao dao = new AutoEtrGenerateAuditDaoImpl();
		 dao.updateAutoETROnSave(request);

	    }
}
