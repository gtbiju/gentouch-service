/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Dec 9, 2013
 */
package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;

// TODO: Auto-generated Javadoc
/**
 * The Interface interface LifeEngageProductService.
 */
public interface LifeEngageProductService {

    /**
     * Gets the product.
     *
     * @param json the json
     * @param version the version
     * @return the product
     * @throws BusinessException the business exception
     */
    String getProduct(final String json, String version) throws BusinessException;

    /**
     * Get the products details.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String getProducts(final String json) throws BusinessException;

    /**
     * Get the active products details.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String getActiveProducts(final String json) throws BusinessException;

    /**
     * Used to check whether a product is active or not.
     *
     * @param json the json
     * @param version the version
     * @return the string
     * @throws BusinessException the business exception
     */
    String isProductActive(final String json, String version) throws BusinessException;
    
    /**
     * Get all products by filter : carrierId & ChannelId.
     *
     * @param json the json
     * @param version the version
     * @return the string
     * @throws BusinessException the business exception
     */
    String getAllProductsByFilter(final String json, String version) throws BusinessException;

    /**
     * Get all active products by filter : carrierId & ChannelId.
     *
     * @param json the json
     * @param version the version
     * @return the string
     * @throws BusinessException the business exception
     */
    String getAllActiveProductsByFilter(final String json, String version) throws BusinessException;

    /**
     * Gets the products by filter.
     *
     * @param json the json
     * @param version
     * @return the products by filter
     * @throws BusinessException the business exception
     */
    String getProductsByFilter(final String json, String version) throws BusinessException;
    /**
     * Gets the product templates by filter.
     * 
     * @param json
     *            the json
     * @return the product templates by filter
     * @throws BusinessException
     *             the business exception
     */
    String getProductTemplatesByFilter(final String json) throws BusinessException;

}
