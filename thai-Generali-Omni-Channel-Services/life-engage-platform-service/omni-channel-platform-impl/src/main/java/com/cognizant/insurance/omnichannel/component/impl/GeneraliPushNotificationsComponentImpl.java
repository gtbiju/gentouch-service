package com.cognizant.insurance.omnichannel.component.impl;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.omnichannel.component.GeneraliPushNotificationsComponent;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliPushNotificationRepository;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;

@Component
public class GeneraliPushNotificationsComponentImpl implements GeneraliPushNotificationsComponent {

    @Autowired
    private GeneraliPushNotificationRepository pushNotificationRepository;

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliPushNotificationsComponentImpl.class);

    @Override
    @Async
    public void pushNotifications(HashMap<String, Integer> agentMap,
			RequestInfo requestInfo,String type,String message)  {
    	for (String agentId : agentMap.keySet()) {
			try {
				Integer count = agentMap.get(agentId);
				pushNotificationRepository.sendPushNotification(agentId, type,count,
						requestInfo,message);
			} catch (ParseException e) {
				LOGGER.error("ParseException in sending pushNotification to agent"
						+ agentId + e.getMessage());
				
			} catch (IOException e) {
				LOGGER.error("IOException in sending pushNotification to agent"
						+ agentId + e.getMessage());
				
			} catch (SystemException e) {
				LOGGER.error("SystemException", e);
				final StatusData statusData = new StatusData();
				statusData.setStatus(Constants.FAILURE);
			}
		}
		LOGGER.info("EXIT:pushNotification");

       
    }

}
