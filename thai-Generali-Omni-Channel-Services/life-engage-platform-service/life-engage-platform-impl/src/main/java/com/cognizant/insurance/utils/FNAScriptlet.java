/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Oct 7, 2015
 */
package com.cognizant.insurance.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;

/**
 * The Class class FNAScriptlet.
 */
public class FNAScriptlet extends JRDefaultScriptlet {

    /** The goal and need. */
    private GoalAndNeed goalAndNeed;

    /** The request json object. */
    private JSONObject requestJSONObject;

    /** The myself json object. */
    private JSONObject myselfJSONObject;

    /** The json party obj. */
    private JSONObject jsonPartyObj;

    /** The agent. */
    private Agent agent;

    /** The Constant FNA_MY_SELF. */
    private static final String FNA_MY_SELF = "FNAMyself";

    /** The Constant BASICDETAILS. */
    private static final String BASICDETAILS = "BasicDetails";

    /** The Constant LIFESTAGE. */
    private static final String LIFESTAGE = "lifeStage";

    /** The Constant CONTACTDETAILS. */
    private static final String CONTACTDETAILS = "ContactDetails";

    /** The Constant RISKPROFILE. */
    private static final String RISKPROFILE = "riskProfile";

    /** The Constant AGENT. */
    private static final String AGENT = "Agent";

    /*
     * (non-Javadoc)
     * 
     * @see net.sf.jasperreports.engine.JRDefaultScriptlet#beforeDetailEval()
     */
    public void beforeDetailEval() throws JRScriptletException {
        goalAndNeed = (GoalAndNeed) this.getFieldValue("goalAndNeed");

        agent = (Agent) this.getFieldValue("agent");

        String requestJSON = goalAndNeed.getRequestJSON();
        try {
            requestJSONObject = new JSONObject(requestJSON);

            JSONArray partiesArray = requestJSONObject.getJSONArray("parties");
            if (partiesArray != null) {
                for (int i = 0; i < partiesArray.length(); i++) {
                    jsonPartyObj = partiesArray.getJSONObject(i);
                    if (jsonPartyObj.has("type") && FNA_MY_SELF.equals(jsonPartyObj.getString("type"))) {
                        myselfJSONObject = jsonPartyObj;
                    }
                }

            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Gets the myself name.
     * 
     * @return the myself name
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getMyselfName() throws JRScriptletException {
        return ((JSONObject) myselfJSONObject.get(BASICDETAILS)) != null ? ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get(
                "firstName")
            .toString()
                : "";

    }

    /**
     * Gets the fNA completion.
     * 
     * @return the fNA completion
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getFNACompletion() throws JRScriptletException {
        return requestJSONObject.get("fnaCompletion") != null ? requestJSONObject.get("fnaCompletion").toString() : "";
    }

    /**
     * Gets the agent name.
     * 
     * @return the agent name
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getAgentName() throws JRScriptletException {
        return agent.getFullname() != null ? agent.getFullname() : "";
    }

    /**
     * Gets the description.
     * 
     * @return the description
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getDescription() throws JRScriptletException {
        return ((JSONObject) requestJSONObject.get(LIFESTAGE)) != null ? ((JSONObject) requestJSONObject.get(LIFESTAGE)).get(
                "description")
            .toString()
                : "";

    }

    /**
     * Gets the address.
     * 
     * @return the address
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getAddress() throws JRScriptletException {
        String address = "";
        if (null != myselfJSONObject.get(CONTACTDETAILS)) {
            JSONObject contactJSONObj = (JSONObject) myselfJSONObject.get(CONTACTDETAILS);
            address = ((JSONObject) contactJSONObj.get("currentAddress")).get("address").toString();
        }
        return address;
    }

    /**
     * Gets the risk profile.
     * 
     * @return the risk profile
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getRiskProfile() throws JRScriptletException {
        return ((JSONObject) requestJSONObject.get(RISKPROFILE)) != null ? ((JSONObject) requestJSONObject.get(RISKPROFILE)).get(
                "level")
            .toString()
                : "";
    }

    /**
     * Gets the contact.
     * 
     * @return the contact
     * @throws JRScriptletException
     *             the jR scriptlet exception
     */
    public String getContact() throws JRScriptletException {
        return ((JSONObject) myselfJSONObject.get(CONTACTDETAILS)) != null ? ((JSONObject) myselfJSONObject.get(CONTACTDETAILS)).get(
                "homeNumber1")
            .toString()
                : "";
    }

    /**
     * Gets the age.
     * 
     * @return the age
     * @throws JRScriptletException
     *             the jR scriptlet exception
     * @throws ParseException
     *             the parse exception
     */
    public String getAge() throws JRScriptletException, ParseException {
        Integer age = null;
        if (myselfJSONObject.get(BASICDETAILS) != null) {
            JSONObject basicDetailsJsonObj = (JSONObject) myselfJSONObject.get(BASICDETAILS);
            String birthDate = basicDetailsJsonObj.getString("dob").toString();
            Calendar dob = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date = format.parse(birthDate);
            dob.setTime(date);
            Calendar today = Calendar.getInstance();
            age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
            if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR)) {
                age--;
            }
        }
        return age.toString();
    }

    /**
     * Gets the image.
     * 
     * @return the image
     */
    public String getImage() {
        String image = "";
        String temptoken = null;
        if (myselfJSONObject.get(BASICDETAILS) != null) {
            JSONObject basicDetailsJsonObj = (JSONObject) myselfJSONObject.get(BASICDETAILS);
            image = basicDetailsJsonObj.getString("photo").toString();
            StringTokenizer token = new StringTokenizer(image, ",");
            while (token.hasMoreTokens()) {
                temptoken = token.nextToken();
            }
            if (temptoken != null) {
                image = temptoken;
            }
        }
        return image;
    }
}
