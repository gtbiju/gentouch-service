/**
 * 
 */
package com.cognizant.insurance.domain.party.persondetailsubtypes;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.domain.commonelements.commoncodelists.FrequencyCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.party.partycodelists.IncomeTypeCodeList;
import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;

/**
 * This represents general information regarding income details for a given
 * person.
 * 
 * Usage Note: Use as many instances of this as necessary to adequately
 * represent a person's income.
 * 
 * Example 1: Person with two different incomes (1) Salary (2) Investment
 * 
 * Example 2: Person with salary income for multiple years (1) Salary,
 * effectivePeriod=current year (2) Salary, effective Period=prior year (3) Etc.
 * 
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("INCOMEDETAIL")
public class IncomeDetail extends PersonDetail {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 116330457605371650L;

    /**
     * The most recent known gross income of this person. This amount includes
     * salaries and non-salary income (e.g. stock sale, property sale, etc.).
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount grossAmount;

    /**
     * The most recent known net income of this person (gross income - taxes and
     * other deductions = net income). 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount netAmount;

    /**
     * The amount of money that this person can spend after having paid all the
     * fixed expenses such as rent, mortgage repayment and so on. This
     * information should be provided by the person when not all composing
     * elements for calculating the disposable income are available.
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
	private CurrencyAmount disposableAmount;

    /**
     * A code indicating the applicable type of income.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private IncomeTypeCodeList typeCode;

    /**
     * A code indicating the frequency of income. 
     * 
     * 
     * 
     */
    @Enumerated
    private FrequencyCodeList frequencyCode;

    /** The Estimated gross annual other income. */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount estGrossAnnualOtherIncome;
    
    /******Newly added for EAp - ********/
    
    /**
     * An indicator that specifies if income proof has been submitted. 
     */
    private Boolean incomeProofSubmittedIndicator;
    
     /**
     * Specifies the document that is submitted as income proof. 
     */
    private String incomeProof;
    
    /** The Estimated gross annual income of spouse. */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount annualIncomeOfSpouse;

    /**
     * Specifies the Permanent Account Number. 
     */
    private String permanentAccountNumber;
    
    /**
     * Specifies the nature Of Work Of Parent/Spouse 
     */
    private String natureOfWorkOfParentOrSpouse;


    /******Newly added for EAp - End ********/
    /**
     * Newly added for generali.
     */
    private String description;

    private String grossIncome;

    private String grossIncomeDesc;
    
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGrossIncome() {
        return grossIncome;
    }

    public void setGrossIncome(String grossIncome) {
        this.grossIncome = grossIncome;
    }

    public String getGrossIncomeDesc() {
        return grossIncomeDesc;
    }

    public void setGrossIncomeDesc(String grossIncomeDesc) {
        this.grossIncomeDesc = grossIncomeDesc;
    }

    /**
     * Gets the grossAmount.
     * 
     * @return Returns the grossAmount.
     */
    public CurrencyAmount getGrossAmount() {
        return grossAmount;
    }

    /**
     * Gets the netAmount.
     * 
     * @return Returns the netAmount.
     */
    public CurrencyAmount getNetAmount() {
        return netAmount;
    }

    /**
     * Gets the disposableAmount.
     * 
     * @return Returns the disposableAmount.
     */
    public CurrencyAmount getDisposableAmount() {
        return disposableAmount;
    }

    /**
     * Gets the typeCode.
     * 
     * @return Returns the typeCode.
     */
    public IncomeTypeCodeList getTypeCode() {
        return typeCode;
    }

    /**
     * Gets the frequencyCode.
     * 
     * @return Returns the frequencyCode.
     */
    public FrequencyCodeList getFrequencyCode() {
        return frequencyCode;
    }

    /**
     * Sets The grossAmount.
     * 
     * @param grossAmount
     *            The grossAmount to set.
     */
    public void setGrossAmount(final CurrencyAmount grossAmount) {
        this.grossAmount = grossAmount;
    }

    /**
     * Sets The netAmount.
     * 
     * @param netAmount
     *            The netAmount to set.
     */
    public void setNetAmount(final CurrencyAmount netAmount) {
        this.netAmount = netAmount;
    }

    /**
     * Sets The disposableAmount.
     * 
     * @param disposableAmount
     *            The disposableAmount to set.
     */
    public void setDisposableAmount(final CurrencyAmount disposableAmount) {
        this.disposableAmount = disposableAmount;
    }

    /**
     * Sets The typeCode.
     * 
     * @param typeCode
     *            The typeCode to set.
     */
    public void setTypeCode(final IncomeTypeCodeList typeCode) {
        this.typeCode = typeCode;
    }

    /**
     * Sets The frequencyCode.
     * 
     * @param frequencyCode
     *            The frequencyCode to set.
     */
    public void setFrequencyCode(final FrequencyCodeList frequencyCode) {
        this.frequencyCode = frequencyCode;
    }

    /**
     * Sets The estGrossAnnualOtherIncome.
     *
     * @param estGrossAnnualOtherIncome The estGrossAnnualOtherIncome to set.
     */
    public void setEstGrossAnnualOtherIncome(final CurrencyAmount estGrossAnnualOtherIncome) {
        this.estGrossAnnualOtherIncome = estGrossAnnualOtherIncome;
    }

    /**
     * Gets the estGrossAnnualOtherIncome.
     *
     * @return Returns the estGrossAnnualOtherIncome.
     */
    public CurrencyAmount getEstGrossAnnualOtherIncome() {
        return estGrossAnnualOtherIncome;
    }

    /**
     * Gets the incomeProofSubmittedIndicator.
     *
     * @return Returns the incomeProofSubmittedIndicator.
     */
    public final Boolean getIncomeProofSubmittedIndicator() {
        return incomeProofSubmittedIndicator;
    }

    /**
     * Gets the incomeProof.
     *
     * @return Returns the incomeProof.
     */
    public final String getIncomeProof() {
        return incomeProof;
    }

    /**
     * Gets the annualIncomeOfSpouse.
     *
     * @return Returns the annualIncomeOfSpouse.
     */
    public final CurrencyAmount getAnnualIncomeOfSpouse() {
        return annualIncomeOfSpouse;
    }

    /**
     * Gets the permanentAccountNumber.
     *
     * @return Returns the permanentAccountNumber.
     */
    public final String getPermanentAccountNumber() {
        return permanentAccountNumber;
    }

    /**
     * Gets the natureOfWorkOfParentOrSpouse.
     *
     * @return Returns the natureOfWorkOfParentOrSpouse.
     */
    public final String getNatureOfWorkOfParentOrSpouse() {
        return natureOfWorkOfParentOrSpouse;
    }

    /**
     * Sets The incomeProofSubmittedIndicator.
     *
     * @param incomeProofSubmittedIndicator The incomeProofSubmittedIndicator to set.
     */
    public final void setIncomeProofSubmittedIndicator(
            Boolean incomeProofSubmittedIndicator) {
        this.incomeProofSubmittedIndicator = incomeProofSubmittedIndicator;
    }

    /**
     * Sets The incomeProof.
     *
     * @param incomeProof The incomeProof to set.
     */
    public final void setIncomeProof(String incomeProof) {
        this.incomeProof = incomeProof;
    }

    /**
     * Sets The annualIncomeOfSpouse.
     *
     * @param annualIncomeOfSpouse The annualIncomeOfSpouse to set.
     */
    public final void setAnnualIncomeOfSpouse(CurrencyAmount annualIncomeOfSpouse) {
        this.annualIncomeOfSpouse = annualIncomeOfSpouse;
    }

    /**
     * Sets The permanentAccountNumber.
     *
     * @param permanentAccountNumber The permanentAccountNumber to set.
     */
    public final void setPermanentAccountNumber(String permanentAccountNumber) {
        this.permanentAccountNumber = permanentAccountNumber;
    }

    /**
     * Sets The natureOfWorkOfParentOrSpouse.
     *
     * @param natureOfWorkOfParentOrSpouse The natureOfWorkOfParentOrSpouse to set.
     */
    public final void setNatureOfWorkOfParentOrSpouse(
            String natureOfWorkOfParentOrSpouse) {
        this.natureOfWorkOfParentOrSpouse = natureOfWorkOfParentOrSpouse;
    }

}
