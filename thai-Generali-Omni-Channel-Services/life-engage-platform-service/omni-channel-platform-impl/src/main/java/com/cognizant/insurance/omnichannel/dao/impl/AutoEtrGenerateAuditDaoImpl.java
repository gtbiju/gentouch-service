package com.cognizant.insurance.omnichannel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.omnichannel.dao.AutoEtrGenerateAuditDao;
import com.cognizant.insurance.omnichannel.domain.AutoEtrGenerateAudit;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;

public class AutoEtrGenerateAuditDaoImpl extends DaoImpl implements AutoEtrGenerateAuditDao {
	@PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;	

	@Override
	public List<AutoEtrGenerateAudit> retrieveAutoEtrGenerateAudit(String eappNo) {

    	String query;
    	final Request<AutoEtrGenerateAudit> autoEtrGenerateAuditRequest =
                new JPARequestImpl<AutoEtrGenerateAudit>(entityManager, null, "");
     // query = "select id, application_no, paymentDate, product_id, premium, payor_role, application_date, cash_payment, source_of_data, payor_email,  policy_holder,  payor_name, pay_for, sum_assured ,agent_code ,mobile ,etrDate ,dob_policy_holder ,split ,etrNumber ,bankName ,bankBranchName ,bankAccountNumber ,branchCode ,bankBranchCode ,splitPayment from AutoEtrGenerateAudit where application_no = ?1";
      query = "select id, application_no, paymentDate, product_id, premium,payor_role, application_date, cash_payment, source_of_data, payor_email, policy_holder, payor_name, pay_for, sum_assured, agent_code, mobile, etrDate, dob_policy_holder, etrNumber, bankName, bankBranchName, bankAccountNumber, branchCode, bankBranchCode, splitPayment, ispaymentDone from AutoEtrGenerateAudit where application_no = ?1";
      List<AutoEtrGenerateAudit> autoEtrGenerateAuditObjectList = (List<AutoEtrGenerateAudit>) findByQuery(autoEtrGenerateAuditRequest, query, eappNo);
        /*List<AutoEtrGenerateAudit> autoEtrGenerateAuditList = new ArrayList<AutoEtrGenerateAudit>();
        for(AutoEtrGenerateAudit autoEtrGenerateAudit: autoEtrGenerateAuditObjectList) {
        	autoEtrGenerateAuditList.add(autoEtrGenerateAudit);
        }*/
    	return autoEtrGenerateAuditObjectList;
    
	}

}
