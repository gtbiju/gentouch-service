/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 356551
 * @version       : 0.1, Mar 10, 2016
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.insurance.component.LifeEngageEncryptionKeyComponent;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.services.EncryptionKeyService;

/**
 * The Class class EncryptionKeyServiceImpl.
 */
@Service("encryptionKeyService")
public class EncryptionKeyServiceImpl implements EncryptionKeyService {

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

   
    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

   
    /** The life engage push notification component. */
    @Autowired
    private LifeEngageEncryptionKeyComponent lifeEngageEncryptionKeyComponent;

    
    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(EncryptionKeyServiceImpl.class);

   /* (non-Javadoc)
    * @see com.cognizant.insurance.services.EncryptionKeyService#fetchKey(java.lang.String)
    */
   @Override
    public final String fetchKey(final String json) throws BusinessException {
        JSONObject jsonObject;
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        String fetchKeyRs = null;
        try {
            jsonObject = new JSONObject(json);

            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
               fetchKeyRs =
                	lifeEngageEncryptionKeyComponent.fetchKey();
                jsonRsArray.put(new JSONObject(fetchKeyRs));
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.error("Parse exception while registering device" + e);
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }

    
}
