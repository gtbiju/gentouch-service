/**
 * 
 */
package com.cognizant.insurance.agent.domain.finance.paymentmethodsubtypes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.finance.financialactivitysubtypes.PaymentMethod;

/**
 * This concept states a cash payment .
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("CASH")
public class CashPayment extends PaymentMethod {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 105159734484877146L;
}
