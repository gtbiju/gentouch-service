package com.cognizant.insurance.omnichannel.component;

import java.text.ParseException;
import java.util.List;

import org.json.JSONArray;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.vo.AgentReportVo;
import com.cognizant.insurance.request.vo.RequestInfo;

public interface GeneraliSalesActivityComponent {
    
    String retrieveSalesActivityDetails(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException,
            ParseException;
    List<AgentReportVo> getAgentReport(String mode) ;

}
