package com.cognizant.insurance.omnichannel.service;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.domain.TinyUrlGenerator;

public interface GeneraliPaymentService {

	String retrievePaymentService(String json) throws BusinessException;

	String makePayment(String json) throws BusinessException;
	
	TinyUrlGenerator makePaymentViaUrl(String uuid,String id) throws BusinessException;

	//String getCardPaymentStatus(String invoice) throws BusinessException;
	
	String getUrlPaymentStatus(String json) throws BusinessException;

}
