/**
 * 
 */
package com.cognizant.insurance.domain.party.persondetailsubtypes;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;
import com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes.HouseholdRelationship;

/**
 * This concept provides family details for a person.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("FAMILYDETAIL")
public class FamilyDetail extends PersonDetail {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2666020702408726364L;

    /**
     * Number of children of a person.
     * 
     * 
     * 
     * 
     * 
     */
    private BigDecimal childrenCount;

    /**
     * This relationship links a family detail to a household relationship.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private HouseholdRelationship providingHousehold;
    
    
    private Boolean parentIndicator;
    
    private Boolean spouseIndicator;

    /**
     * Gets the childrenCount.
     * 
     * @return Returns the childrenCount.
     */
    public BigDecimal getChildrenCount() {
        return childrenCount;
    }

    /**
     * Gets the providingHousehold.
     * 
     * @return Returns the providingHousehold.
     */
    public HouseholdRelationship getProvidingHousehold() {
        return providingHousehold;
    }

    /**
     * Sets The childrenCount.
     * 
     * @param childrenCount
     *            The childrenCount to set.
     */
    public void setChildrenCount(final BigDecimal childrenCount) {
        this.childrenCount = childrenCount;
    }

    /**
     * Sets The providingHousehold.
     * 
     * @param providingHousehold
     *            The providingHousehold to set.
     */
    public void setProvidingHousehold(
            final HouseholdRelationship providingHousehold) {
        this.providingHousehold = providingHousehold;
    }

	public Boolean getParentIndicator() {
		return parentIndicator;
	}

	public void setParentIndicator(Boolean parentIndicator) {
		this.parentIndicator = parentIndicator;
	}

	public Boolean getSpouseIndicator() {
		return spouseIndicator;
	}

	public void setSpouseIndicator(Boolean spouseIndicator) {
		this.spouseIndicator = spouseIndicator;
	}
    

}
