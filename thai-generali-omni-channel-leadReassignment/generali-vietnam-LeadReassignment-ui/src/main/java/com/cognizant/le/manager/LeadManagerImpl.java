package com.cognizant.le.manager;

import java.util.Random;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.cognizant.le.model.dto.PropertyFileSource;

@Component
public class LeadManagerImpl implements LeadManager {

	static Logger logger = Logger.getLogger(AgentServiceManagerImpl.class);

	@Autowired
	PropertyFileSource propertyFileSource;

	@Autowired
	RestTemplate restTemplate;
	
	/** The million. */
    private static final int MILLION = 100000;

    /** The random. */
    private static final int RANDOM = 900000;
    
    /** The Constant generator. */
    private static final Random GENERATOR = new Random();
    
    public static final String LEADREASSIGN = "leadReAssign";

	@Override
	public String getLeads(String agentId) {

		String LEAD_RETRIEVE_REQUEST_URL = propertyFileSource
				.getLeadRetrieveUrl();

		String LEAD_RETRIEVE_REQUEST = buildLeadRetrieveRequest(agentId);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Content-Type", "application/json;charset=utf-8");
		headers.add("Accept","application/json;charset=utf-8");
		HttpEntity<String> httpRequest = new HttpEntity<String>(
				LEAD_RETRIEVE_REQUEST, headers);
		ResponseEntity<String> response = restTemplate.postForEntity(
				LEAD_RETRIEVE_REQUEST_URL, httpRequest, String.class);
		return response.getBody();

	}

	private String buildLeadRetrieveRequest(String agentId) {

		JSONObject jsonObject = new JSONObject();
		try {
			String url = "/agentProfile/Service_Request.json";
			String request = new Scanner(this.getClass().getResourceAsStream(
					url)).useDelimiter("\\Z").next();
			jsonObject = new JSONObject(request);
			JSONArray reqTransactionArray = jsonObject.getJSONObject("Request")
					.getJSONObject("RequestPayload")
					.getJSONArray("Transactions");

			String keysUrl = "/agentProfile/RetrieveLeadRqKeyStructure.json";
			String keysRequest = new Scanner(this.getClass()
					.getResourceAsStream(keysUrl)).useDelimiter("\\Z").next();
			JSONObject keyJsonObject = new JSONObject(keysRequest);
			JSONObject txnObject = null;

			String transaction = keyJsonObject.toString();
			transaction = transaction.replace("--AgentId--", agentId);
			//transaction = transaction.replace("--BranchCode--", branchCode);
			transaction = transaction.replace("--ReassignKey--", "ReAssign");
			transaction = transaction.replace("--Type--", "LMS");
			transaction = transaction.replace("--UserAction--", "displayLeads");
			txnObject = new JSONObject(transaction);
			reqTransactionArray.put(txnObject);

		} catch (Exception ex) {
			logger.error("Error while building Lead Retrieve request");
		}

		return jsonObject.toString();
	}
	
	public Long generate() {
		return (long) (MILLION + GENERATOR.nextInt(RANDOM));
	}

	@Override
	public String getLeadsAndUpdate(String[] leadIds, String reAssignAgentId,
 String agentId) {

		// String updateSaveResponseBody = "FAILURE";
		String LEAD_RETRIEVE_REQUEST_URL = propertyFileSource.getLeadRetrieveRqUrl();
		// loop
		String reqHeaders = "/agentProfile/Service_Request.json";
		String finalResponse = "FAILURE";
		String illustrationResponse = StringUtils.EMPTY;
		try {

			String request = new Scanner(this.getClass().getResourceAsStream(reqHeaders)).useDelimiter("\\Z").next();
			JSONObject reqJsonObject = new JSONObject(request);
			JSONArray reqTransactionArray = reqJsonObject.getJSONObject("Request").getJSONObject("RequestPayload")
					.getJSONArray("Transactions");

			for (int i = 0; i < leadIds.length; i++) {

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				headers.add("Content-Type", "application/json;charset=utf-8");
				headers.add("Accept", "application/json;charset=utf-8");

				String LEAD_RETRIEVE_REQUEST = buildLeadRetrieveResponse(leadIds[i], reAssignAgentId);
				HttpEntity<String> httpRequest = new HttpEntity<String>(LEAD_RETRIEVE_REQUEST, headers);
				ResponseEntity<String> response = restTemplate.postForEntity(LEAD_RETRIEVE_REQUEST_URL, httpRequest,
						String.class);
				String leadStatus = getResponseStatus(response.getBody());
				String newTranTrackingId = getTransTrackingId(response.getBody());

				String FNA_REASSIGN_REQUEST = buildReassignRequest(leadIds[i], reAssignAgentId, agentId,"FNA",newTranTrackingId);
				HttpEntity<String> fnaRequest = new HttpEntity<String>(FNA_REASSIGN_REQUEST, headers);
				ResponseEntity<String> fnaServiceresponse = restTemplate.postForEntity(LEAD_RETRIEVE_REQUEST_URL,
						fnaRequest, String.class);
				String fnaStatus = getResponseStatus(fnaServiceresponse.getBody());
				
				String ILLUSTRATION_REASSIGN_REQUEST = buildReassignRequest(leadIds[i], reAssignAgentId, agentId,"illustration",newTranTrackingId);
				HttpEntity<String> illustrationRequest = new HttpEntity<String>(ILLUSTRATION_REASSIGN_REQUEST, headers);
				ResponseEntity<String> illustrationServiceresponse = restTemplate.postForEntity(LEAD_RETRIEVE_REQUEST_URL,
						illustrationRequest, String.class);
				String illustrationStatus = getResponseStatus(illustrationServiceresponse.getBody());
				
				String EAPP_REASSIGN_REQUEST = buildReassignRequest(leadIds[i], reAssignAgentId, agentId,"eApp",newTranTrackingId);
				HttpEntity<String> eappRequest = new HttpEntity<String>(EAPP_REASSIGN_REQUEST, headers);
				ResponseEntity<String> eappServiceresponse = restTemplate.postForEntity(LEAD_RETRIEVE_REQUEST_URL,
						eappRequest, String.class);
				String eappStatus = getResponseStatus(eappServiceresponse.getBody());

				if (leadStatus.equalsIgnoreCase("SUCCESS") && fnaStatus.equalsIgnoreCase("SUCCESS")) {
					finalResponse = response.getBody();
				}

			}
		} catch (JSONException e) {
			logger.error("Error while Saving  Lead Reassign request");
		}

		return finalResponse;

	}
	
	private String getTransTrackingId(String leadResponse) throws JSONException {
		JSONObject leadResObj = new JSONObject(leadResponse);
		JSONObject transObj = (JSONObject) leadResObj.getJSONObject("Response").getJSONObject("ResponsePayload").getJSONArray("Transactions").get(0);
		return transObj.getString("TransTrackingID");
		
	}

	private String getResponseStatus(String leadResponse) throws JSONException {
		JSONObject leadResObj = new JSONObject(leadResponse);
		JSONObject transObj = (JSONObject) leadResObj.getJSONObject("Response").getJSONObject("ResponsePayload").getJSONArray("Transactions").get(0);
		return transObj.getJSONObject("StatusData").getString("Status");
	}

	private String buildLeadRetrieveResponse(String leadId,String reAssignAgentId) {

		JSONObject jsonObject = new JSONObject();
		try {
			String url = "/agentProfile/Service_Request.json";
			String request = new Scanner(this.getClass().getResourceAsStream(
					url)).useDelimiter("\\Z").next();
			jsonObject = new JSONObject(request);
			JSONArray reqTransactionArray = jsonObject.getJSONObject("Request")
					.getJSONObject("RequestPayload")
					.getJSONArray("Transactions");

			String keysUrl = "/agentProfile/RetrieveLeadRq.json";
			String keysRequest = new Scanner(this.getClass()
					.getResourceAsStream(keysUrl)).useDelimiter("\\Z").next();
			JSONObject keyJsonObject = new JSONObject(keysRequest);
			JSONObject txnObject = null;
			String newTransTrackingId = LEADREASSIGN+"-"+reAssignAgentId+"-"+String.valueOf(generate())+"-"+String.valueOf(generate());

			String transaction = keyJsonObject.toString();
			transaction = transaction.replace("--LeadId--", leadId);
			transaction = transaction.replace("--AgentId--", reAssignAgentId);
			transaction = transaction.replace("--Type--", "LMS");
			transaction = transaction.replace("--transTrackingID--", newTransTrackingId);
			transaction = transaction.replace("--lmsTransTrackingId--", "");
			txnObject = new JSONObject(transaction);
			reqTransactionArray.put(txnObject);

		} catch (Exception ex) {
			logger.error("Error while building Lead Retrieve request");
		}

		return jsonObject.toString();
	}
	
	private String buildReassignRequest(String leadId,String reAssignAgentId,String agentId,String type,String lmsTranTrackingId) {

		JSONObject jsonObject = new JSONObject();
		try {
			String url = "/agentProfile/Service_Request.json";
			String request = new Scanner(this.getClass().getResourceAsStream(
					url)).useDelimiter("\\Z").next();
			jsonObject = new JSONObject(request);
			JSONArray reqTransactionArray = jsonObject.getJSONObject("Request")
					.getJSONObject("RequestPayload")
					.getJSONArray("Transactions");

			String keysUrl = "/agentProfile/RetrieveLeadRq.json";
			String keysRequest = new Scanner(this.getClass()
					.getResourceAsStream(keysUrl)).useDelimiter("\\Z").next();
			JSONObject keyJsonObject = new JSONObject(keysRequest);
			JSONObject txnObject = null;

			String transaction = keyJsonObject.toString();
			transaction = transaction.replace("--LeadId--", leadId);
			transaction = transaction.replace("--AgentId--", agentId);
			transaction = transaction.replace("--reassignAgentId--", reAssignAgentId);
			transaction = transaction.replace("--Type--", type);
			transaction = transaction.replace("--lmsTransTrackingId--", lmsTranTrackingId);
			transaction = transaction.replace("--transTrackingID--", lmsTranTrackingId);
			txnObject = new JSONObject(transaction);
			reqTransactionArray.put(txnObject);

		} catch (Exception ex) {
			logger.error("Error while building Lead Retrieve request");
		}

		return jsonObject.toString();
	}

}
