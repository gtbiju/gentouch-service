package com.cognizant.insurance.omnichannel.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.LifeEngageDocumentComponent;
import com.cognizant.insurance.component.repository.ProductRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliDocumentComponent;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.service.impl.DocumentServiceImpl;
import com.cognizant.insurance.utils.Base64Utils;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class GeneraliDocumentServiceImpl extends DocumentServiceImpl {

    @Autowired
    private GeneraliDocumentComponent lifeEngageDocumentComponent;

    @Autowired
    private ProductRepository productRepository;
    
    @Value("${generali.platform.bpm.staticPDFPath}")
    private String staticPDFPath;
    
    @Value("${le.platform.service.fileUploadPath}")
    private String fileUploadPath;
    
    @Value("${generali.platform.bpm.commonMemoTemplateID}")
    private String commonMemoTemplateId;
    
    @Value("${generali.platform.bpm.COFMemoTemplateID}")
    private String memoCOFTemplateId;
    
    @Value("${le.platform.default.eAppTemplate}")
	private String eAppTemplateId;
    
    private final static String PDF = ".pdf";

    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly = true)
    public String generateBase64StringPdf(final String json) throws BusinessException {
        String base64pdf = "";
        String type = "";
        String proposalNumber = "";
        JSONObject jsonObject;

        final JSONObject jsonObjectRs = new JSONObject();

        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            final JSONArray jsonRsArray = new JSONArray();
            ByteArrayOutputStream byteStream = null;
            if (jsonRqArray.length() > 0) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    JSONObject jsonObj = jsonRqArray.getJSONObject(i);
                    JSONObject responseJsonObj = new JSONObject();
                    type = jsonObj.getString(TYPE);                   
                    String templateId = "";
                    try {
                        if (E_APP.equals(jsonObj.getString(TYPE))) {

                           /* Response<List<ProductTemplateMapping>> templateResponse =
                                    productRepository.getProductTemplatesByFilter(buildProductSearchCriteria(jsonObj));
                            List<ProductTemplateMapping> templateMappings = templateResponse.getType();

                            if (templateMappings != null && !templateMappings.isEmpty()) {
                                for (ProductTemplateMapping productTemplateMapping : templateMappings) {
                                    templateId = productTemplateMapping.getProductTemplates().getTemplateId();
                                }

                            }*/
                            templateId = eAppTemplateId;    
                            proposalNumber = (String) jsonObj.get(KEY4);
                            // get your file as InputStream
                            byteStream =
                                    (ByteArrayOutputStream) lifeEngageDocumentComponent.generatePdfGet(proposalNumber,
                                            type, templateId);

                        } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
                            /* Getting IllustartionOutput object from json */

                            Response<List<ProductTemplateMapping>> templateResponse =
                                    productRepository.getProductTemplatesByFilter(buildProductSearchCriteria(jsonObj));
                            List<ProductTemplateMapping> templateMappings = templateResponse.getType();

                            if (templateMappings != null && !templateMappings.isEmpty()) {
                                for (ProductTemplateMapping productTemplateMapping : templateMappings) {
                                    templateId = productTemplateMapping.getProductTemplates().getTemplateId();
                                }

                            }
                            proposalNumber = (String) jsonObj.get(KEY3);
                            byteStream =
                                    (ByteArrayOutputStream) lifeEngageDocumentComponent.generateIllustrationPdf(
                                            proposalNumber, type, templateId);
                        } else if(GeneraliConstants.MEMO.equals(jsonObj.getString(TYPE))){
                            String applicationNumber = (String) jsonObj.get(Constants.KEY21);
                            byteStream =
                                           (ByteArrayOutputStream) lifeEngageDocumentComponent.generateMemoPDF(applicationNumber, false).get(0);
                        } 
                        
                        byte[] data = byteStream.toByteArray();
                        base64pdf = Base64Utils.encode(data);
                        responseJsonObj.put("base64pdf", base64pdf);
                        jsonRsArray.put(responseJsonObj);
                    } catch (SystemException e) {
                        jsonRsArray.put(new JSONObject(e.getMessage()));
                    }
                }
            }
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
        }
        return jsonObjectRs.toString();
    }
    
    private ProductSearchCriteria buildProductSearchCriteria(JSONObject object) {
        ProductSearchCriteria criteria = new ProductSearchCriteria();
        String type = object.getString(TYPE);
        final JSONObject productObj = object.getJSONObject(TRANSACTIONDATA).getJSONObject(PRODUCT);
        String productCode = productObj.getJSONObject(PRODUCT_DETAILS).getString(PRODUCTCODE);
        String language = productObj.getJSONObject(TEMPLATES).getString(SELECTED_LANGUAGE);
        String templateTypeCode = "";
        if (ILLUSTRATION.equals(type)) {
            templateTypeCode = GeneraliConstants.ILLUSTRATION_PDF_TEMPLATE_TYPE;
        } else if (E_APP.equals(type)) {
            templateTypeCode = GeneraliConstants.EAPP_PDF_TEMPLATE_TYPE;
        } else if (FNA.equals(type)) {
            templateTypeCode = GeneraliConstants.FNA_TEMPLATE_TYPE;
        }
        criteria.setId(Long.parseLong(productCode));
        criteria.setCarrierCode(1l);
        criteria.setTemplateType(templateTypeCode);
        criteria.setTemplateLanguage(language);
        return criteria;
    }

    private static final String SAVE_ERROR_MESSAGE = "Error while saving file";

    /** The Constant PROPOSAL_NUMBER. */
    private static final String PROPOSAL_NUMBER = "proposalNumber";

    /** The Constant ERROR. */
    private static final String ERROR = "Error";

    /** The Constant HEADER_KEY. */
    private static final String HEADER_KEY = "Content-Disposition";

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant ILLUSTRATION. */
    private static final String ILLUSTRATION = "illustration";

    private static final String FNA = "FNA";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant TYPE. */
    private static final String KEY4 = "Key4";

    /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = "TransactionData";

    /** The Constant TYPE. */
    private static final String KEY3 = "Key3";

    /** The Constant TYPE. */
    private static final String KEY2 = "Key2";
    
    /** The Constant PRODUCTDETAILS. */
    private static final String PRODUCTDETAILS = "ProductDetails";

    /** The Constant PRODUCTCODE. */
    private static final String PRODUCTCODE = "productCode";

    private static final String TEMPLATES = "templates";

    private static final String E_APP_PDF_TEMPALTE = "eAppPdfTemplate";

    private static final String ILLUSTRATION_PDF_TEMPLATE = "illustrationPdfTemplate";

    /** The Constant "Product". */
    private static final String PRODUCT = "Product";

    private static final String PRODUCT_DETAILS = "ProductDetails";

    private static final String SELECTED_LANGUAGE = "selectedLanguage";
    
    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly=true)
    public void generatePdfGet(String proposalnumber, String type, String templateId, HttpServletResponse response)
            throws BusinessException, ParseException {

        InputStream inputStream = null;
        ByteArrayOutputStream byteStream = null;
        String fileName = "";

        try {
            if (E_APP.equals(type)) {
                // get your file as InputStream
            	//templateId= eAppTemplateId;
                byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
                        .generatePdfGet(proposalnumber, type,templateId);
                inputStream = new ByteArrayInputStream(byteStream.toByteArray());
                fileName = "EappProposal_" + proposalnumber + ".pdf";
            } else if (ILLUSTRATION.equals(type)) {
                
                byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
                        .generateIllustrationPdf(proposalnumber, type ,templateId);
                inputStream = new ByteArrayInputStream(byteStream.toByteArray());
                fileName = "IllustrationProposal_" + proposalnumber + ".pdf";
            } else if (FNA.equals(type)) {
                byteStream =
                        (ByteArrayOutputStream) lifeEngageDocumentComponent.generateFNAReport(proposalnumber, type,
                                templateId);
                inputStream = new ByteArrayInputStream(byteStream.toByteArray());
                fileName = "FNAReport_" + proposalnumber + ".pdf";
            } else if (GeneraliConstants.MEMO.equals(type)) {
                byteStream =
                        (ByteArrayOutputStream) lifeEngageDocumentComponent.generateMemoPDF(proposalnumber, false).get(0);
                inputStream = new ByteArrayInputStream(byteStream.toByteArray());
                fileName = "MemoReport_" + proposalnumber + ".pdf";
            }

            // set content attributes for the response
            response.setContentType(Constants.TYPE_APPLICATION_PDF);
            response.setContentLength(byteStream.toByteArray().length);

            // set headers for the response
            final String headerKey = HEADER_KEY;
            final String headerValue = String.format(
                    "attachment; filename=\"%s\"", fileName);
            response.setHeader(headerKey, headerValue);

            // copy it to response's OutputStream
            IOUtils.copy(inputStream, response.getOutputStream());

            inputStream.close();
            response.flushBuffer();

        } catch (Exception ex) {
            LOGGER.error("Error writing file to output stream. Filename was '" + "'");
            throw new SystemException(Constants.OUTPUT_STREAM_IO_ERROR, ex);
        }
    }
}
