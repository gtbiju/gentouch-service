package com.cognizant.insurance.omnichannel.component.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.goalandneed.component.impl.GoalAndNeedComponentImpl;
import com.cognizant.insurance.omnichannel.component.GeneraliGoalAndNeedComponent;
import com.cognizant.insurance.omnichannel.dao.GeneraliGoalAndNeedDao;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public class GeneraliGoalAndNeedComponentImpl extends GoalAndNeedComponentImpl implements GeneraliGoalAndNeedComponent {

    @Autowired
    private GeneraliGoalAndNeedDao goalAndNeedDao;

    public Response<List<GoalAndNeed>> retrieveRelatedTransactions(Request<SearchCriteria> searchCriteriatRequest) {

        return goalAndNeedDao.retrieveRelatedTransactions(searchCriteriatRequest);

    }

    public Response<SearchCountResult> getGoalAndNeedCountforLMS(Request<SearchCriteria> searchCriteriatRequest) {

        return goalAndNeedDao.getGoalAndNeedCountforLMS(searchCriteriatRequest);

    }

}
