/**
 * 
 */
package com.cognizant.insurance.domain.finance;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.commoncodelists.StatusReasonCodeList;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ValidityStatusCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.domain.finance.financecodelists.AccountStatusCodeList;
import com.cognizant.insurance.domain.party.Party;

/**
 * Accounts are logical or physical concepts for holding similar "things" (e.g.,
 * money, policies) that can be managed.
 * 
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ACCOUNT_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class Account extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5502188602712972584L;

    /**
     * Name of the Account.
     * 
     * 
     * 
     */
    private String name;

    /**
     * A free-text statement used to explain what is represented by this
     * account. The description may contain an indication of the financial items
     * grouped under the account, the purpose of the account, and so on.
     * 
     * 
     * 
     */
    private String description;

    /**
     * The duration on which the account is opened. From its start date the
     * initial balance of the account is zero and entries can be made for the
     * account. On the end date the balance of the account needs to be set to
     * zero and no more entries can be made for the account.
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private TimePeriod effecitvePeriod;

    /**
     * Status of the Account.
     * 
     * 
     * 
     */
    @Enumerated
    private AccountStatusCodeList statusCode;

    /**
     * Date of status change for the Account.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date statusDate;

    /**
     * The reason explaining why there was a status change.
     * 
     * 
     * 
     */
    @Enumerated
    private StatusReasonCodeList statusReasonCode;

    /**
     * An identifier assigned to the account by the account authority for
     * identification purposes.
     * 
     * 
     * 
     */
    private Long accountIdentifier;

    /**
     * This relationship links an account entry to an account.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Set<AccountEntry> holdsAccountEntry;

    /**
     * valid, not valid.
     * 
     * 
     * 
     */
    @Enumerated
    private ValidityStatusCodeList validityStatusCode;

    /**
     * This relationship links an account to its owner. This represents the
     * party that owns the account.
     * 
     * 
     * 
     */
    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Party> owner;

    /**
     * The identification of an account as a shadow of another account. Entries
     * made to one account are duplicated by entries made to the other account
     * where they are reflected for different management and control purposes.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Collection<Account> isShadowedBy;

    /**
     * The identification of an account as a shadow of another account. Entries
     * made to one account are duplicated by entries made to the other account
     * where they are reflected for different management and control purposes.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Account shadows;

    /**
     * The identification of an account as one of the accounts consolidated into
     * another account. One account can be consolidated into multiple parent
     * accounts for reporting purposes.
     * 
     * e.g: The account related to campaign mailing expenses is consolidated
     * into the account related to campaign expenses.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Collection<Account> isConsolidatedInto;

    /**
     * The identification of an account as one of the accounts consolidated into
     * another account. One account can be consolidated into multiple parent
     * accounts for reporting purposes.
     * 
     * e.g: The account related to campaign mailing expenses is consolidated
     * into the account related to campaign expenses.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Collection<Account> consolidates;

    /**
     * An indicator of a third party account.
     * 
     * 
     * 
     */
    private Boolean thirdPartyIndicator;

    /**
     * The balance of the account (that can be derived from all the amounts in
     * the account entries for the monetary account).
     * 
     * 
     * 
     * 
     */
    @OneToOne(cascade=CascadeType.ALL)
    private CurrencyAmount balanceAmount;

    /**
     * Indicates the code identifying the currency selected by the customer to
     * be used on any monetary information provided to the customer.
     * 
     * 
     * SOURCE: ISO-4217
     * 
     * 
     * 
     */
    @ManyToOne(cascade = CascadeType.PERSIST)
    private ExternalCode currencyCode;

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the effecitvePeriod.
     * 
     * @return Returns the effecitvePeriod.
     */
    public TimePeriod getEffecitvePeriod() {
        return effecitvePeriod;
    }

    /**
     * Gets the statusCode.
     * 
     * @return Returns the statusCode.
     */
    public AccountStatusCodeList getStatusCode() {
        return statusCode;
    }

    /**
     * Gets the statusDate.
     * 
     * @return Returns the statusDate.
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * Gets the statusReasonCode.
     * 
     * @return Returns the statusReasonCode.
     */
    public StatusReasonCodeList getStatusReasonCode() {
        return statusReasonCode;
    }

    /**
     * Gets the accountIdentifier.
     * 
     * @return Returns the accountIdentifier.
     */
    public Long getAccountIdentifier() {
        return accountIdentifier;
    }

    /**
     * Gets the holdsAccountEntry.
     * 
     * @return Returns the holdsAccountEntry.
     */
    public Set<AccountEntry> getHoldsAccountEntry() {
        return holdsAccountEntry;
    }

    /**
     * Gets the validityStatusCode.
     * 
     * @return Returns the validityStatusCode.
     */
    public ValidityStatusCodeList getValidityStatusCode() {
        return validityStatusCode;
    }

    /**
     * Gets the owner.
     * 
     * @return Returns the owner.
     */
    public Set<Party> getOwner() {
        return owner;
    }

    /**
     * Gets the isShadowedBy.
     * 
     * @return Returns the isShadowedBy.
     */
    public Collection<Account> getIsShadowedBy() {
        return isShadowedBy;
    }

    /**
     * Gets the shadows.
     * 
     * @return Returns the shadows.
     */
    public Account getShadows() {
        return shadows;
    }

    /**
     * Gets the isConsolidatedInto.
     * 
     * @return Returns the isConsolidatedInto.
     */
    public Collection<Account> getIsConsolidatedInto() {
        return isConsolidatedInto;
    }

    /**
     * Gets the consolidates.
     * 
     * @return Returns the consolidates.
     */
    public Collection<Account> getConsolidates() {
        return consolidates;
    }

    /**
     * Gets the thirdPartyIndicator.
     * 
     * @return Returns the thirdPartyIndicator.
     */
    public Boolean getThirdPartyIndicator() {
        return thirdPartyIndicator;
    }

    /**
     * Gets the balanceAmount.
     * 
     * @return Returns the balanceAmount.
     */
    public CurrencyAmount getBalanceAmount() {
        return balanceAmount;
    }

    /**
     * Gets the currencyCode.
     * 
     * @return Returns the currencyCode.
     */
    public ExternalCode getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The effecitvePeriod.
     * 
     * @param effecitvePeriod
     *            The effecitvePeriod to set.
     */
    public void setEffecitvePeriod(final TimePeriod effecitvePeriod) {
        this.effecitvePeriod = effecitvePeriod;
    }

    /**
     * Sets The statusCode.
     * 
     * @param statusCode
     *            The statusCode to set.
     */
    public void setStatusCode(final AccountStatusCodeList statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Sets The statusDate.
     * 
     * @param statusDate
     *            The statusDate to set.
     */
    public void setStatusDate(final Date statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * Sets The statusReasonCode.
     * 
     * @param statusReasonCode
     *            The statusReasonCode to set.
     */
    public void setStatusReasonCode(
            final StatusReasonCodeList statusReasonCode) {
        this.statusReasonCode = statusReasonCode;
    }

    /**
     * Sets The accountIdentifier.
     * 
     * @param accountIdentifier
     *            The accountIdentifier to set.
     */
    public void setAccountIdentifier(final Long accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
    }

    /**
     * Sets The holdsAccountEntry.
     * 
     * @param holdsAccountEntry
     *            The holdsAccountEntry to set.
     */
    public void setHoldsAccountEntry(
            final Set<AccountEntry> holdsAccountEntry) {
        this.holdsAccountEntry = holdsAccountEntry;
    }

    /**
     * Sets The validityStatusCode.
     * 
     * @param validityStatusCode
     *            The validityStatusCode to set.
     */
    public void setValidityStatusCode(
            final ValidityStatusCodeList validityStatusCode) {
        this.validityStatusCode = validityStatusCode;
    }

    /**
     * Sets The owner.
     * 
     * @param owner
     *            The owner to set.
     */
    public void setOwner(final Set<Party> owner) {
        this.owner = owner;
    }

    /**
     * Sets The isShadowedBy.
     * 
     * @param isShadowedBy
     *            The isShadowedBy to set.
     */
    public void setIsShadowedBy(final Collection<Account> isShadowedBy) {
        this.isShadowedBy = isShadowedBy;
    }

    /**
     * Sets The shadows.
     * 
     * @param shadows
     *            The shadows to set.
     */
    public void setShadows(final Account shadows) {
        this.shadows = shadows;
    }

    /**
     * Sets The isConsolidatedInto.
     * 
     * @param isConsolidatedInto
     *            The isConsolidatedInto to set.
     */
    public void setIsConsolidatedInto(
            final Collection<Account> isConsolidatedInto) {
        this.isConsolidatedInto = isConsolidatedInto;
    }

    /**
     * Sets The consolidates.
     * 
     * @param consolidates
     *            The consolidates to set.
     */
    public void setConsolidates(final Collection<Account> consolidates) {
        this.consolidates = consolidates;
    }

    /**
     * Sets The thirdPartyIndicator.
     * 
     * @param thirdPartyIndicator
     *            The thirdPartyIndicator to set.
     */
    public void setThirdPartyIndicator(final Boolean thirdPartyIndicator) {
        this.thirdPartyIndicator = thirdPartyIndicator;
    }

    /**
     * Sets The balanceAmount.
     * 
     * @param balanceAmount
     *            The balanceAmount to set.
     */
    public void setBalanceAmount(final CurrencyAmount balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    /**
     * Sets The currencyCode.
     * 
     * @param currencyCode
     *            The currencyCode to set.
     */
    public void setCurrencyCode(final ExternalCode currencyCode) {
        this.currencyCode = currencyCode;
    }

}
