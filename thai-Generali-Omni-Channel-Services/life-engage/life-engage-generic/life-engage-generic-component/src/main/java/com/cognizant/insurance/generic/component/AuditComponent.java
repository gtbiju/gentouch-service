/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.component;

import java.util.List;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.audit.LifeEngagePayloadAudit;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;

// TODO: Auto-generated Javadoc
/**
 * The Interface interface AuditComponent.
 *
 * @author 300797
 */
public interface AuditComponent {

    /**
     * Savelife engage audit.
     *
     * @param <T> the generic type
     * @param request the request
     * @return the response
     */
    <T> Response<Object> save(Request<T> request);

    /**
     * Update.
     *
     * @param <T> the generic type
     * @param request the request
     */
    <T> void update(Request<T> request);

    /**
     * Retrieve life engage payload audit.
     *
     * @param request the request
     * @return the response
     */
    Response<List<LifeEngagePayloadAudit>> retrieveLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request);
    
    /**
     * Retrieve LMS payload audit.
     *
     * @param request the request
     * @return the response
     */
    Response<List<LifeEngagePayloadAudit>> retrieveLMSPayloadAudit(Request<LifeEngagePayloadAudit> request);

    /**
     * Gets the life engage audit.
     *
     * @param request the request
     * @return the life engage audit
     */
    Response<LifeEngageAudit> getLifeEngageAudit(Request<LifeEngageAudit> request);

    /**
     * Retrieve last life engage payload audit.
     *
     * @param request the request
     * @return the response
     */
    Response<LifeEngagePayloadAudit> retrieveLastLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request);
	
	/**
     * Retrieve last life engage payload audit.
     *
     * @param request the request
     * @return the response
     */
	Response<List<LifeEngagePayloadAudit>> readLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request);
}
