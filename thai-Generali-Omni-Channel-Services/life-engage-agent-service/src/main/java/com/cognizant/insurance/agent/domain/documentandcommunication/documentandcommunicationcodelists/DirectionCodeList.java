/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists;

/**
 * Identifies a classification of Communications according to their direction.
 * 
 * @author 300797
 * 
 * 
 */
public enum DirectionCodeList {
    /**
     * Identifies a Communication with direction 'External'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    External,
    /**
     * Identifies a Communication with direction 'Inbound'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Inbound,
    /**
     * Identifies a Communication with direction 'Internal'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Internal,
    /**
     * Identifies a Communication with direction 'Outbound'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Outbound
}
