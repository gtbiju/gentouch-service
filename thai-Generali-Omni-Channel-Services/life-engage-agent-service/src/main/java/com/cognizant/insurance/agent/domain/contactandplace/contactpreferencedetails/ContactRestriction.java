/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.contactpreferencedetails;

import java.math.BigDecimal;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;

/**
 * A restriction on the frequency and type of contact.
 * 
 * e.g: For John Doe, maximum of 2 contacts in any one day, 15 contacts in a
 * year.
 * 
 * @author 301350
 * 
 * 
 */


public class ContactRestriction extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6149611330896105944L;

    /**
     * The maximum number of times of contact in a given period. When a no
     * contacted is preferred, the value is set to zero. 
     * 
     * 
     * 
     */
    private BigDecimal maximumContactCount;

    /**
     * The maximum number of contacts that can be active concurrently.
     * 
     * 
     * 
     */
    private BigDecimal maximumActiveContactCount;

    /**
     * The time period for which the maximum contact count is allowed.
     * 
     * 
     * 
     */
    
    private TimePeriod restrictionDuration;

    /**
     * @return the maximumActiveContactCount
     * 
     * 
     */
    public BigDecimal getMaximumActiveContactCount() {
        return maximumActiveContactCount;
    }

    /**
     * @return the maximumContactCount
     * 
     * 
     */
    public BigDecimal getMaximumContactCount() {
        return maximumContactCount;
    }

    /**
     * @return the restrictionDuration
     * 
     * 
     */
    public TimePeriod getRestrictionDuration() {
        return restrictionDuration;
    }

    /**
     * @param maximumActiveContactCount
     *            the maximumActiveContactCount to set
     * 
     * 
     */
    public void setMaximumActiveContactCount(
            final BigDecimal maximumActiveContactCount) {
        this.maximumActiveContactCount = maximumActiveContactCount;
    }

    /**
     * @param maximumContactCount
     *            the maximumContactCount to set
     * 
     * 
     */
    public void setMaximumContactCount(
            final BigDecimal maximumContactCount) {
        this.maximumContactCount = maximumContactCount;
    }

    /**
     * @param restrictionDuration
     *            the restrictionDuration to set
     * 
     * 
     */
    public void setRestrictionDuration(final TimePeriod restrictionDuration) {
        this.restrictionDuration = restrictionDuration;
    }
}
