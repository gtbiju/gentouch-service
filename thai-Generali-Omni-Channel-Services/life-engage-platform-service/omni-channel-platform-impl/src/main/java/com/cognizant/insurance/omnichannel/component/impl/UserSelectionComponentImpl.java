package com.cognizant.insurance.omnichannel.component.impl;

import java.util.Calendar;
import java.util.Set;
import java.util.UUID;

import com.cognizant.insurance.domain.commonelements.commonclasses.GLISelectedOption;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.omnichannel.component.UserSelectionComponent;
import com.cognizant.insurance.request.Request;

public class UserSelectionComponentImpl implements UserSelectionComponent {

	@Override
	public void saveUserSelection(Request<Set<UserSelection>> request) {
		//final UserSelectionDao userSelectionDao = new UserSelectionDaoImpl();
		for (UserSelection selection : (Set<UserSelection>) request.getType()) {
			UUID idOne = UUID.randomUUID();
			String uuid_lsd = idOne.getLeastSignificantBits() + "";
			uuid_lsd = uuid_lsd.replace("-", "");
			selection.setId(Long.parseLong(uuid_lsd));
			if (null != selection && null != selection.getSelectedOptions()
					&& !selection.getSelectedOptions().isEmpty()) {
				for (GLISelectedOption options : selection.getSelectedOptions()) {
					UUID idOption = UUID.randomUUID();
					String uuid_lsd_option = idOption.getLeastSignificantBits()
							+ "";
					uuid_lsd_option = uuid_lsd_option.replace("-", "");
					options.setId(Long.parseLong(uuid_lsd_option));
				}
			}
		}
		//userSelectionDao.save(request);
	}


	/**
	 * 
	 */
	@Override
	public void saveUserSelection(Set<UserSelection> selections) {

		if (null != selections && !selections.isEmpty()) {
			for (UserSelection selection :selections) {
				selection.setId(Calendar.getInstance().getTimeInMillis());
				if (null != selection && null != selection.getSelectedOptions()
						&& !selection.getSelectedOptions().isEmpty()) {
					for (GLISelectedOption options : selection.getSelectedOptions()) {
						UUID idOption = UUID.randomUUID();
						String uuid_lsd_option = idOption.getLeastSignificantBits()
								+ "";
						uuid_lsd_option = uuid_lsd_option.replace("-", "");
						options.setId(Long.parseLong(uuid_lsd_option));
					}
				}
			}
		}
	}
}
