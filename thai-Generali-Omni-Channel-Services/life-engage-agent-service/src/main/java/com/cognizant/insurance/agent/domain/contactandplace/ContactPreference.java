/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.ContactPreferencePurposeCodeList;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.ContactPreferenceStatusCodeList;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.ContactPreferenceUsageCodeList;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.ValidationResultCodeList;

/**
 * The characteristics of the way a role or party wants to be contacted. This
 * includes the contact points, preferred communication profile (including
 * language and medium), name and timing preferences, preferred contacting
 * person, and restrictions on the contact frequency. It also defines the usage
 * such as business or private and the purpose, such as billing or mailing.
 * 
 * e.g: Simon can be contacted on Monday and Friday between 5 am and 11 pm (two
 * timing preferences since the periods are not contiguous) preferably by
 * telephone (preferred medium) and in double Dutch (preferred language) at 44
 * 3687 47 48 (contact point preference).
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class ContactPreference extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2680928360740663L;

    /**
     * The date the contact preference was last checked for validity.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date lastValidatedDate;

    /**
     * The recorded advice which may be helpful when contacting the role player.
     * 
     * e.g: Be aware of the dog when making a house call.
     * 
     * 
     * 
     */
    private String contactInstructions;

    /**
     * The relative priority of the contact preference with respect to the
     * others.
     * 
     * e.g. 1 (Main/primary preference)
     * 
     * e.g. 5 (Low preference)
     * 
     * 
     * 
     */
    private Integer priorityLevel;

    /**
     * The conclusion of the most recent validity check.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private ValidationResultCodeList validationResultCode;

    /**
     * The time period on which the contact preference can be used.
     * 
     * NOTE: By utilizing the data type "TimePeriod", this property can be
     * utilized in a flexible way (e.g. Date to Date, Month to Month, Month Day
     * to Month Day, Year Month to Year Month, etc.).
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private TimePeriod availablePeriod;

    /**
     * The context in which this contact preference is used, such as business
     * contacts to be made while at work or personal contacts to be made while
     * at home.
     * 
     * 
     * 
     */
    @Enumerated
    private ContactPreferenceUsageCodeList usageCode;

    /**
     * The purpose for which a party/party role uses a contact preference.
     * Purpose is more a classification, whereas contactUsage describes how a
     * particular contact is used. 
     * 
     * 
     * 
     */
    @Enumerated
    private ContactPreferencePurposeCodeList purposeCode;

    /**
     * A text representation of the purpose for which a party/party role uses a
     * contact preference. Purpose is more a classification, whereas
     * contactUsage describes how a particular contact is used.
     * 
     * NOTE: This attribute is for use when the contact purpose is not
     * enumerated (e.g. refer to the corresponding purposeCode). <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    private String purpose;

    /**
     * An indicator this is the default contact point - there is no preference
     * concerning priority for contact. 
     * 
     * 
     * 
     */
    private Boolean defaultIndicator;

    /**
     * An indicator as to whether the contact point may be used for solicitation
     * on this contact preference.
     * 
     * This indicator is derived from the purpose of the contact. Purposes that
     * are considered to indicate a solicitable contact preference are
     * 'marketing' and 'advertising'.
     * 
     * 
     * 
     */
    private Boolean solicitableIndicator;

    /**
     * Specifies if the contact point is in any red lists, such as those that
     * exist in the United States and France. Such red lists prohibit the
     * contact from being used in things like phone books, phone solicitation
     * and mail solicitation.
     * 
     * e.g., French "red list" for phone book listings
     * 
     * e.g., United States "Do Not Call List" for phone numbers 
     *
     * 
     * 
     * 
     */
    private Boolean redListIndicator;

    /**
     * This relationship links a contact preference with a contact point.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE },fetch=FetchType.EAGER)
    private ContactPoint preferredContactPoint;

    /**
     * The current status of the contact preference. 
     * 
     * 
     * 
     */
    @Enumerated
    private ContactPreferenceStatusCodeList statusCode;

    /**
     * Gets the lastValidatedDate.
     * 
     * @return Returns the lastValidatedDate.
     */
    public Date getLastValidatedDate() {
        return lastValidatedDate;
    }

    /**
     * Gets the contactInstructions.
     * 
     * @return Returns the contactInstructions.
     */
    public String getContactInstructions() {
        return contactInstructions;
    }

    /**
     * Gets the priorityLevel.
     * 
     * @return Returns the priorityLevel.
     */
    public Integer getPriorityLevel() {
        return priorityLevel;
    }

    /**
     * Gets the validationResultCode.
     * 
     * @return Returns the validationResultCode.
     */
    public ValidationResultCodeList getValidationResultCode() {
        return validationResultCode;
    }

    /**
     * Gets the availablePeriod.
     * 
     * @return Returns the availablePeriod.
     */
    public TimePeriod getAvailablePeriod() {
        return availablePeriod;
    }

    /**
     * Gets the usageCode.
     * 
     * @return Returns the usageCode.
     */
    public ContactPreferenceUsageCodeList getUsageCode() {
        return usageCode;
    }

    /**
     * Gets the purposeCode.
     * 
     * @return Returns the purposeCode.
     */
    public ContactPreferencePurposeCodeList getPurposeCode() {
        return purposeCode;
    }

    /**
     * Gets the purpose.
     * 
     * @return Returns the purpose.
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * Gets the defaultIndicator.
     * 
     * @return Returns the defaultIndicator.
     */
    public Boolean getDefaultIndicator() {
        return defaultIndicator;
    }

    /**
     * Gets the solicitableIndicator.
     * 
     * @return Returns the solicitableIndicator.
     */
    public Boolean getSolicitableIndicator() {
        return solicitableIndicator;
    }

    /**
     * Gets the redListIndicator.
     * 
     * @return Returns the redListIndicator.
     */
    public Boolean getRedListIndicator() {
        return redListIndicator;
    }

    /**
     * Gets the preferredContactPoint.
     * 
     * @return Returns the preferredContactPoint.
     */
    public ContactPoint getPreferredContactPoint() {
        return preferredContactPoint;
    }

    /**
     * Gets the statusCode.
     * 
     * @return Returns the statusCode.
     */
    public ContactPreferenceStatusCodeList getStatusCode() {
        return statusCode;
    }

    /**
     * Sets The lastValidatedDate.
     * 
     * @param lastValidatedDate
     *            The lastValidatedDate to set.
     */
    public void setLastValidatedDate(final Date lastValidatedDate) {
        this.lastValidatedDate = lastValidatedDate;
    }

    /**
     * Sets The contactInstructions.
     * 
     * @param contactInstructions
     *            The contactInstructions to set.
     */
    public void setContactInstructions(final String contactInstructions) {
        this.contactInstructions = contactInstructions;
    }

    /**
     * Sets The priorityLevel.
     * 
     * @param priorityLevel
     *            The priorityLevel to set.
     */
    public void setPriorityLevel(final Integer priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    /**
     * Sets The validationResultCode.
     * 
     * @param validationResultCode
     *            The validationResultCode to set.
     */
    public void setValidationResultCode(
            final ValidationResultCodeList validationResultCode) {
        this.validationResultCode = validationResultCode;
    }

    /**
     * Sets The availablePeriod.
     * 
     * @param availablePeriod
     *            The availablePeriod to set.
     */
    public void setAvailablePeriod(final TimePeriod availablePeriod) {
        this.availablePeriod = availablePeriod;
    }

    /**
     * Sets The usageCode.
     * 
     * @param usageCode
     *            The usageCode to set.
     */
    public void setUsageCode(
            final ContactPreferenceUsageCodeList usageCode) {
        this.usageCode = usageCode;
    }

    /**
     * Sets The purposeCode.
     * 
     * @param purposeCode
     *            The purposeCode to set.
     */
    public void setPurposeCode(
            final ContactPreferencePurposeCodeList purposeCode) {
        this.purposeCode = purposeCode;
    }

    /**
     * Sets The purpose.
     * 
     * @param purpose
     *            The purpose to set.
     */
    public void setPurpose(final String purpose) {
        this.purpose = purpose;
    }

    /**
     * Sets The defaultIndicator.
     * 
     * @param defaultIndicator
     *            The defaultIndicator to set.
     */
    public void setDefaultIndicator(final Boolean defaultIndicator) {
        this.defaultIndicator = defaultIndicator;
    }

    /**
     * Sets The solicitableIndicator.
     * 
     * @param solicitableIndicator
     *            The solicitableIndicator to set.
     */
    public void
            setSolicitableIndicator(final Boolean solicitableIndicator) {
        this.solicitableIndicator = solicitableIndicator;
    }

    /**
     * Sets The redListIndicator.
     * 
     * @param redListIndicator
     *            The redListIndicator to set.
     */
    public void setRedListIndicator(final Boolean redListIndicator) {
        this.redListIndicator = redListIndicator;
    }

    /**
     * Sets The preferredContactPoint.
     * 
     * @param preferredContactPoint
     *            The preferredContactPoint to set.
     */
    public void setPreferredContactPoint(
            final ContactPoint preferredContactPoint) {
        this.preferredContactPoint = preferredContactPoint;
    }

    /**
     * Sets The statusCode.
     * 
     * @param statusCode
     *            The statusCode to set.
     */
    public void setStatusCode(
            final ContactPreferenceStatusCodeList statusCode) {
        this.statusCode = statusCode;
    }

}
