/**
 *
 * Copyright 2016, Cognizant 
 *
 * @author        : 291446
 * @version       : 0.1, Sep 8, 2016
 */
package com.cognizant.insurance.endpoint;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.domain.TinyUrlGenerator;
import com.cognizant.insurance.omnichannel.service.GeneraliManagerialLevelService;
import com.cognizant.insurance.omnichannel.service.GeneraliPaymentService;
import com.cognizant.insurance.services.LifeEngageSyncService;

/**
 * Class GeneraliPaymentEndPoint - Payment End Point
 * 
 * @author 291446
 *
 */

@Controller
@RequestMapping("/payment")
public class GeneraliPaymentEndPoint {

	/** The logger. */
	public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliPaymentEndPoint.class);

	@Autowired
	private GeneraliPaymentService generaliPaymentService;
	
	@Value("${generali.platform.payment.url.expiryDays}")
    private String expiryDate;

	/**
	 * Method to get the payment validation details
	 * 
	 * @param json
	 * @return json
	 */

	@RequestMapping(value = "/paymentValidation", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
	@ResponseBody
	public final String retrieveByPayment(@RequestBody final String json) {
		return new ServiceTemplate() {
			@Override
			public String doExecute(final String json) throws BusinessException {
				return generaliPaymentService.retrievePaymentService(json);
			}
		}.execute(json);
	}

	@RequestMapping(value = "/makePayment", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
	@ResponseBody
	public final String makePayment(@RequestBody final String json) {
		return new ServiceTemplate() {
			@Override
			public String doExecute(final String json) throws BusinessException {
				return generaliPaymentService.makePayment(json);
			}
		}.execute(json);
	}

	/*@RequestMapping(value = "/getPaymentStatus/{invoice}", method = RequestMethod.GET)
	@ResponseBody
	public final String getCardPaymentStatus(@PathVariable String invoice) {
		String response = StringUtils.EMPTY;
		try {
			response = generaliPaymentService.getCardPaymentStatus(invoice);
		} catch (BusinessException e) {

			e.printStackTrace();
		}
		return response;

	}*/
	
	@RequestMapping(value = "/getPaymentStatus", method = RequestMethod.POST)
	@ResponseBody
	public final String getUrlPaymentStatus(@RequestBody final String json) {
		String response = StringUtils.EMPTY;
		try {
			response = generaliPaymentService.getUrlPaymentStatus(json);
		} catch (BusinessException e) {

			e.printStackTrace();
		}
		return response;

	}
	
	@RequestMapping(value = "/getUrl/{UUID}/{id}", method = RequestMethod.GET)
	@ResponseBody
	public final String makePaymentViaURL(@PathVariable String UUID,@PathVariable String id,
			final HttpServletResponse response) {
		TinyUrlGenerator url = null;
		try {
			url = generaliPaymentService.makePaymentViaUrl(UUID,id);
				if(null != url && !isUrlExpired(url)){
				response.sendRedirect(url.getOriginalUrl());
				}
				else{
					response.sendError(400,"Invalid Url");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		catch (BusinessException e) {

			e.printStackTrace();
		}
		return id;

	}
	
	private boolean isUrlExpired(TinyUrlGenerator result) {
		Date createdDate = result.getCreationDateTime();
		Date currentDate = new Date();
		if((currentDate.getTime()-createdDate.getTime())/86400000>Integer.valueOf(expiryDate)){
			return true;
		}
		return false;
	}

	public GeneraliPaymentService getGeneraliPaymentService() {
		return generaliPaymentService;
	}

	public void setGeneraliPaymentService(GeneraliPaymentService generaliPaymentService) {
		this.generaliPaymentService = generaliPaymentService;
	}

}
