/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 356551
 * @version       : 0.1, Apr 19, 2018
 */
package com.cognizant.insurance.omnichannel.executor;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.omnichannel.component.repository.GeneraliEAppRepository;
import com.cognizant.insurance.service.impl.LifeEngageSyncServiceImpl;

public class GeneraliGAOEAppUnlockExecutor {
    
    @Autowired
    GeneraliEAppRepository eAppRepository;
    
    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageSyncServiceImpl.class);
    
    @Transactional(rollbackFor = { Exception.class })
    public void run(){
        
        try {
            eAppRepository.updateGAOLockStatus(); 
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        
    }
    
  
    
}
