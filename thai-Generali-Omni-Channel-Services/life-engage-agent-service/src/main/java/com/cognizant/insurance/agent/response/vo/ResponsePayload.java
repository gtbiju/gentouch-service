/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.agent.response.vo;

import java.util.List;

//import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.agent.request.vo.Transactions;
//import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;

/**
 * The Class class RequestInfo.
 * 
 * @author 345166
 * 
 * 
 */
public class ResponsePayload {

    /** The transactions. */
    private List<Transactions> transactions;

    /** The product specification. *//*
    private List<ProductSpecification> productSpecification;

    *//** The product search criteria. *//*
    private ProductSearchCriteria productSearchCriteria;*/

    /**
     * Gets the transactions.
     * 
     * @return the transactions
     */
    public List<Transactions> getTransactions() {
        return transactions;
    }

    /**
     * Sets the transactions.
     * 
     * @param transactions
     *            the transactions to set
     */
    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }

    /**
     * Gets the product specification.
     * 
     * @return the productSpecification
     *//*
    public List<ProductSpecification> getProductSpecification() {
        return productSpecification;
    }

    *//**
     * Sets the product specification.
     * 
     * @param productSpecification
     *            the productSpecification to set
     *//*
    public void setProductSpecification(List<ProductSpecification> productSpecification) {
        this.productSpecification = productSpecification;
    }

    *//**
     * Gets the product search criteria.
     * 
     * @return the productSearchCriteria
     *//*
    public ProductSearchCriteria getProductSearchCriteria() {
        return productSearchCriteria;
    }

    *//**
     * Sets the product search criteria.
     * 
     * @param productSearchCriteria
     *            the productSearchCriteria to set
     *//*
    public void setProductSearchCriteria(ProductSearchCriteria productSearchCriteria) {
        this.productSearchCriteria = productSearchCriteria;
    }
*/
}
