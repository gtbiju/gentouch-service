/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 25, 2013
 */
package com.cognizant.insurance.agent.component.helper;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.constants.ErrorConstants;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.core.exception.SystemException;
import com.cognizant.insurance.agent.dao.exception.DaoException;
import com.cognizant.insurance.agent.request.vo.RequestInfo;
import com.cognizant.insurance.agent.request.vo.StatusData;
import com.cognizant.insurance.agent.request.vo.Transactions;

// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageComponentHelper.
 * 
 * @author 300797
 */
public final class LifeEngageComponentHelper {

    /**
     * Instantiates a new life engage component helper.
     */
    private LifeEngageComponentHelper() {
        // not called
    }

    /** The Constant BASIC_DETAILS. */
    public static final String BASIC_DETAILS = "BasicDetails";

    /** The Constant FAILURE. */
    public static final String FAILURE = "FAILURE";

    /** The Constant FAILURE_CODE. */
    public static final String FAILURE_CODE = "400";

    /** The Constant FULL_DETAILS. */
    public static final String FULL_DETAILS = "FullDetails";

    /** The Constant ID. */
    public static final String OFFLINE_ID = "Id";

    /** The Constant KEY_1. */
    public static final String KEY_1 = "Key1";

    /** The Constant KEY_10. */
    public static final String KEY_10 = "Key10";

    /** The Constant KEY_11. */
    public static final String KEY_11 = "Key11";

    /** The Constant KEY_13. */
    public static final String KEY_13 = "Key13";

    /** The Constant KEY_14. */
    public static final String KEY_14 = "Key14";

    /** The Constant KEY_15. */
    public static final String KEY_15 = "Key15";

    /** The Constant KEY_2. */
    public static final String KEY_2 = "Key2";

    /** The Constant KEY_3. */
    public static final String KEY_3 = "Key3";

    /** The Constant KEY_4. */
    public static final String KEY_4 = "Key4";

    /** The Constant KEY_5. */
    public static final String KEY_5 = "Key5";

    /** The Constant KEY_6. */
    public static final String KEY_6 = "Key6";

    /** The Constant KEY_7. */
    public static final String KEY_7 = "Key7";

    /** The Constant KEY_8. */
    public static final String KEY_8 = "Key8";

    /** The Constant KEY_18. */
    public static final String KEY_18 = "Key18";

    /** The Constant KEY_18. */
    public static final String KEY_12 = "Key12";

    /** The Constant KEY_19. */
    public static final String KEY_19 = "Key19";

    /** The Constant KEY_20. */
    public static final String KEY_20 = "Key20";

    /** The Constant KEY_21. */
    public static final String KEY_21 = "Key21";

    /** The Constant KEY_22. */
    public static final String KEY_22 = "Key22";

    /** The Constant KEY_23. */
    public static final String KEY_23 = "Key23";

    /** The Constant KEY_24. */
    public static final String KEY_24 = "Key24";

    /** The Constant KEY_25. */
    public static final String KEY_25 = "Key25";

    /** The Constant SUCCESS. */
    public static final String SUCCESS = "SUCCESS";

    /** The Constant SUCCESS_CODE. */
    public static final String SUCCESS_CODE = "100";

    /** The Constant REG_SUCCES_CODE. */
    public static final String SUCCESS_CODE_PUSH_NOTIFICATION = "200";

    /** The Constant NOTIFICATION_PARTIAL_SUCCES_CODE. */
    public static final String NOTIFICATION_PARTIAL_SUCCES_CODE = "201";

    /** The Constant NOTIFICATION_PARTIAL_SUCCESS. */
    public static final String NOTIFICATION_PARTIAL_SUCCESS = "Notification(s) Dispatched Partially";

    /** The Constant REJECTED. */
    public static final String REJECTED = "REJECTED";

    /** The Constant REJECTED_CODE. */
    public static final String REJECTED_CODE = "102";

    /** The Constant REJECTED. */
    public static final String VALID = "Valid";

    /** The Constant REJECTED_CODE. */
    public static final String VALID_CODE = "101";

    /** The Constant REJECTED. */
    public static final String VALID_PROPOSAL_MESSAGE = "Valid Proposal";

    /** The Constant REJECTED. */
    public static final String INVALID = "Invalid";

    /** The Constant REJECTED_CODE. */
    public static final String INVALID_CODE = "401";

    /** The Constant REJECTED_CODE. */
    public static final String INVALID_PROPOSAL_MESSAGE = "No Proposal Exists";

    /** The Constant TRANS_TRACKING_ID. */
    public static final String TRANS_TRACKING_ID = "TransTrackingID";

    /** The Constant TYPE. */
    public static final String TYPE = "Type";

    /** The Constant DATEFORMAT_YYYY_MM_DD_HH_MM_SS. */
    public static final String DATEFORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    /** The Constant REGISTRATION_SUCCESS. */
    public static final String REGISTRATION_SUCCESS = "Device registered successfully";

    /** The Constant NOTIFICATION_SUCCESS. */
    public static final String NOTIFICATION_SUCCESS = "Notification(s) Dispatched Successfully";

    /** The Constant REGISTRATION_FAILED. */
    public static final String REGISTRATION_FAILED = "System error while registering the device";

    /** The Constant UNREGISTER_SUCCESS. */
    public static final String UNREGISTER_SUCCESS = "Device unregistered successfully";

    /** The Constant UNREGISTER_FAILED. */
    public static final String UNREGISTER_FAILED = "System error while unregistering device";

    /** The Constant NOTIFICATION_FAILED. */
    public static final String NOTIFICATION_FAILED = "System error while pushing data in notification Hash";

    /** The Constant SUCCESS_CODE_FETCH_ENCRYPTION_KEY. */
    public static final String SUCCESS_CODE_FETCH_ENCRYPTION_KEY = "200";

    /** The Constant FETCH_ENCRYPTION_KEY_FAILED. */
    public static final String FETCH_ENCRYPTION_KEY_FAILED = "Problem while fetching encryption key certificate";

    /** The Constant FETCH_ENCRYPTION_KEY_MISSING. */
    public static final String FETCH_ENCRYPTION_KEY_MISSING = "Encryption key certificate not found in server";

    /** The Constant FETCH_ENCRYPTION_KEY_SUCCESS. */
    public static final String FETCH_ENCRYPTION_KEY_SUCCESS = "Encryption Key fetched successfully";

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageComponentHelper.class);
    
    public static final String DATEFORMAT_MM_DD_YYYY = "MM-dd-yyyy";
    
    /** The Constant USER_NAME. */
    private static final String USER_NAME = "UserName";

    /** The Constant CREATION_DATE. */
    private static final String CREATION_DATE = "CreationDate";

    /** The Constant CREATION_TIME. */
    private static final String CREATION_TIME = "CreationTime";

    /** The Constant SOURCE_INFO_NAME. */
    private static final String SOURCE_INFO_NAME = "SourceInfoName";

    /** The Constant REQUESTOR_TOKEN. */
    private static final String REQUESTOR_TOKEN = "RequestorToken";

    /** The Constant USER_EMAIL. */
    private static final String USER_EMAIL = "UserEmail";
    
    /** The Constant TRANSACTION_ID. */
    private static final String TRANSACTION_ID = "TransactionId";
    
    /** The Constant DEVICE_UUID. */
    private static final String DEVICE_UUID = "Device_uuid";
    
    /** The Constant DATEFORMAT_HH_MM_SS. */
    public static final String DATEFORMAT_HH_MM_SS = "hh:mm:ss";
    
    private static final String TRANSACTIONS = "Transactions";
    
    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";
    
    /** The Constant RESPONSE_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";
	
	  /** Added for uploadRequirementDoc Fix
     *  The Constant MODIFIED_DATE. */
    private static final String MODIFIED_DATE = "ModifiedDate";

    /**
     * Builds the transactions list.
     * 
     * @param transactions
     *            the transactions
     * @param jsonObj
     *            the json obj
     * @throws ParseException
     *             the parse exception
     */
    public static void buildTransactionsList(final List<Transactions> transactions, final JSONObject jsonObj)
            throws ParseException {
        if (CollectionUtils.isNotEmpty(transactions)) {
            for (Transactions transactionsElement : transactions) {
                parseTrasactionDetails(jsonObj.toString(), transactionsElement, true);
            }
        }
    }

    /**
     * Gets the string key value.
     * 
     * @param key
     *            the key
     * @param jsonObj
     *            the json obj
     * @return the string key value
     */
    public static String getStringKeyValue(final String key, final JSONObject jsonObj) {
        String result = null;
        if (jsonObj.has(key)) {
            result = jsonObj.getString(key);
        }

        return result;
    }

    /**
     * Gets the date key value.
     * 
     * @param dateFormat
     *            the date format
     * @param key
     *            the key
     * @param jsonObj
     *            the json obj
     * @return the date key value
     * @throws ParseException
     *             the parse exception
     */
    public static Date getDateKeyValue(final SimpleDateFormat dateFormat, final String key, final JSONObject jsonObj)
            throws ParseException {
        Date result = null;
        if (jsonObj.has(key) && !jsonObj.isNull(key) && !"".equals(jsonObj.getString(key))) {
            result = dateFormat.parse(jsonObj.getString(key));
        }

        return result;
    }

    /**
     * Parses the trasaction details.
     * 
     * @param json
     *            the json
     * @param transactions
     *            the transactions
     * @param isFromResponse
     *            the is from response
     * @throws ParseException
     *             the parse exception
     */
    public static void
            parseTrasactionDetails(final String json, final Transactions transactions, boolean isFromResponse)
                    throws ParseException {

        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        final JSONObject jsonObject = new JSONObject(json);
        transactions.setTransTrackingId(getStringKeyValue(TRANS_TRACKING_ID, jsonObject));
        transactions.setType(getStringKeyValue(TYPE, jsonObject));
        transactions.setOfflineIdentifier(getStringKeyValue(OFFLINE_ID, jsonObject));
        transactions.setCustomerId(getStringKeyValue(KEY_1, jsonObject));
        transactions.setAgentId(getStringKeyValue(KEY_11, jsonObject));
        transactions.setProductId(getStringKeyValue(KEY_5, jsonObject));
        transactions.setCreationDateTime(getDateKeyValue(dateFormat, KEY_13, jsonObject));
        transactions.setRetrieveType(getStringKeyValue(KEY_10, jsonObject));
        transactions.setKey18(getStringKeyValue(KEY_18, jsonObject));
        transactions.setKey19(getStringKeyValue(KEY_19, jsonObject));
        transactions.setKey20(getStringKeyValue(KEY_20, jsonObject));
        transactions.setKey21(getStringKeyValue(KEY_21, jsonObject));
        transactions.setKey22(getStringKeyValue(KEY_22, jsonObject));
        transactions.setKey23(getStringKeyValue(KEY_23, jsonObject));
        transactions.setKey24(getStringKeyValue(KEY_24, jsonObject));
        transactions.setKey25(getStringKeyValue(KEY_25, jsonObject));
        transactions.setKey7(getStringKeyValue(KEY_7, jsonObject));
        // Setting the lastSyncdate from rquest to transactions object

        if (transactions.getLastSyncDate() == null || transactions.getLastSyncDate().toString().equals("")) {
            transactions.setLastSyncDate(getDateKeyValue(dateFormat, KEY_12, jsonObject));
        }
		
		 // Added for uploadRequirementDoc Fix
        
        if(getDateKeyValue(dateFormat, CREATION_DATE, jsonObject)!=null){
            
            transactions.setBusinessCreationDate(getDateKeyValue(dateFormat, CREATION_DATE, jsonObject));
        }
        if(getDateKeyValue(dateFormat, MODIFIED_DATE, jsonObject)!=null){
            
            transactions.setBusinessModifiedDate(getDateKeyValue(dateFormat, MODIFIED_DATE, jsonObject));
        }
        
        
        
        

        if (!isFromResponse) {
            transactions.setProposalNumber(getStringKeyValue(KEY_4, jsonObject));
            transactions.setStatus(getStringKeyValue(KEY_15, jsonObject));
            transactions.setFnaId(getStringKeyValue(KEY_2, jsonObject));
            transactions.setIllustrationId(getStringKeyValue(KEY_3, jsonObject));
            transactions.setModifiedTimeStamp(getDateKeyValue(dateFormat, KEY_14, jsonObject));
        }

    }

    /**
     * Parses the trasaction details.
     * 
     * @param json
     *            the json
     * @param transactions
     *            the transactions
     * @throws ParseException
     *             the parse exception
     */
    public static void parseLMSTrasactionDetails(final String json, final Transactions transactions)
            throws ParseException {

        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        final JSONObject jsonObject = new JSONObject(json);
        transactions.setTransTrackingId(getStringKeyValue(TRANS_TRACKING_ID, jsonObject));
        transactions.setKey1(getStringKeyValue(KEY_1, jsonObject));
        transactions.setKey2(getStringKeyValue(KEY_2, jsonObject));
        transactions.setKey3(getStringKeyValue(KEY_3, jsonObject));
        transactions.setKey4(getStringKeyValue(KEY_4, jsonObject));
        transactions.setKey5(getStringKeyValue(KEY_5, jsonObject));
        transactions.setKey6(getStringKeyValue(KEY_6, jsonObject));
        transactions.setKey7(getStringKeyValue(KEY_7, jsonObject));
        transactions.setKey8(getStringKeyValue(KEY_8, jsonObject));
        transactions.setRetrieveType(getStringKeyValue(KEY_10, jsonObject));
        transactions.setAgentId(getStringKeyValue(KEY_11, jsonObject));
        transactions.setCreationDateTime(getDateKeyValue(dateFormat, KEY_13, jsonObject));
        transactions.setModifiedTimeStamp(getDateKeyValue(dateFormat, KEY_14, jsonObject));

        transactions.setType(getStringKeyValue(TYPE, jsonObject));
        transactions.setOfflineIdentifier(getStringKeyValue(OFFLINE_ID, jsonObject));
        transactions.setKey18(getStringKeyValue(KEY_18, jsonObject));
        transactions.setKey19(getStringKeyValue(KEY_19, jsonObject));
        transactions.setKey20(getStringKeyValue(KEY_20, jsonObject));
        transactions.setKey21(getStringKeyValue(KEY_21, jsonObject));
        transactions.setKey22(getStringKeyValue(KEY_22, jsonObject));
        transactions.setKey23(getStringKeyValue(KEY_23, jsonObject));
        transactions.setKey24(getStringKeyValue(KEY_24, jsonObject));
        transactions.setKey25(getStringKeyValue(KEY_25, jsonObject));
		
		
        if(getDateKeyValue(dateFormat, CREATION_DATE, jsonObject)!=null){
            
            transactions.setBusinessCreationDate(getDateKeyValue(dateFormat, CREATION_DATE, jsonObject));
        }
        if(getDateKeyValue(dateFormat, MODIFIED_DATE, jsonObject)!=null){
            
            transactions.setBusinessModifiedDate(getDateKeyValue(dateFormat, MODIFIED_DATE, jsonObject));
        }
    }

    /**
     * Gets the currentdate.
     * 
     * @return the currentdate
     * @throws ParseException
     *             the parse exception
     */
    public static Date getCurrentdate() throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        final Calendar calendar = new GregorianCalendar();
        final String currentDate = dateFormat.format(calendar.getTime());
        return dateFormat.parse(currentDate);
    }

    /**
     * Creates the response status.
     * 
     * @param transactions
     *            the transactions
     * @param proposalstatus
     *            the proposalstatus
     * @param message
     *            the message
     * @param errorCode
     *            the error code
     */
    public static void createResponseStatus(Transactions transactions, String proposalstatus, String message,
            String errorCode) {
        final StatusData statusData = new StatusData();
        if (Constants.REJECTED.equals(proposalstatus)) {
            statusData.setStatus(LifeEngageComponentHelper.REJECTED);
            statusData.setStatusCode(ErrorConstants.LE_SYNC_ERR_106);
            statusData.setStatusMessage(message);
            transactions.setStatus(proposalstatus);
            transactions.setSyncError(ErrorConstants.LE_SYNC_ERR_106);

        } else if (Constants.FAILURE.equals(proposalstatus)) {
            statusData.setStatus(LifeEngageComponentHelper.FAILURE);
            statusData.setStatusCode(errorCode);
            statusData.setStatusMessage(message);
            transactions.setSyncError(errorCode);
        } else {
            statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
            statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE);
            statusData.setStatusMessage(message);
            if (transactions.getMode() == Constants.STATUS_UPDATE) {

                Date lastSyncDate = transactions.getModifiedTimeStamp();
                transactions.setLastSyncDate(lastSyncDate);
            }

            else if (transactions.getMode() == Constants.STATUS_SAVE) {

                Date lastSyncDate = transactions.getCreationDateTime();
                transactions.setLastSyncDate(lastSyncDate);
            }
            transactions.setStatus(proposalstatus);
        }
        transactions.setStatusData(statusData);
        transactions.setSyncStatus(statusData.getStatus());

    }

    /**
     * Genaralized Method added to set response code and status in Transaction to reduce method cyclomatic complexity
     * from component layer.
     * 
     * @param transactions
     *            the transactions
     * @param e
     *            the e
     */
    public static void createResponseStatusForExceptionType(Transactions transactions, Exception e) {
        if (e instanceof DaoException) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_105);
        } else if (e instanceof BusinessException) {
            LOGGER.error("BusinessException", e);
            if (((BusinessException) e).getErrorCode() == null
                    || "".equals(((BusinessException) e).getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_103);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ((BusinessException) e).getErrorCode());
            }
        } else if (e instanceof SystemException) {
            LOGGER.error("SystemException", e);
            if (((SystemException) e).getErrorCode() == null || "".equals(((SystemException) e).getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ((SystemException) e).getErrorCode());
            }
        } else if (e instanceof IOException) {
            LOGGER.error("IO Exception : ", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_105);
        } else if (e instanceof Exception) {
            LOGGER.error("Exception : ", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_100);
        }
    }

    /**
     * Gets the currentdate without time.
     * 
     * @return the currentdate without time
     * @throws ParseException
     *             the parse exception
     */
    public static Date getCurrentdateWithoutTime() throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATEFORMAT_YYYY_MM, Locale.getDefault());
        final Calendar calendar = new GregorianCalendar();
        final String currentDate = dateFormat.format(calendar.getTime());
        return dateFormat.parse(currentDate);
    }

    /**
     * Gets the current date limit.
     * 
     * @return the current date limit
     * @throws ParseException
     *             the parse exception
     */
    public static List<Date> getCurrentDateLimit() throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        List<Date> currentDateLimit = new ArrayList<Date>();
        final Calendar calendar = new GregorianCalendar();
        getStartOfADay(calendar);
        currentDateLimit.add(dateFormat.parse(dateFormat.format(calendar.getTime())));
        getEndOfADay(calendar);
        currentDateLimit.add(dateFormat.parse(dateFormat.format(calendar.getTime())));
        return currentDateLimit;
    }

    /**
     * Gets the start and end date of week.
     * 
     * @return the start and end date of week
     * @throws ParseException
     *             the parse exception
     */
    public static List<Date> getStartAndEndDateOfWeek() throws ParseException {
        List<Date> startAndEndWeeKList = new ArrayList<Date>();
        // set the date
        Calendar cal = Calendar.getInstance();

        // "calculate" the start date of the week
        Calendar first = (Calendar) cal.clone();
        first.add(Calendar.DAY_OF_WEEK, first.getFirstDayOfWeek() - first.get(Calendar.DAY_OF_WEEK));
        getStartOfADay(first);
        // and add six days to the end date
        Calendar last = (Calendar) first.clone();
        last.add(Calendar.DAY_OF_YEAR, 6);
        getEndOfADay(last);
        // print the result
        SimpleDateFormat df = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        startAndEndWeeKList.add(df.parse(df.format(first.getTime())));
        startAndEndWeeKList.add(df.parse(df.format(last.getTime())));
        return startAndEndWeeKList;
    }

    /**
     * Gets the start and end date of month.
     * 
     * @return the start and end date of month
     * @throws ParseException
     *             the parse exception
     */
    public static List<Date> getStartAndEndDateOfMonth() throws ParseException {
        List<Date> startAndEndMonthList = new ArrayList<Date>();
        SimpleDateFormat format = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        Calendar c = new GregorianCalendar();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = 1;
        c.set(year, month, day);
        int numOfDaysInMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        getStartOfADay(c);
        startAndEndMonthList.add(format.parse(format.format(c.getTime())));
        c.add(Calendar.DAY_OF_MONTH, numOfDaysInMonth - 1);
        getEndOfADay(c);
        startAndEndMonthList.add(format.parse(format.format(c.getTime())));
        return startAndEndMonthList;
    }

    /**
     * Gets the start and end date of limit.
     * 
     * @param dateLimit
     *            the date limit
     * @return the start and end date of limit
     * @throws ParseException
     *             the parse exception
     */
    public static List<Date> getStartAndEndDateOfLimit(String dateLimit) throws ParseException {
        List<Date> startAndEndDateList = new ArrayList<Date>();
        Calendar currentDate = new GregorianCalendar();
        Calendar startDate = (Calendar) currentDate.clone();
        Calendar endDate = (Calendar) currentDate.clone();
        startDate.add(Calendar.DATE, -Integer.valueOf(dateLimit));
        getStartOfADay(startDate);
        endDate.add(Calendar.DATE, Integer.valueOf(dateLimit));
        getEndOfADay(endDate);
        SimpleDateFormat format = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        startAndEndDateList.add(format.parse(format.format(startDate.getTime())));
        startAndEndDateList.add(format.parse(format.format(endDate.getTime())));
        return startAndEndDateList;
    }

    /**
     * Gets the start of a day.
     * 
     * @param date
     *            the date
     * @return the start of a day
     */
    private static void getStartOfADay(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
    }

    /**
     * Gets the end of a day.
     * 
     * @param date
     *            the date
     * @return the end of a day
     */
    private static void getEndOfADay(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 59);
        date.set(Calendar.SECOND, 59);
        date.set(Calendar.MILLISECOND, 999);
    }

    /**
     * Gets the current year dob.
     * 
     * @param dob
     *            the dob
     * @return the current year dob
     * @throws ParseException
     *             the parse exception
     */
    public static Date getCurrentYearDOB(String dateOfBirth) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATEFORMAT_YYYY_MM, Locale.getDefault());
        Calendar dob = new GregorianCalendar();
        dob.setTime(format.parse(dateOfBirth));
        int month = dob.get(dob.MONTH);
        int date = dob.get(dob.DATE);
        dob = new GregorianCalendar();
        dob.set(Calendar.MONTH, month);
        dob.set(Calendar.DATE, date);
        return dob.getTime();
    }
    
    public static void createStatusResponse(final RequestInfo requestInfo, final JSONArray jsonRsArray,
            final JSONObject jsonObjectRs,String response){
    	 final JSONObject jsonObject = new JSONObject();
    	 final JSONObject statusObject=new JSONObject();
    	// final StatusData statusData = new StatusData();
    	 //final JSONArray jsonArray=new JSONArray();
         if (response.equalsIgnoreCase("Success")) {
        	 statusObject.put("Status", LifeEngageComponentHelper.SUCCESS);
             statusObject.put("StatusCode", LifeEngageComponentHelper.SUCCESS_CODE);
             statusObject.put("Message", "");
         } else {
        	 statusObject.put("Status", LifeEngageComponentHelper.FAILURE);
             statusObject.put("StatusCode", LifeEngageComponentHelper.FAILURE_CODE);
             statusObject.put("Message", "");
         }
         jsonRsArray.put(statusObject);
         final JSONObject jsonResponceObject = new JSONObject();
         final JSONObject jsonResponseInfoObject = new JSONObject();

         final SimpleDateFormat formatterForDate = new SimpleDateFormat(DATEFORMAT_MM_DD_YYYY, Locale.getDefault());
         final SimpleDateFormat formatterForTime = new SimpleDateFormat(DATEFORMAT_HH_MM_SS, Locale.getDefault());

         setStringValueInJsonObject(USER_NAME, requestInfo.getUserName(), jsonResponseInfoObject);
         setDateValueInJsonObject(CREATION_DATE, requestInfo.getCreationDateTime(), jsonResponseInfoObject,
                 formatterForDate);
         setDateValueInJsonObject(CREATION_TIME, requestInfo.getCreationDateTime(), jsonResponseInfoObject,
                 formatterForTime);
         setStringValueInJsonObject(SOURCE_INFO_NAME, requestInfo.getSourceInfoName(), jsonResponseInfoObject);
         setStringValueInJsonObject(TRANSACTION_ID, requestInfo.getTransactionId(), jsonResponseInfoObject);
         setStringValueInJsonObject(REQUESTOR_TOKEN, requestInfo.getRequestorToken(), jsonResponseInfoObject);
         setStringValueInJsonObject(USER_EMAIL, requestInfo.getUserEmail(), jsonResponseInfoObject);
         setStringValueInJsonObject(DEVICE_UUID, requestInfo.getDeviceUUID(), jsonResponseInfoObject);
         
         jsonResponceObject.put("ResponseInfo", jsonResponseInfoObject);
         jsonObject.put(TRANSACTIONS, jsonRsArray);
         jsonResponceObject.put(RESPONSE_PAYLOAD, jsonObject);
         jsonObjectRs.put(RESPONSE, jsonResponceObject);
    	
    }
    
    private static void setStringValueInJsonObject(final String key, 
            final String value, final JSONObject jsonObjectRs) {
        if (StringUtils.isNotEmpty(value)) {
            jsonObjectRs.put(key, value);
        } else {
            jsonObjectRs.put(key, "");
        }
    }
    
    private static void setDateValueInJsonObject(final String key, final Date value, final JSONObject jsonObjectRs,
            final SimpleDateFormat formatterForDate) {
        if (value == null) {
            jsonObjectRs.put(key, "");
        } else {
            jsonObjectRs.put(key, formatterForDate.format(value));
        }
    }
    
 // Newly Added for Thailand
    /**
     * Creates the response status.
     * 
     * @param transactions
     *            the transactions
     * @param proposalstatus
     *            the proposalstatus
     * @param message
     *            the message
     * @param errorCode
     *            the error code
     */
    public static void generateResponseStatusObj(Transactions transactions, String status, String message,String errorCode) {   
        final StatusData statusData = new StatusData();
        statusData.setStatus(status);
        statusData.setStatusCode(errorCode);
        statusData.setStatusMessage(message);
        transactions.setStatusData(statusData);
        transactions.setSyncStatus(statusData.getStatus());
    }


}
