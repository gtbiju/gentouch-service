/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304000
 * @version       : 0.1, Feb 10, 2014
 */
package com.cognizant.insurance.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngageFNAComponent;
import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.FNARepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;

// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageFNAComponentImpl.
 */
@Service("lifeEngageFNAComponent")
public class LifeEngageFNAComponentImpl implements LifeEngageFNAComponent {

    /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;

    /** The fna repository. */
    @Autowired
    private FNARepository fnaRepository;

    /** The save fna holder. */
    @Autowired
    @Qualifier("fnaSaveMapping")
    private LifeEngageSmooksHolder saveFNAHolder;

    /** The retrieve FNA holder. */
    @Autowired
    @Qualifier("fnaRetrieveMapping")
    private LifeEngageSmooksHolder retrieveFNAHolder;

    /** The retrieve fna holder. */
    @Autowired
    @Qualifier("fnaRetrieveBasicDtlsMapping")
    private LifeEngageSmooksHolder retrieveBasicDtlsFNAHolder;

    /** The retrieve fna holder. */
    @Autowired
    @Qualifier("fnaRetrieveFullDtlsMapping")
    private LifeEngageSmooksHolder retrieveFullDtlsFNAHolder;

    /** The id generator. */
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;

    /** The Constant SUCCESS_MESSAGE_SAVE. */
    private static final String SUCCESS_MESSAGE_SAVE = "FNA Saved";

    /** The Constant SUCCESS_MESSAGE_UPDATE. */
    private static final String SUCCESS_MESSAGE_UPDATE = "FNA Updated";

    /** The Constant REJECTED_MESSAGE. */
    private static final String REJECTED_MESSAGE = "FNA Rejected";

    /** The Constant FNA. */
    private static final String FNA = "FNA";

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageFNAComponentImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageFNAComponentImpl#saveFNA(java.lang.String,
     * com.cognizant.insurance.request.vo.RequestInfo, com.cognizant.insurance.audit.LifeEngageAudit)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String
            saveFNA(final String json, final RequestInfo requestInfo, final LifeEngageAudit lifeEngageAudit) {

        LOGGER.info(":::: Entering saveFNA in LifeEngageFNAComponentImpl 1 : input json >>" + json);

        String fnaResponse;
        final Transactions transactions = new Transactions();
        String successMessage;
        String offlineIdentifier;
        try {
            LifeEngageComponentHelper.parseTrasactionDetails(json, transactions, false);
            // Store to a temp variable to set it back to response.
            offlineIdentifier = transactions.getOfflineIdentifier();

            // Do not process FNA data if already done
            if (auditRepository.isTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                throwSystemException(true, ErrorConstants.LE_SYNC_ERR_102, Constants.PROCESSED_TRANSACTION);
            }
			/**
			 * Update flow was depending on fnaId alone. Instead will verify
			 * whether the data exist for transTrackingId and AgentId
			 * combination. If so , considered as update flow. Taken this as a
			 * bug fix when multple save was happenning for same record with
			 * same transTrackingId, eg: data gets saved in backend, but not
			 * getting updated in UI and hence the same record comes again
			 * without fnaId
			 **/
			Transactions retrieveTransaction = new Transactions();
			GoalAndNeed goalAndNeed = new GoalAndNeed();
			if (StringUtils.isNotEmpty(transactions.getFnaId())
					|| StringUtils
							.isNotEmpty(transactions.getTransTrackingId())) {
				retrieveTransaction = fnaRepository.retrieveFNA(transactions,
						requestInfo);
			}
			if (retrieveTransaction != null
					&& retrieveTransaction.getGoalAndNeed() != null) {
				// If fna identifier exists ==> the mode is update
				goalAndNeed = (GoalAndNeed) retrieveTransaction
						.getGoalAndNeed();
				if (StringUtils.isEmpty(transactions.getFnaId())) {
					transactions.setFnaId(retrieveTransaction.getGoalAndNeed()
							.getIdentifier());
				}
				transactions.setMode("update");
				successMessage = SUCCESS_MESSAGE_UPDATE;
			} else {

				transactions.setFnaId(transactions.getAgentId()
						+ String.valueOf(idGenerator.generate(FNA)));
				transactions.setMode("save");
				successMessage = SUCCESS_MESSAGE_SAVE;
			}
            onBeforeSave(transactions);
            Transactions fnaTransactions = fnaRepository.saveFNA(transactions, requestInfo, json,goalAndNeed);
            onAfterSave(transactions);
            final String proposalstatus = fnaTransactions.getStatus();
            if (Constants.REJECTED.equals(proposalstatus)) {
                successMessage = REJECTED_MESSAGE;
            }
            LifeEngageComponentHelper.createResponseStatus(transactions, proposalstatus, successMessage, null);

            transactions.setOfflineIdentifier(offlineIdentifier);
            transactions.setLastSyncDate(fnaTransactions.getLastSyncDate());

            auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit,"");
            fnaResponse = saveFNAHolder.parseBO(transactions);

        } catch (ParseException e) {
            LOGGER.error("ParseException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_101);
            fnaResponse = saveFNAHolder.parseBO(transactions);
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_104);
            fnaResponse = saveFNAHolder.parseBO(transactions);

        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                        + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_105);

			} else if (ErrorConstants.LE_SYNC_ERR_102.equals(e.getErrorCode())) {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						LifeEngageComponentHelper.SUCCESS,
						SUCCESS_MESSAGE_SAVE, null);

			} else {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						Constants.FAILURE, e.getMessage() + " : "
								+ getExceptionMessage(e), e.getErrorCode());
			}
            fnaResponse = saveFNAHolder.parseBO(transactions);

        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
            fnaResponse = saveFNAHolder.parseBO(transactions);

        }
        LOGGER.info(":::: Exiting saveFNA in LifeEngageFNAComponentImpl 1 : fnaResponse>>" + fnaResponse);
        return fnaResponse;
    }

    /**
     * On before save.
     * 
     * @param transactions
     *            the transactions
     */
    protected void onBeforeSave(Transactions transactions) {

    }

    /**
     * On after save.
     * 
     * @param transactions
     *            the transactions
     */
    protected void onAfterSave(Transactions transactions) {

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageFNAComponent#retrieveAllFNA(com.cognizant.insurance
     * .request.vo.RequestInfo, org.json.JSONObject)
     */
    @Override
    public String retrieveAllFNA(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
            ParseException {

        LOGGER.debug(":::: Entering retrieveAllFNA in LifeEngageFNAComponentImpl 1 : input json >>" + " "
                + jsonObj.toString());

        List<Transactions> transactionlist = new ArrayList<Transactions>();
        final Transactions transactions = new Transactions();
        LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), transactions, false);
        String fnaMapToReturn = "{\"Transactions\":[]}";
        boolean fullFetch = true;
        if (jsonObj.has(LifeEngageComponentHelper.KEY_10)
                && jsonObj.getString(LifeEngageComponentHelper.KEY_10) != null) {
            if (LifeEngageComponentHelper.FULL_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                // Retrieve All details for all proposals for an agent
                transactionlist = fnaRepository.retrieveAll(transactions, requestInfo, true);

            } else if (LifeEngageComponentHelper.BASIC_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                // Retrieve basic details for listing all proposals for an agent
                transactionlist = fnaRepository.retrieveAll(transactions, requestInfo, false);
                fullFetch = false;
            }
        }
        LifeEngageComponentHelper.buildTransactionsList(transactionlist, jsonObj);
        if (fullFetch) {
            fnaMapToReturn = retrieveFullDtlsFNAHolder.parseBO(transactionlist);
        } else {
            fnaMapToReturn = retrieveBasicDtlsFNAHolder.parseBO(transactionlist);
        }
        LOGGER.debug(":::: Exiting retrieveAllFNA in LifeEngageFNAComponentImpl 1 : fnaMapToReturn >>" + fnaMapToReturn);
        return fnaMapToReturn;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageIllustrationComponent#retrieveIllustration(com.cognizant.insurance
     * .request.vo.RequestInfo, org.json.JSONObject)
     */
    @Override
    public String retrieveFNA(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
            ParseException {

        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        if (jsonObj.has(LifeEngageComponentHelper.KEY_2) && jsonObj.getString(LifeEngageComponentHelper.KEY_2) != null
                && !"".equals(jsonObj.getString(LifeEngageComponentHelper.KEY_2))
                && !jsonObj.getString(LifeEngageComponentHelper.KEY_2).equals("null")) {
            final Transactions transactions = (Transactions) retrieveFNAHolder.parseJson(jsonObj.toString());
            try {
                transaction = fnaRepository.retrieveFNA(transactions, requestInfo);
            } catch (Exception e) {
                LOGGER.error("Unable to retrieve FNA :" + e.getMessage());
                transaction = transactions;
                LifeEngageComponentHelper.createResponseStatus(transaction, Constants.FAILURE, e.getMessage() + " : "
                        + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
            }
            LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), transaction, true);
            transactionsList.add(transaction);
        }
        return retrieveFNAHolder.parseBO(transactionsList);
    }

    /**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }

    /**
     * Gets the auditRepository.
     * 
     * @return Returns the auditRepository.
     */
    public AuditRepository getAuditRepository() {
        return auditRepository;
    }

    /**
     * Sets The auditRepository.
     * 
     * @param auditRepository
     *            The auditRepository to set.
     */
    public final void setAuditRepository(final AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    /**
     * Gets the fnaRepository.
     * 
     * @return Returns the fnaRepository.
     */
    public FNARepository getFnaRepository() {
        return fnaRepository;
    }

    /**
     * Sets The fnaRepository.
     * 
     * @param fnaRepository
     *            The fnaRepository to set.
     */
    public final void setFnaRepository(final FNARepository fnaRepository) {
        this.fnaRepository = fnaRepository;
    }

    /**
     * Gets the saveFNAHolder.
     * 
     * @return Returns the saveFNAHolder.
     */
    public LifeEngageSmooksHolder getSaveFNAHolder() {
        return saveFNAHolder;
    }

    /**
     * Sets The saveFNAHolder.
     * 
     * @param saveFNAHolder
     *            The saveFNAHolder to set.
     */
    public final void setSaveFNAHolder(final LifeEngageSmooksHolder saveFNAHolder) {
        this.saveFNAHolder = saveFNAHolder;
    }

    /**
     * Gets the retrieveBasicDtlsFNAHolder.
     * 
     * @return Returns the retrieveBasicDtlsFNAHolder.
     */
    public LifeEngageSmooksHolder getRetrieveBasicDtlsFNAHolder() {
        return retrieveBasicDtlsFNAHolder;
    }

    /**
     * Sets The retrieveBasicDtlsFNAHolder.
     * 
     * @param retrieveBasicDtlsFNAHolder
     *            The retrieveBasicDtlsFNAHolder to set.
     */
    public final void setRetrieveBasicDtlsFNAHolder(final LifeEngageSmooksHolder retrieveBasicDtlsFNAHolder) {
        this.retrieveBasicDtlsFNAHolder = retrieveBasicDtlsFNAHolder;
    }

    /**
     * Gets the retrieveFullDtlsFNAHolder.
     * 
     * @return Returns the retrieveFullDtlsFNAHolder.
     */
    public LifeEngageSmooksHolder getRetrieveFullDtlsFNAHolder() {
        return retrieveFullDtlsFNAHolder;
    }

    /**
     * Sets The retrieveFullDtlsFNAHolder.
     * 
     * @param retrieveFullDtlsFNAHolder
     *            The retrieveFullDtlsFNAHolder to set.
     */
    public final void setRetrieveFullDtlsFNAHolder(final LifeEngageSmooksHolder retrieveFullDtlsFNAHolder) {
        this.retrieveFullDtlsFNAHolder = retrieveFullDtlsFNAHolder;
    }

}
