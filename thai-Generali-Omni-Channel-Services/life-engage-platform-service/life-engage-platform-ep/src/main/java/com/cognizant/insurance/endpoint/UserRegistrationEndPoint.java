package com.cognizant.insurance.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.omnichannel.service.UserService;

@Controller
@RequestMapping("/userService")
public class UserRegistrationEndPoint {
	
	  @Autowired
	  private UserService userService;
	 
	 	 
	    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	    @ResponseBody
	    public String register(@RequestBody String inputJson) {
	        return userService.register(inputJson);
	    }
	 
	    @RequestMapping(value = "/createPassword", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	    @ResponseBody
	    public String createPassword(@RequestBody String inputJson) {
	          return userService.createPassword(inputJson);
	    }
	   
	    @RequestMapping(value = "/forgetPassword", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	    @ResponseBody
	    public String forgetPassword(@RequestBody String inputJson) {
	        return userService.forgetPassword(inputJson);
	    }	
	    
	    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	    @ResponseBody
	    public String resetPassword(@RequestBody String inputJson) {
	          return userService.resetPassword(inputJson);
	    }
	    
}
