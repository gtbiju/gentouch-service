package com.cognizant.insurance.omnichannel.component.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agreement.component.impl.AgreementComponentImpl;
import com.cognizant.insurance.agreement.dao.AgreementDao;
import com.cognizant.insurance.agreement.dao.impl.AgreementDaoImpl;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.RiderSequence;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.omnichannel.component.GeneraliAgreementComponent;
import com.cognizant.insurance.omnichannel.dao.GeneraliAgreementDao;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliAgreementDaoImpl;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public class GeneraliAgreementComponentImpl extends AgreementComponentImpl implements GeneraliAgreementComponent {
    
    @Autowired
    private GeneraliAgreementDao agreementDao;

   
    @Override
    public Response<SearchCountResult> getAgreementCountForLMS(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        
        Response<SearchCountResult> searchCountResponse =
                agreementDao.getAgreementCountForLMS(searchCriteriatRequest, statusRequest);
        return searchCountResponse;
    }
    
    @Override
    public Response<List<Agreement>> getAgreementByFilterRelatedTransactions(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
       // final AgreementDao agreementDao = new AgreementDaoImpl();
        final List<Agreement> agreements = agreementDao.getAgreementByFilterRelatedTransactions(searchCriteriatRequest, statusRequest);

        final Response<List<Agreement>> agreementFilterResponse = new ResponseImpl<List<Agreement>>();
        if (!agreements.isEmpty()) {
            agreementFilterResponse.setType(agreements);
        }
        return agreementFilterResponse;
    }
    
    @Override
    public Response<List<Agreement>> getAgreementByFilter(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final List<Agreement> agreements = agreementDao.getAgreementByFilter(searchCriteriatRequest, statusRequest);

        final Response<List<Agreement>> agreementFilterResponse = new ResponseImpl<List<Agreement>>();
        if (!agreements.isEmpty()) {
            agreementFilterResponse.setType(agreements);
        }
        return agreementFilterResponse;
    }

    @Override
    public Response<Set<AgreementDocument>> getBase64DocumentDetils(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        // TODO Auto-generated method stub
        return  agreementDao.getBase64DocumentDetils(agreementRequest, statusRequest);
    }
    
    @Override
    public Agreement updateInsuranceAgreementWithRequirementDocUpdates(
            Request<Agreement> agreementRequest, Request<List<AgreementStatusCodeList>> statusRequest) {
        final GeneraliAgreementDao agreementDao = new GeneraliAgreementDaoImpl();
        Agreement agreementWithRequirement=agreementDao.getInsuranceAgreementForReqDocUpdates(agreementRequest, statusRequest);
		return agreementWithRequirement;
    }

	@Override
	public Response<List<Agreement>> getRelatedAgreementsForLMS(
			Request<SearchCriteria> searchCriteriatRequest,
			Request<List<AgreementStatusCodeList>> statusRequest) {
		   final List<Agreement> agreements = agreementDao.getRelatedAgreementsForLMS(searchCriteriatRequest, statusRequest);

	        final Response<List<Agreement>> agreementFilterResponse = new ResponseImpl<List<Agreement>>();
	        if (!agreements.isEmpty()) {
	            agreementFilterResponse.setType(agreements);
	        }
	        
	        return agreementFilterResponse;
	
	}

    @Override
    public Response<List<Object>> getCountForSalesActivity(Request<SearchCriteria> searchCriteriatRequest) {
        final List<Object> countListSA = agreementDao.getCountForSalesActivity(searchCriteriatRequest);
        final Response<List<Object>> countList = new ResponseImpl<List<Object>>();
        countList.setType(countListSA);

        return countList;
    }

    public Response<GeneraliAgent>retrieveGeneraliAgent(Request<GeneraliAgent> agentRequest,String agentID){
    	GeneraliAgent generaliagent =  agreementDao.getGeneraliAgent(agentRequest,agentID);
    	final Response<GeneraliAgent> generaliAgentResponse = new ResponseImpl<GeneraliAgent>();
    	generaliAgentResponse.setType(generaliagent);
    	return generaliAgentResponse;
    	
    }

    @Override
    public Map<String, Integer> retrieveMonthwiseCount(Request<SearchCriteria> searchCriteriatRequest) {
        final Map<String,Integer> monthwiseCount = agreementDao.retrieveMonthwiseCount(searchCriteriatRequest);
        return monthwiseCount;
    }

    @Override
    public Map<String, Integer> retrieveWeeklyCount(Request<SearchCriteria> searchCriteriatRequest) {
        final Map<String,Integer> weeklyCount = agreementDao.retrieveWeeklyCount(searchCriteriatRequest);
        return weeklyCount;
    }

    @Override
    public Response<List<Agreement>> getMonthlyData(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final List<Agreement> agreements = agreementDao.getMonthlyData(searchCriteriatRequest, statusRequest);

        final Response<List<Agreement>> agreementFilterResponse = new ResponseImpl<List<Agreement>>();
        if (!agreements.isEmpty()) {
            agreementFilterResponse.setType(agreements);
        }
        return agreementFilterResponse;
    }

    @Override
    public Response<InsuranceAgreement> getEAppAgreementByAppNo(Request<InsuranceAgreement> aggrementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final InsuranceAgreement agreement = agreementDao.getEAppAgreementByAppNo(aggrementRequest, statusRequest);
        final Response<InsuranceAgreement> agreementResponse = new ResponseImpl<InsuranceAgreement>();
        agreementResponse.setType(agreement);
        return agreementResponse;
    }
    
    @Override
    public Response<SearchCountResult> getAgreementCountForIllustration(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest, final Request<List<AgreementStatusCodeList>> statusRequestForDraft) {
        Response<SearchCountResult> searchCountResponse =
                agreementDao.getAgreementCountForIllustration(searchCriteriatRequest, statusRequest, statusRequestForDraft);
        return searchCountResponse;
    }
    
    @Override
    public Response<SearchCountResult> getAgreementCountforEapp(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        Response<SearchCountResult> searchCountResponse =
                agreementDao.getAgreementCountForEApp(searchCriteriatRequest, statusRequest);
        return searchCountResponse;
    }

    @Override
    public List<Agreement> getAggrementsWithExpiredGAOLocks(Request<Agreement> agreementRequest) {
       return agreementDao.getAggrementsWithExpiredGAOLocks(agreementRequest);
    }

    @Override
    public Response<List<Agreement>> getAgreementsList(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest, Request<Integer> limitRequest) {
        final List<Agreement> agreements = agreementDao.getAgreements(agreementRequest, statusRequest, limitRequest);

        final Response<List<Agreement>> response = new ResponseImpl<List<Agreement>>();
        if (!agreements.isEmpty()) {
            response.setType(agreements);
        }
        return response;
    }
    
    @Override
    public List<RiderSequence> retrieveRiderSequence() {
    	List<RiderSequence> riderSequence = agreementDao.retrieveRiderSequence();
    	return riderSequence;
    }

    @Override
    public Map<String, String> getEAppIllustrationCountForReport(Request<SearchCriteria> searchCriteriatRequest) {
        final Map<String, String> count = agreementDao.getEAppIllustrationCountForReport(searchCriteriatRequest);
        return count;
    }
}
