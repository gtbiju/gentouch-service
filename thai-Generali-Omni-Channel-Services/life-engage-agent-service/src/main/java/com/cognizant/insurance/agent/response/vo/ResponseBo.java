/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.agent.response.vo;


// TODO: Auto-generated Javadoc
/**
 * The Class class ResponseBo.
 *
 * @author 345166
 * 
 * Class ResponseBo - Created for EApp. Used to map ResponseBo details.
 */
public class ResponseBo {
	
	/** The response info. */
	private ResponseInfo responseInfo;
    
	/** The response payload. */
	private ResponsePayload responsePayload;
	
	
	/**
	 * Gets the response info.
	 *
	 * @return the response info
	 */
	public ResponseInfo getResponseInfo() {
		return responseInfo;
	}

	/**
	 * Sets the response info.
	 *
	 * @param responseInfo the response info to set.
	 */
	public void setResponseInfo(ResponseInfo responseInfo) {
		this.responseInfo = responseInfo;
	}

	/**
	 * Gets the response payload.
	 *
	 * @return the response payload
	 */
	public ResponsePayload getResponsePayload() {
		return responsePayload;
	}

	/**
	 * Sets the response payload.
	 *
	 * @param responsePayload the response payload to set.
	 */
	public void setResponsePayload(ResponsePayload responsePayload) {
		this.responsePayload = responsePayload;
	}
}
