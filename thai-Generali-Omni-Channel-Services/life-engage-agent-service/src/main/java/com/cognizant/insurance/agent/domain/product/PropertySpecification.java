/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;

/**
 * The Class PropertySpecification.
 *
 * @author 304007
 */

@Entity
@Table(name = "CORE_PROD_PROPERTIES")
public class PropertySpecification extends InformationModelObject {
        
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8730014899225576705L;
    
    /** The name. */
    @Column(name="NAME")
    private String name;
    
    /** The version. */
    @Column(name="VERSION")
    private String version;

    /** The default value. */
    @Column(name="DEFAULT_VALUE")
    private String defaultValue;
    
    /** The min value. */
    @Column(name="MIN_VALUE")
    private String minValue;
    
    /** The max value. */
    @Column(name="MAX_VALUE")
    private String maxValue;
    
    /** The optional indicator. */
    @Column(name="OPTIONAL_INDICATOR")
    private String optionalIndicator;
    
    /** The calculated indicator. */
    @Column(name="CALCULATED_INDICATOR")
    private String calculatedIndicator;
    
    /** The constant indicator. */
    @Column(name="CONSTANT_INDICATOR")
    private String constantIndicator;
    
    /** The property values. */  
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE },fetch = FetchType.EAGER, mappedBy="propertySpecification" )
    private Set<PropertyValues> propertyValues;
    
    /** The parent product. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }) 
    private ProductSpecification parentProduct;
    
    /** The property type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }) 
    @JoinColumn(name="propertyType_ID")
    private PropertySpecificationType propertyType;
    
    /**
     * Gets the default value.
     *
     * @return the default value
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the default value.
     *
     * @param defaultValue the default value
     */
    public void setDefaultValue(final String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Gets the min value.
     *
     * @return the min value
     */
    public String getMinValue() {
        return minValue;
    }

    /**
     * Sets the min value.
     *
     * @param minValue the min value
     */
    public void setMinValue(final String minValue) {
        this.minValue = minValue;
    }

    /**
     * Gets the max value.
     *
     * @return the max value
     */
    public String getMaxValue() {
        return maxValue;
    }

    /**
     * Sets the max value.
     *
     * @param maxValue the max value
     */
    public void setMaxValue(final String maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * Gets the optional indicator.
     *
     * @return the optional indicator
     */
    public String getOptionalIndicator() {
        return optionalIndicator;
    }

    /**
     * Sets the optional indicator.
     *
     * @param optionalIndicator the optional indicator
     */
    public void setOptionalIndicator(final String optionalIndicator) {
        this.optionalIndicator = optionalIndicator;
    }

    /**
     * Gets the calculated indicator.
     *
     * @return the calculated indicator
     */
    public String getCalculatedIndicator() {
        return calculatedIndicator;
    }

    /**
     * Sets the calculated indicator.
     *
     * @param calculatedIndicator the calculated indicator
     */
    public void setCalculatedIndicator(final String calculatedIndicator) {
        this.calculatedIndicator = calculatedIndicator;
    }

    /**
     * Gets the constant indicator.
     *
     * @return the constant indicator
     */
    public String getConstantIndicator() {
        return constantIndicator;
    }

    /**
     * Sets the constant indicator.
     *
     * @param constantIndicator the constant indicator
     */
    public void setConstantIndicator(final String constantIndicator) {
        this.constantIndicator = constantIndicator;
    }

    /**
     * Sets The propertyValues.
     *
     * @param propertyValues The propertyValues to set.
     */
    public void setPropertyValues(final Set<PropertyValues> propertyValues) {
        this.propertyValues = propertyValues;
    }

    /**
     * Gets the propertyValues.
     *
     * @return Returns the propertyValues.
     */
    public Set<PropertyValues> getPropertyValues() {
        return propertyValues;
    }

    /**
     * Gets the name.
     *
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The name.
     *
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the version.
     *
     * @return Returns the version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets The version.
     *
     * @param version The version to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * Gets the parentProduct.
     *
     * @return Returns the parentProduct.
     */
    public ProductSpecification getParentProduct() {
        return parentProduct;
    }

    /**
     * Sets The parentProduct.
     *
     * @param parentProduct The parentProduct to set.
     */
    public void setParentProduct(final ProductSpecification parentProduct) {
        this.parentProduct = parentProduct;
    }

    /**
     * Gets the propertyType.
     *
     * @return Returns the propertyType.
     */
    public PropertySpecificationType getPropertyType() {
        return propertyType;
    }

    /**
     * Sets The propertyType.
     *
     * @param propertyType The propertyType to set.
     */
    public void setPropertyType(final PropertySpecificationType propertyType) {
        this.propertyType = propertyType;
    }
}