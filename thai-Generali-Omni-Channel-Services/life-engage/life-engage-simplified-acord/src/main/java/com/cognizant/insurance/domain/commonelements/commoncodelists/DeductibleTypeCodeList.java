/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * A code list comprised of deductible types. 
 * 
 * @author 301350
 * 
 * 
 */
public enum DeductibleTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Flat,
    /**
     * 
     * 
     * 
     * 
     */
    Graduated,
    /**
     * 
     * 
     * 
     * 
     */
    Percent
}
