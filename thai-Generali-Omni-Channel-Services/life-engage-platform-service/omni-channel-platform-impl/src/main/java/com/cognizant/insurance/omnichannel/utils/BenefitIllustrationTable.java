package com.cognizant.insurance.omnichannel.utils;

/**
 * @author 471501
 * 
 *         The Class BenefitIllustrationTable.
 * 
 *         This class is a POJO for Benefit illustration table in illustration PDF.
 */

public class BenefitIllustrationTable {

    /** The policy year. */
    private String policyYear;
    
    /** The Age. */
    private String age;

    /** The Premiums Per year */
    private String annualisedPremium;

    /** The Annual Pension %. */
    private String benefitRate;
    
    /** The Annual Pension Amount. */
    private String benefitAmount;
    
    /** The Tax Amount. */
    private String taxDeducAmount;
    
    /** The Life Coverage - Before the Pension. */
    private String beforeAnnuityAmount;
    
    /** The Life Coverage - Pension Period. */
    private String afterAnnuityAmount;
    
    /** The Total Living Benefit. */
    private String totalLivingBenefit;
    
    /** The Total Taxation Benefit. */
    private String totalTaxationBenefit;
    
    /** The Total Premium Paid */
    private String totalPremiumPaid;
    
    /** The Rate Percent. */
    private String totalRatePercent;
    
    /** The Tax Rate*/
    private String taxRate;
    
    private String benefitTaxAmount;
    
    private String benefitLifeRate;
    
    private String benefitLifeAmount;
    
    private Boolean isGenProLife20;
    
    private Boolean isGenProLife25;
    
    public String getPolicyYear() {
		return policyYear;
	}

	public void setPolicyYear(String policyYear) {
		this.policyYear = policyYear;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getAnnualisedPremium() {
		return annualisedPremium;
	}

	public void setAnnualisedPremium(String annualisedPremium) {
		this.annualisedPremium = annualisedPremium;
	}

	public String getBenefitRate() {
		return benefitRate;
	}

	public void setBenefitRate(String benefitRate) {
		this.benefitRate = benefitRate;
	}

	public String getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(String benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public String getTaxDeducAmount() {
		return taxDeducAmount;
	}

	public void setTaxDeducAmount(String taxDeducAmount) {
		this.taxDeducAmount = taxDeducAmount;
	}

	public String getBeforeAnnuityAmount() {
		return beforeAnnuityAmount;
	}

	public void setBeforeAnnuityAmount(String beforeAnnuityAmount) {
		this.beforeAnnuityAmount = beforeAnnuityAmount;
	}

	public String getAfterAnnuityAmount() {
		return afterAnnuityAmount;
	}

	public void setAfterAnnuityAmount(String afterAnnuityAmount) {
		this.afterAnnuityAmount = afterAnnuityAmount;
	}

	public String getTotalLivingBenefit() {
		return totalLivingBenefit;
	}

	public void setTotalLivingBenefit(String totalLivingBenefit) {
		this.totalLivingBenefit = totalLivingBenefit;
	}

	public String getTotalTaxationBenefit() {
		return totalTaxationBenefit;
	}

	public void setTotalTaxationBenefit(String totalTaxationBenefit) {
		this.totalTaxationBenefit = totalTaxationBenefit;
	}

	public String getTotalPremiumPaid() {
		return totalPremiumPaid;
	}

	public void setTotalPremiumPaid(String totalPremiumPaid) {
		this.totalPremiumPaid = totalPremiumPaid;
	}

	public String getTotalRatePercent() {
		return totalRatePercent;
	}

	public void setTotalRatePercent(String totalRatePercent) {
		this.totalRatePercent = totalRatePercent;
	}

	public String getTaxRate() {
		return taxRate;
	}

	public String getBenefitTaxAmount() {
		return benefitTaxAmount;
	}

	public void setBenefitTaxAmount(String benefitTaxAmount) {
		this.benefitTaxAmount = benefitTaxAmount;
	}

	public String getBenefitLifeRate() {
		return benefitLifeRate;
	}

	public void setBenefitLifeRate(String benefitLifeRate) {
		this.benefitLifeRate = benefitLifeRate;
	}

	public String getBenefitLifeAmount() {
		return benefitLifeAmount;
	}

	public void setBenefitLifeAmount(String benefitLifeAmount) {
		this.benefitLifeAmount = benefitLifeAmount;
	}

	public void setTaxRate(String taxRate) {
		this.taxRate = taxRate;
	}

	public Boolean getIsGenProLife20() {
		return isGenProLife20;
	}

	public void setIsGenProLife20(Boolean isGenProLife20) {
		this.isGenProLife20 = isGenProLife20;
	}

	public Boolean getIsGenProLife25() {
		return isGenProLife25;
	}

	public void setIsGenProLife25(Boolean isGenProLife25) {
		this.isGenProLife25 = isGenProLife25;
	}

 }
