/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Dec 13, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class class TemplateType.
 */

@Entity
@DiscriminatorValue("Template_Type")
public class TemplateType extends Type {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2182594528943276604L;

    /** The product template mappings. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "templateType")
    private Set<ProductTemplateMapping> productTemplateMappings;

    /**
     * The Constructor.
     */
    public TemplateType() {
        super();
    }

    /**
     * Gets the product template mappings.
     * 
     * @return the productTemplateMappings
     */
    public Set<ProductTemplateMapping> getProductTemplateMappings() {
        return productTemplateMappings;
    }

    /**
     * Sets the product template mappings.
     * 
     * @param productTemplateMappings
     *            the productTemplateMappings to set
     */
    public void setProductTemplateMappings(Set<ProductTemplateMapping> productTemplateMappings) {
        this.productTemplateMappings = productTemplateMappings;
    }

    /**
     * The Constructor.
     * 
     * @param id
     *            the id
     */
    public TemplateType(final String id) {
        super(id);
    }
}
