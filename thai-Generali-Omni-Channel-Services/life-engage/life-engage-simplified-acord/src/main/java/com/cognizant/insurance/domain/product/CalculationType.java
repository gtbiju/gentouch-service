/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */


package com.cognizant.insurance.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

/**
 * The Class CalculationType.
 *
 * @author 304007
 */

@Entity
@DiscriminatorValue("Calculation")
public class CalculationType extends Type {
   
   /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5344262090793477749L;

    
    /** The calculations. */ 
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.EAGER, mappedBy="calculationType")    
    private Set<CalculationSpecification> calculations;

    /**
    * The Constructor.
    */
   public CalculationType() {
       super();
   }
   
   /**
    * The Constructor.
    *
    * @param id the id
    */
   public CalculationType(final String id) {
       super(id);
   }

   /**
    * @param calculations the calculations to set
    */
   public void setCalculations(Set<CalculationSpecification> calculations) {
       this.calculations = calculations;
   }

   /**
    * @return the calculations
    */
   public Set<CalculationSpecification> getCalculations() {
       return calculations;
   }
}
