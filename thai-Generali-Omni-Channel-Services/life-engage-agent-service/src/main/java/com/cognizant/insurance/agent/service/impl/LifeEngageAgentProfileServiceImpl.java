/*
 * 
 */
package com.cognizant.insurance.agent.service.impl;

import java.text.ParseException;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.insurance.agent.service.LifeEngageAgentProfileService;
import com.cognizant.insurance.agent.component.LifeEngageAgentComponent;
//import com.cognizant.insurance.component.VendorAgentComponent;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.request.vo.RequestInfo;
import com.cognizant.insurance.agent.service.helper.LifeEngageSyncServiceHelper;

// TODO: Auto-generated Javadoc
/**
 * The Class AgentProfileServiceImpl.
 */
@Service("agentProfileService")
public class LifeEngageAgentProfileServiceImpl implements LifeEngageAgentProfileService {

	/** The Constant REQUEST. */
	private static final String REQUEST = "Request";

	/** The Constant REQUEST_INFO. */
	private static final String REQUEST_INFO = "RequestInfo";

	/** The Constant REQUEST_PAYLOAD. */
	private static final String REQUEST_PAYLOAD = "RequestPayload";

	/** The Constant TRANSACTIONS. */
	private static final String TRANSACTIONS = "Transactions";


	/** The life engage agent component. */
	@Autowired
	private LifeEngageAgentComponent lifeEngageAgentComponent;

	/** The vendor agent component. */
	/*@Autowired
	private VendorAgentComponent vendorAgentComponent;*/
	
	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory
			.getLogger(LifeEngageAgentProfileServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cognizant.insurance.services.AgentProfileService#retrieveAgent(java
	 * .lang.String)
	 */
	@Override
	public String retrieveAgentByCode(String json) throws BusinessException
			{
		JSONObject jsonObject = null;
		String response = null;
		/*String vendorResponse = null;
		String mergedResponse = null;*/
		try {
			jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
				response = lifeEngageAgentComponent.retrieveAgentProfileByCode(requestInfo, jsonTransactionArray);
				/*vendorResponse = vendorAgentComponent
						.retrieveAgentProfileByCode(requestInfo, jsonTransactionArray);
				mergedResponse = mergeResponse(response, vendorResponse);*/

		} catch (BusinessException e) {
			throw e;

		} catch (ParseException e) {
        	LOGGER.error("PARSE ERROR : Parse exception : ## ERR ##" +e.getMessage() , e);
		}

		/*return mergedResponse;*/
		return response;
	}



	/**
	 * Merge response.
	 *
	 * @param response the response
	 * @param vendorResponse the vendor response
	 * @return the string
	 * @throws ParseException the parse exception
	 */
	/*protected String mergeResponse(String response, String vendorResponse)
			throws ParseException {
		String mergedResponse = null;
		if (null != response && null != vendorResponse) {
			JSONObject respObj = new JSONObject(response);
			JSONObject vendorResp = new JSONObject(vendorResponse);
			
			JSONObject respInfoObj = respObj.getJSONObject(RESPONSE_INFO);
			JSONObject vendorRespInfoObj = respObj.getJSONObject(RESPONSE_INFO);
			JSONObject mergedRespInfo = new JSONObject(); 
			mergeJSONObjects(respInfoObj, vendorRespInfoObj, mergedRespInfo);
			
			
			final JSONObject jsonRespPayloadObj = respInfoObj
					.getJSONObject(RESPONSE_PAYLOAD);
			final JSONArray jsonTransactionArray = respInfoObj
					.getJSONArray(TRANSACTIONS);
			
			
			final JSONObject vendRespPayloadObj = respInfoObj
					.getJSONObject(RESPONSE_PAYLOAD);
			final JSONArray vendTransactionArray = respInfoObj
					.getJSONArray(TRANSACTIONS);
			
			JSONObject mergedJSONObject = new JSONObject();
			mergeJSONObjects(respObj, vendorResp, mergedJSONObject);
			mergedResponse = mergedJSONObject.toString();
		}else if(null!=response && vendorResponse == null){
			mergedResponse = response;
		}else if(null!=vendorResponse && response == null){
			mergedResponse = vendorResponse;
		}

		return mergedResponse;
	}*/

	/**
	 * Merge json objects.
	 *
	 * @param obj1 the obj1
	 * @param obj2 the obj2
	 * @param mergedJSON the merged json
	 * @return the jSON object
	 */
	/*private JSONObject mergeJSONObjects(JSONObject obj1,
		JSONObject obj2, JSONObject mergedJSON) {
		JSONObject[] objs = new JSONObject[] { obj1, obj2 };
		for (JSONObject obj : objs) {
			Iterator it = obj.keys();
			while (it.hasNext()) {
				String key = (String) it.next();
				mergedJSON.put(key, obj.get(key));
			}
		}
		return mergedJSON;

	}*/

}
