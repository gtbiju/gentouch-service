/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Jul 30, 2015
 */
package com.cognizant.insurance.searchcriteria;

import java.io.Serializable;
import java.util.List;

/**
 * The Class class SearchCriteriaResponse.
 */
public class SearchCriteriaResponse implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -127477911741190446L;

   
    /** The sub modes. */
    private List<SearchCountResult> modeResults;


    /**
     * Gets the mode results.
     *
     * @return the mode results
     */
    public List<SearchCountResult> getModeResults() {
        return modeResults;
    }


    public void setModeResults(List<SearchCountResult> modeResults) {
        this.modeResults = modeResults;
    }

  
}
