/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Validity Status.
 * 
 * @author 301350
 * 
 * 
 */
public enum ValidityStatusCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Valid,
    /**
     * 
     * 
     * 
     * 
     */
    Notvalid
}
