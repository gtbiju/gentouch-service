package com.cognizant.insurance.omnichannel.jpa.component.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.agreement.AddressTranslation;
import com.cognizant.insurance.omnichannel.dao.LifeAsiaDao;
import com.cognizant.insurance.omnichannel.dao.impl.LifeAsiaDaoImpl;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaMetaData;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaRequest;
import com.cognizant.insurance.omnichannel.domain.UnderWriterMemoDetails;
import com.cognizant.insurance.omnichannel.jpa.component.LifeAsiaRequestComponent;
import com.cognizant.insurance.omnichannel.lifeasia.searchcriteria.LifeAsiaSearchCriteria;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;

@Component
public class LifeAsiaRequestComponentImpl implements LifeAsiaRequestComponent {

	@Override
	public Response<LifeAsiaRequest> save(Request<LifeAsiaRequest> request) {
		LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
		lifeAsiaDao.save(request);
		Response<LifeAsiaRequest> response = new ResponseImpl<LifeAsiaRequest>();
		response.setType(request.getType());
		return response;
	}
	@Override
	public Response<LifeAsiaRequest> merge(Request<LifeAsiaRequest> request) {
		LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
		lifeAsiaDao.merge(request);
		Response<LifeAsiaRequest> response = new ResponseImpl<LifeAsiaRequest>();
		response.setType(request.getType());
		return response;
	}

	@Override
	public Response<LifeAsiaRequest> getLifeAsiaRequest(Request<String> bpmRequest) {
		LifeAsiaDao lifeAsiaDao=new LifeAsiaDaoImpl();
		return lifeAsiaDao.getLifeAsiaRequest(bpmRequest);
	}

	@Override
	public Response<List<LifeAsiaRequest>> getLifeAsiaRequests(
			Request<LifeAsiaSearchCriteria> criteria) {
		LifeAsiaDao lifeAsiaDao=new LifeAsiaDaoImpl();
		return lifeAsiaDao.getLifeAsiaRequests(criteria);
	}

	@Override
	public Response<List<Number>> getLifeAsiaRequestsIds(
			Request<LifeAsiaSearchCriteria> criteria) {
		LifeAsiaDao lifeAsiaDao=new LifeAsiaDaoImpl();
		return lifeAsiaDao.getLifeAsiaRequestsIds(criteria);
	}
	@Override
	public Response<LifeAsiaMetaData> getLifeAsiaMetaData(Request<String> criteria) {
		LifeAsiaDao lifeAsiaDao=new LifeAsiaDaoImpl();
		return lifeAsiaDao.getLifeAsiaMetaData(criteria);
	}
	@Override
	public Response<LifeAsiaMetaData> saveMetaData(Request<LifeAsiaMetaData> request) {
		LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
		lifeAsiaDao.save(request);
		Response<LifeAsiaMetaData> response = new ResponseImpl<LifeAsiaMetaData>();
		response.setType(request.getType());
		return response;
	}
	@Override
	public Response<String> getEappId(Request<LifeAsiaSearchCriteria> request) {
		LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
		Response<String> response= lifeAsiaDao.getEappId(request);
		return response;
	}

    @Override
    public Response<List<String>> getFailedSPAJs(Request<LifeAsiaSearchCriteria> request,int timeRange) {
        LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
        Response<List<String>> response = lifeAsiaDao.getFailedSPAJs(request,timeRange);
        return response;
    }
    @Override
    public Response<List<String>> getFailedSPAJsFromOmni(Request<LifeAsiaSearchCriteria> request) {
        LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
        Response<List<String>> response = lifeAsiaDao.getFailedSPAJsFromOmni(request);
        return response;
    }
	@Override
	public Response<List<String>> getProposalsSubmitted(Request<String> agentRequest,Request<List<String>> statusRequest) {
		LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
		Response<List<String>> response = lifeAsiaDao.getProposalsSubmitted(agentRequest, statusRequest);
        return response;
	}
	
	@Override
	public Response<List<AppianMemoDetails>> retrieveMemoDetails(Request<Transactions> request,String type) {
		LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
		return lifeAsiaDao.getMemoDetails(request,type);
	}
	
	@Override
	public void updateMemoDetails(Request<AppianMemoDetails> request, String type) {
		LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
		if (type.equalsIgnoreCase(Constants.UPDATE)) {
			lifeAsiaDao.merge(request);
		} else if (type.equalsIgnoreCase(Constants.SAVE)) {
			lifeAsiaDao.save(request);
		}
	}
	
	@Override
	public UnderWriterMemoDetails getMemoDescription(Request<String> request) {
		LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
		return lifeAsiaDao.getMemoDescription(request);
	}
	
    /*@Override
    public Map<String, UnderWriterMemoDetails> getUnderWriterMemoDetails(Request<Set<String>> request) {
        LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
        return lifeAsiaDao.getUnderWriterMemoDetails(request);
    }*/
    @Override
    public Response<LifeAsiaRequest> getLifeAsiaRequestByCriteria(Request<LifeAsiaSearchCriteria> lifeAsiaSearchCriteria) {
        LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
        return lifeAsiaDao.getLifeAsiaRequestByCriteria(lifeAsiaSearchCriteria);
    }
    
	
	@Override
	public void updateAppainStatus(Request<LifeAsiaRequest> request) {
		LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
		lifeAsiaDao.merge(request);
	}

    @Override
    public Response<List<AddressTranslation>> getTranslatedAddress(Request<AddressTranslation> request) {
        LifeAsiaDao lifeAsiaDao = new LifeAsiaDaoImpl();
        return lifeAsiaDao.getTranslatedAddress(request);
    }
	
}
