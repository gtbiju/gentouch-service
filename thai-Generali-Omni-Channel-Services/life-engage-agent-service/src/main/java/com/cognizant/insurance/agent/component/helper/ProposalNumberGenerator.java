/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Dec 5, 2013
 */
package com.cognizant.insurance.agent.component.helper;

import java.util.Random;

import com.cognizant.insurance.agent.component.TrackingIdGenerator;

/**
 * The Class class ProposalNumberGenerator.
 * 
 * @author 300797
 */
public final class ProposalNumberGenerator implements TrackingIdGenerator{

    /** The Constant generator. */
    private static final Random GENERATOR = new Random();

    /** The million. */
    private static final int MILLION = 100000;

    /** The random. */
    private static final int RANDOM = 900000;

    /**
     * Instantiates a new proposal number generator.
     */
    private ProposalNumberGenerator() {
    }

    /** The proposal number generator obj. */
    private static ProposalNumberGenerator proposalNumberGeneratorObj = new ProposalNumberGenerator();

    /**
     * Gets the single instance of ProposalNumberGenerator.
     * 
     * @return single instance of ProposalNumberGenerator
     */
    public static ProposalNumberGenerator getInstance() {
        return proposalNumberGeneratorObj;
    }

    /**
     * Generate Proposal Number.
     * 
     * @return the Proposal Number
     */
    public int generateProposalNumber() {
        return MILLION + GENERATOR.nextInt(RANDOM);
    }

	@Override
	public Long generate() {
		return (long) (MILLION + GENERATOR.nextInt(RANDOM));
	}

	@Override
	public Long generate(String context) {
		return generate();
	}
}
