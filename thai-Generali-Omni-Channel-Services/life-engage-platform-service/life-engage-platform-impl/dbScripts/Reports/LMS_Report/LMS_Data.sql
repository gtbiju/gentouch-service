
select pr.id as ID ,
pr.identifier as LeadID,
CONVERT(VARCHAR(10),pr.creationDateTime,103) as CreatedDate,
pr.agentId as AgentCode,
ISNULL(pr.branchId,'') as BranchCode,
CASE WHEN pr.branchId IS NOT NULL and pr.branchId<>''
THEN 'Assigned By Branch'
ELSE 'Self Created'
END as TypeOfLead,
CASE WHEN pr.statusCode=22 THEN 'Non Contactable'
	 WHEN pr.statusCode=23 THEN 'Meeting Fixed'
	 WHEN pr.statusCode=24 THEN 'Successful Sale'
	 WHEN pr.statusCode=25 THEN 'To Be Called'
	 WHEN pr.statusCode=26 THEN 'Contacted And Not Interested'
	 WHEN pr.statusCode=14 THEN 'Interested'
	 WHEN pr.statusCode=27 THEN 'App Submitted'
	 WHEN pr.statusCode=28 THEN 'Pending Documents'
	 WHEN pr.statusCode=29 THEN 'Others'
	 WHEN pr.statusCode=18 THEN 'Suspended'
	 WHEN pr.statusCode=20 THEN 'New'
END as Status,
tk.key15 as StatusHistory

from PartyRoleInRelationship as pr with (nolock)
INNER JOIN TransactionKeys as tk with (nolock) ON (pr.transactionKeys_id=tk.id) 
where pr.identifier is not null
