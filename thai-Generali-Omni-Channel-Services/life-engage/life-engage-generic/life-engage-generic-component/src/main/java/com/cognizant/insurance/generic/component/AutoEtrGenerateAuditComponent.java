package com.cognizant.insurance.generic.component;

import java.util.List;

import com.cognizant.insurance.domain.AutoEtrGenerateAudit;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;

public interface AutoEtrGenerateAuditComponent {
	 <T> Response<Object> save(Request<T> request);

	List<AutoEtrGenerateAudit> retrieveAutoEtrGenerateAudit(String eappNo);

	void updateAutoETROnSave(Request<AutoEtrGenerateAudit> request);

	 
}
