package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;
import java.util.List;

public class TeamDataVo implements Serializable {

    /**  The serialVersionUID **/
    private static final long serialVersionUID = 1L;

    /** The teamName **/
    private String teamName;

    /** The agentDetails **/
    private List<AgentDataVo> agentDetails;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<AgentDataVo> getAgentDetails() {
        return agentDetails;
    }

    public void setAgentDetails(List<AgentDataVo> agentDetails) {
        this.agentDetails = agentDetails;
    }

}
