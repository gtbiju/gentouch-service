package com.cognizant.insurance.omnichannel.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ManagerialLevelUserDetails {
	
	private String userType;
	
	private String userId;
	
	private String userName;
	
	private String mobileNumber;
	
    public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	private Date startDate;
	
	private Date currentDate;
	
	private Date currentMonth;
	
	 /** The selectedAgentIds. */
    private ArrayList<SelectedAgentData> selectedAgentIds;
    
    /** The selectedBranchIds. */
    private ArrayList<String> selectedBranchIds;	
	
	private List<ManagerialLevelUserDetails> userDetails;
	
	private int new_YTD;
	private int closed_YTD;
	private int followUp_YTD;
	
	private int new_MTD;
	private int closed_MTD;
	private int followUp_MTD;
	
	private BigDecimal conversionRate_YTD = new BigDecimal(0);
	private BigDecimal conversionRate_MTD = new BigDecimal(0);
	
	private BigDecimal APE_YTD = new BigDecimal(0);
	private BigDecimal APE_MTD = new BigDecimal(0);
	
	private int activeAgents_MTD;
	private int activeAgents_YTD;
	
	private int totalAgents_YTD;
	private int totalAgents_MTD;
	
	private int totalLeads_YTD;
	private int totalLeads_MTD;
	
	private BigDecimal activityRatio_YTD = new BigDecimal(0);
	private BigDecimal activityRatio_MTD = new BigDecimal(0);
	
	private BigDecimal productivity_YTD = new BigDecimal(0);
	private BigDecimal productivity_MTD = new BigDecimal(0);
	
	private BigDecimal caseSize_YTD = new BigDecimal(0);
	private BigDecimal caseSize_MTD = new BigDecimal(0);
	
	public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	public Date getCurrentMonth() {
		return currentMonth;
	}
	public void setCurrentMonth(Date currentMonth) {
		this.currentMonth = currentMonth;
	}
	public ArrayList<SelectedAgentData> getSelectedAgentIds() {
		return selectedAgentIds;
	}
	public void setSelectedAgentIds(ArrayList<SelectedAgentData> selectedAgentIds) {
		this.selectedAgentIds = selectedAgentIds;
	}
	public ArrayList<String> getSelectedBranchIds() {
		return selectedBranchIds;
	}
	public void setSelectedBranchIds(ArrayList<String> selectedBranchIds) {
		this.selectedBranchIds = selectedBranchIds;
	}
	
	public int getNew_YTD() {
		return new_YTD;
	}
	public void setNew_YTD(int new_YTD) {
		this.new_YTD = new_YTD;
	}
	public int getClosed_YTD() {
		return closed_YTD;
	}
	public void setClosed_YTD(int closed_YTD) {
		this.closed_YTD = closed_YTD;
	}
	public int getFollowUp_YTD() {
		return followUp_YTD;
	}
	public void setFollowUp_YTD(int followUp_YTD) {
		this.followUp_YTD = followUp_YTD;
	}
	public int getNew_MTD() {
		return new_MTD;
	}
	public void setNew_MTD(int new_MTD) {
		this.new_MTD = new_MTD;
	}
	public int getClosed_MTD() {
		return closed_MTD;
	}
	public void setClosed_MTD(int closed_MTD) {
		this.closed_MTD = closed_MTD;
	}
	public int getFollowUp_MTD() {
		return followUp_MTD;
	}
	public void setFollowUp_MTD(int followUp_MTD) {
		this.followUp_MTD = followUp_MTD;
	}
	public BigDecimal getConversionRate_YTD() {
		return conversionRate_YTD;
	}
	public void setConversionRate_YTD(BigDecimal conversionRate_YTD) {
		this.conversionRate_YTD = conversionRate_YTD;
	}
	public BigDecimal getConversionRate_MTD() {
		return conversionRate_MTD;
	}
	public void setConversionRate_MTD(BigDecimal conversionRate_MTD) {
		this.conversionRate_MTD = conversionRate_MTD;
	}

	public int getActiveAgents_MTD() {
		return activeAgents_MTD;
	}
	public void setActiveAgents_MTD(int activeAgents_MTD) {
		this.activeAgents_MTD = activeAgents_MTD;
	}
	public int getActiveAgents_YTD() {
		return activeAgents_YTD;
	}
	public void setActiveAgents_YTD(int activeAgents_YTD) {
		this.activeAgents_YTD = activeAgents_YTD;
	}
	public int getTotalAgents_YTD() {
		return totalAgents_YTD;
	}
	public void setTotalAgents_YTD(int totalAgents_YTD) {
		this.totalAgents_YTD = totalAgents_YTD;
	}
	public int getTotalAgents_MTD() {
		return totalAgents_MTD;
	}
	public void setTotalAgents_MTD(int totalAgents_MTD) {
		this.totalAgents_MTD = totalAgents_MTD;
	}
	public BigDecimal getActivityRatio_YTD() {
		return activityRatio_YTD;
	}
	public void setActivityRatio_YTD(BigDecimal activityRatio_YTD) {
		this.activityRatio_YTD = activityRatio_YTD;
	}
	public BigDecimal getActivityRatio_MTD() {
		return activityRatio_MTD;
	}
	public void setActivityRatio_MTD(BigDecimal activityRatio_MTD) {
		this.activityRatio_MTD = activityRatio_MTD;
	}
	
	public List<ManagerialLevelUserDetails> getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(List<ManagerialLevelUserDetails> userDetails) {
		this.userDetails = userDetails;
	}
	public BigDecimal getAPE_YTD() {
		return APE_YTD;
	}
	public void setAPE_YTD(BigDecimal aPE_YTD) {
		APE_YTD = aPE_YTD;
	}
	public BigDecimal getAPE_MTD() {
		return APE_MTD;
	}
	public void setAPE_MTD(BigDecimal aPE_MTD) {
		APE_MTD = aPE_MTD;
	}
	public BigDecimal getProductivity_YTD() {
		return productivity_YTD;
	}
	public void setProductivity_YTD(BigDecimal productivity_YTD) {
		this.productivity_YTD = productivity_YTD;
	}
	public BigDecimal getProductivity_MTD() {
		return productivity_MTD;
	}
	public void setProductivity_MTD(BigDecimal productivity_MTD) {
		this.productivity_MTD = productivity_MTD;
	}
	public BigDecimal getCaseSize_YTD() {
		return caseSize_YTD;
	}
	public void setCaseSize_YTD(BigDecimal caseSize_YTD) {
		this.caseSize_YTD = caseSize_YTD;
	}
	public BigDecimal getCaseSize_MTD() {
		return caseSize_MTD;
	}
	public void setCaseSize_MTD(BigDecimal caseSize_MTD) {
		this.caseSize_MTD = caseSize_MTD;
	}
	public int getTotalLeads_YTD() {
		return totalLeads_YTD;
	}
	public void setTotalLeads_YTD(int totalLeads_YTD) {
		this.totalLeads_YTD = totalLeads_YTD;
	}
	public int getTotalLeads_MTD() {
		return totalLeads_MTD;
	}
	public void setTotalLeads_MTD(int totalLeads_MTD) {
		this.totalLeads_MTD = totalLeads_MTD;
	}

	
	
	
}
