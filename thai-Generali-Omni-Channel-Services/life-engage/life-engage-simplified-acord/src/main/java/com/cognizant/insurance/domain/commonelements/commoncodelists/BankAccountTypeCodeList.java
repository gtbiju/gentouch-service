/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Bank Account Type.
 * 
 * @author 301350
 * 
 * 
 */
public enum BankAccountTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    SavingsAccount,
    /**
     * 
     * 
     * 
     * 
     */
    CheckingAccount,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * 
     * 
     * 
     * 
     */
    CreditCard,
    /**
     * 
     * 
     * 
     * 
     */
    DebitCard,
    /**
     * An account at a brokerage by which a customer may place money in order to
     * make trades.
     * 
     * 
     * 
     */
    BrokerageAccount,
    /**
     * Bank account type of CD. This is the actual certificate of deposit
     * account.
     * 
     * 
     * 
     */
    CDCertificateofDeposit
}
