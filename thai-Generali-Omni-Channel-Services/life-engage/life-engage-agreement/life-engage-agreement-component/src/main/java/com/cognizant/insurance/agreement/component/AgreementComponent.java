/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */
package com.cognizant.insurance.agreement.component;

import java.util.List;
import java.util.Set;

import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.finance.FinancialProvision;
import com.cognizant.insurance.domain.finance.agreementloan.AgreementLoan;
import com.cognizant.insurance.domain.finance.agreementloan.AgreementWithdrawal;
import com.cognizant.insurance.domain.goalandneed.Agreement_GoalAndNeed;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

/**
 * The Interface interface AgreementComponent.
 */
public interface AgreementComponent {

    /**
     * Gets the agreement loans.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the agreement loans
     */
    Response<Set<AgreementLoan>> getAgreementLoans(Request<Agreement> agreementRequest);

    /**
     * Gets the agreements. This method select the Agreements for a particular agent(Agreement.agentId), for a
     * particular context(agreementRequest.ContextType), for given status(statusRequest) and its creation date is
     * greater than the given date(Agreement.lastSyncDate)
     * 
     * @param agreementRequest
     *            the agreement request
     * @param statusRequest
     *            the status request
     * @param limitRequest
     *            the limit request
     * @return the agreements
     */
    Response<List<Agreement>> getAgreements(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest, Request<Integer> limitRequest);

    /**
     * Gets the agreement withdrawals.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the agreement withdrawals
     */
    Response<Set<AgreementWithdrawal>> getAgreementWithdrawals(Request<Agreement> agreementRequest);

    /**
     * Gets the insurance agreement.This method select the Agreements for a particular identifier, for a particular
     * context(agreementRequest.ContextType), for given status(statusRequest) and its previousPolicyIndicator should be
     * false
     * 
     * @param agreementRequest
     *            the agreement request
     * @param statusRequest
     *            the status request
     * @return the insurance agreement
     */
    Response<Agreement> getInsuranceAgreement(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Gets the previous agreements for a particular PartyRoleInAgreement.
     * 
     * @param partyRoleInAgreementRequest
     *            the party role in agreement request
     * @return the previous agreements
     */
    Response<Set<InsuranceAgreement>> getPreviousAgreements(Request<PartyRoleInAgreement> partyRoleInAgreementRequest);

    /**
     * Retrieve agreement for identifier.
     * 
     * @param agreementRequest
     *            the agreement request
     * @param statusRequest
     *            the status request
     * @return the response
     */
    Response<Agreement> retrieveAgreementForIdentifier(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Save agreement.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<Agreement> saveAgreement(Request<Agreement> agreementRequest);

    /**
     * Save financial provision.
     * 
     * @param finanProvReq
     *            the financial provision request
     * @return the response
     */
    Response<Long> saveFinancialProvision(final Request<FinancialProvision> finanProvReq);

    /**
     * Save agreement.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<Agreement> updateAgreement(Request<Agreement> agreementRequest);

    /**
     * Merge agreement.
     * 
     * @param agreementRequest
     *            the agreement request
     */
    void mergeAgreement(Request<Agreement> agreementRequest);

    /**
     * Gets the agreement document details.
     * 
     * @param agreementRequest
     *            the agreement request is of type Agreement and which contains agreement
     *            identifier(agreement.identifier) and document unique name(agreement.relatedDocument[0].name)
     * @param statusRequest
     *            the status request
     * @return the agreement document details
     */
    Response<AgreementDocument> getAgreementDocumentDetails(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Gets the status of proposal.
     * 
     * @param agreementRequest
     *            the agreement request is of type Agreement and which contains agreement
     *            identifier(agreement.identifier) and document unique name(agreement.relatedDocument[0].name)
     * @param statusRequest
     *            the status request
     * @return the agreement details
     */
    Response<Agreement> getStatus(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * To save the FNA ID on illustration save.
     * 
     * @param agreementgoalAndNeed
     *            the agreementgoal and need
     * @return the response
     */
    Response<Agreement_GoalAndNeed> saveFNAId(final Request<Agreement_GoalAndNeed> agreementgoalAndNeed);

    /**
     * To retrieve FNA ID.
     * 
     * @param request
     *            the request
     * @param illustrationID
     *            the illustration id
     * @return the string
     */
    String retrieveFNAId(final Request<Agreement_GoalAndNeed> request, final String illustrationID);

    /**
     * Gets the agreement count.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the agreement count
     */
    Response<SearchCountResult> getAgreementCount(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Gets the agreement by filter.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the agreement by filter
     */
    Response<List<Agreement>> getAgreementByFilter(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Update agreement status for lead.
     * 
     * @param leadTransTrackingIdRequest
     *            the lead trans tracking id request
     * @param statusRequest
     *            the status request
     */
    void updateAgreementStatusForLead(final Request<String> leadTransTrackingIdRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    
    /**
     * Retrieve agreement for trans tracking id.
     *
     * @param agreementRequest the agreement request
     * @param statusRequest the status request
     * @return the response
     */
    Response<Agreement> retrieveAgreementForTransTrackingId(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    

    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
    Response<Requirement> getRequirementDocumentDetails(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
    void updateInsuranceAgreementWithRequirementDocUpdate(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
    Response<Document> getRequirementDocFile(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
    Response<AgreementDocument> getRequirementDocFileList(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    
    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
    void updateInsuranceAgreementWithRequirement(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    

    Response<Agreement> getInsuranceAgreementPending(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    /**
     * Retrieve latest agreement by trans tracking id,tracking id, status code.
     *
     * @param agreementRequest the agreement request
     * @param statusRequest the status request
     * @return the response
     */
    Response<Agreement> retrieveLatestAgreement(Request<Agreement> agreementRequest);
    

}
