/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Nov 13, 2012
 */
package com.cognizant.insurance.agent.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.cognizant.insurance.agent.request.Request;

/**
 * The Interface interface Dao.
 */
public interface Dao {

    
    /**
     * Save.
     *
     * @param <T> the generic type
     * @param request the request
     */
    <T> void save(Request<T> request);

    /**
     * Save.
     *
     * @param <T> the generic type
     * @param type the type
     * @param entityManager the entity manager
     */
    <T> void save(final T type, final EntityManager entityManager);

    /**
     * Merge.
     *
     * @param <T> the generic type
     * @param request the request
     */
    <T> void merge(final Request<T> request);

    /**
     * Merge which takes Type and EntityManager
     *
     * @param <T>
     * @param type
     * @param entityManager
     */
    <T> void merge(final T type, final EntityManager entityManager);
    
    /**
     * Find class.
     *
     * @param <T> the generic type
     * @param request the request
     * @param primaryKey the primary key
     * @return the object
     */
    <T> Object findById(Request<T> request, Object primaryKey);

    /**
     * Removes the.
     * 
     * @param <T>
     *            the generic type
     * @param request
     *            the request
     */
    <T> void remove(Request<T> request);

    /**
     * Find by named query.
     * 
     * @param <T>
     *            the generic type
     * @param request
     *            the request
     * @param name
     *            the name
     * @param params
     *            the params
     * @return the list
     */
    <T> List<T> findByNamedQuery(Request<T> request, final String name, Object... params);

    /**
     * Find by query.
     * 
     * @param <T>
     *            the generic type
     * @param request
     *            the request
     * @param queryString
     *            the query string
     * @param params
     *            the params
     * @return the list
     */
    <T> List<T> findByQuery(Request<T> request, final String queryString, Object... params);

    /**
     * Find class by id.
     *
     * @param <T> the generic type
     * @param <R> the generic type
     * @param request the request
     * @param primaryKey the primary key
     * @param clazz the clazz
     * @return the t
     */
    <T, R>T findClassById(Request<R> request, Object primaryKey, Class<T> clazz);

    /**
     * Execute update.
     *
     * @param <T> the generic type
     * @param request the request
     * @param queryString the query string
     * @param params the params
     * @return the int
     */
     <T>int executeUpdate(Request<T> request, String queryString, Object... params);
     /**
      * Find by query.
      * 
      * @param <T>
      *            the generic request type
      *            
      * @param <R>
      *            the generic response type
      * @param request
      *            the request
      * @param queryString
      *            the query string
      * @param params
      *            the params
      * @return the list
      */
     
     <T, R> List<R> findByQueryAndReturnDiffType(Request<T> request, final String queryString, Object... params);
}
