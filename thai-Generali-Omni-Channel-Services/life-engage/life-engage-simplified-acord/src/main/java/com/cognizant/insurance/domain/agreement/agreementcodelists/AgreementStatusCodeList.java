/**
 * 
 */
package com.cognizant.insurance.domain.agreement.agreementcodelists;

/**
 * This code list accounts for the various life cycle statuses of agreements.
 * 
 * @author 301350
 * 
 * 
 */
public enum AgreementStatusCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Final,
    /**
     * 
     * 
     * 
     * 
     */
    Inforce,
    /**
     * 
     * 
     * 
     * 
     */
    InForceChangePending,
    /**
     * 
     * 
     * 
     * 
     */
    Initial,
    /**
     * 
     * 
     * 
     * 
     */
    Suspended,
    /**
     * 
     * 
     * 
     * 
     */
    Terminated,
    /**
     * 
     * 
     * 
     * 
     */
    UnderNegotiation,
    /**
     * 
     * 
     * 
     * 
     */
    Cancelled,
    /**
     * 
     * 
     * 
     * 
     */
    Proposed,
    /**
     * 
     * 
     * 
     * 
     */
    NotMentioned,
    
    /**
     * Added for Generali
     */
    Draft,
    /**
     * 
     */
    Completed,
    /**
     * 
     */
    Confirmed,
    /**
     * 
     */
    ReportSent,
    /**
     * Added for Generali eApp
     */
    New,
    /**
     * 
     */
    Submitted,
    
    /**
     * 
     */
    Lapse,
    /**
     * 
     */
    Surrender,
    /**
     * 
     * 
     */
    InProgress,
    /**
     * 
     */
    Review,
    /**
     * 
     */
    PaymentDone,
    /**
     * 
     */
    PendingSubmission,
    
    /**
     * 
     */
    FNAReportsent
}
