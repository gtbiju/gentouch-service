/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 356551
 * @version       : 0.1, Apr 11, 2018
 */
package com.cognizant.insurance.omnichannel.service;

import com.cognizant.insurance.services.LifeEngageSyncService;

public interface GeneraliSyncService extends LifeEngageSyncService {
    
}
