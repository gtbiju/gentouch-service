/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolerelationshipsubtypes.FamilyRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.FamilyRelationNatureCodeList;

/**
 * This concept represents a family member (e.g. father, mother, son, daughter,
 * aunt, etc.)
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public abstract class FamilyMember extends PartyRoleInRelationship {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8350212811601806654L;

    /**
     * A code indicating the nature of a family member in relation to another
     * family member.
     * 
     * 
     * 
     */
	@Enumerated
    private FamilyRelationNatureCodeList relationNatureCode;

    /**
     * This relationship links related family members to one another.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "FAMILYMEMBER_FAMILYRELATIONSHIP",
            joinColumns = { @JoinColumn(name = "FAMILYMEMBER_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "FAMILYRELATIONSHIP_ID", referencedColumnName = "Id") })
    private FamilyRelationship relationship;

    /**
     * Gets the relationNatureCode.
     * 
     * @return Returns the relationNatureCode.
     */
    public FamilyRelationNatureCodeList getRelationNatureCode() {
        return relationNatureCode;
    }

    /**
     * Gets the relationship.
     * 
     * @return Returns the relationship.
     */
    public FamilyRelationship getRelationship() {
        return relationship;
    }

    /**
     * Sets The relationNatureCode.
     * 
     * @param relationNatureCode
     *            The relationNatureCode to set.
     */
    public void setRelationNatureCode(
            final FamilyRelationNatureCodeList relationNatureCode) {
        this.relationNatureCode = relationNatureCode;
    }

    /**
     * Sets The relationship.
     * 
     * @param relationship
     *            The relationship to set.
     */
    public void setRelationship(final FamilyRelationship relationship) {
        this.relationship = relationship;
    }

}
