package com.cognizant.insurance.agent.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.core.exception.SystemException;


public abstract class ServiceTemplate {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(ServiceTemplate.class);

    /**
     * Execute.
     * 
     * @param json
     *            the json
     * @return the string
     */
    public final String execute(final String json) {

        String result = null;
        
        try {
            result = doExecute(json);
        } catch (BusinessException e) {
        	int randomNo1 = (int )(Math.random() * 5000 + 1);
        	LOGGER.error("Service Template : Business exception : ## ERR" + randomNo1 +" ##" +e.getMessage() , e);
        	result = "Service execution failed ( Refer logs : ERR"  + randomNo1 + " )";
        } catch (SystemException e) {
        	int randomNo2 = (int )(Math.random() * 5000 + 1);
        	LOGGER.error("Service Template : System exception :## ERR" + randomNo2 +" ##" +e.getMessage() , e);
        	result = "Service execution failed ( Refer logs : ERR"  + randomNo2 + " )";
        }

        return result;
    }

    /**
     * Do execute.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    protected abstract String doExecute(String json) throws BusinessException;
}
