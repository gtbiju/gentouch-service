/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 180790
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Auto-generated Javadoc
/**
 * The Class ProductCollaterals.
 * 
 * 
 */

@Entity
@Table(name = "CORE_PROD_COLLATERALS")
public class ProductCollaterals {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4668530434397933338L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    /** The product Id *. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "PROD_ID")
    private ProductSpecification productId;

    /** The file name *. */
    @Column(name = "FILE_NAME")
    private String fileName;

    /** The document type Id *. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "DOCUMENT_TYPE_ID")
    private DocumentType documentType;

    /** The file type Id *. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "FILE_TYPE_ID")
    private FileType fileType;

    /** The withdrawal date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    /** The withdrawal date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATED_DATE")
    private Date updatedDate;

    /** The created user *. */
    @Column(name = "CREATED_USER")
    private String createdUser;

    /** The updated user *. */
    @Column(name = "UPDATED_USER")
    private String updatedUser;

    /** The language *. */
    @Column(name = "LANGUAGE")
    private String language;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public final Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public final void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the product id.
     * 
     * @return the productId
     */
    public ProductSpecification getProductId() {
        return productId;
    }

    /**
     * Sets the product id.
     * 
     * @param productId
     *            the productId to set
     */
    public void setProductId(ProductSpecification productId) {
        this.productId = productId;
    }

    /**
     * Gets the file name.
     * 
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the file name.
     * 
     * @param fileName
     *            the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets the document type.
     * 
     * @return the document type
     */
    public DocumentType getDocumentType() {
        return documentType;
    }

    /**
     * Sets the document type.
     * 
     * @param documentType
     *            the document type to set.
     */
    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    /**
     * Gets the file type.
     * 
     * @return the file type
     */
    public FileType getFileType() {
        return fileType;
    }

    /**
     * Sets the file type.
     * 
     * @param fileType
     *            the file type to set.
     */
    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    /**
     * Gets the created date.
     * 
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the created date.
     * 
     * @param createdDate
     *            the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Gets the updated date.
     * 
     * @return the updatedDate
     */
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Sets the updated date.
     * 
     * @param updatedDate
     *            the updatedDate to set
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * Gets the created user.
     * 
     * @return the createdUser
     */
    public String getCreatedUser() {
        return createdUser;
    }

    /**
     * Sets the created user.
     * 
     * @param createdUser
     *            the createdUser to set
     */
    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    /**
     * Gets the updated user.
     * 
     * @return the updatedUser
     */
    public String getUpdatedUser() {
        return updatedUser;
    }

    /**
     * Sets the updated user.
     * 
     * @param updatedUser
     *            the updatedUser to set
     */
    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    /**
     * Gets the language.
     * 
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the language.
     * 
     * @param language
     *            the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Gets the serialversionuid.
     * 
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
