/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.ContactStatusCodeList;

/**
 * An identification, or partial identification, defining the method and
 * destination of a communication contact with a party role or party. The
 * sub-types relate to specific communication media: postal address, telephone
 * number, and electronic address. General locations (for example, France, The
 * Ardennes, Washington) are excluded and are represented by place.
 * 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class ContactPoint extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7717743284349572257L;

    /**
     * The time period from which the contact point may be used.
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.PERSIST)
    private TimePeriod availablePeriod;

    /**
     * Indicates the status of the contact point.
     * 
     * 
     * 
     */
    @Enumerated
    private ContactStatusCodeList statusCode;

    /**
     * Gets the availablePeriod.
     * 
     * @return Returns the availablePeriod.
     */
    public TimePeriod getAvailablePeriod() {
        return availablePeriod;
    }

    /**
     * Gets the statusCode.
     * 
     * @return Returns the statusCode.
     */
    public ContactStatusCodeList getStatusCode() {
        return statusCode;
    }

    /**
     * Sets The availablePeriod.
     * 
     * @param availablePeriod
     *            The availablePeriod to set.
     */
    public void setAvailablePeriod(final TimePeriod availablePeriod) {
        this.availablePeriod = availablePeriod;
    }

    /**
     * Sets The statusCode.
     * 
     * @param statusCode
     *            The statusCode to set.
     */
    public void setStatusCode(final ContactStatusCodeList statusCode) {
        this.statusCode = statusCode;
    }

}
