/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 7, 2013
 */
package com.cognizant.insurance.agent.service.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.cognizant.insurance.agent.request.vo.RequestInfo;

/**
 * The Class class LifeEngageSyncServiceHelper.
 * 
 * @author 300797
 */
public final class LifeEngageSyncServiceHelper {
    
    /**
     * Instantiates a new life engage sync service helper.
     */
    private LifeEngageSyncServiceHelper() {
        // not called
    }

    /** The Constant TRANSACTION_ID. */
    private static final String TRANSACTION_ID = "TransactionId";

    /** The Constant USER_NAME. */
    private static final String USER_NAME = "UserName";

    /** The Constant CREATION_DATE. */
    private static final String CREATION_DATE = "CreationDate";

    /** The Constant CREATION_TIME. */
    private static final String CREATION_TIME = "CreationTime";

    /** The Constant SOURCE_INFO_NAME. */
    private static final String SOURCE_INFO_NAME = "SourceInfoName";

    /** The Constant REQUESTOR_TOKEN. */
    private static final String REQUESTOR_TOKEN = "RequestorToken";

    /** The Constant RESPONSE_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";

    /** The Constant USER_EMAIL. */
    private static final String USER_EMAIL = "UserEmail";

    private static final String SYNC_CHUNK_SIZE = "SyncChunkSize";

    /** The Constant LAST_SYNC_DATE. */
    private static final String LAST_SYNC_DATE = "LastSyncDate";
    
    /** The Constant LAST_SYNC_DATE. */
    private static final String FIRST_TIME_SYNC = "FirstTimeSync";
    
    /** The Constant LAST_SYNC_DATE. */
    private static final String ARCHIVAL_PERIOD = "ArchivalPeriod";

    /** The Constant DEVICE_UUID. */
    private static final String DEVICE_UUID = "Device_uuid";

    /** The Constant DEVICE_UUID. */
    private static final String TRANSACTIONS = "Transactions";
       /** The Constant REQUEST. */
    private static final String REQUEST = "Request";
    /** The Constant REQUEST. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";
    /** The Constant REQUEST. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";

    /** The date format. */
    public static final String DATEFORMAT_MM_DD_YYYY = "MM-dd-yyyy";

    /** The Constant DATEFORMAT_MM_DD_YYYY_HH_MM_SS. */
    public static final String DATEFORMAT_MM_DD_YYYY_HH_MM_SS = "MM-dd-yyyy hh:mm:ss";
    
    /** The Constant DATEFORMAT_YYYY_MM_DD_HH_MM_SS. */
    public static final String DATEFORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    /** The Constant DATEFORMAT_HH_MM_SS. */
    public static final String DATEFORMAT_HH_MM_SS = "hh:mm:ss";

    /**
     * Parses the request info.
     * 
     * @param jsonRequestInfoObj
     *            the json request info obj
     * @return the request info
     * @throws ParseException
     *             the parse exception
     */
    public static RequestInfo parseRequestInfo(final JSONObject jsonRequestInfoObj) throws ParseException {

        final RequestInfo requestInfo = new RequestInfo();
        final SimpleDateFormat dateTimeFormat =
                new SimpleDateFormat(DATEFORMAT_MM_DD_YYYY_HH_MM_SS, Locale.getDefault());
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());

        requestInfo.setUserName(getStringKeyValue(USER_NAME, jsonRequestInfoObj));
        requestInfo.setCreationDateTime(getDateTimeKeyValue(dateTimeFormat, CREATION_DATE, CREATION_TIME,
                jsonRequestInfoObj));
        requestInfo.setSourceInfoName(getStringKeyValue(SOURCE_INFO_NAME, jsonRequestInfoObj));
        requestInfo.setTransactionId(getStringKeyValue(TRANSACTION_ID, jsonRequestInfoObj));
        requestInfo.setRequestorToken(getStringKeyValue(REQUESTOR_TOKEN, jsonRequestInfoObj));
        requestInfo.setDeviceUUID(getStringKeyValue(DEVICE_UUID, jsonRequestInfoObj));
        requestInfo.setLastSyncDate(getLastSyncDate(dateFormat, LAST_SYNC_DATE, jsonRequestInfoObj));
        requestInfo.setUserEmail(getStringKeyValue(USER_EMAIL, jsonRequestInfoObj));
        requestInfo.setFirstTimeSync(getBooleanKeyValue(FIRST_TIME_SYNC, jsonRequestInfoObj));
        requestInfo.setArchivalPeriod(getIntegerKeyValue(ARCHIVAL_PERIOD,jsonRequestInfoObj));
        
        return requestInfo;
    }

	/**
     * Sets the details in json response obj.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonRsArray
     *            the json rs array
     * @param jsonObjectRs
     *            the json object rs
     * @throws ParseException
     *             the parse exception
     */
    public static void setDetailsInJsonResponse(final RequestInfo requestInfo, final JSONArray jsonRsArray,
            final JSONObject jsonObjectRs, Integer pageSize) throws ParseException {
        final JSONObject jsonResponceObject = new JSONObject();
        final JSONObject jsonResponseInfoObject = new JSONObject();

        final SimpleDateFormat formatterForDate = new SimpleDateFormat(DATEFORMAT_MM_DD_YYYY, Locale.getDefault());
        final SimpleDateFormat formatterForTime = new SimpleDateFormat(DATEFORMAT_HH_MM_SS, Locale.getDefault());

        setStringValueInJsonObject(USER_NAME, requestInfo.getUserName(), jsonResponseInfoObject);
        setDateValueInJsonObject(CREATION_DATE, requestInfo.getCreationDateTime(), jsonResponseInfoObject,
                formatterForDate);
        setDateValueInJsonObject(CREATION_TIME, requestInfo.getCreationDateTime(), jsonResponseInfoObject,
                formatterForTime);
        setStringValueInJsonObject(SOURCE_INFO_NAME, requestInfo.getSourceInfoName(), jsonResponseInfoObject);
        setStringValueInJsonObject(TRANSACTION_ID, requestInfo.getTransactionId(), jsonResponseInfoObject);
        setStringValueInJsonObject(REQUESTOR_TOKEN, requestInfo.getRequestorToken(), jsonResponseInfoObject);
        setStringValueInJsonObject(USER_EMAIL, requestInfo.getUserEmail(), jsonResponseInfoObject);
        setStringValueInJsonObject(DEVICE_UUID, requestInfo.getDeviceUUID(), jsonResponseInfoObject);
        
        setIntegerValueInJsonObject(SYNC_CHUNK_SIZE, pageSize,jsonResponseInfoObject);
        String lastSyncDate=null;
        if (jsonRsArray != null && jsonRsArray.length() >0) {
        	int transactionLength = jsonRsArray.length();
        	if(!jsonRsArray.getJSONObject(transactionLength-1).isNull("Key14")){
        		lastSyncDate=jsonRsArray.getJSONObject(transactionLength-1).getString("Key14");
        	}        	       	
        } 
        if(lastSyncDate==null||lastSyncDate.isEmpty()){
        	lastSyncDate=getCurrentTimeStamp();
        }
        setStringValueInJsonObject(LAST_SYNC_DATE, lastSyncDate, jsonResponseInfoObject);
        jsonResponceObject.put("ResponseInfo", jsonResponseInfoObject);
        if (jsonRsArray.length() > 0) {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(TRANSACTIONS, jsonRsArray);
            jsonResponceObject.put(RESPONSE_PAYLOAD, jsonObject);
        } else {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(TRANSACTIONS, new JSONArray());
            jsonResponceObject.put(RESPONSE_PAYLOAD, jsonObject);
        }
        
        
        jsonObjectRs.put(RESPONSE, jsonResponceObject);
    }
    /**
     * 
     * @param retrieveEappResponse
     * @param jsonRequestObj
     * @return
     * @throws ParseException
     */

    public static JSONObject setDetailsInJsonRequest(
            String retrieveEappResponse, final JSONObject jsonRequestObj)
            throws ParseException {
        JSONObject jsonRqstObj = new JSONObject();
        JSONObject jsonRqst = new JSONObject();
        final JSONObject jsonRequestInfoObj = jsonRequestObj
                .getJSONObject(REQUEST_INFO);
        JSONObject jsonRequestPayloadObj = jsonRequestObj
                .getJSONObject(REQUEST_PAYLOAD);
        final JSONArray jsonRespTransactionArray = getTransactionArray(retrieveEappResponse);
        if (jsonRespTransactionArray.length() > 0) {
            final JSONObject jsonOb = jsonRespTransactionArray.getJSONObject(0);
            final JSONObject jsonTxDataObj = jsonOb
                    .getJSONObject("TransactionData");
            jsonRequestPayloadObj.getJSONArray(TRANSACTIONS).getJSONObject(0)
                    .remove("TransactionData");
            jsonRequestPayloadObj.getJSONArray(TRANSACTIONS).getJSONObject(0)
                    .put("TransactionData", jsonTxDataObj);
        }
        jsonRqst.put(REQUEST_PAYLOAD, jsonRequestPayloadObj);
        jsonRqst.put(REQUEST_INFO, jsonRequestInfoObj);
        return jsonRqstObj.put(REQUEST, jsonRqst);
    }

    /**
     * 
     * @param json
     * @return
     * @throws ParseException
     */
    public static JSONArray getTransactionArray(String json)
            throws ParseException {
        JSONObject jsonObject = new JSONObject(json);
        final JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        final JSONObject jsonResponsePayloadObj = jsonResponseObj
                .getJSONObject(RESPONSE_PAYLOAD);
        final JSONArray jsonRespTransactionArray = jsonResponsePayloadObj
                .getJSONArray(TRANSACTIONS);
        return jsonRespTransactionArray;
    }
    /**
     * Get current date time stamp
     * @return
     */
    public static String getCurrentTimeStamp() {
        DateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS);
        Calendar cal = Calendar.getInstance();      
        return dateFormat.format(cal.getTime()) ;
    }

    /**
     * Gets the string key value.
     * 
     * @param key
     *            the key
     * @param jsonObj
     *            the json obj
     * @return the string key value
     */
    private static String getStringKeyValue(final String key, final JSONObject jsonObj) {
        String result = null;
        if (jsonObj.has(key)) {
            result = jsonObj.getString(key);
        }

        return result;
    }
    
    /**
     * Gets the boolean key value.
     * 
     * @param firstTimeSync
     *            the key
     * @param jsonObj
     *            the json obj
     * @return the boolean key value
     */
    private static Boolean getBooleanKeyValue(final String firstTimeSync, final JSONObject jsonObj) {
        Boolean result = null;
        if (jsonObj.has(firstTimeSync)) {
            result = jsonObj.getBoolean(firstTimeSync);
        }

        return result;
    }

    /**
     * Gets the date time key value.
     * 
     * @param dateFormat
     *            the date format
     * @param dateKey
     *            the date key
     * @param timeKey
     *            the time key
     * @param jsonObj
     *            the json obj
     * @return the date time key value
     * @throws ParseException
     *             the parse exception
     */
    private static Date getDateTimeKeyValue(final SimpleDateFormat dateFormat, final String dateKey,
            final String timeKey, final JSONObject jsonObj) throws ParseException {
        Date result = null;
        if (jsonObj.has(dateKey) && jsonObj.has(timeKey) && !jsonObj.isNull(dateKey) && !jsonObj.isNull(timeKey)) {
            result = dateFormat.parse(jsonObj.getString(dateKey) + " " + jsonObj.getString(timeKey));
        }

        return result;
    }

    /**
     * Gets the date key value.
     * 
     * @param dateFormat
     *            the date format
     * @param key
     *            the key
     * @param jsonObj
     *            the json obj
     * @return the date key value
     * @throws ParseException
     *             the parse exception
     */
    private static Date getDateKeyValue(final SimpleDateFormat dateFormat, final String key, final JSONObject jsonObj)
            throws ParseException {
        Date result = dateFormat.parse("1900-01-01 11:50:36");
        if (jsonObj.has(key) && !jsonObj.isNull(key) && !"".equals(jsonObj.getString(key))) {
            result = dateFormat.parse(jsonObj.getString(key));
        }

        return result;
    }

    /**
     * Sets the string value in json object.
     * 
     * @param key
     *            the key
     * @param value
     *            the value
     * @param jsonObjectRs
     *            the json object rs
     */
    private static void setStringValueInJsonObject(final String key, 
            final String value, final JSONObject jsonObjectRs) {
        if (StringUtils.isNotEmpty(value)) {
            jsonObjectRs.put(key, value);
        } else {
            jsonObjectRs.put(key, "");
        }
    }

    /**
     * Sets the Integer value in json object.
     * 
     * @param key
     *            the key
     * @param value
     *            the value
     * @param jsonObjectRs
     *            the json object rs
     */
    private static void setIntegerValueInJsonObject(final String key, 
            final Integer value, final JSONObject jsonObjectRs) {
        if (value != null) {
            jsonObjectRs.put(key, value);
        }
    }

    /**
     * Sets the date value in json object.
     * 
     * @param key
     *            the key
     * @param value
     *            the value
     * @param jsonObjectRs
     *            the json object rs
     * @param formatterForDate
     *            the formatter for date
     */
    private static void setDateValueInJsonObject(final String key, final Date value, final JSONObject jsonObjectRs,
            final SimpleDateFormat formatterForDate) {
        if (value == null) {
            jsonObjectRs.put(key, "");
        } else {
            jsonObjectRs.put(key, formatterForDate.format(value));
        }
    }
    private static Integer getIntegerKeyValue(String archivalPeriod,
			JSONObject jsonObj) {
    	  Integer result = null;
          if (jsonObj.has(archivalPeriod)) {
              result = jsonObj.optInt(archivalPeriod);
          }

          return result;
	}
    private static Date getLastSyncDate(final SimpleDateFormat dateFormat, final String key, final JSONObject jsonObj)
    throws ParseException {
    	Date result = null;
    	String lastSyncDate=null;
    	Integer archivalPeriod=getIntegerKeyValue(ARCHIVAL_PERIOD, jsonObj);
    	if (jsonObj.has(key) && !jsonObj.isNull(key) && !"".equals(jsonObj.getString(key))) {
		    lastSyncDate=jsonObj.getString(key);
		}
    	if(archivalPeriod!=null && archivalPeriod>0&&(lastSyncDate==null||"".equals(lastSyncDate.trim()))){
    		Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -(archivalPeriod));    
            result= cal.getTime();	
    	}else{
    		result = dateFormat.parse("1900-01-01 11:50:36");
    		if (jsonObj.has(key) && !jsonObj.isNull(key) && !"".equals(jsonObj.getString(key).trim())) {
    		    result = dateFormat.parse(jsonObj.getString(key));
    		}
    	}
		
		
		return result;
    }


}
