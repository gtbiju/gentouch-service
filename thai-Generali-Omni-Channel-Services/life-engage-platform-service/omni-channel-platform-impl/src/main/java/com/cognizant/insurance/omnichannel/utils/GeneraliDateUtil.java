package com.cognizant.insurance.omnichannel.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class GeneraliDateUtil {
	
	public static String getDefaultDate() {
		//Setting general Default Date irrespective of Time Zone
		String dateString = "";
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final Date defaultDate;
		try {
			defaultDate = sd.parse("1900-01-01 01:01:01");
			dateString = defaultDate.toString();
		} catch (ParseException e) {

		}

		return dateString;

	}
	
	public static long getNoOfDaysBetweenDates(Date firstDate, Date secondDate) {
		long noOfDays = 0l;
		if (null != firstDate && null != secondDate) {
			long diff = firstDate.getTime() - secondDate.getTime();

			noOfDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		}
		return noOfDays;
	}

}
