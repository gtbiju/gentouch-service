/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists;

/**
 * Identifies a classification of Communications according to their priority.
 * 
 * @author 300797
 * 
 * 
 */
public enum PriorityCodeList {
    /**
     * Identifies a Communication with priority '1 (High Priority)'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    HighPriority,
    /**
     * Identifies a Communication with priority '5 (Low Priority)'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    LowPriority
}
