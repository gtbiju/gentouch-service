package com.cognizant.insurance.omnichannel.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationInteractionTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationStatusCodeList;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
import com.cognizant.insurance.omnichannel.vo.AgentData;
import com.cognizant.insurance.omnichannel.vo.ManagerialLevelUserDetails;
import com.cognizant.insurance.party.dao.PartyDao;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public interface GeneraliPartyDao extends PartyDao {

    Response<Set<Insured>> retrieveInsuredWithAdditionalInsured(Request<Agreement> agreementRequest);

    Response<List<SearchCountResult>> getRelatedTransactionCountForLMS(
            final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);
     Response<AgreementProducer> retrieveAgent(Request<Agreement> agreementRequest);
     
     public List<String> getAgentLeadCount(final Request<List<String>> agentCodeRequst,
 			final Request<List<CustomerStatusCodeList>> statusRequest);

	List<AgentData> getYTD(Request<List<AgentData>> agentCodeRequst,
			Request<List<CustomerStatusCodeList>> statusRequestClosed,
			Request<List<CustomerStatusCodeList>> statusRequestTotal);

	ManagerialLevelUserDetails getAgentStatusCount(
			Request<ManagerialLevelUserDetails> managerialLevelUserDetailsRequest,
			List<CustomerStatusCodeList> statusListNew,
			List<CustomerStatusCodeList> statusListClosed,
			List<CustomerStatusCodeList> statusListFollowUp);


	public Response<Customer> retrieveCustomerWithTransTrackId(Request<String> trackIdRequest,Request<List<CustomerStatusCodeList>> statusRequest);

	Response<SearchCountResult> getRetrieveFilterCount(
			Request<SearchCriteria> searchCriteriaRequestObj,
			List<CustomerStatusCodeList> statusRequest,
			List<CustomerStatusCodeList> statusListClosed);

	Response<SearchCountResult> getRetrieveFilterCountBranchUser(
			Request<SearchCriteria> searchCriteriaRequestObj,
			List<CustomerStatusCodeList> statusRequest,
			List<CustomerStatusCodeList> statusListClosed);

	Map<String,Integer> retrieveMonthlySalesActivityCustomers(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest, Request<List<CommunicationStatusCodeList>> communicationStatusRequest,
			Request<List<CommunicationInteractionTypeCodeList>> communicationInteractionCodeRequest);

	Map<String,Integer> retrieveMonthwiseSalesActivityCustomers(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

	Map<String, Integer> retrieveWeekwiseSalesActivityCustomers(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

	Map<String, Integer> retrieveNewProspect(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

	Map<String, Integer> retrieveMonthwiseNewProspect(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

	List<Customer> retrieveMonthlyNewProspect(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

    Map<String, String> retrieveMonthlyNewCount(Request<SearchCriteria> searchCriteriatRequest);

    Map<String, Map<String, String>> retrieveLeadStatistics(Request<SearchCriteria> searchCriteriatRequest);



	
    

}
