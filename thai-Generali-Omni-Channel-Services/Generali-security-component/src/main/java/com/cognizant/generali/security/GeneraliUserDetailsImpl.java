package com.cognizant.generali.security;

import com.cognizant.le.security.userauth.LifeEngageUserDetailsImpl;

public class GeneraliUserDetailsImpl extends LifeEngageUserDetailsImpl{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4248419346895670276L;
	
	private boolean isSuspented =false;

	public boolean isSuspented() {
		return isSuspented;
	}

	public void setSuspented(boolean isSuspented) {
		this.isSuspented = isSuspented;
	}
	
	

}
