/**
 *
 * Copyright 2016, Cognizant 
 *
 * @author        : 291446
 * @version       : 0.1, Sep 8, 2016
 */
package com.cognizant.insurance.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.service.GeneraliManagerialLevelService;
import com.cognizant.insurance.services.LifeEngageSyncService;

/**
 * Class GeneraliManagerialLevelEndPoint - Hierarchy End Point
 * @author 291446
 *
 */

@Controller
@RequestMapping("/generaliManagerialLevelService")
public class GeneraliManagerialLevelEndPoint {
	
	 /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliManagerialLevelEndPoint.class);

    @Autowired
    private GeneraliManagerialLevelService generaliManagerialLevelService;
    
    /**
     * Method to get the Managerial Hierarchy details
     * @param json
     * @return json
     */
    
    @RequestMapping(value = "/retrieveByHierarchicalLevel", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String retrieveByHierarchicalLevel(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return generaliManagerialLevelService.retrieveByManagerialLevel(json);
            }
        }.execute(json);
    }
    
    public GeneraliManagerialLevelService getGeneraliManagerialLevelService() {
		return generaliManagerialLevelService;
	}


	public void setGeneraliManagerialLevelService(
			GeneraliManagerialLevelService generaliManagerialLevelService) {
		this.generaliManagerialLevelService = generaliManagerialLevelService;
	}
    
}
