/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 21, 2013
 */
package com.cognizant.insurance.component;

import java.text.ParseException;

import org.json.JSONObject;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;
/**
 * The Interface interface LifeEngageIllustrationComponent.
 * 
 * @author 300797
 */
public interface LifeEngageIllustrationComponent {

    /**
     * Retrieve all illustration.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonObj
     *            the json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveAllIllustration(RequestInfo requestInfo, JSONObject jsonObj) throws BusinessException,
            ParseException;

    /**
     * Retrieve illustration.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonObj
     *            the json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveIllustration(RequestInfo requestInfo, JSONObject jsonObj) throws BusinessException, ParseException;

    /**
     * Save illustration.
     * 
     * @param json
     *            the json
     * @param requestInfo
     *            the request info
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     */
    String saveIllustration(String json, String jsonIllustrationOutput, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit);

    /**
     * Run illustration.
     * 
     * @param inputJson
     *            the input json
     * @param illustrationRuleName
     *            the illustration rule name
     * @param validationRuleName
     *            the validation rule name
     * @return the string
     */
    String runIllustration(JSONObject inputJson);

    /**
     * Execute Rule.
     * 
     * @param inputJson
     *            the input json
     * @param illustrationRuleName
     *            the illustration rule name
     * @param validationRuleName
     *            the validation rule name
     * @return the string
     */
    String executeRule(JSONObject inputJson);
    
    /**
     * Generate illustration.
     *
     * @param requestInfo the request info
     * @param inputJson the input json
     * @return the string
     */
    String generateIllustration(JSONObject inputJson) throws ParseException;
}
