package com.cognizant.generali.security.exception;

import org.springframework.security.core.AuthenticationException;

import com.cognizant.le.security.exception.UserNotFoundException;

public class InvalidAuthResponseException extends UserNotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1072551998339871172L;

	public InvalidAuthResponseException(String msg) {
		super(msg);		
	}

}
