/**
 *
 * © Copyright 2013, Cognizant 
 *
 * @author        : 301350
 * @version       : 0.1, Aug 13, 2013
 */
package com.cognizant.insurance.agent.response.vo;

import java.util.List;
import java.util.Map;

/**
 * The Class class CountryLookUpResponse.
 * 
 * @author 301350 Added as part of eApp - 13Aug2013
 */
public class CountryLookUpResponse {

    /** The country name. */
    private String name;
    
    /** The map containing state-city mapping. */
    private Map<String, List<String>> stateCityMap;
    
    /** The response info. */
    private ResponseInfo responseInfo;

    /** The country state or city list. */
    private Map<String, String> countryStateOrCityMap;
    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the stateCityMap.
     * 
     * @return Returns the stateCityMap.
     */
    public final Map<String, List<String>> getStateCityMap() {
        return stateCityMap;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * Sets The stateCityMap.
     * 
     * @param stateCityMap
     *            The stateCityMap to set.
     */
    public final void setStateCityMap(Map<String, List<String>> stateCityMap) {
        this.stateCityMap = stateCityMap;
    }

    /**
     * Gets the responseInfo.
     *
     * @return Returns the responseInfo.
     */
    public final ResponseInfo getResponseInfo() {
        return responseInfo;
    }

    /**
     * Sets The responseInfo.
     *
     * @param responseInfo The responseInfo to set.
     */
    public final void setResponseInfo(ResponseInfo responseInfo) {
        this.responseInfo = responseInfo;
    }

    /**
     * @return the countryStateOrCityMap
     */
    public Map<String, String> getCountryStateOrCityMap() {
        return countryStateOrCityMap;
    }

    /**
     * @param countryStateOrCityMap the countryStateOrCityMap to set
     */
    public void setCountryStateOrCityMap(Map<String, String> countryStateOrCityMap) {
        this.countryStateOrCityMap = countryStateOrCityMap;
    }
}
