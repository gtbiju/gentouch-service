package com.cognizant.insurance.agent.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.agent.domain.agent.Agent;
import com.cognizant.insurance.agent.dao.exception.DaoException;
import com.cognizant.insurance.agent.dao.impl.DaoImpl;
import com.cognizant.insurance.agent.dao.AgentDao;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.request.Request;

public class AgentDaoImpl extends DaoImpl implements AgentDao {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory
			.getLogger(AgentDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cognizant.insurance.generic.dao.AgentDao#retrieveAgentByCode(com.
	 * cognizant.insurance.request.Request)
	 */
	@Override
	public GeneraliAgent retrieveAgentByCode(Request<GeneraliAgent> request) {
		GeneraliAgent retrievedAgent = null;
		GeneraliAgent inpAgent = request.getType();
		String query = "SELECT agent FROM GeneraliAgent agent WHERE agent.agentCode = ?1";
		List<GeneraliAgent> agentsList = findByQuery(request, query,inpAgent.getAgentCode());
		if (agentsList.size() > 0) {
			retrievedAgent = agentsList.get(0);
		}
		return retrievedAgent;
	}
}
