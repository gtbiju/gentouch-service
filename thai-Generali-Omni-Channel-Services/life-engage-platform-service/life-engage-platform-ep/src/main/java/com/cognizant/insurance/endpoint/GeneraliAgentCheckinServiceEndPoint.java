/**
 * 
 */
package com.cognizant.insurance.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.service.CheckinService;

/**
 * @author 397850
 *
 */
@Controller
@RequestMapping("/agentCheckinService")
public class GeneraliAgentCheckinServiceEndPoint {
	
	@Autowired
	CheckinService checkinService;
	
	@RequestMapping(value = "/agentCheckin", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
	public final String retrieveAgent(@RequestBody final String json) {
		return new ServiceTemplate() {
			@Override
			public String doExecute(final String json) throws BusinessException {
				return checkinService.checkinAgentDetails(json);
			}
		}.execute(json);
	}
	
    @RequestMapping(value = "/retrieveDetails", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String retrieve(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return checkinService.retrieveDetails(json);
            }
        }.execute(json);
    }

}
