/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 397850
 * @version       : 0.1, Dec 1, 2016
 */
package com.cognizant.insurance.agent.domain.agent;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

// TODO: Auto-generated Javadoc
/**
 * The Class BranchDetails.
 *
 * @author 397850
 */
@Entity
@Table(name = "BRANCH_DETAILS", uniqueConstraints = {@UniqueConstraint(columnNames={"id"})})
public class BranchDetails {
    
    /** The id. */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    /** The branch id. */
    @Column(name = "branchCode",length = 30  )
    private String branchCode;
    
    /** The branch name. */
    @Column(name = "branchName")
    private String branchName;  
    
    /** The latitude. */
    @Column(name= "latitude",length = 30)
    private String latitude;
    
    /** The longitude. */
    @Column(name= "longitude",length = 30)
    private String longitude;
    
    /** The radius. */
    @Column(name="radius",length = 30)
    private String radius;
    
    /** The agents. */
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "branchDetails")
    private List<Agent> agents;



    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the branch code.
	 *
	 * @return the branch code
	 */
	public String getBranchCode() {
		return branchCode;
	}

	/**
	 * Sets the branch code.
	 *
	 * @param branchCode the new branch code
	 */
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	/**
     * Gets the branch name.
     *
     * @return the branch name
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * Sets the branch name.
     *
     * @param branchName the new branch name
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * Gets the latitude.
     *
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * Sets the latitude.
     *
     * @param latitude the new latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * Gets the longitude.
     *
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * Sets the longitude.
     *
     * @param longitude the new longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * Gets the radius.
     *
     * @return the radius
     */
    public String getRadius() {
        return radius;
    }

    /**
     * Sets the radius.
     *
     * @param radius the new radius
     */
    public void setRadius(String radius) {
        this.radius = radius;
    }

    /**
     * Gets the agents.
     *
     * @return the agents
     */
    public List<Agent> getAgents() {
        return agents;
    }

    /**
     * Sets the agents.
     *
     * @param agents the agents to set
     */
    public void setAgents(List<Agent> agents) {
        this.agents = agents;
    }
    
    
    
    
    
    

}
