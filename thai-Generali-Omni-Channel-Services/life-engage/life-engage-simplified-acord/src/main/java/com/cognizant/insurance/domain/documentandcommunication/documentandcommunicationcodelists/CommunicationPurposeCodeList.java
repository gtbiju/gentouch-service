/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists;

/**
 * Identifies a classification of Communications according to their purpose.
 * 
 * @author 300797
 * 
 * 
 */
public enum CommunicationPurposeCodeList {
    /**
     * Identifies a Communication with purpose 'Campaign mailing'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    CampaignMailing,
    /**
     * Identifies a Communication with purpose 'Complaint'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Complaint,
    /**
     * Identifies a Communication with purpose 'Demographic data'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    DemographicData,
    /**
     * Identifies a Communication with purpose 'Quotation request'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    QuotationRequest,
    /**
     * Identifies a Communication with purpose 'Regular statement'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    RegularStatement,
    /**
     * Identifies a Communication with purpose 'Returned mail'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    ReturnedMail,
    /**
     * Identifies a Communication with purpose 'Sale'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Sale,
    /**
     * Identifies a Communication with purpose 'Complaint Handling'.
     * 
     * 
     * 
     */
    ComplaintHandling,
    //For Generali
    
    Open,
    New,
    //For Generali vietnam
    Non_Contactable,
	
	Meeting_Fixed,
	
	Successful_Sale,
	
	ToBeCalled,
	
	ContactedAndNotInterested,
	
	App_Submitted,
	
	Pending_Documents,
	
	Others,
	
	NotMentioned
    
    
}
