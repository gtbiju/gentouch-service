/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Nov 4, 2013
 */

package com.cognizant.insurance.domain.product;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class ProductTemplates.
 * 
 * 
 */

@Entity
@Table(name = "CORE_PROD_TEMPLATES")
public class ProductTemplates implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7136100978621736164L;

    /** The TEMPLATE_ID. */
    @Id
    @Column(name = "TEMPLATE_ID")
    private String templateId;

    /** The product template mappings. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, mappedBy = "productTemplates")
    private Set<ProductTemplateMapping> productTemplateMappings;

    /** The template name. */
    @Column(name = "TEMPLATE_NAME", unique = true)
    private String templateName;

    /** The language. */
    @Column(name = "LANGUAGE")
    private String language;

    /** The country code. */
    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    /** The version. */
    @Column(name = "VERSION", columnDefinition = "decimal(5,3) DEFAULT NULL")
    private Double version;

    /** The carrier. imp */
    @ManyToOne(cascade = { CascadeType.REFRESH })
    @JoinColumn(name = "CARRIER_ID")
    private Carrier carrier;
    
    /**
     * Gets the product template mappings.
     * 
     * @return the productTemplateMappings
     */
    public Set<ProductTemplateMapping> getProductTemplateMappings() {
        return productTemplateMappings;
    }

    /**
     * Sets the product template mappings.
     * 
     * @param productTemplateMappings
     *            the productTemplateMappings to set
     */
    public void setProductTemplateMappings(Set<ProductTemplateMapping> productTemplateMappings) {
        this.productTemplateMappings = productTemplateMappings;
    }

    /**
     * Gets the language.
     * 
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the language.
     * 
     * @param language
     *            the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Gets the template id.
     * 
     * @return the templateId
     */
    public String getTemplateId() {
        return templateId;
    }

    /**
     * Sets the template id.
     * 
     * @param templateId
     *            the templateId to set
     */
    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    /**
     * Gets the template name.
     * 
     * @return the templateName
     */
    public String getTemplateName() {
        return templateName;
    }

    /**
     * Sets the template name.
     * 
     * @param templateName
     *            the templateName to set
     */
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    /**
     * Gets the country code.
     * 
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the country code.
     * 
     * @param countryCode
     *            the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * Gets the version.
     * 
     * @return the version
     */
    public Double getVersion() {
        return version;
    }

    /**
     * Sets the version.
     * 
     * @param version
     *            the version to set
     */
    public void setVersion(Double version) {
        this.version = version;
    }
    
    /**
	 * Gets the carrier.
	 *
	 * @return the carrier
	 */
	public Carrier getCarrier() {
		return carrier;
	}

	/**
	 * Sets the carrier.
	 *
	 * @param carrier the carrier to set
	 */
	public void setCarrier(Carrier carrier) {
		this.carrier = carrier;
	}
    
}
