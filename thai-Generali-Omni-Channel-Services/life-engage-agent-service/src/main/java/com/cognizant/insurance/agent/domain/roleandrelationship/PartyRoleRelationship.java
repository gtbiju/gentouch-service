/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;

/**
 * This concept is the general notion gathering all relationships between party
 * roles (played by person or organizations).
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PARTY_ROLE_RELATIONSHIP_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class PartyRoleRelationship extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6716556226553825694L;

    /**
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private TimePeriod effectivePeriod;

    /**
     * 
     * 
     * 
     * 
     */
    private String description;

    /**
     * Status of the relationship.
     * 
     * 
     * 
     */
    private String status;

    /**
     * Date of change of the status of the relationship.
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date statusDate;

    /**
     * This relationship links a role in agreement to a relationship.
     * 
     * e.g. An employee (role) is related to an employment (relationship). This
     * concept represents the party role involved in a party role relationship.
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "PARTYROLEINAGREEMENT_PARTYROLERELATIONSHIP",
            joinColumns = { @JoinColumn(name = "PARTYROLERELATIONSHIP_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "PARTYROLEINAGREEMENT_ID", referencedColumnName = "Id") })
    private Set<PartyRoleInAgreement> involvedRoleInAgreement;

    /**
     * This relationship links a role in relationship to a relationship.
     * 
     * e.g. An employee (role) is related to an employment (relationship). This
     * concept represents the party role involved in a party role relationship.
     * 
     * 
     * 
     * 
     */
//    @OneToMany(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
//    @JoinTable(name = "PARTYROLEINRELATIONSHIP_PARTYROLERELATIONSHIP",
//            joinColumns = { @JoinColumn(name = "PARTYROLERELATIONSHIP_ID", referencedColumnName = "Id") },
//            inverseJoinColumns = { @JoinColumn(name = "PARTYROLEINRELATIONSHIP_ID", referencedColumnName = "Id") })
    @Transient
    private Set<PartyRoleInRelationship> involvedPartyRoleInRelationship;

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the effectivePeriod.
     * 
     * @return Returns the effectivePeriod.
     */
    public TimePeriod getEffectivePeriod() {
        return effectivePeriod;
    }

    /**
     * Gets the status.
     * 
     * @return Returns the status.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Gets the statusDate.
     * 
     * @return Returns the statusDate.
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The effectivePeriod.
     * 
     * @param effectivePeriod
     *            The effectivePeriod to set.
     */
    public void setEffectivePeriod(final TimePeriod effectivePeriod) {
        this.effectivePeriod = effectivePeriod;
    }

    /**
     * Sets The status.
     * 
     * @param status
     *            The status to set.
     */
    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Sets The statusDate.
     * 
     * @param statusDate
     *            The statusDate to set.
     */
    public void setStatusDate(final Date statusDate) {
        this.statusDate = statusDate;
    }

    /**
     *  Get involvedRoleInAgreement
     * @return Set<PartyRoleInAgreement>
     */
    public Set<PartyRoleInAgreement> getInvolvedRoleInAgreement() {
        return involvedRoleInAgreement;
    }

    /**
     * Set involvedRoleInAgreement 
     * @param involvedRoleInAgreement
     */
    public void setInvolvedRoleInAgreement(Set<PartyRoleInAgreement> involvedRoleInAgreement) {
        this.involvedRoleInAgreement = involvedRoleInAgreement;
    }

    /**
     *  Get involvedRoleInClaim
     * @return Set<PartyRoleInRelationship>
     */
    public Set<PartyRoleInRelationship> getInvolvedPartyRoleInRelationship() {
        return involvedPartyRoleInRelationship;
    }
    
    /**
     * Set involvedPartyRoleInRelationship
     * @param involvedPartyRoleInRelationship
     */
    public void setInvolvedPartyRoleInRelationship(Set<PartyRoleInRelationship> involvedPartyRoleInRelationship) {
        this.involvedPartyRoleInRelationship = involvedPartyRoleInRelationship;
    }
    
    
}