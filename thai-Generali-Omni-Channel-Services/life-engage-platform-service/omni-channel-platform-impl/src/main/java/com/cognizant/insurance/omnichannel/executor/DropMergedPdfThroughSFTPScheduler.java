package com.cognizant.insurance.omnichannel.executor;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.omnichannel.component.GeneraliBPMEmailPdfComponent;

@Component
public class DropMergedPdfThroughSFTPScheduler {
	
	@Value("${le.platform.default.batFileUpload}")
	private String batFile;
	
	@Value("${le.platform.default.batRootFileLocation}")
	private String batRootFileLocation;
	
	@Autowired
	private GeneraliBPMEmailPdfComponent generaliBPMEmailPdfComponent; 
	
	/** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(DropMergedPdfThroughSFTPScheduler.class);

	public void run() {

	    try {
	    	LOGGER.info("Drop PDF to IN folder through SFTP - Starts");
	    	String batLogFileName = generateBatLogFileName();
	    	LOGGER.info("Log File : " + batLogFileName);
	    	generaliBPMEmailPdfComponent.triggerSFTP(batFile + ".bat", batLogFileName, batRootFileLocation, "");
	    	LOGGER.info("Drop PDF to IN folder through SFTP - Ends");
	    	
	    	LOGGER.info("Delete sucessful transferred file from Temp Folder - Starts");
	    	boolean saveSuccess = generaliBPMEmailPdfComponent.readTransferredFilesFromTemp(batLogFileName, true);
	    	LOGGER.info("readTransferredFilesFromTemp function executed for Inbound");
	    	/*boolean saveSuccess = generaliBPMEmailPdfComponent.saveSuccessfullyTransferredFilesFromTemp(transferredFileReadList); 
	    	LOGGER.info("saveSuccessfullyTransferredFilesFromTemp function executed for Inbound");*/
	    	if(saveSuccess){
	    		boolean isDelete = generaliBPMEmailPdfComponent.deleteSuccessfullyTransferredFilesFromTemp(true);
	    		LOGGER.info("deleteSuccessfullyTransferredFilesFromTemp function executed " + isDelete);
	    	}
	    	LOGGER.info("Drop PDF to IN folder through SFTP - Ends");
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	LOGGER.error("Schedular Trigger Failed ", e);
	    }

	  }
	
	private String generateBatLogFileName() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddHHmm");
        return  batFile.substring(batFile.lastIndexOf("/") + 1) + "_" + ft.format(cal.getTime());
	}
	
}
