/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291430
 * @version       : 0.1, Dec 30, 2013
 */
package com.cognizant.icr.pdf.file;

/**
 * The Interface FileNameGenerator.
 */
public interface FileNameGenerator {
	
	 /**
 	 * Generate file name.
 	 *
 	 * @param <T> the generic type
 	 * @param object the object
 	 * @return the string
 	 */
 	<T> String generateFileName(T object);

}
