/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.contactcodelists;

/**
 * A classification for the use of a contact point - that is, how a particular
 * contact is used on the context of some purpose (see the ContactPointPurpose
 * enumeration).
 * 
 * For example, the purpose may be Billing and the usage is Biller Remittance
 * Address. Another example is the purpose of Mailing and the usage of Primary
 * Residence.
 * 
 * Purpose and Usage do NOT have to used together as shown in the examples. <!--
 * end-UML-doc -->
 * 
 * @author 301350
 * 
 * 
 */
public enum ContactPreferenceUsageCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    Business,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * This is for a non-business contact point such as a home address or
     * personal email address.
     * 
     * 
     * 
     */
    Personal
}
