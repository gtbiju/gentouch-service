
package com.cognizant.insurance.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.activation.DataSource;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.apache.commons.codec.binary.Base64;

import com.cognizant.insurance.component.LifeEngageDocumentComponent;
import com.cognizant.insurance.component.LifeEngageEmailComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.EmailRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;


// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageEAppComponentImpl.
 * 
 * @author 300797
 */
@Service("lifeEngageEmailComponent")
public class LifeEngageEmailComponentImpl implements LifeEngageEmailComponent {
	
	/** The Constant EMAIL_ID. */
    private static final String EMAIL_ID = "toMailIds";

	/** The Constant CC_MAIL_ID. */
	private static final String CC_MAIL_ID = "ccMailIds";
	
	 /** The Constant CC_MAIL_ID. */
    private static final String FROM_MAIL_ID = "fromMailId";
    
	/** The Constant MAIL_SUBJECT. */
   	private static final String MAIL_SUBJECT = "mailSubject";
	
	 /** The Constant TEMPLATE_ID. */
    private static final String TEMPLATE_ID = "templateId";
    
    /** The Constant TYPE. */
    private static final String TYPE = "Type";
    
    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";
    
    /** The Constant FNA. */
    private static final String FNA = "FNA";
    
    /** The Constant ID. */
    private static final String ID = "id";
    
    /** The Constant template resource loader. */
    private static final String TEMPLATE_RESOURCE_LOADER = "resource.loader";
    
    /** The Constant template resource loader value. */
    private static final String TEMPLATE_RESOURCE_LOADER_VALUE = "file";
    
    /** The Constant template resource loader class. */
    private static final String TEMPLATE_RESOURCE_LOADER_CLASS = "file.resource.loader.class";
    
    /** The Constant template resource loader class value. */
    private static final String TEMPLATE_RESOURCE_LOADER_CLASS_VALUE = "org.apache.velocity.runtime.resource.loader.FileResourceLoader";
    
    /** The Constant template resource path. */
    private static final String TEMPLATE_RESOURCE_PATH= "file.resource.loader.path";
    
    /** The Constant email msg content. */
    private static final String EMAIL_MSG_CONTENT= "text/html; charset=utf-8";
    
    /** The Constant pdf type. */
    private static final String PDF_TYPE= "application/pdf";
    
    /** The Constant EMAIL_JSON. */
    private static final String EMAIL_JSON = "emailJson";
    
    /** The Constant ATTEMPT_NUMBER. */
    private static final String ATTEMPT_NUMBER = "attemptNo";
    
    /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = "TransactionData";

    /** The Constant PRODUCT. */
    private static final String PRODUCT = "Product";

    /** The Constant PRODUCT_NAME. */
    private static final String PRODUCT_NAME = "productName";

    /** The Constant PRODUCT_DETAILS. */
    private static final String PRODUCT_DETAILS = "ProductDetails";

    /** The Constant POLICY_DETAILS. */
    private static final String POLICY_DETAILS = "policyDetails";

    /** The Constant COMMITTED_PREMIUM. */
    private static final String COMMITTED_PREMIUM = "committedPremium";

    /** The Constant Email. */
    private static final String EMAIL = "Email";

    /** The Constant Key18. */
    private static final String KEY_18 = "Key18";

    /** The Constant Key4. */
    private static final String KEY_4 = "Key4";

    /** The Constant Key3. */
    private static final String KEY_3 = "Key3";

    /** The Constant Key5. */
    private static final String KEY_5 = "Key5";

    /** The Constant Key6. */
    private static final String KEY_6 = "Key6";

    /** The Constant AGENT ID. */
    private static final String AGENT_ID = "agentId";

    /** The Constant EMAIL_TEMPLATE. */
    private static final String EMAIL_TEMPLATE = "emailTemplate";

    /** The Constant INITIALISED. */
    private static final String INITIALISED = "initialised";
    
    /** The Constant FILE_NAME. */
    private static final String FILE_NAME = "fileName";
    
    /** The Constant BASE64_VALUE. */
    private static final String BASE64_VALUE = "base64Value";
    
    /** The Constant APPLICATION_ALL_TYPE. */
    private static final String APPLICATION_ALL_TYPE= "application/*";

    /** The retrieve by Ids holder. */
    @Autowired
    @Qualifier("eMailDetailsUpdateMapping")
    private LifeEngageSmooksHolder eMailUpdateHolder;
    
    @Value("${le.platform.service.email.templatePath}")
    private String templatePath; 
    
    @Value("${le.platform.service.email.fromMailId}")
    private String fromMailId;
    
    @Value("${le.platform.service.email.fromMailPassword}")
    private String fromMailPassword;
    
    @Value("${le.platform.service.email.auth}")
    private String mailAuth;
    
    @Value("${le.platform.service.email.authValue}")
    private String mailAuthValue;
    
    @Value("${le.platform.service.email.lts}")
    private String mailLTS;
    
    @Value("${le.platform.service.email.ltsEnable}")
    private String mailLTSEnable;
    
    @Value("${le.platform.service.email.hostName}")
    private String mailHostKey;
    
    @Value("${le.platform.service.email.host}")
    private String mailHost;
    
    @Value("${le.platform.service.email.portName}")
    private String mailPortName;
    
    @Value("${le.platform.service.email.port}")
    private String mailPort;
    
    
    @Autowired
	Job job;

	@Autowired
	JobLauncher jobLauncher;
	
	@Autowired
    private LifeEngageDocumentComponent lifeEngageDocumentComponent;
	
	@Autowired
	private EmailRepository emailRepository;
	
	/** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageSearchCriteriaComponentImpl.class);
    
	public StatusData sendEmail(final String json, final String templateName, final String templateValues, final String pdfName) throws BusinessException{
		 Properties mailProperties = null;
	        String emailStatus = Constants.SUCCESS;
	        final StatusData statusData = new StatusData();
	        statusData.setStatus(emailStatus);
	        ByteArrayOutputStream byteStream = null;

	        try {
	        	mailProperties = new Properties();
	        	mailProperties.put(mailAuth, mailAuthValue);
	        	mailProperties.put(mailLTS, mailLTSEnable);
	        	mailProperties.put(mailHostKey, mailHost);
	        	mailProperties.put(mailPortName, mailPort);

	            final Session session = Session.getDefaultInstance(mailProperties, new javax.mail.Authenticator() {
	                protected PasswordAuthentication getPasswordAuthentication() {
	                    return new PasswordAuthentication(fromMailId, fromMailPassword);
	                 }
	              });
	            final MimeMessage mimeMessage = new MimeMessage(session);
	            final MimeBodyPart messagePart = new MimeBodyPart();
	            final Multipart multiPart = new MimeMultipart();
	            Properties templateProperties = new Properties();
	            templateProperties.setProperty( TEMPLATE_RESOURCE_LOADER, TEMPLATE_RESOURCE_LOADER_VALUE);
	            templateProperties.setProperty( TEMPLATE_RESOURCE_LOADER_CLASS, TEMPLATE_RESOURCE_LOADER_CLASS_VALUE);
	            templateProperties.setProperty( TEMPLATE_RESOURCE_PATH, templatePath);
	            VelocityEngine ve = new VelocityEngine();
	            ve.init(templateProperties);
	            Template template = ve.getTemplate(templateName);
	            /*  create a context and add data */
	            VelocityContext context = new VelocityContext();
	            final InternetAddress fromInternetAddress = new InternetAddress(fromMailId);
	            final JSONObject emailObj = new JSONObject(json);
	            final String emailId = emailObj.getString(EMAIL_ID);
	            final String id =  emailObj.getString(ID);
	            final JSONObject templateObj = new JSONObject(templateValues);
	            Iterator<?> keys = templateObj.keys();
	            while( keys.hasNext() ) {
	                String key = (String)keys.next();
	                String value= (String) templateObj.get(key);
	                context.put(key,value);
	            }
	            /* rendering the template into a StringWriter */
	            StringWriter writer = new StringWriter();
	            template.merge( context, writer );
	            final String emailMessage = writer.toString();
	            final String ccedMailId = emailObj.getString(CC_MAIL_ID);
	            final String tempalteId = emailObj.getString(TEMPLATE_ID);
	            final String type = emailObj.getString(TYPE);
	            final String subject = emailObj.getString(MAIL_SUBJECT);
	            mimeMessage.setFrom(fromInternetAddress);
	            mimeMessage.setRecipients(Message.RecipientType.CC,InternetAddress.parse(ccedMailId));
	            mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailId));
	            mimeMessage.setSubject(subject);
	            mimeMessage.setSentDate(new Date());
	            messagePart.setContent(emailMessage, EMAIL_MSG_CONTENT);
	            multiPart.addBodyPart(messagePart);
	            MimeBodyPart attachmentPart = new MimeBodyPart();	
	            if(!tempalteId.isEmpty())  {      
		            if (E_APP.equals(emailObj.getString(TYPE))) {
								byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
										.generatePdfGet(id, type,tempalteId);
	
					} else if (ILLUSTRATION.equals(emailObj.getString(TYPE))){
			                   byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
			    						.generateIllustrationPdf(id, type,tempalteId);
                    } else if (FNA.equals(emailObj.getString(TYPE))) {
                        byteStream =
                                (ByteArrayOutputStream) lifeEngageDocumentComponent.generateFNAReport(id, type, tempalteId);
                    }
	            }
	            InputStream pdfByteArray = new ByteArrayInputStream(byteStream.toByteArray());
	        	ByteArrayDataSource fileDataSource = new ByteArrayDataSource(pdfByteArray, PDF_TYPE);
	        	attachmentPart.setDataHandler(new DataHandler(fileDataSource));
	        	attachmentPart.setFileName(pdfName);
	        	multiPart.addBodyPart(attachmentPart);
	            mimeMessage.setContent(multiPart);
	            Transport.send(mimeMessage);
	        } catch (ParseException e) {
	    		statusData.setStatus(Constants.FAILURE);
	    		statusData.setStatusMessage(e.getMessage());
	        } catch (MessagingException e) {
	        	statusData.setStatus(Constants.FAILURE);
	    		statusData.setStatusMessage(e.getMessage());
	        }catch (Exception e) {
	        	statusData.setStatus(Constants.FAILURE);
	    		statusData.setStatusMessage(e.getMessage());
	        }
	        return statusData;
	}
	public void triggerEmail(final String agentId, final String type) throws BusinessException{
		try {
			 List<LifeEngageEmail> lifeEngageEmails = emailRepository.getInitialisedLifeEngageEmails(agentId,type);
			 for(LifeEngageEmail email : lifeEngageEmails){
			 jobLauncher.run(
						job,
						new JobParametersBuilder().addLong(ID, email.getId()).addString(EMAIL_JSON, email.getEmailValues()).addString(ATTEMPT_NUMBER, email.getAttemptNumber()).toJobParameters());
			 }
		 } catch (Exception e) {
		    	e.printStackTrace();
		    }
	}
	/*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageEmailComponent#updateEmailDetails(java.lang.String,
     * java.lang.String)
     */
    public String updateEmailDetails(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException {
        Transactions transactions = new Transactions();
        String response = null;
        String agentId = null;
        String successMessage;
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
        try {
            if (jsonRqArray != null) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);

                    String emailFlag = (String) jsonTransactionsObj.getString(KEY_18);
                    agentId = jsonTransactionsObj.getString(Constants.KEY11);
                    try {
                        if (E_APP.equals(jsonTransactionsObj.getString(TYPE))) {
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                lifeEngageEmail = saveLifeEngageEmail(jsonTransactionsObj);
                            }
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.updateEmailDetails EApp Transaction Object "
                                    + jsonTransactionsObj.toString());
                        } else if (ILLUSTRATION.equals(jsonTransactionsObj.getString(TYPE))) {
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                lifeEngageEmail = saveLifeEngageEmail(jsonTransactionsObj);
                            }
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.updateEmailDetails illustration Transaction Object "
                                    + jsonTransactionsObj.toString());
                        }

                        if (lifeEngageEmail != null) {
                            transactions.setLifeEngageEmail(lifeEngageEmail);
                            transactions.setAgentId(agentId);
                            transactions.setStatus(INITIALISED);
                            transactions.setType(lifeEngageEmail.getType());
                            transactions.setKey3(jsonTransactionsObj.getString(KEY_3));
                            transactions.setKey5(jsonTransactionsObj.getString(KEY_5));
                            transactions.setKey5(jsonTransactionsObj.getString(KEY_6));
                            successMessage = Constants.EMAIL_DETAILS_SAVED;
                            final StatusData statusData = new StatusData();
                            statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
                            statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE);
                            statusData.setStatusMessage(successMessage);
                            transactions.setStatusData(statusData);
                            transactions.setStatus(LifeEngageComponentHelper.SUCCESS);
                            transactionsList.add(transactions);
                        } else {
                            throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, Constants.EMAIL_DETAILS_MISSING);
                        }

                    } catch (SystemException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (Exception e) {
            LOGGER.error("Unable to get email details :" + e.getMessage());
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

        } finally {
            response = eMailUpdateHolder.parseBO(transactionsList);

        }

        return response;

    }

    /**
     * Save life engage email.
     * 
     * @param jsonObj
     *            the json obj
     * @return the life engage email
     * @throws ParseException
     *             the parse exception
     */
    protected LifeEngageEmail saveLifeEngageEmail(final JSONObject jsonObj) throws ParseException {
        final JSONObject emailObject = new JSONObject();
        final JSONObject emailTemplateObj = new JSONObject();
        LifeEngageEmail lifeEngagemail = new LifeEngageEmail();

        String agentId = (String) jsonObj.getString(Constants.KEY11);

        final JSONObject emailObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(EMAIL);
        String toMailIds = (String) emailObj.getString(EMAIL_ID);
        String ccMailIds = (String) emailObj.getString(CC_MAIL_ID);
        String fromMailId = (String) emailObj.getString(FROM_MAIL_ID);

        String templateId = "";
        String type = "";
        String id = null;
        String mailSubject = "";
        String pdfName = "";
        JSONObject productObj = new JSONObject();

        if (E_APP.equals(jsonObj.getString(TYPE))) {
            productObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(PRODUCT);
            templateId = "321";
            type = E_APP;
            id = (String) jsonObj.getString(KEY_4);
            mailSubject = getMailSubject(productObj);
        } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
            productObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(PRODUCT);
            templateId = "430";
            type = ILLUSTRATION;
            id = (String) jsonObj.getString(KEY_3);
            mailSubject = getMailSubject(productObj);
        }

        emailObject.put(MAIL_SUBJECT, mailSubject);
        emailObject.put(TEMPLATE_ID, templateId);
        emailObject.put(TYPE, type);
        emailObject.put(EMAIL_ID, toMailIds);
        emailObject.put(CC_MAIL_ID, ccMailIds);
        emailObject.put(FROM_MAIL_ID, fromMailId);
        emailObject.put(AGENT_ID, agentId);

        if (!id.isEmpty()) {
            emailTemplateObj.put(ID, id);
            emailObject.put(ID, id);
            emailObject.put(EMAIL_TEMPLATE, emailTemplateObj);

            LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
            lifeEngageEmail.setSendTime(LifeEngageComponentHelper.getCurrentdate());
            lifeEngageEmail.setEmailValues(emailObject.toString());
            lifeEngageEmail.setAttemptNumber("1");
            lifeEngageEmail.setStatus(INITIALISED);
            lifeEngageEmail.setAgentId(agentId);
            lifeEngageEmail.setType(type);

            lifeEngagemail = emailRepository.savelifeEngageEmail(lifeEngageEmail);
        }

        return lifeEngagemail;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageEmailComponent#sendEmailWithAttachment(java.lang.String, java.lang.String,
     * org.json.JSONArray)
     */  
    public final StatusData sendEmailWithAttachment(final String json, final String emailBody, final JSONArray jsonEmailAttachmentsArr) throws BusinessException{
		
	    Properties mailProperties = null;
        String emailStatus = Constants.SUCCESS;
        final StatusData statusData = new StatusData();
        statusData.setStatus(emailStatus);

        try {
        	mailProperties = getMailProperties();

        	final Session session = Session.getDefaultInstance(mailProperties);
          
            final MimeMessage mimeMessage = new MimeMessage(session);
            final MimeBodyPart messagePart = new MimeBodyPart();
            final Multipart multiPart = new MimeMultipart();
         
            final JSONObject emailObj = new JSONObject(json);
            
            String ccedMailId = "";
            String toEmailId = "";
            final String subject = emailObj.getString(MAIL_SUBJECT);
            JSONArray jsonEmailToArray = emailObj.getJSONArray(EMAIL_ID); 
	    	
	    	for (int i=0; i < jsonEmailToArray.length(); i++){ 
	    		toEmailId += jsonEmailToArray.get(i) + ",";
			   } 
	    	
	    	JSONArray jsonEmailCCArray = emailObj.getJSONArray(CC_MAIL_ID); 
	    	
	    	if(jsonEmailCCArray.length() > 0){
	    		ccedMailId = (String)jsonEmailCCArray.get(0);
	    	}
	    	
	    	setMimeMessageEmailDetails(mimeMessage, toEmailId, ccedMailId, subject);
            
            messagePart.setContent(emailBody, EMAIL_MSG_CONTENT);
            multiPart.addBodyPart(messagePart);
            
	            for (int i = 0; i < jsonEmailAttachmentsArr.length(); i++) {
	            	
	                final JSONObject jsonObj = jsonEmailAttachmentsArr.getJSONObject(i);
	                String fileName = (String)jsonObj.getString(FILE_NAME);
	                String base64Value = (String)jsonObj.getString(BASE64_VALUE);
	                
				    multiPart.addBodyPart(getMimeBodyPartBase64(fileName, base64Value));
				    
	            }
	            
	        mimeMessage.setContent(multiPart);
	        
            Transport.send(mimeMessage);
            
        } catch (ParseException e) {
    		statusData.setStatus(Constants.FAILURE);
    		statusData.setStatusMessage(e.getMessage());
        } catch (MessagingException e) {
        	statusData.setStatus(Constants.FAILURE);
    		statusData.setStatusMessage(e.getMessage());
        }catch (Exception e) {
        	statusData.setStatus(Constants.FAILURE);
    		statusData.setStatusMessage(e.getMessage());
        }
        return statusData;
}

	/**
	 * Sets the MimeMessageEmailDetails.
	 * 
	 * @param MimeMessage
	 *            the mimeMessage
	 * @param toEmailId
	 *            the toEmailId
	 * @param ccedMailId
	 *            the ccedMailId
	 * @param subject
	 *            the subject            
	 */
	private void setMimeMessageEmailDetails(final MimeMessage mimeMessage, final String toEmailId, final String ccedMailId, final String subject)
									throws AddressException, MessagingException {
		
		final InternetAddress fromInternetAddress = new InternetAddress(fromMailId);
	    
	    mimeMessage.setFrom(fromInternetAddress);
	    mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmailId));
	    mimeMessage.setRecipients(Message.RecipientType.CC,InternetAddress.parse(ccedMailId));
	    mimeMessage.setSubject(subject);
	    mimeMessage.setSentDate(new Date());
	    
	}
	
	/**
	 * Creates the attachmentPart.
	 * 
	 * @param fileName
	 *            the fileName
	 * @param base64Value
	 *            the base64Value
	 * @return the attachmentPart
	 */
	private MimeBodyPart getMimeBodyPartBase64(final String fileName, final String base64Value) throws MessagingException{
		
		 final MimeBodyPart attachmentPart = new MimeBodyPart();
		    
		 byte[] imageBytes = Base64.decodeBase64(base64Value);
		 DataSource dataSource = new ByteArrayDataSource(imageBytes, APPLICATION_ALL_TYPE);
		 attachmentPart.setDataHandler(new DataHandler(dataSource));
		 attachmentPart.setFileName(fileName);
	    return attachmentPart;
	}
	
	 /**
	 * Creates the mailProperties
	 * 
	 * @return the mailProperties
	 */
	private Properties getMailProperties() {
		final Properties mailProperties = new Properties();
		mailProperties.put(mailAuth, mailAuthValue);
		mailProperties.put(mailLTS, mailLTSEnable);
		mailProperties.put(mailHostKey, mailHost);
		mailProperties.put(mailPortName, mailPort);
	    return mailProperties;
	}

    /**
     * Gets the mail subject.
     * 
     * @param productObj
     *            the product obj
     * @return the mail subject
     */
    public String getMailSubject(final JSONObject productObj) {
        final String premium = productObj.getJSONObject(POLICY_DETAILS).getString(COMMITTED_PREMIUM);
        final String productName = productObj.getJSONObject(PRODUCT_DETAILS).getString(PRODUCT_NAME);
        final String mailSubject = productName + "_" + premium;
        return mailSubject;
    }

    /**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }	
	
}
