/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Mar 14, 2013
 */
package com.cognizant.insurance.domain.codes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * The Class class CodeLocaleLookUp.
 */

@Entity
@Table(name = "CODE_LOCALE_LOOKUP")
public class CodeLocaleLookUp {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5263482274166191172L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /** The look up code. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JoinColumn(name = "LOOKUPCODE_ID")
    private CodeLookUp lookUpCode;

    /** The language. */
    @Column(name = "LANGUAGE")
    private String language;

    /** The country. */
    @Column(name = "COUNTRY")
    private String country;

    /** The value. */
    @Column(name = "VALUE",columnDefinition="nvarchar(200)")
    private String value;

    /**
     * Gets the id.
     * 
     * @return Returns the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets The id.
     * 
     * @param id
     *            The id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the lookUpCode.
     * 
     * @return Returns the lookUpCode.
     */
    public CodeLookUp getLookUpCode() {
        return lookUpCode;
    }

    /**
     * Sets The lookUpCode.
     * 
     * @param lookUpCode
     *            The lookUpCode to set.
     */
    public void setLookUpCode(CodeLookUp lookUpCode) {
        this.lookUpCode = lookUpCode;
    }

    /**
     * Gets the language.
     * 
     * @return Returns the language.
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets The language.
     * 
     * @param language
     *            The language to set.
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Gets the country.
     * 
     * @return Returns the country.
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets The country.
     * 
     * @param country
     *            The country to set.
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets the value.
     * 
     * @return Returns the value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets The value.
     * 
     * @param value
     *            The value to set.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LocaleLookUp [lookUpCode=" + lookUpCode + ", language=" + language + ", country=" + country
                + ", value=" + value + "]";
    }

}
