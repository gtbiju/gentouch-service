package com.cognizant.insurance.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cognizant.insurance.domain.codes.CodeLocaleLookUp;
import com.cognizant.insurance.domain.codes.CodeLookUp;
import com.cognizant.insurance.service.repository.CodeLookUpRepository;

public interface GeneraliCodeLookUpRepository extends CodeLookUpRepository,JpaRepository<CodeLookUp, Long> {

	@Query("select localeLook from CodeLookUp cl JOIN cl.localeLookUps localeLook  where cl.code = :code and localeLook.language= :language and localeLook.lookUpCode.codeType.name= :typeName")
	 public  CodeLocaleLookUp findByCodeAndType(@Param("code")String code,@Param("language")String language,@Param("typeName") String typeName);
}
