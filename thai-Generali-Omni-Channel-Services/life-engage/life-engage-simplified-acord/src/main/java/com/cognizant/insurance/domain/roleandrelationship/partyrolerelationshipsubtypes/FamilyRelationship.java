/**
 * 
 */
package com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import com.cognizant.insurance.domain.roleandrelationship.PartyRoleRelationship;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.FamilyMember;

/**
 * This represents a relationship between family members. The family members may
 * or may not reside in the same household.
 * 
 * e.g. Upon reaching adulthood, a daughter may have moved out of her parents'
 * home, but that does not change the fact that she remains their daughter. <!--
 * end-UML-doc -->
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class FamilyRelationship extends PartyRoleRelationship {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 188977401740964471L;
    
    /**
     * This relationship links related family members to one another.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "FAMILYMEMBER_FAMILYRELATIONSHIP",
            joinColumns = { @JoinColumn(name = "FAMILYRELATIONSHIP_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "FAMILYMEMBER_ID", referencedColumnName = "Id") })
    private Set<FamilyMember> familyMember;

    /**
     * Gets the familyMember.
     * 
     * @return Returns the familyMember.
     */
    public Set<FamilyMember> getFamilyMember() {
        return familyMember;
    }

    /**
     * Sets The familyMember.
     * 
     * @param familyMember
     *            The familyMember to set.
     */
    public void setFamilyMember(final Set<FamilyMember> familyMember) {
        this.familyMember = familyMember;
    }

}
