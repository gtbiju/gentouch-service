package com.cognizant.insurance.omnichannel.component;

import java.util.Set;

import org.json.JSONObject;

import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;

public interface GeneraliBPMEmailPdfComponent {

	public void saveGeneraliEmail(JSONObject requestJson);

	public Set<AgreementDocument> generateBPMPdf(JSONObject requestJson);

	public void triggerEmail(String agentId, String type);

	public void triggerBPMRequest(String eAppId);

	public void saveBPMPdf(Set<AgreementDocument> agreementDocuments,
			String eAppId);
	public void updateEappBPMStatus(String eAppId,String status,String emailFlag);

}
