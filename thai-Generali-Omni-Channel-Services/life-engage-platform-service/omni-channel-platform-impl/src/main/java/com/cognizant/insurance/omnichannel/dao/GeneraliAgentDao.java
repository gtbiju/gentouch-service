package com.cognizant.insurance.omnichannel.dao;

import java.util.List;

import com.cognizant.insurance.agent.AgentAchievement;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliAgentAddress;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.generic.dao.AgentDao;
import com.cognizant.insurance.request.Request;

/**
 * The Interface GeneraliAgentDao.
 */
public interface GeneraliAgentDao extends AgentDao{ 

	/**
	 * Retrieve agent by code.
	 *
	 * @param request the request
	 * @return the agent
	 */
	GeneraliAgent retrieveAgentProfileByCode(Request<GeneraliAgent> request);
	
	List<Customer> retrieveLeads(Request<Customer> request);

    List<ProductSpecification> retrieveAgentProductByCode(Request<ProductSpecification> agentProductRequest);
    
    List<GeneraliAgent> retrieveAgentProfileForReassign(Request<GeneraliAgent> request);
    
    List<GeneraliAgent> retrieveActiveAgentProfileToReassign(Request<GeneraliAgent> request);

	Integer retrieveIllustrationCount(Request<Customer> request);

	Integer retrieveEappCount(Request<Customer> request);

	Integer retrieveFnaCount(Request<Customer> request);
	
	String retrieveLeadIdForFna(Request<Customer> request);
	
	List<GoalAndNeed> retrieveGoalAndNeed(Request<GoalAndNeed> request);
	
	List<Agreement> retrieveAgreement(Request<InsuranceAgreement> request);
	
	List<AgentAchievement> retrieveAgentAchievements(Request<AgentAchievement> agentAchievementRequest);
	
	List<InsuranceAgreement> retrieveIllustration(Request<InsuranceAgreement> request);

 	GeneraliGAOAgent retrieveGAOAgentProfileByCode(Request<GeneraliGAOAgent> request);

	GeneraliAgentAddress retrieveGAOAddress(Request<GeneraliAgentAddress> request);
    
}
