package com.cognizant.insurance.omnichannel.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class AgentData implements Comparable<AgentData> {
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AgentData [agentCode=" + agentCode + ", contofClosedleads="
				+ contofClosedleads + ", countofTotalLeads="
				+ countofTotalLeads + ", YTD=" + YTD + ", startDate="
				+ startDate + ", currentDate=" + currentDate + ", agentName="
				+ agentName + "]";
	}

	private String agentCode;
	
	private Long contofClosedleads;
	
	private Long countofTotalLeads;
	
	private BigDecimal YTD;
	
	private Date startDate;
	
	private Date currentDate;
	
	private String agentName;
	
	private Map<String,BankBranch> mappedBranches;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	

	public BigDecimal getYTD() {
		return YTD;
	}

	public void setYTD(BigDecimal yTD) {
		YTD = yTD;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentName() {
		return agentName;
	}

	public long getContofClosedleads() {
		return contofClosedleads;
	}

	public void setContofClosedleads(long contofClosedleads) {
		this.contofClosedleads = contofClosedleads;
	}

	public long getCountofTotalLeads() {
		return countofTotalLeads;
	}

	public void setCountofTotalLeads(long countofTotalLeads) {
		this.countofTotalLeads = countofTotalLeads;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((YTD == null) ? 0 : YTD.hashCode());
		result = prime * result
				+ ((agentCode == null) ? 0 : agentCode.hashCode());
		result = prime * result
				+ ((agentName == null) ? 0 : agentName.hashCode());
		result = prime
				* result
				+ ((contofClosedleads == null) ? 0 : contofClosedleads
						.hashCode());
		result = prime
				* result
				+ ((countofTotalLeads == null) ? 0 : countofTotalLeads
						.hashCode());
		result = prime * result
				+ ((currentDate == null) ? 0 : currentDate.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgentData other = (AgentData) obj;
		if (YTD == null) {
			if (other.YTD != null)
				return false;
		} else if (!YTD.equals(other.YTD))
			return false;
		if (agentCode == null) {
			if (other.agentCode != null)
				return false;
		} else if (!agentCode.equals(other.agentCode))
			return false;
		if (agentName == null) {
			if (other.agentName != null)
				return false;
		} else if (!agentName.equals(other.agentName))
			return false;
		if (contofClosedleads == null) {
			if (other.contofClosedleads != null)
				return false;
		} else if (!contofClosedleads.equals(other.contofClosedleads))
			return false;
		if (countofTotalLeads == null) {
			if (other.countofTotalLeads != null)
				return false;
		} else if (!countofTotalLeads.equals(other.countofTotalLeads))
			return false;
		if (currentDate == null) {
			if (other.currentDate != null)
				return false;
		} else if (!currentDate.equals(other.currentDate))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}

	@Override
	public int compareTo(AgentData o) {
		// TODO Auto-generated method stub
		return o.YTD.compareTo(YTD);
	}

	/**
	 * @param mappedBranches the mappedBranches to set
	 */
	public void setMappedBranches(Map<String,BankBranch> mappedBranches) {
		this.mappedBranches = mappedBranches;
	}

	/**
	 * @return the mappedBranches
	 */
	public Map<String,BankBranch> getMappedBranches() {
		return mappedBranches;
	}


	

}
