/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

// TODO: Auto-generated Javadoc
/**
 * A code list representing various types of payment frequency.
 * 
 * @author 301350
 * 
 * 
 */
public enum PaymentFrequencyCodeList {
    /**
     * Used in Product Definitions only.
     * 
     * 
     * 
     */
    Anypaymentmodeisallowed,
    /**
     * Assumes fiscal date is based off start date.
     * 
     * 
     * 
     */
    FiscalQuarterlymodeofpayment,
    /**
     * Policy is paid on a quarterly basis with first payment being paid at
     * beginning of billing cycle, and final payment being paid at end of
     * billing cycle.
     * 
     * 
     * 
     */
    Paymentmade5timesayear,
    
    /** The Paymentsmade7timesperyear. */
    Paymentsmade7timesperyear,
    
    /** The Paymentsmade28timesperyear. */
    Paymentsmade28timesperyear,
    
    /** The Paymentsmade11timesperyear. */
    Paymentsmade11timesperyear,
    
    /** The Paymentsmade14timesperyear. */
    Paymentsmade14timesperyear,
    
    /** The Single payment. */
    SinglePayment,
    
    /** The Annual. */
    Annual,
    
    /** The Annually. */
    Annually,
    
    /** The Quarterly. */
    Quarterly,
    
    /** The Half yearly. */
    HalfYearly,
    
    /** The Monthly. */
    Monthly,
    
    /** The Accumulated. */
    Accumulated,
    
    /** The Yearly. */
    Yearly,
    
    /** The Not mentioned. */
    NotMentioned, 
    
    SemiAnnual,
    SemiAnnually,
    Single,
    Semester,
    
    /** The Half yearly for Vietnam Illustration. */
    Halfyearly;
    
    
}
