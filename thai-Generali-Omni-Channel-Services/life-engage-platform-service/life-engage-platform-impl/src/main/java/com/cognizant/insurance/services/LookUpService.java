/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * The Interface interface LookUpService.
 */
public interface LookUpService {

    /**
     * Gets the Country-State-City details.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String getCountrySpecificDetails(String json) throws BusinessException;

    /**
     * Gets the list of Countries, States or Cities.
     * 
     * @param json
     *            the json
     * @return the list of Countries, States or Cities.
     * @throws BusinessException
     *             the business exception
     */
    String getCountryOrCountrySubDivisionList(String json) throws BusinessException;;
}
