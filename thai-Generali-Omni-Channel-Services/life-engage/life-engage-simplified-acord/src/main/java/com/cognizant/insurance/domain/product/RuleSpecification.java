/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.domain.product;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Class RuleSpecification.
 *
 * @author 304007,420118
 */

@Entity
@Table(name = "CORE_PROD_RULE_SPEC")
public class RuleSpecification extends Component {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The name. */
    @Column(name="NAME")
    private String name;   

    /** The input parameters. */
    @Column(name="INPUT_PARAMETERS", columnDefinition = "varchar(max)")
    private String inputParameters;

    /** The output parameters. */
    @Column(name="OUTPUT_PARAMETERS", columnDefinition = "varchar(max)")
    private String outputParameters;

    /** The canbe overwritten indicator. */
    @Column(name="CANBE_OVERWRITTEN_INDICATOR")
    private Boolean canbeOverwrittenIndicator;

    /** The comment on failure. */
    @Column(name="COMMENT_ON_FAILURE")
    private String commentOnFailure;

    /** The actual rule ref. */
    @Column(name="ACTUAL_RULE_REF")
    private String actualRuleRef;
    
    /** The parent product. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "parentProduct_id")
    private ProductSpecification parentProduct;
    
    /** The rule type. */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "ruleType_ID")
    private RuleType ruleType;
    
    /** Group Name**/
    @Column(name = "GROUP_NAME")
    private String groupName;
    
    /** The basic data complete code. */
    @Column(name= "basicDataCompleteCode")
    private Integer basicDataCompleteCode;
    
    /** The context id. */
    @Column(name= "contextId")
    private Integer contextId;
    
    /** The creation date time. */
    @Column(name= "creationDateTime")
    private Date creationDateTime;
    
    /** The modified date time. */
    @Column(name= "modifiedDateTime")
    private Date modifiedDateTime;
    
    /** The trans tracking id. */
    @Column(name= "transTrackingId")
    private String transTrackingId;
    
    /** The transaction id. */
    @Column(name= "transactionId")
    private String transactionId;
    
    /** The type name. */
    @Column(name= "typeName")
    private String typeName;
    
    /** The transaction keys_id. */
    @Column(name= "transactionKeys_id")
    private Long transactionKeys_id;

    /**
     * The Constructor.
     */
    public RuleSpecification() {
        super();
    }

    /**
     * The Constructor.
     *
     * @return the input parameters
     */
    
    public String getInputParameters() {
        return inputParameters;
    }

    /**
     * Sets the input parameters.
     *
     * @param inputParameters the input parameters
     */
    public void setInputParameters(final String inputParameters) {
        this.inputParameters = inputParameters;
    }

    /**
     * Gets the output parameters.
     *
     * @return the output parameters
     */
    public String getOutputParameters() {
        return outputParameters;
    }

    /**
     * Sets the output parameters.
     *
     * @param outputParameters the output parameters
     */
    public void setOutputParameters(final String outputParameters) {
        this.outputParameters = outputParameters;
    }

    /**
     * Gets the canbe overwritten indicator.
     *
     * @return the canbe overwritten indicator
     */
    public Boolean getCanbeOverwrittenIndicator() {
        return canbeOverwrittenIndicator;
    }

    /**
     * Sets the canbe overwritten indicator.
     *
     * @param canbeOverwrittenIndicator the canbe overwritten indicator
     */
    public void setCanbeOverwrittenIndicator(final Boolean canbeOverwrittenIndicator) {
        this.canbeOverwrittenIndicator = canbeOverwrittenIndicator;
    }

    /**
     * Gets the comment on failure.
     *
     * @return the comment on failure
     */
    public String getCommentOnFailure() {
        return commentOnFailure;
    }

    /**
     * Sets the comment on failure.
     *
     * @param commentOnFailure the comment on failure
     */
    public void setCommentOnFailure(final String commentOnFailure) {
        this.commentOnFailure = commentOnFailure;
    }

    /**
     * Gets the actual rule ref.
     *
     * @return the actual rule ref
     */
    public String getActualRuleRef() {
        return actualRuleRef;
    }

    /**
     * Sets the actual rule ref.
     *
     * @param actualRuleRef the actual rule ref
     */
    public void setActualRuleRef(final String actualRuleRef) {
        this.actualRuleRef = actualRuleRef;
    }

    /**
     * Gets the name.
     *
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The name.
     *
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the parentProduct.
     *
     * @return Returns the parentProduct.
     */
    public ProductSpecification getParentProduct() {
        return parentProduct;
    }

    /**
     * Sets The parentProduct.
     *
     * @param parentProduct The parentProduct to set.
     */
    public void setParentProduct(final ProductSpecification parentProduct) {
        this.parentProduct = parentProduct;
    }

    /**
     * Gets the ruleType.
     *
     * @return Returns the ruleType.
     */
    public RuleType getRuleType() {
        return ruleType;
    }

    /**
     * Sets The ruleType.
     *
     * @param ruleType The ruleType to set.
     */
    public void setRuleType(final RuleType ruleType) {
        this.ruleType = ruleType;
    }

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
    
    /**
     * Gets the basic data complete code.
     *
     * @return the basic data complete code
     */
    public Integer getBasicDataCompleteCode() {
        return basicDataCompleteCode;
    }


    /**
     * Sets the basic data complete code.
     *
     * @param basicDataCompleteCode the basic data complete code to set.
     */
    public void setBasicDataCompleteCode(Integer basicDataCompleteCode) {
        this.basicDataCompleteCode = basicDataCompleteCode;
    }


    /**
     * Gets the context id.
     *
     * @return the context id
     */
    public Integer getContextId() {
        return contextId;
    }


    /**
     * Sets the context id.
     *
     * @param contextId the context id to set.
     */
    public void setContextId(Integer contextId) {
        this.contextId = contextId;
    }


    /**
     * Gets the creation date time.
     *
     * @return the creation date time
     */
    public Date getCreationDateTime() {
        return creationDateTime;
    }


    /**
     * Sets the creation date time.
     *
     * @param creationDateTime the creation date time to set.
     */
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }


    /**
     * Gets the modified date time.
     *
     * @return the modified date time
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }


    /**
     * Sets the modified date time.
     *
     * @param modifiedDateTime the modified date time to set.
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }


    /**
     * Gets the trans tracking id.
     *
     * @return the trans tracking id
     */
    public String getTransTrackingId() {
        return transTrackingId;
    }


    /**
     * Sets the trans tracking id.
     *
     * @param transTrackingId the trans tracking id to set.
     */
    public void setTransTrackingId(String transTrackingId) {
        this.transTrackingId = transTrackingId;
    }


    /**
     * Gets the transaction id.
     *
     * @return the transaction id
     */
    public String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transaction id.
     *
     * @param transactionId the transaction id to set.
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the type name.
     *
     * @return the type name
     */
    public String getTypeName() {
        return typeName;
    }


    /**
     * Sets the type name.
     *
     * @param typeName the type name to set.
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


    /**
     * Gets the transaction keys_id.
     *
     * @return the transaction keys_id
     */
    public Long getTransactionKeys_id() {
        return transactionKeys_id;
    }


    /**
     * Sets the transaction keys_id.
     *
     * @param transactionKeys_id the transaction keys_id to set.
     */
    public void setTransactionKeys_id(Long transactionKeys_id) {
        this.transactionKeys_id = transactionKeys_id;
    }
    
}