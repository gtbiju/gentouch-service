/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.generic.dao.CountryDetailsDao;
import com.cognizant.insurance.lookup.CountryDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.response.vo.CountryLookUpResponse;

/**
 * The Class class CountryDetailsDaoImpl.
 *
 * @author 300797
 */
public class CountryDetailsDaoImpl extends DaoImpl implements CountryDetailsDao {
 
    
    /* (non-Javadoc)
     * @see com.cognizant.insurance.generic.dao.CountryDetailsDao#retrieve(com.cognizant.insurance.request.Request)
     */
    public Response<CountryLookUpResponse> retrieve(Request<CountryDetails> request){
        
        CountryDetails countryDetails = request.getType();
        String query = "select st from CountryDetails st where st.parentID =?1 and st.subDivisionType='State'";
        List<Object>  stateList = findUsingQuery(request,query,countryDetails.getIdentifier());        
        Map<String, List<String>> stateCityMap = new HashMap<String, List<String>>();
        query = "select ct.name from CountryDetails ct where ct.parentID =?1 and ct.subDivisionType='City'";
        for (Object state : stateList) {
            CountryDetails stateObj = (CountryDetails)state; 
            List<String>  cityList = executeQuery(request,query,stateObj.getIdentifier());      
            stateCityMap.put(stateObj.getName(), cityList);
        }
        
        CountryLookUpResponse countryLookUpResponse = new CountryLookUpResponse();
        countryLookUpResponse.setName(countryDetails.getName());
        countryLookUpResponse.setStateCityMap(stateCityMap);
        
        final Response<CountryLookUpResponse> response = new ResponseImpl<CountryLookUpResponse>();
        response.setType(countryLookUpResponse);
        
        return response;
    }

  
    /* (non-Javadoc)
     * @see com.cognizant.insurance.generic.dao.CountryDetailsDao#retrieveCountryOrCountrySubDivisionList(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<CountryLookUpResponse>retrieveCountryOrCountrySubDivisionList(Request<CountryDetails> request) {
        
        CountryDetails countryDetails = request.getType();
        String query = "select st from CountryDetails st where st.subDivisionType=?1 and st.parentID=?2";
        List<Object>  countryList = findUsingQuery(request,query,countryDetails.getType(),countryDetails.getParentID());
        Map<String, String> countryMap = new HashMap<String, String>();
        if ( countryList.size() >0 ) {
            for (Object object : countryList) {
                CountryDetails countryObj = (CountryDetails)object; 
                countryMap.put(countryObj.getIdentifier().toString(), countryObj.getName());           
            }
        }
        CountryLookUpResponse countryLookUpResponse = new CountryLookUpResponse();
        countryLookUpResponse.setCountryStateOrCityMap(countryMap);

        final Response<CountryLookUpResponse> response = new ResponseImpl<CountryLookUpResponse>();
        response.setType(countryLookUpResponse);
        
        return response;
    }
    
    /**
     * Find using query.
     *
     * @param request the request
     * @param queryString the query string
     * @param params the params
     * @return the list
     */
    private List<Object> findUsingQuery(Request request, String queryString,  Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return  (List<Object>)query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * Execute query.
     *
     * @param request the request
     * @param queryString the query string
     * @param params the params
     * @return the list
     */
    private List<String> executeQuery(Request request, String queryString,  Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return  (List<String>)query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

}
