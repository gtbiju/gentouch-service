/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Dec 13, 2013
 */
package com.cognizant.insurance.domain.product;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class class ProductFunds.
 * 
 * @author 300797
 */

@Entity
@Table(name = "CORE_PROD_FUNDS")
public class ProductFunds implements Serializable {
	
	private static final long serialVersionUID = 5906813451812031347L;
	
    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    /** The fund name. */
    @Column(name = "FUND_NAME")
    private String fundName;

    /** The fund category type1. */
    @ManyToOne(cascade = { CascadeType.REFRESH })
    @JoinColumn(name = "FUND_CATEGORY1_ID")
    private FundCategoryType fundCategoryType1;

    /** The fund category type2. */
    @ManyToOne(cascade = { CascadeType.REFRESH })
    @JoinColumn(name = "FUND_CATEGORY2_ID")
    private FundCategoryType fundCategoryType2;

    /** The child products. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, mappedBy = "productFunds")
    private Set<ProductFundMapping> productFundMappings;

    /** The fund name. */
    @Column(name = "FUND_CODE")
    private String fundCode;
    
    /** The carrier. imp */
    @ManyToOne(cascade = { CascadeType.REFRESH })
    @JoinColumn(name = "CARRIER_ID")
    private Carrier carrier;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the fund name.
     * 
     * @return the fundName
     */
    public String getFundName() {
        return fundName;
    }

    /**
     * Sets the fund name.
     * 
     * @param fundName
     *            the fundName to set
     */
    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    /**
     * Gets the fund category type1.
     * 
     * @return the fundCategoryType1
     */
    public FundCategoryType getFundCategoryType1() {
        return fundCategoryType1;
    }

    /**
     * Sets the fund category type1.
     * 
     * @param fundCategoryType1
     *            the fundCategoryType1 to set
     */
    public void setFundCategoryType1(FundCategoryType fundCategoryType1) {
        this.fundCategoryType1 = fundCategoryType1;
    }

    /**
     * Gets the fund category type2.
     * 
     * @return the fundCategoryType2
     */
    public FundCategoryType getFundCategoryType2() {
        return fundCategoryType2;
    }

    /**
     * Sets the fund category type2.
     * 
     * @param fundCategoryType2
     *            the fundCategoryType2 to set
     */
    public void setFundCategoryType2(FundCategoryType fundCategoryType2) {
        this.fundCategoryType2 = fundCategoryType2;
    }

    /**
     * Gets the product fund mappings.
     * 
     * @return the productFundMappings
     */
    public Set<ProductFundMapping> getProductFundMappings() {
        return productFundMappings;
    }

    /**
     * Sets the product fund mappings.
     * 
     * @param productFundMappings
     *            the productFundMappings to set
     */
    public void setProductFundMappings(Set<ProductFundMapping> productFundMappings) {
        this.productFundMappings = productFundMappings;
    }

	/**
	 * @return the fundCode
	 */
	public String getFundCode() {
		return fundCode;
	}

	/**
	 * @param fundCode the fundCode to set
	 */
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	
	
	/**
	 * Gets the carrier.
	 *
	 * @return the carrier
	 */
	public Carrier getCarrier() {
		return carrier;
	}

	/**
	 * Sets the carrier.
	 *
	 * @param carrier the carrier to set
	 */
	public void setCarrier(Carrier carrier) {
		this.carrier = carrier;
	}

}
