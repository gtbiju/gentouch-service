/**
 * 
 */
package com.cognizant.insurance.omnichannel.dao;

import java.util.Date;
import java.util.List;

import com.cognizant.insurance.dao.Dao;
import com.cognizant.insurance.omnichannel.domain.CheckinData;
import com.cognizant.insurance.omnichannel.domain.CheckinData.Status;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;

/**
 * @author 397850
 *
 */
public interface CheckedinDetailsDoa extends Dao {

	CheckinData retriveCheckedinDetails(Request<CheckinData> checkedinRequest, String agentId, Status status);

	void update(Request<CheckinData> checkedinRequest, CheckinData retirevedData);

	List<CheckinData> retireveCheckinList(Request<CheckinData> checkedinRequest,
			Status status, Request<List<String>> statusRequest);

	void updateCheckinDetails(Request<CheckinData> checkedinRequest, Date date);


}
