/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */
package com.cognizant.insurance.agent.searchcriteria;

import java.io.Serializable;

import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.ContextTypeCodeList;

/**
 * The Class class GoalAndNeedSearchCriteria.
 */
public class GoalAndNeedSearchCriteria implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 482345960501742672L;

    /** The id. */
    private Long id;

    /** The context id. */
    private ContextTypeCodeList contextId;

    /** The transaction id. */
    private String transactionId;

    /**
     * Sets The id.
     * 
     * @param id
     *            The id to set.
     */
    public final void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the id.
     * 
     * @return Returns the id.
     */
    public final Long getId() {
        return id;
    }

    /**
     * Gets the contextId.
     *
     * @return Returns the contextId.
     */
    public final ContextTypeCodeList getContextId() {
        return contextId;
    }

    /**
     * Sets The contextId.
     *
     * @param contextId The contextId to set.
     */
    public final void setContextId(ContextTypeCodeList contextId) {
        this.contextId = contextId;
    }

    /**
     * Gets the transactionId.
     *
     * @return Returns the transactionId.
     */
    public final String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets The transactionId.
     *
     * @param transactionId The transactionId to set.
     */
    public final void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
