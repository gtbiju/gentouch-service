/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.contactpreferencedetails;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.DayOfWeekCodeList;

/**
 * The preferred times and days of contact.
 * 
 * e.g: Robert Paul prefers telephone contact on weekdays between 20:00 and
 * 21:00.
 * 
 * @author 301350
 * 
 * 
 */

public class TimingPreference extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1657744835803860575L;

    /**
     * The day of the week that starts the timing preference period.
     * 
     * e.g: Monday
     * 
     * 
     * 
     */
    
    private DayOfWeekCodeList startDayOfWeekCode;

    /**
     * The day of the week that ends the timing preference period.
     * 
     * e.g: Friday
     * 
     * 
     * 
     */
    
    private DayOfWeekCodeList endDayOfWeekCode;

    /**
     * The earliest time for a contact to start within the period delimited by
     * the start and end days of the week.
     * 
     * 
     * 
     */
    private TimePeriod startTime;

    /**
     * The latest time for a contact to end within the period delimited by the
     * start and end days of the week. 
     * 
     * 
     * 
     */
    private TimePeriod endTime;

    /**
     * @return the endDayOfWeekCode
     * 
     * 
     */
    public DayOfWeekCodeList getEndDayOfWeekCode() {
        return endDayOfWeekCode;
    }

    /**
     * @return the endTime
     * 
     * 
     */
    public TimePeriod getEndTime() {
        return endTime;
    }

    /**
     * @return the startDayOfWeekCode
     * 
     * 
     */
    public DayOfWeekCodeList getStartDayOfWeekCode() {
        return startDayOfWeekCode;
    }

    /**
     * @return the startTime
     * 
     * 
     */
    public TimePeriod getStartTime() {
        return startTime;
    }

    /**
     * @param endDayOfWeekCode
     *            the endDayOfWeekCode to set
     * 
     * 
     */
    public void setEndDayOfWeekCode(
            final DayOfWeekCodeList endDayOfWeekCode) {
        this.endDayOfWeekCode = endDayOfWeekCode;
    }

    /**
     * @param endTime
     *            the endTime to set
     * 
     * 
     */
    public void setEndTime(final TimePeriod endTime) {
        this.endTime = endTime;
    }

    /**
     * @param startDayOfWeekCode
     *            the startDayOfWeekCode to set
     * 
     * 
     */
    public void setStartDayOfWeekCode(
            final DayOfWeekCodeList startDayOfWeekCode) {
        this.startDayOfWeekCode = startDayOfWeekCode;
    }

    /**
     * @param startTime
     *            the startTime to set
     * 
     * 
     */
    public void setStartTime(final TimePeriod startTime) {
        this.startTime = startTime;
    }
}
