package com.cognizant.insurance.omnichannel.component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Set;

import com.cognizant.insurance.component.LifeEngageDocumentComponent;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.omnichannel.vo.GeneraliTransactions;
import com.lowagie.text.DocumentException;

public interface GeneraliDocumentComponent extends LifeEngageDocumentComponent {

    //public void generateBPMPdf(String inputJson);

    public Set<AgreementDocument> getBase64DocumentDetils(String proposalNumber);
    
    public Set<AgreementDocument> getAdditionalInsuredPdf(String proposalNumber, String type, String templateId);

    public List<ByteArrayOutputStream> generateMemoPDF(String applicationNumber, boolean isMailPDF) throws BusinessException;

    void doMerge(List<InputStream> list, OutputStream outputStream) throws DocumentException, IOException;
    
    ByteArrayOutputStream generateEncryptedPdf(String proposalnumber, String type,String templateId, String masterPassword);

	ByteArrayOutputStream generateCommonMemoPDF(GeneraliTransactions generaliTxn);

	ByteArrayOutputStream generateCounterOfferMemoPDF(
			GeneraliTransactions generaliTxn);


}
