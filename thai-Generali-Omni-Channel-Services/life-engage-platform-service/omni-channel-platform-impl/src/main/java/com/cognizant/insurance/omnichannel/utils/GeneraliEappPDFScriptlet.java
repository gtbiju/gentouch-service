package com.cognizant.insurance.omnichannel.utils;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.icr.pdf.util.PropertyFileReader;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.domain.agreement.Coverage;
import com.cognizant.insurance.domain.agreement.agreementpremium.Premium;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.commonelements.commonclasses.FamilyHealthHistory;
import com.cognizant.insurance.domain.commonelements.commonclasses.GLISelectedOption;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.commonelements.commoncodelists.FinancialMediumTypeCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.domain.contactandplace.ContactPoint;
import com.cognizant.insurance.domain.contactandplace.ContactPreference;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.AddressNatureCodeList;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.ElectronicContact;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.PostalAddressContact;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.TelephoneCallContact;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.City;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.Country;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.CountryElement;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.CountrySubdivision;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.PostCode;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.extension.Extension;
import com.cognizant.insurance.domain.finance.FinancialScheduler;
import com.cognizant.insurance.domain.finance.bankaccount.BankAccount;
import com.cognizant.insurance.domain.finance.paymentmethodsubtypes.CashPayment;
import com.cognizant.insurance.domain.finance.paymentmethodsubtypes.CheckPayment;
import com.cognizant.insurance.domain.finance.paymentmethodsubtypes.CreditCardPayment;
import com.cognizant.insurance.domain.party.Party;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;
import com.cognizant.insurance.domain.party.partyname.PersonName;
import com.cognizant.insurance.domain.party.persondetailsubtypes.EducationDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.IncomeDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.OccupationDetail;
import com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes.EmploymentRelationship;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.bridge.OmniContextBridge;
import com.cognizant.insurance.omnichannel.component.repository.helper.GeneraliComponentRepositoryHelper;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.utils.FileUtils;

/**
 * @author 444873 Class for eApp pdf data mapping
 * 
 */
public class GeneraliEappPDFScriptlet extends JRDefaultScriptlet {

	private Set<PersonName> personNames;

	private Person person;

	private Set<PersonDetail> personDetails;

	private IncomeDetail incomeDetail;

	private Set<Country> countries;

	private Set<ContactPreference> contactPreferences;

	private PostalAddressContact postalAddressContact;

	private PostalAddressContact permanentAddress;

	private List<OccupationDetail> occupationDetail;

	private TelephoneCallContact home1Contact;

	private ElectronicContact electronicContact;

	private String insuredMobileNumber1;

	private String insuredHomeNumber1;

	private String insuredOfficeNumber;

	/** Declaration Questions set **/
	private Set<UserSelection> declationQuestions;

	private String agentSignDate;

	private String agentSignPlace;

	private Date spajSignDate;

	private String SpajSignPlace;

	private String commisionRateOne;

	private String commisionRateTwo;

	private String partnerCode;

	private String commisionAgent;

	private String identityNo;

	private Date insuredIdentityDate;

	private String insuredIdentityPlace;

	private BigDecimal insuredHeight;

	private BigDecimal insuredWeight;

	/** The proposer. */
	private AgreementHolder proposer;

	private Person proposerPerson;

	private Set<PersonName> proposerPersonNames;

	private Date proposerdob;

	private Date insuredDob;

	String proposerFullName;

	private Set<PersonDetail> proposerPersonDetails;

	private Set<ContactPreference> proposerContactPreferences;

	private Set<Country> proposerCountries;

	private Country proposerResidenceCountry;

	private Country insuredResidenceCountry;

	private ElectronicContact proposerElectronicContact;

	private ElectronicContact agentElectronicContact;

	private PostalAddressContact proposerPostalAddressContact;

	private PostalAddressContact proposerPermanentAddress;

	private TelephoneCallContact telephoneCallContact;

	private String proposerMobileNumber1;

	private String proposerHomeNumber1;

	private String proposerOfficeNumber;

	private String agentMobileNumber1;

	private String agentHomeNumber1;

	private String agentEmailId;

	private String relationshipWithProposer = "";

	private OccupationDetail proposerOccupationDetail;

	private String proposerIdentityProof;

	private Date proposerIdentityDate;

	private String proposerIdentityPlace;

	private String proposerIdentityNo;

	IncomeDetail proposerIncomeDetail;

	EducationDetail proposerEducationDetail;

	String proposerPaymentMethod;

	/** The beneficiary list. */

	private Person firstBeneficiaryPerson;

	private Set<PersonName> firstBeneficiaryPersonNames;

	private Date firstBeneficiaryDOB;

	private String firstBeneficiaryGender;

	private Float firstBeneficiarySharePercentage;

	private String firstBeneficiaryRelationWithInsured;

	private Person secondBeneficiaryPerson;

	private Set<PersonName> secondBeneficiaryPersonNames;

	private Date secondBeneficiaryDOB;

	private String secondBeneficiaryGender;

	private Float secondBeneficiarySharePercentage;

	private String secondBeneficiaryRelationWithInsured;

	private Person thirdBeneficiaryPerson;

	private Set<PersonName> thirdBeneficiaryPersonNames;

	private Date thirdBeneficiaryDOB;

	private String thirdBeneficiaryGender;

	private Float thirdBeneficiarySharePercentage;

	private String thirdBeneficiaryRelationWithInsured;

	private Person fourthBeneficiaryPerson;

	private Set<PersonName> fourthBeneficiaryPersonNames;

	private Date fourthBeneficiaryDOB;

	private String fourthBeneficiaryGender;

	private Float fourthBeneficiarySharePercentage;

	private String fourthBeneficiaryRelationWithInsured;

	private String firstBeneficiaryTypeOfIdentity;

	private String secondBeneficiaryTypeOfIdentity;

	private String thirdBeneficiaryTypeOfIdentity;

	private String fourthBeneficiaryTypeOfIdentity;

	private Set<UserSelection> proposerUserSelections;
	/* fatca questions */
	private String proposerFatcaQues1;

	private String proposerFatcaQues11;

	private String proposerFatcaQues12;

	private String proposerFatcaQues13;

	private String proposerFatcaQues2;

	private String proposerFatcaQues3;

	private String proposerFatcaQues4;

	/* --- */

	private Set<UserSelection> firstBeneficiarySelectedOptions;

	private Set<UserSelection> secondBeneficiarySelectedOptions;

	private Set<UserSelection> thirdBeneficiarySelectedOptions;

	private Set<UserSelection> fourthBeneficiarySelectedOptions;

	private Set<UserSelection> fifthBeneficiarySelectedOptions;

	Boolean beneficiaryFatcaQues1;

	Boolean beneficiaryFatcaQues2;

	String spajNumber;

	String illustrationNumber;

	String agentCode;

	private String insuredIdentityType;

	private String proposerIdentityProofType;

	private Set<Insured> additionalInsuredes;

	/** Previous Policy */
	private Set<Insured> insuredPreviousPolicyList;

	private String firstCompanyName;

	private String firstPrevInsuredName;

	private String firstFinalDecision;

	private String firstTypeOfInsurance;

	private BigDecimal firstPrevSumAssured;

	private String firstPolicyStatus;

	private String secondCompanyName;

	private String secondPrevInsuredName;

	private String secondFinalDecision;

	private String secondTypeOfInsurance;

	private BigDecimal secondPrevSumAssured;

	private String secondPolicyStatus;

	private String thirdCompanyName;

	private String thirdPrevInsuredName;

	private String thirdFinalDecision;

	private String thirdTypeOfInsurance;

	private BigDecimal thirdPrevSumAssured;

	private String thirdPolicyStatus;

	private String fourthCompanyName;

	private String fourthPrevInsuredName;

	private String fourthFinalDecision;

	private String fourthTypeOfInsurance;

	private BigDecimal fourthPrevSumAssured;

	private String fourthPolicyStatus;

	private String firstPrePolicyType;

	private String secondPrePolicyType;

	private String thirdPrePolicyType;

	private String fourthPrePolicyType;

	private Date secondPrePolicyEffectiveDate;

	private Date firstPrePolicyEffectiveDate;

	private Date thirdPrePolicyEffectiveDate;

	private Date fourthPrePolicyEffectiveDate;

	private Boolean isPropDiffFromInsured;

	private String firstAddInsuredTypeOfIdentity;

	private String secondAddInsuredTypeOfIdentity;

	private String thirdAddInsuredTypeOfIdentity;

	private String fourthAddInsuredTypeOfIdentity;

	private Set<UserSelection> insuredUserSelections;

	private Set<UserSelection> payerUserSelections;

	private String insuredFamilyMembersDiagWithDiseases;

	private Set<FamilyHealthHistory> familyHealthHistories;

	private Integer firstFamilyMemAgeAtDeath;

	private Integer firstFamilyMemAge;

	private String firstFamilyMemCauseOfDeath;

	private String firstFamilyMemRelation;

	private Integer secondFamilyMemAgeAtDeath;

	private Integer secondFamilyMemAge;

	private String secondFamilyMemCauseOfDeath;

	private String secondFamilyMemRelation;

	private Integer thirdFamilyMemAgeAtDeath;

	private Integer thirdFamilyMemAge;

	private String thirdFamilyMemCauseOfDeath;

	private String thirdFamilyMemRelation;

	private Set<FamilyHealthHistory> familyAddFirstHealthHistories;

	private Integer firstAddFirstFamilyMemAgeAtDeath;

	private Integer firstAddFirstFamilyMemAge;

	private String firstAddFirstFamilyMemCauseOfDeath;

	private String firstAddFirstFamilyMemRelation;

	private Integer secondAddFirstFamilyMemAgeAtDeath;

	private Integer secondAddFirstFamilyMemAge;

	private String secondAddFirstFamilyMemCauseOfDeath;

	private String secondAddFirstFamilyMemRelation;

	private Integer thirdAddFirstFamilyMemAgeAtDeath;

	private Integer thirdAddFirstFamilyMemAge;

	private String thirdAddFirstFamilyMemCauseOfDeath;

	private String thirdAddFirstFamilyMemRelation;

	private Set<FamilyHealthHistory> familyAddSecondHealthHistories;

	private Integer firstAddSecondFamilyMemAgeAtDeath;

	private Integer firstAddSecondFamilyMemAge;

	private String firstAddSecondFamilyMemCauseOfDeath;

	private String firstAddSecondFamilyMemRelation;

	private Integer secondAddSecondFamilyMemAgeAtDeath;

	private Integer secondAddSecondFamilyMemAge;

	private String secondAddSecondFamilyMemCauseOfDeath;

	private String secondAddSecondFamilyMemRelation;

	private Integer thirdAddSecondFamilyMemAgeAtDeath;

	private Integer thirdAddSecondFamilyMemAge;

	private String thirdAddSecondFamilyMemCauseOfDeath;

	private String thirdAddSecondFamilyMemRelation;

	private Set<FamilyHealthHistory> familyAddThirdHealthHistories;

	private Integer firstAddThirdFamilyMemAgeAtDeath;

	private Integer firstAddThirdFamilyMemAge;

	private String firstAddThirdFamilyMemCauseOfDeath;

	private String firstAddThirdFamilyMemRelation;

	private Integer secondAddThirdFamilyMemAgeAtDeath;

	private Integer secondAddThirdFamilyMemAge;

	private String secondAddThirdFamilyMemCauseOfDeath;

	private String secondAddThirdFamilyMemRelation;

	private Integer thirdAddThirdFamilyMemAgeAtDeath;

	private Integer thirdAddThirdFamilyMemAge;

	private String thirdAddThirdFamilyMemCauseOfDeath;

	private String thirdAddThirdFamilyMemRelation;

	private Set<FamilyHealthHistory> familyAddFourthHealthHistories;

	private Integer firstAddFourthFamilyMemAgeAtDeath;

	private Integer firstAddFourthFamilyMemAge;

	private String firstAddFourthFamilyMemCauseOfDeath;

	private String firstAddFourthFamilyMemRelation;

	private Integer secondAddFourthFamilyMemAgeAtDeath;

	private Integer secondAddFourthFamilyMemAge;

	private String secondAddFourthFamilyMemCauseOfDeath;

	private String secondAddFourthFamilyMemRelation;

	private Integer thirdAddFourthFamilyMemAgeAtDeath;

	private Integer thirdAddFourthFamilyMemAge;

	private String thirdAddFourthFamilyMemCauseOfDeath;

	private String thirdAddFourthFamilyMemRelation;

	private String insadditionalInfo;

	private String payadditionalInfo;

	/* illnesstreatment Insured */
	private String illnessTreatmentQ1 = "";

	private String illnessTreatmentQ111 = "";

	private String illnessTreatmentQ112 = "";

	private String illnessTreatmentQ113 = "";

	private String illnessTreatmentQ114 = "";

	private String illnessTreatmentQ121 = "";

	private String illnessTreatmentQ122 = "";

	private String illnessTreatmentQ123 = "";

	private String illnessTreatmentQ124 = "";

	private String illnessTreatmentQ131 = "";

	private String illnessTreatmentQ132 = "";

	private String illnessTreatmentQ133 = "";

	private String illnessTreatmentQ134 = "";

	private String illnessTreatmentQ141 = "";

	private String illnessTreatmentQ142 = "";

	private String illnessTreatmentQ143 = "";

	private String illnessTreatmentQ144 = "";

	private String illnessTreatmentQ151 = "";

	private String illnessTreatmentQ152 = "";

	private String illnessTreatmentQ153 = "";

	private String illnessTreatmentQ154 = "";

	private String illnessTreatmentQ161 = "";

	private String illnessTreatmentQ162 = "";

	private String illnessTreatmentQ163 = "";

	private String illnessTreatmentQ164 = "";

	private String illnessTreatmentQ171 = "";

	private String illnessTreatmentQ172 = "";

	private String illnessTreatmentQ173 = "";

	private String illnessTreatmentQ174 = "";

	private String illnessTreatmentQ181 = "";

	private String illnessTreatmentQ182 = "";

	private String illnessTreatmentQ183 = "";

	private String illnessTreatmentQ184 = "";

	private String illnessTreatmentQ2 = "";

	private String illnessTreatmentQ211 = "";

	private String illnessTreatmentQ212 = "";

	private String illnessTreatmentQ213 = "";

	private String illnessTreatmentQ214 = "";

	private String illnessTreatmentQ221 = "";

	private String illnessTreatmentQ222 = "";

	private String illnessTreatmentQ223 = "";

	private String illnessTreatmentQ224 = "";

	private String illnessTreatmentQ231 = "";

	private String illnessTreatmentQ232 = "";

	private String illnessTreatmentQ233 = "";

	private String illnessTreatmentQ234 = "";

	private String illnessTreatmentQ241 = "";

	private String illnessTreatmentQ242 = "";

	private String illnessTreatmentQ243 = "";

	private String illnessTreatmentQ244 = "";

	private String illnessTreatmentQ3 = "";

	private String illnessTreatmentQ311 = "";

	private String illnessTreatmentQ312 = "";

	private String illnessTreatmentQ313 = "";

	private String illnessTreatmentQ314 = "";

	private String illnessTreatmentQ321 = "";

	private String illnessTreatmentQ322 = "";

	private String illnessTreatmentQ323 = "";

	private String illnessTreatmentQ324 = "";

	private String illnessTreatmentQ331 = "";

	private String illnessTreatmentQ332 = "";

	private String illnessTreatmentQ333 = "";

	private String illnessTreatmentQ334 = "";

	private String illnessTreatmentQ341 = "";

	private String illnessTreatmentQ342 = "";

	private String illnessTreatmentQ343 = "";

	private String illnessTreatmentQ344 = "";

	private String illnessTreatmentQ351 = "";

	private String illnessTreatmentQ352 = "";

	private String illnessTreatmentQ353 = "";

	private String illnessTreatmentQ354 = "";

	private String illnessTreatmentQ361 = "";

	private String illnessTreatmentQ362 = "";

	private String illnessTreatmentQ363 = "";

	private String illnessTreatmentQ364 = "";

	private String illnessTreatmentQ371 = "";

	private String illnessTreatmentQ372 = "";

	private String illnessTreatmentQ373 = "";

	private String illnessTreatmentQ374 = "";

	private String illnessTreatmentQ4 = "";

	private String illnessTreatmentQ411 = "";

	private String illnessTreatmentQ412 = "";

	private String illnessTreatmentQ413 = "";

	private String illnessTreatmentQ414 = "";

	private String illnessTreatmentQ421 = "";

	private String illnessTreatmentQ422 = "";

	private String illnessTreatmentQ423 = "";

	private String illnessTreatmentQ424 = "";

	private String illnessTreatmentQ431 = "";

	private String illnessTreatmentQ432 = "";

	private String illnessTreatmentQ433 = "";

	private String illnessTreatmentQ434 = "";

	private String illnessTreatmentQ441 = "";

	private String illnessTreatmentQ442 = "";

	private String illnessTreatmentQ443 = "";

	private String illnessTreatmentQ444 = "";

	private String illnessTreatmentQ451 = "";

	private String illnessTreatmentQ452 = "";

	private String illnessTreatmentQ453 = "";

	private String illnessTreatmentQ454 = "";

	private String illnessTreatmentQ461 = "";

	private String illnessTreatmentQ462 = "";

	private String illnessTreatmentQ463 = "";

	private String illnessTreatmentQ464 = "";

	private String illnessTreatmentQ5 = "";

	private String illnessTreatmentQ511 = "";

	private String illnessTreatmentQ512 = "";

	private String illnessTreatmentQ513 = "";

	private String illnessTreatmentQ514 = "";

	private String illnessTreatmentQ521 = "";

	private String illnessTreatmentQ522 = "";

	private String illnessTreatmentQ523 = "";

	private String illnessTreatmentQ524 = "";

	private String illnessTreatmentQ531 = "";

	private String illnessTreatmentQ532 = "";

	private String illnessTreatmentQ533 = "";

	private String illnessTreatmentQ534 = "";

	private String illnessTreatmentQ6 = "";

	private String illnessTreatmentQ611 = "";

	private String illnessTreatmentQ612 = "";

	private String illnessTreatmentQ613 = "";

	private String illnessTreatmentQ614 = "";

	private String illnessTreatmentQ621 = "";

	private String illnessTreatmentQ622 = "";

	private String illnessTreatmentQ623 = "";

	private String illnessTreatmentQ624 = "";

	private String illnessTreatmentQ631 = "";

	private String illnessTreatmentQ632 = "";

	private String illnessTreatmentQ633 = "";

	private String illnessTreatmentQ634 = "";

	private String illnessTreatmentQ7 = "";

	private String illnessTreatmentQ711 = "";

	private String illnessTreatmentQ712 = "";

	private String illnessTreatmentQ713 = "";

	private String illnessTreatmentQ714 = "";

	private String illnessTreatmentQ721 = "";

	private String illnessTreatmentQ722 = "";

	private String illnessTreatmentQ723 = "";

	private String illnessTreatmentQ724 = "";

	private String illnessTreatmentQ731 = "";

	private String illnessTreatmentQ732 = "";

	private String illnessTreatmentQ733 = "";

	private String illnessTreatmentQ734 = "";

	private String illnessTreatmentQ741 = "";

	private String illnessTreatmentQ742 = "";

	private String illnessTreatmentQ743 = "";

	private String illnessTreatmentQ744 = "";

	private String illnessTreatmentQ751 = "";

	private String illnessTreatmentQ752 = "";

	private String illnessTreatmentQ753 = "";

	private String illnessTreatmentQ754 = "";

	private String illnessTreatmentQ8 = "";

	private String illnessTreatmentQ811 = "";

	private String illnessTreatmentQ812 = "";

	private String illnessTreatmentQ813 = "";

	private String illnessTreatmentQ814 = "";

	private String illnessTreatmentQ821 = "";

	private String illnessTreatmentQ822 = "";

	private String illnessTreatmentQ823 = "";

	private String illnessTreatmentQ824 = "";

	private String illnessTreatmentQ831 = "";

	private String illnessTreatmentQ832 = "";

	private String illnessTreatmentQ833 = "";

	private String illnessTreatmentQ834 = "";

	private String illnessTreatmentQ841 = "";

	private String illnessTreatmentQ842 = "";

	private String illnessTreatmentQ843 = "";

	private String illnessTreatmentQ844 = "";

	private String illnessTreatmentQ851 = "";

	private String illnessTreatmentQ852 = "";

	private String illnessTreatmentQ853 = "";

	private String illnessTreatmentQ854 = "";

	private String illnessTreatmentQ9 = "";

	private String illnessTreatmentQ911 = "";

	private String illnessTreatmentQ912 = "";

	private String illnessTreatmentQ913 = "";

	private String illnessTreatmentQ914 = "";

	private String illnessTreatmentQ921 = "";

	private String illnessTreatmentQ922 = "";

	private String illnessTreatmentQ923 = "";

	private String illnessTreatmentQ924 = "";

	private String illnessTreatmentQ10 = "";

	private String illnessTreatmentQ1011 = "";

	private String illnessTreatmentQ1012 = "";

	private String illnessTreatmentQ1013 = "";

	private String illnessTreatmentQ1014 = "";

	private String illnessTreatmentQ1021 = "";

	private String illnessTreatmentQ1022 = "";

	private String illnessTreatmentQ1023 = "";

	private String illnessTreatmentQ1024 = "";

	private String illnessTreatmentQ1031 = "";

	private String illnessTreatmentQ1032 = "";

	private String illnessTreatmentQ1033 = "";

	private String illnessTreatmentQ1034 = "";

	private String illnessTreatmentQ1041 = "";

	private String illnessTreatmentQ1042 = "";

	private String illnessTreatmentQ1043 = "";

	private String illnessTreatmentQ1044 = "";

	private String illnessTreatmentQ1051 = "";

	private String illnessTreatmentQ1052 = "";

	private String illnessTreatmentQ1053 = "";

	private String illnessTreatmentQ1054 = "";

	private String illnessTreatmentQ1061 = "";

	private String illnessTreatmentQ1062 = "";

	private String illnessTreatmentQ1063 = "";

	private String illnessTreatmentQ1064 = "";

	private String illnessTreatmentQ11 = "";

	private String illnessTreatmentQ1111 = "";

	private String illnessTreatmentQ1112 = "";

	private String illnessTreatmentQ1113 = "";

	private String illnessTreatmentQ1114 = "";

	private String illnessTreatmentQ1121 = "";

	private String illnessTreatmentQ1122 = "";

	private String illnessTreatmentQ1123 = "";

	private String illnessTreatmentQ1124 = "";

	private String illnessTreatmentQ1131 = "";

	private String illnessTreatmentQ1132 = "";

	private String illnessTreatmentQ1133 = "";

	private String illnessTreatmentQ1134 = "";

	private String illnessTreatmentQ1141 = "";

	private String illnessTreatmentQ1142 = "";

	private String illnessTreatmentQ1143 = "";

	private String illnessTreatmentQ1144 = "";

	private String illnessTreatmentQ1151 = "";

	private String illnessTreatmentQ1152 = "";

	private String illnessTreatmentQ1153 = "";

	private String illnessTreatmentQ1154 = "";

	private String illnessTreatmentQ1161 = "";

	private String illnessTreatmentQ1162 = "";

	private String illnessTreatmentQ1163 = "";

	private String illnessTreatmentQ1164 = "";

	private String illnessTreatmentQ1171 = "";

	private String illnessTreatmentQ1172 = "";

	private String illnessTreatmentQ1173 = "";

	private String illnessTreatmentQ1174 = "";

	private String illnessTreatmentQ12 = "";

	private String illnessTreatmentQ1211 = "";

	private String illnessTreatmentQ1212 = "";

	private String illnessTreatmentQ1213 = "";

	private String illnessTreatmentQ1214 = "";

	private String illnessTreatmentQ1221 = "";

	private String illnessTreatmentQ1222 = "";

	private String illnessTreatmentQ1223 = "";

	private String illnessTreatmentQ1224 = "";

	private String illnessTreatmentQ13 = "";

	private String illnessTreatmentQ1311 = "";

	private String illnessTreatmentQ1312 = "";

	private String illnessTreatmentQ1313 = "";

	private String illnessTreatmentQ1314 = "";

	private String illnessTreatmentQ1321 = "";

	private String illnessTreatmentQ1322 = "";

	private String illnessTreatmentQ1323 = "";

	private String illnessTreatmentQ1324 = "";

	private String illnessTreatmentQ14 = "";

	private String illnessTreatmentQ1411 = "";

	private String illnessTreatmentQ1412 = "";

	private String illnessTreatmentQ1413 = "";

	private String illnessTreatmentQ1414 = "";

	private String illnessTreatmentQ1421 = "";

	private String illnessTreatmentQ1422 = "";

	private String illnessTreatmentQ1423 = "";

	private String illnessTreatmentQ1424 = "";

	private String illnessTreatmentQ1431 = "";

	private String illnessTreatmentQ1432 = "";

	private String illnessTreatmentQ1433 = "";

	private String illnessTreatmentQ1434 = "";

	private String illnessTreatmentQ15 = "";

	private String illnessTreatmentQ1511 = "";

	private String illnessTreatmentQ1512 = "";

	private String illnessTreatmentQ1513 = "";

	private String illnessTreatmentQ1514 = "";

	private String illnessTreatmentQ16 = "";

	private String illnessTreatmentQ1611 = "";

	private String illnessTreatmentQ1612 = "";

	private String illnessTreatmentQ1613 = "";

	private String illnessTreatmentQ1614 = "";

	private String illnessTreatmentQ17 = "";

	private String illnessTreatmentQ1711 = "";

	private String illnessTreatmentQ1712 = "";

	private String illnessTreatmentQ1713 = "";

	private String illnessTreatmentQ1714 = "";

	private String illnessTreatmentQ18 = "";

	private String illnessTreatmentQ1811 = "";

	private String illnessTreatmentQ1812 = "";

	private String illnessTreatmentQ1813 = "";

	private String illnessTreatmentQ1814 = "";

	private String illnessTreatmentQ19 = "";

	private String illnessTreatmentQ1911 = "";

	private String illnessTreatmentQ1912 = "";

	private String illnessTreatmentQ1913 = "";

	private String illnessTreatmentQ1914 = "";

	private String illnessTreatmentQ20 = "";

	private String illnessTreatmentQ2011 = "";

	private String illnessTreatmentQ21 = "";

	private String illnessTreatmentQ2111 = "";

	private String illnessTreatmentQ2112 = "";

	private String illnessTreatmentQ2113 = "";

	private String illnessTreatmentQ2114 = "";

	private String illnessTreatmentQ2121 = "";

	private String illnessTreatmentQ22 = "";

	private String illnessTreatmentQ2211 = "";

	private String illnessTreatmentQ2212 = "";

	private String illnessTreatmentQ2213 = "";

	private String illnessTreatmentQ2214 = "";

	private String illnessTreatmentQ2215 = "";

	private String illnessTreatmentQ23 = "";

	private String illnessTreatmentQ2311 = "";

	private String illnessTreatmentQ2312 = "";

	private String illnessTreatmentQ2313 = "";

	private String illnessTreatmentQ2314 = "";

	private String illnessTreatmentQ2315 = "";

	private String illnessTreatmentQ24 = "";

	private String illnessTreatmentQ2411 = "";

	private String illnessTreatmentQ2412 = "";

	private String illnessTreatmentQ2413 = "";

	private String illnessTreatmentQ2414 = "";

	private String illnessTreatmentQ25 = "";

	private String illnessTreatmentQ2511 = "";

	private String illnessTreatmentQ2512 = "";

	private String illnessTreatmentQ2513 = "";

	private String illnessTreatmentQ2514 = "";

	private String illnessTreatmentQ2515 = "";

	private String illnessTreatmentQ2521 = "";

	private String illnessTreatmentQ2522 = "";

	private String illnessTreatmentQ2523 = "";

	private String illnessTreatmentQ2524 = "";

	private String illnessTreatmentQ2525 = "";

	private String illnessTreatmentQ2531 = "";

	private String illnessTreatmentQ2532 = "";

	private String illnessTreatmentQ2533 = "";

	private String illnessTreatmentQ2534 = "";

	private String illnessTreatmentQ2535 = "";

	private String illnessTreatmentQ2541 = "";

	private String illnessTreatmentQ2542 = "";

	private String illnessTreatmentQ2543 = "";

	private String illnessTreatmentQ2544 = "";

	private String illnessTreatmentQ2545 = "";

	private String illnessTreatmentQ2551 = "";

	private String illnessTreatmentQ2552 = "";

	private String illnessTreatmentQ2553 = "";

	private String illnessTreatmentQ2554 = "";

	private String illnessTreatmentQ2555 = "";

	private String illnessTreatmentQ2561 = "";

	private String illnessTreatmentQ2562 = "";

	private String illnessTreatmentQ2563 = "";

	private String illnessTreatmentQ2564 = "";

	private String illnessTreatmentQ2565 = "";

	private String illnessTreatmentQ2571 = "";

	private String illnessTreatmentQ2572 = "";

	private String illnessTreatmentQ2573 = "";

	private String illnessTreatmentQ2574 = "";

	private String illnessTreatmentQ2575 = "";

	private String illnessTreatmentQ26 = "";

	private String illnessTreatmentQ2611 = "";

	private String illnessTreatmentQ2612 = "";

	private String illnessTreatmentQ2613 = "";

	private String illnessTreatmentQ2614 = "";

	private String illnessTreatmentQ2615 = "";

	private String illnessTreatmentQ2621 = "";

	private String illnessTreatmentQ2622 = "";

	private String illnessTreatmentQ2623 = "";

	private String illnessTreatmentQ2624 = "";

	private String illnessTreatmentQ2625 = "";

	private String illnessTreatmentQ2631 = "";

	private String illnessTreatmentQ2632 = "";

	private String illnessTreatmentQ2633 = "";

	private String illnessTreatmentQ2634 = "";

	private String illnessTreatmentQ2635 = "";

	private String illnessTreatmentQ2641 = "";

	private String illnessTreatmentQ2642 = "";

	private String illnessTreatmentQ2643 = "";

	private String illnessTreatmentQ2644 = "";

	private String illnessTreatmentQ2645 = "";

	private String illnessTreatmentQ2651 = "";

	private String illnessTreatmentQ2652 = "";

	private String illnessTreatmentQ2653 = "";

	private String illnessTreatmentQ2654 = "";

	private String illnessTreatmentQ2655 = "";

	private String illnessTreatmentQ2661 = "";

	private String illnessTreatmentQ2662 = "";

	private String illnessTreatmentQ2663 = "";

	private String illnessTreatmentQ2664 = "";

	private String illnessTreatmentQ2665 = "";

	private String illnessTreatmentQ2671 = "";

	private String illnessTreatmentQ2672 = "";

	private String illnessTreatmentQ2673 = "";

	private String illnessTreatmentQ2674 = "";

	private String illnessTreatmentQ2675 = "";

	private String illnessTreatmentQ27 = "";

	private String illnessTreatmentQ2711 = "";

	private String illnessTreatmentQ2712 = "";

	private String illnessTreatmentQ2713 = "";

	private String illnessTreatmentQ2714 = "";

	private String illnessTreatmentQ2715 = "";

	private String illnessTreatmentQ2721 = "";

	private String illnessTreatmentQ2722 = "";

	private String illnessTreatmentQ2723 = "";

	private String illnessTreatmentQ2724 = "";

	private String illnessTreatmentQ2725 = "";

	private String illnessTreatmentQ2731 = "";

	private String illnessTreatmentQ2732 = "";

	private String illnessTreatmentQ2733 = "";

	private String illnessTreatmentQ2734 = "";

	private String illnessTreatmentQ2735 = "";

	private String illnessTreatmentQ2741 = "";

	private String illnessTreatmentQ2742 = "";

	private String illnessTreatmentQ2743 = "";

	private String illnessTreatmentQ2744 = "";

	private String illnessTreatmentQ2745 = "";

	private String illnessTreatmentQ2751 = "";

	private String illnessTreatmentQ2752 = "";

	private String illnessTreatmentQ2753 = "";

	private String illnessTreatmentQ2754 = "";

	private String illnessTreatmentQ2755 = "";

	private String illnessTreatmentQ2761 = "";

	private String illnessTreatmentQ2762 = "";

	private String illnessTreatmentQ2763 = "";

	private String illnessTreatmentQ2764 = "";

	private String illnessTreatmentQ2765 = "";

	private String illnessTreatmentQ2771 = "";

	private String illnessTreatmentQ2772 = "";

	private String illnessTreatmentQ2773 = "";

	private String illnessTreatmentQ2774 = "";

	private String illnessTreatmentQ2775 = "";

	private String illnessTreatmentQ28 = "";

	private String illnessTreatmentQ2811 = "";

	private String illnessTreatmentQ2812 = "";

	private String illnessTreatmentQ2813 = "";

	private String illnessTreatmentQ2814 = "";

	private String illnessTreatmentQ2815 = "";

	private String illnessTreatmentQ2821 = "";

	private String illnessTreatmentQ2822 = "";

	private String illnessTreatmentQ2823 = "";

	private String illnessTreatmentQ2824 = "";

	private String illnessTreatmentQ2825 = "";

	private String disease1="";
	
	private String disease2="";
	
	private String disease3="";
	
	private String disease4="";
	
	private String disease5="";
	
	private String disease6="";
	
	private String disease7="";
	
	private String disease8="";
	
	private String disease9="";
	
	private String disease10="";
	
    private String disease11="";
	
	private String disease12="";
	
	private String disease13="";
	
	private String disease14="";
	
	private String disease15="";
	
	private String disease16="";
	
	private String disease17="";
	
	private String disease18="";
	
	private String disease19="";
	
	private String disease20="";
	
    private String disease21="";
	
	private String disease22="";
	
	private String disease23="";
	
	private String disease24="";
	
	private String disease25="";
	
	private String disease26="";
	
	private String disease27="";
	
	private String disease28="";
	
	private String disease29="";
	
	private String disease30="";
	
	private String disease31="";
	
	private String disease32="";
	
	private String disease33="";
	
	private String disease34="";
	
	private String disease35="";
	
	private String disease36="";
	
	private String disease37="";
	
	private String disease38="";
	
	private String disease39="";
	
	private String disease40="";
	
    private String disease41="";
	
	private String disease42="";
	
	private String disease43="";
	
	private String disease44="";
	
	private String disease45="";
	
	private String disease46="";
	
	private String disease47="";
	
	private String disease48="";
	
	private String disease49="";
	
	private String disease50="";
	
    private String disease51="";
	
	private String disease52="";
	
	private String disease53="";
	
	private String disease54="";
	
	private String disease55="";
	
	private String disease56="";
	
	private String disease57="";
	
	private String disease58="";
	
	private String disease59="";
	
	private String disease60="";
	
	private String disease61="";
	
	private String disease62="";
	
	private String disease63="";
	
	private String disease64="";
	
	private String disease65="";
	
	private String disease66="";
	
	private String disease67="";
	
	private String disease68="";
	
	private String disease73="";
	
	private String disease74="";
	
	private String disease75="";
	
	private String disease76="";
	
	private String disease77="";
	
	private String disease78="";
	
	private String disease79="";
	
	private String disease80="";
	
    private String disease81="";
	
	private String disease82="";
	
	private String disease83="";
	
	private String disease84="";
	
	private String disease85="";
	
	private String disease86="";
	
	private String disease87="";
	
	private String disease88="";
	
	private String disease89="";
	
	private String disease90="";
	
     private String disease91="";
	
	private String disease92="";
	
	private String disease93="";
	
	private String diseasep1="";
	
	private String diseasep2="";
	
	private String diseasep3="";
	
	private String diseasep4="";
	
	private String diseasep5="";
	
	private String diseasep6="";
	
	private String diseasep7="";
	
	private String diseasep8="";
	
	private String diseasep9="";
	
	private String diseasep10="";
	
    private String diseasep11="";
	
	private String diseasep12="";
	
	private String diseasep13="";
	
	private String diseasep14="";
	
	private String diseasep15="";
	
	private String diseasep16="";
	
	private String diseasep17="";
	
	private String diseasep18="";
	
	private String diseasep19="";
	
	private String diseasep20="";
	
    private String diseasep21="";
	
	private String diseasep22="";
	
	private String diseasep23="";
	
	private String diseasep24="";
	
	private String diseasep25="";
	
	private String diseasep26="";
	
	private String diseasep27="";
	
	private String diseasep28="";
	
	private String diseasep29="";
	
	private String diseasep30="";
	
	private String diseasep31="";
	
	private String diseasep32="";
	
	private String diseasep33="";
	
	private String diseasep34="";
	
	private String diseasep35="";
	
	private String diseasep36="";
	
	private String diseasep37="";
	
	private String diseasep38="";
	
	private String diseasep39="";
	
	private String diseasep40="";
	
    private String diseasep41="";
	
	private String diseasep42="";
	
	private String diseasep43="";
	
	private String diseasep44="";
	
	private String diseasep45="";
	
	private String diseasep46="";
	
	private String diseasep47="";
	
	private String diseasep48="";
	
	private String diseasep49="";
	
	private String diseasep50="";
	
    private String diseasep51="";
	
	private String diseasep52="";
	
	private String diseasep53="";
	
	private String diseasep54="";
	
	private String diseasep55="";
	
	private String diseasep56="";
	
	private String diseasep57="";
	
	private String diseasep58="";
	
	private String diseasep59="";
	
	private String diseasep60="";
	
	private String diseasep61="";
	
	private String diseasep62="";
	
	private String diseasep63="";
	
	private String diseasep64="";
	
	private String diseasep65="";
	
	private String diseasep66="";
	
	private String diseasep67="";
	
	private String diseasep68="";


	/*---*/

	/* illnessTreatment Payer */
	private String illnessTreatmentPQ1 = "";

	private String illnessTreatmentPQ111 = "";

	private String illnessTreatmentPQ112 = "";

	private String illnessTreatmentPQ113 = "";

	private String illnessTreatmentPQ114 = "";

	private String illnessTreatmentPQ121 = "";

	private String illnessTreatmentPQ122 = "";

	private String illnessTreatmentPQ123 = "";

	private String illnessTreatmentPQ124 = "";

	private String illnessTreatmentPQ131 = "";

	private String illnessTreatmentPQ132 = "";

	private String illnessTreatmentPQ133 = "";

	private String illnessTreatmentPQ134 = "";

	private String illnessTreatmentPQ141 = "";

	private String illnessTreatmentPQ142 = "";

	private String illnessTreatmentPQ143 = "";

	private String illnessTreatmentPQ144 = "";

	private String illnessTreatmentPQ151 = "";

	private String illnessTreatmentPQ152 = "";

	private String illnessTreatmentPQ153 = "";

	private String illnessTreatmentPQ154 = "";

	private String illnessTreatmentPQ161 = "";

	private String illnessTreatmentPQ162 = "";

	private String illnessTreatmentPQ163 = "";

	private String illnessTreatmentPQ164 = "";

	private String illnessTreatmentPQ171 = "";

	private String illnessTreatmentPQ172 = "";

	private String illnessTreatmentPQ173 = "";

	private String illnessTreatmentPQ174 = "";

	private String illnessTreatmentPQ181 = "";

	private String illnessTreatmentPQ182 = "";

	private String illnessTreatmentPQ183 = "";

	private String illnessTreatmentPQ184 = "";

	private String illnessTreatmentPQ2 = "";

	private String illnessTreatmentPQ211 = "";

	private String illnessTreatmentPQ212 = "";

	private String illnessTreatmentPQ213 = "";

	private String illnessTreatmentPQ214 = "";

	private String illnessTreatmentPQ221 = "";

	private String illnessTreatmentPQ222 = "";

	private String illnessTreatmentPQ223 = "";

	private String illnessTreatmentPQ224 = "";

	private String illnessTreatmentPQ231 = "";

	private String illnessTreatmentPQ232 = "";

	private String illnessTreatmentPQ233 = "";

	private String illnessTreatmentPQ234 = "";

	private String illnessTreatmentPQ241 = "";

	private String illnessTreatmentPQ242 = "";

	private String illnessTreatmentPQ243 = "";

	private String illnessTreatmentPQ244 = "";

	private String illnessTreatmentPQ3 = "";

	private String illnessTreatmentPQ311 = "";

	private String illnessTreatmentPQ312 = "";

	private String illnessTreatmentPQ313 = "";

	private String illnessTreatmentPQ314 = "";

	private String illnessTreatmentPQ321 = "";

	private String illnessTreatmentPQ322 = "";

	private String illnessTreatmentPQ323 = "";

	private String illnessTreatmentPQ324 = "";

	private String illnessTreatmentPQ331 = "";

	private String illnessTreatmentPQ332 = "";

	private String illnessTreatmentPQ333 = "";

	private String illnessTreatmentPQ334 = "";

	private String illnessTreatmentPQ341 = "";

	private String illnessTreatmentPQ342 = "";

	private String illnessTreatmentPQ343 = "";

	private String illnessTreatmentPQ344 = "";

	private String illnessTreatmentPQ351 = "";

	private String illnessTreatmentPQ352 = "";

	private String illnessTreatmentPQ353 = "";

	private String illnessTreatmentPQ354 = "";

	private String illnessTreatmentPQ361 = "";

	private String illnessTreatmentPQ362 = "";

	private String illnessTreatmentPQ363 = "";

	private String illnessTreatmentPQ364 = "";

	private String illnessTreatmentPQ371 = "";

	private String illnessTreatmentPQ372 = "";

	private String illnessTreatmentPQ373 = "";

	private String illnessTreatmentPQ374 = "";

	private String illnessTreatmentPQ4 = "";

	private String illnessTreatmentPQ411 = "";

	private String illnessTreatmentPQ412 = "";

	private String illnessTreatmentPQ413 = "";

	private String illnessTreatmentPQ414 = "";

	private String illnessTreatmentPQ421 = "";

	private String illnessTreatmentPQ422 = "";

	private String illnessTreatmentPQ423 = "";

	private String illnessTreatmentPQ424 = "";

	private String illnessTreatmentPQ431 = "";

	private String illnessTreatmentPQ432 = "";

	private String illnessTreatmentPQ433 = "";

	private String illnessTreatmentPQ434 = "";

	private String illnessTreatmentPQ441 = "";

	private String illnessTreatmentPQ442 = "";

	private String illnessTreatmentPQ443 = "";

	private String illnessTreatmentPQ444 = "";

	private String illnessTreatmentPQ451 = "";

	private String illnessTreatmentPQ452 = "";

	private String illnessTreatmentPQ453 = "";

	private String illnessTreatmentPQ454 = "";

	private String illnessTreatmentPQ461 = "";

	private String illnessTreatmentPQ462 = "";

	private String illnessTreatmentPQ463 = "";

	private String illnessTreatmentPQ464 = "";

	private String illnessTreatmentPQ5 = "";

	private String illnessTreatmentPQ511 = "";

	private String illnessTreatmentPQ512 = "";

	private String illnessTreatmentPQ513 = "";

	private String illnessTreatmentPQ514 = "";

	private String illnessTreatmentPQ521 = "";

	private String illnessTreatmentPQ522 = "";

	private String illnessTreatmentPQ523 = "";

	private String illnessTreatmentPQ524 = "";

	private String illnessTreatmentPQ531 = "";

	private String illnessTreatmentPQ532 = "";

	private String illnessTreatmentPQ533 = "";

	private String illnessTreatmentPQ534 = "";

	private String illnessTreatmentPQ6 = "";

	private String illnessTreatmentPQ611 = "";

	private String illnessTreatmentPQ612 = "";

	private String illnessTreatmentPQ613 = "";

	private String illnessTreatmentPQ614 = "";

	private String illnessTreatmentPQ621 = "";

	private String illnessTreatmentPQ622 = "";

	private String illnessTreatmentPQ623 = "";

	private String illnessTreatmentPQ624 = "";

	private String illnessTreatmentPQ631 = "";

	private String illnessTreatmentPQ632 = "";

	private String illnessTreatmentPQ633 = "";

	private String illnessTreatmentPQ634 = "";

	private String illnessTreatmentPQ7 = "";

	private String illnessTreatmentPQ711 = "";

	private String illnessTreatmentPQ712 = "";

	private String illnessTreatmentPQ713 = "";

	private String illnessTreatmentPQ714 = "";

	private String illnessTreatmentPQ721 = "";

	private String illnessTreatmentPQ722 = "";

	private String illnessTreatmentPQ723 = "";

	private String illnessTreatmentPQ724 = "";

	private String illnessTreatmentPQ731 = "";

	private String illnessTreatmentPQ732 = "";

	private String illnessTreatmentPQ733 = "";

	private String illnessTreatmentPQ734 = "";

	private String illnessTreatmentPQ741 = "";

	private String illnessTreatmentPQ742 = "";

	private String illnessTreatmentPQ743 = "";

	private String illnessTreatmentPQ744 = "";

	private String illnessTreatmentPQ751 = "";

	private String illnessTreatmentPQ752 = "";

	private String illnessTreatmentPQ753 = "";

	private String illnessTreatmentPQ754 = "";

	private String illnessTreatmentPQ8 = "";

	private String illnessTreatmentPQ811 = "";

	private String illnessTreatmentPQ812 = "";

	private String illnessTreatmentPQ813 = "";

	private String illnessTreatmentPQ814 = "";

	private String illnessTreatmentPQ821 = "";

	private String illnessTreatmentPQ822 = "";

	private String illnessTreatmentPQ823 = "";

	private String illnessTreatmentPQ824 = "";

	private String illnessTreatmentPQ831 = "";

	private String illnessTreatmentPQ832 = "";

	private String illnessTreatmentPQ833 = "";

	private String illnessTreatmentPQ834 = "";

	private String illnessTreatmentPQ841 = "";

	private String illnessTreatmentPQ842 = "";

	private String illnessTreatmentPQ843 = "";

	private String illnessTreatmentPQ844 = "";

	private String illnessTreatmentPQ851 = "";

	private String illnessTreatmentPQ852 = "";

	private String illnessTreatmentPQ853 = "";

	private String illnessTreatmentPQ854 = "";

	private String illnessTreatmentPQ9 = "";

	private String illnessTreatmentPQ911 = "";

	private String illnessTreatmentPQ912 = "";

	private String illnessTreatmentPQ913 = "";

	private String illnessTreatmentPQ914 = "";

	private String illnessTreatmentPQ921 = "";

	private String illnessTreatmentPQ922 = "";

	private String illnessTreatmentPQ923 = "";

	private String illnessTreatmentPQ924 = "";

	private String illnessTreatmentPQ10 = "";

	private String illnessTreatmentPQ1011 = "";

	private String illnessTreatmentPQ1012 = "";

	private String illnessTreatmentPQ1013 = "";

	private String illnessTreatmentPQ1014 = "";

	private String illnessTreatmentPQ1021 = "";

	private String illnessTreatmentPQ1022 = "";

	private String illnessTreatmentPQ1023 = "";

	private String illnessTreatmentPQ1024 = "";

	private String illnessTreatmentPQ1031 = "";

	private String illnessTreatmentPQ1032 = "";

	private String illnessTreatmentPQ1033 = "";

	private String illnessTreatmentPQ1034 = "";

	private String illnessTreatmentPQ1041 = "";

	private String illnessTreatmentPQ1042 = "";

	private String illnessTreatmentPQ1043 = "";

	private String illnessTreatmentPQ1044 = "";

	private String illnessTreatmentPQ1051 = "";

	private String illnessTreatmentPQ1052 = "";

	private String illnessTreatmentPQ1053 = "";

	private String illnessTreatmentPQ1054 = "";

	private String illnessTreatmentPQ1061 = "";

	private String illnessTreatmentPQ1062 = "";

	private String illnessTreatmentPQ1063 = "";

	private String illnessTreatmentPQ1064 = "";

	private String illnessTreatmentPQ11 = "";

	private String illnessTreatmentPQ1111 = "";

	private String illnessTreatmentPQ1112 = "";

	private String illnessTreatmentPQ1113 = "";

	private String illnessTreatmentPQ1114 = "";

	private String illnessTreatmentPQ1121 = "";

	private String illnessTreatmentPQ1122 = "";

	private String illnessTreatmentPQ1123 = "";

	private String illnessTreatmentPQ1124 = "";

	private String illnessTreatmentPQ1131 = "";

	private String illnessTreatmentPQ1132 = "";

	private String illnessTreatmentPQ1133 = "";

	private String illnessTreatmentPQ1134 = "";

	private String illnessTreatmentPQ1141 = "";

	private String illnessTreatmentPQ1142 = "";

	private String illnessTreatmentPQ1143 = "";

	private String illnessTreatmentPQ1144 = "";

	private String illnessTreatmentPQ1151 = "";

	private String illnessTreatmentPQ1152 = "";

	private String illnessTreatmentPQ1153 = "";

	private String illnessTreatmentPQ1154 = "";

	private String illnessTreatmentPQ1161 = "";

	private String illnessTreatmentPQ1162 = "";

	private String illnessTreatmentPQ1163 = "";

	private String illnessTreatmentPQ1164 = "";

	private String illnessTreatmentPQ1171 = "";

	private String illnessTreatmentPQ1172 = "";

	private String illnessTreatmentPQ1173 = "";

	private String illnessTreatmentPQ1174 = "";

	private String illnessTreatmentPQ12 = "";

	private String illnessTreatmentPQ1211 = "";

	private String illnessTreatmentPQ1212 = "";

	private String illnessTreatmentPQ1213 = "";

	private String illnessTreatmentPQ1214 = "";

	private String illnessTreatmentPQ1221 = "";

	private String illnessTreatmentPQ1222 = "";

	private String illnessTreatmentPQ1223 = "";

	private String illnessTreatmentPQ1224 = "";

	private String illnessTreatmentPQ13 = "";

	private String illnessTreatmentPQ1311 = "";

	private String illnessTreatmentPQ1312 = "";

	private String illnessTreatmentPQ1313 = "";

	private String illnessTreatmentPQ1314 = "";

	private String illnessTreatmentPQ1321 = "";

	private String illnessTreatmentPQ1322 = "";

	private String illnessTreatmentPQ1323 = "";

	private String illnessTreatmentPQ1324 = "";

	private String illnessTreatmentPQ14 = "";

	private String illnessTreatmentPQ1411 = "";

	private String illnessTreatmentPQ1412 = "";

	private String illnessTreatmentPQ1413 = "";

	private String illnessTreatmentPQ1414 = "";

	private String illnessTreatmentPQ1421 = "";

	private String illnessTreatmentPQ1422 = "";

	private String illnessTreatmentPQ1423 = "";

	private String illnessTreatmentPQ1424 = "";

	private String illnessTreatmentPQ1431 = "";

	private String illnessTreatmentPQ1432 = "";

	private String illnessTreatmentPQ1433 = "";

	private String illnessTreatmentPQ1434 = "";

	private String illnessTreatmentPQ15 = "";

	private String illnessTreatmentPQ1511 = "";

	private String illnessTreatmentPQ1512 = "";

	private String illnessTreatmentPQ1513 = "";

	private String illnessTreatmentPQ1514 = "";

	private String illnessTreatmentPQ16 = "";

	private String illnessTreatmentPQ1611 = "";

	private String illnessTreatmentPQ1612 = "";

	private String illnessTreatmentPQ1613 = "";

	private String illnessTreatmentPQ1614 = "";

	private String illnessTreatmentPQ17 = "";

	private String illnessTreatmentPQ1711 = "";

	private String illnessTreatmentPQ1712 = "";

	private String illnessTreatmentPQ1713 = "";

	private String illnessTreatmentPQ1714 = "";

	private String illnessTreatmentPQ18 = "";

	private String illnessTreatmentPQ1811 = "";

	private String illnessTreatmentPQ1812 = "";

	private String illnessTreatmentPQ1813 = "";

	private String illnessTreatmentPQ1814 = "";

	private String illnessTreatmentPQ19 = "";

	private String illnessTreatmentPQ1911 = "";

	private String illnessTreatmentPQ1912 = "";

	private String illnessTreatmentPQ1913 = "";

	private String illnessTreatmentPQ1914 = "";

	private String illnessTreatmentPQ20 = "";

	private String illnessTreatmentPQ2011 = "";

	private String illnessTreatmentPQ21 = "";

	private String illnessTreatmentPQ2111 = "";

	private String illnessTreatmentPQ2112 = "";

	private String illnessTreatmentPQ2113 = "";

	private String illnessTreatmentPQ2114 = "";
	
	private String illnessTreatmentPQ2121 = "";

	private String illnessTreatmentPQ22 = "";

	private String illnessTreatmentPQ2211 = "";

	private String illnessTreatmentPQ2212 = "";

	private String illnessTreatmentPQ2213 = "";

	private String illnessTreatmentPQ2214 = "";

	private String illnessTreatmentPQ2215 = "";

	private String illnessTreatmentPQ23 = "";

	private String illnessTreatmentPQ2311 = "";

	private String illnessTreatmentPQ2312 = "";

	private String illnessTreatmentPQ2313 = "";

	private String illnessTreatmentPQ2314 = "";

	private String illnessTreatmentPQ2315 = "";

	private String illnessTreatmentPQ24 = "";

	private String illnessTreatmentPQ2411 = "";

	private String illnessTreatmentPQ2412 = "";

	private String illnessTreatmentPQ2413 = "";

	private String illnessTreatmentPQ2414 = "";

	/* ----- */

	private AgreementProducer agent;

	private Person agentPerson;

	/** The proposal. */
	private InsuranceAgreement proposal;

	/** The appointee agreement Documents. */
	public Set<Document> appionteeDocuments;

	private String productName;

	private BigDecimal productSumAssured;

	private Integer productPolicyTerm;

	private Integer productPremiumTerm;

	private String firstRiderName;

	private String secondRiderName;

	private String thirdRiderName;

	private String fourthRiderName;

	private String fifthRiderName;

	private String sixthRiderName;

	private String seventhRiderName;

	private String eightthRiderName;

	private String ninthRiderName;

	private String firstLifeAssuredName;

	private String secondLifeAssuredName;

	private String thirdLifeAssuredName;

	private String fourthLifeAssuredName;

	private String fifthLifeAssuredName;

	private String sixthLifeAssuredName;

	private String seventhLifeAssuredName;

	private String eightthLifeAssuredName;

	private String ninthLifeAssuredName;

	private BigDecimal firstRiderSumAssured;

	private BigDecimal secondRiderSumAssured;

	private BigDecimal thirdRiderSumAssured;

	private BigDecimal fourthRiderSumAssured;

	private BigDecimal fifthRiderSumAssured;

	private BigDecimal sixthRiderSumAssured;

	private BigDecimal seventhRiderSumAssured;

	private BigDecimal eightthRiderSumAssured;

	private BigDecimal ninthRiderSumAssured;

	private Integer firstRiderTerm;

	private Integer secondRiderTerm;

	private Integer thirdRiderTerm;

	private Integer fourthRiderTerm;

	private Integer fifthRiderTerm;

	private Integer sixthRiderTerm;

	private Integer seventhRiderTerm;

	private Integer eightthRiderTerm;

	private Integer ninthRiderTerm;

	private BigDecimal firstRiderInitialPremium;

	private BigDecimal secondRiderInitialPremium;

	private BigDecimal thirdRiderInitialPremium;

	private BigDecimal fourthRiderInitialPremium;

	private BigDecimal fifthRiderInitialPremium;

	private BigDecimal sixthRiderInitialPremium;

	private BigDecimal seventhRiderInitialPremium;

	private BigDecimal eightthRiderInitialPremium;

	private BigDecimal ninthRiderInitialPremium;

	private String productPremiumFrequency;

	private Boolean productPBRider;
	
	private BigDecimal totalInitialPremium;

	private BigDecimal initialPremium;

	private String receiptNumber;

	private String paymentMethods;

	public String insuredDeclarationPlace;

	public String proposerDeclarationPlace;

	public Date insuredDeclarationDateTime;

	public Date paymentDate;

	public Date proposerDeclarationDateTime;

	private Set<ContactPreference> agentContactPreferences;

	public static String pdfReqInput;

	private JSONObject jsonObjectPdfInput;

	/** The payer. */
	private PremiumPayer payer;

	/** The payer person. */
	private Person payerPerson;

	private GeneraliAgent generaliAgent;
	
	private ArrayList<AppianMemoDetails> appianMemoDetails;

	private String applicationNumber;

	private String dateMonthInThai;

	private JSONObject jsonObject;
	
	private JSONObject jsonObject1;

	private Map<String, Map<String, String>> codeValuesMap;

	private String type;

	/* lifestyle insured */

	private String lifestyleQuest1;

	private String lifestyleQuest2;

	private String lifestyleQuest3;

	private String lifestyleQuest4;

	private String lifestyleQuest5;

	private String lifestyleQuest6;

	private String lifestyleQuest7;

	private String lifestyleQuest8;

	private String lifestyleQuest9;

	private String lifestyleQuest10;

	private String lifestyleQuest11;

	private String lifestyleQuest12;

	private String lifestyleQuest13;

	private String lifestyleQuest14;

	private String lifestyleQuest15;

	private String lifestyleQuest16;

	private String lifestyleQuest17;

	private String lifestyleQuest18;

	private String lifestyleQuest19;

	private String lifestyleQuest20;

	private String lifestyleQuest21;

	private String lifestyleQuest22;

	private String lifestyleQuest23;

	private String lifestyleQuest24;

	private String lifestyleQuest25;

	private String lifestyleQuest26;

	private String lifestyleQuest27;

	private String lifestyleQuest28;

	private String lifestyleQuest29;

	private String lifestyleQuest30;

	private String lifestyleQuest31;

	private String lifestyleQuest32;

	private String lifestyleQuest33;

	private String lifestyleQuest34;

	private String lifestyleQuest35;

	private String lifestyleQuest36;

	private String lifestyleQuest37;

	private String lifestyleQuest38;

	/* lifestyle payer */

	private String payLifeQuest1;

	private String payLifeQuest2;

	private String payLifeQuest3;

	private String payLifeQuest4;

	private String payLifeQuest5;

	private String payLifeQuest6;

	private String payLifeQuest7;

	private String payLifeQuest8;

	private String payLifeQuest9;

	private String payLifeQuest10;

	private String payLifeQuest11;

	private String payLifeQuest12;

	private String payLifeQuest13;

	private String payLifeQuest14;

	private String payLifeQuest15;

	private String payLifeQuest16;

	private String payLifeQuest17;

	private String payLifeQuest18;

	private String payLifeQuest19;

	private String payLifeQuest20;

	private String payLifeQuest21;

	private String payLifeQuest22;

	private String payLifeQuest23;

	private String payLifeQuest24;

	private String payLifeQuest25;

	private String payLifeQuest26;

	private String payLifeQuest27;

	private String payLifeQuest28;

	private String payLifeQuest29;

	private String payLifeQuest30;

	private String payLifeQuest31;

	private String payLifeQuest32;

	private String payLifeQuest33;

	private String payLifeQuest34;

	private String payLifeQuest35;

	private String payLifeQuest36;

	private String payLifeQuest37;

	private String payLifeQuest38;

	private String isInsuredSameAsPayer = "";

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GeneraliEappPDFScriptlet.class);

	private static final String ZERO = "0";

	private String spouseName;
	private String spouseSurName;
	private Date insuredIdentityExpireDate;

	private String riderHsextraInsuredName;
	private String riderHsextraTerm;
	private String riderHsextraInitialPremium;
	private String riderHsextraName;
	private String riderHsextraSumAssured;

	private String riderAddextraInsuredName;
	private String riderAddextraTerm;
	private String riderAddextraInitialPremium;
	private String riderAddextraName;
	private String riderAddextraSumAssured;

	private String riderHbextraInsuredName;
	private String riderHbextraTerm;
	private String riderHbextraInitialPremium;
	private String riderHbextraName;
	private String riderHbextraSumAssured;

	private String riderCIextraInsuredName;
	private String riderCIextraTerm;
	private String riderCIextraInitialPremium;
	private String riderCIextraName;
	private String riderCIextraSumAssured;

	private String riderAIInsuredName;
	private String riderAITerm;
	private String riderAIInitialPremium;
	private String riderAIName;
	private String riderAISumAssured;

	private String riderRCCADBInsuredName;
	private String riderRCCADBTerm;
	private String riderRCCADBInitialPremium;
	private String riderRCCADBName;
	private String riderRCCADBSumAssured;

	private String riderADBRiderInsuredName;
	private String riderADBRiderTerm;
	private String riderADBRiderInitialPremium;
	private String riderADBRiderName;
	private String riderADBRiderSumAssured;

	private String riderWPRiderInsuredName;
	private String riderWPRiderTerm;
	private String riderWPRiderInitialPremium;
	private String riderWPRiderName;
	private String riderWPRiderSumAssured;

	private String riderDDRiderInsuredName;
	private String riderDDRiderTerm;
	private String riderDDRiderInitialPremium;
	private String riderDDRiderName;
	private String riderDDRiderSumAssured;

	private String riderRCCAIRiderInsuredName;
	private String riderRCCAIRiderTerm;
	private String riderRCCAIRiderInitialPremium;
	private String riderRCCAIRiderName;
	private String riderRCCAIRiderSumAssured;

	private String riderPBRiderInsuredName;
	private String riderPBRiderTerm;
	private String riderPBRiderInitialPremium;
	private String riderPBRiderName;
	private String riderPBRiderSumAssured;

	private String riderRCCADDRiderRiderInsuredName;
	private String riderRCCADDRiderRiderTerm;
	private String riderRCCADDRiderRiderInitialPremium;
	private String riderRCCADDRiderRiderName;
	private String riderRCCADDRiderRiderSumAssured;
	private PostalAddressContact officeAddress;

	private ElectronicContact payerElectronicContact;
	private Set<ContactPreference> payerContactPreferences;
	private BigDecimal payerHeight;

	private BigDecimal payerWeight;

	private Set<PersonName> payerPersonNames;

	private Set<PersonDetail> payerPersonDetails;

	private Set<Country> payerCountries;

	private Country payerResidenceCountry;

	private Date payerDob;

	private String payerIdentityType;

	private Date payerIdentityDate;

	private String payerIdentityPlace;

	private String payerIdentityNo;

	private IncomeDetail payerIncomeDetail;

	private OccupationDetail payerOccupationDetail;

	private PostalAddressContact payerPostalAddressContact;

	private PostalAddressContact payerPermanentAddress;

	private PostalAddressContact payerOfficeAddress;

	private String payerMobileNumber1;

	private String payerHomeNumber1;

	private String payerOfficeNumber;

	private Person payerperson;

	private String payerspouseName;

	private Date payerIdentityExpireDate;

	private String payerspouseSurName;

	private String payerprefContacttype;

	private String productPremiumAmount;

	private String productPremiumPeriod;

	private List<OccupationDetail> payerOccupationDetails;

	private String creditCardHolderNameSplitOne;

	private String creditCardHolderNameSplitTwo;

	private String BankNameSplitOne;

	private String BankNameSplitTwo;

	private String BranchNameSplitOne;

	private String BranchNameSplitTwo;

	private String etrNumberSplitOne;

	private String etrNumberSplitTwo;

	private String renewalCreditCardHolderName;
	private String renewalBankName;
	private String renewalBranchName;
	private String renewalCashETRNumber;
	private String renewalChequeETRNumber;
	private String renewalCreditCareExpiryDate;

	private String creditCardNumberSplitOne;
	private String creditCardNumberSplitTwo;
	private String renewalCreditCardNumber;
	private String proposerRenewalPaymentMethod;

	private String proposerRefundPaymentMethod;
	private String refundBankName;
	private String refundBranchName;
	private String refundCreditCardHolderName;
	private String refundcreditCardNumber;
	private String refundCashETRNumber;
	private String refundChequeETRNumber;

	private String proposerPaymentMethodRenewalPayment;
	private String proposerPaymentMethodSplitOne;
	private String proposerPaymentMethodSplitTwo;

	private BigDecimal premiumPaidSplitOne;
	private BigDecimal premiumPaidSplitTwo;

	private String writtenAt;
	private String place;
	private Date dateAndTime;
	String IdentityProof;
	
	private String guardianName;
	private String guardianType;
	private String witnessName;
	
	private Map<String,String> relationship;
	
	private Map<String,String> relationshipWithInsured;
	
	private List<String> additionalQuestion;
	
	private String otherAddInfoLine1;
	private String otherAddInfoLine2;
	private String otherAddInfoLine3;
	private String otherAddInfoLine4;
	private String otherAddInfoLine5;
	private String otherAddInfoLine6;
	private String otherAddInfoLine7;
	
	private String firstPolicyNumber;
	private String secondPolicyNumber;
	private String thirdPolicyNumber;
	
	private String previousPolicyQ1;
	private String previousPolicyQ1Other;
	private String previousPolicyQ2;
	private String previousPolicyQ3;
	private String previousPolicyQ4;
	private String previousPolicyQ5;
	private String previousPolicyQ6;
	private String previousPolicyQ7;
	private String previousPolicyQ8;

	private String taxConsent;
	private String taxId;
		
	public void beforeDetailEval() {
		try {
			com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured insured = (com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured) this
					.getFieldValue("insured");
			try {
				if (null != insured) {
					person = (Person) insured.getPlayerParty();
					isInsuredSameAsPayer = insured.getIsInsuredSameAsPayer();
					relationshipWithProposer = insured
							.getRelationshipWithProposer();
					if (null != insured.getDeclares()) {
						writtenAt = insured.getDeclares().getDescription();
						place = insured.getDeclares().getName();
						dateAndTime = (insured.getDeclares()
								.getActualTimePeriod());
					}
				}
			} catch (Exception e) {
				LOGGER.error("Error while getting insured Person "
						+ e.getMessage());
			}
			try {
				payer = (PremiumPayer) this.getFieldValue("payer");
				if (null != payer) {
					payerPerson = (Person) payer.getPlayerParty();
				}
			} catch (Exception e) {
				LOGGER.error("Error while getting fieldValue payer "
						+ e.getMessage());
			}

			/*try {
				agent = (AgreementProducer) this
						.getFieldValue("agreementProducer");
			} catch (Exception e) {
				LOGGER.error("Error while getting fieldValue agent "
						+ e.getMessage());
			}
*/
			try {
				if (null != payerPerson) {
					payerPersonNames = payerPerson.getName();
					payerPersonDetails = payerPerson.getDetail();
					payerContactPreferences = payerPerson.getPreferredContact();
					payerCountries = payerPerson.getNationalityCountry();
					payerResidenceCountry = payerPerson.getResidenceCountry();
					payerDob = payerPerson.getBirthDate();
					if (payerPerson.getHeight() != null
							&& !"".equals(payerPerson.getHeight())) {
						payerHeight = payerPerson.getHeight().getValue();
					}
					if (payerPerson.getWeight() != null
							&& !"".equals(payerPerson.getWeight())) {
						payerWeight = payerPerson.getWeight().getValue();
					}
					payerIdentityType = payerPerson.getIdentityProof();
					if ((payerIdentityType != null)
							&& (payerIdentityType.equalsIgnoreCase(GeneraliConstants.YES))) {
						payerIdentityType = GeneraliConstants.YES;
					} else {
						payerIdentityType = GeneraliConstants.NO;
					}
					payerIdentityDate = payerPerson.getIdentityDate();
					payerIdentityPlace = payerPerson.getIdentityPlace();
					payerIdentityNo = payerPerson.getNationalId();
					IdentityProof = payerPerson.getNationalIdType();
					payerIdentityExpireDate = payerPerson.getIdentityExpDate();
				}
				if (null != payerPersonDetails) {
					List<OccupationDetail> occupationDetailList = new ArrayList<OccupationDetail>();
					for (PersonDetail personDetail : payerPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							incomeDetail = (IncomeDetail) personDetail;
						} else if (personDetail instanceof OccupationDetail) {
							occupationDetailList
									.add((OccupationDetail) personDetail);
							payerOccupationDetails = occupationDetailList;
						}
					}
				}

				if (payerContactPreferences != null) {
					for (ContactPreference contactPreference : payerContactPreferences) {
						ContactPoint contactPoint = contactPreference
								.getPreferredContactPoint();
						if (contactPoint instanceof PostalAddressContact) {
							if (((PostalAddressContact) contactPoint)
									.getAddressNatureCode().equals(
											AddressNatureCodeList.Current)) {
								payerPostalAddressContact = (PostalAddressContact) contactPoint;
							} else if (((PostalAddressContact) contactPoint)
									.getAddressNatureCode().equals(
											AddressNatureCodeList.Permanent)) {
								payerPermanentAddress = (PostalAddressContact) contactPoint;
							} else if (((PostalAddressContact) contactPoint)
									.getAddressNatureCode().equals(
											AddressNatureCodeList.Office)) {
								payerOfficeAddress = (PostalAddressContact) contactPoint;
							}
						} else if (contactPoint instanceof TelephoneCallContact) {
							telephoneCallContact = (TelephoneCallContact) contactPoint;
							if(telephoneCallContact!=null && telephoneCallContact.getTypeCode()!= null){
							if (telephoneCallContact.getTypeCode().equals(
									TelephoneTypeCodeList.Mobile) && telephoneCallContact.getIdentifier() == 1) {
								payerMobileNumber1 = telephoneCallContact
										.getFullNumber();
							} else if (telephoneCallContact.getTypeCode()
									.equals(TelephoneTypeCodeList.Home)) {
								payerHomeNumber1 = telephoneCallContact
										.getFullNumber();
							} else if (telephoneCallContact.getTypeCode()
									.equals(TelephoneTypeCodeList.Business)) {
								payerOfficeNumber = telephoneCallContact
										.getFullNumber();
								 if(!StringUtils.isEmpty(telephoneCallContact.getExtension()))
									{
									 payerOfficeNumber = payerOfficeNumber + GeneraliConstants.EXTENSIONTHAI + telephoneCallContact.getExtension();
									}
							}
							}
						} else if (contactPoint instanceof ElectronicContact) {
							payerElectronicContact = (ElectronicContact) contactPoint;
						}
					}
				}

			} catch (Exception e) {
				LOGGER.error("Error  appointeePerson" + e.getMessage());
			}
			try {
				if (null != person) {
					personNames = person.getName();
					personDetails = person.getDetail();
					contactPreferences = person.getPreferredContact();
					countries = person.getNationalityCountry();
					insuredResidenceCountry = person.getResidenceCountry();
					insuredDob = person.getBirthDate();
					if (person.getHeight() != null
							&& !"".equals(person.getHeight())) {
						insuredHeight = person.getHeight().getValue();
					}
					if (person.getWeight() != null
							&& !"".equals(person.getWeight())) {
						insuredWeight = person.getWeight().getValue();
					}
					insuredIdentityType = person.getIdentityProof();
					if ((insuredIdentityType != null)
							&& (insuredIdentityType
									.equalsIgnoreCase(GeneraliConstants.YES))) {
						insuredIdentityType = GeneraliConstants.YES;
					} else {
						insuredIdentityType = GeneraliConstants.NO;
					}
					insuredIdentityDate = person.getIdentityDate();
					insuredIdentityExpireDate = person.getIdentityExpDate();
					insuredIdentityPlace = person.getIdentityPlace();
					identityNo = person.getNationalId();
				}
			} catch (Exception e) {
				LOGGER.error("Error  appointeePerson" + e.getMessage());
			}

			try {
				insuredPreviousPolicyList = (Set<Insured>) this
						.getFieldValue("insuredPreviousPolicyList");
			} catch (Exception e) {
				LOGGER.error("Error while getting fieldValue insuredPreviousPolicyList "
						+ e.getMessage());
			}
			
			

			if (null != personDetails) {
				List<OccupationDetail> occupationDetailList = new ArrayList<OccupationDetail>();
				for (PersonDetail personDetail : personDetails) {
					if (personDetail instanceof IncomeDetail) {
						incomeDetail = (IncomeDetail) personDetail;
					} else if (personDetail instanceof OccupationDetail) {
						occupationDetailList
								.add((OccupationDetail) personDetail);
						occupationDetail = occupationDetailList;
					}
				}
			}

			if (null != contactPreferences) {
				for (ContactPreference contactPreference : contactPreferences) {
					ContactPoint contactPoint = contactPreference
							.getPreferredContactPoint();
					if (contactPoint instanceof PostalAddressContact) {
						if (((PostalAddressContact) contactPoint)
								.getAddressNatureCode().equals(
										AddressNatureCodeList.Current)) {
							postalAddressContact = (PostalAddressContact) contactPoint;
						} else if (((PostalAddressContact) contactPoint)
								.getAddressNatureCode().equals(
										AddressNatureCodeList.Permanent)) {
							permanentAddress = (PostalAddressContact) contactPoint;
						} else if (((PostalAddressContact) contactPoint)
								.getAddressNatureCode().equals(
										AddressNatureCodeList.Office)) {
							officeAddress = (PostalAddressContact) contactPoint;
						}

					} else if (contactPoint instanceof TelephoneCallContact) {
						telephoneCallContact = (TelephoneCallContact) contactPoint;
						if(telephoneCallContact != null && telephoneCallContact.getTypeCode() != null){
						if (telephoneCallContact.getTypeCode().equals(
								TelephoneTypeCodeList.Mobile) && telephoneCallContact.getIdentifier() == 1) {
							insuredMobileNumber1 = telephoneCallContact.getFullNumber();
						} else if (telephoneCallContact.getTypeCode().equals(
								TelephoneTypeCodeList.Home)) {
							insuredHomeNumber1 = telephoneCallContact.getFullNumber();
						} else if (telephoneCallContact.getTypeCode().equals(
								TelephoneTypeCodeList.Business)) {
							insuredOfficeNumber = telephoneCallContact.getFullNumber();
							if(!StringUtils.isEmpty(telephoneCallContact.getExtension()))
							{
								insuredOfficeNumber = insuredOfficeNumber + GeneraliConstants.EXTENSIONTHAI + telephoneCallContact.getExtension();
							}
						}
						}
					}  else if (contactPoint instanceof ElectronicContact) {
						electronicContact = (ElectronicContact) contactPoint;
					}
				}
			}

			relationship = new HashMap<String, String>();
			relationship.put("Father", "บิดา");
			relationship.put("Mother", "มารดา");
			relationship.put("Spouse", "คู่สมรส");
			relationship.put("Child", "ลูก");
			relationship.put("Sibling", "พี่น้อง");
			relationship.put("Paternal Grand Parents", "ปู่ย่า");
			relationship.put("Maternal Grand Parents", "ตายาย");
			relationship.put("Uncle", "ลุง");
			relationship.put("Aunt", "ป้า");
			relationship.put("Niece / Nephew", "หลานสาว / หลายชาย");
			relationship.put("Others", "อื่นๆ");
			
			relationshipWithInsured = new HashMap<String,String>();
			relationshipWithInsured.put("Parent", "บิดา/มารดา");
			relationshipWithInsured.put("Child", "ลูก");
			relationshipWithInsured.put("Spouse", "สามี/ภรรยา");
			relationshipWithInsured.put("Guardian", "ผู้ปกครอง");
			relationshipWithInsured.put("Other", "อื่นๆ");			
			com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument document = (com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument) this
					.getFieldValue("document");

			if (null != document) {
				appionteeDocuments = document.getPages();

			}
			
			
			try {
				if (pdfReqInput == null)
					pdfReqInput = GeneraliPDFContentReader
							.getPDFContent("pdfTemplates/eAppPdfResourceJson.json");
			} catch (Exception e) {
				LOGGER.error("error pdf json." + e.getMessage());
			}

			JSONParser jsonParser = new JSONParser();
			try {
				if(pdfReqInput != null){
					Object obj1 = jsonParser.parse(pdfReqInput);
					jsonObjectPdfInput = (JSONObject) obj1;
				}
				else{
					jsonObjectPdfInput = new JSONObject();
				}
			} catch (Exception e) {
				LOGGER.error("Out of beforeDetailEval " + e.getMessage());
			}
			
			try {
				InsuranceAgreement insuranceAgreement = (InsuranceAgreement) this
						.getFieldValue("proposal");
				generaliAgent = (GeneraliAgent) this
						.getFieldValue("generaligent");
				type = (String) this.getFieldValue("type");
				codeValuesMap = (Map) this.getFieldValue("mapForLookUpValues");

				if (!type.equals(GeneraliConstants.EAPP)) {
					appianMemoDetails = (ArrayList<AppianMemoDetails>) this
							.getFieldValue("appianMemoDetails");
					dateMonthInThai = (String) this
							.getFieldValue("dateMonthInThai");
					applicationNumber = (String) this.getFieldValue("key21");
					if (appianMemoDetails != null) {
						for (AppianMemoDetails test : appianMemoDetails) {
							if (test.getMemoDetails().getMemoCode()
									.equals("5CF") || test.getMemoDetails().getMemoCode()
									.equals("CTF")||test.getMemoDetails().getMemoCode()
									.equals("XCL")) {
								String json = test.getDetails();
								Object obj = jsonParser.parse(json);
								jsonObject = (JSONObject) obj;
							}
						}
					}
				}
				proposal = insuranceAgreement;
				try {
					if (proposal != null) {
						
							if (proposal.getAgreementExtension() != null) {
							guardianName = proposal.getAgreementExtension().getSpajDeclarationName1();
							guardianType = proposal.getAgreementExtension().getSpajDeclarationName2();
							witnessName  = proposal.getAgreementExtension().getSpajDeclarationName3();
						}
					}
				} catch (Exception e) {
					LOGGER.error("Error while getting ProposalFields " + e.getMessage());
				}

				
				try {
					if (insuranceAgreement != null) {
						TransactionKeys transactionKey = insuranceAgreement
								.getTransactionKeys();
						if (transactionKey != null) {
							spajNumber = transactionKey.getKey21();
							illustrationNumber = transactionKey.getKey24();
							agentCode = transactionKey.getKey11();
						}
					}
				} catch (Exception e) {
					LOGGER.error("Error while getting keys " + e.getMessage());
				}

				if (insuranceAgreement != null
						&& insuranceAgreement.getAgreementExtension() != null) {
					declationQuestions = insuranceAgreement
							.getAgreementExtension().getSelectedOptions();
					agentSignDate = insuranceAgreement.getAgreementExtension()
							.getAgentSignDate();
					agentSignPlace = insuranceAgreement.getAgreementExtension()
							.getAgentSignPlace();
					spajSignDate = insuranceAgreement.getAgreementExtension()
							.getSpajDate();
					SpajSignPlace = insuranceAgreement.getAgreementExtension()
							.getSpajSignPlace();
					if (insuranceAgreement.getAgreementExtension()
							.getCommissionRateOne() != null
							&& !"".equals(insuranceAgreement
									.getAgreementExtension()
									.getCommissionRateOne())) {
						commisionRateOne = insuranceAgreement
								.getAgreementExtension().getCommissionRateOne()
								.toString();
					}
					if (insuranceAgreement.getAgreementExtension()
							.getCommissionRateTwo() != null
							&& !"".equals(insuranceAgreement
									.getAgreementExtension()
									.getCommissionRateTwo())) {
						commisionRateTwo = insuranceAgreement
								.getAgreementExtension().getCommissionRateTwo()
								.toString();
					}
					partnerCode = insuranceAgreement.getAgreementExtension()
							.getPartnerCode();
					commisionAgent = insuranceAgreement.getAgreementExtension()
							.getCommissionAgent();

				}

				if (insuranceAgreement != null) {
					for (Premium premium : insuranceAgreement
							.getAgreementPremium()) {
						if ((GeneraliConstants.REGULAR_PREMIUM).equals(premium.getNatureCode()
								.name())) {

							for (FinancialScheduler financialScheduler : premium
									.getAttachedFinancialScheduler()) {

								if (financialScheduler.getPaymentType() != null) {
									if ((GeneraliConstants.SPLIT_ONE).equals(
											premium.getDescription())) {

										proposerPaymentMethodSplitOne = financialScheduler
												.getDescription();
										if (financialScheduler
												.getRelatedBankAccount() != null) {
											BankNameSplitOne = financialScheduler
													.getRelatedBankAccount()
													.getBankName();
											BranchNameSplitOne = financialScheduler
													.getRelatedBankAccount()
													.getBranchName();
										}
										if(premium.getAmount() != null){
											premiumPaidSplitOne = premium
													.getAmount().getAmount();
										}

									} else {

										proposerPaymentMethodSplitTwo = financialScheduler
												.getDescription();
										if (financialScheduler
												.getRelatedBankAccount() != null) {
											BankNameSplitTwo = financialScheduler
													.getRelatedBankAccount()
													.getBankName();
											BranchNameSplitTwo = financialScheduler
													.getRelatedBankAccount()
													.getBranchName();
										}
										
										if(premium.getAmount() != null){
											premiumPaidSplitTwo = premium
													.getAmount().getAmount();
										}
									}

									if (FinancialMediumTypeCodeList.CreditCard.toString().equals(financialScheduler
													.getPaymentType())) {
										CreditCardPayment creditCardPayment = (CreditCardPayment) financialScheduler
												.getPaymentMeans();
										if(creditCardPayment != null){
											if ((GeneraliConstants.SPLIT_ONE).equals(premium.getDescription())) {
												creditCardHolderNameSplitOne = creditCardPayment.getCardholderName() != null ? 
														creditCardPayment.getCardholderName().getFullName():StringUtils.EMPTY;
														creditCardNumberSplitOne = creditCardPayment
																.getCardNumber();

											} else {
												creditCardHolderNameSplitTwo = creditCardPayment.getCardholderName() != null ? 
														creditCardPayment.getCardholderName().getFullName():StringUtils.EMPTY;
														creditCardNumberSplitTwo = creditCardPayment
																.getCardNumber();
											}
										}
									} else if (FinancialMediumTypeCodeList.Cash.toString().equals(financialScheduler
													.getPaymentType())) {
										CashPayment cashPayment = (CashPayment) financialScheduler
												.getPaymentMeans();
										if(cashPayment != null){
											if ((GeneraliConstants.SPLIT_ONE).equals(premium.getDescription())) {
												etrNumberSplitOne = cashPayment
														.getEtrNumber();
											} else {
												etrNumberSplitTwo = cashPayment
														.getEtrNumber();
											}
										}
									} else if (FinancialMediumTypeCodeList.PersonalCheck.toString().equals(financialScheduler
													.getPaymentType())) {
										CheckPayment chequePayment = (CheckPayment) financialScheduler
												.getPaymentMeans();
										if(chequePayment!=null){
											if ((GeneraliConstants.SPLIT_ONE).equals(premium.getDescription())) {
												etrNumberSplitOne = chequePayment
														.getEtrNumber();
											} else {
												etrNumberSplitTwo = chequePayment
														.getEtrNumber();
											}
										}
									}
								}
							}
						} else if (((GeneraliConstants.RENEWAL_PREMIUM).equals(premium
								.getNatureCode().name()))) {
							for (FinancialScheduler financialScheduler : premium
									.getAttachedFinancialScheduler()) {
								proposerPaymentMethodRenewalPayment = financialScheduler
										.getPaymentMethodCode().name();
								if (financialScheduler.getPaymentMeans() != null) {
									proposerRenewalPaymentMethod = financialScheduler
											.getPaymentMeans().toString();
									if (financialScheduler
											.getRelatedBankAccount() != null) {
										renewalBankName = financialScheduler
												.getRelatedBankAccount()
												.getBankName();
										renewalBranchName = financialScheduler
												.getRelatedBankAccount()
												.getBranchName();
									}
									if (FinancialMediumTypeCodeList.CreditCard.equals(financialScheduler
											.getPaymentMethodCode())) {
										CreditCardPayment creditCardPayment = (CreditCardPayment) financialScheduler
												.getPaymentMeans();
										if (creditCardPayment != null) {
											renewalCreditCardHolderName = creditCardPayment.getCardholderName() != null ? 
													creditCardPayment.getCardholderName().getFullName():StringUtils.EMPTY;
											renewalCreditCardNumber = creditCardPayment
													.getCardNumber();
											renewalCreditCareExpiryDate = creditCardPayment
													.getExpiryDate().toString();
										}
									} else if (FinancialMediumTypeCodeList.Cash.equals(financialScheduler
															.getPaymentMethodCode())) {
										CashPayment cashPayment = (CashPayment) financialScheduler
												.getPaymentMeans();
										if (cashPayment != null) {
											renewalCashETRNumber = cashPayment
													.getEtrNumber();
										}
									} else if (FinancialMediumTypeCodeList.PersonalCheck.equals(financialScheduler
													.getPaymentMethodCode())) {
										CheckPayment chequePayment = (CheckPayment) financialScheduler
												.getPaymentMeans();
										if (chequePayment != null) {
											renewalChequeETRNumber = chequePayment
													.getEtrNumber();
										}
									}
								}
							}
						}
					}
				}
				if (insuranceAgreement != null) {
					for (FinancialScheduler financialScheduler : insuranceAgreement
							.getRefundDetails().getAttachedFinancialScheduler()) {

						if (financialScheduler.getPaymentMethodCode() != null) {
							proposerRefundPaymentMethod = financialScheduler
									.getPaymentMethodCode().name();
							if (financialScheduler.getRelatedBankAccount() != null) {
								refundBankName = financialScheduler
										.getRelatedBankAccount().getBankName();
								refundBranchName = financialScheduler
										.getRelatedBankAccount()
										.getBranchName();
							}
							if (financialScheduler
									.getPaymentMethodCode()
									.equals(FinancialMediumTypeCodeList.DebitAccount)) {
								BankAccount bankAccount = financialScheduler
										.getRelatedBankAccount();
								if (bankAccount != null) {
									refundcreditCardNumber = bankAccount
											.getAccountIdentifier().toString();
									for (Party party : bankAccount.getOwner()) {
										Person person = (Person) party;
										for (PersonName personName : person
												.getName()) {
											refundCreditCardHolderName = personName
													.getGivenName() != null ? personName
													.getGivenName()
													: StringUtils.EMPTY;
										}
									}
								}
							} else if (financialScheduler.getPaymentMeans()
									.equals(FinancialMediumTypeCodeList.Cash)) {
								CashPayment cashPayment = (CashPayment) financialScheduler
										.getPaymentMeans();
								if (cashPayment != null) {
									refundCashETRNumber = cashPayment
											.getEtrNumber();
								}
							}
						}

					}
				}
				/* Product Details */
				if (insuranceAgreement != null) {
					ArrayList<Coverage> riderList = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA1 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA2 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA3 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA4 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA5 = new ArrayList<Coverage>();
					ArrayList<RiderTableForEapp> riderdeatilslist = new ArrayList();
					if (insuranceAgreement.getIncludesCoverage() != null
							&& insuranceAgreement.getIncludesCoverage().size() > 0) {
						int riderCount = 0;

						for (Coverage coverage : insuranceAgreement
								.getIncludesCoverage()) {
							if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("BasicCoverage")) {
								riderList.add(coverage);
								break;

							}
						}

						for (Coverage coverage : insuranceAgreement
								.getIncludesCoverage()) {
							if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("Rider")) {
								if (coverage.getInsuredType() != null) {
									if (coverage.getInsuredType().equals("LA1")) {
										riderListLA1.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA2")) {
										riderListLA2.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA3")) {
										riderListLA3.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA4")) {
										riderListLA4.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA5")) {
										riderListLA5.add(coverage);

									}
								}
							}
						}

						if (riderListLA1 != null && riderListLA1.size() > 0) {
							riderList.addAll(riderNames(riderListLA1));
						}
						if (riderListLA2 != null && riderListLA2.size() > 0) {
							riderList.addAll(riderNames(riderListLA2));
						}
						if (riderListLA3 != null && riderListLA3.size() > 0) {
							riderList.addAll(riderNames(riderListLA3));
						}
						if (riderListLA4 != null && riderListLA4.size() > 0) {
							riderList.addAll(riderNames(riderListLA4));
						}
						if (riderListLA5 != null && riderListLA5.size() > 0) {
							riderList.addAll(riderNames(riderListLA5));
						}
						for (Coverage coverage : riderList) {
							RiderTableForEapp riderData = new RiderTableForEapp();

							if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("BasicCoverage")) {
								// riderList.add(coverage);
								if (coverage.getProductType() != null) {
									// productName=
									// coverage.getProductCode().getCode();
									productName = coverage.getMarketingName();

								}
								if (coverage.getSumAssured() != null) {
									productSumAssured = coverage
											.getSumAssured().getAmount();

								}
								productPremiumAmount = coverage
										.getPremiumAmount();

								productPolicyTerm = coverage.getPolicyTerm();

								productPremiumPeriod = coverage
										.getPremiumPeriod();
								
								productPBRider=coverage.getHasPBRider();

								productPremiumTerm = coverage.getPremiumTerm();

								if (coverage.getPremiumFrequency() != null) {
									productPremiumFrequency = coverage
											.getPremiumFrequency().name();
								}
								if (coverage.getPremium() != null
										&& coverage.getPremium().size() > 0) {
									for (Premium premium : coverage
											.getPremium()) {
										if (premium.getNatureCode() != null
												&& premium
														.getNatureCode()
														.name()
														.equalsIgnoreCase(
																"TotalInitialPremium")) {
											if (premium.getAmount() != null
													&& premium.getAmount()
															.getAmount() != null) {
												totalInitialPremium = premium
														.getAmount()
														.getAmount();
											}
										} else if (premium.getNatureCode() != null
												&& premium
														.getNatureCode()
														.name()
														.equalsIgnoreCase(
																"TotalPremium")) {
											if (premium.getAmount() != null
													&& premium.getAmount()
															.getAmount() != null) {
												initialPremium = premium
														.getAmount()
														.getAmount();

											}
										}
									}
								}
							} else if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("Rider")) {
								String lifeAssuredName = "";
								String riderName;
								Integer riderTerm;
								BigDecimal riderInitialPremium;
								BigDecimal riderSA;

								// riderName = coverage.getName();
								riderName = coverage.getTypeName();
								riderData.setRiderName(riderName);

								if (coverage.getSumAssured() != null) {
									riderSA = coverage.getSumAssured()
											.getAmount();
									riderData.setRiderSumAssured(riderSA);

								}
								riderTerm = coverage.getRiderTerm();
								// Value should be Blank in PDF;
								if ("OutPatient".equals(coverage.getName())
										|| "Dental".equals(coverage.getName())
										|| "HIP1"
												.equals(coverage.getTypeName())
										|| "HIP2"
												.equals(coverage.getTypeName())
										|| "HIP3"
												.equals(coverage.getTypeName())) {
									riderTerm = 0;
								}
								riderData.setRiderTerm(riderTerm);

								if (coverage.getPremium() != null
										&& coverage.getPremium().size() > 0) {
									for (Premium premium : coverage
											.getPremium()) {
										if (premium.getNatureCode() != null
												&& premium
														.getNatureCode()
														.name()
														.equalsIgnoreCase(
																"MinimumPremium")) {
											if (premium.getAmount() != null
													&& premium.getAmount()
															.getAmount() != null) {
												riderInitialPremium = premium
														.getAmount()
														.getAmount();
												// Value should be Blank in PDF;
												if ("OutPatient"
														.equals(coverage
																.getName())
														|| "Dental"
																.equals(coverage
																		.getName())) {
													BigDecimal bd = new BigDecimal(
															"0");
													riderInitialPremium = bd;
												}
												riderData
														.setRiderInitialPremium(riderInitialPremium);
											}

										}
									}
								}
								if (coverage.getInsuredType() != null) {
									if (coverage.getInsuredType()
											.equalsIgnoreCase("LA1")) {
										lifeAssuredName = getInsuredFullName();
									}
								}
								riderData.setLifeAssuredName(lifeAssuredName);
								riderdeatilslist.add(riderData);
							}
							// riderCount++;
						}

						/* rider Details */

						for (int i = 0; i < riderdeatilslist.size(); i++) {
							if (riderdeatilslist.get(i) != null) {

								RiderTableForEapp riderdetails = new RiderTableForEapp();
								riderdetails = riderdeatilslist.get(i);

								if ((riderdetails.getRiderName()
										.equalsIgnoreCase("HSRiders"))
										|| (riderdetails.getRiderName()
												.equalsIgnoreCase("HSRider"))) {
									riderHsextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderHsextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderHsextraInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderHsextraName = riderdetails
											.getRiderName();
									riderHsextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if ((riderdetails.getRiderName()
										.equalsIgnoreCase("ADDRiders"))
										|| (riderdetails.getRiderName()
												.equalsIgnoreCase("ADDRider"))) {
									riderAddextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderAddextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderAddextraInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderAddextraName = riderdetails
											.getRiderName();
									riderAddextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if ((riderdetails.getRiderName()
										.equalsIgnoreCase("HBRiders"))
										|| (riderdetails.getRiderName()
												.equalsIgnoreCase("HBRider"))) {
									riderHbextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderHbextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderHbextraInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderHbextraName = riderdetails
											.getRiderName();
									riderHbextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("CIRider")) {
									riderCIextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderCIextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderCIextraInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderCIextraName = riderdetails
											.getRiderName();
									riderCIextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("AIRider")) {
									riderAIInsuredName = riderdetails
											.getLifeAssuredName();
									riderAITerm = (riderdetails.getRiderTerm())
											.toString();
									riderAIInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderAIName = riderdetails.getRiderName();
									riderAISumAssured = String.format("%,.2f",
											(riderdetails.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("RCCADBRider")) {
									riderRCCADBInsuredName = riderdetails
											.getLifeAssuredName();
									riderRCCADBTerm = (riderdetails
											.getRiderTerm()).toString();
									riderRCCADBInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderRCCADBName = riderdetails
											.getRiderName();
									riderRCCADBSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("ADBRider")) {
									riderADBRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderADBRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderADBRiderInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderADBRiderName = riderdetails
											.getRiderName();
									riderADBRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("WPRider")) {
									riderWPRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderWPRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderWPRiderInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderWPRiderName = riderdetails
											.getRiderName();
									riderWPRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("DDRider")) {
									riderDDRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderDDRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderDDRiderInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderDDRiderName = riderdetails
											.getRiderName();
									riderDDRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("RCCAIRider")) {
									riderRCCAIRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderRCCAIRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderRCCAIRiderInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderRCCAIRiderName = riderdetails
											.getRiderName();
									riderRCCAIRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("RCCADDRider")) {
									riderRCCADDRiderRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderRCCADDRiderRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderRCCADDRiderRiderInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderRCCADDRiderRiderName = riderdetails
											.getRiderName();
									riderRCCADDRiderRiderSumAssured = String
											.format("%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("PBRider")) {
									riderPBRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderPBRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderPBRiderInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderPBRiderName = riderdetails
											.getRiderName();
									riderPBRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								}
							}
						}
					}
					/* Payment Details */
					try {
						if (insuranceAgreement != null) {
							if (insuranceAgreement.getAgreementPremium() != null
									&& insuranceAgreement.getAgreementPremium()
											.size() > 0) {
								for (Premium premium : insuranceAgreement
										.getAgreementPremium()) {
									if (premium.getNatureCode() != null
											&& premium
													.getNatureCode()
													.name()
													.equalsIgnoreCase(
															"RegularPremium")) {
										paymentDate = premium
												.getEffectivePeriod()
												.getEndDateTime();
										if (premium
												.getAttachedFinancialScheduler() != null
												&& premium
														.getAttachedFinancialScheduler()
														.size() > 0) {
											for (FinancialScheduler scheduler : premium
													.getAttachedFinancialScheduler()) {
												receiptNumber = scheduler
														.getTransactionRefNumber();
												paymentMethods = scheduler
														.getDescription();
											}
										}
									}
								}
							}
						}
					} catch (Exception e) {
						LOGGER.error("ScriptletSample Error :while getting payment details"
								+ e);
					}

				}
			} catch (Exception e) {
				LOGGER.error("ScriptletSample Error :while getting field value Payment"
						+ e);
			}
			
			
			
			/* payer - questionnairee */
			try {
				if (payer != null) {
					payerUserSelections = payer.getSelectedOptions();
					if (payerUserSelections != null
							&& payerUserSelections.size() > 0) {
						for (UserSelection payUserselections : payerUserSelections) {
							if (payUserselections != null
									&& payUserselections.getQuestionnaireType()
											.name()
											.equalsIgnoreCase("LifeStyle")
									&& productPBRider) {

								if (payUserselections.getQuestionId() == 211000) {
									payLifeQuest1 = payUserselections
											.getDetailedInfo();
								} else if (payUserselections.getQuestionId() == 211001) {
									payLifeQuest2 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 211002) {
									payLifeQuest3 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 211003) {
									payLifeQuest4 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 211004) {
									payLifeQuest5 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 211005) {
									payLifeQuest6 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 211006) {
									payLifeQuest7 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 212000) {
									payLifeQuest8 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 212001) {
									payLifeQuest9 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 212002) {
									payLifeQuest10 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 212003) {
									payLifeQuest11 = payUserselections
											.getDetailedInfo();
									if(!StringUtils.isBlank(payLifeQuest11))
									{
										payLifeQuest11=this.dateFormatter(payLifeQuest11);
									}

								} else if (payUserselections.getQuestionId() == 213000) {
									payLifeQuest12 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 213001) {
									payLifeQuest13 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 214000) {
									payLifeQuest14 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 214001) {
									payLifeQuest15 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 214002) {
									payLifeQuest16 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 214003) {
									payLifeQuest17 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 214004) {
									payLifeQuest18 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 215000) {
									payLifeQuest19 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 215001) {
									payLifeQuest20 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 215002) {
									payLifeQuest21 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 215003) {
									payLifeQuest22 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 215004) {
									payLifeQuest23 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 216000) {
									payLifeQuest24 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 216001) {
									payLifeQuest25 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 216002) {
									payLifeQuest26 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 216003) {
									payLifeQuest27 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 216004) {
									payLifeQuest28 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 217000) {
									payLifeQuest29 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 217001) {
									if (payUserselections.getDetailedInfo()
											.equals("0")) {
										payLifeQuest30 = "-";
									} else {
										payLifeQuest30 = payUserselections
												.getDetailedInfo();
									}

								} else if (payUserselections.getQuestionId() == 217002) {
									if (payUserselections.getDetailedInfo()
											.equals("0")) {
										payLifeQuest31 = "-";
									} else {
										payLifeQuest31 = payUserselections
												.getDetailedInfo();
									}

								} else if (payUserselections.getQuestionId() == 217003) {
									payLifeQuest32 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 218000) {
									payLifeQuest33 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 218001) {
									payLifeQuest34 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 218002) {
									payLifeQuest35 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 218003) {
									payLifeQuest36 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 219000) {
									payLifeQuest37 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 219001) {
									payLifeQuest38 = payUserselections
											.getDetailedInfo();
								}
							} else if (payUserselections != null
									&& payUserselections.getQuestionnaireType() != null
									&& payUserselections
											.getQuestionnaireType()
											.name()
											.equalsIgnoreCase(
													"Additional_Basic") && productPBRider) {
								if (payUserselections.getQuestionId() == 241000) {
									payadditionalInfo = payUserselections
											.getDetailedInfo();
								}
							}

							else if (payUserselections != null
									&& payUserselections.getQuestionnaireType() != null
									) 
							if( payUserselections.getQuestionnaireType()
											.name().equalsIgnoreCase("Other")
									&& productPBRider){
								if (payUserselections.getQuestionId() == 221001) {
									illnessTreatmentPQ1 = payUserselections
											.getDetailedInfo();
								} 
								else if (payUserselections.getQuestionId() == 221011) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.HYPERTENSION.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep1 = GeneraliConstants.THAI_HYPERTENSION;
										}
										if(GeneraliConstants.HEARTDISEASE.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep2 = GeneraliConstants.THAI_HEARTDISEASE;
										}
										if(GeneraliConstants.CORONHEARTDISEASE.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep3 = GeneraliConstants.THAI_CORONHEARTDISEASE;
										}
										if(GeneraliConstants.CARDIO.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep4 = GeneraliConstants.THAI_CARDIO;
										}
										if(GeneraliConstants.CEREBRO.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep5 = GeneraliConstants.THAI_CEREBRO;
										}
										if(GeneraliConstants.PARALYSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep6 = GeneraliConstants.THAI_PARALYSIS;
										}
										if(GeneraliConstants.DIABETICS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep7 = GeneraliConstants.THAI_DIABETICS;
										}
										if(GeneraliConstants.THYROID.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep8 = GeneraliConstants.THAI_THYROID;
										}
									}
								}
								else if (payUserselections.getQuestionId() == 2210121) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ111 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210131) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ112 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210141) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ113 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2210151) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ114 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210122) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ121 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210132) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ122 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210142) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ123 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2210152) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ124 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210123) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ131 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210133) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ132 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210143) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ133 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2210153) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ134 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210124) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ141 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210134) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ142 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210144) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ143 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2210154) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ144 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210125) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ151 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210135) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ152 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210145) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ153 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2210155) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ154 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210126) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ161 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210136) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ162 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210146) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ163 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2210156) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ164 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210127) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ171 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210137) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ172 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210147) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ173 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2210157) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ174 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210128) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ181 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210138) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ182 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2210148) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ183 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2210158) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ184 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 221020) {
									illnessTreatmentPQ2 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 221021) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.CANCER.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep9 = GeneraliConstants.THAI_CANCER;
										}
										if(GeneraliConstants.LYMPH.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep10 = GeneraliConstants.THAI_LYMPH;
										}
										if(GeneraliConstants.TUMOR.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep11 = GeneraliConstants.THAI_TUMOR;
										}
										if(GeneraliConstants.MASSCYST.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep12 = GeneraliConstants.THAI_MASSCYST;
										}
									}
								} 

							 else if (payUserselections.getQuestionId() == 2210221) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ211 = GeneraliConstants.THAI_DATE+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210231) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ212 = GeneraliConstants.TRTMETHOD+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210241) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ213 = GeneraliConstants.CURSYMPTOM+payUserselections
											.getDetailedInfo().concat("\n");

								}
							} else if (payUserselections.getQuestionId() == 2210251) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ214 = GeneraliConstants.HOSPITAL+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210222) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ221 = GeneraliConstants.THAI_DATE+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210232) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {
									if (!StringUtils
											.isBlank(payUserselections
													.getDetailedInfo()))
										illnessTreatmentPQ222 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(
														"  ");

								}
							} else if (payUserselections.getQuestionId() == 2210242) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ223 = GeneraliConstants.CURSYMPTOM+payUserselections
											.getDetailedInfo().concat("\n");

								}
							} else if (payUserselections.getQuestionId() == 2210252) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ224 = GeneraliConstants.HOSPITAL+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210223) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ231 = GeneraliConstants.THAI_DATE+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210233) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ232 = GeneraliConstants.TRTMETHOD+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210243) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ233 = GeneraliConstants.CURSYMPTOM+payUserselections
											.getDetailedInfo().concat("\n");

								}
							} else if (payUserselections.getQuestionId() == 2210253) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ234 = GeneraliConstants.HOSPITAL+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210224) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ241 = GeneraliConstants.THAI_DATE+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210234) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ242 = GeneraliConstants.TRTMETHOD+payUserselections
											.getDetailedInfo().concat(",  ");

								}
							} else if (payUserselections.getQuestionId() == 2210244) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {
									if (!StringUtils
											.isBlank(payUserselections
													.getDetailedInfo()))
										illnessTreatmentPQ243 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

								}
							} else if (payUserselections.getQuestionId() == 2210254) {
								if (!StringUtils.isBlank(payUserselections
										.getDetailedInfo())) {

									illnessTreatmentPQ244 = GeneraliConstants.HOSPITAL+payUserselections
											.getDetailedInfo().concat(",  ");
								}
							} else if (payUserselections.getQuestionId() == 221030) {
									illnessTreatmentPQ3 = payUserselections
											.getDetailedInfo();

								} 
								
								else if (payUserselections.getQuestionId() == 221031) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.PANCREATITIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep13 = GeneraliConstants.THAI_PANCREATITIS;
										}
										if(GeneraliConstants.KIDNEY.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep14 = GeneraliConstants.THAI_KIDNEY;
										}
										if(GeneraliConstants.JAUNDICE.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep15 = GeneraliConstants.THAI_JAUNDICE;
										}
										if(GeneraliConstants.SPLENOMEGALY.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep16 = GeneraliConstants.THAI_SPLENOMEGALY;
										}
										if(GeneraliConstants.PEPTICULCER.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep17 = GeneraliConstants.THAI_PEPTICULCER;
										}
										if(GeneraliConstants.LIVER.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep18 = GeneraliConstants.THAI_LIVER;
										}
										if(GeneraliConstants.ALOCHOLISM.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep19 = GeneraliConstants.THAI_ALOCHOLISM;
										}
									}
								} 
								else if (payUserselections.getQuestionId() == 2210321) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ311 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210331) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ312 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210341) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ313 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210351) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ314 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210322) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ321 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210332) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ322 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210342) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ323 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210352) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ324 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210323) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ331 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210333) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ332 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210343) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ333 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210353) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ334 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210324) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ341 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210334) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ342 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210344) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ343 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210354) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ344 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210325) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ351 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210335) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ352 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210345) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ353 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210355) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ354 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210326) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ361 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210336) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ362 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210346) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ363 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210356) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ364 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210327) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ371 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210337) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ372 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210347) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ373 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210357) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ374 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								}  else if (payUserselections.getQuestionId() == 221040) {
									illnessTreatmentPQ4 = payUserselections
											.getDetailedInfo();

								}
								
								else if (payUserselections.getQuestionId() == 221041) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.PNEUMONIA.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep20 = GeneraliConstants.THAI_PNEUMONIA;
										}
										if(GeneraliConstants.TUBERCULOSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep21 = GeneraliConstants.THAI_TUBERCULOSIS;
										}
										if(GeneraliConstants.ASTHMA.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep22 = GeneraliConstants.THAI_ASTHMA;
										}
										if(GeneraliConstants.PULMONARY.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep23 = GeneraliConstants.THAI_PULMONARY;
										}
										if(GeneraliConstants.EMPHYSEMA.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep24 = GeneraliConstants.THAI_EMPHYSEMA;
										}
										if(GeneraliConstants.SLEEPAPNEA.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep25 = GeneraliConstants.THAI_SLEEPAPNEA;
										}
									}
								} 
								else if (payUserselections.getQuestionId() == 2210421) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ411 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210431) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ412 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210441) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ413 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210451) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ414 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210422) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ421 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210432) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ422 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210442) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ423 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210452) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ424 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210423) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ431 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210433) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ432 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210443) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ433 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210453) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ434 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210424) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ441 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210434) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ442 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210444) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ443 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210454) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ444 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210425) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ451 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210435) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ452 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210445) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ453 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210455) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ454 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210426) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ461 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210436) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ462 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210446) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ463 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210456) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ464 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 221050) {
									illnessTreatmentPQ5 = payUserselections
											.getDetailedInfo();

								}
								else if (payUserselections.getQuestionId() == 221051) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.IMPAIREDVISION.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep26 = GeneraliConstants.THAI_IMPAIREDVISION;
										}
										if(GeneraliConstants.RETINA.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep27 = GeneraliConstants.THAI_RETINA;
										}
										if(GeneraliConstants.GLAUCOMA.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep28 = GeneraliConstants.THAI_GLAUCOMA;
										}
									}
								} 
								else if (payUserselections.getQuestionId() == 2210521) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ511 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210531) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ512 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210541) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ513 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210551) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ514 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210522) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ521 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210532) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ522 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210542) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ523 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210552) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ524 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210523) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ531 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210533) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ532 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210543) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ533 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210553) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ534 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 221060) {
									illnessTreatmentPQ6 = payUserselections
											.getDetailedInfo();

								}
								
								else if (payUserselections.getQuestionId() == 221061) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.PARKINSONS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep29 = GeneraliConstants.THAI_PARKINSONS;
										}
										if(GeneraliConstants.ALZHEIMERS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep30 = GeneraliConstants.THAI_ALZHEIMERS ;
										}
										if(GeneraliConstants.EPILEPSY.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep31 = GeneraliConstants.THAI_EPILEPSY;
										}
									}
								}
								else if (payUserselections.getQuestionId() == 2210621) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ611 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210631) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ612 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210641) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ613 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210651) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ614 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210622) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ621 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210632) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ622 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210642) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ623 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210652) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ624 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210623) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ631 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210633) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ632 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210643) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ633 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210653) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ634 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 221070) {
									illnessTreatmentPQ7 = payUserselections
											.getDetailedInfo();

								}
								
								else if (payUserselections.getQuestionId() == 221071) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.ARTHRITIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep32 = GeneraliConstants.THAI_ARTHRITIS;
										}
										if(GeneraliConstants.GOUT.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep33 = GeneraliConstants.THAI_GOUT;
										}
										if(GeneraliConstants.SLE.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep34 = GeneraliConstants.THAI_SLE;
										}
										if(GeneraliConstants.SCLERODERMA.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep35 = GeneraliConstants.THAI_SCLERODERMA;
										}
										if(GeneraliConstants.BLOODDISEASE.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep36 = GeneraliConstants.THAI_BLOODDISEASE;
										}
									}
								} 
								else if (payUserselections.getQuestionId() == 2210721) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ711 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210731) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ712 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210741) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ713 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210751) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ714 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210722) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ721 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210732) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ722 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210742) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ723 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210752) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ724 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210723) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ731 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210733) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ732 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210743) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ733 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210753) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ734 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210724) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ741 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210734) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ742 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210744) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ743 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210754) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ744 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210725) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ751 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210735) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ752 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210745) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ753 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210755) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ754 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 221080) {
									illnessTreatmentPQ8 = payUserselections
											.getDetailedInfo();

								}  
								else if (payUserselections.getQuestionId() == 221081) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.PSYCHOSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep37 = GeneraliConstants.THAI_PSYCHOSIS;
										}
										if(GeneraliConstants.NEUROSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep38 = GeneraliConstants.THAI_NEUROSIS;
										}
										if(GeneraliConstants.DEPRESSION.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep39 = GeneraliConstants.THAI_DEPRESSION;
										}
										if(GeneraliConstants.DOWNS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep40 = GeneraliConstants.THAI_DOWNS;
										}
										if(GeneraliConstants.PHYSICALDIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep41 = GeneraliConstants.THAI_PHYSICALDIS;
										}
									}
								}
								else if (payUserselections.getQuestionId() == 2210821) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ811 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210831) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ812 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210841) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ813 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210851) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ814 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210822) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ821 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210832) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ822 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210842) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ823 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210852) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ824 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210823) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ831 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210833) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ832 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210843) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ833 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210853) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ834 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210824) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ841 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210834) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ842 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210844) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ843 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210854) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ844 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210825) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ851 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210835) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ852 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210845) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ853 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210855) {
									if (!StringUtils.isBlank(payUserselections
											.getDetailedInfo())) {
										illnessTreatmentPQ854 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 221090) {
									illnessTreatmentPQ9 = payUserselections
											.getDetailedInfo();

								}
								
								else if (payUserselections.getQuestionId() == 221091) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.AIDS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep42 = GeneraliConstants.THAI_AIDS;
										}
										if(GeneraliConstants.VENEREAL.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep43 = GeneraliConstants.THAI_VENEREAL;
										}
									}
								}
								else if (payUserselections.getQuestionId() == 2210921) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ911 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210931) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ912 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210941) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ913 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210951) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ914 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210922) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ921 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210932) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ922 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2210942) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ923 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2210952) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ924 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 222010) {
									illnessTreatmentPQ10 = payUserselections
											.getDetailedInfo();

								}
								
								else if (payUserselections.getQuestionId() == 222011) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.CHESTPAIN.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep44 = GeneraliConstants.THAI_CHESTPAIN;
										}
										if(GeneraliConstants.PALPITATION.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep45 = GeneraliConstants.THAI_PALPITATION;
										}
										if(GeneraliConstants.ABNORFATIGUE.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep46 = GeneraliConstants.THAI_ABNORFATIGUE;
										}
										if(GeneraliConstants.MUSCWEAK.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep47 = GeneraliConstants.THAI_MUSCWEAK;
										}
										if(GeneraliConstants.ABNORPHYMOV.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep48 = GeneraliConstants.THAI_ABNORPHYMOV;
										}
										if(GeneraliConstants.LOSSSENSNEU.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep49 = GeneraliConstants.THAI_LOSSSENSNEU;
										}
									}
								}
								else if (payUserselections.getQuestionId() == 2220121) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1011 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220131) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1012 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220141) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1013 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2220151) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1014 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220122) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1021 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220132) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1022 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220142) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1023 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220152) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1024 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220123) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1031 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220133) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1032 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220143) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1033 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220153) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1034 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220124) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1041 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220134) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1042 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220144) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1043 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220154) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1044 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220125) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1051 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220135) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1052 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220145) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1053 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220155) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1054 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								}  else if (payUserselections.getQuestionId() == 2220126) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1061 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220136) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1062 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220146) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1063 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220156) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1064 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 222020) {
									illnessTreatmentPQ11 = payUserselections
											.getDetailedInfo();

								}
								
								else if (payUserselections.getQuestionId() == 222021) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.CHRONICSTOMACHACHE.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep50 = GeneraliConstants.THAI_CHRONICSTOMACHACHE;
										}
										if(GeneraliConstants.HEMATEMESIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep51 = GeneraliConstants.THAI_HEMATEMESIS;
										}
										if(GeneraliConstants.DROPSY.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep52 = GeneraliConstants.THAI_DROPSY;
										}
										if(GeneraliConstants.CHRONICDIARRHEA.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep53 = GeneraliConstants.THAI_CHRONICDIARRHEA;
										}
										if(GeneraliConstants.HEMATURIA.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep54 = GeneraliConstants.THAI_HEMATURIA;
										}
										if(GeneraliConstants.CHRONICCOUGH.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep55 = GeneraliConstants.THAI_CHRONICCOUGH;
										}
										if(GeneraliConstants.HEMOPTYSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep56 =GeneraliConstants.THAI_HEMOPTYSIS;
										}
									}
								}
								else if (payUserselections.getQuestionId() == 2220221) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1111 =GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220231) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1112 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220241) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1113 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220251) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1114 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220222) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1121 =GeneraliConstants.THAI_DATE+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220232) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1122 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220242) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1123 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220252) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1124 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220223) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1131 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220233) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1132 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220243) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1133 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220253) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1134 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220224) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1141 =GeneraliConstants.THAI_DATE+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220234) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1142 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220244) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1143 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220254) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1144 =GeneraliConstants.HOSPITAL+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220225) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1151 =GeneraliConstants.THAI_DATE+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220235) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1152 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220245) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1153 =GeneraliConstants.CURSYMPTOM+ payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220255) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1154 =GeneraliConstants.HOSPITAL+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220226) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1161 =GeneraliConstants.THAI_DATE+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220236) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1162 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220246) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1163 =GeneraliConstants.CURSYMPTOM+ payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220256) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1164 =GeneraliConstants.HOSPITAL+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220227) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1171 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220237) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1172 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220247) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1173 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220257) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1174 =GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 222030) {
									illnessTreatmentPQ12 = payUserselections
											.getDetailedInfo();
								} 
								
								else if (payUserselections.getQuestionId() == 222031) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.PALPABLETUMOR.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep57 =GeneraliConstants.THAI_PALPABLETUMOR;
										}
										if(GeneraliConstants.CHRONICSEVHEADACHE.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep58 =GeneraliConstants.THAI_CHRONICSEVHEADACHE;
										}

									}
								}
								
								else if (payUserselections.getQuestionId() == 2220321) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1211 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220331) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1212 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220341) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1213 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220351) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1214 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220322) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1221 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220332) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1222 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220342) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1223 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220352) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1224 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 222040) {
									illnessTreatmentPQ13 = payUserselections
											.getDetailedInfo();
								} 
								
								else if (payUserselections.getQuestionId() == 222041) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.CHRONICJOINT.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep59 = GeneraliConstants.THAI_CHRONICJOINT;
										}
										if(GeneraliConstants.BRUISES.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep60 = GeneraliConstants.THAI_BRUISES;
										}

									}
								}
								else if (payUserselections.getQuestionId() == 2220421) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1311 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220431) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1312 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220441) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1313 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2220451) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1314 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220422) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1321 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220432) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1322 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220442) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1323 =GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2220452) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1324 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 222050) {
									illnessTreatmentPQ14 = payUserselections
											.getDetailedInfo();
								}
								
								else if (payUserselections.getQuestionId() == 222051) {
									for(GLISelectedOption option : payUserselections.getSelectedOptions()){
										if(GeneraliConstants.IMPAIREDVISION2.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep61 = GeneraliConstants.THAI_IMPAIREDVISION2;
										}
										if(GeneraliConstants.SLOWDEV.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep62 = GeneraliConstants.THAI_SLOWDEV;
										}
										if(GeneraliConstants.HURTONESELF.equalsIgnoreCase(option.getSelectedOptionKey()) 
												&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
											diseasep63 = GeneraliConstants.THAI_HURTONESELF;
										}

									}
								}
								else if (payUserselections.getQuestionId() == 2220521) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1411 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220531) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1412 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220541) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1413 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 2220551) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1414 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220522) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1421 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220532) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1422 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 2220542) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1423 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220552) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1424 =GeneraliConstants.HOSPITAL+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220523) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1431 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 2220533) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1432 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(", ");

									}
								} else if (payUserselections.getQuestionId() == 2220543) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1433 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 2220553) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1434 =GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 223010) {
									illnessTreatmentPQ15 = payUserselections
											.getDetailedInfo();
									if(illnessTreatmentPQ15.equalsIgnoreCase("Yes"))
									{
										diseasep64=GeneraliConstants.THAI_FATIGUE;
									}
								}
								else if (payUserselections.getQuestionId() == 223011) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())){
										illnessTreatmentPQ1511 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 223012) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1512 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 223013) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1513 =GeneraliConstants.CURSYMPTOM+ payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 223014) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1514 =GeneraliConstants.HOSPITAL+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 223020) {
									illnessTreatmentPQ16 = payUserselections
											.getDetailedInfo();
									if(illnessTreatmentPQ16.equalsIgnoreCase("Yes"))
									{
										diseasep65=GeneraliConstants.THAI_WGHTLOSS;
									}
								}

								else if (payUserselections.getQuestionId() == 223021) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1611 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 223022) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1612 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 223023) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1613 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 223024) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1614 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								}

								else if (payUserselections.getQuestionId() == 223030) {
									illnessTreatmentPQ17 = payUserselections
											.getDetailedInfo();
									if(illnessTreatmentPQ17.equalsIgnoreCase("Yes"))
									{
										diseasep66=GeneraliConstants.THAI_CHRONICDIARREA;
									}
								}

								else if (payUserselections.getQuestionId() == 223031) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1711 = GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 223032) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1712 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 223033) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1713 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 223034) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1714 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 223040) {
									illnessTreatmentPQ18 = payUserselections
											.getDetailedInfo();
									if(illnessTreatmentPQ18.equalsIgnoreCase("Yes"))
									{
										diseasep67=GeneraliConstants.THAI_PROLONGEDFEVER;
									}
								} 
								else if (payUserselections.getQuestionId() == 223041) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())){
										illnessTreatmentPQ1811 =GeneraliConstants.THAI_DATE+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 223042) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1812 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 223043) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1813 = GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 223044) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1814 =GeneraliConstants.HOSPITAL+ payUserselections
												.getDetailedInfo().concat(",  ");
									}
								}

								else if (payUserselections.getQuestionId() == 223050) {
									illnessTreatmentPQ19 = payUserselections
											.getDetailedInfo();
									if(illnessTreatmentPQ19.equalsIgnoreCase("Yes"))
									{
										diseasep68=GeneraliConstants.THAI_CHRONICSKIN;
									}
								} 

								else if (payUserselections.getQuestionId() == 223051) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1911 =GeneraliConstants.THAI_DATE+ payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 223052) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())){
										illnessTreatmentPQ1912 = GeneraliConstants.TRTMETHOD+payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 223053) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())){
										illnessTreatmentPQ1913 =GeneraliConstants.CURSYMPTOM+ payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 223054) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ1914 =GeneraliConstants.HOSPITAL+ payUserselections
												.getDetailedInfo().concat(",  ");
									}
								} else if (payUserselections.getQuestionId() == 224010) {
									illnessTreatmentPQ20 = payUserselections
											.getDetailedInfo();

								} else if (payUserselections.getQuestionId() == 224011) {
									if (!StringUtils.isBlank(payUserselections.getSelectedOptionExt())) {
									illnessTreatmentPQ2011 = payUserselections
											.getSelectedOptionExt().concat("  ");
									}

								} else if (payUserselections.getQuestionId() == 224020) {
									illnessTreatmentPQ21 = payUserselections
											.getDetailedInfo();

								}
								else if (payUserselections.getQuestionId() == 224021) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())){
										illnessTreatmentPQ2111 =payUserselections
												.getDetailedInfo().concat(", ");

									}
								}
								else if (payUserselections.getQuestionId() == 224022) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())){
										illnessTreatmentPQ2112 =GeneraliConstants.THAI_DATE+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 224023) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2113 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 224024) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2114 =GeneraliConstants.CURSYMPTOM+payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 224025) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2121 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} 
							
								else if (payUserselections.getQuestionId() == 224030) {
									illnessTreatmentPQ22 = payUserselections
											.getDetailedInfo();

								}
								else if (payUserselections.getQuestionId() == 224031) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2211 = payUserselections
												.getDetailedInfo().concat(", ");

									}
								} else if (payUserselections.getQuestionId() == 224032) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2212 = GeneraliConstants.THAI_DATE+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 224033) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2213 =GeneraliConstants.TRTMETHOD+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 224034) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2214 =GeneraliConstants.CURSYMPTOM+ payUserselections
												.getDetailedInfo().concat("\n");

									}
								} else if (payUserselections.getQuestionId() == 224035) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2215 = GeneraliConstants.HOSPITAL+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								}

								else if (payUserselections.getQuestionId() == 225010) {
									illnessTreatmentPQ23 = payUserselections
											.getDetailedInfo();

								}

								else if (payUserselections.getQuestionId() == 225011) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2311 = payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 225012) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())){
										illnessTreatmentPQ2312 ="รักษาเมื่อ: "+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 225013) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2313 ="โดย: "+ payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 225014) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2314 ="ผลการตรวจ: "+ payUserselections
												.getDetailedInfo().concat(",   ");

									}
								} else if (payUserselections.getQuestionId() == 225015) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2315 = "ข้อสังเกตล: "+payUserselections
												.getDetailedInfo().concat("\n");
									}
								} else if (payUserselections.getQuestionId() == 225020) {
									illnessTreatmentPQ24 = payUserselections
											.getDetailedInfo();

								} 

								else if (payUserselections.getQuestionId() == 225021) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())){
										illnessTreatmentPQ2411 = "อาการตอนนี้: "+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 225022) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())){
										illnessTreatmentPQ2412 = "รักษาเมื่อ: "+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 225023) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2413 = "ที่: "+payUserselections
												.getDetailedInfo().concat(",  ");

									}
								} else if (payUserselections.getQuestionId() == 225024) {
									if (!StringUtils.isBlank(payUserselections.getDetailedInfo())) {
										illnessTreatmentPQ2414 = "ผลการตรวจ: "+payUserselections
												.getDetailedInfo().concat("\n");
									}
								}
							}
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error("Error in getting payer userselection"
						+ e.getMessage());
				e.printStackTrace();
			}
			// --------------------------------//
			try {
				proposer = (com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder) this
						.getFieldValue("proposer");
				if (null != proposer) {
					proposerPerson = (Person) proposer.getPlayerParty();
					proposerUserSelections = proposer.getSelectedOptions();
					isPropDiffFromInsured = proposer
							.getProposerDifferentFromInsuredIndicator();
				}
				if (proposerUserSelections != null) {
					for (UserSelection selection : proposerUserSelections) {
						if (selection != null
								&& selection.getQuestionnaireType() != null) {
							if (selection.getQuestionnaireType().name()
									.equalsIgnoreCase("Other")) {
								if (selection.getQuestionId() == 31000) {
									proposerFatcaQues1 = selection
											.getDetailedInfo();
								} else if (selection.getQuestionId() == 31010) {
									proposerFatcaQues11 = selection
											.getDetailedInfo();
								} else if (selection.getQuestionId() == 31020) {
									proposerFatcaQues12 = selection
											.getDetailedInfo();
								} else if (selection.getQuestionId() == 31030) {
									proposerFatcaQues13 = selection
											.getDetailedInfo();
								} else if (selection.getQuestionId() == 32000) {
									proposerFatcaQues2 = selection
											.getDetailedInfo();
								} else if (selection.getQuestionId() == 33000) {
									proposerFatcaQues3 = selection
											.getDetailedInfo();
								} else if (selection.getQuestionId() == 34000) {
									proposerFatcaQues4 = selection
											.getDetailedInfo();
								}

							}
						}
					}
				}

				if (null != proposerPerson) {
					proposerdob = proposerPerson.getBirthDate();
					proposerPersonNames = proposerPerson.getName();
					proposerPersonDetails = proposerPerson.getDetail();
					proposerContactPreferences = proposerPerson
							.getPreferredContact();
					proposerCountries = proposerPerson.getNationalityCountry();
					proposerResidenceCountry = proposerPerson
							.getResidenceCountry();
					proposerIdentityProofType = proposerPerson
							.getIdentityProof();
					proposerIdentityPlace = proposerPerson.getIdentityPlace();
					proposerIdentityNo = proposerPerson.getNationalId();
					proposerIdentityDate = proposerPerson.getIdentityDate();
				}

				if (null != proposerPersonDetails) {
					for (PersonDetail personDetail : proposerPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							proposerIncomeDetail = (IncomeDetail) personDetail;
						} else if (personDetail instanceof EducationDetail) {
							proposerEducationDetail = (EducationDetail) personDetail;
						} else if (personDetail instanceof OccupationDetail) {
							proposerOccupationDetail = (OccupationDetail) personDetail;
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error("ScriptletSample Error :while getting field value proposer"
						+ e);
			}

			beneficiaryFatcaQues1 = false;
			beneficiaryFatcaQues2 = false;
			Set<Beneficiary> beneficiaries = null;
			try {
				beneficiaries = (Set<Beneficiary>) this
						.getFieldValue("beneficiaries");

				Beneficiary firstBeneficiary = null;
				Beneficiary secondBeneficiary = null;
				Beneficiary thirdBeneficiary = null;
				Beneficiary fourthBeneficiary = null;

				if (beneficiaries != null) {
					for (Beneficiary beneficiary : beneficiaries) {
						if ((beneficiary != null)
								&& (beneficiary.getPlayerParty() != null)) {
							for (Extension benExtension : beneficiary
									.getPlayerParty().getIncludesExtension()) {
								if (benExtension != null
										&& benExtension.getName() != null
										&& !"".equalsIgnoreCase(benExtension
												.getName())) {
									if (benExtension != null
											&& benExtension.getName()
													.equalsIgnoreCase(
															"BeneficiaryId")
											&& benExtension.getValue()
													.equalsIgnoreCase("1")) {
										firstBeneficiary = beneficiary;
									} else if (benExtension != null
											&& benExtension.getName()
													.equalsIgnoreCase(
															"BeneficiaryId")
											&& benExtension.getValue()
													.equalsIgnoreCase("2")) {
										secondBeneficiary = beneficiary;
									} else if (benExtension != null
											&& benExtension.getName()
													.equalsIgnoreCase(
															"BeneficiaryId")
											&& benExtension.getValue()
													.equalsIgnoreCase("3")) {
										thirdBeneficiary = beneficiary;
									} else if (benExtension != null
											&& benExtension.getName()
													.equalsIgnoreCase(
															"BeneficiaryId")
											&& benExtension.getValue()
													.equalsIgnoreCase("4")) {
										fourthBeneficiary = beneficiary;
									}
								}
							}
						}
					}

					if (firstBeneficiary != null) {
						firstBeneficiarySelectedOptions = firstBeneficiary
								.getSelectedOptions();
						firstBeneficiaryPerson = (Person) firstBeneficiary
								.getPlayerParty();
						if (firstBeneficiaryPerson != null) {
							firstBeneficiaryPersonNames = firstBeneficiaryPerson
									.getName();
							firstBeneficiaryTypeOfIdentity = firstBeneficiaryPerson
									.getNationalId();
						}
						if (firstBeneficiarySelectedOptions != null
								&& firstBeneficiarySelectedOptions.size() > 0) {
							for (UserSelection selection : firstBeneficiarySelectedOptions) {
								if (selection != null
										&& selection.getQuestionnaireType() != null) {
									if (selection.getQuestionnaireType().name()
											.equalsIgnoreCase("Basic")) {
										if (selection.getQuestionId() == 1
												&& selection
														.getSelectedOption() != null
												&& selection
														.getSelectedOption() == true) {
											beneficiaryFatcaQues1 = selection
													.getSelectedOption();
										} else if (selection.getQuestionId() == 2
												&& selection
														.getSelectedOption() != null
												&& selection
														.getSelectedOption() == true) {
											beneficiaryFatcaQues2 = selection
													.getSelectedOption();
										}
									}
								}
							}
						}
						firstBeneficiaryDOB = firstBeneficiaryPerson
								.getBirthDate();
						/*
						 * firstBeneficiaryGender = firstBeneficiaryPerson
						 * .getGenderCode().name();
						 */
						firstBeneficiarySharePercentage = firstBeneficiary
								.getSharePercentage();
						
						firstBeneficiaryRelationWithInsured = firstBeneficiary
								.getRelationWithInsured();
					}

					if (secondBeneficiary != null) {
						secondBeneficiarySelectedOptions = secondBeneficiary
								.getSelectedOptions();
						secondBeneficiaryPerson = (Person) secondBeneficiary
								.getPlayerParty();
						if (secondBeneficiaryPerson != null) {
							secondBeneficiaryPersonNames = secondBeneficiaryPerson
									.getName();
							secondBeneficiaryTypeOfIdentity = secondBeneficiaryPerson
									.getNationalId();

						}
						secondBeneficiaryDOB = secondBeneficiaryPerson
								.getBirthDate();
						/*
						 * secondBeneficiaryGender = secondBeneficiaryPerson
						 * .getGenderCode().name();
						 */
						secondBeneficiarySharePercentage = secondBeneficiary
								.getSharePercentage();
						secondBeneficiaryRelationWithInsured = secondBeneficiary
								.getRelationWithInsured();
						if (!beneficiaryFatcaQues1 || !beneficiaryFatcaQues2) {
							if (secondBeneficiarySelectedOptions != null
									&& secondBeneficiarySelectedOptions.size() > 0) {
								for (UserSelection selection : secondBeneficiarySelectedOptions) {
									if (selection != null
											&& selection.getQuestionnaireType() != null) {
										if (selection.getQuestionnaireType()
												.name()
												.equalsIgnoreCase("Basic")) {
											if (selection.getQuestionId() == 1
													&& selection
															.getSelectedOption() != null
													&& selection
															.getSelectedOption() == true) {
												beneficiaryFatcaQues1 = selection
														.getSelectedOption();
											} else if (selection
													.getQuestionId() == 2
													&& selection
															.getSelectedOption() != null
													&& selection
															.getSelectedOption() == true) {
												beneficiaryFatcaQues2 = selection
														.getSelectedOption();
											}
										}
									}
								}
							}
						}
					}

					if (thirdBeneficiary != null) {
						thirdBeneficiarySelectedOptions = thirdBeneficiary
								.getSelectedOptions();
						thirdBeneficiaryPerson = (Person) thirdBeneficiary
								.getPlayerParty();
						if (thirdBeneficiaryPerson != null) {
							thirdBeneficiaryPersonNames = thirdBeneficiaryPerson
									.getName();
							thirdBeneficiaryTypeOfIdentity = thirdBeneficiaryPerson
									.getNationalId();
						}
						thirdBeneficiaryDOB = thirdBeneficiaryPerson
								.getBirthDate();
						/*
						 * thirdBeneficiaryGender = thirdBeneficiaryPerson
						 * .getGenderCode().name();
						 */
						thirdBeneficiarySharePercentage = thirdBeneficiary
								.getSharePercentage();
						thirdBeneficiaryRelationWithInsured = thirdBeneficiary
								.getRelationWithInsured();
						if (!beneficiaryFatcaQues1 || !beneficiaryFatcaQues2) {
							if (thirdBeneficiarySelectedOptions != null
									&& thirdBeneficiarySelectedOptions.size() > 0) {
								for (UserSelection selection : thirdBeneficiarySelectedOptions) {
									if (selection != null
											&& selection.getQuestionnaireType() != null) {
										if (selection.getQuestionnaireType()
												.name()
												.equalsIgnoreCase("Basic")) {
											if (selection.getQuestionId() == 1
													&& selection
															.getSelectedOption() != null
													&& selection
															.getSelectedOption() == true) {
												beneficiaryFatcaQues1 = selection
														.getSelectedOption();
											} else if (selection
													.getQuestionId() == 2
													&& selection
															.getSelectedOption() != null
													&& selection
															.getSelectedOption() == true) {
												beneficiaryFatcaQues2 = selection
														.getSelectedOption();
											}
										}
									}
								}
							}
						}
					}

					if (fourthBeneficiary != null) {
						fourthBeneficiarySelectedOptions = fourthBeneficiary
								.getSelectedOptions();
						fourthBeneficiaryPerson = (Person) fourthBeneficiary
								.getPlayerParty();
						if (fourthBeneficiaryPerson != null) {
							fourthBeneficiaryPersonNames = fourthBeneficiaryPerson
									.getName();
							fourthBeneficiaryTypeOfIdentity = fourthBeneficiaryPerson
									.getNationalId();
						}

						fourthBeneficiaryDOB = fourthBeneficiaryPerson
								.getBirthDate();
						/*
						 * fourthBeneficiaryGender = fourthBeneficiaryPerson
						 * .getGenderCode().name();
						 */
						fourthBeneficiarySharePercentage = fourthBeneficiary
								.getSharePercentage();
						fourthBeneficiaryRelationWithInsured = fourthBeneficiary
								.getRelationWithInsured();
						if (!beneficiaryFatcaQues1 || !beneficiaryFatcaQues2) {
							if (fourthBeneficiarySelectedOptions != null
									&& fourthBeneficiarySelectedOptions.size() > 0) {
								for (UserSelection selection : fourthBeneficiarySelectedOptions) {
									if (selection != null
											&& selection.getQuestionnaireType() != null) {
										if (selection.getQuestionnaireType()
												.name()
												.equalsIgnoreCase("Basic")) {
											if (selection.getQuestionId() == 1
													&& selection
															.getSelectedOption() != null
													&& selection
															.getSelectedOption() == true) {
												beneficiaryFatcaQues1 = selection
														.getSelectedOption();
											} else if (selection
													.getQuestionId() == 2
													&& selection
															.getSelectedOption() != null
													&& selection
															.getSelectedOption() == true) {
												beneficiaryFatcaQues2 = selection
														.getSelectedOption();

											}
										}
									}
								}
							}
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error(
						"ScriptletSample Error : populating beneficiaries:", e);
			}
			


			try {
				InsuranceAgreement insuranceAgreement = (InsuranceAgreement) this
						.getFieldValue("proposal");
				generaliAgent = (GeneraliAgent) this
						.getFieldValue("generaligent");
				type = (String) this.getFieldValue("type");
				codeValuesMap = (Map) this.getFieldValue("mapForLookUpValues");

				if (!type.equals(GeneraliConstants.EAPP)) {
					appianMemoDetails = (ArrayList<AppianMemoDetails>) this
							.getFieldValue("appianMemoDetails");
					dateMonthInThai = (String) this
							.getFieldValue("dateMonthInThai");
					applicationNumber = (String) this.getFieldValue("key21");
					if (appianMemoDetails != null) {
						for (AppianMemoDetails test : appianMemoDetails) {
							if (test.getMemoDetails().getMemoCode()
									.equals("5CF") || test.getMemoDetails().getMemoCode()
									.equals("CTF")||test.getMemoDetails().getMemoCode()
									.equals("XCL")) {
								String json = test.getDetails();
								Object obj = jsonParser.parse(json);
								jsonObject = (JSONObject) obj;
							}
						}
					}
				}
				proposal = insuranceAgreement;
				try {
					if (proposal != null) {
						
							if (proposal.getAgreementExtension() != null) {
							guardianName = proposal.getAgreementExtension().getSpajDeclarationName1();
							guardianType = proposal.getAgreementExtension().getSpajDeclarationName2();
							witnessName  = proposal.getAgreementExtension().getSpajDeclarationName3();
						}
					}
				} catch (Exception e) {
					LOGGER.error("Error while getting ProposalFields " + e.getMessage());
				}
				
				try {
					if (insuranceAgreement != null) {
						TransactionKeys transactionKey = insuranceAgreement
								.getTransactionKeys();
						if (transactionKey != null) {
							spajNumber = transactionKey.getKey21();
							illustrationNumber = transactionKey.getKey24();
							agentCode = transactionKey.getKey11();
						}
					}
				} catch (Exception e) {
					LOGGER.error("Error while getting keys " + e.getMessage());
				}

				if (insuranceAgreement != null
						&& insuranceAgreement.getAgreementExtension() != null) {
					declationQuestions = insuranceAgreement
							.getAgreementExtension().getSelectedOptions();
					agentSignDate = insuranceAgreement.getAgreementExtension()
							.getAgentSignDate();
					agentSignPlace = insuranceAgreement.getAgreementExtension()
							.getAgentSignPlace();
					spajSignDate = insuranceAgreement.getAgreementExtension()
							.getSpajDate();
					SpajSignPlace = insuranceAgreement.getAgreementExtension()
							.getSpajSignPlace();
					if (insuranceAgreement.getAgreementExtension()
							.getCommissionRateOne() != null
							&& !"".equals(insuranceAgreement
									.getAgreementExtension()
									.getCommissionRateOne())) {
						commisionRateOne = insuranceAgreement
								.getAgreementExtension().getCommissionRateOne()
								.toString();
					}
					if (insuranceAgreement.getAgreementExtension()
							.getCommissionRateTwo() != null
							&& !"".equals(insuranceAgreement
									.getAgreementExtension()
									.getCommissionRateTwo())) {
						commisionRateTwo = insuranceAgreement
								.getAgreementExtension().getCommissionRateTwo()
								.toString();
					}
					partnerCode = insuranceAgreement.getAgreementExtension()
							.getPartnerCode();
					commisionAgent = insuranceAgreement.getAgreementExtension()
							.getCommissionAgent();

				}

				if (insuranceAgreement != null) {
					for (Premium premium : insuranceAgreement
							.getAgreementPremium()) {
						if ((GeneraliConstants.REGULAR_PREMIUM).equals(premium.getNatureCode()
								.name())) {

							for (FinancialScheduler financialScheduler : premium
									.getAttachedFinancialScheduler()) {

								if (financialScheduler.getPaymentType() != null) {
									if ((GeneraliConstants.SPLIT_ONE).equals(
											premium.getDescription())) {

										proposerPaymentMethodSplitOne = financialScheduler
												.getDescription();
										if (financialScheduler
												.getRelatedBankAccount() != null) {
											BankNameSplitOne = financialScheduler
													.getRelatedBankAccount()
													.getBankName();
											BranchNameSplitOne = financialScheduler
													.getRelatedBankAccount()
													.getBranchName();
										}
										if(premium.getAmount() != null){
											premiumPaidSplitOne = premium
													.getAmount().getAmount();
										}

									} else {

										proposerPaymentMethodSplitTwo = financialScheduler
												.getDescription();
										if (financialScheduler
												.getRelatedBankAccount() != null) {
											BankNameSplitTwo = financialScheduler
													.getRelatedBankAccount()
													.getBankName();
											BranchNameSplitTwo = financialScheduler
													.getRelatedBankAccount()
													.getBranchName();
										}
										
										if(premium.getAmount() != null){
											premiumPaidSplitTwo = premium
													.getAmount().getAmount();
										}
									}

									if (FinancialMediumTypeCodeList.CreditCard.toString().equals(financialScheduler
													.getPaymentType())) {
										CreditCardPayment creditCardPayment = (CreditCardPayment) financialScheduler
												.getPaymentMeans();
										if(creditCardPayment != null){
											if ((GeneraliConstants.SPLIT_ONE).equals(premium.getDescription())) {
												creditCardHolderNameSplitOne = creditCardPayment.getCardholderName() != null ? 
														creditCardPayment.getCardholderName().getFullName():StringUtils.EMPTY;
														creditCardNumberSplitOne = creditCardPayment
																.getCardNumber();

											} else {
												creditCardHolderNameSplitTwo = creditCardPayment.getCardholderName() != null ? 
														creditCardPayment.getCardholderName().getFullName():StringUtils.EMPTY;
														creditCardNumberSplitTwo = creditCardPayment
																.getCardNumber();
											}
										}
									} else if (FinancialMediumTypeCodeList.Cash.toString().equals(financialScheduler
													.getPaymentType())) {
										CashPayment cashPayment = (CashPayment) financialScheduler
												.getPaymentMeans();
										if(cashPayment != null){
											if ((GeneraliConstants.SPLIT_ONE).equals(premium.getDescription())) {
												etrNumberSplitOne = cashPayment
														.getEtrNumber();
											} else {
												etrNumberSplitTwo = cashPayment
														.getEtrNumber();
											}
										}
									} else if (FinancialMediumTypeCodeList.PersonalCheck.toString().equals(financialScheduler
													.getPaymentType())) {
										CheckPayment chequePayment = (CheckPayment) financialScheduler
												.getPaymentMeans();
										if(chequePayment!=null){
											if ((GeneraliConstants.SPLIT_ONE).equals(premium.getDescription())) {
												etrNumberSplitOne = chequePayment
														.getEtrNumber();
											} else {
												etrNumberSplitTwo = chequePayment
														.getEtrNumber();
											}
										}
									}
								}
							}
						} else if (((GeneraliConstants.RENEWAL_PREMIUM).equals(premium
								.getNatureCode().name()))) {
							for (FinancialScheduler financialScheduler : premium
									.getAttachedFinancialScheduler()) {
								proposerPaymentMethodRenewalPayment = financialScheduler
										.getPaymentMethodCode().name();
								if (financialScheduler.getPaymentMeans() != null) {
									proposerRenewalPaymentMethod = financialScheduler
											.getPaymentMeans().toString();
									if (financialScheduler
											.getRelatedBankAccount() != null) {
										renewalBankName = financialScheduler
												.getRelatedBankAccount()
												.getBankName();
										renewalBranchName = financialScheduler
												.getRelatedBankAccount()
												.getBranchName();
									}
									if (FinancialMediumTypeCodeList.CreditCard.equals(financialScheduler
											.getPaymentMethodCode())) {
										CreditCardPayment creditCardPayment = (CreditCardPayment) financialScheduler
												.getPaymentMeans();
										if (creditCardPayment != null) {
											renewalCreditCardHolderName = creditCardPayment.getCardholderName() != null ? 
													creditCardPayment.getCardholderName().getFullName():StringUtils.EMPTY;
											renewalCreditCardNumber = creditCardPayment
													.getCardNumber();
											renewalCreditCareExpiryDate = creditCardPayment
													.getExpiryDate().toString();
										}
									} else if (FinancialMediumTypeCodeList.Cash.equals(financialScheduler
															.getPaymentMethodCode())) {
										CashPayment cashPayment = (CashPayment) financialScheduler
												.getPaymentMeans();
										if (cashPayment != null) {
											renewalCashETRNumber = cashPayment
													.getEtrNumber();
										}
									} else if (FinancialMediumTypeCodeList.PersonalCheck.equals(financialScheduler
													.getPaymentMethodCode())) {
										CheckPayment chequePayment = (CheckPayment) financialScheduler
												.getPaymentMeans();
										if (chequePayment != null) {
											renewalChequeETRNumber = chequePayment
													.getEtrNumber();
										}
									}
								}
							}
						}
					}
				}
				if (insuranceAgreement != null) {
					for (FinancialScheduler financialScheduler : insuranceAgreement
							.getRefundDetails().getAttachedFinancialScheduler()) {

						if (financialScheduler.getPaymentMethodCode() != null) {
							proposerRefundPaymentMethod = financialScheduler
									.getPaymentMethodCode().name();
							if (financialScheduler.getRelatedBankAccount() != null) {
								refundBankName = financialScheduler
										.getRelatedBankAccount().getBankName();
								refundBranchName = financialScheduler
										.getRelatedBankAccount()
										.getBranchName();
							}
							if (financialScheduler
									.getPaymentMethodCode()
									.equals(FinancialMediumTypeCodeList.DebitAccount)) {
								BankAccount bankAccount = financialScheduler
										.getRelatedBankAccount();
								if (bankAccount != null) {
									refundcreditCardNumber = bankAccount
											.getAccountIdentifier().toString();
									for (Party party : bankAccount.getOwner()) {
										Person person = (Person) party;
										for (PersonName personName : person
												.getName()) {
											refundCreditCardHolderName = personName
													.getGivenName() != null ? personName
													.getGivenName()
													: StringUtils.EMPTY;
										}
									}
								}
							} else if (financialScheduler.getPaymentMeans()
									.equals(FinancialMediumTypeCodeList.Cash)) {
								CashPayment cashPayment = (CashPayment) financialScheduler
										.getPaymentMeans();
								if (cashPayment != null) {
									refundCashETRNumber = cashPayment
											.getEtrNumber();
								}
							}
						}

					}
				}
				/* Product Details */
				if (insuranceAgreement != null) {
					ArrayList<Coverage> riderList = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA1 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA2 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA3 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA4 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA5 = new ArrayList<Coverage>();
					ArrayList<RiderTableForEapp> riderdeatilslist = new ArrayList();
					if (insuranceAgreement.getIncludesCoverage() != null
							&& insuranceAgreement.getIncludesCoverage().size() > 0) {
						int riderCount = 0;

						for (Coverage coverage : insuranceAgreement
								.getIncludesCoverage()) {
							if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("BasicCoverage")) {
								riderList.add(coverage);
								break;

							}
						}

						for (Coverage coverage : insuranceAgreement
								.getIncludesCoverage()) {
							if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("Rider")) {
								if (coverage.getInsuredType() != null) {
									if (coverage.getInsuredType().equals("LA1")) {
										riderListLA1.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA2")) {
										riderListLA2.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA3")) {
										riderListLA3.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA4")) {
										riderListLA4.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA5")) {
										riderListLA5.add(coverage);

									}
								}
							}
						}

						if (riderListLA1 != null && riderListLA1.size() > 0) {
							riderList.addAll(riderNames(riderListLA1));
						}
						if (riderListLA2 != null && riderListLA2.size() > 0) {
							riderList.addAll(riderNames(riderListLA2));
						}
						if (riderListLA3 != null && riderListLA3.size() > 0) {
							riderList.addAll(riderNames(riderListLA3));
						}
						if (riderListLA4 != null && riderListLA4.size() > 0) {
							riderList.addAll(riderNames(riderListLA4));
						}
						if (riderListLA5 != null && riderListLA5.size() > 0) {
							riderList.addAll(riderNames(riderListLA5));
						}
						for (Coverage coverage : riderList) {
							RiderTableForEapp riderData = new RiderTableForEapp();

							if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("BasicCoverage")) {
								// riderList.add(coverage);
								if (coverage.getProductType() != null) {
									// productName=
									// coverage.getProductCode().getCode();
									productName = coverage.getMarketingName();

								}
								if (coverage.getSumAssured() != null) {
									productSumAssured = coverage
											.getSumAssured().getAmount();

								}
								productPremiumAmount = coverage
										.getPremiumAmount();

								productPolicyTerm = coverage.getPolicyTerm();

								productPremiumPeriod = coverage
										.getPremiumPeriod();

								productPremiumTerm = coverage.getPremiumTerm();

								if (coverage.getPremiumFrequency() != null) {
									productPremiumFrequency = coverage
											.getPremiumFrequency().name();
								}
								if (coverage.getPremium() != null
										&& coverage.getPremium().size() > 0) {
									for (Premium premium : coverage
											.getPremium()) {
										if (premium.getNatureCode() != null
												&& premium
														.getNatureCode()
														.name()
														.equalsIgnoreCase(
																"TotalInitialPremium")) {
											if (premium.getAmount() != null
													&& premium.getAmount()
															.getAmount() != null) {
												totalInitialPremium = premium
														.getAmount()
														.getAmount();
											}
										} else if (premium.getNatureCode() != null
												&& premium
														.getNatureCode()
														.name()
														.equalsIgnoreCase(
																"TotalPremium")) {
											if (premium.getAmount() != null
													&& premium.getAmount()
															.getAmount() != null) {
												initialPremium = premium
														.getAmount()
														.getAmount();

											}
										}
									}
								}
							} else if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("Rider")) {
								String lifeAssuredName = "";
								String riderName;
								Integer riderTerm;
								BigDecimal riderInitialPremium;
								BigDecimal riderSA;

								// riderName = coverage.getName();
								riderName = coverage.getTypeName();
								riderData.setRiderName(riderName);

								if (coverage.getSumAssured() != null) {
									riderSA = coverage.getSumAssured()
											.getAmount();
									riderData.setRiderSumAssured(riderSA);

								}
								riderTerm = coverage.getRiderTerm();
								// Value should be Blank in PDF;
								if ("OutPatient".equals(coverage.getName())
										|| "Dental".equals(coverage.getName())
										|| "HIP1"
												.equals(coverage.getTypeName())
										|| "HIP2"
												.equals(coverage.getTypeName())
										|| "HIP3"
												.equals(coverage.getTypeName())) {
									riderTerm = 0;
								}
								riderData.setRiderTerm(riderTerm);

								if (coverage.getPremium() != null
										&& coverage.getPremium().size() > 0) {
									for (Premium premium : coverage
											.getPremium()) {
										if (premium.getNatureCode() != null
												&& premium
														.getNatureCode()
														.name()
														.equalsIgnoreCase(
																"MinimumPremium")) {
											if (premium.getAmount() != null
													&& premium.getAmount()
															.getAmount() != null) {
												riderInitialPremium = premium
														.getAmount()
														.getAmount();
												// Value should be Blank in PDF;
												if ("OutPatient"
														.equals(coverage
																.getName())
														|| "Dental"
																.equals(coverage
																		.getName())) {
													BigDecimal bd = new BigDecimal(
															"0");
													riderInitialPremium = bd;
												}
												riderData
														.setRiderInitialPremium(riderInitialPremium);
											}

										}
									}
								}
								if (coverage.getInsuredType() != null) {
									if (coverage.getInsuredType()
											.equalsIgnoreCase("LA1")) {
										lifeAssuredName = getInsuredFullName();
									}
								}
								riderData.setLifeAssuredName(lifeAssuredName);
								riderdeatilslist.add(riderData);
							}
							// riderCount++;
						}

						/* rider Details */

						for (int i = 0; i < riderdeatilslist.size(); i++) {
							if (riderdeatilslist.get(i) != null) {

								RiderTableForEapp riderdetails = new RiderTableForEapp();
								riderdetails = riderdeatilslist.get(i);

								if ((riderdetails.getRiderName()
										.equalsIgnoreCase("HSRiders"))
										|| (riderdetails.getRiderName()
												.equalsIgnoreCase("HSRider"))) {
									riderHsextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderHsextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderHsextraInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderHsextraName = riderdetails
											.getRiderName();
									riderHsextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if ((riderdetails.getRiderName()
										.equalsIgnoreCase("ADDRiders"))
										|| (riderdetails.getRiderName()
												.equalsIgnoreCase("ADDRider"))) {
									riderAddextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderAddextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderAddextraInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderAddextraName = riderdetails
											.getRiderName();
									riderAddextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if ((riderdetails.getRiderName()
										.equalsIgnoreCase("HBRiders"))
										|| (riderdetails.getRiderName()
												.equalsIgnoreCase("HBRider"))) {
									riderHbextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderHbextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderHbextraInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderHbextraName = riderdetails
											.getRiderName();
									riderHbextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("CIRider")) {
									riderCIextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderCIextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderCIextraInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderCIextraName = riderdetails
											.getRiderName();
									riderCIextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("AIRider")) {
									riderAIInsuredName = riderdetails
											.getLifeAssuredName();
									riderAITerm = (riderdetails.getRiderTerm())
											.toString();
									riderAIInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderAIName = riderdetails.getRiderName();
									riderAISumAssured = String.format("%,.2f",
											(riderdetails.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("RCCADBRider")) {
									riderRCCADBInsuredName = riderdetails
											.getLifeAssuredName();
									riderRCCADBTerm = (riderdetails
											.getRiderTerm()).toString();
									riderRCCADBInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderRCCADBName = riderdetails
											.getRiderName();
									riderRCCADBSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("ADBRider")) {
									riderADBRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderADBRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderADBRiderInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderADBRiderName = riderdetails
											.getRiderName();
									riderADBRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("WPRider")) {
									riderWPRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderWPRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderWPRiderInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderWPRiderName = riderdetails
											.getRiderName();
									riderWPRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("DDRider")) {
									riderDDRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderDDRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderDDRiderInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderDDRiderName = riderdetails
											.getRiderName();
									riderDDRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("RCCAIRider")) {
									riderRCCAIRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderRCCAIRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderRCCAIRiderInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderRCCAIRiderName = riderdetails
											.getRiderName();
									riderRCCAIRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("RCCADDRider")) {
									riderRCCADDRiderRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderRCCADDRiderRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderRCCADDRiderRiderInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderRCCADDRiderRiderName = riderdetails
											.getRiderName();
									riderRCCADDRiderRiderSumAssured = String
											.format("%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("PBRider")) {
									riderPBRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderPBRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderPBRiderInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderPBRiderName = riderdetails
											.getRiderName();
									riderPBRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								}
							}
						}
					}
					/* Payment Details */
					try {
						if (insuranceAgreement != null) {
							if (insuranceAgreement.getAgreementPremium() != null
									&& insuranceAgreement.getAgreementPremium()
											.size() > 0) {
								for (Premium premium : insuranceAgreement
										.getAgreementPremium()) {
									if (premium.getNatureCode() != null
											&& premium
													.getNatureCode()
													.name()
													.equalsIgnoreCase(
															"RegularPremium")) {
										paymentDate = premium
												.getEffectivePeriod()
												.getEndDateTime();
										if (premium
												.getAttachedFinancialScheduler() != null
												&& premium
														.getAttachedFinancialScheduler()
														.size() > 0) {
											for (FinancialScheduler scheduler : premium
													.getAttachedFinancialScheduler()) {
												receiptNumber = scheduler
														.getTransactionRefNumber();
												paymentMethods = scheduler
														.getDescription();
											}
										}
									}
								}
							}
						}
					} catch (Exception e) {
						LOGGER.error("ScriptletSample Error :while getting payment details"
								+ e);
					}

				}
			} catch (Exception e) {
				LOGGER.error("ScriptletSample Error :while getting field value Payment"
						+ e);
			}


			try {
				InsuranceAgreement insuranceAgreement = (InsuranceAgreement) this
						.getFieldValue("proposal");
				generaliAgent = (GeneraliAgent) this
						.getFieldValue("generaligent");
				type = (String) this.getFieldValue("type");
				codeValuesMap = (Map) this.getFieldValue("mapForLookUpValues");

				if (!type.equals(GeneraliConstants.EAPP)) {
					appianMemoDetails = (ArrayList<AppianMemoDetails>) this
							.getFieldValue("appianMemoDetails");
					dateMonthInThai = (String) this
							.getFieldValue("dateMonthInThai");
					applicationNumber = (String) this.getFieldValue("key21");
					if (appianMemoDetails != null) {
						for (AppianMemoDetails test : appianMemoDetails) {
							if (test.getMemoDetails().getMemoCode()
									.equals("5CF")) {
								String json = test.getDetails();
								Object obj = jsonParser.parse(json);
								jsonObject = (JSONObject) obj;
							}
							else if(test.getMemoDetails().getMemoCode()
									.equals("XCL")){
								String json1 = test.getDetails();
								Object obj1 = jsonParser.parse(json1);
								jsonObject1 = (JSONObject) obj1;
								
							
							}
								
							
						}
					}
				}
				proposal = insuranceAgreement;
				try {
					if (proposal != null) {
						
							if (proposal.getAgreementExtension() != null) {
							guardianName = proposal.getAgreementExtension().getSpajDeclarationName1();
							guardianType = proposal.getAgreementExtension().getSpajDeclarationName2();
							witnessName  = proposal.getAgreementExtension().getSpajDeclarationName3();
						}
					}
				} catch (Exception e) {
					LOGGER.error("Error while getting ProposalFields " + e.getMessage());
				}
				
				try {
					if (insuranceAgreement != null) {
						TransactionKeys transactionKey = insuranceAgreement
								.getTransactionKeys();
						if (transactionKey != null) {
							spajNumber = transactionKey.getKey21();
							illustrationNumber = transactionKey.getKey24();
							agentCode = transactionKey.getKey11();
						}
					}
				} catch (Exception e) {
					LOGGER.error("Error while getting keys " + e.getMessage());
				}

				if (insuranceAgreement != null
						&& insuranceAgreement.getAgreementExtension() != null) {
					declationQuestions = insuranceAgreement
							.getAgreementExtension().getSelectedOptions();
					agentSignDate = insuranceAgreement.getAgreementExtension()
							.getAgentSignDate();
					agentSignPlace = insuranceAgreement.getAgreementExtension()
							.getAgentSignPlace();
					spajSignDate = insuranceAgreement.getAgreementExtension()
							.getSpajDate();
					SpajSignPlace = insuranceAgreement.getAgreementExtension()
							.getSpajSignPlace();
					if (insuranceAgreement.getAgreementExtension()
							.getCommissionRateOne() != null
							&& !"".equals(insuranceAgreement
									.getAgreementExtension()
									.getCommissionRateOne())) {
						commisionRateOne = insuranceAgreement
								.getAgreementExtension().getCommissionRateOne()
								.toString();
					}
					if (insuranceAgreement.getAgreementExtension()
							.getCommissionRateTwo() != null
							&& !"".equals(insuranceAgreement
									.getAgreementExtension()
									.getCommissionRateTwo())) {
						commisionRateTwo = insuranceAgreement
								.getAgreementExtension().getCommissionRateTwo()
								.toString();
					}
					partnerCode = insuranceAgreement.getAgreementExtension()
							.getPartnerCode();
					commisionAgent = insuranceAgreement.getAgreementExtension()
							.getCommissionAgent();

				}

				if (insuranceAgreement != null) {
					for (Premium premium : insuranceAgreement
							.getAgreementPremium()) {
						if ((GeneraliConstants.REGULAR_PREMIUM).equals(premium.getNatureCode()
								.name())) {

							for (FinancialScheduler financialScheduler : premium
									.getAttachedFinancialScheduler()) {

								if (financialScheduler.getPaymentType() != null) {
									if ((GeneraliConstants.SPLIT_ONE).equals(
											premium.getDescription())) {

										proposerPaymentMethodSplitOne = financialScheduler
												.getDescription();
										if (financialScheduler
												.getRelatedBankAccount() != null) {
											BankNameSplitOne = financialScheduler
													.getRelatedBankAccount()
													.getBankName();
											BranchNameSplitOne = financialScheduler
													.getRelatedBankAccount()
													.getBranchName();
										}
										if(premium.getAmount() != null){
											premiumPaidSplitOne = premium
													.getAmount().getAmount();
										}

									} else {

										proposerPaymentMethodSplitTwo = financialScheduler
												.getDescription();
										if (financialScheduler
												.getRelatedBankAccount() != null) {
											BankNameSplitTwo = financialScheduler
													.getRelatedBankAccount()
													.getBankName();
											BranchNameSplitTwo = financialScheduler
													.getRelatedBankAccount()
													.getBranchName();
										}
										
										if(premium.getAmount() != null){
											premiumPaidSplitTwo = premium
													.getAmount().getAmount();
										}
									}

									if (FinancialMediumTypeCodeList.CreditCard.toString().equals(financialScheduler
													.getPaymentType())) {
										CreditCardPayment creditCardPayment = (CreditCardPayment) financialScheduler
												.getPaymentMeans();
										if(creditCardPayment != null){
											if ((GeneraliConstants.SPLIT_ONE).equals(premium.getDescription())) {
												creditCardHolderNameSplitOne = creditCardPayment.getCardholderName() != null ? 
														creditCardPayment.getCardholderName().getFullName():StringUtils.EMPTY;
														creditCardNumberSplitOne = creditCardPayment
																.getCardNumber();

											} else {
												creditCardHolderNameSplitTwo = creditCardPayment.getCardholderName() != null ? 
														creditCardPayment.getCardholderName().getFullName():StringUtils.EMPTY;
														creditCardNumberSplitTwo = creditCardPayment
																.getCardNumber();
											}
										}
									} else if (FinancialMediumTypeCodeList.Cash.toString().equals(financialScheduler
													.getPaymentType())) {
										CashPayment cashPayment = (CashPayment) financialScheduler
												.getPaymentMeans();
										if(cashPayment != null){
											if ((GeneraliConstants.SPLIT_ONE).equals(premium.getDescription())) {
												etrNumberSplitOne = cashPayment
														.getEtrNumber();
											} else {
												etrNumberSplitTwo = cashPayment
														.getEtrNumber();
											}
										}
									} else if (FinancialMediumTypeCodeList.PersonalCheck.toString().equals(financialScheduler
													.getPaymentType())) {
										CheckPayment chequePayment = (CheckPayment) financialScheduler
												.getPaymentMeans();
										if(chequePayment!=null){
											if ((GeneraliConstants.SPLIT_ONE).equals(premium.getDescription())) {
												etrNumberSplitOne = chequePayment
														.getEtrNumber();
											} else {
												etrNumberSplitTwo = chequePayment
														.getEtrNumber();
											}
										}
									}
								}
							}
						} else if (((GeneraliConstants.RENEWAL_PREMIUM).equals(premium
								.getNatureCode().name()))) {
							for (FinancialScheduler financialScheduler : premium
									.getAttachedFinancialScheduler()) {
								proposerPaymentMethodRenewalPayment = financialScheduler
										.getPaymentMethodCode().name();
								if (financialScheduler.getPaymentMeans() != null) {
									proposerRenewalPaymentMethod = financialScheduler
											.getPaymentMeans().toString();
									if (financialScheduler
											.getRelatedBankAccount() != null) {
										renewalBankName = financialScheduler
												.getRelatedBankAccount()
												.getBankName();
										renewalBranchName = financialScheduler
												.getRelatedBankAccount()
												.getBranchName();
									}
									if (FinancialMediumTypeCodeList.CreditCard.equals(financialScheduler
											.getPaymentMethodCode())) {
										CreditCardPayment creditCardPayment = (CreditCardPayment) financialScheduler
												.getPaymentMeans();
										if (creditCardPayment != null) {
											renewalCreditCardHolderName = creditCardPayment.getCardholderName() != null ? 
													creditCardPayment.getCardholderName().getFullName():StringUtils.EMPTY;
											renewalCreditCardNumber = creditCardPayment
													.getCardNumber();
											renewalCreditCareExpiryDate = creditCardPayment
													.getExpiryDate().toString();
										}
									} else if (FinancialMediumTypeCodeList.Cash.equals(financialScheduler
															.getPaymentMethodCode())) {
										CashPayment cashPayment = (CashPayment) financialScheduler
												.getPaymentMeans();
										if (cashPayment != null) {
											renewalCashETRNumber = cashPayment
													.getEtrNumber();
										}
									} else if (FinancialMediumTypeCodeList.PersonalCheck.equals(financialScheduler
													.getPaymentMethodCode())) {
										CheckPayment chequePayment = (CheckPayment) financialScheduler
												.getPaymentMeans();
										if (chequePayment != null) {
											renewalChequeETRNumber = chequePayment
													.getEtrNumber();
										}
									}
								}
							}
						}
					}
				}
				if (insuranceAgreement != null) {
					for (FinancialScheduler financialScheduler : insuranceAgreement
							.getRefundDetails().getAttachedFinancialScheduler()) {

						if (financialScheduler.getPaymentMethodCode() != null) {
							proposerRefundPaymentMethod = financialScheduler
									.getPaymentMethodCode().name();
							if (financialScheduler.getRelatedBankAccount() != null) {
								refundBankName = financialScheduler
										.getRelatedBankAccount().getBankName();
								refundBranchName = financialScheduler
										.getRelatedBankAccount()
										.getBranchName();
							}
							if (financialScheduler
									.getPaymentMethodCode()
									.equals(FinancialMediumTypeCodeList.DebitAccount)) {
								BankAccount bankAccount = financialScheduler
										.getRelatedBankAccount();
								if (bankAccount != null) {
									refundcreditCardNumber = bankAccount
											.getAccountIdentifier().toString();
									for (Party party : bankAccount.getOwner()) {
										Person person = (Person) party;
										for (PersonName personName : person
												.getName()) {
											refundCreditCardHolderName = personName
													.getGivenName() != null ? personName
													.getGivenName()
													: StringUtils.EMPTY;
										}
									}
								}
							} else if (financialScheduler.getPaymentMeans()
									.equals(FinancialMediumTypeCodeList.Cash)) {
								CashPayment cashPayment = (CashPayment) financialScheduler
										.getPaymentMeans();
								if (cashPayment != null) {
									refundCashETRNumber = cashPayment
											.getEtrNumber();
								}
							}
						}

					}
				}
				/* Product Details */
				if (insuranceAgreement != null) {
					ArrayList<Coverage> riderList = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA1 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA2 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA3 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA4 = new ArrayList<Coverage>();
					ArrayList<Coverage> riderListLA5 = new ArrayList<Coverage>();
					ArrayList<RiderTableForEapp> riderdeatilslist = new ArrayList();
					if (insuranceAgreement.getIncludesCoverage() != null
							&& insuranceAgreement.getIncludesCoverage().size() > 0) {
						int riderCount = 0;

						for (Coverage coverage : insuranceAgreement
								.getIncludesCoverage()) {
							if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("BasicCoverage")) {
								riderList.add(coverage);
								break;

							}
						}

						for (Coverage coverage : insuranceAgreement
								.getIncludesCoverage()) {
							if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("Rider")) {
								if (coverage.getInsuredType() != null) {
									if (coverage.getInsuredType().equals("LA1")) {
										riderListLA1.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA2")) {
										riderListLA2.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA3")) {
										riderListLA3.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA4")) {
										riderListLA4.add(coverage);
									} else if (coverage.getInsuredType()
											.equals("LA5")) {
										riderListLA5.add(coverage);

									}
								}
							}
						}

						if (riderListLA1 != null && riderListLA1.size() > 0) {
							riderList.addAll(riderNames(riderListLA1));
						}
						if (riderListLA2 != null && riderListLA2.size() > 0) {
							riderList.addAll(riderNames(riderListLA2));
						}
						if (riderListLA3 != null && riderListLA3.size() > 0) {
							riderList.addAll(riderNames(riderListLA3));
						}
						if (riderListLA4 != null && riderListLA4.size() > 0) {
							riderList.addAll(riderNames(riderListLA4));
						}
						if (riderListLA5 != null && riderListLA5.size() > 0) {
							riderList.addAll(riderNames(riderListLA5));
						}
						for (Coverage coverage : riderList) {
							RiderTableForEapp riderData = new RiderTableForEapp();

							if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("BasicCoverage")) {
								// riderList.add(coverage);
								if (coverage.getProductType() != null) {
									// productName=
									// coverage.getProductCode().getCode();
									productName = coverage.getMarketingName();

								}
								if (coverage.getSumAssured() != null) {
									productSumAssured = coverage
											.getSumAssured().getAmount();

								}
								productPremiumAmount = coverage
										.getPremiumAmount();

								productPolicyTerm = coverage.getPolicyTerm();

								productPremiumPeriod = coverage
										.getPremiumPeriod();

								productPremiumTerm = coverage.getPremiumTerm();

								if (coverage.getPremiumFrequency() != null) {
									productPremiumFrequency = coverage
											.getPremiumFrequency().name();
								}
								if (coverage.getPremium() != null
										&& coverage.getPremium().size() > 0) {
									for (Premium premium : coverage
											.getPremium()) {
										if (premium.getNatureCode() != null
												&& premium
														.getNatureCode()
														.name()
														.equalsIgnoreCase(
																"TotalInitialPremium")) {
											if (premium.getAmount() != null
													&& premium.getAmount()
															.getAmount() != null) {
												totalInitialPremium = premium
														.getAmount()
														.getAmount();
											}
										} else if (premium.getNatureCode() != null
												&& premium
														.getNatureCode()
														.name()
														.equalsIgnoreCase(
																"TotalPremium")) {
											if (premium.getAmount() != null
													&& premium.getAmount()
															.getAmount() != null) {
												initialPremium = premium
														.getAmount()
														.getAmount();

											}
										}
									}
								}
							} else if (coverage.getCoverageCode() != null
									&& coverage.getCoverageCode().name()
											.equalsIgnoreCase("Rider")) {
								String lifeAssuredName = "";
								String riderName;
								Integer riderTerm;
								BigDecimal riderInitialPremium;
								BigDecimal riderSA;

								// riderName = coverage.getName();
								riderName = coverage.getTypeName();
								riderData.setRiderName(riderName);

								if (coverage.getSumAssured() != null) {
									riderSA = coverage.getSumAssured()
											.getAmount();
									riderData.setRiderSumAssured(riderSA);

								}
								riderTerm = coverage.getRiderTerm();
								// Value should be Blank in PDF;
								if ("OutPatient".equals(coverage.getName())
										|| "Dental".equals(coverage.getName())
										|| "HIP1"
												.equals(coverage.getTypeName())
										|| "HIP2"
												.equals(coverage.getTypeName())
										|| "HIP3"
												.equals(coverage.getTypeName())) {
									riderTerm = 0;
								}
								riderData.setRiderTerm(riderTerm);

								if (coverage.getPremium() != null
										&& coverage.getPremium().size() > 0) {
									for (Premium premium : coverage
											.getPremium()) {
										if (premium.getNatureCode() != null
												&& premium
														.getNatureCode()
														.name()
														.equalsIgnoreCase(
																"MinimumPremium")) {
											if (premium.getAmount() != null
													&& premium.getAmount()
															.getAmount() != null) {
												riderInitialPremium = premium
														.getAmount()
														.getAmount();
												// Value should be Blank in PDF;
												if ("OutPatient"
														.equals(coverage
																.getName())
														|| "Dental"
																.equals(coverage
																		.getName())) {
													BigDecimal bd = new BigDecimal(
															"0");
													riderInitialPremium = bd;
												}
												riderData
														.setRiderInitialPremium(riderInitialPremium);
											}

										}
									}
								}
								if (coverage.getInsuredType() != null) {
									if (coverage.getInsuredType()
											.equalsIgnoreCase("LA1")) {
										lifeAssuredName = getInsuredFullName();
									}
								}
								riderData.setLifeAssuredName(lifeAssuredName);
								riderdeatilslist.add(riderData);
							}
							// riderCount++;
						}

						/* rider Details */

						for (int i = 0; i < riderdeatilslist.size(); i++) {
							if (riderdeatilslist.get(i) != null) {

								RiderTableForEapp riderdetails = new RiderTableForEapp();
								riderdetails = riderdeatilslist.get(i);

								if ((riderdetails.getRiderName()
										.equalsIgnoreCase("HSRiders"))
										|| (riderdetails.getRiderName()
												.equalsIgnoreCase("HSRider"))) {
									riderHsextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderHsextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderHsextraInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderHsextraName = riderdetails
											.getRiderName();
									riderHsextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if ((riderdetails.getRiderName()
										.equalsIgnoreCase("ADDRiders"))
										|| (riderdetails.getRiderName()
												.equalsIgnoreCase("ADDRider"))) {
									riderAddextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderAddextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderAddextraInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderAddextraName = riderdetails
											.getRiderName();
									riderAddextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if ((riderdetails.getRiderName()
										.equalsIgnoreCase("HBRiders"))
										|| (riderdetails.getRiderName()
												.equalsIgnoreCase("HBRider"))) {
									riderHbextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderHbextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderHbextraInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderHbextraName = riderdetails
											.getRiderName();
									riderHbextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("CIRider")) {
									riderCIextraInsuredName = riderdetails
											.getLifeAssuredName();
									riderCIextraTerm = (riderdetails
											.getRiderTerm()).toString();
									riderCIextraInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderCIextraName = riderdetails
											.getRiderName();
									riderCIextraSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("AIRider")) {
									riderAIInsuredName = riderdetails
											.getLifeAssuredName();
									riderAITerm = (riderdetails.getRiderTerm())
											.toString();
									riderAIInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderAIName = riderdetails.getRiderName();
									riderAISumAssured = String.format("%,.2f",
											(riderdetails.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("RCCADBRider")) {
									riderRCCADBInsuredName = riderdetails
											.getLifeAssuredName();
									riderRCCADBTerm = (riderdetails
											.getRiderTerm()).toString();
									riderRCCADBInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderRCCADBName = riderdetails
											.getRiderName();
									riderRCCADBSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("ADBRider")) {
									riderADBRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderADBRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderADBRiderInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderADBRiderName = riderdetails
											.getRiderName();
									riderADBRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("WPRider")) {
									riderWPRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderWPRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderWPRiderInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderWPRiderName = riderdetails
											.getRiderName();
									riderWPRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("DDRider")) {
									riderDDRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderDDRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderDDRiderInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderDDRiderName = riderdetails
											.getRiderName();
									riderDDRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("RCCAIRider")) {
									riderRCCAIRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderRCCAIRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderRCCAIRiderInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderRCCAIRiderName = riderdetails
											.getRiderName();
									riderRCCAIRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("RCCADDRider")) {
									riderRCCADDRiderRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderRCCADDRiderRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderRCCADDRiderRiderInitialPremium = String
											.format("%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderRCCADDRiderRiderName = riderdetails
											.getRiderName();
									riderRCCADDRiderRiderSumAssured = String
											.format("%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								} else if (riderdetails.getRiderName()
										.equalsIgnoreCase("PBRider")) {
									riderPBRiderInsuredName = riderdetails
											.getLifeAssuredName();
									riderPBRiderTerm = (riderdetails
											.getRiderTerm()).toString();
									riderPBRiderInitialPremium = String.format(
											"%,.2f", (riderdetails
													.getRiderInitialPremium())
													.setScale(2,
															RoundingMode.DOWN));
									riderPBRiderName = riderdetails
											.getRiderName();
									riderPBRiderSumAssured = String.format(
											"%,.2f", (riderdetails
													.getRiderSumAssured())
													.setScale(2,
															RoundingMode.DOWN));
								}
							}
						}
					}
					/* Payment Details */
					try {
						if (insuranceAgreement != null) {
							if (insuranceAgreement.getAgreementPremium() != null
									&& insuranceAgreement.getAgreementPremium()
											.size() > 0) {
								for (Premium premium : insuranceAgreement
										.getAgreementPremium()) {
									if (premium.getNatureCode() != null
											&& premium
													.getNatureCode()
													.name()
													.equalsIgnoreCase(
															"RegularPremium")) {
										paymentDate = premium
												.getEffectivePeriod()
												.getEndDateTime();
										if (premium
												.getAttachedFinancialScheduler() != null
												&& premium
														.getAttachedFinancialScheduler()
														.size() > 0) {
											for (FinancialScheduler scheduler : premium
													.getAttachedFinancialScheduler()) {
												receiptNumber = scheduler
														.getTransactionRefNumber();
												paymentMethods = scheduler
														.getDescription();
											}
										}
									}
								}
							}
						}
					} catch (Exception e) {
						LOGGER.error("ScriptletSample Error :while getting payment details"
								+ e);
					}

				}
			} catch (Exception e) {
				LOGGER.error("ScriptletSample Error :while getting field value Payment"
						+ e);
			}

			/** Previous Policy */
			try {
				if (insured != null) {
					insuredUserSelections = insured.getSelectedOptions();

					if (insuredUserSelections != null
							&& insuredUserSelections.size() > 0) {
						for (UserSelection insUserselections : insuredUserSelections) {
							if (insUserselections != null
									&& insUserselections.getQuestionnaireType()
											.name().equalsIgnoreCase("Additional_Other")) {

								if (insUserselections.getQuestionId() == 152000) {
									previousPolicyQ1 = insUserselections
											.getDetailedInfo();
									previousPolicyQ1Other = StringUtils.isEmpty(insUserselections.getSelectedOptionExt())?
											StringUtils.EMPTY : insUserselections.getSelectedOptionExt();
								}
								else if (insUserselections.getQuestionId() == 153000) {
									previousPolicyQ2 = insUserselections
											.getDetailedInfo();
								}
								else if (insUserselections.getQuestionId() == 154010) {
									previousPolicyQ3 = insUserselections
											.getDetailedInfo();
								}
								else if (insUserselections.getQuestionId() == 154020) {
									previousPolicyQ4 = insUserselections
											.getDetailedInfo();
								}
								else if (insUserselections.getQuestionId() == 154030) {
									previousPolicyQ5 = insUserselections
											.getDetailedInfo();
								}
								else if (insUserselections.getQuestionId() == 154040) {
									previousPolicyQ6 = insUserselections
											.getDetailedInfo();
								}
								else if (insUserselections.getQuestionId() == 154050) {
									previousPolicyQ7 = insUserselections
											.getDetailedInfo();
								}
								else if (insUserselections.getQuestionId() == 154060) {
									previousPolicyQ8 = insUserselections
											.getDetailedInfo();
								}
								
							}
						}
					}
				}
			}
			catch (Exception e) {
				LOGGER.error("Error in getting previouspolicy question"
						+ e.getMessage());
				e.printStackTrace();
			}

			try {
				if (insuredPreviousPolicyList != null
						&& insuredPreviousPolicyList.size() > 0) {
					/*LOGGER.error("insuredPreviousPolicyList  "
							+ insuredPreviousPolicyList.size());*/
					int count = 0;
					for (Insured ins : insuredPreviousPolicyList) {
						if (ins.getIsPartyRoleIn() != null && ins.getIsPartyRoleIn().getAgreementExtension()!=null
								&& !StringUtils.isEmpty(ins.getIsPartyRoleIn().getAgreementExtension().getIllustrationPolicyNumber())) {
							/*count = Integer.parseInt(ins.getIsPartyRoleIn()
									.getAgreementExtension().getPolicyNumber()
									.toString());*/
							count++;
						
						
						if (count == 1) {
							if (ins.getIsPartyRoleIn() != null) {
								InsuranceAgreement agmnt = (InsuranceAgreement) ins
										.getIsPartyRoleIn();
								if (agmnt != null) {
									firstCompanyName = agmnt.getInsurerName();
									/*firstPolicyStatus = agmnt.getStatusCode()
											.name();*/
									if (agmnt.getAgreementExtension() != null) {
										/*firstPrevInsuredName = agmnt
												.getAgreementExtension()
												.getPrevPolicyInsuredName();
										firstPrePolicyType = agmnt
												.getAgreementExtension()
												.getRemark();*/
										firstPrePolicyEffectiveDate = agmnt
												.getAgreementExtension()
												.getSpajDate();
										if(agmnt.getAgreementExtension().getIllustrationPolicyNumber()!=null){
											firstPolicyNumber = agmnt.getAgreementExtension().getIllustrationPolicyNumber();
										}
										for (Coverage coverage : agmnt
												.getIncludesCoverage()) {
											/*firstTypeOfInsurance = coverage
													.getMarketingName();*/
											if(coverage.getSumAssured()!=null){
											firstPrevSumAssured = coverage.getSumAssured().getAmount();
											}
										}

									}
								}
							}
						} else if (count == 2) {
							if (ins.getIsPartyRoleIn() != null) {
								InsuranceAgreement agmnt = (InsuranceAgreement) ins
										.getIsPartyRoleIn();
								if (agmnt != null) {
									secondCompanyName = agmnt.getInsurerName();
									/*secondPolicyStatus = agmnt.getStatusCode()
											.name();*/
									if (agmnt.getAgreementExtension() != null) {
										/*secondPrevInsuredName = agmnt
												.getAgreementExtension()
												.getPrevPolicyInsuredName();
										secondPrePolicyType = agmnt
												.getAgreementExtension()
												.getRemark();*/
										secondPrePolicyEffectiveDate = agmnt
												.getAgreementExtension()
												.getSpajDate();
										if(agmnt.getAgreementExtension().getIllustrationPolicyNumber()!=null){
											secondPolicyNumber = agmnt.getAgreementExtension().getIllustrationPolicyNumber();
										}
										for (Coverage coverage : agmnt
												.getIncludesCoverage()) {
											/*secondTypeOfInsurance = coverage
													.getMarketingName();*/
											if(coverage.getSumAssured()!=null){
											secondPrevSumAssured = coverage
													.getSumAssured()
													.getAmount();
											}
										}
									}
								}
							}
						} else if (count == 3) {
							if (ins.getIsPartyRoleIn() != null) {
								InsuranceAgreement agmnt = (InsuranceAgreement) ins
										.getIsPartyRoleIn();
								if (agmnt != null) {
									thirdCompanyName = agmnt.getInsurerName();
									/*thirdPolicyStatus = agmnt.getStatusCode()
											.name();*/
									if (agmnt.getAgreementExtension() != null) {
										/*thirdPrevInsuredName = agmnt
												.getAgreementExtension()
												.getPrevPolicyInsuredName();
										thirdPrePolicyType = agmnt
												.getAgreementExtension()
												.getRemark();*/
										thirdPrePolicyEffectiveDate = agmnt
												.getAgreementExtension()
												.getSpajDate();
										if(agmnt.getAgreementExtension().getIllustrationPolicyNumber()!=null){
											thirdPolicyNumber = agmnt.getAgreementExtension().getIllustrationPolicyNumber();
										}

										for (Coverage coverage : agmnt
												.getIncludesCoverage()) {
											/*thirdTypeOfInsurance = coverage
													.getMarketingName();*/
											if(coverage.getSumAssured()!=null){
											thirdPrevSumAssured = coverage
													.getSumAssured()
													.getAmount();
											}
										}
									}
								}
							}
						} else if (count == 4) {
							if (ins.getIsPartyRoleIn() != null) {
								InsuranceAgreement agmnt = (InsuranceAgreement) ins
										.getIsPartyRoleIn();
								if (agmnt != null) {
									fourthCompanyName = agmnt.getInsurerName();
									/*fourthPolicyStatus = agmnt.getStatusCode()
											.name();*/
									if (agmnt.getAgreementExtension() != null) {
										/*fourthPrevInsuredName = agmnt
												.getAgreementExtension()
												.getPrevPolicyInsuredName();
										fourthPrePolicyType = agmnt
												.getAgreementExtension()
												.getRemark();*/
										fourthPrePolicyEffectiveDate = agmnt
												.getAgreementExtension()
												.getSpajDate();
										for (Coverage coverage : agmnt
												.getIncludesCoverage()) {
											/*fourthTypeOfInsurance = coverage
													.getMarketingName();*/
											if(coverage.getSumAssured()!=null){
											fourthPrevSumAssured = coverage
													.getSumAssured()
													.getAmount();
											}
										}
									}
								}
							}
						} else if (count == 5) {
						}
					}
				}
				}
			} catch (Exception e) {
				LOGGER.warn("Error in getting previous policy details" + e);
			}

			/** Insured User selection */

			try {
				if (insured != null) {
					insuredUserSelections = insured.getSelectedOptions();

					if (insuredUserSelections != null
							&& insuredUserSelections.size() > 0) {
						mapQuestions();
					}
				}
			} catch (Exception e) {
				LOGGER.error("Error in getting insured userselection"
						+ e.getMessage());
				e.printStackTrace();
			}
			
			additionalQuestion = buildAdditionalQuestion();			

			/* Agent Details */
			try {
				if (agent != null) {
					agentPerson = (Person) agent.getPlayerParty();
					agentContactPreferences = agentPerson.getPreferredContact();
					// agentCode = agent.getPartyIdentifier();
				}
			} catch (Exception e) {
				LOGGER.error("Error while getting agent details : "
						+ e.getMessage());
			}

			try {
				if (insured != null && insured.getDeclares() != null) {
					insuredDeclarationDateTime = insured.getDeclares()
							.getActualTimePeriod();
					insuredDeclarationPlace = insured.getDeclares().getName();
				}
			} catch (Exception e) {
				LOGGER.error("Error while getting insured declaration details : "
						+ e.getMessage());
			}

			try {
				if (proposer != null && proposer.getDeclares() != null) {
					proposerDeclarationDateTime = proposer.getDeclares()
							.getActualTimePeriod();
					proposerDeclarationPlace = proposer.getDeclares().getName();
				}
			} catch (Exception e) {
				LOGGER.error("Error while getting proposer declaration details : "
						+ e.getMessage());
			}

		} catch (Exception e) {
			LOGGER.warn("Error in beforeDetailEval" + e);
		}
		
	}

	private void mapQuestions() {
		for (UserSelection insUserselections : insuredUserSelections) {
			if (insUserselections != null
					&& insUserselections.getQuestionnaireType()
							.name().equalsIgnoreCase("Other")) {

				if (insUserselections.getQuestionId() == 121001) {
					illnessTreatmentQ1 = insUserselections
							.getDetailedInfo();
				}else if (insUserselections.getQuestionId() == 121011) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.HYPERTENSION.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease1 = GeneraliConstants.THAI_HYPERTENSION;
						}
						if(GeneraliConstants.HEARTDISEASE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease2 =GeneraliConstants.THAI_HEARTDISEASE;
						}
						if(GeneraliConstants.CORONHEARTDISEASE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease3 =GeneraliConstants.THAI_CORONHEARTDISEASE ;
						}
						if(GeneraliConstants.CARDIO.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease4 = GeneraliConstants.THAI_CARDIO;
						}
						if(GeneraliConstants.CEREBRO.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease5 = GeneraliConstants.THAI_CEREBRO;
						}
						if(GeneraliConstants.PARALYSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease6 = GeneraliConstants.THAI_PARALYSIS;
						}
						if(GeneraliConstants.DIABETICS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease7 = GeneraliConstants.THAI_DIABETICS;
						}
						if(GeneraliConstants.THYROID.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease8 = GeneraliConstants.THAI_THYROID;
						}
					}
				}
				else if (insUserselections.getQuestionId() == 1210121) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ111 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210131) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ112 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210141) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ113 = GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210151) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ114 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210122) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ121 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210132) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ122 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210142) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ123 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210152) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ124 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210123) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ131 =GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210133) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ132 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210143) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ133 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");
					}
				} else if (insUserselections.getQuestionId() == 1210153) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ134 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210124) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ141 = GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210134) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ142 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210144) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ143 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");
					}
				} else if (insUserselections.getQuestionId() == 1210154) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ144 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210125) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ151 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210135) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ152 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210145) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ153 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");
					}
				} else if (insUserselections.getQuestionId() == 1210155) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ154 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210126) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ161 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210136) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ162 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210146) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ163 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");
					}
				} else if (insUserselections.getQuestionId() == 1210156) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ164 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210127) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ171 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210137) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ172 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210147) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ173 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");
					}
				} else if (insUserselections.getQuestionId() == 1210157) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ174 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210128) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ181 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210138) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ182 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1210148) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ183 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");
					}
				} else if (insUserselections.getQuestionId() == 1210158) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ184 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 121020) {
					illnessTreatmentQ2 = insUserselections
							.getDetailedInfo();

				} else if (insUserselections.getQuestionId() == 121021) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.CANCER.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease9 = GeneraliConstants.THAI_CANCER;
						}
						if(GeneraliConstants.LYMPH.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease10 = GeneraliConstants.THAI_LYMPH;
						}
						if(GeneraliConstants.TUMOR.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease11 = GeneraliConstants.THAI_TUMOR;
						}
						if(GeneraliConstants.MASSCYST.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease12 = GeneraliConstants.THAI_MASSCYST;
						}
					}
				} else if (insUserselections.getQuestionId() == 1210221) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ211 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210231) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ212 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210241) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ213 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210251) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ214 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210222) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ221 =GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210232) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ222 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210242) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ223 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210252) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ224 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210223) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ231 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210233) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ232 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210243) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ233 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210253) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ234 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210224) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ241 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210234) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ242 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210244) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ243 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210254) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ244 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}
				else if (insUserselections.getQuestionId() == 121031) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.PANCREATITIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease13 = GeneraliConstants.THAI_PANCREATITIS;
						}
						if(GeneraliConstants.KIDNEY.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease14 = GeneraliConstants.THAI_KIDNEY;
						}
						if(GeneraliConstants.JAUNDICE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease15 = GeneraliConstants.THAI_JAUNDICE;
						}
						if(GeneraliConstants.SPLENOMEGALY.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease16 = GeneraliConstants.THAI_SPLENOMEGALY;
						}
						if(GeneraliConstants.PEPTICULCER.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease17 = GeneraliConstants.THAI_PEPTICULCER;
						}
						if(GeneraliConstants.LIVER.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease18 = GeneraliConstants.THAI_LIVER;
						}
						if(GeneraliConstants.ALOCHOLISM.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease19 = GeneraliConstants.THAI_ALOCHOLISM;
						}
					}
				} 
				else if (insUserselections.getQuestionId() == 121030) {
					illnessTreatmentQ3 = insUserselections
							.getDetailedInfo();

				} else if (insUserselections.getQuestionId() == 1210321) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ311 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210331) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ312 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210341) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ313 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210351) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ314 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210322) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ321 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210332) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ322 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210342) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ323 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210352) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ324 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210323) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ331 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210333) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ332 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210343) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ333 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210353) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ334 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210324) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ341 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210334) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ342 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210344) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ343 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210354) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ344 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210325) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ351 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210335) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ352 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210345) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ353 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210355) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ354 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210326) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ361 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210336) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ362 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210346) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ363 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210356) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ364 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210327) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ371 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210337) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ372 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210347) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ373 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210357) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ374 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 121040) {
					illnessTreatmentQ4 = insUserselections
							.getDetailedInfo();

				} 
				else if (insUserselections.getQuestionId() == 121041) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.PNEUMONIA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease20 = GeneraliConstants.THAI_PNEUMONIA;
						}
						if(GeneraliConstants.TUBERCULOSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease21 = GeneraliConstants.THAI_TUBERCULOSIS;
						}
						if(GeneraliConstants.ASTHMA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease22 = GeneraliConstants.THAI_ASTHMA;
						}
						if(GeneraliConstants.PULMONARY.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease23 = GeneraliConstants.THAI_PULMONARY;
						}
						if(GeneraliConstants.EMPHYSEMA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease24 = GeneraliConstants.THAI_EMPHYSEMA;
						}
						if(GeneraliConstants.SLEEPAPNEA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease25 = GeneraliConstants.THAI_SLEEPAPNEA;
						}
					}
				} 
				
				else if (insUserselections.getQuestionId() == 1210421) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ411 =GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210431) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ412 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210441) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ413 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210451) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ414 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210422) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ421 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210432) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ422 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210442) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ423 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210452) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ424 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210423) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ431 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210433) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ432 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210443) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ433 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210453) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ434 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210424) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ441 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210434) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ442 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210444) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ443 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210454) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ444 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210425) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ451 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210435) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ452 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210445) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ453 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210455) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ454 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210426) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ461 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210436) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ462 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210446) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ463 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210456) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ464 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 121050) {
					illnessTreatmentQ5 = insUserselections
							.getDetailedInfo();

				}
				else if (insUserselections.getQuestionId() == 121051) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.IMPAIREDVISION.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease26 = GeneraliConstants.THAI_IMPAIREDVISION;
						}
						if(GeneraliConstants.RETINA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease27 = GeneraliConstants.THAI_RETINA;
						}
						if(GeneraliConstants.GLAUCOMA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease28 = GeneraliConstants.THAI_GLAUCOMA;
						}
					}
				} 
				else if (insUserselections.getQuestionId() == 1210521) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ511 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210531) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ512 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210541) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ513 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210551) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ514 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210522) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ521 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210532) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ522 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210542) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ523 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210552) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ524 =GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210523) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ531 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210533) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ532 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210543) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ533 =GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210553) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ534 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}
		         
				else if (insUserselections.getQuestionId() == 121060) {
					illnessTreatmentQ6 = insUserselections
							.getDetailedInfo();

				}
				else if (insUserselections.getQuestionId() == 121061) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.PARKINSONS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease29 = GeneraliConstants.THAI_PARKINSONS;
						}
						if(GeneraliConstants.ALZHEIMERS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease30 = GeneraliConstants.THAI_ALZHEIMERS;
						}
						if(GeneraliConstants.EPILEPSY.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease31 = GeneraliConstants.THAI_EPILEPSY;
						}
					}
				} 
				
				else if (insUserselections.getQuestionId() == 1210621) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ611 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210631) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ612 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210641) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ613 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210651) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ614 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210622) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ621 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210632) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ622 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210642) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ623 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210652) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ624 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210623) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ631 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210633) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ632 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210643) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ633 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210653) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ634 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 121070) {
					illnessTreatmentQ7 = insUserselections
							.getDetailedInfo();

				} 
				else if (insUserselections.getQuestionId() == 121071) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.ARTHRITIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease32 = GeneraliConstants.THAI_ARTHRITIS;
						}
						if(GeneraliConstants.GOUT.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease33 = GeneraliConstants.THAI_GOUT;
						}
						if(GeneraliConstants.SLE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease34 = GeneraliConstants.THAI_SLE;
						}
						if(GeneraliConstants.SCLERODERMA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease35 = GeneraliConstants.THAI_SCLERODERMA;
						}
						if(GeneraliConstants.BLOODDISEASE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease36 = GeneraliConstants.THAI_BLOODDISEASE;
						}
					}
				} 
				
				else if (insUserselections.getQuestionId() == 1210721) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ711 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 12107231) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ712 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210741) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ713 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210751) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ714 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210722) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ721 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210732) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ722 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210742) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ723 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210752) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ724 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210723) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ731 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210733) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ732 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210743) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ733 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210753) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ734 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210724) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ741 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210734) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ742 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210744) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ743 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210754) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ744 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 12107025) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ751 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210735) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ752 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210745) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ753 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210755) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ754 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 121080) {
					illnessTreatmentQ8 = insUserselections
							.getDetailedInfo();

				}
				else if (insUserselections.getQuestionId() == 121081) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.PSYCHOSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease37 =GeneraliConstants.THAI_PSYCHOSIS;
						}
						if(GeneraliConstants.NEUROSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease38 = GeneraliConstants.THAI_NEUROSIS;
						}
						if(GeneraliConstants.DEPRESSION.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease39 = GeneraliConstants.THAI_DEPRESSION;
						}
						if(GeneraliConstants.DOWNS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease40 = GeneraliConstants.THAI_DOWNS;
						}
						if(GeneraliConstants.PHYSICALDIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease41 = GeneraliConstants.THAI_PHYSICALDIS;
						}
					}
				}
				
				else if (insUserselections.getQuestionId() == 1210821) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ811 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210831) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ812 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210841) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ813 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210851) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ814 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210822) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ821 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210832) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ822 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210842) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ823 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210852) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ824 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210823) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ831 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210833) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ832 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210843) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ833 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210853) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ834 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210824) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ841 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210834) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ842 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210844) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ843 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210854) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ844 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210825) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ851 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210835) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ852 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210845) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ853 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210855) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ854 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 121090) {
					illnessTreatmentQ9 = insUserselections
							.getDetailedInfo();

				} 
				else if (insUserselections.getQuestionId() == 121091) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.AIDS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease42 = GeneraliConstants.THAI_AIDS;
						}
						if(GeneraliConstants.VENEREAL.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease43 = GeneraliConstants.THAI_VENEREAL;
						}
					}
				}
				else if (insUserselections.getQuestionId() == 1210921) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ911 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210931) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ912 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210941) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ913 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210951) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ914 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210922) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ921 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210932) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ922 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1210942) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ923 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1210952) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ924 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 122010) {
					illnessTreatmentQ10 = insUserselections
							.getDetailedInfo();

				} 
				else if (insUserselections.getQuestionId() == 122011) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.CHESTPAIN.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease44 = GeneraliConstants.THAI_CHESTPAIN;
						}
						if(GeneraliConstants.PALPITATION.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease45 =GeneraliConstants.THAI_PALPITATION;
						}
						if(GeneraliConstants.ABNORFATIGUE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease46 = GeneraliConstants.THAI_ABNORFATIGUE;
						}
						if(GeneraliConstants.MUSCWEAK.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease47 = GeneraliConstants.THAI_MUSCWEAK;
						}
						if(GeneraliConstants.ABNORPHYMOV.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease48 = GeneraliConstants.THAI_ABNORPHYMOV;
						}
						if(GeneraliConstants.LOSSSENSNEU.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease49 = GeneraliConstants.THAI_LOSSSENSNEU;
						}
					}
				}
				
				else if (insUserselections.getQuestionId() == 1220121) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1011 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 1220131) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1012 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 1220141) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1013 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				}

				else if (insUserselections.getQuestionId() == 1220151) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1014 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat("  ");

					}
				} else if (insUserselections.getQuestionId() == 1220122) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1021 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220132) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1022 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220142) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1023 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220152) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1024 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220123) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1031 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220133) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1032 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220143) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1033 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220153) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1034 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220124) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1041 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220134) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1042 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220144) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1043 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220154) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1044 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220125) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1051 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220135) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1052 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220145) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1053 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220155) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1054 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220126) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1061 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat("  ");

					}
				} else if (insUserselections.getQuestionId() == 1220136) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1062 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220146) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1063 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220156) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1064 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 122020) {
					illnessTreatmentQ11 = insUserselections
							.getDetailedInfo();

				}
				else if (insUserselections.getQuestionId() == 122021) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.CHRONICSTOMACHACHE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease50 = GeneraliConstants.THAI_CHRONICSTOMACHACHE;
						}
						if(GeneraliConstants.HEMATEMESIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease51 = GeneraliConstants.THAI_HEMATEMESIS;
						}
						if(GeneraliConstants.DROPSY.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease52 = GeneraliConstants.THAI_DROPSY;
						}
						if(GeneraliConstants.CHRONICDIARRHEA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease53 = GeneraliConstants.THAI_CHRONICDIARRHEA;
						}
						if(GeneraliConstants.HEMATURIA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease54 = GeneraliConstants.THAI_HEMATURIA;
						}
						if(GeneraliConstants.CHRONICCOUGH.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease55 = GeneraliConstants.THAI_CHRONICCOUGH;
						}
						if(GeneraliConstants.HEMOPTYSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease56 = GeneraliConstants.THAI_HEMOPTYSIS;
						}
					}
				}

				else if (insUserselections.getQuestionId() == 1220221) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1111 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220231) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1112 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220241) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1113 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220251) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1114 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220222) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1121 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220232) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1122 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220242) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1123 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220252) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1124 =GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220223) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1131 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220233) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1132 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220243) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1133 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220253) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1134 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220224) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1141 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220234) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1142 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220244) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1143 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220254) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1144 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220225) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1151 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220235) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1152 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220245) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1153 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220255) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1154 =GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220226) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1161 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220236) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1162 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220246) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1163 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220256) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1164 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220227) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1171 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220237) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1172 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220247) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1173 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220257) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1174 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 122030) {
					illnessTreatmentQ12 = insUserselections
							.getDetailedInfo();
				}
				else if (insUserselections.getQuestionId() == 122031) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.PALPABLETUMOR.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease57 = GeneraliConstants.THAI_PALPABLETUMOR;
						}
						if(GeneraliConstants.CHRONICSEVHEADACHE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease58 = GeneraliConstants.THAI_CHRONICSEVHEADACHE;
						}

					}
				}
				else if (insUserselections.getQuestionId() == 1220321) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1211 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220331) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1212 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220341) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1213 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220351) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1214 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220322) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1221 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220332) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1222 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220342) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1223 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220352) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1224 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 122040) {
					illnessTreatmentQ13 = insUserselections
							.getDetailedInfo();
				}
				else if (insUserselections.getQuestionId() == 122041) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.CHRONICJOINT.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease59 = GeneraliConstants.THAI_CHRONICJOINT;
						}
						if(GeneraliConstants.BRUISES.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease60 =GeneraliConstants.THAI_BRUISES;
						}

					}
				}
				else if (insUserselections.getQuestionId() == 1220421) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1311 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1220431) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1312 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1220441) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1313 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");
					}
				} else if (insUserselections.getQuestionId() == 1220451) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1314 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1220422) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1321 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1220432) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1322 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				} else if (insUserselections.getQuestionId() == 1220442) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1323 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");
					}
				} else if (insUserselections.getQuestionId() == 1220452) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1324 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");
					}
				}

				else if (insUserselections.getQuestionId() == 122050) {
					illnessTreatmentQ14 = insUserselections
							.getDetailedInfo();

				}
				else if (insUserselections.getQuestionId() == 122051) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.IMPAIREDVISION2.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease61 = GeneraliConstants.THAI_IMPAIREDVISION2;
						}
						if(GeneraliConstants.SLOWDEV.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease62 = GeneraliConstants.THAI_SLOWDEV;
						}
						if(GeneraliConstants.HURTONESELF.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease63 = GeneraliConstants.THAI_HURTONESELF;
						}

					}
				}
				else if (insUserselections.getQuestionId() == 1220521) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1411 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220531) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1412 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220541) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1413 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220551) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1414 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220522) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1421 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220532) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1422 =GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220542) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1423 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220552) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1424 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220523) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1431 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220533) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1432 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1220543) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1433 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1220553) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1434 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 123010) {
					illnessTreatmentQ15 = insUserselections
							.getDetailedInfo();
					if(illnessTreatmentQ15.equalsIgnoreCase("Yes"))
					{
						disease64=GeneraliConstants.THAI_FATIGUE;
					}
				} 

				else if (insUserselections.getQuestionId() == 123011) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1511 =GeneraliConstants.THAI_DATE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 123012) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1512 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 123013) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1513 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 123014) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1514 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} 

				else if (insUserselections.getQuestionId() == 123020) {
					illnessTreatmentQ16 = insUserselections
							.getDetailedInfo();
					if(illnessTreatmentQ16.equalsIgnoreCase("Yes"))
					{
						disease65=GeneraliConstants.THAI_WGHTLOSS;
					}
				} 

				else if (insUserselections.getQuestionId() == 123021) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1611 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 123022) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1612 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 123023) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1613 =GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 123024) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1614 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 123030) {
					illnessTreatmentQ17 = insUserselections
							.getDetailedInfo();
					if(illnessTreatmentQ17.equalsIgnoreCase("Yes"))
					{
						disease66=GeneraliConstants.THAI_CHRONICDIARREA;
					}
				}

				else if (insUserselections.getQuestionId() == 123031) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1711 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 123032) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1712 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 123033) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1713 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 123034) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1714 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 123040) {
					illnessTreatmentQ18 = insUserselections
							.getDetailedInfo();
					if(illnessTreatmentQ18.equalsIgnoreCase("Yes"))
					{
						disease67=GeneraliConstants.THAI_PROLONGEDFEVER;
					}
				}
				else if (insUserselections.getQuestionId() == 123041) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1811 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 123042) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1812 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 123043) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1813 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 123044) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1814 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 123050) {
					illnessTreatmentQ19 = insUserselections
							.getDetailedInfo();
					if(illnessTreatmentQ19.equalsIgnoreCase("Yes"))
					{
						disease68=GeneraliConstants.THAI_CHRONICSKIN;
					}
				}

				else if (insUserselections.getQuestionId() == 123051) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1911 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 123052) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1912 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 123053) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1913 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				}

				else if (insUserselections.getQuestionId() == 123054) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ1914 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 124010) {
					illnessTreatmentQ20 = insUserselections
							.getDetailedInfo();

				} else if (insUserselections.getQuestionId() == 124011) {
					if(!StringUtils.isBlank(insUserselections.getSelectedOptionExt()))
					{illnessTreatmentQ2011 = insUserselections
							.getSelectedOptionExt().concat("  ");
					}

				} else if (insUserselections.getQuestionId() == 124020) {
					illnessTreatmentQ21 = insUserselections
							.getDetailedInfo();

				}
				else if (insUserselections.getQuestionId() == 124021) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2111 =insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 124022) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2112 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 124023) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2113 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 124024) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2114 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 124025) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2121 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 124030) {
					illnessTreatmentQ22 = insUserselections
							.getDetailedInfo();

				}
				else if (insUserselections.getQuestionId() == 124031) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2211 = insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 124032) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2212 =GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 124033) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2213 =GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 124034) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2214 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 124035) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2215 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}
				else if (insUserselections.getQuestionId() == 125010) {
					illnessTreatmentQ23 = insUserselections
							.getDetailedInfo();

				} 
				 
				else if (insUserselections.getQuestionId() == 125011) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2311 = insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 125012) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2312 = "รักษาเมื่อ:  "+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 125013) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2313 = "โดย: "+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 125014) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2314 = "ผลการตรวจ: "+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 125015) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2315 = "ข้อสังเกตล: "+insUserselections
								.getDetailedInfo().concat("\n");

					}
				}

				else if (insUserselections.getQuestionId() == 125020) {
					illnessTreatmentQ24 = insUserselections
							.getDetailedInfo();

				}

				else if (insUserselections.getQuestionId() == 125021) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2411 = "อาการตอนนี้: "+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 125022) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2412 ="รักษาเมื่อ: "+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 125023) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2413 ="ที่:"+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 125024) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2414 ="ผลการตรวจ: "+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				}
			} else if (insUserselections != null
					&& insUserselections.getQuestionnaireType() != null
					&& insUserselections.getQuestionnaireType()
							.name().equalsIgnoreCase("Health")) {

				if (insUserselections.getQuestionId() == 131010) {
					illnessTreatmentQ25 = insUserselections
							.getDetailedInfo();

				} 
				else if (insUserselections.getQuestionId() == 131011) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.OTITISMEDIA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease73 = GeneraliConstants.THAI_OTITISMEDIA;
						}
						if(GeneraliConstants.CHRONICTONSIL.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease74 = GeneraliConstants.THAI_CHRONICTONSIL;
						}
						if(GeneraliConstants.SINUSITIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease75 = GeneraliConstants.THAI_SINUSITIS;
						}
						if(GeneraliConstants.MIGRAINE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease76 = GeneraliConstants.THAI_MIGRAINE;
						}
						if(GeneraliConstants.ALLERGY.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease77 = GeneraliConstants.THAI_ALLERGY;
						}
						if(GeneraliConstants.CHRONICBRONCHTIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease78 = GeneraliConstants.THAI_CHRONICBRONCHTIS;
						}
					}
				}
				
				else if (insUserselections.getQuestionId() == 1310121) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2511 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310131) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2512 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310141) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2513 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310151) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2514 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310161) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2515 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",   ");

					}
				} else if (insUserselections.getQuestionId() == 1310122) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2521 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310132) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2522 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310142) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2523 =  GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310152) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2524 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310162) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2525 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310123) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2531 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310133) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2532 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310143) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2533 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310153) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2534 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310163) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2535 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310124) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2541 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310134) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2542 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310144) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2543 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310154) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2544 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310164) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2545 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310125) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2551 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310135) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2552 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310145) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2553 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310155) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2554 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310165) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2555 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310126) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2561 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310136) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2562 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310146) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2563 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310156) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2564 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310166) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2565 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 131020) {
					illnessTreatmentQ26 = insUserselections
							.getDetailedInfo();

				} else if (insUserselections.getQuestionId() == 131021) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.GASTROESOPHAGEAL.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease79 = GeneraliConstants.THAI_GASTROESOPHAGEAL;
						}
						if(GeneraliConstants.URINARYTRACTSTONE.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease80 = GeneraliConstants.THAI_URINARYTRACTSTONE;
						}
						if(GeneraliConstants.CHOLECYSTITIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease81 = GeneraliConstants.THAI_CHOLECYSTITIS;
						}
						if(GeneraliConstants.HERNIA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease82 = GeneraliConstants.THAI_HERNIA;
						}
						if(GeneraliConstants.HEMORRHOID.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease83 = GeneraliConstants.THAI_HEMORRHOID;
						}
						if(GeneraliConstants.ANALFISTULA.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease84 = GeneraliConstants.THAI_ANALFISTULA;
						}
					}
				}else if (insUserselections.getQuestionId() == 1310221) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2611 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310231) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2612 =GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310241) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2613 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310251) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2614 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310261) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2615 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310222) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2621 =GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310232) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2622 =GeneraliConstants.NOTICE+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310242) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2623 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310252) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2624 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310262) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2625 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310223) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2631 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310233) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2632 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310243) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2633 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310253) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2634 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310263) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2635 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310224) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2641 =GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310234) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2642 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310244) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2643 =GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310254) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2644 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310264) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2645 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310226) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2661 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310236) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2662 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310246) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2663 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310256) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2664 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310266) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2665 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310227) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2671 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310237) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2672 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310247) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2673 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310257) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2674 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310267) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2675 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}

				else if (insUserselections.getQuestionId() == 131030) {
					illnessTreatmentQ27 = insUserselections
							.getDetailedInfo();

				}
				
				else if (insUserselections.getQuestionId() == 131031) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.ENDOMETROSIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease85 = GeneraliConstants.THAI_ENDOMETROSIS;
						}
						if(GeneraliConstants.SPONDYLOLISTHESIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease86 = GeneraliConstants.THAI_SPONDYLOLISTHESIS;
						}
						if(GeneraliConstants.HERNIATEDVERTEBRALDISC.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease87 = GeneraliConstants.THAI_HERNIATEDVERTEBRALDISC;
						}
						if(GeneraliConstants.HERNIATEDNUCLEUSPULPOSUS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease88 = GeneraliConstants.THAI_HERNIATEDNUCLEUSPULPOSUS;
						}
						if(GeneraliConstants.DEGENERATIVEOSTEOARTHRITIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease89 = GeneraliConstants.THAI_DEGENERATIVEOSTEOARTHRITIS;
						}
						if(GeneraliConstants.CHRONICTENDINITIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease90 = GeneraliConstants.THAI_CHRONICTENDINITIS;
						}
						if(GeneraliConstants.PERIPHERNEURITIS.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease91 = GeneraliConstants.THAI_PERIPHERNEURITIS;
						}
					}
				}
				else if (insUserselections.getQuestionId() == 1310321) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2711 =GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310331) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2712 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310341) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2713 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310351) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2714 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310361) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2715 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310322) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2721 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310332) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2722 =GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310342) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2723 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310352) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2724 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310362) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2725 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310323) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2731 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310333) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2732 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310343) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2733 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310353) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2734 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310363) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2735 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310324) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2741 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310334) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2742 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310344) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2743 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310354) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2744 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310364) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2745 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310325) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2751 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310335) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2752 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310345) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2753 =GeneraliConstants.CURSYMPTOM+ insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310355) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2754 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310365) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2755 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310326) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2761 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310336) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2762 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310346) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2763 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310356) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2764 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310366) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2765 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310327) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2771 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310337) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2772 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310347) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2773 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310357) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2774 =GeneraliConstants.TRTMETHOD+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310367) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2775 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 131040) {
					illnessTreatmentQ28 = insUserselections
							.getDetailedInfo();

				}
				else if (insUserselections.getQuestionId() == 131041) {
					for(GLISelectedOption option : insUserselections.getSelectedOptions()){
						if(GeneraliConstants.AUTISTIC.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease92 = GeneraliConstants.THAI_AUTISTIC;
						}
						if(GeneraliConstants.HYPERACTIVITY.equalsIgnoreCase(option.getSelectedOptionKey()) 
								&& "true".equalsIgnoreCase(option.getSelectedOptionValue())){
							disease93 = GeneraliConstants.THAI_HYPERACTIVITY;
						}
						
					}
				}
				
				else if (insUserselections.getQuestionId() == 1310421) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2811 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310431) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2812 =GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310441) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2813 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310451) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2814 =GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310461) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2815 =GeneraliConstants.HOSPITAL+ insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310422) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2821 = GeneraliConstants.THAI_DATE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310432) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2822 = GeneraliConstants.NOTICE+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310442) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2823 = GeneraliConstants.CURSYMPTOM+insUserselections
								.getDetailedInfo().concat("\n");

					}
				} else if (insUserselections.getQuestionId() == 1310452) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2824 = GeneraliConstants.TRTMETHOD+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				} else if (insUserselections.getQuestionId() == 1310462) {
					if (!StringUtils.isBlank(insUserselections.getDetailedInfo())) {
						illnessTreatmentQ2825 = GeneraliConstants.HOSPITAL+insUserselections
								.getDetailedInfo().concat(",  ");

					}
				}
			} else if (insUserselections != null
					&& insUserselections.getQuestionnaireType() != null
					&& insUserselections.getQuestionnaireType()
							.name()
							.equalsIgnoreCase("Lifestyle")) {

				if (insUserselections.getQuestionId() == 111000) {
					lifestyleQuest1 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 111001) {
					lifestyleQuest2 = insUserselections
							.getDetailedInfo();

				} else if (insUserselections.getQuestionId() == 111002) {
					lifestyleQuest3 = insUserselections
							.getDetailedInfo();

				} else if (insUserselections.getQuestionId() == 111003) {
					lifestyleQuest4 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 111004) {
					lifestyleQuest5 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 111005) {
					lifestyleQuest6 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 111006) {
					lifestyleQuest7 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 112000) {
					lifestyleQuest8 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 112001) {
					lifestyleQuest9 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 112002) {
					lifestyleQuest10 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 112003) {
					lifestyleQuest11 = insUserselections
							.getDetailedInfo();
					if(!StringUtils.isBlank(lifestyleQuest11))
					{
						lifestyleQuest11=this.dateFormatter(lifestyleQuest11);
					}
					
				} else if (insUserselections.getQuestionId() == 113000) {
					lifestyleQuest12 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 113001) {
					lifestyleQuest13 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 114000) {
					lifestyleQuest14 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 114001) {
					lifestyleQuest15 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 114002) {
					lifestyleQuest16 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 114003) {
					lifestyleQuest17 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 114004) {
					lifestyleQuest18 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 115000) {
					lifestyleQuest19 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 115001) {
					lifestyleQuest20 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 115002) {
					lifestyleQuest21 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 115003) {
					lifestyleQuest22 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 115004) {
					lifestyleQuest23 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 116000) {
					lifestyleQuest24 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 116001) {
					lifestyleQuest25 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 116002) {
					lifestyleQuest26 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 116003) {
					lifestyleQuest27 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 116004) {
					lifestyleQuest28 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 117000) {
					lifestyleQuest29 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 117001) {
					lifestyleQuest30 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 117002) {
					lifestyleQuest31 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 117003) {
					lifestyleQuest32 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 118000) {
					lifestyleQuest33 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 118001) {
					lifestyleQuest34 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 118002) {
					lifestyleQuest35 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 118003) {
					lifestyleQuest36 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 119000) {
					lifestyleQuest37 = insUserselections
							.getDetailedInfo();
				} else if (insUserselections.getQuestionId() == 119001) {
					lifestyleQuest38 = insUserselections
							.getDetailedInfo();
				}
			}
			else
				if(insUserselections != null
						&& insUserselections.getQuestionnaireType() != null
						&& insUserselections.getQuestionnaireType()
								.name()
								.equalsIgnoreCase("Additional_Health"))
				{
					if (insUserselections.getQuestionId() == 700000) {
						taxConsent= insUserselections
								.getDetailedInfo();
						taxId=insUserselections.getSelectedOptionExt();
						
					}
				}
			else if (insUserselections != null
					&& insUserselections.getQuestionnaireType() != null
					&& insUserselections
							.getQuestionnaireType()
							.name()
							.equalsIgnoreCase(
									"Additional_Basic")) {
				if (insUserselections.getQuestionId() == 141000) {
					insadditionalInfo = insUserselections
							.getDetailedInfo();
				}
			}

		}
	}

	// ****************************************************************************//

	public void populateCP() {
		for (ContactPreference contactPreference : contactPreferences) {
			ContactPoint contactPoint = contactPreference
					.getPreferredContactPoint();
			if (contactPoint instanceof PostalAddressContact) {
				if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Current)) {
					postalAddressContact = (PostalAddressContact) contactPoint;
				} else if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Permanent)) {
					permanentAddress = (PostalAddressContact) contactPoint;
				}

			} else if (contactPoint instanceof TelephoneCallContact) {
				telephoneCallContact = (TelephoneCallContact) contactPoint;
				if (telephoneCallContact.getTypeCode().equals(
						TelephoneTypeCodeList.Mobile)) {
					insuredMobileNumber1 = telephoneCallContact.getFullNumber();
				} else if (telephoneCallContact.getTypeCode().equals(
						TelephoneTypeCodeList.Home)) {
					insuredHomeNumber1 = telephoneCallContact.getFullNumber();
				} else if (telephoneCallContact.getTypeCode().equals(
						TelephoneTypeCodeList.Business)) {
					insuredOfficeNumber = telephoneCallContact.getFullNumber();
					if(!StringUtils.isEmpty(telephoneCallContact.getExtension()))
					{
						insuredOfficeNumber = insuredOfficeNumber + GeneraliConstants.EXTENSIONTHAI + telephoneCallContact.getExtension();
					}
				}
			} else if (contactPoint instanceof ElectronicContact) {
				electronicContact = (ElectronicContact) contactPoint;
			}
		}
	}

	/** Payer Contact Details */
	public void populatePayerCP() {
		try {
			for (ContactPreference contactPreference : payerContactPreferences) {
				ContactPoint contactPoint = contactPreference
						.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (AddressNatureCodeList.Current.equals(((PostalAddressContact) contactPoint)
									.getAddressNatureCode())) {
						payerPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (AddressNatureCodeList.Permanent.equals(((PostalAddressContact) contactPoint)
									.getAddressNatureCode())) {
						payerPermanentAddress = (PostalAddressContact) contactPoint;
					} else if (AddressNatureCodeList.Office.equals(((PostalAddressContact) contactPoint)
									.getAddressNatureCode())) {
						payerOfficeAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
					telephoneCallContact = (TelephoneCallContact) contactPoint;
					if (TelephoneTypeCodeList.Mobile.equals(telephoneCallContact.getTypeCode())) {
						payerMobileNumber1 = telephoneCallContact
								.getFullNumber();
					} else if (TelephoneTypeCodeList.Home.equals(telephoneCallContact.getTypeCode())) {
						payerHomeNumber1 = telephoneCallContact.getFullNumber();
					} else if (TelephoneTypeCodeList.Business.equals(telephoneCallContact.getTypeCode())) {
						payerOfficeNumber = telephoneCallContact
								.getFullNumber();
						if(!StringUtils.isEmpty(telephoneCallContact.getExtension()))
						{
							payerOfficeNumber = payerOfficeNumber + GeneraliConstants.EXTENSIONTHAI + telephoneCallContact.getExtension();
						}
					}
				} else if (contactPoint instanceof ElectronicContact) {
					payerElectronicContact = (ElectronicContact) contactPoint;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :proposerContactPreferences"
					+ e.getStackTrace());
		}
	}

	/** proposer contact details */
	public void populateProposerCP() {
		try {
			for (ContactPreference contactPreference : proposerContactPreferences) {
				ContactPoint contactPoint = contactPreference
						.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (((PostalAddressContact) contactPoint)
							.getAddressNatureCode().equals(
									AddressNatureCodeList.Current)) {
						proposerPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (((PostalAddressContact) contactPoint)
							.getAddressNatureCode().equals(
									AddressNatureCodeList.Permanent)) {
						proposerPermanentAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
					telephoneCallContact = (TelephoneCallContact) contactPoint;
					if (telephoneCallContact.getTypeCode().equals(
							TelephoneTypeCodeList.Mobile)) {
						proposerMobileNumber1 = telephoneCallContact
								.getFullNumber();
					} else if (telephoneCallContact.getTypeCode().equals(
							TelephoneTypeCodeList.Home)) {
						proposerHomeNumber1 = telephoneCallContact
								.getFullNumber();
					} else if (telephoneCallContact.getTypeCode().equals(
							TelephoneTypeCodeList.Business)) {
						proposerOfficeNumber = telephoneCallContact
								.getFullNumber();
					}
				} else if (contactPoint instanceof ElectronicContact) {
					proposerElectronicContact = (ElectronicContact) contactPoint;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :proposerContactPreferences"
					+ e.getStackTrace());
		}
	}

	/** agent contact details */
	public void populateAgentCP() {
		try {
			for (ContactPreference contactPreference : agentContactPreferences) {
				ContactPoint contactPoint = contactPreference
						.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (((PostalAddressContact) contactPoint)
							.getAddressNatureCode() != null
							&& ((PostalAddressContact) contactPoint)
									.getAddressNatureCode().equals(
											AddressNatureCodeList.Current)) {
						proposerPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (((PostalAddressContact) contactPoint)
							.getAddressNatureCode() != null
							&& ((PostalAddressContact) contactPoint)
									.getAddressNatureCode().equals(
											AddressNatureCodeList.Permanent)) {
						proposerPermanentAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
					telephoneCallContact = (TelephoneCallContact) contactPoint;
					if (telephoneCallContact.getTypeCode() != null
							&& telephoneCallContact.getTypeCode().equals(
									TelephoneTypeCodeList.Mobile)) {
						agentMobileNumber1 = telephoneCallContact
								.getFullNumber();
					} else if (telephoneCallContact.getTypeCode() != null
							&& telephoneCallContact.getTypeCode().equals(
									TelephoneTypeCodeList.Home)) {
						agentHomeNumber1 = telephoneCallContact.getFullNumber();
					}
				} else if (contactPoint instanceof ElectronicContact) {
					agentElectronicContact = (ElectronicContact) contactPoint;
					if (agentElectronicContact.getEmailAddress() != null) {
						agentEmailId = agentElectronicContact.getEmailAddress();
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :agentContactPreferences"
					+ e.getStackTrace());
		}
	}
	
	public List<String> buildAdditionalQuestion() throws JRScriptletException	{
		List<String> additionalQuestion = new ArrayList<String>();
		try{
			if(GeneraliConstants.YES.equalsIgnoreCase(lifestyleQuest8) && GeneraliConstants.YES.equalsIgnoreCase(payLifeQuest8))
			{
				additionalQuestion.add("ข้อ 10 [ผชบ]    บริษัท  "+ payLifeQuest9 +"  สาเหตุ  "+payLifeQuest10+"  เมื่อใด "+payLifeQuest11);
			}
			if(GeneraliConstants.YES.equalsIgnoreCase(lifestyleQuest12) && GeneraliConstants.YES.equalsIgnoreCase(payLifeQuest12))
			{
				additionalQuestion.add("ข้อ 11 [ผชบ] "+payLifeQuest13);
			}
			if(GeneraliConstants.YES.equalsIgnoreCase(lifestyleQuest29) && GeneraliConstants.YES.equalsIgnoreCase(payLifeQuest29))
			{
				if(StringUtils.isBlank(payLifeQuest30))
				{additionalQuestion.add("ข้อ 15 [ผชบ] ลดลง "+payLifeQuest31 +" ก.ก.   สาเหตุที่น้ำหนักเปลี่ยนแปลง "+payLifeQuest32);
				}
				else
					if(StringUtils.isBlank(payLifeQuest31))
					{additionalQuestion.add("ข้อ 15 [ผชบ] เพิ่มขึ้น  "+payLifeQuest30 +"  ก.ก.  สาเหตุที่น้ำหนักเปลี่ยนแปลง "+payLifeQuest32);
					}
					
			}
			if(GeneraliConstants.YES.equalsIgnoreCase(lifestyleQuest33) && GeneraliConstants.YES.equalsIgnoreCase(payLifeQuest33))
			{
				additionalQuestion.add("ข้อ 16 [ผชบ] บุคคลที่เป็น "+ payLifeQuest34+"  โรค  "+ payLifeQuest35+"  อายุที่เริ่มเป็น  "+payLifeQuest36 +"  ปี");
			}
			if(GeneraliConstants.YES.equalsIgnoreCase(lifestyleQuest37) && GeneraliConstants.YES.equalsIgnoreCase(payLifeQuest37))
			{
				additionalQuestion.add("ข้อ 17 [ผชบ]  โรคที่เป็น  "+payLifeQuest38);
			}
			if(!StringUtils.isBlank(insadditionalInfo))
			{
				additionalQuestion.add(insadditionalInfo);
			}
			if(!StringUtils.isBlank(payadditionalInfo))
			{
				additionalQuestion.add(payadditionalInfo);
			}
			
			otherAddInfoLine1 = additionalQuestion.size() >= 1 ? additionalQuestion.get(0) : StringUtils.EMPTY;
			otherAddInfoLine2 = additionalQuestion.size() >= 2 ? additionalQuestion.get(1) : StringUtils.EMPTY;
			otherAddInfoLine3 = additionalQuestion.size() >= 3 ? additionalQuestion.get(2) : StringUtils.EMPTY;
			otherAddInfoLine4 = additionalQuestion.size() >= 4 ? additionalQuestion.get(3) : StringUtils.EMPTY;
			otherAddInfoLine5 = additionalQuestion.size() >= 5 ? additionalQuestion.get(4) : StringUtils.EMPTY;
			otherAddInfoLine6 = additionalQuestion.size() >= 6 ? additionalQuestion.get(5) : StringUtils.EMPTY;
			otherAddInfoLine7 = additionalQuestion.size() >= 7 ? additionalQuestion.get(6) : StringUtils.EMPTY;
		}
		catch(Exception e) {
			LOGGER.error("Error in getting AdditionalQuestiontable "+ e);
		}
		return additionalQuestion; 
	}

	/* Main Insured Details */

	public String getInsuredFullName() throws JRScriptletException {
		String name = null;
		String lname = null;
		String fname = null;
		for (PersonName personName : personNames) {
			fname = personName.getGivenName();
			lname = personName.getSurname();
			name = createFullName(fname, lname);
			if (name != null && !"".equals(name)) {
				name = name.toUpperCase();
			}
		}
		return name;
	}

	private Date convertDateToThai(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, 543);
		Date outputDate = calendar.getTime();
		return outputDate;
	}

	public Date getInsuredDob() {
		Date insuredDobInThai = convertDateToThai(insuredDob);
		return insuredDobInThai;
	}

	public String getInsuredYearOfBirth() {
		Date insDobValue = getInsuredDob();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(insDobValue);
		String year = Integer.toString(calendar.get(Calendar.YEAR));
		return year;
	}

	public String getInsuredMonthOfBirth() {
		Date insDobValue = getInsuredDob();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(insDobValue);
		String month = Integer.toString(calendar.get(Calendar.MONTH) + 1);
		return month;
	}

	public String getInsuredDayOfBirth() {
		Date insDobValue = getInsuredDob();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(insDobValue);
		String date = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
		return date;
	}
	
	public String getInsuredDateofBirth(){
		Date insDobValue = getInsuredDob();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String date = sdf.format(insDobValue);
		return date;	
	}

	public String getInsuredGender() {
		if(person!=null && person.getGenderCode()!=null){
		return person.getGenderCode().name();
		}
		else{
			return "";
		}
	}

	public String getInsuredNationality() {
		String nationality = "";
		String newC = "";
		try {
			for (Country country : countries) {
				nationality = country.getName();
			}
			newC = OmniContextBridge.services().getValueForCode(nationality,
					"th", "NATIONALITY");
		} catch (Exception e) {
			LOGGER.error("-- getInsuredNationality -- " + e.getMessage());
		}
		return newC;
	}

	public String getInsuredMaritalStatus() {
		if(person!=null && person.getMaritalStatusCode()!=null){
			return person.getMaritalStatusCode().name();
		}
		else{
			return "";
		}
	}

	public BigDecimal getInsuredHeight() {
		return insuredHeight;
	}

	public BigDecimal getInsuredWeight() {
		return insuredWeight;
	}

	public String getInsuredEmail() {
		return electronicContact.getEmailAddress();
	}

	public String getInsuredMobileNumber() {
		return insuredMobileNumber1;
	}

	public String getInsuredOfficeNumber() {
		return insuredOfficeNumber;
	}

	public String getInsuredHomeNumber() {
		return insuredHomeNumber1;
	}

	public String getIdentityNo() {
		return identityNo;
	}

	public String getInsuredIdentityDate() {
		String DateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if (insuredIdentityDate != null) {
				DateString = sdf.format(insuredIdentityDate);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting insuredIdentityDate ", e);
		}
		return DateString;
	}

	public String getInsuredIdentityPlace() {
		return insuredIdentityPlace;
	}

	public String getInsuredPerStreetName() {
		if(permanentAddress!=null){
			return permanentAddress.getStreetName();
		}
		else{
			return "";
		}
	}

	public String getInsuredPerHouseNo() {
		if(permanentAddress!=null){
			return permanentAddress.getBoxNumber();
		}
		else{
			return "";
		}
	}

	public String getInsuredPermWard() {
		String place = "";
		String insuredPermWard = "";
		try {
			if(permanentAddress!=null){
			place = permanentAddress.getAddressLine2();
			insuredPermWard = OmniContextBridge.services().getValueForCode(
					place, "en", "WARD");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredPermWard " + e.getMessage());
		}
		return insuredPermWard;
	}

	public String getInsuredPermDistrict() {
		String place = "";
		String insuredPermDistrict = "";
		try {
			if(permanentAddress!=null){
			place = permanentAddress.getAddressLine1();
			insuredPermDistrict = OmniContextBridge.services().getValueForCode(
					place, "en", "DISTRICT");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredPermDistrict " + e.getMessage());
		}
		return insuredPermDistrict;
	}

	public String getInsuredPermCity() {
		String place = "";
		String insuredPermCity = "";
		Set<CountryElement> countryElements = permanentAddress
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				place = ((City) countryElement).getName();
			}
		}
		try {
			if (place != null) {
				insuredPermCity = OmniContextBridge.services().getValueForCode(
						place, "en", "STATE");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredPermCity " + e.getMessage());
		}

		return insuredPermCity;
	}

	public String getInsuredCurrentStreetName() {
		if(postalAddressContact!=null){
			return postalAddressContact.getStreetName();
		}
		else{
			return "";
		}
	}

	public String getInsuredCurrentHouseNo() {
		if(postalAddressContact!=null){
			return postalAddressContact.getBoxNumber();
		}
		else{
			return "";
		}
	}

	public String getInsuredCurrentWard() {
		String place = "";
		String insuredCurmWard = "";
		try {
			if(postalAddressContact!=null){
			place = postalAddressContact.getAddressLine2();
			insuredCurmWard = OmniContextBridge.services().getValueForCode(
					place, "en", "WARD");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredCurrentWard " + e.getMessage());
		}
		return insuredCurmWard;
	}

	public String getInsuredCurrentDistrict() {
		String place = "";
		String insuredCurDistrict = "";
		try {
			if(postalAddressContact!=null){
			place = postalAddressContact.getAddressLine1();
			insuredCurDistrict = OmniContextBridge.services().getValueForCode(
					place, "en", "DISTRICT");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredPermDistrict " + e.getMessage());
		}
		return insuredCurDistrict;
	}

	public String getInsuredCurrentCity() {
		String place = "";
		String insuredCurCity = "";
		Set<CountryElement> countryElements = postalAddressContact
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				place = ((City) countryElement).getName();
			}
		}
		try {
			if (place != null) {
				insuredCurCity = OmniContextBridge.services().getValueForCode(
						place, "en", "STATE");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredPermCity " + e.getMessage());
		}
		return insuredCurCity;
	}

	/*
	 * public String getInsuredNameOfComapny() { return
	 * occupationDetail.getNameofInstitution(); }
	 */

	public String getInsuredAddressOfComapny() {
		if(postalAddressContact!=null){
			return postalAddressContact.getAddressLine3();
		}
		else{
			return "";
		}
	}

	/*
	 * public String getInsuredOccupation() { String occupation = ""; String
	 * insuredOccupation = ""; try { occupation =
	 * occupationDetail.getOccupationClass(); insuredOccupation =
	 * OmniContextBridge.services().getValueForCode( occupation, "vt",
	 * "OCCUPATION_LIST"); } catch (Exception e) {
	 * LOGGER.error("Error .. getInsuredOccupation " + e.getMessage()); } return
	 * insuredOccupation; }
	 * 
	 * public String getInsuredNatureOfWork() { return
	 * occupationDetail.getNatureofWork(); }
	 */

	public String getInsuredAnnualIncome() {
		String insuredAnnualIncome = "";
		if (incomeDetail != null && incomeDetail.getGrossIncome() != null)
			try {
				insuredAnnualIncome = formatter(incomeDetail.getGrossIncome(),
						"vt");
			} catch (Exception e) {
				LOGGER.error("getInsuredAnnualIncome -- " + e.getMessage());
			}
		return insuredAnnualIncome;
	}

	public String getInsuredBirthPlace() {
		String resCountry = "";
		String insuredBirthPlace = "";
		try {
			if (insuredResidenceCountry != null) {
				resCountry = insuredResidenceCountry.getName();
			}
			insuredBirthPlace = OmniContextBridge.services().getValueForCode(
					resCountry, "en", "NATIONALITY");
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredBirthPlace " + e.getMessage());
		}
		return insuredBirthPlace;
	}

	public String getInsuredRelationshipWithProposer() {
		String relationShip = "";
		if (relationshipWithProposer != null
				&& !StringUtils.isEmpty(relationshipWithProposer)) {
			if (jsonObjectPdfInput.containsKey(relationshipWithProposer)) {
				relationShip = jsonObjectPdfInput.get(relationshipWithProposer)
						.toString();
			}
		}
		return relationShip;
	}

	/*-------  Proposer Details -----------*/

	public String getProposerFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName proposerPersonName : proposerPersonNames) {
			name = proposerPersonName.getGivenName();
		}
		return name;
	}

	public String getProposerLastName() throws JRScriptletException {
		String name = null;
		for (PersonName proposerPersonName : proposerPersonNames) {
			name = proposerPersonName.getSurname();
		}
		return name;
	}

	public String getProposerFirstAndLastName() {
		try {
			proposerFullName = getProposerFirstName() + " "
					+ getProposerLastName();
			if (proposerFullName != null
					&& !"".equalsIgnoreCase(proposerFullName)) {
				proposerFullName.toUpperCase();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving ProposerFirstAndLastName "
					+ e.getMessage());
		}
		return proposerFullName;

	}

	public String getProposerGender() {
		String gender = null;
		try {
			if (proposerPerson != null && proposerPerson.getGenderCode() != null) {
				gender = proposerPerson.getGenderCode().name();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving getProposerGender "
					+ e.getMessage());
		}

		return gender;
	}

	public Date getProposerdob() {
		if (proposerdob != null && !"".equals(proposerdob)) {
			return proposerdob;
		}
		return proposerdob;
	}

	public String getProposerNationality() {
		String nationality = null;
		String proposerNationality = "";
		for (Country country : proposerCountries) {
			nationality = country.getName();
		}
		try {
			proposerNationality = OmniContextBridge.services().getValueForCode(
					nationality, "en", "NATIONALITY");
		} catch (Exception e) {
			LOGGER.error("-- getInsuredNationality -- " + e.getMessage());
		}
		return proposerNationality;
	}

	public String getProposerEmail() {
		populateProposerCP();
		return proposerElectronicContact.getEmailAddress();
	}

	public String getProposerMobileNumber() {
		populateProposerCP();
		return proposerMobileNumber1;
	}

	public String getProposerHomeNumber() {
		populateProposerCP();
		return proposerHomeNumber1;
	}

	public String getProposerOfficeNumber() {
		populateProposerCP();
		return proposerOfficeNumber;
	}

	public String getAgentMobileNumber() {
		populateAgentCP();
		return agentMobileNumber1;
	}

	public String getAgentEamilID() {
		populateAgentCP();
		return agentEmailId;
	}

	public String getProposerIdentityProof() {
		return proposerIdentityProof;
	}

	public String getProposerIdentityDate() {
		String DateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if (proposerIdentityDate != null) {
				DateString = sdf.format(proposerIdentityDate);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting proposerIdentityDate ", e);
		}
		return DateString;
	}

	public String getProposerIdentityPlace() {
		return proposerIdentityPlace;
	}

	public String getProposerMaritalStatus() {
		if (proposerPerson != null && proposerPerson.getMaritalStatusCode() != null) {
			return proposerPerson.getMaritalStatusCode().name();
		}
		return StringUtils.EMPTY;
	}

	public String getProposerBirthPlace() {
		String resCountry = "";
		String proposerBirthPlace = "";

		try {
			if (proposerResidenceCountry != null) {
				resCountry = proposerResidenceCountry.getName();
			}
			proposerBirthPlace = OmniContextBridge.services().getValueForCode(
					resCountry, "en", "NATIONALITY");
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredBirthPlace " + e.getMessage());
		}
		return proposerBirthPlace;
	}

	public String getProposerPerStreetName() {
		if(proposerPermanentAddress!=null){
			return proposerPermanentAddress.getStreetName();
		}
		else{
			return "";
		}
	}

	public String getProposerPermWard() {
		String place = "";
		String proposerPermWard = "";
		try {
			if(proposerPermanentAddress!=null){
				place = proposerPermanentAddress.getAddressLine2();
				proposerPermWard = OmniContextBridge.services().getValueForCode(
						place, "en", "WARD");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredPermWard " + e.getMessage());
		}
		return proposerPermWard;
	}

	public String getProposerPerHouseNo() {
		if(proposerPermanentAddress!=null){
			return proposerPermanentAddress.getBoxNumber();
		}
		else{
			return "";
		}
	}

	public String getProposerPermDistrict() {
		String place = "";
		String proposerPermDistrict = "";
		try {
			if(proposerPermanentAddress!=null){
				place = proposerPermanentAddress.getAddressLine1();
				proposerPermDistrict = OmniContextBridge.services()
						.getValueForCode(place, "en", "DISTRICT");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getProposerPermDistrict " + e.getMessage());
		}
		return proposerPermDistrict;
	}

	public String getProposerCurrentDistrict() {
		String place = "";
		String proposerCurrentDistrict = "";
		try {
			if(proposerPostalAddressContact!=null){
				place = proposerPostalAddressContact.getAddressLine1();
				proposerCurrentDistrict = OmniContextBridge.services()
						.getValueForCode(place, "en", "DISTRICT");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getProposerPermDistrict " + e.getMessage());
		}
		return proposerCurrentDistrict;
	}

	public String getProposerCurrentStreetName() {
		if(proposerPostalAddressContact!=null){
			return proposerPostalAddressContact.getStreetName();
		}
		else{
			return "";
		}
	}

	public String getProposerCurrentWard() {
		String place = "";
		String proposerCurrentWard = "";
		try {
			if(proposerPostalAddressContact!=null){
				place = proposerPostalAddressContact.getAddressLine2();
				proposerCurrentWard = OmniContextBridge.services().getValueForCode(
					place, "en", "WARD");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredPermWard " + e.getMessage());
		}
		return proposerCurrentWard;
	}

	public String getProposerCurrentHouseNo() {
		if(proposerPostalAddressContact!=null){
			return proposerPostalAddressContact.getBoxNumber();
		}
		else{
			return "";
		}
	}

	public String getProposerPermCity() {
		populateProposerCP();
		String place = "";
		String proposerPermCity = "";
		Set<CountryElement> countryElements = proposerPermanentAddress
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				place = ((City) countryElement).getName();
			}
		}

		try {
			if (place != null) {
				proposerPermCity = OmniContextBridge.services()
						.getValueForCode(place, "en", "STATE");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getProposerPermCity " + e.getMessage());
		}

		return proposerPermCity;
	}

	public String getProposerCurrentCity() {
		String place = "";
		String proposerCurrentCity = "";

		Set<CountryElement> countryElements = proposerPostalAddressContact
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				place = ((City) countryElement).getName();
			}
		}

		try {
			if (place != null) {
				proposerCurrentCity = OmniContextBridge.services()
						.getValueForCode(place, "en", "STATE");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getProposerPermCity " + e.getMessage());
		}
		return proposerCurrentCity;
	}

	public String getProposerNameOfComapny() {
		if(proposerOccupationDetail!=null){
			return proposerOccupationDetail.getNameofInstitution();
		}
		else{
			return "";
		}
	}

	public String getProposerNatureOfWork() {
		if(proposerOccupationDetail!=null){
			return proposerOccupationDetail.getNatureofWork();
		}
		else{
			return "";
		}
	}

	public String getProposerAddressOfComapny() {
		if(proposerPostalAddressContact!=null){
			return proposerPostalAddressContact.getAddressLine3();
		}
		else{
			return "";
		}
	}

	public String getProposerOccupation() {
		String occupation = "";
		String proposerOccupation = "";
		try {
			if(proposerOccupationDetail != null){
				occupation = proposerOccupationDetail.getOccupationClass();
				proposerOccupation = OmniContextBridge.services().getValueForCode(
					occupation, "vt", "OCCUPATION_LIST");
			}
		} catch (Exception e) {
			LOGGER.error("Error .. getInsuredOccupation " + e.getMessage());
		}
		return proposerOccupation;
	}

	public String getProposerAnnualIncome() {
		String proposerannualIncome = "";
		try {
			if (proposerIncomeDetail != null && proposerIncomeDetail.getGrossIncome() != null) {
				proposerannualIncome = formatter(
						proposerIncomeDetail.getGrossIncome(), "vt");
			}
		} catch (Exception e) {
			LOGGER.error("getProposerAnnualIncome --" + e.getMessage());
		}
		return proposerannualIncome;
	}

	/** Beneficiary */
	/** First beneficiary */
	public String getFirstBeneficiaryAge() throws JRScriptletException {
		String firstBeneficiaryAge = "";
		if(firstBeneficiaryAge != null && firstBeneficiaryPerson.getAge() !=null){
			firstBeneficiaryAge = firstBeneficiaryPerson.getAge().toString();
		}
		return firstBeneficiaryAge;
	}

	public String getFirstBeneficiaryAddress() throws JRScriptletException {

		try {
			if(firstBeneficiaryPerson!=null){
			for (ContactPreference pName : firstBeneficiaryPerson
					.getPreferredContact()) {
				if(pName!=null){
				ContactPoint contactPoint = pName.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					PostalAddressContact address = (PostalAddressContact) contactPoint;
					if(address!=null && address.getAddressNatureCode() != null){
					if (address.getAddressNatureCode().equals(
							AddressNatureCodeList.Current)) {
						return address.getAddressLine1();

					}
					}
				}
				}
			}
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving FirstBeneficiaryAddress "
					+ e.getMessage());
		}
		return StringUtils.EMPTY;
	}

	public String getFirstBeneficiaryFirstName() throws JRScriptletException {
		String firstBeneficiaryFirstName = "";
		if(firstBeneficiaryPersonNames != null){
			for (PersonName firstBeneficiaryPersonName : firstBeneficiaryPersonNames) {
				firstBeneficiaryFirstName = firstBeneficiaryPersonName
						.getGivenName();
			}
		}
		return firstBeneficiaryFirstName;
	}

	public String getFirstBeneficiaryLastName() throws JRScriptletException {
		String firstBeneficiaryLastName = "";
		if(firstBeneficiaryPersonNames != null){
			for (PersonName firstBeneficiaryPersonName : firstBeneficiaryPersonNames) {
				firstBeneficiaryLastName = firstBeneficiaryPersonName.getSurname();
			}
		}
		return firstBeneficiaryLastName;
	}

	public String getFirstBeneficiaryFirstAndLastName() {
		String firstBeneficiaryFullName = "";
		try {
			if (getFirstBeneficiaryLastName() != null && getFirstBeneficiaryFirstName() != null) {
				firstBeneficiaryFullName = getFirstBeneficiaryFirstName() + " "
						+ getFirstBeneficiaryLastName();
			}else if(getFirstBeneficiaryFirstName()!= null){
				firstBeneficiaryFullName = getFirstBeneficiaryFirstName();
			} else {
				firstBeneficiaryFullName = getFirstBeneficiaryLastName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving BeneficiaryFirstAndLastName "
					+ e.getMessage());
		}
		return firstBeneficiaryFullName;
	}

	public Date getFirstBeneficiaryDob() {
		return firstBeneficiaryDOB;
	}

	public String getFirstBeneficiaryGender() {
		String gender = "";
		try {
			gender = getBenGender(firstBeneficiaryGender);
		} catch (Exception e) {
			LOGGER.error("getFirstBeneficiaryGender :" + e.getMessage());
		}
		return gender;
	}

	public Float getFirstBeneficiarySharePercentage() {
		return firstBeneficiarySharePercentage;
	}

	public String getFirstBeneficiaryRelationWithInsured() {
		String newC = "";
		try {
			if(!StringUtils.isEmpty(firstBeneficiaryRelationWithInsured)){
				newC = OmniContextBridge.services().getValueForCode(firstBeneficiaryRelationWithInsured,
						"th", GeneraliConstants.BENEFICIARYRELATIONSIP);
			}
		} catch (Exception e) {
			LOGGER.error("-- getFirstBeneficiaryRelationWithInsured -- " + e.getMessage());
		}
		return newC;
	}

	public String getFirstBeneficiaryYearOfBirth() {
		String year = null;
		try {
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if (firstBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(firstBeneficiaryDobValue)) {
				year = firstBeneficiaryDobValue.substring(0, 4);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting first beneficiary year of birth ");
		}
		return year;
	}

	public String getFirstBeneficiaryMonthOfBirth() {
		String month = null;
		try {
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if (firstBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(firstBeneficiaryDobValue)) {
				month = firstBeneficiaryDobValue.substring(5, 7);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting first beneficiary month of birth ");
		}
		return month;
	}

	public String getFirstBeneficiaryDayOfBirth() {
		String date = null;
		try {
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if (firstBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(firstBeneficiaryDobValue)) {
				date = firstBeneficiaryDobValue.substring(8, 10);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting first beneficiary day of birth ");
		}
		return date;
	}

	/** Second beneficiary */

	public String getSecondBeneficiaryFirstName() throws JRScriptletException {
		String secondBeneficiaryFirstName = "";
		if(secondBeneficiaryPersonNames != null){
			for (PersonName secondBeneficiaryPersonName : secondBeneficiaryPersonNames) {
				secondBeneficiaryFirstName = secondBeneficiaryPersonName
						.getGivenName();
			}
		}
		return secondBeneficiaryFirstName;
	}

	public String getSecondBeneficiaryLastName() throws JRScriptletException {
		String secondBeneficiaryLastName = "";
		if(secondBeneficiaryPersonNames != null){
			for (PersonName secondBeneficiaryPersonName : secondBeneficiaryPersonNames) {
				secondBeneficiaryLastName = secondBeneficiaryPersonName
						.getSurname();
			}
		}
		return secondBeneficiaryLastName;
	}

	public String getSecondBeneficiaryFirstAndLastName() {
		String secondBeneficiaryFullName = "";
		try {
			if (getSecondBeneficiaryLastName() != null && getSecondBeneficiaryFirstName() != null ) {
				secondBeneficiaryFullName = getSecondBeneficiaryFirstName()
						+ " " + getSecondBeneficiaryLastName();
			}else if(getSecondBeneficiaryFirstName()!= null){
				secondBeneficiaryFullName = getSecondBeneficiaryFirstName();
			} else {
				secondBeneficiaryFullName = getSecondBeneficiaryLastName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving SecondBeneficiaryFirstAndLastName "
					+ e.getMessage());
		}
		return secondBeneficiaryFullName;
	}

	public Date getSecondBeneficiaryDob() {
		return secondBeneficiaryDOB;
	}

	public String getSecondBeneficiaryGender() {
		String gender = "";
		try {
			gender = getBenGender(secondBeneficiaryGender);
		} catch (Exception e) {
			LOGGER.error("getFirstBeneficiaryGender :" + e.getMessage());
		}
		return gender;
	}

	public Float getSecondBeneficiarySharePercentage() {
		return secondBeneficiarySharePercentage;
	}

	public String getSecondBeneficiaryRelationWithInsured() {
		String newC = "";
		try {
			if(!StringUtils.isEmpty(secondBeneficiaryRelationWithInsured)){
				newC = OmniContextBridge.services().getValueForCode(secondBeneficiaryRelationWithInsured,
						"th", GeneraliConstants.BENEFICIARYRELATIONSIP);
			}
		} catch (Exception e) {
			LOGGER.error("-- getSecondBeneficiarySharePercentage -- " + e.getMessage());
		}
		return newC;
	}

	public String getSecondBeneficiaryYearOfBirth() {
		String year = null;
		try {
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if (secondBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(secondBeneficiaryDobValue)) {
				year = secondBeneficiaryDobValue.substring(0, 4);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting second beneficiary year of birth ");
		}
		return year;
	}

	public String getSecondBeneficiaryMonthOfBirth() {
		String month = null;
		try {
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if (secondBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(secondBeneficiaryDobValue)) {
				month = secondBeneficiaryDobValue.substring(5, 7);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting second beneficiary month of birth ");
		}
		return month;
	}

	public String getSecondBeneficiaryDayOfBirth() {
		String date = null;
		try {
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if (secondBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(secondBeneficiaryDobValue)) {
				date = secondBeneficiaryDobValue.substring(8, 10);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting second beneficiary day of birth ");
		}
		return date;
	}

	public String getSecondBeneficiaryAge() throws JRScriptletException {
		String secondBeneficiaryAge="";
		if(secondBeneficiaryPerson != null && secondBeneficiaryPerson.getAge() != null)
		{
			 secondBeneficiaryAge = secondBeneficiaryPerson.getAge().toString();
			return secondBeneficiaryAge;
		}
		return "";
	}

	public String getSecondBeneficiaryAddress() throws JRScriptletException {

		try {
			if(secondBeneficiaryPerson!=null){
			for (ContactPreference pName : secondBeneficiaryPerson
					.getPreferredContact()) {
				if(pName!=null){
				ContactPoint contactPoint = pName.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					PostalAddressContact address = (PostalAddressContact) contactPoint;
					if(address!=null && address.getAddressNatureCode() != null){
						if (address.getAddressNatureCode().equals(
								AddressNatureCodeList.Current)) {
							return address.getAddressLine1();
						}
					}
				}
				}
			}
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving SecondBeneficiaryAddress "
					+ e.getMessage());
		}
		return StringUtils.EMPTY;
	}

	/** Third beneficiary */

	public String getThirdBeneficiaryFirstName() throws JRScriptletException {
		String thirdBeneficiaryFirstName = "";
		if(thirdBeneficiaryPersonNames != null){
			for (PersonName thirdBeneficiaryPersonName : thirdBeneficiaryPersonNames) {
				thirdBeneficiaryFirstName = thirdBeneficiaryPersonName
						.getGivenName();
			}
		}
		return thirdBeneficiaryFirstName;
	}

	public String getThirdBeneficiaryLastName() throws JRScriptletException {
		String thirdBeneficiaryLastName = "";
		if(thirdBeneficiaryPersonNames != null){
			for (PersonName thirdBeneficiaryPersonName : thirdBeneficiaryPersonNames) {
				thirdBeneficiaryLastName = thirdBeneficiaryPersonName.getSurname();
			}
		}
		return thirdBeneficiaryLastName;
	}

	public String getThirdBeneficiaryFirstAndLastName() {
		String thirdBeneficiaryFullName = "";
		try {
			if (getThirdBeneficiaryLastName() != null && getThirdBeneficiaryFirstName()!= null) {
				thirdBeneficiaryFullName = getThirdBeneficiaryFirstName() + " "
						+ getThirdBeneficiaryLastName();
			} else if(getThirdBeneficiaryFirstName()!= null){
				thirdBeneficiaryFullName = getThirdBeneficiaryFirstName();
			} else {
				thirdBeneficiaryFullName = getThirdBeneficiaryLastName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving ThirdBeneficiaryFirstAndLastName "
					+ e.getMessage());
		}
		return thirdBeneficiaryFullName;
	}

	public Date getThirdBeneficiaryDob() {
		String DateString = null;
		Date dateValue = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			if (thirdBeneficiaryDOB != null) {
				DateString = sdf.format(thirdBeneficiaryDOB);
				dateValue = formatter.parse(DateString);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting thirdBeneficiaryDOB ", e);
		}
		return dateValue;
	}

	public String getThirdBeneficiaryGender() {
		String gender = "";
		try {
			gender = getBenGender(thirdBeneficiaryGender);
		} catch (Exception e) {
			LOGGER.error("getFirstBeneficiaryGender :" + e.getMessage());
		}
		return gender;
	}

	public Float getThirdBeneficiarySharePercentage() {
		return thirdBeneficiarySharePercentage;
	}

	public String getThirdBeneficiaryRelationWithInsured() {
		String newC = "";
		try {
			if(!StringUtils.isEmpty(thirdBeneficiaryRelationWithInsured)){
				newC = OmniContextBridge.services().getValueForCode(thirdBeneficiaryRelationWithInsured,
						"th", GeneraliConstants.BENEFICIARYRELATIONSIP);
			}
		} catch (Exception e) {
			LOGGER.error("-- thirdBeneficiaryRelationWithInsured -- " + e.getMessage());
		}
		return newC;
	}

	public String getThirdBeneficiaryAge() throws JRScriptletException {
		String thirdBeneficiaryAge="";
		if(thirdBeneficiaryPerson != null && thirdBeneficiaryPerson.getAge() != null){
		 thirdBeneficiaryAge = thirdBeneficiaryPerson.getAge().toString();
		return thirdBeneficiaryAge;
		}
		return thirdBeneficiaryAge;
	}

	public String getThirdBeneficiaryAddress() throws JRScriptletException {

		try {
			if(thirdBeneficiaryPerson!=null){
			for (ContactPreference pName : thirdBeneficiaryPerson
					.getPreferredContact()) {
				if(pName!=null){
				ContactPoint contactPoint = pName.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					PostalAddressContact address = (PostalAddressContact) contactPoint;
					if(address!=null && address.getAddressNatureCode() != null){
					if (address.getAddressNatureCode().equals(
							AddressNatureCodeList.Current)) {
						return address.getAddressLine1();
					}
					}
				}
				}
			}
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving ThirdBeneficiaryAddress "
					+ e.getMessage());
		}
		return StringUtils.EMPTY;
	}

	public String getThirdBeneficiaryYearOfBirth() {
		String year = null;
		try {
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if (thirdBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(thirdBeneficiaryDobValue)) {
				year = thirdBeneficiaryDobValue.substring(0, 4);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting third beneficiary year of birth ");
		}
		return year;
	}

	public String getThirdBeneficiaryMonthOfBirth() {
		String month = null;
		try {
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if (thirdBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(thirdBeneficiaryDobValue)) {
				month = thirdBeneficiaryDobValue.substring(5, 7);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting third beneficiary month of birth ");
		}
		return month;
	}

	public String getThirdBeneficiaryDayOfBirth() {
		String date = null;
		try {
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if (thirdBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(thirdBeneficiaryDobValue)) {
				date = thirdBeneficiaryDobValue.substring(8, 10);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting third beneficiary day of birth ");
		}
		return date;
	}

	/** Fourth beneficiary */

	public String getFourthBeneficiaryFirstName() throws JRScriptletException {
		String fourthBeneficiaryFirstName = "";
		if(fourthBeneficiaryPersonNames!=null){
			for (PersonName fourthBeneficiaryPersonName : fourthBeneficiaryPersonNames) {
				fourthBeneficiaryFirstName = fourthBeneficiaryPersonName
						.getGivenName();
			}
		}
		return fourthBeneficiaryFirstName;
	}

	public String getFourthBeneficiaryLastName() throws JRScriptletException {
		String fourthBeneficiaryLastName = "";
		if(fourthBeneficiaryPersonNames!=null){
			for (PersonName fourthBeneficiaryPersonName : fourthBeneficiaryPersonNames) {
				fourthBeneficiaryLastName = fourthBeneficiaryPersonName
						.getSurname();
			}
		}
		return fourthBeneficiaryLastName;
	}

	public String getFourthBeneficiaryFirstAndLastName() {
		String fourthBeneficiaryFullName = "";
		try {
			if (getFourthBeneficiaryLastName() != null && getFourthBeneficiaryFirstName()!=null) {
				fourthBeneficiaryFullName = getFourthBeneficiaryFirstName()
						+ " " + getFourthBeneficiaryLastName();
			} else if(getFourthBeneficiaryFirstName()!= null){
				fourthBeneficiaryFullName = getFourthBeneficiaryFirstName();
			} else {
				fourthBeneficiaryFullName = getFourthBeneficiaryLastName();
			}
			
		} catch (Exception e) {
			LOGGER.error("Error while retrieving FourthBeneficiaryFirstAndLastName "
					+ e.getMessage());
		}
		return fourthBeneficiaryFullName;
	}

	public Date getFourthBeneficiaryDob() {
		String DateString = null;
		Date dateValue = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			if (fourthBeneficiaryDOB != null) {
				DateString = sdf.format(fourthBeneficiaryDOB);
				dateValue = formatter.parse(DateString);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting fourthBeneficiaryDOB ", e);
		}
		return dateValue;
	}

	public String getFourthBeneficiaryGender() {
		String gender = "";
		try {
			gender = getBenGender(fourthBeneficiaryGender);
		} catch (Exception e) {
			LOGGER.error("getFourthBeneficiaryGender :" + e.getMessage());
		}
		return gender;
	}

	public Float getFourthBeneficiarySharePercentage() {
		return fourthBeneficiarySharePercentage;
	}

	public String getFourthBeneficiaryRelationWithInsured() {
		String newC = "";
		try {
			if(!StringUtils.isEmpty(fourthBeneficiaryRelationWithInsured)){
				newC = OmniContextBridge.services().getValueForCode(fourthBeneficiaryRelationWithInsured,
						"th", GeneraliConstants.BENEFICIARYRELATIONSIP);
			}
		} catch (Exception e) {
			LOGGER.error("-- getFourthBeneficiaryRelationWithInsured -- " + e.getMessage());
		}
		return newC;
	}

	public String getFourthBeneficiaryAge() throws JRScriptletException {
		String fourthBeneficiaryAge = "";
		if(fourthBeneficiaryPerson != null && fourthBeneficiaryPerson.getAge() != null){
			fourthBeneficiaryAge = fourthBeneficiaryPerson.getAge().toString();
			return fourthBeneficiaryAge;
		}
		return "";
	}

	public String getFourthBeneficiaryAddress() throws JRScriptletException {

		try {
			if(fourthBeneficiaryPerson!=null){
			for (ContactPreference pName : fourthBeneficiaryPerson
					.getPreferredContact()) {
				if(pName!=null){
					ContactPoint contactPoint = pName.getPreferredContactPoint();
					if (contactPoint instanceof PostalAddressContact) {
						PostalAddressContact address = (PostalAddressContact) contactPoint;
						if(address!=null && address.getAddressNatureCode() != null){
							if (address.getAddressNatureCode().equals(
									AddressNatureCodeList.Current)) {
								return address.getAddressLine1();
							}
						}
					}
				}
			}
		}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving FourthBeneficiaryAddress "
					+ e.getMessage());
		}
		return StringUtils.EMPTY;
	}

	public String getFourthBeneficiaryYearOfBirth() {
		String year = null;
		try {
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if (fourthBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(fourthBeneficiaryDobValue)) {
				year = fourthBeneficiaryDobValue.substring(0, 4);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting fourth beneficiary year of birth ");
		}
		return year;
	}

	public String getFourthBeneficiaryMonthOfBirth() {
		String month = null;
		try {
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if (fourthBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(fourthBeneficiaryDobValue)) {
				month = fourthBeneficiaryDobValue.substring(5, 7);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting fourth beneficiary month of birth ");
		}
		return month;
	}

	public String getFourthBeneficiaryDayOfBirth() {
		String date = null;
		try {
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if (fourthBeneficiaryDobValue != null
					&& !"".equalsIgnoreCase(fourthBeneficiaryDobValue)) {
				date = fourthBeneficiaryDobValue.substring(8, 10);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting fourth beneficiary day of birth ");
		}
		return date;
	}

	/** Financial Data */

	public String getProposerSourceOfIncome() {
		String proposerSourceOfIncome = null;
		if(proposerIncomeDetail != null && proposerIncomeDetail.getTypeCode()!=null){
		proposerSourceOfIncome = proposerIncomeDetail.getTypeCode().name();
		}
		return proposerSourceOfIncome;
	}

	public String getProposerSourceOfIncomeDescription() {
		String proposerSourceOfIncomeDesc = null;
		if(proposerIncomeDetail != null && proposerIncomeDetail.getTypeCode()!=null){
		proposerSourceOfIncomeDesc = proposerIncomeDetail.getDescription();
		}
		return proposerSourceOfIncomeDesc;
	}

	public String getProposerGrossIncome() {
		String proposerGrossIncome = null;
		if(proposerIncomeDetail != null && proposerIncomeDetail.getTypeCode()!=null){
		proposerGrossIncome = proposerIncomeDetail.getGrossIncome();
		}
		return proposerGrossIncome;
	}

	public String getProposerGrossIncomeDescription() {
		String proposerGrossIncomeDesc = null;
		if(proposerIncomeDetail != null && proposerIncomeDetail.getTypeCode()!=null){
		proposerGrossIncomeDesc = proposerIncomeDetail.getGrossIncomeDesc();
		}
		return proposerGrossIncomeDesc;
	}

	public String getProposerPaymentMethod() {
		return proposerPaymentMethod;
	}

	public String getSpajNumber() {
		return spajNumber;
	}

	public String getIllustrationNumber() {
		return illustrationNumber;
	}

	public String getProposerYearOfBirth() {
		String year = null;
		try {
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if (propDobValue != null && !"".equalsIgnoreCase(propDobValue)) {
				year = propDobValue.substring(0, 4);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting proposer year of birth ");
		}
		return year;
	}

	public String getProposerMonthOfBirth() {
		String month = null;
		try {
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if (propDobValue != null && !"".equalsIgnoreCase(propDobValue)) {
				month = propDobValue.substring(5, 7);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting proposer month of birth ");
		}
		return month;
	}

	public String getProposerDayOfBirth() {
		String date = null;
		try {
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if (propDobValue != null && !"".equalsIgnoreCase(propDobValue)) {
				date = propDobValue.substring(8, 10);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting proposer month of birth ");
		}
		return date;
	}

	public String getInsuredRelationWithProposer() {
		String relationshipWithProp = "";
		if (GeneraliConstants.NO.equalsIgnoreCase(isInsuredSameAsPayer)) {
			return relationshipWithInsured.get(relationshipWithProposer);
		}
		return relationshipWithProp;
	}

	public String splitDate(Date date, int firstIndex, int lastIndex)
			throws Exception {
		String dobValue = date.toString();
		String dateSplit = "";

		if (dobValue != null && !"".equalsIgnoreCase(dobValue)) {
			dateSplit = dobValue.substring(firstIndex, lastIndex);
		}

		return dateSplit;
	}

	public String getDecalrationQuestionAnswer(String Number) {
		String answer = "";
		try {

			for (UserSelection question : declationQuestions) {
				if (Number.equals(question.getQuestionId().toString())
						&& ("Basic").equals(question.getQuestionnaireType()
								.name())) {
					if (true == question.getSelectedOption()) {
						answer = "true";

					} else if (false == question.getSelectedOption()) {
						answer = "false";
					}
					break;
				}
			}

		} catch (Exception e) {
			LOGGER.error("Error while getting decalartion answer option of question "
					+ Number);
		}
		return answer;

	}

	public String getProposerFacta1() {
		return proposerFatcaQues1;
	}

	public String getProposerFacta2() {
		return proposerFatcaQues2;
	}

	public String getProposerFacta11() {
		return proposerFatcaQues11;
	}

	public String getProposerFacta12() {
		return proposerFatcaQues12;
	}

	public String getProposerFacta13() {
		return proposerFatcaQues13;
	}

	public String getProposerFacta3() {
		return proposerFatcaQues3;
	}

	public String getProposerFacta4() {
		return proposerFatcaQues4;
	}

	public Boolean getBeneficiaryFacta1() {
		return beneficiaryFatcaQues1;
	}

	public Boolean getBeneficiaryFacta2() {
		return beneficiaryFatcaQues2;
	}

	public String getAgentCode() {
		return agentCode;
	}

	/** Previous Policy */

	public String getFirstCompanyName() {
		return firstCompanyName;
	}

	public String getFirstPrevInsuredName() {
		return firstPrevInsuredName;
	}

	public String getFirstFinalDecision() {
		return firstFinalDecision;
	}

	public String getFirstTypeOfInsurance() {
		return firstTypeOfInsurance;
	}

	public String getFirstPolicyStatus() {
		return firstPolicyStatus;
	}

	public String getFirstPrevSumAssured() {
		String sumAssured = "";
		try {
			if (firstPrevSumAssured != null) {
				sumAssured = bigDecimalformatter(firstPrevSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("getThirdPrevSumAssured -- ");
		}
		return sumAssured;
	}

	public String getSecondCompanyName() {
		return secondCompanyName;
	}

	public String getSecondPrevInsuredName() {
		return secondPrevInsuredName;
	}

	public String getSecondFinalDecision() {
		return secondFinalDecision;
	}

	public String getSecondTypeOfInsurance() {
		return secondTypeOfInsurance;
	}

	public String getSecondPolicyStatus() {
		return secondPolicyStatus;
	}

	public String getSecondPrevSumAssured() {
		String sumAssured = "";
		try {
			if (secondPrevSumAssured != null) {
				sumAssured = bigDecimalformatter(secondPrevSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("getThirdPrevSumAssured -- ");
		}
		return sumAssured;
	}

	public String getThirdCompanyName() {
		return thirdCompanyName;
	}

	public String getThirdPrevInsuredName() {
		return thirdPrevInsuredName;
	}

	public String getThirdFinalDecision() {
		return thirdFinalDecision;
	}

	public String getThirdTypeOfInsurance() {
		return thirdTypeOfInsurance;
	}

	public String getThirdPolicyStatus() {
		return thirdPolicyStatus;
	}

	public String getThirdPrevSumAssured() {
		String sumAssured = "";
		try {
			if (thirdPrevSumAssured != null) {
				sumAssured = bigDecimalformatter(thirdPrevSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("getThirdPrevSumAssured -- ");
		}
		return sumAssured;
	}

	public String getFourthCompanyName() {
		return fourthCompanyName;
	}

	public String getFourthPrevInsuredName() {
		return fourthPrevInsuredName;
	}

	public String getFourthFinalDecision() {
		return fourthFinalDecision;
	}

	public String getFourthTypeOfInsurance() {
		return fourthTypeOfInsurance;
	}

	public String getFourthPolicyStatus() {
		return fourthPolicyStatus;
	}

	public BigDecimal getFourthPrevSumAssured() {
		return fourthPrevSumAssured;
	}

	/* illnesstreatment getters */
	public String getIllnessTreatmentQ1() {
		return illnessTreatmentQ1;
	}

	public String getIllnessTreatmentQ2() {
		return illnessTreatmentQ2;
	}

	public String getIllnessTreatmentQ3() {
		return illnessTreatmentQ3;
	}

	public String getIllnessTreatmentQ4() {
		return illnessTreatmentQ4;
	}

	public String getIllnessTreatmentQ5() {
		return illnessTreatmentQ5;
	}

	public String getIllnessTreatmentQ6() {
		return illnessTreatmentQ6;
	}

	public String getIllnessTreatmentQ7() {
		return illnessTreatmentQ7;
	}

	public String getIllnessTreatmentQ8() {
		return illnessTreatmentQ8;
	}

	public String getIllnessTreatmentQ9() {
		return illnessTreatmentQ9;
	}

	public String getIllnessTreatmentQ10() {
		return illnessTreatmentQ10;
	}

	public String getIllnessTreatmentQ11() {
		return illnessTreatmentQ11;
	}

	public String getIllnessTreatmentQ12() {
		return illnessTreatmentQ12;
	}

	public String getIllnessTreatmentQ13() {
		return illnessTreatmentQ13;
	}

	public String getIllnessTreatmentQ14() {
		return illnessTreatmentQ14;
	}

	public String getIllnessTreatmentQ15() {
		return illnessTreatmentQ15;
	}

	public String getIllnessTreatmentQ16() {
		return illnessTreatmentQ16;
	}

	public String getIllnessTreatmentQ17() {
		return illnessTreatmentQ17;
	}

	public String getIllnessTreatmentQ18() {
		return illnessTreatmentQ18;
	}

	public String getIllnessTreatmentQ19() {
		return illnessTreatmentQ19;
	}

	public String getIllnessTreatmentQ20() {
		return illnessTreatmentQ20;
	}

	public String getIllnessTreatmentQ21() {
		return illnessTreatmentQ21;
	}

	public String getIllnessTreatmentQ22() {
		return illnessTreatmentQ22;
	}

	public String getIllnessTreatmentQ24() {
		return illnessTreatmentQ24;
	}

	public String getIllnessTreatmentQ25() {
		return illnessTreatmentQ25;
	}

	public String getIllnessTreatmentQ26() {
		return illnessTreatmentQ26;
	}

	public String getIllnessTreatmentQ27() {
		return illnessTreatmentQ27;
	}

	public String getIllnessTreatmentQ28() {
		return illnessTreatmentQ28;
	}

	public String getIllnessTreatmentQ111() {
		return illnessTreatmentQ111;
	}

	public String getIllnessTreatmentQ112() {
		return illnessTreatmentQ112;
	}

	public String getIllnessTreatmentQ113() {
		return illnessTreatmentQ113;
	}

	public String getIllnessTreatmentQ114() {
		return illnessTreatmentQ114;
	}

	public String getIllnessTreatmentQ121() {
		return illnessTreatmentQ121;
	}

	public String getIllnessTreatmentQ122() {
		return illnessTreatmentQ122;
	}

	public String getIllnessTreatmentQ123() {
		return illnessTreatmentQ123;
	}

	public String getIllnessTreatmentQ124() {
		return illnessTreatmentQ124;
	}

	public String getIllnessTreatmentQ131() {
		return illnessTreatmentQ131;
	}

	public String getIllnessTreatmentQ132() {
		return illnessTreatmentQ132;
	}

	public String getIllnessTreatmentQ133() {
		return illnessTreatmentQ133;
	}

	public String getIllnessTreatmentQ134() {
		return illnessTreatmentQ134;
	}

	public String getIllnessTreatmentQ141() {
		return illnessTreatmentQ141;
	}

	public String getIllnessTreatmentQ142() {
		return illnessTreatmentQ142;
	}

	public String getIllnessTreatmentQ143() {
		return illnessTreatmentQ143;
	}

	public String getIllnessTreatmentQ144() {
		return illnessTreatmentQ144;
	}

	public String getIllnessTreatmentQ151() {
		return illnessTreatmentQ151;
	}

	public String getIllnessTreatmentQ152() {
		return illnessTreatmentQ152;
	}

	public String getIllnessTreatmentQ153() {
		return illnessTreatmentQ153;
	}

	public String getIllnessTreatmentQ154() {
		return illnessTreatmentQ154;
	}

	public String getIllnessTreatmentQ161() {
		return illnessTreatmentQ161;
	}

	public String getIllnessTreatmentQ162() {
		return illnessTreatmentQ162;
	}

	public String getIllnessTreatmentQ163() {
		return illnessTreatmentQ163;
	}

	public String getIllnessTreatmentQ164() {
		return illnessTreatmentQ164;
	}

	public String getIllnessTreatmentQ171() {
		return illnessTreatmentQ171;
	}

	public String getIllnessTreatmentQ172() {
		return illnessTreatmentQ172;
	}

	public String getIllnessTreatmentQ173() {
		return illnessTreatmentQ173;
	}

	public String getIllnessTreatmentQ174() {
		return illnessTreatmentQ174;
	}

	public String getIllnessTreatmentQ181() {
		return illnessTreatmentQ181;
	}

	public String getIllnessTreatmentQ182() {
		return illnessTreatmentQ182;
	}

	public String getIllnessTreatmentQ183() {
		return illnessTreatmentQ183;
	}

	public String getIllnessTreatmentQ184() {
		return illnessTreatmentQ184;
	}

	public String getIllnessTreatmentQ211() {
		return illnessTreatmentQ211;
	}

	public String getIllnessTreatmentQ212() {
		return illnessTreatmentQ212;
	}

	public String getIllnessTreatmentQ213() {
		return illnessTreatmentQ213;
	}

	public String getIllnessTreatmentQ214() {
		return illnessTreatmentQ214;
	}

	public String getIllnessTreatmentQ221() {
		return illnessTreatmentQ221;
	}

	public String getIllnessTreatmentQ222() {
		return illnessTreatmentQ222;
	}

	public String getIllnessTreatmentQ223() {
		return illnessTreatmentQ223;
	}

	public String getIllnessTreatmentQ224() {
		return illnessTreatmentQ224;
	}

	public String getIllnessTreatmentQ231() {
		return illnessTreatmentQ231;
	}

	public String getIllnessTreatmentQ232() {
		return illnessTreatmentQ232;
	}

	public String getIllnessTreatmentQ233() {
		return illnessTreatmentQ233;
	}

	public String getIllnessTreatmentQ234() {
		return illnessTreatmentQ234;
	}

	public String getIllnessTreatmentQ241() {
		return illnessTreatmentQ241;
	}

	public String getIllnessTreatmentQ242() {
		return illnessTreatmentQ242;
	}

	public String getIllnessTreatmentQ243() {
		return illnessTreatmentQ243;
	}

	public String getIllnessTreatmentQ244() {
		return illnessTreatmentQ244;
	}

	public String getIllnessTreatmentQ311() {
		return illnessTreatmentQ311;
	}

	public String getIllnessTreatmentQ312() {
		return illnessTreatmentQ312;
	}

	public String getIllnessTreatmentQ313() {
		return illnessTreatmentQ313;
	}

	public String getIllnessTreatmentQ314() {
		return illnessTreatmentQ314;
	}

	public String getIllnessTreatmentQ321() {
		return illnessTreatmentQ321;
	}

	public String getIllnessTreatmentQ322() {
		return illnessTreatmentQ322;
	}

	public String getIllnessTreatmentQ323() {
		return illnessTreatmentQ323;
	}

	public String getIllnessTreatmentQ324() {
		return illnessTreatmentQ324;
	}

	public String getIllnessTreatmentQ331() {
		return illnessTreatmentQ331;
	}

	public String getIllnessTreatmentQ332() {
		return illnessTreatmentQ332;
	}

	public String getIllnessTreatmentQ333() {
		return illnessTreatmentQ333;
	}

	public String getIllnessTreatmentQ334() {
		return illnessTreatmentQ334;
	}

	public String getIllnessTreatmentQ341() {
		return illnessTreatmentQ341;
	}

	public String getIllnessTreatmentQ342() {
		return illnessTreatmentQ342;
	}

	public String getIllnessTreatmentQ343() {
		return illnessTreatmentQ343;
	}

	public String getIllnessTreatmentQ344() {
		return illnessTreatmentQ344;
	}

	public String getIllnessTreatmentQ351() {
		return illnessTreatmentQ351;
	}

	public String getIllnessTreatmentQ352() {
		return illnessTreatmentQ352;
	}

	public String getIllnessTreatmentQ353() {
		return illnessTreatmentQ353;
	}

	public String getIllnessTreatmentQ354() {
		return illnessTreatmentQ354;
	}

	public String getIllnessTreatmentQ361() {
		return illnessTreatmentQ361;
	}

	public String getIllnessTreatmentQ362() {
		return illnessTreatmentQ362;
	}

	public String getIllnessTreatmentQ363() {
		return illnessTreatmentQ363;
	}

	public String getIllnessTreatmentQ364() {
		return illnessTreatmentQ364;
	}

	public String getIllnessTreatmentQ371() {
		return illnessTreatmentQ371;
	}

	public String getIllnessTreatmentQ372() {
		return illnessTreatmentQ372;
	}

	public String getIllnessTreatmentQ373() {
		return illnessTreatmentQ373;
	}

	public String getIllnessTreatmentQ374() {
		return illnessTreatmentQ374;
	}

	public String getIllnessTreatmentQ411() {
		return illnessTreatmentQ411;
	}

	public String getIllnessTreatmentQ412() {
		return illnessTreatmentQ412;
	}

	public String getIllnessTreatmentQ413() {
		return illnessTreatmentQ413;
	}

	public String getIllnessTreatmentQ414() {
		return illnessTreatmentQ414;
	}

	public String getIllnessTreatmentQ421() {
		return illnessTreatmentQ421;
	}

	public String getIllnessTreatmentQ422() {
		return illnessTreatmentQ422;
	}

	public String getIllnessTreatmentQ423() {
		return illnessTreatmentQ423;
	}

	public String getIllnessTreatmentQ424() {
		return illnessTreatmentQ424;
	}

	public String getIllnessTreatmentQ431() {
		return illnessTreatmentQ431;
	}

	public String getIllnessTreatmentQ432() {
		return illnessTreatmentQ432;
	}

	public String getIllnessTreatmentQ433() {
		return illnessTreatmentQ433;
	}

	public String getIllnessTreatmentQ434() {
		return illnessTreatmentQ434;
	}

	public String getIllnessTreatmentQ441() {
		return illnessTreatmentQ441;
	}

	public String getIllnessTreatmentQ442() {
		return illnessTreatmentQ442;
	}

	public String getIllnessTreatmentQ443() {
		return illnessTreatmentQ443;
	}

	public String getIllnessTreatmentQ444() {
		return illnessTreatmentQ444;
	}

	public String getIllnessTreatmentQ451() {
		return illnessTreatmentQ451;
	}

	public String getIllnessTreatmentQ452() {
		return illnessTreatmentQ452;
	}

	public String getIllnessTreatmentQ453() {
		return illnessTreatmentQ453;
	}

	public String getIllnessTreatmentQ454() {
		return illnessTreatmentQ454;
	}

	public String getIllnessTreatmentQ461() {
		return illnessTreatmentQ461;
	}

	public String getIllnessTreatmentQ462() {
		return illnessTreatmentQ462;
	}

	public String getIllnessTreatmentQ463() {
		return illnessTreatmentQ463;
	}

	public String getIllnessTreatmentQ464() {
		return illnessTreatmentQ464;
	}

	public String getIllnessTreatmentQ511() {
		return illnessTreatmentQ511;
	}

	public String getIllnessTreatmentQ512() {
		return illnessTreatmentQ512;
	}

	public String getIllnessTreatmentQ513() {
		return illnessTreatmentQ513;
	}

	public String getIllnessTreatmentQ514() {
		return illnessTreatmentQ514;
	}

	public String getIllnessTreatmentQ521() {
		return illnessTreatmentQ521;
	}

	public String getIllnessTreatmentQ522() {
		return illnessTreatmentQ522;
	}

	public String getIllnessTreatmentQ523() {
		return illnessTreatmentQ523;
	}

	public String getIllnessTreatmentQ524() {
		return illnessTreatmentQ524;
	}

	public String getIllnessTreatmentQ531() {
		return illnessTreatmentQ531;
	}

	public String getIllnessTreatmentQ532() {
		return illnessTreatmentQ532;
	}

	public String getIllnessTreatmentQ533() {
		return illnessTreatmentQ533;
	}

	public String getIllnessTreatmentQ534() {
		return illnessTreatmentQ534;
	}

	public String getIllnessTreatmentQ611() {
		return illnessTreatmentQ611;
	}

	public String getIllnessTreatmentQ612() {
		return illnessTreatmentQ612;
	}

	public String getIllnessTreatmentQ613() {
		return illnessTreatmentQ613;
	}

	public String getIllnessTreatmentQ614() {
		return illnessTreatmentQ614;
	}

	public String getIllnessTreatmentQ621() {
		return illnessTreatmentQ621;
	}

	public String getIllnessTreatmentQ622() {
		return illnessTreatmentQ622;
	}

	public String getIllnessTreatmentQ623() {
		return illnessTreatmentQ623;
	}

	public String getIllnessTreatmentQ624() {
		return illnessTreatmentQ624;
	}

	public String getIllnessTreatmentQ631() {
		return illnessTreatmentQ631;
	}

	public String getIllnessTreatmentQ632() {
		return illnessTreatmentQ632;
	}

	public String getIllnessTreatmentQ633() {
		return illnessTreatmentQ633;
	}

	public String getIllnessTreatmentQ634() {
		return illnessTreatmentQ634;
	}

	public String getIllnessTreatmentQ711() {
		return illnessTreatmentQ711;
	}

	public String getIllnessTreatmentQ712() {
		return illnessTreatmentQ712;
	}

	public String getIllnessTreatmentQ713() {
		return illnessTreatmentQ713;
	}

	public String getIllnessTreatmentQ714() {
		return illnessTreatmentQ714;
	}

	public String getIllnessTreatmentQ721() {
		return illnessTreatmentQ721;
	}

	public String getIllnessTreatmentQ722() {
		return illnessTreatmentQ722;
	}

	public String getIllnessTreatmentQ723() {
		return illnessTreatmentQ723;
	}

	public String getIllnessTreatmentQ724() {
		return illnessTreatmentQ724;
	}

	public String getIllnessTreatmentQ731() {
		return illnessTreatmentQ731;
	}

	public String getIllnessTreatmentQ732() {
		return illnessTreatmentQ732;
	}

	public String getIllnessTreatmentQ733() {
		return illnessTreatmentQ733;
	}

	public String getIllnessTreatmentQ734() {
		return illnessTreatmentQ734;
	}

	public String getIllnessTreatmentQ741() {
		return illnessTreatmentQ741;
	}

	public String getIllnessTreatmentQ742() {
		return illnessTreatmentQ742;
	}

	public String getIllnessTreatmentQ743() {
		return illnessTreatmentQ743;
	}

	public String getIllnessTreatmentQ744() {
		return illnessTreatmentQ744;
	}

	public String getIllnessTreatmentQ751() {
		return illnessTreatmentQ751;
	}

	public String getIllnessTreatmentQ752() {
		return illnessTreatmentQ752;
	}

	public String getIllnessTreatmentQ753() {
		return illnessTreatmentQ753;
	}

	public String getIllnessTreatmentQ754() {
		return illnessTreatmentQ754;
	}

	public String getIllnessTreatmentQ811() {
		return illnessTreatmentQ811;
	}

	public String getIllnessTreatmentQ812() {
		return illnessTreatmentQ812;
	}

	public String getIllnessTreatmentQ813() {
		return illnessTreatmentQ813;
	}

	public String getIllnessTreatmentQ814() {
		return illnessTreatmentQ814;
	}

	public String getIllnessTreatmentQ821() {
		return illnessTreatmentQ821;
	}

	public String getIllnessTreatmentQ822() {
		return illnessTreatmentQ822;
	}

	public String getIllnessTreatmentQ823() {
		return illnessTreatmentQ823;
	}

	public String getIllnessTreatmentQ824() {
		return illnessTreatmentQ824;
	}

	public String getIllnessTreatmentQ831() {
		return illnessTreatmentQ831;
	}

	public String getIllnessTreatmentQ832() {
		return illnessTreatmentQ832;
	}

	public String getIllnessTreatmentQ833() {
		return illnessTreatmentQ833;
	}

	public String getIllnessTreatmentQ834() {
		return illnessTreatmentQ834;
	}

	public String getIllnessTreatmentQ841() {
		return illnessTreatmentQ841;
	}

	public String getIllnessTreatmentQ842() {
		return illnessTreatmentQ842;
	}

	public String getIllnessTreatmentQ843() {
		return illnessTreatmentQ843;
	}

	public String getIllnessTreatmentQ844() {
		return illnessTreatmentQ844;
	}

	public String getIllnessTreatmentQ851() {
		return illnessTreatmentQ851;
	}

	public String getIllnessTreatmentQ852() {
		return illnessTreatmentQ852;
	}

	public String getIllnessTreatmentQ853() {
		return illnessTreatmentQ853;
	}

	public String getIllnessTreatmentQ854() {
		return illnessTreatmentQ854;
	}

	public String getIllnessTreatmentQ911() {
		return illnessTreatmentQ911;
	}

	public String getIllnessTreatmentQ912() {
		return illnessTreatmentQ912;
	}

	public String getIllnessTreatmentQ913() {
		return illnessTreatmentQ913;
	}

	public String getIllnessTreatmentQ914() {
		return illnessTreatmentQ914;
	}

	public String getIllnessTreatmentQ921() {
		return illnessTreatmentQ921;
	}

	public String getIllnessTreatmentQ922() {
		return illnessTreatmentQ922;
	}

	public String getIllnessTreatmentQ923() {
		return illnessTreatmentQ923;
	}

	public String getIllnessTreatmentQ924() {
		return illnessTreatmentQ924;
	}

	public String getIllnessTreatmentQ1011() {
		return illnessTreatmentQ1011;
	}

	public String getIllnessTreatmentQ1012() {
		return illnessTreatmentQ1012;
	}

	public String getIllnessTreatmentQ1013() {
		return illnessTreatmentQ1013;
	}

	public String getIllnessTreatmentQ1014() {
		return illnessTreatmentQ1014;
	}

	public String getIllnessTreatmentQ1021() {
		return illnessTreatmentQ1021;
	}

	public String getIllnessTreatmentQ1022() {
		return illnessTreatmentQ1022;
	}

	public String getIllnessTreatmentQ1023() {
		return illnessTreatmentQ1023;
	}

	public String getIllnessTreatmentQ1024() {
		return illnessTreatmentQ1024;
	}

	public String getIllnessTreatmentQ1031() {
		return illnessTreatmentQ1031;
	}

	public String getIllnessTreatmentQ1032() {
		return illnessTreatmentQ1032;
	}

	public String getIllnessTreatmentQ1033() {
		return illnessTreatmentQ1033;
	}

	public String getIllnessTreatmentQ1034() {
		return illnessTreatmentQ1034;
	}

	public String getIllnessTreatmentQ1041() {
		return illnessTreatmentQ1041;
	}

	public String getIllnessTreatmentQ1042() {
		return illnessTreatmentQ1042;
	}

	public String getIllnessTreatmentQ1043() {
		return illnessTreatmentQ1043;
	}

	public String getIllnessTreatmentQ1044() {
		return illnessTreatmentQ1044;
	}

	public String getIllnessTreatmentQ1051() {
		return illnessTreatmentQ1051;
	}
	
	public String getIllnessTreatmentQ1052() {
		return illnessTreatmentQ1052;
	}

	public String getIllnessTreatmentQ1053() {
		return illnessTreatmentQ1053;
	}

	public String getIllnessTreatmentQ1054() {
		return illnessTreatmentQ1054;
	}

	public String getIllnessTreatmentQ1061() {
		return illnessTreatmentQ1061;
	}

	public String getIllnessTreatmentQ1062() {
		return illnessTreatmentQ1062;				
	}

	public String getIllnessTreatmentQ1063() {
		return illnessTreatmentQ1063;
	}

	public String getIllnessTreatmentQ1064() {
		return illnessTreatmentQ1064;
	}

	public String getIllnessTreatmentQ1111() {
		return illnessTreatmentQ1111;
	}

	public String getIllnessTreatmentQ1112() {
		return illnessTreatmentQ1112;
	}

	public String getIllnessTreatmentQ1113() {
		return illnessTreatmentQ1113;
	}

	public String getIllnessTreatmentQ1114() {
		return illnessTreatmentQ1114;
	}

	public String getIllnessTreatmentQ1121() {
		return illnessTreatmentQ1121;
	}

	public String getIllnessTreatmentQ1122() {
		return illnessTreatmentQ1122;
	}

	public String getIllnessTreatmentQ1123() {
		return illnessTreatmentQ1123;
	}

	public String getIllnessTreatmentQ1124() {
		return illnessTreatmentQ1124;
	}

	public String getIllnessTreatmentQ1131() {
		return illnessTreatmentQ1131;
	}

	public String getIllnessTreatmentQ1132() {
		return illnessTreatmentQ1132;
	}

	public String getIllnessTreatmentQ1133() {
		return illnessTreatmentQ1133;
	}

	public String getIllnessTreatmentQ1134() {
		return illnessTreatmentQ1134;
	}

	public String getIllnessTreatmentQ1141() {
		return illnessTreatmentQ1141;
	}

	public String getIllnessTreatmentQ1142() {
		return illnessTreatmentQ1142;
	}

	public String getIllnessTreatmentQ1143() {
		return illnessTreatmentQ1143;
	}

	public String getIllnessTreatmentQ1144() {
		return illnessTreatmentQ1144;
	}

	public String getIllnessTreatmentQ1151() {
		return illnessTreatmentQ1151;
	}

	public String getIllnessTreatmentQ1152() {
		return illnessTreatmentQ1152;
	}

	public String getIllnessTreatmentQ1153() {
		return illnessTreatmentQ1153;
	}

	public String getIllnessTreatmentQ1154() {
		return illnessTreatmentQ1154;
	}

	public String getIllnessTreatmentQ1161() {
		return illnessTreatmentQ1161;
	}

	public String getIllnessTreatmentQ1162() {
		return illnessTreatmentQ1162;
	}

	public String getIllnessTreatmentQ1163() {
		return illnessTreatmentQ1163;
	}

	public String getIllnessTreatmentQ1164() {
		return illnessTreatmentQ1164;
	}

	public String getIllnessTreatmentQ1171() {
		return illnessTreatmentQ1171;
	}

	public String getIllnessTreatmentQ1172() {
		return illnessTreatmentQ1172;
	}

	public String getIllnessTreatmentQ1173() {
		return illnessTreatmentQ1173;
	}

	public String getIllnessTreatmentQ1174() {
		return illnessTreatmentQ1174;
	}

	public String getIllnessTreatmentQ1211() {
		return illnessTreatmentQ1211;
	}

	public String getIllnessTreatmentQ1212() {
		return illnessTreatmentQ1212;
	}

	public String getIllnessTreatmentQ1213() {
		return illnessTreatmentQ1213;
	}

	public String getIllnessTreatmentQ1214() {
		return illnessTreatmentQ1214;
	}

	public String getIllnessTreatmentQ1221() {
		return illnessTreatmentQ1221;
	}

	public String getIllnessTreatmentQ1222() {
		return illnessTreatmentQ1222;
	}

	public String getIllnessTreatmentQ1223() {
		return illnessTreatmentQ1223;
	}

	public String getIllnessTreatmentQ1224() {
		return illnessTreatmentQ1224;
	}

	public String getIllnessTreatmentQ1311() {
		return illnessTreatmentQ1311;
	}

	public String getIllnessTreatmentQ1312() {
		return illnessTreatmentQ1312;
	}

	public String getIllnessTreatmentQ1313() {
		return illnessTreatmentQ1313;
	}

	public String getIllnessTreatmentQ1314() {
		return illnessTreatmentQ1314;
	}

	public String getIllnessTreatmentQ1321() {
		return illnessTreatmentQ1321;
	}

	public String getIllnessTreatmentQ1322() {
		return illnessTreatmentQ1322;
	}

	public String getIllnessTreatmentQ1323() {
		return illnessTreatmentQ1323;
	}

	public String getIllnessTreatmentQ1324() {
		return illnessTreatmentQ1324;
	}

	public String getIllnessTreatmentQ1411() {
		return illnessTreatmentQ1411;
	}

	public String getIllnessTreatmentQ1412() {
		return illnessTreatmentQ1412;
	}

	public String getIllnessTreatmentQ1413() {
		return illnessTreatmentQ1413;
	}

	public String getIllnessTreatmentQ1414() {
		return illnessTreatmentQ1414;
	}

	public String getIllnessTreatmentQ1421() {
		return illnessTreatmentQ1421;
	}

	public String getIllnessTreatmentQ1422() {
		return illnessTreatmentQ1422;
	}

	public String getIllnessTreatmentQ1423() {
		return illnessTreatmentQ1423;
	}

	public String getIllnessTreatmentQ1424() {
		return illnessTreatmentQ1424;
	}

	public String getIllnessTreatmentQ1431() {
		return illnessTreatmentQ1431;
	}

	public String getIllnessTreatmentQ1432() {
		return illnessTreatmentQ1432;
	}

	public String getIllnessTreatmentQ1433() {
		return illnessTreatmentQ1433;
	}

	public String getIllnessTreatmentQ1434() {
		return illnessTreatmentQ1434;
	}

	public String getIllnessTreatmentQ1511() {
		return illnessTreatmentQ1511;
	}

	public String getIllnessTreatmentQ1512() {
		return illnessTreatmentQ1512;
	}

	public String getIllnessTreatmentQ1513() {
		return illnessTreatmentQ1513;
	}

	public String getIllnessTreatmentQ1514() {
		return illnessTreatmentQ1514;
	}

	public String getIllnessTreatmentQ1611() {
		return illnessTreatmentQ1611;
	}

	public String getIllnessTreatmentQ1612() {
		return illnessTreatmentQ1612;
	}

	public String getIllnessTreatmentQ1613() {
		return illnessTreatmentQ1613;
	}

	public String getIllnessTreatmentQ1614() {
		return illnessTreatmentQ1614;
	}

	public String getIllnessTreatmentQ1711() {
		return illnessTreatmentQ1711;
	}

	public String getIllnessTreatmentQ1712() {
		return illnessTreatmentQ1712;
	}

	public String getIllnessTreatmentQ1713() {
		return illnessTreatmentQ1713;
	}

	public String getIllnessTreatmentQ1714() {
		return illnessTreatmentQ1714;
	}

	public String getIllnessTreatmentQ1811() {
		return illnessTreatmentQ1811;
	}

	public String getIllnessTreatmentQ1812() {
		return illnessTreatmentQ1812;
	}

	public String getIllnessTreatmentQ1813() {
		return illnessTreatmentQ1813;
	}

	public String getIllnessTreatmentQ1814() {
		return illnessTreatmentQ1814;
	}

	public String getIllnessTreatmentQ1911() {
		return illnessTreatmentQ1911;
	}

	public String getIllnessTreatmentQ1912() {
		return illnessTreatmentQ1912;
	}

	public String getIllnessTreatmentQ1913() {
		return illnessTreatmentQ1913;
	}

	public String getIllnessTreatmentQ1914() {
		return illnessTreatmentQ1914;
	}

	public String getIllnessTreatmentQ2011() {
		return illnessTreatmentQ2011;
	}

	public String getIllnessTreatmentQ2111() {
		return illnessTreatmentQ2111;
	}

	public String getIllnessTreatmentQ2112() {
		return illnessTreatmentQ2112;
	}

	public String getIllnessTreatmentQ2113() {
		return illnessTreatmentQ2113;
	}

	public String getIllnessTreatmentQ2114() {
		return illnessTreatmentQ2114;
	}

	public String getIllnessTreatmentQ2121() {
		return illnessTreatmentQ2121;
	}

	public String getIllnessTreatmentQ2211() {
		return illnessTreatmentQ2211;
	}

	public String getIllnessTreatmentQ2212() {
		return illnessTreatmentQ2212;
	}

	public String getIllnessTreatmentQ2213() {
		return illnessTreatmentQ2213;
	}

	public String getIllnessTreatmentQ2214() {
		return illnessTreatmentQ2214;
	}

	public String getIllnessTreatmentQ2215() {
		return illnessTreatmentQ2215;
	}

	public String getIllnessTreatmentQ23() {
		return illnessTreatmentQ23;
	}

	public String getIllnessTreatmentQ2311() {
		return illnessTreatmentQ2311;
	}

	public String getIllnessTreatmentQ2312() {
		return illnessTreatmentQ2312;
	}

	public String getIllnessTreatmentQ2313() {
		return illnessTreatmentQ2313;
	}

	public String getIllnessTreatmentQ2314() {
		return illnessTreatmentQ2314;
	}

	public String getIllnessTreatmentQ2315() {
		return illnessTreatmentQ2315;
	}

	public String getIllnessTreatmentQ2411() {
		return illnessTreatmentQ2411;
	}

	public String getIllnessTreatmentQ2412() {
		return illnessTreatmentQ2412;
	}

	public String getIllnessTreatmentQ2413() {
		return illnessTreatmentQ2413;
	}

	public String getIllnessTreatmentQ2414() {
		return illnessTreatmentQ2414;
	}

	public String getIllnessTreatmentQ2511() {
		return illnessTreatmentQ2511;
	}

	public String getIllnessTreatmentQ2512() {
		return illnessTreatmentQ2512;
	}

	public String getIllnessTreatmentQ2513() {
		return illnessTreatmentQ2513;
	}

	public String getIllnessTreatmentQ2514() {
		return illnessTreatmentQ2514;
	}

	public String getIllnessTreatmentQ2515() {
		return illnessTreatmentQ2515;
	}

	public String getIllnessTreatmentQ2521() {
		return illnessTreatmentQ2521;
	}

	public String getIllnessTreatmentQ2522() {
		return illnessTreatmentQ2522;
	}

	public String getIllnessTreatmentQ2523() {
		return illnessTreatmentQ2523;
	}

	public String getIllnessTreatmentQ2524() {
		return illnessTreatmentQ2524;
	}

	public String getIllnessTreatmentQ2525() {
		return illnessTreatmentQ2525;
	}

	public String getIllnessTreatmentQ2531() {
		return illnessTreatmentQ2531;
	}

	public String getIllnessTreatmentQ2532() {
		return illnessTreatmentQ2532;
	}

	public String getIllnessTreatmentQ2533() {
		return illnessTreatmentQ2533;
	}

	public String getIllnessTreatmentQ2534() {
		return illnessTreatmentQ2534;
	}

	public String getIllnessTreatmentQ2535() {
		return illnessTreatmentQ2535;
	}

	public String getIllnessTreatmentQ2541() {
		return illnessTreatmentQ2541;
	}

	public String getIllnessTreatmentQ2542() {
		return illnessTreatmentQ2542;
	}

	public String getIllnessTreatmentQ2543() {
		return illnessTreatmentQ2543;
	}

	public String getIllnessTreatmentQ2544() {
		return illnessTreatmentQ2544;
	}

	public String getIllnessTreatmentQ2545() {
		return illnessTreatmentQ2545;
	}

	public String getIllnessTreatmentQ2551() {
		return illnessTreatmentQ2551;
	}

	public String getIllnessTreatmentQ2552() {
		return illnessTreatmentQ2552;
	}

	public String getIllnessTreatmentQ2553() {
		return illnessTreatmentQ2553;
	}

	public String getIllnessTreatmentQ2554() {
		return illnessTreatmentQ2554;
	}

	public String getIllnessTreatmentQ2555() {
		return illnessTreatmentQ2555;
	}

	public String getIllnessTreatmentQ2561() {
		return illnessTreatmentQ2561;
	}

	public String getIllnessTreatmentQ2562() {
		return illnessTreatmentQ2562;
	}

	public String getIllnessTreatmentQ2563() {
		return illnessTreatmentQ2563;
	}

	public String getIllnessTreatmentQ2564() {
		return illnessTreatmentQ2564;
	}

	public String getIllnessTreatmentQ2565() {
		return illnessTreatmentQ2565;
	}

	public String getIllnessTreatmentQ2571() {
		return illnessTreatmentQ2571;
	}

	public String getIllnessTreatmentQ2572() {
		return illnessTreatmentQ2572;
	}

	public String getIllnessTreatmentQ2573() {
		return illnessTreatmentQ2573;
	}

	public String getIllnessTreatmentQ2574() {
		return illnessTreatmentQ2574;
	}

	public String getIllnessTreatmentQ2575() {
		return illnessTreatmentQ2575;
	}

	public String getIllnessTreatmentQ2611() {
		return illnessTreatmentQ2611;
	}

	public String getIllnessTreatmentQ2612() {
		return illnessTreatmentQ2612;
	}

	public String getIllnessTreatmentQ2613() {
		return illnessTreatmentQ2613;
	}

	public String getIllnessTreatmentQ2614() {
		return illnessTreatmentQ2614;
	}

	public String getIllnessTreatmentQ2615() {
		return illnessTreatmentQ2615;
	}

	public String getIllnessTreatmentQ2621() {
		return illnessTreatmentQ2621;
	}

	public String getIllnessTreatmentQ2622() {
		return illnessTreatmentQ2622;
	}

	public String getIllnessTreatmentQ2623() {
		return illnessTreatmentQ2623;
	}

	public String getIllnessTreatmentQ2624() {
		return illnessTreatmentQ2624;
	}

	public String getIllnessTreatmentQ2625() {
		return illnessTreatmentQ2625;
	}

	public String getIllnessTreatmentQ2631() {
		return illnessTreatmentQ2631;
	}

	public String getIllnessTreatmentQ2632() {
		return illnessTreatmentQ2632;
	}

	public String getIllnessTreatmentQ2633() {
		return illnessTreatmentQ2633;
	}

	public String getIllnessTreatmentQ2634() {
		return illnessTreatmentQ2634;
	}

	public String getIllnessTreatmentQ2635() {
		return illnessTreatmentQ2635;
	}

	public String getIllnessTreatmentQ2641() {
		return illnessTreatmentQ2641;
	}

	public String getIllnessTreatmentQ2642() {
		return illnessTreatmentQ2642;
	}

	public String getIllnessTreatmentQ2643() {
		return illnessTreatmentQ2643;
	}

	public String getIllnessTreatmentQ2644() {
		return illnessTreatmentQ2644;
	}

	public String getIllnessTreatmentQ2645() {
		return illnessTreatmentQ2645;
	}

	public String getIllnessTreatmentQ2651() {
		return illnessTreatmentQ2651;
	}

	public String getIllnessTreatmentQ2652() {
		return illnessTreatmentQ2652;
	}

	public String getIllnessTreatmentQ2653() {
		return illnessTreatmentQ2653;
	}

	public String getIllnessTreatmentQ2654() {
		return illnessTreatmentQ2654;
	}

	public String getIllnessTreatmentQ2655() {
		return illnessTreatmentQ2655;
	}

	public String getIllnessTreatmentQ2661() {
		return illnessTreatmentQ2661;
	}

	public String getIllnessTreatmentQ2662() {
		return illnessTreatmentQ2662;
	}

	public String getIllnessTreatmentQ2663() {
		return illnessTreatmentQ2663;
	}

	public String getIllnessTreatmentQ2664() {
		return illnessTreatmentQ2664;
	}

	public String getIllnessTreatmentQ2665() {
		return illnessTreatmentQ2665;
	}

	public String getIllnessTreatmentQ2671() {
		return illnessTreatmentQ2671;
	}

	public String getIllnessTreatmentQ2672() {
		return illnessTreatmentQ2672;
	}

	public String getIllnessTreatmentQ2673() {
		return illnessTreatmentQ2673;
	}

	public String getIllnessTreatmentQ2674() {
		return illnessTreatmentQ2674;
	}

	public String getIllnessTreatmentQ2675() {
		return illnessTreatmentQ2675;
	}

	public String getIllnessTreatmentQ2711() {
		return illnessTreatmentQ2711;
	}

	public String getIllnessTreatmentQ2712() {
		return illnessTreatmentQ2712;
	}

	public String getIllnessTreatmentQ2713() {
		return illnessTreatmentQ2713;
	}

	public String getIllnessTreatmentQ2714() {
		return illnessTreatmentQ2714;
	}

	public String getIllnessTreatmentQ2715() {
		return illnessTreatmentQ2715;
	}

	public String getIllnessTreatmentQ2721() {
		return illnessTreatmentQ2721;
	}

	public String getIllnessTreatmentQ2722() {
		return illnessTreatmentQ2722;
	}

	public String getIllnessTreatmentQ2723() {
		return illnessTreatmentQ2723;
	}

	public String getIllnessTreatmentQ2724() {
		return illnessTreatmentQ2724;
	}

	public String getIllnessTreatmentQ2725() {
		return illnessTreatmentQ2725;
	}

	public String getIllnessTreatmentQ2731() {
		return illnessTreatmentQ2731;
	}

	public String getIllnessTreatmentQ2732() {
		return illnessTreatmentQ2732;
	}

	public String getIllnessTreatmentQ2733() {
		return illnessTreatmentQ2733;
	}

	public String getIllnessTreatmentQ2734() {
		return illnessTreatmentQ2734;
	}

	public String getIllnessTreatmentQ2735() {
		return illnessTreatmentQ2735;
	}

	public String getIllnessTreatmentQ2741() {
		return illnessTreatmentQ2741;
	}

	public String getIllnessTreatmentQ2742() {
		return illnessTreatmentQ2742;
	}

	public String getIllnessTreatmentQ2743() {
		return illnessTreatmentQ2743;
	}

	public String getIllnessTreatmentQ2744() {
		return illnessTreatmentQ2744;
	}

	public String getIllnessTreatmentQ2745() {
		return illnessTreatmentQ2745;
	}

	public String getIllnessTreatmentQ2751() {
		return illnessTreatmentQ2751;
	}

	public String getIllnessTreatmentQ2752() {
		return illnessTreatmentQ2752;
	}

	public String getIllnessTreatmentQ2753() {
		return illnessTreatmentQ2753;
	}

	public String getIllnessTreatmentQ2754() {
		return illnessTreatmentQ2754;
	}

	public String getIllnessTreatmentQ2755() {
		return illnessTreatmentQ2755;
	}

	public String getIllnessTreatmentQ2761() {
		return illnessTreatmentQ2761;
	}

	public String getIllnessTreatmentQ2762() {
		return illnessTreatmentQ2762;
	}

	public String getIllnessTreatmentQ2763() {
		return illnessTreatmentQ2763;
	}

	public String getIllnessTreatmentQ2764() {
		return illnessTreatmentQ2764;
	}

	public String getIllnessTreatmentQ2765() {
		return illnessTreatmentQ2765;
	}

	public String getIllnessTreatmentQ2771() {
		return illnessTreatmentQ2771;
	}

	public String getIllnessTreatmentQ2772() {
		return illnessTreatmentQ2772;
	}

	public String getIllnessTreatmentQ2773() {
		return illnessTreatmentQ2773;
	}

	public String getIllnessTreatmentQ2774() {
		return illnessTreatmentQ2774;
	}

	public String getIllnessTreatmentQ2775() {
		return illnessTreatmentQ2775;
	}

	public String getIllnessTreatmentQ2811() {
		return illnessTreatmentQ2811;
	}

	public String getIllnessTreatmentQ2812() {
		return illnessTreatmentQ2812;
	}

	public String getIllnessTreatmentQ2813() {
		return illnessTreatmentQ2813;
	}

	public String getIllnessTreatmentQ2814() {
		return illnessTreatmentQ2814;
	}

	public String getIllnessTreatmentQ2815() {
		return illnessTreatmentQ2815;
	}

	public String getIllnessTreatmentQ2821() {
		return illnessTreatmentQ2821;
	}

	public String getIllnessTreatmentQ2822() {
		return illnessTreatmentQ2822;
	}

	public String getIllnessTreatmentQ2823() {
		return illnessTreatmentQ2823;
	}

	public String getIllnessTreatmentQ2824() {
		return illnessTreatmentQ2824;
	}

	public String getIllnessTreatmentQ2825() {
		return illnessTreatmentQ2825;
	}

	public String getIllnessTreatmentPQ1() {
		return illnessTreatmentPQ1;
	}

	public String getIllnessTreatmentPQ111() {
		return illnessTreatmentPQ111;
	}

	public String getIllnessTreatmentPQ112() {
		return illnessTreatmentPQ112;
	}

	public String getIllnessTreatmentPQ113() {
		return illnessTreatmentPQ113;
	}

	public String getIllnessTreatmentPQ114() {
		return illnessTreatmentPQ114;
	}

	public String getIllnessTreatmentPQ121() {
		return illnessTreatmentPQ121;
	}

	public String getIllnessTreatmentPQ122() {
		return illnessTreatmentPQ122;
	}

	public String getIllnessTreatmentPQ123() {
		return illnessTreatmentPQ123;
	}

	public String getIllnessTreatmentPQ124() {
		return illnessTreatmentPQ124;
	}

	public String getIllnessTreatmentPQ131() {
		return illnessTreatmentPQ131;
	}

	public String getIllnessTreatmentPQ132() {
		return illnessTreatmentPQ132;
	}

	public String getIllnessTreatmentPQ133() {
		return illnessTreatmentPQ133;
	}

	public String getIllnessTreatmentPQ134() {
		return illnessTreatmentPQ134;
	}

	public String getIllnessTreatmentPQ141() {
		return illnessTreatmentPQ141;
	}

	public String getIllnessTreatmentPQ142() {
		return illnessTreatmentPQ142;
	}

	public String getIllnessTreatmentPQ143() {
		return illnessTreatmentPQ143;
	}

	public String getIllnessTreatmentPQ144() {
		return illnessTreatmentPQ144;
	}

	public String getIllnessTreatmentPQ151() {
		return illnessTreatmentPQ151;
	}

	public String getIllnessTreatmentPQ152() {
		return illnessTreatmentPQ152;
	}

	public String getIllnessTreatmentPQ153() {
		return illnessTreatmentPQ153;
	}

	public String getIllnessTreatmentPQ154() {
		return illnessTreatmentPQ154;
	}

	public String getIllnessTreatmentPQ161() {
		return illnessTreatmentPQ161;
	}

	public String getIllnessTreatmentPQ162() {
		return illnessTreatmentPQ162;
	}

	public String getIllnessTreatmentPQ163() {
		return illnessTreatmentPQ163;
	}

	public String getIllnessTreatmentPQ164() {
		return illnessTreatmentPQ164;
	}

	public String getIllnessTreatmentPQ171() {
		return illnessTreatmentPQ171;
	}

	public String getIllnessTreatmentPQ172() {
		return illnessTreatmentPQ172;
	}

	public String getIllnessTreatmentPQ173() {
		return illnessTreatmentPQ173;
	}

	public String getIllnessTreatmentPQ174() {
		return illnessTreatmentPQ174;
	}

	public String getIllnessTreatmentPQ181() {
		return illnessTreatmentPQ181;
	}

	public String getIllnessTreatmentPQ182() {
		return illnessTreatmentPQ182;
	}

	public String getIllnessTreatmentPQ183() {
		return illnessTreatmentPQ183;
	}

	public String getIllnessTreatmentPQ184() {
		return illnessTreatmentPQ184;
	}

	public String getIllnessTreatmentPQ2() {
		return illnessTreatmentPQ2;
	}

	public String getIllnessTreatmentPQ211() {
		return illnessTreatmentPQ211;
	}

	public String getIllnessTreatmentPQ212() {
		return illnessTreatmentPQ212;
	}

	public String getIllnessTreatmentPQ213() {
		return illnessTreatmentPQ213;
	}

	public String getIllnessTreatmentPQ214() {
		return illnessTreatmentPQ214;
	}

	public String getIllnessTreatmentPQ221() {
		return illnessTreatmentPQ221;
	}

	public String getIllnessTreatmentPQ222() {
		return illnessTreatmentPQ222;
	}

	public String getIllnessTreatmentPQ223() {
		return illnessTreatmentPQ223;
	}

	public String getIllnessTreatmentPQ224() {
		return illnessTreatmentPQ224;
	}

	public String getIllnessTreatmentPQ231() {
		return illnessTreatmentPQ231;
	}

	public String getIllnessTreatmentPQ232() {
		return illnessTreatmentPQ232;
	}

	public String getIllnessTreatmentPQ233() {
		return illnessTreatmentPQ233;
	}

	public String getIllnessTreatmentPQ234() {
		return illnessTreatmentPQ234;
	}

	public String getIllnessTreatmentPQ241() {
		return illnessTreatmentPQ241;
	}

	public String getIllnessTreatmentPQ242() {
		return illnessTreatmentPQ242;
	}

	public String getIllnessTreatmentPQ243() {
		return illnessTreatmentPQ243;
	}

	public String getIllnessTreatmentPQ244() {
		return illnessTreatmentPQ244;
	}

	public String getIllnessTreatmentPQ3() {
		return illnessTreatmentPQ3;
	}

	public String getIllnessTreatmentPQ311() {
		return illnessTreatmentPQ311;
	}

	public String getIllnessTreatmentPQ312() {
		return illnessTreatmentPQ312;
	}

	public String getIllnessTreatmentPQ313() {
		return illnessTreatmentPQ313;
	}

	public String getIllnessTreatmentPQ314() {
		return illnessTreatmentPQ314;
	}

	public String getIllnessTreatmentPQ321() {
		return illnessTreatmentPQ321;
	}

	public String getIllnessTreatmentPQ322() {
		return illnessTreatmentPQ322;
	}

	public String getIllnessTreatmentPQ323() {
		return illnessTreatmentPQ323;
	}

	public String getIllnessTreatmentPQ324() {
		return illnessTreatmentPQ324;
	}

	public String getIllnessTreatmentPQ331() {
		return illnessTreatmentPQ331;
	}

	public String getIllnessTreatmentPQ332() {
		return illnessTreatmentPQ332;
	}

	public String getIllnessTreatmentPQ333() {
		return illnessTreatmentPQ333;
	}

	public String getIllnessTreatmentPQ334() {
		return illnessTreatmentPQ334;
	}

	public String getIllnessTreatmentPQ341() {
		return illnessTreatmentPQ341;
	}

	public String getIllnessTreatmentPQ342() {
		return illnessTreatmentPQ342;
	}

	public String getIllnessTreatmentPQ343() {
		return illnessTreatmentPQ343;
	}

	public String getIllnessTreatmentPQ344() {
		return illnessTreatmentPQ344;
	}

	public String getIllnessTreatmentPQ351() {
		return illnessTreatmentPQ351;
	}

	public String getIllnessTreatmentPQ352() {
		return illnessTreatmentPQ352;
	}

	public String getIllnessTreatmentPQ353() {
		return illnessTreatmentPQ353;
	}

	public String getIllnessTreatmentPQ354() {
		return illnessTreatmentPQ354;
	}

	public String getIllnessTreatmentPQ361() {
		return illnessTreatmentPQ361;
	}

	public String getIllnessTreatmentPQ362() {
		return illnessTreatmentPQ362;
	}

	public String getIllnessTreatmentPQ363() {
		return illnessTreatmentPQ363;
	}

	public String getIllnessTreatmentPQ364() {
		return illnessTreatmentPQ364;
	}

	public String getIllnessTreatmentPQ371() {
		return illnessTreatmentPQ371;
	}

	public String getIllnessTreatmentPQ372() {
		return illnessTreatmentPQ372;
	}

	public String getIllnessTreatmentPQ373() {
		return illnessTreatmentPQ373;
	}

	public String getIllnessTreatmentPQ374() {
		return illnessTreatmentPQ374;
	}

	public String getIllnessTreatmentPQ4() {
		return illnessTreatmentPQ4;
	}

	public String getIllnessTreatmentPQ411() {
		return illnessTreatmentPQ411;
	}

	public String getIllnessTreatmentPQ412() {
		return illnessTreatmentPQ412;
	}

	public String getIllnessTreatmentPQ413() {
		return illnessTreatmentPQ413;
	}

	public String getIllnessTreatmentPQ414() {
		return illnessTreatmentPQ414;
	}

	public String getIllnessTreatmentPQ421() {
		return illnessTreatmentPQ421;
	}

	public String getIllnessTreatmentPQ422() {
		return illnessTreatmentPQ422;
	}

	public String getIllnessTreatmentPQ423() {
		return illnessTreatmentPQ423;
	}

	public String getIllnessTreatmentPQ424() {
		return illnessTreatmentPQ424;
	}

	public String getIllnessTreatmentPQ431() {
		return illnessTreatmentPQ431;
	}

	public String getIllnessTreatmentPQ432() {
		return illnessTreatmentPQ432;
	}

	public String getIllnessTreatmentPQ433() {
		return illnessTreatmentPQ433;
	}

	public String getIllnessTreatmentPQ434() {
		return illnessTreatmentPQ434;
	}

	public String getIllnessTreatmentPQ441() {
		return illnessTreatmentPQ441;
	}

	public String getIllnessTreatmentPQ442() {
		return illnessTreatmentPQ442;
	}

	public String getIllnessTreatmentPQ443() {
		return illnessTreatmentPQ443;
	}

	public String getIllnessTreatmentPQ444() {
		return illnessTreatmentPQ444;
	}

	public String getIllnessTreatmentPQ451() {
		return illnessTreatmentPQ451;
	}

	public String getIllnessTreatmentPQ452() {
		return illnessTreatmentPQ452;
	}

	public String getIllnessTreatmentPQ453() {
		return illnessTreatmentPQ453;
	}

	public String getIllnessTreatmentPQ454() {
		return illnessTreatmentPQ454;
	}

	public String getIllnessTreatmentPQ461() {
		return illnessTreatmentPQ461;
	}

	public String getIllnessTreatmentPQ462() {
		return illnessTreatmentPQ462;
	}

	public String getIllnessTreatmentPQ463() {
		return illnessTreatmentPQ463;
	}

	public String getIllnessTreatmentPQ464() {
		return illnessTreatmentPQ464;
	}

	public String getIllnessTreatmentPQ5() {
		return illnessTreatmentPQ5;
	}

	public String getIllnessTreatmentPQ511() {
		return illnessTreatmentPQ511;
	}

	public String getIllnessTreatmentPQ512() {
		return illnessTreatmentPQ512;
	}

	public String getIllnessTreatmentPQ513() {
		return illnessTreatmentPQ513;
	}

	public String getIllnessTreatmentPQ514() {
		return illnessTreatmentPQ514;
	}

	public String getIllnessTreatmentPQ521() {
		return illnessTreatmentPQ521;
	}

	public String getIllnessTreatmentPQ522() {
		return illnessTreatmentPQ522;
	}

	public String getIllnessTreatmentPQ523() {
		return illnessTreatmentPQ523;
	}

	public String getIllnessTreatmentPQ524() {
		return illnessTreatmentPQ524;
	}

	public String getIllnessTreatmentPQ531() {
		return illnessTreatmentPQ531;
	}

	public String getIllnessTreatmentPQ532() {
		return illnessTreatmentPQ532;
	}

	public String getIllnessTreatmentPQ533() {
		return illnessTreatmentPQ533;
	}

	public String getIllnessTreatmentPQ534() {
		return illnessTreatmentPQ534;
	}

	public String getIllnessTreatmentPQ6() {
		return illnessTreatmentPQ6;
	}

	public String getIllnessTreatmentPQ611() {
		return illnessTreatmentPQ611;
	}

	public String getIllnessTreatmentPQ612() {
		return illnessTreatmentPQ612;
	}

	public String getIllnessTreatmentPQ613() {
		return illnessTreatmentPQ613;
	}

	public String getIllnessTreatmentPQ614() {
		return illnessTreatmentPQ614;
	}

	public String getIllnessTreatmentPQ621() {
		return illnessTreatmentPQ621;
	}

	public String getIllnessTreatmentPQ622() {
		return illnessTreatmentPQ622;
	}

	public String getIllnessTreatmentPQ623() {
		return illnessTreatmentPQ623;
	}

	public String getIllnessTreatmentPQ624() {
		return illnessTreatmentPQ624;
	}

	public String getIllnessTreatmentPQ631() {
		return illnessTreatmentPQ631;
	}

	public String getIllnessTreatmentPQ632() {
		return illnessTreatmentPQ632;
	}

	public String getIllnessTreatmentPQ633() {
		return illnessTreatmentPQ633;
	}

	public String getIllnessTreatmentPQ634() {
		return illnessTreatmentPQ634;
	}

	public String getIllnessTreatmentPQ7() {
		return illnessTreatmentPQ7;
	}

	public String getIllnessTreatmentPQ711() {
		return illnessTreatmentPQ711;
	}

	public String getIllnessTreatmentPQ712() {
		return illnessTreatmentPQ712;
	}

	public String getIllnessTreatmentPQ713() {
		return illnessTreatmentPQ713;
	}

	public String getIllnessTreatmentPQ714() {
		return illnessTreatmentPQ714;
	}

	public String getIllnessTreatmentPQ721() {
		return illnessTreatmentPQ721;
	}

	public String getIllnessTreatmentPQ722() {
		return illnessTreatmentPQ722;
	}

	public String getIllnessTreatmentPQ723() {
		return illnessTreatmentPQ723;
	}

	public String getIllnessTreatmentPQ724() {
		return illnessTreatmentPQ724;
	}

	public String getIllnessTreatmentPQ731() {
		return illnessTreatmentPQ731;
	}

	public String getIllnessTreatmentPQ732() {
		return illnessTreatmentPQ732;
	}

	public String getIllnessTreatmentPQ733() {
		return illnessTreatmentPQ733;
	}

	public String getIllnessTreatmentPQ734() {
		return illnessTreatmentPQ734;
	}

	public String getIllnessTreatmentPQ741() {
		return illnessTreatmentPQ741;
	}

	public String getIllnessTreatmentPQ742() {
		return illnessTreatmentPQ742;
	}

	public String getIllnessTreatmentPQ743() {
		return illnessTreatmentPQ743;
	}

	public String getIllnessTreatmentPQ744() {
		return illnessTreatmentPQ744;
	}

	public String getIllnessTreatmentPQ751() {
		return illnessTreatmentPQ751;
	}

	public String getIllnessTreatmentPQ752() {
		return illnessTreatmentPQ752;
	}

	public String getIllnessTreatmentPQ753() {
		return illnessTreatmentPQ753;
	}

	public String getIllnessTreatmentPQ754() {
		return illnessTreatmentPQ754;
	}

	public String getIllnessTreatmentPQ8() {
		return illnessTreatmentPQ8;
	}

	public String getIllnessTreatmentPQ811() {
		return illnessTreatmentPQ811;
	}

	public String getIllnessTreatmentPQ812() {
		return illnessTreatmentPQ812;
	}

	public String getIllnessTreatmentPQ813() {
		return illnessTreatmentPQ813;
	}

	public String getIllnessTreatmentPQ814() {
		return illnessTreatmentPQ814;
	}

	public String getIllnessTreatmentPQ821() {
		return illnessTreatmentPQ821;
	}

	public String getIllnessTreatmentPQ822() {
		return illnessTreatmentPQ822;
	}

	public String getIllnessTreatmentPQ823() {
		return illnessTreatmentPQ823;
	}

	public String getIllnessTreatmentPQ824() {
		return illnessTreatmentPQ824;
	}

	public String getIllnessTreatmentPQ831() {
		return illnessTreatmentPQ831;
	}

	public String getIllnessTreatmentPQ832() {
		return illnessTreatmentPQ832;
	}

	public String getIllnessTreatmentPQ833() {
		return illnessTreatmentPQ833;
	}

	public String getIllnessTreatmentPQ834() {
		return illnessTreatmentPQ834;
	}

	public String getIllnessTreatmentPQ841() {
		return illnessTreatmentPQ841;
	}

	public String getIllnessTreatmentPQ842() {
		return illnessTreatmentPQ842;
	}

	public String getIllnessTreatmentPQ843() {
		return illnessTreatmentPQ843;
	}

	public String getIllnessTreatmentPQ844() {
		return illnessTreatmentPQ844;
	}

	public String getIllnessTreatmentPQ851() {
		return illnessTreatmentPQ851;
	}

	public String getIllnessTreatmentPQ852() {
		return illnessTreatmentPQ852;
	}

	public String getIllnessTreatmentPQ853() {
		return illnessTreatmentPQ853;
	}

	public String getIllnessTreatmentPQ854() {
		return illnessTreatmentPQ854;
	}

	public String getIllnessTreatmentPQ9() {
		return illnessTreatmentPQ9;
	}

	public String getIllnessTreatmentPQ911() {
		return illnessTreatmentPQ911;
	}

	public String getIllnessTreatmentPQ912() {
		return illnessTreatmentPQ912;
	}

	public String getIllnessTreatmentPQ913() {
		return illnessTreatmentPQ913;
	}

	public String getIllnessTreatmentPQ914() {
		return illnessTreatmentPQ914;
	}

	public String getIllnessTreatmentPQ921() {
		return illnessTreatmentPQ921;
	}

	public String getIllnessTreatmentPQ922() {
		return illnessTreatmentPQ922;
	}

	public String getIllnessTreatmentPQ923() {
		return illnessTreatmentPQ923;
	}

	public String getIllnessTreatmentPQ924() {
		return illnessTreatmentPQ924;
	}

	public String getIllnessTreatmentPQ10() {
		return illnessTreatmentPQ10;
	}

	public String getIllnessTreatmentPQ1011() {
		return illnessTreatmentPQ1011;
	}

	public String getIllnessTreatmentPQ1012() {
		return illnessTreatmentPQ1012;
	}

	public String getIllnessTreatmentPQ1013() {
		return illnessTreatmentPQ1013;
	}

	public String getIllnessTreatmentPQ1014() {
		return illnessTreatmentPQ1014;
	}

	public String getIllnessTreatmentPQ1021() {
		return illnessTreatmentPQ1021;
	}

	public String getIllnessTreatmentPQ1022() {
		return illnessTreatmentPQ1022;
	}

	public String getIllnessTreatmentPQ1023() {
		return illnessTreatmentPQ1023;
	}

	public String getIllnessTreatmentPQ1024() {
		return illnessTreatmentPQ1024;
	}

	public String getIllnessTreatmentPQ1031() {
		return illnessTreatmentPQ1031;
	}

	public String getIllnessTreatmentPQ1032() {
		return illnessTreatmentPQ1032;
	}

	public String getIllnessTreatmentPQ1033() {
		return illnessTreatmentPQ1033;
	}

	public String getIllnessTreatmentPQ1034() {
		return illnessTreatmentPQ1034;
	}

	public String getIllnessTreatmentPQ1041() {
		return illnessTreatmentPQ1041;
	}

	public String getIllnessTreatmentPQ1042() {
		return illnessTreatmentPQ1042;
	}

	public String getIllnessTreatmentPQ1043() {
		return illnessTreatmentPQ1043;
	}

	public String getIllnessTreatmentPQ1044() {
		return illnessTreatmentPQ1044;
	}

	public String getIllnessTreatmentPQ1051() {
		return illnessTreatmentPQ1051;
	}

	public String getIllnessTreatmentPQ1052() {
		return illnessTreatmentPQ1052;
	}

	public String getIllnessTreatmentPQ1053() {
		return illnessTreatmentPQ1053;
	}

	public String getIllnessTreatmentPQ1054() {
		return illnessTreatmentPQ1054;
	}

	public String getIllnessTreatmentPQ1061() {
		return illnessTreatmentPQ1061;
	}

	public String getIllnessTreatmentPQ1062() {
		return illnessTreatmentPQ1062;
	}

	public String getIllnessTreatmentPQ1063() {
		return illnessTreatmentPQ1063;
	}

	public String getIllnessTreatmentPQ1064() {
		return illnessTreatmentPQ1064;
	}

	public String getIllnessTreatmentPQ11() {
		return illnessTreatmentPQ11;
	}

	public String getIllnessTreatmentPQ1111() {
		return illnessTreatmentPQ1111;
	}

	public String getIllnessTreatmentPQ1112() {
		return illnessTreatmentPQ1112;
	}

	public String getIllnessTreatmentPQ1113() {
		return illnessTreatmentPQ1113;
	}

	public String getIllnessTreatmentPQ1114() {
		return illnessTreatmentPQ1114;
	}

	public String getIllnessTreatmentPQ1121() {
		return illnessTreatmentPQ1121;
	}

	public String getIllnessTreatmentPQ1122() {
		return illnessTreatmentPQ1122;
	}

	public String getIllnessTreatmentPQ1123() {
		return illnessTreatmentPQ1123;
	}

	public String getIllnessTreatmentPQ1124() {
		return illnessTreatmentPQ1124;
	}

	public String getIllnessTreatmentPQ1131() {
		return illnessTreatmentPQ1131;
	}

	public String getIllnessTreatmentPQ1132() {
		return illnessTreatmentPQ1132;
	}

	public String getIllnessTreatmentPQ1133() {
		return illnessTreatmentPQ1133;
	}

	public String getIllnessTreatmentPQ1134() {
		return illnessTreatmentPQ1134;
	}

	public String getIllnessTreatmentPQ1141() {
		return illnessTreatmentPQ1141;
	}

	public String getIllnessTreatmentPQ1142() {
		return illnessTreatmentPQ1142;
	}

	public String getIllnessTreatmentPQ1143() {
		return illnessTreatmentPQ1143;
	}

	public String getIllnessTreatmentPQ1144() {
		return illnessTreatmentPQ1144;
	}

	public String getIllnessTreatmentPQ1151() {
		return illnessTreatmentPQ1151;
	}

	public String getIllnessTreatmentPQ1152() {
		return illnessTreatmentPQ1152;
	}

	public String getIllnessTreatmentPQ1153() {
		return illnessTreatmentPQ1153;
	}

	public String getIllnessTreatmentPQ1154() {
		return illnessTreatmentPQ1154;
	}

	public String getIllnessTreatmentPQ1161() {
		return illnessTreatmentPQ1161;
	}

	public String getIllnessTreatmentPQ1162() {
		return illnessTreatmentPQ1162;
	}

	public String getIllnessTreatmentPQ1163() {
		return illnessTreatmentPQ1163;
	}

	public String getIllnessTreatmentPQ1164() {
		return illnessTreatmentPQ1164;
	}

	public String getIllnessTreatmentPQ1171() {
		return illnessTreatmentPQ1171;
	}

	public String getIllnessTreatmentPQ1172() {
		return illnessTreatmentPQ1172;
	}

	public String getIllnessTreatmentPQ1173() {
		return illnessTreatmentPQ1173;
	}

	public String getIllnessTreatmentPQ1174() {
		return illnessTreatmentPQ1174;
	}

	public String getIllnessTreatmentPQ12() {
		return illnessTreatmentPQ12;
	}

	public String getIllnessTreatmentPQ1211() {
		return illnessTreatmentPQ1211;
	}

	public String getIllnessTreatmentPQ1212() {
		return illnessTreatmentPQ1212;
	}

	public String getIllnessTreatmentPQ1213() {
		return illnessTreatmentPQ1213;
	}

	public String getIllnessTreatmentPQ1214() {
		return illnessTreatmentPQ1214;
	}

	public String getIllnessTreatmentPQ1221() {
		return illnessTreatmentPQ1221;
	}

	public String getIllnessTreatmentPQ1222() {
		return illnessTreatmentPQ1222;
	}

	public String getIllnessTreatmentPQ1223() {
		return illnessTreatmentPQ1223;
	}

	public String getIllnessTreatmentPQ1224() {
		return illnessTreatmentPQ1224;
	}

	public String getIllnessTreatmentPQ13() {
		return illnessTreatmentPQ13;
	}

	public String getIllnessTreatmentPQ1311() {
		return illnessTreatmentPQ1311;
	}

	public String getIllnessTreatmentPQ1312() {
		return illnessTreatmentPQ1312;
	}

	public String getIllnessTreatmentPQ1313() {
		return illnessTreatmentPQ1313;
	}

	public String getIllnessTreatmentPQ1314() {
		return illnessTreatmentPQ1314;
	}

	public String getIllnessTreatmentPQ1321() {
		return illnessTreatmentPQ1321;
	}

	public String getIllnessTreatmentPQ1322() {
		return illnessTreatmentPQ1322;
	}

	public String getIllnessTreatmentPQ1323() {
		return illnessTreatmentPQ1323;
	}

	public String getIllnessTreatmentPQ1324() {
		return illnessTreatmentPQ1324;
	}

	public String getIllnessTreatmentPQ14() {
		return illnessTreatmentPQ14;
	}

	public String getIllnessTreatmentPQ1411() {
		return illnessTreatmentPQ1411;
	}

	public String getIllnessTreatmentPQ1412() {
		return illnessTreatmentPQ1412;
	}

	public String getIllnessTreatmentPQ1413() {
		return illnessTreatmentPQ1413;
	}

	public String getIllnessTreatmentPQ1414() {
		return illnessTreatmentPQ1414;
	}

	public String getIllnessTreatmentPQ1421() {
		return illnessTreatmentPQ1421;
	}

	public String getIllnessTreatmentPQ1422() {
		return illnessTreatmentPQ1422;
	}

	public String getIllnessTreatmentPQ1423() {
		return illnessTreatmentPQ1423;
	}

	public String getIllnessTreatmentPQ1424() {
		return illnessTreatmentPQ1424;
	}

	public String getIllnessTreatmentPQ1431() {
		return illnessTreatmentPQ1431;
	}

	public String getIllnessTreatmentPQ1432() {
		return illnessTreatmentPQ1432;
	}

	public String getIllnessTreatmentPQ1433() {
		return illnessTreatmentPQ1433;
	}

	public String getIllnessTreatmentPQ1434() {
		return illnessTreatmentPQ1434;
	}

	public String getIllnessTreatmentPQ15() {
		return illnessTreatmentPQ15;
	}

	public String getIllnessTreatmentPQ1511() {
		return illnessTreatmentPQ1511;
	}

	public String getIllnessTreatmentPQ1512() {
		return illnessTreatmentPQ1512;
	}

	public String getIllnessTreatmentPQ1513() {
		return illnessTreatmentPQ1513;
	}

	public String getIllnessTreatmentPQ1514() {
		return illnessTreatmentPQ1514;
	}

	public String getIllnessTreatmentPQ16() {
		return illnessTreatmentPQ16;
	}

	public String getIllnessTreatmentPQ1611() {
		return illnessTreatmentPQ1611;
	}

	public String getIllnessTreatmentPQ1612() {
		return illnessTreatmentPQ1612;
	}

	public String getIllnessTreatmentPQ1613() {
		return illnessTreatmentPQ1613;
	}

	public String getIllnessTreatmentPQ1614() {
		return illnessTreatmentPQ1614;
	}

	public String getIllnessTreatmentPQ17() {
		return illnessTreatmentPQ17;
	}

	public String getIllnessTreatmentPQ1711() {
		return illnessTreatmentPQ1711;
	}

	public String getIllnessTreatmentPQ1712() {
		return illnessTreatmentPQ1712;
	}

	public String getIllnessTreatmentPQ1713() {
		return illnessTreatmentPQ1713;
	}

	public String getIllnessTreatmentPQ1714() {
		return illnessTreatmentPQ1714;
	}

	public String getIllnessTreatmentPQ18() {
		return illnessTreatmentPQ18;
	}

	public String getIllnessTreatmentPQ1811() {
		return illnessTreatmentPQ1811;
	}

	public String getIllnessTreatmentPQ1812() {
		return illnessTreatmentPQ1812;
	}

	public String getIllnessTreatmentPQ1813() {
		return illnessTreatmentPQ1813;
	}

	public String getIllnessTreatmentPQ1814() {
		return illnessTreatmentPQ1814;
	}

	public String getIllnessTreatmentPQ19() {
		return illnessTreatmentPQ19;
	}

	public String getIllnessTreatmentPQ1911() {
		return illnessTreatmentPQ1911;
	}

	public String getIllnessTreatmentPQ1912() {
		return illnessTreatmentPQ1912;
	}

	public String getIllnessTreatmentPQ1913() {
		return illnessTreatmentPQ1913;
	}

	public String getIllnessTreatmentPQ1914() {
		return illnessTreatmentPQ1914;
	}

	public String getIllnessTreatmentPQ20() {
		return illnessTreatmentPQ20;
	}

	public String getIllnessTreatmentPQ2011() {
		return illnessTreatmentPQ2011;
	}

	public String getIllnessTreatmentPQ21() {
		return illnessTreatmentPQ21;
	}

	public String getIllnessTreatmentPQ2111() {
		return illnessTreatmentPQ2111;
	}

	public String getIllnessTreatmentPQ2112() {
		return illnessTreatmentPQ2112;
	}

	public String getIllnessTreatmentPQ2113() {
		return illnessTreatmentPQ2113;
	}

	public String getIllnessTreatmentPQ2114() {
		return illnessTreatmentPQ2114;
	}
	
	public String getIllnessTreatmentPQ2121() {
		return illnessTreatmentPQ2121;
	}

	public String getIllnessTreatmentPQ22() {
		return illnessTreatmentPQ22;
	}

	public String getIllnessTreatmentPQ2211() {
		return illnessTreatmentPQ2211;
	}

	public String getIllnessTreatmentPQ2212() {
		return illnessTreatmentPQ2212;
	}

	public String getIllnessTreatmentPQ2213() {
		return illnessTreatmentPQ2213;
	}

	public String getIllnessTreatmentPQ2214() {
		return illnessTreatmentPQ2214;
	}

	public String getIllnessTreatmentPQ2215() {
		return illnessTreatmentPQ2215;
	}

	public String getIllnessTreatmentPQ23() {
		return illnessTreatmentPQ23;
	}

	public String getIllnessTreatmentPQ2311() {
		return illnessTreatmentPQ2311;
	}

	public String getIllnessTreatmentPQ2312() {
		return illnessTreatmentPQ2312;
	}

	public String getIllnessTreatmentPQ2313() {
		return illnessTreatmentPQ2313;
	}

	public String getIllnessTreatmentPQ2314() {
		return illnessTreatmentPQ2314;
	}

	public String getIllnessTreatmentPQ2315() {
		return illnessTreatmentPQ2315;
	}

	public String getIllnessTreatmentPQ24() {
		return illnessTreatmentPQ24;
	}

	public String getIllnessTreatmentPQ2411() {
		return illnessTreatmentPQ2411;
	}

	public String getIllnessTreatmentPQ2412() {
		return illnessTreatmentPQ2412;
	}

	public String getIllnessTreatmentPQ2413() {
		return illnessTreatmentPQ2413;
	}

	public String getIllnessTreatmentPQ2414() {
		return illnessTreatmentPQ2414;
	}

	public String getInsadditionalInfo() {
		return insadditionalInfo;
	}

	public String getPayadditionalInfo() {
		return payadditionalInfo;
	}

	public String getLifestyleQuest1() {
		return lifestyleQuest1;
	}

	public String getLifestyleQuest2() {
		String question2="";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.COMPANYNAME);
		if(!StringUtils.isEmpty(lifestyleQuest2)){
			question2 =  titleMap.get(lifestyleQuest2);
		}
		return question2;
	}

	public String getLifestyleQuest3() {
		String question3="";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.COMPANYNAME);
		if(!StringUtils.isEmpty(lifestyleQuest3)){
			question3 =  titleMap.get(lifestyleQuest3);
		}
		return question3;
	}

	public String getLifestyleQuest4() {
		if (!StringUtils.isEmpty(lifestyleQuest4)) {
			BigDecimal amt = new BigDecimal(lifestyleQuest4);
			lifestyleQuest4 = String.format("%,.2f",
					amt.setScale(2, RoundingMode.DOWN));
		}
		return lifestyleQuest4;
	}

	public String getLifestyleQuest5() {
		if (!lifestyleQuest5.isEmpty()) {
			BigDecimal amt = new BigDecimal(lifestyleQuest5);
			lifestyleQuest5 = String.format("%,.2f",
					amt.setScale(2, RoundingMode.DOWN));
		}
		return lifestyleQuest5;
	}

	public String getLifestyleQuest6() {
		if (!lifestyleQuest6.isEmpty()) {
			BigDecimal amt = new BigDecimal(lifestyleQuest6);
			lifestyleQuest6 = String.format("%,.2f",
					amt.setScale(2, RoundingMode.DOWN));
		}
		return lifestyleQuest6;
	}

	public String getLifestyleQuest7() {
		if (!lifestyleQuest7.isEmpty()) {
			BigDecimal amt = new BigDecimal(lifestyleQuest7);
			lifestyleQuest7 = String.format("%,.2f",
					amt.setScale(2, RoundingMode.DOWN));
		}
		return lifestyleQuest7;
	}

	public String getLifestyleQuest8() {
		return lifestyleQuest8;
	}

	public String getLifestyleQuest9() {
		String question9="";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.COMPANYNAME);
		if(!StringUtils.isEmpty(lifestyleQuest9)){
			question9 =  titleMap.get(lifestyleQuest9);
		}
		
		return question9;
	}

	public String getLifestyleQuest10() {
		return lifestyleQuest10;
	}

	public String getLifestyleQuest11() {
		return lifestyleQuest11;
	}

	public String getLifestyleQuest12() {
		return lifestyleQuest12;
	}

	public String getLifestyleQuest13() {
		return lifestyleQuest13;
	}

	public String getLifestyleQuest14() {
		return lifestyleQuest14;
	}

	public String getLifestyleQuest15() {
		return lifestyleQuest15;
	}

	public String getLifestyleQuest16() {
		return lifestyleQuest16;
	}

	public String getLifestyleQuest17() {
		return lifestyleQuest17;
	}

	public String getLifestyleQuest18() {
		return lifestyleQuest18;
	}

	public String getLifestyleQuest19() {
		return lifestyleQuest19;
	}

	public String getLifestyleQuest20() {
		return lifestyleQuest20;
	}

	public String getLifestyleQuest21() {
		return lifestyleQuest21;
	}

	public String getLifestyleQuest22() {
		return lifestyleQuest22;
	}

	public String getLifestyleQuest23() {
		return lifestyleQuest23;
	}

	public String getLifestyleQuest24() {
		return lifestyleQuest24;
	}

	public String getLifestyleQuest25() {
		return lifestyleQuest25;
	}

	public String getLifestyleQuest26() {
		return lifestyleQuest26;
	}

	public String getLifestyleQuest27() {
		return lifestyleQuest27;
	}

	public String getLifestyleQuest28() {
		return lifestyleQuest28;
	}

	public String getLifestyleQuest29() {
		return lifestyleQuest29;
	}

	public String getLifestyleQuest30() {
		return lifestyleQuest30;
	}

	public String getLifestyleQuest31() {
		return lifestyleQuest31;
	}

	public String getLifestyleQuest32() {
		return lifestyleQuest32;
	}

	public String getLifestyleQuest33() {
		return lifestyleQuest33;
	}

	public String getLifestyleQuest34() {
		return lifestyleQuest34;
	}

	public String getLifestyleQuest35() {
		return lifestyleQuest35;
	}

	public String getLifestyleQuest36() {
		return lifestyleQuest36;
	}

	public String getLifestyleQuest37() {
		return lifestyleQuest37;
	}

	public String getLifestyleQuest38() {
		return lifestyleQuest38;
	}

	public String getPayLifeStyle1() {
		return payLifeQuest1;
	}

	public String getPayLifeStyle2() {
		String question2="";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.COMPANYNAME);
		if(!StringUtils.isEmpty(payLifeQuest2)){
			question2 =  titleMap.get(payLifeQuest2);
		}
		return question2;
	}

	public String getPayLifeStyle3() {
		String question3="";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.COMPANYNAME);
		if(!StringUtils.isEmpty(payLifeQuest3)){
			question3 =  titleMap.get(payLifeQuest3);
		}
		return question3;
	}

	public String getPayLifeStyle4() {
		if (!payLifeQuest4.isEmpty()) {
			BigDecimal amt = new BigDecimal(payLifeQuest4);
			payLifeQuest4 = String.format("%,.2f",
					amt.setScale(2, RoundingMode.DOWN));
		}
		return payLifeQuest4;
	}

	public String getPayLifeStyle5() {
		if (!payLifeQuest5.isEmpty()) {
			BigDecimal amt = new BigDecimal(payLifeQuest5);
			payLifeQuest5 = String.format("%,.2f",
					amt.setScale(2, RoundingMode.DOWN));
		}
		return payLifeQuest5;
	}

	public String getPayLifeStyle6() {
		if (!payLifeQuest6.isEmpty()) {
			BigDecimal amt = new BigDecimal(payLifeQuest6);
			payLifeQuest6 = String.format("%,.2f",
					amt.setScale(2, RoundingMode.DOWN));
		}
		return payLifeQuest6;
	}

	public String getPayLifeStyle7() {
		if (!payLifeQuest7.isEmpty()) {
			BigDecimal amt = new BigDecimal(payLifeQuest7);
			payLifeQuest7 = String.format("%,.2f",
					amt.setScale(2, RoundingMode.DOWN));
		}
		return payLifeQuest7;
	}

	public String getPayLifeStyle8() {
		return payLifeQuest8;
	}

	public String getPayLifeStyle9() {

		String question9="";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.COMPANYNAME);
		if(!StringUtils.isEmpty(payLifeQuest9)){
			question9 =  titleMap.get(payLifeQuest9);
		}
		
		return question9;
	}

	public String getPayLifeStyle10() {
		return payLifeQuest10;
	}

	public String getPayLifeStyle11() {
		return payLifeQuest11;
	}

	public String getPayLifeStyle12() {
		return payLifeQuest12;
	}

	public String getPayLifeStyle13() {
		return payLifeQuest13;
	}

	public String getPayLifeStyle14() {
		return payLifeQuest14;
	}

	public String getPayLifeStyle15() {
		return payLifeQuest15;
	}

	public String getPayLifeStyle16() {
		return payLifeQuest16;
	}

	public String getPayLifeStyle17() {
		return payLifeQuest17;
	}

	public String getPayLifeStyle18() {
		return payLifeQuest18;
	}

	public String getPayLifeStyle19() {
		return payLifeQuest19;
	}

	public String getPayLifeStyle20() {
		return payLifeQuest20;
	}

	public String getPayLifeStyle21() {
		return payLifeQuest21;
	}

	public String getPayLifeStyle22() {
		return payLifeQuest22;
	}

	public String getPayLifeStyle23() {
		return payLifeQuest23;
	}

	public String getPayLifeStyle24() {
		return payLifeQuest24;
	}

	public String getPayLifeStyle25() {
		return payLifeQuest25;
	}

	public String getPayLifeStyle26() {
		return payLifeQuest26;
	}

	public String getPayLifeStyle27() {
		return payLifeQuest27;
	}

	public String getPayLifeStyle28() {
		return payLifeQuest28;
	}

	public String getPayLifeStyle29() {
		return payLifeQuest29;
	}

	public String getPayLifeStyle30() {
		return payLifeQuest30;
	}

	public String getPayLifeStyle31() {
		return payLifeQuest31;
	}

	public String getPayLifeStyle32() {
		return payLifeQuest32;
	}

	public String getPayLifeStyle33() {
		return payLifeQuest33;
	}

	public String getPayLifeStyle34() {
		return payLifeQuest34;
	}

	public String getPayLifeStyle35() {
		return payLifeQuest35;
	}

	public String getPayLifeStyle36() {
		return payLifeQuest36;
	}

	public String getPayLifeStyle37() {
		return payLifeQuest37;
	}

	public String getPayLifeStyle38() {
		return payLifeQuest38;
	}

	public String getIsInsuredSameAsPayer() {
		return isInsuredSameAsPayer;
	}

	public String getCreditCardHolderNameSplitOne() {
		String cardholdername="";
		if(!StringUtils.isEmpty(creditCardHolderNameSplitOne)){
		return creditCardHolderNameSplitOne;
		}
		return cardholdername;
	}

	public String getBankNameSplitOne() {
		String bankname="";
		if(!StringUtils.isEmpty(BankNameSplitOne)){
		return BankNameSplitOne;
		}
		return bankname;
	}

	public String getBranchNameSplitOne() {
		String branchname="";
		if(!StringUtils.isEmpty(BranchNameSplitOne)){
		return BranchNameSplitOne;
		}
		return branchname;
	}

	public String getCreditCardHolderNameSplitTwo() {
		String cardholdername="";
		if(!StringUtils.isEmpty(creditCardHolderNameSplitTwo)){
		return creditCardHolderNameSplitTwo;
		}
		return cardholdername;
	}

	public String getBankNameSplitTwo() {
		String bankname="";
		if(!StringUtils.isEmpty(BankNameSplitTwo)){
		return BankNameSplitTwo;
		}
		return bankname;
	}

	public String getBranchNameSplitTwo() {
		String branchname="";
		if(!StringUtils.isEmpty(BranchNameSplitTwo)){
		return BranchNameSplitTwo;
		}
		return branchname;
	}

	public String getCreditCardNumberSplitOne() {
		String creditCardNo="";
		if(!StringUtils.isEmpty(creditCardNumberSplitOne)){
		return creditCardNumberSplitOne;
		}
		return creditCardNo;
	}

	public String getCreditCardNumberSplitTwo() {
		String creditCardNo="";
		if(!StringUtils.isEmpty(creditCardNumberSplitTwo)){
		return creditCardNumberSplitTwo;
		}
		return creditCardNo;
	}

	/* Cash */
	public String getEtrNumberSplitOne() {
		String etrNo="";
		if(!StringUtils.isEmpty(etrNumberSplitOne)){
		return etrNumberSplitOne;
		}
		return etrNo;
	}
public String getEtrNumberSplitTwo() {
		String etrNo="";
		if(!StringUtils.isEmpty(etrNumberSplitTwo)){
		return etrNumberSplitTwo;

		}
		return etrNo;
	}

	public String getPayerRelationShipWithInsured() {
		return payer.getRelationWithInsured();
	}

	/* Renewal Payment */
	public String getRenewalCreditCardHolderName() {
		return renewalCreditCardHolderName;
	}

	public String getRenewalCreditCardBankName() {
		return renewalBankName;
	}

	public String getRenewalCreditCardBranchName() {
		return renewalBranchName;
	}

	public String getRenewalcreditCardNumber() {
		return renewalCreditCardNumber;
	}

	public String getRenewalCashETRNumber() {
		return renewalCashETRNumber;
	}

	public String getRenewalChequeETRNumber() {
		return renewalChequeETRNumber;
	}

	public String getProposerRenewalPaymentMethod() {
		return proposerPaymentMethodRenewalPayment;
	}

	public String getRenewalCreditCareExpiryDate() {
		return renewalCreditCareExpiryDate;
	}

	/* Refund Payment */

	public String getProposerRefundPaymentMethod() {
		return proposerRefundPaymentMethod;
	}

	public String getRefundBankName() {
		return refundBankName;
	}

	public String getRefundBranchName() {
		return refundBranchName;
	}

	public String getRefundCreditCardHolderName() {
		return refundCreditCardHolderName;
	}

	public String getRefundcreditCardNumber() {
		return refundcreditCardNumber;
	}

	public String getRefundCashETRNumber() {
		return refundCashETRNumber;
	}

	public String getRefundChequeETRNumber() {
		return refundChequeETRNumber;
	}

	public String getAgentSignDate() {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String agentDate = "";
		try {
			Date date = (Date) formatter.parse(agentSignDate);
			agentDate = sdf.format(date);
		} catch (Exception e) {
			LOGGER.error("getAgentSignDate");
		}
		return agentDate;
	}

	public String getFirstPrePolicyType() {
		return firstPrePolicyType;
	}

	public String getSecondPrePolicyType() {
		return secondPrePolicyType;
	}

	public String getThirdPrePolicyType() {
		return thirdPrePolicyType;
	}

	public String getFourthPrePolicyType() {
		return fourthPrePolicyType;
	}

	public String getSecondPrePolicyEffectiveDate() {
		String dateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if (secondPrePolicyEffectiveDate != null) {
				dateString = sdf.format(convertDateToThai(secondPrePolicyEffectiveDate));
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting secondPrePolicyEffectiveDate ", e);
		}
		return dateString;
	}

	public String getFirstPrePolicyEffectiveDate() {
		String dateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if (firstPrePolicyEffectiveDate != null) {
				dateString = sdf.format(convertDateToThai(firstPrePolicyEffectiveDate));
				}
			
		} catch (Exception e) {
			LOGGER.error("Error in getting firstPrePolicyEffectiveDate ", e);
		}
		return dateString;
	}

	public String getThirdPrePolicyEffectiveDate() {
		String dateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if (thirdPrePolicyEffectiveDate != null) {
				dateString = sdf.format(convertDateToThai(thirdPrePolicyEffectiveDate));
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting thirdPrePolicyEffectiveDate ", e);
		}
		return dateString;
	}

	public String getFourthPrePolicyEffectiveDate() {
		String dateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if (fourthPrePolicyEffectiveDate != null) {
				dateString = sdf.format(fourthPrePolicyEffectiveDate);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting fourthPrePolicyEffectiveDate ", e);
		}
		return dateString;
	}

	public String getAgentSignPlace() {
		return agentSignPlace;
	}

	public Date getSpajSignDate() {
		return spajSignDate;
	}

	public String getSpajSignPlace() {
		return SpajSignPlace;
	}

	public Boolean getIsPropDiffFromInsured() {
		return isPropDiffFromInsured;
	}

	public String getBenRelationInBahsa(String relation) {
		String rel = "";
		try {
			if (relation != null && !"".equals(relation)) {
				if (relation.equalsIgnoreCase("Husband")) {
					rel = "Suami";
				} else if (relation.equalsIgnoreCase("Son")) {
					rel = "Anak Laki Laki";
				} else if (relation.equalsIgnoreCase("Father")) {
					rel = "Ayah";
				} else if (relation.equalsIgnoreCase("Wife")) {
					rel = "Istri";
				} else if (relation.equalsIgnoreCase("Daughter")) {
					rel = "Anak Perempuan";
				} else if (relation.equalsIgnoreCase("Mother")) {
					rel = "Ibu";
				} else if (relation.equalsIgnoreCase("Siblings")) {
					rel = "Saudara Kandung";
				} else if (relation.equalsIgnoreCase("Others")) {
					rel = "Lainnya";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in getBenRelationInBahsa :" + e.getMessage());
		}
		return rel;
	}

	public String getBenGender(String gender) {
		String gen = "";
		if (gender != null) {
			if (gender.equalsIgnoreCase("Male")) {
				gen = "Pria";
			} else if (gender.equalsIgnoreCase("Female")) {
				gen = "Wanita";
			}
		}
		return gen;
	}

	public String populateAgentESignature() {
		String sign = null;
		String temptoken = null;
		Document documentDetail = null;
		LOGGER.info("appionteeDocuments ---- " + appionteeDocuments);
		// appointee signature detail
		if (appionteeDocuments != null) {
			try {
				for (Document agreementDocument : appionteeDocuments) {
					if (agreementDocument != null) {
						documentDetail = agreementDocument;
						if (("AgentSignature").equals(agreementDocument
								.getSignatureType())) {
							LOGGER.info("Type Code is Signature ---"
									+ documentDetail.getSignatureType());
							sign = documentDetail.getBase64string();
							StringTokenizer token = new StringTokenizer(sign,
									",");
							while (token.hasMoreTokens()) {
								temptoken = token.nextToken();
							}
							if (temptoken != null) {
								sign = temptoken;
							}
						}
					}

				}
			} catch (Exception e) {
				LOGGER.error("Error in getting  agent sign for spaj ");
			}
		}
		// System.out.println("Signature for PDF is " + sign);
		return sign;
	}

	public String populateInsuredSignature() {
		String sign = null;
		String temptoken = null;
		Document documentDetail = null;
		LOGGER.info("appionteeDocuments ---- " + appionteeDocuments);
		// appointee signature detail
		if (appionteeDocuments != null) {
			try {
				for (Document agreementDocument : appionteeDocuments) {
					if (agreementDocument != null) {
						documentDetail = agreementDocument;
						if (("MainInsuredSignature").equals(agreementDocument
								.getSignatureType())) {
							LOGGER.info("Type Code is Signature ---"
									+ documentDetail.getSignatureType());
							sign = documentDetail.getBase64string();
							StringTokenizer token = new StringTokenizer(sign,
									",");
							while (token.hasMoreTokens()) {
								temptoken = token.nextToken();
							}
							if (temptoken != null) {
								sign = temptoken;
							}
						}
					}

				}
			} catch (Exception e) {
				LOGGER.error("Error in getting  insured sign for eApp ");
			}
		}
		// System.out.println("Signature for PDF is " + sign);
		return sign;
	}

	public String populateProposerSignature() {
		String sign = null;
		String temptoken = null;
		Document documentDetail = null;
		LOGGER.info("appionteeDocuments ---- " + appionteeDocuments);
		// appointee signature detail
		if (appionteeDocuments != null) {
			try {
				for (Document agreementDocument : appionteeDocuments) {
					if (agreementDocument != null) {
						documentDetail = agreementDocument;
						if (("PolicyHolderSignature").equals(agreementDocument
								.getSignatureType())) {
							LOGGER.info("Type Code is Signature ---"
									+ documentDetail.getSignatureType());
							sign = documentDetail.getBase64string();
							StringTokenizer token = new StringTokenizer(sign,
									",");
							while (token.hasMoreTokens()) {
								temptoken = token.nextToken();
							}
							if (temptoken != null) {
								sign = temptoken;
							}
						}
					}

				}
			} catch (Exception e) {
				LOGGER.error("Error in getting  proposer sign for eApp ");
			}
		}
		// System.out.println("Signature for PDF is " + sign);
		return sign;
	}

	public String getInsuredFamilyMembersDiagWithDiseases() {
		return insuredFamilyMembersDiagWithDiseases;
	}

	public Integer getFirstFamilyMemAgeAtDeath() {
		return firstFamilyMemAgeAtDeath;
	}

	public Integer getFirstFamilyMemAge() {
		return firstFamilyMemAge;
	}

	public String getFirstFamilyMemCauseOfDeath() {
		return firstFamilyMemCauseOfDeath;
	}

	public String getFirstFamilyMemRelation() {
		String relationShip = "";
		if (firstFamilyMemRelation != null
				&& !"".equalsIgnoreCase(firstFamilyMemRelation)) {
			if (jsonObjectPdfInput.containsKey(firstFamilyMemRelation)) {
				relationShip = jsonObjectPdfInput.get(firstFamilyMemRelation)
						.toString();
			}
		}
		return relationShip;
	}

	public Integer getSecondFamilyMemAgeAtDeath() {
		return secondFamilyMemAgeAtDeath;
	}

	public Integer getSecondFamilyMemAge() {
		return secondFamilyMemAge;
	}

	public String getSecondFamilyMemCauseOfDeath() {
		return secondFamilyMemCauseOfDeath;
	}

	public String getSecondFamilyMemRelation() {
		String relationShip = "";
		if (secondFamilyMemRelation != null
				&& !"".equalsIgnoreCase(secondFamilyMemRelation)) {
			if (jsonObjectPdfInput.containsKey(secondFamilyMemRelation)) {
				relationShip = jsonObjectPdfInput.get(secondFamilyMemRelation)
						.toString();
			}
		}
		return relationShip;
	}

	public Integer getThirdFamilyMemAgeAtDeath() {
		return thirdFamilyMemAgeAtDeath;
	}

	public Integer getThirdFamilyMemAge() {
		return thirdFamilyMemAge;
	}

	public String getThirdFamilyMemCauseOfDeath() {
		return thirdFamilyMemCauseOfDeath;
	}

	public String getThirdFamilyMemRelation() {
		String relationShip = "";
		if (thirdFamilyMemRelation != null
				&& !"".equalsIgnoreCase(thirdFamilyMemRelation)) {
			if (jsonObjectPdfInput.containsKey(thirdFamilyMemRelation)) {
				relationShip = jsonObjectPdfInput.get(thirdFamilyMemRelation)
						.toString();
			}
		}
		return relationShip;
	}

	// Additional Insured One FamilyHistory
	public Integer getAddFirstFamilyMemAgeAtDeath() {
		return firstAddFirstFamilyMemAgeAtDeath;
	}

	public Integer getAddFirstFamilyMemAge() {
		return firstAddFirstFamilyMemAge;
	}

	public String getAddFirstFamilyMemCauseOfDeath() {
		return firstAddFirstFamilyMemCauseOfDeath;
	}

	public String getAddFirstFamilyMemRelation() {
		return firstAddFirstFamilyMemRelation;
	}

	public Integer getSecondAddFirstFamilyMemAgeAtDeath() {
		return secondAddFirstFamilyMemAgeAtDeath;
	}

	public Integer getSecondAddFirstFamilyMemAge() {
		return secondAddFirstFamilyMemAge;
	}

	public String getSecondAddFirstFamilyMemCauseOfDeath() {
		return secondAddFirstFamilyMemCauseOfDeath;
	}

	public String getSecondAddFirstFamilyMemRelation() {
		return secondAddFirstFamilyMemRelation;
	}

	public Integer getThirdAddFirstFamilyMemAgeAtDeath() {
		return thirdAddFirstFamilyMemAgeAtDeath;
	}

	public Integer getThirdAddFirstFamilyMemAge() {
		return thirdAddFirstFamilyMemAge;
	}

	public String getThirdAddFirstFamilyMemCauseOfDeath() {
		return thirdAddFirstFamilyMemCauseOfDeath;
	}

	public String getThirdAddFirstFamilyMemRelation() {
		return thirdAddFirstFamilyMemRelation;
	}

	// Additional Insured Two FamilyHistory
	public Integer getAddTwoFirstFamilyMemAgeAtDeath() {
		return firstAddSecondFamilyMemAgeAtDeath;
	}

	public Integer getAddTwoFirstFamilyMemAge() {
		return firstAddSecondFamilyMemAge;
	}

	public String getAddTwoFirstFamilyMemCauseOfDeath() {
		return firstAddSecondFamilyMemCauseOfDeath;
	}

	public String getAddTwoFirstFamilyMemRelation() {
		return firstAddSecondFamilyMemRelation;
	}

	public Integer getSecondAddTwoFirstFamilyMemAgeAtDeath() {
		return secondAddSecondFamilyMemAgeAtDeath;
	}

	public Integer getSecondAddTwoFirstFamilyMemAge() {
		return secondAddSecondFamilyMemAge;
	}

	public String getSecondAddTwoFirstFamilyMemCauseOfDeath() {
		return secondAddSecondFamilyMemCauseOfDeath;
	}

	public String getSecondAddTwoFirstFamilyMemRelation() {
		return secondAddSecondFamilyMemRelation;
	}

	public Integer getThirdAddTwoFirstFamilyMemAgeAtDeath() {
		return thirdAddSecondFamilyMemAgeAtDeath;
	}

	public Integer getThirdAddTwoFirstFamilyMemAge() {
		return thirdAddSecondFamilyMemAge;
	}

	public String getThirdAddTwoFirstFamilyMemCauseOfDeath() {
		return thirdAddSecondFamilyMemCauseOfDeath;
	}

	public String getThirdAddTwoFirstFamilyMemRelation() {
		return thirdAddSecondFamilyMemRelation;
	}

	// Additional Insured Three FamilyHistory
	public Integer getAddThreeFirstFamilyMemAgeAtDeath() {
		return firstAddThirdFamilyMemAgeAtDeath;
	}

	public Integer getAddThreeFirstFamilyMemAge() {
		return firstAddThirdFamilyMemAge;
	}

	public String getAddThreeFirstFamilyMemCauseOfDeath() {
		return firstAddThirdFamilyMemCauseOfDeath;
	}

	public String getAddThreeFirstFamilyMemRelation() {
		return firstAddThirdFamilyMemRelation;
	}

	public Integer getSecondAddThreeFirstFamilyMemAgeAtDeath() {
		return secondAddThirdFamilyMemAgeAtDeath;
	}

	public Integer getSecondAddThreeFirstFamilyMemAge() {
		return secondAddThirdFamilyMemAge;
	}

	public String getSecondAddThreeFirstFamilyMemCauseOfDeath() {
		return secondAddThirdFamilyMemCauseOfDeath;
	}

	public String getSecondAddThreeFirstFamilyMemRelation() {
		return secondAddThirdFamilyMemRelation;
	}

	public Integer getThirdAddThreeFirstFamilyMemAgeAtDeath() {
		return thirdAddThirdFamilyMemAgeAtDeath;
	}

	public Integer getThirdAddThreeFirstFamilyMemAge() {
		return thirdAddThirdFamilyMemAge;
	}

	public String getThirdAddThreeFirstFamilyMemCauseOfDeath() {
		return thirdAddThirdFamilyMemCauseOfDeath;
	}

	public String getThirdAddThreeFirstFamilyMemRelation() {
		return thirdAddThirdFamilyMemRelation;
	}

	// Additional Insured Four FamilyHistory
	public Integer getAddFourFirstFamilyMemAgeAtDeath() {
		return firstAddFourthFamilyMemAgeAtDeath;
	}

	public Integer getAddFourFirstFamilyMemAge() {
		return firstAddFourthFamilyMemAge;
	}

	public String getAddFourFirstFamilyMemCauseOfDeath() {
		return firstAddFourthFamilyMemCauseOfDeath;
	}

	public String getAddFourFirstFamilyMemRelation() {
		return firstAddFourthFamilyMemRelation;
	}

	public Integer getSecondAddFourFirstFamilyMemAgeAtDeath() {
		return secondAddFourthFamilyMemAgeAtDeath;
	}

	public Integer getSecondAddFourFirstFamilyMemAge() {
		return secondAddFourthFamilyMemAge;
	}

	public String getSecondAddFourFirstFamilyMemCauseOfDeath() {
		return secondAddFourthFamilyMemCauseOfDeath;
	}

	public String getSecondAddFourFirstFamilyMemRelation() {
		return secondAddFourthFamilyMemRelation;
	}

	public Integer getThirdAddFourFirstFamilyMemAgeAtDeath() {
		return thirdAddFourthFamilyMemAgeAtDeath;
	}

	public Integer getThirdAddFourFirstFamilyMemAge() {
		return thirdAddFourthFamilyMemAge;
	}

	public String getThirdAddFourFirstFamilyMemCauseOfDeath() {
		return thirdAddFourthFamilyMemCauseOfDeath;
	}

	public String getThirdAddFourFirstFamilyMemRelation() {
		return thirdAddFourthFamilyMemRelation;
	}

	public String getAgentName() {
		String agentName = null;
		try {
			if (agentPerson != null) {
				for (PersonName personName : agentPerson.getName()) {
					agentName = personName.getFullName();
				}
			}
		} catch (Exception e) {
			LOGGER.error("-- getAgentName -- " + e.getMessage());
		}
		return agentName;
	}

	public String getAgentPhoneNumber() {
		String agentNum = "";
		try {
			if (agent.getPreferredContactPreference() != null) {

			}

		} catch (Exception e) {
			LOGGER.error("-- getAgentPhoneNumber -- " + e.getMessage());
		}
		return agentNum;
	}

	public String getProductName() {
		return productName;
	}

	public String getProductSumAssured() {
		String sumAssured = "";
		if (productSumAssured != null) {
			sumAssured = String.format("%,.2f",
					productSumAssured.setScale(2, RoundingMode.DOWN));
		}
		return sumAssured;
	}

	public Integer getProductPolicyTerm() {
		return productPolicyTerm;
	}

	public String getProductPremiumAmount() {
		if (productPremiumAmount != null) {
			BigDecimal amt = new BigDecimal(productPremiumAmount);
			productPremiumAmount = String.format("%,.2f",
					amt.setScale(2, RoundingMode.DOWN));
		}
		return productPremiumAmount;
	}

	public Integer getProductPremiumTerm() {
		return productPremiumTerm;
	}

	public String getFirstRiderName() {
		String riderName = "";
		if (firstRiderName != null && !"".equalsIgnoreCase(firstRiderName)) {
			if (jsonObjectPdfInput.containsKey(firstRiderName)) {
				riderName = jsonObjectPdfInput.get(firstRiderName).toString();
			}
		}
		return riderName;
	}

	public String getSecondRiderName() {
		String riderName = "";
		if (secondRiderName != null && !"".equalsIgnoreCase(secondRiderName)) {
			if (jsonObjectPdfInput.containsKey(secondRiderName)) {
				riderName = jsonObjectPdfInput.get(secondRiderName).toString();
			}
		}
		return riderName;
	}

	public String getThirdRiderName() {
		String riderName = "";
		if (thirdRiderName != null && !"".equalsIgnoreCase(thirdRiderName)) {
			if (jsonObjectPdfInput.containsKey(thirdRiderName)) {
				riderName = jsonObjectPdfInput.get(thirdRiderName).toString();
			}
		}
		return riderName;
	}

	public String getFourthRiderName() {
		String riderName = "";
		if (fourthRiderName != null && !"".equalsIgnoreCase(fourthRiderName)) {
			if (jsonObjectPdfInput.containsKey(fourthRiderName)) {
				riderName = jsonObjectPdfInput.get(fourthRiderName).toString();
			}
		}
		return riderName;
	}

	public String getFifthRiderName() {
		String riderName = "";
		if (fifthRiderName != null && !"".equalsIgnoreCase(fifthRiderName)) {
			if (jsonObjectPdfInput.containsKey(fifthRiderName)) {
				riderName = jsonObjectPdfInput.get(fifthRiderName).toString();
			}
		}
		return riderName;
	}

	public String getSixthRiderName() {
		String riderName = "";
		if (sixthRiderName != null && !"".equalsIgnoreCase(sixthRiderName)) {
			if (jsonObjectPdfInput.containsKey(sixthRiderName)) {
				riderName = jsonObjectPdfInput.get(sixthRiderName).toString();
			}
		}
		return riderName;
	}

	public String getSeventhRiderName() {
		String riderName = "";
		if (seventhRiderName != null && !"".equalsIgnoreCase(seventhRiderName)) {
			if (jsonObjectPdfInput.containsKey(seventhRiderName)) {
				riderName = jsonObjectPdfInput.get(seventhRiderName).toString();
			}
		}
		return riderName;
	}

	public String getEightthRiderName() {
		String riderName = "";
		if (eightthRiderName != null && !"".equalsIgnoreCase(eightthRiderName)) {
			if (jsonObjectPdfInput.containsKey(eightthRiderName)) {
				riderName = jsonObjectPdfInput.get(eightthRiderName).toString();
			}
		}
		return riderName;
	}

	public String getNinthRiderName() {
		String riderName = "";
		if (ninthRiderName != null && !"".equalsIgnoreCase(ninthRiderName)) {
			if (jsonObjectPdfInput.containsKey(ninthRiderName)) {
				riderName = jsonObjectPdfInput.get(ninthRiderName).toString();
			}
		}
		return riderName;
	}

	public String getFirstRiderSumAssured() {
		String sumAssured = "";
		try {
			if (firstRiderSumAssured != null) {
				sumAssured = bigDecimalformatter(firstRiderSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("getFirstRiderSumAssured -- ");
		}
		return sumAssured;
	}

	public String getSecondRiderSumAssured() {
		String sumAssured = "";
		try {
			if (secondRiderSumAssured != null) {
				sumAssured = bigDecimalformatter(secondRiderSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("getSecondRiderSumAssured -- ");
		}
		return sumAssured;
	}

	public String getThirdRiderSumAssured() {
		String sumAssured = "";
		try {
			if (thirdRiderSumAssured != null) {
				sumAssured = bigDecimalformatter(thirdRiderSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("getThirdRiderSumAssured -- ");
		}
		return sumAssured;
	}

	public String getFourthRiderSumAssured() {
		String sumAssured = "";
		try {
			if (fourthRiderSumAssured != null) {
				sumAssured = bigDecimalformatter(fourthRiderSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("fourthRiderSumAssured -- ");
		}
		return sumAssured;
	}

	public String getFifthRiderSumAssured() {
		String sumAssured = "";
		try {
			if (fifthRiderSumAssured != null) {
				sumAssured = bigDecimalformatter(fifthRiderSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("getFifthRiderSumAssured -- ");
		}
		return sumAssured;
	}

	public String getSixthRiderSumAssured() {
		String sumAssured = "";
		try {
			if (sixthRiderSumAssured != null) {
				sumAssured = bigDecimalformatter(sixthRiderSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("getSixthRiderSumAssured -- ");
		}
		return sumAssured;
	}

	public String getSeventhRiderSumAssured() {
		String sumAssured = "";
		try {
			if (seventhRiderSumAssured != null) {
				sumAssured = bigDecimalformatter(seventhRiderSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("getSeventhRiderSumAssured -- ");
		}
		return sumAssured;
	}

	public String getEightthRiderSumAssured() {
		String sumAssured = "";
		try {
			if (eightthRiderSumAssured != null) {
				sumAssured = bigDecimalformatter(eightthRiderSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("eightthRiderSumAssured -- ");
		}
		return sumAssured;
	}

	public String getNinthRiderSumAssured() {
		String sumAssured = "";
		try {
			if (ninthRiderSumAssured != null) {
				sumAssured = bigDecimalformatter(ninthRiderSumAssured);
				if (ZERO.equalsIgnoreCase(sumAssured)) {
					sumAssured = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("ninthRiderSumAssured -- ");
		}
		return sumAssured;
	}

	public Integer getFirstRiderTerm() {
		if (ZERO.equalsIgnoreCase(firstRiderTerm.toString())) {
			firstRiderTerm = null;
		}
		return firstRiderTerm;
	}

	public Integer getSecondRiderTerm() {
		if (ZERO.equalsIgnoreCase(secondRiderTerm.toString())) {
			secondRiderTerm = null;
		}
		return secondRiderTerm;
	}

	public Integer getThirdRiderTerm() {
		if (ZERO.equalsIgnoreCase(thirdRiderTerm.toString())) {
			thirdRiderTerm = null;
		}
		return thirdRiderTerm;
	}

	public Integer getFourthRiderTerm() {
		if (ZERO.equalsIgnoreCase(fourthRiderTerm.toString())) {
			fourthRiderTerm = null;
		}
		return fourthRiderTerm;
	}

	public Integer getFifthRiderTerm() {
		if (ZERO.equalsIgnoreCase(fifthRiderTerm.toString())) {
			fifthRiderTerm = null;
		}
		return fifthRiderTerm;
	}

	public Integer getSixthRiderTerm() {
		if (ZERO.equalsIgnoreCase(sixthRiderTerm.toString())) {
			sixthRiderTerm = null;
		}
		return sixthRiderTerm;
	}

	public Integer getSeventhRiderTerm() {
		if (ZERO.equalsIgnoreCase(seventhRiderTerm.toString())) {
			seventhRiderTerm = null;
		}
		return seventhRiderTerm;
	}

	public Integer getEightthRiderTerm() {
		if (ZERO.equalsIgnoreCase(eightthRiderTerm.toString())) {
			eightthRiderTerm = null;
		}
		return eightthRiderTerm;
	}

	public Integer getNinthRiderTerm() {
		if (ZERO.equalsIgnoreCase(ninthRiderTerm.toString())) {
			ninthRiderTerm = null;
		}
		return ninthRiderTerm;
	}

	public String getProductPremiumFrequency() {
		String premiumFreq = "";
		try {
			if (productPremiumFrequency != null) {
				premiumFreq = productPremiumFrequency;
				if (productPremiumFrequency != null
						&& !"".equalsIgnoreCase(productPremiumFrequency)) {
					if (jsonObjectPdfInput.containsKey(productPremiumFrequency)) {
						premiumFreq = jsonObjectPdfInput.get(
								productPremiumFrequency).toString();
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error .. productPremiumFrequency " + e.getMessage());
		}
		return premiumFreq;
	}

	public String getFirstLifeAssuredName() {
		return firstLifeAssuredName;
	}

	public String getSecondLifeAssuredName() {
		return secondLifeAssuredName;
	}

	public String getThirdLifeAssuredName() {
		return thirdLifeAssuredName;
	}

	public String getFourthLifeAssuredName() {
		return fourthLifeAssuredName;
	}

	public String getFifthLifeAssuredName() {
		return fifthLifeAssuredName;
	}

	public String getSixthLifeAssuredName() {
		return sixthLifeAssuredName;
	}

	public String getSeventhLifeAssuredName() {
		return seventhLifeAssuredName;
	}

	public String getEighthLifeAssuredName() {
		return eightthLifeAssuredName;
	}

	public String getNinthLifeAssuredName() {
		return ninthLifeAssuredName;
	}

	public String readCheckMark() {
		byte[] bytes;
		byte[] encoded = null;
		InputStream imageInputStream = this.getClass().getResourceAsStream(
				"/pdfTemplates/Tick.jpg");
		try {
			bytes = IOUtils.toByteArray(imageInputStream);
			encoded = Base64.encodeBase64(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String(encoded);
	}

	public String getImageLocation() {
		return PropertyFileReader.fetchProperty(
				GeneraliConstants.PDF_IMAGE_LOC,
				GeneraliConstants.CONFIG_PROPERTY_FILE);
	}

	/**
	 * Encodes the byte array into base64 string
	 * 
	 * @param imageByteArray
	 *            - byte array
	 * @return String a {@link java.lang.String}
	 */
	public String encodeImage(String fileName) {
		File file = new File(fileName);
		byte[] bytes;
		byte[] encoded = null;
		try {
			bytes = FileUtils.loadFile(file);
			encoded = Base64.encodeBase64(bytes);
		} catch (IOException e) {
			LOGGER.error(" Error while encode image : " + e.getMessage());
		}
		return new String(encoded);
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public String getPaymentMethod() {
		String paymentMethod = "";
		if (paymentMethods != null && !"".equalsIgnoreCase(paymentMethods)) {
			if (jsonObjectPdfInput.containsKey(paymentMethods)) {
				paymentMethod = jsonObjectPdfInput.get(paymentMethods)
						.toString();
			}
		}
		return paymentMethod;
	}

	public String getInsuredIdentityType() {
		return insuredIdentityType;
	}

	public String getProposerIdentityProofType() {
		return proposerIdentityProofType;
	}

	public String getProposerIdentityNo() {
		return proposerIdentityNo;
	}

	public String getFirstAddInsuredTypeOfIdentity() {
		return firstAddInsuredTypeOfIdentity;
	}

	public String getSecondAddInsuredTypeOfIdentity() {
		return secondAddInsuredTypeOfIdentity;
	}

	public String getThirdAddInsuredTypeOfIdentity() {
		return thirdAddInsuredTypeOfIdentity;
	}

	public String getFourthAddInsuredTypeOfIdentity() {
		return fourthAddInsuredTypeOfIdentity;
	}

	public String getTotalInitialPremium() {
		String totalPremium = "";
		try {
			if (totalInitialPremium != null) {
				totalPremium = bigDecimalformatter(totalInitialPremium);
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting firstRiderInitialPremium "
					+ e.getMessage());
		}
		return totalPremium;
	}

	public String getFirstBeneficiaryTypeOfIdentity() {
		// String typeOfIdentity="";
		// String firstBeneTypeOfIdentity="";
		/*
		 * try{ typeOfIdentity = firstBeneficiaryTypeOfIdentity;
		 * firstBeneTypeOfIdentity =
		 * OmniContextBridge.services().getValueForCode(typeOfIdentity, "vt",
		 * "IDENTITY"); }catch(Exception e){
		 * LOGGER.error("Error .. getFirstBeneficiaryTypeOfIdentity "
		 * +e.getMessage()); }
		 */
		return firstBeneficiaryTypeOfIdentity;
	}

	public String getSecondBeneficiaryTypeOfIdentity() {
		/*
		 * String typeOfIdentity=""; String secondBeneTypeOfIdentity=""; try{
		 * typeOfIdentity = secondBeneficiaryTypeOfIdentity;
		 * secondBeneTypeOfIdentity =
		 * OmniContextBridge.services().getValueForCode(typeOfIdentity, "vt",
		 * "IDENTITY"); }catch(Exception e){
		 * LOGGER.error("Error .. getSecondBeneficiaryTypeOfIdentity "
		 * +e.getMessage()); }
		 */
		return secondBeneficiaryTypeOfIdentity;
	}

	public String getThirdBeneficiaryTypeOfIdentity() {
		return thirdBeneficiaryTypeOfIdentity;
	}

	public String getFourthBeneficiaryTypeOfIdentity() {
		return fourthBeneficiaryTypeOfIdentity;
	}

	public String getFirstRiderInitialPremium() {
		String riderPremium = "";
		try {
			if (firstRiderInitialPremium != null) {
				riderPremium = bigDecimalformatter(firstRiderInitialPremium);
				if (ZERO.equalsIgnoreCase(riderPremium)) {
					riderPremium = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting firstRiderInitialPremium "
					+ e.getMessage());
		}
		return riderPremium;
	}

	public String getSecondRiderInitialPremium() {
		String riderPremium = "";
		try {
			if (secondRiderInitialPremium != null) {
				riderPremium = bigDecimalformatter(secondRiderInitialPremium);
				if (ZERO.equalsIgnoreCase(riderPremium)) {
					riderPremium = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting secondRiderInitialPremium "
					+ e.getMessage());
		}
		return riderPremium;
	}

	public String getThirdRiderInitialPremium() {
		String riderPremium = "";
		try {
			if (thirdRiderInitialPremium != null) {
				riderPremium = bigDecimalformatter(thirdRiderInitialPremium);
				if (ZERO.equalsIgnoreCase(riderPremium)) {
					riderPremium = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting thirdRiderInitialPremium "
					+ e.getMessage());
		}
		return riderPremium;
	}

	public String getFourthRiderInitialPremium() {
		String riderPremium = "";
		try {
			if (fourthRiderInitialPremium != null) {
				riderPremium = bigDecimalformatter(fourthRiderInitialPremium);
				if (ZERO.equalsIgnoreCase(riderPremium)) {
					riderPremium = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting fourthRiderInitialPremium "
					+ e.getMessage());
		}
		return riderPremium;
	}

	public String getFifthRiderInitialPremium() {
		String riderPremium = "";
		try {
			if (fifthRiderInitialPremium != null) {
				riderPremium = bigDecimalformatter(fifthRiderInitialPremium);
				if (ZERO.equalsIgnoreCase(riderPremium)) {
					riderPremium = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting fifthRiderInitialPremium "
					+ e.getMessage());
		}
		return riderPremium;
	}

	public String getSixthRiderInitialPremium() {
		String riderPremium = "";
		try {
			if (sixthRiderInitialPremium != null) {
				riderPremium = bigDecimalformatter(sixthRiderInitialPremium);
				if (ZERO.equalsIgnoreCase(riderPremium)) {
					riderPremium = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting sixthRiderInitialPremium "
					+ e.getMessage());
		}
		return riderPremium;
	}

	public String getSeventhRiderInitialPremium() {
		String riderPremium = "";
		try {
			if (seventhRiderInitialPremium != null) {
				riderPremium = bigDecimalformatter(seventhRiderInitialPremium);
				if (ZERO.equalsIgnoreCase(riderPremium)) {
					riderPremium = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting seventhRiderInitialPremium "
					+ e.getMessage());
		}
		return riderPremium;
	}

	public String getEightthRiderInitialPremium() {
		String riderPremium = "";
		try {
			if (eightthRiderInitialPremium != null) {
				riderPremium = bigDecimalformatter(eightthRiderInitialPremium);
				if (ZERO.equalsIgnoreCase(riderPremium)) {
					riderPremium = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting eightthRiderInitialPremium "
					+ e.getMessage());
		}
		return riderPremium;
	}

	public String getNinthRiderInitialPremium() {
		String riderPremium = "";
		try {
			if (ninthRiderInitialPremium != null) {
				riderPremium = bigDecimalformatter(ninthRiderInitialPremium);
				if (ZERO.equalsIgnoreCase(riderPremium)) {
					riderPremium = "";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while getting ninthRiderInitialPremium "
					+ e.getMessage());
		}
		return riderPremium;
	}

	// -----------------------------------------------------------------------------------------------

	/**
	 * @param commisionRateOne
	 *            the commisionRateOne to set
	 */
	public void setCommisionRateOne(String commisionRateOne) {
		this.commisionRateOne = commisionRateOne;
	}

	/**
	 * @return the commisionRateOne
	 */
	public String getCommisionRateOne() {
		return commisionRateOne;
	}

	/**
	 * @param commisionRateTwo
	 *            the commisionRateTwo to set
	 */
	public void setCommisionRateTwo(String commisionRateTwo) {
		this.commisionRateTwo = commisionRateTwo;
	}

	/**
	 * @return the commisionRateTwo
	 */
	public String getCommisionRateTwo() {
		if (commisionRateTwo.equals("0")) {
			return null;
		} else {
			return commisionRateTwo;
		}
	}

	/**
	 * @param agentName2Code
	 *            the agentName2Code to set
	 */
	public void setPartnerCode(String commisionAgent) {
		this.commisionAgent = commisionAgent;
	}

	/**
	 * @return the agentName2Code
	 */
	public String getPartnerCode() {
		return commisionAgent;
	}

	/**
	 * @param commisionAgent
	 *            the commisionAgent to set
	 */
	public void setAgentNameTwo(String commisionAgent) {
		this.commisionAgent = commisionAgent;
	}

	/**
	 * @return the commisionAgent
	 */
	public String getAgentNameTwo() {
		String agentName = null;
		try {
			if (agentPerson != null) {
				for (PersonName personName : agentPerson.getName()) {
					agentName = personName.getGivenName();
				}
			}
		} catch (Exception e) {
			LOGGER.error("-- getAgentName -- " + e.getMessage());
		}
		return agentName;
	}

	/*
	 * public String getAgentEmail() { populateAgentCP(); return
	 * agentElectronicContact.getEmailAddress(); }
	 */

	/**
	 * populateESignature
	 * 
	 * @param signatureType
	 * @return
	 */
	public String populateESignature(String signatureType) {
		String sign = null;
		String temptoken = null;
		Document documentDetail = null;
		LOGGER.info("appionteeDocuments ---- " + appionteeDocuments);
		// appointee signature detail
		if (appionteeDocuments != null) {
			try {
				for (Document agreementDocument : appionteeDocuments) {
					if (agreementDocument != null) {
						documentDetail = agreementDocument;
						if (signatureType.equals(agreementDocument
								.getSignatureType())) {
							LOGGER.info("Type Code is Signature ---"
									+ documentDetail.getSignatureType());
							sign = documentDetail.getBase64string();
							StringTokenizer token = new StringTokenizer(sign,
									",");
							while (token.hasMoreTokens()) {
								temptoken = token.nextToken();
							}
							if (temptoken != null) {
								sign = temptoken;
							}
						}
					}

				}
			} catch (Exception e) {
				LOGGER.error("Error in getting sign for spaj " + spajNumber
						+ "tyep : " + signatureType);
			}
		}
		return sign;
	}

	public String dateValue() {
		String newDateString = "";
		try {
			Date newDate = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			newDateString = sdf.format(newDate);
		} catch (Exception e) {
			LOGGER.error("Error in getting the current date ", e);
		}
		return newDateString;
	}

	public String insuredDate() {
		String newDateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if (insuredDeclarationDateTime != null) {
				newDateString = sdf.format(insuredDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting insuredDeclarationDateTime ", e);
		}
		return newDateString;
	}

	public String insuredSignedTime() {
		String newDateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			if (insuredDeclarationDateTime != null) {
				newDateString = sdf.format(insuredDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting insuredDeclarationTime ", e);
		}
		return newDateString;
	}

	public String proposerDate() {
		String newDateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if (proposerDeclarationDateTime != null) {
				newDateString = sdf.format(proposerDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting proposerDeclarationDateTime ", e);
		}
		return newDateString;
	}

	public String proposerSignedTime() {
		String newDateString = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			if (proposerDeclarationDateTime != null) {
				newDateString = sdf.format(proposerDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting proposerDeclarationTime ", e);
		}
		return newDateString;
	}

	/**
	 * Adds comma to separate the number.
	 * 
	 * @return the number with comma as string
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String formatter(String number, String language) {
		double amount = 0;
		BigDecimal bd = new BigDecimal("0");
		String result = number;

		try {
			bd = new BigDecimal(number);
			amount = Double.parseDouble(number);
		} catch (NumberFormatException e) {
			LOGGER.error("Error in formatting value " + number, e);
		} finally {

			DecimalFormat formatter = (DecimalFormat) NumberFormat
					.getInstance(Locale.US);
			DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
			// if ("vn".equals(language)) {
			symbols.setGroupingSeparator('.');
			// } else {
			// symbols.setGroupingSeparator(',');
			// }
			formatter.setDecimalFormatSymbols(symbols);
			if (amount > 0) {
				result = formatter.format(bd);
			}
		}
		return result;
	}

	public String bigDecimalformatter(BigDecimal number) {
		String amount = "";
		try {
			DecimalFormat formatter = (DecimalFormat) NumberFormat
					.getInstance(Locale.US);
			DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
			symbols.setGroupingSeparator(',');
			formatter.setDecimalFormatSymbols(symbols);
			amount = (formatter.format(number.longValue()));

		} catch (Exception e) {
			LOGGER.error("Error in bigDecimalformatter " + number, e);
		}
		return amount;
	}

	public String createFullName(String firstName, String lastName) {
		String fullname = "";
		if (firstName != null) {
			StringBuffer sbFullname = new StringBuffer(firstName);
			try {
				if (lastName != null) {

					sbFullname = sbFullname.append(" ");
					sbFullname = sbFullname.append(lastName);

				}
			} catch (Exception e) {
				LOGGER.error("Error in concatinating full name, First Name: "
						+ firstName + "Last Name : " + lastName, e.getMessage());
			}
			fullname = sbFullname.toString();
		}
		return fullname;
	}

	/** get Person First Name **/
	public String getPersonFirstName(Set<PersonName> personNames) {
		String firstName = "";

		for (PersonName personName : personNames) {
			firstName = personName.getGivenName();
		}

		return firstName;

	}

	public String getPersonLastName(Set<PersonName> personNames) {
		String lastName = "";

		for (PersonName personName : personNames) {
			lastName = personName.getSurname();
		}

		return lastName;

	}

	public List<Coverage> riderNames(List<Coverage> riderList) {
		ArrayList<Coverage> riderListLA = new ArrayList<Coverage>();
		List<Coverage> a1 = new ArrayList<Coverage>();
		List<Coverage> a2 = new ArrayList<Coverage>();
		for (Coverage coverage : riderList) {
			// Added extra logic to handle sub riders of VITA Golden Health
			// VITA Golden health Standard(HIP1), Executive(HIP2) and VIP(HIP3)
			// have two added benefits,
			// Outpatient or Dental, which needs to be shown
			// just below in eApp PDF.
			if ("HIP1".equals(coverage.getTypeName())
					|| "HIP2".equals(coverage.getTypeName())
					|| "HIP3".equals(coverage.getTypeName())) {
				a1.add(0, coverage);
			} else if ("OutPatient".equals(coverage.getName())) {
				if (a1.size() > 0) {
					a1.add(1, coverage);
				} else {
					a1.add(0, coverage);
				}

			} else if ("Dental".equals(coverage.getName())) {

				if (a1.size() > 1) {
					a1.add(2, coverage);
				} else if (a1.size() > 0) {
					a1.add(1, coverage);
				} else {
					a1.add(0, coverage);
				}
			} else {
				a2.add(coverage);
			}
		}
		if (a1 != null && a1.size() > 0) {
			riderListLA.addAll(a1);
		}
		if (a2 != null && a2.size() > 0) {
			riderListLA.addAll(a2);
		}
		return riderListLA;
	}

	/*** Generali Thailand - Memo Template Changes */

	public String getInsuredSalutation() throws JRScriptletException {
		String insuredSalutation = "";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.TITLE);
		for (PersonName pName : person.getName()) {
			insuredSalutation = pName.getTitle();
		}
		return titleMap.get(insuredSalutation);
	}

	public String getPayerSalutation() throws JRScriptletException {
		String payerSalutation = "";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.TITLE);
		for (PersonName pName : payerPerson.getName()) {
			payerSalutation = pName.getTitle();
		}
		return titleMap.get(payerSalutation);
	}

	public String getPayerName() throws JRScriptletException {
		String fullname = "";
		for (PersonName pName : payerPerson.getName()) {
			fullname = pName.getGivenName().concat(" "+pName.getSurname());
		}
		return fullname;
	}

	public String getCustomerName() throws JRScriptletException {
		String fullname = "";
		for (PersonName pName : person.getName()) {
			fullname = pName.getFullName();
		}
		return fullname;
	}

	public PostalAddressContact getInsuredContactAddress()
			throws JRScriptletException {
		String prefContacttype = "";

		for (Extension ext : person.getIncludesExtension()) {
			if (ext.getName().equals("convenientContact")) {
				prefContacttype = ext.getValue();
			}
		}
		AddressNatureCodeList addressType = null;
		if (prefContacttype.equals("HouseholdRegistrationAddress")) {
			addressType = AddressNatureCodeList.Permanent;
		} else if (prefContacttype.equals("OfficeAddress")) {
			addressType = AddressNatureCodeList.Office;
		} else {
			addressType = AddressNatureCodeList.Current;
		}

		for (ContactPreference pName : person.getPreferredContact()) {
			ContactPoint test = pName.getPreferredContactPoint();

			if (test instanceof PostalAddressContact) {
				PostalAddressContact address = (PostalAddressContact) test;
				if (address.getAddressNatureCode().equals(addressType)) {
					return address;
				}
			}
		}
		return null;
	}

	public String getMooNumber() throws JRScriptletException {
		String mooNo = "";
		try {
			PostalAddressContact address = getInsuredContactAddress();
			if (address != null) {
				mooNo = address.getUnitNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting MooNumber" + spajNumber, e);
		}
		return mooNo;
	}

	public String getBuildingName() throws JRScriptletException {
		String buildingName = "";
		try {
			PostalAddressContact address = getInsuredContactAddress();
			if (address != null) {
				buildingName = address.getBuildingName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting BuildingName" + spajNumber, e);
		}
		return buildingName;
	}

	public String getHouseNumber() throws JRScriptletException {
		String houseNumber = "";
		try {
			PostalAddressContact address = getInsuredContactAddress();
			if (address != null) {
				houseNumber = address.getBoxNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting BoxNumber" + spajNumber, e);
		}
		return houseNumber;
	}

	public String getLane() throws JRScriptletException {
		String lane = "";
		try {
			PostalAddressContact address = getInsuredContactAddress();
			if (address != null) {
				lane = address.getStreetNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting StreetNumber" + spajNumber, e);
		}
		return lane;
	}

	public String getStreetName() throws JRScriptletException {
		String streetName = "";
		try {
			PostalAddressContact address = getInsuredContactAddress();
			if (address != null) {
				streetName = address.getStreetName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting StreetName" + spajNumber, e);
		}
		return streetName;
	}

	public String getWard() throws JRScriptletException {
		String ward = "";
		try {
			PostalAddressContact address = getInsuredContactAddress();
				ward = address.getAddressLine3();
				return ward;
			

		} catch (Exception e) {
			LOGGER.error("Error in getting Ward" + spajNumber, e);
		}
		return "";
	}

	public String getCityValue() throws JRScriptletException {
		String cityValue = "";
		try {
			PostalAddressContact address = getInsuredContactAddress();
			if (address != null) {
				cityValue = address.getAddressLine1();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting CityValue" + spajNumber, e);
		}
		return cityValue;
	}

	public String getState() throws JRScriptletException {
		String state = "";
		try {
			PostalAddressContact address = getInsuredContactAddress();
			if (address != null) {
				CountrySubdivision countrySubDiv = address
						.getPostalCountrySubdivision();
				state = countrySubDiv.getName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting CityValue" + spajNumber, e);
		}
		return state;
	}

	public String getPostCode() throws JRScriptletException {
		String postalCode = "";
		try {
			PostalAddressContact address = getInsuredContactAddress();
			if (address != null) {
				PostCode postCode = address.getPostalPostCode();
				ExternalCode code = postCode.getAssignedCode();
				postalCode = code.getCode();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting CityValue" + spajNumber, e);
		}
		return postalCode;
	}

	public String getGeneraliAgentName() throws JRScriptletException {
		String agentName = "";
		try {
			if(generaliAgent != null && generaliAgent.getAgentName() != null){
				agentName = generaliAgent.getAgentName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Agent Name" + spajNumber, e);
		}
		return agentName;
	}

	public String getGeneraliAgentPhone() throws JRScriptletException {
		String phoneNumber = "";
		try {
			phoneNumber = generaliAgent.getZmPhone();
		} catch (Exception e) {
			LOGGER.error("Error in getting Agent Contact Number" + spajNumber,
					e);
		}
		return phoneNumber;
	}

	public String getAgentSMName() throws JRScriptletException {
		String smName = "";
		try {
			smName = generaliAgent.getSmName();
		} catch (Exception e) {
			LOGGER.error("Error in getting Agent SM Name" + spajNumber, e);
		}
		return smName;
	}

	public String getGAOCode() throws JRScriptletException {
		String gaoCode = "";
		try {
			gaoCode = generaliAgent.getGaoCode();
		} catch (Exception e) {
			LOGGER.error("Error in getting GAO Code" + spajNumber, e);
		}
		return gaoCode;
	}

	public String getTeamCode() throws JRScriptletException {
		String teamCode = "";
		try {
			teamCode = generaliAgent.getTeamEng();
		} catch (Exception e) {
			LOGGER.error("Error in getting Team Code" + spajNumber, e);
		}
		return teamCode;
	}

	public List<AppianMemoDetails> getMemoDescription()
			throws JRScriptletException {
		List<AppianMemoDetails> memoDetails = new ArrayList<AppianMemoDetails>();
		try {
			for (AppianMemoDetails test : appianMemoDetails) {
				AppianMemoDetails appMemoDetails = new AppianMemoDetails();
				if (test.getMemoStatus().equals(GeneraliConstants.OPEN)
						|| test.getMemoStatus().equals(
								GeneraliConstants.PENDING_UW)) {
					appMemoDetails.setMemoDesc(test.getMemoDetails().getMemoDescription());
					appMemoDetails.setRemarks(test.getRemarks());
					memoDetails.add(appMemoDetails);
				}
				
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Memo Description" + spajNumber, e);
		}
		return memoDetails;
	}

	public String getHeight() throws JRScriptletException {
		String height = "";
		try {
			height = (person.getHeight().getValue()).toString();
		} catch (Exception e) {
			LOGGER.error("Error in getting Height" + spajNumber, e);
		}
		return height;
	}

	public String getWeight() throws JRScriptletException {
		String weight = "";
		try {
			weight = (person.getWeight().getValue()).toString();
		} catch (Exception e) {
			LOGGER.error("Error in getting Height" + spajNumber, e);
		}
		return weight;
	}

	public String getAge() throws JRScriptletException {
		String age = "";
		try {
			age = (person.getAge()).toString();
		} catch (Exception e) {
			LOGGER.error("Error in getting Age" + spajNumber, e);
		}
		return age;
	}

	public String dateOfBirth() throws JRScriptletException {
		String dob = "";
		try {
			dob = (person.getBirthDate()).toString();
		} catch (Exception e) {
			LOGGER.error("Error in getting DOB" + spajNumber, e);
		}
		return dob;
	}

	public String nationalId() throws JRScriptletException {
		String nationalId = "";
		try {
			nationalId = (person.getNationalId());
		} catch (Exception e) {
			LOGGER.error("Error in getting NationalID" + spajNumber, e);
		}
		return nationalId;
	}

	public String spouseName() throws JRScriptletException {
		String spouseName = "";

		try {
			spouseName = (person.getSpouseOrParentName().getFullName());
		} catch (Exception e) {
			LOGGER.error("Error in getting SpouseName" + spajNumber, e);
		}
		return spouseName;

	}

	public String maritalStatus() throws JRScriptletException {

		String martialStatus = "";

		try {
			martialStatus = (person.getMaritalStatusCode()).toString();
		} catch (Exception e) {
			LOGGER.error("Error in getting MaritalStatus" + spajNumber, e);
		}
		return martialStatus;
	}

	/* Current Address Details */

	public String getCurrentMooNumber() throws JRScriptletException {
		String currentMooNo = "";
		try {

			if (postalAddressContact != null) {
				currentMooNo = postalAddressContact.getUnitNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting CurrentMooNumber" + spajNumber, e);
		}
		return currentMooNo;
	}

	public String getCurrentBuildingName() throws JRScriptletException {
		String CurrentBuildingName = "";
		try {

			if (postalAddressContact != null) {
				CurrentBuildingName = postalAddressContact.getBuildingName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting CurrentBuildingName" + spajNumber, e);
		}
		return CurrentBuildingName;
	}

	public String getCurrentHouseNumber() throws JRScriptletException {
		String currentHouseNumber = "";
		try {

			if (postalAddressContact != null) {
				currentHouseNumber = postalAddressContact.getBoxNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting currentHouseNumber" + spajNumber, e);
		}
		return currentHouseNumber;
	}

	public String getCurrentLane() throws JRScriptletException {
		String currentLane = "";
		try {

			if (postalAddressContact != null) {
				currentLane = postalAddressContact.getStreetNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting currentLane" + spajNumber, e);
		}
		return currentLane;
	}

	public String getCurrentStreetName() throws JRScriptletException {
		String currentStreetName = "";
		try {

			if (postalAddressContact != null) {
				currentStreetName = postalAddressContact.getStreetName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting currentStreetName" + spajNumber, e);
		}
		return currentStreetName;
	}

	public String getCurrentWard() throws JRScriptletException {
		String currentWard = "";
		try {
			if (postalAddressContact != null) {
				currentWard = postalAddressContact.getAddressLine3();
				return currentWard;	
		}
		}catch (Exception e) {
			LOGGER.error("Error in getting currentWard" + spajNumber, e);
		}
		return "";
	}

	public String getCurrentCityValue() throws JRScriptletException {
		String currentCityValue = "";
		try {

			if (postalAddressContact != null) {
				currentCityValue = postalAddressContact.getAddressLine1();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting currentCityValue" + spajNumber, e);
		}
		return currentCityValue;
	}

	public String getCurrentState() throws JRScriptletException {
		String currentState = "";
		try {

			if (postalAddressContact != null) {
				CountrySubdivision countrySubDiv = postalAddressContact
						.getPostalCountrySubdivision();
				currentState = countrySubDiv.getName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting currentState" + spajNumber, e);
		}
		return currentState;
	}

	public String getCurrentPostCode() throws JRScriptletException {
		String currentPostalCode = "";
		try {

			if (postalAddressContact != null) {
				PostCode postCode = postalAddressContact.getPostalPostCode();
				ExternalCode code = postCode.getAssignedCode();
				currentPostalCode = code.getCode();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting currentPostalCode" + spajNumber, e);
		}
		return currentPostalCode;
	}

	/* Permanent Address Details */

	public String getPermanentMooNumber() throws JRScriptletException {
		String permanentMooNo = "";
		try {

			if (permanentAddress != null) {
				permanentMooNo = permanentAddress.getUnitNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PermanentMooNumber" + spajNumber, e);
		}
		return permanentMooNo;
	}

	public String getPermanentBuildingName() throws JRScriptletException {
		String permanentBuildingName = "";
		try {

			if (permanentAddress != null) {
				permanentBuildingName = permanentAddress.getBuildingName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting permanentBuildingName" + spajNumber,
					e);
		}
		return permanentBuildingName;
	}

	public String getPermanentHouseNumber() throws JRScriptletException {
		String permanentHouseNumber = "";
		try {

			if (permanentAddress != null) {
				permanentHouseNumber = permanentAddress.getBoxNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting permanentHouseNumber" + spajNumber,
					e);
		}
		return permanentHouseNumber;
	}

	public String getPermanentLane() throws JRScriptletException {
		String permanentLane = "";
		try {

			if (permanentAddress != null) {
				permanentLane = permanentAddress.getStreetNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting permanentLane" + spajNumber, e);
		}
		return permanentLane;
	}

	public String getPermanentStreetName() throws JRScriptletException {
		String permanentStreetName = "";
		try {

			if (permanentAddress != null) {
				permanentStreetName = permanentAddress.getStreetName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting permanentStreetName" + spajNumber, e);
		}
		return permanentStreetName;
	}

	public String getPermanentWard() throws JRScriptletException {
		String permanentWard = "";
		try {
			if (permanentAddress != null) {
				permanentWard = permanentAddress.getAddressLine3();
				return permanentWard;
		}
		}catch (Exception e) {
			LOGGER.error("Error in getting currentWard" + spajNumber, e);
		}
		return "";
	}

	public String getPermanentCityValue() throws JRScriptletException {
		String permanentCityValue = "";
		try {

			if (permanentAddress != null) {
				permanentCityValue = permanentAddress.getAddressLine1();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting permanentCityValue" + spajNumber, e);
		}
		return permanentCityValue;
	}

	public String getPermanentState() throws JRScriptletException {
		String permanentState = "";
		try {

			if (permanentAddress != null) {
				CountrySubdivision countrySubDiv = permanentAddress
						.getPostalCountrySubdivision();
				permanentState = countrySubDiv.getName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting permanentState" + spajNumber, e);
		}
		return permanentState;
	}

	public String getPermanentPostCode() throws JRScriptletException {
		String permanentPostalCode = "";
		try {

			if (permanentAddress != null) {
				PostCode postCode = permanentAddress.getPostalPostCode();
				ExternalCode code = postCode.getAssignedCode();
				permanentPostalCode = code.getCode();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting permanentPostalCode" + spajNumber, e);
		}
		return permanentPostalCode;
	}

	/* Office Address Details */

	public String getOfficeMooNumber() throws JRScriptletException {
		String officeMooNo = "";
		try {

			if (officeAddress != null) {
				officeMooNo = officeAddress.getUnitNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting officeMooNo" + spajNumber, e);
		}
		return officeMooNo;
	}

	public String getOfficeBuildingName() throws JRScriptletException {
		String officeBuildingName = "";
		try {

			if (officeAddress != null) {
				officeBuildingName = officeAddress.getBuildingName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting officeBuildingName" + spajNumber, e);
		}
		return officeBuildingName;
	}

	public String getOfficeHouseNumber() throws JRScriptletException {
		String officeHouseNumber = "";
		try {

			if (officeAddress != null) {
				officeHouseNumber = officeAddress.getBoxNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting officeHouseNumber" + spajNumber, e);
		}
		return officeHouseNumber;
	}

	public String getOfficeLane() throws JRScriptletException {
		String officeLane = "";
		try {

			if (officeAddress != null) {
				officeLane = officeAddress.getStreetNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting officeLane" + spajNumber, e);
		}
		return officeLane;
	}

	public String getOfficeStreetName() throws JRScriptletException {
		String officeStreetName = "";
		try {

			if (officeAddress != null) {
				officeStreetName = officeAddress.getStreetName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting officeStreetName" + spajNumber, e);
		}
		return officeStreetName;
	}

	public String getOfficeWard() throws JRScriptletException {
		String officeWard = "";
		try {
			if (officeAddress != null) {
				officeWard = officeAddress.getAddressLine3();
				return officeWard;
		}
		}catch (Exception e) {
			LOGGER.error("Error in getting officeWard" + spajNumber, e);
		}
		return "";	
	}

	public String getOfficeCityValue() throws JRScriptletException {
		String officeCityValue = "";
		try {

			if (officeAddress != null) {
				officeCityValue = officeAddress.getAddressLine1();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting officeCityValue" + spajNumber, e);
		}
		return officeCityValue;
	}

	public String getOfficeState() throws JRScriptletException {
		String officeState = "";
		try {

			if (officeAddress != null) {
				CountrySubdivision countrySubDiv = officeAddress
						.getPostalCountrySubdivision();
				officeState = countrySubDiv.getName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting officeState" + spajNumber, e);
		}
		return officeState;
	}

	public String getOfficePostCode() throws JRScriptletException {
		String officePostalCode = "";
		try {

			if (officeAddress != null) {
				PostCode postCode = officeAddress.getPostalPostCode();
				ExternalCode code = postCode.getAssignedCode();
				officePostalCode = code.getCode();

			}
		} catch (Exception e) {
			LOGGER.error("Error in getting officePostalCode" + spajNumber, e);
		}
		return officePostalCode;
	}

	/* Person Details */

	public String getNationality() throws JRScriptletException {
		Set<Country> nationalities;
		String nationality = "";
		try {

			if (person != null) {
				nationalities = person.getNationalityCountry();
				for (Country s : nationalities) {
					nationality = s.getName();
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting officePostalCode" + spajNumber, e);
		}
		return nationality;
	}

	public String checkForValue(JSONObject obj, String key) {
		String value = "";
		if (obj.containsKey(key)) {
			Object jsonValue = obj.get(key);
			if (jsonValue != null) {
				value = jsonValue.toString();
			}
		}
		return value;
	}

	public JSONObject getValue(JSONObject obj, String key) {
		JSONObject value = null;
		if (obj.containsKey(key)) {
			Object jsonValue = obj.get(key);
			value = (JSONObject) jsonValue;
		}
		return value;
	}

	public String getInsuredName() throws JRScriptletException {

		String insuredName = "";
		try {
			if (jsonObject != null) {
				if (jsonObject.containsKey("InsuredName")) {
					insuredName = checkForValue(jsonObject, "InsuredName");
				}

			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Sum Insured" + spajNumber, e);
		}
		return insuredName;
	}

	public String getTotalPremium() throws JRScriptletException {

		String totalPremium = "";
		try {
			if (jsonObject != null) {
				if (jsonObject.containsKey("Details")) {
					totalPremium = checkForValue(
							getValue(jsonObject, "Details"), "TotalPremium");
					if (!totalPremium.isEmpty()) {
						BigDecimal amt = new BigDecimal(totalPremium);
						totalPremium = String.format("%,.0f",
								amt.setScale(2, RoundingMode.DOWN));
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Total Premium" + spajNumber, e);
		}
		return totalPremium;

	}

	public String getReceivedPremium() throws JRScriptletException {

		String receivedPremium = "";
		try {
			if (jsonObject != null) {
				if (jsonObject.containsKey("ReceivedPremium")) {
					receivedPremium = checkForValue(jsonObject,"ReceivedPremium");
					if (!receivedPremium.isEmpty()) {
						BigDecimal amt = new BigDecimal(receivedPremium);
						receivedPremium = String.format("%,.0f",
								amt.setScale(2, RoundingMode.DOWN));
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting getReceivedPremium" + spajNumber, e);
		}
		return receivedPremium;

	}

	public String getValidDateToResponse() throws JRScriptletException {

		String validDateToResponse = "";
		String dateInThai = "";
		try {
			if (jsonObject != null) {
				if (jsonObject.containsKey("ValidDateToResponse")) {
					validDateToResponse = checkForValue(jsonObject,"ValidDateToResponse");
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					//Date validResponseDate = df.parse(validDateToResponse);
					Calendar c = Calendar.getInstance();
				    c.setTime(df.parse(validDateToResponse));  
					c.add(Calendar.YEAR, 543);
					dateInThai = GeneraliComponentRepositoryHelper
							.buildDateInThai(c.getTime());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting getValidDateToResponse");
		}
		return dateInThai;

	}

	public String getPremiumToCollect() throws JRScriptletException {

		String premiumToCollect = "";
		try {
			if (jsonObject != null) {
				if (jsonObject.containsKey("PremiumToCollect")) {
					premiumToCollect = checkForValue(jsonObject,"PremiumToCollect");
					if (!premiumToCollect.isEmpty()) {
						BigDecimal amt = new BigDecimal(premiumToCollect);
						premiumToCollect = String.format("%,.0f",
								amt.setScale(2, RoundingMode.DOWN));
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Total Premium" + spajNumber, e);
		}
		return premiumToCollect;

	}

	public String getCountOfRiders() throws JRScriptletException {

		String countofRiders = "";
		try {
			if (jsonObject != null) {
				if (jsonObject.containsKey("Details")) {
					countofRiders = checkForValue(
							getValue(jsonObject, "Details"), "CountOfRiders");
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Total Premium" + spajNumber, e);
		}
		return countofRiders;

	}

	public String getGender() throws JRScriptletException {
		String gender = "";
		try {
			if (person != null && person.getGenderCode()!=null) {
				gender = person.getGenderCode().name();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting gender" + spajNumber, e);
		}
		return gender;
	}

	public String getMaritalStatus() throws JRScriptletException {
		String maritalStatus = "";
		try {
			if (person != null && person.getMaritalStatusCode()!=null) {
				maritalStatus = person.getMaritalStatusCode().name();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting MaritalStatus" + spajNumber, e);
		}
		return maritalStatus;
	}

	/* Insured First Name */
	public String getInsuredFirstName() throws JRScriptletException {
		String firstName = "";
		for (PersonName pName : person.getName()) {
			firstName = pName.getGivenName();
		}
		return firstName;
	}

	/* Insured Last Name */
	public String getInsuredLastName() throws JRScriptletException {
		String lastName = "";
		for (PersonName pName : person.getName()) {
			lastName = pName.getMiddleName();
		}
		return lastName;
	}

	/* Insured Sur Name */
	public String getInsuredSurName() throws JRScriptletException {
		String surName = "";
		for (PersonName pName : person.getName()) {
			surName = pName.getSurname();
		}
		return surName;
	}

	/* Rider Details */

	public String getHSRiderPremium() {
		return riderHsextraInitialPremium;
	}

	public String getHSRiderSumAssured() {
		return riderHsextraSumAssured;
	}

	public String getHBRiderPremium() {
		return riderHbextraInitialPremium;
	}

	public String getHBRiderSumAssured() {
		return riderHbextraSumAssured;
	}

	public String getCIRiderPremium() {
		return riderCIextraInitialPremium;
	}

	public String getCIRiderSumAssured() {
		return riderCIextraSumAssured;
	}

	public String getADDRiderPremium() {
		return riderAddextraInitialPremium;
	}

	public String getADDRiderSumAssured() {
		return riderAddextraSumAssured;
	}

	public String getADBRiderPremium() {
		return riderADBRiderInitialPremium;
	}

	public String getADBRiderSumAssured() {
		return riderADBRiderSumAssured;
	}

	public String getAIRiderPremium() {
		return riderAIInitialPremium;
	}

	public String getAIRiderSumAssured() {
		return riderAISumAssured;
	}

	public String getRCCADBRiderPremium() {
		return riderRCCADBInitialPremium;
	}

	public String getRCCADBRiderSumAssured() {
		return riderRCCADBSumAssured;
	}

	public String getWPRiderPremium() {
		return riderWPRiderInitialPremium;
	}

	public String getWPRiderSumAssured() {
		return riderWPRiderSumAssured;
	}

	public String getDDRiderSumAssured() {
		return riderDDRiderSumAssured;
	}

	public String getDDRiderPremium() {
		return riderDDRiderInitialPremium;
	}

	public String getRCCAIRiderSumAssured() {
		return riderRCCAIRiderSumAssured;
	}

	public String getRCCAIRiderPremium() {
		return riderRCCAIRiderInitialPremium;
	}

	public String getRCCADDRRiderSumAssured() {
		return riderRCCADDRiderRiderSumAssured;
	}

	public String getRCCDDRiderPremium() {
		return riderRCCADDRiderRiderInitialPremium;
	}

	public String getPBRiderSumAssured() {
		return riderPBRiderSumAssured;
	}

	public String getPBRiderPremium() {
		return riderPBRiderInitialPremium;
	}

	public String getModeofPayment() {
		return productPremiumFrequency;
	}

	/* Spouse Name or Parent Name */

	public String getSpouseOrParentOldName() {
		String spouseName="";
		if(person!=null && person.getSpouseOrParentName()!=null)
		{
		spouseName = person.getSpouseOrParentName().getMiddleName();
		}
		return spouseName;
	}

	public String getSpouseOrParentLastName() {
		String spouseSurName="";
		if(person!=null && person.getSpouseOrParentName()!=null)
		{
		spouseSurName = person.getSpouseOrParentName().getSurname();
		}
		return spouseSurName;
	}

	/* Convenient Contact Address */
	public String getConvenientContactAddress() {
		String prefContacttype = "";
		for (Extension ext : person.getIncludesExtension()) {
			if (ext.getName().equals("convenientContact")) {
				prefContacttype = ext.getValue();
			}
		}
		return prefContacttype;
	}

	/* IdentificationExpiryDate */
	public Date getInsuredIdentityExpireDate() {
		Date insuredExpiryDate = convertDateToThai(insuredIdentityExpireDate);
		return insuredExpiryDate;
	}

	/* IdenticationProofValue */
	public String getInsuredIdentityProofValue() {
		String IdentityProof = "";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.IDENTITYTYPE);
		if (person != null) {
			IdentityProof = person.getNationalIdType();
		}
		return titleMap.get(IdentityProof);
	}

	/* Payer Personal Details */

	public String getPayerHeight() throws JRScriptletException {
		String payerHeight = "";
		try {
			if(payerPerson!=null && payerPerson.getHeight()!=null){
			payerHeight = (payerPerson.getHeight().getValue()).toString();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PayerHeight" + spajNumber, e);
		}
		return payerHeight;
	}

	public String getPayerWeight() throws JRScriptletException {
		String payerWeight = "";
		try {
			if(payerPerson!=null && payerPerson.getHeight()!=null){
			payerWeight = (payerPerson.getWeight().getValue()).toString();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Payerweight" + spajNumber, e);
		}
		return payerWeight;
	}

	public String getPayerAge() throws JRScriptletException {
		String payerAge = "";
		try {
			if(payerPerson!=null){
			payerAge = (payerPerson.getAge()).toString();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Age" + spajNumber, e);
		}
		return payerAge;
	}

	public String getPayerYearOfBirth() {
		Date insDobValue = getInsuredDob();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(insDobValue);
		String payerYearOB = Integer.toString(calendar.get(Calendar.YEAR));
		return payerYearOB;
	}

	public String getPayerMonthOfBirth() {
		Date insDobValue = getInsuredDob();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(insDobValue);
		String payerMonthOB = Integer
				.toString(calendar.get(Calendar.MONTH) + 1);
		return payerMonthOB;
	}

	public String getPayerDayOfBirth() {
		Date insDobValue = getInsuredDob();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(insDobValue);
		String payerDayOB = Integer.toString(calendar
				.get(Calendar.DAY_OF_MONTH));
		return payerDayOB;
	}
	
	public String getPayerDateofBirth(){
		Date insDobValue = getPayerDob();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String date = sdf.format(insDobValue);
		return date;
	}
	

	public String payerNationalId() throws JRScriptletException {
		String payerNationalId = "";
		try {
			payerNationalId = (payerPerson.getNationalId());
		} catch (Exception e) {
			LOGGER.error("Error in getting NationalID" + spajNumber, e);
		}
		return payerNationalId;
	}

	public String payerSpouseName() throws JRScriptletException {
		String payerSpouseName = "";
		try {
			payerSpouseName = (payerPerson.getSpouseOrParentName()
					.getFullName());
		} catch (Exception e) {
			LOGGER.error("Error in getting SpouseName" + spajNumber, e);
		}
		return payerSpouseName;

	}

	public String payerMaritalStatus() throws JRScriptletException {
		String payerMartialStatus = "";
		try {
			if(payerPerson!=null && payerPerson.getMaritalStatusCode()!=null){
			payerMartialStatus = (payerPerson.getMaritalStatusCode())
					.toString();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting MaritalStatus" + spajNumber, e);
		}
		return payerMartialStatus;
	}

	public String getPayerNationality() throws JRScriptletException {
		String nationality = "";
		String newP = "";
		try {
			for (Country country : payerCountries) {
				nationality = country.getName();
			}
			newP = OmniContextBridge.services().getValueForCode(nationality,
					"th", "NATIONALITY");
		} catch (Exception e) {
			LOGGER.error("-- getInsuredNationality -- " + e.getMessage());
		}
		return newP;

	}

	public String getPayerGender() throws JRScriptletException {
		String payerGender = "";
		try {
			if (payerPerson != null) {
				payerGender = payerPerson.getGenderCode().name();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting gender" + spajNumber, e);
		}
		return payerGender;
	}

	/* Payer Current Address */

	public String getPayerCurrentMooNumber() throws JRScriptletException {
		String payerCurrentMooNo = "";
		try {
			if (payerPostalAddressContact != null) {

				payerCurrentMooNo = payerPostalAddressContact.getUnitNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting CurrentMooNumber" + spajNumber, e);
		}
		return payerCurrentMooNo;
	}

	public String getPayerCurrentBuildingName() throws JRScriptletException {
		String payerCurrentbuildingName = "";
		try {
			if (payerPostalAddressContact != null) {
				payerCurrentbuildingName = payerPostalAddressContact
						.getBuildingName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting BuildingName" + spajNumber, e);
		}
		return payerCurrentbuildingName;
	}

	public String getPayerCurrentHouseNumber() throws JRScriptletException {
		String payerCurrenthouseNumber = "";
		try {
			if (payerPostalAddressContact != null) {
				payerCurrenthouseNumber = payerPostalAddressContact
						.getBoxNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting BoxNumber" + spajNumber, e);
		}
		return payerCurrenthouseNumber;
	}

	public String getPayerCurrentLane() throws JRScriptletException {
		String payerCurrentlane = "";
		try {
			if (payerPostalAddressContact != null) {
				payerCurrentlane = payerPostalAddressContact.getStreetNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting StreetNumber" + spajNumber, e);
		}
		return payerCurrentlane;
	}

	public String getPayerCurrentStreetName() throws JRScriptletException {
		String payerCurrentstreetName = "";
		try {
			if (payerPostalAddressContact != null) {
				payerCurrentstreetName = payerPostalAddressContact
						.getStreetName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting StreetName" + spajNumber, e);
		}
		return payerCurrentstreetName;
	}

	public String getPayerCurrentWard() throws JRScriptletException {
		String payerCurrentWard = "";
		try {
			if (payerPostalAddressContact != null) {
				payerCurrentWard = payerPostalAddressContact.getAddressLine3();
				return payerCurrentWard;
		}
		}catch (Exception e) {
			LOGGER.error("Error in getting PayerCurrentWard" + spajNumber, e);
		}
		return "";	
	}

	public String getPayerCurrentCityValue() throws JRScriptletException {
		String payerCurrentcityValue = "";
		try {
			if (payerPostalAddressContact != null) {
				payerCurrentcityValue = payerPostalAddressContact
						.getAddressLine1();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting CityValue" + spajNumber, e);
		}
		return payerCurrentcityValue;
	}

	public String getPayerCurrentState() throws JRScriptletException {
		String payerCurrentstate = "";
		try {
			if (payerPostalAddressContact != null) {
				CountrySubdivision pcountrySubDiv = payerPostalAddressContact
						.getPostalCountrySubdivision();
				payerCurrentstate = pcountrySubDiv.getName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting CityValue" + spajNumber, e);
		}
		return payerCurrentstate;
	}

	public String getPayerCurrentPostCode() throws JRScriptletException {
		String payerCurrentpostalCode = "";
		try {

			if (payerPostalAddressContact != null) {
				PostCode ppostCode = payerPostalAddressContact
						.getPostalPostCode();
				ExternalCode code = ppostCode.getAssignedCode();
				payerCurrentpostalCode = code.getCode();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting currentPostcode" + spajNumber, e);
		}
		return payerCurrentpostalCode;
	}

	/* Payer Permanent Address */

	public String getPayerPermanentMooNumber() throws JRScriptletException {
		String payerPermanentcurrentMooNo = "";
		try {

			if (payerPermanentAddress != null) {
				payerPermanentcurrentMooNo = payerPermanentAddress
						.getUnitNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PermanentMooNumber" + spajNumber, e);
		}
		return payerPermanentcurrentMooNo;
	}

	public String getPayerPermanentBuildingName() throws JRScriptletException {
		String payerPermanentbuildingName = "";
		try {
			if (payerPermanentAddress != null) {
				payerPermanentbuildingName = payerPermanentAddress
						.getBuildingName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PermanentBuildingName" + spajNumber, e);
		}
		return payerPermanentbuildingName;
	}

	public String getPayerPermanentHouseNumber() throws JRScriptletException {
		String payerPermanenthouseNumber = "";
		try {
			if (payerPermanentAddress != null) {
				payerPermanenthouseNumber = payerPermanentAddress
						.getBoxNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PermanentHouseNumber" + spajNumber, e);
		}
		return payerPermanenthouseNumber;
	}

	public String getPayerPermanentLane() throws JRScriptletException {
		String payerPermanentlane = "";
		try {
			if (payerPermanentAddress != null) {
				payerPermanentlane = payerPermanentAddress.getStreetNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PermanentStreetNumber" + spajNumber, e);
		}
		return payerPermanentlane;
	}

	public String getPayerPermanentStreetName() throws JRScriptletException {
		String payerPermanentstreetName = "";
		try {
			if (payerPermanentAddress != null) {
				payerPermanentstreetName = payerPermanentAddress
						.getStreetName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PermanentStreetName" + spajNumber, e);
		}
		return payerPermanentstreetName;
	}

	public String getPayerPermanentWard() throws JRScriptletException {
		String payerPermanentward = "";
		try {
			if (payerPermanentAddress != null) {
				payerPermanentward = payerPermanentAddress.getAddressLine3();	
				return payerPermanentward;
		}
		}catch (Exception e) {
			LOGGER.error("Error in getting PayerPermanentWard" + spajNumber, e);
		}
		return "";		
	}

	public String getPayerPermanentCityValue() throws JRScriptletException {
		String payerPermanentcityValue = "";
		try {
			if (payerPermanentAddress != null) {
				payerPermanentcityValue = payerPermanentAddress
						.getAddressLine1();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting CityValue" + spajNumber, e);
		}
		return payerPermanentcityValue;
	}

	public String getPayerPermanentState() throws JRScriptletException {
		String payerState = "";
		try {
			if (payerPermanentAddress != null) {
				CountrySubdivision pcountrySubDiv = payerPermanentAddress
						.getPostalCountrySubdivision();
				payerState = pcountrySubDiv.getName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting permanentState" + spajNumber, e);
		}
		return payerState;
	}

	public String getPayerPermanentPostCode() throws JRScriptletException {
		String payerPostalCode = "";
		try {

			if (payerPermanentAddress != null) {
				PostCode ppostCode = payerPermanentAddress.getPostalPostCode();
				ExternalCode code = ppostCode.getAssignedCode();
				payerPostalCode = code.getCode();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Postcode" + spajNumber, e);
		}
		return payerPostalCode;
	}

	/* Payer Office Address */

	public String getPayerOfficeMooNumber() throws JRScriptletException {
		String payerCurrentMooNo = "";
		try {

			if (payerOfficeAddress != null) {
				payerCurrentMooNo = payerOfficeAddress.getUnitNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting OfficeMooNumber" + spajNumber, e);
		}
		return payerCurrentMooNo;
	}

	public String getPayerOfficeBuildingName() throws JRScriptletException {
		String payerBuildingName = "";
		try {
			if (payerOfficeAddress != null) {
				payerBuildingName = payerOfficeAddress.getBuildingName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting OfficeBuildingName" + spajNumber, e);
		}
		return payerBuildingName;
	}

	public String getPayerOfficeHouseNumber() throws JRScriptletException {
		String payerHouseNumber = "";
		try {
			if (payerOfficeAddress != null) {
				payerHouseNumber = payerOfficeAddress.getBoxNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting OfficeBoxNumber" + spajNumber, e);
		}
		return payerHouseNumber;
	}

	public String getPayerOfficeLane() throws JRScriptletException {
		String payerLane = "";
		try {
			if (payerOfficeAddress != null) {
				payerLane = payerOfficeAddress.getStreetNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting OfficeStreetNumber" + spajNumber, e);
		}
		return payerLane;
	}

	public String getPayerOfficeStreetName() throws JRScriptletException {
		String payerStreetName = "";
		try {
			if (payerOfficeAddress != null) {
				payerStreetName = payerOfficeAddress.getStreetName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting OfficeStreetName" + spajNumber, e);
		}
		return payerStreetName;
	}

	public String getPayerOfficeWard() throws JRScriptletException {
		String payerWard = "";
		try {
			if (payerOfficeAddress != null) {
				payerWard = payerOfficeAddress.getAddressLine3();
				return payerWard;
		}
		}catch (Exception e) {
			LOGGER.error("Error in getting PayerOfficeWard" + spajNumber, e);
		}
		return "";	
	}

	public String getPayerOfficeCityValue() throws JRScriptletException {
		String payerCityValue = "";
		try {
			if (payerOfficeAddress != null) {
				payerCityValue = payerOfficeAddress.getAddressLine1();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PayerCityValue" + spajNumber, e);
		}
		return payerCityValue;
	}

	public String getPayerOfficeState() throws JRScriptletException {
		String payerState = "";
		try {
			if (payerOfficeAddress != null) {
				CountrySubdivision pcountrySubDiv = payerOfficeAddress
						.getPostalCountrySubdivision();
				payerState = pcountrySubDiv.getName();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PayerState" + spajNumber, e);
		}
		return payerState;
	}

	public String getPayerOfficePostCode() throws JRScriptletException {
		String payerPostalCode = "";
		try {
			if (payerOfficeAddress != null) {
				PostCode ppostCode = payerOfficeAddress.getPostalPostCode();
				ExternalCode code = ppostCode.getAssignedCode();
				payerPostalCode = code.getCode();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Payerofficepostcode" + spajNumber, e);
		}
		return payerPostalCode;
	}

	public String getPIdentityNo() {
		return payerIdentityNo;
	}

	public String getPayerNatureOfWork() throws JRScriptletException {
		return payerOccupationDetail.getNatureofWork();
	}

	public String getPayerAnnualIncome() {
		String payerAnnualIncome = "";
		if (payerIncomeDetail.getGrossIncome() != null) {
			try {
				payerAnnualIncome = formatter(
						payerIncomeDetail.getGrossIncome(), "vt");
			} catch (Exception e) {
				LOGGER.error("getInsuredAnnualIncome -- " + e.getMessage());
			}
		}
		return payerAnnualIncome;
	}

	public String getPayerSpouseOrParentLastName() {
		String payerspouseName = StringUtils.EMPTY;
		try {
			if (payerPerson != null && payerPerson.getSpouseOrParentName()!= null) {
				payerspouseName = payerPerson.getSpouseOrParentName()
						.getSurname() != null ? payerPerson.getSpouseOrParentName()
								.getSurname() : StringUtils.EMPTY ;
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PayerSpouseLastname" + spajNumber, e);
		}
		return payerspouseName;
	}

	public String getPayerSpouseOrParentOldName() {
		String payerspouseSurName="";
		try {
			if (payerPerson != null && payerPerson.getSpouseOrParentName()!=null) {
				payerspouseSurName = payerPerson.getSpouseOrParentName()
						.getMiddleName() != null ? payerPerson.getSpouseOrParentName()
								.getMiddleName() : StringUtils.EMPTY ;
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting PayerSpouseOldname" + spajNumber, e);
		}
		return payerspouseSurName;
	}

	/* Convenient Contact Address */
	public String getPayerConvenientContactAddress() {
		for (Extension ext : payerPerson.getIncludesExtension()) {
			if (ext.getName().equals("convenientContact")) {
				payerprefContacttype = ext.getValue();
			}
		}
		return payerprefContacttype;
	}

	/* IdentificationExpiryDate */
	public Date getDateConversion(Date d) {
		Date payerDobInThai = convertDateToThai(payerDob);
		return payerDobInThai;
	}

	public Date getPayerDob() {
		Date payerDobInThai = convertDateToThai(payerDob);
		return payerDobInThai;
	}

	public Date getPayerIdentityExpireDate() {
		Date payerExpiryDate = convertDateToThai(payerIdentityExpireDate);
		return payerExpiryDate;
	}

	/* IdenticationProofValue */
	public String getPayerIdentityProofValue() {
		String payerIdentityProof = payerPerson.getNationalIDTypeValue();
		return payerIdentityProof;
	}

	/* occupation Position */

	public String getPayerPosition() {
		String payerPosition = null;
		for (EmploymentRelationship employmentRelationship : payerOccupationDetail
				.getProvidingEmployment())
			payerPosition = employmentRelationship.getOccupationTypeCode();
		return payerPosition;
	}

	/* Insured Job */

	public String getPayerJob() {
		String payerjob = "";
		payerjob = payerOccupationDetail.getJob();
		return payerjob;
	}

	/* Payer Annual Income */
	public String getPayerAnnualIncomes() {
		String payerannualIncome = "";
		payerannualIncome = payerOccupationDetail.getJobValue();
		return payerannualIncome;
	}

	/* Payer First Name */
	public String getPayerFirstName() throws JRScriptletException {
		String payerfirstName = "";
		for (PersonName pName : payerPerson.getName()) {
			payerfirstName = pName.getGivenName();
		}
		return payerfirstName;
	}

	/* Payer Last Name */
	public String getPayerLastName() throws JRScriptletException {
		String payerLastName = "";
		for (PersonName pName : payerPerson.getName()) {
			payerLastName = pName.getMiddleName();
		}
		return payerLastName;
	}

	/* Payer Sur Name */
	public String getPayerSurName() throws JRScriptletException {
		String payerSurName = "";
		for (PersonName pName : payerPerson.getName()) {
			payerSurName = pName.getSurname();
		}
		return payerSurName;
	}

	public String getPayerEmail() {
		return payerElectronicContact.getEmailAddress();
	}

	public String getPayerMobileNumber() {
		return payerMobileNumber1;
	}

	public String getPayerOfficeNumber() {
		return payerOfficeNumber;
	}

	public String getPayerHomeNumber() {
		return payerHomeNumber1;
	}

	public String getProductPremiumPeriod() {
		return productPremiumPeriod;
	}

	public String getProductInitialPremium() {
		return String.format("%,.2f",
				(initialPremium).setScale(2, RoundingMode.DOWN));
	}

	public List<RiderDetailsTableData> getCounterOfferDetails()
			throws JRScriptletException {
		JSONObject detailsObj;
		JSONArray jsonArray = null;
		List<RiderDetailsTableData> cofDetails = new ArrayList<RiderDetailsTableData>();
		List<RiderDetailsTableData> cofDetailsArranged = new ArrayList<RiderDetailsTableData>();
		//detailsObj = (JSONObject) jsonObject.get("Details");
		jsonArray = (JSONArray) jsonObject.get("RiderDetails");
		
		String riderCode = "";
		String sumInsured = "";
		String standardPremium = "";
		String additionalPremium = "";
		String premiumPeriod = "";
		String totalPremiumOfRider = "";
		String totalPremium = "";
		String riderNameThai = "";
		ArrayList<String> riderSequence = setRiderSequence();

		try {
			for (Object obj : jsonArray) {
				JSONObject jsonObj = (JSONObject) obj;
				RiderDetailsTableData appMemoDetails = new RiderDetailsTableData();
				
				if (jsonObj != null) {
					if (jsonObj.containsKey("RiderCode")) {
						riderCode = checkForValue(jsonObj, "RiderCode");
						/*if (!riderCode.equals("GenProLife")
								&& !riderCode.equals("GenProLife25")) {*/
						
						/*} else {
							riderNameThai = riderCode;
						
						}*/
						if (riderNameThai != null) {
							appMemoDetails.setRiderCode(riderCode);
						}
					}
					if (jsonObj.containsKey("SumInsured")) {
						sumInsured = checkForValue(jsonObj, "SumInsured");
						if (!sumInsured.isEmpty()) {
							BigDecimal amt = new BigDecimal(sumInsured);
							sumInsured = String.format("%,.0f",
									amt.setScale(2, RoundingMode.DOWN));
						}
						appMemoDetails.setSumInsured(sumInsured);
					}
					if (jsonObj.containsKey("StandardPremium")) {
						standardPremium = checkForValue(jsonObj,
								"StandardPremium");
						if (!standardPremium.isEmpty()) {
							BigDecimal amt = new BigDecimal(standardPremium);
							standardPremium = String.format("%,.0f",
									amt.setScale(2, RoundingMode.DOWN));
						}
						appMemoDetails.setStandardPremium(standardPremium);
					}
					if (jsonObj.containsKey("AdditionalPremium")) {
						additionalPremium = checkForValue(jsonObj,
								"AdditionalPremium");
						if (!additionalPremium.isEmpty()) {
							BigDecimal amt = new BigDecimal(additionalPremium);
							additionalPremium = String.format("%,.0f",
									amt.setScale(2, RoundingMode.DOWN));
						}
						appMemoDetails.setAdditionalPremium(additionalPremium);
					}
					if (jsonObj.containsKey("PremiumPeriod")) {
						premiumPeriod = checkForValue(jsonObj, "PremiumPeriod");
						appMemoDetails.setPremiumPeriod(premiumPeriod);
					}
					if (jsonObj.containsKey("TotalPremiumOfRider")) {
						totalPremiumOfRider = checkForValue(jsonObj,
								"TotalPremiumOfRider");
						if (!totalPremiumOfRider.isEmpty()) {
							BigDecimal amt = new BigDecimal(totalPremiumOfRider);
							totalPremiumOfRider = String.format("%,.0f",
									amt.setScale(2, RoundingMode.DOWN));
						}
						appMemoDetails
								.setTotalPremiumOfRider(totalPremiumOfRider);
					}

					totalPremium = checkForValue(jsonObject,"TotalPremiumOfBasicAndRider");
					if (!totalPremium.isEmpty()) {
						BigDecimal amt = new BigDecimal(totalPremium);
						totalPremium = String.format("%,.0f",
								amt.setScale(2, RoundingMode.DOWN));
					}
					appMemoDetails.setTotalPremium(totalPremium);
					cofDetails.add(appMemoDetails);
					
				}
			}
			//Adding Base Plan
			if(cofDetails != null){
				if(cofDetails.get(0) != null){
					riderNameThai = getRiderNameInThai(cofDetails.get(0).getRiderCode());
					cofDetails.get(0).setRiderCode(riderNameThai);
					cofDetailsArranged.add(cofDetails.get(0));
				}
				for(String sequence : riderSequence){
					for(RiderDetailsTableData riderDetails : cofDetails){
						if(sequence.equalsIgnoreCase(riderDetails.getRiderCode()))
						{
							if(!StringUtils.isEmpty(riderDetails.getRiderCode())){
								riderNameThai = getRiderNameInThai(riderDetails.getRiderCode());
								riderDetails.setRiderCode(riderNameThai);
							}
							cofDetailsArranged.add(riderDetails);
						}
						
					}
				}
				
			
			}
			
			
			
		} catch (Exception e) {
			LOGGER.error("getPolicyValueData .... " + e.getMessage() + " ");
		}
		return cofDetailsArranged;
	}

	private ArrayList<String> setRiderSequence() {
		ArrayList<String> riderSequence = new ArrayList<String>();
		riderSequence.add("9440");
		riderSequence.add("9431");
		riderSequence.add("9910");
		riderSequence.add("9911");
		riderSequence.add("9920");
		riderSequence.add("9921");
		riderSequence.add("9930");
		riderSequence.add("9931");
		riderSequence.add("9941");
		riderSequence.add("9413");
		riderSequence.add("9922");
		riderSequence.add("9954");
		riderSequence.add("9942");
		riderSequence.add("9953");
		riderSequence.add("9414"); 

		return riderSequence;
	}

	public List<ExclusionsTableData> getExclusionsDescription()
			throws JRScriptletException {

		List<ExclusionsTableData> exclusionsListDetails = new ArrayList<ExclusionsTableData>();
		JSONArray jsonArray = null;
		JSONObject detailsObj;
		JSONArray jsonArrayForExcl = null;
		String exclusion = "";
		String countofRiders = "";

		//detailsObj = (JSONObject) jsonObject.get("Details");
		jsonArray = (JSONArray) jsonObject1.get("RiderDetails");

		if (jsonObject1 != null) {
			if (jsonObject1.containsKey("CountOfRiders")) {
				countofRiders = checkForValue(jsonObject1,"CountOfRiders");
			}
		}
		try {
			for (Object obj : jsonArray) {
                int index = jsonArray.indexOf(obj);
                JSONObject jsonObj = (JSONObject) obj;
                if (jsonObj != null) {
                       jsonArrayForExcl = (JSONArray) jsonObj.get("ExclusionDesc");
                       for (Object exclObj : jsonArrayForExcl) {
                              JSONObject jsonObj1 = (JSONObject) exclObj;
                              if(!StringUtils.isEmpty(checkForValue(jsonObj1, "Desc"))){
                              ExclusionsTableData exclusions = new ExclusionsTableData();
                              if (jsonObj1.containsKey("Desc")) {
                                     exclusion = checkForValue(jsonObj1, "Desc");
                                     int count = index + 1;
                                     int flag=0;
                                     for(int j=0;j<exclusionsListDetails.size();j++)
                                     {
                                            if(exclusion.trim().equalsIgnoreCase(exclusionsListDetails.get(j).getDescription().trim()))
                                            {  
                                            	flag=1;
                                                   String jIndex= exclusionsListDetails.get(j).getIndex();
                                                   if(jIndex.contains("และ")){
                                                   jIndex = jIndex.replace("และ",",");
                                                   }
                                               String appendIndex=jIndex+" และ "+Integer.toString(count);
                                                   exclusionsListDetails.get(j).setIndex(appendIndex);
                                            }
                                     }
                                     if(flag==0)
                                     {
                                     exclusions.setDescription(exclusion);
                                     exclusions.setIndex(Integer.toString(count));
                                     exclusions.setCountOfRiders(countofRiders);                     
                                     exclusionsListDetails.add(exclusions);
                                     }
                              }
                             
                              }
                       }
                }
          }



		} catch (Exception e) {
			LOGGER.error("getPolicyValueData .... " + e.getMessage() + " ");
		}
		return exclusionsListDetails;
	}

	public String getInsuredPrimaryJob() {
		String insuredPrimaryJobCode = "";
		Map<String, Map<String, String>> codeValuesMapForOccupation = codeValuesMap;
		Map<String, String> mapForOccupationValues = codeValuesMapForOccupation
				.get(GeneraliConstants.NATUREOFWORK);
		if (occupationDetail.size() > 0) {
			OccupationDetail primaryOccupationDetail = occupationDetail.get(0);
			insuredPrimaryJobCode = mapForOccupationValues
					.get(primaryOccupationDetail.getJob());
		} else {
			insuredPrimaryJobCode = "";
		}
		return insuredPrimaryJobCode;
	}

	public String getInsuredSecondaryJob() {
		String insuredSecondaryJobCode = "";
		Map<String, Map<String, String>> codeValuesMapForOccupation = codeValuesMap;
		Map<String, String> mapForOccupationValues = codeValuesMapForOccupation
				.get(GeneraliConstants.NATUREOFWORK);
		if (occupationDetail.size() > 1) {
			OccupationDetail secondaryOccupationDetail = occupationDetail
					.get(1);
			insuredSecondaryJobCode = mapForOccupationValues
					.get(secondaryOccupationDetail.getJob());
		} else {
			insuredSecondaryJobCode = "";
		}
		return insuredSecondaryJobCode;
	}

	public String getPayerPrimaryJob() {
		String payerPrimaryJobCode = "";
		Map<String, Map<String, String>> codeValuesMapForOccupation = codeValuesMap;
		Map<String, String> mapForOccupationValues = codeValuesMapForOccupation
				.get(GeneraliConstants.NATUREOFWORK);
		if (payerOccupationDetails.size() > 0) {
			OccupationDetail primaryOccupationDetail = payerOccupationDetails
					.get(0);
			payerPrimaryJobCode = mapForOccupationValues
					.get(primaryOccupationDetail.getJob());
		} else {
			payerPrimaryJobCode = "";
		}
		return payerPrimaryJobCode;
	}

	public String getPayerSecondaryJob() {
		String payerSecondaryJobCode = "";
		Map<String, Map<String, String>> codeValuesMapForOccupation = codeValuesMap;
		Map<String, String> mapForOccupationValues = codeValuesMapForOccupation
				.get(GeneraliConstants.NATUREOFWORK);
		if (payerOccupationDetails.size() > 1) {
			OccupationDetail secondaryOccupationDetail = payerOccupationDetails
					.get(1);
			payerSecondaryJobCode = mapForOccupationValues
					.get(secondaryOccupationDetail.getJob());
		} else {
			payerSecondaryJobCode = "";
		}
		return payerSecondaryJobCode;
	}

	public String getInsuredPrimaryAnnualIncome() {
		String insuredPrimaryAnnualIncome = "";
		if (occupationDetail.size() > 0) {
			OccupationDetail primaryOccupationDetail = occupationDetail.get(0);
			insuredPrimaryAnnualIncome = primaryOccupationDetail.getJobValue();
			if (insuredPrimaryAnnualIncome != null) {
				BigDecimal amt = new BigDecimal(insuredPrimaryAnnualIncome);
				insuredPrimaryAnnualIncome = String.format("%,.2f",
						amt.setScale(2, RoundingMode.DOWN));
			}
		} else {
			insuredPrimaryAnnualIncome = "";
		}
		return insuredPrimaryAnnualIncome;
	}

	public String getInsuredSecondaryAnnualIncome() {
		String insuredSecondaryAnnualIncome = "";
		if (occupationDetail.size() > 1) {
			OccupationDetail primaryOccupationDetail = occupationDetail.get(1);
			insuredSecondaryAnnualIncome = primaryOccupationDetail
					.getJobValue();
			if (insuredSecondaryAnnualIncome != null) {
				BigDecimal amt = new BigDecimal(insuredSecondaryAnnualIncome);
				insuredSecondaryAnnualIncome = String.format("%,.2f",
						amt.setScale(2, RoundingMode.DOWN));
			}
		} else {
			insuredSecondaryAnnualIncome = "";
		}
		return insuredSecondaryAnnualIncome;
	}

	public String getPayerPrimaryAnnualIncome() {
		String payerPrimaryAnnualIncome = "";
		if (payerOccupationDetails.size() > 0) {
			OccupationDetail primaryOccupationDetail = payerOccupationDetails
					.get(0);
			payerPrimaryAnnualIncome = primaryOccupationDetail.getJobValue();
			if (payerPrimaryAnnualIncome != null) {
				BigDecimal amt = new BigDecimal(payerPrimaryAnnualIncome);
				payerPrimaryAnnualIncome = String.format("%,.2f",
						amt.setScale(2, RoundingMode.DOWN));
			}
		} else {
			payerPrimaryAnnualIncome = "";
		}
		return payerPrimaryAnnualIncome;
	}

	public String getPayerSecondaryAnnualIncome() {
		String payerSecondaryAnnualIncome = "";
		if (payerOccupationDetails.size() > 1) {
			OccupationDetail primaryOccupationDetail = payerOccupationDetails
					.get(1);
			payerSecondaryAnnualIncome = primaryOccupationDetail.getJobValue();
			if (payerSecondaryAnnualIncome != null) {
				BigDecimal amt = new BigDecimal(payerSecondaryAnnualIncome);
				payerSecondaryAnnualIncome = String.format("%,.2f",
						amt.setScale(2, RoundingMode.DOWN));
			}
		} else {
			payerSecondaryAnnualIncome = "";
		}
		return payerSecondaryAnnualIncome;
	}

	public String getProposerPaymentMethodSplitOne() {
		return proposerPaymentMethodSplitOne;
	}

	public String getProposerPaymentMethodSplitTwo() {
		return proposerPaymentMethodSplitTwo;
	}

	public String getPayerIdentityProof() {
		String IdentityProof = "";
		Map<String, Map<String, String>> codeValuesMapForTitle = codeValuesMap;
		Map<String, String> titleMap = codeValuesMapForTitle
				.get(GeneraliConstants.IDENTITYTYPE);
		if (payerPerson != null) {
			IdentityProof = payerPerson.getNationalIdType();
		}
		return titleMap.get(IdentityProof);
	}

	public String getRiderNameInThai(String riderCode) {
		String riderName = null;
		Map<String, String> riderMap = new HashMap<String, String>();

		riderMap.put(GeneraliConstants.ADB, GeneraliConstants.ADB_Rider_Thai);
		riderMap.put(GeneraliConstants.ADD, GeneraliConstants.ADD_Rider_Thai);
		riderMap.put(GeneraliConstants.AI, GeneraliConstants.AI_Rider_Thai);
		riderMap.put(GeneraliConstants.RCC_ADD,
				GeneraliConstants.ADDRCC_Rider_Thai);
		riderMap.put(GeneraliConstants.RCC_AI,
				GeneraliConstants.AIRCC_Rider_Thai);
		riderMap.put(GeneraliConstants.RCC_ADB,
				GeneraliConstants.ADBRCC_Rider_Thai);
		riderMap.put(GeneraliConstants.DD, GeneraliConstants.DD_Rider_Thai);
		riderMap.put(GeneraliConstants.HB, GeneraliConstants.HB_RiderThai);
		riderMap.put(GeneraliConstants.WP, GeneraliConstants.WP_RiderThai);
		riderMap.put(GeneraliConstants.PB, GeneraliConstants.PB_Rider_Thai);
		riderMap.put(GeneraliConstants.HS_Extra,GeneraliConstants.HS_Extra_Rider_Thai);
		riderMap.put(GeneraliConstants.CI_Extra,GeneraliConstants.CI_EXTRA_RiderThai);
		riderMap.put(GeneraliConstants.HB_Extra,GeneraliConstants.HB_EXTRA_Rider_Thai);
		riderMap.put(GeneraliConstants.ADD_Extra,GeneraliConstants.ADD_EXTRA_Rider_Thai);
		riderMap.put(GeneraliConstants.HS,GeneraliConstants.HS_RiderThai);
		
		riderMap.put("4M80", "GenCompleteHealth");
		riderMap.put("1625", "GenProLife25");
		riderMap.put("1620", "GenProLife20");
		riderMap.put("3620", "GenSave20Plus");
		riderMap.put("7B08", "GenBumnan");

		if (riderMap.containsKey(riderCode)) {
			for (Map.Entry<String, String> rider : riderMap.entrySet()) {
				if (rider.getKey().equals(riderCode)) {
					riderName = rider.getValue();
				}
			}
		}
		return riderName;
	}

	public BigDecimal getPremiumPaidAmount() {

		premiumPaidSplitOne = premiumPaidSplitOne != null ? premiumPaidSplitOne
				: BigDecimal.ZERO;
		premiumPaidSplitTwo = premiumPaidSplitTwo != null ? premiumPaidSplitTwo
				: BigDecimal.ZERO;

		return premiumPaidSplitOne.add(premiumPaidSplitTwo);
	}

	public String getPayerIdentityType() {
		return payerIdentityType;
	}

	public String getWrittenAt() {
		return writtenAt;
	}

	public String getPlace() {
		return place;
	}

	public Date getDateAndTime() {
		Date dateAndTimeInThai = convertDateToThai(dateAndTime);
		return dateAndTimeInThai;
	}
	public String getDeclarationDate(){
		Date DobValue =  getDateAndTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String date = sdf.format(DobValue);
		return date;	
	}
	public String getDeclarationYear() {
		String subYear="";
		String year = getDeclarationDate();
		if(!StringUtils.isEmpty(year))
		{
			subYear=year.substring(6);
		}
		
		return subYear;
	}

	public String getDeclarationMonth() {
		String subMonth="";
		String month = getDeclarationDate();
		if(!StringUtils.isEmpty(month))
		{
			subMonth=month.substring(3,5);
		}
		
		return subMonth;
	}

	public String getDeclarationDay() {
		String subDay="";
		String day = getDeclarationDate();
		if(!StringUtils.isEmpty(day))
		{
			subDay=day.substring(0,2);
		}
		
		return subDay;
	}

	public String populateWitnessSignature() {
		String sign = null;
		String temptoken = null;
		Document documentDetail = null;
		LOGGER.info("appionteeDocuments ---- " + appionteeDocuments);
		// appointee signature detail
		if (appionteeDocuments != null) {
			try {
				for (Document agreementDocument : appionteeDocuments) {
					if (agreementDocument != null) {
						documentDetail = agreementDocument;
						if (("signatureWitness").equals(agreementDocument
								.getSignatureType())) {
							LOGGER.info("Type Code is Signature ---"
									+ documentDetail.getSignatureType());
							sign = documentDetail.getBase64string();
							StringTokenizer token = new StringTokenizer(sign,
									",");
							while (token.hasMoreTokens()) {
								temptoken = token.nextToken();
							}
							if (temptoken != null) {
								sign = temptoken;
							}
						}
					}

				}
			} catch (Exception e) {
				LOGGER.error("Error in getting  proposer sign for eApp ");
			}
		}
		// System.out.println("Signature for PDF is " + sign);
		return sign;
	}
	public String getPayerIdentityDates(){
		Date dobValue = getPayerIdentityExpireDate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String date = sdf.format(dobValue);
		return date;
	}

	public String getPayerIdentityYear() {
		Date DobValue = getPayerIdentityExpireDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DobValue);
		String payerYear = Integer.toString(calendar.get(Calendar.YEAR));
		return payerYear;
	}

	public String getPayerIdentityMonth() {
		Date DobValue = getPayerIdentityExpireDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DobValue);
		String payerMonth = Integer.toString(calendar.get(Calendar.MONTH) + 1);
		return payerMonth;
	}

	public String getPayerIdentityDay() {
		Date DobValue = getPayerIdentityExpireDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DobValue);
		String payerDay = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
		return payerDay;
	}
	public String getInsuredIdentityDates(){
		Date dobValue = getInsuredIdentityExpireDate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String date = sdf.format(dobValue);
		return date;
	}


	public String getInsuredIdentityYear() {
		Date DobValue = getInsuredIdentityExpireDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DobValue);
		String insuredYear = Integer.toString(calendar.get(Calendar.YEAR));
		return insuredYear;
	}

	public String getInsuredIdentityMonth() {
		Date DobValue = getInsuredIdentityExpireDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DobValue);
		String insuredMonth = Integer
				.toString(calendar.get(Calendar.MONTH) + 1);
		return insuredMonth;
	}

	public String getInsuredIdentityDay() {
		Date DobValue = getInsuredIdentityExpireDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(DobValue);
		String insuredDay = Integer.toString(calendar
				.get(Calendar.DAY_OF_MONTH));
		return insuredDay;
	}

	public String getAgentLicenceNumber() {
		String agentLicenceNo = "";
		try {
			if(generaliAgent != null && generaliAgent.getLicenseNumber() != null){
				agentLicenceNo = generaliAgent.getLicenseNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting LicenceNo" + spajNumber, e);
		}
		return agentLicenceNo;
	}
	public String getGuardianName(){
		return guardianName;
	}
	public String getWitnessName(){
		return witnessName;
	}
	public String getGuardianType(){
		return guardianType;
	}
	
	public List<String> getAdditionalQuestion() {
		return additionalQuestion;
	}
	public String getOtherAddInfoLine1() {
		return otherAddInfoLine1;
	}
	public String getOtherAddInfoLine2() {
		return otherAddInfoLine2;
	}
	public String getOtherAddInfoLine3() {
		return otherAddInfoLine3;
	}
	public String getOtherAddInfoLine4() {
		return otherAddInfoLine4;
	}
	public String getOtherAddInfoLine5() {
		return otherAddInfoLine5;
	}
	public String getOtherAddInfoLine6() {
		return otherAddInfoLine6;
	}
	public String getOtherAddInfoLine7() {
		return otherAddInfoLine7;
	}
	/*Previous Policy*/
	public String getFirstPolicyNumber(){
		return firstPolicyNumber;
	}
	public String getSecondPolicyNumber(){
		return secondPolicyNumber;
	}
	public String getThirdPolicyNumber(){
		return thirdPolicyNumber;
	}
	public String getPreviousPolicyQ1() {
		return previousPolicyQ1;
	}

	public String getPreviousPolicyQ2() {
		return previousPolicyQ2;
	}

	public String getPreviousPolicyQ3() {
		return previousPolicyQ3;
	}

	public String getPreviousPolicyQ4() {
		return previousPolicyQ4;
	}

	public String getPreviousPolicyQ5() {
		return previousPolicyQ5;
	}

	public String getPreviousPolicyQ6() {
		return previousPolicyQ6;
	}

	public String getPreviousPolicyQ7() {
		return previousPolicyQ7;
	}

	public String getPreviousPolicyQ8() {
		return previousPolicyQ8;
	}
	
	public String getPreviousPolicyQ1Other(){
		return previousPolicyQ1Other;
	}
	public String getMemoPolicyNo(){
		String memoPolicyNo="";
		try {
			for (AppianMemoDetails test : appianMemoDetails) {
				if (GeneraliConstants.OPEN.equals(test.getMemoStatus())
						|| GeneraliConstants.PENDING_UW.equals(test.getMemoStatus())) {
					if(!StringUtils.isEmpty(test.getPolicyNumber())){
						memoPolicyNo=test.getPolicyNumber();
						break;
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting Memo PolicyNo" , e);
		}
		return memoPolicyNo;
	}

	public String getDisease1() {
		return disease1;
	}

	public String getDisease2() {
		return disease2;
	}

	public String getDisease3() {
		return disease3;
	}

	public String getDisease4() {
		return disease4;
	}

	public String getDisease5() {
		return disease5;
	}

	public String getDisease6() {
		return disease6;
	}

	public String getDisease7() {
		return disease7;
	}

	public String getDisease8() {
		return disease8;
	}

	public String getDisease9() {
		return disease9;
	}

	public String getDisease10() {
		return disease10;
	}

	public String getDisease11() {
		return disease11;
	}

	public String getDisease12() {
		return disease12;
	}

	public String getDisease13() {
		return disease13;
	}

	public String getDisease14() {
		return disease14;
	}

	public String getDisease15() {
		return disease15;
	}

	public String getDisease16() {
		return disease16;
	}

	public String getDisease17() {
		return disease17;
	}

	public String getDisease18() {
		return disease18;
	}

	public String getDisease19() {
		return disease19;
	}

	public String getDisease20() {
		return disease20;
	}

	public String getDisease21() {
		return disease21;
	}

	public String getDisease22() {
		return disease22;
	}

	public String getDisease23() {
		return disease23;
	}

	public String getDisease24() {
		return disease24;
	}

	public String getDisease25() {
		return disease25;
	}

	public String getDisease26() {
		return disease26;
	}

	public String getDisease27() {
		return disease27;
	}

	public String getDisease28() {
		return disease28;
	}

	public String getDisease29() {
		return disease29;
	}

	public String getDisease30() {
		return disease30;
	}

	public String getDisease31() {
		return disease31;
	}

	public String getDisease32() {
		return disease32;
	}

	public String getDisease33() {
		return disease33;
	}

	public String getDisease34() {
		return disease34;
	}

	public String getDisease35() {
		return disease35;
	}

	public String getDisease36() {
		return disease36;
	}

	public String getDisease37() {
		return disease37;
	}

	public String getDisease38() {
		return disease38;
	}

	public String getDisease39() {
		return disease39;
	}

	public String getDisease40() {
		return disease40;
	}

	public String getDisease41() {
		return disease41;
	}

	public String getDisease42() {
		return disease42;
	}

	public String getDisease43() {
		return disease43;
	}

	public String getDisease44() {
		return disease44;
	}

	public String getDisease45() {
		return disease45;
	}

	public String getDisease46() {
		return disease46;
	}

	public String getDisease47() {
		return disease47;
	}

	public String getDisease48() {
		return disease48;
	}

	public String getDisease49() {
		return disease49;
	}

	public String getDisease50() {
		return disease50;
	}

	public String getDisease51() {
		return disease51;
	}

	public String getDisease52() {
		return disease52;
	}

	public String getDisease53() {
		return disease53;
	}

	public String getDisease54() {
		return disease54;
	}

	public String getDisease55() {
		return disease55;
	}

	public String getDisease56() {
		return disease56;
	}

	public String getDisease57() {
		return disease57;
	}

	public String getDisease58() {
		return disease58;
	}

	public String getDisease59() {
		return disease59;
	}

	public String getDisease60() {
		return disease60;
	}

	public String getDisease61() {
		return disease61;
	}

	public String getDisease62() {
		return disease62;
	}

	public String getDisease63() {
		return disease63;
	}

	public String getDisease64() {
		return disease64;
	}

	public String getDisease65() {
		return disease65;
	}

	public String getDisease66() {
		return disease66;
	}

	public String getDisease67() {
		return disease67;
	}

	public String getDisease68() {
		return disease68;
	}

	public String getDisease73() {
		return disease73;
	}

	public String getDisease74() {
		return disease74;
	}

	public String getDisease75() {
		return disease75;
	}

	public String getDisease76() {
		return disease76;
	}

	public String getDisease77() {
		return disease77;
	}

	public String getDisease78() {
		return disease78;
	}

	public String getDisease79() {
		return disease79;
	}

	public String getDisease80() {
		return disease80;
	}

	public String getDisease81() {
		return disease81;
	}

	public String getDisease82() {
		return disease82;
	}

	public String getDisease83() {
		return disease83;
	}

	public String getDisease84() {
		return disease84;
	}

	public String getDisease85() {
		return disease85;
	}

	public String getDisease86() {
		return disease86;
	}

	public String getDisease87() {
		return disease87;
	}

	public String getDisease88() {
		return disease88;
	}

	public String getDisease89() {
		return disease89;
	}

	public String getDisease90() {
		return disease90;
	}

	public String getDisease91() {
		return disease91;
	}

	public String getDisease92() {
		return disease92;
	}

	public String getDisease93() {
		return disease93;
	}

	public String getDiseasep1() {
		return diseasep1;
	}

	public String getDiseasep2() {
		return diseasep2;
	}

	public String getDiseasep3() {
		return diseasep3;
	}

	public String getDiseasep4() {
		return diseasep4;
	}

	public String getDiseasep5() {
		return diseasep5;
	}

	public String getDiseasep6() {
		return diseasep6;
	}

	public String getDiseasep7() {
		return diseasep7;
	}

	public String getDiseasep8() {
		return diseasep8;
	}

	public String getDiseasep9() {
		return diseasep9;
	}

	public String getDiseasep10() {
		return diseasep10;
	}

	public String getDiseasep11() {
		return diseasep11;
	}

	public String getDiseasep12() {
		return diseasep12;
	}

	public String getDiseasep13() {
		return diseasep13;
	}

	public String getDiseasep14() {
		return diseasep14;
	}

	public String getDiseasep15() {
		return diseasep15;
	}

	public String getDiseasep16() {
		return diseasep16;
	}

	public String getDiseasep17() {
		return diseasep17;
	}

	public String getDiseasep18() {
		return diseasep18;
	}

	public String getDiseasep19() {
		return diseasep19;
	}

	public String getDiseasep20() {
		return diseasep20;
	}

	public String getDiseasep21() {
		return diseasep21;
	}

	public String getDiseasep22() {
		return diseasep22;
	}

	public String getDiseasep23() {
		return diseasep23;
	}

	public String getDiseasep24() {
		return diseasep24;
	}

	public String getDiseasep25() {
		return diseasep25;
	}

	public String getDiseasep26() {
		return diseasep26;
	}

	public String getDiseasep27() {
		return diseasep27;
	}

	public String getDiseasep28() {
		return diseasep28;
	}

	public String getDiseasep29() {
		return diseasep29;
	}

	public String getDiseasep30() {
		return diseasep30;
	}

	public String getDiseasep31() {
		return diseasep31;
	}

	public String getDiseasep32() {
		return diseasep32;
	}

	public String getDiseasep33() {
		return diseasep33;
	}

	public String getDiseasep34() {
		return diseasep34;
	}

	public String getDiseasep35() {
		return diseasep35;
	}

	public String getDiseasep36() {
		return diseasep36;
	}

	public String getDiseasep37() {
		return diseasep37;
	}

	public String getDiseasep38() {
		return diseasep38;
	}

	public String getDiseasep39() {
		return diseasep39;
	}

	public String getDiseasep40() {
		return diseasep40;
	}

	public String getDiseasep41() {
		return diseasep41;
	}

	public String getDiseasep42() {
		return diseasep42;
	}

	public String getDiseasep43() {
		return diseasep43;
	}

	public String getDiseasep44() {
		return diseasep44;
	}

	public String getDiseasep45() {
		return diseasep45;
	}

	public String getDiseasep46() {
		return diseasep46;
	}

	public String getDiseasep47() {
		return diseasep47;
	}

	public String getDiseasep48() {
		return diseasep48;
	}

	public String getDiseasep49() {
		return diseasep49;
	}

	public String getDiseasep50() {
		return diseasep50;
	}

	public String getDiseasep51() {
		return diseasep51;
	}

	public String getDiseasep52() {
		return diseasep52;
	}

	public String getDiseasep53() {
		return diseasep53;
	}

	public String getDiseasep54() {
		return diseasep54;
	}

	public String getDiseasep55() {
		return diseasep55;
	}

	public String getDiseasep56() {
		return diseasep56;
	}

	public String getDiseasep57() {
		return diseasep57;
	}

	public String getDiseasep58() {
		return diseasep58;
	}

	public String getDiseasep59() {
		return diseasep59;
	}

	public String getDiseasep60() {
		return diseasep60;
	}

	public String getDiseasep61() {
		return diseasep61;
	}

	public String getDiseasep62() {
		return diseasep62;
	}

	public String getDiseasep63() {
		return diseasep63;
	}

	public String getDiseasep64() {
		return diseasep64;
	}

	public String getDiseasep65() {
		return diseasep65;
	}

	public String getDiseasep66() {
		return diseasep66;
	}

	public String getDiseasep67() {
		return diseasep67;
	}

	public String getDiseasep68() {
		return diseasep68;
	}
	
	public String getTaxConsent() {
		return taxConsent;
	}

	public String getTaxId() {
		return taxId;
	}
	
	public String dateFormatter(String dateString) 
	{    String strDate="";
		try
		{
			DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = sdf.parse(dateString);
	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
	    strDate= formatter.format(date); 
		}
		catch(Exception e)
		{
			LOGGER.error("Error .. dateFormatter " + e.getMessage());
		}
	    return strDate;
	}




}
