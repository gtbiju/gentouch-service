package com.cognizant.insurance.omnichannel.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agreement.dao.AgreementDao;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.RiderSequence;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.omnichannel.vo.PaymentBO;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public interface GeneraliAgreementDao extends AgreementDao {

    
    /**
     * Gets the agreement count for LMS.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the agreement count
     */
    Response<SearchCountResult> getAgreementCountForLMS(Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);
    
    List<Agreement> getAgreementByFilterRelatedTransactions(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    
    Response<Set<AgreementDocument>> getBase64DocumentDetils(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);
    
    Agreement getInsuranceAgreementForReqDocUpdates(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

	List<Agreement> getRelatedAgreementsForLMS(
			Request<SearchCriteria> searchCriteriatRequest,
			Request<List<AgreementStatusCodeList>> statusRequest);

	Boolean isSpajExisting(Request<InsuranceAgreement> spajRequest,
			String spajNo,
			Request<List<AgreementStatusCodeList>> statusRequest,
			String identifier);

	Boolean isRecieptExisting(Request<PaymentBO> paymentReciptCheckRequest);

    List<Object> getCountForSalesActivity(Request<SearchCriteria> searchCriteriatRequest);
    
    GeneraliAgent getGeneraliAgent(Request<GeneraliAgent> agentRequest,String agentID);

    Map<String, Integer> retrieveMonthwiseCount(Request<SearchCriteria> searchCriteriatRequest);

    Map<String, Integer> retrieveWeeklyCount(Request<SearchCriteria> searchCriteriatRequest);

    List<Agreement> getMonthlyData(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    
    Response<SearchCountResult> getAgreementCountForIllustration(Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest, final Request<List<AgreementStatusCodeList>> statusRequestForDraft);

    InsuranceAgreement getEAppAgreementByAppNo(Request<InsuranceAgreement> aggrementRequest, Request<List<AgreementStatusCodeList>> statusRequest);
    
    Response<SearchCountResult> getAgreementCountForEApp(Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);
    
    public void updateAgreementStatusForLeadReassign(Request<Agreement> request); 

    List<Agreement> getAggrementsWithExpiredGAOLocks(Request<Agreement> agreementRequest);

    Map<String, String> getEAppIllustrationCountForReport(Request<SearchCriteria> searchCriteriatRequest);

	List<RiderSequence> retrieveRiderSequence();
}
