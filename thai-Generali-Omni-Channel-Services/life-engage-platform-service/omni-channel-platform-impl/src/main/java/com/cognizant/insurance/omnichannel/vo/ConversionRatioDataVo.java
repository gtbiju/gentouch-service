package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ConversionRatioDataVo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The columnHeader **/
    private String columnHeader;

    /** The closeCaseRatio **/
    private String closeCaseRatio;
    
    private Map<String,Map<String,BigDecimal>> conversionRatio = new LinkedHashMap<String,Map<String,BigDecimal>>();
    
    private Map<String,Map<String,String>> colorForConversionRatio = new LinkedHashMap<String,Map<String,String>>();
    
    public Map<String, Map<String, BigDecimal>> getConversionRatio() {
		return conversionRatio;
	}

	public void setConversionRatio(
			Map<String, Map<String, BigDecimal>> conversionRatio) {
		this.conversionRatio = conversionRatio;
	}

    public String getColumnHeader() {
        return columnHeader;
    }

    public void setColumnHeader(String columnHeader) {
        this.columnHeader = columnHeader;
    }

    public String getCloseCaseRatio() {
        return closeCaseRatio;
    }

    public void setCloseCaseRatio(String closeCaseRatio) {
        this.closeCaseRatio = closeCaseRatio;
    }

	public Map<String,Map<String,String>> getColorForConversionRatio() {
		return colorForConversionRatio;
	}

	public void setColorForConversionRatio(Map<String,Map<String,String>> colorForConversionRatio) {
		this.colorForConversionRatio = colorForConversionRatio;
	}

}
