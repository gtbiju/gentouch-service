/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Dec 22, 2015
 */
package com.cognizant.insurance.pushnotification;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class class ApplicationDetails.
 */
@Entity
@Table(name = "AWS_SNS_APPLICATION_DETAILS")
public class ApplicationDetails {

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    /** The application name. */
    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    /** The platform type. */
    @Column(name = "PLATFORM_TYPE")
    private String platformType;

    /** The app private key. */
    @Column(name = "APP_PRIVATE_KEY")
    private String appPrivateKey;

    /** The app certificatekey. */
    @Column(name = "APP_CERTIFICATE_KEY")
    private String appCertificatekey;

    /** The platform application arn. */
    @Column(name = "PLATFORM_APPLICATION_ARN")
    private String platformApplicationArn;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the application name.
     * 
     * @return the application name
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Sets the application name.
     * 
     * @param applicationName
     *            the application name to set.
     */
    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    /**
     * Gets the platform type.
     * 
     * @return the platform type
     */
    public String getPlatformType() {
        return platformType;
    }

    /**
     * Sets the platform type.
     * 
     * @param platformType
     *            the platform type to set.
     */
    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }

    /**
     * Gets the app private key.
     * 
     * @return the app private key
     */
    public String getAppPrivateKey() {
        return appPrivateKey;
    }

    /**
     * Sets the app private key.
     * 
     * @param appPrivateKey
     *            the app private key to set.
     */
    public void setAppPrivateKey(String appPrivateKey) {
        this.appPrivateKey = appPrivateKey;
    }

    /**
     * Gets the app certificatekey.
     * 
     * @return the app certificatekey
     */
    public String getAppCertificatekey() {
        return appCertificatekey;
    }

    /**
     * Sets the app certificatekey.
     * 
     * @param appCertificatekey
     *            the app certificatekey to set.
     */
    public void setAppCertificatekey(String appCertificatekey) {
        this.appCertificatekey = appCertificatekey;
    }

    /**
     * Gets the platform application arn.
     * 
     * @return the platform application arn
     */
    public String getPlatformApplicationArn() {
        return platformApplicationArn;
    }

    /**
     * Sets the platform application arn.
     * 
     * @param platformApplicationArn
     *            the platform application arn to set.
     */
    public void setPlatformApplicationArn(String platformApplicationArn) {
        this.platformApplicationArn = platformApplicationArn;
    }

}
