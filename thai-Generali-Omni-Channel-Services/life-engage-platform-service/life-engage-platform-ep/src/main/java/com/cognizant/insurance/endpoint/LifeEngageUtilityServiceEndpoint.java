/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Nov 23, 2016
 */

package com.cognizant.insurance.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LifeEngageUtilityService;

// TODO: Auto-generated Javadoc

/**
 * The Class LifeEngageUtilityServiceEndpoint for LE_datawipe.
 */
@Controller
@RequestMapping("/leUtilityService")
public class LifeEngageUtilityServiceEndpoint {

    /** The life engage utility service. */
    @Autowired
    private LifeEngageUtilityService lifeEngageUtilityService;

    /**
     * Save data wipe audit.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/saveDataWipeAudit", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String saveDataWipeAudit(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageUtilityService.saveDataWipeAudit(json);
            }
        }.execute(json);
    }

}
