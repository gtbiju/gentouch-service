/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partyname;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;

import com.cognizant.insurance.agent.domain.party.partycodelists.OrganizationNameUsageCodeList;

/**
 * This concept represents an organization's name. 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("Organization")
public class OrganizationName extends PartyName {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4737442205939678213L;
    /**
     * A code indicating the use of the organization's name.
     * 
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private OrganizationNameUsageCodeList usageCode;

    /**
     * Gets the usageCode.
     * 
     * @return Returns the usageCode.
     */
    public OrganizationNameUsageCodeList getUsageCode() {
        return usageCode;
    }

    /**
     * Sets The usageCode.
     * 
     * @param usageCode
     *            The usageCode to set.
     */
    public void
            setUsageCode(final OrganizationNameUsageCodeList usageCode) {
        this.usageCode = usageCode;
    }

}
