/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Dec 13, 2013
 */
package com.cognizant.insurance.agent.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class class ProductFunds.
 * 
 * @author 300797
 */

@Entity
@Table(name = "CORE_PROD_FUNDS")
public class ProductFunds {
    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    /** The fund name. */
    @Column(name = "FUND_NAME")
    private String fundName;

    /** The fund category type1. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "FUND_CATEGORY1_ID")
    private FundCategoryType fundCategoryType1;

    /** The fund category type2. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "FUND_CATEGORY2_ID")
    private FundCategoryType fundCategoryType2;

    /** The child products. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "productFunds")
    private Set<ProductFundMapping> productFundMappings;

    /** The fund name. */
    @Column(name = "FUND_CODE")
    private String fundCode;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the fund name.
     * 
     * @return the fundName
     */
    public String getFundName() {
        return fundName;
    }

    /**
     * Sets the fund name.
     * 
     * @param fundName
     *            the fundName to set
     */
    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    /**
     * Gets the fund category type1.
     * 
     * @return the fundCategoryType1
     */
    public FundCategoryType getFundCategoryType1() {
        return fundCategoryType1;
    }

    /**
     * Sets the fund category type1.
     * 
     * @param fundCategoryType1
     *            the fundCategoryType1 to set
     */
    public void setFundCategoryType1(FundCategoryType fundCategoryType1) {
        this.fundCategoryType1 = fundCategoryType1;
    }

    /**
     * Gets the fund category type2.
     * 
     * @return the fundCategoryType2
     */
    public FundCategoryType getFundCategoryType2() {
        return fundCategoryType2;
    }

    /**
     * Sets the fund category type2.
     * 
     * @param fundCategoryType2
     *            the fundCategoryType2 to set
     */
    public void setFundCategoryType2(FundCategoryType fundCategoryType2) {
        this.fundCategoryType2 = fundCategoryType2;
    }

    /**
     * Gets the product fund mappings.
     * 
     * @return the productFundMappings
     */
    public Set<ProductFundMapping> getProductFundMappings() {
        return productFundMappings;
    }

    /**
     * Sets the product fund mappings.
     * 
     * @param productFundMappings
     *            the productFundMappings to set
     */
    public void setProductFundMappings(Set<ProductFundMapping> productFundMappings) {
        this.productFundMappings = productFundMappings;
    }

	/**
	 * @return the fundCode
	 */
	public String getFundCode() {
		return fundCode;
	}

	/**
	 * @param fundCode the fundCode to set
	 */
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

}
