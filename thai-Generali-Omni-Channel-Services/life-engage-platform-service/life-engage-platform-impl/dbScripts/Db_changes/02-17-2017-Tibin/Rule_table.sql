-- CORE_PROD_RULE_SPEC

SET IDENTITY_INSERT [dbo].[CORE_PROD_RULE_SPEC] ON 
INSERT [dbo].[CORE_PROD_RULE_SPEC] ([id], [basicDataCompleteCode], [contextId], [creationDateTime], [modifiedDateTime], [transTrackingId], [transactionId], [typeName], [ACTUAL_RULE_REF], [CANBE_OVERWRITTEN_INDICATOR], [COMMENT_ON_FAILURE], [GROUP_NAME], [INPUT_PARAMETERS], [NAME], [OUTPUT_PARAMETERS], [VERSION], [transactionKeys_id], [parentProduct_id], [ruleType_ID]) VALUES (2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ECI_Validations', 0, N'', N'10014', N'', N'ECI Validations', N'', N'1', NULL, 9, N'703')
SET IDENTITY_INSERT [dbo].[CORE_PROD_RULE_SPEC] OFF

-- GROUP_TABLE

SET IDENTITY_INSERT [dbo].[GROUP_TABLE] ON 
INSERT [dbo].[GROUP_TABLE] ([ID], [GROUP_NAME], [TYPE_ID], [COMMENTS]) VALUES (10014, N'ECI', 1, N'Enhanced Critical Illness')
SET IDENTITY_INSERT [dbo].[GROUP_TABLE] OFF

-- CORE_PROD_CALC_SPEC

SET IDENTITY_INSERT [dbo].[CORE_PROD_CALC_SPEC] ON 
INSERT [dbo].[CORE_PROD_CALC_SPEC] ([id], [basicDataCompleteCode], [contextId], [creationDateTime], [modifiedDateTime], [transTrackingId], [transactionId], [typeName], [ACTUAL_RULE_REF], [GROUP_NAME], [INPUT_PARAMETERS], [NAME], [OUTPUT_PARAMETERS], [VERSION], [transactionKeys_id], [calculationType_ID], [parentProduct_id]) VALUES (3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ECIProduct', N'10014', N'', N'ECI Product', N'', N'1', NULL, N'50121', 9)
INSERT [dbo].[CORE_PROD_CALC_SPEC] ([id], [basicDataCompleteCode], [contextId], [creationDateTime], [modifiedDateTime], [transTrackingId], [transactionId], [typeName], [ACTUAL_RULE_REF], [GROUP_NAME], [INPUT_PARAMETERS], [NAME], [OUTPUT_PARAMETERS], [VERSION], [transactionKeys_id], [calculationType_ID], [parentProduct_id]) VALUES (4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ECI_Illustration', N'10014', N'', N'ECI Illustration', N'', N'1', NULL, N'50118', 9)
SET IDENTITY_INSERT [dbo].[CORE_PROD_CALC_SPEC] OFF

-- CORE_PROD_TEMPLATES

INSERT INTO [dbo].[CORE_PROD_TEMPLATES] ([TEMPLATE_ID],[TEMPLATE_NAME],[VERSION],[LANGUAGE],[COUNTRY_CODE]) VALUES(1013,N'ECI_IllustrationOutput',CAST(1.000 AS Decimal(5, 3)),N'en',NULL);
INSERT INTO [dbo].[CORE_PROD_TEMPLATES] ([TEMPLATE_ID],[TEMPLATE_NAME],[VERSION],[LANGUAGE],[COUNTRY_CODE]) VALUES(1014,N'eAppProductJsonECI',CAST(1.000 AS Decimal(5, 3)),N'en',NULL);

-- CORE_PROD_TEMPLATE_MAPPING

INSERT [dbo].[CORE_PROD_TEMPLATE_MAPPING] ([ID], [PROD_ID], [TEMPLATE_CONTEXT], [TEMPLATE_TYPE], [TEMPLATE_ID]) VALUES (13,9,'','22003','1013')
INSERT [dbo].[CORE_PROD_TEMPLATE_MAPPING] ([ID], [PROD_ID], [TEMPLATE_CONTEXT], [TEMPLATE_TYPE], [TEMPLATE_ID]) VALUES (14,9,'','22006','1014')