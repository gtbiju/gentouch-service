package com.cognizant.insurance.omnichannel.component;

import java.text.ParseException;

import org.json.JSONArray;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

public interface GeneraliManagerialLevelComponent {
	
	String retrieveByManagerialLevelCount(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException,
    ParseException;

}
