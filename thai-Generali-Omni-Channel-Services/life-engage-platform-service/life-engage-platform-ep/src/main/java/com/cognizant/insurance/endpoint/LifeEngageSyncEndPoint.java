/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.json.JSONArray;
import org.json.JSONObject;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.component.impl.LifeAsiaProcessEngine;
import com.cognizant.insurance.service.helper.LifeEngageValidationHelper;
import com.cognizant.insurance.services.LifeEngageSyncService;
import java.text.ParseException;
// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageSyncEndPoint.
 * 
 * @author 300797
 */
@Controller
@RequestMapping("/lifeEngageService")
public class LifeEngageSyncEndPoint {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageSyncEndPoint.class);

    /** The life engage sync service. */
    @Autowired
    private LifeEngageSyncService lifeEngageSyncService;
    
    @Autowired
    private LifeAsiaProcessEngine engine;
    
    @Autowired
    private LifeEngageValidationHelper lifeEngageValidationHelper;
    /**
     * Save.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String save(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
				if (!json.isEmpty()) {
            		validateAgent(json); 
            	}
                return lifeEngageSyncService.save(json);
            }
        }.execute(json);
    }

    /**
     * Delete EApp.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String delete(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.delete(json);
            }
        }.execute(json);
    }

    /**
     * Retrieve all data.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieve", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String retrieve(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.retrieve(json);
            }
        }.execute(json);
    }

    /**
     * Retrieve customer list.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveCustomers", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String retrieveCustomerList(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.retrieveCustomerList(json);
            }
        }.execute(json);
    }

    /**
     * Retrieve All customer Data for an Agent.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveAll", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String retrieveAll(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.retrieveAll(json);
            }
        }.execute(json);
    }

    /**
     * Save observations.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/saveObservations", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String saveObservations(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.saveObservations(json);
            }
        }.execute(json);
    }

    /**
     * Run illustration.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/runIllustration", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String runIllustration(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.runIllustration(json);
            }
        }.execute(json);
    }

    /**
     * Execute Rule.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/executeRule", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String executeRule(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageSyncService.executeRule(json);
            }
        }.execute(json);
    }

    /**
     * Validate Proposal.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/validateProposal", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getStatus(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.getStatus(json);
            }
        }.execute(json);
    }

    /**
     * Retrieve by filter count.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveByFilterCount", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String retrieveByFilterCount(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.retrieveByFilterCount(json);
            }
        }.execute(json);
    }

    /**
     * Retrieve By Filter.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveByFilter", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String retrieveByFilter(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.retrieveByFilter(json);
            }
        }.execute(json);
    }
    
    /**
     * Retrieve Ids By EmailTrigger.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveByIds", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String retrieveIdsByEmailTrigger(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageSyncService.retrieveByIds(json);
            }
        }.execute(json);
    }

    /**
     * Gets the life engage sync service.
     * 
     * @return the lifeEngageSyncService
     */
    public final LifeEngageSyncService getLifeEngageSyncService() {
        return lifeEngageSyncService;
    }

    /**
     * Sets the life engage sync service.
     * 
     * @param lifeEngageSyncService
     *            the lifeEngageSyncService to set
     */
    public final void setLifeEngageSyncService(final LifeEngageSyncService lifeEngageSyncService) {
        this.lifeEngageSyncService = lifeEngageSyncService;
    }

    /**
     * Generate illustration.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/generateIllustration", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String generateIllustration(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return lifeEngageSyncService.generateIllustration(json);
            }
        }.execute(json);
    }
    
    @RequestMapping(value = "/testLife", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getLifeAsiaMsg(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return engine.getLifeAsiaMsg(json);
            }
        }.execute(json);
    }
    public void validateAgent(String json) {
    	JSONObject jsonObject;
    	try {
    		jsonObject = new JSONObject(json);
    		final JSONObject jsonRequestObj = jsonObject.getJSONObject("Request");
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject("RequestInfo");
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject("RequestPayload");
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray("Transactions");
            if(jsonRqArray.length() > 0){
            	lifeEngageValidationHelper.validateUser(jsonRqArray.getJSONObject(0));
            }
    	} catch (ParseException e) {
    		e.printStackTrace();
    	}
    }
}
