package com.cognizant.icr.test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.cognizant.insurance.domain.agreement.agreementcodelists.LineOfBusinessCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Applicant;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insurer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.commoncodelists.FrequencyCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.party.partycodelists.EducationLevelCodeList;
import com.cognizant.insurance.domain.party.partycodelists.IncomeTypeCodeList;
import com.cognizant.insurance.domain.party.partycodelists.PersonDesignationCodeList;
import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;
import com.cognizant.insurance.domain.party.partyname.PersonName;
import com.cognizant.insurance.domain.party.persondetailsubtypes.EducationDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.FamilyDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.IncomeDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.OccupationDetail;
import com.cognizant.insurance.domain.roleandrelationship.familymembersubtypes.Child;
import com.cognizant.insurance.domain.roleandrelationship.familymembersubtypes.Parent;
import com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes.FamilyRelationship;
import com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes.HouseholdRelationship;


public class TestDataInsertion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		EntityManager em = null;
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("SIMPLE_ACORD");
			em = emf.createEntityManager();
			em.getTransaction().begin();
			
			System.out.println("Entity Manager Created");
			System.out.println("Begin Data Insertion");

			InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
			insuranceAgreement.setName("Sample FSA");
			insuranceAgreement.setLineOfBusinessCode(LineOfBusinessCodeList.Auto);
			em.persist(insuranceAgreement);
			
			AgreementHolder agreementHolder = new AgreementHolder();
			agreementHolder.setDescription("Testing agreementHolder");
			agreementHolder.setIsPartyRoleIn(insuranceAgreement);			
			em.persist(agreementHolder);
			
			Applicant applicant = new Applicant();
			applicant.setDescription("Testing applicant");
			applicant.setIsPartyRoleIn(insuranceAgreement);
			em.persist(applicant);
			
			Insurer insurer = new Insurer();
			insurer.setDescription("Testing Insurer");
			insurer.setIsPartyRoleIn(insuranceAgreement);
			em.persist(insurer);
			
			Beneficiary beneficiary = new Beneficiary();
			beneficiary.setDescription("Testing Beneficiary");
			beneficiary.setIsPartyRoleIn(insuranceAgreement);
            em.persist(beneficiary);

			PremiumPayer premiumPayer = new PremiumPayer();
			premiumPayer.setDescription("Testing PremiumPayer");
			premiumPayer.setIsPartyRoleIn(insuranceAgreement);            
			em.persist(premiumPayer);
			
			Insured insured = new Insured();
			insured.setDescription("Testing Insured");
			insured.setIsPartyRoleIn(insuranceAgreement);
			em.persist(insured);
			

			
			Child child1 = new Child();
			PersonName personName1 = new PersonName();
			personName1.setFullName("ChildOneName");			
			Set nameSet = new HashSet();
			nameSet.add(personName1);			
			Person child1Party = new Person();
			child1Party.setName(nameSet);			
			child1.setPlayerParty(child1Party);
			em.persist(child1);
			
			Child child2 = new Child();
			PersonName personName2 = new PersonName();
            personName2.setFullName("ChildTwoName"); 
            Set nameSet2 = new HashSet();
            nameSet2.add(personName2);  
            Person child2Party = new Person();
            child2Party.setName(nameSet2);  
            child2.setPlayerParty(child2Party);
            em.persist(child2);
            
            Parent parent = new Parent();
            PersonName personName3 = new PersonName();
            personName3.setFullName("ParentName"); 
            
            Set nameSet3 = new HashSet();
            nameSet3.add(personName3);  
            Person parentParty = new Person();
            parentParty.setName(nameSet3);  
            parent.setPlayerParty(parentParty);
            em.persist(parent);
            
            Set familyMembers = new HashSet();
            familyMembers.add(child1);
            familyMembers.add(child2);
            familyMembers.add(parent);
            
            FamilyRelationship familyRelationship = new FamilyRelationship();
            familyRelationship.setDescription("Testing FamilyRelationship");
			familyRelationship.setFamilyMember(familyMembers);
			em.persist(familyRelationship);
			
			//CREATE PERSON
/*			Person person =  new Person();
			Date birthDate = new Date();
			person.setBirthDate(birthDate);
			person.setBasicDataCompleteCode(DataCompleteCodeList.Partial);
			person.setBloodTypeCode(BloodTypeCodeList.Ab);
			person.setGenderCode(GenderCodeList.Female);
			person.setEthnicityCode(EthnicityCodeList.Caucasian);
			person.setCreationDateTime(new Date());
			person.setMaritalStatusCode(MaritalStatusCodeList.Single);
			person.setDeathDate(null);
			person.setDeathIndicator(false);
			person.setMissingIndicator(false);
			person.setTypeName("typeName");
			person.setCreationDateTime(new Date());
			person.setPrimaryLanguageCode(new ExternalCode());
			
			Organization organization = new Organization();
			OrganizationName organizationName = new OrganizationName();
			organizationName.setFullName("Cognizant");
			Set organizationNames = new HashSet();
			organizationNames.add(organizationName);
			organization.setName(organizationNames);
*/
						
//			setCountries(person);
//			
//			setPostalAddress(person);
//			
//			setCustomerRelationshp(person);
//
//			setDomicile(person);
//
//			setPersonRegistrations(person);
//
//			setOtherregistrations(person);
//			
//			setOwnedGeneralOwnership(person);
//			
//			setTaxRegistrations(person);
//			
//			setPersonName(person);
//	
//			setCriminalIncident(person);

/*          setPersonDetail(person);
			em.persist(person);
	        em.persist(organization);
*/	        
			
			em.getTransaction().commit();
			System.out.println("Done Data Insertion");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Done");
	}
//	
//	static void setCriminalIncident(Person person) {
//		Set<Party> parties =  new HashSet<Party>();
//		parties.add(person);
//		
//		CriminalIncident involvingCriminalIncident1 =  new CriminalIncident();
//		involvingCriminalIncident1.setCreationDateTime(new Date());
//		involvingCriminalIncident1.setTypeCode(CriminalIncidentCodeList.Arson);
//		involvingCriminalIncident1.setStatusCode(CriminalIncidentStatusCodeList.Convicted);
//		involvingCriminalIncident1.setStatusDate(new Date());
//		involvingCriminalIncident1.setDescription("involvingCriminalIncident1 description");
//		involvingCriminalIncident1.setInvolvedParty(parties);
//		
//		CriminalIncident involvingCriminalIncident2 =  new CriminalIncident();
//		involvingCriminalIncident2.setCreationDateTime(new Date());
//		involvingCriminalIncident2.setTypeCode(CriminalIncidentCodeList.Bribery);
//		involvingCriminalIncident2.setStatusCode(CriminalIncidentStatusCodeList.Indicted);
//		involvingCriminalIncident2.setStatusDate(new Date());
//		involvingCriminalIncident2.setDescription("involvingCriminalIncident2 description");
//		involvingCriminalIncident2.setInvolvedParty(parties);
//		
//		Set<CriminalIncident> involvingCriminalIncident = new HashSet<CriminalIncident>();
//		involvingCriminalIncident.add(involvingCriminalIncident1);
//		involvingCriminalIncident.add(involvingCriminalIncident2);
//		person.setInvolvingCriminalIncident(involvingCriminalIncident);		
//	}
//	
//	static void setOtherregistrations(Person person) {
//		MembershipRegistration membershipRegistration =  new MembershipRegistration();
//		membershipRegistration.setMembershipStatusCode(MembershipStatusCodeList.Active);
//		Set<MembershipRegistration> membershipRegistrations =  new HashSet<MembershipRegistration>();
//		person.setRegisteringMembershipRegistration(membershipRegistrations);
//		
//		CivilRegistration civilRegistration = new CivilRegistration();
//		Set<CivilRegistration> civilRegistrations =  new HashSet<CivilRegistration>();
//		civilRegistrations.add(civilRegistration);
//		person.setRegisteringCivilRegistration(civilRegistrations);
//	}
//	
//	static void setDomicile(Person person) {
//		FiscalDomicile domicile =  new FiscalDomicile();
//		domicile.setCreationDateTime(new Date());
//		domicile.setTaxCode(DomicileTaxCodeCodeList.PrimaryTax);
//		Place domiciledPlace = new Place();
//		domiciledPlace.setName("Place name");
//		domiciledPlace.setDescription("Place description");
//		domiciledPlace.setAvailablePeriod(new TimePeriod());
//		domicile.setDomiciledPlace(domiciledPlace);
//		domicile.setEffectivePeriod(new TimePeriod());
//		domicile.setTypeName("domicile type");
//		Set<FiscalDomicile> domiciles =  new HashSet<FiscalDomicile>();
//		domiciles.add(domicile);
//		person.setEstablishedFiscalDomicile(domiciles);
//	}
//	
//	static void setOwnedGeneralOwnership(Person person) {
//		GeneralOwnershipInformation ownedGeneralOwnership =  new GeneralOwnershipInformation();
//		BodyPart bodyPart = new BodyPart();
//		Set<PhysicalObject> ownedPhysicalObject =  new HashSet<PhysicalObject>();
//		ownedPhysicalObject.add(bodyPart);
//		ownedGeneralOwnership.setOwnedPhysicalObject(ownedPhysicalObject);
//		ownedGeneralOwnership.setOwnershipTypeCode(OwnershipTypeCodeList.FinancialAssets);
//		ownedGeneralOwnership.setOwnershipCount(10);
//		ownedGeneralOwnership.setOwnershipAmount(new CurrencyAmount());
//		Set<GeneralOwnershipInformation> ownedGeneralOwnerships =  new HashSet<GeneralOwnershipInformation>();
//		ownedGeneralOwnerships.add(ownedGeneralOwnership);
//		
//		person.setOwnedGeneralOwnership(ownedGeneralOwnerships);
//	}
//	
//	
//	static void setCountries(Person person) {
//		Country citizenshipCountry = new Country();
//		citizenshipCountry.setName("citizenship country name");
//		citizenshipCountry.setDescription("citizenship country description");
//		citizenshipCountry.setAlphaISOCode(new ExternalCode());
//		citizenshipCountry.setExtendedISOCode(new ExternalCode());
//		citizenshipCountry.setTelephonePrefixCode(new ExternalCode());
//		Set<Country>citizenshipCountries = new HashSet<Country>();
//		citizenshipCountries.add(citizenshipCountry);
//		person.setCitizenshipCountry(citizenshipCountries);
//		
//		Country nationalityCountry = new Country();
//		nationalityCountry.setName("nationality country name");
//		nationalityCountry.setDescription("nationality country description");
//		nationalityCountry.setAlphaISOCode(new ExternalCode());
//		nationalityCountry.setExtendedISOCode(new ExternalCode());
//		nationalityCountry.setTelephonePrefixCode(new ExternalCode());
//		Set<Country>nationalityCountries = new HashSet<Country>();
//		nationalityCountries.add(nationalityCountry);
//		person.setNationalityCountry(nationalityCountries);
//	}
//	
//	static void setPostalAddress(Person person) {
//		PostalAddress birthAddress = new PostalAddress();
//		birthAddress.setStreetName("street Name");
//		birthAddress.setStreetNumber("street Number");
//		birthAddress.setStreetTypeCode(new ExternalCode());
//		birthAddress.setBoxNumber("box Number");
//		birthAddress.setFloorNumber("floor Number");
//		birthAddress.setUnitNumber("unit Number");
//		birthAddress.setBuildingName("building Name");
//		birthAddress.setAddressLines("address Lines");
//		birthAddress.setPostalBarcode("postal Barcode");
//		birthAddress.setPreDirectionCode(DirectionTypeCodeList.E);
//		birthAddress.setPostDirectionCode(DirectionTypeCodeList.N);
//		birthAddress.setCarrierRoute("carrier Route");
//		City city = new City();
//		city.setAdministrativeSubDivisionCode(AdministrativeSubDivisionCodeList.CountryCapital);
//		Zone zone = new Zone();
//		Set<CountryElement> countryElements =  new HashSet<CountryElement>();
//		countryElements.add(zone);
//		countryElements.add(city);
//		birthAddress.setIncludedCountryElement(countryElements);
//		Country postalCountry = new Country();
//		postalCountry.setName("country name");
//		postalCountry.setDescription("country description");
//		postalCountry.setAlphaISOCode(new ExternalCode());
//		postalCountry.setExtendedISOCode(new ExternalCode());
//		postalCountry.setTelephonePrefixCode(new ExternalCode());
//		birthAddress.setPostalCountry(postalCountry);
//		
//		StreetAddress streetAddress = new StreetAddress();
//		streetAddress.setName("street address name");
//		streetAddress.setNumber("street number");
//		streetAddress.setPreDirectionCode(DirectionTypeCodeList.E);
//		streetAddress.setPostDirectionCode(DirectionTypeCodeList.N);
//		streetAddress.setTypeCode(new ExternalCode());
//		birthAddress.setIncludedStreetAddress(streetAddress);
//		
//		Municipality postalMunicipality = new Municipality();
//		postalMunicipality.setAdministrativeSubDivisionCode(AdministrativeSubDivisionCodeList.CountySeat);
//		postalMunicipality.setTypeCode(MunicipalityTypeCodeList.Borough);
//		postalMunicipality.setAssignedCode(new ExternalCode());
//		Set<Municipality> postalMunicipalities = new HashSet<Municipality>();
//		postalMunicipalities.add(postalMunicipality);
//		birthAddress.setPostalMunicipality(postalMunicipalities);
//		
//		PostCode postalPostCode = new PostCode();
//		postalPostCode.setAssignedCode(new ExternalCode());
//		postalPostCode.setAssignedCodeExtension("assigned Code Extension");
//		postalPostCode.setSubSystemTypeCode(PostalSubSystemCodeList.CEDEX);
//		birthAddress.setPostalPostCode(postalPostCode);
//		
//		CountrySubdivision postalCountrySubdivision = new CountrySubdivision();
//		postalCountrySubdivision.setTypeCode(CountrySubdivisionTypeCodeList.District);
//		postalCountrySubdivision.setAlphaISOCode(new ExternalCode());
//		birthAddress.setPostalCountrySubdivision(postalCountrySubdivision);
//		
//		person.setBirthAddress(birthAddress);
//	}
//	
	static void setPersonDetail(Person person) {
		Set<PersonDetail> personDetails = new HashSet<PersonDetail>();
		person.setDetail(personDetails);
		EducationDetail educationDetail = new EducationDetail();
		educationDetail.setEducationLevelCode(EducationLevelCodeList.Associate);
		educationDetail.setFullTimeStudentIndicator(true);
		educationDetail.setDesignationCode(PersonDesignationCodeList.CPCU);
//		Set<EducationCertificate> certificates =  new HashSet<EducationCertificate>();
//		educationDetail.setSupportingEducationCertificate(certificates);
//		EducationCertificate educationCertificate = new EducationCertificate();
//		educationCertificate.setStatusCode(RegistrationStatusCodeList.Active);
//		educationCertificate.setStatusReasonCode(StatusReasonCodeList.AgreementAdditionChangeinProgress);
//		educationCertificate.setSubjectAreaCode(SubjectAreaCodeList.Accountancy);
//		educationCertificate.setGradeCode(EducationGradeCodeList.BlackBelt);
//		educationCertificate.setName("education name");
//		educationCertificate.setTitleCode(EducationCertificateTitleCodeList.BSc);
//		certificates.add(educationCertificate);
		
//		EducationCertificate educationCertificate2 = new EducationCertificate();
//		educationCertificate2.setStatusCode(RegistrationStatusCodeList.Active);
//		educationCertificate2.setStatusReasonCode(StatusReasonCodeList.AgreementAdditionChangeinProgress);
//		educationCertificate2.setSubjectAreaCode(SubjectAreaCodeList.Accountancy);
//		educationCertificate2.setGradeCode(EducationGradeCodeList.BlackBelt);
//		educationCertificate2.setName("education name");
//		educationCertificate2.setTitleCode(EducationCertificateTitleCodeList.BSc);
//		certificates.add(educationCertificate);
//		certificates.add(educationCertificate2);
		personDetails.add(educationDetail);
		
		FamilyDetail familyDetail = new FamilyDetail();
		familyDetail.setChildrenCount(new BigDecimal(2.0f));
		HouseholdRelationship householdRelationship = new HouseholdRelationship();
		householdRelationship.setDescription("householdRelationshipDesc");
		householdRelationship.setYoungestDependentAdultBirthDate(new Date());
		familyDetail.setProvidingHousehold(householdRelationship);
		personDetails.add(familyDetail);
		
		IncomeDetail incomeDetail = new IncomeDetail();
		incomeDetail.setGrossAmount(new CurrencyAmount());
		incomeDetail.setNetAmount(new CurrencyAmount());
		incomeDetail.setDisposableAmount(new CurrencyAmount());
		incomeDetail.setTypeCode(IncomeTypeCodeList.Alimony);
		incomeDetail.setFrequencyCode(FrequencyCodeList.AnnualCalendar);
		personDetails.add(incomeDetail);
		
//		MedicalDetail medicalDetail = new MedicalDetail();
//		medicalDetail.setGeneralEvaluationDescription("Evaluation Description");
//		medicalDetail.setMedicalFileLastUpdateDate(new Date());
//		medicalDetail.setConditionStatusCode(ConditionStatusCodeList.Completed);
//		medicalDetail.setCauseOfConditionCode(CauseOfMedicalConditionCodeList.Anxiety);
//		personDetails.add(medicalDetail);
		
		OccupationDetail occupationDetail = new OccupationDetail();
		occupationDetail.setOccupationClass("occupation Class");
		occupationDetail.setAnticipatedRetirementIndicator(false);
		occupationDetail.setAnticipatedRetirementDate(new Date());
		occupationDetail.setRetirementDate(new Date());
		occupationDetail.setCurrentEmploymentIndicator(true);
		occupationDetail.setPrimaryOccupationIndicator(true);
		occupationDetail.setProfessionalTitle("professional Title");
		personDetails.add(occupationDetail);
		
//		PersonLegalCapacity personLegalCapacity =  new PersonLegalCapacity();
//		personLegalCapacity.setLegalCapacity("legal Capacity");
//		personDetails.add(personLegalCapacity);
//		
//		LifestyleActivity lifestyleActivity =  new LifestyleActivity();
//		lifestyleActivity.setDescription("description");
//		lifestyleActivity.setFrequencyCode(FrequencyCodeList.AnnualCalendar);
//		lifestyleActivity.setMotive("motive");
//		lifestyleActivity.setLastOccurrenceDate(new Date());
//		lifestyleActivity.setName("style activityname");
//		lifestyleActivity.setAverageTimeSpentDuration(new TimePeriod());
//		personDetails.add(lifestyleActivity);
		
//		SubstanceUse substanceUse = new SubstanceUse();
//		substanceUse.setDescription("description");
//		substanceUse.setFrequencyCode(FrequencyCodeList.AnnualCalendar);
//		substanceUse.setMotive("motive");
//		substanceUse.setLastOccurrenceDate(new Date());
//		substanceUse.setAmount(new CurrencyAmount());
//		substanceUse.setDuration(new TimePeriod());
//		substanceUse.setAlcoholTypeCode(AlcoholTypeCodeList.Beer);
//		substanceUse.setTobaccoTypeCode(TobaccoTypeCodeList.ChewingTobacco);
//		substanceUse.setDrugTypeCode(DrugTypeCodeList.Amphetamines);
//		personDetails.add(substanceUse);
	}
//	
//	static void setTaxRegistrations(Person person) {
//		Set<TaxRegistration> taxRegistrations = new HashSet<TaxRegistration>();
//		person.setTaxRegistrations(taxRegistrations);
//		TaxRegistration taxRegistration = new TaxRegistration();
//		taxRegistrations.add(taxRegistration);
//		taxRegistration.setEffectivePeriod(new TimePeriod());
//		taxRegistration.setCalculationTypeCode(TaxCalculationCodeList.Gross);
//		taxRegistration.setCreationDateTime(new Date());
//		taxRegistration.setDescription("tax registration");
//		taxRegistration.setDisqualificationDate(new Date());
//		taxRegistration.setDisqualificationReasonCode(StatusReasonCodeList.AgreementAdditionChangeinProgress);
//		
//		Registry includedInRegistry =  new Registry();
//		includedInRegistry.setName("tax Registry name");
//		taxRegistration.setIncludedInRegistry(includedInRegistry);
//		
//		Place issuedPlace = new Place();
//		issuedPlace.setName("tax registration place");
//		issuedPlace.setAvailablePeriod(new TimePeriod());
//		
//		taxRegistration.setIssuedPlace(issuedPlace);
//		taxRegistration.setLastUsedDate(new Date());
//		taxRegistration.setLastVerifiedDate(new Date());
//		taxRegistration.setLatestStatusDate(new Date());
//		taxRegistration.setMembershipType("membershipType");
//		taxRegistration.setRegistrationAuthorityTypeCode(RegistrationAuthorityTypeCodeList.GovernmentAgency);
//		taxRegistration.setRequestDate(new Date());
//		taxRegistration.setStatusCode(RegistrationStatusCodeList.Active);
//		taxRegistration.setStatusReasonCode(StatusReasonCodeList.AgreementAdditionChangeinProgress);
//		taxRegistration.setTypeName("typeName");
//		taxRegistration.setTaxRegistrant(person);
//	}
//	
//	static void setPersonName(Person person) {
//		PersonName personName =  new PersonName();
//		Set<PersonName> names =  new HashSet<PersonName>();
//		person.setName(names);
//		names.add(personName);
//		personName.setGivenName("givenname");
//		personName.setFullName("fullName");
//		personName.setLanguageCode(new ExternalCode());
//		personName.setMiddleName("middleName");
//		personName.setSuffix("suffix");
//		personName.setSurname("surname");
//		personName.setTypeName("typeName");
//		personName.setUsageCode(PersonNameUsageCodeList.BirthName);
//		personName.setDefaultIndicator(false);
//		personName.setCreationDateTime(new Date());
//		personName.setDescription("description");
//		personName.setEffectivePeriod(new TimePeriod());
//		personName.setPrefixTitleCode(PrefixTitleCodeList.Mr);
//		
//		
//		ContactPreference contactPreference =  new ContactPreference();
//		
//		Set<ContactPreference> preferredContactPreference = new HashSet<ContactPreference>();
//		preferredContactPreference.add(contactPreference);
//        contactPreference.setBasicDataCompleteCode(DataCompleteCodeList.Full);
//        contactPreference.setContactInstructions("HANDLE WITH CARE");
//        contactPreference.setContextId(new Long(9));
//        Date creationDateTime1 = new Date();
//        creationDateTime1.setDay(12);
//        creationDateTime1.setMonth(4);
//        creationDateTime1.setYear(2011);
//        contactPreference.setCreationDateTime(creationDateTime1);
//        contactPreference.setDefaultIndicator(true);
//        contactPreference.setLastValidatedDate(creationDateTime1);
//        Set<CommunicationProfile> preferredCommunicationProfile =
//                new HashSet<CommunicationProfile>();
//        CommunicationProfile communicationProfile = new CommunicationProfile();
//        communicationProfile.setCreationDateTime(creationDateTime1);
//        communicationProfile.setPriorityLevel(5);
//        preferredCommunicationProfile.add(communicationProfile);
//        contactPreference.setPreferredCommunicationProfile(preferredCommunicationProfile);
//        TelephoneCallContact preferredContactPoint = new TelephoneCallContact();
//        preferredContactPoint.setTypeName("TELEPHONE CONTACT");
//        preferredContactPoint.setStatusCode(ContactStatusCodeList.Listed);
//        contactPreference.setPreferredContactPoint(preferredContactPoint);
//        
//        ContactPreference contactPreference2 = new ContactPreference();
//        
//        TextContact textContact = new TextContact();
//        DataCompleteCodeList basicDataCompleteCode =
//                DataCompleteCodeList.Partial;
//        textContact.setBasicDataCompleteCode(basicDataCompleteCode);
//        TelephoneNumber identifyingTelephoneNumber = new TelephoneNumber();
//        identifyingTelephoneNumber.setExtension("9871-");
//        identifyingTelephoneNumber.setLocalNumber("109-0989-111");
//        identifyingTelephoneNumber.setFullNumber("90876555111");
//        identifyingTelephoneNumber.setTrunkPrefix("121");
//        textContact.setIdentifyingTelephoneNumber(identifyingTelephoneNumber);
//        ContactStatusCodeList statusCode = ContactStatusCodeList.Listed;
//        textContact.setStatusCode(statusCode);
//        textContact.setContextId(new Long(10));
//        contactPreference2.setPreferredContactPoint(textContact);
//        preferredContactPreference.add(contactPreference2);
//        
//		ContactPreference contactPreference3 = new ContactPreference();
//		EmailContact emailContact = new EmailContact();
//		emailContact.setEmailAddress("abjuyng//@acvb.co.in");
//		emailContact.setStatusCode(ContactStatusCodeList.Inactive);
//		emailContact.setContextId(new Long(231));
//		emailContact.setTypeName("Type1");
//		emailContact.setUniformResourceLocation(new UniformResourceLocation());
//		contactPreference3.setPreferredContactPoint(emailContact);
//		preferredContactPreference.add(contactPreference3);
//		
//		ContactPreference contactPreference4 = new ContactPreference();
//		WebPageContact webPageContact = new WebPageContact();
//        UniformResourceLocation uniformResourceLocation =
//                new UniformResourceLocation();
//        webPageContact.setUniformResourceLocation(uniformResourceLocation);
//        webPageContact.setContextId(new Long(2));
//        webPageContact.setStatusCode(ContactStatusCodeList.Inactive);
//        webPageContact.setTypeName("tYpe webpage Contact");
//        webPageContact.setUrl("www.cognizant.com");
//        Date creationDateTime = new Date();
//        creationDateTime1.setDay(21);
//        creationDateTime1.setMonth(5);
//        creationDateTime1.setYear(2012);
//        webPageContact.setCreationDateTime(creationDateTime);
//        contactPreference4.setPreferredContactPoint(webPageContact);
//		preferredContactPreference.add(contactPreference4);
//        
//		ContactPreference contactPreference5 = new ContactPreference();
//		PostalMailContact postalMailContact = new PostalMailContact();
//        postalMailContact.setBasicDataCompleteCode(DataCompleteCodeList.Partial);
//        postalMailContact.setContextId(new Long(4));
//        PostalAddress deliveryAddress = new PostalAddress();
//        StreetAddress includedStreetAddress = new StreetAddress();
//        includedStreetAddress.setName("Postal mail contact name");
//        includedStreetAddress.setNumber("13b");
//        includedStreetAddress.setUnstructuredAddress("Uncondition address in an unstructured way- POSTAL MAIL");
//        DirectionTypeCodeList postDirectionCode = DirectionTypeCodeList.SE;
//        includedStreetAddress.setPostDirectionCode(postDirectionCode);
//        deliveryAddress.setIncludedStreetAddress(includedStreetAddress);
//        postalMailContact.setDeliveryAddress(deliveryAddress);
//        postalMailContact.setTypeName("POSTAL MAIL CONTACT OUTER NAME");
//        contactPreference5.setPreferredContactPoint(postalMailContact);
//        preferredContactPreference.add(contactPreference5);
//        
//        ContactPreference contactPreference6 = new ContactPreference();
//        PostalContact postalContact = new PostalContact();
//        postalContact.setAddressNatureCode(AddressNatureCodeList.Shipping);
//        PostalAddress identifyingPostalAddress = new PostalAddress();
//        StreetAddress includedStreetAddress1 = new StreetAddress();
//        includedStreetAddress1.setName("Name POSTAL CONTACT");
//        includedStreetAddress1.setPostDirectionCode(DirectionTypeCodeList.SE);
//        identifyingPostalAddress.setIncludedStreetAddress(includedStreetAddress1);
//        postalContact.setIdentifyingPostalAddress(identifyingPostalAddress);
//        postalContact.setStatusCode(ContactStatusCodeList.Inactive);
//        contactPreference6.setPreferredContactPoint(postalContact);
//        preferredContactPreference.add(contactPreference6);
//        
//        
//        ContactPreference contactPreference7 = new ContactPreference();
//        InPersonContact inPersonContact = new InPersonContact();
//        inPersonContact.setBasicDataCompleteCode(DataCompleteCodeList.Partial);
//        inPersonContact.setContextId(new Long(87));
//        PlaceAddress meetingAddress = new PlaceAddress();
//        Date creationDateTime2 = new Date();
//        creationDateTime2.setDay(21);
//        creationDateTime2.setMonth(5);
//        creationDateTime2.setYear(2012);
//        meetingAddress.setCreationDateTime(creationDateTime2);
//        meetingAddress.setContextId(new Long(23));
//        meetingAddress.setUnstructuredAddress("ABCADJADJ");
//        meetingAddress.setTypeName("TYpe IN PERSON CONTACT");
//        meetingAddress.setUnstructuredAddress("IN PERSONY CNTAACT");
//        inPersonContact.setMeetingAddress(meetingAddress);
//        inPersonContact.setCreationDateTime(creationDateTime2);
//        contactPreference7.setPreferredContactPoint(inPersonContact);
//        preferredContactPreference.add(contactPreference7);
//        
//        
//        ContactPreference contactPreference8 = new ContactPreference();
//        InNetworkContact inNetworkContact = new InNetworkContact();
//        inNetworkContact.setBasicDataCompleteCode(DataCompleteCodeList.Partial);
//        inNetworkContact.setContextId(new Long(21));
//        TelephoneNumber telephoneNumber = new TelephoneNumber();
//        telephoneNumber.setExtension("9871-");
//        telephoneNumber.setLocalNumber("109-0989-111");
//        telephoneNumber.setFullNumber("90876555111");
//        telephoneNumber.setTrunkPrefix("121");
//        inNetworkContact.setTelephoneNumber(telephoneNumber);
//        inNetworkContact.setTypeCode(InNetworkContactTypeCodeList.Skype);
//        inNetworkContact.setTypeName("IN NETWORK TYPE");
//        contactPreference8.setPreferredContactPoint(inNetworkContact);
//        preferredContactPreference.add(contactPreference8);
//        
//        ContactPreference contactPreference9 = new ContactPreference();
//        FaxContact faxContact = new FaxContact();
//        faxContact.setBasicDataCompleteCode(DataCompleteCodeList.Removed);
//        faxContact.setContextId(new Long(3));
//        faxContact.setTypeName("type FAX");
//        TelephoneNumber telephoneNumber1 = new TelephoneNumber();
//        telephoneNumber1.setExtension("9871-");
//        telephoneNumber1.setLocalNumber("109-0989-111");
//        telephoneNumber1.setFullNumber("90876555111");
//        telephoneNumber1.setTrunkPrefix("121");
//        faxContact.setTelephoneNumber(telephoneNumber1);
//        contactPreference9.setPreferredContactPoint(faxContact);
//        preferredContactPreference.add(contactPreference9);
//        
////        ContactPreference contactPreference10 = new ContactPreference();
////        MmsiNumber mmsiNumber = new MmsiNumber();
////        mmsiNumber.setBasicDataCompleteCode(DataCompleteCodeList.ReadOnly);
////        mmsiNumber.setIdentifier(112);
////        mmsiNumber.setStationTypeCode(MMSICodeList._5);
////        mmsiNumber.setLocalNumber("90888765551");
////        Date creationDateTime3 = new Date();
////        creationDateTime3.setDay(21);
////        creationDateTime3.setMonth(5);
////        creationDateTime3.setYear(2012);
////        mmsiNumber.setCreationDateTime(creationDateTime3);
////        contactPreference10.setPreferredContactPoint(mmsiNumber);
////        preferredContactPreference.add(contactPreference10);
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        Set<ContactRestriction> preferredContactRestriction =
//                new HashSet<ContactRestriction>();
//        ContactRestriction contactRestriction = new ContactRestriction();
//        contactRestriction.setContextId(new Long(23));
//        contactRestriction.setMaximumActiveContactCount(new BigDecimal(13));
//        contactRestriction.setTypeName("Type of COntact Restriction");
//        contactRestriction.setBasicDataCompleteCode(DataCompleteCodeList.Partial);
//        preferredContactRestriction.add(contactRestriction);
//        contactPreference.setPreferredContactRestriction(preferredContactRestriction);
//        Set<TimingPreference> preferredTimingPreference =
//                new HashSet<TimingPreference>();
//        TimingPreference timingPreference = new TimingPreference();
//        timingPreference.setTypeName("PREFERENCE 1 ");
//        timingPreference.setCreationDateTime(creationDateTime1);
//        preferredTimingPreference.add(timingPreference);
//        contactPreference.setPreferredTimingPreference(preferredTimingPreference);
//		
//		
//		
//		
//		personName.setPreferredContactPreference(preferredContactPreference);
//	}
//	
//	static Set<PersonRegistration> setPersonRegistrations(Person person) {
//		Set<PersonRegistration> personRegistrations =  new HashSet<PersonRegistration>();
//		BirthCertificate birthCertificate =  new BirthCertificate();
//		birthCertificate.setRegisteredBirthDate(new Date());
//		personRegistrations.add(birthCertificate);
//		
//		CivilRegistration civilRegistration =  new CivilRegistration();
//		civilRegistration.setRegistrantPerson(person);// doubt
//		personRegistrations.add(civilRegistration);
//		
//		DeathCertificate deathCertificate = new DeathCertificate();
//		deathCertificate.setRegisteredDeathDate(new Date());
//		Death death = new Death();
//		Illness causeOfDeath =  new Illness();
//		causeOfDeath.setTypeCode(new ExternalCode());
//		causeOfDeath.setName("illness name");
//		causeOfDeath.setPermanentIndicator(true);
//		death.setCauseOfDeath(causeOfDeath);
//		deathCertificate.setRegisteredDeath(death);
//		Set<DeathCertificate> certificates =  new HashSet<DeathCertificate>();
//		certificates.add(deathCertificate);
//		death.setRegisteringDeathCertificate(certificates);
//		personRegistrations.add(deathCertificate);
//		
//		DriverLicense driverLicense =  new DriverLicense();
//		driverLicense.setLicenseStatusCode(LicenseStatusCodeList.Active);
//		driverLicense.setLicenseStatusDate(new Date());
//		driverLicense.setDrivingPointsCount(new BigDecimal(new BigInteger("11")));
//		personRegistrations.add(driverLicense);
//		
//		EducationCertificate educationCertificate = new EducationCertificate();
//		educationCertificate.setStatusCode(RegistrationStatusCodeList.Active);
//		educationCertificate.setStatusReasonCode(StatusReasonCodeList.AgreementAdditionChangeinProgress);
//		educationCertificate.setSubjectAreaCode(SubjectAreaCodeList.Accountancy);
//		educationCertificate.setGradeCode(EducationGradeCodeList.BlackBelt);
//		educationCertificate.setName("education name");
//		educationCertificate.setTitleCode(EducationCertificateTitleCodeList.BSc);
//		personRegistrations.add(educationCertificate);
//		
//		IdentityCard identityCard =  new IdentityCard();
//		identityCard.setIdCardStatusDate(new Date());
//		identityCard.setValidityStatusCode(ValidityStatusCodeList.Valid);
//		personRegistrations.add(identityCard);
//		
//		Passport passport =  new Passport();
//		passport.setValidityStatusCode(ValidityStatusCodeList.Valid);
//		passport.setPassportStatusDate(new Date());
//		personRegistrations.add(passport);
//		
//		PersonalActivityLicense activityLicense = new PersonalActivityLicense();
//		activityLicense.setRequiredInstructionDuration(new TimePeriod());
//		activityLicense.setRenewalIntentionIndicator(false);
//		activityLicense.setName("activity name");
//		activityLicense.setCompetitionLicenseIndicator(true);
//		personRegistrations.add(activityLicense);
//		
//		person.setRegisteringPersonRegistration(personRegistrations);
//		
//		return personRegistrations;
//	}
//	
//	static void setCustomerRelationshp(Person person) {
//		CustomerRelationship customerRelationship =  new CustomerRelationship();
//		Set<CustomerRelationship> customerRelationships =  new HashSet<CustomerRelationship>();
//		customerRelationships.add(customerRelationship);
//		customerRelationship.setCustomershipDate(new Date());
//		customerRelationship.setTotalAnnualSubscriptionsAmount(new CurrencyAmount());
//		customerRelationship.setEstimationOfLifeAgreementAmount(new CurrencyAmount());
//		customerRelationship.setClaimPeriodDuration(new TimePeriod());
//		customerRelationship.setClaimDuringClaimPeriodCount(new BigDecimal(23232));
//		customerRelationship.setFileWatchAlertIndicator(false);
//		customerRelationship.setEstablishedAmount(new CurrencyAmount());
//		customerRelationship.setPotentialAmount(new CurrencyAmount());
//		customerRelationship.setBenefitingFromCommercialAssistanceIndicator(false);
//		customerRelationship.setCommercialAssistanceDescription("commercial Assistance Description");
//		customerRelationship.setQualifiedForCommercialAssistanceIndicator(false);
//		customerRelationship.setPotentialCommercialAssistanceDescription("potential Commercial Assistance Description");
//		customerRelationship.setLatestBatchProcessingDate(new Date());
//		customerRelationship.setCustomerEvaluation("customer Evaluation");
//		customerRelationship.setVipIndicator(true);
//		customerRelationship.setFirstContactMethod("first Contact Method");
//		customerRelationship.setLegalEntityCode(LegalEntityCodeList.Association);
//		customerRelationship.setImportanceLevelCode(CustomerImportanceLevelCodeList.HighestPriority);
//		ServiceProvider serviceProvider = new ServiceProvider();
//		serviceProvider.setCustomerRelationship(customerRelationships);
//		
//		customerRelationship.setServiceProvider(serviceProvider);
//		customerRelationship.setStatusCode(CustomerStatusCodeList.Active);
//		customerRelationship.setCustomer(person);
//		
//		Set<PartyRole> involvedRoles = setPartyRoles(person);
//		customerRelationship.setInvolvedRole(involvedRoles);
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		person.setCustomerRelationship(customerRelationships);
//		
//	}
//	
//	static Set<PartyRole> setPartyRoles(Person person) {
//		Set<PartyRole> involvedRoles =  new HashSet<PartyRole>();
//		Insured insured =  new Insured();
//		insured.setPlayerParty(person);
//		
//		Beneficiary beneficiary =  new Beneficiary();
//		beneficiary.setPlayerParty(person);
//		beneficiary.setDesignationCode(BeneficiaryDesignationCodeList.Allnaturalchildrenoftheinsured);
//		beneficiary.setIrrevocableIndicator(false);
//		beneficiary.setLegalWording("legal Wording");
//		beneficiary.setAcceptedIndicator(true);
//		beneficiary.setPartExemptFromTaxesPercentage(1.02f);
//		beneficiary.setBenefitDistributionCalculationCode(BenefitDistributionCalculationCodeList.Balance);
//		
//		PremiumPayer premiumPayer  =  new PremiumPayer();
//		premiumPayer.setPlayerParty(person);
//		
//		Insurer insurer =  new Insurer();
//		insurer.setPlayerParty(person);
//		insurer.setRejectionReasonCode(InsurerRejectionReasonCodeList.HighRisk);
//		insurer.setWrittenLinePercentage(1.0f);
//		insurer.setSignedLinePercentage(1.0f);
//		insurer.setLeaderIndicator(true);
//		insurer.setManagementByAperitorIndicator(false);
//		
//		involvedRoles.add(insured);
//		involvedRoles.add(beneficiary);
//		involvedRoles.add(premiumPayer);
//		involvedRoles.add(insurer);
//		
//		
//		return involvedRoles;
//	}
}
