/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 19, 2015
 */
package com.cognizant.insurance.generic.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.generic.dao.PushNotificationDao;
import com.cognizant.insurance.pushnotification.ApplicationDetails;
import com.cognizant.insurance.pushnotification.CompositeKey;
import com.cognizant.insurance.pushnotification.DeviceRegister;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;

/**
 * The Class class PushNotificationDaoImpl.
 */
public class PushNotificationDaoImpl extends DaoImpl implements PushNotificationDao {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.dao.PushNotificationDao#isDeviceIdExists(com.cognizant.insurance.request.Request)
     */
    @Override
    public boolean isDeviceIdExists(Request<DeviceRegister> request) {
        boolean isDeviceIdExists = false;
        String query =
                "select deviceReg from  DeviceRegister deviceReg where deviceReg.applicationName =?1 and deviceReg.deviceID = ?2 and deviceReg.key.mobileAppBundleID = ?3 and deviceReg.key.agentID =?4";
        DeviceRegister deviceRegister = request.getType();
        final CompositeKey key = deviceRegister.getKey();
        List<DeviceRegister> deviceRegisterList =
                findByQuery(request, query, deviceRegister.getApplicationName(), deviceRegister.getDeviceID(),
                        key.getMobileAppBundleID(), key.getAgentID());
        if (CollectionUtils.isNotEmpty(deviceRegisterList)) {
            isDeviceIdExists = true;
            for (DeviceRegister deviceRegisterObj : deviceRegisterList) {
                if ("D".equals(deviceRegisterObj.getStatus())) {
                    deviceRegisterObj.setStatus("E");
                }
            }
        }
        return isDeviceIdExists;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.dao.PushNotificationDao#registerDevice(com.cognizant.insurance.request.Request)
     */
    @Override
    public void unRegisterDevice(Request<DeviceRegister> deviceRegRequest) {

        String query =
                "update DeviceRegister deviceReg set deviceReg.status = ?1 where deviceReg.deviceID = ?2 and deviceReg.key.mobileAppBundleID = ?3 and deviceReg.applicationName = ?4 and deviceReg.key.agentID = ?5 and deviceReg.status='E'";
        final DeviceRegister deviceRegister = (DeviceRegister) deviceRegRequest.getType();
        final CompositeKey key = deviceRegister.getKey();
        int result =
                executeUpdate(deviceRegRequest, query, deviceRegister.getStatus(), deviceRegister.getDeviceID(),
                        key.getMobileAppBundleID(), deviceRegister.getApplicationName(), key.getAgentID());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.dao.PushNotificationDao#getDeviceDetails(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<DeviceRegister>> getDeviceDetails(Request<DeviceRegister> request) {
        final Response<List<DeviceRegister>> deviceIdRespones = new ResponseImpl<List<DeviceRegister>>();
        List<DeviceRegister> deviceRegisterList = new ArrayList<DeviceRegister>();
        DeviceRegister deviceRegister = request.getType();
        final CompositeKey key = deviceRegister.getKey();
        String query =
                "select deviceReg from  DeviceRegister deviceReg where deviceReg.applicationName =?1 and deviceReg.key.agentID = ?2 and deviceReg.status='E'";
        if (!("ALL".equals(deviceRegister.getPlatformtype()))) {
            query = query + " and deviceReg.platformtype =?3";
            deviceRegisterList =
                    findByQuery(request, query, deviceRegister.getApplicationName(), key.getAgentID(),
                            deviceRegister.getPlatformtype());
        } else {
            deviceRegisterList =
                    findByQuery(request, query, deviceRegister.getApplicationName(), key.getAgentID());
        }

        deviceIdRespones.setType(deviceRegisterList);
        return deviceIdRespones;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.dao.PushNotificationDao#getApplicationCredentials(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public Response<List<ApplicationDetails>> getApplicationDetails(Request<ApplicationDetails> applnDetailsRequest) {
        final Response<List<ApplicationDetails>> applnDetailsResponse = new ResponseImpl<List<ApplicationDetails>>();
        List<ApplicationDetails> applicationDetailsList = new ArrayList<ApplicationDetails>();

        ApplicationDetails applnDetails = applnDetailsRequest.getType();
        String query =
                "select applnDetails from ApplicationDetails applnDetails where applnDetails.applicationName =?1";
        if (!("ALL".equals(applnDetails.getPlatformType()))) {
            query = query + " and applnDetails.platformType =?2";
            applicationDetailsList =
                    findByQuery(applnDetailsRequest, query, applnDetails.getApplicationName(),
                            applnDetails.getPlatformType());
        } else {
            applicationDetailsList = findByQuery(applnDetailsRequest, query, applnDetails.getApplicationName());
        }

        applnDetailsResponse.setType(applicationDetailsList);
        return applnDetailsResponse;
    }

}
