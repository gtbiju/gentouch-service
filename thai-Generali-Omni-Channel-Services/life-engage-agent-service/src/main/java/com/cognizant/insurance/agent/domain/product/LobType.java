/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class LobType.
 *
 * @author 304007
 */

@Entity
@DiscriminatorValue("Lob")
public class LobType extends Type {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 203226768877951932L;

    /**
     * The Constructor.
     */
    public LobType() {
        super();
    }

    /**
     * The Constructor.
     *
     * @param id the id
     */
    public LobType(final String id) {
        super(id);
    }
    
    /** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "lineOfBusiness")
    private Set<ProductSpecification> productSpecifications;

    /**
     * @param productSpecifications
     *            the productSpecifications to set
     */
    public void setProductSpecifications(Set<ProductSpecification> productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    /**
     * @return the productSpecifications
     */
    public Set<ProductSpecification> getProductSpecifications() {
        return productSpecifications;
    }
}
