package com.cognizant.insurance.omnichannel.component;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.omnichannel.domain.TinyUrlGenerator;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.vo.RequestInfo;

public interface GeneraliPaymentComponent {
	
	public String makePaymentViaOnline(RequestInfo requestInfo,String json) throws BusinessException;
	
	//public String getCardPaymentStatus(String transactionRefNumber) throws BusinessException;
	
	public String getUrlPaymentStatus(String json) throws BusinessException;
	
	public String processNotifaction(String notification) throws BusinessException;
	
	public String processResponse(String status,String transactionId,String data) throws BusinessException;
	
	String retrievePaymentService(String json) throws BusinessException,
    ParseException;
	
	String isRecieptNoExisting(JSONArray jsonTransactionArray,
			RequestInfo requestInfo) throws BusinessException;
	
	public String getAuthenticationToken ()throws BusinessException;

	public String makePaymentViaURL(JSONObject jsonObject, String transactionId) throws BusinessException;
	
	//public String retrieveOriginalPaymentURL(JSONObject jsonObject, String transactionId) throws BusinessException;

	public TinyUrlGenerator makePaymentViaUrl(String uuid, String id);

	public String retrieveAndSendMail(JSONObject jsonObject, String randomUUID,RequestInfo requestInfo);

	LifeEngageEmail saveGeneraliEmail(JSONObject jsonObj);

	void triggerEmail(String agentId, String type);

}
