package com.cognizant.le.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cognizant.le.manager.AgentServiceManager;
import com.cognizant.le.model.dto.LoginStatus;

public class RetrieveActiveAgents extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(RetrieveActiveAgents.class);
	
	private WebApplicationContext springContext;
	
	@Autowired
	AgentServiceManager service;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RetrieveActiveAgents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 *//*
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}*/

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 LOGGER.debug("Method Entry : RetrieveActiveAgents.doPost");
		
		JSONObject activeAgentDetailsJSON = new JSONObject();
		Boolean isValidSession = false;
	    HttpSession session = request.getSession(true);
	    
	    String agentId = request.getParameter("agentname");
       String[] agentCodeParts = null;
       if (null != agentId) {
           agentCodeParts = agentId.split("-");
       }

       String agentCode = agentCodeParts[0];
	  
       try {
       	activeAgentDetailsJSON = service.getAgentDetails(agentCode, "activeAgents");
           if (session != null) {

               isValidSession = true;
           }
           activeAgentDetailsJSON.getJSONObject("Response").getJSONObject("ResponseInfo").put("isValidSession", isValidSession);
           
           request.setAttribute("activeAgentDetailsJSON", activeAgentDetailsJSON);
           response.getWriter().print(activeAgentDetailsJSON);

       } catch (Exception e) {
           LOGGER.error(e.getMessage());
       }
       LOGGER.debug("Method Exit : RetrieveActiveAgents.doPost");			
		
	}
	
	 public void init(ServletConfig config)throws ServletException {
				super.init(config);
				 springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
			        final AutowireCapableBeanFactory beanFactory = springContext.getAutowireCapableBeanFactory();
			        beanFactory.autowireBean(this);	   
		  }
}
