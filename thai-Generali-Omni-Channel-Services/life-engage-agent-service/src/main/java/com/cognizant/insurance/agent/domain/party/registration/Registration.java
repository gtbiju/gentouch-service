package com.cognizant.insurance.agent.domain.party.registration;

import java.util.Date;

import javax.persistence.MappedSuperclass;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.party.Organization;



/**
 * A formal recording, by an authorized body, of the granting of rights, privileges, favors, statuses, or qualifications. Registrations are important from the perspective of being a qualified source of information. They are similar to assessments in the respect that the associated information can be considered more reliable than the same information not associated with a registration.
 */
@MappedSuperclass
public class Registration extends InformationModelObject {
	/**
	 * A free-text statement used to explain the meaning of the registration.
	 */
	public String  description;
	
	
	/**
	 * The date on which the request for the registration was filed with the registration authority. 
	 * Derived from the status related information.
	 */
	public Date requestDate;
	
	
	/**
	 * This is the registration identifier assigned by the registration authority.
	 * 
	 * e.g. driver's license number, vehicle registration number, passport number, tax id number, social security number (SSN), Federal Employment Information Number (FEIN), National Insurance Producer Registry Number (NIPR), International Securities Identification Number (ISIN), etc.
	 * 
	 * 
	 * 
	 *   
	 */
	public String registrationIdentifier;
	
	/**
	 * The identification of a party role player as the official registration authority of a registration.
	 * 
	 * e.g: The Maryland Department of Motor Vehicles is registration authority for the Registration of  David Dziwulski's driving license.
	 * 
	 * e.g: Dun & Bradstreet (D&B) is the registration authority for organizations with a D&B Number.
	 */
	public Organization isRegisteredBy;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getRegistrationIdentifier() {
		return registrationIdentifier;
	}

	public void setRegistrationIdentifier(String registrationIdentifier) {
		this.registrationIdentifier = registrationIdentifier;
	}

	public Organization getIsRegisteredBy() {
		return isRegisteredBy;
	}

	public void setIsRegisteredBy(Organization isRegisteredBy) {
		this.isRegisteredBy = isRegisteredBy;
	}

	
}
