package com.cognizant.le.model.dao;

public interface FileUploadDao {
	
	void uploadFile(String fileName, String type ,String user) throws Exception;

}
