/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Dec 5, 2013
 */
package com.cognizant.insurance.component.helper;

import java.util.Random;

/**
 * The Class class ProposalNumberGenerator.
 * 
 * @author 300797
 */
public final class LeadIdentifierGenerator {

    /** The Constant generator. */
    private static final Random GENERATOR = new Random();

    /** The million. */
    private static final int MILLION = 100000;

    /** The random. */
    private static final int RANDOM = 900000;

    /**
     * Instantiates a new proposal number generator.
     */
    private LeadIdentifierGenerator() {
    }

    /** The proposal number generator obj. */
    private static LeadIdentifierGenerator leadIdentifierGeneratorObj = new LeadIdentifierGenerator();

    /**
     * Gets the single instance of ProposalNumberGenerator.
     * 
     * @return single instance of ProposalNumberGenerator
     */
    public static LeadIdentifierGenerator getInstance() {
        return leadIdentifierGeneratorObj;
    }

    /**
     * Generate Proposal Number.
     * 
     * @return the Proposal Number
     */
    public int generateLeadIdentifier() {
        return MILLION + GENERATOR.nextInt(RANDOM);
    }
}
