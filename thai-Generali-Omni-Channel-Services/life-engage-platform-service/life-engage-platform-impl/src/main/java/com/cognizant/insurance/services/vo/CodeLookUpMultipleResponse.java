/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jan 23, 2014
 */
package com.cognizant.insurance.services.vo;

import java.util.List;

import com.cognizant.insurance.response.vo.ResponseInfo;

/**
 * The Class class CodeLookUpResponse.
 * 
 * @author 300797
 */
public class CodeLookUpMultipleResponse {

	/** The response info. */
	private ResponseInfo responseInfo;

	/** The code look up transactions. */
	private List<CodeLookUpTransaction> codeLookUpTransactions;

	/**
	 * Gets the code look up transactions.
	 * 
	 * @return the codeLookUpTransactions
	 */
	public List<CodeLookUpTransaction> getCodeLookUpTransactions() {
		return codeLookUpTransactions;
	}

	/**
	 * Sets the code look up transactions.
	 * 
	 * @param codeLookUpTransactions
	 *            the codeLookUpTransactions to set
	 */
	public void setCodeLookUpTransactions(
			List<CodeLookUpTransaction> codeLookUpTransactions) {
		this.codeLookUpTransactions = codeLookUpTransactions;
	}

	/**
	 * Gets the response info.
	 * 
	 * @return the responseInfo
	 */
	public final ResponseInfo getResponseInfo() {
		return responseInfo;
	}

	/**
	 * Sets the response info.
	 * 
	 * @param responseInfo
	 *            the responseInfo to set
	 */
	public final void setResponseInfo(final ResponseInfo responseInfo) {
		this.responseInfo = responseInfo;
	}

}
