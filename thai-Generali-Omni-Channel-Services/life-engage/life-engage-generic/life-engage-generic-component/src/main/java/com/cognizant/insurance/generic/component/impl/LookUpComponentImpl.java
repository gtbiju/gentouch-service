/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.component.impl;

import com.cognizant.insurance.generic.component.LookUpComponent;
import com.cognizant.insurance.generic.dao.CountryDetailsDao;
import com.cognizant.insurance.generic.dao.impl.CountryDetailsDaoImpl;
import com.cognizant.insurance.lookup.CountryDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.vo.CountryLookUpResponse;

/**
 * @author 300797
 *
 */
public class LookUpComponentImpl implements LookUpComponent {

    /* (non-Javadoc)
     * @see com.cognizant.insurance.audit.component.AuditComponent#savelifeEngageAudit(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<CountryLookUpResponse> getCountrySpecificDetails(Request<CountryDetails> request) {
        
        final CountryDetailsDao countryDetailsDao = new CountryDetailsDaoImpl();  
        return countryDetailsDao.retrieve(request);       
    }

    /* (non-Javadoc)
     * @see com.cognizant.insurance.generic.component.LookUpComponent#getCountryOrCountrySubDivisionList(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<CountryLookUpResponse> getCountryOrCountrySubDivisionList(
            Request<CountryDetails> request) {
        
        final CountryDetailsDao countryDetailsDao = new CountryDetailsDaoImpl();  
        return countryDetailsDao.retrieveCountryOrCountrySubDivisionList(request);   
    }

}
