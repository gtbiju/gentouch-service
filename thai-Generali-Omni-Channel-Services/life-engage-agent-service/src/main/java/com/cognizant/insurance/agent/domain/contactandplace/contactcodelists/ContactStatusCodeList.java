/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.contactcodelists;

/**
 * A classification of the various statuses that a contact point can be in.
 * 
 * @author 301350
 * 
 * 
 */
public enum ContactStatusCodeList {
    /**
     * The contact point is active.
     * 
     * 
     * 
     */
    Active,
    /**
     * The contact is not active anymore. It is very unlikely that communication
     * to this contact will be successfully delivered.
     * 
     * e.g: A phone number that is not attributed anymore.
     * 
     * e.g: An email address from an out-of-service domain.
     * 
     * e.g: An email address that is not used anymore.
     * 
     * 
     * 
     */
    Inactive,
    /**
     * The contact point is listed. Although it would normally be made publicly
     * available, it has been explicitly asked for it to remain confidential.
     * 
     * e.g: Home phone numbers for which their owner asked their phone company
     * not to be included in the local phone book.
     * 
     * 
     * 
     */
    Listed,
    /**
     * The contact is not published. It is not meant to be made publicly
     * available.
     * 
     * e.g: Phone extension numbers in a company.
     * 
     * 
     * 
     */
    NotPublished,
    /**
     * The contact is not listed. It is therefore publicly unavailable.
     * 
     * 
     * 
     */
    Unlisted
}
