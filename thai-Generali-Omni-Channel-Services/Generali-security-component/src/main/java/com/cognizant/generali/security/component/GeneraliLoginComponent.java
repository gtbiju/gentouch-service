package com.cognizant.generali.security.component;



public interface GeneraliLoginComponent {
	
	public boolean validateAgentLicenseExpiry(String agentCode);
	
	public boolean validateAgentStatus(String agentCode);

}
