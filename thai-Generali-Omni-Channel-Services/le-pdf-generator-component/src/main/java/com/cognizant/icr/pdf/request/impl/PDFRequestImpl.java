/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.icr.pdf.request.impl;

import java.util.Map;

import com.cognizant.icr.pdf.request.PDFRequest;

/**
 * The Class class PDFRequestImpl.
 *
 * @param <T> the generic type
 */
public class PDFRequestImpl<T> implements PDFRequest<T> {
	
	/** The template id. */
	private String templateID;
	
	
	/** The data objs map. */
    private Map<String, T> dataObjsMap;
	
	/** The template version. */
	private String templateVersion;
	
	/** The user id. */
	private String userId;
	
	/** The template type. */
	private String templateType;
	
	/** The owner password. */
	private String ownerPassword;
	
	/** The user password. */
	private String userPassword;
	
	

	/**
	 * Gets the template id.
	 *
	 * @return the template id
	 */
	public final String getTemplateID() {
		return templateID;
	}

	
	/**
	 * Sets the template id.
	 *
	 * @param templateID the template id to set.
	 */
	public final void setTemplateID(final String templateID) {
		this.templateID = templateID;
	}
	


	/**
	 * Gets the template version.
	 *
	 * @return the template version
	 */
	public final String getTemplateVersion() {
		return templateVersion;
	}


	/**
	 * Sets the template version.
	 *
	 * @param templateVersion the new template version
	 */
	public final void setTemplateVersion(final String templateVersion) {
		this.templateVersion = templateVersion;
	}


	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public final String getUserId() {
		return userId;
	}


	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public final void setUserId(final String userId) {
		this.userId = userId;
	}


	

	/**
	 * Gets the data objs map.
	 *
	 * @return the data objs map
	 */
	public final Map<String, T> getDataObjsMap() {
		return dataObjsMap;
	}


	/**
	 * Sets the data objs map.
	 *
	 * @param dataObjsMap the data objs map
	 */
	public final void setDataObjsMap(final Map<String, T> dataObjsMap) {
		this.dataObjsMap = dataObjsMap;
	}


	/**
	 * Gets the template type.
	 *
	 * @return the template type
	 */
	public final String getTemplateType() {
		return templateType;
	}


	/**
	 * Sets the template type.
	 *
	 * @param templateType the template type to set.
	 */
	public final void setTemplateType(final String templateType) {
		this.templateType = templateType;
	}


	/**
	 * Gets the owner password.
	 *
	 * @return the owner password
	 */
	public String getOwnerPassword() {
		return ownerPassword;
	}


	/**
	 * Sets the owner password.
	 *
	 * @param ownerPassword the new owner password
	 */
	public void setOwnerPassword(String ownerPassword) {
		this.ownerPassword = ownerPassword;
	}


	/**
	 * Gets the user password.
	 *
	 * @return the user password
	 */
	public String getUserPassword() {
		return userPassword;
	}


	/**
	 * Sets the user password.
	 *
	 * @param userPassword the new user password
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	
		
	
}
