/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partycodelists;

/**
 * Allowed life cycle states for an organization. Used for all statuses other
 * than foundation and dissolution.
 * 
 * @author 301350
 * 
 * 
 */
public enum OrganizationStatusCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Active,
    /**
     * 
     * 
     * 
     * 
     */
    InSeparation,
    /**
     * 
     * 
     * 
     * 
     */
    Stopped
}
