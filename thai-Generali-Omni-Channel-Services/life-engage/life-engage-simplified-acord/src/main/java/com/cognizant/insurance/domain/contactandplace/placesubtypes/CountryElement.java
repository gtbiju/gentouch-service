/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.placesubtypes;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.cognizant.insurance.domain.contactandplace.Place;

/**
 * The top-level generalization for the parts of a country (from a geographical
 * point of view). Subtypes of this concept, which can be extended, represent
 * the geography-specific parts of a country. Many of the country elements are
 * used by many countries.
 * 
 * e.g., State (United State) e.g., Department (France) e.g., Province (Canada)
 * 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public abstract class CountryElement extends Place {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4524487504760679277L;
    /**
     * The standard abbreviation or code for the geographic area.
     * 
     * e.g: NY
     * 
     * 
     * 
     */
    @Transient
    private String abbreviation;

    /**
     * @return the abbreviation
     * 
     * 
     */
    public String getAbbreviation() {

        return abbreviation;
    }

    /**
     * @param abbreviation
     *            the abbreviation to set
     * 
     * 
     */
    public void setAbbreviation(final String abbreviation) {

        this.abbreviation = abbreviation;
    }
}
