/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.agreementcodelists;

/**
 * Loan Interest Timing.
 * 
 * @author 301350
 * 
 * 
 */
public enum LoanInterestTimingCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    InterestinAdvance,
    /**
     * 
     * 
     * 
     * 
     */
    InterestinArrears,
    /**
     * 
     * 
     * 
     * 
     */
    Other
}
