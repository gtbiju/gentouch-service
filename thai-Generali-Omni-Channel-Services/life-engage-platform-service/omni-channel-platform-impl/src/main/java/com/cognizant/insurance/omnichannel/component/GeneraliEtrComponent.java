package com.cognizant.insurance.omnichannel.component;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;

import org.json.JSONObject;

import com.cognizant.insurance.core.exception.BusinessException;

public interface GeneraliEtrComponent {

   String retrieveEtr(JSONObject json,String agentId) throws BusinessException,ParseException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, IOException;
}
