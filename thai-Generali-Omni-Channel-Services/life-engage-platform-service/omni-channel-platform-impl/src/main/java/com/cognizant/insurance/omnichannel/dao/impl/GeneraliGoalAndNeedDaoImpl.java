package com.cognizant.insurance.omnichannel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.goalandneed.dao.impl.GoalAndNeedDaoImpl;
import com.cognizant.insurance.omnichannel.dao.GeneraliGoalAndNeedDao;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public class GeneraliGoalAndNeedDaoImpl extends GoalAndNeedDaoImpl implements GeneraliGoalAndNeedDao {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.omnichannel.dao.GeneraliGoalAndNeedDaoImpl#retrieveRelatedTransactions(com.cognizant.
     * insurance.request .Request)
     */
    @Override
    public Response<List<GoalAndNeed>> retrieveRelatedTransactions(Request<SearchCriteria> searchCriteriatRequest) {
        final Response<List<GoalAndNeed>> goalAndNeedResponse = new ResponseImpl<List<GoalAndNeed>>();
        List<Object> goalAndNeedObjList = null;
        String query;
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select goalAndNeed from GoalAndNeed goalAndNeed "
                                + "WHERE goalAndNeed.contextId=?1 and goalAndNeed.agentId=?2 and"
                                + "(goalAndNeed.transactionKeys.key1 like ?3 or goalAndNeed.transTrackingId like ?3 and goalAndNeed.status != 'Cancelled')";
                goalAndNeedObjList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteriatRequest.getContextId(),
                                searchCriteria.getAgentId(), "%" + searchCriteria.getValue() + "%");

            } else {
                query =
                        "select goalAndNeed from GoalAndNeed goalAndNeed "
                                + "WHERE goalAndNeed.contextId=?1 and goalAndNeed.agentId=?2 and goalAndNeed.status !='Cancelled'";
                goalAndNeedObjList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteriatRequest.getContextId(),
                                searchCriteria.getAgentId());
            }

            final List<GoalAndNeed> goalAndNeeds = new ArrayList<GoalAndNeed>();
            if (CollectionUtils.isNotEmpty(goalAndNeedObjList)) {
                for (Object goalObj : goalAndNeedObjList) {
                    goalAndNeeds.add((GoalAndNeed) goalObj);
                }
            }
            goalAndNeedResponse.setType(goalAndNeeds);
        }
        return goalAndNeedResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.omnichannel.dao.GeneraliGoalAndNeedDaoImpl#getGoalAndNeedCountforLMS(com.cognizant.insurance
     * .request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<SearchCountResult> getGoalAndNeedCountforLMS(Request<SearchCriteria> searchCriteriatRequest) {
        final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
        List<Object> searchCriteriaList = null;
        String type = null;
        String query;
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            type = searchCriteria.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select count(goalAndNeed.identifier),goalAndNeed.status from GoalAndNeed goalAndNeed where goalAndNeed.agentId =?1 "
                                + "and goalAndNeed.contextId =?2 and goalAndNeed.transactionKeys.key1 like ?3 and goalAndNeed.status != 'Cancelled' group by goalAndNeed.status";

                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                searchCriteriatRequest.getContextId(), "%" + searchCriteria.getValue() + "%");

            }

        }
        final SearchCountResult searchCountResult = new SearchCountResult();

        Long totalModCount = 0l;
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object searchCriteria : searchCriteriaList) {
                Object[] values = (Object[]) searchCriteria;
                Long count = (Long) values[0];
                totalModCount += count;
            }

        }
        searchCountResult.setCount(String.valueOf(totalModCount));
        searchCountResult.setMode(type);
        searchCountResponse.setType(searchCountResult);
        return searchCountResponse;
    }

    public void updateGoalAndNeedStatusForLeadReassign(Request<GoalAndNeed> goalAndNeedRequest) {
    	if (goalAndNeedRequest.getType() != null) {
    		final GoalAndNeed goalAndNeed = (GoalAndNeed) goalAndNeedRequest.getType();

    		GoalAndNeed goalAndNeedData = goalAndNeed;
    		goalAndNeedData.setStatus(goalAndNeed.getStatus());
    		goalAndNeedRequest.setType(goalAndNeedData);
    		merge(goalAndNeedRequest);

    	}
    }

}
