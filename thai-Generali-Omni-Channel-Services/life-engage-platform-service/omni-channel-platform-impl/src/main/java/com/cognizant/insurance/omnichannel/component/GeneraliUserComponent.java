package com.cognizant.insurance.omnichannel.component;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cognizant.insurance.core.exception.RecentPasswordUsageException;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.InputValidationException;
import com.cognizant.insurance.request.vo.RequestInfo;


public interface GeneraliUserComponent {

    public String register(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException;	

	public String createPassword(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException;	

	public String forgetPassword(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException;
	
	public String retrieveLeads(RequestInfo requestInfo,JSONArray jsonRqArray)throws InputValidationException;
	
	public String resetPassword(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException, RecentPasswordUsageException;	
	
	String validateAgentCode(JSONObject jsonObject, RequestInfo requestInfo);			
}
