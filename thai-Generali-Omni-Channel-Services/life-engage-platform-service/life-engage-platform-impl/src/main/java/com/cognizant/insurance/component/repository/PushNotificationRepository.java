/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 19, 2015
 */
package com.cognizant.insurance.component.repository;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformApplicationRequest;
import com.amazonaws.services.sns.model.CreatePlatformApplicationResult;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.cognizant.insurance.component.impl.LifeEngagePushNotificationComponentImpl;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.generic.component.PushNotificationComponent;
import com.cognizant.insurance.pushnotification.ApplicationDetails;
import com.cognizant.insurance.pushnotification.DeviceRegister;
import com.cognizant.insurance.pushnotification.SNSDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;

//@Repository
/**
 * The Class class PushNotificationRepository.
 */
public class PushNotificationRepository {

    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;

    /** The sns endpoint. */
    @Value("${le.platform.service.snsEndpoint}")
    private String snsEndpoint;

    /** The aws credential file. */
    @Value("${le.platform.service.awsCredentialFile}")
    private String awsCredentialFile;

    /** The push notification component. */
    @Autowired
    private PushNotificationComponent pushNotificationComponent;

    /** The sns client. */
    private AmazonSNS snsClient;

    /** The platform application arn. */
    protected String platformApplicationArn = null;

    /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = "TransactionData";

    /** The Constant TransactionData. */
    private static final String PUSHNOTIFICATIONS_REQUEST = "pushNotificationsRequest";

    /** The Constant MESSAGE. */
    private static final String MESSAGE = "Message";

    /** The successcount. */
    protected static int successcount;

    /** The error count. */
    protected static int errorCount;
    
    public static Logger LOGGER = LoggerFactory.getLogger(PushNotificationRepository.class);

    /**
     * Register device.
     * 
     * @param requestInfo
     *            the request info
     * @param transactions
     *            the transactions
     * @throws ParseException
     *             the parse exception
     */
    public void registerDevice(final RequestInfo requestInfo, final Transactions transactions) throws ParseException {

        final Request<DeviceRegister> registerDevicerRq =
                new JPARequestImpl<DeviceRegister>(entityManager, ContextTypeCodeList.PUSH_NOTIFICATION,
                        requestInfo.getTransactionId());
        DeviceRegister deviceRegister = transactions.getDeviceRegister();
        deviceRegister.setStatus(Constants.ENABLE);
        deviceRegister.setRequestedTime(new java.util.Date());
        registerDevicerRq.setType(deviceRegister);
        boolean isDeviceIdExists = pushNotificationComponent.isDeviceIdExists(registerDevicerRq);
        if (!isDeviceIdExists) {
            pushNotificationComponent.registerDevice(registerDevicerRq);
        } 
    }

    /**
     * Un register device.
     * 
     * @param requestInfo
     *            the request info
     * @param transactions
     *            the transactions
     * @throws ParseException
     *             the parse exception
     */
    public void unRegisterDevice(final RequestInfo requestInfo, final Transactions transactions) throws ParseException {

        final Request<DeviceRegister> unRegisterDevicerRq =
                new JPARequestImpl<DeviceRegister>(entityManager, ContextTypeCodeList.PUSH_NOTIFICATION,
                        requestInfo.getTransactionId());
        DeviceRegister deviceRegister = transactions.getDeviceRegister();
        deviceRegister.setStatus(Constants.DISABLE);
        unRegisterDevicerRq.setType(deviceRegister);
        pushNotificationComponent.unRegisterDevice(unRegisterDevicerRq);

    }

    /**
     * Push notifications.
     * 
     * @param requestInfo
     *            the request info
     * @param transactions
     *            the transactions
     * @param json
     *            the json
     * @return true, if successful
     * @throws ParseException
     *             the parse exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public boolean pushNotifications(final RequestInfo requestInfo, final Transactions transactions, final String json)
            throws ParseException, IOException {

        Map<String, List<DeviceRegister>> deviceIdMap = new HashMap<String, List<DeviceRegister>>();
        SNSDetails snsServices = transactions.getSnsServices();
        snsServices.setSnsRequestTime(new java.util.Date());

        final JSONObject transactionJsonObject = new JSONObject(json);
        JSONObject jsonTransactionData = transactionJsonObject.getJSONObject(TRANSACTIONDATA);
        JSONObject pushNotificationJson = jsonTransactionData.getJSONObject(PUSHNOTIFICATIONS_REQUEST);
        JSONArray messagearray = pushNotificationJson.getJSONArray(MESSAGE);
        snsServices.setRequestJSON(pushNotificationJson.toString());
        snsServices.setMessage(messagearray.get(0).toString());

        ApplicationDetails applnDetails = new ApplicationDetails();
        applnDetails.setApplicationName(snsServices.getApplicationName());
        applnDetails.setPlatformType(snsServices.getPlatformFilter());

        List<ApplicationDetails> applicationDetailsList = getApplicationDetails(requestInfo, applnDetails);
        if (applicationDetailsList.isEmpty()) {
            throwSystemException(true, Constants.ERROR_CODE_400, Constants.INVALID_APPLICATION_INFORMATION);
        }

        DeviceRegister deviceRegister = transactions.getDeviceRegister();
        List<DeviceRegister> deviceList = getDeviceDetails(requestInfo, deviceRegister);

        if (deviceList.isEmpty()) {
            throwSystemException(true, Constants.ERROR_CODE_400, Constants.INVALID_DEVICE_INFORMATION);
        } else {
            deviceIdMap = getDeviceDetailsMap(deviceList);

        }

        successcount = 0;
        errorCount = 0;
        for (ApplicationDetails applicationDetails : applicationDetailsList) {
            platformApplicationArn = null;
            List<DeviceRegister> deviceRegList = deviceIdMap.get(applicationDetails.getPlatformType());
            if (deviceRegList.size() > 0) {
                sendNotifications(requestInfo, deviceRegList, applicationDetails, snsServices);
            }
        }
        return getStatusMessage();

    }

    /**
     * Gets the status message.
     * 
     * @return the status message
     */
    private boolean getStatusMessage() {
        boolean isSuccess = false;
        if (successcount > 0 && errorCount == 0) {
            isSuccess = true;
        } else if (errorCount > 0 && successcount == 0) {
            throwSystemException(true, Constants.ERROR_CODE_402, Constants.NOTIFICATION_ERROR);
        } else if (successcount > 0 && errorCount > 0) {
            isSuccess = false;
        }
        return isSuccess;
    }

    /**
     * Gets the device details map.
     * 
     * @param deviceList
     *            the device list
     * @return the device details map
     */
    protected Map<String, List<DeviceRegister>> getDeviceDetailsMap(List<DeviceRegister> deviceList) {
        Map<String, List<DeviceRegister>> deviceIdMap = new HashMap<String, List<DeviceRegister>>();

        List<DeviceRegister> gcmList = new ArrayList<DeviceRegister>();
        List<DeviceRegister> apnsList = new ArrayList<DeviceRegister>();
        List<DeviceRegister> apnsSandBoxList = new ArrayList<DeviceRegister>();

        for (DeviceRegister deviceReg : deviceList) {
            String platformType = deviceReg.getPlatformtype();           
            if (Constants.GCM.equals(platformType)) {
                gcmList.add(deviceReg);
            } else if (Constants.APNS.equals(platformType)) {
                apnsList.add(deviceReg);
            } else {
                apnsSandBoxList.add(deviceReg);
            }

        }
        deviceIdMap.put(Constants.GCM, gcmList);
        deviceIdMap.put(Constants.APNS, apnsList);
        deviceIdMap.put(Constants.APNS_SANDBOX, apnsSandBoxList);
        return deviceIdMap;

    }

    /**
     * Main action.
     * 
     * @param requestInfo
     *            the request info
     * @param deviceList
     *            the device list
     * @param applicationDetails
     *            the application details
     * @param snsDetails
     *            the sns services
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void sendNotifications(final RequestInfo requestInfo, List<DeviceRegister> deviceList,
        ApplicationDetails applicationDetails, SNSDetails snsDetails) throws IOException {
        String endPointArn = null;
        snsClient = new AmazonSNSClient(new PropertiesCredentials(getAWSCredentials()));
        snsClient.setEndpoint(snsEndpoint);
        for (DeviceRegister deviceRegister : deviceList) {
            try {

                if (deviceRegister.getSnsPlatformEndpointARN() != null
                        && !("").equalsIgnoreCase(deviceRegister.getSnsPlatformEndpointARN())) {
                    endPointArn = deviceRegister.getSnsPlatformEndpointARN();
                } else {
                	  LOGGER.info("platformApplicationArn :"+platformApplicationArn);
                    if (platformApplicationArn == null) {
                        if (applicationDetails.getPlatformApplicationArn() != null
                                && !("").equalsIgnoreCase(applicationDetails.getPlatformApplicationArn())) {
                            platformApplicationArn = applicationDetails.getPlatformApplicationArn();
                        } else {
                        	
                        	LOGGER.info("applicationDetails" + applicationDetails.getApplicationName()+applicationDetails.getAppPrivateKey());
                            // Create Platform Application. This corresponds to an app on a platform.
                            CreatePlatformApplicationResult platformApplicationResult =
                                   createPlatformApplication(applicationDetails);
                            
                            platformApplicationArn = platformApplicationResult.getPlatformApplicationArn();
                            applicationDetails.setPlatformApplicationArn(platformApplicationArn);
                            LOGGER.info("ARN Result = " + platformApplicationArn);

                        }
                    }

                    // Create an Endpoint. This corresponds to an app on a device.
                    CreatePlatformEndpointResult platformEndpointResult =
                            createPlatformEndpoint("CustomData - Useful to store endpoint specific data",
                                    deviceRegister.getDeviceID(), platformApplicationArn);
                    LOGGER.info("End Point Result = " + platformEndpointResult);
                    endPointArn = platformEndpointResult.getEndpointArn();
                    deviceRegister.setSnsPlatformEndpointARN(endPointArn);

                }
                // Publish a push notification to an Endpoint.
                PublishResult publishResult =
                        publish(endPointArn, applicationDetails.getPlatformType(), snsDetails.getMessage());

                snsDetails.setDeviceId(deviceRegister.getDeviceID());
                snsDetails.setSnsStatus(Constants.SUCCESS);
                snsDetails.setSnsStatusMessage(publishResult.getMessageId());
                snsDetails.setSnsReceivedTime(new java.util.Date());
                savePushNotificationDetails(requestInfo, snsDetails);
                successcount++;
                LOGGER.info("Exit");

            } catch (Exception e) {
                LOGGER.error("ERROR IN SENDING PUSH NOITIFICATIONS",e);
                errorCount++;
                snsDetails.setDeviceId(deviceRegister.getDeviceID());
                snsDetails.setSnsStatus(Constants.FAILURE);
                snsDetails.setSnsStatusMessage(e.getMessage());
                snsDetails.setSnsReceivedTime(new java.util.Date());
                savePushNotificationDetails(requestInfo, snsDetails);
            }
        }

    }

    /**
     * Creates the platform application.
     * 
     * @param applicationDetails
     *            the application details
     * @return the creates the platform application result
     */
    public CreatePlatformApplicationResult createPlatformApplication(ApplicationDetails applicationDetails) {
    	LOGGER.info("INSIDE createPlatformApplication");
        CreatePlatformApplicationRequest platformApplicationRequest = new CreatePlatformApplicationRequest();
        Map<String, String> attributes = new HashMap<String, String>();
        if ("GCM".equals(applicationDetails.getPlatformType())) {
            attributes.put("PlatformPrincipal", "NA");
        } else {
            attributes.put("PlatformPrincipal", applicationDetails.getAppCertificatekey());
        }
        attributes.put("PlatformCredential", applicationDetails.getAppPrivateKey());
        platformApplicationRequest.setAttributes(attributes);
        platformApplicationRequest.setName(applicationDetails.getApplicationName());
        platformApplicationRequest.setPlatform(applicationDetails.getPlatformType());
        return snsClient.createPlatformApplication(platformApplicationRequest);
    	
    }

    /**
     * Creates the platform endpoint.
     * 
     * @param customData
     *            the custom data
     * @param platformToken
     *            the platform token
     * @param applicationArn
     *            the application arn
     * @return the creates the platform endpoint result
     */
    private CreatePlatformEndpointResult createPlatformEndpoint(String customData, String platformToken,
            String applicationArn) {
        CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();
        platformEndpointRequest.setCustomUserData(customData);
        platformEndpointRequest.setToken(platformToken);
        platformEndpointRequest.setPlatformApplicationArn(applicationArn);
        return snsClient.createPlatformEndpoint(platformEndpointRequest);
    }

    /**
     * Gets the device details.
     * 
     * @param requestInfo
     *            the request info
     * @param deviceRegister
     *            the device register
     * @return the device details
     * @throws ParseException
     *             the parse exception
     */
    public List<DeviceRegister> getDeviceDetails(final RequestInfo requestInfo, final DeviceRegister deviceRegister)
            throws ParseException {
        Response<List<DeviceRegister>> response = null;

        final Request<DeviceRegister> registerDevicerRq =
                new JPARequestImpl<DeviceRegister>(entityManager, ContextTypeCodeList.PUSH_NOTIFICATION,
                        requestInfo.getTransactionId());
        registerDevicerRq.setType(deviceRegister);
        response = pushNotificationComponent.getDeviceDetails(registerDevicerRq);

        return response.getType();

    }

    /**
     * Gets the application credentials.
     * 
     * @param requestInfo
     *            the request info
     * @param applicaionDetails
     *            the applicaion details
     * @return the application credentials
     * @throws ParseException
     *             the parse exception
     */
    public List<ApplicationDetails> getApplicationDetails(final RequestInfo requestInfo,
            final ApplicationDetails applicaionDetails) throws ParseException {
        Response<List<ApplicationDetails>> response = null;
        final Request<ApplicationDetails> applinDetailsRq =
                new JPARequestImpl<ApplicationDetails>(entityManager, ContextTypeCodeList.PUSH_NOTIFICATION,
                        requestInfo.getTransactionId());
        applinDetailsRq.setType(applicaionDetails);
        response = pushNotificationComponent.getApplicationDetails(applinDetailsRq);
        return response.getType();

    }

    /**
     * Save push notification details.
     * 
     * @param requestInfo
     *            the request info
     * @param snsServices
     *            the sns services
     */
    public void savePushNotificationDetails(final RequestInfo requestInfo, final SNSDetails snsServices) {
        final Request<SNSDetails> snsServiceRq =
                new JPARequestImpl<SNSDetails>(entityManager, ContextTypeCodeList.PUSH_NOTIFICATION,
                        requestInfo.getTransactionId());
        snsServiceRq.setType(snsServices);
        pushNotificationComponent.savePushNotificationDetails(snsServiceRq);

    }

    /**
     * Gets the aWS credentials.
     * 
     * @return the aWS credentials
     */
    private InputStream getAWSCredentials() {
        return getClass().getClassLoader().getResourceAsStream(awsCredentialFile);
    }

    /**
     * Publish.
     * 
     * @param endpointArn
     *            the endpoint arn
     * @param platform
     *            the platform
     * @param msg
     *            the msg
     * @return the publish result
     */
    private PublishResult publish(String endpointArn, String platform, String msg) {
        PublishRequest publishRequest = new PublishRequest();
        Map<String, String> messageMap = new HashMap<String, String>();
        String message;
        messageMap.put("default", msg);
        messageMap.put(platform, getPlatformMessage(platform, msg));
        // For direct publish to mobile end points, topicArn is not relevant.
        publishRequest.setTargetArn(endpointArn);
        publishRequest.setMessageStructure("json");

        message = jsonify(messageMap);

        // Display the message that will be sent to the endpoint/
        System.out.println(message);

        publishRequest.setMessage(message);
        return snsClient.publish(publishRequest);
    }

    /**
     * Jsonify.
     * 
     * @param message
     *            the message
     * @return the string
     */
    private static String jsonify(Object message) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(message);
        } catch (Exception e) {
            e.printStackTrace();
            throw (RuntimeException) e;
        }
    }

    /**
     * Gets the platform message.
     * 
     * @param platform
     *            the platform
     * @param message
     *            the message
     * @return the platform message
     */
    private String getPlatformMessage(String platform, String message) {
        String platformMessage = "";
        if (Constants.APNS.equals(platform)) {
            platformMessage = getAppleMessage(message);
        } else if (Constants.APNS_SANDBOX.equals(platform)) {
            platformMessage = getAppleMessage(message);
        } else if (Constants.GCM.equals(platform)) {
            platformMessage = getAndroidMessage(message);
        }
        return platformMessage;
    }

    /**
     * Gets the apple message.
     * 
     * @param message
     *            the message
     * @return the apple message
     */
    private String getAppleMessage(String message) {
        Map<String, Object> appleMessageMap = new HashMap<String, Object>();
        Map<String, Object> appMessageMap = new HashMap<String, Object>();
        appMessageMap.put("alert", message);
        appMessageMap.put("badge", 1);
        appMessageMap.put("sound", "default");
        appleMessageMap.put("aps", appMessageMap);
        return jsonify(appleMessageMap);
    }

    /**
     * Gets the android message.
     * 
     * @param message
     *            the message
     * @return the android message
     */
    private String getAndroidMessage(String message) {
        Map<String, Object> androidMessageMap = new HashMap<String, Object>();
        androidMessageMap.put("collapse_key", "Welcome");
        androidMessageMap.put("data", getData(message));
        androidMessageMap.put("delay_while_idle", true);
        androidMessageMap.put("time_to_live", 125);
        androidMessageMap.put("dry_run", false);
        return jsonify(androidMessageMap);
    }

    /**
     * Gets the data.
     * 
     * @param message
     *            the message
     * @return the data
     */
    private Map<String, String> getData(String message) {
        Map<String, String> payload = new HashMap<String, String>();
        payload.put("message", message);
        return payload;
    }

}
