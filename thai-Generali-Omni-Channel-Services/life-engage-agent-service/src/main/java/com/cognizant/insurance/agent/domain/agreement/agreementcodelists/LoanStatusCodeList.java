/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.agreementcodelists;

/**
 * Loan Status.
 * 
 * @author 301350
 * 
 * 
 */
public enum LoanStatusCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    Available,
    /**
     * 
     * 
     * 
     * 
     */
    Requested,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * 
     * 
     * 
     * 
     */
    Taken,
    /**
     * The status of the loan is delinquent. Past due notices have been sent.
     * 
     * 
     * 
     */
    Delinquent,
    /**
     * The status of the loan is defaulted. 
     * 
     * 
     * 
     */
    Defaulted
}
