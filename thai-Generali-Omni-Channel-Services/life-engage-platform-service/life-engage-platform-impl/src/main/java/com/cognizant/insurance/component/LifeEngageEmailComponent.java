package com.cognizant.insurance.component;

import org.json.JSONArray;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;


// TODO: Auto-generated Javadoc
/**
 * The Interface interface LifeEngageEmailComponent.
 */
public interface LifeEngageEmailComponent {
	
	StatusData sendEmail(final String json, final String templateName, final String templateValues, final String pdfName) throws BusinessException;
	
	void triggerEmail(final String agentId, final String type) throws BusinessException;
	
	String updateEmailDetails(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException;
	
	/**
     * Send sendEmailWithAttachment.
     * 
     * @param json
     *            the json
     * @param emailBody
     *            the emailBody
     * @param JSONArray
     *            the jsonEmailAttachmentsArr
     * @throws BusinessException
     *             the business exception
     */
    StatusData sendEmailWithAttachment(final String json, final String emailBody, final JSONArray jsonEmailAttachmentsArr) throws BusinessException;


}
