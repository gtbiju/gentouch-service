/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of households according to their home ownership.
 * 
 * @author 301350
 * 
 * 
 */
public enum HomeOwnershipCodeList {
    /**
     * Identifies a household with home ownership 'rent'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Rent,
    /**
     * Identifies a household with home ownership 'own'.
     * 
     * 
     * 
     * 
     */
    Own
}
