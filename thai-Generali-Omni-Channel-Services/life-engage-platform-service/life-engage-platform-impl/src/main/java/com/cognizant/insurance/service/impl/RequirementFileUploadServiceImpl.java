/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 325754
 * @version       : 0.1, Oct 2, 2015
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.LifeEngageRequirementDocFileComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.services.RequirementFileUploadService;

@Service("requirementDocFileUploadService")
public class RequirementFileUploadServiceImpl implements RequirementFileUploadService {
    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(RequirementFileUploadServiceImpl.class);

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The life engage document component. */
    @Autowired
    private LifeEngageRequirementDocFileComponent lifeEngageRequirementDocFileComponent;

    /**
     * 
     * @param inputJsonObj
     * @return
     * @throws BusinessException
     */

    @Transactional(rollbackFor = { Exception.class })
    public String uploadRequirement(String inputJsonObj) throws BusinessException {
        LOGGER.trace("Inside RequirementServiceImpl : uploadRequirement" + inputJsonObj);
        Transactions transactions = new Transactions();
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        RequestInfo requestInfo = null;
        String uploadResponse = null;
        try {
            JSONObject jsonObject = new JSONObject(inputJsonObj);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonRqArray.length() > 0) {
                final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
                try {
                    uploadResponse =
                            lifeEngageRequirementDocFileComponent.uploadRequirementDocFile(jsonObj.toString(),
                                    requestInfo.getTransactionId());
                    jsonRsArray.put(new JSONObject(uploadResponse));
                } catch (SystemException e) {
                    jsonRsArray.put(new JSONObject(e.getMessage()));
                } catch (Exception e) {
                    LOGGER.trace("Unknown Exception" + e.toString());
                    throwBusinessException(true, e.getMessage());
                }
                LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
            }
        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.Save : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        return jsonObjectRs.toString();
    }

    /**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }

    /**
     * Gets the status dat object.
     * 
     * @param status
     *            the status
     * @param statusCode
     *            the status code
     * @param statusMessage
     *            the status message
     * @return the status dat object
     */
    protected StatusData getStatusDatObject(final String status, final String statusCode, final String statusMessage) {
        final StatusData statusData = new StatusData();
        statusData.setStatus(status);
        statusData.setStatusCode(statusCode);
        statusData.setStatusMessage(statusMessage);

        return statusData;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.RequirementFileUploadService#uploadRequirementDocFile(java.lang.String)
     */
    @Override
    public String uploadRequirementDocFile(String inputJsonObj) throws BusinessException {
        LOGGER.trace("Inside RequirementFileUploadServiceImpl : uploadRequirement" + inputJsonObj);
        Transactions transactions = new Transactions();
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        RequestInfo requestInfo = null;
        String uploadResponse = null;
        try {
            JSONObject jsonObject = new JSONObject(inputJsonObj);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);

            if (jsonRqArray.length() > 0) {
                final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
                try {
                    LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), transactions, false);
                    uploadResponse =
                            lifeEngageRequirementDocFileComponent.uploadRequirementDocFile(jsonObj.toString(),
                                    requestInfo.getTransactionId());
                } catch (SystemException e) {
                    jsonRsArray.put(new JSONObject(e.getMessage()));
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                }
            }
            jsonRsArray.put(new JSONObject(uploadResponse));
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
        } catch (ParseException e) {
            LOGGER.trace("RequirementFileUploadServiceImpl : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        return jsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.RequirementFileUploadService#getRequirement(java.lang.String)
     */
    @Override
    public String getRequirementDocumentFile(String inputJsonObj) throws BusinessException {
        Transactions transactions = new Transactions();
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        RequestInfo requestInfo = null;
        String getFileResponse = null;
        try {
            JSONObject jsonObject = new JSONObject(inputJsonObj);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonRqArray.length() > 0) {
                final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
                try {
                    LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), transactions, false);
                    getFileResponse =
                            lifeEngageRequirementDocFileComponent.getRequirementDocFile(jsonObj,
                                    requestInfo.getTransactionId());
                } catch (SystemException e) {
                    jsonRsArray.put(new JSONObject(e.getMessage()));
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                }
            }
            jsonRsArray.put(new JSONObject(getFileResponse));
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
        } catch (ParseException e) {
            LOGGER.trace("RequirementFileUploadServiceImpl : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        return jsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.RequirementFileUploadService#getRequirementDocFilesList(java.lang.String)
     */
    @Override
    public String getRequirementDocFilesList(String inputJsonObj) throws BusinessException {
        Transactions transactions = new Transactions();
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        RequestInfo requestInfo = null;
        String getFileResponse = null;
        try {
            JSONObject jsonObject = new JSONObject(inputJsonObj);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonRqArray.length() > 0) {
                final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
                try {
                    LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), transactions, false);
                    getFileResponse =
                            lifeEngageRequirementDocFileComponent.getRequirementDocFileList(jsonObj,
                                    requestInfo.getTransactionId());
                } catch (SystemException e) {
                    jsonRsArray.put(new JSONObject(e.getMessage()));
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                }

            }
            jsonRsArray.put(new JSONObject(getFileResponse));
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
        } catch (ParseException e) {
            LOGGER.trace("RequirementFileUploadServiceImpl : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        return jsonObjectRs.toString();
    }
    
       
    /**
     * 
     * @param inputJsonObj
     * @return
     * @throws BusinessException
     */

    @Transactional(rollbackFor = { Exception.class })
    public String saveRequirement(String inputJsonObj) throws BusinessException {
        LOGGER.trace("Inside RequirementServiceImpl : saveRequirement" + inputJsonObj);
        Transactions transactions = new Transactions();
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        RequestInfo requestInfo = null;
        String saveReqResponse = null;
        try {
            JSONObject jsonObject = new JSONObject(inputJsonObj);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonRqArray.length() > 0) {
                final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
                try {
                	saveReqResponse =
                            lifeEngageRequirementDocFileComponent.saveRequirement(jsonObj.toString(),
                                    requestInfo.getTransactionId());
                    jsonRsArray.put(new JSONObject(saveReqResponse));
                } catch (SystemException e) {
                    jsonRsArray.put(new JSONObject(e.getMessage()));
                } catch (Exception e) {
                    LOGGER.trace("Unknown Exception" + e.toString());
                    throwBusinessException(true, e.getMessage());
                }
                LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
            }
        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.Save : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        return jsonObjectRs.toString();
    }   
    
    
}
