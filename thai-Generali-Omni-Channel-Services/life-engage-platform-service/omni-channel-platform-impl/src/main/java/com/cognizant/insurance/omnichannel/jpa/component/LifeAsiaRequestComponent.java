package com.cognizant.insurance.omnichannel.jpa.component;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cognizant.insurance.domain.agreement.AddressTranslation;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaMetaData;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaRequest;
import com.cognizant.insurance.omnichannel.domain.UnderWriterMemoDetails;
import com.cognizant.insurance.omnichannel.lifeasia.searchcriteria.LifeAsiaSearchCriteria;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;

public interface LifeAsiaRequestComponent {
	
	public Response<LifeAsiaRequest> save(Request<LifeAsiaRequest> request);
	public Response<LifeAsiaRequest> merge(Request<LifeAsiaRequest> request);
	
	public Response<LifeAsiaRequest> getLifeAsiaRequest(Request<String> lifeAsiaRequest);
	
	public Response<List<LifeAsiaRequest>> getLifeAsiaRequests(Request<LifeAsiaSearchCriteria> criteria);
	public Response<List<Number>>getLifeAsiaRequestsIds(Request<LifeAsiaSearchCriteria> criteria);
	public Response<LifeAsiaMetaData> getLifeAsiaMetaData(Request<String> criteria);
	public Response<LifeAsiaMetaData> saveMetaData(Request<LifeAsiaMetaData> request) ;
	
	public Response<String> getEappId(Request<LifeAsiaSearchCriteria> request);
	
	public Response<List<String>> getFailedSPAJs(Request<LifeAsiaSearchCriteria> request,int timeRange);
	
	public Response<List<String>> getFailedSPAJsFromOmni(Request<LifeAsiaSearchCriteria> request);
	public Response<List<String>> getProposalsSubmitted(Request<String> agentRequest,Request<List<String>> statusRequest);
	
	public void updateMemoDetails(Request<AppianMemoDetails> request, String type);
	public Response<List<AppianMemoDetails>> retrieveMemoDetails(Request<Transactions> request, String type);
	public UnderWriterMemoDetails getMemoDescription(Request<String> request);
    //public Map<String, UnderWriterMemoDetails> getUnderWriterMemoDetails(Request<Set<String>> request);
    public Response<LifeAsiaRequest> getLifeAsiaRequestByCriteria(Request<LifeAsiaSearchCriteria> request);
	public void updateAppainStatus(Request<LifeAsiaRequest> request);
    public Response<List<AddressTranslation>> getTranslatedAddress(Request<AddressTranslation> request);

}
