/**
 *
 *  Copyright 2013, Cognizant
 *
 * @author        : 297653
 * @version       : 0.1, Feb 4, 2013
 */
package com.cognizant.insurance.agent.core.smooks;

import static com.cognizant.insurance.agent.core.helper.ExceptionHelper.throwBusinessException;
import static com.cognizant.insurance.agent.core.helper.ExceptionHelper.throwSystemException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.milyn.Smooks;
import org.milyn.container.ExecutionContext;
import org.milyn.payload.JavaResult;
import org.milyn.payload.JavaSource;
import org.milyn.validation.OnFailResult;
import org.milyn.validation.ValidationResult;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.core.exception.SystemException;
import com.google.common.base.Strings;

/**
 * The Class class SmooksHolder.
 */
@Component
public class LifeEngageSmooksHolder {
    /** The xml to bo smooks. */
    private Smooks jsonToBoSmooks;

    /** The bo to xml smooks. */
    private Smooks boToJsonSmooks;

    /**
     * Instantiates a new smooks holder.
     */
    public LifeEngageSmooksHolder() {

    }


    /**
     * Sets the json to bo mapping file.
     *
     * @param jsonToBoMappingFile the json to bo mapping file to set.
     */
    public final void setJsonToBoMappingFile(final String jsonToBoMappingFile) {
        try {
            jsonToBoSmooks = new Smooks(jsonToBoMappingFile);
        } catch (IOException e) {
            throw new SystemException(e);
        } catch (SAXException e) {
            throw new SystemException(e);
        }
    }

   
    /**
     * Sets the bo to json mapping file.
     *
     * @param boToJsonMappingFile the bo to json mapping file to set.
     */
    public final void setBoToJsonMappingFile(final String boToJsonMappingFile) {
        try {
            boToJsonSmooks = new Smooks(boToJsonMappingFile);
        } catch (IOException e) {
            throw new SystemException(e);
        } catch (SAXException e) {
            throw new SystemException(e);
        }

    }

    /**
     * Parses the bo.
     * 
     * @param o
     *            the o
     * @return the string
     */
    public final String parseBO(final Object o) {

        ExecutionContext executionContext = boToJsonSmooks.createExecutionContext();
        StringWriter writer = new StringWriter();
        executionContext.getBeanContext().addBean(Constants.RESULT_OBJECT, o);
        boToJsonSmooks.filterSource(executionContext, new JavaSource(o), new StreamResult(writer));
        return writer.toString();

    }

    /**
     * Parses the inputJSON.
     * 
     * @param inputJSON
     *            the input JSON
     * @return the object
     * @throws BusinessException
     *             the business exception
     */
    public final Object parseJson(String inputJSON) throws BusinessException {
        if (!Strings.isNullOrEmpty(inputJSON)) {
            inputJSON = inputJSON.replaceAll(Constants.REG_EXPRESSION, Constants.REPLACEMENT);
            inputJSON = inputJSON.replaceAll("\\$\\$", "");
            inputJSON = inputJSON.replaceAll("<", "&lt;");
            inputJSON = inputJSON.replaceAll(">", "&gt;");
        }
        JavaResult result = new JavaResult();
        ExecutionContext executionContext = jsonToBoSmooks.createExecutionContext();
        final ValidationResult validationResult = new ValidationResult();
       /* try {
			executionContext.setEventListener(new HtmlReportGenerator("jsonreport.html"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
        jsonToBoSmooks.filterSource(executionContext, new StreamSource(new ByteArrayInputStream(inputJSON.getBytes())),
                validationResult, result);

        StringBuilder systemExBuilder = new StringBuilder();
        StringBuilder businessExBuilder = new StringBuilder();

        if (CollectionUtils.isNotEmpty(validationResult.getErrors())) {

            for (OnFailResult validationError : validationResult.getErrors()) {

                String[] errorMessage = validationError.getMessage().split(":");
                if (StringUtils.equals(Constants.ERROR_MESSAGE_S, StringUtils.trim(errorMessage[0]))) {
                    systemExBuilder.append(errorMessage[1]).append(":").append(errorMessage[2]).append(",");
                } else if (StringUtils.equals(Constants.ERROR_MESSAGE_C, StringUtils.trim(errorMessage[0]))) {
                    businessExBuilder.append(errorMessage[1]).append(":").append(errorMessage[2]).append(",");
                }
            }

            throwBusinessException(businessExBuilder.length() > 0, businessExBuilder.toString());
            throwSystemException(systemExBuilder.length() > 0, systemExBuilder.toString());
        }

        return result.getBean(Constants.RESULT);

    }
}
