/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Dec 13, 2013
 */
package com.cognizant.insurance.domain.product;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class class ProductChannel.
 * 
 * @author 300797
 */

@Entity
@Table(name = "CORE_PROD_CHANNEL_MAPPING")
public class ProductChannel {
    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer id;

    /** The channel type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "CHANNEL_ID")
    private ChannelType channelType;

    /** The product specification. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "PROD_ID")
    private ProductSpecification productSpecification;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public final Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public final void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the channel type.
     * 
     * @return the channelType
     */
    public final ChannelType getChannelType() {
        return channelType;
    }

    /**
     * Sets the channel type.
     * 
     * @param channelType
     *            the channelType to set
     */
    public final void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    /**
     * Gets the product specification.
     * 
     * @return the productSpecification
     */
    public final ProductSpecification getProductSpecification() {
        return productSpecification;
    }

    /**
     * Sets the product specification.
     * 
     * @param productSpecification
     *            the productSpecification to set
     */
    public final void setProductSpecification(ProductSpecification productSpecification) {
        this.productSpecification = productSpecification;
    }
}
