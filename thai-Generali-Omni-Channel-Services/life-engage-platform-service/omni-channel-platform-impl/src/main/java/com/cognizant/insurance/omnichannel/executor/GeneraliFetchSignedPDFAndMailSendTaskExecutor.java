package com.cognizant.insurance.omnichannel.executor;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.component.LifeEngageEmailComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.EmailRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.omnichannel.component.GeneraliBPMEmailPdfComponent;
import com.cognizant.insurance.request.vo.StatusData;

/**
 * @Class GeneraliMailExecutor
 * @Desc To execute email notification in an async manner
 * 
 */

public class GeneraliFetchSignedPDFAndMailSendTaskExecutor implements Tasklet, StepExecutionListener {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory
			.getLogger(GeneraliFetchSignedPDFAndMailSendTaskExecutor.class);
	
    /** The id. */
    private Long id;

    /** The email json. */
    private String emailJson = null;

    /** The attempt Number. */
    private String attemptNo;

    /** The currentDate. */
    private Date currentDate;

    /** The Constant EMAIL_TEMPLATE. */
    private static final String EMAIL_TEMPLATE = "emailTemplate";

    /** The Constant AGENT ID. */
    private static final String AGENT_ID = "agentId";
    
    //added for OIC PDF generation
    /** The Constant GROUP_JOB. */
    private static final String GROUP_JOB = "eProposal";
    
    /** The Constant TAX_INVOICE. */
    private static final String TAX_INVOICE = "N";
    
    /** The Constant JOB_TYPE. */
    private static final String JOB_TYPE = "E";
    
    /** The Constant UNDERSCORE. */
    private static final String UNDERSCORE = "_";
    //added for OIC PDF generation

    /** The Constant ID_CARD. */
    private static final String ID_CARD = "identificationNumber";
    
    /** The Constant THAI_DOB. */
    private static final String THAI_DOB = "insuredThaiDOB";
    
    /** The Constant APP_NO. */
    private static final String APP_NO = "spajNumber";
    
    /** The Constant LEAD_NAME. */
    private static final String LEAD_NAME = "leadName";
    
    /** The Constant PDF. */
	private static final String PDF = ".pdf";
	
	/** The Constant DOC_CREATION_DATE. */
    private static final String DOC_CREATION_DATE = "docCreationDate";
    
    @Autowired
    private LifeEngageEmailComponent lifeEngageEmailComponent;
    
    @Autowired
    private GeneraliBPMEmailPdfComponent generaliBPMEmailPdfComponent;

    @Autowired
    private EmailRepository emailRepository;
    
	@Value("${le.platform.default.batFileDownload}")
	private String batFile;
	
	@Value("${le.platform.default.batRootFileLocation}")
	private String batRootFileLocation;
	
	@Value("${le.platform.service.fileUploadPath}")
	private String fileUploadPath;
	
	@Value("${le.platform.service.slash}")
	private String pathSeparator;
	
	private static final String SIGNEDPDFSFOLDER = "SIGNEDPDFS";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailJson() {
        return emailJson;
    }

    public void setEmailJson(String emailJson) {
        this.emailJson = emailJson;
    }

    public String getAttemptNo() {
        return attemptNo;
    }

    public void setAttemptNo(String attemptNo) {
        this.attemptNo = attemptNo;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
    	
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }

    /**
     * executes the batch job
     * 
     * @param arg0
     *            the Step Contribution
     * @param chunkContext
     *            the chunk Context
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        String statusFlag;
        try {
            JobExecution jobExecution = chunkContext.getStepContext().getStepExecution().getJobExecution();
            Long jobId = jobExecution.getJobId();
            String templateName = "";           
            final JSONObject emailJSONObj = new JSONObject(emailJson);
            final JSONObject templateObj = emailJSONObj.getJSONObject(EMAIL_TEMPLATE);
            String type = emailJSONObj.getString(Constants.TYPE);
            String agentId = templateObj.getString(AGENT_ID);
            String identificationNumber = emailJSONObj.getString(ID_CARD);
            String insuredThaiDOB = emailJSONObj.getString(THAI_DOB);
            String applicationNo=templateObj.getString(APP_NO);
            String docCreationDate = emailJSONObj.getString(DOC_CREATION_DATE);
            
            //OIC Password Logic
            String userPassword = identificationNumber.substring(identificationNumber.length()-4,identificationNumber.length()) + insuredThaiDOB;
           
           	//OIC PDF Filename generation
           	String fileName = GROUP_JOB + UNDERSCORE + TAX_INVOICE + UNDERSCORE + JOB_TYPE + UNDERSCORE + 
                   applicationNo + UNDERSCORE + userPassword + UNDERSCORE + docCreationDate + PDF;
            
           	LOGGER.info("File to be fetch over SFTP : " + fileName);
            // Get the signed PDF from the OUT folder - SFTP call
           	LOGGER.info("Triggered SFTP to fetch " + fileName + " starts");
           	String batLogFileName =  generateBatLogFileName();
           	LOGGER.info("Log File : " + batLogFileName);
           	generaliBPMEmailPdfComponent.triggerSFTP(batFile, batLogFileName, batRootFileLocation, fileName);
           	LOGGER.info("Triggered SFTP to fetch " + fileName + " ends");
           	
           	boolean saveSuccess =generaliBPMEmailPdfComponent.readTransferredFilesFromTemp(batLogFileName, false);
	    	LOGGER.info("readTransferredFilesFromTemp function executed for OutBound");
//	    	boolean saveSuccess = generaliBPMEmailPdfComponent.saveSuccessfullyTransferredFilesFromTemp(transferredFileReadList); 
//	    	LOGGER.info("saveSuccessfullyTransferredFilesFromTemp function executed for OutBound");
	    	if(saveSuccess) {
	           	// Rename File
	           	File pdfFileName = new File(fileUploadPath + pathSeparator + SIGNEDPDFSFOLDER + pathSeparator + fileName);
	            if(pdfFileName.exists()) {
		           	String pdfNewName = applicationNo + UNDERSCORE + templateObj.getString(LEAD_NAME) + PDF;
		           	File renamedpdfFileNameWithPath = new File(fileUploadPath + pathSeparator + SIGNEDPDFSFOLDER + pathSeparator + pdfNewName);
		           	
		           	pdfFileName.renameTo(renamedpdfFileNameWithPath);
		           	LOGGER.info("File renamed to " + renamedpdfFileNameWithPath);
		           	
		            // Send mail with the PDF as an attachment
		           	LOGGER.info("lifeEngageEmailComponent.sendEmail() - triggered");
		            StatusData statusData =
		            		lifeEngageEmailComponent.sendEmail(emailJson, "", templateObj.toString(), pdfNewName);
		            statusFlag = statusData.getStatus();
		            LOGGER.info("lifeEngageEmailComponent.sendEmail() - statusFlag" + statusFlag);
		            final LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
		            lifeEngageEmail.setSendTime(LifeEngageComponentHelper.getCurrentdate());
		            lifeEngageEmail.setId(id);
		            lifeEngageEmail.setEmailValues(emailJson);
		            lifeEngageEmail.setStatus(statusFlag);
		            lifeEngageEmail.setAttemptNumber(attemptNo);
		            lifeEngageEmail.setStatusMessage(statusData.getStatusMessage());
		            lifeEngageEmail.setBatchJobId(jobId);
		            lifeEngageEmail.setType(type);
		            lifeEngageEmail.setAgentId(agentId);
		            if (Constants.SUCCESS.equals(statusFlag)) {
		            	LOGGER.info("Mail sent successfully.");
		            	statusFlag = Constants.SUCCESS;
		                emailRepository.updatelifeEngageEmail(lifeEngageEmail);
		                LOGGER.info("Mail sent successfully and DB updated.");
		                //OIC changes - starts
		                LOGGER.info("Archieving Starts");
		                generaliBPMEmailPdfComponent.archieveSignedPDFOnSuccessfulMail(pdfNewName, renamedpdfFileNameWithPath);
		                LOGGER.info("Archieving Completed");
		                //OIC changes - ends		           
		            } else {
		                statusFlag = Constants.FAILURE;
		                emailRepository.updatelifeEngageEmail(lifeEngageEmail);
		                LOGGER.info("DB updated with Failure reasons of sendEmail()");
		            }
	            } else {
	            	LOGGER.info("No file is there to transfer or archive.");
	            }
	    	} else {
	    		LOGGER.info("No mail has to be sent as the file transfer is not successful or no file is there to transfer.");
	    	}
        } catch (Exception e) {
        	LOGGER.error("From execute() method :", e);
        }
        return null;
    }
    
    
    private String generateBatLogFileName() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat ft = new SimpleDateFormat("yyyyMMddHHmm");
        return  batFile.substring(batFile.lastIndexOf("/") + 1) + "_" + ft.format(cal.getTime());
	}
    //OIC changes - ends

}
