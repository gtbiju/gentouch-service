package com.cognizant.insurance.omnichannel.component.repository.helper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONException;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.UserSelectionComponent;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.omnichannel.domain.UnderWriterMemoDetails;
import com.cognizant.insurance.omnichannel.utils.MemoApplicationData;
import com.cognizant.insurance.omnichannel.vo.GeneraliTransactions;
import com.cognizant.insurance.party.component.PartyComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;


@Component
public class GeneraliComponentRepositoryHelper  {
	
	
	 /**
     * Save payer.
     * 
     * @param additionalInsureds
     *            the additional Insureds
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     * @param entityManager
     *            the entity manager
     * @param contextType
     *            the context type
     * @param partyComponent
     *            the party component
     */
    public void saveAdditionalInsuredes( Set<Insured> additionalInsuredes ,  RequestInfo requestInfo,
             InsuranceAgreement insuranceAgreement,  EntityManager entityManager,
             ContextTypeCodeList contextType,  PartyComponent partyComponent,UserSelectionComponent userSelectionComponent) {
    	if (CollectionUtils.isNotEmpty(additionalInsuredes)) {
    	      Request<PartyRoleInAgreement> additionalInsuredRequest =
                  new JPARequestImpl<PartyRoleInAgreement>(entityManager, contextType, requestInfo.getTransactionId());
            for (Insured additionalInsured : additionalInsuredes) {
            	additionalInsured.setIsPartyRoleIn(insuranceAgreement);
            	additionalInsured.setTransactionId(requestInfo.getTransactionId());
            	additionalInsuredRequest.setType(additionalInsured);
            	if ((!insuranceAgreement.getTransactionKeys().getKey40().equals("rm_reassign")) && null!=additionalInsured.getSelectedOptions() && !additionalInsured.getSelectedOptions().isEmpty()){
            		saveUserSelections(additionalInsured.getSelectedOptions(), userSelectionComponent, entityManager, contextType, requestInfo);
            		//userSelectionComponent.saveUserSelection(additionalInsured.getSelectedOptions());
                }
                partyComponent.savePartyRoleInAgreement(additionalInsuredRequest);
            }
        }
    }


    /**
     * Creates the replacing Document.
     * 
     * @param replacingAgeements
     *            the replacingAgeements
     * 
     * @return the AgreementDocument
     */

    
    public Set<AgreementDocument> createReplaceDocument(Set<Agreement> replacingAgeements) {

        Set<AgreementDocument> agreementRelatedDocumentSet = new HashSet<AgreementDocument>();
        Set<AgreementDocument> agreementDocumentSet = new HashSet<AgreementDocument>();
        AgreementDocument agreementDocument;
        Document documentPages;
        Set<Document> document;

        for (Agreement agreement : replacingAgeements) {
            agreementDocumentSet = agreement.getRelatedDocument();

            for (AgreementDocument agrDocument : agreementDocumentSet) {
                agreementDocument = new AgreementDocument();
                agreementDocument.setName(agrDocument.getName());
                agreementDocument.setDescription(agrDocument.getDescription());
                agreementDocument.setTypeCode(agrDocument.getTypeCode());
                agreementDocument.setDocumentProofSubmitted(agrDocument.getDocumentProofSubmitted());
                agreementDocument.setDocumentStatus(agrDocument.getDocumentStatus());
                agreementDocument.setStorageLocation(agrDocument.getStorageLocation());
                agreementDocument.setFileContent(agrDocument.getFileContent());
                agreementDocument.setSignedIndicator(agrDocument.getSignedIndicator());
                agreementDocument.setLanguageCode(agrDocument.getLanguageCode());
                agreementDocument.setDocumentTypeCode(agrDocument.getDocumentTypeCode());
                agreementDocument.setFileNames(agrDocument.getFileNames());
                agreementDocument.setContentType(agrDocument.getContentType());

                document = new HashSet<Document>();
                for (Document doc : agrDocument.getPages()) {
                    documentPages = new Document();
                    documentPages.setName(doc.getName());
                    documentPages.setDescription(doc.getDescription());
                    documentPages.setTypeCode(doc.getTypeCode());
                    documentPages.setDocumentProofSubmitted(doc.getDocumentProofSubmitted());
                    documentPages.setDocumentStatus(doc.getDocumentStatus());
                    documentPages.setStorageLocation(doc.getStorageLocation());
                    documentPages.setFileContent(doc.getFileContent());
                    documentPages.setSignedIndicator(doc.getSignedIndicator());
                    documentPages.setLanguageCode(doc.getLanguageCode());
                    documentPages.setDocumentTypeCode(doc.getDocumentTypeCode());
                    documentPages.setFileNames(doc.getFileNames());
                    documentPages.setContentType(doc.getContentType());
                    documentPages.setBase64string(doc.getBase64string());
                    documentPages.setModifiableIndicator(doc.getModifiableIndicator());
                    document.add(documentPages);
                }

                agreementDocument.setPages(document);
                agreementRelatedDocumentSet.add(agreementDocument);
            }
        }

        return agreementRelatedDocumentSet;

    }
    
    /**
     * Save Agent.
     * 
     * @param proposer
     *            the proposer
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    public void saveAgent(final AgreementProducer agent, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement, PartyComponent partyComponent,EntityManager entityManager, ContextTypeCodeList contextType) {

        if (agent != null) {
        	agent.setIsPartyRoleIn(insuranceAgreement);
        	agent.setTransactionId(requestInfo.getTransactionId());
            final Request<PartyRoleInAgreement> proposerRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, contextType,
                            requestInfo.getTransactionId());
            proposerRequest.setType(agent);
            partyComponent.savePartyRoleInAgreement(proposerRequest);
        }
    }

    public static void saveUserSelections(Set<UserSelection> userSelections,UserSelectionComponent userSelectionComponent,EntityManager entityManager, ContextTypeCodeList contextType,final RequestInfo requestInfo) {
    	if (null!=userSelections && !userSelections.isEmpty()){
    		Request<Set<UserSelection>> request = new JPARequestImpl<Set<UserSelection>>(entityManager, contextType, requestInfo.getTransactionId());
    		request.setType(userSelections);
    		userSelectionComponent.saveUserSelection(request);
    	}
	}
    
    /*public void saveUserSelections(Set<UserSelection> userSelections,UserSelectionComponent userSelectionComponent){
    	if (null!=userSelections && !userSelections.isEmpty()){
    		userSelectionComponent.saveUserSelection(userSelections);
    	}
	}*/

    /**
     * Save insured.
     * 
     * @param insured
     *            the insured
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     * @param entityManager
     *            the entity manager
     * @param contextType
     *            the context type
     * @param partyComponent
     *            the party component
     */
    public void saveInsured(final Insured insured, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement, final EntityManager entityManager,
            final ContextTypeCodeList contextType, final PartyComponent partyComponent,UserSelectionComponent userSelectionComponent) {
        if (insured != null) {
            insured.setIsPartyRoleIn(insuranceAgreement);
            insured.setTransactionId(requestInfo.getTransactionId());
            if(!insuranceAgreement.getTransactionKeys().getKey40().equalsIgnoreCase("rm_reassign")){
             saveUserSelections(insured.getSelectedOptions(),userSelectionComponent, entityManager, contextType, requestInfo);
            }
            //userSelectionComponent.saveUserSelection(insured.getSelectedOptions());
            Request<PartyRoleInAgreement> insuredRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, contextType, requestInfo.getTransactionId());
            insuredRequest.setType(insured);
            partyComponent.savePartyRoleInAgreement(insuredRequest);
        }
    }
    
    public static void parseMemoAppData(final MemoApplicationData memoAppData, final JSONObject jsonObj) throws BusinessException{
        memoAppData.setAppNumber(jsonObj.getString(GeneraliConstants.APP_NO));
        if(memoAppData.getAppNumber()!= null && memoAppData.getAppNumber().isEmpty() ){
            throw new BusinessException(ErrorConstants.LE_SYNC_ERR_105, GeneraliConstants.SPAJ_NO_MISSING);
        }
        memoAppData.setAppStatus(jsonObj.getString(GeneraliConstants.APP_STATUS));
        memoAppData.setMemoDate(jsonObj.getString(GeneraliConstants.MEMO_DATE));
        if (jsonObj.has(GeneraliConstants.POLICY_NO)) {
            memoAppData.setPolicyNumber(jsonObj.getString(GeneraliConstants.POLICY_NO));
        }
       
        JSONArray memoDetailArray = jsonObj.getJSONArray(GeneraliConstants.MEMO_DETAILS);
        List<AppianMemoDetails> memoList = new ArrayList<AppianMemoDetails>();
        for(int j=0; j < memoDetailArray.length(); j++) {
			JSONObject jsonObjCheck = memoDetailArray.getJSONObject(j);
			if (jsonObjCheck.getString(GeneraliConstants.MEMO_CODE) != null
					&& !jsonObjCheck.getString(GeneraliConstants.MEMO_CODE)
							.equals("")) {
				AppianMemoDetails memoData = new AppianMemoDetails();
				parseMemoData(memoData, memoAppData,
						memoDetailArray.getJSONObject(j));
				memoList.add(memoData);
			}
           
        }
        memoAppData.setMemoList(memoList);
        
    }
    
    public static void parseMemoData(final AppianMemoDetails appianMemoDetails, final MemoApplicationData memoAppData, final JSONObject jsonObj){
        
        appianMemoDetails.setAppStatus(memoAppData.getAppStatus());
        appianMemoDetails.setAppNumber(memoAppData.getAppNumber());
        appianMemoDetails.setMemoDate(memoAppData.getMemoDate());
        appianMemoDetails.setPolicyNumber(memoAppData.getPolicyNumber());
        
        UnderWriterMemoDetails uWMemoDet = new UnderWriterMemoDetails();
        uWMemoDet.setMemoCode(jsonObj.getString(GeneraliConstants.MEMO_CODE));
        appianMemoDetails.setMemoDetails(uWMemoDet);
        appianMemoDetails.setCategory(jsonObj.getString(GeneraliConstants.CATEGORY));
        appianMemoDetails.setMemoId(jsonObj.getString(GeneraliConstants.MEMO_ID));
        appianMemoDetails.setRemarks(jsonObj.getString(GeneraliConstants.REMARKS));
        appianMemoDetails.setMemoDesc(jsonObj.getString(GeneraliConstants.MEMO_DESC));
        appianMemoDetails.setMemoReason(jsonObj.getString(GeneraliConstants.MEMO_REASON));
        appianMemoDetails.setMemoStatus(jsonObj.getString(GeneraliConstants.MEMO_STATUS));
        
        List<String> counterMemoList = new ArrayList<String>();

		counterMemoList.add("5CF");
		counterMemoList.add("CTF");
		counterMemoList.add("XCL");
		
        if(counterMemoList.contains(uWMemoDet.getMemoCode())){
            String details = jsonObj.getJSONObject(GeneraliConstants.DETAILS).toString();
            appianMemoDetails.setDetails(details);
        }
    }
    
    public static void buildTransactionsList(final List<GeneraliTransactions> transactions, final JSONObject jsonObj)
            throws ParseException {
        if (CollectionUtils.isNotEmpty(transactions)) {
            for (GeneraliTransactions transactionsElement : transactions) {
                LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), transactionsElement, true);
            }
        }
    }
    
    /**
     * Save Payer
     * @param payer
     * @param requestInfo
     * @param insuranceAgreement
     * @param entityManager
     * @param contextType
     * @param partyComponent
     * @param userSelectionComponent
     */
    public static void savePayer(final PremiumPayer payer, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement, final EntityManager entityManager,
            final ContextTypeCodeList contextType, final PartyComponent partyComponent, UserSelectionComponent userSelectionComponent) {

        if (payer != null) {
            payer.setIsPartyRoleIn(insuranceAgreement);
            payer.setTransactionId(requestInfo.getTransactionId());
            if(!insuranceAgreement.getTransactionKeys().getKey40().equalsIgnoreCase("rm_reassign")){
            saveUserSelections(payer.getSelectedOptions(),userSelectionComponent, entityManager, contextType, requestInfo);
            }
            final Request<PartyRoleInAgreement> payerRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, contextType, requestInfo.getTransactionId());
            payerRequest.setType(payer);
            partyComponent.savePartyRoleInAgreement(payerRequest);
        }
    }


   public static void buildTransaction(GeneraliTransactions txn, JSONObject jsonObj) throws BusinessException {
        txn.setType(jsonObj.getString(Constants.TYPE));
        if(txn.getType().equalsIgnoreCase(Constants.EAPP)){
            txn.setProposalNumber(jsonObj.getString(GeneraliConstants.IDENTIFIER)); 
        }else if(txn.getType().equalsIgnoreCase(Constants.ILLUSTRATION)){
            txn.setIllustrationId(jsonObj.getString(GeneraliConstants.ID));
        }
        GeneraliAgent genAgent = new GeneraliAgent();
        if(jsonObj.getString(GeneraliConstants.ACTION).equalsIgnoreCase(GeneraliConstants.LOCK)){
            txn.setLockOrUnlockMode(GeneraliConstants.LOCK);
            genAgent.setGaoCode(jsonObj.getString(GeneraliConstants.GAO_CODE));
            txn.setGeneraligent(genAgent);
        }else if(jsonObj.getString(GeneraliConstants.ACTION).equalsIgnoreCase(GeneraliConstants.UNLOCK)){
            txn.setLockOrUnlockMode(GeneraliConstants.UNLOCK);
        }else if(jsonObj.getString(GeneraliConstants.ACTION).equalsIgnoreCase(GeneraliConstants.ASSIGN_GAO)){
            txn.setLockOrUnlockMode(GeneraliConstants.ASSIGN_GAO);
            genAgent.setGivenName(jsonObj.getString(GeneraliConstants.AGENT_NAME));
            genAgent.setZone(jsonObj.getString(GeneraliConstants.GAO_OFFICE));
            txn.setGeneraligent(genAgent);
        }else if(jsonObj.getString(GeneraliConstants.ACTION).equalsIgnoreCase(GeneraliConstants.MARK_COMPLETE)){
            txn.setLockOrUnlockMode(GeneraliConstants.MARK_COMPLETE);
        }else{
                throw new BusinessException("Invalid action mode for GAO Flow");
        }
    }
   public static String buildDateInThai(Date memoDateInThaiCalendar){
	   	
	   	Map<Integer,String> monthNamesInThai = new HashMap<Integer,String>();
	   	monthNamesInThai.put(0,GeneraliConstants.JANUARY);
	   	monthNamesInThai.put(1,GeneraliConstants.FEBURARY);
	   	monthNamesInThai.put(2,GeneraliConstants.MARCH);
	   	monthNamesInThai.put(3,GeneraliConstants.APRIL);
	   	monthNamesInThai.put(4,GeneraliConstants.MAY);
	   	monthNamesInThai.put(5,GeneraliConstants.JUNE);
	   	monthNamesInThai.put(6,GeneraliConstants.JULY);
	   	monthNamesInThai.put(7,GeneraliConstants.AUGUST);
	   	monthNamesInThai.put(8,GeneraliConstants.SEPTEMBER);
	   	monthNamesInThai.put(9,GeneraliConstants.OCTOBER);
	   	monthNamesInThai.put(10,GeneraliConstants.NOVEMBER);
	   	monthNamesInThai.put(11,GeneraliConstants.DECEMBER);
	   	
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(memoDateInThaiCalendar);
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			int month = calendar.get(Calendar.MONTH);
			int year = calendar.get(Calendar.YEAR);
			String monthInThai = monthNamesInThai.get(month);

			StringBuilder finalDate = new StringBuilder();
			finalDate.append(Integer.toString(day)).append(" ").append(monthInThai)
					.append(" ").append(Integer.toString(year));

			return finalDate.toString();
	   }
}
