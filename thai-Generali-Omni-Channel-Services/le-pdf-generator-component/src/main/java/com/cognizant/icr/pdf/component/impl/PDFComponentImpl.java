/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.icr.pdf.component.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.icr.exception.RuleException;
import com.cognizant.icr.pdf.component.PDFComponent;
import com.cognizant.icr.pdf.dao.CommonDAOImpl;
import com.cognizant.icr.pdf.file.FileNameGenerator;
import com.cognizant.icr.pdf.repository.FileRepository;
import com.cognizant.icr.pdf.request.PDFRequest;
import com.cognizant.icr.pdf.response.ResponseBean;
import com.cognizant.icr.pdf.util.Constants;
import com.cognizant.icr.pdf.util.DataSourceFactory;
import com.cognizant.icr.pdf.util.PropertyFileReader;
import com.cognizant.icr.rule.bom.RuleCriteria;
import com.cognizant.icr.rule.v2.DbRuleEngineImpl;
import com.cognizant.icr.rule.v2.RuleEngine;

/**
 * The Class class PDFComponentImpl.
 * 
 * @param <T>
 *            the generic type
 */
public class PDFComponentImpl<T> implements PDFComponent<T>  {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PDFComponentImpl.class);

    /** The file name generator. */
    private FileNameGenerator fileNameGenerator;

    /** The file repository. */
    private FileRepository fileRepository;

    /** The is enabled. */
    private Boolean isEnabled;

    /** The rule name. */
    private String ruleName;

    /** The template name. */
    private String templateName;

    /** The template file. */
    private File templateFile;
    
    /** The rule category. */
    private String ruleCategory;
    
    /** The rule client id. */
    private String ruleClientId;

    /**
     * Parses the bean to json string.
     *
     * @param dataObject the data object
     * @return the string
     */
    private String parseBeanToJsonString(final T dataObject) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("rule", dataObject);
        String jsonString = jsonObject.toString();
        System.out.println(jsonString);
        return jsonString;
    }


    /**
     * Fill pdf.
     *
     * @param dataObject the data object
     * @param jasperTemplate the template file
     * @return the jasper print
     */
    public final JasperPrint fillPdf(final T dataObject, final File jasperTemplate) {
        LOGGER.debug("Method Entry : PDFComponent-> fillPdf");
        JasperPrint jasperPrint = null;
        try {

            List<T> objList = new ArrayList<T>();
            objList.add(dataObject);
            JRBeanCollectionDataSource beanDataSource = new JRBeanCollectionDataSource(objList);
            JasperReport jasperReport;
            jasperReport = (JasperReport) JRLoader.loadObject(jasperTemplate);
            jasperPrint = JasperFillManager.fillReport(jasperReport, null, beanDataSource);

        } catch (JRException e) {
            e.printStackTrace();
        }
        LOGGER.debug("Method Exit : PDFComponent-> fillPdf");
        return jasperPrint;
    }

    /**
     * Validate json.
     *
     * @param json the json
     * @param valRuleName the rule name
     * @return true, if successful
     */
    public final boolean validateJSON(final String json, final String valRuleName) {
    	LOGGER.debug("Method Entry : PDFComponent-> validateJSON");
    	Boolean result = false;        
        try {
        	DataSource dataSource = new DataSourceFactory().createConnection();
            RuleEngine engine = new DbRuleEngineImpl(dataSource);
            RuleCriteria ruleCriteria = new RuleCriteria();
            ruleCriteria.setCategory(getRuleCategory());
            ruleCriteria.setClientId(getRuleClientId());
            // Object json = JSON.parse(jsonInput);
            ruleCriteria.setRule(valRuleName);
            String jsonOutput = null;
            try {
                jsonOutput = engine.executeRuleWithJson(ruleCriteria, json);
                JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(jsonOutput);
                result = jsonObject.getBoolean("isSuccess");
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (RuleException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
        	LOGGER.error("Error", e);
        } catch (Exception e) {
            LOGGER.error("Error", e);
        }
        LOGGER.debug("Method Exit : PDFComponent-> validateJSON");
        return result;
    }

    /**
     * Populate configurations.
     *
     * @param pdfRequestImpl the pdf request impl
     */
    public final void populateConfigurations(final PDFRequest<T> pdfRequestImpl) {
    	LOGGER.debug("Method Entry : PDFComponent-> populateConfigurations"); 
        isEnabled = PropertyFileReader.fetchProperty(Constants.IS_ENABLED, Constants.CONFIG_PROPERTY_FILE).equals("true");
        ruleName =
                PropertyFileReader.fetchProperty(pdfRequestImpl.getTemplateID(), Constants.CONFIG_PROPERTY_FILE);

        templateName = fileRepository.getTemplateName(pdfRequestImpl.getTemplateID());
        ruleClientId= PropertyFileReader.fetchProperty(Constants.RULE_CLIENT_ID, Constants.CONFIG_PROPERTY_FILE);
        ruleCategory= PropertyFileReader.fetchProperty(Constants.RULE_CATEGORY, Constants.CONFIG_PROPERTY_FILE);
        try {
            templateFile = fileRepository.getTemplate(templateName);
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        LOGGER.debug("Method Exit : PDFComponent-> populateConfigurations");
    }
    
    
    public final List<ResponseBean> generateEncryptedPdf(final PDFRequest<T> pdfRequestImpl) {
    	LOGGER.info("Method Entry 2: PDFComponent-> generateEncryptedPdf");
    	List<ResponseBean> responseBeans = processPdfToFS(pdfRequestImpl,true);
    	LOGGER.info("Method Exit 2: PDFComponent-> generateEncryptedPdf");
    	return responseBeans;
    	
    }
    
    /**
     * Generate pdf.
     *
     * @param pdfRequestImpl the pdf request impl
     * @return the list
     */
    public final List<ResponseBean> generatePdf(final PDFRequest<T> pdfRequestImpl) {
    	LOGGER.info("Method Entry 2: PDFComponent-> generatePdf");
    	List<ResponseBean> responseBeans = processPdfToFS(pdfRequestImpl,false);
    	LOGGER.info("Method Exit 2: PDFComponent-> generatePdf");
    	return responseBeans;
    }

    
    public List<ResponseBean> processPdfToFS(final PDFRequest<T> pdfRequestImpl, boolean encrypt){
        LOGGER.info("Method Entry 3: PDFComponent-> processPdfToFS");
        List<ResponseBean> responseBeans = new ArrayList<ResponseBean>();
        try {
            populateConfigurations(pdfRequestImpl);
            Boolean isRuleAvailable = new CommonDAOImpl().isRuleAvailable(ruleName);
            String outputFileName;
            String outputFileLoc =
                    PropertyFileReader.fetchProperty("le.pdf.output." + pdfRequestImpl.getTemplateType(), Constants.CONFIG_PROPERTY_FILE);
            Map<String, T> dataBeanMap = pdfRequestImpl.getDataObjsMap();
            Set<String> keySet = dataBeanMap.keySet();
            for (String key : keySet) {
                Boolean valid = true;
                T dataObject = dataBeanMap.get(key);
                if (isEnabled && isRuleAvailable) {
                    String json = parseBeanToJsonString(dataObject);
                    valid = validateJSON(json, ruleName);
                }
                if (valid) {
                    outputFileName = fileNameGenerator.generateFileName(dataObject);
                    outputFileLoc = outputFileLoc + "/" + outputFileName + Constants.EXT_PDF;
                    JasperPrint jasperPrint = null;
                    jasperPrint = fillPdf(dataObject, templateFile);
                    ResponseBean responseBean = new ResponseBean();
                    if(!encrypt){
                    	JasperExportManager.exportReportToPdfFile(jasperPrint, outputFileLoc);
                    }else{
                    	FileOutputStream fos = new FileOutputStream(outputFileLoc);
                        JRPdfExporter exporter = new JRPdfExporter();
                        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, fos);
                        exporter.setParameter(JRPdfExporterParameter.OWNER_PASSWORD, pdfRequestImpl.getOwnerPassword());
                        exporter.setParameter(JRPdfExporterParameter.USER_PASSWORD, pdfRequestImpl.getUserPassword());
                        exporter.setParameter(JRPdfExporterParameter.IS_ENCRYPTED, Boolean.TRUE);
                        exporter.exportReport();
                        fos.flush();
                        fos.close();
                    }
                    
                    responseBean.setFileName(outputFileName);
                    responseBean.setFileLocation(outputFileLoc);
                    responseBeans.add(responseBean);
                }
            }

        } catch (JRException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        LOGGER.info("Method Exit 3: PDFComponent-> processPdfToFS");
        return responseBeans;
    }
    
    
    
    /**
     * Creates the pdf.
     *
     * @param pdfRequestImpl the pdf request impl
     * @return the byte array output stream
     */
    public final ByteArrayOutputStream processPdfToByteStream(final PDFRequest<T> pdfRequestImpl, boolean encrypt) {
        LOGGER.info("Method Entry 3 : PDFComponent-> processPdfToByteStream");
        java.io.ByteArrayOutputStream byteOutputStream = null;
        try {
            populateConfigurations(pdfRequestImpl);
            Map<String, T> dataBeanMap = pdfRequestImpl.getDataObjsMap();
            Set<String> keySet = dataBeanMap.keySet();
            for (String key : keySet) {
                Boolean valid = true;
                T dataObject = dataBeanMap.get(key);
                if (isEnabled && (new CommonDAOImpl().isRuleAvailable(ruleName))) {
                        String json = parseBeanToJsonString(dataObject);
                        valid = validateJSON(json, ruleName);
                }
                if (valid) {
                    JasperPrint jasperPrint = null;
                    jasperPrint = fillPdf(dataObject, templateFile);
                    byteOutputStream = new ByteArrayOutputStream();
                    if(!encrypt){
                    	JasperExportManager.exportReportToPdfStream(jasperPrint, byteOutputStream);
                    }else{
                        JRPdfExporter exporter = new JRPdfExporter();
                        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteOutputStream);
                        exporter.setParameter(JRPdfExporterParameter.OWNER_PASSWORD, pdfRequestImpl.getOwnerPassword());
                        exporter.setParameter(JRPdfExporterParameter.USER_PASSWORD, pdfRequestImpl.getUserPassword());
                        exporter.setParameter(JRPdfExporterParameter.IS_ENCRYPTED, Boolean.TRUE);
                        exporter.exportReport();
                    }
                }
            }

        } catch (JRException e) {
            e.printStackTrace();
        }
        LOGGER.info("Method Exit 3 : PDFComponent-> processPdfToByteStream");
        return byteOutputStream;
    }
    
    
    public final ByteArrayOutputStream createPdf(final PDFRequest<T> pdfRequestImpl){
    	LOGGER.info("Method Entry 2: PDFComponent-> createPdf");
    	ByteArrayOutputStream byteStream = processPdfToByteStream(pdfRequestImpl,false);
    	LOGGER.info("Method Exit 2: PDFComponent-> createPdf");
		return byteStream;
    }
    
    public final ByteArrayOutputStream createEncryptedPdf(final PDFRequest<T> pdfRequestImpl){
    	LOGGER.info("Method Entry 2: PDFComponent-> createEncryptedPdf");
    	ByteArrayOutputStream byteStream = processPdfToByteStream(pdfRequestImpl,true);
    	LOGGER.info("Method Exit 2: PDFComponent-> createEncryptedPdf");
		return byteStream;
    }
    

    /**
     * Gets the file name generator.
     * 
     * @return the file name generator
     */
    public final FileNameGenerator getFileNameGenerator() {
        return fileNameGenerator;
    }

    /**
     * Sets the file name generator.
     * 
     * @param fileNameGenerator
     *            the new file name generator
     */
    public final void setFileNameGenerator(final FileNameGenerator fileNameGenerator) {
        this.fileNameGenerator = fileNameGenerator;
    }

    /**
     * Gets the file repository.
     * 
     * @return the file repository
     */
    public final FileRepository getFileRepository() {
        return fileRepository;
    }

    /**
     * Sets the file repository.
     * 
     * @param fileRepository
     *            the new file repository
     */
    public final void setFileRepository(final FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

	/**
	 * Gets the rule category.
	 *
	 * @return the rule category
	 */
	public String getRuleCategory() {
		return ruleCategory;
	}

	/**
	 * Sets the rule category.
	 *
	 * @param ruleCategory the new rule category
	 */
	public void setRuleCategory(String ruleCategory) {
		this.ruleCategory = ruleCategory;
	}

	/**
	 * Gets the rule client id.
	 *
	 * @return the rule client id
	 */
	public String getRuleClientId() {
		return ruleClientId;
	}

	/**
	 * Sets the rule client id.
	 *
	 * @param ruleClientId the new rule client id
	 */
	public void setRuleClientId(String ruleClientId) {
		this.ruleClientId = ruleClientId;
	}

}
