package com.cognizant.insurance.omnichannel.vo;

public class BankBranch  implements Comparable<BankBranch>{
	
private String bank;
private String state;
private String branch;
private String bankOrStateOrBranchCode;
private String branchCode;
private String branchName;


@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((bank == null) ? 0 : bank.hashCode());
	result = prime * result + ((branch == null) ? 0 : branch.hashCode());
	result = prime * result + ((state == null) ? 0 : state.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	BankBranch other = (BankBranch) obj;
	if (bank == null) {
		if (other.bank != null)
			return false;
	} else if (!bank.equals(other.bank))
		return false;
	if (branch == null) {
		if (other.branch != null)
			return false;
	} else if (!branch.equals(other.branch))
		return false;
	if (state == null) {
		if (other.state != null)
			return false;
	} else if (!state.equals(other.state))
		return false;
	return true;
}
public String getBank() {
	return bank;
}
public void setBank(String bank) {
	this.bank = bank;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public void setBranch(String branch) {
	this.branch = branch;
}
public String getBranch() {
	return branch;
}
public void setBankOrStateOrBranchCode(String bankOrStateOrBranchCode) {
	this.bankOrStateOrBranchCode = bankOrStateOrBranchCode;
}
public String getBankOrStateOrBranchCode() {
	return bankOrStateOrBranchCode;
}

public String getBranchCode() {
	return branchCode;
}
public void setBranchCode(String branchCode) {
	this.branchCode = branchCode;
}
public String getBranchName() {
	return branchName;
}
public void setBranchName(String branchName) {
	this.branchName = branchName;
}
@Override
public int compareTo(BankBranch o) {
	// TODO Auto-generated method stub
	return bankOrStateOrBranchCode.compareTo(o.bankOrStateOrBranchCode);
}

}
