package com.cognizant.insurance.omnichannel.vo;

import java.util.Date;
import java.util.List;

public class LifeAsiaDataVo {
	
	private String documentId;
	private String status;
	private String messages;
	
	private Date from;
	private Date to;
	private String userName;
	private String password;
	private List<String>  spajIds;
	private String agentId;
	
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessages() {
		return messages;
	}
	public void setMessages(String messages) {
		this.messages = messages;
	}
	public Date getFrom() {
		return from;
	}
	public void setFrom(Date from) {
		this.from = from;
	}
	public Date getTo() {
		return to;
	}
	public void setTo(Date to) {
		this.to = to;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<String> getSpajIds() {
		return spajIds;
	}
	public void setSpajIds(List<String> spajIds) {
		this.spajIds = spajIds;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	
	

}
