/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists;

/**
 * This list defines civil relation types. 
 * 
 * @author 301350
 * 
 * 
 */
public enum CivilRelationNatureCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Husband,
    /**
     * 
     * 
     * 
     * 
     */
    Wife
}
