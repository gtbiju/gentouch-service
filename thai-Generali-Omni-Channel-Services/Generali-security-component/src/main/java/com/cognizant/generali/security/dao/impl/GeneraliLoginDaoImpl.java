package com.cognizant.generali.security.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.generali.security.dao.GeneraliLoginDao;
import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.le.security.entity.User;
import com.cognizant.le.security.exception.DaoException;
import com.cognizant.le.security.request.Request;
import com.cognizant.le.security.request.impl.JPARequestImpl;

public class GeneraliLoginDaoImpl implements GeneraliLoginDao {

	public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliLoginDaoImpl.class);

	@Value("${le.platform.service.security.accountLock.onAttempt}")
	private int accountLockOnMaxAttempt;
	
	public GeneraliTHUser getUserDetails(Request<String> userNameRq) {
		LOGGER.debug("METHOD ENTRY : getUserDetails ");
        GeneraliTHUser user = null;    
       
        try {

            final EntityManager entityManager = ((JPARequestImpl<String>) userNameRq).getEntityManager();

            final Query queryDetails = entityManager.createQuery("select user from GeneraliTHUser user where user.userId = :userId");
            queryDetails.setParameter("userId", userNameRq.getType());
            List<Object> userList = queryDetails.getResultList();
            if (CollectionUtils.isNotEmpty(userList)) {
                if (userList.size() > 1) {
                    throw new DaoException("Duplicate user available in system.");
                }
                user = (GeneraliTHUser) userList.get(0);
                user.setPassword(user.getPassword());
            }

        } catch (Exception e) {
            throw new DaoException(e);
        }
        LOGGER.debug("METHOD EXIT : getUserDetails ");
        return user;
	}
	
	public void updateFailedAttempt(Request<String> userNameRq){
		final EntityManager entityManager = ((JPARequestImpl<String>) userNameRq).getEntityManager();
		GeneraliTHUser generaliThUser = getUserDetails(userNameRq);
		int attempts = generaliThUser.getAttempts();
		if(attempts < accountLockOnMaxAttempt){
			generaliThUser.setAttempts(++attempts);
			if (attempts == accountLockOnMaxAttempt) {
				generaliThUser.setPasswordLocked(true);
			}
		}
		entityManager.merge(generaliThUser);
	}
	
	public void resetAttempts(Request<String> userNameRq){
		final EntityManager entityManager = ((JPARequestImpl<String>) userNameRq).getEntityManager();
		User user = getUserDetails(userNameRq);
		if(user instanceof GeneraliTHUser){		
			GeneraliTHUser genUser = (GeneraliTHUser) user;		
			genUser.setAttempts(0);
			entityManager.merge(genUser);
		}
	}
	
}
