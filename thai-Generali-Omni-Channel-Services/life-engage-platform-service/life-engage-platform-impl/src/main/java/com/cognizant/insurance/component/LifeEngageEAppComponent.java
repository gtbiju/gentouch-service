/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.component;

import java.text.ParseException;

import org.json.JSONObject;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * The Interface interface LifeEngageEAppComponent.
 * 
 * @author 300797
 */
public interface LifeEngageEAppComponent {

    /**
     * Retrieve all EApp.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonObj
     *            the JSON object
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveAllEApp(RequestInfo requestInfo, JSONObject jsonObj) throws BusinessException, ParseException;

    /**
     * Retrieve EApp.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonObj
     *            the JSON object
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
     String retrieveEApp(RequestInfo requestInfo, JSONObject jsonObj) throws BusinessException, ParseException;

    /**
     * Save EApp.
     * 
     * @param json
     *            the json
     * @param requestInfo
     *            the request info
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     */
    String saveEApp(String json, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit);
    
    /**
     * save Observation called from syncTOServer service
     * 
     * @param json
     *            the json
     * @param requestInfo
     *            the request info
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     */
    String saveToSyncObservation(String json, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit);

    /**
     * Save Observations.
     * 
     * @param json
     *            the json
     */
    void saveObservations(String json);
    
    /**
     * Get Status.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonObj
     *            the JSON object
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
	String getStatus(RequestInfo requestInfo, String proposalNo, String type)throws BusinessException, ParseException;	
}
