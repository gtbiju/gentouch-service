/**
 * 
 */
package com.cognizant.insurance.agent.domain.extension;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * The Class class Extension contains a name value pair that can be used to save custom extension fields
 */

@Entity
public class Extension implements Serializable{
    
    

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6500873826132622734L;
    
    /** The id. */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
      
    /** The name. */
    private String name;
    
    /** The value. */
    private String value;
    
   
  

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value to set.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
     * Gets the id.
     *
     * @return Returns the id.
     */
    public final Long getId() {
        return id;
    }

    /**
     * Sets The id.
     *
     * @param id The id to set.
     */
    public final void setId(Long id) {
        this.id = id;
    }

	
}
