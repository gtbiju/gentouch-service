package com.cognizant.insurance.omnichannel.utils;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import net.sf.jasperreports.engine.JRChart;
import net.sf.jasperreports.engine.JRChartCustomizer;
import java.awt.BasicStroke;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

public class BarChartCustomizer implements JRChartCustomizer {

	@Override
	public void customize(JFreeChart chart, JRChart jasperChart) {
		// TODO Auto-generated method stub

		BarRenderer renderer = (BarRenderer) chart.getCategoryPlot()
				.getRenderer();
		renderer.setMaximumBarWidth(0.10);
		CategoryPlot categoryPlot = chart.getCategoryPlot();
		
		categoryPlot.setRangeGridlinesVisible(false);
		categoryPlot.setDomainGridlinesVisible(false);
	}
}
