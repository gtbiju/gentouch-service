/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.agreementcodelists;

/**
 * Contractual change Type.
 * 
 * @author 301350
 * 
 * 
 */
public enum AgreementChangeTypeCodeList {
    /**
     * 
     */
    Cancel,
    /**
     * 
     */
    Renewval,
    /**
     * 
     */
    ExtenstionofPeriod,
    /**
     * 
     */
    Owner

}
