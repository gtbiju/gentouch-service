/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 262471
 * @version       : 0.1, Feb 14, 2013
 */
package com.cognizant.insurance.core.helper;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * The Class class ExceptionHandler.
 */
@Aspect
@Component
public class ExceptionHandler {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);

    /** The Constant TRANSACTION_DETAILS. */
    public static final String TRANSACTION_DETAILS = "TransactionDetails";

    /** The Constant FAILURE. */
    public static final String FAILURE = "FAILURE";

    /**
     * After throwing from transactional method.
     * 
     * @param joinPoint
     *            the join point
     * @param error
     *            the error
     * @throws BusinessException
     *             the business exception
     */
    @AfterThrowing(pointcut = "execution(* com.cognizant.insurance.service..*.*(..))", throwing = "error")
    public final void afterThrowingFromTransactionalMethod(final JoinPoint joinPoint, final Throwable error)
            throws BusinessException {
        LOGGER.info("[inside afterThrowingFromTransactionalMethod]");
    }
}
