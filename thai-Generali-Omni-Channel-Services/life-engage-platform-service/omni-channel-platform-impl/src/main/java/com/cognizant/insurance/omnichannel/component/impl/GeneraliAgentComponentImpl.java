package com.cognizant.insurance.omnichannel.component.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.agent.AgentAchievement;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliAgentAddress;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.generic.component.impl.AgentComponentImpl;
import com.cognizant.insurance.omnichannel.component.GeneraliAgentComponent;
import com.cognizant.insurance.omnichannel.dao.GeneraliAgentDao;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;

/**
 * The Class AgentComponentImpl.
 */
public class GeneraliAgentComponentImpl extends AgentComponentImpl implements GeneraliAgentComponent {
	
	 @Autowired
	 private GeneraliAgentDao agentDao;

	/* (non-Javadoc)
	 * @see com.cognizant.insurance.generic.component.AgentComponent#retrieveAgentByCode(com.cognizant.insurance.request.Request)
	 */
	 
	@Override
	public Response<GeneraliAgent> retrieveAgentByODSCode(Request<GeneraliAgent> agentRequest) {
		Response<GeneraliAgent> agentResponse = new ResponseImpl<GeneraliAgent>();
		agentResponse.setType(agentDao.retrieveAgentProfileByCode(agentRequest));
		return agentResponse;
	}
	
	/*@Override
    public Response<List<GeneraliAgentProduct>> retrieveProductsByCode(Request<List<String>> agentRequest) {
        Response<List<GeneraliAgentProduct>> agentProdResponse = new ResponseImpl<List<GeneraliAgentProduct>>();
        agentProdResponse.setType(agentDao.retrieveAgentProductByCode(agentRequest));
        return agentProdResponse;
    }*/
	
	@Override
    public Response<List<ProductSpecification>> retrieveProductsByCode(Request<ProductSpecification> agentProductRequest) {
        Response<List<ProductSpecification>> agentProdResponse = new ResponseImpl<List<ProductSpecification>>();
        agentProdResponse.setType(agentDao.retrieveAgentProductByCode(agentProductRequest));
        return agentProdResponse;
    }
	
	@Override
	public List<GeneraliAgent> retrieveAgentByODSForReassign(Request<GeneraliAgent> request) {
		/*Response<List<GeneraliAgent>> agentResponse = new ResponseImpl<GeneraliAgent>();
		agentResponse.setType(agentDao.retrieveAgentProfileByCode(request));*/
		//agentDao.retrieveAgentProfileByCode(request)
		return agentDao.retrieveAgentProfileForReassign(request);
	}
	
	@Override
	public List<GeneraliAgent> retrieveActiveAgentByODSToReassign(Request<GeneraliAgent> request) {		
		return agentDao.retrieveActiveAgentProfileToReassign(request);
	}
	
	@Override
	public List<Customer> retrieveLeads(Request<Customer> request) {
		/*Response<List<GeneraliAgent>> agentResponse = new ResponseImpl<GeneraliAgent>();
		agentResponse.setType(agentDao.retrieveAgentProfileByCode(request));*/
		//agentDao.retrieveAgentProfileByCode(request)
		return agentDao.retrieveLeads(request);
	}

	
	@Override
	public Integer retrieveIllustrationCount(Request<Customer> request) {
		return agentDao.retrieveIllustrationCount(request);
	}
	
	@Override
	public Integer retrieveEappCount(Request<Customer> request) {
		return agentDao.retrieveEappCount(request);
	}
	
	@Override
	public Integer retrieveFnaCount(Request<Customer> request) {
		return agentDao.retrieveFnaCount(request);
	}
	
	@Override
	public String retrieveLeadIdForFna(Request<Customer> request) {
		return agentDao.retrieveLeadIdForFna(request);
	}

	@Override
	public List<GoalAndNeed> retrieveGoalAndNeed(Request<GoalAndNeed> agentRequest) {
		return agentDao.retrieveGoalAndNeed(agentRequest);
	}
	
	@Override
	public List<Agreement> retrieveAgreement(Request<InsuranceAgreement> agentRequest) {
		return agentDao.retrieveAgreement(agentRequest);
	}
	
	@Override
	public Response<List<AgentAchievement>> retrieveAchievements(Request<AgentAchievement> agentAchievementRequest) {
		Response<List<AgentAchievement>> agentAchievementResponse = new ResponseImpl<List<AgentAchievement>>();
		agentAchievementResponse.setType(agentDao.retrieveAgentAchievements(agentAchievementRequest));
		return agentAchievementResponse;
	}
	
	@Override
	public List<InsuranceAgreement> retrieveIllustration(Request<InsuranceAgreement> agentRequest) {
		return agentDao.retrieveIllustration(agentRequest);
	}

	    
	@Override
	public Response<GeneraliGAOAgent> retrieveGAOAgentByODSCode(Request<GeneraliGAOAgent> agentRequest) {
		Response<GeneraliGAOAgent> agentResponse = new ResponseImpl<GeneraliGAOAgent>();
		agentResponse.setType(agentDao.retrieveGAOAgentProfileByCode(agentRequest));
		return agentResponse;
	}
	
	@Override
	public Response<GeneraliAgentAddress> retrieveGAOAddress(Request<GeneraliAgentAddress> agentRequest) {
		Response<GeneraliAgentAddress> addressResponse = new ResponseImpl<GeneraliAgentAddress>();
		addressResponse.setType(agentDao.retrieveGAOAddress(agentRequest));
		return addressResponse;
	}

}
