/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 19, 2015
 */
package com.cognizant.insurance.component.repository;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;

//@Repository
/**
 * The Class class EncryptionKeyRepository.
 */
@Repository
public class EncryptionKeyRepository {

	/** The fetch key devic holder. */
    @Autowired
    @Qualifier("fetchKeymapping")
    private LifeEngageSmooksHolder fetchKeyHolder;
    
    /** The log path. */
    @Value("${le.platform.service.encryptionkeyPath}")
    private String encryptionkeyPath;
    
    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(EncryptionKeyRepository.class);

    /**
     * Fetch key.
     *
     * @return the string
     */
    public String fetchKey() {
    	Transactions transactions = new Transactions();
        String response = "{}";
        try {
         	final File file = new File(encryptionkeyPath + "cacert.pem");

             // if file doesnt exists, then create it
             if (file.exists()) {
             	 FileInputStream fis = new FileInputStream(file);
                  DataInputStream dis = new DataInputStream(fis);
                  byte[] keyBytes = new byte[(int) file.length()];
                  dis.readFully(keyBytes);
                  dis.close();
                  String temp = new String(keyBytes);
                  String publicKeyPEM = temp.replace("-----BEGIN CERTIFICATE-----\n", "");
                  publicKeyPEM = publicKeyPEM.replace("-----END CERTIFICATE-----", "");
                  publicKeyPEM = publicKeyPEM.replace("\n", "");
                  transactions.setEncryptionKey(publicKeyPEM);
                  final StatusData statusData = new StatusData();
                  statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
                  statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE_FETCH_ENCRYPTION_KEY);
                  statusData.setStatusMessage(LifeEngageComponentHelper.FETCH_ENCRYPTION_KEY_SUCCESS);
                  transactions.setStatusData(statusData);
                  response = fetchKeyHolder.parseBO(transactions);
             }else{
            	 final StatusData statusData = new StatusData();
                 statusData.setStatus(LifeEngageComponentHelper.FAILURE);
                 statusData.setStatusCode(LifeEngageComponentHelper.FAILURE_CODE);
                 statusData.setStatusMessage(LifeEngageComponentHelper.FETCH_ENCRYPTION_KEY_MISSING);
                 transactions.setStatusData(statusData);
                 response = fetchKeyHolder.parseBO(transactions);
             }
        } catch (Exception e) {
             LOGGER.error("Exception", e);
             LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                     LifeEngageComponentHelper.FETCH_ENCRYPTION_KEY_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
             response = fetchKeyHolder.parseBO(transactions);

        }
        return response;
     
    	
    }

    
}
