package com.cognizant.le.model.dto;

/**
 * The Class Carrier.
 */
public class Carrier extends MasterDBEntry {

	/** The carrier id. */
	private int carrierId;

	/** The type name. */
	private String carrierName;

	/**
	 * Gets the carrier id.
	 * 
	 * @return the carrier id
	 */
	public int getCarrierId() {
		return carrierId;
	}

	/**
	 * Sets the carrier id.
	 * 
	 * @param carrierId
	 *            the new carrier id
	 */
	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}

	/**
	 * Gets the type name.
	 * 
	 * @return the type name
	 */
	public String getCarrierName() {
		return carrierName;
	}

	/**
	 * Sets the type name.
	 * 
	 * @param typeName
	 *            the new type name
	 */
	public void setCarrierName(String typeName) {
		this.carrierName = typeName;
	}
}
