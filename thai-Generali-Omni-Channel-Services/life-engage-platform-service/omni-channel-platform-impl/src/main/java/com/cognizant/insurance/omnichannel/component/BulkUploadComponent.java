package com.cognizant.insurance.omnichannel.component;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.cognizant.insurance.omnichannel.vo.BulkUploadData;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * Interface BulkUploadComponent
 * 
 * @author 390229
 * 
 */
public interface BulkUploadComponent {

	Map<String, List<BulkUploadData>> readAgency(String fileName, String user);

	Map<String, List<BulkUploadData>> readBanca(String fileName, String user);

	Map<String, String> saveBulkUploadAgencyLead(
			Map<String, List<BulkUploadData>> agencyLeadMap, String type,
			Map<String, Integer> pushNotificationMap,
			Map<String, Map<String, String>> lookUpData);

	RequestInfo createRequestInfo(String json) throws ParseException;

	void moveFiles(String fileName);

	Map<String, Map<String, String>> getLookUpData();
}
