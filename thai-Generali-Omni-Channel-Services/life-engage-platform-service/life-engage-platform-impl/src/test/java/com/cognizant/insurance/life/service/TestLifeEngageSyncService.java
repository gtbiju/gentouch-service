/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 22, 2013
 */
package com.cognizant.insurance.life.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LifeEngageSyncService;

/**
 * The Class class TestLifeEngageSyncService.
 * 
 * @author 300797
 */
@ContextConfiguration("/service-context-test.xml")
public class TestLifeEngageSyncService extends AbstractJUnit4SpringContextTests {

    /** The Constant APPOINTEE. */
    private static final String APPOINTEE = "Appointee";

    /** The Constant BASIC_DETAILS. */
    private static final String BASIC_DETAILS = "BasicDetails";

    /** The Constant BENEFICIARIES. */
    private static final String BENEFICIARIES = "Beneficiaries";

    /** The Constant CONTACT_DETAILS. */
    private static final String CONTACT_DETAILS = "ContactDetails";

    /** The Constant CUST_RELTNSHP. */
    private static final String CUST_RELTNSHP = "CustomerRelationship";

    /** The Constant DECLARATION. */
    private static final String DECLARATION = "Declaration";

    /** The Constant FLYHLTHHISTRY. */
    private static final String FLYHLTHHISTRY = "FamilyHealthHistory";

    /** The Constant FIRST_NAME. */
    private static final String FIRST_NAME = "firstName";

    /** The Constant FUND_INFORMATION. */
    private static final String FUND_INFORMATION = "FundInformation";

    /** The Constant HEALTHDETAILS. */
    private static final String HEALTHDETAILS = "HealthDetails";

    /** The Constant INCOME_DETAILS. */
    private static final String INCOME_DETAILS = "IncomeDetails";

    /** The Constant INSURED. */
    private static final String INSURED = "Insured";

    /** The Constant KEY2. */
    private static final String KEY2 = "Key2";

    /** The Constant KEY4. */
    private static final String KEY4 = "Key4";

    /** The Constant KEY6. */
    private static final String KEY6 = "Key6";

    /** The Constant KEY7. */
    private static final String KEY7 = "Key7";
    
    /** The Constant KEY11. */
    private static final String KEY11 = "Key11";

    /** The Constant KEY13. */
    private static final String KEY13 = "Key13";
    
    /** The Constant KEY15. */
    private static final String KEY15 = "Key15";
    
    /** The Constant KEY16. */
    private static final String KEY16 = "Key16";
    
    /** The Constant LAST_NAME. */
    private static final String LAST_NAME = "lastName";

    /** The Constant LIFESTYLEDETAILS. */
    private static final String LIFESTYLEDETAILS = "LifestyleDetails";

    /** The Constant OCCUPTN_DTLS. */
    private static final String OCCUPTN_DTLS = "OccupationDetails";

    /** The Constant PAYER. */
    private static final String PAYER = "Payer";

    /** The Constant PREVSPOLCYHISTY. */
    private static final String PREVSPOLCYHISTY = "PreviouspolicyHistory";

    /** The Constant PRODUCT. */
    private static final String PRODUCT = "Product";

    /** The Constant PRODUCT_DETAILS. */
    private static final String PRODUCT_DETAILS = "ProductDetails";

    /** The Constant PRODUCT_NAME. */
    private static final String PRODUCT_NAME = "productName";

    /** The Constant PRODUCT_TYPE. */
    private static final String PRODUCT_TYPE = "productType";

    /** The Constant PROPOSER. */
    private static final String PROPOSER = "Proposer";

    /** The Constant QUESTIONNAIRE. */
    private static final String QUESTIONNAIRE = "Questionnaire";

    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";

    /** The Constant RIDER_DETAILS. */
    private static final String RIDER_DETAILS = "RiderDetails";

    /** The Constant STATUS. */
    private static final String STATUS = "Status";

    /** The Constant STATUS_CODE. */
    private static final String STATUS_CODE = "StatusCode";

    /** The Constant STATUS_DATA. */
    private static final String STATUS_DATA = "StatusData";

    /** The Constant STATUS_MESSAGE. */
    private static final String STATUS_MESSAGE = "StatusMessage";

    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant UPLOAD. */
    private static final String UPLOAD = "Upload";

    /** The data source. */
    @Autowired
    private DataSource dataSource;

    /** The life engage sync service. */
    @Autowired
    LifeEngageSyncService lifeEngageSyncService;

    /*
     * Save observations.
     * 
     * @throws IOException Signals that an I/O exception has occurred.
     * 
     * @throws BusinessException the business exception
     */
          
    
    /**
     * Save observations.
     * 
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws BusinessException
     *             the business exception
     */
    @Test
    public void saveObservations() throws IOException, BusinessException {

        String responseJson =
                lifeEngageSyncService.saveObservations(getRequest("observationRequest/observations.json"));
        assertNotNull(responseJson);
        assertEquals("Success", responseJson);
    }

    /**
     * Get status.
     * 
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws BusinessException
     *             the business exception
     * @throws ParseException 
     */
    @Test
    public void getStatus() throws IOException, BusinessException, ParseException {
    	/*
    	String getStatusRequest = getRequest("eAppRequest/ValidateProposalRq_getStatus.json");
		String getStatusResponseJson = lifeEngageSyncService.getStatus(getStatusRequest);
		System.out.println(getStatusResponseJson);
    	*/
		
    	String agentId;
		final SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final Calendar calendar = new GregorianCalendar();
		final String currentDate = newDt.format(calendar.getTime());
		String proposalNumber = null;
		JSONObject jsonObj = null;		
		agentId = "Agent4";
		String productTypeFirst = "Term";
		String insFstNmeFst = "Sanvi";
		String proposerFirstNameFirst = "Sanvi";
		String proposerLastNameFirst = "Duth";
		String productFirstproductname = "lifeEngage";
		
		/** creating the eapp save request */
		String saveRequest1 = null;
		saveRequest1 = getRequest("eAppRequest/eAppServiceRq.json");
		String saveRequest = saveRequest1.replace("{1}", agentId)
				.replace("{2}", currentDate)
				.replace("{3}", productFirstproductname)
				.replace("{4}", productTypeFirst).replace("{5}", proposerFirstNameFirst)
				.replace("{6}", insFstNmeFst)
				.replace("{7}", proposerLastNameFirst)
				.replace("{8}", proposerFirstNameFirst).replace("{9}", "")
				.replace("{10}", "");;

		/** Saving eapp Request **/
		
		String responseJson = null;
		responseJson = lifeEngageSyncService.save(saveRequest);
		
		jsonObj = new JSONObject(responseJson);
		JSONObject jsonResponseObj = jsonObj.getJSONObject(RESPONSE);
		JSONObject jsonResponsePayloadObj = jsonResponseObj
				.getJSONObject(RESPONSE_PAYLOAD);
		JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj
				.getJSONArray(TRANSACTIONS);
		jsonObj = new JSONObject(responseJson);
		jsonResponseObj = jsonObj.getJSONObject(RESPONSE);
		jsonResponsePayloadObj = jsonResponseObj
				.getJSONObject(RESPONSE_PAYLOAD);
		jsonRsTransactionsArray = jsonResponsePayloadObj
				.getJSONArray(TRANSACTIONS);
		assertEquals(1, jsonRsTransactionsArray.length());
		jsonObj = jsonRsTransactionsArray.getJSONObject(0);
		JSONObject statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
		proposalNumber = jsonObj.getString(KEY4);
		statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
		assertNotNull(statusDataObj);
		
		/** Get Status of the saved eapp**/
		getStatusOfSavedProposal(currentDate, proposalNumber);
		
    }
    
    
    /**
	 * Get Status of the saved proposal.
	 * @param currentDate
	 * @param proposalNumber
	 * @throws IOException
	 * @throws BusinessException
	 * @throws ParseException
	 */
	private void getStatusOfSavedProposal(final String currentDate,
			 String proposalNumber) throws IOException,
			BusinessException, ParseException {
		JSONObject jsonResponseObj;
		JSONObject jsonResponsePayloadObj;
		JSONArray jsonRsTransactionsArray;
		JSONObject jsonObj = null;
		/** Get Status of the saved proposal **/

		String VALID_PROPOSAL_MESSAGE = "Valid Proposal";
		String VALID_CODE = "101";
		String VALID = "Valid";
		
		String getStatusRequest = getRequest("eAppRequest/ValidateProposalRq_getStatus.json");

		/** set the proposal number to get status **/		
		
				getStatusRequest = getStatusRequest.replace("{1}", currentDate)
				.replace("{2}", proposalNumber);

		/** Execute the getStatus request **/
		
		String getStatusResponseJson = lifeEngageSyncService.getStatus(getStatusRequest);
		//System.out.println(getStatusResponseJson);
		jsonObj = new JSONObject(getStatusResponseJson);

		/** Verify response **/
		jsonResponseObj = jsonObj.getJSONObject(RESPONSE);
		jsonResponsePayloadObj = jsonResponseObj
				.getJSONObject(RESPONSE_PAYLOAD);
		jsonRsTransactionsArray = jsonResponsePayloadObj
				.getJSONArray(TRANSACTIONS);
		assertEquals(1, jsonRsTransactionsArray.length());

		/** verify the Transaction object **/
		jsonObj = jsonRsTransactionsArray.getJSONObject(0);

		/** verify the proposal number **/
		assertNotNull(jsonObj.getString(KEY4));

		/** Verify the Status object **/
		JSONObject getStatusDataObj = jsonObj.getJSONObject(STATUS_DATA);

		/** Verify the delete status */		
		assertNotNull(getStatusDataObj);
		assertEquals(VALID_CODE, getStatusDataObj.getString(STATUS_CODE));
		assertEquals(VALID, getStatusDataObj.getString(STATUS));
		assertEquals(VALID_PROPOSAL_MESSAGE,
				getStatusDataObj.getString(STATUS_MESSAGE));
	}
    
    
    
    /**
     * Save with exception.
     * 
     * @throws BusinessException
     *             the business exception
     */
    @Test(expected = BusinessException.class)
    public void saveWithException() throws BusinessException {
        lifeEngageSyncService.save("Test");
    }

    /**
     * This Method Tests the eapp Save, retrieve, retreiveAll(basic details and full details) methods.
     * 
     * @throws BusinessException
     *             the business exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public void eappTest() throws BusinessException, IOException, ParseException {
        stprule();
        String agentId;
        final SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Calendar calendar = new GregorianCalendar();
        final String currentDate = newDt.format(calendar.getTime());
        agentId = "Agent101";
        String productTypeFirst = "Term";
        String insFstNmeFst = "Johans";
        String insuredLastNameFirst = "Nems";
        String proposerFirstNameFirst = "Johns";
        String proposerLastNameFirst = "Nemaas";
        String productFirstproductname = "lifeEngage";
        String productTypeSecond = "WholeLife";
        String insuredFirstNameSecond = "Philip";
        String insuredLastNameSecond = "Cartor";
        String proposerFirstNameSecond = "Philips";
        String proposerLastNameSecond = "Cartors";
        String productSecondproductname = "lifeengage";
        String eappStatusInitial = "Initial";
        String eappStatusRejected = "Rejected";
        String statusCode100 = "100";
        String statusSuccess = "SUCCESS";
        String statusCode400 = "400";
        String rejectedCode102 = "102";
        String statusRejected = "REJECTED";
        String statusFailure = "FAILURE";
        String statusMessage = "Proposal Saved";
        String status = "SUCCESS";
        String statusMessageUpdate = "Proposal Updated";
        String rejectedMessage = "Proposal Rejected";
        String type = "eApp";

        // creating the first eapp save request
        String saveRequest1 = getRequest("eAppRequest/eAppServiceRq.json");
        String saveRequest =
                saveRequest1.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", productFirstproductname)
                    .replace("{4}", productTypeFirst)
                    .replace("{5}", insuredLastNameFirst)
                    .replace("{6}", insFstNmeFst)
                    .replace("{7}", proposerLastNameFirst)
                    .replace("{8}", proposerFirstNameFirst)
                    .replace("{9}", "")
                    .replace("{10}", "");;

        // Saving first eapp Request
        String responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        JSONObject jsonObject = new JSONObject(responseJson);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(eappStatusInitial, jsonObj.getString(KEY15));
        String eappIdFirst = jsonObj.getString(KEY4);
        assertNotNull(eappIdFirst);
        JSONObject statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));
        assertEquals(status, jsonObj.getString(KEY16));
        
        // Trying to save same eapp Request
        responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals("", jsonObj.getString(KEY15));
        assertEquals("", jsonObj.getString(KEY4));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode400, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusFailure, statusDataObj.getString(STATUS));
        assertTrue(statusDataObj.getString(STATUS_MESSAGE).contains("This Trasaction Data is already processed"));

        // Retrieving the first saved eapp
        String retrieveRequest = getRequest("eAppRequest/eAppServiceRq_Retrieve.json");
        retrieveRequest = retrieveRequest.replace("{1}", currentDate).replace("{2}", eappIdFirst);
        // System.out.println(retrieveRequest);
        String retrieveResponseJson = lifeEngageSyncService.retrieve(retrieveRequest);
        // System.out.println(retrieveResponseJson);
        assertNotNull(retrieveResponseJson);
        jsonObject = new JSONObject(retrieveResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(0).getString(KEY11));
        //assertEquals(currentDate, jsonRsTransactionsArray.getJSONObject(0).getString(KEY13));
        assertEquals(eappIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY4));
        assertEquals(type, jsonRsTransactionsArray.getJSONObject(0).getString(TYPE));
        assertEquals(eappStatusInitial, jsonRsTransactionsArray.getJSONObject(0).getString(KEY15));
        assertEquals(2, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONArray(FUND_INFORMATION)
            .length());
        assertEquals(2, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONArray(RIDER_DETAILS)
            .length());
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONObject(PRODUCT_DETAILS));
        assertEquals(productTypeFirst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONObject(PRODUCT_DETAILS)
            .getString(PRODUCT_TYPE));
        assertEquals(productFirstproductname, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONObject(PRODUCT_DETAILS)
            .getString(PRODUCT_NAME));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(INSURED));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(BASIC_DETAILS));
        assertEquals(insFstNmeFst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(BASIC_DETAILS)
            .getString(FIRST_NAME));
        assertEquals(insuredLastNameFirst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(BASIC_DETAILS)
            .getString(LAST_NAME));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(OCCUPTN_DTLS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(CUST_RELTNSHP));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(CONTACT_DETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(INCOME_DETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(DECLARATION));
        assertEquals(2,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONArray(BENEFICIARIES)
                    .length());
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(PROPOSER));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PROPOSER)
            .getJSONObject(OCCUPTN_DTLS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PROPOSER)
            .getJSONObject(CONTACT_DETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PROPOSER)
            .getJSONObject(INCOME_DETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PROPOSER)
            .getJSONObject(BASIC_DETAILS));
        assertEquals(proposerFirstNameFirst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PROPOSER)
            .getJSONObject(BASIC_DETAILS)
            .getString(FIRST_NAME));
        assertEquals(proposerLastNameFirst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PROPOSER)
            .getJSONObject(BASIC_DETAILS)
            .getString(LAST_NAME));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(APPOINTEE));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(APPOINTEE)
            .getJSONObject(CUST_RELTNSHP));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(APPOINTEE)
            .getJSONObject(CONTACT_DETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(APPOINTEE)
            .getJSONObject(BASIC_DETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(APPOINTEE)
            .getJSONObject(INCOME_DETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(PAYER));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PAYER)
            .getJSONObject(BASIC_DETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PAYER)
            .getJSONObject(CUST_RELTNSHP));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PAYER)
            .getJSONObject(INCOME_DETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(UPLOAD));        
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(QUESTIONNAIRE));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(QUESTIONNAIRE)
            .getJSONObject(LIFESTYLEDETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(QUESTIONNAIRE)
            .getJSONObject(FLYHLTHHISTRY));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(QUESTIONNAIRE)
            .getJSONObject(HEALTHDETAILS));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(QUESTIONNAIRE)
            .getJSONObject(PREVSPOLCYHISTY));

        // creating the Second eapp save request
        saveRequest = getRequest("eAppRequest/eAppServiceRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", productSecondproductname)
                    .replace("{4}", productTypeSecond)
                    .replace("{5}", insuredLastNameSecond)
                    .replace("{6}", insuredFirstNameSecond)
                    .replace("{7}", proposerLastNameSecond)
                    .replace("{8}", proposerFirstNameSecond)
                    .replace("{9}", "").replace("{10}", "");;

        // Saving Second eappRequest
        responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(eappStatusInitial, jsonObj.getString(KEY15));
        String eappIdSecond = jsonObj.getString(KEY4);
        assertNotNull(eappIdSecond);
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));

        // Retrieving the eapp list - basic details
        String retrieveListRequest = getRequest("eAppRequest/eAppServiceRq_RetrieveList.json");
        retrieveListRequest =
                retrieveListRequest.replace("{1}", "").replace("{2}", agentId).replace("{3}", BASIC_DETAILS);
        String retrieveListResponseJson = lifeEngageSyncService.retrieveAll(retrieveListRequest);
        // System.out.println(retrieveListResponseJson);
        assertNotNull(retrieveListResponseJson);
        jsonObject = new JSONObject(retrieveListResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(2, jsonRsTransactionsArray.length());
        for (int i = 0; i < jsonRsTransactionsArray.length(); i++) {
            assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(i).getString(KEY11));
            //assertEquals(currentDate, jsonRsTransactionsArray.getJSONObject(i).getString(KEY13));
            assertEquals(type, jsonRsTransactionsArray.getJSONObject(i).getString(TYPE));
            assertEquals(eappStatusInitial, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
            assertEquals(false,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .has(FUND_INFORMATION));
            assertEquals(false,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .has(RIDER_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(PRODUCT_DETAILS));

            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PROPOSER)
                .getJSONObject(BASIC_DETAILS));
            assertEquals(true, jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).has(INSURED));
            assertEquals(false,
                    jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).has(DECLARATION));
            assertEquals(false,
                    jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).has(BENEFICIARIES));
            assertEquals(false, jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).has(APPOINTEE));
            assertEquals(false, jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).has(UPLOAD));
            assertEquals(false,
                    jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).has(QUESTIONNAIRE));
            assertEquals(false, jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).has(PAYER));
            if (eappIdFirst.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY4))) {

                assertEquals(productFirstproductname,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PRODUCT)
                            .getJSONObject(PRODUCT_DETAILS)
                            .getString(PRODUCT_NAME));

                assertEquals(proposerFirstNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(proposerLastNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(LAST_NAME));
            } else if (eappIdSecond.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY4))) {

                assertEquals(productSecondproductname,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PRODUCT)
                            .getJSONObject(PRODUCT_DETAILS)
                            .getString(PRODUCT_NAME));

                assertEquals(proposerFirstNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(proposerLastNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(LAST_NAME));
            }

        }

        // Retrieving the eapp list - Full details
        retrieveListRequest = getRequest("eAppRequest/eAppServiceRq_RetrieveList.json");
        retrieveListRequest =
                retrieveListRequest.replace("{1}", "").replace("{2}", agentId).replace("{3}", "FullDetails");
        retrieveListResponseJson = lifeEngageSyncService.retrieveAll(retrieveListRequest);
        // System.out.println(retrieveListResponseJson);
        assertNotNull(retrieveListResponseJson);
        jsonObject = new JSONObject(retrieveListResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(2, jsonRsTransactionsArray.length());
        for (int i = 0; i < jsonRsTransactionsArray.length(); i++) {
            assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(i).getString(KEY11));
//            assertEquals(currentDate, jsonRsTransactionsArray.getJSONObject(i).getString(KEY13));
            assertEquals(type, jsonRsTransactionsArray.getJSONObject(i).getString(TYPE));
            assertEquals(eappStatusInitial, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
            assertEquals(2,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .getJSONArray(FUND_INFORMATION)
                        .length());
            assertEquals(2,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .getJSONArray(RIDER_DETAILS)
                        .length());
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(PRODUCT_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PROPOSER));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PROPOSER)
                .getJSONObject(BASIC_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(DECLARATION));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(APPOINTEE));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(APPOINTEE)
                .getJSONObject(BASIC_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(APPOINTEE)
                .getJSONObject(CONTACT_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(APPOINTEE)
                .getJSONObject(INCOME_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(UPLOAD));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(QUESTIONNAIRE));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(QUESTIONNAIRE)
                .getJSONObject(LIFESTYLEDETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(QUESTIONNAIRE)
                .getJSONObject(FLYHLTHHISTRY));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(QUESTIONNAIRE)
                .getJSONObject(HEALTHDETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(QUESTIONNAIRE)
                .getJSONObject(PREVSPOLCYHISTY));
            assertEquals(2,
                    jsonRsTransactionsArray.getJSONObject(0)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONArray(BENEFICIARIES)
                        .length());
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONObject(PAYER));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PAYER)
                .getJSONObject(BASIC_DETAILS));
            if (eappIdFirst.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY4))) {

                assertEquals(productFirstproductname,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PRODUCT)
                            .getJSONObject(PRODUCT_DETAILS)
                            .getString(PRODUCT_NAME));

                assertEquals(proposerFirstNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(proposerLastNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(LAST_NAME));
            } else if (eappIdSecond.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY4))) {

                assertEquals(productSecondproductname,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PRODUCT)
                            .getJSONObject(PRODUCT_DETAILS)
                            .getString(PRODUCT_NAME));
                assertEquals(proposerFirstNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(proposerLastNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(PROPOSER)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(LAST_NAME));
            }
        }

        // creating the first eapp update request
        saveRequest1 = getRequest("eAppRequest/eAppServiceRq.json");
        saveRequest =
                saveRequest1.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", productFirstproductname)
                    .replace("{4}", productTypeFirst)
                    .replace("{5}", insuredLastNameFirst)
                    .replace("{6}", insFstNmeFst)
                    .replace("{7}", proposerLastNameFirst)
                    .replace("{8}", proposerFirstNameFirst)
                    .replace("{9}", eappIdFirst).replace("{10}", "");;

        // updating first eapp Request
        responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(eappStatusInitial, jsonObj.getString(KEY15));
        assertEquals(eappIdFirst, jsonObj.getString(KEY4));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessageUpdate, statusDataObj.getString(STATUS_MESSAGE));
        
        // creating the first eapp update request again with ConflictResolution true and sourceOfTruth as server
        saveRequest1 = getRequest("eAppRequest/eAppServiceRq.json");
        calendar.add(Calendar.DATE, -1); 
        final String previousDate = newDt.format(calendar.getTime());
        saveRequest =
                saveRequest1.replace("{1}", agentId)
                    .replace("{2}", previousDate)
                    .replace("{3}", productFirstproductname)
                    .replace("{4}", productTypeFirst)
                    .replace("{5}", insuredLastNameFirst)
                    .replace("{6}", insFstNmeFst)
                    .replace("{7}", proposerLastNameFirst)
                    .replace("{8}", proposerFirstNameFirst)
                    .replace("{9}", eappIdFirst)
                    .replace("{10}", previousDate);

        // updating first eapp Request with ConflictResolution true and sourceOfTruth as server
        responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(eappStatusRejected, jsonObj.getString(KEY15));
        assertEquals(eappIdFirst, jsonObj.getString(KEY4));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(rejectedCode102, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusRejected, statusDataObj.getString(STATUS));
        assertEquals(rejectedMessage, statusDataObj.getString(STATUS_MESSAGE));

    }
   /**
    * Test case for delete Eapp operation 
    * @throws BusinessException
    * @throws IOException
    * @throws ParseException
    */
    @Test
	public final void deleteEappTest() throws BusinessException, IOException,
			ParseException {
		String agentId;
		final SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final Calendar calendar = new GregorianCalendar();
		final String currentDate = newDt.format(calendar.getTime());
		String proposalNumber = null;
		JSONObject jsonObj = null;		
		agentId = "Agent7";
		String productTypeFirst = "Term";
		String insFstNmeFst = "Johan";
		String proposerFirstNameFirst = "Johnn1";
		String proposerLastNameFirst = "Nemaai";
		String productFirstproductname = "lifeEngage";
		
		/** creating the eapp save request */
		String saveRequest1 = null;
		saveRequest1 = getRequest("eAppRequest/eAppServiceRq.json");
		String saveRequest = saveRequest1.replace("{1}", agentId)
				.replace("{2}", currentDate)
				.replace("{3}", productFirstproductname)
				.replace("{4}", productTypeFirst).replace("{5}", proposerFirstNameFirst)
				.replace("{6}", insFstNmeFst)
				.replace("{7}", proposerLastNameFirst)
				.replace("{8}", proposerFirstNameFirst).replace("{9}", "").replace("{10}", "");

		/** Saving eapp Request **/
		
		String responseJson = null;
		responseJson = lifeEngageSyncService.save(saveRequest);
		
		jsonObj = new JSONObject(responseJson);
		JSONObject jsonResponseObj = jsonObj.getJSONObject(RESPONSE);
		JSONObject jsonResponsePayloadObj = jsonResponseObj
				.getJSONObject(RESPONSE_PAYLOAD);
		JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj
				.getJSONArray(TRANSACTIONS);
		jsonObj = new JSONObject(responseJson);
		jsonResponseObj = jsonObj.getJSONObject(RESPONSE);
		jsonResponsePayloadObj = jsonResponseObj
				.getJSONObject(RESPONSE_PAYLOAD);
		jsonRsTransactionsArray = jsonResponsePayloadObj
				.getJSONArray(TRANSACTIONS);
		assertEquals(1, jsonRsTransactionsArray.length());
		jsonObj = jsonRsTransactionsArray.getJSONObject(0);
		JSONObject statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
		proposalNumber = jsonObj.getString(KEY4);
		statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
		assertNotNull(statusDataObj);

		/** Delete the saved eapp**/
		deleteSavedEAppProposal(currentDate, proposalNumber);		
	}
	
	/**
	 * Deleting the saved eapp.
	 * @param currentDate
	 * @param proposalNumber
	 * @throws IOException
	 * @throws BusinessException
	 * @throws ParseException
	 */
	private void deleteSavedEAppProposal(final String currentDate,
			 String proposalNumber) throws IOException,
			BusinessException, ParseException {
		JSONObject jsonResponseObj;
		JSONObject jsonResponsePayloadObj;
		JSONArray jsonRsTransactionsArray;
		JSONObject jsonObj = null;
		/** Deleting the saved eapp **/

		String SUCCESS_MESSAGE_DELETE = "Proposal Updated";
		String statusCode100 = "100";
		String statusSuccess = "SUCCESS";
		
		String deleteRequest = getRequest("eAppRequest/eAppServiceRq_Delete.json");

		/** set the proposal number to be deleted **/		
		
		deleteRequest = deleteRequest.replace("{1}", "")
				.replace("{2}", proposalNumber);

		/** Execute the delete request **/
		
		String deleteResponseJson = lifeEngageSyncService.delete(deleteRequest);
		jsonObj = new JSONObject(deleteResponseJson);

		/** Verify response **/
		jsonResponseObj = jsonObj.getJSONObject(RESPONSE);
		jsonResponsePayloadObj = jsonResponseObj
				.getJSONObject(RESPONSE_PAYLOAD);
		jsonRsTransactionsArray = jsonResponsePayloadObj
				.getJSONArray(TRANSACTIONS);
		assertEquals(1, jsonRsTransactionsArray.length());

		/** verify the Transaction object **/
		jsonObj = jsonRsTransactionsArray.getJSONObject(0);

		/** verify the proposal number **/
		assertNotNull(jsonObj.getString(KEY4));

		/** Verify the Status object **/
		JSONObject deleteStatusDataObj = jsonObj.getJSONObject(STATUS_DATA);

		/** Verify the delete status */		
		assertNotNull(deleteStatusDataObj);
		assertEquals(statusCode100, deleteStatusDataObj.getString(STATUS_CODE));
		assertEquals(statusSuccess, deleteStatusDataObj.getString(STATUS));
		assertEquals(SUCCESS_MESSAGE_DELETE,
				deleteStatusDataObj.getString(STATUS_MESSAGE));
	}
	
	    
    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(new String(line.getBytes(), "UTF-8"));
        }

        return sb.toString();
    }

    /**
     * Gets the data source.
     * 
     * @return the data source
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     * 
     * @param dataSource
     *            the data source to set.
     */
    public void setDataSource(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Before
    public void stprule() {

        try {

            QueryRunner quryrnr = new QueryRunner(dataSource);

            quryrnr.update("Drop table IF EXISTS `rule_table`;");

            quryrnr.update("CREATE TABLE rule_table (Id int(11) NOT NULL AUTO_INCREMENT,RuleName varchar(100) DEFAULT NULL,RuleDesc varchar(8000) DEFAULT NULL,Category varchar(20) DEFAULT NULL,ClientId varchar(20) DEFAULT NULL,PRIMARY KEY (Id))");

            quryrnr.update("INSERT INTO rule_table (RuleName, RuleDesc, Category, ClientId) VALUES ('STP_InitValidation','function STP_InitValidation(inoutMap){var STPStatus = \"Pass\"; updateJSON(inoutMap, \"STPStatus\", STPStatus);ValidationResults = {}; updateJSON(inoutMap, \"ValidationResults\", ValidationResults);return JSON.stringify(inoutMap);}','STPRule', 'INS');");

            quryrnr.update("INSERT INTO rule_table (RuleName, RuleDesc, Category, ClientId) VALUES ('STPRules', 'function STPRules(inoutMap) {STP_InitValidation(inoutMap); LowerAgeLimit(inoutMap); UpperAgeLimit(inoutMap); MinSumAssuredLimit(inoutMap); MaxSumAssuredLimit(inoutMap); AnnualIncomeLimit(inoutMap); IsNonMedicalCase(inoutMap); return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO rule_table (RuleName, RuleDesc, Category, ClientId) VALUES ('LowerAgeLimit', 'function LowerAgeLimit(inoutMap) {dobStr = (inoutMap.TransactionData.Insured.BasicDetails.dob).replace(/-/g,\"/\"); age = getAge(dobStr); if (Number(age) < Number(inoutMap.TransactionData.Product.ProductDetails.minInsuredAge) ) {inoutMap.ValidationResults.LowerAgeLimit = (\"Insured Age less than allowed.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO rule_table (RuleName, RuleDesc, Category, ClientId) VALUES ('UpperAgeLimit', 'function UpperAgeLimit(inoutMap) {dobStr = (inoutMap.TransactionData.Insured.BasicDetails.dob).replace(/-/g,\"/\"); age = getAge(dobStr); if (Number(age) > Number(inoutMap.TransactionData.Product.ProductDetails.maxInsuredAge) ) {inoutMap.ValidationResults.UpperAgeLimit = (\"Insured Age greater than allowed.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO rule_table (RuleName, RuleDesc, Category, ClientId) VALUES ('MinSumAssuredLimit', 'function MinSumAssuredLimit(inoutMap) {sumAssured = (inoutMap.TransactionData.Product.ProductDetails.sumAssured); if (Number(sumAssured) < Number(inoutMap.TransactionData.Product.ProductDetails.minSumAssured)){inoutMap.ValidationResults.MinSumAssuredLimit = (\"SumAssured Less than Minimum Sum Assured for the Product.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO rule_table (RuleName, RuleDesc, Category, ClientId) VALUES ('MaxSumAssuredLimit', 'function MaxSumAssuredLimit(inoutMap) {sumAssured = (inoutMap.TransactionData.Product.ProductDetails.sumAssured); if (Number(sumAssured) > Number(inoutMap.TransactionData.Product.ProductDetails.maxSumAssured)){inoutMap.ValidationResults.MaxSumAssuredLimit = (\"SumAssured Greater than Maximum Sum Assured for the Product.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO rule_table (RuleName, RuleDesc, Category, ClientId) VALUES ('AnnualIncomeLimit', 'function AnnualIncomeLimit(inoutMap) {annualIncome = (inoutMap.TransactionData.Insured.IncomeDetails.annualIncome); if (Number(annualIncome) < 10000){inoutMap.ValidationResults.AnnualIncomeLimit = (\"Annual Income lesser than limit.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO rule_table (RuleName, RuleDesc, Category, ClientId) VALUES ('IsNonMedicalCase', 'function IsNonMedicalCase(inoutMap) {nonMedicalCase = (inoutMap.TransactionData.Questionnaire.HealthDetails.isNonMedicalCase); if (nonMedicalCase == \"true\"){inoutMap.ValidationResults.IsNonMedicalCase = (\"Non Medical Case.\");} else {inoutMap.ValidationResults.IsNonMedicalCase = (\"Medical Case.\");} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

        } catch (SQLException e) {
            // System.out.println("=======================================> Error :" + e.getMessage());
        }

    }
    
    @After
    public void cleanup() {
        try {
            QueryRunner queryRunner = new QueryRunner(dataSource);
            queryRunner.update("Drop table IF EXISTS `rule_table`;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
