/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.agent.component.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cognizant.insurance.agent.audit.LifeEngageAudit;
import com.cognizant.insurance.agent.audit.LifeEngagePayloadAudit;
import com.cognizant.insurance.agent.component.AuditComponent;
import com.cognizant.insurance.agent.dao.AuditDao;
import com.cognizant.insurance.agent.dao.impl.AuditDaoImpl;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.response.impl.ResponseImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class class AuditComponentImpl.
 *
 * @author 300797
 */
@Component
public class AuditComponentImpl implements AuditComponent {

    /* (non-Javadoc)
     * @see com.cognizant.insurance.audit.component.AuditComponent#savelifeEngageAudit(com.cognizant.insurance.request.Request)
     */
    @Override
    public <T> Response<Object> save(Request<T> request) {
        final AuditDao auditDao = new AuditDaoImpl();
        auditDao.save(request);
        
        final Response<Object> response = new ResponseImpl<Object>();
        response.setType(request.getType());
        
        return response;
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.insurance.generic.component.AuditComponent#update(com.cognizant.insurance.request.Request)
     */
    @Override
    public <T> void update(Request<T> request) {
        final AuditDao auditDao = new AuditDaoImpl();
        auditDao.merge(request);
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.insurance.generic.component.AuditComponent#retrieveLifeEngagePayloadAudit(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<LifeEngagePayloadAudit>> retrieveLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request) {
        Response<List<LifeEngagePayloadAudit>> response = new ResponseImpl<List<LifeEngagePayloadAudit>>();
        final AuditDao auditDao = new AuditDaoImpl();
        response.setType(auditDao.retrieveLifeEngagePayloadAudit(request));

        return response;
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.insurance.generic.component.AuditComponent#retrieveLifeEngagePayloadAudit(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<LifeEngagePayloadAudit>> retrieveLMSPayloadAudit(Request<LifeEngagePayloadAudit> request) {
        Response<List<LifeEngagePayloadAudit>> response = new ResponseImpl<List<LifeEngagePayloadAudit>>();
        final AuditDao auditDao = new AuditDaoImpl();
        response.setType(auditDao.retrieveLMSPayloadAudit(request));

        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.component.AuditComponent#retrieveLastLifeEngagePayloadAudit(com.cognizant.insurance
     * .request.Request)
     */
    @Override
    public Response<LifeEngagePayloadAudit> retrieveLastLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request) {
        Response<LifeEngagePayloadAudit> response = new ResponseImpl<LifeEngagePayloadAudit>();
        final AuditDao auditDao = new AuditDaoImpl();
        response.setType(auditDao.retrieveLastLifeEngagePayloadAudit(request));

        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.component.AuditComponent#getLifeEngageAudit(com.cognizant.insurance.request.Request
     * )
     */
    @Override
    public Response<LifeEngageAudit> getLifeEngageAudit(Request<LifeEngageAudit> request) {
        Response<LifeEngageAudit> response = new ResponseImpl<LifeEngageAudit>();
        final AuditDao auditDao = new AuditDaoImpl();
        response.setType((LifeEngageAudit) auditDao.findById(request, request.getType().getId()));

        return response;
    }

}
