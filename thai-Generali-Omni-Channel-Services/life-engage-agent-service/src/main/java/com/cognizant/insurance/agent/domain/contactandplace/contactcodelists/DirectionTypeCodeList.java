/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.contactcodelists;

/**
 * A code list indicating the direction when an address consists of a
 * directional indicator.
 * 
 * @author 301350
 * 
 * 
 */
public enum DirectionTypeCodeList {
    /**
     * East.
     * 
     * 
     * 
     */
    E,
    /**
     * North.
     * 
     * 
     * 
     */
    N,
    /**
     * North East.
     * 
     * 
     * 
     */
    NE,
    /**
     * North West.
     * 
     * 
     * 
     */
    NW,
    /**
     * South.
     * 
     * 
     * 
     */
    S,
    /**
     * South East.
     * 
     * 
     * 
     */
    SE,
    /**
     * South West.
     * 
     * 
     * 
     */
    SW,
    /**
     * West.
     * 
     * 
     * 
     */
    W
}
