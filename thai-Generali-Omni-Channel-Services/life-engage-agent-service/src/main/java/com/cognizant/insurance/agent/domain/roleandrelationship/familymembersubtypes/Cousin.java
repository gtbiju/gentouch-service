/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.familymembersubtypes;

import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.FamilyMember;

/**
 * This concept represents a cousin.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Cousin extends FamilyMember {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2453974234398515959L;
}
