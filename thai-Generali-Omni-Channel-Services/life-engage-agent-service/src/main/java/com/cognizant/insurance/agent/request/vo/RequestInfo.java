/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.agent.request.vo;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class class RequestInfo.
 *
 * @author 300797
 * 
 * Class RequestInfo - Created for EApp. Used to map RequestInfo details.
 */
public class RequestInfo {
    
    /** The user name. */
    private String userName;
    
    /** The creation date time. */
    private Date creationDateTime;   
    
    /** The source info name. */
    private String sourceInfoName;
    
    /** The transaction id. */
    private String transactionId;
    
    /** The requestor token. */
    private String requestorToken;
    
    /** The lastSyncDate. */
    private Date lastSyncDate;
    
    /** The user email. */
    private String userEmail;
    
    /** The device uuid. */
    private String deviceUUID;
    
    /** The device first sync info. */
    private Boolean firstTimeSync;
    
    private Integer archivalPeriod;
  

	/**
     * Gets the user name.
     *
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }
    
    /**
     * Sets the user name.
     *
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    /**
     * Gets the creation date time.
     *
     * @return the creationDateTime
     */
    public Date getCreationDateTime() {
        return creationDateTime;
    }
    
    /**
     * Sets the creation date time.
     *
     * @param creationDateTime the creationDateTime to set
     */
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
    
    /**
     * Gets the source info name.
     *
     * @return the sourceInfoName
     */
    public String getSourceInfoName() {
        return sourceInfoName;
    }
    
    /**
     * Sets the source info name.
     *
     * @param sourceInfoName the sourceInfoName to set
     */
    public void setSourceInfoName(String sourceInfoName) {
        this.sourceInfoName = sourceInfoName;
    }
    
    /**
     * Gets the transaction id.
     *
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }
    
    /**
     * Sets the transaction id.
     *
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    
    /**
     * Gets the requestor token.
     *
     * @return the requestorToken
     */
    public String getRequestorToken() {
        return requestorToken;
    }
    
    /**
     * Sets the requestor token.
     *
     * @param requestorToken the requestorToken to set
     */
    public void setRequestorToken(String requestorToken) {
        this.requestorToken = requestorToken;
    }
    
    /**
     * Gets the LastSyncDate.
     *
     * @return the lastSyncDate
     */
    public Date getLastSyncDate() {
		return lastSyncDate;
	}

    /**
     * Sets the LastSyncDate .
     *
     * @param lastSyncDate the lastSyncDate to set
     */
	public void setLastSyncDate(Date lastSyncDate) {
		this.lastSyncDate = lastSyncDate;
	}

    /**
     * Gets the user email.
     *
     * @return the user email
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * Sets the user email.
     *
     * @param userEmail the user email to set.
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * Gets the device uuid.
     *
     * @return the device uuid
     */
    public String getDeviceUUID() {
        return deviceUUID;
    }

    /**
     * Sets the device uuid.
     *
     * @param deviceUUID the device uuid to set.
     */
    public void setDeviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
    }

	public Boolean getFirstTimeSync() {
		return firstTimeSync;
	}

	public void setFirstTimeSync(Boolean firstTimeSync) {
		this.firstTimeSync = firstTimeSync;
	}

	public Integer getArchivalPeriod() {
		return archivalPeriod;
	}

	public void setArchivalPeriod(Integer archivalPeriod) {
		this.archivalPeriod = archivalPeriod;
	}
	
}
