package com.cognizant.insurance.omnichannel.component;

import java.util.List;

import com.cognizant.insurance.agent.AgentAchievement;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliAgentAddress;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.generic.component.AgentComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.Agreement;

/**
 * The Interface AgentComponent.
 */
public interface GeneraliAgentComponent extends AgentComponent{
	
	/**
	 * Retrieve agent by code.
	 *
	 * @param request the agent request
	 * @return the response
	 */
	Response<GeneraliAgent> retrieveAgentByODSCode(Request<GeneraliAgent> request);
	
	List<Customer> retrieveLeads(Request<Customer> request);

    Response<List<ProductSpecification>> retrieveProductsByCode(Request<ProductSpecification> agentProductRequest);
    
    List<GeneraliAgent> retrieveAgentByODSForReassign(Request<GeneraliAgent> request);
    
    List<GeneraliAgent> retrieveActiveAgentByODSToReassign(Request<GeneraliAgent> request);
   
	Integer retrieveIllustrationCount(Request<Customer> agentRequest);

	Integer retrieveEappCount(Request<Customer> agentRequest);

	Integer retrieveFnaCount(Request<Customer> agentRequest);
	
	String retrieveLeadIdForFna(Request<Customer> agentRequest);
	
	List<GoalAndNeed> retrieveGoalAndNeed(Request<GoalAndNeed> agentRequest);
	
	List<Agreement> retrieveAgreement(Request<InsuranceAgreement> agentRequest);

	Response<List<AgentAchievement>> retrieveAchievements(Request<AgentAchievement> agentAchievementRequest);
	
	List<InsuranceAgreement> retrieveIllustration(Request<InsuranceAgreement> agentRequest);
   
	Response<GeneraliGAOAgent> retrieveGAOAgentByODSCode(Request<GeneraliGAOAgent> request);

	Response<GeneraliAgentAddress> retrieveGAOAddress(Request<GeneraliAgentAddress> agentRequest);
}
