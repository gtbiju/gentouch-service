/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Sep 20, 2017
 */

package com.cognizant.insurance.domain.product;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cognizant.insurance.domain.product.BroadLobType;
import com.cognizant.insurance.domain.product.Carrier;
import com.cognizant.insurance.domain.product.CategoryType;
import com.cognizant.insurance.domain.product.ComponentSubTypes;
import com.cognizant.insurance.domain.product.CoverType;
import com.cognizant.insurance.domain.product.InvestmentType;
import com.cognizant.insurance.domain.product.LobSubType;
import com.cognizant.insurance.domain.product.LobType;
import com.cognizant.insurance.domain.product.ProductGroupType;
import com.cognizant.insurance.domain.product.ProductType;

// TODO: Auto-generated Javadoc
/**
 * The Class MasterComponent.
 */
@Entity
@NamedQueries({
	 @NamedQuery(name = "MasterComponent.retrieveAllMasterComponents", query = "select mstComponent from MasterComponent mstComponent join fetch mstComponent.carrier carrier where (:carrierCode is null or carrier.id = :carrierCode)"),
	 @NamedQuery(name="MasterComponent.retrieveComponentById", query = "select mstComponent from MasterComponent mstComponent join fetch mstComponent.carrier carrier where mstComponent.id = :id and (:carrierCode is null or carrier.id = :carrierCode)" )
})

@Table(name = "MST_PRODUCT_COMPONENT")
public class MasterComponent {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	/** The name. */
	@Column(name="NAME")
	private String name;
	
	/** The short name. */
	@Column(name="SHORT_NAME")
	private String shortName;
	
	/** The product code. */
	@Column(name="PRODUCT_CODE")
	private String productCode;
	
	/** The local name. */
	@Column(name="LOCAL_NAME")
	private String localName;
	
	/** The broad lob code. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
	@JoinColumn(name="BROAD_LOB_CODE")
	private BroadLobType broadLOBCode;
	
	/** The lob code. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
	@JoinColumn(name="lineOfBusiness_ID")
	private LobType lobCode;
	
	/** The lob sub code. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
	@JoinColumn(name="LOB_SUB_CODE",insertable = false, updatable = false)
	private LobSubType lobSubCode;
	
	/** The marketable indicator. */
	@Column(name="MARKETABLE_INDICATOR")
	private Boolean marketableIndicator;
	
	/** The version. */
	private BigDecimal version;
	
	/** The launch date. */
	@Column(name="LAUNCH_DATE")
	private Date launchDate;
	
	/** The withdrawal date. */
	@Column(name="WITHDRAWAL_DATE")
	private Date withdrawalDate;
	
	/** The suspension date. */
	@Column(name="SUSPENSION_DATE")
	private Date suspensionDate;
	
	/** The termination date. */
	@Column(name="TERMINATION_DATE")
	private Date terminationDate;
	
	/** The business description. */
	@Column(name="BUSINESS_DESCRIPTION")
	private String businessDescription;
	
	/** The formal definition. */
	@Column(name="FORMAL_DEFINITION")
	private String formalDefinition;
	
	/** The investment type. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
	@JoinColumn(name="investmentType_ID")
	private InvestmentType investmentType;
	
	/** The product type. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
	@JoinColumn(name="productType_ID")
	private ProductType productType;
	
	/** The cover type. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
	@JoinColumn(name="coverType_ID")
	private CoverType coverType;
	
	/** The auto include at issue. */
	@Column(name="AUTO_INCLUDE_AT_ISSUE")
	private Boolean autoIncludeAtIssue;
	
	/** The product group. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
	@JoinColumn(name="PRODUCT_GROUP")
	private ProductGroupType productGroup;
	
	/** The version date. */
	@Column(name="version_date")
	private Date versionDate;
	
	/** The carrier. imp */
    @ManyToOne(cascade = { CascadeType.REFRESH })
    private Carrier carrier;
    
    /** The child components. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "parentComponent")
	private List<MasterComponent> childComponents;
    
    /** The parent component. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
    @JoinColumn(name = "parentProduct_id")
    private MasterComponent parentComponent;
    
    /** The properties. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL },fetch = FetchType.LAZY, mappedBy = "parentComponent")
    private Set<MasterAttribute> properties = new HashSet<MasterAttribute>();
	
	/** The created date. */
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	/** The category. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
	@JoinColumn(name="category_ID")
	private CategoryType category;
	
	/** The push sell indicator. */
	@Column(name="PUSH_SELL_INDICATOR")
	private boolean pushSellIndicator;
	
    /** The comp sub types. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
    @JoinColumn(name = "COMP_SUBTYPES")
    private ComponentSubTypes compSubTypes;
	
	/** The prod_id. */
	@Column(name="PRODUCT_ID")
	private int prod_id;
	
	/** The marketable name. */
	private String marketableName;
	
	/** The created by. */
    @Column(name= "CREATED_BY")
    private String createdBy;
    

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the short name.
	 *
	 * @return the short name
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * Sets the short name.
	 *
	 * @param shortName the new short name
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * Gets the local name.
	 *
	 * @return the local name
	 */
	public String getLocalName() {
		return localName;
	}

	/**
	 * Sets the local name.
	 *
	 * @param localName the new local name
	 */
	public void setLocalName(String localName) {
		this.localName = localName;
	}

	/**
	 * Gets the broad lob code.
	 *
	 * @return the broad lob code
	 */
	public BroadLobType getBroadLOBCode() {
		return broadLOBCode;
	}

	/**
	 * Sets the broad lob code.
	 *
	 * @param broadLOBCode the new broad lob code
	 */
	public void setBroadLOBCode(BroadLobType broadLOBCode) {
		this.broadLOBCode = broadLOBCode;
	}

	/**
	 * Gets the lob code.
	 *
	 * @return the lob code
	 */
	public LobType getLobCode() {
		return lobCode;
	}

	/**
	 * Sets the lob code.
	 *
	 * @param lobCode the new lob code
	 */
	public void setLobCode(LobType lobCode) {
		this.lobCode = lobCode;
	}

	/**
	 * Gets the lob sub code.
	 *
	 * @return the lob sub code
	 */
	public LobSubType getLobSubCode() {
		return lobSubCode;
	}

	/**
	 * Sets the lob sub code.
	 *
	 * @param lobSubCode the new lob sub code
	 */
	public void setLobSubCode(LobSubType lobSubCode) {
		this.lobSubCode = lobSubCode;
	}

	/**
	 * Gets the marketable indicator.
	 *
	 * @return the marketable indicator
	 */
	public Boolean getMarketableIndicator() {
		return marketableIndicator;
	}

	/**
	 * Sets the marketable indicator.
	 *
	 * @param marketableIndicator the new marketable indicator
	 */
	public void setMarketableIndicator(Boolean marketableIndicator) {
		this.marketableIndicator = marketableIndicator;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public BigDecimal getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(BigDecimal version) {
		this.version = version;
	}

	/**
	 * Gets the launch date.
	 *
	 * @return the launch date
	 */
	public Date getLaunchDate() {
		return launchDate;
	}

	/**
	 * Sets the launch date.
	 *
	 * @param launchDate the new launch date
	 */
	public void setLaunchDate(Date launchDate) {
		this.launchDate = launchDate;
	}

	/**
	 * Gets the withdrawal date.
	 *
	 * @return the withdrawal date
	 */
	public Date getWithdrawalDate() {
		return withdrawalDate;
	}

	/**
	 * Sets the withdrawal date.
	 *
	 * @param withdrawalDate the new withdrawal date
	 */
	public void setWithdrawalDate(Date withdrawalDate) {
		this.withdrawalDate = withdrawalDate;
	}

	/**
	 * Gets the suspension date.
	 *
	 * @return the suspension date
	 */
	public Date getSuspensionDate() {
		return suspensionDate;
	}

	/**
	 * Sets the suspension date.
	 *
	 * @param suspensionDate the new suspension date
	 */
	public void setSuspensionDate(Date suspensionDate) {
		this.suspensionDate = suspensionDate;
	}

	/**
	 * Gets the termination date.
	 *
	 * @return the termination date
	 */
	public Date getTerminationDate() {
		return terminationDate;
	}

	/**
	 * Sets the termination date.
	 *
	 * @param terminationDate the new termination date
	 */
	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	/**
	 * Gets the business description.
	 *
	 * @return the business description
	 */
	public String getBusinessDescription() {
		return businessDescription;
	}

	/**
	 * Sets the business description.
	 *
	 * @param businessDescription the new business description
	 */
	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}

	/**
	 * Gets the formal definition.
	 *
	 * @return the formal definition
	 */
	public String getFormalDefinition() {
		return formalDefinition;
	}

	/**
	 * Sets the formal definition.
	 *
	 * @param formalDefinition the new formal definition
	 */
	public void setFormalDefinition(String formalDefinition) {
		this.formalDefinition = formalDefinition;
	}

	/**
	 * Gets the investment type.
	 *
	 * @return the investment type
	 */
	public InvestmentType getInvestmentType() {
		return investmentType;
	}

	/**
	 * Sets the investment type.
	 *
	 * @param investmentType the new investment type
	 */
	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}

	/**
	 * Gets the product type.
	 *
	 * @return the product type
	 */
	public ProductType getProductType() {
		return productType;
	}

	/**
	 * Sets the product type.
	 *
	 * @param productType the new product type
	 */
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	/**
	 * Gets the cover type.
	 *
	 * @return the cover type
	 */
	public CoverType getCoverType() {
		return coverType;
	}

	/**
	 * Sets the cover type.
	 *
	 * @param coverType the new cover type
	 */
	public void setCoverType(CoverType coverType) {
		this.coverType = coverType;
	}

	/**
	 * Gets the auto include at issue.
	 *
	 * @return the auto include at issue
	 */
	public Boolean getAutoIncludeAtIssue() {
		return autoIncludeAtIssue;
	}

	/**
	 * Sets the auto include at issue.
	 *
	 * @param autoIncludeAtIssue the new auto include at issue
	 */
	public void setAutoIncludeAtIssue(Boolean autoIncludeAtIssue) {
		this.autoIncludeAtIssue = autoIncludeAtIssue;
	}

	/**
	 * Gets the product group.
	 *
	 * @return the product group
	 */
	public ProductGroupType getProductGroup() {
		return productGroup;
	}

	/**
	 * Sets the product group.
	 *
	 * @param productGroup the new product group
	 */
	public void setProductGroup(ProductGroupType productGroup) {
		this.productGroup = productGroup;
	}

	/**
	 * Gets the version date.
	 *
	 * @return the version date
	 */
	public Date getVersionDate() {
		return versionDate;
	}

	/**
	 * Sets the version date.
	 *
	 * @param versionDate the new version date
	 */
	public void setVersionDate(Date versionDate) {
		this.versionDate = versionDate;
	}

	/**
	 * Gets the carrier.
	 *
	 * @return the carrier
	 */
	public Carrier getCarrier() {
		return carrier;
	}

	/**
	 * Sets the carrier.
	 *
	 * @param carrier the carrier to set
	 */
	public void setCarrier(Carrier carrier) {
		this.carrier = carrier;
	}

	/**
	 * Gets the parent component.
	 *
	 * @return the parent component
	 */
	public MasterComponent getParentComponent() {
		return parentComponent;
	}

	/**
	 * Sets the parent component.
	 *
	 * @param parentComponent the new parent component
	 */
	public void setParentComponent(MasterComponent parentComponent) {
		this.parentComponent = parentComponent;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate the new created date
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Gets the category.
	 *
	 * @return the category
	 */
	public CategoryType getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 *
	 * @param category the new category
	 */
	public void setCategory(CategoryType category) {
		this.category = category;
	}

	/**
	 * Checks if is push sell indicator.
	 *
	 * @return true, if is push sell indicator
	 */
	public boolean isPushSellIndicator() {
		return pushSellIndicator;
	}

	/**
	 * Sets the push sell indicator.
	 *
	 * @param pushSellIndicator the new push sell indicator
	 */
	public void setPushSellIndicator(boolean pushSellIndicator) {
		this.pushSellIndicator = pushSellIndicator;
	}

	/**
	 * Gets the prod_id.
	 *
	 * @return the prod_id
	 */
	public int getProd_id() {
		return prod_id;
	}

	/**
	 * Sets the prod_id.
	 *
	 * @param prod_id the new prod_id
	 */
	public void setProd_id(int prod_id) {
		this.prod_id = prod_id;
	}

	/**
	 * Gets the child components.
	 *
	 * @return the child components
	 */
	public List<MasterComponent> getChildComponents() {
		return childComponents;
	}

	/**
	 * Sets the child components.
	 *
	 * @param childComponents the new child components
	 */
	public void setChildComponents(List<MasterComponent> childComponents) {
		this.childComponents = childComponents;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public Set<MasterAttribute> getProperties() {
		return properties;
	}

	/**
	 * Sets the properties.
	 *
	 * @param properties the new properties
	 */
	public void setProperties(Set<MasterAttribute> properties) {
		this.properties = properties;
	}

	/**
	 * Gets the comp sub types.
	 *
	 * @return the comp sub types
	 */
	public ComponentSubTypes getCompSubTypes() {
		return compSubTypes;
	}

	/**
	 * Sets the comp sub types.
	 *
	 * @param compSubTypes the new comp sub types
	 */
	public void setCompSubTypes(ComponentSubTypes compSubTypes) {
		this.compSubTypes = compSubTypes;
	}

	/**
	 * Gets the marketable name.
	 *
	 * @return the marketable name
	 */
	public String getMarketableName() {
		return marketableName;
	}

	/**
	 * Sets the marketable name.
	 *
	 * @param marketableName the new marketable name
	 */
	public void setMarketableName(String marketableName) {
		this.marketableName = marketableName;
	}

	/**
	 * Gets the created by.
	 *
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the created by.
	 *
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
}