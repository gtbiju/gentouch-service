/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Common Options.
 * 
 * @author 301350
 * 
 * 
 */
public enum CommonOptionsCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Included,
    /**
     * 
     * 
     * 
     * 
     */
    Suspended,
    /**
     * 
     * 
     * 
     * 
     */
    Excluded,
    /**
     * 
     * 
     * 
     * 
     */
    Reinstated,
    /**
     * 
     * 
     * 
     * 
     */
    Rejected
}
