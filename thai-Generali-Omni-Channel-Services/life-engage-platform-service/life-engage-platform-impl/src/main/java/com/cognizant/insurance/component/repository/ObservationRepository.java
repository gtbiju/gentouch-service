/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.component.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.generic.component.ObservationComponent;
import com.cognizant.insurance.observation.Observation;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.ObservationInfo;

/**
 * The Class class ObservationRepository.
 *
 * @author 300797
 */
//@Repository
public class ObservationRepository {
    /** The entity manager. */
    @PersistenceContext(unitName="LE_Platform")
    private EntityManager entityManager;
    
    /** The Observation component. */
    @Autowired
    private ObservationComponent observationComponent;


    /**
     * Save observations.
     *
     * @param observationInfo the observation info
     */
    public final void saveObservations(final Observation observation) {
        
        
        final Request<Observation> request =
            new JPARequestImpl<Observation>(entityManager,  observation.getContextId(), null);
        request.setType(observation);
        
        observationComponent.saveObservations(request);
    }


    /**
     * @return the entityManager
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }


    /**
     * @param entityManager the entityManager to set
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    /**
     * @return the observationComponent
     */
    public final ObservationComponent getObservationComponent() {
        return observationComponent;
    }


    /**
     * @param observationComponent the observationComponent to set
     */
    public final void setObservationComponent(final ObservationComponent observationComponent) {
        this.observationComponent = observationComponent;
    }
 }
