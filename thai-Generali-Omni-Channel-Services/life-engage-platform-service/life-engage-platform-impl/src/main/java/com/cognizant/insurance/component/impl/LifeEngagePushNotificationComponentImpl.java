/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 19, 2015
 */
package com.cognizant.insurance.component.impl;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngagePushNotificationComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.PushNotificationRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;

/**
 * The Class class LifeEngagePushNotificationComponentImpl.
 */
@Service("lifeEngagePushNotificationComponent")
public class LifeEngagePushNotificationComponentImpl implements LifeEngagePushNotificationComponent {

    /** The register devic holder. */
    @Autowired
    @Qualifier("registerDevicemapping")
    private LifeEngageSmooksHolder registerDevicHolder;

    /** The un register devic holder. */
    @Autowired
    @Qualifier("unRegisterDevicemapping")
    private LifeEngageSmooksHolder unRegisterDevicHolder;

    /** The push notification holder. */
    @Autowired
    @Qualifier("pushNotificationmapping")
    private LifeEngageSmooksHolder pushNotificationHolder;

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngagePushNotificationComponentImpl.class);

    /** The push notification repository. */
    @Autowired
    private PushNotificationRepository pushNotificationRepository;

    /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngagePushNotificationComponent#registerDevice(com.cognizant.insurance.
     * request.vo.RequestInfo, org.json.JSONObject)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String
            registerDevice(final RequestInfo requestInfo, final String json, final LifeEngageAudit lifeEngageAudit) {
        Transactions transactions = new Transactions();
        String response;
        try {
            transactions = (Transactions) registerDevicHolder.parseJson(json);
            pushNotificationRepository.registerDevice(requestInfo, transactions);

            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
            statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE_PUSH_NOTIFICATION);
            statusData.setStatusMessage(LifeEngageComponentHelper.REGISTRATION_SUCCESS);
            transactions.setStatusData(statusData);

            auditRepository.savelifeEngagePayloadAuditWithoutKeys(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit);
            response = registerDevicHolder.parseBO(transactions);
        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.REGISTRATION_FAILED, LifeEngageComponentHelper.FAILURE_CODE);

            response = registerDevicHolder.parseBO(transactions);
        } catch (ParseException e) {
            LOGGER.error("ParseException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.REGISTRATION_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
            response = registerDevicHolder.parseBO(transactions);
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.REGISTRATION_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
            response = registerDevicHolder.parseBO(transactions);

        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngagePushNotificationComponent#unRegisterDevice(com.cognizant.insurance
     * .request.vo.RequestInfo, org.json.JSONObject)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String unRegisterDevice(final RequestInfo requestInfo, final String json,
            final LifeEngageAudit lifeEngageAudit) {
        Transactions transactions = new Transactions();
        String response;
        try {
            transactions = (Transactions) unRegisterDevicHolder.parseJson(json);
            pushNotificationRepository.unRegisterDevice(requestInfo, transactions);

            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
            statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE_PUSH_NOTIFICATION);
            statusData.setStatusMessage(LifeEngageComponentHelper.UNREGISTER_SUCCESS);
            transactions.setStatusData(statusData);
            auditRepository.savelifeEngagePayloadAuditWithoutKeys(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit);

            response = unRegisterDevicHolder.parseBO(transactions);

        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.UNREGISTER_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
            response = unRegisterDevicHolder.parseBO(transactions);
        } catch (ParseException e) {
            LOGGER.error("ParseException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.UNREGISTER_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
            response = unRegisterDevicHolder.parseBO(transactions);
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.UNREGISTER_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
            response = unRegisterDevicHolder.parseBO(transactions);

        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngagePushNotificationComponent#pushNotifications(com.cognizant.insurance
     * .request.vo.RequestInfo, org.json.JSONObject)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String pushNotifications(final RequestInfo requestInfo, final String json,
            final LifeEngageAudit lifeEngageAudit) {
        Transactions transactions = new Transactions();
        String response;
        try {
            transactions = (Transactions) pushNotificationHolder.parseJson(json);
            boolean isSuccess = pushNotificationRepository.pushNotifications(requestInfo, transactions, json);
            
           if(isSuccess){
               final StatusData  statusData = new StatusData();
                statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
                statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE_PUSH_NOTIFICATION);
                statusData.setStatusMessage(LifeEngageComponentHelper.NOTIFICATION_SUCCESS);
                transactions.setStatusData(statusData);
           } else {
               final StatusData  statusData = new StatusData();
               statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
               statusData.setStatusCode(LifeEngageComponentHelper.NOTIFICATION_PARTIAL_SUCCES_CODE);
               statusData.setStatusMessage(LifeEngageComponentHelper.NOTIFICATION_PARTIAL_SUCCESS);
               transactions.setStatusData(statusData);
           }

            auditRepository.savelifeEngagePayloadAuditWithoutKeys(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit);
            response = pushNotificationHolder.parseBO(transactions);

        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            final StatusData statusData = new StatusData();
            statusData.setStatus(Constants.FAILURE);
            statusData.setStatusCode(e.getErrorCode());
            statusData.setStatusMessage(e.getMessage());
            transactions.setStatusData(statusData);
            response = pushNotificationHolder.parseBO(transactions);
        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.NOTIFICATION_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
            response = pushNotificationHolder.parseBO(transactions);
        } catch (ParseException e) {
            LOGGER.error("ParseException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.NOTIFICATION_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
            response = pushNotificationHolder.parseBO(transactions);
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);

            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.NOTIFICATION_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
            response = pushNotificationHolder.parseBO(transactions);

        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE,
                    LifeEngageComponentHelper.NOTIFICATION_FAILED, LifeEngageComponentHelper.FAILURE_CODE);
            response = pushNotificationHolder.parseBO(transactions);

        }
        return response;
    }

    /**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }
}
