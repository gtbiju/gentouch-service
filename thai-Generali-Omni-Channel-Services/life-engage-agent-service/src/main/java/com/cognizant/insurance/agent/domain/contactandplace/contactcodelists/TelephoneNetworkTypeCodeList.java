/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.contactcodelists;

/**
 * A code list identifying various types of telephone network contacts.
 * 
 * @author 301350
 * 
 * 
 */
public enum TelephoneNetworkTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Cell,
    /**
     * 
     * 
     * 
     * 
     */
    Landline,
    /**
     * Voice over internet protocol.
     * 
     * 
     * 
     */
    VoIP
}
