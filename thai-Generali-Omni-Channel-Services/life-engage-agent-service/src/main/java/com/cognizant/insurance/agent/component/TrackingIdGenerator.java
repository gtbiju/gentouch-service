package com.cognizant.insurance.agent.component;

public interface TrackingIdGenerator {
	
	public Long generate(String context);
	public Long generate();
	

}
