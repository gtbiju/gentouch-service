/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.dao.impl;

import java.util.List;

import com.cognizant.insurance.audit.LifeEngagePayloadAudit;
import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.generic.dao.AuditDao;
import com.cognizant.insurance.request.Request;

/**
 * @author 300797
 * 
 */
public class AuditDaoImpl extends DaoImpl implements AuditDao {

    /* (non-Javadoc)
     * @see com.cognizant.insurance.generic.dao.AuditDao#retrieveLifeEngagePayloadAudit(com.cognizant.insurance.request.Request)
     */
    @Override
    public List<LifeEngagePayloadAudit>
            retrieveLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request) {
        LifeEngagePayloadAudit engagePayloadAudit = request.getType();
        String query =
                "select payAudit from LifeEngagePayloadAudit payAudit where payAudit.key11 =?1 "
                        + " and payAudit.key5 =?2  and payAudit.transTrackingID =?3 and payAudit.type =?4 and payAudit.requestHashCode =?5";
        List<LifeEngagePayloadAudit> lifeEngagePayloadAuditList =
                findByQuery(request, query, engagePayloadAudit.getKey11(),
                        engagePayloadAudit.getKey5(),
                        engagePayloadAudit.getTransTrackingID(), engagePayloadAudit.getType(), engagePayloadAudit.getRequestHashCode());
       return lifeEngagePayloadAuditList;
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.insurance.generic.dao.AuditDao#readLifeEngagePayloadAudit(com.cognizant.insurance.request.Request)
     */
    @Override
    public List<LifeEngagePayloadAudit>
            readLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request) {
        LifeEngagePayloadAudit engagePayloadAudit = request.getType();
        String query =
                "select payAudit from LifeEngagePayloadAudit payAudit where payAudit.key11 =?1 "
                        + " and payAudit.key5 =?2  and payAudit.transTrackingID =?3 and payAudit.type =?4 and payAudit.key15 =?5";
        List<LifeEngagePayloadAudit> lifeEngagePayloadAuditList =
                findByQuery(request, query, engagePayloadAudit.getKey11(),
                        engagePayloadAudit.getKey5(),
                        engagePayloadAudit.getTransTrackingID(), engagePayloadAudit.getType(), engagePayloadAudit.getKey15());
       return lifeEngagePayloadAuditList;
    }
	
    /* (non-Javadoc)
     * @see com.cognizant.insurance.generic.dao.AuditDao#retrieveLMSPayloadAudit(com.cognizant.insurance.request.Request)
     */
    @Override
    public List<LifeEngagePayloadAudit>
            retrieveLMSPayloadAudit(Request<LifeEngagePayloadAudit> request) {
        LifeEngagePayloadAudit engagePayloadAudit = request.getType();
        String query =
                "select payAudit from LifeEngagePayloadAudit payAudit where payAudit.key2 =?1 "
                        + " and payAudit.key4 =?2 and payAudit.transTrackingID =?3 and payAudit.type =?4 and payAudit.requestHashCode =?5";
        List<LifeEngagePayloadAudit> lifeEngagePayloadAuditList =
                findByQuery(request, query, engagePayloadAudit.getKey2(),
                        engagePayloadAudit.getKey4(),
                        engagePayloadAudit.getTransTrackingID(), engagePayloadAudit.getType(), engagePayloadAudit.getRequestHashCode());
       return lifeEngagePayloadAuditList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.dao.AuditDao#retrieveLastLifeEngagePayloadAudit(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public LifeEngagePayloadAudit retrieveLastLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request) {
        LifeEngagePayloadAudit engagePayloadAudit = request.getType();
        LifeEngagePayloadAudit lifeEngagePayloadAudit = null;
        String query =
                "select payAudit from LifeEngagePayloadAudit payAudit where payAudit.key6 =?1 ORDER BY payAudit.id desc";
        List<LifeEngagePayloadAudit> lifeEngagePayloadAuditList =
                findByQuery(request, query, engagePayloadAudit.getKey6());
        if (lifeEngagePayloadAuditList.size() > 0) {
            lifeEngagePayloadAudit = lifeEngagePayloadAuditList.get(0);
        }
        return lifeEngagePayloadAudit;
    }
}
