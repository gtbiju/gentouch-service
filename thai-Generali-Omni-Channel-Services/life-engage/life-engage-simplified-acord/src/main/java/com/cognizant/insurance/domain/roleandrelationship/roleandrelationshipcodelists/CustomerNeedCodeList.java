package com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists;

public enum CustomerNeedCodeList {
	
	Education,
	
	Retirement,
	
	Investment,
	
	Health,
	
	Other,
	
	NotMentioned
}
