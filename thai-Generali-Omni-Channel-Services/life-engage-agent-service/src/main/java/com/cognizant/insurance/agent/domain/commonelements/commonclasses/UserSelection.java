/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 331701
 * @version       : 0.1, Feb 25, 2013
 */
package com.cognizant.insurance.agent.domain.commonelements.commonclasses;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.QuestionnaireTypeCodeList;

/**
 * The Class UserSelection.
 * Newly added for EApp - 301350, 01Aug2013. Not part of ACORD. 
 */

@Entity
public class UserSelection /*extends InformationModelObject*/ {
   
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8254159605449610889L;
    
    @Id
    private Long id;
    
    /** The question id. */
    private Long questionId;

    /** The user selected option. */
    private Boolean selectedOption;
    
    /** The user selected option. */
    private String selectedOptionExt;
    
    /** The detailed info. */
    private String detailedInfo;
    
    /** The quantity per day. */
    private Integer quantityPerDay;
    
    /** The unit. */
    private Integer unit;
    
    /** The number of years. */
    private Integer numberOfYears;

    /** The Questionnaire type. */
    @Enumerated
    private QuestionnaireTypeCodeList questionnaireType; 
    
    /**
     * Extensible Class - To capture user selected options  
     * For Generali - SelectedOption class extended to GLISelectedOption
     */
    @OneToMany(cascade=CascadeType.ALL)
    private Set<GLISelectedOption> selectedOptions;
    
    /**
     * parent question Id if any, In case of any reflexive questions.
     */
    private String parentQuestionId;
    
    /**
     * type of answer, like Single selection / Multiple selection 
     */
    private String answerType;
    
    /**
     * Question level, like 1,2,3...
     */
    
    private String level;
    
    /** The transaction Tracking Id used to identify individual eAppdata. */
    private String transTrackingId;
    
    private Long orderId;
    
    private String detailsQuestionInfo;
    
    public Boolean getSelectedOption() {
        return selectedOption;
    }


    public void setSelectedOption(Boolean selectedOption) {
        this.selectedOption = selectedOption;
    }


    public UserSelection() {
        this.questionId = 0l;
    }
    
    
   /* @OneToMany(cascade=CascadeType.ALL)
    private Set<Options> options;*/
   
    /**
     * Gets the questionId.
     *
     * @return Returns the questionId.
     */
    public final Long getQuestionId() {
        return questionId;
    }

   

    /**
     * Gets the detailedInfo.
     *
     * @return Returns the detailedInfo.
     */
    public final String getDetailedInfo() {
        return detailedInfo;
    }

    /**
     * Gets the quantityPerDay.
     *
     * @return Returns the quantityPerDay.
     */
    public final Integer getQuantityPerDay() {
        return quantityPerDay;
    }

    /**
     * Gets the unit.
     *
     * @return Returns the unit.
     */
    public final Integer getUnit() {
        return unit;
    }

    /**
     * Gets the numberOfYears.
     *
     * @return Returns the numberOfYears.
     */
    public final Integer getNumberOfYears() {
        return numberOfYears;
    }

    /**
     * Sets The questionId.
     *
     * @param questionId The questionId to set.
     */
    public final void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }


    /**
     * Sets The detailedInfo.
     *
     * @param detailedInfo The detailedInfo to set.
     */
    public final void setDetailedInfo(String detailedInfo) {
        this.detailedInfo = detailedInfo;
    }

    /**
     * Sets The quantityPerDay.
     *
     * @param quantityPerDay The quantityPerDay to set.
     */
    public final void setQuantityPerDay(Integer quantityPerDay) {
        this.quantityPerDay = quantityPerDay;
    }

    /**
     * Sets The unit.
     *
     * @param unit The unit to set.
     */
    public final void setUnit(Integer unit) {
        this.unit = unit;
    }

    /**
     * Sets The numberOfYears.
     *
     * @param numberOfYears The numberOfYears to set.
     */
    public final void setNumberOfYears(Integer numberOfYears) {
        this.numberOfYears = numberOfYears;
    }

    /**
     * Gets the questionnaireType.
     *
     * @return Returns the questionnaireType.
     */
    public final QuestionnaireTypeCodeList getQuestionnaireType() {
        return questionnaireType;
    }

    /**
     * Sets The questionnaireType.
     *
     * @param questionnaireType The questionnaireType to set.
     */
    public final void setQuestionnaireType(
            QuestionnaireTypeCodeList questionnaireType) {
        this.questionnaireType = questionnaireType;
    }

	public String getParentQuestionId() {
		return parentQuestionId;
	}

	public void setParentQuestionId(String parentQuestionId) {
		this.parentQuestionId = parentQuestionId;
	}

	public String getAnswerType() {
		return answerType;
	}

	public void setAnswerType(String answerType) {
		this.answerType = answerType;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

    public Set<GLISelectedOption> getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(Set<GLISelectedOption> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

	public String getSelectedOptionExt() {
		return selectedOptionExt;
	}

	public void setSelectedOptionExt(String selectedOptionExt) {
		this.selectedOptionExt = selectedOptionExt;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTransTrackingId() {
		return transTrackingId;
	}


	public void setTransTrackingId(String transTrackingId) {
		this.transTrackingId = transTrackingId;
	}


	public Long getOrderId() {
		return orderId;
	}


	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	
	public String getDetailsQuestionInfo() {
		return detailsQuestionInfo;
	}

	public void setDetailsQuestionInfo(String detailsQuestionInfo) {
		this.detailsQuestionInfo = detailsQuestionInfo;
	}
    
    

  
  
}
