package com.cognizant.insurance.omnichannel.utils;

public class AdditionalInsuredDataGli {

    /** The Insured Name. */
    private String name;

    /** The age */
    private String age;

    /** The sex */
    private String gender;

    /** The insured relationship. */
    private String dob;

    /** The idNumber */
    private String occupationClass;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the age
     */
    public String getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(String age) {
        this.age = age;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the occupationClass
     */
    public String getOccupationClass() {
        return occupationClass;
    }

    /**
     * @param occupationClass the occupationClass to set
     */
    public void setOccupationClass(String occupationClass) {
        this.occupationClass = occupationClass;
    }

}
