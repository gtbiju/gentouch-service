/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */
package com.cognizant.insurance.agreement.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;

import com.cognizant.insurance.agreement.dao.AgreementDao;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.finance.agreementloan.AgreementLoan;
import com.cognizant.insurance.domain.finance.agreementloan.AgreementWithdrawal;
import com.cognizant.insurance.domain.goalandneed.Agreement_GoalAndNeed;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

/**
 * The Class class AgreementDaoImpl.
 */
public class AgreementDaoImpl extends DaoImpl implements AgreementDao {

    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;

    
    private static final String DELETEDSTATUS = "Deleted";

    private static final String SAVEDSTATUS = "Saved";
    
    /**
     * Find using query.
     * 
     * @param request
     *            the request
     * @param queryString
     *            the query string
     * @param params
     *            the params
     * @return the list
     */
    protected List<Object> findUsingQuery(final Request request, final String queryString, Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<Object>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /**
     * Find using query with limit.
     * 
     * @param request
     *            the request
     * @param queryString
     *            the query string
     * @param limit
     *            the limit
     * @param params
     *            the params
     * @return the list
     */
    protected List<Object> findUsingQueryWithLimit(final Request request, final String queryString, Integer limit,
            Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query =
                    jpaRequestImpl.getEntityManager().createQuery(queryString).setMaxResults(limit.intValue());
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<Object>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#getAgreementLoans(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Set<AgreementLoan>> getAgreementLoans(final Request<Agreement> agreementRequest) {
        final Response<Set<AgreementLoan>> response = new ResponseImpl<Set<AgreementLoan>>();
        String query;
        if (agreementRequest.getType() != null) {

            query =
                    "select agreementLoan from AgreementLoan agreementLoan"
                            + " where agreementLoan.relatedAgreement.id = ?1";
            final List<Object> agreementLoanList =
                    findUsingQuery(agreementRequest, query, agreementRequest.getType().getId());
            if (CollectionUtils.isNotEmpty(agreementLoanList)) {
                final Set<AgreementLoan> agreementLoans = new HashSet<AgreementLoan>();
                for (Object agreementLoan : agreementLoanList) {
                    agreementLoans.add((AgreementLoan) agreementLoan);
                }
                response.setType(agreementLoans);
            }
        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.agreement.dao.AgreementDao#getAgreements(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public List<Agreement> getAgreements(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest, Request<Integer> limitRequest) {
        String query;
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
        query =
                "select insAgr from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                        + "and insAgr.statusCode in ?2 and insAgr.modifiedDateTime>=?3 and insAgr.contextId =?4 "
                        + "order by insAgr.modifiedDateTime asc";
        List<Object> insuranceAgreementIdList;
        if (limitRequest != null && limitRequest.getType() != null && limitRequest.getType() > 0) {
            insuranceAgreementIdList =
                    findUsingQueryWithLimit(agreementRequest, query, limitRequest.getType(),
                            insuranceAgreement.getAgentId(), statusRequest.getType(),
                            insuranceAgreement.getLastSyncDate(), agreementRequest.getContextId());
        } else {
            insuranceAgreementIdList =
                    findUsingQuery(agreementRequest, query, insuranceAgreement.getAgentId(), statusRequest.getType(),
                            insuranceAgreement.getLastSyncDate(), agreementRequest.getContextId());
        }
        final List<Agreement> agreements = new ArrayList<Agreement>();
        if (CollectionUtils.isNotEmpty(insuranceAgreementIdList)) {
            for (Object agreement : insuranceAgreementIdList) {
                agreements.add((InsuranceAgreement) agreement);
            }
        }
        return agreements;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.agreement.dao.AgreementDao#getAgreementWithdrawals
     * (com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Set<AgreementWithdrawal>> getAgreementWithdrawals(final Request<Agreement> agreementRequest) {
        final Response<Set<AgreementWithdrawal>> response = new ResponseImpl<Set<AgreementWithdrawal>>();
        String query;
        if (agreementRequest.getType() != null) {
            query =
                    "select agreementwithdrawal from AgreementWithdrawal agreementwithdrawal where "
                            + "agreementwithdrawal.relatedAgreement.id = ?1";
            final List<Object> agreementWithdrawalList =
                    findUsingQuery(agreementRequest, query, agreementRequest.getType().getId());
            if (CollectionUtils.isNotEmpty(agreementWithdrawalList)) {
                final Set<AgreementWithdrawal> agreementWithdrawals = new HashSet<AgreementWithdrawal>();
                for (Object agreementWithdrawal : agreementWithdrawalList) {
                    agreementWithdrawals.add((AgreementWithdrawal) agreementWithdrawal);
                }
                response.setType(agreementWithdrawals);
            }
        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#getInsuranceAgreement(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Agreement> getInsuranceAgreement(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<Agreement> agreementResponse = new ResponseImpl<Agreement>();
        String query;
        if (agreementRequest.getType() != null && statusRequest.getType() != null) {
            final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
            query =
                    "select insAgr from InsuranceAgreement insAgr where insAgr.identifier =?1 and "
                            + "insAgr.statusCode in ?2 and insAgr.contextId = ?3 order by id desc";
            final List<Object> insuranceAgreementList =
                    findUsingQuery(agreementRequest, query, insuranceAgreement.getIdentifier(),
                            statusRequest.getType(), agreementRequest.getContextId());
            if (CollectionUtils.isEmpty(insuranceAgreementList)) {
                throw new DaoException("No agreement with identifier : " + insuranceAgreement.getIdentifier());
            }
            agreementResponse.setType((InsuranceAgreement) insuranceAgreementList.get(0));
        }

        return agreementResponse;
    }
    
    
    @Override
    public Response<Agreement> getInsuranceAgreementPending(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<Agreement> agreementResponse = new ResponseImpl<Agreement>();
        String query;
        if (agreementRequest.getType() != null && statusRequest.getType() != null) {
            final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
            query =
                    "select insAgr from InsuranceAgreement insAgr where insAgr.identifier =?1 and "
                            + "insAgr.statusCode in ?2 and insAgr.contextId = ?3 order by id desc";
            final List<Object> insuranceAgreementList =
                    findUsingQuery(agreementRequest, query, insuranceAgreement.getIdentifier(),
                            statusRequest.getType(), agreementRequest.getContextId());
            if (CollectionUtils.isNotEmpty(insuranceAgreementList) && insuranceAgreementList.get(0) != null ) {
                agreementResponse.setType((InsuranceAgreement) insuranceAgreementList.get(0));
            }
        }
        return agreementResponse;
    }
    

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#getPreviousAgreements(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Set<InsuranceAgreement>> getPreviousAgreements(
            final Request<PartyRoleInAgreement> partyRoleInAgreementRequest) {
        final Response<Set<InsuranceAgreement>> previousAgreementResponse = new ResponseImpl<Set<InsuranceAgreement>>();
        String query;
        if (partyRoleInAgreementRequest.getType() != null
                && partyRoleInAgreementRequest.getType().getPlayerParty() != null) {
            query =
                    "select insAgr from InsuranceAgreement insAgr,Insured ins where ins.playerParty.id =?1 and "
                            + "insAgr.id=ins.isPartyRoleIn.id and insAgr.previousPolicyIndicator=?2";
            final List<Object> prevAgreementList =
                    findUsingQuery(partyRoleInAgreementRequest, query, partyRoleInAgreementRequest.getType()
                        .getPlayerParty()
                        .getId(), Boolean.TRUE);
            if (CollectionUtils.isNotEmpty(prevAgreementList)) {
                final Set<InsuranceAgreement> prevAgreements = new HashSet<InsuranceAgreement>();
                for (Object object : prevAgreementList) {
                    prevAgreements.add((InsuranceAgreement) object);
                }
                previousAgreementResponse.setType(prevAgreements);
            }
        }
        return previousAgreementResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#retrieveAgreementForIdentifier(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public List<Agreement> retrieveAgreementForIdentifier(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        String query;
        query = "select ins from InsuranceAgreement ins where ins.identifier =?1 and statusCode in ?2 order by id desc";
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
        List<Agreement> agrs =
                (List<Agreement>) findByQuery(agreementRequest, query, insuranceAgreement.getIdentifier(),
                        statusRequest.getType());
        if (CollectionUtils.isEmpty(agrs)) {
            throw new DaoException("No agreement with identifier : " + insuranceAgreement.getIdentifier());
        }
        return agrs;
    }
	
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#getAgreementDocumentDetails(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public List<Agreement> retrieveLatestAgreement(final Request<Agreement> agreementRequest
            ) {
        String query;
        query =
            "select agrmnt from Agreement agrmnt where agrmnt.transTrackingId =?1 and agrmnt.statusCode in ('12') and "
                    + "agrmnt.transactionId =?2 order by id desc";
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
        List<Agreement> agrs =
            (List<Agreement>) findByQuery(agreementRequest, query, insuranceAgreement.getTransTrackingId(),
            		 insuranceAgreement.getTransactionId());
        return agrs;
    }
	
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#retrieveAgreementForTransTrackingId(com.cognizant.insurance
     * .request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public List<Agreement> retrieveAgreementForTransTrackingId(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        String query;
        query =
            "select ins from InsuranceAgreement ins where ins.transTrackingId =?1 and statusCode in ?2 and "
                    + "ins.agentId =?3 order by id desc";
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
        List<Agreement> agrs =
            (List<Agreement>) findByQuery(agreementRequest, query, insuranceAgreement.getTransTrackingId(),
                    statusRequest.getType(), insuranceAgreement.getAgentId());
   /* if (CollectionUtils.isEmpty(agrs)) {
       // throw new DaoException("No agreement with identifier : " + insuranceAgreement.getTransTrackingId());
        return null;
    }*/
        return agrs;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#getAgreementDocumentDetails(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<AgreementDocument> getAgreementDocumentDetails(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<AgreementDocument> response = new ResponseImpl<AgreementDocument>();
        String query =
                "select agreement " + "from AgreementDocument agreementDocument,InsuranceAgreement agreement "
                        + "left join fetch agreement.relatedDocument relatedDocument "
                        + "where agreementDocument.name = relatedDocument.name and "
                        + "agreement.identifier =?1 and agreement.statusCode in ?2 and " + "agreementDocument.name=?3";
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
        if (insuranceAgreement.getIdentifier() == null || statusRequest.getType().size() == 0
                || insuranceAgreement.getRelatedDocument() == null) {
            return response;
        }
        for (AgreementDocument agreementDocument : insuranceAgreement.getRelatedDocument()) {
            List<Agreement> agreementList =
                    findByQuery(agreementRequest, query, insuranceAgreement.getIdentifier(), statusRequest.getType(),
                            agreementDocument.getName());
            if (CollectionUtils.isNotEmpty(agreementList) && agreementList.get(0) != null
                    && CollectionUtils.isNotEmpty(agreementList.get(0).getRelatedDocument())) {
                for (AgreementDocument document : agreementList.get(0).getRelatedDocument()) {
                    if (agreementDocument.getName().equals(document.getName())) {
                        response.setType(document);
                        break;
                    }
                }
            }
        }

        return response;

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#getStatusForIdentifierandStatuslist(com.cognizant.insurance
     * .request .Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Agreement> getStatus(Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<Agreement> agreementResponse = new ResponseImpl<Agreement>();
        String query;
        if (agreementRequest.getType() != null) {
            final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
            query = "select insAgr from InsuranceAgreement insAgr where insAgr.identifier =?1 and statusCode not in ?2";
            final List<Object> insuranceAgreementList =
                    findUsingQuery(agreementRequest, query, insuranceAgreement.getIdentifier(), statusRequest.getType());
            if (CollectionUtils.isEmpty(insuranceAgreementList)) {
                throw new DaoException("No agreement with identifier : " + insuranceAgreement.getIdentifier());
            }
            agreementResponse.setType((InsuranceAgreement) insuranceAgreementList.get(0));
        }

        return agreementResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.agreement.dao.AgreementDao#getFnaId(com.cognizant.insurance.request.Request,
     * java.lang.String)
     */
    @Override
    public String getFnaId(final Request<Agreement_GoalAndNeed> request, String illustrationID) {
        String query;
        String fnaID;
        query = "select fnaID from Agreement_GoalAndNeed ags where ags.illustrationID =?1";
        List<Object> res = findUsingQuery(request, query, illustrationID);
        if (res.isEmpty()) {
            return null;
        } else {

            fnaID = (String) res.get(0);
        }
        return fnaID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#getAgreementCount(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<SearchCountResult> getAgreementCount(Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
        List<Object> searchCriteriaList = null;
        String type = null;
        String query;

        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            type = searchCriteria.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select count(insAgr.identifier), insAgr.statusCode from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 and "
                                + "(insAgr.transactionKeys.key6 like ?4 or insAgr.transactionKeys.key8 like ?4 or insAgr.transactionKeys.key26 like ?4) "
                                + "group by insAgr.statusCode";
                
                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType(), searchCriteriatRequest.getContextId(),
                                "%" + searchCriteria.getValue() + "%");

            } else {
                query =
                        "select count(insAgr.identifier), insAgr.statusCode from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 "
                                + "and MONTH(insAgr.creationDateTime) = MONTH(GETDATE()) and YEAR(insAgr.creationDateTime) = YEAR(GETDATE())"
                                + "group by insAgr.statusCode";

                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType(), searchCriteriatRequest.getContextId());
            }
        }
        final List<SearchCountResult> subModeResults = new ArrayList<SearchCountResult>();
        final SearchCountResult searchCountResult = new SearchCountResult();
        SearchCountResult submode;
        Integer totalModCount = 0;
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object searchCriteria : searchCriteriaList) {
                Object[] values = (Object[]) searchCriteria;
                Long count = (Long) values[0];
                AgreementStatusCodeList mode = (AgreementStatusCodeList) values[1];
                submode = new SearchCountResult();
                submode.setCount(count.toString());
                submode.setMode(mode.toString());

                subModeResults.add(submode);
                totalModCount += Integer.valueOf(submode.getCount());
            }
            searchCountResult.setSubModeResults(subModeResults);

        }
        searchCountResult.setCount(String.valueOf(totalModCount));
        searchCountResult.setMode(type);

        searchCountResponse.setType(searchCountResult);
        return searchCountResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#getAgreementByFilter(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public List<Agreement> getAgreementByFilter(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        String query;
        List<Object> searchCriteriaList = null;

        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select insAgr from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 "
                                + "(insAgr.transactionKeys.key6 like ?4 or insAgr.transactionKeys.key8 like ?4)";
                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType(), searchCriteriatRequest.getContextId(),
                                "%" + searchCriteria.getValue() + "%");

            } else {
                query =
                        "select insAgr from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 ";

                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType(), searchCriteriatRequest.getContextId());
            }
        }

        final List<Agreement> agreements = new ArrayList<Agreement>();
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object agreement : searchCriteriaList) {
                agreements.add((InsuranceAgreement) agreement);
            }
        }
        return agreements;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#updateAgreementStatusForLead(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.Request)
     */
    public void updateAgreementStatusForLead(Request<String> leadTransTrackingIdRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        JPARequestImpl<String> jpaRequestObject = null;
        if (leadTransTrackingIdRequest instanceof JPARequestImpl<?>) {
            jpaRequestObject = (JPARequestImpl<String>) leadTransTrackingIdRequest;
        }
        String query =
                "select insAgr from InsuranceAgreement insAgr where insAgr.contextId =?1 and insAgr.transactionKeys.key1 =?2 and insAgr.statusCode in ?3";

        if (null != jpaRequestObject) {
            final List<Object> insuranceAgreementList =
                    findUsingQuery(jpaRequestObject, query, jpaRequestObject.getContextId(),
                            (String) jpaRequestObject.getType(), statusRequest.getType());
            if (CollectionUtils.isNotEmpty(insuranceAgreementList)) {
                for (Object agreement : insuranceAgreementList) {
                    InsuranceAgreement insrAgreement = ((InsuranceAgreement) agreement);
                    insrAgreement.setStatusCode(AgreementStatusCodeList.Cancelled);
                    merge(insrAgreement, jpaRequestObject.getEntityManager());

                }
            }
        }
    }
    

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.agreement.dao.AgreementDao#getRequirementDocumentDetails(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Requirement> getRequirementDocumentDetails(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<Requirement> response = new ResponseImpl<Requirement>();
        String query =
                "select agreement from InsuranceAgreement agreement , Requirement requirement "
                        + "left join fetch agreement.requirements relatedRequirement "
                        + "where requirement.requirementName = relatedRequirement.requirementName and "
                        + "agreement.identifier =?1 and agreement.statusCode in ?2 and "
                        + "requirement.requirementName=?3";
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
        if (insuranceAgreement.getIdentifier() == null || statusRequest.getType().size() == 0
                || insuranceAgreement.getRequirements() == null) {
            return response;
        }
        for (Requirement agreementRequirementObj : insuranceAgreement.getRequirements()) {
            List<Agreement> agreementWithRequirement =
                    findByQuery(agreementRequest, query, insuranceAgreement.getIdentifier(), statusRequest.getType(),
                            agreementRequirementObj.getRequirementName());
            System.out.println(agreementWithRequirement);
            if (CollectionUtils.isNotEmpty(agreementWithRequirement) && agreementWithRequirement.get(0) != null
                    && CollectionUtils.isNotEmpty(agreementWithRequirement.get(0).getRequirements())) {
                for (Requirement requirementTemp : agreementWithRequirement.get(0).getRequirements()) {
                    if (requirementTemp.getRequirementName().equals(agreementRequirementObj.getRequirementName())) {
                        response.setType(requirementTemp);
                        break;
                    }
                }
            }
        }

        return response;
    }

    public void getInsuranceAgreementForReqDocUpdate(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        // final Response<AgreementDocument> response = new
        // ResponseImpl<AgreementDocument>();
        String query =
                "select agreement from InsuranceAgreement agreement "
                        + "left join agreement.requirements relatedRequirement "
                        + "left join  relatedRequirement.requirementDocuments requirementDocument "
                        + "where requirementDocument.name =  ?4 and "
                        + "agreement.identifier =?1 and agreement.statusCode in ?2 and "
                        + "relatedRequirement.requirementName=?3";
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();

        for (Requirement agreementRequirementObj : insuranceAgreement.getRequirements()) {
            for (AgreementDocument document : agreementRequirementObj.getRequirementDocuments()) {
                List<Agreement> agreementWithRequirement =
                        findByQuery(agreementRequest, query, insuranceAgreement.getIdentifier(),
                                statusRequest.getType(), agreementRequirementObj.getRequirementName(),
                                document.getName());
                if (CollectionUtils.isNotEmpty(agreementWithRequirement) && agreementWithRequirement.get(0) != null
                        && CollectionUtils.isNotEmpty(agreementWithRequirement.get(0).getRequirements())) {
                    for (Requirement agreementRequirementObjResult : agreementWithRequirement.get(0).getRequirements())
                        for (AgreementDocument documentResult : agreementRequirementObjResult.getRequirementDocuments()) {
                            if (document.getName().equals(documentResult.getName())) {                            	
                                // add page of this doc
                                for (Document page : document.getPages()) {
                                    if (page.getDocumentStatus().equals(SAVEDSTATUS)) {
                                        // Comment Save and Signature save on second time confirm click
                                        if (documentResult.getPages().contains(page)) {
                                            for (Document pageToCopy : documentResult.getPages()) {
                                                if (pageToCopy.getFileNames().equals(page.getFileNames())) {
                                                    pageToCopy.setBase64string(page.getBase64string());
                                                    pageToCopy.setDescription(page.getDescription());

                                                }
                                            }

                                        } else { // New page
                                            documentResult.getPages().add(page);
                                            break;
                                        }
                                    } else if (page.getDocumentStatus().equals(DELETEDSTATUS)) {
                                        for (Document targetPage : documentResult.getPages()) {
                                            if (targetPage.getFileNames().equals(page.getFileNames())) {
                                                targetPage.setDocumentStatus(DELETEDSTATUS);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                } else {
                    throw new DaoException("No agreement Document matching : " + insuranceAgreement.getIdentifier());
                }
            }
        }
    }
    
    
    
    public void getInsuranceAgreementForSaveReq(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
    	Boolean docnameFlag = false;
        String query =
                "select agreement from InsuranceAgreement agreement "
                        + "left join agreement.requirements relatedRequirement "                        
                        + "where agreement.identifier =?1 and agreement.statusCode in ?2 and "
                        + "relatedRequirement.requirementName=?3";
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();

        for (Requirement agreementRequirementObj : insuranceAgreement.getRequirements()) {
            for (AgreementDocument document : agreementRequirementObj.getRequirementDocuments()) {
                List<Agreement> agreementWithRequirement =
                        findByQuery(agreementRequest, query, insuranceAgreement.getIdentifier(),
                                statusRequest.getType(), agreementRequirementObj.getRequirementName());
                if (CollectionUtils.isNotEmpty(agreementWithRequirement) && agreementWithRequirement.get(0) != null
                        && CollectionUtils.isNotEmpty(agreementWithRequirement.get(0).getRequirements())) {
                    for (Requirement agreementRequirementObjResult : agreementWithRequirement.get(0).getRequirements()) {
                    	if(agreementRequirementObjResult.getRequirementName().equals(agreementRequirementObj.getRequirementName())) {
	                        for (AgreementDocument documentResult : agreementRequirementObjResult.getRequirementDocuments()) {
		                            if (document.getName().equals(documentResult.getName())) {
		                            	documentResult.setDocumentProofSubmitted(document.getDocumentProofSubmitted());
		                            	docnameFlag = true;
		                            	break;
		                            }                                                    
		                    } 
	                        if(!docnameFlag){
	                        	agreementRequirementObjResult.getRequirementDocuments().add(document);
	                        }  
                    	}                      
                      }
                } else if(CollectionUtils.isEmpty(agreementWithRequirement)) {
                	String queryInside =
                            "select agreement from InsuranceAgreement agreement "
                                    + "left join agreement.requirements relatedRequirement "                        
                                    + "where agreement.identifier =?1 and agreement.statusCode in ?2 ";                                   
                	 List<Agreement> agreementWithNewRequirement =
                             findByQuery(agreementRequest, queryInside, insuranceAgreement.getIdentifier(),
                                     statusRequest.getType());
	                	
	                	 if (CollectionUtils.isNotEmpty(agreementWithNewRequirement) && agreementWithNewRequirement.get(0) != null
	                             && CollectionUtils.isNotEmpty(agreementWithNewRequirement.get(0).getRequirements())) {
	                		 
	                		 agreementWithNewRequirement.get(0).getRequirements().add(agreementRequirementObj);
	                		 
	                	 } else {                		 
	                		 agreementWithNewRequirement.get(0).setRequirements(insuranceAgreement.getRequirements());                 		 
	                	 }            	
                    
                } else {
                	throw new DaoException("No agreement Document matching : " + insuranceAgreement.getIdentifier());
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.agreement.dao.AgreementDao#getRequirementDocFile
     * (com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Document> getRequirementDocFile(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<Document> response = new ResponseImpl<Document>();
        String query =
                "select page from InsuranceAgreement agreement "
                        + "left join agreement.requirements relatedRequirement "
                        + "left join  relatedRequirement.requirementDocuments requirementDocument "
                        + "left join  requirementDocument.pages page " + "where requirementDocument.name =  ?4 and "
                        + "page.fileNames = ?5 and " + "agreement.identifier =?1 and agreement.statusCode in ?2 and "
                        + "relatedRequirement.requirementName=?3";
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();

        for (Requirement agreementRequirementObj : insuranceAgreement.getRequirements()) {
            for (AgreementDocument document : agreementRequirementObj.getRequirementDocuments()) {
                for (Document page : document.getPages()) {
                    List<AgreementDocument> documentFile =
                            findByQueryAndReturnDiffType(agreementRequest, query, insuranceAgreement.getIdentifier(),
                                    statusRequest.getType(), agreementRequirementObj.getRequirementName(),
                                    document.getName(), page.getFileNames());
                    if (CollectionUtils.isEmpty(documentFile)) {
                        throw new DaoException("No File with fileName : ");
                    } else {
                        Document responseFile = documentFile.get(0);
                        response.setType(responseFile);
                    }
                }
            }
        }
        return response;
    }


    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.agreement.dao.AgreementDao#getRequirementDocFileList
     * (com.cognizant.insurance.request. Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<AgreementDocument> getRequirementDocFileList(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<AgreementDocument> response = new ResponseImpl<AgreementDocument>();
        String query =
                "select requirementDocument from InsuranceAgreement agreement "
                        + "left join agreement.requirements relatedRequirement "
                        + "left join  relatedRequirement.requirementDocuments requirementDocument "
                        + "where requirementDocument.name =  ?4 and "
                        + "agreement.identifier =?1 and agreement.statusCode in ?2 and "
                        + "relatedRequirement.requirementName=?3";
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();

        for (Requirement agreementRequirementObj : insuranceAgreement.getRequirements()) {
            for (AgreementDocument document : agreementRequirementObj.getRequirementDocuments()) {
                List<AgreementDocument> documentWithFiles =
                        findByQueryAndReturnDiffType(agreementRequest, query, insuranceAgreement.getIdentifier(),
                                statusRequest.getType(), agreementRequirementObj.getRequirementName(),
                                document.getName());

                if (CollectionUtils.isEmpty(documentWithFiles)) {
                    throw new DaoException("No Document with fileName : ");
                } else {
                    AgreementDocument responseDoc = documentWithFiles.get(0);
                    response.setType(responseDoc);
                }
            }
        }
        return response;
    }
    

}
