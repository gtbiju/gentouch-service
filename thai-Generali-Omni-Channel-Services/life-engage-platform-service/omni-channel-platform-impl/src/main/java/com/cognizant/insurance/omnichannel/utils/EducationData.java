package com.cognizant.insurance.omnichannel.utils;

public class EducationData {

    private Long totalCost;

    private String categoryAxisSavings;

    public String getCategoryAxisSavings() {
        return categoryAxisSavings;
    }

    public void setCategoryAxisSavings(String categoryAxisSavings) {
        this.categoryAxisSavings = categoryAxisSavings;
    }

    public String getCategoryAxisFund() {
        return categoryAxisFund;
    }

    public void setCategoryAxisFund(String categoryAxisFund) {
        this.categoryAxisFund = categoryAxisFund;
    }

    private String categoryAxisFund;

    public Long getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Long totalCost) {
        this.totalCost = totalCost;
    }

    public Long getShortFall() {
        return shortFall;
    }

    public void setShortFall(Long shortFall) {
        this.shortFall = shortFall;
    }

    public Long getAvailableSavings() {
        return availableSavings;
    }

    public void setAvailableSavings(Long availableSavings) {
        this.availableSavings = availableSavings;
    }

    private Long shortFall;

    private Long availableSavings;
}
