/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.agent.domain.documentandcommunication;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.agent.domain.commonelements.commonclasses.Specification;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.ExternalCode;

/**
 * This defines any element that can be used to form communication content. The elementary components of a template are
 * either static or dynamic pieces of text or graphics.
 * 
 * @author 300797
 * 
 * 
 */

@Entity
public abstract class CommunicationContentSpecification extends Specification {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2610666451527251015L;

    /**
     * The label that identifies the non-unique specification of this document specification.
     * 
     * e.g: Addressee section, main body, policyholder section and Appendix A.
     * 
     * 
     * 
     * 
     */
    private String title;

    /**
     * A code representing the language used in this standard text specification (see ISO 639).
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private ExternalCode languageCode;

    /**
     * Version number.
     * 
     * 
     * 
     */
    private String versionNumber;

    /**
     * Gets the title.
     * 
     * @return Returns the title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the languageCode.
     * 
     * @return Returns the languageCode.
     */
    public ExternalCode getLanguageCode() {
        return languageCode;
    }

    /**
     * Gets the versionNumber.
     * 
     * @return Returns the versionNumber.
     */
    public String getVersionNumber() {
        return versionNumber;
    }

    /**
     * Sets The title.
     * 
     * @param title
     *            The title to set.
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Sets The languageCode.
     * 
     * @param languageCode
     *            The languageCode to set.
     */
    public void setLanguageCode(final ExternalCode languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * Sets The versionNumber.
     * 
     * @param versionNumber
     *            The versionNumber to set.
     */
    public void setVersionNumber(final String versionNumber) {
        this.versionNumber = versionNumber;
    }

}
