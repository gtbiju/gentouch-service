package com.cognizant.insurance.agent.service.helper;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.agent.core.exception.InputValidationException;
import com.cognizant.le.security.userauth.LifeEngageUserDetailsImpl;


/**
 * @author 391230
 *
 */

@Component
public class LifeEngageValidationHelper {     
    /** The Constant FAILURE. */
    public static final String FAILURE = "FAILURE";

    /** The Constant FAILURE_CODE. */
    public static final String FAILURE_CODE = "400";
    
    private static Boolean userValidation;
    
    public static boolean validateInputString(String input,String validationPattern) throws InputValidationException{
		if(input==null){
			throw new IllegalArgumentException("input is not valid");
		}
		input = Normalizer.normalize(input, Form.NFKC);
		Pattern pattern = Pattern.compile(validationPattern,Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);
		Matcher matcher = pattern.matcher(input);
		if (matcher.find()) {
		  throw new InputValidationException("unsecured data found on request");
		} 		
		return true;
	}    
    
    
	/**
	 * validate user
	 *
	 * @param transaction the transaction
	 */
    public static void  validateUser(JSONObject transaction){	
    	if(userValidation){
		String requestUserId=transaction.getString("Key11");		
		  String tokenUserId=getUserDetails();
			if(tokenUserId==null||!(tokenUserId.equals(requestUserId))){
				throw new CookieTheftException("Unable to validate User details with useCode:"+requestUserId);
			}
    	}
	}
	
	
	/**
	 * Gets the user details.
	 *
	 * @return the user details
	 */
	private static String getUserDetails(){		
		if(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken){
			return null;
		}
		LifeEngageUserDetailsImpl user=(LifeEngageUserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user.getUsername();
	}
	
	
	/**
	 * Sets the user validation.
	 *
	 * @param validate the user validation to set.
	 */
	
	@Value("${le.platform.service.security.userValidation}")
	public void setUserValidation(Boolean validate) {
		LifeEngageValidationHelper.userValidation = validate;
	}
 
}
