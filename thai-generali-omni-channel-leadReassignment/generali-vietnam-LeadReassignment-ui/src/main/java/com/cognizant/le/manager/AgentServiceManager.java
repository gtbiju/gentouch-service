package com.cognizant.le.manager;

import org.codehaus.jettison.json.JSONObject;

public interface AgentServiceManager {

	
	
	public String isValidUser(String userid,String password ) throws Exception;
	
	public JSONObject getAgentDetails(String userid,String userAction) throws Exception;
	

}
