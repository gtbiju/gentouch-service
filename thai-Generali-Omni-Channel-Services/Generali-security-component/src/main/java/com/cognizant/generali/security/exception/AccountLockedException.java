package com.cognizant.generali.security.exception;

import org.springframework.security.authentication.BadCredentialsException;

public class AccountLockedException extends BadCredentialsException{

	private static final long serialVersionUID = -4190898176521039064L;

    private String errorCode;

    /**
     * Instantiates a new lE bad credentials exception.
     * 
     * @param msg the msg
     */
    public AccountLockedException(String msg) {
        super(msg);
    }

    /**
     * Instantiates a new lE bad credentials exception.
     * 
     * @param msg the msg
     * @param t the t
     */
    public AccountLockedException(String msg, Throwable t) {
        super(msg, t);
    }

    /**
     * Instantiates a new lE bad credentials exception.
     * 
     * @param errorCode the error code
     * @param msg the msg
     */
    public AccountLockedException(final String errorCode, final String msg) {
        super(msg);
        this.setErrorCode(errorCode);
    }

    /**
     * Instantiates a new lE bad credentials exception.
     * 
     * @param errorCode the error code
     * @param msg the msg
     * @param cause the cause
     */
    public AccountLockedException(final String errorCode, final String msg, final Throwable cause) {
        super(msg, cause);
        this.setErrorCode(errorCode);
    }

    /**
     * Sets the error code.
     * 
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Gets the error code.
     * 
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }
}
