package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;
import java.util.List;

public class HierarchyLevelResult implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -127477911741190446L;

    /** The mode. */
    private String mode;

    /** The count. */
    private String count;

    /** The ActivityRatio_YTD. */
    private String ActivityRatio_YTD;

    /** The ActivityRatio_MTD. */
    private String ActivityRatio_MTD;
    
    /** The APE_YTD. */
    private String APE_YTD;

    /** The APE_MTD. */
    private String APE_MTD;
    
    private List<HierarchyLevelResult> subLevelList;
    
    
    /** The dashboard data. */
    private DashBoardData dashBoardData;

    /** The productivity data. */
    private List<ProductivityData> productivityData;

    /**
     * Gets the dashboard data.
     * 
     * @return the dashboard data
     */
    public DashBoardData getDashBoardData() {
        return dashBoardData;
    }

    /**
     * Sets the dashboard data.
     * 
     * @param dashBoardData
     *            the new dashboard data
     */
    public void setDashBoardData(DashBoardData dashBoardData) {
        this.dashBoardData = dashBoardData;
    }

    /**
     * Gets the productivity data.
     * 
     * @return the productivity data
     */
    public List<ProductivityData> getProductivityData() {
        return productivityData;
    }

    /**
     * Sets the productivity data.
     * 
     * @param productivityData
     *            the productivity data to set.
     */
    public void setProductivityData(List<ProductivityData> productivityData) {
        this.productivityData = productivityData;
    }

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getActivityRatio_YTD() {
		return ActivityRatio_YTD;
	}

	public void setActivityRatio_YTD(String activityRatio_YTD) {
		ActivityRatio_YTD = activityRatio_YTD;
	}

	public String getActivityRatio_MTD() {
		return ActivityRatio_MTD;
	}

	public void setActivityRatio_MTD(String activityRatio_MTD) {
		ActivityRatio_MTD = activityRatio_MTD;
	}

	public String getAPE_YTD() {
		return APE_YTD;
	}

	public void setAPE_YTD(String aPE_YTD) {
		APE_YTD = aPE_YTD;
	}

	public String getAPE_MTD() {
		return APE_MTD;
	}

	public void setAPE_MTD(String aPE_MTD) {
		APE_MTD = aPE_MTD;
	}

	public List<HierarchyLevelResult> getSubLevelList() {
		return subLevelList;
	}

	public void setSubLevelList(List<HierarchyLevelResult> subLevelList) {
		this.subLevelList = subLevelList;
	}
}
