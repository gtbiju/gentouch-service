/**
 * 
 */
package com.cognizant.insurance.domain.agreement.agreementcodelists;

/**
 * Contractual change Type.
 * 
 * @author 301350
 * 
 * 
 */
public enum AgreementChangeTypeCodeList {
    /**
     * 
     */
    Cancel,
    /**
     * 
     */
    Renewval,
    /**
     * 
     */
    ExtenstionofPeriod,
    /**
     * 
     */
    Owner

}
