package com.cognizant.insurance.agent.service;

import com.cognizant.insurance.agent.core.exception.BusinessException;

/**
 * The Interface interface LookUpservice.
 */

public interface CodeLookUpService {

    /**
     * Gets the look up data.
     * 
     * @param json
     *            the json
     * @return the look up data
     * @throws BusinessException
     *             the business exception
     */
    String getLookUpData(final String json) throws BusinessException;
}
