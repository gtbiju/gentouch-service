-- CORE_PROD_PROPERTIES
-- INSERT
SET IDENTITY_INSERT [dbo].[CORE_PROD_PROPERTIES] ON
INSERT [dbo].[CORE_PROD_PROPERTIES] ([id], [basicDataCompleteCode], [contextId] , [creationDateTime], [transactionId], [typeName], [CALCULATED_INDICATOR], [CONSTANT_INDICATOR], [DEFAULT_VALUE], [MAX_VALUE], [MIN_VALUE], [NAME], [OPTIONAL_INDICATOR], [VERSION], [parentProduct_id], [propertyType_ID], [modifiedDateTime], [transTrackingId], [transactionKeys_id])
VALUES (80, NULL, NULL , NULL, NULL, NULL, N'', N'', N'Regular', 0, 0, N'Premium Type', N'', 1, 1, N'88107', NULL, NULL, NULL)
INSERT [dbo].[CORE_PROD_PROPERTIES] ([id], [basicDataCompleteCode], [contextId] , [creationDateTime], [transactionId], [typeName], [CALCULATED_INDICATOR], [CONSTANT_INDICATOR], [DEFAULT_VALUE], [MAX_VALUE], [MIN_VALUE], [NAME], [OPTIONAL_INDICATOR], [VERSION], [parentProduct_id], [propertyType_ID], [modifiedDateTime], [transTrackingId], [transactionKeys_id])
VALUES (81, NULL, NULL , NULL, NULL, NULL, N'', N'', N'VND', 0, 0, N'currency', N'', 1, 1, N'88006', NULL, NULL, NULL)
INSERT [dbo].[CORE_PROD_PROPERTIES] ([id], [basicDataCompleteCode], [contextId] , [creationDateTime], [transactionId], [typeName], [CALCULATED_INDICATOR], [CONSTANT_INDICATOR], [DEFAULT_VALUE], [MAX_VALUE], [MIN_VALUE], [NAME], [OPTIONAL_INDICATOR], [VERSION], [parentProduct_id], [propertyType_ID], [modifiedDateTime], [transTrackingId], [transactionKeys_id])
VALUES (82, NULL, NULL , NULL, NULL, NULL, N'', N'', N'', 0, 0, N'Sum Assured', N'', 1, 1, N'88506', NULL, NULL, NULL)
INSERT [dbo].[CORE_PROD_PROPERTIES] ([id], [basicDataCompleteCode], [contextId] , [creationDateTime], [transactionId], [typeName], [CALCULATED_INDICATOR], [CONSTANT_INDICATOR], [DEFAULT_VALUE], [MAX_VALUE], [MIN_VALUE], [NAME], [OPTIONAL_INDICATOR], [VERSION], [parentProduct_id], [propertyType_ID], [modifiedDateTime], [transTrackingId], [transactionKeys_id])
VALUES (83, NULL, NULL , NULL, NULL, NULL, N'', N'', N'', 0, 0, N'Premium Amount', N'', 1, 1, N'88122', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[CORE_PROD_PROPERTIES] OFF