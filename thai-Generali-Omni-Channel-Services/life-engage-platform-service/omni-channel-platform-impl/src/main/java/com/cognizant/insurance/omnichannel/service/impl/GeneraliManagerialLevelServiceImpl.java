/**
 *
 * Copyright 2016, Cognizant 
 *
 * @author        : 291446
 * @version       : 0.1, Sep 08, 2016
 */
package com.cognizant.insurance.omnichannel.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.component.GeneraliManagerialLevelComponent;
import com.cognizant.insurance.omnichannel.service.GeneraliManagerialLevelService;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;

/**
 * The Class class GeneraliManagerialLevelServiceImpl.
 * 
 * @author 291446
 */
@Service("gliManagerialLevelService")
public class GeneraliManagerialLevelServiceImpl implements GeneraliManagerialLevelService {

	@Autowired
    private GeneraliManagerialLevelComponent generaliManagerialLevelComponent;
	
	/** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";   

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliManagerialLevelServiceImpl.class);
	
	
	@Override
	@Transactional(rollbackFor = { Exception.class }, readOnly = true)
	public String retrieveByManagerialLevel(String json)
			throws BusinessException {
		
		LOGGER.trace("Inside GeneraliManagerialLevelServiceImpl.retrieveByManagerialLevel :json " + json);
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        String transactionArray = "{\"Transactions\":[]}";
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);

            transactionArray = generaliManagerialLevelComponent.retrieveByManagerialLevelCount(requestInfo, jsonRqArray);

            JSONArray jsonRsArray = new JSONArray();
            jsonRsArray.put(new JSONObject(transactionArray));
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("GeneraliManagerialLevelServiceImpl.retrieveByManagerialLevel: Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        LOGGER.trace("Inside GeneraliManagerialLevelServiceImpl retrieveByManagerialLevel : jsonObjectRs" + jsonObjectRs.toString());
        return jsonObjectRs.toString();
	}

}
