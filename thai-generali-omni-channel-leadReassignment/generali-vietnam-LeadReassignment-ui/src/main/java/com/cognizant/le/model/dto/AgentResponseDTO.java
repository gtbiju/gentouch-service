package com.cognizant.le.model.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author 444873
 * Class used for storing agent details got from third party service.
 */
public class AgentResponseDTO {

	private String agentId;
	private String agentName;
	private String agentStatus;
	
	private List<MappedBranch> mappedBranchDetails;	
	private List<AgentResponseDTO> activeAgentsDetails;	
	private List<AgentResponseDTO> inActiveAgentsDetails;
	
	/**
	 * @return the agentId
	 */
	public String getAgentId() {
		return agentId;
	}
	/**
	 * @param agentId the agentId to set
	 */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}
	/**
	 * @param agentName the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	/**
	 * @return the agentStatus
	 */
	public String getAgentStatus() {
		return agentStatus;
	}
	/**
	 * @param agentStatus the agentStatus to set
	 */
	public void setAgentStatus(String agentStatus) {
		this.agentStatus = agentStatus;
	}
	
	public List<MappedBranch> getMappedBranchDetails() {
		return mappedBranchDetails;
	}
	public void setMappedBranchDetails(List<MappedBranch> mappedBranchDetails) {
		this.mappedBranchDetails = mappedBranchDetails;
	}
	
	public List<AgentResponseDTO> getActiveAgentsDetails() {
		return activeAgentsDetails;
	}
	public void setActiveAgentsDetails(List<AgentResponseDTO> activeAgentsDetails) {
		this.activeAgentsDetails = activeAgentsDetails;
	}
	public List<AgentResponseDTO> getInActiveAgentsDetails() {
		return inActiveAgentsDetails;
	}
	public void setInActiveAgentsDetails(
			List<AgentResponseDTO> inActiveAgentsDetails) {
		this.inActiveAgentsDetails = inActiveAgentsDetails;
	}
	
}
