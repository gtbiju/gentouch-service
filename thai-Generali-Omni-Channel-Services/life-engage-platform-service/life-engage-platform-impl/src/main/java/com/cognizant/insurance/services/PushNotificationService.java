/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 19, 2015
 */
package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * The Interface interface PushNotificationService.
 */
public interface PushNotificationService {

    /**
     * Register device.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String registerDevice(final String json) throws BusinessException;

    /**
     * Un register device.
     * 
     * @param json
     *            the json
     * @throws BusinessException
     *             the business exception
     */
    String unRegisterDevice(final String json) throws BusinessException;

    /**
     * Push notifications.
     * 
     * @param json
     *            the json
     * @throws BusinessException
     *             the business exception
     */
    String pushNotifications(final String json) throws BusinessException;
}
