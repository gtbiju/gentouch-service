package com.cognizant.insurance.generic.dao.impl;

import java.util.List;

import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.generic.dao.EmailDao;
import com.cognizant.insurance.request.Request;


public class EmailDaoImpl extends DaoImpl implements EmailDao {
	
	/** The Constant FAILURE. */
    private static final String FAILURE = "Failure";
    
    /** The Constant INITIALISED. */
    private static final String INITIALISED = "initialised";
    
	@Override
    public List<LifeEngageEmail>
    retrieveFailedLifeEngageEmailDetails(Request<LifeEngageEmail> request, String maxAttemptNo) {
        String query =
                "select emailDetails from LifeEngageEmail emailDetails where emailDetails.status =?1 and emailDetails.attemptNumber <?2";
        List<LifeEngageEmail> lifeEngageEmailList =
                findByQuery(request, query, FAILURE, maxAttemptNo);
       return lifeEngageEmailList;
    }
	
	@Override
    public List<LifeEngageEmail>
    retrieveInitialisedLifeEngageEmailDetails(Request<LifeEngageEmail> request) {
	    List<LifeEngageEmail> lifeEngageEmailList = null;
        if (request.getType() != null) {
            LifeEngageEmail lifeEngageEmail = request.getType();
            String query =
                    "select emailDetails from LifeEngageEmail emailDetails where emailDetails.status =?1 and emailDetails.agentId =?2 and emailDetails.type =?3";
            lifeEngageEmailList =
                    findByQuery(request, query, INITIALISED, lifeEngageEmail.getAgentId(), lifeEngageEmail.getType());
        }	    
       return lifeEngageEmailList;
    }
}
