/**
 *

 * Copyright 2012, Cognizant
 *
 * @author        : 304000
 * @version       : 0.1, Feb 21, 2014
 */
package com.cognizant.insurance.domain.goalandneed;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * The Class Agreement_goalAndNeed.
 */
@Entity
public class Agreement_GoalAndNeed{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The id. */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long ID;
	
	private String fnaID;
	
	private String illustrationID;
	
	public String getFnaID() {
		return fnaID;
	}

	public void setFnaID(String fnaID) {
		this.fnaID = fnaID;
	}

	public String getIllustrationID() {
		return illustrationID;
	}

	public void setIllustrationID(String illustrationID) {
		this.illustrationID = illustrationID;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}


}
