--Query to fetch eApp Details

select a.identifier 'EappNo' , tk.key21 'Application Number', case statusCode  when 0 then'Final'
    when 1 then'Inforce'
    when 2 then'InForceChangePending'  
    when 3 then'Initial' 
    when 4 then'Suspended'  
    when 5 then'Terminated'  
    when 6 then'UnderNegotiation'   
    when 7 then'Cancelled' 
    when 8 then'Proposed'  
    when 9 then'NotMentioned'
    when 10 then'Draft'   
    when 11 then'Completed'   
    when 12 then'Confirmed'  
    when 13 then'ReportSent'    
    when 14 then'New' 
    when 15 then'Submitted'   
    when 16 then'Lapse'   
    when 17 then'Surrender'   
    when 18 then'InProgress'   
    when 19 then'Review'    
    when 20 then'PaymentDone' END  as Status, 
ISnULL(case ae.bpmStatus 
	WHEN 'PE' THEN 'Pending'
	WHEN 'IF' THEN 'Inforce'
	WHEN 'DC' THEN 'Decline'
	WHEN 'WD' THEN 'Withdraw'
	WHEN 'NT' THEN 'NTU'
	WHEN 'CF' THEN 'Cancel'
	WHEN 'PO' THEN 'Postpone'
	WHEN 'RI' THEN 'Ready to issue'
	WHEN 'Submitted' THEN 'Submitted' END,'') 'Status In Life Asia',
IsNull((CONVERT(VARCHAR(30),crtnDateTemp.crtdDateTime,101) +' ' + CONVERT(VARCHAR(30),crtnDateTemp.crtdDateTime,8)),'') as 'Created Date Time (Synced)', 
IsNull((CONVERT(VARCHAR(30),Newtmp.newStatustTime,101) +' ' + CONVERT(VARCHAR(30),Newtmp.newStatustTime,8)),'') as 'Time when Status Changed to NEW(Synced)', 
IsNull((CONVERT(VARCHAR(30),CompltdTmp.completedStatustTime,101) +' ' + CONVERT(VARCHAR(30),CompltdTmp.completedStatustTime,8)),'') as 'Completed Date Time (Synced)', 
IsNull((CONVERT(VARCHAR(30),Cnftmp.cnfstatustTime,101) +' ' + CONVERT(VARCHAR(30),Cnftmp.cnfstatustTime,8)),'') as 'Confirmed Date Time (Synced)', 
IsNull((CONVERT(VARCHAR(30),paymntTemp.paymentDateTime,101) +' ' + CONVERT(VARCHAR(30),paymntTemp.paymentDateTime,8)),'') as 'Payment Date Time (Synced)', 
CnfDate. CnfdDateTime as 'Confirmed Date Time (Offline)',
IsNull(CONVERT(VARCHAR(20),lr.CreationDate,113),'') as 'Submitted Date Time (To Life Asia)',
tk.key11 Agent, isnull(ae.partnerCode,'') as 'Branch Code', ISNULL(APESET.APE,0) Premium, 
isNULL(case APESET.pf
		WHEN 0 THEN 'Anypaymentmodeisallowed'
		WHEN 1 THEN 'FiscalQuarterlymodeofpayment'
		WHEN 2 THEN 'Paymentmade5timesayear'
		WHEN 3 THEN 'Paymentsmade7timesperyear'
		WHEN 4 THEN 'Paymentsmade28timesperyear'
		WHEN 5 THEN 'Paymentsmade11timesperyear'
		WHEN 6 THEN 'Paymentsmade14timesperyear'
		WHEN 7 THEN 'Single Payment'
		WHEN 8 THEN 'Annual'
		WHEN 9 THEN 'Annually'
		WHEN 10 THEN 'Quarterly'
		WHEN 11 THEN 'HalfYearly'
		WHEN 12 THEN 'Monthly'
		WHEN 13 THEN 'Accumulated'
		WHEN 14 THEN 'Yearly'
		WHEN 15 THEN ''
		WHEN 16 THEN 'Semi Annual'
		WHEN 17 THEN 'Semi Annually'
		WHEN 18 THEN 'Single'
		WHEN 19 THEN 'Semester'
		WHEN 20 THEN 'Half Yearly'END,'') 'Premium Frequency',	
		ISNULL(APESET.APE,0)*PFValue APE,
ae.Source Source, isNULL(case ProductCodeSET.ProductCode
WHEN 1 THEN 'CIB1'
WHEN 9 THEN 'CIB2'END,'') 'Product'

from agreement a  with (nolock) join transactionkeys tk  with (nolock) on a.transactionkeys_id=tk.id 
INNER join agreementExtension ae  with (nolock) on a.agreementextension_id=ae.id 

left join (select e.code ProductCode,agmt.id agmtID  from coverage c with (nolock) inner join externalcode e with (nolock) on c.productCode_id=e.id inner join agreement_coverage ac  
with (nolock) on c.id =ac.includesCoverage_id inner join agreement agmt  with (nolock) on agmt.id=ac.agreement_id
where agmt.contextID = 4 and agmt.statuscode NOT IN (4) and c. coverageCode=1)ProductCodeSET on ProductCodeSET.agmtID=a.id
--Retrieves APE value
left join  (select agmt.id agmtID, ca.amount APE, isnull(CASE c.premiumFrequency
		WHEN 10 THEN 4 -- QUARTERLY
		WHEN 14 THEN 1 --YEARLY
		WHEN 15 THEN 0 --NULL
		WHEN 20 THEN 2 -- HALF YEARLY
		END,0) PFValue,c.premiumFrequency pf from  currencyAmount ca   with (nolock) inner join 
premium p  with (nolock) on ca.id= p.amount_id inner join Coverage_Premium cp   with (nolock) on cp.premium_id=p.id 
inner join coverage c with (nolock) on c.id=cp.Coverage_id inner join agreement_coverage ac  
with (nolock) on c.id =ac.includesCoverage_id inner join agreement agmt  with (nolock) on agmt.id=ac.agreement_id
where agmt.contextID = 4 and agmt.statuscode NOT IN (4) and c. coverageCode=1 and p.natureCode=16) APESET on APESET.agmtID=a.id

--Gets the Date and time at which proposer signs and confirms eApp
left join (select agmnt.id , IsNull((CONVERT(VARCHAR(30),c.actualTimePeriod,101) +' ' + CONVERT(VARCHAR(30),c.actualTimePeriod,8)),'') 
CnfdDateTime from agreement agmnt with (nolock) inner join partyroleinagreement pa with (nolock) on agmnt.id = pa.isPartyRolein_id and 
pa.PARTY_ROLE_IN_AGREEMENT_TYPE='AGREEMENTHOLDER' and agmnt.statusCode not in (4) inner join CommunicationContent cc with (nolock) on 
cc.id=pa.declares_id inner join Communication c with (nolock) on cc.id=c.id) CnfDate on CnfDate.id=a.id 

--To get life asia related details
left join lifeAsiaRequest lr with (nolock) on lr.eAPPID=a.identifier 

--To get the time at which eApp is synced with a status New
LEFT JOIN (select min (a.id) ID ,a.identifier identifier ,min(modifiedDatetime) newStatustTime from agreement a 
with (nolock) inner join transactionkeys tk with (nolock) on a.transactionkeys_id=tk.id   where tk.key15='New'
and a.contextId=4  group by a.identifier) Newtmp ON Newtmp.identifier=a.identifier 

--To get the time at which eApp is synced with a status Completed
LEFT JOIN (select min (a.id) ID,a.identifier identifier ,min(modifiedDatetime) completedStatustTime 
from agreement a with (nolock) inner join transactionkeys tk with (nolock) on a.transactionkeys_id=tk.id   
where tk.key15='Completed' and a.contextId=4  group by a.identifier) CompltdTmp on CompltdTmp.identifier=a.identifier

----To get the time at which eApp is synced with a status Confirmed
LEFT JOIN (select min (a.id) ID,a.identifier identifier ,min(modifiedDatetime) cnfstatustTime from agreement a 
with (nolock) inner join transactionkeys tk with (nolock) on a.transactionkeys_id=tk.id   where tk.key15='Confirmed' 
and a.contextId=4  group by a.identifier) Cnftmp ON Cnftmp.identifier=a.identifier

----To get the time at which eApp is synced with a status PaymentDone
LEFT JOIN (select min (a.id) ID,a.identifier identifier ,min(modifiedDatetime) paymentDateTime from agreement a 
with (nolock) inner join transactionkeys tk with (nolock) on a.transactionkeys_id=tk.id   where tk.key15='PaymentDone' 
and a.contextId=4  group by a.identifier) paymntTemp ON paymntTemp.identifier=a.identifier

--To get the Creation date; Date at which first Sync happens.
LEFT JOIN (select min (ag.id) ID,min(ag.creationDateTime) crtdDateTime,  ag.identifier identifier 
from agreement ag with (nolock) group by ag.identifier) crtnDateTemp on crtnDateTemp.identifier=a.identifier

-- Removing suspended records.
where a.statusCode not in (4) order by a.id

