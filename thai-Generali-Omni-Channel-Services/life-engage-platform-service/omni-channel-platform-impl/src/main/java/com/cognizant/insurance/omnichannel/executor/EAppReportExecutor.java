package com.cognizant.insurance.omnichannel.executor;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.omnichannel.component.GeneraliEAppComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliEmailComponent;
import com.cognizant.insurance.omnichannel.utils.GeneraliExcelSheetUtil;
import com.cognizant.insurance.omnichannel.vo.AgentReportVo;

/**
 * Class : - EAppReportExecutor - Scheduler to create eApp report
 * 
 * @author 675473
 * 
 */
@Component
public class EAppReportExecutor {
	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory.getLogger(AgentWeeklyReportExecutor.class);

	@Autowired
	private GeneraliEmailComponent generaliEmailComponent;

	@Autowired
	private GeneraliExcelSheetUtil generaliExcelSheetUtil;

	@Autowired
	private GeneraliEAppComponent generaliEAppComponent;

	private static final String EAPP = "eApp";

	public void run() {
		List<AgentReportVo> agentData = null;
		String fileName = null;
		try {
			LOGGER.info("eApp Report Data Load-> Start");
			agentData = generaliEAppComponent.getEAppReport(EAPP);
			LOGGER.info("eApp Report Data Load-> End");
			LOGGER.info("eApp Report Excel generation-> Start");
			fileName = generaliExcelSheetUtil.generateEAppExcelReport(agentData, EAPP);
			LOGGER.info("eApp Report Excel generation-> End");
			LOGGER.info("eApp Report mail-> Start");
			generaliEmailComponent.sendAgentReport(fileName, EAPP);
			LOGGER.info("eApp Report mail-> End");
		} catch (Exception e) {
			LOGGER.error("Error in eApp Report scheduler {}", e.getMessage());
		}
	}

}
