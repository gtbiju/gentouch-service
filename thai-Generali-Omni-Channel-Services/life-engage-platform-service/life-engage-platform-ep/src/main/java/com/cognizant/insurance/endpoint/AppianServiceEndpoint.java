package com.cognizant.insurance.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.service.GeneraliService;

/**
 * Class contains the end point service to interact with Appian service.
 *
 */
@Controller
@RequestMapping("/appianService")
public class AppianServiceEndpoint {

	
	@Autowired
	GeneraliService generaliService;
	
	/** The update memo service. */
	@RequestMapping(value = "/updateMemo", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
    public final String updateMemoDetails(@RequestBody final String json) throws BusinessException {
		return generaliService.updateMemoDetails(json);

    }
	
	/** The service for updating missing bpm data. */
	@RequestMapping(value = "/updateAppainStatus", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
    public final String updateAppainStatus(@RequestBody final String json) throws BusinessException {
		return generaliService.updateAppainStatus(json);

    }
	
}
