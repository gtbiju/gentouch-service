package com.cognizant.insurance.agent.domain.documentandcommunication.documentandcommunicationcodelists;

public enum CommunicationInteractionTypeCodeList {

    /** Face to Face. */
    F2F,

    /** PhoneCall. */
    PhoneCall,
    
    Email,
    
    Others,
    
    Appointment,

    /** Not Mentioned. */
    NotMentioned
}
