/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.agreementcodelists;

/**
 * It indicates the ownership/leased status of this vehicle.
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */
public enum VehicleLeasedCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Ownedbyinsuredleasedorhiredtoothers,
    /**
     * 
     * 
     * 
     * 
     */
    Notownedbyinsuredleasedorhiredfromothers,
    /**
     * 
     * 
     * 
     * 
     */
    Ownedbyinsurednotleasedtoothers
}
