/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.contactcodelists;

/**
 * Identifies a classification of TimingPreferences according to their day of a
 * week.
 * 
 * @author 301350
 * 
 * 
 */
public enum DayOfWeekCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Monday,
    /**
     * 
     * 
     * 
     * 
     */
    Tuesday,
    /**
     * 
     * 
     * 
     * 
     */
    Wednesday,
    /**
     * 
     * 
     * 
     * 
     */
    Thursday,
    /**
     * 
     * 
     * 
     * 
     */
    Friday,
    /**
     * 
     * 
     * 
     * 
     */
    Saturday,
    /**
     * 
     * 
     * 
     * 
     */
    Sunday
}
