/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */

package com.cognizant.insurance.agent.agreement.dao;

import java.util.List;
import java.util.Set;

import com.cognizant.insurance.agent.dao.Dao;
import com.cognizant.insurance.agent.domain.agreement.Agreement;
import com.cognizant.insurance.agent.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.agent.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.agent.domain.documentandcommunication.Document;
import com.cognizant.insurance.agent.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.agent.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.agent.domain.finance.agreementloan.AgreementLoan;
import com.cognizant.insurance.agent.domain.finance.agreementloan.AgreementWithdrawal;
import com.cognizant.insurance.agent.domain.goalandneed.Agreement_GoalAndNeed;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.searchcriteria.SearchCountResult;
import com.cognizant.insurance.agent.searchcriteria.SearchCriteria;

/**
 * The Interface interface AgreementDao.
 */
public interface AgreementDao extends Dao {

    /**
     * Gets the agreement loans.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the agreement loans
     */
    Response<Set<AgreementLoan>> getAgreementLoans(Request<Agreement> agreementRequest);

    /**
     * Gets the agreements. This method select the Agreements for a particular agent(Agreement.agentId), for a
     * particular context(agreementRequest.ContextType), for given status(statusRequest) and its creation date is
     * greater than the given date(Agreement.lastSyncDate)
     * 
     * @param agreementRequest
     *            the agreement request
     * @param statusRequest
     *            the status request
     * @param limitRequest
     *            the limit request
     * @return the agreements
     */
    List<Agreement> getAgreements(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest, Request<Integer> limitRequest);

    /**
     * Gets the agreement withdrawals.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the agreement withdrawal
     */
    Response<Set<AgreementWithdrawal>> getAgreementWithdrawals(Request<Agreement> agreementRequest);

    /**
     * Gets the insurance agreement.
     * 
     * @param agreementRequest
     *            the agreement request
     * @param statusRequest
     *            the status request
     * @return the insurance agreement
     */
    Response<Agreement> getInsuranceAgreement(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Gets the previous agreements for a party.
     * 
     * @param partyRoleInAgreementRequest
     *            the party role in agreement request
     * @return the previous agreements
     */
    Response<Set<InsuranceAgreement>> getPreviousAgreements(Request<PartyRoleInAgreement> partyRoleInAgreementRequest);

    /**
     * Retrieve agreement for identifier.
     * 
     * @param agreementRequest
     *            the agreement request
     * @param statusRequest
     *            the status request
     * @return the list
     */
    List<Agreement> retrieveAgreementForIdentifier(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Gets the agreement document details.
     * 
     * @param agreementRequest
     *            the agreement request
     * @param statusRequest
     *            the status request
     * @return the agreement document details
     */
    Response<AgreementDocument> getAgreementDocumentDetails(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Gets the Status of Proposal Number.
     * 
     * @param agreementRequest
     *            the agreement request
     * @param statusRequest
     *            the status request
     * @return the status
     */
    Response<Agreement> getStatus(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Gets the Fna id.
     * 
     * @param request
     *            the request
     * @param illustrationID
     *            the illustration id
     * @return the fna id
     */
    String getFnaId(Request<Agreement_GoalAndNeed> request, String illustrationID);

    /**
     * Gets the agreement count.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the agreement count
     */
    Response<SearchCountResult> getAgreementCount(Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * Gets the agreement by filter.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the agreement by filter
     */
    List<Agreement> getAgreementByFilter(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    
    /**
     * Update agreement status for lead.
     *
     * @param leadTransTrackingIdRequest the lead trans tracking id request
     * @param statusRequest the status request
     */
    void updateAgreementStatusForLead(Request<String> leadTransTrackingIdRequest, Request<List<AgreementStatusCodeList>> statusRequest);
    
    /**
     * Retrieve agreement for trans tracking id.
     *
     * @param agreementRequest the agreement request
     * @param statusRequest the status request
     * @return the list
     */
    List<Agreement> retrieveAgreementForTransTrackingId(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    
    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
    Response<Requirement> getRequirementDocumentDetails(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
   void getInsuranceAgreementForReqDocUpdate(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
    Response<Document> getRequirementDocFile(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);

    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
    Response<AgreementDocument> getRequirementDocFileList(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    
    
    /**
     * @param agreementRequest
     * @param statusRequest
     * @return
     */
   void getInsuranceAgreementForSaveReq(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    
}
