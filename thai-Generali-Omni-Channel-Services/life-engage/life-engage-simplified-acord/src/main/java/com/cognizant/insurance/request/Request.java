/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Dec 7, 2012
 */

package com.cognizant.insurance.request;

import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;

/**
 * The Interface interface Request.
 * 
 * @param <T>
 *            the generic type
 */
public interface Request<T> {

    /**
     * Sets the type.
     * 
     * @param type
     *            the type to set.
     */
    void setType(T type);

    /**
     * Gets the type.
     * 
     * @return the type
     */
    T getType();

    /**
     * Gets the contextId.
     * 
     * @return Returns the contextId.
     */
    ContextTypeCodeList getContextId();

    /**
     * Sets The contextId.
     * 
     * @param contextId
     *            The contextId to set.
     */
    void setContextId(ContextTypeCodeList contextId);

    /**
     * Gets the transactionId.
     * 
     * @return Returns the transactionId.
     */
    String getTransactionId();

    /**
     * Sets The transactionId.
     * 
     * @param transactionId
     *            The transactionId to set.
     */
    void setTransactionId(String transactionId);

}
