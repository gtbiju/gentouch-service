/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Class PropertyValues.
 * 
 * @author 304007
 */

@Entity
@Table(name = "CORE_PROPERTY_VALUES")
public class PropertyValues implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @Column(name = "ID")
    private Long id;

    /** The data value. */
    @Column(name = "DATA_VALUE")
    private String dataValue;

    /** The data type. */
    @Column(name = "DATA_TYPE")
    private String dataType;

    /** The default indicator. */
    @Column(name = "DEFAULT_INDICATOR")
    private String defaultIndicator;

    /** The property specification. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "propertySpecification_id")
    private PropertySpecification propertySpecification;

    /**
     * The Constructor.
     */
    public PropertyValues() {
        super();
    }

    /**
     * The Constructor.
     * 
     * @param id
     *            the id
     */
    public PropertyValues(final Long id) {
        this.id = id;
    }

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the data value.
     * 
     * @return the data value
     */
    public String getDataValue() {
        return dataValue;
    }

    /**
     * Sets the data value.
     * 
     * @param dataValue
     *            the data value
     */
    public void setDataValue(final String dataValue) {
        this.dataValue = dataValue;
    }

    /**
     * Gets the data type.
     * 
     * @return the data type
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * Sets the data type.
     * 
     * @param dataType
     *            the data type
     */
    public void setDataType(final String dataType) {
        this.dataType = dataType;
    }

    /**
     * Gets the default indicator.
     * 
     * @return the default indicator
     */
    public String getDefaultIndicator() {
        return defaultIndicator;
    }

    /**
     * Sets the default indicator.
     * 
     * @param defaultIndicator
     *            the default indicator
     */
    public void setDefaultIndicator(final String defaultIndicator) {
        this.defaultIndicator = defaultIndicator;
    }

    /**
     * Sets The propertySpecification.
     * 
     * @param propertySpecification
     *            The propertySpecification to set.
     */
    public void setPropertySpecification(final PropertySpecification propertySpecification) {
        this.propertySpecification = propertySpecification;
    }

    /**
     * Gets the propertySpecification.
     * 
     * @return Returns the propertySpecification.
     */
    public PropertySpecification getPropertySpecification() {
        return propertySpecification;
    }

}
