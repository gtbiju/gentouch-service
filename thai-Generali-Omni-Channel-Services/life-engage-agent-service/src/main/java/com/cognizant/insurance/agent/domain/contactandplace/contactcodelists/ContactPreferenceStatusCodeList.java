/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.contactcodelists;

/**
 * A classification of the various statuses of a contact preference.
 * 
 * @author 301350
 * 
 * 
 */
public enum ContactPreferenceStatusCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Current,
    /**
     * 
     * 
     * 
     * 
     */
    Previous,
    /**
     * 
     * 
     * 
     * 
     */
    Temporary
}
