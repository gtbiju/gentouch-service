/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */

package com.cognizant.insurance.agent.party.component;

import java.util.List;
import java.util.Set;

import com.cognizant.insurance.agent.domain.agreement.Agreement;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Appointee;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.agent.domain.party.Person;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.searchcriteria.SearchCountResult;
import com.cognizant.insurance.agent.searchcriteria.SearchCriteria;
/*import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;*/

// TODO: Auto-generated Javadoc
/**
 * The Interface interface PartyComponent.
 */
public interface PartyComponent {

    /**
     * Save person.
     * 
     * @param request
     *            the request
     * @return the response
     */
    Response<Long> savePerson(Request<Person> request);

    /**
     * Save party role in agreement.
     * 
     * @param request
     *            the request
     * @return the response
     */
    void savePartyRoleInAgreement(Request<PartyRoleInAgreement> request);

    /**
     * Retrieve insured.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<Insured> retrieveInsured(Request<Agreement> agreementRequest);

    /**
     * Retrieve payer.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<PremiumPayer> retrievePayer(Request<Agreement> agreementRequest);

    /**
     * Retrieve proposer.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<AgreementHolder> retrieveProposer(Request<Agreement> agreementRequest);

    /**
     * Retrieve beneficiaries.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<Set<Beneficiary>> retrieveBeneficiaries(Request<Agreement> agreementRequest);

    /**
     * Retrieve appointee.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<Appointee> retrieveAppointee(Request<Agreement> agreementRequest);

    /**
     * Retrieves the Insured Previous policy - ie PartyRoleInAgreement entries.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the Insured
     */
    Response<Insured> retrieveInsuredPreviousPolicyAgreement(Request<Agreement> agreementRequest);

    /**
     * Save party role in Relationship.
     * 
     * @param request
     *            the request
     * @return the response
     */
    void savePartyRoleInRelationship(Request<PartyRoleInRelationship> request);

    /**
     * Retrieve Customer.
     * 
     * @param identifier
     *            the identifier
     * @param statusRequest
     *            the status request
     * @return the response
     */

	Response<Customer> retrieveCustomer(Request<Customer> identifierRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

    /**
     * Retrieve Customers.
     * 
     * @param customerRequest
     *            the customer request
     * @param statusRequest
     *            the status request
     * @param limitRequest
     *            the limit request
     * @return the response
     */
    Response<List<Customer>> retrieveCustomers(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest, Request<Integer> limitRequest);

    /**
     * Retrieve Customer with identifier.
     * 
     * @param customerRequest
     *            the customer request
     * @param statusRequest
     *            the status request
     * @return the response
     */
    Response<Customer> retrieveCustomerForIdentifier(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest);

    /**
     * update Customer.
     * 
     * @param customerRequest
     *            the customer request
     * @return the response
     */
    Response<Customer> updateCustomer(final Request<Customer> customerRequest);

    /**
     * Retrieve duplicate lead list.
     * 
     * @param customerRequest
     *            the customer request
     * @param transactions
     *            the transactions
     * @param statusRequest
     *            the status request
     * @param telephoneTypeRequest
     *            the telephone type request
     * @return the response
     */
    Response<List<Customer>> retrieveDuplicateLeadList(final Request<PartyRoleInRelationship> customerRequest,
            final Transactions transactions, final Request<List<CustomerStatusCodeList>> statusRequest,
            final Request<List<TelephoneTypeCodeList>> telephoneTypeRequest);

    /**
     * Gets the customer count.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the customer count
     */
   Response<SearchCountResult> getCustomerCount(final Request<SearchCriteria> searchCriteriatRequest,final Request<List<CustomerStatusCodeList>> statusRequest);

    /**
     * Retrieve filter customers.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the response
     */
    /*Response<List<Customer>> retrieveFilterCustomers(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest);*/
    
    /**
     * Merge party role in agreement.
     * 
     * @param request
     *            the request
     * @return the response
     */
    void mergePartyRoleInAgreement(Request<PartyRoleInAgreement> request);
    
    Response<List<Customer>> retrieveFilterCustomers(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest);

}
