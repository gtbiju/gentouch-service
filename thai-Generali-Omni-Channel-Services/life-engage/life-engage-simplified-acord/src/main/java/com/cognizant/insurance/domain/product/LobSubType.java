/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Sep 20, 2017
 */

package com.cognizant.insurance.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class LobSubType.
 */
@Entity
@DiscriminatorValue("Lob_Subcode")
public class LobSubType extends Type {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7425252243905330696L;
	
	/**
	 * Instantiates a new lob sub type.
	 */
	public LobSubType(){
		super();
	}
	
	
	/**
	 * Instantiates a new lob sub type.
	 *
	 * @param id the id
	 */
	public LobSubType(final String id){
		super(id);
	}
	
    /** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL }, mappedBy = "lineOfBusiness")
    private Set<ProductSpecification> productSpecifications;

    /**
     * Sets the product specifications.
     *
     * @param productSpecifications the productSpecifications to set
     */
    public void setProductSpecifications(Set<ProductSpecification> productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    /**
     * Gets the product specifications.
     *
     * @return the productSpecifications
     */
    public Set<ProductSpecification> getProductSpecifications() {
        return productSpecifications;
    }

}
