package com.cognizant.insurance.omnichannel.component;

import org.json.JSONArray;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

public interface GeneraliSPAJNoComponent {

	public String generateSPAJNo(JSONArray jsonTransactionArray,
			RequestInfo requestInfo) throws BusinessException;

	String isSpajExisting(JSONArray jsonTransactionArray,
			RequestInfo requestInfo) throws BusinessException;;
}
