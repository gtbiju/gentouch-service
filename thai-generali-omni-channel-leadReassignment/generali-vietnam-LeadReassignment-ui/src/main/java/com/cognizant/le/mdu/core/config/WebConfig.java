/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 298738
 * @version       : 0.1, Apr 11, 2013
 */
package com.cognizant.le.mdu.core.config;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
//import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.cognizant.generali.encryption.util.EncryptionUtil;
import com.cognizant.le.model.dto.PropertyFileSource;
import com.cognizant.le.util.Constants;
//import com.mchange.v2.c3p0.ComboPooledDataSource;

// TODO: Auto-generated Javadoc
/**
 * The Class class WebConfig.
 */
@Configuration
@EnableWebMvc
@PropertySource({ "classpath:/"+Constants.PROPERTY_FILE})
@ComponentScan(basePackages = { "com.cognizant.le" })
@EnableTransactionManagement
public class WebConfig extends WebMvcConfigurerAdapter {

   //** The environment. *//*
    /** The environment. */
   @Autowired
    private Environment environment;


    /**
     * Jdbc template.
     *
     * @param registry the registry
     * @return the jdbc template
     */


    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #addViewControllers(org.springframework.web.servlet.config.annotation. ViewControllerRegistry)
     */
    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/index.html").setViewName("index");
    }

    /**
     * View resolver.
     * 
     * @return the internal resource view resolver
     */
    @Bean
    public InternalResourceViewResolver viewResolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/jsp/");
        resolver.setSuffix(".jsp");
        resolver.setOrder(1);
        return resolver;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #addResourceHandlers(org.springframework.web.servlet.config.annotation. ResourceHandlerRegistry)
     */
    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/css/**").addResourceLocations("/css/");
        registry.addResourceHandler("/img/**").addResourceLocations("/img/");
        registry.addResourceHandler("/js/**").addResourceLocations("/js/");
        registry.addResourceHandler("/fonts/**").addResourceLocations("/fonts/");
        
        registry.addResourceHandler("/lib/**").addResourceLocations("/resources/lib/");
        registry.addResourceHandler("/controller/**").addResourceLocations("/resources/controller/");
        registry.addResourceHandler("/directive/**").addResourceLocations("/resources/directive/");
        registry.addResourceHandler("/template/**").addResourceLocations("/resources/template/");
        registry.addResourceHandler("/jsp/css/**").addResourceLocations("/resources/css/");
        registry.addResourceHandler("/jsp/images/**").addResourceLocations("/resources/images/");
        registry.addResourceHandler("/jsp/lib/**").addResourceLocations("/resources/lib/");
        registry.addResourceHandler("/jsp/controller/**").addResourceLocations("/resources/controller/");
        registry.addResourceHandler("/jsp/directive/**").addResourceLocations("/resources/directive/");
        registry.addResourceHandler("/jsp/template/**").addResourceLocations("/resources/template/");
    }
    
/*    *//**
     * Data source.
     * 
     * @return the data source
     * @throws PropertyVetoException
     *             the exception
     *//*
    @Bean(destroyMethod = "close")
    public DataSource dataSource() throws PropertyVetoException {

        final ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass(environment.getRequiredProperty("le.mdu.app.jdbc.driverClassName"));
        dataSource.setJdbcUrl(environment.getRequiredProperty("le.mdu.app.jdbc.url"));
        dataSource.setUser(environment.getRequiredProperty("le.mdu.app.jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("le.mdu.app.jdbc.password"));
        dataSource.setAcquireIncrement(5);
        dataSource.setIdleConnectionTestPeriod(60);
        dataSource.setMaxPoolSize(100);
        dataSource.setMaxStatements(50);
        dataSource.setMinPoolSize(10);
        return dataSource;
    }
    
    *//**
     * Jdbc template.
     * 
     * @return the jdbc template
     * @throws PropertyVetoException
     *             the property veto exception
     *//*
    @Bean
    public JdbcTemplate jdbcTemplate() throws PropertyVetoException {
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
        return jdbcTemplate;
    }*/

    @Bean
    public RestTemplate restTemplate(){
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }
    @Bean
    public PropertyFileSource propertySource() throws PropertyVetoException {

        final PropertyFileSource propertySource = new PropertyFileSource();
        //propertySource.setDefaultLanguage(environment.getRequiredProperty("DEFAULT_LANGUAGE"));
        //propertySource.setSharedFolderPath(environment.getRequiredProperty("SHARED_CONTENT_FOLDER"));
        propertySource.setAuthUlrl(environment.getRequiredProperty("generali.platform.security.authURL"));
        propertySource.setAuthRequestTemplate(environment.getRequiredProperty("generali.platform.security.authReqTemplate"));
        //propertySource.setUploadRequestTemplate(environment.getRequiredProperty("generali.platform.upload.uploadReqTemplate"));
        //propertySource.setUploadUlrl(environment.getRequiredProperty("generali.platform.upload.uploadURL"));
        propertySource.setAgentprofile(environment.getRequiredProperty("generali.platform.agentProfile.vendorURL"));
        propertySource.setLeadRetrieveUrl(environment.getRequiredProperty("generali.platform.lead.retrieveURL"));
        //propertySource.setLeadUpdateUrl(environment.getRequiredProperty("generali.platform.lead.updateURL"));
        propertySource.setLeadRetrieveRqUrl(environment.getRequiredProperty("generali.platform.lead.leadRetrieveRqUrl"));
        
        return propertySource;
    }
    
	/**
	 * Multipart resolver.
	 *
	 * @return the multipart resolver
	 */
	@Bean
	public MultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(5 * 1024 * 1024);
		return multipartResolver;
	}
	
	@Override
		public void configureMessageConverters(
				List<HttpMessageConverter<?>> converters) {
		converters.add(converter());
		}
	
	@Bean
	public EncryptionUtil encryptionUtil() {
		final EncryptionUtil encryptionUtil = new EncryptionUtil();
		return encryptionUtil;
	}
    @Bean
    MappingJacksonHttpMessageConverter converter() {
        MappingJacksonHttpMessageConverter converter = new MappingJacksonHttpMessageConverter();
        List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
        supportedMediaTypes.add(MediaType.TEXT_PLAIN);
        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
		converter.setSupportedMediaTypes(supportedMediaTypes);
        return converter;
    }
    
    
}
