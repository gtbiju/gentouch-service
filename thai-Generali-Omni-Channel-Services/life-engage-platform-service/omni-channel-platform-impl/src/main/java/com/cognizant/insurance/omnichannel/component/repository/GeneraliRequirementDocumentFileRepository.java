package com.cognizant.insurance.omnichannel.component.repository;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.component.repository.RequirementDocumentFileRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.omnichannel.component.GeneraliAgreementComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.utils.Base64Utils;
import com.cognizant.insurance.utils.eAppScriptlet;

public class GeneraliRequirementDocumentFileRepository extends RequirementDocumentFileRepository {

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant ILLUSTRATION. */
    private static final String ILLUSTRATION = "illustration";
    
    private static final String EMPTY_STRING = "";
    
    private static final String REQUIREMENTSFOLDER = "REQUIREMENTS";
    
    private static final String DELETEDSTATUS = "Deleted";

	private static final String SAVEDSTATUS = "Saved";
	private static final String RESUBMITTEDSTATUS =  "Resubmitted";
    
    @Value("${le.platform.service.fileUploadPath}")
	private String fileUploadPath;
    
    @Value("${le.platform.service.slash}")
	private String pathSeparator;

    /** The agreement component. */
    @Autowired
    private GeneraliAgreementComponent agreementComponent;
 
    
    
    /** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(GeneraliRequirementDocumentFileRepository.class);
    
    protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(String type) {

        List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();

        if (E_APP.equals(type)) {
            statusList.add(AgreementStatusCodeList.PendingSubmission);
            statusList.add(AgreementStatusCodeList.Submitted);
            statusList.add(AgreementStatusCodeList.Confirmed);
            statusList.add(AgreementStatusCodeList.Completed);
            statusList.add(AgreementStatusCodeList.Draft);
            statusList.add(AgreementStatusCodeList.PaymentDone);
            statusList.add(AgreementStatusCodeList.Final);
            statusList.add(AgreementStatusCodeList.Initial);
            statusList.add(AgreementStatusCodeList.InProgress);
            statusList.add(AgreementStatusCodeList.Proposed);
            statusList.add(AgreementStatusCodeList.Review);
          
        } else if (ILLUSTRATION.equals(type)) {
            statusList.add(AgreementStatusCodeList.Draft);
            statusList.add(AgreementStatusCodeList.Completed);
            statusList.add(AgreementStatusCodeList.Confirmed);

        }

        return statusList;

    }
    
   
	public Set<Requirement> getRequirementDocFilesForPDF(
			Transactions transaction, String transactionId) throws IOException {

		Set<Requirement> requirementSignature = new HashSet<Requirement>();
		String id=null;
        if (Constants.EAPP.equals(transaction.getType())) {
            id = transaction.getProposalNumber();
        } else if (Constants.ILLUSTRATION.equals(transaction.getType())) {
            id = transaction.getIllustrationId();
        }
		if (CollectionUtils.isNotEmpty(transaction.getProposal()
				.getRequirements())) {
			for (Requirement requirementInRequest : transaction.getProposal()
					.getRequirements()) {
				
				if (requirementInRequest.getRequirementType().toString()
						.equals(DocumentTypeCodeList.Signature.toString())) {

					List<AgreementDocument> documents = requirementInRequest
							.getRequirementDocuments();

					Set<Document> docContent = documents.get(0).getPages();
				
					for (Document doc : docContent) {
						//to handle the second time hit for signature
						if(doc.getBase64string()==null || doc.getBase64string()==""){
						addFileContent(doc, id);
						}
					}

					if (CollectionUtils.isEmpty(documents)) {
						LOGGER.error(Constants.DOCUMENT_DETAILS_MISSING);

					} else {
						if (CollectionUtils
								.isEmpty(documents.get(0).getPages())) {
							LOGGER.error(Constants.FILE_DETAILS_MISSING);

						}
					}
					requirementSignature.add(requirementInRequest);

				}
			}
		} else {

			LOGGER.error(Constants.REQUIREMENT_DETAILS_MISSING);
		}
		return requirementSignature;

	}
    @Override
    public void saveRequirementDocFile(Transactions transaction,
			String transactionId) throws BusinessException, IOException {
    	
    	LOGGER.info("Inside method: saveRequirementDocFile");
		ContextTypeCodeList contextTypeCodeList = null;
		Document docToBeSaved = null;
		Document pageToSave = null;
		// Requirement requirementToSave = null;
		if (CollectionUtils.isNotEmpty(transaction.getRequirements())) {
			for (Requirement requirementInRequest : transaction
					.getRequirements()) {
				// requirementToSave = requirementInRequest;-
				List<AgreementDocument> documents = requirementInRequest
						.getRequirementDocuments();
				if (CollectionUtils.isNotEmpty(documents)) {
					docToBeSaved = documents.get(0);
					if (null != docToBeSaved
							&& CollectionUtils.isNotEmpty(docToBeSaved
									.getPages())) {
						for (Document doc : docToBeSaved.getPages()) {
							pageToSave = doc;
							break;
						}
					}
					break;
				} else {
					throwBusinessException(true, Constants.FILE_DETAILS_MISSING);
				}
			}
		} else {
			throwBusinessException(true, Constants.REQUIREMENT_DETAILS_MISSING);
		}
		List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();
		String type = transaction.getType();
		String id = EMPTY_STRING;
		if (E_APP.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.EAPP;
			id = transaction.getProposalNumber();
			statusList = getAgreementStatusCodesFor(type);

		} else if (ILLUSTRATION.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.ILLUSTRATION;
			id = transaction.getIllustrationId();
			statusList = getAgreementStatusCodesFor(type);
		}

		// get the Agreement object with proposal number final

		final Request<Agreement> agreementRequest = new JPARequestImpl<Agreement>(
				getEntityManager(), contextTypeCodeList, transactionId);
		final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
		insuranceAgreement.setIdentifier(id);
		agreementRequest.setType(insuranceAgreement);

		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				getEntityManager(), contextTypeCodeList, transactionId);
		statusRequest.setType(statusList);
		insuranceAgreement.setRequirements(transaction.getRequirements());
		// final Response<Agreement> agreementsResponse =
		// agreementComponent.getInsuranceAgreement
		// ForReqDocUpdate(agreementRequest, statusRequest);
		final File tempDirectory = new File(fileUploadPath + id + pathSeparator
				+ REQUIREMENTSFOLDER);
		createDirectory(tempDirectory);
		if (pageToSave.getDocumentStatus().equals(SAVEDSTATUS)||pageToSave.getDocumentStatus().equals(RESUBMITTEDSTATUS)) {
			createOnePageInDocument(pageToSave, tempDirectory);
		} else if (pageToSave.getDocumentStatus().equals(DELETEDSTATUS)) {
			deletePageOfDocument(pageToSave, tempDirectory);
		}
		 Agreement agreementWithRequirement=agreementComponent.updateInsuranceAgreementWithRequirementDocUpdates(
				agreementRequest, statusRequest);
		 agreementRequest.setType(agreementWithRequirement);
		  agreementComponent.mergeAgreement(agreementRequest); 
		  
		
	}

    

}
