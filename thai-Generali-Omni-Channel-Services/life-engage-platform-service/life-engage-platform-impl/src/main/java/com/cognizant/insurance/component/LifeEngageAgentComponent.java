package com.cognizant.insurance.component;

import org.json.JSONArray;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * The Interface LifeEngageAgentComponent.
 */
public interface LifeEngageAgentComponent {

	/**
	 * Retrieve agent profile.
	 *
	 * @param requestInfo the request info
	 * @param jsonArray the json array
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String retrieveAgentProfileByCode(RequestInfo requestInfo, JSONArray jsonArray)
			throws BusinessException;

}
