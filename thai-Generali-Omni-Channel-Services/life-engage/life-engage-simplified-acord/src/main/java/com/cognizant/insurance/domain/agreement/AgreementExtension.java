package com.cognizant.insurance.domain.agreement;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;

/**
 * @author 444873
 *
 */
@Entity
public class AgreementExtension implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * policy number
     */
    private Long policyNumber;

    /**
     * Generali Illustration policy number
     */
    private String illustrationPolicyNumber;

    /*
     * 
     * Generali selected Language
     */

    private String language;

    /**
     * Generali Insurance Purpose Protection
     */
    private String insurancePurposeProtection;

    /**
     * Generali Insurance Purpose Investment
     */
    private String insurancePurposeInvestment;

    /**
     * Generali Insurance Purpose Education
     */
    private String insurancePurposeEducation;

    /**
     * Generali Insurance Purpose Pension Fund
     */
    private String insurancePurposePensionFund;

    /**
     * Generali Insurance Purpose Business
     */
    private String insurancePurposeBusiness;

    /**
     * Generali Insurance Purpose Other
     */
    private String insurancePurposeOther;

    /**
     * Generali Insurance Purpose Description
     */
    private String insurancePurposeDesc;
    
    
    /**
     * Generali previous policy status Details
     */
    private String remark;
    
    /**
     * Generali previous reason;
     */
    private String reason;
    
    private String lastVisitedUrl;
    
    private String prevPolicyInsuredName;
    
    /**
     * Generali places and dates 
     */
    private String spajSignDate;
    private String spajSignPlace;
    private String questionaireSignDate;
    private String questionaireSignPlace;
    private String agentSignDate;
    private String agentSignPlace;
    
    @Temporal(TemporalType.DATE)
    private Date spajDate;
    
    private String spajDeclarationName1;
    private String spajDeclarationName2;
    private String spajDeclarationName3;
    private String spajDeclarationName4;
    private String spajDeclaration;
    private String questionaireDeclaration;
    
    private String bpmDocId;
	private String bpmPendingInfo;
	private String bpmStatus;
	
	private String premiumPayerType ; 
	
	private String hasInsuredQuestions;
	private String hasAdditionalInsured1Questions;
	private String hasAdditionalInsured2Questions;
	private String hasAdditionalInsured3Questions;
	private String hasAdditionalInsured4Questions;
	private String hasBeneficialOwnerForm;
	private String isProposerForeigner;
	private String hasInsuranceEngagementLetter;
	private String hasMedicalQuestions;
	private String clientdeclarationAgreed;
	private String source;

	private String servicingAgent;
	
	private Long commissionRateOne;
	
	private String commissionAgent;
	
	private Long commissionRateTwo;
	
	private String partnerCode;

	
	//Added as part of CR to proceed with out saving eApp.
	private String confirmAgentSignature;
	
	private String confirmSignature;
	
	private String illustrationStatus;
		
	private String policyHolderConfirmed;
	
	private String mainInsuredConfirmed;
	
	private String agentConfirmed;
	
	private String additionalInsuredOneConfirmed; 
	
	private String additionalInsuredTwoConfirmed; 
	
	private String additionalInsuredThreeConfirmed;
	
	private String additionalInsuredFourConfirmed;
    
	private Boolean restrictMandatory;

	public String getClientdeclarationAgreed() {
		return clientdeclarationAgreed;
	}

	public void setClientdeclarationAgreed(String clientdeclarationAgreed) {
		this.clientdeclarationAgreed = clientdeclarationAgreed;
	}

	public String getSpajDeclaration() {
		return spajDeclaration;
	}

	public void setSpajDeclaration(String spajDeclaration) {
		this.spajDeclaration = spajDeclaration;
	}

	public String getQuestionaireDeclaration() {
		return questionaireDeclaration;
	}

	public void setQuestionaireDeclaration(String questionaireDeclaration) {
		this.questionaireDeclaration = questionaireDeclaration;
	}

	
	
    public String getBpmDocId() {
		return bpmDocId;
	}

	public void setBpmDocId(String bpmDocId) {
		this.bpmDocId = bpmDocId;
	}

	public String getBpmPendingInfo() {
		return bpmPendingInfo;
	}

	public void setBpmPendingInfo(String bpmPendingInfo) {
		this.bpmPendingInfo = bpmPendingInfo;
	}

	public String getBpmStatus() {
		return bpmStatus;
	}

	public void setBpmStatus(String bpmStatus) {
		this.bpmStatus = bpmStatus;
	}

	public Date getSpajDate() {
		return spajDate;
	}

	public void setSpajDate(Date spajDate) {
		this.spajDate = spajDate;
	}

	/**
     * Indicates the set of UserSelections made as part of a questionnaire(declaration). 
     */ 
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    private Set<UserSelection> selectedOptions;
    

    public String getInsurancePurposeProtection() {
        return insurancePurposeProtection;
    }

    public void setInsurancePurposeProtection(String insurancePurposeProtection) {
        this.insurancePurposeProtection = insurancePurposeProtection;
    }

    public String getInsurancePurposeInvestment() {
        return insurancePurposeInvestment;
    }

    public void setInsurancePurposeInvestment(String insurancePurposeInvestment) {
        this.insurancePurposeInvestment = insurancePurposeInvestment;
    }

    public String getInsurancePurposeEducation() {
        return insurancePurposeEducation;
    }

    public void setInsurancePurposeEducation(String insurancePurposeEducation) {
        this.insurancePurposeEducation = insurancePurposeEducation;
    }

    public String getInsurancePurposePensionFund() {
        return insurancePurposePensionFund;
    }

    public void setInsurancePurposePensionFund(String insurancePurposePensionFund) {
        this.insurancePurposePensionFund = insurancePurposePensionFund;
    }

    public String getInsurancePurposeBusiness() {
        return insurancePurposeBusiness;
    }

    public void setInsurancePurposeBusiness(String insurancePurposeBusiness) {
        this.insurancePurposeBusiness = insurancePurposeBusiness;
    }

    public String getInsurancePurposeOther() {
        return insurancePurposeOther;
    }

    public void setInsurancePurposeOther(String insurancePurposeOther) {
        this.insurancePurposeOther = insurancePurposeOther;
    }

    public String getInsurancePurposeDesc() {
        return insurancePurposeDesc;
    }

    public void setInsurancePurposeDesc(String insurancePurposeDesc) {
        this.insurancePurposeDesc = insurancePurposeDesc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(Long policyNumber) {
        this.policyNumber = policyNumber;
    }

    public void setIllustrationPolicyNumber(String illustrationPolicyNumber) {
        this.illustrationPolicyNumber = illustrationPolicyNumber;
    }

    public String getIllustrationPolicyNumber() {
        return illustrationPolicyNumber;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

	public Set<UserSelection> getSelectedOptions() {
		return selectedOptions;
	}

	public void setSelectedOptions(Set<UserSelection> selectedOptions) {
		this.selectedOptions = selectedOptions;
	}

	public String getLastVisitedUrl() {
		return lastVisitedUrl;
	}

	public void setLastVisitedUrl(String lastVisitedUrl) {
		this.lastVisitedUrl = lastVisitedUrl;
	}

	public String getPrevPolicyInsuredName() {
		return prevPolicyInsuredName;
	}

	public void setPrevPolicyInsuredName(String prevPolicyInsuredName) {
		this.prevPolicyInsuredName = prevPolicyInsuredName;
	}

	public String getSpajSignDate() {
		return spajSignDate;
	}

	public void setSpajSignDate(String spajSignDate) {
		this.spajSignDate = spajSignDate;
	}

	public String getSpajSignPlace() {
		return spajSignPlace;
	}

	public void setSpajSignPlace(String spajSignPlace) {
		this.spajSignPlace = spajSignPlace;
	}

	public String getQuestionaireSignDate() {
		return questionaireSignDate;
	}

	public void setQuestionaireSignDate(String questionaireSignDate) {
		this.questionaireSignDate = questionaireSignDate;
	}

	public String getQuestionaireSignPlace() {
		return questionaireSignPlace;
	}

	public void setQuestionaireSignPlace(String questionaireSignPlace) {
		this.questionaireSignPlace = questionaireSignPlace;
	}

	public String getAgentSignDate() {
		return agentSignDate;
	}

	public void setAgentSignDate(String agentSignDate) {
		this.agentSignDate = agentSignDate;
	}

	public String getAgentSignPlace() {
		return agentSignPlace;
	}

	public void setAgentSignPlace(String agentSignPlace) {
		this.agentSignPlace = agentSignPlace;
	}

	public String getSpajDeclarationName1() {
		return spajDeclarationName1;
	}

	public void setSpajDeclarationName1(String spajDeclarationName1) {
		this.spajDeclarationName1 = spajDeclarationName1;
	}

	public String getSpajDeclarationName2() {
		return spajDeclarationName2;
	}

	public void setSpajDeclarationName2(String spajDeclarationName2) {
		this.spajDeclarationName2 = spajDeclarationName2;
	}

	public String getSpajDeclarationName3() {
		return spajDeclarationName3;
	}

	public void setSpajDeclarationName3(String spajDeclarationName3) {
		this.spajDeclarationName3 = spajDeclarationName3;
	}

	public String getSpajDeclarationName4() {
		return spajDeclarationName4;
	}

	public void setSpajDeclarationName4(String spajDeclarationName4) {
		this.spajDeclarationName4 = spajDeclarationName4;
	}


	public String getPremiumPayerType() {
		return premiumPayerType;
	}

	public void setPremiumPayerType(String premiumPayerType) {
		this.premiumPayerType = premiumPayerType;
	}

	public String getHasMedicalQuestions() {
		return hasMedicalQuestions;
	}

	public void setHasMedicalQuestions(String hasMedicalQuestions) {
		this.hasMedicalQuestions = hasMedicalQuestions;
	}

	public String getHasInsuredQuestions() {
		return hasInsuredQuestions;
	}

	public void setHasInsuredQuestions(String hasInsuredQuestions) {
		this.hasInsuredQuestions = hasInsuredQuestions;
	}

	public String getHasAdditionalInsured1Questions() {
		return hasAdditionalInsured1Questions;
	}

	public void setHasAdditionalInsured1Questions(
			String hasAdditionalInsured1Questions) {
		this.hasAdditionalInsured1Questions = hasAdditionalInsured1Questions;
	}

	public String getHasAdditionalInsured2Questions() {
		return hasAdditionalInsured2Questions;
	}

	public void setHasAdditionalInsured2Questions(
			String hasAdditionalInsured2Questions) {
		this.hasAdditionalInsured2Questions = hasAdditionalInsured2Questions;
	}

	public String getHasAdditionalInsured3Questions() {
		return hasAdditionalInsured3Questions;
	}

	public void setHasAdditionalInsured3Questions(
			String hasAdditionalInsured3Questions) {
		this.hasAdditionalInsured3Questions = hasAdditionalInsured3Questions;
	}

	public String getHasAdditionalInsured4Questions() {
		return hasAdditionalInsured4Questions;
	}

	public void setHasAdditionalInsured4Questions(
			String hasAdditionalInsured4Questions) {
		this.hasAdditionalInsured4Questions = hasAdditionalInsured4Questions;
	}

	public String getHasBeneficialOwnerForm() {
		return hasBeneficialOwnerForm;
	}

	public void setHasBeneficialOwnerForm(String hasBeneficialOwnerForm) {
		this.hasBeneficialOwnerForm = hasBeneficialOwnerForm;
	}

	public String getIsProposerForeigner() {
		return isProposerForeigner;
	}

	public void setIsProposerForeigner(String isProposerForeigner) {
		this.isProposerForeigner = isProposerForeigner;
	}

	public String getHasInsuranceEngagementLetter() {
		return hasInsuranceEngagementLetter;
	}

	public void setHasInsuranceEngagementLetter(String hasInsuranceEngagementLetter) {
		this.hasInsuranceEngagementLetter = hasInsuranceEngagementLetter;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @param servicingAgent the servicingAgent to set
	 */
	public void setServicingAgent(String servicingAgent) {
		this.servicingAgent = servicingAgent;
	}

	/**
	 * @return the servicingAgent
	 */
	public String getServicingAgent() {
		return servicingAgent;
	}

	/**
	 * @param commissionRateOne the commissionRateOne to set
	 */
	public void setCommissionRateOne(Long commissionRateOne) {
		this.commissionRateOne = commissionRateOne;
	}

	/**
	 * @return the commissionRateOne
	 */
	public Long getCommissionRateOne() {
		return commissionRateOne;
	}

	/**
	 * @param commissionAgent the commissionAgent to set
	 */
	public void setCommissionAgent(String commissionAgent) {
		this.commissionAgent = commissionAgent;
	}

	/**
	 * @return the commissionAgent
	 */
	public String getCommissionAgent() {
		return commissionAgent;
	}

	/**
	 * @param commissionRateTwo the commissionRateTwo to set
	 */
	public void setCommissionRateTwo(Long commissionRateTwo) {
		this.commissionRateTwo = commissionRateTwo;
	}

	/**
	 * @return the commissionRateTwo
	 */
	public Long getCommissionRateTwo() {
		return commissionRateTwo;
	}

	/**
	 * @param partnerCode the partnerCode to set
	 */
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	/**
	 * @return the partnerCode
	 */
	public String getPartnerCode() {
		return partnerCode;
	}

	/**
	 * @param confirmAgentSignature the confirmAgentSignature to set
	 */
	public void setConfirmAgentSignature(String confirmAgentSignature) {
		this.confirmAgentSignature = confirmAgentSignature;
	}

	/**
	 * @return the confirmAgentSignature
	 */
	public String getConfirmAgentSignature() {
		return confirmAgentSignature;
	}
	
	 /**
	 * @return the illustrationStatus
	 */
	public String getIllustrationStatus() {
		return illustrationStatus;
	}

	/**
	 * @param illustrationStatus the illustrationStatus to set
	 */
	public void setIllustrationStatus(String illustrationStatus) {
		this.illustrationStatus = illustrationStatus;
	}

	/**
	 * @return the confirmSignature
	 */
	public String getConfirmSignature() {
		return confirmSignature;
	}

	/**
	 * @param confirmSignature the confirmSignature to set
	 */
	public void setConfirmSignature(String confirmSignature) {
		this.confirmSignature = confirmSignature;
	}

	/**
	 * @return the policyHolderConfirmed
	 */
	public String getPolicyHolderConfirmed() {
		return policyHolderConfirmed;
	}

	/**
	 * @param policyHolderConfirmed the policyHolderConfirmed to set
	 */
	public void setPolicyHolderConfirmed(String policyHolderConfirmed) {
		this.policyHolderConfirmed = policyHolderConfirmed;
	}

	/**
	 * @return the mainInsuredConfirmed
	 */
	public String getMainInsuredConfirmed() {
		return mainInsuredConfirmed;
	}

	/**
	 * @param mainInsuredConfirmed the mainInsuredConfirmed to set
	 */
	public void setMainInsuredConfirmed(String mainInsuredConfirmed) {
		this.mainInsuredConfirmed = mainInsuredConfirmed;
	}

	/**
	 * @return the agentConfirmed
	 */
	public String getAgentConfirmed() {
		return agentConfirmed;
	}

	/**
	 * @param agentConfirmed the agentConfirmed to set
	 */
	public void setAgentConfirmed(String agentConfirmed) {
		this.agentConfirmed = agentConfirmed;
	}

	/**
	 * @return the additionalInsuredOneConfirmed
	 */
	public String getAdditionalInsuredOneConfirmed() {
		return additionalInsuredOneConfirmed;
	}

	/**
	 * @param additionalInsuredOneConfirmed the additionalInsuredOneConfirmed to set
	 */
	public void setAdditionalInsuredOneConfirmed(
			String additionalInsuredOneConfirmed) {
		this.additionalInsuredOneConfirmed = additionalInsuredOneConfirmed;
	}

	/**
	 * @return the additionalInsuredTwoConfirmed
	 */
	public String getAdditionalInsuredTwoConfirmed() {
		return additionalInsuredTwoConfirmed;
	}

	/**
	 * @param additionalInsuredTwoConfirmed the additionalInsuredTwoConfirmed to set
	 */
	public void setAdditionalInsuredTwoConfirmed(
			String additionalInsuredTwoConfirmed) {
		this.additionalInsuredTwoConfirmed = additionalInsuredTwoConfirmed;
	}

	/**
	 * @return the additionalInsuredThreeConfirmed
	 */
	public String getAdditionalInsuredThreeConfirmed() {
		return additionalInsuredThreeConfirmed;
	}

	/**
	 * @param additionalInsuredThreeConfirmed the additionalInsuredThreeConfirmed to set
	 */
	public void setAdditionalInsuredThreeConfirmed(
			String additionalInsuredThreeConfirmed) {
		this.additionalInsuredThreeConfirmed = additionalInsuredThreeConfirmed;
	}

	/**
	 * @return the additionalInsuredFourConfirmed
	 */
	public String getAdditionalInsuredFourConfirmed() {
		return additionalInsuredFourConfirmed;
	}

	/**
	 * @param additionalInsuredFourConfirmed the additionalInsuredFourConfirmed to set
	 */
	public void setAdditionalInsuredFourConfirmed(
			String additionalInsuredFourConfirmed) {
		this.additionalInsuredFourConfirmed = additionalInsuredFourConfirmed;
	}

	public Boolean getRestrictMandatory() {
		return restrictMandatory;
	}

	public void setRestrictMandatory(Boolean restrictMandatory) {
		this.restrictMandatory = restrictMandatory;
	}
}
