package com.cognizant.insurance.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.service.GeneraliSalesActivityService;

/**
 * Class GeneraliSalesActivityEndPoint - SalesActivity End Point
 * 
 * @author 481774
 * 
 */
@Controller
@RequestMapping("/generaliSalesActivityService")
public class GeneraliSalesActivityEndPoint {

    @Autowired
    private GeneraliSalesActivityService generaliSalesActivityService;

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliSalesActivityEndPoint.class);

    /**
     * Method to get Sales Activity Details
     * 
     * @param json
     * @return json
     */

    @RequestMapping(value = "/retrieveSalesActivityDetails", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String retrieveSalesActivityDetails(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return generaliSalesActivityService.retrieveSalesActivityDetails(json);
            }
        }.execute(json);
    }
}
