/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partyname;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;

/**
 * This abstract concept includes name details common to persons and
 * organizations.
 * 
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="PARTY_NAME_TYPE", discriminatorType=DiscriminatorType.STRING)
public abstract class PartyName extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 217078358065955133L;

    /**
     * The time period associated with a party name.
     * 
     * e.g. The given name for a person has an end date in the event of
     * marriage.
     * 
     * e.g. The organization name "Lloyd's of London" changed to "Lloyd's". <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private TimePeriod effectivePeriod;

    /**
     * The textual description of party name. 
     * 
     * 
     * 
     */
    private String description;

    /**
     * A code representing the language in which the party name is expressed.
     * This is particularly useful for multilingual countries where persons or
     * companies can have multiple names, one in each language. (see ISO 639).
     * 
     * REFERENCE:
     * http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail
     * .htm?csnumber=22109
     * 
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY )
    private ExternalCode languageCode;

    /**
     * The name of the party. This is an unstructured name for a person or
     * organization.
     * 
     * With regards to a person's name, this can be utilized as the complete
     * aggregated name used for identifying a person when the components of the
     * name are unavailable or inapplicable. This may be viewed as a derived
     * name, derived as a concatenation of the various elements that comprise
     * the entire name or portions thereof. e.g: Dr. Mary Jane Wilson, PhD; Dr.
     * Mary Wilson.
     * 
     * 
     * 
     * 
     * 
     */
    private String fullName;

    /**
     * Indicates this is the default party name.
     * 
     * e.g. If the party has only one name and it is designated as the "default"
     * name, that name applies for all scenarios (party and related party
     * roles).
     * 
     * 
     * 
     */
    private boolean defaultIndicator;

    /**
     * Gets the effectivePeriod.
     * 
     * @return Returns the effectivePeriod.
     */
    public TimePeriod getEffectivePeriod() {
        return effectivePeriod;
    }

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the languageCode.
     * 
     * @return Returns the languageCode.
     */
    public ExternalCode getLanguageCode() {
        return languageCode;
    }

    /**
     * Gets the fullName.
     * 
     * @return Returns the fullName.
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Gets the defaultIndicator.
     * 
     * @return Returns the defaultIndicator.
     */
    public boolean getDefaultIndicator() {
        return defaultIndicator;
    }

    /**
     * Sets The effectivePeriod.
     * 
     * @param effectivePeriod
     *            The effectivePeriod to set.
     */
    public void setEffectivePeriod(final TimePeriod effectivePeriod) {
        this.effectivePeriod = effectivePeriod;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The languageCode.
     * 
     * @param languageCode
     *            The languageCode to set.
     */
    public void setLanguageCode(final ExternalCode languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * Sets The fullName.
     * 
     * @param fullName
     *            The fullName to set.
     */
    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    /**
     * Sets The defaultIndicator.
     * 
     * @param defaultIndicator
     *            The defaultIndicator to set.
     */
    public void setDefaultIndicator(final boolean defaultIndicator) {
        this.defaultIndicator = defaultIndicator;
    }

}
