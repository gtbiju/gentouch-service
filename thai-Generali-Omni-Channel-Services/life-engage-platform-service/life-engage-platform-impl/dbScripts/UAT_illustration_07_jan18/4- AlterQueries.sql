
ALTER TABLE CODE_LOOKUP ALTER COLUMN DESCRIPTION nvarchar(600)

ALTER TABLE CODE_LOOKUP ALTER COLUMN CODE nvarchar(600)

ALTER TABLE CODE_LOCALE_LOOKUP ALTER COLUMN VALUE nvarchar(600)

ALTER TABLE PartyName ALTER COLUMN title nvarchar(200)

ALTER TABLE PersonDetail ALTER COLUMN industry nvarchar(300)

ALTER TABLE PersonDetail ALTER COLUMN description nvarchar(300)

ALTER TABLE PersonDetail ALTER COLUMN job nvarchar(300)

ALTER TABLE PersonDetail ALTER COLUMN jobValue nvarchar(300)

ALTER TABLE PersonDetail ALTER COLUMN subJob nvarchar(300)

ALTER TABLE PersonDetail ALTER COLUMN subJobValue nvarchar(300)

ALTER TABLE PersonDetail ALTER COLUMN occupationCategoryValue nvarchar(300)

ALTER TABLE PersonDetail ALTER COLUMN occupationCategory nvarchar(300)

ALTER TABLE PersonDetail ALTER COLUMN descriptionValue nvarchar(300)

ALTER TABLE PersonDetail ALTER COLUMN industryValue nvarchar(300)

ALTER TABLE LifeEngageEmail ALTER COLUMN emailValues nvarchar(max)


ALTER TABLE PartyName ALTER COLUMN fullName nvarchar(300)

ALTER TABLE TransactionKeys ALTER COLUMN key3 nvarchar(300) 

ALTER TABLE TransactionKeys ALTER COLUMN key19 nvarchar(300) 

ALTER TABLE LifeEngageAudit ALTER COLUMN requestJson nvarchar(max)

ALTER TABLE RULE_TABLE ALTER COLUMN RULEJSON nvarchar(max)

ALTER TABLE RULE_TABLE ALTER COLUMN RULEDESC nvarchar(max)

ALTER TABLE IllustrationResponsePayLoad ALTER COLUMN responseJson nvarchar(max)

ALTER TABLE TransactionKeys ALTER COLUMN key38 nvarchar(300) 

ALTER TABLE PostalAddressContact ALTER COLUMN buildingName nvarchar(300) 

ALTER TABLE PostalAddressContact ALTER COLUMN unitNumber nvarchar(300) 

ALTER TABLE PostalAddressContact ALTER COLUMN streetNumber nvarchar(300) 

ALTER TABLE AgreementExtension ALTER COLUMN spajDeclarationName1 nvarchar(300) 

ALTER TABLE AgreementExtension ALTER COLUMN spajDeclarationName2 nvarchar(300) 

ALTER TABLE AgreementExtension ALTER COLUMN spajDeclarationName3 nvarchar(300) 


ALTER TABLE CommunicationContent ALTER COLUMN description nvarchar(300) 

ALTER TABLE CommunicationContent ALTER COLUMN name nvarchar(300) 


ALTER TABLE UserSelection ALTER COLUMN detailedInfo nvarchar(500)  

 ALTER TABLE Account  ALTER COLUMN branchName nvarchar(300)
 ALTER TABLE Account  ALTER COLUMN bankName nvarchar(300)

 ALTER TABLE Coverage  ALTER COLUMN planName nvarchar(300)
 
ALTER TABLE APPIAN_MEMO_DETAILS ALTER COLUMN details nvarchar(max)