/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291430
 * @version       : 0.1, Dec 30, 2013
 */
package com.cognizant.icr.pdf.util;

import java.beans.PropertyVetoException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.icr.pdf.exception.SystemException;
import com.cognizant.icr.pdf.repository.TemplateRepository;
import com.cognizant.icr.pdf.repository.impl.FileRepositoryImpl;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

/**
 * The Class Utility.
 */
public class DataSourceFactory implements ConnectionFactory{
    /** The user. */
    private String user;

    /** The password. */
    private String password;

    /** The database name. */
    private String driverClass;

    /** The server name. */
    private String url;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceFactory.class);

    /**
     * Gets the implementation.
     * 
     * @param repoType
     *            the repo type
     * @return the implementation
     */
    public static TemplateRepository getImplementation(final String repoType) {
        LOGGER.debug("Method Entry : Utility-> getImplementation");
        if (repoType.equals("FILE")) {
            return new FileRepositoryImpl();
        }
        LOGGER.debug("Method Exit : Utility-> getImplementation");
        return null;
    }

    /**
     * Generate datasource.
     *
     * @return the mysql data source
     */
    public final DataSource createConnection() {
        user = PropertyFileReader.fetchProperty(Constants.RE_USER, Constants.CONFIG_PROPERTY_FILE);
        password = PropertyFileReader.fetchProperty(Constants.RE_PASSWORD, Constants.CONFIG_PROPERTY_FILE);
        url = PropertyFileReader.fetchProperty(Constants.RE_URL, Constants.CONFIG_PROPERTY_FILE);
        driverClass = PropertyFileReader.fetchProperty(Constants.RE_DRIVER_CLASS, Constants.CONFIG_PROPERTY_FILE);
        ComboPooledDataSource dataSoucre = new ComboPooledDataSource();
        
        try {
			dataSoucre.setDriverClass(driverClass);
		} catch (PropertyVetoException e) {
			LOGGER.debug("Failed to create datasource", e);
			throw new SystemException("Failed to create datasource", e);
		}
        dataSoucre.setJdbcUrl(url);
        dataSoucre.setUser(user);
        dataSoucre.setPassword(password);
        dataSoucre.setAcquireIncrement(5);
        dataSoucre.setIdleConnectionTestPeriod(60);
        dataSoucre.setMaxPoolSize(100);
        dataSoucre.setMaxStatements(50);
        dataSoucre.setMinPoolSize(10);
        return dataSoucre;
    }

}
