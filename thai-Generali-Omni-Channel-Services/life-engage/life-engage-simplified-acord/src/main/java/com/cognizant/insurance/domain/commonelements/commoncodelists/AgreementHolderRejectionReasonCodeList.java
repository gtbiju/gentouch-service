/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of Policyholders according to their rejection
 * reason.
 * 
 * @author 301350
 * 
 * 
 */
public enum AgreementHolderRejectionReasonCodeList {
    /**
     * Identifies a Policyholder with rejection reason 'Decided for different
     * financial services agreement'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    DifferentAgreement,
    /**
     * Identifies a Policyholder with rejection reason 'Decided for different
     * financial services provider (insurer)'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    DifferentInsurer,
    /**
     * 
     * 
     * 
     * 
     */
    Unspecified
}
