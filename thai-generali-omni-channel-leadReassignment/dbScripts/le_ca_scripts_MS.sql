

INSERT INTO AGENT_APP_LOGIN VALUES ('admin','21232f297a57a5a743894a0e4a801fc3','active',NULL),('admin1','*4ACFE3202A5FF5CF467','active',NULL),('admin2','*4ACFE3202A5FF5CF467898FC58AAB1D615029441','active',NULL);



INSERT INTO APPLICATION_CONTEXTS VALUES (1,'common'),(2,'msales'),(3,'m-app'),(4,'check-app'),(5,'generali');


INSERT INTO CONTENT_TYPES (id,type,priority,extensions) VALUES (1,'PDF',3,'pdf'),(2,'Image',2,'jpg,jpeg,png'),(3,'Offline DB',1,'db'),(4,'Validation Rule',1,'js'),(5,'Illustration Rule',1,'js'),(6,'Video',4,'avi,wmv,mpg,mpeg,mkv,3gp,mp4'),(7,'Config Files',1,'properties,json'),(8,'HTML',1,'html,htm'),(9,'dummy',5,'xls');


INSERT INTO CONTENT_LANGUAGES (id, name) VALUES('1', 'English'), ('2', 'Thai'), ('3', 'Malayalam'), ('4', 'Hindi'),('5', 'Tamil'), ('6', 'Telugu'), ('7', 'Kannada'), ('8', 'Marathi');

update  CONTENT_TYPES set optionality=0, previewable=0