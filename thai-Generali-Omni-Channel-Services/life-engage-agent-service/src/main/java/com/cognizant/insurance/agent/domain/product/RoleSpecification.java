/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.party.Party;
import com.cognizant.insurance.agent.domain.roleandrelationship.PartyRole;

/**
 * The Class RoleSpecification.
 *
 * @author 304007
 */

public class RoleSpecification extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7811939962231008780L;
    
    private String name;
    
    /** The version. */
    private String version;

    /** The role player id. */
    private Party rolePlayer;
    
    /** 
     * This relationship links a party role to a specification in order to
     * identify the designer of the specification.
     * 
     * The role kind. 
     */
    private PartyRole roleKind;
    
    /** The role player type. */
    private RolePlayerType rolePlayerType;
    
    /** The parent product. */
    private ProductSpecification parentProduct;

    /** The role spec type. */
    private RoleSpecType roleSpecType;
    /**
     * The Constructor.
     */
    public RoleSpecification() {
        super();
    }

    /**
     * Gets the role kind.
     *
     * @return the role kind
     */
    public PartyRole getRoleKind() {
        return roleKind;
    }

    /**
     * Sets the role kind.
     *
     * @param roleKind the role kind
     */
    public void setRoleKind(final PartyRole roleKind) {
        this.roleKind = roleKind;
    }

    /**
     * Gets the role player type.
     *
     * @return the role player type
     */
    public RolePlayerType getRolePlayerType() {
        return rolePlayerType;
    }

    /**
     * Sets the role player type.
     *
     * @param rolePlayerType the role player type
     */
    public void setRolePlayerType(final RolePlayerType rolePlayerType) {
        this.rolePlayerType = rolePlayerType;
    }


    /**
     * Gets the rolePlayer.
     *
     * @return Returns the rolePlayer.
     */
    public Party getRolePlayer() {
        return rolePlayer;
    }


    /**
     * Sets The rolePlayer.
     *
     * @param rolePlayer The rolePlayer to set.
     */
    public void setRolePlayer(final Party rolePlayer) {
        this.rolePlayer = rolePlayer;
    }

    /**
     * Gets the parentProduct.
     *
     * @return Returns the parentProduct.
     */
    public ProductSpecification getParentProduct() {
        return parentProduct;
    }

    /**
     * Sets The parentProduct.
     *
     * @param parentProduct The parentProduct to set.
     */
    public void setParentProduct(final ProductSpecification parentProduct) {
        this.parentProduct = parentProduct;
    }

    /**
     * Gets the name.
     *
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The name.
     *
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the version.
     *
     * @return Returns the version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets The version.
     *
     * @param version The version to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * Sets The roleSpecType.
     *
     * @param roleSpecType The roleSpecType to set.
     */
    public void setRoleSpecType(final RoleSpecType roleSpecType) {
        this.roleSpecType = roleSpecType;
    }

    /**
     * Gets the roleSpecType.
     *
     * @return Returns the roleSpecType.
     */
    public RoleSpecType getRoleSpecType() {
        return roleSpecType;
    }
}
