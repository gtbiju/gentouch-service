/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Sep 20, 2017
 */

package com.cognizant.insurance.domain.product;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

public class TypeIdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SessionImplementor session, Object arg1)
			throws HibernateException {
		Connection connection = session.connection();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement
					.executeQuery("select count(*)  from CORE_PROD_CODE_TYPE_LOOK_UP");

			if (rs.next()) {
				int id = rs.getInt(1) + 100000;
				String generatedId =  new Integer(id).toString();
				return generatedId;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
