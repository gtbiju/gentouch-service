/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 29, 2013
 */
package com.cognizant.insurance.agent.component;

import com.cognizant.insurance.agent.audit.LifeEngageAudit;
import com.cognizant.insurance.agent.request.vo.RequestInfo;

/**
 * The Interface interface LifeEngageAuditComponent.
 * 
 * @author 300797
 */
public interface LifeEngageAuditComponent {

    /**
     * Savelife engage audit.
     * 
     * @param requestInfo
     *            the request info
     * @param json
     *            the json
     * @return the life engage audit
     */
    LifeEngageAudit savelifeEngageAudit(RequestInfo requestInfo, String json);
    
    /* Added for LE_datawipe functionality */
    /**
     * Save life engage data wipe audit.
     * 
     * @param json
     *            the json
     * @param requestInfo
     *            the request info
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     */
    String saveLifeEngageDataWipeAudit(String json, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit);
    /* Added for LE_datawipe functionality */
}
