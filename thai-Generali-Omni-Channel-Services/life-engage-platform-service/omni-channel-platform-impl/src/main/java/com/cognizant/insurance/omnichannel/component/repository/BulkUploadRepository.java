package com.cognizant.insurance.omnichannel.component.repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.domain.codes.CodeLocaleLookUp;
import com.cognizant.insurance.domain.codes.CodeLookUp;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.vo.BulkUploadData;
import com.cognizant.insurance.service.repository.CodeLookUpRepository;

/**
 * Class BulkUploadRepository
 * @author 390229
 *
 */
@Component
public class BulkUploadRepository {
	@Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;
	
	/** The look up repository. */
    @Autowired
    private CodeLookUpRepository codeLookUpRepository;
	
	public static final Logger LOGGER = LoggerFactory
	.getLogger(BulkUploadRepository.class);
	
	/**
	 * Method process data before save
	 * @param bulkUploadData
	 * @param lookUpData
	 */
	public  void  processData( BulkUploadData bulkUploadData,Map<String,Map<String,String>>lookUpData) {
        String nextMeetingDateAndTime = "";
        String nextMeetingDate;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            
                String meetingTime = bulkUploadData.getNextMeetingTime();
                String occupation=bulkUploadData.getOccupation();
                String state=bulkUploadData.getStateCode();
               
                String nationality=bulkUploadData.getNationality();
                String income=bulkUploadData.getAnnualIncome();
                String need=bulkUploadData.getNeed();
               
                Map<String,String>occupationMap=lookUpData.get("OCCUPATION_LIST");
                Map<String,String>nationalityMap=lookUpData.get("NATIONALITY");
                Map<String,String>incomeMap=lookUpData.get("INCOME");
                Map<String,String>needMap=lookUpData.get("NEED");
                String agentCode=bulkUploadData.getAgentCode();
                String agentCodeMod=agentCode;
                
                Date meetingDate = bulkUploadData.getNextmeetingDate();
                if (meetingDate != null) {
                    nextMeetingDate = sdf.format(meetingDate);
                    nextMeetingDateAndTime = nextMeetingDate.concat(" ").concat(meetingTime);
                } else {
                	//Bug fix 87 -UAT
                    nextMeetingDateAndTime ="";
                }
                if(state!=null&&!GeneraliConstants.JSON_EMPTY.equals(state)){
                	
                	bulkUploadData.setStateValue(state);
                	
                }
               
                if(occupation!=null&&!GeneraliConstants.JSON_EMPTY.equals(occupation)){
                	occupation=occupation+" ";
                	bulkUploadData.setOccupationValue(getFromMap(occupation,occupationMap));
                	
                }
                if(nationality!=null&&!GeneraliConstants.JSON_EMPTY.equals(nationality)){
                	
                	bulkUploadData.setNationalityValue(getFromMap(nationality,nationalityMap));
                	//bulkUploadData.setNationalityValue(nationality);
                }
                if(income!=null&&!GeneraliConstants.JSON_EMPTY.equals(income)){
                	income=income+" ";
                	bulkUploadData.setIncomeValue(getFromMap(income,incomeMap));
                }
                if(need!=null&&!GeneraliConstants.JSON_EMPTY.equals(need)){
                	need=need+" ";
                	bulkUploadData.setNeedValue(getFromMap(need,needMap));
                }
                if(agentCode!=null && agentCode.contains("\n")){
                	agentCodeMod=agentCode.replaceAll("\n", GeneraliConstants.JSON_EMPTY);
                	
                }
                bulkUploadData.setAgentModified(agentCodeMod);
                Date current= new Date();
                bulkUploadData.setCurrentDate(current);
                bulkUploadData.setMeetingDateAndTime(nextMeetingDateAndTime);
                bulkUploadData.setTransTrackingId(generateTranstrackingId(bulkUploadData.getAgentCode()));
            
        } catch (NullPointerException e) {
            LOGGER.error("Null pointer Exception" + e.getCause().toString(), e);
        }
        
    }
	/**
	 * Creates TranstrackingID
	 * @param agentcode
	 * @return
	 */
	public String generateTranstrackingId(String agentcode){
		String transTrackingId="";
		transTrackingId=GeneraliConstants.BULKUPLOAD+"-"+GeneraliConstants.TYPE_AGENCY+"-"+agentcode+String.valueOf(idGenerator.generate());
		return transTrackingId;
		
		
	}

	/**
	 * Get lookup values from Code lookup table
	 * @param type
	 * @param language
	 * @param Country
	 * @return
	 */
	public Map<String,String> getLookUpdata(String type,String language, String Country){
		Map<String, String> codeLocaleLookUpMap= new HashMap<String, String>();
		 List<CodeLookUp> typeLookUps=codeLookUpRepository.fetchLookUpData(type, language, Country);
		
		
		 for(CodeLookUp codeLookUp: typeLookUps){
			 List<CodeLocaleLookUp> codeLookups=new ArrayList<CodeLocaleLookUp>();
			 codeLookups.addAll(codeLookUp.getLocaleLookUps());
			
			 CodeLocaleLookUp CodeLocaleLookUp= new CodeLocaleLookUp();
			 CodeLocaleLookUp=codeLookups.get(0);
			 codeLocaleLookUpMap.put(CodeLocaleLookUp.getValue(),codeLookUp.getCode());
		 }
		return codeLocaleLookUpMap;
		
	}
	/**
	 * To read from map
	 */
	public String getFromMap(String key,Map<String,String>codeLookUpMap){
		String value=key;
		if(codeLookUpMap!=null && !codeLookUpMap.isEmpty()){
			if(codeLookUpMap.containsKey(key)){
				value=codeLookUpMap.get(key);
			}
		}
		return value;
		
	}

}
