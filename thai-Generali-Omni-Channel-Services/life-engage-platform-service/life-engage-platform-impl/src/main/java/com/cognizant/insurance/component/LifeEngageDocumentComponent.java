/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 20, 2014
 */
package com.cognizant.insurance.component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.request.vo.Transactions;

public interface LifeEngageDocumentComponent {
	
	/**
	 * Generate pdf_ get.
	 *
	 * @param proposalnumber the proposalnumber
	 * @param type the type
	 * @return the byte array output stream
	 */
	ByteArrayOutputStream generatePdfGet(String proposalnumber, String type,String templateId);
	
	/**
	 * Generate illustration pdf.
	 *
	 * @param proposalnumber the proposal number
	 * @param type the type
	 * @param productCode the product code
	 * @return the byte array output stream
	 */
	ByteArrayOutputStream generateIllustrationPdf(String proposalNumber, String type, String productCode);
	
	 /**
     * Gets the document.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param documentName
     *            the document name
     * @param transaction
     *            the transaction
     * @return the document
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
	AgreementDocument getDocument(final Transactions transaction, final String documentName,
			final String transactionId) throws IOException;
	 /**
     * Upload documents.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param transaction
     *            the transaction
     * @param document
     *            the document
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws BusinessException
     *             the business exception
     */
    void uploadDocuments(final Transactions transaction, final String transactionId,
            final AgreementDocument document) throws IOException, BusinessException ;
    /**
     * Generate fna report.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param type
     *            the type
     * @param templateId
     *            the template id
     * @param agentId
     *            the agent id
     * @return the byte array output stream
     */
    ByteArrayOutputStream generateFNAReport(String proposalNumber, String type, String templateId);
}
