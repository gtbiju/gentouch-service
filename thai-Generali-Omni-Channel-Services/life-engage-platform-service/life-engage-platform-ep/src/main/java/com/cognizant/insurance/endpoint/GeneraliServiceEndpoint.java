package com.cognizant.insurance.endpoint;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.service.GeneraliService;
import com.cognizant.insurance.omnichannel.service.GeneraliSyncService;
import com.cognizant.insurance.services.AgentProfileService;
import com.cognizant.insurance.services.LifeEngageSyncService;

/**
 * Class contains the end point service to generate the proposal numbers.
 *
 */
@Controller
@RequestMapping("/generaliService")
public class GeneraliServiceEndpoint {

	/** The agent profile service. */
	@Autowired
	GeneraliService generaliService;
	
	@Autowired
	AgentProfileService agentProfileService;
	
	@Autowired
	private LifeEngageSyncService lifeEngageSyncService;
	
	/**
	 * Retrieve agent.
	 * 
	 * @param json
	 *            the json
	 * @return the string
	 */
	@RequestMapping(value = "/generateSpaj", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
	public final String retrieveAgent(@RequestBody final String json) {
		return new ServiceTemplate() {
			@Override
			public String doExecute(final String json) throws BusinessException {
				return generaliService.generateSPAJNo(json);
			}
		}.execute(json);
	}

	/**
	 * Checking whether Spaj number already exists (RDS User)
	 * 
	 * @param json
	 *            the json
	 * @return the string
	 */
	@RequestMapping(value = "/isSpajExisting", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
	public final String isSpajExisting(@RequestBody final String json) {
		return new ServiceTemplate() {
			@Override
			public String doExecute(final String json) throws BusinessException {
				return generaliService.isSpajExisting(json);
			}
		}.execute(json);
	}
	
	/**
	 * The method will generate BPM PDF.One its done, It will trigger Email and
	 * BPM service
	 * 
	 * @param json
	 * @return json
	 */
	@RequestMapping(value = "/generateBPMEmailPdf", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
	public final String generateBPMEmailPdf(@RequestBody final String json) {

		return new ServiceTemplate() {
			@Override
			public String doExecute(final String json) throws BusinessException {
				return generaliService.generateBPMEmailPdf(json);
			}
		}.execute(json);
	}
	
	@RequestMapping(value = "/generateICMInfo/{proposalnumber}", method = RequestMethod.GET)

    public final void generateICMData(@PathVariable("proposalnumber") String proposalnumber) throws BusinessException, ParseException {
		generaliService.generateICMInfo(proposalnumber);

    }
	
	@RequestMapping(value = "/resubmit/{proposalnumber}", method = RequestMethod.GET)
	@ResponseBody
    public final String resubmit(@PathVariable("proposalnumber") String proposalnumber) throws BusinessException, ParseException {
		generaliService.resubmit(proposalnumber);
		return "{}";
    }
	
	@RequestMapping(value = "/isLASAvailable", method = RequestMethod.GET)
	@ResponseBody
    public final String isLASAvailable() throws BusinessException, ParseException {
		return generaliService.isLASAvailable();

    }
	
	@RequestMapping(value = "/validateAgentCode", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
    public final String validateAgentCode(@RequestBody final String json) throws BusinessException {
		return generaliService.validateAgentCode(json);

    }
	
	@RequestMapping(value = "/retrieveMemo", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
    public final String retrieveMemo(@RequestBody final String json) throws BusinessException {
		return generaliService.retrieveMemoDetails(json);

    }
	
	@RequestMapping(value = "/generateEtr", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
    public final String generateEtr(@RequestBody final String json){
		
		return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            		return generaliService.generateEtr(json);
            }
		}.execute(json);
    }
	
	@RequestMapping(value = "/lockUnlockCase", method= RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
	public final String lockOrUnlockCase(@RequestBody final String json){
	    return new ServiceTemplate(){
	        @Override
	        public String doExecute(final String json) throws  BusinessException {
	            return generaliService.lockOrUnlockCase(json);
	        }
	    }.execute(json);
	}
	
	@RequestMapping(value="/sendMemoUpdateToBPM", method= RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
	@ResponseBody
	public final String sendMemoUpdateToBPM(@RequestBody final String json){
	    return generaliService.sendUpdateToBPM(json);
	}
	
	/*@RequestMapping(value = "/validateLogin", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final String validateReassignLogin(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return agentProfileService.validateReassignLogin(json);
            }
        }.execute(json);
    }
	
	@RequestMapping(value = "/retrieveAgentProfile", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final String retrieveAgentsForReassign(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return agentProfileService.retrieveAgentsForReassign(json);
            }
        }.execute(json);
    }
	
	@RequestMapping(value = "/retrieveLeads", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final String retrieveLeads(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return agentProfileService.retrieveLeads(json);
            }
        }.execute(json);
    }
	
	@RequestMapping(value = "/reassignLeadsToAgents", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String reassignLeadsToAgents(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	return lifeEngageSyncService.save(json);
            	
            }
        }.execute(json);
    }*/

	
	 
}
