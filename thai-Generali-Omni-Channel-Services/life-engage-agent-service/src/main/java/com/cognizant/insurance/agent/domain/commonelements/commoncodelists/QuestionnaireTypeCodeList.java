/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * This list defines the type of Questionnaires.
 * 
 * @author 301350 
 * Newly added for EApp - 301350, 01Aug2013. Not part of ACORD. 
 * 
 * 
 */
public enum QuestionnaireTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Lifestyle,
    /**
     * 
     * 
     * 
     * 
     */
    Health,
    
    Other,
    Additional_Lifestyle,
    Additional_Health,
    Additional_Other,
    /**
     * 
     * 
     * 
     * 
     */
    Basic,
    /**
     * 
     */
    Additional_Basic
   }
