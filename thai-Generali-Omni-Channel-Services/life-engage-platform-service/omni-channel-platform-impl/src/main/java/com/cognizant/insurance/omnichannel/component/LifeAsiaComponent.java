package com.cognizant.insurance.omnichannel.component;

import org.json.JSONObject;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.InputValidationException;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaRequest;
import com.cognizant.insurance.omnichannel.utils.MemoApplicationData;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;

public interface LifeAsiaComponent {
	//TODO change retrun type
	public LifeAsiaRequest createLifeAsiaRequest(String spajNo)  throws BusinessException;
	public LifeAsiaRequest createLifeAsiaRequest(Transactions transactions) throws Exception;
	public void submit(LifeAsiaRequest lifeAsiaReq);
	public void submit(Number lifeAsiaRequestId);
	public String createLifeAsiaRequestString(RequestInfo requestInfo,JSONObject jsonObject) throws BusinessException;
	public String submitLifeAsiaWithPayload(String lifeAsiaReq);
	public boolean updateApplication(String lifeAsiaDocId,String status,String pendingInfo) throws BusinessException;
	public void fetchAndUpdateStatus(String agentId) throws BusinessException;
	public void createICMInfo(String eAppId,boolean isResubmit);
	public String isLASAvailable_withDetails();
	public boolean isLASAvailable();
	public String updateMemoDetails(MemoApplicationData memoAppData) throws InputValidationException;
	public String retrieveMemoDetails(RequestInfo requestInfo, JSONObject jsonRqObj) throws InputValidationException;
    public void triggerEappMemoEmail(String appNumber);
	public String updateAppainStatus(String appNo, String requestJson) throws InputValidationException;
    public String updateApplicationStatus(MemoApplicationData memoAppData);
    String sendUpdateToBPM(String json, String url) throws BusinessException;
	
}
