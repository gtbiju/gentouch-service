/**
 * 
 */
package com.cognizant.insurance.agent.audit;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;



import com.cognizant.insurance.agent.domain.agreement.Agreement;
import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;

/**
 * The Class class IllustrationRespondPayLoad.
 * 
 * @author 330012
 */
@Entity
public class IllustrationResponsePayLoad  extends InformationModelObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8829050581892581693L;

	/** The request hash code. */
	private Integer responseHashCode;

	  /** The request json. */
    @Lob
    private String responseJson;
    
    /** The premiumSummary json. */
    @Lob
    private String summaryJson;
    /**
     * Proposal Number
     */
    private Integer identifier;

    /**
     * Reference of agreement which illustration belongs to
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }) 
    private Agreement agreement;
	/**
	 * @return the requestHashCode
	 */
	public Integer getResponsetHashCode() {
		return responseHashCode;
	}

    /**
     * Gets the responseJson.
     *
     * @return the responseJson
     */
    public String getResponseJson() {
        return responseJson;
    }
    
    /**
     * Sets the request json.
     *
     * @param v the responseJson to set
     */
    public void setResponseJson(String responseJson) {
        this.responseJson = responseJson;
    }



	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}



	public Integer getIdentifier() {
		return identifier;
	}

	/**
	 * @param agreement the agreement to set
	 */
	public void setAgreement(Agreement agreement) {
		this.agreement = agreement;
	}

	/**
	 * @return the agreement
	 */
	public Agreement getAgreement() {
		return agreement;
	}

	public void setSummaryJson(String summaryJson) {
		this.summaryJson = summaryJson;
	}

	public String getSummaryJson() {
		return summaryJson;
	}
   
}
