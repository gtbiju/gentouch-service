package com.cognizant.insurance.agent.domain.commonelements.commonclasses;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GLISelectedOption /*extends SelectedOption*/{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Id
	private Long id;
    private String selectedOptionKey;

    private String selectedOptionValue;
    
    private Long selectedOptionId;
    
    private String additionalQuestionOne;
    
    private String additionalQuestionTwo;
    
    private String additionalQuestionThree;
    
    private String additionalQuestionFour;
    
    private String additionalQuestionFive;
    
    public GLISelectedOption(){
    	this.selectedOptionId = 0l;
    }

    public Long getSelectedOptionId() {
        return selectedOptionId;
    }

    public void setSelectedOptionId(Long selectedOptionId) {
        this.selectedOptionId = selectedOptionId;
    }

    public String getSelectedOptionKey() {
        return selectedOptionKey;
    }

    public void setSelectedOptionKey(String selectedOptionKey) {
        this.selectedOptionKey = selectedOptionKey;
    }

    public String getSelectedOptionValue() {
        return selectedOptionValue;
    }

    public void setSelectedOptionValue(String selectedOptionValue) {
        this.selectedOptionValue = selectedOptionValue;
    }
    
    public String toString(){
    	return selectedOptionValue;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the additionalQuestionOne
	 */
	public String getAdditionalQuestionOne() {
		return additionalQuestionOne;
	}

	/**
	 * @param additionalQuestionOne the additionalQuestionOne to set
	 */
	public void setAdditionalQuestionOne(String additionalQuestionOne) {
		this.additionalQuestionOne = additionalQuestionOne;
	}

	/**
	 * @return the additionalQuestionTwo
	 */
	public String getAdditionalQuestionTwo() {
		return additionalQuestionTwo;
	}

	/**
	 * @param additionalQuestionTwo the additionalQuestionTwo to set
	 */
	public void setAdditionalQuestionTwo(String additionalQuestionTwo) {
		this.additionalQuestionTwo = additionalQuestionTwo;
	}

	/**
	 * @return the additionalQuestionThree
	 */
	public String getAdditionalQuestionThree() {
		return additionalQuestionThree;
	}

	/**
	 * @param additionalQuestionThree the additionalQuestionThree to set
	 */
	public void setAdditionalQuestionThree(String additionalQuestionThree) {
		this.additionalQuestionThree = additionalQuestionThree;
	}

	/**
	 * @return the additionalQuestionFour
	 */
	public String getAdditionalQuestionFour() {
		return additionalQuestionFour;
	}

	/**
	 * @param additionalQuestionFour the additionalQuestionFour to set
	 */
	public void setAdditionalQuestionFour(String additionalQuestionFour) {
		this.additionalQuestionFour = additionalQuestionFour;
	}

	/**
	 * @return the additionalQuestionFive
	 */
	public String getAdditionalQuestionFive() {
		return additionalQuestionFive;
	}

	/**
	 * @param additionalQuestionFive the additionalQuestionFive to set
	 */
	public void setAdditionalQuestionFive(String additionalQuestionFive) {
		this.additionalQuestionFive = additionalQuestionFive;
	}

}
