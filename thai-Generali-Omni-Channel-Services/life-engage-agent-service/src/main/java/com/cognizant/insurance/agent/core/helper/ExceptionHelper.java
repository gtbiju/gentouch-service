/**
 *
 * © Copyright 2013, Cognizant 
 *
 * @author        : 262471
 * @version       : 0.1, Feb 23, 2013
 */
package com.cognizant.insurance.agent.core.helper;

import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.core.exception.SystemException;

/**
 * The Class class ExceptionHelper.
 */
public final class ExceptionHelper {

    /**
     * Instantiates a new exception helper.
     */
    private ExceptionHelper() {
        // not called
    }

    /**
     * Throw business exception, if condition is true.
     * 
     * @param condition
     *            the condition
     * @param message
     *            the message
     * @throws BusinessException
     *             the business exception
     */
    public static void throwBusinessException(final boolean condition, final String message) throws BusinessException {
        if (condition) {
            throw new BusinessException(message);
        }
    }

    /**
     * Throw business exception.
     * 
     * @param condition
     *            the condition
     * @param errorCode
     *            the error code
     * @param message
     *            the message
     * @throws BusinessException
     *             the business exception
     */
    public static void throwBusinessException(final boolean condition, final String errorCode, final String message)
            throws BusinessException {
        if (condition) {
            throw new BusinessException(errorCode, message);
        }
    }

    /**
     * Throw business exception.
     * 
     * @param condition
     *            the condition
     * @param message
     *            the message
     * @param cause
     *            the cause
     * @throws BusinessException
     *             the business exception
     */
    public static void throwBusinessException(final boolean condition, final String message, Throwable cause)
            throws BusinessException {
        if (condition) {
            throw new BusinessException(message, cause);
        }
    }

    /**
     * Throw business exception.
     * 
     * @param condition
     *            the condition
     * @param errorCode
     *            the error code
     * @param message
     *            the message
     * @param cause
     *            the cause
     * @throws BusinessException
     *             the business exception
     */
    public static void throwBusinessException(final boolean condition, final String errorCode, final String message,
            Throwable cause) throws BusinessException {
        if (condition) {
            throw new BusinessException(message, cause);
        }
    }

    /**
     * Throw system exception, if condition is true.
     * 
     * @param condition
     *            the condition
     * @param message
     *            the message
     */
    public static void throwSystemException(final boolean condition, final String message) {
        if (condition) {
            throw new SystemException(message);
        }
    }

    /**
     * Throw system exception.
     * 
     * @param condition
     *            the condition
     * @param errorCode
     *            the error code
     * @param message
     *            the message
     */
    public static void throwSystemException(final boolean condition, final String errorCode, final String message) {
        if (condition) {
            throw new SystemException(errorCode, message);
        }
    }

    /**
     * Throw system exception.
     * 
     * @param condition
     *            the condition
     * @param message
     *            the message
     * @param cause
     *            the cause
     * @throws BusinessException
     *             the business exception
     */
    public static void throwSystemException(final boolean condition, final String message, Throwable cause) {
        if (condition) {
            throw new SystemException(message, cause);
        }
    }

    /**
     * Throw system exception.
     * 
     * @param condition
     *            the condition
     * @param message
     *            the message
     * @param errorCode
     *            the error code
     * @param cause
     *            the cause
     * @throws BusinessException
     *             the business exception
     */
    public static void throwSystemException(final boolean condition, final String message, final String errorCode,
            Throwable cause) {
        if (condition) {
            throw new SystemException(errorCode, message, cause);
        }
    }
}
