/**
 * 
 */
package com.cognizant.insurance.domain.agreement.agreementcodelists;

/**
 * Loan Interest Type.
 * 
 * @author 301350
 * 
 * 
 */
public enum LoanInterestTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    Fixedloaninterestrate,
    /**
     * 
     * 
     * 
     * 
     */
    Variableloaninterestrate,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * 
     * 
     * 
     * 
     */
    Indexedloaninterestrate,
    /**
     * 
     * 
     * 
     * 
     */
    Adjustableloaninterestrate
}
