package com.cognizant.le.model.dao;

import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cognizant.le.model.dto.Carrier;
import com.cognizant.le.model.dto.CodeLocaleLookup;
import com.cognizant.le.model.dto.CodeLookup;
import com.cognizant.le.model.dto.CodeRelation;
import com.cognizant.le.model.dto.CodeTypeLookup;
import com.cognizant.le.util.Constants;
import com.cognizant.le.util.ScriptRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class LookupDAOImpl.
 */
@Repository
public class LookupDAOImpl implements LookupDAO {

/*	*//** The jdbc template. *//*
	@Autowired
	private JdbcTemplate jdbcTemplate;*/

	/** The environment. */
	@Autowired
	private Environment environment;
	


	/**
	 * Insert carriers.
	 * 
	 * @param carrierList
	 *            the carrier list
	 */
	public void insertCarriers(final List<Carrier> carrierList) {
/*		jdbcTemplate.batchUpdate(
				environment.getProperty(Constants.INSERT_CARRIER),
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						Carrier carrier = carrierList.get(i);
						ps.setInt(1, carrier.getCarrierId());
						ps.setString(2, carrier.getCarrierName());

					}

					public int getBatchSize() {
						return carrierList.size();
					}
				});*/
	}

	/**
	 * Insert types.
	 * 
	 * @param codeTypes
	 *            the code types
	 */
	public void insertTypes(final List<CodeTypeLookup> codeTypes) {
		/*jdbcTemplate.batchUpdate(
				environment.getProperty(Constants.INSERT_CODE_TYPE_LOOKUP),
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						CodeTypeLookup codeTypeLookup = codeTypes.get(i);
						ps.setInt(1, codeTypeLookup.getId());
						ps.setString(2, codeTypeLookup.getTypeName());
						ps.setInt(3, codeTypeLookup.getCarrier_id());

					}

					public int getBatchSize() {
						return codeTypes.size();
					}
				});
*/
	}

	/**
	 * Insert look up.
	 * 
	 * @param codeLookups
	 *            the code lookups
	 */
	public void insertLookUp(final List<CodeLookup> codeLookups) {
		/*jdbcTemplate.batchUpdate(
				environment.getProperty(Constants.INSERT_CODE_LOOKUP),
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						CodeLookup codeLookup = codeLookups.get(i);
						ps.setInt(1, codeLookup.getId());
						ps.setString(2, codeLookup.getCode());
						ps.setString(3, codeLookup.getDescription());
						ps.setInt(4, codeLookup.getTypeId());

					}

					public int getBatchSize() {
						return codeLookups.size();
					}
				});*/

	}

	/**
	 * Insert relations.
	 * 
	 * @param codeRelations
	 *            the code relations
	 */
	public void insertRelations(final List<CodeRelation> codeRelations) {
		/*jdbcTemplate.batchUpdate(
				environment.getProperty(Constants.INSERT_RELATIONS),
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						CodeRelation codeRelation = codeRelations.get(i);
						ps.setInt(1, codeRelation.getChildCode());
						ps.setInt(2, codeRelation.getParentCode());
						ps.setInt(3, codeRelation.getCodeType());
					}

					public int getBatchSize() {
						return codeRelations.size();
					}
				});
*/
	}

	/**
	 * Insert locale.
	 * 
	 * @param codeLocaleLookups
	 *            the code locale lookups
	 */
	public void insertLocale(final List<CodeLocaleLookup> codeLocaleLookups) {
		/*jdbcTemplate.batchUpdate(
				environment.getProperty(Constants.INSERT_LOCALE_LOOKUP),
				new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i)
							throws SQLException {
						CodeLocaleLookup codeLocaleLookup = codeLocaleLookups
								.get(i);
						ps.setString(1, codeLocaleLookup.getCountry());
						ps.setString(2, codeLocaleLookup.getLanguage());
						ps.setString(3, codeLocaleLookup.getValue());
						ps.setInt(4, codeLocaleLookup.getCodeId());
					}

					public int getBatchSize() {
						return codeLocaleLookups.size();
					}
				});
*/
	}

	/**
	 * Delete existing entries. A temporary function to delete all the existing entries.
	 */
	public void deleteExistingEntries() {
		String sql[] = new String[10];
		sql[0] = "DELETE FROM CODE_LOCALE_LOOKUP";
		sql[1] = "DELETE FROM CODE_RELATION";
		sql[2] = "DELETE FROM CODE_LOOKUP";
		sql[3] = "DELETE FROM CODE_TYPE_LOOKUP";
		sql[4] = "DELETE FROM CORE_CARRIER";
		//jdbcTemplate.batchUpdate(sql);
	}
	
/*	*//**
	 * Check table.
	 *
	 * @param schemaName the schema name
	 * @param tableName the table name
	 * @return the boolean
	 *//*
	public Boolean checkTable(String schemaName, String tableName) throws DataAccessException{
		String query = environment.getProperty(Constants.CHECK_TABLE);
		query = query.replace("table_schema", schemaName);
		query = query.replace("table_name", tableName);
		Integer count =  jdbcTemplate.queryForInt(query);
		if(count!=1){
			return false;
		}else{
			return true;
		}
	}*/
	
/*	public void createTable(InputStreamReader isr){
		Connection con =null;
		try {
			con = jdbcTemplate.getDataSource().getConnection();
			ScriptRunner scr = new ScriptRunner(con,false,true);
			scr.runScript(isr);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}*/
	
	
	public static final String createDatabase(String dbName, String selectedDBType){
	    Connection con1 = null;
	    try {
		 con1 = null;
		 String sql = null;
		 if (null != selectedDBType && selectedDBType.equals("MySQL")) {
			 sql+= " "+dbName+";";
		 }else if (null != selectedDBType && selectedDBType.equals("MS-SQL")) {
			 sql+= " ["+dbName+"];";	 
		 }
		 PreparedStatement ps= con1.prepareStatement(sql);
		 ps.execute();
		 ps.close();
		} catch (Exception e) {
			return e.getMessage();
		}finally{
		    if(con1!=null){
		    try {
				con1.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		    }
		}
		 return "Successfully created database.";
	}

@Override
public Boolean checkTable(String schemaName, String tableName) {
    // TODO Auto-generated method stub
    return null;
}

@Override
public void createTable(InputStreamReader isr) {
    // TODO Auto-generated method stub
    
}

}
