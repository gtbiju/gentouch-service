/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partycodelists;

/**
 * A listing of the various determinants concerning the applicability of
 * something. These values are used to specify whether a corresponding concept
 * is known to "apply."
 * 
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */
public enum ApplicabilityCodeList {
    /**
     * Known to apply. Same as a YES response to a question.
     * 
     * 
     * 
     */
    Applies,
    /**
     * Does not apply. Same as a NO response to a question.
     * 
     * 
     * 
     */
    Doesnotapply,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * May or may not apply. This is slightly different from "Unknown". If a
     * question was asked and not answered - or if a question was never asked,
     * then the Applicability response is "Unknown". However, if a question was
     * asked, and a the response provided is "Unknown" (or some equivalent e.g.
     * "don't know"), then such an answer or reply is modeled using the method
     * representing "Stated as Not Known".
     * 
     * 
     * 
     * 
     */
    StatedasNotKnown,
    /**
     * 
     * 
     * 
     * 
     */
    Unknown
}
