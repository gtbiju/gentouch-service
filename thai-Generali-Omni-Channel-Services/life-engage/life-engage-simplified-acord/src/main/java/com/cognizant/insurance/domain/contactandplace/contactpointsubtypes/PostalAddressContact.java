/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.contactpointsubtypes;

import java.util.Date;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.domain.contactandplace.ContactPoint;
import com.cognizant.insurance.domain.contactandplace.Place;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.AddressNatureCodeList;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.DirectionTypeCodeList;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.Country;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.CountryElement;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.CountrySubdivision;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.Municipality;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.PostCode;

/**
 * This concept represents a contact utilized for delivery of traditional
 * physical mail.
 * 
 * @author 301350
 * 
 * 
 */


@Entity
public class PostalAddressContact extends ContactPoint {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4335810489944210768L;

    
    /**
     * The street/road number as a component of a street address. For example:
     * "123 Main St" the "123" is the street number. 
     * 
     * 
     * 
     */

	@Column(columnDefinition = "NVARCHAR(MAX)")
    private String streetNumber;

    /**
     * The name of the street where the address is located.
     * 
     * 
     * 
     */
    @Column(columnDefinition="nvarchar(100)")
    private String streetName;

    /**
     * A code indicating the type of street. The source of the code list varies
     * based on locale.
     * 
     * USA: USPS Suffix List or the ISO Street Type abbreviations list. <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    private ExternalCode streetTypeCode;

    /**
     * The mail receptacle located at a public post office or other postal
     * service provider.
     * 
     * 
     * 
     */
    @Column(columnDefinition="nvarchar(50)")
    private String boxNumber;

    /**
     * The floor number.
     * 
     * 
     * 
     */
    private String floorNumber;

    /**
     * The numeric portion or alphanumeric label that identifies a unit within a
     * building.
     * 
     * e.g: 14 (as in Apartment 14, Bath Road)
     * 
     * 
     * 
     */

	@Column(columnDefinition = "NVARCHAR(MAX)")
    private String unitNumber;

    /**
     * The name of the building.
     * 
     * e.g: Residence Petit Paradis
     * 
     * 
     * 
     */

	@Column(columnDefinition = "NVARCHAR(MAX)")
    private String buildingName;

    /**
     * The lines of the formatted address. This may be derived using other
     * elements of the PostalAddress. In fact, such derivation is probably
     * preferred.
     * 
     * 
     * 
     */
    private String addressLines;
    
    /** The address line1. */
    @Basic(fetch=FetchType.EAGER)
    @Column(columnDefinition="nvarchar(100)")
    private String addressLine1;
    
    /** The address line2. */
    @Basic(fetch=FetchType.EAGER)
    @Column(columnDefinition="nvarchar(100)")
    private String addressLine2;
    
    /** The address line3. */
    @Basic(fetch=FetchType.EAGER)
    @Column(columnDefinition="nvarchar(100)")
    private String addressLine3;
    
    /**
     * A code classifying an address.     
     */
    @Enumerated
    private AddressNatureCodeList addressNatureCode;

    /**
     * The representation of this address as a bar code that can be printed on
     * mail. Some mail operators offer discount on bulk mail if a valid barcode
     * has been printed by the sender.
     * 
     * e.g: The US Zip+4 Postnet barcode is a 12 digits postal bar code
     * structured as follows: 5 digits for the ZIP code. 4 digits for the ZIP+4
     * code. 2 digits representing the last 2 digits of the street address.1
     * digit checksum calculated as the sum of the other digits modulo 10
     * 
     * e.g: The US Zip+4 Postnet barcode is a 12 digits postal bar code
     * structured as follows: 5 digits for the ZIP code. 4 digits for the ZIP+4
     * code. 2 digits representing the last 2 digits of the street address.1
     * digit checksum calculated as the sum of the other digits modulo 10 <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    private String postalBarcode;

    /**
     * A PostalAddress is a very geographic-centric concept because addresses
     * differ in many countries around the world. This aggregate relationship
     * allows PostalAddress to contain any necessary CountryElements.
     * 
     * The extension point for different parts of an address is CountryElement,
     * not attributes on PostalAddress.
     * 
     * 
     * 
     * 
     * 
     */
    
    @OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name = "postaladdresscontactid")
    private Set<CountryElement> includedCountryElement;

    /**
     * When an address consists of a directional indicator before the street
     * address, a code identifying the direction.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private DirectionTypeCodeList preDirectionCode;

    /**
     * When an address consists of a directional indicator after the street
     * address, a code identifying the direction.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private DirectionTypeCodeList postDirectionCode;

    /**
     * This relationship links a postal address with its related country.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.REMOVE }, fetch=FetchType.EAGER)
    private Country postalCountry;

    /**
     * This relationship links a postal address with its related country
     * subdivision (e.g. Province, State, etc.).
     * 
     * 
     * 
     * 
     * 
     */
    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.REMOVE }, fetch=FetchType.EAGER)
    private CountrySubdivision postalCountrySubdivision;

    /**
     * This relationship links a postal address with its related municipality
     * (e.g. City, Town, Village, etc.).
     * 
     * 
     * 
     * 
     * 
     */
    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST ,CascadeType.REMOVE})
    private Municipality postalMunicipality;

    /**
     * This relationship links a postal address with its related post code (e.g.
     * zip code).
     * 
     * 
     * 
     * 
     * 
     */
    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.REMOVE }, fetch=FetchType.EAGER)
    private PostCode postalPostCode;

    /**
     * A postal carrier route is the group of addresses assigned the same code
     * to aid in mail delivery. This is not synonymous with a zip or postal
     * code.
     * 
     * 
     * 
     */
    private String carrierRoute;
    
    /**
     * Number of years lived or living at this address.
     */
    private Float yearsAtAddress;
    
    /**
     * Mail Returned to Company.
     */
    private Boolean returnedMailInd;
    
    /**
     * Address Validation Indicator.
     */
    private Boolean addressValidInd;
    
    
    /**
     * Free text description of the address (e.g. place descriptor).
     * 
     * For example, this could represent a full postal address as utilized for a
     * mailing/shipping label.
     * 
     * 
     * 
     */
    private String unstructuredAddress;

    /**
     * This relationship links a place with the address that can be utilized to
     * identify or locate the place.
     * 
     * For example, the corresponding address might involve the use of
     * coordinates. Such coordinates are complex because there is more than one
     * coordinate system.
     * 
     * A given place can have more than one coordinate (there can be very many
     * for large places). A single coordinate, however, can only refer to a
     * single place.
     * 
     * 
     * 
     * 
     * 
     */
    private Place identifiedPlace;

    /**
     * Add bar code to addressInd.
     */
    private Boolean addressBarCodeInd;

    /**
     * An indicator used to prevent systematic address changes when an addressed
     * is standardized.
     */
    private Boolean preventOverrideInd;

    /**
     * Indicator whether the address is the legal residence.
     */
    private Boolean legalAddressInd;
    
    /******Newly added for EAp ********/
    
    /**
     * Specifies the document that is submitted as address proof. 
     */
    private String addressProof;
    /**
     * Identifies the address proof document issuing authority. 
     */
    private String addressProofIssuingAuthority;
    
    /**
     * Identifies the address proof document issued date. 
     */
    @Temporal(TemporalType.DATE)
     private Date addressProofIssuedDate;

    /******Newly added for EAp - End ********/
    
    /**
     * @return the addressBarCodeInd
     */
    public Boolean getAddressBarCodeInd() {
        return addressBarCodeInd;
    }

    /**
     * @return the identifiedPlace
     * 
     * 
     */
    public Place getIdentifiedPlace() {

        return identifiedPlace;

    }

    /**
     * @return the legalAddressInd
     */
    public Boolean getLegalAddressInd() {
        return legalAddressInd;
    }

    /**
     * @return the preventOverrideInd
     */
    public Boolean getPreventOverrideInd() {
        return preventOverrideInd;
    }

    /**
     * @return the unstructuredAddress
     * 
     * 
     */
    public String getUnstructuredAddress() {

        return unstructuredAddress;

    }

    /**
     * @param addressBarCodeInd
     *            the addressBarCodeInd to set
     */
    public void setAddressBarCodeInd(final Boolean addressBarCodeInd) {
        this.addressBarCodeInd = addressBarCodeInd;
    }

    /**
     * @param identifiedPlace
     *            the identifiedPlace to set
     * 
     * 
     */
    public void setIdentifiedPlace(final Place identifiedPlace) {

        this.identifiedPlace = identifiedPlace;

    }

    /**
     * @param legalAddressInd
     *            the legalAddressInd to set
     */
    public void setLegalAddressInd(final Boolean legalAddressInd) {
        this.legalAddressInd = legalAddressInd;
    }

    /**
     * @param preventOverrideInd
     *            the preventOverrideInd to set
     */
    public void setPreventOverrideInd(final Boolean preventOverrideInd) {
        this.preventOverrideInd = preventOverrideInd;
    }

    /**
     * @param unstructuredAddress
     *            the unstructuredAddress to set
     * 
     * 
     */
    public void setUnstructuredAddress(final String unstructuredAddress) {

        this.unstructuredAddress = unstructuredAddress;

    }
    
    
    /**
     * Gets the addressLines.
     * 
     * @return Returns the addressLines.
     */
    public String getAddressLines() {
        return addressLines;
    }

    /**
     * Gets the address valid ind.
     *
     * @return the addressValidInd
     */
	public Boolean getAddressValidInd() {
		return addressValidInd;
	}

    /**
     * Gets the boxNumber.
     * 
     * @return Returns the boxNumber.
     */
    public String getBoxNumber() {
        return boxNumber;
    }

    /**
     * Gets the buildingName.
     * 
     * @return Returns the buildingName.
     */
    public String getBuildingName() {
        return buildingName;
    }

    /**
     * Gets the carrierRoute.
     * 
     * @return Returns the carrierRoute.
     */
    public String getCarrierRoute() {
        return carrierRoute;
    }

    /**
     * Gets the floorNumber.
     * 
     * @return Returns the floorNumber.
     */
    public String getFloorNumber() {
        return floorNumber;
    }

    /**
     * Gets the includedCountryElement.
     * 
     * @return Returns the includedCountryElement.
     */
    public Set<CountryElement> getIncludedCountryElement() {
        return includedCountryElement;
    }

    /**
     * Gets the postalBarcode.
     * 
     * @return Returns the postalBarcode.
     */
    public String getPostalBarcode() {
        return postalBarcode;
    }

    /**
     * Gets the postalCountry.
     * 
     * @return Returns the postalCountry.
     */
    public Country getPostalCountry() {
        return postalCountry;
    }

    /**
     * Gets the postalCountrySubdivision.
     * 
     * @return Returns the postalCountrySubdivision.
     */
    public CountrySubdivision getPostalCountrySubdivision() {
        return postalCountrySubdivision;
    }

    /**
     * Gets the postalMunicipality.
     * 
     * @return Returns the postalMunicipality.
     */
    public Municipality getPostalMunicipality() {
        return postalMunicipality;
    }

    /**
     * Gets the postalPostCode.
     * 
     * @return Returns the postalPostCode.
     */
    public PostCode getPostalPostCode() {
        return postalPostCode;
    }

    /**
     * Gets the postDirectionCode.
     * 
     * @return Returns the postDirectionCode.
     */
    public DirectionTypeCodeList getPostDirectionCode() {
        return postDirectionCode;
    }

    /**
     * Gets the preDirectionCode.
     * 
     * @return Returns the preDirectionCode.
     */
    public DirectionTypeCodeList getPreDirectionCode() {
        return preDirectionCode;
    }

    /**
     * Gets the returned mail ind.
     *
     * @return the returnedMailInd
     */
	public Boolean getReturnedMailInd() {
		return returnedMailInd;
	}

    /**
     * Gets the streetName.
     * 
     * @return Returns the streetName.
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Gets the streetNumber.
     * 
     * @return Returns the streetNumber.
     */
    public String getStreetNumber() {
        return streetNumber;
    }

    /**
     * Gets the streetTypeCode.
     * 
     * @return Returns the streetTypeCode.
     */
    public ExternalCode getStreetTypeCode() {
        return streetTypeCode;
    }

    /**
     * Gets the unitNumber.
     * 
     * @return Returns the unitNumber.
     */
    public String getUnitNumber() {
        return unitNumber;
    }

    /**
     * Gets the years at address.
     *
     * @return the yearsAtAddress
     */
	public Float getYearsAtAddress() {
		return yearsAtAddress;
	}

    /**
     * Sets The addressLines.
     * 
     * @param addressLines
     *            The addressLines to set.
     */
    public void setAddressLines(final String addressLines) {
        this.addressLines = addressLines;
    }

    /**
     * Sets the address valid ind.
     *
     * @param addressValidInd the addressValidInd to set
     */
	public void setAddressValidInd(final Boolean addressValidInd) {
		this.addressValidInd = addressValidInd;
	}

    /**
     * Sets The boxNumber.
     * 
     * @param boxNumber
     *            The boxNumber to set.
     */
    public void setBoxNumber(final String boxNumber) {
        this.boxNumber = boxNumber;
    }

    /**
     * Sets The buildingName.
     * 
     * @param buildingName
     *            The buildingName to set.
     */
    public void setBuildingName(final String buildingName) {
        this.buildingName = buildingName;
    }

    /**
     * Sets The carrierRoute.
     * 
     * @param carrierRoute
     *            The carrierRoute to set.
     */
    public void setCarrierRoute(final String carrierRoute) {
        this.carrierRoute = carrierRoute;
    }

    /**
     * Sets The floorNumber.
     * 
     * @param floorNumber
     *            The floorNumber to set.
     */
    public void setFloorNumber(final String floorNumber) {
        this.floorNumber = floorNumber;
    }

    /**
     * Sets The includedCountryElement.
     * 
     * @param includedCountryElement
     *            The includedCountryElement to set.
     */
    public void setIncludedCountryElement(
            final Set<CountryElement> includedCountryElement) {
        this.includedCountryElement = includedCountryElement;
    }

    /**
     * Sets The postalBarcode.
     * 
     * @param postalBarcode
     *            The postalBarcode to set.
     */
    public void setPostalBarcode(final String postalBarcode) {
        this.postalBarcode = postalBarcode;
    }

    /**
     * Sets The postalCountry.
     * 
     * @param postalCountry
     *            The postalCountry to set.
     */
    public void setPostalCountry(final Country postalCountry) {
        this.postalCountry = postalCountry;
    }

    /**
     * Sets The postalCountrySubdivision.
     * 
     * @param postalCountrySubdivision
     *            The postalCountrySubdivision to set.
     */
    public void setPostalCountrySubdivision(
            final CountrySubdivision postalCountrySubdivision) {
        this.postalCountrySubdivision = postalCountrySubdivision;
    }

    /**
     * Sets The postalMunicipality.
     * 
     * @param postalMunicipality
     *            The postalMunicipality to set.
     */
    public void setPostalMunicipality(
            final Municipality postalMunicipality) {
        this.postalMunicipality = postalMunicipality;
    }

    /**
     * Sets The postalPostCode.
     * 
     * @param postalPostCode
     *            The postalPostCode to set.
     */
    public void setPostalPostCode(final PostCode postalPostCode) {
        this.postalPostCode = postalPostCode;
    }

    /**
     * Sets The postDirectionCode.
     * 
     * @param postDirectionCode
     *            The postDirectionCode to set.
     */
    public void setPostDirectionCode(
            final DirectionTypeCodeList postDirectionCode) {
        this.postDirectionCode = postDirectionCode;
    }

    /**
     * Sets The preDirectionCode.
     * 
     * @param preDirectionCode
     *            The preDirectionCode to set.
     */
    public void setPreDirectionCode(
            final DirectionTypeCodeList preDirectionCode) {
        this.preDirectionCode = preDirectionCode;
    }

	/**
	 * Sets the returned mail ind.
	 *
	 * @param returnedMailInd the returnedMailInd to set
	 */
	public void setReturnedMailInd(final Boolean returnedMailInd) {
		this.returnedMailInd = returnedMailInd;
	}

	/**
     * Sets The streetName.
     * 
     * @param streetName
     *            The streetName to set.
     */
    public void setStreetName(final String streetName) {
        this.streetName = streetName;
    }

	/**
     * Sets The streetNumber.
     * 
     * @param streetNumber
     *            The streetNumber to set.
     */
    public void setStreetNumber(final String streetNumber) {
        this.streetNumber = streetNumber;
    }

	/**
     * Sets The streetTypeCode.
     * 
     * @param streetTypeCode
     *            The streetTypeCode to set.
     */
    public void setStreetTypeCode(final ExternalCode streetTypeCode) {
        this.streetTypeCode = streetTypeCode;
    }

	/**
     * Sets The unitNumber.
     * 
     * @param unitNumber
     *            The unitNumber to set.
     */
    public void setUnitNumber(final String unitNumber) {
        this.unitNumber = unitNumber;
    }

	/**
	 * Sets the years at address.
	 *
	 * @param yearsAtAddress the yearsAtAddress to set
	 */
	public void setYearsAtAddress(final Float yearsAtAddress) {
		this.yearsAtAddress = yearsAtAddress;
	}

    /**
     * Gets the address line1.
     *
     * @return the address line1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * Sets the address line1.
     *
     * @param addressLine1 the address line1 to set.
     */
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     * Gets the address line2.
     *
     * @return the address line2
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * Sets the address line2.
     *
     * @param addressLine2 the address line2 to set.
     */
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     * Gets the address line3.
     *
     * @return the address line3
     */
    public String getAddressLine3() {
        return addressLine3;
    }

    /**
     * Sets the address line3.
     *
     * @param addressLine3 the address line3 to set.
     */
    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }	

    
    /**
     * @return the addressNatureCode
     * 
     * 
     */
    public AddressNatureCodeList getAddressNatureCode() {
        return addressNatureCode;
    }

  /**
     * @param addressNatureCode
     *            the addressNatureCode to set
     * 
     * 
     */
    public void setAddressNatureCode(
            final AddressNatureCodeList addressNatureCode) {
        this.addressNatureCode = addressNatureCode;
    }

/**
 * Gets the addressProof.
 *
 * @return Returns the addressProof.
 */
public final String getAddressProof() {
    return addressProof;
}

/**
 * Gets the addressProofIssuingAuthority.
 *
 * @return Returns the addressProofIssuingAuthority.
 */
public final String getAddressProofIssuingAuthority() {
    return addressProofIssuingAuthority;
}

/**
 * Gets the addressProofIssuedDate.
 *
 * @return Returns the addressProofIssuedDate.
 */
public final Date getAddressProofIssuedDate() {
    return addressProofIssuedDate;
}

/**
 * Sets The addressProof.
 *
 * @param addressProof The addressProof to set.
 */
public final void setAddressProof(String addressProof) {
    this.addressProof = addressProof;
}

/**
 * Sets The addressProofIssuingAuthority.
 *
 * @param addressProofIssuingAuthority The addressProofIssuingAuthority to set.
 */
public final void setAddressProofIssuingAuthority(
        String addressProofIssuingAuthority) {
    this.addressProofIssuingAuthority = addressProofIssuingAuthority;
}

/**
 * Sets The addressProofIssuedDate.
 *
 * @param addressProofIssuedDate The addressProofIssuedDate to set.
 */
public final void setAddressProofIssuedDate(Date addressProofIssuedDate) {
    this.addressProofIssuedDate = addressProofIssuedDate;
}


}
