package com.cognizant.insurance.agent.domain.party.registration.subtypes;

import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.party.registration.PartyRegistration;


/**
 * A registration issued by a tax assessing and collection authority to identify the tax paying status and history of a party.
 * 
 * e.g. The Social Security Number (SSN) for a person in the USA.
 * 
 * The Federal Employment Information Number (FEIN) for an organization in the USA.
 */
@Entity
public class TaxRegistration extends PartyRegistration {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Identifies the nature/type of the tax file.
	 * 
	 * e.g. Social Security Number (SSN) for persons in the USA.
	 * 
	 * e.g. Federal Employment Information Number (FEIN) for organizations in the USA.
	 */
	public String taxFileName;

	public String getTaxFileName() {
		return taxFileName;
	}

	public void setTaxFileName(String taxFileName) {
		this.taxFileName = taxFileName;
	}
	
	
}