/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.cognizant.insurance.agent.domain.agreement.agreementpremium.Premium;

/**
 * An agreement between a financial services provider and a financial services
 * role based on a product specification. A financial services agreement is
 * structured as marketable agreement, services agreement component, or
 * agreement group.
 * 
 * e.g. An insurance policy is a financial services agreement where the owner of
 * the agreement is the policyholder and the agreement specifies the obligation
 * of the insurer to pay benefits and of the premium payer to pay premiums. <!--
 * end-UML-doc -->
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class FinancialServicesAgreement extends Agreement {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4055091705144756357L;

    /**
     * The integration of two financial services agreements providing similar
     * coverages or services.
     * 
     * e.g: The individual agreement doctor's office cover, between John Doe and
     * Acme Insurance Company integrates the individual agreement Health cover
     * between John Doe and Social Security.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinTable(name = "FINSERVAGREEMENT_INTEGRATEDINFINSERVAGREEMENT", 
            joinColumns = { @JoinColumn(name = "FINANCIALSERVICESAGREEMENT_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "INTEGRATEDINFINSERVAGREEMENT_ID", referencedColumnName = "Id") })
    private Collection<FinancialServicesAgreement> isIntegratedIn;

    /**
     * The integration of two financial services agreements providing similar
     * coverages or services.
     * 
     * e.g: The individual agreement doctor's office cover, between John Doe and
     * Acme Insurance Company integrates the individual agreement Health cover
     * between John Doe and Social Security.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinTable(name = "FINSERVAGREEMENT_INTEGRATESFINSERVAGREEMENT",
            joinColumns = { @JoinColumn(name = "FINANCIALSERVICESAGREEMENT_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "INTEGRATESFINSERVAGREEMENT_ID", referencedColumnName = "Id") })
    private Collection<FinancialServicesAgreement> integrates;

    /**
     * The identification of a financial services agreement as a payment
     * mechanism for a financial services agreement.
     * 
     * e.g: A credit card agreement pays the premiums of the individual
     * agreement term life.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinTable(name = "FINSERVAGREEMENT_PAYSFINSERVAGREEMENT",
            joinColumns = { @JoinColumn(name = "FINANCIALSERVICESAGREEMENT_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "PAYSFINSERVAGREEMENT_ID", referencedColumnName = "Id") })
    private Set<FinancialServicesAgreement> pays;

    /**
     * The identification of a financial services agreement as a payment
     * mechanism for a financial services agreement.
     * 
     * e.g: A credit card agreement pays the premiums of the individual
     * agreement term life.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinTable(name = "FINSERVAGREEMENT_ISPAIDBYFINSERVAGREEMENT",
            joinColumns = { @JoinColumn(name = "FINANCIALSERVICESAGREEMENT_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "ISPAIDBYFINSERVAGREEMENT_ID", referencedColumnName = "Id") })
    private Set<FinancialServicesAgreement> isPaidBy;

   
    /**
     * Indicates whether the contract is opened for a single subscription or
     * whether a co-subscription is allowed. 
     * 
     * 
     * 
     */
    private Boolean coSubscriptionIndicator;
 
    /**
     * This relationship identifies the subscribed premiums that apply to the
     * contract.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    private Set<Premium> agreementPremium;
 
    /**
     * Gets the isIntegratedIn.
     * 
     * @return Returns the isIntegratedIn.
     */
    public Collection<FinancialServicesAgreement> getIsIntegratedIn() {
        return isIntegratedIn;
    }

    /**
     * Gets the integrates.
     * 
     * @return Returns the integrates.
     */
    public Collection<FinancialServicesAgreement> getIntegrates() {
        return integrates;
    }

    /**
     * Gets the pays.
     * 
     * @return Returns the pays.
     */
    public Set<FinancialServicesAgreement> getPays() {
        return pays;
    }

    /**
     * Gets the isPaidBy.
     * 
     * @return Returns the isPaidBy.
     */
    public Set<FinancialServicesAgreement> getIsPaidBy() {
        return isPaidBy;
    }

    /**
     * Gets the coSubscriptionIndicator.
     * 
     * @return Returns the coSubscriptionIndicator.
     */
    public Boolean getCoSubscriptionIndicator() {
        return coSubscriptionIndicator;
    }

    /**
     * Gets the agreementPremium.
     * 
     * @return Returns the agreementPremium.
     */
    public Set<Premium> getAgreementPremium() {
        return agreementPremium;
    }

    /**
     * Sets The isIntegratedIn.
     * 
     * @param isIntegratedIn
     *            The isIntegratedIn to set.
     */
    public void setIsIntegratedIn(
            final Collection<FinancialServicesAgreement> isIntegratedIn) {
        this.isIntegratedIn = isIntegratedIn;
    }

    /**
     * Sets The integrates.
     * 
     * @param integrates
     *            The integrates to set.
     */
    public void setIntegrates(
            final Collection<FinancialServicesAgreement> integrates) {
        this.integrates = integrates;
    }

    /**
     * Sets The pays.
     * 
     * @param pays
     *            The pays to set.
     */
    public void setPays(final Set<FinancialServicesAgreement> pays) {
        this.pays = pays;
    }

    /**
     * Sets The isPaidBy.
     * 
     * @param isPaidBy
     *            The isPaidBy to set.
     */
    public void
            setIsPaidBy(final Set<FinancialServicesAgreement> isPaidBy) {
        this.isPaidBy = isPaidBy;
    }

    /**
     * Sets The coSubscriptionIndicator.
     * 
     * @param coSubscriptionIndicator
     *            The coSubscriptionIndicator to set.
     */
    public void setCoSubscriptionIndicator(
            final Boolean coSubscriptionIndicator) {
        this.coSubscriptionIndicator = coSubscriptionIndicator;
    }

    /**
     * Sets The agreementPremium.
     * 
     * @param agreementPremium
     *            The agreementPremium to set.
     */
    public void setAgreementPremium(final Set<Premium> agreementPremium) {
        this.agreementPremium = agreementPremium;
    }

}
