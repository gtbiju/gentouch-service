package com.cognizant.insurance.omnichannel.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.impl.LifeEngageEAppComponentImpl;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliEAppComponent;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliAgentProfileRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliEAppRepository;
import com.cognizant.insurance.omnichannel.component.repository.LifeAsiaRepository;
import com.cognizant.insurance.omnichannel.component.repository.helper.GeneraliComponentRepositoryHelper;
import com.cognizant.insurance.omnichannel.dao.GeneraliAgreementDao;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliAgreementDaoImpl;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.omnichannel.vo.GeneraliTransactions;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.omnichannel.dao.GeneraliAgreementDao;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliAgreementDaoImpl;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;

public class GeneraliEAppComponenetImpl extends LifeEngageEAppComponentImpl implements GeneraliEAppComponent{
		
    @Autowired
    private GeneraliEAppRepository eAppRepository;
    
    @Autowired
    private LifeAsiaRepository bpmRepository;
    
    @Autowired
    @Qualifier("GAORequestMapping")
    private LifeEngageSmooksHolder GAOLockUnlockHolder;
    
    @Autowired
    private AuditRepository auditRepository;
    
    @Autowired
    @Qualifier("eAppSaveMapping")
    private LifeEngageSmooksHolder saveEAppHolder;
    
    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;
    
    /** The Constant SUCCESS_MESSAGE_UPDATE. */
    private static final String SUCCESS_MESSAGE_UPDATE = "Proposal Updated";
    
    /** The Constant SUCCESS_MESSAGE_SAVE. */
    private static final String SUCCESS_MESSAGE_SAVE = "Proposal Saved";
    
    /** The Constant LMS. */
    private static final String EAPP = "EAPP";
    
	/** The BPM status code. */
	private static final String pendingUW = "PendingUW";
	
    /** The id generator. */
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;
    
    /** The Constant E_APP. */
    private static final String E_APP = "eApp";
    
    /** The Constant REJECTED_MESSAGE. */
    private static final String REJECTED_MESSAGE = "Proposal Rejected";

	 /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";
    
    @Autowired
	 GeneraliAgentProfileRepository agentRepository;
    
    /**
     * Manage transaction data before saving.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @throws ParseException
     *             the parse exception
     */
    
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String saveEApp(final String json, final RequestInfo requestInfo, final LifeEngageAudit lifeEngageAudit) {
        LOGGER.trace("Inside LifeEngageEApp component saveEApp json: json" + json);
        String eAppResponse = StringUtils.EMPTY;
        Transactions transactions = new Transactions();
        String successMessage = StringUtils.EMPTY;
        String offlineIdentifier = "";
        Boolean isDeleteRequest = Boolean.FALSE;
        try {
            long initial_time = System.currentTimeMillis();
            LifeEngageComponentHelper.parseTrasactionDetails(json, transactions, false);
            String newAgentId = transactions.getKey39();
    		
            LOGGER.trace("IInside LifeEngageEApp component saveEApp...transactions.getgetLastSyncDate()"
                    + transactions.getLastSyncDate());
            // Store to a temp variable to set it back to response.
            offlineIdentifier = transactions.getOfflineIdentifier();
            
            JSONObject jsonObject = new JSONObject(json);
            
            JSONObject transObject = jsonObject.has("TransactionData")?jsonObject.getJSONObject("TransactionData"):new JSONObject();
            String typeVariableObj = transObject.has("AllocationType")?transObject.getString("AllocationType"):StringUtils.EMPTY;
			
            
            // Do not process eApp data if already done
            if (auditRepository.isTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                throwSystemException(true, ErrorConstants.LE_SYNC_ERR_102, Constants.PROCESSED_TRANSACTION);
            }
			
			// Added for Pentest fix
			 if(org.springframework.util.StringUtils.hasText(transactions.getStatus()) && transactions.getStatus().equals(AgreementStatusCodeList.Submitted.toString())){
            	if (!transactions.getKey7().equalsIgnoreCase(pendingUW)) {
            	if (auditRepository.checkTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                	throw new CookieTheftException(Constants.PROCESSED_TRANSACTION);
                }
              }
            }
			
            if (transactions.getStatus() != null
                    && transactions.getStatus().equals(AgreementStatusCodeList.Cancelled.toString())) {
                isDeleteRequest = Boolean.TRUE;
            }


            long start_time = System.currentTimeMillis();
            transactions = (Transactions) saveEAppHolder.parseJson(json);
            long end_time = System.currentTimeMillis();
            long difference = end_time-start_time;
            LOGGER.info("*****Eapp Smooks Time in Millis ******"+difference);

          
            List<Transactions> transactions2=new ArrayList<Transactions>();
            transactions2.add(transactions);
            LOGGER.trace("after saveEAppHolder.parseJson ...transactions.getLastSyncDate()"
                    + transactions.getLastSyncDate());
            if (isDeleteRequest) {
                transactions.setStatus(AgreementStatusCodeList.Cancelled.toString());
            }
            
            if(typeVariableObj.equalsIgnoreCase("rm_reassign")){
            	String identifier = transactions.getCustomerId();
        		String newTrId = transactions.getProposal().getTransactionKeys().getKey22();
        		
            	String leadId = agentRepository.retrieveLeadIdForFna(requestInfo, identifier);
            	List<Agreement> agreementList = agentRepository.retrieveAgreement(requestInfo, leadId);
            	if(!agreementList.isEmpty()){
            		
            	for(Agreement agreement:agreementList){
            		
            		InsuranceAgreement insAgr = (InsuranceAgreement)agreement;
            		transactions.setProposal(insAgr);
            		
            		Transactions newTransaction = buildTransactionWithAgreementData(transactions);
            		newTransaction.setKey40(typeVariableObj);
            		newTransaction.setKey39(newAgentId);
            		newTransaction.setKey22(newTrId);
            		
                	String eappReassignIdentifier = newAgentId+String.valueOf(idGenerator.generate(EAPP));
            		
            		String proposalstatus = eAppRepository.saveEApp(newTransaction, requestInfo, insAgr);
            		
            		LifeEngageComponentHelper.createResponseStatus(newTransaction, proposalstatus, successMessage, null);

            		newTransaction.setOfflineIdentifier(offlineIdentifier);

                    auditRepository.savelifeEngagePayloadAudit(newTransaction, requestInfo.getTransactionId(), json,
                            lifeEngageAudit,"");
                    eAppResponse = saveEAppHolder.parseBO(newTransaction);
            	}
            	
            	}
            	else{
            		LifeEngageComponentHelper.createResponseStatus(transactions, Constants.SUCCESS, successMessage, null);
                    eAppResponse = saveEAppHolder.parseBO(transactions);
            	}
            }
            else{
            // Fetching agreement By transTracking Id - Fix for duplicate Eapp when key4 not returned properly
            InsuranceAgreement agreement = null;
            if (null != transactions.getTransTrackingId()) {
                agreement =
                        eAppRepository.retrieveEappByTransTrackingId(transactions.getTransTrackingId(),
                                transactions.getAgentId(), requestInfo.getTransactionId());
            }
            if (agreement != null  || StringUtils.isNotEmpty(transactions.getProposalNumber())) {
            	if(agreement != null){
                transactions.setProposalNumber(String.valueOf(agreement.getIdentifier()));
            	}
                transactions.setMode(Constants.STATUS_UPDATE);
                successMessage = SUCCESS_MESSAGE_UPDATE;
            } else {
                transactions.setProposalNumber(transactions.getAgentId()+String.valueOf(idGenerator.generate(E_APP)));
                transactions.setMode(Constants.STATUS_SAVE);
                successMessage = SUCCESS_MESSAGE_SAVE;
            }

            // Run STP rules on final submit
            executeStpRules(transactions, json);

            onBeforeSave(transactions, requestInfo);

            final String proposalstatus = eAppRepository.saveEApp(transactions, requestInfo, agreement);

            onAfterSave(transactions);
            if (Constants.REJECTED.equals(proposalstatus)) {
                successMessage = REJECTED_MESSAGE;
            }

            LifeEngageComponentHelper.createResponseStatus(transactions, proposalstatus, successMessage, null);

            transactions.setOfflineIdentifier(offlineIdentifier);

            auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit,"");
            eAppResponse = saveEAppHolder.parseBO(transactions);
            }
            
            end_time = System.currentTimeMillis();
            difference = end_time-initial_time;
            LOGGER.info("*****Eapp Complete Save Time in Millis ******"+difference);

            
        } catch (DaoException e) {
        	
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_104);
            eAppResponse = saveEAppHolder.parseBO(transactions);

        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_103);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
            eAppResponse = saveEAppHolder.parseBO(transactions);
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
						ErrorConstants.LE_SYNC_ERR_105);
			} else if (ErrorConstants.LE_SYNC_ERR_102.equals(e.getErrorCode())) {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						LifeEngageComponentHelper.SUCCESS,
						SUCCESS_MESSAGE_SAVE, null);

			} else {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						Constants.FAILURE, e.getMessage(), e.getErrorCode());
			}
            eAppResponse = saveEAppHolder.parseBO(transactions);
			
        } catch (CookieTheftException e) {
            LOGGER.error("CookieTheftException", e);
            throw new CookieTheftException(Constants.PROCESSED_TRANSACTION);
        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_100);
            eAppResponse = saveEAppHolder.parseBO(transactions);
        }
        LOGGER.trace("Inside LifeEngageEApp component eAppResponse : eAppResponse" + eAppResponse);
        return eAppResponse;
    }
    
    private Transactions buildTransactionWithAgreementData(Transactions transactions) throws BusinessException, ParseException{
    	List<Transactions> transList = new ArrayList<Transactions>();
    	transList.add(transactions);
    	String Respjson = getRetrieveEAppHolder().parseBO(transList);
    	JSONObject jsonObj = new JSONObject(Respjson);
    	JSONArray jsonRqArray = jsonObj
				.getJSONArray(TRANSACTIONS);
    	JSONObject jsonTransactionObj = jsonRqArray.getJSONObject(0);
    	Transactions newTransaction = (Transactions) saveEAppHolder.parseJson(jsonTransactionObj.toString());
    	return newTransaction;
    }
    
	@Override
    protected void onBeforeSave(final Transactions transactions, final RequestInfo requestInfo) throws ParseException {    
        transactions.setCreationDateTime(LifeEngageComponentHelper.getCurrentdate());
        transactions.setModifiedTimeStamp(LifeEngageComponentHelper.getCurrentdate());
        // Changes done for audit table
        transactions.setKey21(transactions.getProposal().getTransactionKeys().getKey21());
    }
	
	@Override
	public String retrieveEApp(RequestInfo requestInfo, JSONObject jsonObj) throws BusinessException, ParseException {
        String response = null;
        final List<GeneraliTransactions> transactionsList = new ArrayList<GeneraliTransactions>();
        if (jsonObj.has(LifeEngageComponentHelper.KEY_4) && jsonObj.getString(LifeEngageComponentHelper.KEY_4) != null
                && !"".equals(jsonObj.getString(LifeEngageComponentHelper.KEY_4))
                && !jsonObj.getString(LifeEngageComponentHelper.KEY_4).equals(Constants.NULL)) {
            GeneraliTransactions responseTransactions = null;
            final Transactions requestPayload = (Transactions) getRetrieveEAppHolder().parseJson(jsonObj.toString());
            try {

                responseTransactions =
                         eAppRepository.retrieveEAppWithMemo(requestPayload.getProposalNumber(), requestInfo.getTransactionId());
                LOGGER.trace("Inside LifeEngageEApp component retrieveEApp : responseTransactions"
                        + responseTransactions);

                // illustration id retrieve

                final Set<Agreement> referenceAgreementList =
                        responseTransactions.getProposal().getReferencedAgreement();
                String key3 = "";
                for (Agreement list : referenceAgreementList) {
                    if (ContextTypeCodeList.ILLUSTRATION.equals(list.getContextId())) {
                        key3 = list.getIdentifier().toString();
                    }

                }
                responseTransactions.setIllustrationId(key3);
                if(responseTransactions.getProposal() != null && responseTransactions.getProposal().getTransactionKeys() != null && responseTransactions.getProposal().getTransactionKeys().getKey21() != null 
                        && !responseTransactions.getProposal().getTransactionKeys().getKey21().trim().isEmpty()){
                    updateTxnWithMemos(responseTransactions);   
                }
                
            } catch (Exception e) {
                LOGGER.error("Unable to retrieve EApp :" + e.getMessage());
                responseTransactions = new GeneraliTransactions();
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage()
                        + " : " + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
            }

            LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), responseTransactions, true);
            transactionsList.add(responseTransactions);
            
            response = getRetrieveEAppHolder().parseBO(transactionsList);
            response = response.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
            
            LOGGER.trace("LifeEngageEAppComponent.retrieveEApp: responseTransactions" + responseTransactions);
            
            
        }
        return response;
    }
	
	@Override
	@Transactional(rollbackFor = { Exception.class }, readOnly = true)
	public String retrieveAllEApp(RequestInfo requestInfo, JSONObject jsonObj)
	        throws BusinessException, ParseException {

        final Transactions transactions = (Transactions) getRetrieveEAppHolder().parseJson(jsonObj.toString());
        String eAppMapToReturn = "{\"Transactions\":[]}";
        if (jsonObj.has(LifeEngageComponentHelper.KEY_10)
                && jsonObj.getString(LifeEngageComponentHelper.KEY_10) != null) {
            if (LifeEngageComponentHelper.FULL_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                List<GeneraliTransactions> txnsList = new ArrayList<GeneraliTransactions>();
                // Retrieve All details for all proposals for an agent
                txnsList = eAppRepository.retrieveAllWithMemo(transactions, requestInfo, true);
                
                GeneraliComponentRepositoryHelper.buildTransactionsList(txnsList, jsonObj);
                for(GeneraliTransactions txn : txnsList){
                    if(txn.getProposal() != null && txn.getProposal().getTransactionKeys() != null && txn.getProposal().getTransactionKeys().getKey21() != null 
                            && !txn.getProposal().getTransactionKeys().getKey21().trim().isEmpty()){
                        updateTxnWithMemos(txn);   
                    }
                }
                eAppMapToReturn = getRetrieveEAppHolder().parseBO(txnsList);
            } else if (LifeEngageComponentHelper.BASIC_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                List<Transactions> txnsList = new ArrayList<Transactions>();
                // Retrieve Only the basic details required for listing
                transactions.setProposalNumber(null);
                txnsList = eAppRepository.retrieveAll(transactions, requestInfo, false);
                LifeEngageComponentHelper.buildTransactionsList(txnsList, jsonObj);
                eAppMapToReturn = getRetrieveEAppListHolder().parseBO(txnsList);
            }
        }
        eAppMapToReturn = eAppMapToReturn.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
        return eAppMapToReturn;
    }

    private void updateTxnWithMemos(GeneraliTransactions txn) {
        final Transactions transactions = new Transactions();
        Response<List<AppianMemoDetails>> memoDetails = null;
        transactions.setKey21(txn.getProposal().getTransactionKeys().getKey21());
        String memoType = GeneraliConstants.RETRIEVE;
        memoDetails = bpmRepository.retrieveMemoDetails(transactions,memoType);
        txn.setAppianMemoDetails(memoDetails.getType());
    
    }

    @Override
    public String lockOrUnlock(RequestInfo requestInfo, JSONObject jsonObj) {
        String responseString = StringUtils.EMPTY;
        GeneraliTransactions responseTransactions = new GeneraliTransactions();
        StatusData statusData = new StatusData();
        try{
            GeneraliTransactions txn = new GeneraliTransactions();
            GeneraliComponentRepositoryHelper.buildTransaction(txn, jsonObj);
            responseTransactions = eAppRepository.lockOrUnlock(requestInfo, txn);
        }catch(BusinessException e){
            statusData.setStatus(Constants.FAILURE);
            statusData.setStatusCode(Constants.FAILURE_CODE);
            statusData.setStatusMessage(e.getMessage());
            responseTransactions.setStatusData(statusData);
        }
        responseString = GAOLockUnlockHolder.parseBO(responseTransactions);
        return responseString;
    }


}
