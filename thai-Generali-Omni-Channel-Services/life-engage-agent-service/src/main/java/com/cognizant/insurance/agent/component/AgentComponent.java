package com.cognizant.insurance.agent.component;

/*import com.cognizant.insurance.agent.domain.agent.Agent;*/
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.response.Response;

/**
 * The Interface AgentComponent.
 */
public interface AgentComponent {
	
	/**
	 * Retrieve agent by code.
	 *
	 * @param request the agent request
	 * @return the response
	 */
	Response<GeneraliAgent> retrieveAgentByCode(Request<GeneraliAgent> request);

}
