/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * A code list indicating the degree to which data for the object in question is
 * considered complete.
 * 
 * @author 301350
 * 
 * 
 */
public enum DataCompleteCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Full,
    /**
     * 
     * 
     * 
     * 
     */
    Partial,
    /**
     * 
     * 
     * 
     * 
     */
    ReadOnly,
    /**
     * 
     * 
     * 
     * 
     */
    Removed
}
