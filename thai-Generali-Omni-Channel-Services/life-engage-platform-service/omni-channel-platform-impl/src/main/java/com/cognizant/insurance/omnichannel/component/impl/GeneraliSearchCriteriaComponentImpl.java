package com.cognizant.insurance.omnichannel.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.impl.LifeEngageSearchCriteriaComponentImpl;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.documentandcommunication.Communication;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliEAppRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliFNARepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliIllustrationRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliLMSRepository;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;
import com.cognizant.insurance.searchcriteria.SearchCriteriaResponse;

/**
 * The Class class GeneraliSearchCriteriaComponentImpl
 * 
 * @author 481774
 * 
 */

public class GeneraliSearchCriteriaComponentImpl extends LifeEngageSearchCriteriaComponentImpl {

    /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = "TransactionData";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";

    /** The Constant FNA. */
    private static final String FNA = "FNA";

    /** The Constant LMS. */
    private static final String LMS = "LMS";

    private static final String LMS_RELATED_TRANS_COUNT = "LMSRelatedTransCount";

    /** The retrieve by filter holder. */
    @Autowired
    @Qualifier("retrieveByFilterMapping")
    private LifeEngageSmooksHolder retrieveByFilterHolder;
    @Autowired
    @Qualifier("retrieveFullDetailsByFilterMapping")
    private LifeEngageSmooksHolder retrieveFullDetailsByFilterHolder;
    @Autowired
	@Qualifier("retrieveFilterlmsChoosePartyMapping")
	private LifeEngageSmooksHolder retrieveLMSChoosePartyHolder;

    /** The eapp repository. */
    @Autowired
    private GeneraliEAppRepository eAppRepository;

    /** The illustration repository. */
    @Autowired
    private GeneraliIllustrationRepository illustrationRepository;

    /** The fna repository. */
    @Autowired
    private GeneraliFNARepository fnaRepository;

    /** The lms repository. */
    @Autowired
    private GeneraliLMSRepository lmsRepository;

    /** The retrieve by count holder. */
    @Autowired
    @Qualifier("retrieveByCountMapping")
    private LifeEngageSmooksHolder retrieveByCountHolder;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.omnichannel.component.impl.GeneraliSearchCriteriaComponentImpl#retrieveByFilter(com.cognizant
     * .insurance. request.vo.RequestInfo, org.json.JSONArray)
     */
    @Override
    public String retrieveByFilter(final RequestInfo requestInfo, final JSONArray jsonRqArray) {
        Transactions transactions = new Transactions();
        String response = null;
        String agentId = null;
        String branchId = null;
        String agentType = StringUtils.EMPTY;
        boolean fullDetails=true;
        boolean choosparty = false;
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        try {
            if (jsonRqArray != null) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);
                    JSONObject jsonTransactionData = null;
                    SearchCriteria searchCriteria = new SearchCriteria();
                    agentId = jsonTransactionsObj.getString(Constants.KEY11);
                    branchId = jsonTransactionsObj.getString(Constants.KEY25);
                    if (agentId != null && !("").equals(agentId)) {
                        if(jsonTransactionsObj.has(GeneraliConstants.KEY37)){
                            agentType = jsonTransactionsObj.getString(GeneraliConstants.KEY37);
                            if(agentType.equals(GeneraliConstants.GAO)){
                                searchCriteria.setGaoId(jsonTransactionsObj.getString(GeneraliConstants.KEY36));
                                searchCriteria.setGaoOfficeCode(jsonTransactionsObj.getString(GeneraliConstants.KEY35));
                            }else if(agentType.equals(GeneraliConstants.BRANCH_ADMIN)){
                                searchCriteria.setBranchAdminId(jsonTransactionsObj.getString(GeneraliConstants.KEY36));
                            }
                            searchCriteria.setAgentType(agentType);
                        }
                        ArrayList<String> modesList = new ArrayList<String>();
                        ArrayList<String> selectedIdList = new ArrayList<String>();
                        if (!(jsonTransactionsObj.isNull(TRANSACTIONDATA))) {
                            jsonTransactionData = jsonTransactionsObj.getJSONObject(TRANSACTIONDATA);
                            if(LifeEngageComponentHelper.BASIC_DETAILS.equals(jsonTransactionsObj.getString(LifeEngageComponentHelper.KEY_10))){
                            	fullDetails=false;
                            }
                            final JSONObject searchCriteriaRqJson =
                                    jsonTransactionData.getJSONObject(Constants.SEARCHCRITERIAREQUEST);
                            JSONArray jsonModesArray = searchCriteriaRqJson.getJSONArray(Constants.MODES);
                            JSONArray jsonIdsArray =null;
							if (searchCriteriaRqJson.has(Constants.IDS)) {
								jsonIdsArray = searchCriteriaRqJson
										.getJSONArray(Constants.IDS);
							}
							for (int j = 0; j < jsonModesArray.length(); j++) {
								//To set the choose party flag.
								if (GeneraliConstants.CHOOSEPARTY
										.equals((String) jsonModesArray.get(j))) {
									choosparty = true;
								}
								modesList.add((String) jsonModesArray.get(j));

							}
							if (jsonIdsArray != null) {
								for (int k = 0; k < jsonIdsArray.length(); k++) {
									selectedIdList.add((String) jsonIdsArray
											.get(k));

								}
							}
                            String command = searchCriteriaRqJson.getString(Constants.COMMAND);
                            searchCriteria.setCommand(command);
                            searchCriteria.setValue(searchCriteriaRqJson.getString(Constants.VALUE));
                            searchCriteria.setAgentId(agentId);
                            searchCriteria.setModes(modesList);
                            searchCriteria.setSelectedIds(selectedIdList);
                            if(branchId != null && !("").equals(branchId)) {
                            	searchCriteria.setBranchId(branchId);
                            }
                            if (Constants.LISTING_DASHBOARD.equals(command)) {
                                transactionsList = retrieveFilter(requestInfo, searchCriteria);
                            if(LMS.equals(modesList.get(0)) || LMS_RELATED_TRANS_COUNT.equals(modesList.get(0))){    
                                for(Transactions transaction : transactionsList){
                                	Set<Communication> communicationList = transaction.getCustomer().getReceivesCommunication();
                                	Date appointmentDate = null;
                                	for(Communication communication : communicationList){
                                	    List<Date> appointmentDateList = new ArrayList<Date>();
                                		if(communication.getInteractionType().toString().equals(Constants.APPOINTMENT)){
                                			Set<Communication> followUps = communication.getFollowsUp();
                                			for(Communication followUp : followUps){
                                				String metTimeString = followUp.getPlannedTime();
                                				SimpleDateFormat sf = new SimpleDateFormat("HH:mm");
                                				Date metTime = sf.parse(metTimeString);
                                				Date combinedDate = getDateTime(followUp.getCommunicationStatusDate(),metTime);
                                				appointmentDateList.add(combinedDate);
                                			}
                                			if(!appointmentDateList.isEmpty()){
                                			Collections.sort(appointmentDateList);
                                			for(Date appointment : appointmentDateList){
                                			if(appointment.after(new Date())){
                                			appointmentDate = appointment;
                                			break;
                                			}
                                			}
                                			}
                                		}
                                	}
                                	if(appointmentDate != null){
                                	SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                                	String appointmentDateString = sf.format(appointmentDate);
                                	transaction.getCustomer().getTransactionKeys().setKey19(appointmentDateString);
                                	}
                                }
                            }
                            } else if (GeneraliConstants.RELATED_TRANSACTIONS.equals(command)) {

                                transactionsList = retrieveRelatedTransactions(requestInfo, searchCriteria);
                            }
                        }
                    } else {
                        throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, Constants.AGENTID_MISSING);
                    }
                }
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (Exception e) {
            LOGGER.error("Unable to retrieve listing details for retrieve filter :" + e.getMessage());
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

        } finally {
        	// If mode selected is choose party, then we need to retrieve whole details of lead using the 
			//transtrackingid passed from UI.
			if (choosparty) {
				response = retrieveLMSChoosePartyHolder.parseBO(transactionsList);
			} else if(fullDetails){
        		 response = retrieveFullDetailsByFilterHolder.parseBO(transactionsList);
        	}else {
        		 response = retrieveByFilterHolder.parseBO(transactionsList);
			}
           

        }

        return response;
    }
    
    private Date getDateTime(Date date, Date time) {
    	Calendar aDate = Calendar.getInstance();
        aDate.setTime(date);

        Calendar aTime = Calendar.getInstance();
        aTime.setTime(time);

        aDate.set(Calendar.HOUR_OF_DAY, aTime.get(Calendar.HOUR_OF_DAY));
        aDate.set(Calendar.MINUTE, aTime.get(Calendar.MINUTE));

        return aDate.getTime();
    }

    /**
     * Retrieve RelatedTransactions.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    protected List<Transactions> retrieveRelatedTransactions(final RequestInfo requestInfo,
            final SearchCriteria searchCriteria) throws BusinessException, ParseException {
        LOGGER.trace("Inside GeneraliSearchCriteriaComponentImpl retrieveFilter : requestInfo" + requestInfo);
        ArrayList<String> modeList = searchCriteria.getModes();
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        if (modeList.size() > 0) {
            String mode = modeList.get(0);
            if (E_APP.equals(mode)) {
                searchCriteria.setType(mode);
                //Retrieving eApp based on illustration transTrackingId
                transactionsList = eAppRepository.retrieveByFilterEAppRelatedTransactions(requestInfo, searchCriteria);
                
            }/* else if (ILLUSTRATION.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = illustrationRepository.retrieveByFilterIllustration(requestInfo, searchCriteria);
            }*/ else if (FNA.equals(mode)) {
            	//Retrieve all FNA based on LMS transTrackingId
                searchCriteria.setType(mode);
                transactionsList = fnaRepository.retrieveRelatedTransactions(requestInfo, searchCriteria);
            } else if (LMS.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = eAppRepository.retrieveEappForLMS(requestInfo, searchCriteria);
            }else if (GeneraliConstants.CHOOSEPARTY.equals(mode)) {
            	// To fetch the Choose party (Lead Details) for an Illustration 
            	//using the TranstrackingId
            	searchCriteria.setType(mode);
            	transactionsList = lmsRepository
					.retrieveRelatedLMSforIllustration(requestInfo,
							searchCriteria);
		}
            LOGGER.trace("Inside GeneraliSearchCriteriaComponentImpl retrieveFilter ");
        }
        return transactionsList;	
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.omnichannel.component.impl.GeneraliSearchCriteriaComponentImpl#retrieveByFilterCount(
     * com.cognizant.insurance .request.vo.RequestInfo, org.json.JSONArray)
     */
    @Override
    public String retrieveByFilterCount(final RequestInfo requestInfo, final JSONArray jsonRqArray) {
        Transactions transactions = new Transactions();
        SearchCriteriaResponse searchCriteriaResponse = new SearchCriteriaResponse();
        String response = null;
        String agentId = null;
        String branchId = null;
        String agentType = null;
        try {
            if (jsonRqArray != null) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);
                    JSONObject jsonTransactionData = null;
                    SearchCriteria searchCriteria = new SearchCriteria();
                    agentId = jsonTransactionsObj.getString(Constants.KEY11);
                    branchId = jsonTransactionsObj.getString(Constants.KEY25);
                    if (agentId != null && !("").equals(agentId)) {
                        ArrayList<String> modesList = new ArrayList<String>();
                        ArrayList<String> selectedIdList = new ArrayList<String>();
                        if (!(jsonTransactionsObj.isNull(TRANSACTIONDATA))) {
                            jsonTransactionData = jsonTransactionsObj.getJSONObject(TRANSACTIONDATA);
                            if(jsonTransactionData.has("agentType")) {
                            	agentType = jsonTransactionData.getString("agentType");
                            } else {
                            	if(branchId != null && !("").equals(branchId)) {
                            		agentType ="BranchUser"; 
                            	}
                            	else{
                            		agentType ="IOIS"; 
                            	}
                            }
                             
                                                        
                            final JSONObject searchCriteriaRqJson =
                                    jsonTransactionData.getJSONObject(Constants.SEARCHCRITERIAREQUEST);
                            JSONArray jsonModesArray = searchCriteriaRqJson.getJSONArray(Constants.MODES);
                            JSONArray jsonIdsArray =null;
							if (searchCriteriaRqJson.has(Constants.IDS)) {
								jsonIdsArray = searchCriteriaRqJson
										.getJSONArray(Constants.IDS);
							}
                            
                            for (int j = 0; j < jsonModesArray.length(); j++) {
                                modesList.add((String) jsonModesArray.get(j));

                            }
							if (jsonIdsArray != null) {
								for (int k = 0; k < jsonIdsArray.length(); k++) {
									selectedIdList.add((String) jsonIdsArray
											.get(k));

								}
							}
                            String command = searchCriteriaRqJson.getString(Constants.COMMAND);
                            searchCriteria.setCommand(command);
                            searchCriteria.setValue(searchCriteriaRqJson.getString(Constants.VALUE));
                            searchCriteria.setAgentId(agentId);
                            searchCriteria.setBranchId(branchId);
                            searchCriteria.setModes(modesList);
                            searchCriteria.setSelectedIds(selectedIdList);
                            searchCriteria.setAgentType(agentType);
                            
                            if ((Constants.LANDING_DASHBOARD_COUNT).equals(command)) {
                                searchCriteriaResponse = retrieveByCount(requestInfo, searchCriteria);
                            } else if (GeneraliConstants.RELATED_TRANSACTIONS.equals(command)) {
                                searchCriteriaResponse = relatedTransactionCount(requestInfo, searchCriteria);
                            }
                        }
                    } else {
                        throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, Constants.AGENTID_MISSING);
                    }
                }
            }
            transactions.setAgentId(agentId);
            transactions.setSearchCriteriaResponse(searchCriteriaResponse);
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (Exception e) {
            LOGGER.error("Unable to retrieve count by each module :" + e.getMessage());
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

        } finally {
            response = retrieveByCountHolder.parseBO(transactions);

        }

        return response;
    }

    /**
     * RelatedTransactionCount
     * 
     * @param requestInfo
     * @param searchCriteria
     * @return
     */
    private SearchCriteriaResponse relatedTransactionCount(RequestInfo requestInfo, SearchCriteria searchCriteria) {

        LOGGER.trace("Inside GeneraliSearchCriteriaComponentImpl relatedTransactionCount : requestInfo" + requestInfo);
        SearchCountResult searchCountResult = null;
        SearchCriteriaResponse searchCriteriaResponse = new SearchCriteriaResponse();
        List<SearchCountResult> searchCountResultList = new ArrayList<SearchCountResult>();
        ArrayList<String> modeList = searchCriteria.getModes();

        if (modeList.size() > 0) {
            for (String mode : modeList) {

                if (LMS.equals(mode)) {
                    searchCriteria.setType(FNA);
                    searchCountResult = new SearchCountResult();
                    searchCountResult = fnaRepository.retrieveFNACountForLMS(requestInfo, searchCriteria);
                    searchCountResultList.add(searchCountResult);

                    searchCriteria.setType(ILLUSTRATION);
                    searchCountResult = new SearchCountResult();
                    searchCountResult =
                            illustrationRepository.retrieveIllustrationCountForLMS(requestInfo, searchCriteria);
                    searchCountResultList.add(searchCountResult);

                    searchCriteria.setType(E_APP);
                    searchCountResult = new SearchCountResult();
                    searchCountResult = eAppRepository.retrieveEAppCountForLMS(requestInfo, searchCriteria);
                    searchCountResultList.add(searchCountResult);

                }
            }
        }
        searchCriteriaResponse.setModeResults(searchCountResultList);
        return searchCriteriaResponse;
    }

    /**
     * Retrieve filter.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    protected List<Transactions> retrieveFilter(final RequestInfo requestInfo, final SearchCriteria searchCriteria)
            throws BusinessException, ParseException {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
        ArrayList<String> modeList = searchCriteria.getModes();
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        if (modeList.size() > 0) {
            String mode = modeList.get(0);
            if (E_APP.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = eAppRepository.retrieveByFilterEApp(requestInfo, searchCriteria);
            } else if (ILLUSTRATION.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = illustrationRepository.retrieveByFilterIllustration(requestInfo, searchCriteria);
            } else if (FNA.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = fnaRepository.retrieveByFilterFNA(requestInfo, searchCriteria);
            } else if (LMS.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = lmsRepository.retrieveByFilterLMS(requestInfo, searchCriteria);
            } else if (LMS_RELATED_TRANS_COUNT.equals(mode)) {
                searchCriteria.setType(LMS);
                transactionsList = lmsRepository.retrieveLMSRelatedTransCount(requestInfo, searchCriteria);
            }
            LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter ");
        }
        return transactionsList;
    }
}
