package com.cognizant.insurance.domain.agreement;

import java.util.List;
import java.util.Set;

public class BPMRiderDetails {
	
	private List<RiderSequence> riderSequenceList;
	
	private Set<Coverage> includesCoverage;

	public List<RiderSequence> getRiderSequenceList() {
		return riderSequenceList;
	}

	public void setRiderSequenceList(List<RiderSequence> riderSequenceList) {
		this.riderSequenceList = riderSequenceList;
	}

	public Set<Coverage> getIncludesCoverage() {
		return includesCoverage;
	}

	public void setIncludesCoverage(Set<Coverage> includesCoverage) {
		this.includesCoverage = includesCoverage;
	}		

}
