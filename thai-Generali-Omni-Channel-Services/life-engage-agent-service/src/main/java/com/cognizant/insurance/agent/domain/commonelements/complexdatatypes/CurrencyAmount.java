package com.cognizant.insurance.agent.domain.commonelements.complexdatatypes;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * A monetary amount including the currency code used.
 * 
 * e.g: $150.00 USD
 */

@Entity
public class CurrencyAmount {

    /** The id. */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * The amount expressed as a currency value.
     */
    private BigDecimal amount;

    /**
     * The code identifying the currency type.
     * 
     * REFERENCE: ISO-4217
     * http://www.iso.org/iso/support/faqs/faqs_widely_used_standards
     * /widely_used_standards_other/currency_codes.htm
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,	CascadeType.REMOVE })
    private ExternalCode currencyCode;

    /**
     * Gets the amount.
     * 
     * @return Returns the amount.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Gets the currencyCode.
     * 
     * @return Returns the currencyCode.
     */
    public ExternalCode getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets The amount.
     * 
     * @param amount
     *            The amount to set.
     */
    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Sets The currencyCode.
     * 
     * @param currencyCode
     *            The currencyCode to set.
     */
    public void setCurrencyCode(final ExternalCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }
}
