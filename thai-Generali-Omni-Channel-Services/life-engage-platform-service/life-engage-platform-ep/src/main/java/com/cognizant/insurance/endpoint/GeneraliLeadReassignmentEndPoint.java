/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.AgentProfileService;
import com.cognizant.insurance.services.LifeEngageSyncService;

// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageSyncEndPoint.
 * 
 * @author 300797
 */
@Controller
@RequestMapping("/gliLeadReassignment")
public class GeneraliLeadReassignmentEndPoint {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliLeadReassignmentEndPoint.class);

    /** The life engage sync service. */
    @Autowired
    private LifeEngageSyncService lifeEngageSyncService;
    
    @Autowired
	AgentProfileService agentProfileService;

    /**
     * Save.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/leadReassignmentSave", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String save(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageSyncService.save(json);
            }
        }.execute(json);
    }
    
    @RequestMapping(value = "/validateLogin", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final String validateReassignLogin(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return agentProfileService.validateReassignLogin(json);
            }
        }.execute(json);
    }
	
	@RequestMapping(value = "/retrieveAgentProfile", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final String retrieveAgentsForReassign(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return agentProfileService.retrieveAgentsForReassign(json);
            }
        }.execute(json);
    }
	
	@RequestMapping(value = "/retrieveLeads", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final String retrieveLeads(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return agentProfileService.retrieveLeads(json);
            }
        }.execute(json);
    }
	
	@RequestMapping(value = "/reassignLeadsToAgents", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String reassignLeadsToAgents(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	return lifeEngageSyncService.save(json);
            	
            }
        }.execute(json);
    }
   

    /**
     * Retrieve all data.
     * 
     * @param json
     *            the json
     * @return the string
     *//*
    @RequestMapping(value = "/leadReassignmentRetrieve", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String retrieve(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageSyncService.retrieve(json);
            }
        }.execute(json);
    }
   

    *//**
     * Retrieve All customer Data for an Agent.
     * 
     * @param json
     *            the json
     * @return the string
     *//*
    @RequestMapping(value = "/leadReassignmentRetrieveAll", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String retrieveAll(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageSyncService.retrieveAll(json);
            }
        }.execute(json);
    }*/
 

    /**
     * Gets the life engage sync service.
     * 
     * @return the lifeEngageSyncService
     */
    public final LifeEngageSyncService getLifeEngageSyncService() {
        return lifeEngageSyncService;
    }

    /**
     * Sets the life engage sync service.
     * 
     * @param lifeEngageSyncService
     *            the lifeEngageSyncService to set
     */
    public final void setLifeEngageSyncService(final LifeEngageSyncService lifeEngageSyncService) {
        this.lifeEngageSyncService = lifeEngageSyncService;
    }

}
