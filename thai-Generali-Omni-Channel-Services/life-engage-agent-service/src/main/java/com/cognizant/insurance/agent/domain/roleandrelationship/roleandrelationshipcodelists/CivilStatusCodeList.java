/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists;

/**
 * A code list representing various civil status' with regards to a civil
 * relationship.
 * 
 * @author 301350
 * 
 * 
 */
public enum CivilStatusCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Married,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * 
     * 
     * 
     * 
     */
    Divorced,
    /**
     * 
     * 
     * 
     * 
     */
    Widowed,
    /**
     * 
     * 
     * 
     * 
     */
    LegallySeparated,
    /**
     * This de facto type of marriage exists in long-standing (the length of
     * time varies) relationships that have never had a marriage ceremony.
     * Common Law marriages have legal consequence in some countries but not
     * others.
     * 
     * Some countries use this term to also include Civil Unions, but Civil
     * Union stands alone in this enumeration.
     * 
     * 
     * 
     */
    CommonLawDefacto,
    /**
     * When a married couple is not legally separated but is living apart.
     * 
     * 
     * 
     * 
     * 
     */
    Estranged,
    /**
     * Civil Union is similar to marriage, which grants rights similar to
     * marriage to same-gender couples.
     * 
     * 
     * 
     */
    CivilUnion
}
