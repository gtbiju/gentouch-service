package com.cognizant.insurance.agent;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "AGENT_ACHIEVEMENT")
public class AgentAchievementDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id	
	@Column(name = "ACHIEVE_NO")
	private int number;
	
	@Column(name = "ACHIEVEMENT_NAME", columnDefinition = "VARCHAR(30)")
	private String achievementName;
	
	@Column(name = "POSITION",columnDefinition = "CHAR(2)")
	private String position;
	
	@Column(name = "FREQUENCY",columnDefinition = "VARCHAR(30)")
	private String frequency;
	
	@Column(name = "EFFECTIVE_DATE",columnDefinition = "CHAR(8)")
	private String effectiveDate;
	
	@Column(name = "STATUS",columnDefinition = "CHAR(9)")
	private String status;

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getAchievementName() {
		return achievementName;
	}

	public void setAchievementName(String achievementName) {
		this.achievementName = achievementName;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	

}
