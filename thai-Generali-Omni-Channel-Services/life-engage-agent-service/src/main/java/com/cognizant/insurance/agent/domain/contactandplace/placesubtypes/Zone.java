/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.placesubtypes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;



/**
 * This concept represents an unformalized geographical zone: the composition is
 * this zone is only specified by the zone name.
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("ZONE")
public class Zone extends CountryElement {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1037236090195705420L;
}
