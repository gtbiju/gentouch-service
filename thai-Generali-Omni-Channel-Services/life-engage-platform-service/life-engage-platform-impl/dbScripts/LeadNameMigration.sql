
IF OBJECT_ID ( 'dbo.LeadNameMigration', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.LeadNameMigration; 
GO  
CREATE PROCEDURE dbo.LeadNameMigration   
   
AS   
	
	DECLARE @ID bigint
	DECLARE @firstName nvarchar(200)
	DECLARE @lastName nvarchar(200)
	DECLARE @strngLen int
	DECLARE @newFirstName nvarchar(200)
	DECLARE @lastNamelength int
	
	DECLARE firstname_lastname CURSOR FOR   
	select id,givenname,surname from PartyName  where surname is null and givenname is not null and givenname<>'';

	OPEN firstname_lastname  
  
	FETCH NEXT FROM firstname_lastname   
	INTO @ID, @firstName,@lastname
  
	WHILE @@FETCH_STATUS = 0  
	BEGIN
    
	SET @strngLen = CHARINDEX(' ', @firstName)
	IF @strngLen > 0
		SET @lastName = REVERSE(SUBSTRING(REVERSE(@firstName),0,CHARINDEX(' ',REVERSE(@firstName))));
		SET @lastNamelength = LEN(@lastName);
		SET @newFirstName = SUBSTRING(@firstName,0,LEN(@firstName) - @lastNamelength);
		
		update PartyName  set givenname=@newFirstName,surname=@lastName where id =@ID
		
	
	PRINT @ID;
	PRINT @firstName;
	PRINT @lastName;
	PRINT @newFirstName;
	SET @ID=null;
	SET @firstName=null;
	SET @lastName=null;
	SET @newFirstName=null;
	FETCH NEXT FROM firstname_lastname Into @id, @firstName,@lastname--Get Next Lead
	END
	CLOSE firstname_lastname;  
	DEALLOCATE firstname_lastname;  
GO  
--Executing Procedure
exec dbo.LeadNameMigration 

