package com.cognizant.insurance.omnichannel.bridge;

import org.apache.commons.collections.map.LRUMap;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.domain.codes.CodeLocaleLookUp;
import com.cognizant.insurance.domain.codes.CodeLookUp;
import com.cognizant.insurance.service.repository.CodeLookUpRepository;
import com.cognizant.insurance.service.repository.GeneraliCodeLookUpRepository;

@Component
public class OmniContextBridge implements OmniContextBridgeServices,
		ApplicationContextAware {

	private static ApplicationContext context;

	@Autowired
	private GeneraliCodeLookUpRepository codeLookUpRepository;

	private LRUMap dataCashe;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.context = applicationContext;
	}

	public static OmniContextBridgeServices services() {
		return context.getBean(OmniContextBridgeServices.class);
	}

	@Override
	public String getValueForCode(String code, String language, String type) {
		String response = "";
		if (dataCashe == null) {
			dataCashe = new LRUMap(100);
		}
		String key = code + language + type;
		if (dataCashe.containsKey(key)) {
			response = (String) dataCashe.get(key);
		} else {
			CodeLocaleLookUp lookUp = codeLookUpRepository.findByCodeAndType(
					code, language, type);
			if (lookUp != null) {
				response = lookUp.getValue();
				dataCashe.put(key, response);
			}

		}
		return response;
	}

}
