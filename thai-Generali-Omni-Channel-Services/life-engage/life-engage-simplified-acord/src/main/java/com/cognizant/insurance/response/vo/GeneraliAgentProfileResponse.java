package com.cognizant.insurance.response.vo;

import java.util.List;

import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;

public class GeneraliAgentProfileResponse extends AgentProfileResponse{

	private List<GeneraliAgent> activeAgentList;
	
	private List<GeneraliAgent> terminatedAgentList;
	
	private List<Customer> customerList;
	
	private List<String> generaliAgentIdList;


	public List<String> getGeneraliAgentIdList() {
		return generaliAgentIdList;
	}

	public void setGeneraliAgentIdList(List<String> generaliAgentIdList) {
		this.generaliAgentIdList = generaliAgentIdList;
	}

	public List<Customer> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<Customer> customerList) {
		this.customerList = customerList;
	}

	public List<GeneraliAgent> getActiveAgentList() {
		return activeAgentList;
	}

	public void setActiveAgentList(List<GeneraliAgent> activeAgentList) {
		this.activeAgentList = activeAgentList;
	}

	public List<GeneraliAgent> getTerminatedAgentList() {
		return terminatedAgentList;
	}

	public void setTerminatedAgentList(List<GeneraliAgent> terminatedAgentList) {
		this.terminatedAgentList = terminatedAgentList;
	}

}
