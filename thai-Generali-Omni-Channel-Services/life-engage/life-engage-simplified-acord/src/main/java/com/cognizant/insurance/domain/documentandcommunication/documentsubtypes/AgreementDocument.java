/**
 * 
 */
package com.cognizant.insurance.domain.documentandcommunication.documentsubtypes;



import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.cognizant.insurance.domain.documentandcommunication.Document;


/**
 * This concept represents any document related to an agreement. This could be
 * in the form of a printed policy form, declarations page, coverage letter or
 * other types of agreement documents. 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("AGREEMENTDOCUMENT")
public class AgreementDocument extends Document {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6516850161227719973L;
}
