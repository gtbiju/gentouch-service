/**
 * 
 */
package com.cognizant.insurance.domain.agreement.partyroleinagreement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;

/**
 * This concept states the active role of the producer, who is responsible for
 * getting the business.
 * 
 * e.g. Agent, Agency, Broker, Brokerage, etc.
 * 
 * e.g. The producer who negotiates a contract of reinsurance between the
 * reassured and the reinsurer - developed into the Producing Broker (the broker
 * who handles the business on behalf of the reassured) and the Placing Broker
 * (the broker, who may be the same as the placing broker or a correspondent
 * broker, who negotiates and places the business on rates / terms agreed with
 * the reinsurer).
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("AGENT")
public class AgreementProducer extends PartyRoleInAgreement {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -9215467308146301605L;
    
    /**
     * The percent of interest the agreement producer has with regards to a
     * related agreement. Collectively, the interests of one or more agreement
     * producers should total 100%.
     * 
     * e.g. If one agreement has only one agreement producer, that producer has
     * 100% interest.
     * 
     * e.g. If one agreement has two agreement producers, each would have its
     * respective interest in the agreement (50% and 50%; 60% and 40%; etc.).
     * 
     * 
     * 
     * 
     */
    private Float interestPercentage;

    /**
     * Gets the interestPercentage.
     * 
     * @return Returns the interestPercentage.
     */
    public Float getInterestPercentage() {
        return interestPercentage;
    }

    /**
     * Sets The interestPercentage.
     * 
     * @param interestPercentage
     *            The interestPercentage to set.
     */
    public void setInterestPercentage(final Float interestPercentage) {
        this.interestPercentage = interestPercentage;
    }

}
