/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class class CategoryType.
 */

@Entity
@DiscriminatorValue("Category")
public class CategoryType extends Type {

    /** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "category")
    private Set<ProductSpecification> productSpecifications;

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2182594528943276604L;

    /**
     * The Constructor.
     */
    public CategoryType() {
        super();
    }

    /**
     * The Constructor.
     * 
     * @param id
     *            the id
     */
    public CategoryType(final String id) {
        super(id);
    }

    /**
     * @param productSpecifications
     *            the productSpecifications to set
     */
    public void setProductSpecifications(Set<ProductSpecification> productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    /**
     * @return the productSpecifications
     */
    public Set<ProductSpecification> getProductSpecifications() {
        return productSpecifications;
    }
}
