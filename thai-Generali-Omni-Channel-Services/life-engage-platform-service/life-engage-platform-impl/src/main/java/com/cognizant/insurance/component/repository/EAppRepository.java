/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.component.repository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.agreement.component.AgreementComponent;
import com.cognizant.insurance.component.repository.helper.LifeEngageComponentRepositoryHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Appointee;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.commonclasses.GLISelectedOption;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.party.component.PartyComponent;
import com.cognizant.insurance.product.component.ProductComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

/**
 * The Class EAppRepository.
 * 
 * @author 300797
 */
// @Repository
public class EAppRepository {

    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;

    /** The agreement component. */
    @Autowired
    private AgreementComponent agreementComponent;

    /** The party component. */
    @Autowired
    private PartyComponent partyComponent;

    /** The agreement component. */
    @Autowired
    private ProductComponent productComponent;

    /** The Constant STP_Pass. */
    private static final String STP_PASS = "Pass";

    /** The conflict resolution. */
    @Value("${le.platform.service.conflictResolution}")
    private String conflictResolution;

    /** The source of truth. */
    @Value("${le.platform.service.sourceOfTruth}")
    private String sourceOfTruth;

    /** The e app limit. */
    @Value("${le.platform.service.eApp.fetchLimit}")
    private Integer eAppLimit;
    
    @Value("${le.platform.service.fileUploadPath}")
    private String fileUploadPath;

    @Value("${le.platform.service.slash}")
    private String slash;

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(EAppRepository.class);

    /**
     * Save EAppR.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the string
     * @throws ParseException
     *             the parse exception
     */
    public String saveEApp(final Transactions transactions, final RequestInfo requestInfo,InsuranceAgreement savedInsuranceAgreement) throws ParseException {
        LOGGER.trace("Inside EAppRepository saveEApp : requestInfo" + requestInfo);
        final Request<Agreement> request =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) transactions.getProposal();
        insuranceAgreement.setContextId(ContextTypeCodeList.EAPP);

        if (Constants.UPDATE.equals(transactions.getMode())) {
            String status = onAgreementUpdate(transactions, requestInfo,savedInsuranceAgreement);
            if (!Constants.SUCCESS.equals(status)) {
                return status;
            }

        } else {
            insuranceAgreement.setCreationDateTime(transactions.getCreationDateTime());
        }

        onBeforeSave(transactions, requestInfo);

        request.setType(insuranceAgreement);

        saveAgreementProductSpecification(transactions, requestInfo, insuranceAgreement);
        
      
        insuranceAgreement.setRequirements(transactions.getRequirements());
      
        agreementComponent.saveAgreement(request);

        LifeEngageComponentRepositoryHelper.saveInsured(transactions.getInsured(), requestInfo, insuranceAgreement,
                entityManager, ContextTypeCodeList.EAPP, partyComponent);

        saveProposer(transactions.getProposer(), requestInfo, insuranceAgreement);

        saveBeneficiary(transactions.getBeneficiaries(), requestInfo, insuranceAgreement);

        saveAppointee(transactions.getAppointee(), requestInfo, insuranceAgreement);

        LifeEngageComponentRepositoryHelper.savePayer(transactions.getPayer(), requestInfo, insuranceAgreement,
                entityManager, ContextTypeCodeList.EAPP, partyComponent);

        saveInsuredPreviousPolicyList(transactions.getInsuredPreviousPolicyList(), requestInfo,
                transactions.getInsured());

        return insuranceAgreement.getStatusCode().toString();
    }

    /**
     * On agreement update.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the string
     */
    protected String onAgreementUpdate(Transactions transactions, RequestInfo requestInfo,InsuranceAgreement savedInsuranceAgreement) {
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.UPDATE_OP);
        InsuranceAgreement agreement = null;
        if(null != savedInsuranceAgreement) // We already have agreement with transTracking Id
            agreement = savedInsuranceAgreement;
        else {
        agreement =
                LifeEngageComponentRepositoryHelper.createReplacingAgreement(transactions, requestInfo, entityManager,
                        agreementComponent, ContextTypeCodeList.EAPP, statusList);
        }
        // Checking whether the status code is cancelled, if so return directly.
        if (agreement.getStatusCode() != null && agreement.getStatusCode().equals(AgreementStatusCodeList.Cancelled)) {
            return AgreementStatusCodeList.Cancelled.toString();

        }

        // Checking whether the server data is latest when source of truth is server, if so reject the client data and
        // send rejected status
        if (Constants.TRUE.equals(conflictResolution) && Constants.SERVER.equals(sourceOfTruth)) {
            Date modifiedTimeStamp = agreement.getModifiedDateTime();

            // Changed the modified date to lastsync date

            if (!(transactions.getLastSyncDate().toString().equals(Constants.DEFAULT_DATE))
                    && ((null != modifiedTimeStamp) && (modifiedTimeStamp.after(transactions.getLastSyncDate())))) {

                LOGGER.trace("Inside if case and rejected the case..");
                return Constants.REJECTED;

            }
        }
        Set<Agreement> replacingAgeements =
                LifeEngageComponentRepositoryHelper.updateAgreementStatus(agreement, agreementComponent, requestInfo,
                        entityManager, ContextTypeCodeList.EAPP);
        transactions.getProposal().setCreationDateTime(agreement.getCreationDateTime());
        transactions.getProposal().setReplacesAgreements(replacingAgeements);

        Set<AgreementDocument> agreementDocumentsSet = new HashSet<AgreementDocument>();
        if (replacingAgeements != null) {
            agreementDocumentsSet = LifeEngageComponentRepositoryHelper.createReplaceDocument(replacingAgeements);
            LOGGER.trace("Previous agreementDocument Size=" + agreementDocumentsSet.size());
            // While copying all the Previous documents will be set in the current agreement
            transactions.getProposal().setRelatedDocument(agreementDocumentsSet);

        }
        
        // replacingAgeements is set as the previous obj to current agreement - so its requirements needs to be compared
        // wth current

        if (replacingAgeements != null) {
            LifeEngageComponentRepositoryHelper.createReplacingRequirementSet(transactions, replacingAgeements,
                    fileUploadPath + slash);
            // While copying all the Previous requirements will be set in the current agreement
        }

        
        return Constants.SUCCESS;
    }

    /**
     * On before save.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     */
    protected void onBeforeSave(Transactions transactions, RequestInfo requestInfo) {
        final InsuranceAgreement insuranceAgreement = transactions.getProposal();
        // Since ModifiedDate is using for the chunk retrieve and SourceOfTruth functionality
        insuranceAgreement.setModifiedDateTime(transactions.getModifiedTimeStamp());

        insuranceAgreement.setTransactionId(requestInfo.getTransactionId());
        insuranceAgreement.setIdentifier(transactions.getProposalNumber());
        /**
         * In case of an update flow , The existing agreement will be suspended and will be created as a new agreeement.
         * While suspending and creating a new agreement , if any documents exist in the suspended agreement, all those
         * documents are retrieved in order to set in the current agreement.
         * 
         * The deletion of documents is handled seperatly using uploadDocs service
         */

        // Set Status as "Initial" on intermediate saves, "Final" on document submission and "Proposed" if STP succeeds.
        if (null != transactions.getStatus()
                && transactions.getStatus().equals(AgreementStatusCodeList.Cancelled.toString())) {

            insuranceAgreement.setStatusCode(AgreementStatusCodeList.Cancelled);
        } else if (null != insuranceAgreement.getIsFinalSubmit() && insuranceAgreement.getIsFinalSubmit()
                && (STP_PASS.equals(transactions.getSTPStatus()))) {
            insuranceAgreement.setStatusCode(AgreementStatusCodeList.Proposed);
        } else if (insuranceAgreement.getRelatedDocument() != null
                && insuranceAgreement.getRelatedDocument().size() > 0) {

            insuranceAgreement.setStatusCode(AgreementStatusCodeList.Final);
        } 
        
        else {
            insuranceAgreement.setStatusCode(AgreementStatusCodeList.Initial);
        }

        if (transactions.getAgentId() != null) {
            insuranceAgreement.setAgentId(transactions.getAgentId());
        }

    }

    /**
     * Save Product specification related to the agreement.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    protected void saveAgreementProductSpecification(final Transactions transactions, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement) {
        if (null != transactions.getProductId()) {
            final Request<ProductSearchCriteria> requestProductSpec =
                    new JPARequestImpl<ProductSearchCriteria>(entityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
            ProductSearchCriteria searchCriteria = new ProductSearchCriteria();
            searchCriteria.setId(Long.parseLong(transactions.getProductId()));
            requestProductSpec.setType(searchCriteria);
            final Response<ProductSpecification> productSpec = productComponent.getProductById(requestProductSpec, "1");
            insuranceAgreement.setIsBasedOn(productSpec.getType());
        }
    }

    /**
     * Save insured prevous policy list.
     * 
     * @param previousHistoryInsuredList
     *            the previous history insured list
     * @param requestInfo
     *            the request info
     * @param insured
     *            the insured
     */
    protected void saveInsuredPreviousPolicyList(final Set<Insured> previousHistoryInsuredList,
            final RequestInfo requestInfo, final Insured insured) {
        final Request<PartyRoleInAgreement> previousHistoryInsuredRequest =
                new JPARequestImpl<PartyRoleInAgreement>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        if (CollectionUtils.isNotEmpty(previousHistoryInsuredList)) {
            for (Insured previousHistoryInsured : previousHistoryInsuredList) {
                if (insured != null) {
                    previousHistoryInsured.setPlayerParty(insured.getPlayerParty());
                }
                previousHistoryInsured.setTransactionId(requestInfo.getTransactionId());
                previousHistoryInsuredRequest.setType(previousHistoryInsured);
                partyComponent.savePartyRoleInAgreement(previousHistoryInsuredRequest);
            }
        }
    }

    /**
     * Save appointee.
     * 
     * @param appointee
     *            the appointee
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    protected void saveAppointee(final Appointee appointee, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement) {

        if (appointee != null) {
        	if (null!=appointee.getSelectedOptions() && !appointee.getSelectedOptions().isEmpty()){
        		Set<UserSelection> selectedOptions = appointee.getSelectedOptions();
        		for (UserSelection selection : selectedOptions){
        			UUID idOne = UUID.randomUUID();
        		 	String uuid_lsd = idOne.getLeastSignificantBits()+"";
        		 	uuid_lsd = uuid_lsd.replace("-", "");
        			selection.setId(Long.parseLong(uuid_lsd));
        			for (GLISelectedOption options : selection.getSelectedOptions()){
        				UUID idOne1 = UUID.randomUUID();
            		 	String uuid_lsd1 = idOne1.getLeastSignificantBits()+"";
            		 	uuid_lsd1 = uuid_lsd1.replace("-", "");
            			options.setId(Long.parseLong(uuid_lsd1));
        			}
        		}
        	}
            appointee.setIsPartyRoleIn(insuranceAgreement);
            appointee.setTransactionId(requestInfo.getTransactionId());
            final Request<PartyRoleInAgreement> appointeeRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, ContextTypeCodeList.EAPP,
                            requestInfo.getTransactionId());
            appointeeRequest.setType(appointee);
            partyComponent.savePartyRoleInAgreement(appointeeRequest);
        }
    }

    /**
     * Save beneficiary.
     * 
     * @param beneficiaries
     *            the beneficiaries
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    protected void saveBeneficiary(final Set<Beneficiary> beneficiaries, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement) {

        if (CollectionUtils.isNotEmpty(beneficiaries)) {
            final Request<PartyRoleInAgreement> beneficiaryRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, ContextTypeCodeList.EAPP,
                            requestInfo.getTransactionId());
            for (Beneficiary beneficiary : beneficiaries) {
            	if (null!=beneficiary.getSelectedOptions() && !beneficiary.getSelectedOptions().isEmpty()){
            		Set<UserSelection> selectedOptions = beneficiary.getSelectedOptions();
            		for (UserSelection selection : selectedOptions){
            			UUID idOne = UUID.randomUUID();
            		 	String uuid_lsd = idOne.getLeastSignificantBits()+"";
            		 	uuid_lsd = uuid_lsd.replace("-", "");
            			selection.setId(Long.parseLong(uuid_lsd));
            			if (null!=selection.getSelectedOptions() && !selection.getSelectedOptions().isEmpty()){
            				for (GLISelectedOption options : selection.getSelectedOptions()){
                				UUID idOne1 = UUID.randomUUID();
                    		 	String uuid_lsd1 = idOne1.getLeastSignificantBits()+"";
                    		 	uuid_lsd1 = uuid_lsd1.replace("-", "");
                    			options.setId(Long.parseLong(uuid_lsd1));
                			}
            			}
            		}
            	}
                beneficiary.setIsPartyRoleIn(insuranceAgreement);
                beneficiary.setTransactionId(requestInfo.getTransactionId());
                beneficiaryRequest.setType(beneficiary);
                partyComponent.savePartyRoleInAgreement(beneficiaryRequest);
            }
        }
    }

    /**
     * Save proposer.
     * 
     * @param proposer
     *            the proposer
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    protected void saveProposer(final AgreementHolder proposer, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement) {

        if (proposer != null) {
            proposer.setIsPartyRoleIn(insuranceAgreement);
            proposer.setTransactionId(requestInfo.getTransactionId());
            final Request<PartyRoleInAgreement> proposerRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, ContextTypeCodeList.EAPP,
                            requestInfo.getTransactionId());
            proposerRequest.setType(proposer);
            partyComponent.savePartyRoleInAgreement(proposerRequest);
        }
    }

    /**
     * Retrieve eApp.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param transactionId
     *            the transaction id
     * @return the response
     */
    public Transactions retrieveEApp(final String proposalNumber, final String transactionId) {
        LOGGER.trace("LifeEngageEAppRepository.retrieveEApp: proposalNumber" + proposalNumber);
        LOGGER.trace("LifeEngageEAppRepository.retrieveEApp: transactionId" + transactionId);
        final Transactions transaction = new Transactions();
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE);

        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(proposalNumber);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementsResponse =
                agreementComponent.getInsuranceAgreement(agreementRequest, statusRequest);
        if (agreementsResponse.getType() != null) {
            final InsuranceAgreement agreement = (InsuranceAgreement) agreementsResponse.getType();
            transaction.setProposal(agreement);

            agreementRequest.setType(agreement);

            getAgreementDetails(transaction, agreementRequest, transactionId);

            if (transaction.getProposal() != null) {
                transaction.setProposalNumber(agreement.getIdentifier().toString());
            }
            transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
        }
        LOGGER.trace("LifeEngageEAppRepository.retrieveEApp: transaction" + transaction);
        return transaction;
    }

    /**
     * Gets the agreement details.
     * 
     * @param transaction
     *            the transaction
     * @param agreementRequest
     *            the agreement request
     * @param transactionId
     *            the transaction id
     * @return the agreement details
     */
    protected void getAgreementDetails(final Transactions transaction, final Request<Agreement> agreementRequest,
            final String transactionId) {
        final Response<Insured> insuredResponse = partyComponent.retrieveInsured(agreementRequest);
        transaction.setInsured(insuredResponse.getType());

        if (insuredResponse.getType() != null) {
            final Request<PartyRoleInAgreement> partyRoleInAgreementRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, ContextTypeCodeList.EAPP, transactionId);
            partyRoleInAgreementRequest.setType(insuredResponse.getType());
            final Response<Set<InsuranceAgreement>> previousAgreementResponse =
                    agreementComponent.getPreviousAgreements(partyRoleInAgreementRequest);
            transaction.setPreviousAgreements(previousAgreementResponse.getType());
        }

        final Response<AgreementHolder> proposerResponse = partyComponent.retrieveProposer(agreementRequest);
        transaction.setProposer(proposerResponse.getType());

        final Response<Set<Beneficiary>> beneficiariesResponse = partyComponent.retrieveBeneficiaries(agreementRequest);
        transaction.setBeneficiaries(beneficiariesResponse.getType());

        final Response<Appointee> appointeeResponse = partyComponent.retrieveAppointee(agreementRequest);
        transaction.setAppointee(appointeeResponse.getType());

        final Response<PremiumPayer> payerResponse = partyComponent.retrievePayer(agreementRequest);
        transaction.setPayer(payerResponse.getType());

    }

    /**
     * Retrieve all.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param isFullDetailsRequired
     *            the is full details required
     * @return the list
     */
    public List<Transactions> retrieveAll(final Transactions transactions, final RequestInfo requestInfo,
            final boolean isFullDetailsRequired) {
        LOGGER.trace("Inside EAppRepository retrieveAll : retrieveAll" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
         List<AgreementStatusCodeList> statusList =null;
        if (requestInfo.getFirstTimeSync()) {
        	statusList=getAgreementStatusCodesFor(Constants.FIRST_TIME_SYNC);
            
        } else {
        	statusList=getAgreementStatusCodesFor(Constants.RETRIEVE_ALL);
        }
        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setAgentId(transactions.getAgentId());
        insuranceAgreement.setLastSyncDate(requestInfo.getLastSyncDate());
        agreementRequest.setType(insuranceAgreement);
        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);
        Request<Integer> limitRequest = null;
        if (isFullDetailsRequired) {
            if (eAppLimit != null) {
                limitRequest =
                        new JPARequestImpl<Integer>(entityManager, ContextTypeCodeList.EAPP,
                                requestInfo.getTransactionId());
                limitRequest.setType(eAppLimit);
            }
        }
        final Response<List<Agreement>> agreementsResponse =
                agreementComponent.getAgreements(agreementRequest, statusRequest, limitRequest);
        final List<Agreement> agreements = agreementsResponse.getType();
        if (CollectionUtils.isNotEmpty(agreements)) {
            for (Agreement agreement : agreements) {
                transaction = new Transactions();
                transaction.setProposal((InsuranceAgreement) agreement);
                agreementRequest.setType(agreement);
                if (isFullDetailsRequired) {
                    getAgreementDetails(transaction, agreementRequest, requestInfo.getTransactionId());
                } else {
                    final Response<AgreementHolder> proposerResponse =
                            partyComponent.retrieveProposer(agreementRequest);
                    transaction.setProposer(proposerResponse.getType());
                    final Response<Insured> insuredResponse = partyComponent.retrieveInsured(agreementRequest);
                    transaction.setInsured(insuredResponse.getType());
                }
                if (transactions.getProposal() != null) {
                    transactions.setProposalNumber(agreement.getIdentifier().toString());
                }
                if (agreement.getModifiedDateTime() != null && !(agreement.getModifiedDateTime().toString().equals(""))) {
                    transaction.setLastSyncDate(agreement.getModifiedDateTime());
                } else if (agreement.getCreationDateTime() != null
                        && !("".equals(agreement.getCreationDateTime().toString()))) {
                    transaction.setLastSyncDate(agreement.getCreationDateTime());
                }
                // illustration id retrieve
                final Set<Agreement> referenceAgreementList = transaction.getProposal().getReferencedAgreement();
                String key3 = "";
                for (Agreement list : referenceAgreementList) {
                    if (ContextTypeCodeList.ILLUSTRATION.equals(list.getContextId())) {
                        key3 = list.getIdentifier().toString();
                    }
                }
                transaction.setIllustrationId(key3);
                transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }
        // LOGGER.trace("Inside EAppRepository retrieveAll : transactionsList" + transactionsList.size());
        return transactionsList;
    }

    /**
     * Retrieve eApp.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param transactionId
     *            the transaction id
     * @return the response
     */
    public Transactions getStatus(final String proposalNumber, final String transactionId) {

        LOGGER.trace("LifeEngageEAppRepository.getStatus: proposalNumber" + proposalNumber);
        LOGGER.trace("LifeEngageEAppRepository.getStatus: transactionId" + transactionId);

        final Transactions transaction = new Transactions();

        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.GET_STATUS);

        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(proposalNumber);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementsResponse = agreementComponent.getStatus(agreementRequest, statusRequest);

        if (agreementsResponse.getType() != null) {
            final InsuranceAgreement agreement = (InsuranceAgreement) agreementsResponse.getType();
            transaction.setProposal(agreement);
            transaction.setProposalNumber(proposalNumber);

        }
        LOGGER.trace("LifeEngageEAppRepository.getStatus: transaction" + transaction);
        return transaction;
    }

    /**
     * Retrieve by count e app.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the search count result
     */
    public SearchCountResult retrieveByCountEApp(final RequestInfo requestInfo, final SearchCriteria searchCriteria) {

        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_COUNT);
        statusRequest.setType(statusList);

        final Response<SearchCountResult> searchCountResponse =
                agreementComponent.getAgreementCount(searchCriteriatRequest, statusRequest);
        SearchCountResult searchCountResult = null;
        if (searchCountResponse.getType() != null) {
            searchCountResult = (SearchCountResult) searchCountResponse.getType();

        }

        return searchCountResult;

    }

    /**
     * Retrieve by filter e app.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the list
     */
    public List<Transactions> retrieveByFilterEApp(final RequestInfo requestInfo, final SearchCriteria searchCriteria) {
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_FILTER);
       
        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Response<List<Agreement>> searchCriteriatResponse =
                agreementComponent.getAgreementByFilter(searchCriteriatRequest, statusRequest);
        final List<Agreement> agreements = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(agreements)) {
            for (Agreement agreement : agreements) {
                transaction = new Transactions();
                transaction.setProposal((InsuranceAgreement) agreement);
                transaction.setProposalNumber(agreement.getIdentifier().toString());
                transaction.setType(searchCriteria.getType());
                transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }
        LOGGER.trace("Inside EAppRepository retrieveByFilterEApp : transactionsList" + transactionsList.size());
        return transactionsList;
    }
    
    public List<Transactions> retrieveIdsByEmailEApp(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = new Transactions();
        String proposalID = null;
        ArrayList<String> idsList = new ArrayList<String>();
        idsList = searchCriteria.getSelectedIds();
        for (int i = 0; i < idsList.size(); i++) {
            proposalID = idsList.get(i);            
            transaction = retrieveEApp(proposalID, "");
            transactionsList.add(transaction);
        }        
        
        LOGGER.trace("Inside EAppRepository retrieveIdsByEmailEApp : transactionsList" + transactionsList.size());
        return transactionsList;
    }

    /**
     * Gets the entity manager.
     * 
     * @return the entityManager
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Sets the entity manager.
     * 
     * @param entityManager
     *            the entityManager to set
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Gets the agreement component.
     * 
     * @return the agreementComponent
     */
    public final AgreementComponent getAgreementComponent() {
        return agreementComponent;
    }

    /**
     * Sets the agreement component.
     * 
     * @param agreementComponent
     *            the agreementComponent to set
     */
    public final void setAgreementComponent(final AgreementComponent agreementComponent) {
        this.agreementComponent = agreementComponent;
    }

    /**
     * Gets the party component.
     * 
     * @return the partyComponent
     */
    public final PartyComponent getPartyComponent() {
        return partyComponent;
    }

    /**
     * Sets the party component.
     * 
     * @param partyComponent
     *            the partyComponent to set
     */
    public final void setPartyComponent(final PartyComponent partyComponent) {
        this.partyComponent = partyComponent;
    }

    /**
     * Gets the product component.
     * 
     * @return the productComponent
     */
    public final ProductComponent getProductComponent() {
        return productComponent;
    }

    /**
     * Sets the product component.
     * 
     * @param productComponent
     *            the productComponent to set
     */
    public final void setProductComponent(final ProductComponent productComponent) {
        this.productComponent = productComponent;
    }
    
    protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(String operation){
    	List<AgreementStatusCodeList> statusCodeLists=new ArrayList<AgreementStatusCodeList>();
    	if(Constants.FIRST_TIME_SYNC.equals(operation)||Constants.RETRIEVE.equals(operation)||
    			Constants.RETRIEVE_BY_FILTER.equals(operation)||Constants.RETRIEVE_BY_COUNT.equals(operation)){    		
    		statusCodeLists.add(AgreementStatusCodeList.Initial);
    		statusCodeLists.add(AgreementStatusCodeList.Final);
    		statusCodeLists.add(AgreementStatusCodeList.Proposed);
    	}else if(Constants.RETRIEVE_ALL.equals(operation)||
    			Constants.UPDATE_OP.equals(operation)){
    		statusCodeLists.add(AgreementStatusCodeList.Initial);
    		statusCodeLists.add(AgreementStatusCodeList.Final);
    		statusCodeLists.add(AgreementStatusCodeList.Proposed);
    		statusCodeLists.add(AgreementStatusCodeList.Cancelled);
    	}else if(Constants.GET_STATUS.equals(operation)){
    		statusCodeLists.add(AgreementStatusCodeList.Suspended);
    	}
    	return statusCodeLists;
    }
    
    /**
     * @param transTrackingId
     * @return
     */
    public InsuranceAgreement
            retrieveEappByTransTrackingId(String transTrackingId, String agentId, String transactionId) {
        InsuranceAgreement agreementToReturn = null;
        final List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();
        statusList.add(AgreementStatusCodeList.InProgress);
        statusList.add(AgreementStatusCodeList.Final);
        statusList.add(AgreementStatusCodeList.Review);
        statusList.add(AgreementStatusCodeList.Cancelled);

        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setTransTrackingId(transTrackingId);
        insuranceAgreement.setAgentId(agentId);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementResponse =
                agreementComponent.retrieveAgreementForTransTrackingId(agreementRequest, statusRequest);
        final InsuranceAgreement agreement = (InsuranceAgreement) agreementResponse.getType();
        if (agreement != null) {
            agreementToReturn = (InsuranceAgreement) agreementResponse.getType();
        }
        return agreementToReturn;
    }

}
