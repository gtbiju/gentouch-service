package com.cognizant.insurance.omnichannel.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.impl.LifeEngageFNAComponentImpl;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.FNARepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliAgentProfileRepository;
import com.cognizant.insurance.omnichannel.service.GeneraliLifeEngageFNAComponent;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;

public class GeneraliLifeEngageFNAComponentImpl extends LifeEngageFNAComponentImpl implements GeneraliLifeEngageFNAComponent{
	
	/** The Constant FNA. */
	private static final String REASSIGN = "reassign";
	
	/** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;

    /** The fna repository. */
    @Autowired
    private FNARepository fnaRepository;

    /** The save fna holder. */
    @Autowired
    @Qualifier("fnaSaveMapping")
    private LifeEngageSmooksHolder saveFNAHolder;

    /** The retrieve FNA holder. */
    @Autowired
    @Qualifier("fnaRetrieveMapping")
    private LifeEngageSmooksHolder retrieveFNAHolder;

    /** The retrieve fna holder. */
    @Autowired
    @Qualifier("fnaRetrieveBasicDtlsMapping")
    private LifeEngageSmooksHolder retrieveBasicDtlsFNAHolder;

    /** The retrieve fna holder. */
    @Autowired
    @Qualifier("fnaRetrieveFullDtlsMapping")
    private LifeEngageSmooksHolder retrieveFullDtlsFNAHolder;

    /** The id generator. */
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;

    /** The Constant SUCCESS_MESSAGE_SAVE. */
    private static final String SUCCESS_MESSAGE_SAVE = "FNA Saved";

    /** The Constant SUCCESS_MESSAGE_UPDATE. */
    private static final String SUCCESS_MESSAGE_UPDATE = "FNA Updated";

    /** The Constant REJECTED_MESSAGE. */
    private static final String REJECTED_MESSAGE = "FNA Rejected";

    /** The Constant FNA. */
    private static final String FNA = "FNA";

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliLifeEngageFNAComponentImpl.class);
	
	 @Autowired
	 GeneraliAgentProfileRepository agentRepository;
	
	@Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String saveFNA(final String json, final RequestInfo requestInfo, final LifeEngageAudit lifeEngageAudit) {

        LOGGER.info(":::: Entering saveFNA in LifeEngageFNAComponentImpl 1 : input json >>" + json);

        String fnaResponse = StringUtils.EMPTY;
        final Transactions transactions = new Transactions();
        String successMessage;
        String offlineIdentifier;
        try {
            LifeEngageComponentHelper.parseTrasactionDetails(json, transactions, false);
            // Store to a temp variable to set it back to response.
            offlineIdentifier = transactions.getOfflineIdentifier();

            // Do not process FNA data if already done
            if (auditRepository.isTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                throwSystemException(true, ErrorConstants.LE_SYNC_ERR_102, Constants.PROCESSED_TRANSACTION);
            }
			/**
			 * Update flow was depending on fnaId alone. Instead will verify
			 * whether the data exist for transTrackingId and AgentId
			 * combination. If so , considered as update flow. Taken this as a
			 * bug fix when multple save was happenning for same record with
			 * same transTrackingId, eg: data gets saved in backend, but not
			 * getting updated in UI and hence the same record comes again
			 * without fnaId
			 **/
            
            JSONObject jsonObject = new JSONObject(json);
            
            JSONObject transObject = jsonObject.has("TransactionData")?jsonObject.getJSONObject("TransactionData"):new JSONObject();
            String typeVariableObj = transObject.has("AllocationType")?transObject.getString("AllocationType"):StringUtils.EMPTY;
			//Added a check to bypass this code if allocation type is not available in request;
			
				
			Transactions retrieveTransaction = new Transactions();
			GoalAndNeed goalAndNeed = new GoalAndNeed();
			if(typeVariableObj.equalsIgnoreCase("rm_reassign")){
				String identifier = transactions.getCustomerId();
				String leadId = agentRepository.retrieveLeadIdForFna(requestInfo, identifier);
				List<GoalAndNeed> goalAndNeedList = agentRepository.retrieveGoalAndNeed(requestInfo, leadId);
				//goalAndNeed = agentRepository.retrieveGoalAndNeed(requestInfo, leadId);
				if(!goalAndNeedList.isEmpty()){
				for(GoalAndNeed fna : goalAndNeedList){
				//retrieveTransaction.setGoalAndNeed(fna);
				Transactions responseTransaction = new Transactions();
				LifeEngageComponentHelper.parseTrasactionDetails(json, responseTransaction, false);
				responseTransaction.setGoalAndNeed(fna);
				Transactions fnaTransactions = fnaRepository.saveFNA(responseTransaction, requestInfo, json,fna);
				final String proposalstatus = fnaTransactions.getStatus();
				LifeEngageComponentHelper.createResponseStatus(responseTransaction, proposalstatus, SUCCESS_MESSAGE_UPDATE, null);

				responseTransaction.setOfflineIdentifier(offlineIdentifier);
				responseTransaction.setLastSyncDate(fnaTransactions.getLastSyncDate());
				
	            auditRepository.savelifeEngagePayloadAudit(responseTransaction, requestInfo.getTransactionId(), json,
	                    lifeEngageAudit,"");
	            fnaResponse = saveFNAHolder.parseBO(responseTransaction);
				}
				
				}
				else{
				LifeEngageComponentHelper.createResponseStatus(transactions, Constants.SUCCESS, SUCCESS_MESSAGE_UPDATE, null);
	            fnaResponse = saveFNAHolder.parseBO(transactions);
				}
			}
			else{ 
				if(StringUtils.isNotEmpty(transactions.getFnaId())
					|| StringUtils
							.isNotEmpty(transactions.getTransTrackingId())) {
				retrieveTransaction = fnaRepository.retrieveFNA(transactions,
						requestInfo);
			}
			
			if (retrieveTransaction != null
					&& retrieveTransaction.getGoalAndNeed() != null) {
				// If fna identifier exists ==> the mode is update
				goalAndNeed = (GoalAndNeed) retrieveTransaction
						.getGoalAndNeed();
				if (StringUtils.isEmpty(transactions.getFnaId())) {
					transactions.setFnaId(retrieveTransaction.getGoalAndNeed()
							.getIdentifier());
				}
				transactions.setMode("update");
				successMessage = SUCCESS_MESSAGE_UPDATE;
			} else {

				transactions.setFnaId(transactions.getAgentId()
						+ String.valueOf(idGenerator.generate(FNA)));
				transactions.setMode("save");
				successMessage = SUCCESS_MESSAGE_SAVE;
			}
            onBeforeSave(transactions);
            Transactions fnaTransactions = fnaRepository.saveFNA(transactions, requestInfo, json,goalAndNeed);
            onAfterSave(transactions);
            final String proposalstatus = fnaTransactions.getStatus();
            if (Constants.REJECTED.equals(proposalstatus)) {
                successMessage = REJECTED_MESSAGE;
            }
            LifeEngageComponentHelper.createResponseStatus(transactions, proposalstatus, successMessage, null);

            transactions.setOfflineIdentifier(offlineIdentifier);
            transactions.setLastSyncDate(fnaTransactions.getLastSyncDate());
            auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit,"");
            fnaResponse = saveFNAHolder.parseBO(transactions);
			}
            

        }
        catch (ParseException e) {
            LOGGER.error("ParseException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_101);
            fnaResponse = saveFNAHolder.parseBO(transactions);
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_104);
            fnaResponse = saveFNAHolder.parseBO(transactions);

        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                        + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_105);

			} else if (ErrorConstants.LE_SYNC_ERR_102.equals(e.getErrorCode())) {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						LifeEngageComponentHelper.SUCCESS,
						SUCCESS_MESSAGE_SAVE, null);

			} else {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						Constants.FAILURE, e.getMessage() + " : "
								+ getExceptionMessage(e), e.getErrorCode());
			}
            fnaResponse = saveFNAHolder.parseBO(transactions);

        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
            fnaResponse = saveFNAHolder.parseBO(transactions);

        }
        LOGGER.info(":::: Exiting saveFNA in LifeEngageFNAComponentImpl 1 : fnaResponse>>" + fnaResponse);
        return fnaResponse;
    }

}
