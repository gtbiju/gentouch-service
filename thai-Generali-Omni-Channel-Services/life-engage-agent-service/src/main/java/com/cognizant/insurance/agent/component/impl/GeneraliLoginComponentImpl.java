package com.cognizant.insurance.agent.component.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.generali.security.component.GeneraliLoginComponent;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.repository.UserRepository;

public class GeneraliLoginComponentImpl implements GeneraliLoginComponent {
	
	/** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliLoginComponentImpl.class);
    
    @Autowired
    private UserRepository userRepository;

    
	public final boolean validateAgentLicenseExpiry(String agentCode) {

		GeneraliAgent agent = getAgentDetails(agentCode);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String date = sdf.format(new Date());

		if (agent.getLicenseExpireDate().compareTo(date) < 0) {
			return true;
		}
		return false;

	}

	public final boolean validateAgentStatus(String agentCode) {

		GeneraliAgent agent = getAgentDetails(agentCode);

		if (!agent.getAgentStatus().equals("INFORCE")) {
			return true;
		}
		return false;

	}

	private final GeneraliAgent getAgentDetails(String agentCode) {

		LOGGER.debug("METHOD ENTRY : getAgentDetails ");

		LOGGER.debug("getAgentDetails userName>>" +agentCode);

		GeneraliAgent agent = userRepository.agentProfileLoginValidation(agentCode);

		LOGGER.debug("METHOD EXIT :getAgentDetails");
		return agent;
	}	
}
