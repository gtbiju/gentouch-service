package com.cognizant.insurance.omnichannel.utils;

public class RetirementData {

	private Long retirementFund;
	
	private Long retrSavings;
	
	private Long retrShorFall;
	
	private String categoryAxisSavings;
	
	private String categoryAxisFund;
	
	private String categoryAxisShortFall;

	public String getCategoryAxisShortFall() {
		return categoryAxisShortFall;

	}

	public void setCategoryAxisShortFall(String categoryAxisShortFall) {
		this.categoryAxisShortFall = categoryAxisShortFall;
	}

	public String getCategoryAxisFund() {
        return categoryAxisFund;
    }

    public void setCategoryAxisFund(String categoryAxisFund) {
        this.categoryAxisFund = categoryAxisFund;
    }

    public Long getRetirementFund() {
		return retirementFund;
	}

	public void setRetirementFund(Long retirementFund) {
		this.retirementFund = retirementFund;
	}

	public Long getRetrSavings() {
		return retrSavings;
	}

	public void setRetrSavings(Long retrSavings) {
		this.retrSavings = retrSavings;
	}

	public Long getRetrShorFall() {
		return retrShorFall;
	}

	public void setRetrShorFall(Long retrShorFall) {
		this.retrShorFall = retrShorFall;
	}

    public void setCategoryAxisSavings(String categoryAxisSavings) {
        this.categoryAxisSavings = categoryAxisSavings;
    }

    public String getCategoryAxisSavings() {
        return categoryAxisSavings;
    }
}
