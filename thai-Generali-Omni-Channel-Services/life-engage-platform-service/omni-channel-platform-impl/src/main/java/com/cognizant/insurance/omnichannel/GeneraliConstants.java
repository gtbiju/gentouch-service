package com.cognizant.insurance.omnichannel;

import java.math.BigDecimal;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

public final class GeneraliConstants {
	
    public static final String ACR_PDF_TEMPLATE_TYPE = "22007";

    public static final String EAPP_PDF_TEMPLATE_TYPE = "22004";

    public static final String ILLUSTRATION_PDF_TEMPLATE_TYPE = "22005";

    public static final String FNA_TEMPLATE_TYPE = "22006";
    
    public static final long CARRIER_CODE = 1L;
    
    public static final String RELATED_TRANSACTIONS = "RelatedTransactions";
    
	public static final String BPM_SPAJ = "bpm_spaj";

	public static final String BPM_QUESTIONNAIRE = "bpm_questionnaire";
	
	public static final String ADD_INSURED_PDF = "additionalInsured";
	public static final String MAIN_INSURED_PDF = "mainInsured";
	public static final String ACR_PDF = "acr";

	public static final String ADD_INSURED_PDF_ID = "1009";


	public static final String BPM_ILLUSTRATION = "bpm_illustration";
	
	public static final String JSON_EMPTY = "";
	
	public static final String JSON_EMPTY_ARRAY = "[]";
	
	public static final String JSON_EMPTY_OBJECT = "{}";
	
	public static final String KEY21 = "Key21";
	
	public static final String KEY20 = "Key20";
	
	public static final String KEY16 = "Key16";
	
	
	// Vietnam Release Two BulkUpload Related ValidationMeaages.
	
	public static final String EMPTY_CUSTOMER_NAME = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p hÃ¡Â»ï¿½ vÃƒÂ  tÃƒÂªn lÃƒÂ³t khÃƒÂ¡ch hÃƒÂ ng";//Please enter full Name
	public static final String INVALID_CUSTOMER_NAME = "HÃ¡Â»ï¿½ vÃƒÂ  tÃƒÂªn lÃƒÂ³t khÃƒÂ¡ch hÃƒÂ ng chÃ¡Â»â€° Ã„â€˜Ã†Â°Ã¡Â»Â£c chÃ¡Â»Â©a chÃ¡Â»Â¯ cÃƒÂ¡i vÃƒÂ  khoÃ¡ÂºÂ£ng trÃ¡ÂºÂ¯ng";//Last name can have only alphabets
	
	public static final String EMPTY_FIRST_NAME= "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p TÃƒÂªn hÃ¡Â»Â£p lÃ¡Â»â€¡"; //Please enter First NameÃ¢â‚¬ï¿½ 
	public static final String INVALID_FIRST_NAME="TÃƒÂªn chÃ¡Â»â€° Ã„â€˜Ã†Â°Ã¡Â»Â£c chÃ¡Â»Â©a cÃƒÂ¡c chÃ¡Â»Â¯ cÃƒÂ¡i";// First Name can only have alphabets
	public static final String INVALID_FIRST_NAME_LENGTH="TÃƒÂªn khÃƒÂ¡ch hÃƒÂ ng khÃƒÂ´ng vÃ†Â°Ã¡Â»Â£t quÃƒÂ¡ 60 kÃƒÂ½ tÃ¡Â»Â±";//Maximum of 60 characters only allowed for Customer first name
	
	public static final String EMPTY_LAST_NAME= "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p HÃ¡Â»ï¿½ hÃ¡Â»Â£p lÃ¡Â»â€¡"; //Please enter Last NameÃ¢â‚¬ï¿½ 
	public static final String INVALID_LAST_NAME="HÃ¡Â»ï¿½ chÃ¡Â»â€° Ã„â€˜Ã†Â°Ã¡Â»Â£c chÃ¡Â»Â©a cÃƒÂ¡c chÃ¡Â»Â¯ cÃƒÂ¡i";// Last Name can only have alphabets
	public static final String INVALID_LAST_NAME_LENGTH="HÃ¡Â»ï¿½ khÃƒÂ¡ch hÃƒÂ ng khÃƒÂ´ng vÃ†Â°Ã¡Â»Â£t quÃƒÂ¡ 60 kÃƒÂ½ tÃ¡Â»Â±";//Maximum of 60 characters only allowed for Customer last name
	
	public static final String EMPTY_GENDER = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p giÃ¡Â»â€ºi tÃƒÂ­nh";//Please enter gender
	
	public static final String INVALID_MOBILE_NUMBER = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p sÃ¡Â»â€˜ Ã„â€˜iÃ¡Â»â€¡n thoÃ¡ÂºÂ¡i hÃ¡Â»Â£p lÃ¡Â»â€¡"; //"Enter a valid mobile number".
	
	public static final String INVALID_EMAIL_ID = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p email hÃ¡Â»Â£p lÃ¡Â»â€¡";//Ã¢â‚¬Å“Enter a valid email idÃ¢â‚¬ï¿½
	
	public static final String INVALID_DOB = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p ngÃƒÂ y sinh hÃ¡Â»Â£p lÃ¡Â»â€¡ (dd/mm/yyyy)";//Please enter a valid date of birth
	
	public static final String EMPTY_POTENTIAL = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p tiÃ¡Â»ï¿½m nÃ„Æ’ng cÃ¡Â»Â§a khÃƒÂ¡ch hÃƒÂ ng";//Please enter potential
	
	public static final String EMPTY_STATE = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p TÃ¡Â»â€°nh/TP";//Please enter state

	
	public static final String EMPTY_SOURCE = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p nguÃ¡Â»â€œn";//Please enter source
	
	public static final String EMPTY_ACTION = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p hÃƒÂ nh Ã„â€˜Ã¡Â»â„¢ng tiÃ¡ÂºÂ¿p theo";//Please enter action
														//Please select a future date not more than 90 days
	public static final String INAVALID_MEETING_DATE = "NgÃƒÂ y giÃ¡Â»ï¿½ hÃ¡ÂºÂ¹n gÃ¡ÂºÂ·p tiÃ¡ÂºÂ¿p theo phÃ¡ÂºÂ£i lÃƒÂ  ngÃƒÂ y tÃ†Â°Ã†Â¡ng lai nhÃ†Â°ng khÃƒÂ´ng vÃ†Â°Ã¡Â»Â£t quÃƒÂ¡ 90 ngÃƒÂ y kÃ¡Â»Æ’ tÃ¡Â»Â« ngÃƒÂ y hÃƒÂ´m nay";
	
	public static final String EMPTY_DATE = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p ngÃƒÂ y (dd/mm/yyyy ) vÃƒÂ  giÃ¡Â»ï¿½ (hh:mm) hÃ¡ÂºÂ¹n gÃ¡ÂºÂ·p tiÃ¡ÂºÂ¿p theo hÃ¡Â»Â£p lÃ¡Â»â€¡ ";//Please enter a valid date- time.
	
	public static final String INAVALID_BANK_NAME = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p tÃƒÂªn ngÃƒÂ¢n hÃƒÂ ng";//Please enter bank name
	
	public static final String INAVALID_REFFER_NAME = "TÃƒÂªn ngÃ†Â°Ã¡Â»ï¿½i giÃ¡Â»â€ºi thiÃ¡Â»â€¡u chÃ¡Â»â€° Ã„â€˜Ã†Â°Ã¡Â»Â£c chÃ¡Â»Â©a chÃ¡Â»Â¯ cÃƒÂ¡i vÃƒÂ  khoÃ¡ÂºÂ£ng trÃ¡ÂºÂ¯ng"; //Referrer Name can only have alphabets and  spaces 
	
	public static final String EMPTY_CITY = "Please enter city";
	
	public static final String INVALID_REF_NAME = "TÃƒÂªn ngÃ†Â°Ã¡Â»ï¿½i giÃ¡Â»â€ºi thiÃ¡Â»â€¡u chÃ¡Â»â€° Ã„â€˜Ã†Â°Ã¡Â»Â£c chÃ¡Â»Â©a chÃ¡Â»Â¯ cÃƒÂ¡i vÃƒÂ  khoÃ¡ÂºÂ£ng trÃ¡ÂºÂ¯ng";
	
	public static final String SUSPENDED_TERMINATED_AGENT = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p mÃƒÂ£ sÃ¡Â»â€˜ Ã„â€˜Ã¡ÂºÂ¡i lÃƒÂ½ hÃ¡Â»Â£p lÃ¡Â»â€¡";//Please enter a valid Agent code. 
	
	public static final String OTHER_CHANNEL_AGENTS = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p mÃƒÂ£ sÃ¡Â»â€˜ Ã„â€˜Ã¡ÂºÂ¡i lÃƒÂ½ hÃ¡Â»Â£p lÃ¡Â»â€¡";//Please enter a valid Agent code. 
	
	public static final String INVALID_AGENTCODE = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p mÃƒÂ£ sÃ¡Â»â€˜ Ã„â€˜Ã¡ÂºÂ¡i lÃƒÂ½ hÃ¡Â»Â£p lÃ¡Â»â€¡";//Please enter a valid Agent code. 
	
	public static final String INVALID_NATIONALID = "SÃ¡Â»â€˜ CMND tÃ¡Â»â€˜i Ã„â€˜a 30 kÃƒÂ­ tÃ¡Â»Â±"; //Max 30 characters allowed for Customer's National ID
	
	public static final String INVALID_ADDRESS = "Ã„ï¿½Ã¡Â»â€¹a chÃ¡Â»â€° tÃ¡Â»â€˜i Ã„â€˜a 100 kÃƒÂ­ tÃ¡Â»Â±";//Max 100 characters allowed for Customer 's Address
	
	public static final String INVALID_STATE = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p TÃ¡Â»â€°nh/TP";
	
	public static final String INVALID_NATIONALITY = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p QuÃ¡Â»â€˜c tÃ¡Â»â€¹ch"; //Max 50 characters allowed for Customer's Nationality
	
	public static final String INVALID_OCCUPATION = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p nghÃ¡Â»ï¿½ nghiÃ¡Â»â€¡p";//Max 50 characters allowed
	
	public static final String DUPLICATE_LEAD = "KhÃƒÂ¡ch hÃƒÂ ng tiÃ¡Â»ï¿½m nÃ„Æ’ng nÃƒÂ y Ã„â€˜ÃƒÂ£ tÃ¡Â»â€œn tÃ¡ÂºÂ¡i trÃƒÂªn hÃ¡Â»â€¡ thÃ¡Â»â€˜ng";
	
	public static final String INVALID_TEMPLATE = "Invalid template; Syetem can't process Data. Please check the data";
	
	public static final String NO_ALLOCATION="KhÃƒÂ´ng tÃ¡Â»â€œn tÃ¡ÂºÂ¡i chuyÃƒÂªn viÃƒÂªn tÃ†Â° vÃ¡ÂºÂ¥n Ã„â€˜Ã¡Â»Æ’ phÃƒÂ¢n bÃ¡Â»â€¢ thÃƒÂ´ng tin khÃƒÂ¡ch hÃƒÂ ng tiÃ¡Â»ï¿½m nÃ„Æ’ng";//"No agents avaialble to allocate the lead";
	
	public static final String NEXT_MEETING_TIME_INVALID = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p giÃ¡Â»ï¿½ hÃ¡ÂºÂ¹n gÃ¡ÂºÂ·p kÃ¡ÂºÂ¿ tiÃ¡ÂºÂ¿p";// TIME NOT THERE
	
	public static final String NEXT_MEETING_DATE_INVALID = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p ngÃƒÂ y hÃ¡ÂºÂ¹n gÃ¡ÂºÂ·p kÃ¡ÂºÂ¿ tiÃ¡ÂºÂ¿p"; // DATE NOT THERE
	
	public static final String NAME_INVALID_LENGTH = "Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p HÃ¡Â»ï¿½ tÃƒÂªn khÃƒÂ¡ch hÃƒÂ ng hÃ¡Â»Â£p lÃ¡Â»â€¡"; // Name invalid length
	
	public static final String NationalID_INVALID_CHARACTER="Vui lÃƒÂ²ng nhÃ¡ÂºÂ­p Ã„â€˜ÃƒÂºng CMND";
	
	
	
	//Vietnam Relese 2
	
	public static final String BULKUPLOAD = "BulkUpload";
	public static final String BULKUPLOAD_PENDING = "BulkUploadPending";
	
	
	
	// Vietnam Excel type related Constants

	public static final String VALID = "VALID";
	public static final String INVALID = "INVALID";
	public static final String VALID_SUCCESS = "ValidSuccess";
	public static final String AGENT_FAILED = "AllocationFailed";
	
	//Vietnam 2 - General Constants
	
	public static final String TYPE_AGENCY = "Agency";
	public static final String TYPE_BANCA = "Banca";
	public static final String TYPE_BANCA_AGENT = "IOIS";
	public static final String TYPE_REASSIGN = "leadReassign";
	
	public static final String BULK_AGENCY="BulkUploadAgency";
	public static final String BULK_BANCA="BulkUploadBanca";
	
	public static final String STATUS_ACTIVE = "Active";
	
	public static final String KEY12 = "Key12";

	//Vietnam R3- General Constants

	public static final String bulkUploadPushNotificationMessageAgency="Danh sÃƒÂ¡ch KhÃƒÂ¡ch hÃƒÂ ng tiÃ¡Â»ï¿½m nÃ„Æ’ng cÃ¡Â»Â§a bÃ¡ÂºÂ¡n vÃ¡Â»Â«a Ã„â€˜Ã†Â°Ã¡Â»Â£c cÃ¡ÂºÂ­p nhÃ¡ÂºÂ­t.";	

	public static final String pushNotificationMessage="khÃƒÂ¡ch hÃƒÂ ng vÃ¡Â»Â«a Ã„â€˜Ã†Â°Ã¡Â»Â£c giÃ¡Â»â€ºi thiÃ¡Â»â€¡u Ã„â€˜Ã¡ÂºÂ¿n anh/chÃ¡Â»â€¹.";
	
	public static final String MAIL_SUBJECT="Bulk Upload Results";
	
	public static final String LEADREASSIGN = "leadReAssign";
	
	public static final String FNAREASSIGN = "fnaReAssign";
	
	public static final String DELETEBRNCHUSERASSIGNEDLEAD = "branchuserAssignedLeadDelete";

	public static final String DASHBOARDCRITERIAREQUEST = "dashboardCriteriaRequest";	

	public static final String AGENTIDS = "agentIds";
	
	public static final String LISTINGDASHBOARD = "ListingDashBoard";
	
	public static final String RETRIEVE_BY_HIERARCHYLEVEL = "HierarchyLevelRetrieve";
	
	public static final String RETRIEVE_BY_LEADREASSIGN = "LeadReassignRetrieve";
	
	public static final String REASSIGNKEY = "rm_reassign";
	
	public static final String RETRIEVE_BY_CONVERSIONRATE = "ConversionRateRetrieve";
	
	public static String CHOOSEPARTY="CHOOSEPARTY";
	
	public static String LMS="LMS";

	// Code lookup related Constants

	public static final String TYPE_STATE="STATE";	

	public static final String TYPE_NATIONALITY="NATIONALITY";
	
	public static final String TYPE_OCCUPATION="OCCUPATION_LIST";
	
	public static final String TYPE_NEED="NEED";
	
	public static final String TYPE_INCOME="INCOME";
	
	public static final String LANG_EN="en";
	
	public static final String LANG_VT="vt";
	
	public static final String LANG_BULK="bulk";
	
	public static final String COUNTRY_VT="VT";
	
	public static final String BULK="BULK";
	
	public static final String US="US";
	
	public static final String RETRIEVE_BY_NEW_STATUS="RetrieveByNew";
	
	public static final String RETRIEVE_BY_CLOSED_STATUS="RetrieveByClosed";
	
	public static final String RETRIEVE_BY_FOLLOWUP_STATUS="RetrieveByFollowUp";
	public static final String SALES_ACTIVITY="salesActivity";
	public static final String PDF_IMAGE_LOC = "le.pdf.image.location";
	public static final String CONFIG_PROPERTY_FILE = "omni-channel-vietnam-platform-service-qa";
	// codes for geofencing
	
	public static final String AGENTCHECKIN="agentCheckin";
	
	public static final String SUBMITTED="SUBMITTED";
	
	public static final String REQUEST = "Request";
    public static final String REQUEST_INFO = "RequestInfo";
    public static final String TRANSACTIONS = "Transactions";
    public static final String REQUEST_PAYLOAD = "RequestPayload";
    
    public static final String RESPONSE_PAYLOAD = "ResponsePayload";
    public static final String RESPONSE_INFO = "ResponseInfo";

	public static final String TRANSACTION_DATA = "TransactionData";
	public static final String LANGUAGE = "language";
	public static final String LANGUAGE_THAI = "th_TH";
	
	public static final String TEAM = "team";
	public static final String TEAM_AGENT = "agentCode";
	
	//TypesOfAgent
	public static final String GROUP_MANAGER = "GM"; 
	public static final String SALES_MANAGER = "SM";
	public static final String SALES_AGENT = "A";
	
	//AgentCode
	public static final String AGENT_CODE_PT = "PT";
	public static final String AGENT_CODE_I1 = "I1";
	public static final String AGENT_CODE_I2 = "I2";
	public static final String AGENT_CODE_I3 = "I3";
	public static final String AGENT_CODE_FP = "FP";
	public static final String AGENT_CODE_CA = "CA";
	
	//GMCode
	public static final String GM_CODE_D1 = "D1";
	public static final String GM_CODE_D2 = "D2";
	public static final String GM_CODE_D3 = "D3";
	
	//SMCode
	public static final String SM_CODE_E1 = "E1";
	public static final String SM_CODE_E2 = "E2";
	public static final String SM_CODE_E3 = "E3";
	public static final String SM_CODE_SM = "SM";
	
	public static final String APPOINTMENT = "Appointment";
	public static final String NOT_APPOINTMENT = "NotAppointment";
	public static final String CALL = "Call";
	public static final String VISIT = "Visit";
	
    public static final String PRESENTATION = "Presentation";

    public static final String CLOSE_CASE = "CloseCase";

	public static final String BENCHMARK = "Benchmark";
	
	public static final String CURRENT_MONTH = "CurrentMonth";
	
	public static final String PREVIOUS_MONTH = "PreviousMonth";
	 
	public static final String CURRENT_WEEK = "CurrentWeek";
		
	public static final String PREVIOUS_WEEK = "PreviousWeek";
	 
	public static final String LAST_THREE_MONTHS = "LastThreeMonth";
	
	public static final String LAST_SIX_MONTHS = "LastSixMonth";
	
	public static final String TOTAL_POINTS = "TotalPoints";
	public static final String PRE_WEEKLY = "Pre. Week (Activity)";
    public static final String PRE_MONTHLY = "Pre. Month (Activity)";
    public static final String TOTAL_ACTIVITY = "TotalActivity";
    public static final String PERCENT_ACHIEVE = "% Achieve";
    
    public static final String BENCHMARK_CONV_RATIO = "Benchmark (All Team)";
    public static final BigDecimal BENCHMARK_CONV_RATIO_VALUE = new BigDecimal(20);
    public static final String COLOR_WHITE = "white";
	
	public static final BigDecimal PERCENTAGE_HUNDRED = new BigDecimal(100);
	
	public static final String SALES_ACTIVITY_COUNT_STATUS = "(20)";
	
	public static final String INFORCE_AGENT = "inforce";
	
	public static final String CALL_OR_VISIT = "CallOrVisit";
	
	public static final String ALL = "All";
	
	public static final String MINUS_ONE_VALUE = "-1";
	
	public static final String DATEFORMAT_YYYYMMDD = "yyyy-MM-dd";
	
	/** Thai Translations for Thailand PDF Premium Modes*/
	public static final String ANNUAL="รายปี";
	public static final String SEMI_ANNUAL="ราย 6 เดือน";
	public static final String QUARTERLY="ราย 3 เดือน";
	public static final String MONTHLY="รายเดือน";
	public static final String MALE="ชาย";
	public static final String FEMALE="หญิง";
	public static final String OVER_ALL="Overall";
	public static final String COLOR_CODE="colorCode";
	public static final String PACAKGE_PLAN_A="A";
	public static final String PACAKGE_PLAN_B="B";
	public static final String PACAKGE_PLAN_A_CODE="แผน 2000";
	public static final String PACAKGE_PLAN_B_CODE="แผน 5000";
	
	/**WP Rider Translation*/
	public static final String WP_RiderPremium_Free="ฟรี**";
	
	/** Basic Plan Names*/
	/*public static final String GCH="4M80";
	public static final String GPL25="1625";
	public static final String GPL20="1620";
	public static final String GS20P="3620";
	public static final String GBN="7B08";*/
	
	/** Rider Names With code*/
	public static final String WP="9440";
	public static final String PB="9431";	
	public static final String ADB="9910";
	public static final String ADD="9920";
	public static final String AI="9930";
	public static final String RCC_ADB="9911";
	public static final String RCC_ADD="9921";
	public static final String RCC_AI="9931";
	public static final String HB_A="HB(A)";
	public static final String HS_Extra="9954";
	public static final String DD_2551="DD_2551";
	public static final String DD="9414";
	public static final String HB="9941";
	public static final String HS="9953";
	public static final String ADD_Extra="9922";
	public static final String HB_Extra="9942";
	public static final String CI_Extra="9413";
	
	/**Rider Names**/
	public static final String WPName="WP";
	public static final String PBName="PB";	
	public static final String ADBName="ADB";
	public static final String ADDName="ADD";
	public static final String AIName="AI";
	public static final String RCC_ADBName="RCC ADB";
	public static final String RCC_ADDName="RCC ADD";
	public static final String RCC_AIName="RCC AI";
	public static final String HB_AName="HB(A)";
	public static final String HS_ExtraName="HS Extra";
	public static final String DD_2551Name="DD_2551";
	public static final String DDName="DD";
	public static final String HBName="HB";
	
	
	
	public static final String WP_Rider="WP Illustration page";
	public static final String WP_Rider_Thai="สัญญาเพิ่มเติมผลประโยชน์การยกเว้นเบี้ยประกันภัย (WP)**";
	
	public static final String PB_Rider="PB Illustration page";
	public static final String PB_Rider_Thai="สัญญาเพิ่มเติมคุ้มครองผู้ชำระเบี้ยประกันภัย (PB)";
	
	public static final String ADB_Rider="ADB Illustration page";
	public static final String ADB_Rider_Thai="สัญญาเพิ่มเติม การประกันอุบัติเหตุ คุ้มครองการเสียชีวิต (ADB)";
	
	public static final String ADBRCC_Rider="ADBRCC Illustration page";
	public static final String ADBRCC_Rider_Thai="บันทึกสลักหลังการขยายความคุ้มครองฆาตกรรม จลาจล สงครามกลางเมือง แนบท้ายสัญญาเพิ่มเติมการประกันอุบัติเหตุ คุ้มครองการเสียชีวิต(RCC ADB)";
	
	public static final String ADD_Rider="ADD Illustration page";
	public static final String ADD_Rider_Thai="สัญญาเพิ่มเติม การประกันอุบัติเหตุ คุ้มครองการเสียชีวิต สูญเสียอวัยวะ(ADD)";
	
	public static final String ADD_EXTRA_Rider_Thai="สัญญาเพิ่มเติม การประกันอุบัติเหตุ คุ้มครองการเสียชีวิต สูญเสียอวัยวะ เอ็กซ์ตร้า (ADD Extra)";
	
	public static final String ADDRCC_Rider="ADDRCC Illustration page";
	public static final String ADDRCC_Rider_Thai="บันทึกสลักหลังการขยายความคุ้มครองฆาตกรรม จลาจล สงครามกลางเมือง แนบท้ายสัญญาเพิ่มเติมการประกันอุบัติเหตุ คุ้มครองการเสียชีวิต  สูญเสียอวัยวะ(RCC ADD)";
	
	public static final String AI_Rider="AI Illustration page";
	public static final String AI_Rider_Thai="สัญญาเพิ่มเติม การประกันอุบัติเหตุ คุ้มครองการเสียชีวิต สูญเสียอวัยวะ ทุพพลภาพ และศัลยกรรม(AI)";
		
	public static final String AIRCC_Rider="AIRCC Illustration page";
	public static final String AIRCC_Rider_Thai="บันทึกสลักหลังการขยายความคุ้มครองฆาตกรรม จลาจล สงครามกลางเมือง แนบท้ายสัญญาเพิ่มเติมการประกันอุบัติเหตุ คุ้มครองการเสียชีวิต  สูญเสียอวัยวะ ทุพพลภาพ และศัลยกรรม(RCC AI)";
	
	public static final String HB_Rider="HB Illustration page";
	public static final String HB_Rider_Thai="สัญญาเพิ่มเติมค่าชดเชยรายวันกรณีเข้ารักษาในโรงพยาบาล(HB)*";
	public static final String HB_EXTRA_Rider_Thai="สัญญาเพิ่มเติมค่าชดเชยรายวันกรณีเข้ารักษาในโรงพยาบาล เอ็กซ์ตร้า (HB Extra)*";
	
	public static final String HS_Extra_Rider="HS Illustration page";
	public static final String HS_Extra_Rider_Thai="สัญญาเพิ่มเติมประกันสุขภาพ ค่ารักษาพยาบาลและศัลยกรรม เอ็กซ์ตร้า (HS Extra)*";
 		
	public static final String DD_2551_Rider="DD Illustration page";
	public static final String DD_2551_Rider_Thai="สัญญาเพิ่มเติมประกันภัยโรคร้ายแรง(DD)*";
	public static final String DD_Rider_Thai="สัญญาเพิ่มเติมประกันภัยโรคร้ายแรง (DD)";
	public static final String HB_RiderThai="สัญญาเพิ่มเติมค่าชดเชยรายวันกรณีเข้ารักษาในโรงพยาบาล (HB)";
	public static final String HS_Extra_Thai="สัญญาเพิ่มเติมประกันสุขภาพ ค่ารักษาพยาบาลและศัลยกรรม เอ็กซ์ตร้า (HS Extra)";
	public static final String WP_RiderThai="สัญญาเพิ่มเติม ผลประโยชน์การยกเว้นเบี้ยประกันภัย (WP)";
	
	public static final String CI_EXTRA_RiderThai="สัญญาเพิ่มเติมประกันภัยโรคร้ายแรง เอ็กซ์ตร้า (CI Extra)*";
	public static final String HS_RiderThai="ประกันสุขภาพค่ารักษาพยาบาลและศัลยกรรม (H&S)";
	/*EAPP*/
	public static final String SALES_AGENCY_CODE="02";
	
    public static final Set<String> COLORCODING_EXEMPTIONS_SET = ImmutableSet.of(GeneraliConstants.PRE_WEEKLY,
            GeneraliConstants.PRE_MONTHLY, GeneraliConstants.BENCHMARK, GeneraliConstants.PERCENT_ACHIEVE,
            GeneraliConstants.TOTAL_ACTIVITY, GeneraliConstants.TOTAL_POINTS);
    
    public static final Set<String> COLORCODING_EXEMPTIONS_SET_FOR_SCORING =
            ImmutableSet.of(GeneraliConstants.BENCHMARK, GeneraliConstants.TOTAL_POINTS, GeneraliConstants.PRE_WEEKLY,
                    GeneraliConstants.PRE_MONTHLY);
    
    public static final String RETRIEVE_BY_COUNT_ILLUSTRATION="retrieveByCountForIllustration";
    public static final String RETRIEVE_BY_COUNT_ILLUSTRATION_DRAFT="retrieveByCountForIllustrationDraft";
    
    public static final String SPAJ_NO_MISSING = "SPAJ Number is missing";
    
    public static final String MSG_DESCRIPTION = "msgDescription";
    
    public static final String RECORD_SAVED_SUCCESSFULLY = "Record saved successfully";
    
    public static final String MSG_CODE = "msgCode";
    
    public static final String SUCCESS_CODE = "200"; 
    
    public static final String SUCCESS = "Success";
    
    public static final String RECORD_SAVE_FAILED = "Record save failed";
    
    public static final String FAILURE_CODE = "500";
    
    public static final String FAILURE = "Failure";
    
    public static final String MSGINFO = "msginfo";
	
    public static final String RESPONSE = "Response";
    
    public static final String MSG = "msg";
    
    public static final String APP_NO = "AppNo";

	public static final String DOC_STATUS = "DocStatus";

	public static final String DOC_NAME = "DocName";

	public static final String DOC_ID = "DocID";
	
	public static final String MEMO = "Memo";

	public static final String MEMO_STATUS = "MemoStatus";

	public static final String MEMO_DATE = "MemoDate";

	public static final String MEMO_DESC = "MemoDesc";

	public static final String REMARKS = "Remarks";

	public static final String CATEGORY = "Category";

	public static final String MEMO_CODE = "MemoCode";

	public static final String APP_STATUS = "AppStatus";

	public static final String POLICY_NO = "PolicyNo";
	
	public static final String MEMO_FAILURE_MESSAGE = "Looks like memo details for the given application number is absent";
	
	public static final String RETRIEVE = "retrieve";
	
	public static final String UPDATE = "update";
	
	public static final String INVALID_INPUT = "Invalid Input";
	
	public static final String MEMO_DETAILS = "MemoDetails";

    public static final String MEMO_REASON = "MemoReason";

    public static final String MEMO_ID = "ID";
    
    public static final String MEMO_DOC_ID = "DocID";
      
    public static final String MEMO_PDF = "MemoPDF";
    
    public static final String OPEN = "Open";
    
    /*Payment*/
    public static final String PAYMENT_INFO="PaymentInfo";
    public static final String URL_INFO="UrlInfo";
    public static final String PAYMENT_NO="payment_no";
    public static final String PAYMENT_PURPOSE="payment_purpose";
    public static final String FIRSTNAME="first_name";
    public static final String LASTNAME="last_name";
    public static final String PREMIUM="premium";
    public static final String COMPANY="company";
    public static final String GRACE_DUE_DATE="grace_due_date";
    public static final String ALLOW_TO_PAY_SHORTAGE="allow_to_pay_shortage";
    public static final String SOURCE_ID="source_id";
    public static final String RESULTS="results";
    public static final String ACCESS_TOKEN="access_token";
    public static final String UUID="uuid";
    public static final String ID="id";
    public static final String PAYMENT_BY_URL = "paymentByUrl";
    public static final String AUTHORIZATION = "Authorization";
    public static final String EMAIL_TEMPLATE = "emailTemplate";
	public static final String PAYMENT_URL = "paymentUrl";
    
    public static final String UTF8="UTF-8";
    
    public static final String GRANT_TYPE_KEY="grant_type";
    public static final String CLIENT_ID_KEY="client_id";
    public static final String CLIENT_SECRET_KEY="client_secret";
    public static final String SCOPE="scope";
    public static final String PORTAL_INFO="portal-info";

	public static final String UWRULEFIELDS = "uwRuleFields";

	public static final String ERROR = "ERROR";
	
	public static final String FAILED = "FAILED";
	
	public static final String CREATED = "CREATED";
	
	public static final String IGNORED = "IGNORED";
	
	public static final String RESOLVED = "Resolved";
    
    public static final String CANCELLED = "Cancelled";
    
    public static final String EMAIL = "Email";

    public static final String PENDING_UW = "Pending UW";

    public static final String DETAILS = "Details";

    public static final String PENDING_SUBMISSION =  "Pending Submission";

    public static final String COUNTER_MEMO = "5CF";

    public static final String ACTION = "Action";

    public static final String UNLOCK = "unlock";

    public static final String LOCK = "lock";

    public static final String GAO_OFFICE = "GAOOffice";

    public static final String ASSIGN_GAO = "assignGAO";

    public static final String GAO_CODE = "GAOCode";

    public static final String IDENTIFIER = "Id";

    public static final String KEY37 = "Key37";
    
    public static final String KEY36 = "Key36";
    
    public static final String KEY35 = "Key35";

    public static final String GAO = "GAO";

    public static final String BRANCH_ADMIN = "BranchAdmin";

    public static final String MARK_COMPLETE = "marked complete";
    
    public static final String EAPPDETAILS = "eAPPDetails";
    
    public static final String DOCLOCATION = "DocLocation";
    
    public static final String AGENT_NAME = "AgentName";
    
    public static final String EAPP="eApp";
    
    public static final String TITLE="TITLE";
    
    public static final String NATIONALITY="NATIONALITY";
    
    public static final String NATUREOFWORK="NATUREOFWORK";
    
    public static final String JANUARY= "มกราคม";
    
    public static final String FEBURARY= "กุมภาพันธ์";
    
    public static final String MARCH= "มีนาคม";
    
    public static final String APRIL= "เมษายน";
    
    public static final String MAY= "พฤษภาคม";
    
    public static final String JUNE= "มิถุนายน";
    
    public static final String JULY= "กรกฎาคม";
    
    public static final String AUGUST= "สิงหาคม";
    
    public static final String SEPTEMBER= "กันยายน";
    
    public static final String OCTOBER= "ตุลาคม";
    
    public static final String NOVEMBER= "พฤศจิกายน";
    
    public static final String DECEMBER= "ธันวาคม";
    
    public static final String NO= "No";
    
    public static final String YES= "Yes";
    
    public static final String IDENTITYTYPE="IDENTITYTYPE";
    
    public static final String DATE_FORMAT = "yyyyMMdd";
    
	public static final String INFORCE = "INFORCE";
	
	public static final String RENEWAL_PREMIUM = "RenewalPremium";
	
	public static final String REGULAR_PREMIUM = "RegularPremium";
	
	public static final String WARD = "WARD";
	
	public static final String EXTENSIONTHAI = " เบอร์ต่อ  ";
	
	public static final String SPLIT_ONE = "SplitOne";
	
	public static final String TYPE_MARKCOMPLETE = "markComplete";
	
	public static final String BENEFICIARYRELATIONSIP ="BENEFICIARYRELATIONSIP";
	
	public static final String COMPANYNAME = "SHORTCOMPANYNAME";
	
	public static final String THAI_DATE="รักษาเมื่อ: ";
	
	public static final String TRTMETHOD="โดย: ";
	
	public static final String CURSYMPTOM="อาการตอนนี้: ";
	
	public static final String HOSPITAL="ที่:   ";
	
	public static final String NOTICE= "ข้อสังเกตล: ";
	
	
	public static final String HYPERTENSION="Hypertension";
	public static final String HEARTDISEASE="HeartDisease";
	public static final String CORONHEARTDISEASE="CoronaryHeartDisease";
	public static final String CARDIO="CardiovascularDisease";
	public static final String CEREBRO="CerebrovascularDisease";
	public static final String PARALYSIS="PartialOrTotalParalysis";
	public static final String DIABETICS="DiabeticMellitus";
	public static final String THYROID="ThyroidDisease";
	
	
	public static final String THAI_HYPERTENSION="โรคความดันโลหิตสูง,";
	public static final String THAI_HEARTDISEASE="โรคหัวใจ,";
	public static final String THAI_CORONHEARTDISEASE="โรคเส้นเลือดหัวใจตีบ,";
	public static final String THAI_CARDIO="โรคหลอดเลือด,";
	public static final String THAI_CEREBRO="โรคหลอดเลือดในสมอง,";
	public static final String THAI_PARALYSIS="โรคอัมพฤกษ์/โรคอัมพาต,";
	public static final String THAI_DIABETICS="โรคเบาหวาน,";
	public static final String THAI_THYROID="โรคไทรอยด์,";
	
	public static final String CANCER="Cancer";
	public static final String LYMPH="EnlargedLymphNode";
	public static final String TUMOR="Tumor";
	public static final String MASSCYST="MassOrCyst";
	
	public static final String THAI_CANCER="โรคมะเร็ง,";
	public static final String THAI_LYMPH="โรคต่อมน้ำเหลืองโต,";
	public static final String THAI_TUMOR="โรคเนื้องอก,";
	public static final String THAI_MASSCYST="ก้อนหรือถุงน้ำ,";
	
	public static final String PANCREATITIS="Pancreatitis";
	public static final String KIDNEY="KidneyDisease";
	public static final String JAUNDICE="Jaundice";
	public static final String SPLENOMEGALY="Splenomegaly";
	public static final String PEPTICULCER="PepticUlcer";
	public static final String LIVER="LiverAndBileDuctTractdisease";
	public static final String ALOCHOLISM="Alcoholism";
	
	public static final String THAI_PANCREATITIS="โรคตับอ่อนอักเสบ,";
	public static final String THAI_KIDNEY="โรคไต,";
	public static final String THAI_JAUNDICE="โรคดีซ่าน,";
	public static final String THAI_SPLENOMEGALY= "ม้ามโต,";
	public static final String THAI_PEPTICULCER="โรคแผลในทางเดินอาหาร,";
	public static final String THAI_LIVER="โรคตับหรือทางเดินน้ำดี,";
	public static final String THAI_ALOCHOLISM="โรคพิษสุราเรื้อรัง,";
	
	public static final String PNEUMONIA="LungDisease/pneumonia";
	public static final String TUBERCULOSIS="Tuberculosis";
	public static final String ASTHMA="Asthma";
	public static final String PULMONARY="ChronicObstructivePulmonaryDisease";
	public static final String EMPHYSEMA="Emphysema";
	public static final String SLEEPAPNEA="ObstructuveSleepApneaSyndrome";
	
	public static final String THAI_PNEUMONIA="โรคปอดหรือปอดอักเสบ,";
	public static final String THAI_TUBERCULOSIS="โรควัณโรค,";
	public static final String THAI_ASTHMA="โรคหอบหืด,";
	public static final String THAI_PULMONARY="โรคปอดอุดกั้นเรื้อรัง,";
	public static final String THAI_EMPHYSEMA="โรคถุงลมโป่งพอง,";
	public static final String THAI_SLEEPAPNEA="ภาวะหยุดหายใจขณะหลับ,";
	
	
	public static final String IMPAIREDVISION="ImpairedVision";
	public static final String RETINA="RetinaDisease";
	public static final String GLAUCOMA="Glaucoma";
	
	public static final String THAI_IMPAIREDVISION="สายตาพิการ,";
	public static final String THAI_RETINA="โรคจอประสาทต,";
	public static final String THAI_GLAUCOMA="โรคต้อหิน,";
	
	public static final String PARKINSONS="Parkinsons";
	public static final String ALZHEIMERS="Alzheimers";
	public static final String EPILEPSY="Epilepsy";
	
	public static final String THAI_PARKINSONS="โรคพาร์กินสัน,";
	public static final String THAI_ALZHEIMERS="โรคความจำเสื่อม,";
	public static final String THAI_EPILEPSY="โรคชัก,";
	
	public static final String ARTHRITIS="Arthritis";
	public static final String GOUT="Gout";
	public static final String SLE="SLE";
	public static final String SCLERODERMA="Scleroderma";
	public static final String BLOODDISEASE="BloodDisease";
	
	public static final String THAI_ARTHRITIS="โรคข้ออักเสบ,";
	public static final String THAI_GOUT="โรคเก๊าท์,";
	public static final String THAI_SLE="โรคหนังแข็ง,";
	public static final String THAI_SCLERODERMA="โรคเอสแอลอี,";
	public static final String THAI_BLOODDISEASE="โรคเลือด,";
	
	public static final String PSYCHOSIS="Psychosis";
	public static final String NEUROSIS="Neurosis";
	public static final String DEPRESSION="Depression";
	public static final String DOWNS="DownsSyndrome";
	public static final String PHYSICALDIS="PhysicalDisability";
	
	public static final String THAI_PSYCHOSIS="โรคจิต,";
	public static final String THAI_NEUROSIS="โรคประสาท,";
	public static final String THAI_DEPRESSION="โรคซึมเศร้า,";
	public static final String THAI_DOWNS="ดาวน์ซินโดรม,";
	public static final String THAI_PHYSICALDIS="พิการทางร่างกาย,";
	
	public static final String AIDS="AIDS";
	public static final String VENEREAL="VenerealDisease ";
	
	public static final String THAI_AIDS="โรคเอดส์หรือภูมิคุ้มกันบกพร่อง,";
	public static final String THAI_VENEREAL="โรคกามโรค (ภายใน 2 ปี),";

	public static final String CHESTPAIN="ChestPain";
	public static final String PALPITATION="Palpitation ";
	public static final String ABNORFATIGUE="AbnormalFatigue ";
	public static final String MUSCWEAK="MuscularWeakness ";
	public static final String ABNORPHYMOV="AbnormalPhysicalMovement ";
	public static final String LOSSSENSNEU="LossOfSensoryNeuron ";
	
	public static final String THAI_CHESTPAIN="เจ็บหรือแน่นหน้าอก,";
	public static final String THAI_PALPITATION="ใจสั่น, ";
	public static final String THAI_ABNORFATIGUE="เหนื่อยง่ายผิดปกติ ,";
	public static final String THAI_MUSCWEAK="กล้ามเนื้ออ่อนแรง, ";
	public static final String THAI_ABNORPHYMOV="การเคลื่อนไหวของร่างกายผิดปกติ, ";
	public static final String THAI_LOSSSENSNEU="ร่างกายสูญเสียประสาทรับความรู้สึก, ";
	
	public static final String CHRONICSTOMACHACHE="ChronicStomachAche";
	public static final String HEMATEMESIS="HematemesisOrHematochezia";
	public static final String DROPSY="Dropsy";
	public static final String CHRONICDIARRHEA="ChronicDiarrhea";
	public static final String HEMATURIA="Hematuria";
	public static final String CHRONICCOUGH="ChronicCough";
	public static final String HEMOPTYSIS="Hemoptysis";
	
	public static final String THAI_CHRONICSTOMACHACHE="ปวดท้องเรื้อรัง,";
	public static final String THAI_HEMATEMESIS="อาเจียนหรือถ่ายเป็นเลือด,";
	public static final String THAI_DROPSY="ท้องมาน,";
	public static final String THAI_CHRONICDIARRHEA="ท้องเสียเรื้อรัง,";
	public static final String THAI_HEMATURIA="มีเลือดปนในปัสสาวะ,";
	public static final String THAI_CHRONICCOUGH="ไอเรื้อรัง,";
	public static final String THAI_HEMOPTYSIS="ไอเป็นเลือด,";
	
	
	public static final String PALPABLETUMOR="PalpableTumor";
	public static final String CHRONICSEVHEADACHE="ChronicSevereHeadache";

	public static final String THAI_PALPABLETUMOR="คลำพบก้อนเนื้อ,";
	public static final String THAI_CHRONICSEVHEADACHE="ปวดศีรษะรุนแรงเรื้อรัง,";
	
	public static final String CHRONICJOINT="ChronicJointPain";
	public static final String BRUISES="Bruises";
	
	public static final String THAI_CHRONICJOINT="ปวดข้อเรื้อรัง,";
	public static final String THAI_BRUISES="มีจ้ำเลือด,";
	
	public static final String IMPAIREDVISION2="ImpairedVision";
	public static final String SLOWDEV="SlowDevelopment";
	public static final String HURTONESELF="HaveAttemptedToHurtOneself";
	
	public static final String THAI_IMPAIREDVISION2="การมองเห็นผิดปกติ,";
	public static final String THAI_SLOWDEV="มีพัฒนาการช้า,";
	public static final String THAI_HURTONESELF="เคยพยายามทำร้ายร่างกายตนเอง,";
	
	public static final String THAI_FATIGUE="อาการอ่อนเพลีย,";
	public static final String THAI_WGHTLOSS="น้ำหนักลด,";
	public static final String THAI_CHRONICDIARREA="ท้องเดินเรื้อรัง,";
	public static final String THAI_PROLONGEDFEVER="ไข้เรื้อรัง,";
	public static final String THAI_CHRONICSKIN="โรคผิวหนังเรื้อรัง,";
	
	public static final String OTITISMEDIA="OtitisMedia";
	public static final String CHRONICTONSIL="ChronicTonsillitis,";
	public static final String SINUSITIS="Sinusitis";
	public static final String MIGRAINE="Migraine";
	public static final String ALLERGY="Allergy";
	public static final String CHRONICBRONCHTIS="Chronic bronchitis";
	
	public static final String THAI_OTITISMEDIA="ติดเชื้อในหูชั้นกลาง,";
	public static final String THAI_CHRONICTONSIL="ต่อมทอนซิลอักเสบเรื้อรัง,";
	public static final String THAI_SINUSITIS="ไซนัสอักเสบ,";
	public static final String THAI_MIGRAINE="ปวดศีรษะไมเกรน,";
	public static final String THAI_ALLERGY="ภูมิแพ้,";
	public static final String THAI_CHRONICBRONCHTIS= "หลอดลมอักเสบเรื้อรัง,";
	
	public static final String GASTROESOPHAGEAL="Gastro-EsophagealReflux";
	public static final String URINARYTRACTSTONE="UrinaryTractStone,";
	public static final String CHOLECYSTITIS="Cholecystitis";
	public static final String HERNIA="Hernia";
	public static final String HEMORRHOID="Hemorrhoid";
	public static final String ANALFISTULA="AnalFistula";
	
	public static final String THAI_GASTROESOPHAGEAL="กรดไหลย้อน,";
	public static final String THAI_URINARYTRACTSTONE="นิ่ว,";
	public static final String THAI_CHOLECYSTITIS="ถุงน้ำดีอักเสบ,";
	public static final String THAI_HERNIA="ไส้เลื่อน,";
	public static final String THAI_HEMORRHOID="ริดสีดวงทวาร,";
	public static final String THAI_ANALFISTULA= "ฝีคัณฑสูตร,";
	
	public static final String ENDOMETROSIS="Endometroisis";
	public static final String SPONDYLOLISTHESIS="Spondylolisthesis,";
	public static final String HERNIATEDVERTEBRALDISC="HerniatedVertebralDisc";
	public static final String HERNIATEDNUCLEUSPULPOSUS="HerniatedNucleusPulposus";
	public static final String DEGENERATIVEOSTEOARTHRITIS="DegenerativeOsteoarthritis";
	public static final String CHRONICTENDINITIS="ChronicTendinitis";
	public static final String PERIPHERNEURITIS="PeripheraNeuritis";
	
	public static final String THAI_ENDOMETROSIS="เยื่อบุโพรงมดลูกเจริญผิดที่,";
	public static final String THAI_SPONDYLOLISTHESIS="กระดูกสันหลังเคลื่อน,";
	public static final String THAI_HERNIATEDVERTEBRALDISC="หมอนรอง,";
	public static final String THAI_HERNIATEDNUCLEUSPULPOSUS="กระดูกเคลื่อนที่ทับเว้นประสาท,";
	public static final String THAI_DEGENERATIVEOSTEOARTHRITIS="ข้อเสื่อม, ";
	public static final String THAI_CHRONICTENDINITIS="เส้นเอ็นอักเสบเรื้อรัง,";
	public static final String THAI_PERIPHERNEURITIS="เส้นประสาทอักเสบหรือถูกเบียดทับ,";
	
	public static final String AUTISTIC="Autistic";
	public static final String HYPERACTIVITY="AttentionDeficitHyperactivityDisorder";
	
	public static final String THAI_AUTISTIC="ออทิสติก,";
	public static final String THAI_HYPERACTIVITY="สมาธิสั้น,";
	
	
	public static final String GAO_PUSH_MSG_PART1="GAO ได้กรอกข้อมูลใบคำขอของ ";
	public static final String GAO_PUSH_MSG_PART2=" เรียบร้อยแล้ว";
	
	public static final String MANUALEAPPPDF = "eApp.pdf";
	public static final String MANUALPREVIOUSPOLICY = "PreviousPolicy.pdf";
    public static final int SPAJAPPLENGTH = 7;


	
	
	
	
}

