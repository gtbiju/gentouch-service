/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 19, 2015
 */
package com.cognizant.insurance.pushnotification;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The Class class DeviceRegister.
 */
@Entity
@Table(name = "AWS_SNS_DEVICE_REGISTER")
public class DeviceRegister implements Serializable {


    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4050660125968572989L;

    /** The key. */
    @EmbeddedId
    private CompositeKey key;
    
    /** The device id. */
    @Column(name = "SNS_DEVICE_ID")
    private String deviceID;

    /** The platformtype. */
    @Column(name = "SNS_DEVICE_PLATFORM")
    private String platformtype;

    /** The application name. */
    @Column(name = "SNS_DEVICE_APP_NAME")
    private String applicationName;

    /** The status. */
    @Column(name = "SNS_DEVICE_ID_STATUS")
    private String status;

    /** The requested time. */
    @Column(name = "SNS_REQUESTED_TIME")
    private Date requestedTime;

    /** The user group. */
    @Column(name = "SNS_USER_GROUP")
    private String userGroup;

    /** The sns platform endpoint arn. */
    @Column(name = "SNS_PLATFORM_ENDPOINT_ARN")
    private String snsPlatformEndpointARN;


    /**
     * Gets the key.
     *
     * @return the key
     */
    public CompositeKey getKey() {
        return key;
    }

    /**
     * Sets the key.
     *
     * @param key the key to set.
     */
    public void setKey(CompositeKey key) {
        this.key = key;
    }

    /**
     * Gets the device id.
     * 
     * @return the device id
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * Sets the device id.
     * 
     * @param deviceID
     *            the device id to set.
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     * Gets the platformtype.
     * 
     * @return the platformtype
     */
    public String getPlatformtype() {
        return platformtype;
    }

    /**
     * Sets the platformtype.
     * 
     * @param platformtype
     *            the platformtype to set.
     */
    public void setPlatformtype(String platformtype) {
        this.platformtype = platformtype;
    }

    /**
     * Gets the application name.
     * 
     * @return the application name
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Sets the application name.
     * 
     * @param applicationName
     *            the application name to set.
     */
    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    

    /**
     * Gets the status.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the status.
     * 
     * @param status
     *            the status to set.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets the requested time.
     * 
     * @return the requested time
     */
    public Date getRequestedTime() {
        return requestedTime;
    }

    /**
     * Sets the requested time.
     * 
     * @param requestedTime
     *            the requested time to set.
     */
    public void setRequestedTime(Date requestedTime) {
        this.requestedTime = requestedTime;
    }

    /**
     * Gets the user group.
     * 
     * @return the user group
     */
    public String getUserGroup() {
        return userGroup;
    }

    /**
     * Sets the user group.
     * 
     * @param userGroup
     *            the user group to set.
     */
    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    /**
     * Gets the sns platform endpoint arn.
     * 
     * @return the sns platform endpoint arn
     */
    public String getSnsPlatformEndpointARN() {
        return snsPlatformEndpointARN;
    }

    /**
     * Sets the sns platform endpoint arn.
     * 
     * @param snsPlatformEndpointARN
     *            the sns platform endpoint arn to set.
     */
    public void setSnsPlatformEndpointARN(String snsPlatformEndpointARN) {
        this.snsPlatformEndpointARN = snsPlatformEndpointARN;
    }

}
