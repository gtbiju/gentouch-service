/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.dao.impl;

import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.generic.dao.ObservationDao;

/**
 * @author 300797
 * 
 */
public class ObservationDaoImpl extends DaoImpl implements ObservationDao {

}
