package com.cognizant.insurance.omnichannel.utils;

public class ShortFallData {

	private Long protectionShorFall;
	
	private Long protectionCoverShort;
	
	private Long protectionAvailFund;
	
	private String categoryAxisSavings;
    
    public String getCategoryAxisSavings() {
        return categoryAxisSavings;
    }

    public void setCategoryAxisSavings(String categoryAxisSavings) {
        this.categoryAxisSavings = categoryAxisSavings;
    }

    public String getCategoryAxisFund() {
        return categoryAxisFund;
    }

    public void setCategoryAxisFund(String categoryAxisFund) {
        this.categoryAxisFund = categoryAxisFund;
    }

    private String categoryAxisFund;

	public Long getProtectionShorFall() {
		return protectionShorFall;
	}

	public void setProtectionShorFall(Long protectionShorFall) {
		this.protectionShorFall = protectionShorFall;
	}

	public Long getProtectionCoverShort() {
		return protectionCoverShort;
	}

	public void setProtectionCoverShort(Long protectionCoverShort) {
		this.protectionCoverShort = protectionCoverShort;
	}

	public Long getProtectionAvailFund() {
		return protectionAvailFund;
	}

	public void setProtectionAvailFund(Long protectionAvailFund) {
		this.protectionAvailFund = protectionAvailFund;
	}
}
