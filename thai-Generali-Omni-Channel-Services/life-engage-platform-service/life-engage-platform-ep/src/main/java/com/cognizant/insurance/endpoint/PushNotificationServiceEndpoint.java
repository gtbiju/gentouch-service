/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 22, 2015
 */
package com.cognizant.insurance.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.PushNotificationService;

/**
 * The Class class PushNotificationServiceEndpoint.
 */
@Controller
@RequestMapping("/pushNotificationService")
public class PushNotificationServiceEndpoint {

    /** The push notification service. */
    @Autowired
    private PushNotificationService pushNotificationService;

    /**
     * Register device.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/registerDevice", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String registerDevice(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return pushNotificationService.registerDevice(json);
            }
        }.execute(json);
    }

    /**
     * Un register device.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/unRegisterDevice", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String unRegisterDevice(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return pushNotificationService.unRegisterDevice(json);
            }
        }.execute(json);
    }

    /**
     * Push notifications.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/pushNotifications", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String pushNotifications(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return pushNotificationService.pushNotifications(json);
            }
        }.execute(json);
    }
}
