/**
 * 
 */
package com.cognizant.insurance.omnichannel.component;

import java.text.ParseException;
import java.util.List;

import org.json.JSONArray;

import com.cognizant.insurance.omnichannel.domain.CheckinData;
import com.cognizant.insurance.omnichannel.vo.CheckinResponse;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * @author 397850
 *
 */
public interface CheckinComponent {
	
	public String checkinDetailsSave(CheckinData checkinData,
			RequestInfo requestInfo, Boolean sendNotification, List<String> pushNotificationList, String typeVariableObj) throws ParseException;


	public String retrieveDetails(RequestInfo requestInfo, List<String> agentList, List<CheckinResponse> checkinResponseList);


	public void updateCheckinDetails() throws ParseException;


}
