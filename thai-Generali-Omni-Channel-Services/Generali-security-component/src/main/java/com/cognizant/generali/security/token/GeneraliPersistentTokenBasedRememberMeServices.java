/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 347862
 * @version       : 0.1, Apr 14, 2014
 */
package com.cognizant.generali.security.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.Key;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationException;
import org.springframework.util.Assert;

import com.cognizant.le.security.constants.ExceptionMessages;
import com.cognizant.le.security.userauth.LifeEngageUserDetailsImpl;

/**
 * The Class class CustomPersistentTokenBasedRememberMeServices.
 */
public final class GeneraliPersistentTokenBasedRememberMeServices extends AbstractRememberMeServices {

    /** The token repository. */
    private PersistentTokenRepository tokenRepository = new InMemoryTokenRepositoryImpl();

    /** The random. */
    private SecureRandom random;

    /** The signature algorithm. */
    private SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

    /** The Constant DEFAULT_SERIES_LENGTH. */
    public static final int DEFAULT_SERIES_LENGTH = 16;

    /** The Constant DEFAULT_TOKEN_SUBJECT. */
    public static final String DEFAULT_TOKEN_SUBJECT = "TOKEN";

    // Default source is 100 and its for web implementation
    /** The Constant DEFAULT_SOURCE. */
    public static final String WEB_SOURCE = "100";

    public static final String DEVICE_SOURCE = "200";

    public static final String SOURCE_HEADER = "source";

    /** The series length. */
    private int seriesLength = DEFAULT_SERIES_LENGTH;

    /** The token validity device seconds. */
    private int tokenValidityDeviceSeconds = TWO_WEEKS_S;

    /** The token validity web seconds. */
    private int tokenValidityWebSeconds = TWO_WEEKS_S;

    /** The token subject. */
    private String tokenSubject = DEFAULT_TOKEN_SUBJECT;

    /**
     * Instantiates a new custom persistent token based remember me services.
     */
    @Deprecated
    public GeneraliPersistentTokenBasedRememberMeServices() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices#processAutoLoginCookie(
     * java.lang.String[], javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    protected UserDetails processAutoLoginCookie(String[] cookieTokens, HttpServletRequest request,
            HttpServletResponse response) {

        String source = getSourceAfterValidation(request);
        if (cookieTokens.length != 2) {
            throw new InvalidCookieException("Cookie token did not contain " + 2 + " tokens, but contained '"
                    + Arrays.asList(cookieTokens) + "'");
        }

        final String presentedSeries = cookieTokens[0];
        final String presentedToken = cookieTokens[1];

        PersistentRememberMeToken token = tokenRepository.getTokenForSeries(presentedSeries);

        if (token == null) {
            // No series match, so we can't authenticate using this cookie
            throw new RememberMeAuthenticationException("No persistent token found for series id: " + presentedSeries);
        }

        // We have a match for this user/series combination
        if (!presentedToken.equals(token.getTokenValue())) {
            // Token doesn't match series value. Delete all logins for this user and throw an exception to warn them.
            tokenRepository.removeUserTokens(token.getUsername());

            throw new CookieTheftException(messages.getMessage("PersistentTokenBasedRememberMeServices.cookieStolen",
                    "Invalid remember-me token (Series/token) mismatch. Implies previous cookie theft attack."));
        }

        if (!validateToken(presentedToken, token.getUsername(), source)) {
            throw new RememberMeAuthenticationException(ExceptionMessages.INVALID_TOKEN);
        }
        if (token.getDate().getTime() + getTokenValidity(source) * 1000L < System.currentTimeMillis()) {
            throw new RememberMeAuthenticationException(ExceptionMessages.TOKEN_EXPIRED);
        }

        // Token also matches, so login is valid. Update the token value, keeping the *same* series number.
        if (logger.isDebugEnabled()) {
            logger.debug("Refreshing persistent login token for user '" + token.getUsername() + "', series '"
                    + token.getSeries() + "'");
        }

        PersistentRememberMeToken newToken =
                new PersistentRememberMeToken(token.getUsername(), token.getSeries(), token.getTokenValue(), new Date());

        try {
            if (WEB_SOURCE.equals(source)) {
                tokenRepository.updateToken(newToken.getSeries(), newToken.getTokenValue(), newToken.getDate());
            }
            setCookie(new String[] { token.getSeries(), token.getTokenValue() }, getTokenValidity(source), request,
                    response);
        } catch (DataAccessException e) {
            logger.error("Failed to update token: ", e);
            throw new RememberMeAuthenticationException(ExceptionMessages.DATA_ACCESS_PROBLEM);
        }

        return getUserDetailsFromToken(token);
    }
    
    private UserDetails getUserDetailsFromToken(PersistentRememberMeToken token){
    	
    	LifeEngageUserDetailsImpl userDetails = new LifeEngageUserDetailsImpl();
    	userDetails.setUsername(token.getUsername());
    	 List<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
    	 roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
    	userDetails.setAuthorities(roles);
		return userDetails;
    	
    }

    /**
     * Gets the token validity.
     * 
     * @param mode
     *            the mode
     * @return the token validity
     */
    private int getTokenValidity(String mode) {
        if (WEB_SOURCE.equals(mode)) {
            return getTokenValidityWebSeconds();
        } else {
            return getTokenValidityDeviceSeconds();
        }
    }

    /**
     * Creates a new persistent login token with a new series number, stores the data in the persistent token repository
     * and adds the corresponding cookie to the response.
     * 
     * @param request
     *            the request
     * @param response
     *            the response
     * @param successfulAuthentication
     *            the successful authentication
     */
    protected void onLoginSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication successfulAuthentication) {
        String username = successfulAuthentication.getName();

        logger.debug("Creating new persistent login for user " + username);

        String source = getSourceAfterValidation(request);

        PersistentRememberMeToken persistentToken =
                new PersistentRememberMeToken(username, generateSeriesData(), generateTokenData(username, source),
                        new Date());
        try {
            tokenRepository.createNewToken(persistentToken);
            addCookie(persistentToken, request, response, getTokenValidity(source));
        } catch (DataAccessException e) {
            logger.error("Failed to save persistent token ", e);
            throw new RememberMeAuthenticationException("Failed to save persistent token");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices#logout(javax.servlet.http
     * .HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.Authentication)
     */
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        super.logout(request, response, authentication);

        if (authentication != null) {
            tokenRepository.removeUserTokens(authentication.getName());
        }
    }

    /**
     * Generate series data.
     * 
     * @return the string
     */
    protected String generateSeriesData() {
        byte[] newSeries = new byte[seriesLength];
        random.nextBytes(newSeries);
        return new String(Base64.encode(newSeries));
    }

    /**
     * Gets the source.
     * 
     * @param request
     *            the request
     * @return the source
     */
    protected String getSourceAfterValidation(HttpServletRequest request) {
        String source = "";
        if (request.getHeader(SOURCE_HEADER) != null) {
            source = request.getHeader(SOURCE_HEADER);
        }
        if ("".equals(source) || !(WEB_SOURCE.equals(source) || DEVICE_SOURCE.equals(source))) {
            logger.error("unknown Source: " + source);
            throw new RememberMeAuthenticationException("Invalid params");
        }
        return source;
    }

    /**
     * Gets the signing key.
     * 
     * @return the signing key
     */
    protected Key getSigningKey() {
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(getKey());
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        return signingKey;
    }

    /**
     * Validate token.
     * 
     * @param token
     *            the token
     * @param userName
     *            the user name
     * @param source
     *            the source
     * @return true, if successful
     */
    protected boolean validateToken(String token, String userName, String source) {
        Claims claims =
                Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(getKey()))
                    .parseClaimsJws(token)
                    .getBody();

        if (userName.equals(claims.getAudience()) && (getTokenSubject() + ":" + source).equals(claims.getSubject())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Generate token data.
     * 
     * @param userName
     *            the user name
     * @param source
     *            the source
     * @return the string
     */
    protected String generateTokenData(String userName, String source) {
        Claims claims =
                Jwts.claims()
                    .setSubject(getTokenSubject() + ":" + source)
                    .setAudience(userName)
                    .setIssuedAt(new Date())
                    .setIssuer("LE");
        return Jwts.builder()
            .setIssuedAt(new Date())
            .setSubject(getTokenSubject())
            .setClaims(claims)
            .signWith(signatureAlgorithm, getSigningKey())
            .compact();
        // byte[] newToken = new byte[16];
        // random.nextBytes(newToken);
        // return new String(Base64.encode(newToken));
    }

    /**
     * Adds the cookie.
     * 
     * @param token
     *            the token
     * @param request
     *            the request
     * @param response
     *            the response
     * @param tokenValidity
     *            the token validity
     */
    private void addCookie(PersistentRememberMeToken token, HttpServletRequest request, HttpServletResponse response,
            int tokenValidity) {
        setCookie(new String[] { token.getSeries(), token.getTokenValue() }, tokenValidity, request, response);
    }

    /**
     * Sets the series length.
     * 
     * @param seriesLength
     *            the series length to set.
     */
    public void setSeriesLength(int seriesLength) {
        this.seriesLength = seriesLength;
    }

    /**
     * Instantiates a new REST persistent token based remember me services.
     * 
     * @param key
     *            the key
     * @param userDetailsService
     *            the user details service
     * @param tokenRepository
     *            the token repository
     * @throws Exception
     *             the exception
     */
    public GeneraliPersistentTokenBasedRememberMeServices(String key, UserDetailsService userDetailsService,
            PersistentTokenRepository tokenRepository) throws Exception {
        super(key, userDetailsService);
        random = new SecureRandom();
        this.tokenRepository = tokenRepository;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices#rememberMeRequested(javax
     * .servlet.http.HttpServletRequest, java.lang.String)
     */
    @Override
    protected boolean rememberMeRequested(HttpServletRequest request, String parameter) {
        String value = request.getHeader(DEFAULT_PARAMETER);
        return value != null && Boolean.parseBoolean(value) ? Boolean.parseBoolean(value) : super.rememberMeRequested(
                request, parameter);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices#extractRememberMeCookie
     * (javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected String extractRememberMeCookie(HttpServletRequest request) {
        String cookieValue = request.getHeader(super.getCookieName());
        return cookieValue;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices#setCookie(java.lang.String
     * [], int, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void setCookie(String[] tokens, int maxAge, HttpServletRequest request, HttpServletResponse response) {
        String cookieValue = encodeCookie(tokens);
        response.setHeader(super.getCookieName(), cookieValue);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices#
     * setTokenValiditySeconds(int)
     */
    @Override
    public void setTokenValiditySeconds(int tokenValiditySeconds) {
        Assert.isTrue(tokenValiditySeconds > 0, "tokenValiditySeconds must be positive for this implementation");
        super.setTokenValiditySeconds(tokenValiditySeconds);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices#setCookieName(java.lang
     * .String)
     */
    @Override
    public void setCookieName(String cookieName) {
        Assert.hasLength(cookieName, "Cookie name cannot be empty or null");
        super.setCookieName(cookieName);
    }

    /**
     * Sets the token validity device seconds.
     * 
     * @param tokenValidityDeviceSeconds
     *            the tokenValidityDeviceSeconds to set
     */
    public void setTokenValidityDeviceSeconds(int tokenValidityDeviceSeconds) {
        this.tokenValidityDeviceSeconds = tokenValidityDeviceSeconds;
    }

    /**
     * Gets the token validity device seconds.
     * 
     * @return the tokenValidityDeviceSeconds
     */
    public int getTokenValidityDeviceSeconds() {
        return tokenValidityDeviceSeconds;
    }

    /**
     * Sets the token validity web seconds.
     * 
     * @param tokenValidityWebSeconds
     *            the tokenValidityWebSeconds to set
     */
    public void setTokenValidityWebSeconds(int tokenValidityWebSeconds) {
        this.tokenValidityWebSeconds = tokenValidityWebSeconds;
    }

    /**
     * Gets the token validity web seconds.
     * 
     * @return the tokenValidityWebSeconds
     */
    public int getTokenValidityWebSeconds() {
        return tokenValidityWebSeconds;
    }

    /**
     * Sets the token subject.
     * 
     * @param tokenSubject
     *            the tokenSubject to set
     */
    public void setTokenSubject(String tokenSubject) {
        this.tokenSubject = tokenSubject;
    }

    /**
     * Gets the token subject.
     * 
     * @return the tokenSubject
     */
    public String getTokenSubject() {
        return tokenSubject;
    }

}
