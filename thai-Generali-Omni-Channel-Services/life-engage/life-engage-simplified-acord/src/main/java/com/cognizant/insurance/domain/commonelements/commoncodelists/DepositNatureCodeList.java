/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Enables to specify a reinsurer's deposit nature. 
 * 
 * @author 301350
 * 
 * 
 */
public enum DepositNatureCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Cash,
    /**
     * 
     * 
     * 
     * 
     */
    Stock,
    /**
     * 
     * 
     * 
     * 
     */
    Other
}
