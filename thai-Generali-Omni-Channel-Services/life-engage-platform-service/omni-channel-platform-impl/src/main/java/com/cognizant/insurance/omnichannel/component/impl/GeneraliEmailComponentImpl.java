package com.cognizant.insurance.omnichannel.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.impl.LifeEngageEmailComponentImpl;
import com.cognizant.insurance.component.repository.EmailRepository;
import com.cognizant.insurance.component.repository.ProductRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.BulkUploadComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliDocumentComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliEmailComponent;
import com.cognizant.insurance.omnichannel.utils.AdditionalQuestionnaireUtil;
import com.cognizant.insurance.omnichannel.utils.FileOperationsUtil;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;

/**
 * The Class class GeneraliEmailComponentImpl
 * 
 * @author 481774
 * 
 */
public class GeneraliEmailComponentImpl extends LifeEngageEmailComponentImpl implements GeneraliEmailComponent {

    @Value("${le.platform.service.email.auth}")
    private String mailAuth;

    @Value("${le.platform.service.email.authValue}")
    private String mailAuthValue;

    @Value("${le.platform.service.email.lts}")
    private String mailLTS;

    @Value("${le.platform.service.email.ltsEnable}")
    private String mailLTSEnable;

    @Value("${le.platform.service.email.hostName}")
    private String mailHostKey;

    @Value("${le.platform.service.email.portName}")
    private String mailPortName;

    @Value("${le.platform.service.email.host}")
    private String mailHost;

    @Value("${le.platform.service.email.port}")
    private String mailPort;

    @Value("${le.platform.service.email.templatePath}")
    private String templatePath;

    @Value("${le.platform.service.email.fromMailId}")
    private String fromMailId;
    
    @Value("${le.platform.service.bulkUploademail.ccMailId}")
    private String bulkUploadccMailIds;
    
   @Value("${le.platform.service.email.bccId}")
    private String bccMailId;
    
    /** Bulk upload Folder path */
	@Value("${le.platform.service.folderPath}")
	public  String folderPath;
	
	@Value("${le.platform.service.email.fromMailPassword}")
	private String fromMailPassword;
	@Value("${le.platform.service.email.failedSPAJ.to}")
	private String failedSPAJToMailId;
	
	@Value("${generali.platform.bpm.fnaTemplateID}")
    private String fnaTemplateID;

	@Value("${generali.platform.bpm.staticPDFPath}")
	private String staticPDFPath;
	
	@Value("${generali.platform.bpm.commonMemoTemplateID}")
    private String commonMemoTemplateId;
	
	@Value("${generali.platform.bpm.COFMemoTemplateID}")
    private String memoCOFTemplateId;
	
	@Value("${le.platform.default.eAppTemplate}")
	private String eAppTemplateId;
	
	@Value("${le.platform.default.previousPolicyTemplateId}")
	private String previousPolicyTemplateId;

	@Value("${generali.platform.service.memoEmail.subject}")
    private String memoEmailSubject;
	
	@Value("${generali.platform.service.pdf.masterpassword}")
	private String masterPassword;
    
    /** The Constant ID. */
    private static final String ID = "id";

    /** The Constant ILLUSTRATION_ID. */
    private static final String ILLUSTRATION_ID = "illustrationId";
    
    /** The Constant SPAJ_NUMBER. */
    private static final String SPAJ_NUMBER = "spajNumber";
    
    /** The Constant LANGUAGE **/
    private static final String LANGUAGE = "language";
    
    /** The Constant AdditionalQuestionnaire_case. */
    private static final String IS_ADDITIONAL_QUESTIONNAIRE_CASE = "isAdditionalQuestionnaireCase";

    /** The Constant IN. */
    private static final String VT = "vt";
    
    private static final String TH = "th";

    /** The Constant EN. */
    private static final String EN = "en";

    /** The Constant EMAIL_ID. */
    private static final String EMAIL_ID = "toMailIds";
    
    /** The Constant RDS_ILLUS_ID */
    private static final String RDS_ILLUS_ID = "RDS_ILLUS_ID";

    /** The Constant CC_MAIL_ID. */
    private static final String CC_MAIL_ID = "ccMailIds";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant TYPE. */
    private static final String USER_ID = "userId";

    /** The Constant pdf type. */
    private static final String PDF_TYPE = "application/pdf";
    
    /** The Constant pdf type. */
    private static final String EXCEL_TYPE = "application/vnd.ms-excel";

    /** The Constant MAIL_SUBJECT. */
    private static final String MAIL_SUBJECT = "mailSubject";
    
    /** The Constant MAIL_SUBJECT. */
    private static final String MAIL_SUBJECT_bulk = "Bulk Upload Results";


    /** The Constant email msg content. */
    private static final String EMAIL_MSG_CONTENT = "text/html; charset=utf-8";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant PDF. */
    private static final String PDF = ".pdf";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";

    /** The Constant template resource loader. */
    private static final String TEMPLATE_RESOURCE_LOADER = "resource.loader";

    /** The Constant template resource loader class. */
    private static final String TEMPLATE_RESOURCE_LOADER_CLASS = "file.resource.loader.class";

    /** The Constant template resource path. */
    private static final String TEMPLATE_RESOURCE_PATH = "file.resource.loader.path";

    /** The Constant template resource loader value. */
    private static final String TEMPLATE_RESOURCE_LOADER_VALUE = "file";

    /** The Constant template resource loader class value. */
    private static final String TEMPLATE_RESOURCE_LOADER_CLASS_VALUE =
            "org.apache.velocity.runtime.resource.loader.FileResourceLoader";
    
    /** The Constant template resource loader class value. */
    private static final String TEMPLATE_LOG_CLASS =
            "runtime.log.logsystem.class";
    
    /** The Constant template resource loader class value. */
    private static final String TEMPLATE_LOG_CLASS_VALUE =
            "org.apache.velocity.runtime.log.NullLogSystem";

    /** The Constant TEMPLATE_VT **/
    private static final String TEMPLATE_VT = "template_vt";
    
    private static final String TEMPLATE_TH = "template_th";

    /** The Constant TEMPLATE_EN **/
    private static final String TEMPLATE_EN = "template_en"; 
    
    /** The Constant TEMPLATE_ILLUSTRATION_COPY to attch with SPAJ **/
    private static final String TEMPLATE_ILLUSTRATION_COPY = "template_illustration_copy";

    /** The Constant VELOCITY_TEMP_IN **/
    private static final String VELOCITY_TEMP_IN = "template_in.vm";

     /** The Constant VELOCITY_TEMP_EN **/
    private static final String VELOCITY_TEMP_EN = "template_en.vm";  
    /** The Constant VELOCITY_TEMP_ILLUS_EN **/
   // private static final String VELOCITY_TEMP_ILLUS_VIETNAM = "email_Template_BI.vm";
    
    private static final String VELOCITY_TEMP_ILLUS_THAILAND = "email_Template_thai_SI.vm";
    
    /** The Constant VELOCITY_TEMP_FNA **/
    private static final String VELOCITY_TEMP_FNA = "template_fna.vm";    
    
    /** The Constant VELOCITY_TEMP_FNA **/
    private static final String VELOCITY_TEMPLATE_EAPP = "template_SPAJ.vm";
    
    /** The Constant VELOCITY_TEMP_SPAJ **/
    private static final String VELOCITY_TEMPLATE_EAPP_WITHOUT_ADD_QUESTIONNAIRE = "template_SPAJ_without_questionnaire.vm";
    
    private static final String EAPP_EMAIL_TEMPLATE = "email_Template_EAPP.vm";
    
    private static final String PAYMENT_EMAIL_TEMPLATE = "email_Template_thai_Payment.vm";
    
    private static final String MEMO_EMAIL_TEMPLATE = "email_Template_Memo.vm";

    
    /** The Constant VELOCITY_TEMPLATE_LMS_BULK_UPLOAD_BANCA **/
    private static final String VELOCITY_TEMPLATE_LMS_BULK_UPLOAD_BANCA = "template_BULK_upload_Banca.vm";
    
    /** The Constant VELOCITY_TEMPLATE_LMS_BULK_UPLOAD_AGENCY **/
    private static final String VELOCITY_TEMPLATE_LMS_BULK_UPLOAD_AGENCY = "template_BULK_upload_Agency.vm";
    
    /** The Constant FNA. */
    private static final String FNA = "FNA";
    
        
    /** The Constant LEAD_NAME **/
    private static final String LEAD_NAME = "leadName";
    
	/** The Constant RELATED **/
	private static final String RELATED = "related";

	/** The Constant CID **/
	private static final String CID = "SignatureId";

	/** The Constant SIGNATURE **/
	private static final String SIGNATURE = "signature.png";
	
	 /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = "TransactionData";
    
    /** The Constant Email. */
    private static final String EMAIL = "Email";
    
    /** The Constant Key4. */
    private static final String KEY_4 = "Key4";

    /** The Constant Key3. */
    private static final String KEY_3 = "Key3";
    
    /** The Constant Key24. */
    private static final String KEY24 = "Key24";
   
    /** The Constant Key21. */
    private static final String KEY21 = "Key21";
    
    /** The Constant PRODUCT_DETAILS. */
    private static final String PRODUCT_DETAILS = "ProductDetails";
    
    /** The Constant PRODUCT. */
    private static final String PRODUCT = "Product";
    
    /** The Constant PRODUCTCODE */
    private static final String PRODUCT_CODE = "productCode"; 
    
    /** The Constant AGENT_NAME **/
    private static final String AGENT_NAME = "agentName";

    /** The Constant AGENT_EMAIL_ID **/
    private static final String AGENT_EMAIL_ID = "agentEmailId";
    
    /** The Constant KEY2. */
    private static final String KEY2 = "Key2";
    
    /** The Constant TEMPLATES. */
    private static final String TEMPLATES = "templates";
    
    /** The Constant FNA_PDF_TEMPLATE. */
    private static final String FNA_PDF_TEMPLATE = "fnaPdfTemplate";
    
    /** The Constant PREMIUMSUMMARY **/
    private static final String PREMIUMSUMMARY = "premiumSummary";
    
    /** The Constant TOTALPREMIUM **/
    private static final String TOTALPREMIUM = "totalAnnualPremium";
    
    /** The Constant PRODUCT_NAME. */
    private static final String PRODUCT_NAME = "productName";

    private static final String ILLUSTRATION_ = "Illustration_";
    
    /** The Constant CC_EMAIL_ID. */
    private static final String CC_EMAIL_ID = "ccMailIds";
    
    /** The Constant EMAIL_FROM_ADDRESS. */
    private static final String EMAIL_FROM_ADDRESS = "fromMailId";
    
    /** The Constant AGENT ID. */
    private static final String AGENT_ID = "agentId";
    
    /** The Constant DATE_FORMAT **/
    private static final String DATE_FORMAT = "dd/MM/yyyy";

    /** The Constant TIME_FORMAT **/
    private static final String TIME_FORMAT = "HH:mm:ss";
    
    /** The Constant TIME_FORMAT **/
    private static final String TIME_FORMAT_WITHOUT_SECONDS = "HH:mm";
    
    /** The Constant DATE_FORMAT **/
    private static final String DATE_FORMAT_WITHOUT_SLASH = "yyyyMMdd";

    
    /** The Constant EMAIL_TEMPLATE. */
    private static final String EMAIL_TEMPLATE = "emailTemplate";
    
    /** The Constant DATE **/
    private static final String DATE = "date";

    /** The Constant TIME **/
    private static final String TIME = "time";
    
    /** The Constant DAY **/
    private static final String DAY = "day";
    
    /** The Constant daysEN **/
    private static final String[] daysEN = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
            "Saturday" };

    /** The Constant daysIN **/
    private static final String[] daysIN = { "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu" };
    
    private static final String INITIALISED = "initialised";
    
    /** The Constant EMAIL_JSON. */
    private static final String EMAIL_JSON = "emailJson";
    
    /** The Constant ATTEMPT_NUMBER. */
    private static final String ATTEMPT_NUMBER = "attemptNo";
    
    private static final String CURRENT_DATE="currentDate";    
   
    public static final String DATEFORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss"; 
    
    /** The Constant AGENTINFO. */
    private static final String AGENTINFO = "AgentInfo";
    
    /** The Constant ISRDSUSER. */
    private static final String ISRDSUSER = "isRDSUser";
    
  //Added for Thailand
    private static final String FULL_NAME = "fullName";
    
    private static final String INSURED = "Insured";
    
    private static final String BASICDETAILS = "BasicDetails";
    
    private static final String GENERALI_THAILAND = "Generali Thailand-";
    
    /** The Constant Key18. */
    private static final String KEY_18 = "Key18";

    /** The Constant Key5. */
    private static final String KEY_5 = "Key5";

    /** The Constant Key6. */
    private static final String KEY_6 = "Key6";
    
    @Value("${generali.platform.service.agentReportEmail.to}")
    private String agentReportToEmail;
    
    @Value("${generali.platform.service.agentReportEmail.cc}")
    private String agentReportCCEmail;    

    @Value("${generali.platform.service.fnaEmail.subject}")
    private String fnaMailSubject;
    
    private static final String VELOCITY_AGENT_MONTHLY = "agent_Template_Monthly.vm";
    
    private static final String VELOCITY_AGENT_WEEKLY = "agent_Template_Weekly.vm";
    
    @Autowired
    private GeneraliDocumentComponent lifeEngageDocumentComponent;
    
    @Autowired
	private EmailRepository emailRepository;
    
    @Autowired
    private ProductRepository productRepository;
    
    
    @Autowired
    private AdditionalQuestionnaireUtil additionalQuestionnaireUtil;
    
    @Autowired
	BulkUploadComponent bulkUploadComponenet;
    
    @Autowired
    Job job;

    @Autowired
    JobLauncher jobLauncher;
    
    @Autowired
	private FileOperationsUtil fileOperationsUtil;

	
    
    /** The Constant APPLICATION_ALL_TYPE. */
    private static final String APPLICATION_ALL_TYPE= "application/*";

    private static final String USER_PASSWORD = null;

    private static final String OWNER_PASSWORD = null;

    private static final String EMAILSUBJECT = "mailSubject";

    private static final String QUOTATION = "ใบเสนอราคา  ";

    private static final String FORYOU = "สำหรับคุณ ";

    @Autowired
    @Qualifier("eMailDetailsUpdateMapping")
    private LifeEngageSmooksHolder eMailUpdateHolder;
    
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.impl.LifeEngageEmailComponentImpl#sendEmail(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public StatusData sendEmail(String json, String templateName, String templateValues, String pdfName)
            throws BusinessException {

        Properties mailProperties = null;
        String emailStatus = Constants.SUCCESS;
        final StatusData statusData = new StatusData();
        statusData.setStatus(emailStatus);

        try {
        	
        	
            mailProperties = new Properties();
            mailProperties.put(mailAuth, mailAuthValue);
            mailProperties.put(mailLTS, mailLTSEnable);
            mailProperties.put(mailHostKey, mailHost);
            mailProperties.put(mailPortName, mailPort);

         final Session session = Session.getDefaultInstance(mailProperties, null); // for Dev testing
            
           //Uncomment for Local testing
            /*final Session session = Session.getDefaultInstance(mailProperties, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(fromMailId, fromMailPassword);
                 }
              });*/

            final MimeMessage mimeMessage = new MimeMessage(session);
            final MimeBodyPart messagePart = new MimeBodyPart();
            final Multipart multiPart = new MimeMultipart(RELATED);
            Properties templateProperties = new Properties();
            templateProperties.setProperty(TEMPLATE_RESOURCE_LOADER, TEMPLATE_RESOURCE_LOADER_VALUE);
            templateProperties.setProperty(TEMPLATE_RESOURCE_LOADER_CLASS, TEMPLATE_RESOURCE_LOADER_CLASS_VALUE);
            templateProperties.setProperty(TEMPLATE_RESOURCE_PATH, templatePath);
        	templateProperties.setProperty(TEMPLATE_LOG_CLASS, TEMPLATE_LOG_CLASS_VALUE);
            VelocityEngine ve = new VelocityEngine();
            ve.init(templateProperties);
            /* create a context and add data */
			
            VelocityContext context = new VelocityContext();
            final InternetAddress fromInternetAddress = new InternetAddress(fromMailId);
            final JSONObject emailObj = new JSONObject(json);
            final String type = emailObj.getString(TYPE);         
            final String agentName = emailObj.has(AGENT_NAME)?emailObj.getString(AGENT_NAME):StringUtils.EMPTY;  
            final String language = emailObj.has(LANGUAGE)?emailObj.getString(LANGUAGE):StringUtils.EMPTY; 
            final JSONObject userEmailObject = emailObj.has(GeneraliConstants.EMAIL_TEMPLATE)?emailObj.getJSONObject(GeneraliConstants.EMAIL_TEMPLATE):null;
            final String userEmail = userEmailObject.has(GeneraliConstants.PAYMENT_URL)?userEmailObject.getString(GeneraliConstants.PAYMENT_URL):StringUtils.EMPTY;
            final String additionalQuestionnaireCase = emailObj.has(IS_ADDITIONAL_QUESTIONNAIRE_CASE)?emailObj.getString(IS_ADDITIONAL_QUESTIONNAIRE_CASE):StringUtils.EMPTY;
            String bcceMailId="";
            
            if (ILLUSTRATION.equals(type)) {
               	templateName = VELOCITY_TEMP_ILLUS_THAILAND;
            } else if (FNA.equals(type)) {
                templateName = VELOCITY_TEMP_FNA;
            } else if (E_APP.equals(type)) {                
                bcceMailId=bccMailId.replaceAll(";", ",");
            		templateName=EAPP_EMAIL_TEMPLATE;		
            } else if (GeneraliConstants.MEMO.equals(type)){
                templateName=MEMO_EMAIL_TEMPLATE;
            }
            else if(GeneraliConstants.PAYMENT_BY_URL.equals(type)){
            	templateName = PAYMENT_EMAIL_TEMPLATE;
            }
            else if(GeneraliConstants.TYPE_AGENCY.equals(type)){
            	templateName=VELOCITY_TEMPLATE_LMS_BULK_UPLOAD_AGENCY;
            }else if(GeneraliConstants.TYPE_BANCA.equals(type)){
                templateName=VELOCITY_TEMPLATE_LMS_BULK_UPLOAD_AGENCY;
            }
            //Template template = ve.getTemplate(templateName);
            Template template = ve.getTemplate(templateName, "UTF-8");
            final String emailId = emailObj.has(EMAIL_ID)?emailObj.getString(EMAIL_ID):StringUtils.EMPTY;
            final String id = emailObj.has(ID)?emailObj.getString(ID):StringUtils.EMPTY;
            final String illustrationId = emailObj.has(ILLUSTRATION_ID)?emailObj.getString(ILLUSTRATION_ID):StringUtils.EMPTY;
            
            
           final JSONObject templateObj = new JSONObject(templateValues);
           String spajNumber = templateObj.has(SPAJ_NUMBER)?templateObj.getString(SPAJ_NUMBER):StringUtils.EMPTY;
           String leadName = templateObj.has(LEAD_NAME)?templateObj.getString(LEAD_NAME):StringUtils.EMPTY;
            
           //Thailand Changes - CustomerName dynamically loaded in mail template
           Map<String, Object> contextValues = new HashMap<String, Object>();
           contextValues.put("leadName", leadName);
           contextValues.put("paymentUrl", userEmail);
           contextValues.put("agentName", agentName);
            Iterator<?> keys = templateObj.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                String value = (String) templateObj.get(key);
                context.put(key, contextValues.get(key));
            }
           
            /* rendering the template into a StringWriter */
            StringWriter writer = new StringWriter();
            template.merge(context, writer);
            final String emailMessage = writer.toString();
            final String ccedMailId = emailObj.has(CC_MAIL_ID)?emailObj.getString(CC_MAIL_ID):StringUtils.EMPTY;            
            final String subject = emailObj.has(MAIL_SUBJECT)?emailObj.getString(MAIL_SUBJECT):StringUtils.EMPTY;
            
            String toMailIds=emailId.replaceAll(";", ",");
            String ccMailIds=ccedMailId.replaceAll(";", ",");
            
            
           
			mimeMessage.setFrom(fromInternetAddress);

			mimeMessage.setRecipients(Message.RecipientType.CC,
					InternetAddress.parse(ccMailIds));

			mimeMessage.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(toMailIds));
           
			if (null != bcceMailId && !bcceMailId.equals("")) {
				mimeMessage.setRecipients(Message.RecipientType.BCC,
						InternetAddress.parse(bcceMailId));
			}
			mimeMessage.setSubject(subject ,"UTF-8");
            mimeMessage.setSentDate(new Date());
            messagePart.setContent(emailMessage, EMAIL_MSG_CONTENT);
            //Adding Inline Image
            
            MimeBodyPart imagePart = new MimeBodyPart();
            imagePart.attachFile(templatePath+"/"+SIGNATURE);
            imagePart.setContentID("<" + CID + ">");

            imagePart.setDisposition(MimeBodyPart.INLINE);
            multiPart.addBodyPart(messagePart);
            multiPart.addBodyPart(imagePart);
            String template_vt = "";
            String template_th = "";
            String template_illustration_copy="";
            String attachmentName;

            if (E_APP.equals(type)) {
            	
            	template_th = eAppTemplateId;
            	
            	//Eapp PDF
            	addAttachment(multiPart, spajNumber+"_"+leadName+ PDF, id, type, template_th);
            	//Previous Policy -Previous Policy Details_Application number
            	addAttachment(multiPart, "Previous Policy Details_"+spajNumber+ PDF, id, type, previousPolicyTemplateId);
            	
            	
            } 
            else if (ILLUSTRATION.equals(type)) {
                template_th = emailObj.getString(TEMPLATE_TH);
                if (illustrationId != null && !"".equals(illustrationId)) {
                    attachmentName = illustrationId;                 
                }
                
                String RDS_IllusID = emailObj.getString(RDS_ILLUS_ID);
                if (RDS_IllusID != null && !"".equals(RDS_IllusID)) {
                	
                	attachmentName = RDS_IllusID;
                }
                else
                {
                	attachmentName = id;
                }
               
                String attachmentNameThailand = "";
                attachmentNameThailand = attachmentName+"_TH" + PDF;      
                addAttachment(multiPart, attachmentNameThailand, id, type, template_th);
                } else if (FNA.equals(type)) {
                template_th = fnaTemplateID;
               
              
               
                attachmentName = "FNA" + PDF;
                addAttachment(multiPart, attachmentName, id, type, template_th);
            } else if (GeneraliConstants.MEMO.equals(type)) {
            	//Counter Memo Code
				List<String> counterMemoList = new ArrayList<String>();

				counterMemoList.add("5CF");
				counterMemoList.add("CTF");
				counterMemoList.add("XCL");
        		
        		
                //Need to add memo logic
                JSONArray memoJsonArray = emailObj.getJSONArray(GeneraliConstants.MEMO_DETAILS);
                template_th = commonMemoTemplateId;
                for(int i=0;i< memoJsonArray.length();i++){
                String fileName = memoJsonArray.getJSONObject(i).getString(GeneraliConstants.MEMO_PDF);
                String memoStatus = memoJsonArray.getJSONObject(i).getString(GeneraliConstants.MEMO_STATUS);
                String memoCode = memoJsonArray.getJSONObject(i).getString(GeneraliConstants.MEMO_CODE);
                if((fileName!= null && !fileName.trim().isEmpty()) && (memoStatus.equals(GeneraliConstants.OPEN) || memoStatus.equals(GeneraliConstants.PENDING_UW))){
                    File f = new File(staticPDFPath+fileName+PDF);
                    if(f.exists()){
                        DataSource fds = new FileDataSource(f);
                        MimeBodyPart attachmentPart = new MimeBodyPart();
                        attachmentPart.setDataHandler(new DataHandler(fds));
                        attachmentPart.setFileName(memoJsonArray.getJSONObject(i).getString(GeneraliConstants.MEMO_PDF)+PDF);
                        multiPart.addBodyPart(attachmentPart);
                    }else{
                        LOGGER.error("STATIC PDF NOT FOUND");
                    }
                }
                    if(counterMemoList.contains(memoCode) && !(memoStatus.equals(GeneraliConstants.CANCELLED)|| memoStatus.equals(GeneraliConstants.RESOLVED))){
                        template_th = memoCOFTemplateId;
                    }
                }
                addAttachment(multiPart, GeneraliConstants.MEMO+ PDF, spajNumber, type, template_th);
                
            }
            
            mimeMessage.setContent(multiPart);
            Transport.send(mimeMessage);
            
           
        } catch (ParseException e) {
            statusData.setStatus(Constants.FAILURE);
            statusData.setStatusMessage(e.getMessage());
        } catch (MessagingException e) {
            statusData.setStatus(Constants.FAILURE);
            statusData.setStatusMessage(e.getMessage());
        } catch (Exception e) {
            statusData.setStatus(Constants.FAILURE);
            e.printStackTrace();
            statusData.setStatusMessage(e.getMessage());
        }
        return statusData;

    }

    private void addAttachment(Multipart multiPart, String pdfName, String id, String type, String tempalteId)
            throws ParseException, IOException, MessagingException, BusinessException {

        ByteArrayOutputStream byteStream = null;
        
        List<ByteArrayOutputStream> byteStreamList = new ArrayList<ByteArrayOutputStream>();
        if (E_APP.equals(type)) {
            byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent.generateEncryptedPdf(id, type, tempalteId,masterPassword);
            byteStreamList.add(byteStream);
        } else if (ILLUSTRATION.equals(type)) {
            byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent.generateIllustrationPdf(id, type, tempalteId);
            byteStreamList.add(byteStream);
        } else if (FNA.equals(type)) {
            byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent.generateFNAReport(id, type, tempalteId);
            byteStreamList.add(byteStream);
        } else if (GeneraliConstants.MEMO.equals(type)){
        	byteStreamList=  (List<ByteArrayOutputStream> ) lifeEngageDocumentComponent.generateMemoPDF(id, true);
           
        }
        for(ByteArrayOutputStream stream:byteStreamList){
	        InputStream pdfByteArray = new ByteArrayInputStream(stream.toByteArray());
	        //passwordEncrypt(pdfByteArray);
	        ByteArrayDataSource fileDataSource = new ByteArrayDataSource(pdfByteArray, PDF_TYPE);
	        MimeBodyPart attachmentPart = new MimeBodyPart();
	        attachmentPart.setDataHandler(new DataHandler(fileDataSource));
	        attachmentPart.setFileName(MimeUtility.encodeText(pdfName,"UTF-8",null));
	       
	        multiPart.addBodyPart(attachmentPart);
        }
    }
        
   /*private void passwordEncrypt(InputStream pdfByteArray) throws IOException {
        try{
            File file = new File( "D://PDFEncryptionTest//547059.pdf" );
            FileOutputStream pdfFileout = new FileOutputStream( file );
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance( document, pdfFileout );
            
            String user = "test", owner = "test";
            
            writer.setEncryption( user.getBytes(), owner.getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128 );
            writer.createXmpMetadata();
            document.open();
            document.add(new Paragraph("This is create PDF with Password demo."));
            document.close();
            System.out.println("Done");
        }
       catch(Exception e){
        
        }
    }*/
    
     /**
	 * Save life engage email (AgentPortal Service)
	 * 
	 * @param jsonObj
	 *            the json obj
	 * @return the life engage email
	 * @throws ParseException
	 *             the parse exception
	 */
    @Override
	protected LifeEngageEmail saveLifeEngageEmail(final JSONObject jsonObj)
			throws ParseException {
		LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
		if(((String) jsonObj.getString(KEY_4)).isEmpty() && ((String) jsonObj.getString(KEY_3)).isEmpty() && ((String) jsonObj.getString(KEY2)).isEmpty()){
		    return lifeEngageEmail;
		}
		final JSONObject emailObject = new JSONObject();
		final JSONObject emailTemplateObj = new JSONObject();

		String agentId = (String) jsonObj.getString(Constants.KEY11);

		final JSONObject emailObj = jsonObj.getJSONObject(TRANSACTIONDATA)
				.getJSONObject(EMAIL);
		String toMailIds = (String) emailObj.getString(EMAIL_ID);
		if("undefined".equals(toMailIds)){
			toMailIds="";
		}
		String ccMailIds = (String) emailObj.getString(CC_MAIL_ID);
		if("undefined".equals(ccMailIds)){
        	ccMailIds="";
		}
		String fromMailId = "";
		String language = (String) emailObj.getString(LANGUAGE);
		String agentName = "";
		String agentEmailId = "";
		String leadName = "";
		String spajNumber = "";
		String fullName = "";

		String type = "";
		String id = "";
		String illustrationId = "";		
		String RDS_IllusID = "";
       
        String additionalQuestionnaireCase = "false";

		String template_en = "";
		String template_in = "";
		String template_th = "";
		 String template_illustration_copy = "";
		type = jsonObj.getString(TYPE);

		Response<List<ProductTemplateMapping>> templateResponse = null;
		 Response<List<ProductTemplateMapping>> templateResponseIllusCopy = null;
		List<ProductTemplateMapping> templateMappings = null;

		if (E_APP.equals(jsonObj.getString(TYPE))) {

			if (emailObj.has(LEAD_NAME)) {
				leadName = (String) emailObj.getString(LEAD_NAME);
			}
			if("undefined".equals(leadName)){
				leadName="";
			}
			if (emailObj.has(SPAJ_NUMBER)) {
				spajNumber = (String) emailObj.getString(SPAJ_NUMBER);
			}
			illustrationId=(String)(jsonObj.getString(KEY24));
			spajNumber = (String) jsonObj.getString(KEY21);
			id = (String) jsonObj.getString(KEY_4);
			if (additionalQuestionnaireUtil.isAdditionalInsuredPresent(jsonObj)) {
				additionalQuestionnaireCase = "true";
			}
			templateResponse = productRepository
					.getProductTemplatesByFilter(buildProductSearchCriteria(jsonObj));
			templateResponseIllusCopy = productRepository
					.getProductTemplatesByFilter(buildProductSearchCriteriaForEapp(
							jsonObj, ILLUSTRATION));
		

		} else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {		
			
			String  rdsId = "";
			/*final JSONObject agentObj = jsonObj.getJSONObject(TRANSACTIONDATA)
					.getJSONObject(AGENTINFO);*/

			if (jsonObj.getJSONObject(TRANSACTIONDATA).has(AGENTINFO)) {
				final JSONObject agentObj = jsonObj.getJSONObject(
						TRANSACTIONDATA).getJSONObject(AGENTINFO);
				if (agentObj.has(ISRDSUSER)) {
					rdsId = (String) agentObj.getString(ISRDSUSER);
				}
			}
			
			id = (String) jsonObj.getString(KEY_3);

			if (rdsId.equalsIgnoreCase("True")) {
				RDS_IllusID = (String) jsonObj.getString(KEY24);
			}

			// Getting template list from product service - based on the
			// language and product
			 
             
			templateResponse = productRepository
					.getProductTemplatesByFilter(buildProductSearchCriteria(jsonObj));
			
            
			agentName = (String) emailObj.getString(AGENT_NAME);
			agentEmailId = (String) emailObj.getString(AGENT_EMAIL_ID);
			leadName = (String) emailObj.getString(LEAD_NAME);

		} else if (FNA.equals(jsonObj.getString(TYPE))) {

			template_in = jsonObj.getJSONObject(TRANSACTIONDATA)
					.getJSONObject(TEMPLATES).getString(FNA_PDF_TEMPLATE);

			id = (String) jsonObj.getString(KEY2);
			emailObject.put(Constants.PDF_NAME, FNA);
			leadName = (String) emailObj.getString(LEAD_NAME);
			
        } else if (GeneraliConstants.MEMO.equals(jsonObj.getString(TYPE))) {
            if (emailObj.has(LEAD_NAME)) {
                leadName = (String) emailObj.getString(LEAD_NAME);
            }
            if("undefined".equals(leadName)){
                leadName="";
            }
            spajNumber = (String) jsonObj.getString(KEY21);
            id = (String) jsonObj.getString(KEY_4);
            emailObject.put(GeneraliConstants.MEMO_DETAILS, jsonObj.getJSONObject(TRANSACTIONDATA)
                .get(GeneraliConstants.MEMO_DETAILS));
            
        }

		if (null != templateResponse) {
			templateMappings = templateResponse.getType();
		}
		if (templateMappings != null && !templateMappings.isEmpty()) {
			for (ProductTemplateMapping productTemplateMapping : templateMappings) {
				if (EN.equals(productTemplateMapping.getProductTemplates()
						.getLanguage())) {
					template_en = productTemplateMapping.getProductTemplates()
							.getTemplateId();
				} else if (TH.equals(productTemplateMapping
						.getProductTemplates().getLanguage())) {
					template_th = productTemplateMapping.getProductTemplates()
							.getTemplateId();
				}
			}

		}

		 if (null != templateResponseIllusCopy) {               
             templateMappings = templateResponseIllusCopy.getType();
         }
         if (templateMappings != null && !templateMappings.isEmpty()) {
             for (ProductTemplateMapping productTemplateMapping : templateMappings) {
             	if (TH.equals(productTemplateMapping.getProductTemplates().getLanguage())) {
             	template_illustration_copy=productTemplateMapping.getProductTemplates().getTemplateId();
             	}
             }

         }
		//String premium = "";
		String productName = "";
		String mailSubject = "";

		if (ILLUSTRATION.equals(type)) {
			final JSONObject productObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(PRODUCT);	
			final JSONObject insuredObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(INSURED).getJSONObject(BASICDETAILS);
			leadName = insuredObj.getString(FULL_NAME);
			productName = productObj.getJSONObject(PRODUCT_DETAILS).getString(PRODUCT_NAME);
			//Framing mail subject from UI, if not present in UI then from service side
			mailSubject = StringUtils.isEmpty(emailObj.getString(EMAILSUBJECT))? 
			        GENERALI_THAILAND + "_" + QUOTATION +productName + FORYOU  + leadName : emailObj.getString(EMAILSUBJECT);
		} else if (FNA.equals(type)) {
			String leadNameUpper = "";
			if (null != leadName) {
				leadNameUpper = leadName.toUpperCase();
			}
			 mailSubject = fnaMailSubject;
		
		} else if (E_APP.equals(type)) {
			// TO DO
			String proposalNumber = (String) jsonObj.getString(KEY_4);
			mailSubject = "Pengajuan Asuransi Jiwa (SPAJ) Generali Indonesia No. "
					+ spajNumber;
		} else if (GeneraliConstants.MEMO.equals(type)){
		   mailSubject = memoEmailSubject.replace("[Application number]", spajNumber);
	        
		}

		emailObject.put(MAIL_SUBJECT, mailSubject);
		emailObject.put(TEMPLATE_TH, template_th);
		//emailObject.put(TEMPLATE_EN, template_en);
		emailObject.put(TEMPLATE_ILLUSTRATION_COPY, template_illustration_copy);
        emailObject.put(IS_ADDITIONAL_QUESTIONNAIRE_CASE, additionalQuestionnaireCase);
		emailObject.put(TYPE, type);
		emailObject.put(EMAIL_ID, toMailIds);
		emailObject.put(CC_EMAIL_ID, ccMailIds);
		emailObject.put(EMAIL_FROM_ADDRESS, fromMailId);
		emailObject.put(AGENT_ID, agentId);
		emailObject.put(LANGUAGE, language);
		emailObject.put(RDS_ILLUS_ID, RDS_IllusID);

		emailTemplateObj.put(LEAD_NAME, leadName);
		emailTemplateObj.put(AGENT_NAME, agentName);
		emailTemplateObj.put(AGENT_EMAIL_ID, agentEmailId);
		emailTemplateObj.put(SPAJ_NUMBER, spajNumber);


		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT);
		emailObject.put(ILLUSTRATION_ID, illustrationId);
		Date date = new Date();
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);
		int day = cal.get(Calendar.DAY_OF_WEEK);

		emailTemplateObj.put(DATE, dateFormat.format(date));
		emailTemplateObj.put(TIME, timeFormat.format(date));

		if (VT.equals(language)) {
			emailTemplateObj.put(DAY, daysIN[day - 1]);

		} else if (EN.equals(language)) {
			emailTemplateObj.put(DAY, daysEN[day - 1]);
		}

		emailObject.put(ID, id);
		emailObject.put(EMAIL_TEMPLATE, emailTemplateObj);
		lifeEngageEmail.setSendTime(LifeEngageComponentHelper
				.getCurrentdate());
		lifeEngageEmail.setEmailValues(emailObject.toString());
		lifeEngageEmail.setAttemptNumber("1");
		lifeEngageEmail.setStatus(INITIALISED);
		lifeEngageEmail.setAgentId(agentId);
		lifeEngageEmail.setType(type);
		lifeEngageEmail = emailRepository
				.savelifeEngageEmail(lifeEngageEmail);
		
		 // Method to generate BPM PDF
      /*  if (E_APP.equals(type)) {
            final JSONObject bpmPdfObject = new JSONObject();
            bpmPdfObject.put(TYPE, type);
            bpmPdfObject.put(ID, id);
            bpmPdfObject.put(ILLUSTRATION_ID, illustrationId);
            bpmPdfObject.put(TEMPLATE_ILLUSTRATION_COPY, template_illustration_copy);
            bpmPdfObject.put(IS_ADDITIONAL_QUESTIONNAIRE_CASE, additionalQuestionnaireCase);
            bpmPdfObject.put(TEMPLATE_IN, template_in);
            bpmPdfObject.put(TEMPLATE_EN, template_en);

            lifeEngageDocumentComponent.generateBPMPdf(bpmPdfObject.toString());
        }*/
		
		
		return lifeEngageEmail;

	}

	/**
	 * Building search Criteria for the PDF templates
	 * 
	 * @param object
	 * @return
	 */
	private ProductSearchCriteria buildProductSearchCriteria(JSONObject object) {
		ProductSearchCriteria criteria = new ProductSearchCriteria();
		String type = object.getString(TYPE);
		final JSONObject productObj = object.getJSONObject(TRANSACTIONDATA)
				.getJSONObject(PRODUCT);
		String productCode = productObj.getJSONObject(PRODUCT_DETAILS)
				.getString(PRODUCT_CODE);
		String templateTypeCode = "";
		if (ILLUSTRATION.equals(type)) {
			templateTypeCode = GeneraliConstants.ILLUSTRATION_PDF_TEMPLATE_TYPE;
		} else if (E_APP.equals(type)) {
			templateTypeCode = GeneraliConstants.EAPP_PDF_TEMPLATE_TYPE;
		} else if (FNA.equals(type)) {
			templateTypeCode = GeneraliConstants.FNA_TEMPLATE_TYPE;
		}
		criteria.setId(Long.parseLong(productCode));
		criteria.setCarrierCode(GeneraliConstants.CARRIER_CODE);
		criteria.setTemplateType(templateTypeCode);
		return criteria;
	}
	
    private ProductSearchCriteria buildProductSearchCriteriaForEapp(JSONObject object, String type) {
        ProductSearchCriteria criteria = new ProductSearchCriteria();
        final JSONObject productObj = object.getJSONObject(TRANSACTIONDATA).getJSONObject(PRODUCT);
        String productCode = productObj.getJSONObject(PRODUCT_DETAILS).getString(PRODUCT_CODE);
        String templateTypeCode = "";
        if (ILLUSTRATION.equals(type)) {
            templateTypeCode = GeneraliConstants.ILLUSTRATION_PDF_TEMPLATE_TYPE;
        } else if (E_APP.equals(type)) {
            templateTypeCode = GeneraliConstants.EAPP_PDF_TEMPLATE_TYPE;
        } else if (FNA.equals(type)) {
            templateTypeCode = GeneraliConstants.FNA_TEMPLATE_TYPE;
        }
        criteria.setId(Long.parseLong(productCode));
        criteria.setCarrierCode(GeneraliConstants.CARRIER_CODE);
        criteria.setTemplateType(templateTypeCode);
        return criteria;
    }

    public void triggerEmail(final String agentId, final String type) throws BusinessException {
        try {
            List<LifeEngageEmail> lifeEngageEmails = emailRepository.getInitialisedLifeEngageEmails(agentId, type);
            for (LifeEngageEmail email : lifeEngageEmails) {
                jobLauncher.run(
                        job,
                        new JobParametersBuilder().addLong(ID, email.getId())
                            .addString(EMAIL_JSON, email.getEmailValues())
                            .addString(ATTEMPT_NUMBER, email.getAttemptNumber())
                            .addDate(CURRENT_DATE, new Date())
                            .toJobParameters());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void addBase64Attachment(Multipart multiPart, String pdfName, String base64Value) throws MessagingException {
        if (null != base64Value) {
            MimeBodyPart attachmentPart = new MimeBodyPart();
            byte[] imageBytes = Base64.decodeBase64(base64Value);
            DataSource dataSource = new ByteArrayDataSource(imageBytes, APPLICATION_ALL_TYPE);
            attachmentPart.setDataHandler(new DataHandler(dataSource));
            attachmentPart.setFileName(pdfName);
            multiPart.addBodyPart(attachmentPart);
        }
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.impl.LifeEngageEmailComponentImpl#sendBulkUploadEmail(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)
     */

	public StatusData sendBulkUploadEmail(String agentId,String emailId,String type,Map<String, String> excelDataList)
			throws BusinessException {
		LOGGER.info("In BULK UPLOAD eMAI SENT"+ agentId+type);
		Properties mailProperties = null;
		String emailStatus = Constants.SUCCESS;
		final StatusData statusData = new StatusData();
		statusData.setStatus(emailStatus);
		String userId=agentId;
		String attachmentFileName1="";
		String attachmentFileName2="";
		String attachmentFileName3="";
		try {
			mailProperties = new Properties();
			mailProperties.put(mailAuth, mailAuthValue);
			mailProperties.put(mailLTS, mailLTSEnable);
			mailProperties.put(mailHostKey, mailHost);
			mailProperties.put(mailPortName, mailPort);

			/*final Session session = Session.getDefaultInstance(mailProperties,
					null);*/
			
			final Session session = Session.getDefaultInstance(mailProperties, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(fromMailId, fromMailPassword);
                 }
              });

			final MimeMessage mimeMessage = new MimeMessage(session);
			final MimeBodyPart messagePart = new MimeBodyPart();
			final Multipart multiPart = new MimeMultipart(RELATED);
			Properties templateProperties = new Properties();
			templateProperties.setProperty(TEMPLATE_RESOURCE_LOADER,
					TEMPLATE_RESOURCE_LOADER_VALUE);
			templateProperties.setProperty(TEMPLATE_RESOURCE_LOADER_CLASS,
					TEMPLATE_RESOURCE_LOADER_CLASS_VALUE);
			templateProperties
					.setProperty(TEMPLATE_RESOURCE_PATH, templatePath);
			templateProperties.setProperty(TEMPLATE_LOG_CLASS, TEMPLATE_LOG_CLASS_VALUE);
			VelocityEngine ve = new VelocityEngine();
			ve.init(templateProperties);

			/* create a context and add data */
			VelocityContext context = new VelocityContext();
			final InternetAddress fromInternetAddress = new InternetAddress(
					fromMailId);
		
			
			/*final JSONObject emailObj = new JSONObject(emailJson);
			
			final JSONObject templateObj = new JSONObject(templateJson);*/

		
			//final String type = emailObj.getString(TYPE);
			//userId= emailObj.getString(USER_ID);
			String templateName = "";
			 //String emailId = "";
			if(GeneraliConstants.TYPE_AGENCY.equals(type)){
                templateName=VELOCITY_TEMPLATE_LMS_BULK_UPLOAD_AGENCY;
                //emailId=bulkUploadAgencyToMailIds;
                
            }else if(GeneraliConstants.TYPE_BANCA.equals(type)){
                templateName=VELOCITY_TEMPLATE_LMS_BULK_UPLOAD_BANCA;
                //emailId=bulkUploadBancaToMailIds;
            }
			
			
			Template template = ve
					.getTemplate(templateName,"UTF-8");
			
			

			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			SimpleDateFormat dateFormatWithoutSlash = new SimpleDateFormat(DATE_FORMAT_WITHOUT_SLASH);
			SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT_WITHOUT_SECONDS);
			Date date = new Date();
			Calendar cal = GregorianCalendar.getInstance();
			cal.setTime(date);
			String dateAndTime = "";
			String dateTime = "";
			String validationCheckfailed = "";
			String successfullyAllocated = "";
			String unsuccessfullyAllocated = "";
			dateAndTime = dateAndTime.concat(timeFormat.format(date)).concat(" ").concat(dateFormat.format(date));
			dateTime = dateTime.concat(dateFormatWithoutSlash.format(date)).concat("_").concat(timeFormat.format(date));
			
			validationCheckfailed = dateTime.concat("_VALIDATION CHECK FAILED");
			successfullyAllocated = dateTime.concat("_SUCCESSFULLY ALLOCATED");
			unsuccessfullyAllocated = dateTime.concat("_UNSUCCESSFULLY ALLOCATED");
			
			Map<String, String> contextValues = new HashMap<String, String>();
            contextValues.put("userId", userId);
            contextValues.put("dateAndTime", dateAndTime);
            contextValues.put("validationCheckfailed", validationCheckfailed);
            contextValues.put("successfullyAllocated", successfullyAllocated);
            contextValues.put("unsuccessfullyAllocated", unsuccessfullyAllocated);
            
            
            if (contextValues != null && !contextValues.keySet().isEmpty()) {
                for (String key : contextValues.keySet()) {
                    context.put(key, contextValues.get(key));
                }

            }
            
			/* rendering the template into a StringWriter */
			StringWriter writer = new StringWriter();
			template.merge(context, writer);
			final String emailMessage = writer.toString();
			//final String ccedMailId = emailObj.getString(CC_MAIL_ID);
			//final String subject = emailObj.getString(MAIL_SUBJECT);
			
			final String ccedMailId = bulkUploadccMailIds;
			final String subject =MAIL_SUBJECT_bulk+dateAndTime;;

			mimeMessage.setFrom(fromInternetAddress);
			
			 String toMailIds=emailId.replaceAll(";", ",");
	         String ccMailIds=ccedMailId.replaceAll(";", ",");
				
			mimeMessage.setRecipients(Message.RecipientType.CC,
						InternetAddress.parse(ccMailIds));

			mimeMessage.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(toMailIds));

			mimeMessage.setSubject(subject);
			mimeMessage.setSentDate(new Date());
			messagePart.setContent(emailMessage, EMAIL_MSG_CONTENT);

			multiPart.addBodyPart(messagePart);
			

			if (excelDataList.containsKey(GeneraliConstants.VALID)) {

				 attachmentFileName1 = excelDataList
						.get(GeneraliConstants.VALID);
				 if(!GeneraliConstants.JSON_EMPTY.equals(attachmentFileName1)){
				Workbook attachmentFile1 = new XSSFWorkbook(new FileInputStream(attachmentFileName1));
				addExcelAttachment(multiPart, attachmentFile1, successfullyAllocated+".xlsx","");
				 }
				
				
			}
			if (excelDataList.containsKey(GeneraliConstants.INVALID)) {
				attachmentFileName2 = excelDataList
						.get(GeneraliConstants.INVALID);
				 if(!GeneraliConstants.JSON_EMPTY.equals(attachmentFileName2)){
				Workbook attachmentFile2 = new XSSFWorkbook(new FileInputStream(attachmentFileName2));
				addExcelAttachment(multiPart, attachmentFile2,
						validationCheckfailed+".xlsx","");
				 }

			}

			if (excelDataList.containsKey(GeneraliConstants.AGENT_FAILED)) {
				
				 attachmentFileName3 = excelDataList
						.get(GeneraliConstants.AGENT_FAILED);
				 if(!GeneraliConstants.JSON_EMPTY.equals(attachmentFileName3)){
				Workbook attachmentFile3 = new XSSFWorkbook(new FileInputStream(attachmentFileName3));
				addExcelAttachment(multiPart, attachmentFile3,
						unsuccessfullyAllocated+".xlsx","");
				 }

			}

			mimeMessage.setContent(multiPart);
			Transport.send(mimeMessage);
		} catch (MessagingException e) {
			statusData.setStatus(Constants.FAILURE);
			LOGGER.error("Error in sending message "+userId + e.getMessage(), e);
			statusData.setStatusMessage(e.getMessage());
		} catch (Exception e) {
			statusData.setStatus(Constants.FAILURE);
			statusData.setStatusMessage(e.getMessage());
			LOGGER.error("Error in sending message "+userId + e.getMessage(), e);
		}
		if(Constants.SUCCESS.equals(statusData.getStatus()))
		{
			bulkUploadComponenet.moveFiles(attachmentFileName1);
			bulkUploadComponenet.moveFiles(attachmentFileName2);
			bulkUploadComponenet.moveFiles(attachmentFileName3);
		}
		return statusData;
		
	}
	
    private synchronized void addExcelAttachment(Multipart multiPart, Workbook workBook, String filename, String attachmentName)
            throws ParseException, IOException, MessagingException {

        // ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        DataSource fileDataSource = null;
        try {
            // workBook.write(byteStream);

            // InputStream excelByteArray = new ByteArrayInputStream(byteStream.toByteArray());
            // fileDataSource = new ByteArrayDataSource(excelByteArray, EXCEL_TYPE);
            fileDataSource = new FileDataSource(folderPath + filename);

        } catch (Exception e) {
            LOGGER.error("Error attaching Excel" + e);
        }
        // finally {
        // byteStream.flush();
        // byteStream.close();
        // }
        MimeBodyPart attachmentPart = new MimeBodyPart();
        attachmentPart.setDataHandler(new DataHandler(fileDataSource));
        attachmentPart.setFileName(attachmentName);
        multiPart.addBodyPart(attachmentPart);
    }

	/**
	 * triggerBulkUploadEmail
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
	public String triggerBulkUploadEmail(String agentId,String emailId,String type,Map<String, String> excelDataList) throws ParseException, BusinessException{
			String statusFlag = null;
			StatusData statusData = sendBulkUploadEmail(agentId,emailId,type,excelDataList);
			statusFlag = statusData.getStatus();
			String statusMessage=statusData.getStatusMessage();
			String status=statusFlag;
			if(statusMessage!=null){
				status=statusFlag.concat(" : ").concat(statusMessage);
			}
		return status;
	}
	 public void sendFailedSPAJMail(List<String> SPAJs) {
	        Properties mailProperties = null;
	        String subject = "";
	        String emailMessage = "";
	        StringBuilder message = new StringBuilder();
	        int i = 0;
	        try {
	            mailProperties = new Properties();
	            mailProperties.put(mailAuth, mailAuthValue);
	            mailProperties.put(mailLTS, mailLTSEnable);
	            mailProperties.put(mailHostKey, mailHost);
	            mailProperties.put(mailPortName, mailPort);

	            subject = "App submitted and failed to create proposal in Life Asia";
	            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
	            Date date = new Date();
	            String currentDate = dateFormat.format(date);	         
	            final Session session = Session.getDefaultInstance(mailProperties, null); 
	          
	            final MimeMessage mimeMessage = new MimeMessage(session);
	            final MimeBodyPart messagePart = new MimeBodyPart();
	            final Multipart multiPart = new MimeMultipart();
	            final InternetAddress fromInternetAddress = new InternetAddress(fromMailId);
	            mimeMessage.setFrom(fromInternetAddress);
	            String emailId = failedSPAJToMailId.replaceAll(";", ",");
	            mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailId));	        
	            mimeMessage.setSubject(subject);
	            mimeMessage.setSentDate(new Date());
	            message.append("Hi Team,<BR><BR>");
	            message.append("Please find the failed submission on LAS.<BR><BR>");
	            message.append("<table border=1>");
	            message.append("<th>Sl.No</th><th>SPAJ</th><th>Agent Code</th><th>Submitted Time</th><th>Failed Channel</th>");
	            for (String spaj : SPAJs) {
	                StringTokenizer spajToken = new StringTokenizer(spaj, "##");
	                message.append("<tr>");
	                message.append("<td>");
	                message.append(++i);
	                message.append("</td>");
	                while (spajToken.hasMoreTokens()) {
	                    message.append("<td>");
	                    message.append( spajToken.nextToken());
	                    message.append("</td>");
	                  
	                }
	                message.append("</tr>");
	            }
	            message.append("</table>");
	            message.append("<BR><BR>Sincerely,<BR>Generali <BR>");
	            emailMessage = message.toString();
	            messagePart.setContent(emailMessage, EMAIL_MSG_CONTENT);
	            multiPart.addBodyPart(messagePart);
	            mimeMessage.setContent(multiPart);
	            Transport.send(mimeMessage);
	        } catch (MessagingException e) {
	            e.printStackTrace();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }

	    }
	/**
	 * Method to update email details in LifeEngageEmail table 
	 * Modules -  illustration ,eApp and memo
	 */
    public String updateEmailDetails(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException {
        Transactions transactions = new Transactions();
        String response = null;
        String agentId = null;
        String successMessage;
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
        try {
            if (jsonRqArray != null) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);

                    String emailFlag = (String) jsonTransactionsObj.getString(KEY_18);
                    agentId = jsonTransactionsObj.getString(Constants.KEY11);
                    try {
                        if (E_APP.equals(jsonTransactionsObj.getString(TYPE))) {
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                lifeEngageEmail = saveLifeEngageEmail(jsonTransactionsObj);
                            }
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.updateEmailDetails EApp Transaction Object "
                                    + jsonTransactionsObj.toString());
                        } else if (ILLUSTRATION.equals(jsonTransactionsObj.getString(TYPE))) {
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                lifeEngageEmail = saveLifeEngageEmail(jsonTransactionsObj);
                            }
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.updateEmailDetails illustration Transaction Object "
                                    + jsonTransactionsObj.toString());
                        } else if (GeneraliConstants.MEMO.equals(jsonTransactionsObj.getString(TYPE))){
                            
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                lifeEngageEmail = saveLifeEngageEmail(jsonTransactionsObj);
                            }
                            
                            // Need to Add memo Logic 
                        }

                        if (lifeEngageEmail != null) {
                            transactions.setLifeEngageEmail(lifeEngageEmail);
                            transactions.setAgentId(agentId);
                            transactions.setStatus(INITIALISED);
                            transactions.setType(lifeEngageEmail.getType());
                            transactions.setKey3(jsonTransactionsObj.getString(KEY_3));
                            transactions.setKey5(jsonTransactionsObj.getString(KEY_5));
                            transactions.setKey5(jsonTransactionsObj.getString(KEY_6));
                            successMessage = Constants.EMAIL_DETAILS_SAVED;
                            final StatusData statusData = new StatusData();
                            statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
                            statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE);
                            statusData.setStatusMessage(successMessage);
                            transactions.setStatusData(statusData);
                            transactions.setStatus(LifeEngageComponentHelper.SUCCESS);
                            transactionsList.add(transactions);
                        } else {
                            throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, Constants.EMAIL_DETAILS_MISSING);
                        }

                    } catch (SystemException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (Exception e) {
            LOGGER.error("Unable to get email details :" + e.getMessage());
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

        } finally {
            response = eMailUpdateHolder.parseBO(transactionsList);

        }

        return response;

    }

    @Override
    public void sendAgentReport(String fileName, String mode) {

        Properties mailProperties = null;

        String subject = "";
        String templateName = "";
        try {
            Calendar c = Calendar.getInstance();
            Calendar weekInstance = Calendar.getInstance();
            c.add(Calendar.MONTH, -1);
            int month = c.get(Calendar.MONTH) + 1;            
            String mon = "";
            if (month < 10) {

                mon = "0" + month;
            }else{
                mon = month+"";
            }

            DateFormat df = new SimpleDateFormat("yy"); 
            String sYear = df.format(c.getTime());
            DateFormat yrAttach = new SimpleDateFormat("yyyy"); 
            String syrAttach = yrAttach.format(c.getTime());
            
            
            DateFormat wf = new SimpleDateFormat("dd-MM-YYYY"); 
            String wDate = wf.format(weekInstance.getTime());
            
            DateFormat wfA = new SimpleDateFormat("YYYYMMdd"); 
            String wADate = wfA.format(weekInstance.getTime());
            
            if (mode.equals("Monthly")) {
                subject = "GenTouch Sale Activity End of " + mon + "-" + sYear;
            } else {
                subject = "GenTouch Sale Activity as of "+wDate;
            }

            mailProperties = new Properties();
            mailProperties.put(mailAuth, mailAuthValue);
            mailProperties.put(mailLTS, mailLTSEnable);
            mailProperties.put(mailHostKey, mailHost);
            mailProperties.put(mailPortName, mailPort);
            
            if (mode.equals("Monthly")) {
                templateName = VELOCITY_AGENT_MONTHLY;
            } else {
                templateName = VELOCITY_AGENT_WEEKLY;
            }
            Properties templateProperties = new Properties();
            templateProperties.setProperty(TEMPLATE_RESOURCE_LOADER, TEMPLATE_RESOURCE_LOADER_VALUE);
            templateProperties.setProperty(TEMPLATE_RESOURCE_LOADER_CLASS, TEMPLATE_RESOURCE_LOADER_CLASS_VALUE);
            templateProperties.setProperty(TEMPLATE_RESOURCE_PATH, templatePath);
            templateProperties.setProperty(TEMPLATE_LOG_CLASS, TEMPLATE_LOG_CLASS_VALUE);
            VelocityEngine ve = new VelocityEngine();
            ve.init(templateProperties);
            VelocityContext context = new VelocityContext();
            
            Template template = ve.getTemplate(templateName, "UTF-8");
            if (mode.equals("Monthly")) {
                context.put("startDate", mon + "-" + sYear);
            } else {
                DateFormat dtf = new SimpleDateFormat("dd-MM-YYYY");
                Date end = new Date();
                Date start = new Date();
                Calendar cal = Calendar.getInstance();

                cal.add(Calendar.DATE, -1);
                end = cal.getTime();
                cal.add(Calendar.DATE, -6);
                start = cal.getTime();
                String sDate = dtf.format(start);
                String eDate = dtf.format(end);
                context.put("startDate", sDate);
                context.put("endDate", eDate);
            }   
            
            StringWriter writer = new StringWriter();
            template.merge(context, writer);
            final String emailMessage = writer.toString();

            final Session session = Session.getDefaultInstance(mailProperties, null);
            final MimeMessage mimeMessage = new MimeMessage(session);
            final MimeBodyPart messagePart = new MimeBodyPart();
            final Multipart multiPart = new MimeMultipart(RELATED);
           
            final InternetAddress fromInternetAddress = new InternetAddress(fromMailId);

            String toMailIds = agentReportToEmail.replaceAll(";", ",");
            String ccMailIds = agentReportCCEmail.replaceAll(";", ",");

            mimeMessage.setFrom(fromInternetAddress);

            mimeMessage.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccMailIds));

            mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMailIds));

            mimeMessage.setSubject(subject, "UTF-8");
            mimeMessage.setSentDate(new Date());
            messagePart.setContent(emailMessage, EMAIL_MSG_CONTENT);
            
            MimeBodyPart imagePart = new MimeBodyPart();
            imagePart.attachFile(templatePath+"/"+SIGNATURE);
            imagePart.setContentID("<" + CID + ">");
            imagePart.setDisposition(MimeBodyPart.INLINE);
            multiPart.addBodyPart(messagePart);
            multiPart.addBodyPart(imagePart);
            
            
            String attachmentName="";
            if (mode.equals("Monthly")) {
                attachmentName = "AgentActivityinGenTounch_" + syrAttach + mon;// YYYYMM;
            } else {
                attachmentName = "AgentActivityinGenTounch_" + wADate;// YYYYMMDD
            }
            addExcelAttachment(multiPart, null,fileName,attachmentName+".xlsx" );
            mimeMessage.setContent(multiPart);
            Transport.send(mimeMessage);
            LOGGER.info("***Agents Report send successfully ***");

        } catch (MessagingException e) {
            LOGGER.error("Error while sending Agents Report - > MessagingException" + e);
        } catch (Exception e) {
            LOGGER.error("Error while sending Agents Report" + e);
        }

    }
  /*  public static void main(String[] arg){
        Calendar c = Calendar.getInstance();
       // c.add(Calendar.MONTH, -1);
       // int month = c.get(Calendar.MONTH) + 1; // beware of month indexing from zero
       // int year = c.get(Calendar.YEAR);
      //  if(month<10){
       //     mon="0"+month;
       // }
       // System.out.println(mon);
       // System.out.println(year);
        DateFormat df = new SimpleDateFormat("YYYYMMdd"); // Just the year, with 2 digits
        String formattedDate = df.format(c.getTime());
        System.out.println("");
    }*/
}
