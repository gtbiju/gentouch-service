package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;

/**
 * The Class class ProductivityData.
 */
public class ProductivityData implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -127477911741190446L;

    /** The ASMId. */
    private String ASMId;

    /** The RMId. */
    private String RMId;

    /** The agentId. */
    private String agentId;

    /** The status New. */
    private int new_status;

    /** The status Closed. */
    private int closed_status;

    /** The status FollowUp. */
    private int followUp_status;

    /** The ConversionRate. */
    private long conversionRate;

    /** The APE */
    private long APE;

    /** The Productivity */
    private long productivity;

    /** The CaseSize */
    private long caseSize;

    /** The active count */
    private int active;

    /** The total IOIS */
    private int totalIOIS;

    /**
     * Gets the ASM Id.
     * 
     * @return the ASM Id
     */
    public String getASMId() {
        return ASMId;
    }

    /**
     * Sets the ASM Id.
     * 
     * @param ASMId
     *            the new ASM Id
     */
    public void setASMId(String aSMId) {
        ASMId = aSMId;
    }

    /**
     * Gets the RM Id.
     * 
     * @return the new RM Id
     */
    public String getRMId() {
        return RMId;
    }

    /**
     * Sets the RM Id.
     * 
     * @param RMId
     *            the new RM Id
     */
    public void setRMId(String rMId) {
        RMId = rMId;
    }

    /**
     * Gets the Agent Id.
     * 
     * @return the Agent Id
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * Sets the Agent Id.
     * 
     * @param agentId
     *            the new Agent Id
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * Gets the new status.
     * 
     * @return the new status
     */
    public int getNew_status() {
        return new_status;
    }

    /**
     * Sets the new status.
     * 
     * @param new_status
     *            the new status
     */
    public void setNew_status(int new_status) {
        this.new_status = new_status;
    }

    /**
     * Gets the closed status.
     * 
     * @return the closed status
     */
    public int getClosed_status() {
        return closed_status;
    }

    /**
     * Sets the closed status.
     * 
     * @param closed_status
     *            the new closed status
     */
    public void setClosed_status(int closed_status) {
        this.closed_status = closed_status;
    }

    /**
     * Gets the follow up status.
     * 
     * @return the follow up status
     */
    public int getFollowUp_status() {
        return followUp_status;
    }

    /**
     * Sets the follow up status.
     * 
     * @param followUp_status
     *            the new follow up status
     */
    public void setFollowUp_status(int followUp_status) {
        this.followUp_status = followUp_status;
    }

    /**
     * Gets the conversion rate.
     * 
     * @return the conversion rate
     */
    public long getConversionRate() {
        return conversionRate;
    }

    /**
     * Sets the conversion rate.
     * 
     * @param conversionRate
     *            the new conversion rate
     */
    public void setConversionRate(long conversionRate) {
        this.conversionRate = conversionRate;
    }

    /**
     * Gets the APE.
     * 
     * @return the APE
     */
    public long getAPE() {
        return APE;
    }

    /**
     * Sets the APE.
     * 
     * @param APE
     *            the new APE
     */
    public void setAPE(long aPE) {
        APE = aPE;
    }

    /**
     * Gets the productivity.
     * 
     * @return the productivity
     */
    public long getProductivity() {
        return productivity;
    }

    /**
     * Sets the productivity.
     * 
     * @param productivity
     *            the new productivity
     */
    public void setProductivity(long productivity) {
        this.productivity = productivity;
    }

    /**
     * Gets the case size.
     * 
     * @return the case size
     */
    public long getCaseSize() {
        return caseSize;
    }

    /**
     * Sets the case size.
     * 
     * @param caseSize
     *            the new case size
     */
    public void setCaseSize(long caseSize) {
        this.caseSize = caseSize;
    }

    /**
     * Gets the active count.
     * 
     * @return the active count
     */
    public int getActive() {
        return active;
    }

    /**
     * Sets the active count.
     * 
     * @param active
     *            the new active
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * Gets the total IOIS.
     * 
     * @return the total IOIS
     */
    public int getTotalIOIS() {
        return totalIOIS;
    }

    /**
     * Sets the total IOIS.
     * 
     * @param totalIOIS
     *            the new total IOIS
     */
    public void setTotalIOIS(int totalIOIS) {
        this.totalIOIS = totalIOIS;
    }

}
