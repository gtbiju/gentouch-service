/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * It indicates the initial type of treatment provided to the injured or ill
 * party.
 * 
 * @author 301350
 * 
 * 
 */
public enum InitialTreatmentCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    EmergencyRoom,
    /**
     * 
     * 
     * 
     * 
     */
    Hospitalized24hrs,
    /**
     * 
     * 
     * 
     * 
     */
    InHouseTreatment,
    /**
     * 
     * 
     * 
     * 
     */
    MinorbyEmployer,
    /**
     * 
     * 
     * 
     * 
     */
    MinorbyHospitalClinic,
    /**
     * 
     * 
     * 
     * 
     */
    NoTreatmentProvided
}
