package com.cognizant.insurance.agent.response;

import com.cognizant.insurance.agent.domain.agent.Agent;
import com.cognizant.insurance.agent.response.vo.ResponseInfo;

/**
 * The Class AgentResponse.
 */
public class AgentProfileResponse {
	
	/** The response info. */
	private ResponseInfo responseInfo;
	
	/** The agent. */
	private Agent agent;

	/**
	 * Gets the response info.
	 *
	 * @return the response info
	 */
	public ResponseInfo getResponseInfo() {
		return responseInfo;
	}

	/**
	 * Sets the response info.
	 *
	 * @param responseInfo the new response info
	 */
	public void setResponseInfo(ResponseInfo responseInfo) {
		this.responseInfo = responseInfo;
	}

	/**
	 * Gets the agent.
	 *
	 * @return the agent
	 */
	public Agent getAgent() {
		return agent;
	}

	/**
	 * Sets the agent.
	 *
	 * @param agent the new agent
	 */
	public void setAgent(Agent agent) {
		this.agent = agent;
	}

}
