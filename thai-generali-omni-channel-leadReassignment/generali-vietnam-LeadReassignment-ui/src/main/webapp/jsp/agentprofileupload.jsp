<%@page import="com.cognizant.le.model.dto.ExcelStatus"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@ page import="com.cognizant.le.model.dto.AgentDTO" %>
<%@ page import="com.cognizant.le.model.dto.ExcelStatus" %>
<%@ page import = "java.util.ResourceBundle" %>
<% ResourceBundle resource = ResourceBundle.getBundle("resource");%>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<link rel="stylesheet" href="css/css_reset.css" />
<link rel="stylesheet" href="css/styles_workbench.css" />
<title>DAP</title>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<style>

</style>
<script>

var SITE = SITE || {};

SITE.fileInputs = function() {
  var $this = $(this),
      $val = $this.val(),
      valArray = $val.split('\\'),
      newVal = valArray[valArray.length-1],
      $button = $this.siblings('.button'),
      $fakeFile = $this.siblings('.file-holder');
  if(newVal !== '') {
    $button.text('Browse');
    if($fakeFile.length === 0) {
      $button.after('<span class="file-holder">' + newVal + '</span>');
    } else {
      $fakeFile.text(newVal);
    }
  }
};
$(document).ready(function() {
function progressbar() {

	
    var progression = 0,
	
    progress = setInterval(function() 
    {
        $('.ratio').text(progression);
        $('#progress .progress-bar').css({'width':progression+'%'});
        if(progression == 100) {
            clearInterval(progress);
			$('#page_loader').css('height',dynamic_height).fadeIn(300);
            agntUploadResults();
       } else
            progression += 10;

    }, 500);


}

	document.getElementById("display_message").innerHtml="";
	$('.custom-upload input[type=file]').change(function(){
		  $(this).next().find('input').val($(this).val().replace(/^.*\\/, ""));
	});
	
	<%
	if(null== request.getSession(false)){%>
		logoutClick();
	<%	
	}
	%> 		

  $('.custom-upload input[type=file]').bind('change focus click', SITE.fileInputs);
  var currentDate = new Date();
  $('#spanId').append(moment().format('h:mm A Do MMMM YYYY'));
  
  var profile= "<%=request.getAttribute("profile")%>"

  if(profile.length > 4){
	$('#page_loader').empty();
	var dynamic_height = $('body').height();
	$('#uploadFile').show();
	progressbar();
  }
  
	$('.close_pop_up,.close_btn').click(function(){	 
	   $('.modal_pop_up.agtupd_content_pop_up').css("display","none");
	  $("body").removeClass("hide_scroll"); 
	  $('#page_loader').css("display","none");
	  $('#uploadFile').hide();
	  $("#progressbox").hide();
	});
	//disabling upload button
	$('input:file').change(
			
            function(){
				document.getElementById("display_message").innerHTML="";
                if ($(this).val()) {
                    $('#agent_upload_pop_up_btn').attr('disabled',false);
                    // or, as has been pointed out elsewhere:
                    // $('input:submit').removeAttr('disabled'); 
                } 
            });
			
	$('#agent_upload_pop_up_btn').click(function(e){
	
		var fileName = $("#fileName").val();
		var trimmmedFileName = $("#fileName").val().replace(/^.*\\/, "")
		var extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
		var isValidFileType,isValidSheet,emptySheet;
		  var options = {
			        beforeSend : function() {
							$('#uploadFile').show();
							$('.filename_format').html(trimmmedFileName);
							
							$("#progressbox").show();
							$("#progressbar").width('0%');
			                //$("#message").empty();
			                $("#percent").html("0%");
							
			        },
			        uploadProgress : function(event, position, total, percentComplete) {
			                $("#progressbar").width(percentComplete + '%');
			                $("#percent").html(percentComplete + '%');
							if (percentComplete > 50) {
			                //$("#message").html("<font color='red'>File Upload is in progress</font>");
			                }
			        },
			       success : function(response) {
			        	 	var status = JSON.parse(response);
							isValidFileType = status.isValidTemplate;
							isValidSheet = status.fileType;
							emptySheet = status.emptyExcel;
							validUserType = status.validUserType;		          },
				 complete : function(response) {
						$("#progressbar").width('100%');
						$("#percent").html('100%');
						var dynamic_height = $('body').height();
						$('#page_loader').css('height',dynamic_height).fadeIn(300);
						$("#popUp").show();
						if ((isValidFileType) && (isValidSheet) &&(!emptySheet) &&(validUserType)) {
							  $("#validMessage").show();
							  $(".agtupd_content_pop_up .modal_pop_up_header").text('<%=new String(resource.getString("success").getBytes( "ISO-8859-1" ),"UTF-8") %>');
							  $("#errorMessage").hide();
							  $('#validMessage1').html("<%=new String(resource.getString("validFileMessage1").getBytes( "ISO-8859-1" ),"UTF-8")%>");
							  $('#validMessage2').html("<%=new String(resource.getString("validFileMessage2").getBytes( "ISO-8859-1" ),"UTF-8")%>");
				                 } 
			                 else 
				                 {	
			                	 $("#validMessage").hide();	
			                	 $("#errorMessage").show();	
								 $(".agtupd_content_pop_up .modal_pop_up_header").text('Failure');
								 if (!isValidSheet)
					              { $('#errorMessage').html("<%=new String(resource.getString("fileTypeValidationMessage").getBytes( "ISO-8859-1" ),"UTF-8") %>");
			                      }
								 else{									 
									  if (!isValidFileType)
						                  {  $('#errorMessage').html("<%=new String(resource.getString("fileValidationMessage").getBytes( "ISO-8859-1" ),"UTF-8") %>");
				                 
				                          } else  
					                  { 
					                  if(!validUserType){
										 $('#errorMessage').html("<%=new String(resource.getString("fileValidationMessage").getBytes( "ISO-8859-1" ),"UTF-8") %>");
										 }
					                  else 
						                  { 
						                  if (emptySheet)
							                   { $('#errorMessage').html("<%=new String(resource.getString("emptyFileValidationMessage").getBytes( "ISO-8859-1" ),"UTF-8") %>");
							                   }
						                  }
					                  }
				                 }
				                 }
			        },
			        error : function() {
			       // $("#message").html("<font color='red'> ERROR: unable to upload files</font>");
			        }
			};
			$("#uploadForm").ajaxForm(options);
	  if(extension == "") {
		document.getElementById("display_message").innerHTML="<%=resource.getString("selectFile")%>";
		e.preventDefault(); 
	  }
	
	});		
	
});
</script>
    
  <script type="text/javascript">
  function logoutClick(){
		document.location.href="<%=request.getContextPath()%>/Logout";
	}
  function showPopUp(){
	  $(".pageContainer").hide();
	  $("#popupDiv").show();
  }


  function closePopup(){
	  $("#popupDiv").hide();
	  $(".pageContainer").show();	
	  $("#fileName").val('');
	}
  
  $('#closeDiv').click( function() {            
	  $("#popupDiv").hide();
	  $(".pageContainer").show();
	  $("#fileName").val('');
	  $("#progressbox").show();
  });
  
	function agentPage(){
		document.location.href="<%=request.getContextPath()%>/AgentUpload";
	}
	
	function contentPage(){
		document.location.href="<%=request.getContextPath()%>/loadFile";
	}
	function mdbLoader(){
		document.location.href="<%=request.getContextPath()%>/jsp/masterDataTemplate.jsp";
	}
	
  </script>
</head>
<body>
<!-- <div class="blue_bg_header">
		<div class="top_header_components clearfix">
			<span class="environment_txt">Environment</span> <select
				id="filter_environment">
				<option value="Model Office">Model Office</option>
				<option value="Production">Production</option>
				<option value="UAT">UAT</option>
				<option value="Dev">Dev</option>
			</select> 
			<button class="logout_action_btn" id="logout_btn" onClick="logoutClick()">Logout</button>
		</div>
	</div>-->
		<div class="logo_header">
	<img src="img/logo-main.png" alt="logo-main" class="mainLogo" />
	<span class="bulk_upload_heading"><%=new String(resource.getString("bulkUpload").getBytes( "ISO-8859-1" ),"UTF-8") %></span>
	<img src="img/logout.png" onClick="logoutClick()" alt="logo-main" class="logoutLogo" />
		

	<!-- <span
			class="header_title">LifeEngage Configurator</span>-->
	</div>
	<!--	<div class="containerMain">
		<div class="containerMenu ">
			<div class="TopSlideMenu main_nav_bar nav_bg_green">
			<ul class="main_nav_bar_list clearfix">
					<!-- <li class="activeList"><a href="javascript:void(0)" rel="#showTop">About us</a></li>
					<li class="activeList"><a href="javascript:void(0)" rel="#showTop1">Services</a></li>
					<li class="activeList"><a href="javascript:void(0)" rel="#showTop2">Products</a></li> -->
<!--	
					<li class="active activeList last_li"><a class="clearfix"
						 rel="#showUsers" onclick=""><span
							class="menu_link_text">Users</span></a></li>
					<li class="activeList"><a class="clearfix"
						onclick="contentPage()" rel="#showContent"><span
							class="menu_link_text">Content</span></a></li>
					<li class="activeList last_li"><a class="clearfix"
						onClick="mdbLoader();" rel="#showContent"><span
							class="menu_link_text">Master Data</span></a></li>		
				</ul>
			</div>-->
			<!--  <div class="submenu_container slide-menu-top" id="showProducts">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li class="activated"><a href="#" title="menu">Application
									innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>

			<div class="submenu_container slide-menu-top" id="showUsers">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>
				<div class="submenu_column2">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="submenu_container slide-menu-top" id="showContent">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li class="activated"><a href="#" title="menu">
									Add/Update Content </a></li>
						</ul>
					</div>
				</div>

				<div class="clear"></div>
			</div>
			<div class="submenu_container slide-menu-top" id="showApp">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
			<div class="submenu_container slide-menu-top" id="showRules">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
			<div class="submenu_container slide-menu-top" id="showCalcs">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
		</div>
		<div class="clear"></div>
		</div>
		
			<div class="bg_white">
		<ul class="breadcrumb breadcrumb_gradientbg">
			<li><a href="#" class="brd_link_sel">Upload</a></li>
		</ul>
	</div> -->
		<ul class="menu-bar">
			<li class="active"><a href="#" class=""><%=resource.getString("upload") %></a></li>
			<li><a href="LeadAssignment.jsp" class="">Lead Assignment</a></li>
		</ul>
		
		<!-- CONTENT CONTAINER -->
		<div class="bg_white page-title">
			<span><%=new String(resource.getString("upload").getBytes( "ISO-8859-1" ),"UTF-8") %><span>	
		</div>
	<div class="content_container">

		<div class="container_tlr_fluid">
			<div class="blankContainer">
				<div class="blankContainerInner">
					<div class="table_container">
							    <form action="upload" name="uploadForm" id="uploadForm" method="post" enctype="multipart/form-data">
								
									<div class="pageContainer">
										<div class="contentPosition">
										<div class="page foldtr">
													<p class="modal_pop_up_header ">
													<%=new String(resource.getString("browseFile").getBytes( "ISO-8859-1" ),"UTF-8") %>
													</p></div>
													<hr>
													<div class="agent_custom_upload">
														<div class="custom-upload">
															
															
															<input type="file" id="fileName" name="fileName">
															<div class="upload_cont">
																<button class="browse_button"><%=new String(resource.getString("browse").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
																<input type="text" disabled class="browse_pop_up_btn" id="browse_btn"></input>
																<button class="pop_up_upload_btn mrg_update" id="agent_upload_pop_up_btn"><%=new String(resource.getString("upload").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
													
														    </div>
														
														
														</div>
														
													
													</div>
													 
													<div class="upload_part" id="uploadFile" style="display:none;">
													<p class="filename_format"></p>
													<!--<div id="progressbox">
														<div class="progress-bar" id="progressbar"></div>
													</div>-->
													
													<div id="progressbox" style="display:none;">
														<div id="progressbar"></div>
														<p class="percentRatio"><span id="percent" class="ratio">0</span> done</p>
													</div>
													  
													<p class="uploadingText"><%=new String(resource.getString("uploading").getBytes( "ISO-8859-1" ),"UTF-8") %></p>
													</div>
													<span id="display_message" class="clearfix upload_message"></span>

										</div>
									</div>
								</form>
					</div>

				</div>
			</div>
		</div>		
		
	</div>	
	
		<!-- CONTENT CONTAINER -->



	<div class="footer_container">
		<p class="copyright_label">©2017 Generali Life Assurance (Thailand) Plc</p>
		<span class="version">Version No:1.0</span>
	</div>


	<div id="page_loader"></div><!--
	
    
   <div id="popupDiv" class="popup popup2"  style="display:none;">
        <div class="popupHeading">
            <a onclick="closePopup()">close</a>
        </div>
        <div class="popupContent">
        	Invalid file name
        </div>
        <div class="shadow"></div>
        <div class="btn">
        	 <span>
                <input type="button" id="closeDiv" class="orangeButton" value="OK" onclick="closePopup()"/>            
            </span>
		</div>
    </div> 
   
    	--><!-- THE MODAL AGENTUPLOADRESULT CONTENT POPUP -->
	<div class="modal_pop_up agtupd_content_pop_up" id="popUp" style=" display:none; height:75%; overflow: auto; width:50%;top:50%">
		<p class="modal_pop_up_header">
			Success
		</p>
	    <div class="blankContainerInner">
			<div class="innerContainer">
			 <span class='green' id="validMessage" style="display:none;"> 
				<p id="validMessage1"></p><br>
				<p id="validMessage2"></p>
			</span>
			  <span class='red' id="errorMessage" style="display:none;"> </span>
            <%
            if(null!=request.getAttribute("profile")){
            ExcelStatus excelStatus =(ExcelStatus)request.getAttribute("profile");
            %>
            
           <%  boolean isValidFileType = excelStatus.isValidFileType();
            boolean isValidSheet = excelStatus.isValidTemplate();
            boolean emptySheet = excelStatus.isEmptyExcel();
            if ((isValidFileType) && (isValidSheet) &&(!emptySheet)){
                
                %>
                <span class='green'> Validation and upload process started.<br>  The details will be mailed to the user.</span><br> 
                <% } else { %>
                <span class='red'> <% if (!isValidFileType) {%> Invalid file type. Please select an XLS / XLSX file </span><br> 
                 <span class='red'> <% }else  { if (!isValidSheet){ %> Invalid file. Please select the correct file </span><br>
                 <span class='red'> <% } else { if (emptySheet) {%> No records found </span><br> 

       <% }}}}} %> 

		</div>
	<div class="btn-wrapper">
		<button class="pop_up_upload_btn ok-btn close_btn">OK</button>
	</div>
   </div>	

	</div>
	<!-- THE MODAL AGENTUPLOADRESULT CONTENT POPUP -->
    
    
    
</body>
</html>
