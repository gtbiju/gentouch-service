/**
 * 
 */
package com.cognizant.insurance.domain.agreement.agreementcodelists;

/**
 * Loan Interest Method.
 * 
 * @author 301350
 * 
 * 
 */
public enum LoanInterestMethodCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    PaidInCash,
    /**
     * 
     * 
     * 
     * 
     */
    Capitalized,
    /**
     * 
     * 
     * 
     * 
     */
    Other
}
