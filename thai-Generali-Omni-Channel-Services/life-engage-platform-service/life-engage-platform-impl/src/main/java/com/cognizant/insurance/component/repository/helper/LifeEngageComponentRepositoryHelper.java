/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 25, 2013
 */
package com.cognizant.insurance.component.repository.helper;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.EntityManager;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.agreement.component.AgreementComponent;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.party.component.PartyComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;

/**
 * The Class class LifeEngageComponentRepositoryHelper.
 * 
 * @author 300797
 */
public final class LifeEngageComponentRepositoryHelper {

    /**
     * Instantiates a new life engage component repository helper.
     */
    private LifeEngageComponentRepositoryHelper() {
        // not called
    }

    /** The Constant DATEFORMAT_YYYY_MM_DD_HH_MM_SS. */
    public static final String DATEFORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    
    private static final String DELETEDSTATUS = "Deleted";

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageComponentRepositoryHelper.class);


    /**
     * Creates the replacing Document.
     * 
     * @param replacingAgeements
     *            the replacingAgeements
     * 
     * @return the AgreementDocument
     */

    public static Set<AgreementDocument> createReplaceDocument(Set<Agreement> replacingAgeements) {

        Set<AgreementDocument> agreementRelatedDocumentSet = new HashSet<AgreementDocument>();
        Set<AgreementDocument> agreementDocumentSet = new HashSet<AgreementDocument>();
        AgreementDocument agreementDocument;
        Document documentPages;
        Set<Document> document;

        for (Agreement agreement : replacingAgeements) {
            agreementDocumentSet = agreement.getRelatedDocument();

            for (AgreementDocument agrDocument : agreementDocumentSet) {
                agreementDocument = new AgreementDocument();
                agreementDocument.setName(agrDocument.getName());
                agreementDocument.setDescription(agrDocument.getDescription());
                agreementDocument.setTypeCode(agrDocument.getTypeCode());
                agreementDocument.setDocumentProofSubmitted(agrDocument.getDocumentProofSubmitted());
                agreementDocument.setDocumentStatus(agrDocument.getDocumentStatus());
                agreementDocument.setStorageLocation(agrDocument.getStorageLocation());
                agreementDocument.setFileContent(agrDocument.getFileContent());
                agreementDocument.setSignedIndicator(agrDocument.getSignedIndicator());
                agreementDocument.setLanguageCode(agrDocument.getLanguageCode());

                document = new HashSet<Document>();
                for (Document doc : agrDocument.getPages()) {
                    documentPages = new Document();
                    documentPages.setFileNames(doc.getFileNames());
                    documentPages.setBase64string(doc.getBase64string());
                    document.add(documentPages);
                }

                agreementDocument.setPages(document);
                agreementRelatedDocumentSet.add(agreementDocument);
            }
        }

        return agreementRelatedDocumentSet;

    }

    /**
     * Creates the replacing agreement.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param entityManager
     *            the entity manager
     * @param agreementComponent
     *            the agreement component
     * @param contextType
     *            the context type
     * @param statusList
     *            the status list
     * @return the sets the
     */
    public static InsuranceAgreement createReplacingAgreement(final Transactions transactions,
            final RequestInfo requestInfo, final EntityManager entityManager,
            final AgreementComponent agreementComponent, final ContextTypeCodeList contextType,
            final List<AgreementStatusCodeList> statusList) {

        final Request<Agreement> request =
                new JPARequestImpl<Agreement>(entityManager, contextType, requestInfo.getTransactionId());
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        String identifier;
        if (contextType.equals(ContextTypeCodeList.ILLUSTRATION)) {
            identifier = transactions.getIllustrationId();
        } else {
            identifier = transactions.getProposalNumber();
        }
        insuranceAgreement.setIdentifier(identifier);
        request.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, contextType,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);
        final Response<Agreement> response = agreementComponent.retrieveAgreementForIdentifier(request, statusRequest);
        if (response == null || response.getType() == null) {
            throwSystemException(true, "Failed to get proposal with no : " + identifier);
        }
        return (InsuranceAgreement) response.getType();

    }

    /**
     * Updates the status of replacing agreement.
     * 
     * @param insuranceAgreement
     *            the insuranceAgreement
     * @param agreementComponent
     *            the agreement component
     * @param requestInfo
     *            the request info
     * @param entityManager
     *            the entity manager
     * @param contextType
     *            the context type
     * @return the Agreement set
     */

	public static Set<Agreement> updateAgreementStatus(
			InsuranceAgreement insuranceAgreement,
			final AgreementComponent agreementComponent,
			final RequestInfo requestInfo, final EntityManager entityManager,
			final ContextTypeCodeList contextType) {
		final Request<Agreement> request = new JPARequestImpl<Agreement>(
				entityManager, contextType, requestInfo.getTransactionId());
		Set<Agreement> replacingAgeements;
		insuranceAgreement.setStatusCode(AgreementStatusCodeList.Suspended);
		// Change added by Generali implementation, as part of database optimization.
		//By this change additional questionnaire section will be set to blank, before suspending the EAPP.
		//This will reduce the memory usage.
		if (ContextTypeCodeList.EAPP.equals(contextType)) {
			insuranceAgreement.getAgreementExtension().setSelectedOptions(
					new HashSet<UserSelection>());
		}
		request.setType(insuranceAgreement);
		agreementComponent.updateAgreement(request);

		replacingAgeements = new HashSet<Agreement>();
		replacingAgeements.add(insuranceAgreement);
		return replacingAgeements;
	}

    /**
     * Save insured.
     * 
     * @param insured
     *            the insured
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     * @param entityManager
     *            the entity manager
     * @param contextType
     *            the context type
     * @param partyComponent
     *            the party component
     */
    public static void saveInsured(final Insured insured, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement, final EntityManager entityManager,
            final ContextTypeCodeList contextType, final PartyComponent partyComponent) {
        if (insured != null) {
            insured.setIsPartyRoleIn(insuranceAgreement);
            insured.setTransactionId(requestInfo.getTransactionId());
            final Request<PartyRoleInAgreement> insuredRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, contextType, requestInfo.getTransactionId());
            insuredRequest.setType(insured);
            partyComponent.savePartyRoleInAgreement(insuredRequest);
        }
    }

    /**
     * Save payer.
     * 
     * @param payer
     *            the payer
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     * @param entityManager
     *            the entity manager
     * @param contextType
     *            the context type
     * @param partyComponent
     *            the party component
     */
    public static void savePayer(final PremiumPayer payer, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement, final EntityManager entityManager,
            final ContextTypeCodeList contextType, final PartyComponent partyComponent) {

        if (payer != null) {
            payer.setIsPartyRoleIn(insuranceAgreement);
            payer.setTransactionId(requestInfo.getTransactionId());
            final Request<PartyRoleInAgreement> payerRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, contextType, requestInfo.getTransactionId());
            payerRequest.setType(payer);
            partyComponent.savePartyRoleInAgreement(payerRequest);
        }
    }

    /**
     * Gets the currentdate.
     * 
     * @return the currentdate
     * @throws ParseException
     *             the parse exception
     */
    public static Date getCurrentdate() throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        final Calendar calendar = new GregorianCalendar();
        final String currentDate = dateFormat.format(calendar.getTime());
        return dateFormat.parse(currentDate);
    }

    /**
     * Format date.
     * 
     * @param date
     *            the date
     * @return the string
     * @throws ParseException
     *             the parse exception
     */
    public static String formatDate(Date date) throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        return dateFormat.format(date);
    }
    
    /**
     * @param transactions
     * @param replacingAgeements
     * @param fileUploadPath
     * @return
     */
    public static void createReplacingRequirementSet(Transactions transactions, Set<Agreement> replacingAgeements,
            String fileUploadPath) {
        Set<Requirement> oldAgreementRelatedRequirementSet = new HashSet<Requirement>();
        for (Agreement oldAgreement : replacingAgeements) {
            if (CollectionUtils.isNotEmpty(oldAgreement.getRequirements())
                    && CollectionUtils.isNotEmpty(transactions.getRequirements())) {
                oldAgreementRelatedRequirementSet = oldAgreement.getRequirements();
                for (Requirement oldrequirement : oldAgreementRelatedRequirementSet) {
                    for (Requirement newrequirement : transactions.getRequirements()) {
                        if (newrequirement.getRequirementName().equals(oldrequirement.getRequirementName())) {
                            // && !oldrequirement.getRequirementType()
                            // .toString()
                            // .equals(DocumentTypeCodeList.Signature.toString())) {
                            for (Document oldDocument : oldrequirement.getRequirementDocuments()) {
                                for (Document newDocument : newrequirement.getRequirementDocuments()) {
                                    if (oldDocument.getName().equals(newDocument.getName())) {
                                        if (newDocument.getDocumentStatus().equals(DELETEDSTATUS)) {
                                            // Remove from List and delete physical file if any
                                            removeAllPhysicalFileForDocument(oldDocument, fileUploadPath);
                                            // newrequirement.getRequirementDocuments().remove(newDocument);
                                            if (CollectionUtils.isNotEmpty(newDocument.getPages())) {
                                                for (Document docPageOfDeleted : newDocument.getPages()) {
                                                    docPageOfDeleted.setDocumentStatus(DELETEDSTATUS);
                                                }
                                            }
                                        } else {
                                            // newDocument.setPages(oldDocument.getPages());
                                            Set<Document> documentPages = new HashSet<Document>();
                                            Document documentPage;
                                            for (Document doc : oldDocument.getPages()) {
                                                documentPage = new Document();
                                                documentPage.setFileNames(doc.getFileNames());
                                                documentPage.setBase64string(doc.getBase64string());
                                                documentPage.setDocumentStatus(doc.getDocumentStatus());
                                                documentPages.add(documentPage);
                                            }
                                            newDocument.setPages(documentPages);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }

    /**
     * 
     * @param oldDocument
     * @param fileUploadPath
     */
    private static void removeAllPhysicalFileForDocument(Document oldDocument, String fileUploadPath) {
        for (Document currentPage : oldDocument.getPages()) {
            final File file = new File(fileUploadPath + currentPage.getFileNames());
            if (!file.delete()) {
                LOGGER.error("File Deletion Error! : " + file.getPath());
            }
        }
    }
}
