/**
 * 
 */
package com.cognizant.insurance.domain.party;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.commonelements.commoncodelists.GenderCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.Measurement;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.PostalAddressContact;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.Country;
import com.cognizant.insurance.domain.party.partycodelists.BloodTypeCodeList;
import com.cognizant.insurance.domain.party.partycodelists.EthnicityCodeList;
import com.cognizant.insurance.domain.party.partycodelists.MaritalStatusCodeList;
import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;
import com.cognizant.insurance.domain.party.partyname.PersonName;

/**
 * This concept stands for a person.
 * 
 * Note: Various details, such as name, occupation, education, are not included within person because persons may have
 * multiple names, occupations and education details. These concepts are addressed via associations to related classes
 * (PersonName, PersonDetail).
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Person extends Party {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5005275425610578997L;

    /**
     * This relationship links a person with his/her details (if any).
     * 
     * e.g. occupation details, family details, personal activities This represents details (if any) of a person (e.g.
     * occupation details, family details, personal activities, etc.).
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    private Set<PersonDetail> detail;

    /**
     * Date of birth of the person.
     * 
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    /**
     * Age of the person.
     * 
     * 
     * 
     * 
     * 
     */
    private Integer age;

    /**
     * AuthenticationID of the person.
     * 
     * 
     * 
     * 
     * 
     */
    private String authenticationID;

    /**
     * Date of the death of the person.
     * 
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date deathDate;

    /**
     * Indicates a person is dead. This attribute can be used when the death date is unknown.
     * 
     * 
     * 
     * 
     * 
     */
    private Boolean deathIndicator;

    /**
     * The classification of the person's blood with regard to ABO and Rhesus systems in the human blood type profile.
     * 
     * e.g: A
     * 
     * e.g: A+
     * 
     * e.g: AB
     * 
     * e.g: AB+
     * 
     * e.g: B-
     * 
     * e.g: B+
     * 
     * e.g: O
     * 
     * e.g: O+
     * 
     * 
     * 
     */
    @Enumerated
    private BloodTypeCodeList bloodTypeCode;

    /**
     * Person gender.
     * 
     * 
     * 
     */
    @Enumerated
    private GenderCodeList genderCode;

    /**
     * Identifies the ethnic group to which an individual is associated.
     * 
     * e.g: Afro-American.
     * 
     * e.g: Caucasian.
     * 
     * 
     * 
     */
    @Enumerated
    private EthnicityCodeList ethnicityCode;

    /**
     * Indicates whether or not the person's whereabouts are known.
     * 
     * 
     * 
     */
    private Boolean missingIndicator;

    /**
     * The date the person was listed as missing.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date missingDate;

    /**
     * A code representing the primary language of a person (see ISO 639).
     * 
     * REFERENCE: http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail .htm?csnumber=22109
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE }, fetch=FetchType.LAZY)
    private ExternalCode primaryLanguageCode;

    /**
     * This relationship links a person to his or her country of nationality. This represents the country for a person's
     * nationality.
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    @JoinTable(name = "Person_NationalityCountry", joinColumns = @JoinColumn(name = "person_Id", referencedColumnName = "Id"), inverseJoinColumns = @JoinColumn(name = "Country_Id", referencedColumnName = "Id"))
    private Set<Country> nationalityCountry;

    /**
     * This relationship links a person to his/her birth postal address. This represents the postal address of a
     * person's birth.
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE },fetch=FetchType.EAGER)
    private PostalAddressContact birthAddress;

    /**
     * This relationship links a person to his/her name(s). This represents the person's name.
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    private Set<PersonName> name;

    /**
     * This relationship links a person to the country of which she/he is a citizen. This represents the country of
     * which a person is a citizen.
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    @JoinTable(name = "Person_CitizenshipCountry", joinColumns = @JoinColumn(name = "person_Id", referencedColumnName = "Id"), inverseJoinColumns = @JoinColumn(name = "Country_Id", referencedColumnName = "Id"))
    private Set<Country> citizenshipCountry;

    /**
     * A code indicating the person's current marital status.
     * 
     * 
     * 
     */
    @Enumerated(EnumType.STRING)
    private MaritalStatusCodeList maritalStatusCode;

    /** ****Newly added for EAp*******. */

    /**
     * Indicates the country of residence.
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE }, fetch=FetchType.LAZY)
    private Country residenceCountry;

    /**
     * Indicates the national Id Type.
     */
    private String nationalIdType;

    /**
     * Indicates the national Id.
     */
    @Column(columnDefinition="nvarchar(80)")
    private String nationalId;

    /**
     * An indicator that specifies if the current address and the permenent address are the same for a person.
     */
    
    @Column(columnDefinition="nvarchar(200)")
    private String citizenIdentityCard;
    
    private Boolean permanentAddressSameAsCurrentIndicator;

    /**
     * Indicates the name of the spouse/parent.
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE }, fetch=FetchType.LAZY)
    private PersonName spouseOrParentName;

    /**
     * Indicates the document used as proof of age.
     */
    private String ageProof;

    /**
     * Indicates the document used as proof of identity.
     */
    @Column(columnDefinition="nvarchar(80)")
    private String identityProof;
    
    /**
     * ExpDate of the Submitted identity Proof
     */
    @Temporal(TemporalType.DATE)
    private Date identityExpDate;

    /**
     * Indicates the height.
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE }, fetch=FetchType.LAZY)
    private Measurement height;

    /**
     * Indicates the weight.
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE }, fetch=FetchType.LAZY)
    private Measurement weight;

    /** ****Newly added for EAp - End*******. */

    /** ****Newly added for Illustration/BI- Start *******. */

    /** The tobacco premium basis. */
    private String tobaccoPremiumBasis;

    /** The drinking premium basis. */
    private String drinkingPremiumBasis;
    
    /** Added for Generali */
    private String nationalityDesciption;
    
    /** Added for Generali */
        
    private String nationalIDTypeValue;
    
    /** Added for Generali vietnam */
    
    @Temporal(TemporalType.DATE)
    private Date identityDate;
    
    @Column(columnDefinition="nvarchar(50)")
    private String identityPlace;
    
    @Temporal(TemporalType.DATE)
    private Date ageCalculatedDate;
    
    private String nationlityThai;
    
    /**
     * ****Newly added for Illustration/BI- End *******.
     * 
     * @return the detail
     */

    public String getNationalIDTypeValue() {
		return nationalIDTypeValue;
	}

	public void setNationalIDTypeValue(String nationalIDTypeValue) {
		this.nationalIDTypeValue = nationalIDTypeValue;
	}

	/**
     * Gets the detail.
     * 
     * @return Returns the detail.
     */
    public Set<PersonDetail> getDetail() {
        return detail;
    }

    /**
     * Gets the birthDate.
     * 
     * @return Returns the birthDate.
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Gets the age.
     * 
     * @return Returns the age.
     */
    public Integer getAge() {
        return age;
    }

    /**
     * Gets the deathDate.
     * 
     * @return Returns the deathDate.
     */
    public Date getDeathDate() {
        return deathDate;
    }

    /**
     * Gets the deathIndicator.
     * 
     * @return Returns the deathIndicator.
     */
    public Boolean getDeathIndicator() {
        return deathIndicator;
    }

    /**
     * Gets the bloodTypeCode.
     * 
     * @return Returns the bloodTypeCode.
     */
    public BloodTypeCodeList getBloodTypeCode() {
        return bloodTypeCode;
    }

    /**
     * Gets the genderCode.
     * 
     * @return Returns the genderCode.
     */
    public GenderCodeList getGenderCode() {
        return genderCode;
    }

    /**
     * Gets the ethnicityCode.
     * 
     * @return Returns the ethnicityCode.
     */
    public EthnicityCodeList getEthnicityCode() {
        return ethnicityCode;
    }

    /**
     * Gets the missingIndicator.
     * 
     * @return Returns the missingIndicator.
     */
    public Boolean getMissingIndicator() {
        return missingIndicator;
    }

    /**
     * Gets the missingDate.
     * 
     * @return Returns the missingDate.
     */
    public Date getMissingDate() {
        return missingDate;
    }

    /**
     * Gets the primaryLanguageCode.
     * 
     * @return Returns the primaryLanguageCode.
     */
    public ExternalCode getPrimaryLanguageCode() {
        return primaryLanguageCode;
    }

    /**
     * Gets the nationalityCountry.
     * 
     * @return Returns the nationalityCountry.
     */
    public Set<Country> getNationalityCountry() {
        return nationalityCountry;
    }

    /**
     * Gets the birthAddress.
     * 
     * @return Returns the birthAddress.
     */
    public PostalAddressContact getBirthAddress() {
        return birthAddress;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public Set<PersonName> getName() {
        return name;
    }

    /**
     * Gets the citizenshipCountry.
     * 
     * @return Returns the citizenshipCountry.
     */
    public Set<Country> getCitizenshipCountry() {
        return citizenshipCountry;
    }

    /**
     * Gets the maritalStatusCode.
     * 
     * @return Returns the maritalStatusCode.
     */
    public MaritalStatusCodeList getMaritalStatusCode() {
        return maritalStatusCode;
    }

    /**
     * Sets The detail.
     * 
     * @param detail
     *            The detail to set.
     */
    public void setDetail(final Set<PersonDetail> detail) {
        this.detail = detail;
    }

    /**
     * Sets The birthDate.
     * 
     * @param birthDate
     *            The birthDate to set.
     */
    public void setBirthDate(final Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * Sets The age.
     * 
     * @param age
     *            The age to set.
     */
    public void setAge(final Integer age) {
        this.age = age;
    }

    /**
     * Sets The deathDate.
     * 
     * @param deathDate
     *            The deathDate to set.
     */
    public void setDeathDate(final Date deathDate) {
        this.deathDate = deathDate;
    }

    /**
     * Sets The deathIndicator.
     * 
     * @param deathIndicator
     *            The deathIndicator to set.
     */
    public void setDeathIndicator(final Boolean deathIndicator) {
        this.deathIndicator = deathIndicator;
    }

    /**
     * Sets The bloodTypeCode.
     * 
     * @param bloodTypeCode
     *            The bloodTypeCode to set.
     */
    public void setBloodTypeCode(final BloodTypeCodeList bloodTypeCode) {
        this.bloodTypeCode = bloodTypeCode;
    }

    /**
     * Sets The genderCode.
     * 
     * @param genderCode
     *            The genderCode to set.
     */
    public void setGenderCode(final GenderCodeList genderCode) {
        this.genderCode = genderCode;
    }

    /**
     * Sets The ethnicityCode.
     * 
     * @param ethnicityCode
     *            The ethnicityCode to set.
     */
    public void setEthnicityCode(final EthnicityCodeList ethnicityCode) {
        this.ethnicityCode = ethnicityCode;
    }

    /**
     * Sets The missingIndicator.
     * 
     * @param missingIndicator
     *            The missingIndicator to set.
     */
    public void setMissingIndicator(final Boolean missingIndicator) {
        this.missingIndicator = missingIndicator;
    }

    /**
     * Sets The missingDate.
     * 
     * @param missingDate
     *            The missingDate to set.
     */
    public void setMissingDate(final Date missingDate) {
        this.missingDate = missingDate;
    }

    /**
     * Sets The primaryLanguageCode.
     * 
     * @param primaryLanguageCode
     *            The primaryLanguageCode to set.
     */
    public void setPrimaryLanguageCode(final ExternalCode primaryLanguageCode) {
        this.primaryLanguageCode = primaryLanguageCode;
    }

    /**
     * Sets The nationalityCountry.
     * 
     * @param nationalityCountry
     *            The nationalityCountry to set.
     */
    public void setNationalityCountry(final Set<Country> nationalityCountry) {
        this.nationalityCountry = nationalityCountry;
    }

    /**
     * Sets The birthAddress.
     * 
     * @param birthAddress
     *            The birthAddress to set.
     */
    public void setBirthAddress(final PostalAddressContact birthAddress) {
        this.birthAddress = birthAddress;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final Set<PersonName> name) {
        this.name = name;
    }

    /**
     * Sets The citizenshipCountry.
     * 
     * @param citizenshipCountry
     *            The citizenshipCountry to set.
     */
    public void setCitizenshipCountry(final Set<Country> citizenshipCountry) {
        this.citizenshipCountry = citizenshipCountry;
    }

    /**
     * Sets The maritalStatusCode.
     * 
     * @param maritalStatusCode
     *            The maritalStatusCode to set.
     */
    public void setMaritalStatusCode(final MaritalStatusCodeList maritalStatusCode) {
        this.maritalStatusCode = maritalStatusCode;
    }

    /**
     * Gets the authentication id.
     * 
     * @return the authentication id
     */
    public String getAuthenticationID() {
        return authenticationID;
    }

    /**
     * Sets the authentication id.
     * 
     * @param authenticationID
     *            the new authentication id
     */
    public void setAuthenticationID(String authenticationID) {
        this.authenticationID = authenticationID;
    }

    /**
     * Gets the residence country.
     * 
     * @return the residenceCountry
     */
    public Country getResidenceCountry() {
        return residenceCountry;
    }

    /**
     * Sets the residence country.
     * 
     * @param residenceCountry
     *            the residenceCountry to set
     */
    public void setResidenceCountry(Country residenceCountry) {
        this.residenceCountry = residenceCountry;
    }

    /**
     * Gets the national id type.
     * 
     * @return the nationalIdType
     */
    public String getNationalIdType() {
        return nationalIdType;
    }

    /**
     * Sets the national id type.
     * 
     * @param nationalIdType
     *            the nationalIdType to set
     */
    public void setNationalIdType(String nationalIdType) {
        this.nationalIdType = nationalIdType;
    }

    /**
     * Gets the national id.
     * 
     * @return the nationalId
     */
    public String getNationalId() {
        return nationalId;
    }

    /**
     * Sets the national id.
     * 
     * @param nationalId
     *            the nationalId to set
     */
    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    /**
     * Gets the permanentAddressSameAsCurrentIndicator.
     * 
     * @return Returns the permanentAddressSameAsCurrentIndicator.
     */
    public final Boolean getPermanentAddressSameAsCurrentIndicator() {
        return permanentAddressSameAsCurrentIndicator;
    }

    /**
     * Sets The permanentAddressSameAsCurrentIndicator.
     * 
     * @param permanentAddressSameAsCurrentIndicator
     *            The permanentAddressSameAsCurrentIndicator to set.
     */
    public final void setPermanentAddressSameAsCurrentIndicator(Boolean permanentAddressSameAsCurrentIndicator) {
        this.permanentAddressSameAsCurrentIndicator = permanentAddressSameAsCurrentIndicator;
    }

    /**
     * Gets the spouse or parent name.
     * 
     * @return the spouseOrParentName
     */
    public PersonName getSpouseOrParentName() {
        return spouseOrParentName;
    }

    /**
     * Sets the spouse or parent name.
     * 
     * @param spouseOrParentName
     *            the spouseOrParentName to set
     */
    public void setSpouseOrParentName(PersonName spouseOrParentName) {
        this.spouseOrParentName = spouseOrParentName;
    }

    /**
     * Gets the age proof.
     * 
     * @return the ageProof
     */
    public String getAgeProof() {
        return ageProof;
    }

    /**
     * Sets the age proof.
     * 
     * @param ageProof
     *            the ageProof to set
     */
    public void setAgeProof(String ageProof) {
        this.ageProof = ageProof;
    }

    /**
     * Gets the identity proof.
     * 
     * @return the identityProof
     */
    public String getIdentityProof() {
        return identityProof;
    }

    /**
     * Sets the identity proof.
     * 
     * @param identityProof
     *            the identityProof to set
     */
    public void setIdentityProof(String identityProof) {
        this.identityProof = identityProof;
    }

    /**
     * Gets the height.
     * 
     * @return Returns the height.
     */
    public final Measurement getHeight() {
        return height;
    }

    /**
     * Gets the weight.
     * 
     * @return Returns the weight.
     */
    public final Measurement getWeight() {
        return weight;
    }

    /**
     * Sets The height.
     * 
     * @param height
     *            The height to set.
     */
    public final void setHeight(Measurement height) {
        this.height = height;
    }

    /**
     * Sets The weight.
     * 
     * @param weight
     *            The weight to set.
     */
    public final void setWeight(Measurement weight) {
        this.weight = weight;
    }

    /**
     * Gets the tobacco premium basis.
     * 
     * @return the tobaccoPremiumBasis
     */
    public String getTobaccoPremiumBasis() {
        return tobaccoPremiumBasis;
    }

    /**
     * Sets the tobacco premium basis.
     * 
     * @param tobaccoPremiumBasis
     *            the tobaccoPremiumBasis to set
     */
    public void setTobaccoPremiumBasis(String tobaccoPremiumBasis) {
        this.tobaccoPremiumBasis = tobaccoPremiumBasis;
    }

    /**
     * Gets the drinking premium basis.
     * 
     * @return the drinkingPremiumBasis
     */
    public String getDrinkingPremiumBasis() {
        return drinkingPremiumBasis;
    }

    /**
     * Sets the drinking premium basis.
     * 
     * @param drinkingPremiumBasis
     *            the drinkingPremiumBasis to set
     */
    public void setDrinkingPremiumBasis(String drinkingPremiumBasis) {
        this.drinkingPremiumBasis = drinkingPremiumBasis;
    }

	public Date getIdentityExpDate() {
		return identityExpDate;
	}

	public void setIdentityExpDate(Date identityExpDate) {
		this.identityExpDate = identityExpDate;
	}

	public String getNationalityDesciption() {
		return nationalityDesciption;
	}

	public void setNationalityDesciption(String nationalityDesciption) {
		this.nationalityDesciption = nationalityDesciption;
	}

	public Date getIdentityDate() {
		return identityDate;
	}

	public void setIdentityDate(Date identityDate) {
		this.identityDate = identityDate;
	}

	public String getIdentityPlace() {
		return identityPlace;
	}

	public void setIdentityPlace(String identityPlace) {
		this.identityPlace = identityPlace;
	}

	public void setAgeCalculatedDate(Date ageCalculatedDate) {
		this.ageCalculatedDate = ageCalculatedDate;
	}

	public Date getAgeCalculatedDate() {
		return ageCalculatedDate;
	}

	public String getNationlityThai() {
		return nationlityThai;
	}

	public void setNationlityThai(String nationlityThai) {
		this.nationlityThai = nationlityThai;
	}

	public String getCitizenIdentityCard() {
		return citizenIdentityCard;
	}

	public void setCitizenIdentityCard(String citizenIdentityCard) {
		this.citizenIdentityCard = citizenIdentityCard;
	}
    

}
