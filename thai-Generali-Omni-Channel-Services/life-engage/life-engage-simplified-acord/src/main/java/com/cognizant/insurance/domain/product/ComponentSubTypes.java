/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Sep 20, 2017
 */

package com.cognizant.insurance.domain.product;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class ComponentSubTypes.
 */
@Entity
@DiscriminatorValue("Component_SubType")
public class ComponentSubTypes extends Type {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1233623780021528914L;

	/**
	 * Instantiates a new component sub types.
	 */
	public ComponentSubTypes() {
		super();
	}

}
