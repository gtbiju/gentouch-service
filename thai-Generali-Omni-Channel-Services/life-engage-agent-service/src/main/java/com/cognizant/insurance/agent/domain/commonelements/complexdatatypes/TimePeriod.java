package com.cognizant.insurance.agent.domain.commonelements.complexdatatypes;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



/**
 * A duration of time expressed via distinct start and end points in time.
 */

@Entity
public class TimePeriod implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The id. */
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    /**
     * The start date/time of the time period.
     * 
     * The start and end date/time values are considered inclusive. This means
     * that the expressed startDateTime is the first point in time considered to
     * be in the specified Time Period.
     */    
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDateTime;

    /**
     * The end date/time of the time period.
     * 
     * The start and end date/time values are considered inclusive. This means
     * that the expressed endDateTime is the last point in time considered to be
     * in the specified Time Period.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDateTime;

    /**
     * Gets the startDateTime.
     * 
     * @return Returns the startDateTime.
     */
    public Date getStartDateTime() {
        return startDateTime;
    }

    /**
     * Gets the endDateTime.
     * 
     * @return Returns the endDateTime.
     */
    public Date getEndDateTime() {
        return endDateTime;
    }

    /**
     * Sets The startDateTime.
     * 
     * @param startDateTime
     *            The startDateTime to set.
     */
    public void setStartDateTime(final Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    /**
     * Sets The endDateTime.
     * 
     * @param endDateTime
     *            The endDateTime to set.
     */
    public void setEndDateTime(final Date endDateTime) {
        this.endDateTime = endDateTime;
    }

	/**
	 * @param id the id to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
}
