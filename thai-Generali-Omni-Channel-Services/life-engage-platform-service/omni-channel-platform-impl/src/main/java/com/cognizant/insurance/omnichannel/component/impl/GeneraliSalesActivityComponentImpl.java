package com.cognizant.insurance.omnichannel.component.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliSalesActivityComponent;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliEAppRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliIllustrationRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliLMSRepository;
import com.cognizant.insurance.omnichannel.component.repository.UserRepository;
import com.cognizant.insurance.omnichannel.dataresults.dto.TeamDetailsDto;
import com.cognizant.insurance.omnichannel.vo.AgentDataVo;
import com.cognizant.insurance.omnichannel.vo.AgentReportVo;
import com.cognizant.insurance.omnichannel.vo.SalesActivityDataVo;
import com.cognizant.insurance.omnichannel.vo.SalesActivityResponseDataVo;
import com.cognizant.insurance.omnichannel.vo.ScoringDataVo;
import com.cognizant.insurance.omnichannel.vo.ConversionRatioDataVo;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;
import com.google.common.collect.Lists;



@Service("generaliSalesActivityComponent")
public class GeneraliSalesActivityComponentImpl implements GeneraliSalesActivityComponent {
    
    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliSalesActivityComponentImpl.class);

    @Autowired
    private GeneraliLMSRepository lmsRepository;

    @Autowired
    private GeneraliIllustrationRepository illustrationRepository;

    @Autowired
    private GeneraliEAppRepository eAppRepository;
    
    @Autowired
    private UserRepository userRepository;

    /** The retrieve Sales Activity holder. */
    @Autowired
    @Qualifier("retrieveSalesActivityMapping")
    private LifeEngageSmooksHolder retrieveSalesActivityHolder;
    
    @Value("${le.platform.service.salesActivity.benchmark.call}")
    private String salesActivityBenchmarkForCall;
    
    @Value("${le.platform.service.salesActivity.benchmark.appointment}")
    private String salesActivityBenchmarkForAppointment;
    
    @Value("${le.platform.service.salesActivity.benchmark.visiting}")
    private String salesActivityBenchmarkForVisiting;
    
    @Value("${le.platform.service.salesActivity.benchmark.newProspect}")
    private String salesActivityBenchmarkForNewProspect;
    
    @Value("${le.platform.service.salesActivity.benchmark.presentation}")
    private String salesActivityBenchmarkForPresentation;
    
    @Value("${le.platform.service.salesActivity.benchmark.selling}")
    private String salesActivityBenchmarkForSelling;
    
    @Value("${le.platform.service.scoring.benchmark.call}")
    private String scoringBenchmarkForCall;
    
    @Value("${le.platform.service.scoring.benchmark.appointment}")
    private String scoringBenchmarkForAppointment;
    
    @Value("${le.platform.service.scoring.benchmark.visiting}")
    private String scoringBenchmarkForVisiting;
    
    @Value("${le.platform.service.scoring.benchmark.newProspect}")
    private String scoringBenchmarkForNewProspect;
    
    @Value("${le.platform.service.scoring.benchmark.presentation}")
    private String scoringBenchmarkForPresentation;
    
    @Value("${le.platform.service.scoring.benchmark.selling}")
    private String scoringBenchmarkForSelling;
    
    @Value("${le.platform.service.scoring.points.call}")
    private String scoringPointsForCall;
    
    @Value("${le.platform.service.scoring.points.appointment}")
    private String scoringPointsForAppointment;
    
    @Value("${le.platform.service.scoring.points.visiting}")
    private String scoringPointsForVisiting;
    
    @Value("${le.platform.service.scoring.points.newProspect}")
    private String scoringPointsForNewProspect;
    
    @Value("${le.platform.service.scoring.points.presentation}")
    private String scoringPointsForPresentation;
    
    @Value("${le.platform.service.scoring.points.selling}")
    private String scoringPointsForSelling;
    
    @Value("${le.platform.service.benchmark.performance.points.perday}")
    private String benchmarkStandardPerformancePointsPerday;

    @Override
    public String retrieveSalesActivityDetails(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException, ParseException {
        String response = null;
        String agentId = null;
        String typeCode = null;
        boolean thaiLanguageInd = false;
        
        SalesActivityResponseDataVo salesActivityResponseDataVo = new SalesActivityResponseDataVo();
        
        Transactions userTransactions = new Transactions();
        
        try {
            //Thailand Holiday List
            //getThaiHolidays();
            
            if (jsonRqArray != null) {
                final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(0);
                JSONObject jsonTransactionData = null;

                // Login User ID
                agentId = jsonTransactionsObj.getString(Constants.KEY11);
                salesActivityResponseDataVo.setAgentCode(agentId);

                if (!(jsonTransactionsObj.isNull(GeneraliConstants.TRANSACTION_DATA))) {

                    jsonTransactionData = jsonTransactionsObj.getJSONObject(GeneraliConstants.TRANSACTION_DATA);
                    final JSONObject salesActivityCriteriaRqJson =
                            jsonTransactionData.getJSONObject(Constants.SEARCHCRITERIAREQUEST);
                    String command = salesActivityCriteriaRqJson.getString(Constants.COMMAND);
                    String team = salesActivityCriteriaRqJson.getString(GeneraliConstants.TEAM);
                    String teamAgent = salesActivityCriteriaRqJson.getString(GeneraliConstants.TEAM_AGENT);
                    String language = salesActivityCriteriaRqJson.getString(GeneraliConstants.LANGUAGE);
                    
                    team = team.equalsIgnoreCase(GeneraliConstants.ALL) ? StringUtils.EMPTY : team;
                    teamAgent = teamAgent.equalsIgnoreCase(GeneraliConstants.ALL) ? StringUtils.EMPTY : teamAgent;
                    
                    if (language != null && !language.equals(StringUtils.EMPTY) && language.equals(GeneraliConstants.LANGUAGE_THAI)) {
                        thaiLanguageInd = true;
                        userTransactions.setKey5(GeneraliConstants.LANGUAGE_THAI);
                    }

                    // Setting command in Vo
                    if (command != null && !command.equals(StringUtils.EMPTY)) {
                        salesActivityResponseDataVo.setCommand(command);
                    }
                    // Setting selected team in vo
                    if (team != null && !team.equals(StringUtils.EMPTY)) {
                        userTransactions.setKey3(team);
                        salesActivityResponseDataVo.setTeam(team);
                    }
                    // Setting teamAgent in vo
                    if (teamAgent != null && !teamAgent.equals(StringUtils.EMPTY)) {
                        userTransactions.setKey4(teamAgent);
                        salesActivityResponseDataVo.setTeamAgent(teamAgent);
                    }
                    
                    Response<List<TeamDetailsDto>> teamDetailsResponse = null;
                    List<TeamDetailsDto> teamDetailsDtoList = null;
                    HashMap<String, List<AgentDataVo>> teamListMap = new HashMap<String, List<AgentDataVo>>();
                    //Retrieving agent codes list
                    List<AgentDataVo> agentsList = new ArrayList<AgentDataVo>();
                    List<String> agentCodesList = new ArrayList<String>();
                    List<BigDecimal> benchmarkValuesListForSalesActivity = new ArrayList<BigDecimal>();
                    List<BigDecimal> benchmarkValuesListForScoring = new ArrayList<BigDecimal>();
                    ScoringDataVo scoringDataVo = new ScoringDataVo();
                    List<ScoringDataVo> scoringDataVoList = new ArrayList<ScoringDataVo>();
                    SalesActivityDataVo salesActivityDataVo = new SalesActivityDataVo();
                    List<ConversionRatioDataVo> conversionRatioDataVoList = new ArrayList<ConversionRatioDataVo>();
                    Map<String,Map<String,String>> conversionRatioColorMap = new LinkedHashMap<String,Map<String,String>>();
                    ConversionRatioDataVo conversionRatioDataVo = new ConversionRatioDataVo();
                    Map<String,String> achievePercentAcitivity = new HashMap<String,String>();
                    Map<String,Integer> totalMap = new LinkedHashMap<String,Integer>();
                    int benchMarkAgentCount = 1;
                    if (agentId != null && !agentId.equals(StringUtils.EMPTY)) {

                        userTransactions.setKey1(agentId);
                        userTransactions = userRepository.validateAgentProfile(userTransactions);

                        typeCode = validateAgentType(userTransactions);

                        if (typeCode != null) {
                            salesActivityResponseDataVo.setPosition(typeCode);
                            userTransactions.setKey1(agentId);
                            userTransactions.setKey2(typeCode);
                            
                            teamDetailsResponse = userRepository.getTeamDetails(userTransactions);
                            teamDetailsDtoList = teamDetailsResponse.getType();

                            if (!teamDetailsDtoList.isEmpty()) {
                                if(thaiLanguageInd){
                                    teamListMap = getTeamdetailsInThai(teamDetailsDtoList);
                                } else {
                                    teamListMap = getTeamdetailsInEng(teamDetailsDtoList);
                                }
                            }
                            salesActivityResponseDataVo.setTeamListMap(teamListMap);

                            if (!team.equals(StringUtils.EMPTY) && team != null && !teamAgent.equals(StringUtils.EMPTY) && teamAgent != null) {
                                agentCodesList.add(teamAgent);
                                benchMarkAgentCount = 1;
                            } else if (!team.equals(StringUtils.EMPTY) && team != null) {
                                agentsList = teamListMap.get(salesActivityResponseDataVo.getTeam());
                                agentCodesList = getAgentCodes(agentsList);
                                benchMarkAgentCount = agentCodesList.size()>0?agentCodesList.size():benchMarkAgentCount;
                            } else {
                                for (Map.Entry<String, List<AgentDataVo>> teamObj : teamListMap.entrySet()) {
                                    agentsList = teamObj.getValue();
                                    agentCodesList.addAll(getAgentCodes(agentsList));
                                }
                                benchMarkAgentCount = agentCodesList.size()>0?agentCodesList.size():benchMarkAgentCount;
                            }
                        } 
                    }
                    
                    int numberOfWorkingDays = 0;
                    List<Date> dateRange = new ArrayList<Date>();
                    List<Date> dateRangeModified = new ArrayList<Date>();
                    List<Date> dateRangeListForTotPerf = new ArrayList<Date>();
                    dateRange = getDateRange(command);
                    if (!dateRange.isEmpty() ? (dateRange.get(0) != null && dateRange.get(1) != null) : false) {
                        int diff = 0;
                        Date endDateRange = new Date();
                        Calendar endCal = Calendar.getInstance();
                        Calendar friCal = Calendar.getInstance();
                        if (command.equals(GeneraliConstants.CURRENT_WEEK)) {
                            friCal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
                            endCal.setTime(dateRange.get(1));
                            if (endCal.get(7) == 7) {
                                endDateRange = dateRange.get(1);
                            } else {
                                diff = friCal.get(7) - endCal.get(7);
                                endCal.add(Calendar.DAY_OF_WEEK, diff);
                                endDateRange = endCal.getTime();
                            }
                            dateRangeModified = getDatesExcludingPreviousMonth(dateRange.get(0),endDateRange,command);
                            
                            benchmarkValuesListForSalesActivity = getBenchmarkCalculations(dateRangeModified.get(0),
                            		dateRangeModified.get(dateRangeModified.size()-1), salesActivityBenchmarkForCall, salesActivityBenchmarkForAppointment,
                                    salesActivityBenchmarkForVisiting, salesActivityBenchmarkForPresentation,
                                    salesActivityBenchmarkForNewProspect, salesActivityBenchmarkForSelling,benchMarkAgentCount);

                            benchmarkValuesListForScoring = getBenchmarkCalculations(dateRangeModified.get(0),
                            		dateRangeModified.get(dateRangeModified.size()-1),
                                    scoringBenchmarkForCall, scoringBenchmarkForAppointment,
                                    scoringBenchmarkForVisiting, scoringBenchmarkForPresentation,
                                    scoringBenchmarkForNewProspect, scoringBenchmarkForSelling,benchMarkAgentCount);

                            numberOfWorkingDays = getNumberOfWorkingDays(dateRangeModified.get(0), dateRangeModified.get(dateRangeModified.size()-1));

                        } else if (command.equals(GeneraliConstants.CURRENT_MONTH)) {
                            endCal.setTime(dateRange.get(1));
                            endCal.set(Calendar.DAY_OF_MONTH, endCal.getActualMaximum(Calendar.DAY_OF_MONTH));
                            endDateRange = endCal.getTime();
                            benchmarkValuesListForSalesActivity = getBenchmarkCalculations(dateRange.get(0),
                                    endDateRange, salesActivityBenchmarkForCall, salesActivityBenchmarkForAppointment,
                                    salesActivityBenchmarkForVisiting, salesActivityBenchmarkForPresentation,
                                    salesActivityBenchmarkForNewProspect, salesActivityBenchmarkForSelling,benchMarkAgentCount);

                            benchmarkValuesListForScoring = getBenchmarkCalculations(dateRange.get(0), endDateRange,
                                    scoringBenchmarkForCall, scoringBenchmarkForAppointment,
                                    scoringBenchmarkForVisiting, scoringBenchmarkForPresentation,
                                    scoringBenchmarkForNewProspect, scoringBenchmarkForSelling,benchMarkAgentCount);

                            numberOfWorkingDays = getNumberOfWorkingDays(dateRange.get(0), endDateRange);
                            
                            dateRangeListForTotPerf.add(dateRange.get(0));
                            dateRangeListForTotPerf.add(endDateRange);

                        } else if (command.equals(GeneraliConstants.LAST_THREE_MONTHS)) {
                            benchmarkValuesListForSalesActivity =
                                    getBenchmarkCalculations(dateRange.get(4), dateRange.get(1),
                                            salesActivityBenchmarkForCall, salesActivityBenchmarkForAppointment,
                                            salesActivityBenchmarkForVisiting, salesActivityBenchmarkForPresentation,
                                            salesActivityBenchmarkForNewProspect, salesActivityBenchmarkForSelling,benchMarkAgentCount);

                            benchmarkValuesListForScoring = getBenchmarkCalculations(dateRange.get(4), dateRange.get(1),
                                    scoringBenchmarkForCall, scoringBenchmarkForAppointment,
                                    scoringBenchmarkForVisiting, scoringBenchmarkForPresentation,
                                    scoringBenchmarkForNewProspect, scoringBenchmarkForSelling,benchMarkAgentCount);

                            numberOfWorkingDays = getNumberOfWorkingDays(dateRange.get(4), dateRange.get(1));
                            
                            dateRangeListForTotPerf.add(dateRange.get(4));
                            dateRangeListForTotPerf.add(dateRange.get(1));

                        } else if (command.equals(GeneraliConstants.LAST_SIX_MONTHS)) {
                            benchmarkValuesListForSalesActivity =
                                    getBenchmarkCalculations(dateRange.get(10), dateRange.get(1),
                                            salesActivityBenchmarkForCall, salesActivityBenchmarkForAppointment,
                                            salesActivityBenchmarkForVisiting, salesActivityBenchmarkForPresentation,
                                            salesActivityBenchmarkForNewProspect, salesActivityBenchmarkForSelling,benchMarkAgentCount);

                            benchmarkValuesListForScoring = getBenchmarkCalculations(dateRange.get(10),
                                    dateRange.get(1), scoringBenchmarkForCall, scoringBenchmarkForAppointment,
                                    scoringBenchmarkForVisiting, scoringBenchmarkForPresentation,
                                    scoringBenchmarkForNewProspect, scoringBenchmarkForSelling,benchMarkAgentCount);

                            numberOfWorkingDays = getNumberOfWorkingDays(dateRange.get(10), dateRange.get(1));
                            
                            dateRangeListForTotPerf.add(dateRange.get(10));
                            dateRangeListForTotPerf.add(dateRange.get(1));

                        } else {
                        	if(command.equals(GeneraliConstants.PREVIOUS_WEEK)) {
                        		dateRangeModified = getDatesExcludingPreviousMonth(dateRange.get(0),endDateRange,command);
                        		benchmarkValuesListForSalesActivity =
                                        getBenchmarkCalculations(dateRangeModified.get(0), dateRangeModified.get(dateRangeModified.size()-1),
                                                salesActivityBenchmarkForCall, salesActivityBenchmarkForAppointment,
                                                salesActivityBenchmarkForVisiting, salesActivityBenchmarkForPresentation,
                                                salesActivityBenchmarkForNewProspect, salesActivityBenchmarkForSelling,benchMarkAgentCount);

                                benchmarkValuesListForScoring = getBenchmarkCalculations(dateRangeModified.get(0), dateRangeModified.get(dateRangeModified.size()-1),
                                        scoringBenchmarkForCall, scoringBenchmarkForAppointment,
                                        scoringBenchmarkForVisiting, scoringBenchmarkForPresentation,
                                        scoringBenchmarkForNewProspect, scoringBenchmarkForSelling,benchMarkAgentCount);

                                numberOfWorkingDays = getNumberOfWorkingDays(dateRangeModified.get(0), dateRangeModified.get(dateRangeModified.size()-1));
                                
                                dateRangeListForTotPerf.add(dateRangeModified.get(0));
                                dateRangeListForTotPerf.add(dateRangeModified.get(dateRangeModified.size()-1));
                        	}
                        	else {
                        		benchmarkValuesListForSalesActivity =
                                        getBenchmarkCalculations(dateRange.get(0), dateRange.get(1),
                                                salesActivityBenchmarkForCall, salesActivityBenchmarkForAppointment,
                                                salesActivityBenchmarkForVisiting, salesActivityBenchmarkForPresentation,
                                                salesActivityBenchmarkForNewProspect, salesActivityBenchmarkForSelling,benchMarkAgentCount);

                                benchmarkValuesListForScoring = getBenchmarkCalculations(dateRange.get(0), dateRange.get(1),
                                        scoringBenchmarkForCall, scoringBenchmarkForAppointment,
                                        scoringBenchmarkForVisiting, scoringBenchmarkForPresentation,
                                        scoringBenchmarkForNewProspect, scoringBenchmarkForSelling,benchMarkAgentCount);

                                numberOfWorkingDays = getNumberOfWorkingDays(dateRange.get(0), dateRange.get(1));
                                
                                dateRangeListForTotPerf.add(dateRange.get(0));
                                dateRangeListForTotPerf.add(dateRange.get(1));
                        	}
                        }
                    }
                    
                    // Scoring Objects
                    Map<String,String> callCountForScoring = new LinkedHashMap<String, String>();
                    Map<String,String> visitCountForScoring = new LinkedHashMap<String, String>();
                    Map<String,String> appointmentCountForScoring = new LinkedHashMap<String, String>();
                    Map<String,String> presentationCountForScoring = new LinkedHashMap<String, String>();
                    Map<String,String> closecaseCountForScoring = new LinkedHashMap<String, String>();
                    Map<String,String> newProspectCountForScoring = new LinkedHashMap<String, String>();
                    Map<String,String> totalCountForScoring = new LinkedHashMap<String, String>();
                    
                    Map<String,String> callCountForScoringInPercent = new LinkedHashMap<String, String>();
                    Map<String,String> visitCountForScoringInPercent = new LinkedHashMap<String, String>();
                    Map<String,String> appointmentCountForScoringInPercent = new LinkedHashMap<String, String>();
                    Map<String,String> presentationCountForScoringInPercent = new LinkedHashMap<String, String>();
                    Map<String,String> closecaseCountForScoringInPercent = new LinkedHashMap<String, String>();
                    Map<String,String> newProspectCountForScoringInPercent = new LinkedHashMap<String, String>();
                    
                    Map<String,Integer> newProspect = new LinkedHashMap<String,Integer>();
                    if(command.equals(GeneraliConstants.CURRENT_MONTH) || command.equals(GeneraliConstants.PREVIOUS_MONTH)){
                        //List<SalesActivityDataVo> nonAppointmentDetails = getCallVisitDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                        Map<String,Integer> callDetails = getMonthlyCallDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                        Map<String,Integer> visitDetails = getMonthlyVisitDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                        Map<String,Integer> appointmentDetails = getMonthlyAppointmentDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                        Map<String,Integer> closeCaseDetails = getMonthlyEappDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                        Map<String,Integer> presentationDetails = getMonthlyPresentationDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                        newProspect = getMonthlyNewProspectDetails(requestInfo, agentCodesList, salesActivityResponseDataVo,command);
                        
                        callDetails.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(callDetails,0, command));
                        visitDetails.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(visitDetails,0, command));
                        appointmentDetails.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(appointmentDetails,0, command));
                        closeCaseDetails.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(closeCaseDetails,0, command));
                        presentationDetails.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(presentationDetails,0, command));
                        newProspect.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(newProspect,0, command));
                        
                        callDetails.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(0).toString()));
                        visitDetails.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(2).toString()));
                        appointmentDetails.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(1).toString()));
                        presentationDetails.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(3).toString()));
                        closeCaseDetails.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(5).toString()));
                        newProspect.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(4).toString()));
                        
                        SalesActivityDataVo salesActivityVo = new SalesActivityDataVo();
                        salesActivityVo.setAppointmentWeekCount(appointmentDetails);
                        salesActivityVo.setCallWeekCount(callDetails);
                        salesActivityVo.setVisitWeekCount(visitDetails);
                        salesActivityVo.setPresentationWeekCount(presentationDetails);
                        salesActivityVo.setCloseCaseWeekCount(closeCaseDetails);
                        salesActivityVo.setNewProspectDetails(newProspect);
                        
                        
                		achievePercentAcitivity  = getAchieveCustom(salesActivityVo,salesActivityDataVo,command,benchmarkValuesListForSalesActivity);
                		salesActivityVo.setAchieveAcitivity(achievePercentAcitivity);
                		
                		List<String> percentActivity = new ArrayList<String>(achievePercentAcitivity.values());
                		callDetails.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(0)));
                		visitDetails.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(2)));
                        appointmentDetails.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(1)));
                        closeCaseDetails.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(4)));
                        presentationDetails.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(3)));
                        newProspect.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(5)));
                        
                        Map<String, Integer> previousMonthCallCount = new LinkedHashMap<String,Integer>();
                        Map<String, Integer> previousMonthVisitCount = new LinkedHashMap<String,Integer>();
                        Map<String, Integer> previousMonthAppointmentCount = new LinkedHashMap<String,Integer>();
                        Map<String, Integer> previousMonthPresentationCount = new LinkedHashMap<String,Integer>();
                        Map<String, Integer> previousMonthCloseCaseCount = new LinkedHashMap<String,Integer>();
                        Map<String, Integer> previousMonthNewProspectCount = new LinkedHashMap<String,Integer>();
                
                      //Pre-Month
                        if (command.equals(GeneraliConstants.CURRENT_MONTH)) {
                            
                        	previousMonthCallCount = getMonthlyCallDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_MONTH);
                            previousMonthVisitCount = getMonthlyVisitDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_MONTH);
                            previousMonthAppointmentCount = getMonthlyAppointmentDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_MONTH);
                            previousMonthPresentationCount = getMonthlyPresentationDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_MONTH);
                            previousMonthCloseCaseCount = getMonthlyEappDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_MONTH);
                            previousMonthNewProspectCount = getMonthlyNewProspectDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_MONTH);

                            callDetails.put(GeneraliConstants.PRE_MONTHLY, getTotalCountValue(previousMonthCallCount,0, command));
                            visitDetails.put(GeneraliConstants.PRE_MONTHLY, getTotalCountValue(previousMonthVisitCount,0, command));
                            appointmentDetails.put(GeneraliConstants.PRE_MONTHLY, getTotalCountValue(previousMonthAppointmentCount,0, command));
                            closeCaseDetails.put(GeneraliConstants.PRE_MONTHLY, getTotalCountValue(previousMonthCloseCaseCount,0, command));
                            presentationDetails.put(GeneraliConstants.PRE_MONTHLY, getTotalCountValue(previousMonthPresentationCount,0, command));
                            newProspect.put(GeneraliConstants.PRE_MONTHLY, getTotalCountValue(previousMonthNewProspectCount,0, command));
                        }
                        
                    	int totalCount = 0;
                		Set<String> keySet = salesActivityVo.getCallWeekCount().keySet();
                		List<String> keyList = new ArrayList<String>(keySet);
                		BigDecimal totalActivityCount = BigDecimal.ZERO;
            			BigDecimal benchmarkCount = BigDecimal.ONE;
            			BigDecimal percentAchieveTotalCount = BigDecimal.ZERO;
                		for(int i=0;i<=keyList.size()-1;i++){
                			int callCnt = 0;
                			int visitCnt = 0;
                			int appntCnt = 0;
                			int newProsCnt = 0;
                            int presentationCount = 0;
                            int closeCaseCount = 0;
                			
                			if(salesActivityVo.getCallWeekCount().get(keyList.get(i)) > 0){
                				callCnt = salesActivityVo.getCallWeekCount().get(keyList.get(i));
                			}
                			if(salesActivityVo.getVisitWeekCount().get(keyList.get(i)) > 0){
                				visitCnt = salesActivityVo.getVisitWeekCount().get(keyList.get(i));
                			}
                			if(salesActivityVo.getAppointmentWeekCount().get(keyList.get(i)) > 0){
                				appntCnt = salesActivityVo.getAppointmentWeekCount().get(keyList.get(i));
                			}
                			
                			if(salesActivityVo.getNewProspectDetails().get(keyList.get(i)) > 0){
                			    newProsCnt = salesActivityVo.getNewProspectDetails().get(keyList.get(i));
                            }
                			
                			if(salesActivityVo.getPresentationWeekCount().get(keyList.get(i)) > 0){
                				presentationCount = salesActivityVo.getPresentationWeekCount().get(keyList.get(i));
                            }
                			
                			if(salesActivityVo.getCloseCaseWeekCount().get(keyList.get(i)) > 0){
                                closeCaseCount = salesActivityVo.getCloseCaseWeekCount().get(keyList.get(i));
                            }
                            
                            totalCount = callCnt + visitCnt+ appntCnt + newProsCnt + presentationCount + closeCaseCount;
                			
                			if(keyList.get(i).equals(GeneraliConstants.TOTAL_ACTIVITY)){
                				totalActivityCount = new BigDecimal(totalCount);
                			}
                			
                			if(keyList.get(i).equals(GeneraliConstants.BENCHMARK)){
                				benchmarkCount = new BigDecimal(totalCount);
                			}
                			
                			if(keyList.get(i).equals(GeneraliConstants.PERCENT_ACHIEVE)){
                				percentAchieveTotalCount = ((totalActivityCount.divide(benchmarkCount, 2, RoundingMode.HALF_UP)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED)).setScale(0, RoundingMode.HALF_UP);
                				totalCount =  (int) Double.parseDouble(percentAchieveTotalCount.toString());
                			}
                			
                			totalMap.put(keyList.get(i), totalCount);
                		}
                		salesActivityVo.setTotalMap(totalMap);
                		List<SalesActivityDataVo> salesActivityDetails = new ArrayList<SalesActivityDataVo>();
                		salesActivityDetails.add(salesActivityVo);
                		salesActivityResponseDataVo.setSalesActivityDetails(salesActivityDetails);
                		
                		//Scoring calculations
                		callCountForScoring = calculatePointsForScoring(callDetails, scoringPointsForCall, benchmarkValuesListForScoring.get(0));
                		scoringDataVo.setCallCountMap(callCountForScoring);
                		visitCountForScoring = calculatePointsForScoring(visitDetails, scoringPointsForVisiting, benchmarkValuesListForScoring.get(2));
                		scoringDataVo.setVisitCountMap(visitCountForScoring);
                		appointmentCountForScoring = calculatePointsForScoring(appointmentDetails, scoringPointsForAppointment, benchmarkValuesListForScoring.get(1));
                		scoringDataVo.setAppointmentCountMap(appointmentCountForScoring);
                		presentationCountForScoring = calculatePointsForScoring(presentationDetails, scoringPointsForPresentation, benchmarkValuesListForScoring.get(3));
                		scoringDataVo.setPresentationCountMap(presentationCountForScoring);
                		closecaseCountForScoring = calculatePointsForScoring(closeCaseDetails, scoringPointsForSelling, benchmarkValuesListForScoring.get(5));
                		scoringDataVo.setCloseCountMap(closecaseCountForScoring);
                		newProspectCountForScoring = calculatePointsForScoring(newProspect, scoringPointsForNewProspect, benchmarkValuesListForScoring.get(4));
                		scoringDataVo.setNewProspectCountMap(newProspectCountForScoring);
                		totalCountForScoring = getScoringTotalMap(scoringDataVo);
                		scoringDataVo.setTotalCountMap(totalCountForScoring);
                		
                		/*** Map to get the Current Month Details for close case ratio*/
                		Map<String,BigDecimal> closeCaseRatioforCurrentMonth = new LinkedHashMap<String,BigDecimal>();
                		closeCaseRatioforCurrentMonth = buildCloseCaseRatio(closeCaseDetails, presentationDetails,command);
                		
                		
                		
                		/*** Map to get the Previous Month Details for close case ratio*/
                        Map<String,BigDecimal>closeCaseRatioforPrevMonth = new LinkedHashMap<String,BigDecimal>();
                        previousMonthPresentationCount = getMonthlyPresentationDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_MONTH);
                        previousMonthCloseCaseCount = getMonthlyEappDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_MONTH);
                        closeCaseRatioforPrevMonth = buildCloseCaseRatio(previousMonthCloseCaseCount, previousMonthPresentationCount,command);
                        
                        /*** Map to build the final response*/
                        Map<String,Map<String,BigDecimal>> closeCaseRatio = new LinkedHashMap<String,Map<String,BigDecimal>>();
                              
                        if(command.equals(GeneraliConstants.CURRENT_MONTH)){
                        	closeCaseRatio.put(GeneraliConstants.CURRENT_MONTH, closeCaseRatioforCurrentMonth);
                        	setcolorForConversionRatio(conversionRatioColorMap, closeCaseRatioforCurrentMonth,conversionRatioDataVo, GeneraliConstants.CURRENT_MONTH);
                        	closeCaseRatio.put(GeneraliConstants.PREVIOUS_MONTH, closeCaseRatioforPrevMonth);
                        	setcolorForConversionRatio(conversionRatioColorMap, closeCaseRatioforPrevMonth,conversionRatioDataVo, GeneraliConstants.PREVIOUS_MONTH);
                        }
                        else {
                        	closeCaseRatio.put(GeneraliConstants.PREVIOUS_MONTH, closeCaseRatioforPrevMonth);
                        	setcolorForConversionRatio(conversionRatioColorMap, closeCaseRatioforPrevMonth,conversionRatioDataVo, GeneraliConstants.PREVIOUS_MONTH);
                        }
                        conversionRatioDataVo.setColorForConversionRatio(conversionRatioColorMap);
                        conversionRatioDataVo.setConversionRatio(closeCaseRatio);
                        conversionRatioDataVoList.add(conversionRatioDataVo);
                		
                    }
                    
                    else if(command.equals(GeneraliConstants.CURRENT_WEEK) || command.equals(GeneraliConstants.PREVIOUS_WEEK)){
                    	Map<String,Integer> weeklyCallCount = getWeeklyCallDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                    	Map<String,Integer> weeklyVisitCount = getWeeklyVisitDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                    	Map<String,Integer> weeklyAppointmentCount = getWeeklyAppointmentDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                    	//illustrationCount
                    	Map<String,Integer> weeklyPresentationCount = getWeeklyPresentationDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                    	//eApp count
                    	Map<String,Integer> weeklyCloseCaseCount = getWeeklyCloseDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, command);
                    	newProspect = getWeekwiseNewProspectDetails(requestInfo, agentCodesList, salesActivityResponseDataVo,command);
                    	
                    	weeklyCallCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(weeklyCallCount,0, command));
                        weeklyVisitCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(weeklyVisitCount,0, command));
                        weeklyAppointmentCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(weeklyAppointmentCount,0, command));
                        weeklyPresentationCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(weeklyPresentationCount,0, command));
                        weeklyCloseCaseCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(weeklyCloseCaseCount,0, command));
                        newProspect.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(newProspect,0, command));
                        
                        weeklyCallCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(0).toString()));
                        weeklyVisitCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(2).toString()));
                        weeklyAppointmentCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(1).toString()));
                        weeklyPresentationCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(3).toString()));
                        weeklyCloseCaseCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(5).toString()));
                        newProspect.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(4).toString()));
                        
                        SalesActivityDataVo salesActivityVo = new SalesActivityDataVo();
                     	salesActivityVo.setWeekwiseCallCount(weeklyCallCount);
                     	salesActivityVo.setWeekwiseAppointmentCount(weeklyAppointmentCount);
                     	salesActivityVo.setWeekwiseVisitCount(weeklyVisitCount);
                     	salesActivityVo.setWeekwisePresentationCount(weeklyPresentationCount);
                     	salesActivityVo.setWeekwiseCloseCount(weeklyCloseCaseCount);
                     	salesActivityVo.setNewProspectDetails(newProspect);
                     	
                        achievePercentAcitivity  = getAchieveCustom(salesActivityVo,salesActivityDataVo,command,benchmarkValuesListForSalesActivity);
                     	salesActivityVo.setAchieveAcitivity(achievePercentAcitivity);
                     	
                    	List<String> percentActivity = new ArrayList<String>(achievePercentAcitivity.values());
                    	weeklyCallCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(0)));
                    	weeklyVisitCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(2)));
                    	weeklyAppointmentCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(1)));
                    	weeklyCloseCaseCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(4)));
                    	weeklyPresentationCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(3)));
                    	newProspect.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(5)));
                    	
                    	 if (command.equals(GeneraliConstants.CURRENT_WEEK)) {
                             Map<String, Integer> previousWeekCallCount = getWeeklyCallDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_WEEK);
                             Map<String, Integer> previousWeekVisitCount = getWeeklyVisitDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_WEEK);
                             Map<String, Integer> previousWeekAppointmentCount = getWeeklyAppointmentDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_WEEK);
                             Map<String, Integer> previousWeekPresentationCount = getWeeklyPresentationDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_WEEK);
                             Map<String, Integer> previousWeekCloseCaseCount = getWeeklyCloseDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_WEEK);
                             Map<String, Integer> previousWeekNewProspectCount = getWeekwiseNewProspectDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_WEEK);

                             weeklyCallCount.put(GeneraliConstants.PRE_WEEKLY, getTotalCountValue(previousWeekCallCount,0, command));
                             weeklyVisitCount.put(GeneraliConstants.PRE_WEEKLY, getTotalCountValue(previousWeekVisitCount,0, command));
                             weeklyAppointmentCount.put(GeneraliConstants.PRE_WEEKLY, getTotalCountValue(previousWeekAppointmentCount,0, command));
                             weeklyPresentationCount.put(GeneraliConstants.PRE_WEEKLY, getTotalCountValue(previousWeekPresentationCount,0, command));
                             weeklyCloseCaseCount.put(GeneraliConstants.PRE_WEEKLY, getTotalCountValue(previousWeekCloseCaseCount,0, command));
                             newProspect.put(GeneraliConstants.PRE_WEEKLY, getTotalCountValue(previousWeekNewProspectCount,0, command));
                         }
                    	 

                    	int totalCount = 0;
                    	BigDecimal totalActivityCount = BigDecimal.ZERO;
            			BigDecimal benchmarkCount = BigDecimal.ONE;
            			BigDecimal percentAchieveTotalCount = BigDecimal.ZERO;
                		Set<String> keySet = salesActivityVo.getWeekwiseCallCount().keySet();
                		List<String> keyList = new ArrayList<String>(keySet);
                		for(int i=0;i<=keyList.size()-1;i++){
                			
                			int callCnt = 0;
                			int visitCnt = 0;
                			int appntCnt = 0;
                			int newProsCnt = 0;
                            int presentationCount = 0;
                            int closeCaseCount = 0;
                			
                			if(salesActivityVo.getWeekwiseCallCount().get(keyList.get(i)) > 0){
                				callCnt = salesActivityVo.getWeekwiseCallCount().get(keyList.get(i));
                			}
                			if(salesActivityVo.getWeekwiseVisitCount().get(keyList.get(i)) > 0){
                				visitCnt = salesActivityVo.getWeekwiseVisitCount().get(keyList.get(i));
                			}
                			if(salesActivityVo.getWeekwiseAppointmentCount().get(keyList.get(i)) > 0){
                				appntCnt = salesActivityVo.getWeekwiseAppointmentCount().get(keyList.get(i));
                			}
                			
                			if(salesActivityVo.getNewProspectDetails().get(keyList.get(i)) > 0){
                			    newProsCnt = salesActivityVo.getNewProspectDetails().get(keyList.get(i));
                            }
                			
                			if(salesActivityVo.getWeekwisePresentationCount().get(keyList.get(i)) > 0){
                				presentationCount = salesActivityVo.getWeekwisePresentationCount().get(keyList.get(i));
                            }
                			
                			if(salesActivityVo.getWeekwiseCloseCount().get(keyList.get(i)) > 0){
                                closeCaseCount = salesActivityVo.getWeekwiseCloseCount().get(keyList.get(i));
                            }
                            
                            totalCount = callCnt + visitCnt+ appntCnt + newProsCnt + presentationCount + closeCaseCount;
                            
                            if(keyList.get(i).equals(GeneraliConstants.TOTAL_ACTIVITY)){
                				totalActivityCount = new BigDecimal(totalCount);
                			}
                			
                			if(keyList.get(i).equals(GeneraliConstants.BENCHMARK)){
                				benchmarkCount = new BigDecimal(totalCount);
                			}
                			
                			if(keyList.get(i).equals(GeneraliConstants.PERCENT_ACHIEVE)){
                				percentAchieveTotalCount = ((totalActivityCount.divide(benchmarkCount, 2, RoundingMode.HALF_UP)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED)).setScale(0, RoundingMode.HALF_UP);
                				totalCount =  (int) Double.parseDouble(percentAchieveTotalCount.toString());
                			}
                            
                			totalMap.put(keyList.get(i), totalCount);
                		}
                		
                		salesActivityVo.setTotalMap(totalMap);
                    	
                    	List<SalesActivityDataVo> salesActivityDetails = new ArrayList<SalesActivityDataVo>();
                		salesActivityDetails.add(salesActivityVo);
                		salesActivityResponseDataVo.setSalesActivityDetails(salesActivityDetails);
                		
                		//Scoring calculations
                		callCountForScoring = calculatePointsForScoring(weeklyCallCount, scoringPointsForCall, benchmarkValuesListForScoring.get(0));
                		scoringDataVo.setCallCountMap(callCountForScoring);
                		visitCountForScoring = calculatePointsForScoring(weeklyVisitCount, scoringPointsForVisiting, benchmarkValuesListForScoring.get(2));
                		scoringDataVo.setVisitCountMap(visitCountForScoring);
                		appointmentCountForScoring = calculatePointsForScoring(weeklyAppointmentCount, scoringPointsForAppointment, benchmarkValuesListForScoring.get(1));
                		scoringDataVo.setAppointmentCountMap(appointmentCountForScoring);
                		presentationCountForScoring = calculatePointsForScoring(weeklyPresentationCount, scoringPointsForPresentation, benchmarkValuesListForScoring.get(3));
                		scoringDataVo.setPresentationCountMap(presentationCountForScoring);
                		closecaseCountForScoring = calculatePointsForScoring(weeklyCloseCaseCount, scoringPointsForSelling, benchmarkValuesListForScoring.get(5));
                		scoringDataVo.setCloseCountMap(closecaseCountForScoring);
                		newProspectCountForScoring = calculatePointsForScoring(newProspect, scoringPointsForNewProspect, benchmarkValuesListForScoring.get(4));
                		scoringDataVo.setNewProspectCountMap(newProspectCountForScoring);
                		totalCountForScoring = getScoringTotalMap(scoringDataVo);
                		scoringDataVo.setTotalCountMap(totalCountForScoring);
                		
                		/*** Conversion Ratio Calculations */
                		/*** Map to get the Current Week Details for close case ratio*/
                		Map<String,BigDecimal>closeCaseRatioforCurrentWeek = new LinkedHashMap<String, BigDecimal>();
                		closeCaseRatioforCurrentWeek = buildCloseCaseRatio(weeklyCloseCaseCount, weeklyPresentationCount,command);
                		
                		/*** Map to get the Previous Week Details for close case ratio*/
                		Map<String, Integer> previousWeekPresentationCount = getWeeklyPresentationDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_WEEK);
                        Map<String, Integer> previousWeekCloseCaseCount = getWeeklyCloseDetails(requestInfo, agentCodesList, salesActivityResponseDataVo, GeneraliConstants.PREVIOUS_WEEK);
                        Map<String,BigDecimal>closeCaseRatioforPrevWeek = new LinkedHashMap<String, BigDecimal>();
                        closeCaseRatioforPrevWeek = buildCloseCaseRatio(previousWeekCloseCaseCount, previousWeekPresentationCount,command);
                        
                        /*** Map to build the final response*/
                        Map<String,Map<String,BigDecimal>> closeCaseRatio = new HashMap<String,Map<String,BigDecimal>>();
                        
                        if(command.equals(GeneraliConstants.CURRENT_WEEK)){
                        	closeCaseRatio.put(GeneraliConstants.CURRENT_WEEK, closeCaseRatioforCurrentWeek);
                        	setcolorForConversionRatio(conversionRatioColorMap, closeCaseRatioforCurrentWeek,conversionRatioDataVo, GeneraliConstants.CURRENT_WEEK);
                        	closeCaseRatio.put(GeneraliConstants.PREVIOUS_WEEK, closeCaseRatioforPrevWeek);
                        	setcolorForConversionRatio(conversionRatioColorMap, closeCaseRatioforPrevWeek,conversionRatioDataVo, GeneraliConstants.PREVIOUS_WEEK);
                        }
                        else {
                        	closeCaseRatio.put(GeneraliConstants.PREVIOUS_WEEK, closeCaseRatioforPrevWeek);
                        	setcolorForConversionRatio(conversionRatioColorMap, closeCaseRatioforPrevWeek,conversionRatioDataVo, GeneraliConstants.PREVIOUS_WEEK);
                        }
                        conversionRatioDataVo.setColorForConversionRatio(conversionRatioColorMap);
                        conversionRatioDataVo.setConversionRatio(closeCaseRatio);
                        conversionRatioDataVoList.add(conversionRatioDataVo);
                    }
                    
                    else if(command.equals(GeneraliConstants.LAST_THREE_MONTHS) || command.equals(GeneraliConstants.LAST_SIX_MONTHS)){
                    	Map<String,Integer> monthwiseCallCount = getMonthwiseCallDetails(requestInfo, agentCodesList, salesActivityResponseDataVo);
                        Map<String,Integer> monthwiseVisitCount = getMonthwiseVisitDetails(requestInfo, agentCodesList, salesActivityResponseDataVo);
                        Map<String,Integer> monthwiseAppointmentCount = getMonthwiseAppointmentDetails(requestInfo, agentCodesList, salesActivityResponseDataVo);
                        //illustration count
                        Map<String,Integer> monthwisePresentationCount = getMonthwisePresentationDetails(requestInfo, agentCodesList, salesActivityResponseDataVo);
                        //eApp count
                        Map<String,Integer> monthwiseCloseCaseCount = getMonthwiseCloseCaseDetails(requestInfo, agentCodesList, salesActivityResponseDataVo);
                        newProspect = getMonthwiseNewProspectDetails(requestInfo, agentCodesList, salesActivityResponseDataVo);
                        
                        monthwiseCallCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(monthwiseCallCount,0, command));
                        monthwiseVisitCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(monthwiseVisitCount,0, command));
                        monthwiseAppointmentCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(monthwiseAppointmentCount,0, command));
                        monthwisePresentationCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(monthwisePresentationCount,0, command));
                        monthwiseCloseCaseCount.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(monthwiseCloseCaseCount,0, command));
                        newProspect.put(GeneraliConstants.TOTAL_ACTIVITY, getTotalCountValue(newProspect,0, command));
                        
                        
                        monthwiseCallCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(0).toString()));
                        monthwiseVisitCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(2).toString()));
                        monthwiseAppointmentCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(1).toString()));
                        monthwisePresentationCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(3).toString()));
                        monthwiseCloseCaseCount.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(5).toString()));
                        newProspect.put(GeneraliConstants.BENCHMARK, (int)Double.parseDouble(benchmarkValuesListForSalesActivity.get(4).toString()));
                        
                        SalesActivityDataVo salesActivityVo = new SalesActivityDataVo();
                        salesActivityVo.setMonthwiseVisitCount(monthwiseVisitCount);
                        salesActivityVo.setMonthwiseAppointmentCount(monthwiseAppointmentCount);
                        salesActivityVo.setMonthwiseCallCount(monthwiseCallCount);
                        salesActivityVo.setMonthwisePresentationCount(monthwisePresentationCount);
                        salesActivityVo.setMonthwiseCloseCount(monthwiseCloseCaseCount);
                        salesActivityVo.setNewProspectDetails(newProspect);
                        achievePercentAcitivity  = getAchieveCustom(salesActivityVo,salesActivityDataVo,command,benchmarkValuesListForSalesActivity);
                        salesActivityVo.setAchieveAcitivity(achievePercentAcitivity);
                        
                        List<String> percentActivity = new ArrayList<String>(achievePercentAcitivity.values());
                        monthwiseCallCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(0)));
                        monthwiseVisitCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(2)));
                        monthwiseAppointmentCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(1)));
                        monthwiseCloseCaseCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(4)));
                    	monthwisePresentationCount.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(3)));
                    	newProspect.put(GeneraliConstants.PERCENT_ACHIEVE, (int)Double.parseDouble(percentActivity.get(5)));
                    	
                        
                        getTotalMap(totalMap, salesActivityVo);
                		
                		salesActivityVo.setTotalMap(totalMap);
                        
                		List<SalesActivityDataVo> salesActivityDetails = new ArrayList<SalesActivityDataVo>();
                		salesActivityDetails.add(salesActivityVo);
                		salesActivityResponseDataVo.setSalesActivityDetails(salesActivityDetails);
                		
                		//Scoring calculations
                		callCountForScoring = calculatePointsForScoring(monthwiseCallCount, scoringPointsForCall, benchmarkValuesListForScoring.get(0));
                		scoringDataVo.setCallCountMap(callCountForScoring);
                		visitCountForScoring = calculatePointsForScoring(monthwiseVisitCount, scoringPointsForVisiting, benchmarkValuesListForScoring.get(2));
                		scoringDataVo.setVisitCountMap(visitCountForScoring);
                		appointmentCountForScoring = calculatePointsForScoring(monthwiseAppointmentCount, scoringPointsForAppointment, benchmarkValuesListForScoring.get(1));
                		scoringDataVo.setAppointmentCountMap(appointmentCountForScoring);
                		presentationCountForScoring = calculatePointsForScoring(monthwisePresentationCount, scoringPointsForPresentation, benchmarkValuesListForScoring.get(3));
                		scoringDataVo.setPresentationCountMap(presentationCountForScoring);
                		closecaseCountForScoring = calculatePointsForScoring(monthwiseCloseCaseCount, scoringPointsForSelling, benchmarkValuesListForScoring.get(5));
                		scoringDataVo.setCloseCountMap(closecaseCountForScoring);
                		newProspectCountForScoring = calculatePointsForScoring(newProspect, scoringPointsForNewProspect, benchmarkValuesListForScoring.get(4));
                		scoringDataVo.setNewProspectCountMap(newProspectCountForScoring);
                		totalCountForScoring = getScoringTotalMap(scoringDataVo);
                		scoringDataVo.setTotalCountMap(totalCountForScoring);
                		//scoringDataVoList.add(scoringDataVo);
                		
                		/*** Conversion Ratio Calculations*/
                		Map<String,BigDecimal>closeCaseRatioforlastMonths = new LinkedHashMap<String, BigDecimal>();
                		closeCaseRatioforlastMonths = buildCloseCaseRatio(monthwiseCloseCaseCount, monthwisePresentationCount,command);
                		
                		/*** Map to build the final response*/
                        Map<String,Map<String,BigDecimal>> closeCaseRatio = new LinkedHashMap<String,Map<String,BigDecimal>>();
                        
                		if(command.equals(GeneraliConstants.LAST_THREE_MONTHS)){
                        	closeCaseRatio.put(GeneraliConstants.LAST_THREE_MONTHS, closeCaseRatioforlastMonths);
                        	 setcolorForConversionRatio(conversionRatioColorMap,closeCaseRatioforlastMonths,conversionRatioDataVo, GeneraliConstants.LAST_THREE_MONTHS);
                        }
                        else {
                        	closeCaseRatio.put(GeneraliConstants.LAST_SIX_MONTHS, closeCaseRatioforlastMonths);
                        	 setcolorForConversionRatio(conversionRatioColorMap,closeCaseRatioforlastMonths,conversionRatioDataVo, GeneraliConstants.LAST_SIX_MONTHS);
                        }
                		conversionRatioDataVo.setColorForConversionRatio(conversionRatioColorMap);
                		conversionRatioDataVo.setConversionRatio(closeCaseRatio);
                		conversionRatioDataVoList.add(conversionRatioDataVo);
                    }

                    List<SalesActivityDataVo> salesActivityDetails = salesActivityResponseDataVo.getSalesActivityDetails();
                    salesActivityDetails.get(0).setColumnHeader(GeneraliConstants.BENCHMARK);
                    salesActivityDetails.get(0).setCall(benchmarkValuesListForSalesActivity.get(0).toString());
                    salesActivityDetails.get(0).setAppointment(benchmarkValuesListForSalesActivity.get(1).toString());
                    salesActivityDetails.get(0).setVisiting(benchmarkValuesListForSalesActivity.get(2).toString());
                    salesActivityDetails.get(0).setPresentation(benchmarkValuesListForSalesActivity.get(3).toString());
                    salesActivityDetails.get(0).setNewProspect(benchmarkValuesListForSalesActivity.get(4).toString());
                    salesActivityDetails.get(0).setCloseCase(benchmarkValuesListForSalesActivity.get(5).toString());
                    salesActivityDetails.get(0).setTotal(benchmarkValuesListForSalesActivity.get(6).toString());

                    // Scoring calculations
                    dateRange = getDateRange(command);
                    Map<String,String> callColorMap = new LinkedHashMap<String,String>();
                    Map<String,String> visitColorMap = new LinkedHashMap<String,String>();
                    Map<String,String> appointmentColorMap = new LinkedHashMap<String,String>();
                    Map<String,String> newProspectColorMap = new LinkedHashMap<String,String>();
                    Map<String,String> presentationColorMap = new LinkedHashMap<String,String>();
                    Map<String,String> closeCaseColorMap = new LinkedHashMap<String,String>();
                    Map<String,String> totalColorMap = new LinkedHashMap<String,String>();
                    
                    if (!dateRange.isEmpty() ? (dateRange.get(0) != null && dateRange.get(1) != null) : false) {
                        List<Date> weekWiseDateListForColorcoding = new ArrayList<Date>();
                        
                        List<BigDecimal> benchmarkValuesListForCall = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForVisit = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForAppointment = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForNewProspect = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForPresentation = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForCloseCase = new ArrayList<BigDecimal>();
                        
                        List<BigDecimal> benchmarkValuesListForCallScoring = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForVisitScoring = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForAppointmentScoring = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForNewProspectScoring = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForPresentationScoring = new ArrayList<BigDecimal>();
                        List<BigDecimal> benchmarkValuesListForCloseCaseScoring = new ArrayList<BigDecimal>();
                        
                        // Start of Benchmark calculation for color coding only for current week and previous week
                        if(command.equals(GeneraliConstants.CURRENT_WEEK) || command.equals(GeneraliConstants.PREVIOUS_WEEK)){
                            benchmarkValuesListForCall.add(new BigDecimal(salesActivityBenchmarkForCall));
                            benchmarkValuesListForVisit.add(new BigDecimal(salesActivityBenchmarkForVisiting));
                            benchmarkValuesListForAppointment.add(new BigDecimal(salesActivityBenchmarkForAppointment)); 
                            benchmarkValuesListForNewProspect.add(new BigDecimal(salesActivityBenchmarkForNewProspect));
                            benchmarkValuesListForPresentation.add(new BigDecimal(salesActivityBenchmarkForPresentation));
                            benchmarkValuesListForCloseCase.add(new BigDecimal(salesActivityBenchmarkForSelling));
                            
                            benchmarkValuesListForCallScoring.add(new BigDecimal(scoringBenchmarkForCall));
                            benchmarkValuesListForVisitScoring.add(new BigDecimal(scoringBenchmarkForVisiting));
                            benchmarkValuesListForAppointmentScoring.add(new BigDecimal(scoringBenchmarkForAppointment)); 
                            benchmarkValuesListForNewProspectScoring.add(new BigDecimal(scoringBenchmarkForNewProspect));
                            benchmarkValuesListForPresentationScoring.add(new BigDecimal(scoringBenchmarkForPresentation));
                            benchmarkValuesListForCloseCaseScoring.add(new BigDecimal(scoringBenchmarkForSelling));
                        }
                        // End of Benchmark calculation for color coding only for current week and previous week
                        
                        if (command.equals(GeneraliConstants.CURRENT_WEEK)) {
                            Set<String> keySet = salesActivityDetails.get(0).getWeekwiseCallCount().keySet();
                    		List<String> keyList = new ArrayList<String>(keySet);
                            
                            callColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwiseCallCount(), keyList, benchmarkValuesListForCall, command,benchMarkAgentCount);
                    		visitColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwiseVisitCount(), keyList, benchmarkValuesListForVisit, command,benchMarkAgentCount);
                    		appointmentColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwiseAppointmentCount(), keyList, benchmarkValuesListForAppointment, command,benchMarkAgentCount);
                    		newProspectColorMap = extractColorDetails(salesActivityDetails.get(0).getNewProspectDetails(), keyList, benchmarkValuesListForNewProspect, command,benchMarkAgentCount);
                    		presentationColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwisePresentationCount(),keyList, benchmarkValuesListForPresentation, command,benchMarkAgentCount);
                    		closeCaseColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwiseCloseCount(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                            totalColorMap = extractColorDetails(salesActivityDetails.get(0).getTotalMap(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                    		
                    		callColorMap = filterColorWithoutPreviousMonthData(callColorMap,command);
                    		visitColorMap = filterColorWithoutPreviousMonthData(visitColorMap,command);
                    		appointmentColorMap = filterColorWithoutPreviousMonthData(appointmentColorMap,command);
                    		newProspectColorMap = filterColorWithoutPreviousMonthData(newProspectColorMap,command);
                    		presentationColorMap = filterColorWithoutPreviousMonthData(presentationColorMap,command);
                    		closeCaseColorMap = filterColorWithoutPreviousMonthData(closeCaseColorMap,command);
                    		
                        } else if (command.equals(GeneraliConstants.CURRENT_MONTH)) {
                    		Set<String> keySet = salesActivityDetails.get(0).getCallWeekCount().keySet();
                    		List<String> keyList = new ArrayList<String>(keySet);
                    		
                    		weekWiseDateListForColorcoding = getWeekwiseDateForAMonth(dateRangeListForTotPerf.get(0), dateRangeListForTotPerf.get(1));
                            benchmarkValuesListForCall = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForCall);
                            benchmarkValuesListForVisit = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForVisiting);
                            benchmarkValuesListForAppointment = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForAppointment);
                            benchmarkValuesListForNewProspect = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForNewProspect);
                            benchmarkValuesListForPresentation = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForPresentation);
                            benchmarkValuesListForCloseCase = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForSelling);
                            
                            benchmarkValuesListForCallScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForCall);
                            benchmarkValuesListForVisitScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForVisiting);
                            benchmarkValuesListForAppointmentScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForAppointment);
                            benchmarkValuesListForNewProspectScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForNewProspect);
                            benchmarkValuesListForPresentationScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForPresentation);
                            benchmarkValuesListForCloseCaseScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForSelling);
                            
                    		callColorMap = extractColorDetails(salesActivityDetails.get(0).getCallWeekCount(), keyList, benchmarkValuesListForCall, command,benchMarkAgentCount);
                    		visitColorMap = extractColorDetails(salesActivityDetails.get(0).getVisitWeekCount(), keyList, benchmarkValuesListForVisit, command,benchMarkAgentCount);
                    		appointmentColorMap = extractColorDetails(salesActivityDetails.get(0).getAppointmentWeekCount(), keyList, benchmarkValuesListForAppointment, command,benchMarkAgentCount);
                    		newProspectColorMap = extractColorDetails(salesActivityDetails.get(0).getNewProspectDetails(), keyList, benchmarkValuesListForNewProspect, command,benchMarkAgentCount);
                    		presentationColorMap = extractColorDetails(salesActivityDetails.get(0).getPresentationWeekCount(), keyList, benchmarkValuesListForPresentation, command,benchMarkAgentCount);
                    		closeCaseColorMap = extractColorDetails(salesActivityDetails.get(0).getCloseCaseWeekCount(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                    		totalColorMap = extractColorDetails(salesActivityDetails.get(0).getTotalMap(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                            
                        } else if (command.equals(GeneraliConstants.LAST_THREE_MONTHS)) {
                            
                            Set<String> keySet = salesActivityDetails.get(0).getMonthwiseCallCount().keySet();
                    		List<String> keyList = new ArrayList<String>(keySet);
                            
                    		weekWiseDateListForColorcoding = getMonthwiseDateForLastThreeOrSixMonths(dateRangeListForTotPerf.get(0), dateRangeListForTotPerf.get(1));
                    		benchmarkValuesListForCall = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForCall);
                            benchmarkValuesListForVisit = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForVisiting);
                            benchmarkValuesListForAppointment = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForAppointment);
                            benchmarkValuesListForNewProspect = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForNewProspect);
                            benchmarkValuesListForPresentation = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForPresentation);
                            benchmarkValuesListForCloseCase = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForSelling);
                            
                            benchmarkValuesListForCallScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForCall);
                            benchmarkValuesListForVisitScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForVisiting);
                            benchmarkValuesListForAppointmentScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForAppointment);
                            benchmarkValuesListForNewProspectScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForNewProspect);
                            benchmarkValuesListForPresentationScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForPresentation);
                            benchmarkValuesListForCloseCaseScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForSelling);
                            
                            callColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwiseCallCount(), keyList, benchmarkValuesListForCall, command,benchMarkAgentCount);
                    		visitColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwiseVisitCount(), keyList, benchmarkValuesListForVisit, command,benchMarkAgentCount);
                    		appointmentColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwiseAppointmentCount(), keyList, benchmarkValuesListForAppointment, command,benchMarkAgentCount);
                    		newProspectColorMap = extractColorDetails(salesActivityDetails.get(0).getNewProspectDetails(), keyList, benchmarkValuesListForNewProspect, command,benchMarkAgentCount);
                    		presentationColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwisePresentationCount(), keyList, benchmarkValuesListForPresentation, command,benchMarkAgentCount);
                    		closeCaseColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwiseCloseCount(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                    		totalColorMap = extractColorDetails(salesActivityDetails.get(0).getTotalMap(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                            
                        } else if (command.equals(GeneraliConstants.LAST_SIX_MONTHS)) {
                          
                            
                            Set<String> keySet = salesActivityDetails.get(0).getMonthwiseCallCount().keySet();
                    		List<String> keyList = new ArrayList<String>(keySet);
                            
                    		weekWiseDateListForColorcoding = getMonthwiseDateForLastThreeOrSixMonths(dateRangeListForTotPerf.get(0), dateRangeListForTotPerf.get(1));
                            benchmarkValuesListForCall = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForCall);
                            benchmarkValuesListForVisit = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForVisiting);
                            benchmarkValuesListForAppointment = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForAppointment);
                            benchmarkValuesListForNewProspect = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForNewProspect);
                            benchmarkValuesListForPresentation = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForPresentation);
                            benchmarkValuesListForCloseCase = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForSelling);
                            
                            benchmarkValuesListForCallScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForCall);
                            benchmarkValuesListForVisitScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForVisiting);
                            benchmarkValuesListForAppointmentScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForAppointment);
                            benchmarkValuesListForNewProspectScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForNewProspect);
                            benchmarkValuesListForPresentationScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForPresentation);
                            benchmarkValuesListForCloseCaseScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                    weekWiseDateListForColorcoding, keyList, scoringBenchmarkForSelling);
                            
                            callColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwiseCallCount(), keyList, benchmarkValuesListForCall, command,benchMarkAgentCount);
                    		visitColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwiseVisitCount(), keyList, benchmarkValuesListForVisit, command,benchMarkAgentCount);
                    		appointmentColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwiseAppointmentCount(), keyList, benchmarkValuesListForAppointment, command,benchMarkAgentCount);
                    		newProspectColorMap = extractColorDetails(salesActivityDetails.get(0).getNewProspectDetails(), keyList, benchmarkValuesListForNewProspect, command,benchMarkAgentCount);
                    		presentationColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwisePresentationCount(), keyList, benchmarkValuesListForPresentation, command,benchMarkAgentCount);
                    		closeCaseColorMap = extractColorDetails(salesActivityDetails.get(0).getMonthwiseCloseCount(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                    		totalColorMap = extractColorDetails(salesActivityDetails.get(0).getTotalMap(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                    		
                        } else {
                                     
                            
                    		if(command.equals(GeneraliConstants.PREVIOUS_MONTH)){
                    			Set<String> keySet = salesActivityDetails.get(0).getCallWeekCount().keySet();
                        		List<String> keyList = new ArrayList<String>(keySet);
                        		
                        		weekWiseDateListForColorcoding = getWeekwiseDateForAMonth(dateRangeListForTotPerf.get(0), dateRangeListForTotPerf.get(1));
                                benchmarkValuesListForCall = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForCall);
                                benchmarkValuesListForVisit = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForVisiting);
                                benchmarkValuesListForAppointment = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForAppointment);
                                benchmarkValuesListForNewProspect = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForNewProspect);
                                benchmarkValuesListForPresentation = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForPresentation);
                                benchmarkValuesListForCloseCase = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, salesActivityBenchmarkForSelling);
                                
                                benchmarkValuesListForCallScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForCall);
                                benchmarkValuesListForVisitScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForVisiting);
                                benchmarkValuesListForAppointmentScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForAppointment);
                                benchmarkValuesListForNewProspectScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForNewProspect);
                                benchmarkValuesListForPresentationScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForPresentation);
                                benchmarkValuesListForCloseCaseScoring = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForSelling);
                        		
                    			callColorMap = extractColorDetails(salesActivityDetails.get(0).getCallWeekCount(), keyList, benchmarkValuesListForCall, command,benchMarkAgentCount);
                        		visitColorMap = extractColorDetails(salesActivityDetails.get(0).getVisitWeekCount(), keyList, benchmarkValuesListForVisit, command,benchMarkAgentCount);
                        		appointmentColorMap = extractColorDetails(salesActivityDetails.get(0).getAppointmentWeekCount(), keyList, benchmarkValuesListForAppointment, command,benchMarkAgentCount);
                        		newProspectColorMap = extractColorDetails(salesActivityDetails.get(0).getNewProspectDetails(), keyList, benchmarkValuesListForNewProspect, command,benchMarkAgentCount);
                        		presentationColorMap = extractColorDetails(salesActivityDetails.get(0).getPresentationWeekCount(), keyList, benchmarkValuesListForPresentation, command,benchMarkAgentCount);
                        		closeCaseColorMap = extractColorDetails(salesActivityDetails.get(0).getCloseCaseWeekCount(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                        		totalColorMap = extractColorDetails(salesActivityDetails.get(0).getTotalMap(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                        		
                    		}
                    		
                    		else{
                    			Set<String> keySet = salesActivityDetails.get(0).getWeekwiseCallCount().keySet();
                        		List<String> keyList = new ArrayList<String>(keySet);
                        		
                    			callColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwiseCallCount(), keyList, benchmarkValuesListForCall, command,benchMarkAgentCount);
                        		visitColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwiseVisitCount(), keyList, benchmarkValuesListForVisit, command,benchMarkAgentCount);
                        		appointmentColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwiseAppointmentCount(), keyList, benchmarkValuesListForAppointment, command,benchMarkAgentCount);
                        		newProspectColorMap = extractColorDetails(salesActivityDetails.get(0).getNewProspectDetails(), keyList, benchmarkValuesListForNewProspect, command,benchMarkAgentCount);
                        		presentationColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwisePresentationCount(), keyList, benchmarkValuesListForPresentation, command,benchMarkAgentCount);
                        		closeCaseColorMap = extractColorDetails(salesActivityDetails.get(0).getWeekwiseCloseCount(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                        		totalColorMap = extractColorDetails(salesActivityDetails.get(0).getTotalMap(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
                        		
                        		callColorMap = filterColorWithoutPreviousMonthData(callColorMap,command);
                        		visitColorMap = filterColorWithoutPreviousMonthData(visitColorMap,command);
                        		appointmentColorMap = filterColorWithoutPreviousMonthData(appointmentColorMap,command);
                        		newProspectColorMap = filterColorWithoutPreviousMonthData(newProspectColorMap,command);
                        		presentationColorMap = filterColorWithoutPreviousMonthData(presentationColorMap,command);
                        		closeCaseColorMap = filterColorWithoutPreviousMonthData(closeCaseColorMap,command);
                        		
                    		}
                        }
                        salesActivityDetails.get(0).setCallColorMap(callColorMap);
                        salesActivityDetails.get(0).setVisitColorMap(visitColorMap);
                        salesActivityDetails.get(0).setAppointmentColorMap(appointmentColorMap);
                        salesActivityDetails.get(0).setNewProspectColorMap(newProspectColorMap);
                        salesActivityDetails.get(0).setPresentationColorMap(presentationColorMap);
                        salesActivityDetails.get(0).setCloseCaseColorMap(closeCaseColorMap);
                        salesActivityDetails.get(0).setTotalColorMap(totalColorMap);
                    
                       
                    //Scoring calculations - Start of Points to Percentage conversion
                    callCountForScoringInPercent = calculatePointstoPercentageConversion(callCountForScoring, benchmarkValuesListForCallScoring, command);
                    visitCountForScoringInPercent = calculatePointstoPercentageConversion(visitCountForScoring, benchmarkValuesListForVisitScoring, command);
                    appointmentCountForScoringInPercent = calculatePointstoPercentageConversion(appointmentCountForScoring, benchmarkValuesListForAppointmentScoring, command);
                    presentationCountForScoringInPercent = calculatePointstoPercentageConversion(presentationCountForScoring, benchmarkValuesListForPresentationScoring, command);
                    closecaseCountForScoringInPercent = calculatePointstoPercentageConversion(closecaseCountForScoring, benchmarkValuesListForCloseCaseScoring, command);
                    newProspectCountForScoringInPercent = calculatePointstoPercentageConversion(newProspectCountForScoring, benchmarkValuesListForNewProspectScoring, command);
                    
                    callCountForScoringInPercent.put(GeneraliConstants.TOTAL_POINTS, callCountForScoring.get(GeneraliConstants.TOTAL_POINTS));
                    visitCountForScoringInPercent.put(GeneraliConstants.TOTAL_POINTS, visitCountForScoring.get(GeneraliConstants.TOTAL_POINTS));
                    appointmentCountForScoringInPercent.put(GeneraliConstants.TOTAL_POINTS, appointmentCountForScoring.get(GeneraliConstants.TOTAL_POINTS));
                    presentationCountForScoringInPercent.put(GeneraliConstants.TOTAL_POINTS, presentationCountForScoring.get(GeneraliConstants.TOTAL_POINTS));
                    closecaseCountForScoringInPercent.put(GeneraliConstants.TOTAL_POINTS, closecaseCountForScoring.get(GeneraliConstants.TOTAL_POINTS));
                    newProspectCountForScoringInPercent.put(GeneraliConstants.TOTAL_POINTS, newProspectCountForScoring.get(GeneraliConstants.TOTAL_POINTS));
                    
                    callCountForScoringInPercent.put(GeneraliConstants.BENCHMARK, benchmarkValuesListForScoring.get(0).toString());
                    visitCountForScoringInPercent.put(GeneraliConstants.BENCHMARK, benchmarkValuesListForScoring.get(2).toString());
                    appointmentCountForScoringInPercent.put(GeneraliConstants.BENCHMARK, benchmarkValuesListForScoring.get(1).toString());
                    presentationCountForScoringInPercent.put(GeneraliConstants.BENCHMARK, benchmarkValuesListForScoring.get(3).toString());
                    closecaseCountForScoringInPercent.put(GeneraliConstants.BENCHMARK, benchmarkValuesListForScoring.get(5).toString());
                    newProspectCountForScoringInPercent.put(GeneraliConstants.BENCHMARK, benchmarkValuesListForScoring.get(4).toString());
                    
                    callCountForScoringInPercent.put(GeneraliConstants.PERCENT_ACHIEVE, callCountForScoring.get(GeneraliConstants.PERCENT_ACHIEVE));
                    visitCountForScoringInPercent.put(GeneraliConstants.PERCENT_ACHIEVE, visitCountForScoring.get(GeneraliConstants.PERCENT_ACHIEVE));
                    appointmentCountForScoringInPercent.put(GeneraliConstants.PERCENT_ACHIEVE, appointmentCountForScoring.get(GeneraliConstants.PERCENT_ACHIEVE));
                    presentationCountForScoringInPercent.put(GeneraliConstants.PERCENT_ACHIEVE, presentationCountForScoring.get(GeneraliConstants.PERCENT_ACHIEVE));
                    closecaseCountForScoringInPercent.put(GeneraliConstants.PERCENT_ACHIEVE,  closecaseCountForScoring.get(GeneraliConstants.PERCENT_ACHIEVE));
                    newProspectCountForScoringInPercent.put(GeneraliConstants.PERCENT_ACHIEVE,  newProspectCountForScoring.get(GeneraliConstants.PERCENT_ACHIEVE)); 
                    
                    scoringDataVo.setCallCountMapInPercent(callCountForScoringInPercent);
                    scoringDataVo.setVisitCountMapInPercent(visitCountForScoringInPercent);
                    scoringDataVo.setAppointmentCountMapInPercent(appointmentCountForScoringInPercent);
                    scoringDataVo.setPresentationCountMapInPercent(presentationCountForScoringInPercent);
                    scoringDataVo.setCloseCountMapInPercent(closecaseCountForScoringInPercent);
                    scoringDataVo.setNewProspectCountInPercent(newProspectCountForScoringInPercent);
            		scoringDataVo.setTotalCountInPercent(totalCountForScoring);
            		
            		Set<String> scoringKeySet = scoringDataVo.getCallCountMapInPercent().keySet();
            		List<String> scoringKeyList = new ArrayList<String>(scoringKeySet);
            		setScoringColorMaps(scoringDataVo, scoringKeyList, command, dateRangeListForTotPerf,benchMarkAgentCount);
                    
                    // End of Points to Percentage conversion

                    // Scoring Total % Performance
                    if (numberOfWorkingDays != 0) {
                        List<BigDecimal> callListForTotPercentPerformance = new ArrayList<BigDecimal>();
                        List<BigDecimal> visitListForTotPercentPerformance = new ArrayList<BigDecimal>();
                        List<BigDecimal> appointmentListForTotPercentPerformance = new ArrayList<BigDecimal>();
                        List<BigDecimal> presentationListForTotPercentPerformance = new ArrayList<BigDecimal>();
                        List<BigDecimal> closeCaseListForTotPercentPerformance = new ArrayList<BigDecimal>();
                        List<BigDecimal> newProspectListForTotPercentPerformance = new ArrayList<BigDecimal>();
                        List<Date> weekWiseDateList = new ArrayList<Date>();
                        
                        callListForTotPercentPerformance = getColumnListForTotalPercentagePerformance(callCountForScoring);
                        visitListForTotPercentPerformance = getColumnListForTotalPercentagePerformance(visitCountForScoring);
                        appointmentListForTotPercentPerformance = getColumnListForTotalPercentagePerformance(appointmentCountForScoring);
                        presentationListForTotPercentPerformance = getColumnListForTotalPercentagePerformance(presentationCountForScoring);
                        closeCaseListForTotPercentPerformance = getColumnListForTotalPercentagePerformance(closecaseCountForScoring);
                        newProspectListForTotPercentPerformance = getColumnListForTotalPercentagePerformance(newProspectCountForScoring);
                        
                        BigDecimal standardPerformancePoints = BigDecimal.ZERO;
                        List<BigDecimal> totalPointsPerColumnList = new ArrayList<BigDecimal>();
                        List<BigDecimal> totPercentPerformanceList = new ArrayList<BigDecimal>();
                        int numberOfWorkingDaysEven = 0;
                        int numberOfWorkingDaysOdd = 1;
                        
                        if(command.equals(GeneraliConstants.CURRENT_WEEK) || command.equals(GeneraliConstants.PREVIOUS_WEEK)){
                            standardPerformancePoints = new BigDecimal(benchmarkStandardPerformancePointsPerday);
                        } else if(command.equals(GeneraliConstants.CURRENT_MONTH) || command.equals(GeneraliConstants.PREVIOUS_MONTH)) {
                            weekWiseDateList = getWeekwiseDateForAMonth(dateRangeListForTotPerf.get(0), dateRangeListForTotPerf.get(1));
                        } else if (command.equals(GeneraliConstants.LAST_THREE_MONTHS) || command.equals(GeneraliConstants.LAST_SIX_MONTHS)){
                            weekWiseDateList = getMonthwiseDateForLastThreeOrSixMonths(dateRangeListForTotPerf.get(0), dateRangeListForTotPerf.get(1));
                        }
                        
                        for (int k = 0; k <= callListForTotPercentPerformance.size()-1; k++) {
                            BigDecimal totPercentPerformance = BigDecimal.ZERO;
                            BigDecimal actualPerformancePoints = BigDecimal.ZERO;
                            int numberOfWorkingDaysWeekwise = 0;
                                // To add for Presentation and Close case
                                if (!visitListForTotPercentPerformance.isEmpty() && !appointmentListForTotPercentPerformance.isEmpty()
                                        && !presentationListForTotPercentPerformance.isEmpty() && !closeCaseListForTotPercentPerformance.isEmpty()
                                        && !newProspectListForTotPercentPerformance.isEmpty()) {

                                    if (!callListForTotPercentPerformance.get(k).equals(GeneraliConstants.MINUS_ONE_VALUE)) {
                                        actualPerformancePoints = callListForTotPercentPerformance.get(k)
                                            .add(visitListForTotPercentPerformance.get(k))
                                            .add(appointmentListForTotPercentPerformance.get(k))
                                            .add(presentationListForTotPercentPerformance.get(k))
                                            .add(closeCaseListForTotPercentPerformance.get(k))
                                            .add(newProspectListForTotPercentPerformance.get(k));
                                    }
                                }
                                
                                // Week wise logic for a month
                                if (command.equals(GeneraliConstants.CURRENT_MONTH) || command.equals(GeneraliConstants.PREVIOUS_MONTH)
                                        || command.equals(GeneraliConstants.LAST_THREE_MONTHS) || command.equals(GeneraliConstants.LAST_SIX_MONTHS)) {
                                    numberOfWorkingDaysWeekwise = getNumberOfWorkingDays(weekWiseDateList.get(numberOfWorkingDaysEven), weekWiseDateList.get(numberOfWorkingDaysOdd));
                                    standardPerformancePoints = new BigDecimal(numberOfWorkingDaysWeekwise).multiply(new BigDecimal(benchmarkStandardPerformancePointsPerday));
                                    numberOfWorkingDaysEven += 2;
                                    numberOfWorkingDaysOdd += 2;
                                }
                            totalPointsPerColumnList.add(actualPerformancePoints);
                            totPercentPerformance = ((actualPerformancePoints.divide(standardPerformancePoints, 2,
                                        RoundingMode.HALF_UP)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED)).setScale(0, RoundingMode.HALF_UP);
                            totPercentPerformanceList.add(totPercentPerformance);
                        }
                        
                        Map<String,String> totPercentPerformanceMap = new LinkedHashMap<String, String>();
                        for(BigDecimal totPercentPerformance: totPercentPerformanceList){
                            for(Map.Entry<String,String> eachColumnKeyValue: callCountForScoring.entrySet()){
                                if (!eachColumnKeyValue.getKey().equals(GeneraliConstants.TOTAL_POINTS)
                                        && !eachColumnKeyValue.getKey().equals(GeneraliConstants.PERCENT_ACHIEVE)
                                        && !totPercentPerformanceMap.containsKey(eachColumnKeyValue.getKey())) {
                                    totPercentPerformanceMap.put(eachColumnKeyValue.getKey(), totPercentPerformance.toString());
                                    break;
                                }
                            }
                        }
                        totPercentPerformanceMap.put(GeneraliConstants.TOTAL_POINTS, StringUtils.EMPTY);
                        totPercentPerformanceMap.put(GeneraliConstants.BENCHMARK, StringUtils.EMPTY);
                        totPercentPerformanceMap.put(GeneraliConstants.PERCENT_ACHIEVE,StringUtils.EMPTY);
                        scoringDataVo.setTotalPercentPerformanceMap(totPercentPerformanceMap);
                        scoringDataVoList.add(scoringDataVo);
                        salesActivityResponseDataVo.setScoringDetails(scoringDataVoList);
                        salesActivityResponseDataVo.setConversionRatioDetails(conversionRatioDataVoList);
                        
                    }
                }
             }
            }
        }catch (Exception e) {
            LOGGER.error("Unable to retrieve SalesActivity details :" + e.getMessage());
            LifeEngageComponentHelper.createResponseStatus(userTransactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
        } finally {
            response = retrieveSalesActivityHolder.parseBO(salesActivityResponseDataVo);
        }
        return response;
    }
    
	private List<Date> getDatesExcludingPreviousMonth(Date start, Date end, String command) {
		// TODO Auto-generated method stub
		List<Date> dateRange = new ArrayList<Date>();
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		startDate.setTime(start);
		endDate.setTime(end);
		int i = 0;
		if (startDate.get(7) == 7) {
			startDate.add(Calendar.DAY_OF_WEEK, 2);
		}

		else if (startDate.get(7) == 1) {
			startDate.add(Calendar.DAY_OF_WEEK, 1);
		}

		else {
			startDate.set(Calendar.DAY_OF_WEEK, startDate.getActualMinimum(Calendar.DAY_OF_WEEK));
			startDate.add(Calendar.DAY_OF_WEEK, 1);
		}
		while (i < 5) {
			if (isWeekDayCurrentMonth(command, i)) {
				dateRange.add(startDate.getTime());
			}
			i++;
			startDate.add(Calendar.DATE, 1);
		}
		return dateRange;
	}

	/**
     * Method to return previous week/month data used for Pre. Week/Pre. Month Activity
     * 
     * @param previousWeekDataCount
     * @param totalValue
     * @return totalValue for each row
     */    
    private Map<String,BigDecimal> buildCloseCaseRatio(Map<String, Integer> closeCaseDetails,Map<String, Integer> presentationDetails,String command) {
    	
    	
    	Map<String, Integer> totalValueforCloseCaseRatio = new LinkedHashMap<String,Integer>();
    	totalValueforCloseCaseRatio = removeOtherKeysForTotalCalculation(closeCaseDetails);
    	Map<String, Integer> totalValueforPresentation = new LinkedHashMap<String,Integer>();
    	totalValueforPresentation = removeOtherKeysForTotalCalculation(presentationDetails);
    	
    	int overAllCloseCase = getTotalCountValue(totalValueforCloseCaseRatio,0,command);
    	totalValueforCloseCaseRatio.put(GeneraliConstants.OVER_ALL, overAllCloseCase);
    	int overAllPresentationCase = getTotalCountValue(totalValueforPresentation,0,command);
    	totalValueforPresentation.put(GeneraliConstants.OVER_ALL, overAllPresentationCase);
    	
		Map<String,BigDecimal>closeCaseRatio = new LinkedHashMap<String,BigDecimal>();
		for(Map.Entry<String,Integer> closeCaseKeySet: totalValueforCloseCaseRatio.entrySet()){
			for(Map.Entry<String,Integer> presentationKeySet: totalValueforPresentation.entrySet()){
				if(closeCaseKeySet.getKey().equals(presentationKeySet.getKey())){
					BigDecimal eAppCountforCurrentMonth = new BigDecimal(closeCaseKeySet.getValue());
					BigDecimal illustrationCount = new BigDecimal(presentationKeySet.getValue());
					BigDecimal closeCaseCount =new BigDecimal(0);
					if(illustrationCount.compareTo(BigDecimal.ZERO) == 1) {
						closeCaseCount = (eAppCountforCurrentMonth.divide(illustrationCount, 2, RoundingMode.HALF_UP)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED);
					}
					else {
						closeCaseCount = BigDecimal.ZERO;
					}
					closeCaseRatio.put(closeCaseKeySet.getKey(), closeCaseCount);
					break;
				}
			}
		}
		closeCaseRatio.put(GeneraliConstants.BENCHMARK_CONV_RATIO, GeneraliConstants.BENCHMARK_CONV_RATIO_VALUE);
		return closeCaseRatio;
	}
    
    private Map<String, Integer> removeOtherKeysForTotalCalculation(Map<String, Integer> inputDetailsMap) {
    	
    	Map<String, Integer> totalValue = new LinkedHashMap<String,Integer>();
    	for(Map.Entry<String,Integer> inputKeySet: inputDetailsMap.entrySet()){
    		if(! ( inputKeySet.getKey().equals(GeneraliConstants.TOTAL_POINTS) || 
    				inputKeySet.getKey().equals(GeneraliConstants.PRE_MONTHLY) || 
    				inputKeySet.getKey().equals(GeneraliConstants.TOTAL_ACTIVITY) ||
    				inputKeySet.getKey().equals(GeneraliConstants.PERCENT_ACHIEVE) ||
    				inputKeySet.getKey().equals(GeneraliConstants.PRE_WEEKLY) ||
    				inputKeySet.getKey().equals(GeneraliConstants.BENCHMARK) ) ){
    			    totalValue.put(inputKeySet.getKey(), inputKeySet.getValue());
    		}
    	}
    	return totalValue;
    }
    
    private void setcolorForConversionRatio(Map<String,Map<String,String>> calendarDetailsMap,Map<String,BigDecimal> buildCloseCaseRatio ,ConversionRatioDataVo conversionRatioDataVo, String calendarDetails ) {
    	Map<String,String> colorForConversionRatio = new HashMap<String,String>();
    	int intValueCloseCase = 0 ;
    	for(Map.Entry<String,BigDecimal> closeCaseRatioKeySet: buildCloseCaseRatio.entrySet()){
    		if(closeCaseRatioKeySet.getKey().equals(GeneraliConstants.OVER_ALL)){
    		BigDecimal twentyPercent = new BigDecimal(20);
			BigDecimal colorDecider = (closeCaseRatioKeySet.getValue().divide(twentyPercent)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED).setScale(0, RoundingMode.HALF_UP);
			intValueCloseCase = (int) Double.parseDouble(colorDecider.toString());
    		}
    	if(intValueCloseCase > 79){
			colorForConversionRatio.put(closeCaseRatioKeySet.getKey(), "green");
		}else if((intValueCloseCase > 59) && intValueCloseCase <80){
			colorForConversionRatio.put(closeCaseRatioKeySet.getKey(), "yellow");
		}else {
			colorForConversionRatio.put(closeCaseRatioKeySet.getKey(), "red");
		}
    	
    	}
    	colorForConversionRatio.put(GeneraliConstants.BENCHMARK_CONV_RATIO, GeneraliConstants.COLOR_WHITE);
    	calendarDetailsMap.put(calendarDetails, colorForConversionRatio);
    }

    /**
     * 
     * Return List of BigDecimal with values calculated for benchmark for month based on number of working days
     * @param weekWiseDateListForColorcoding
     * @param keyList
     * @param benchmarkValueTobeMultiplied
     * @return List of BigDecimal
     */
    private List<BigDecimal> calculateBenchmarkForMonthBasedOnNoOfWorkingDays(List<Date> weekWiseDateListForColorcoding,
            List<String> keyList, String benchmarkValueTobeMultiplied) {
        
        int numberOfWorkingDaysEvenForColorcoding = 0;
        int numberOfWorkingDaysOddForColorcoding = 1;
        int numberOfWorkingDaysWeekwiseForColorcoding = 0;
        BigDecimal benchmarkValueForColorcodingPerWeek = BigDecimal.ZERO;
        List<BigDecimal> benchmarkValueListForColorcoding = new ArrayList<BigDecimal>();
        List<String> keyListForColorcoding = new ArrayList<String>();
        
        for(String key : keyList){
            if(!GeneraliConstants.COLORCODING_EXEMPTIONS_SET.contains(key)){
                keyListForColorcoding.add(key);
            }
        }

        for (String eachKeyForColorcoding : keyListForColorcoding) {
            numberOfWorkingDaysWeekwiseForColorcoding =
                    getNumberOfWorkingDays(weekWiseDateListForColorcoding.get(numberOfWorkingDaysEvenForColorcoding),
                            weekWiseDateListForColorcoding.get(numberOfWorkingDaysOddForColorcoding));
            benchmarkValueForColorcodingPerWeek = new BigDecimal(numberOfWorkingDaysWeekwiseForColorcoding).multiply(new BigDecimal(benchmarkValueTobeMultiplied));
            benchmarkValueListForColorcoding.add(benchmarkValueForColorcodingPerWeek);
            numberOfWorkingDaysEvenForColorcoding += 2;
            numberOfWorkingDaysOddForColorcoding += 2;
        }
        return benchmarkValueListForColorcoding;
    }

    /**
     * 
     * Return  month wise date for last 3 or 6 months
     * @param startDateRange
     * @param endDateRange
     * @return List<Date> in month wise
     */
    private List<Date> getMonthwiseDateForLastThreeOrSixMonths(Date startDateRange, Date endDateRange) {

        List<Date> dateList = new ArrayList<Date>();
        
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDateRange);
        dateList.add(startCal.getTime());
        
        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDateRange);
        Date endedDate = endCal.getTime();
        
        // Core Logic Starts
        Calendar monthEndCal = startCal; 
        while (endCal.after(monthEndCal)) {
            
            monthEndCal.set(Calendar.DAY_OF_MONTH, monthEndCal.getActualMaximum(Calendar.DAY_OF_MONTH));
            dateList.add(monthEndCal.getTime());
            monthEndCal.add(Calendar.DATE, 1);
            if(monthEndCal.after(endCal)){
                break;
            }
            dateList.add(monthEndCal.getTime());
        }
        return dateList;
    }

    /**
     * 
     * Return week wise date for one specific month
     * @param startDateRange
     * @param endDateRange
     * @return List<Date> in week wise 
     */
    private List<Date> getWeekwiseDateForAMonth(Date startDateRange, Date endDateRange) {

        List<Date> dateList = new ArrayList<Date>();

        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDateRange);

        // Adding first date
        if (startCal.get(7) != 7) {
            dateList.add(startCal.getTime());
        }

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDateRange);
        Date endedDate = endCal.getTime();

        // Core Logic Starts
        Calendar friCal = startCal;
        while (!startCal.after(friCal)) {

            if (friCal.get(7) != 7) {
                friCal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
                if (!friCal.getTime().before(endedDate)) {
                    dateList.add(endedDate);
                    break;
                }
                dateList.add(friCal.getTime());
            }
            if (friCal.get(7) == 7) {
                friCal.add(Calendar.DAY_OF_WEEK, 2);
            } else {
                friCal.add(Calendar.DAY_OF_WEEK, 3);
            }
            startCal = friCal;
            if (startCal.getTime().after(endedDate)) {
                break;
            }
            dateList.add(startCal.getTime());
        }
        return dateList;
    }

    private void setScoringColorMaps(ScoringDataVo scoringDataVo, List<String> keyList, String command, List<Date> dateRangeListForTotPerf,int benchMarkAgentCount) {
		Map<String, String> scoringCallColorMap;
		Map<String, String> scoringVisitColorMap;
		Map<String, String> scoringAppointmentColorMap;
		Map<String, String> scoringNewProspectColorMap;
		Map<String, String> scoringPresentationColorMap;
		Map<String, String> scoringCloseCaseColorMap;
		Map<String, String> scoringTotalColorMap;
		Map<String, String> scoringTotalPercentPerformanceColorMap;
		
		Map<String, String> scoringCallColorMapInPercent;
		Map<String, String> scoringVisitColorMapInPercent;
		Map<String, String> scoringAppointmentColorMapInPercent;
		Map<String, String> scoringPresentationColorMapInPercent;
		Map<String, String> scoringCloseCaseColorMapInPercent;
		Map<String, String> scoringNewProspectColorMapInPercent;
		Map<String, String> scoringTotalColorMapInPercent;
		
		List<Date> weekWiseDateListForColorcoding = new ArrayList<Date>();
        List<BigDecimal> benchmarkValuesListForCall = new ArrayList<BigDecimal>();
        List<BigDecimal> benchmarkValuesListForVisit = new ArrayList<BigDecimal>();
        List<BigDecimal> benchmarkValuesListForAppointment = new ArrayList<BigDecimal>();
        List<BigDecimal> benchmarkValuesListForNewProspect = new ArrayList<BigDecimal>();
        List<BigDecimal> benchmarkValuesListForPresentation = new ArrayList<BigDecimal>();
        List<BigDecimal> benchmarkValuesListForCloseCase = new ArrayList<BigDecimal>();
        
        if(command.equals(GeneraliConstants.CURRENT_WEEK) || command.equals(GeneraliConstants.PREVIOUS_WEEK)){
            
            benchmarkValuesListForCall.add(new BigDecimal(scoringBenchmarkForCall));
            benchmarkValuesListForVisit.add(new BigDecimal(scoringBenchmarkForVisiting));
            benchmarkValuesListForAppointment.add(new BigDecimal(scoringBenchmarkForAppointment)); 
            benchmarkValuesListForNewProspect.add(new BigDecimal(scoringBenchmarkForNewProspect));
            benchmarkValuesListForPresentation.add(new BigDecimal(scoringBenchmarkForPresentation));
            benchmarkValuesListForCloseCase.add(new BigDecimal(scoringBenchmarkForSelling));   
        } else {
            
            if (command.equals(GeneraliConstants.CURRENT_MONTH) || command.equals(GeneraliConstants.PREVIOUS_MONTH)) {
                weekWiseDateListForColorcoding = getWeekwiseDateForAMonth(dateRangeListForTotPerf.get(0), dateRangeListForTotPerf.get(1));
            } else if (command.equals(GeneraliConstants.LAST_THREE_MONTHS) || command.equals(GeneraliConstants.LAST_SIX_MONTHS)) {
                weekWiseDateListForColorcoding = getMonthwiseDateForLastThreeOrSixMonths(dateRangeListForTotPerf.get(0), dateRangeListForTotPerf.get(1));
            }
                benchmarkValuesListForCall = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForCall);
                benchmarkValuesListForVisit = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForVisiting);
                benchmarkValuesListForAppointment = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForAppointment);
                benchmarkValuesListForNewProspect = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForNewProspect);
                benchmarkValuesListForPresentation = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForPresentation);
                benchmarkValuesListForCloseCase = calculateBenchmarkForMonthBasedOnNoOfWorkingDays(
                        weekWiseDateListForColorcoding, keyList, scoringBenchmarkForSelling);
        }
            
		scoringCallColorMap = extractScoringColorDetails(scoringDataVo.getCallCountMap(), keyList, benchmarkValuesListForCall, command,benchMarkAgentCount);
        scoringVisitColorMap = extractScoringColorDetails(scoringDataVo.getVisitCountMap(), keyList, benchmarkValuesListForVisit, command,benchMarkAgentCount);
		scoringAppointmentColorMap = extractScoringColorDetails(scoringDataVo.getAppointmentCountMap(), keyList, benchmarkValuesListForAppointment, command,benchMarkAgentCount);
		scoringNewProspectColorMap = extractScoringColorDetails(scoringDataVo.getNewProspectCountMap(), keyList, benchmarkValuesListForNewProspect, command,benchMarkAgentCount);
		scoringPresentationColorMap = extractScoringColorDetails(scoringDataVo.getPresentationCountMap(), keyList, benchmarkValuesListForPresentation, command,benchMarkAgentCount);
		scoringCloseCaseColorMap = extractScoringColorDetails(scoringDataVo.getCloseCountMap(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
		scoringTotalColorMap = extractScoringColorDetails(scoringDataVo.getTotalCountMap(), keyList, benchmarkValuesListForCloseCase, command,benchMarkAgentCount);
		scoringTotalPercentPerformanceColorMap = extractTotPercentScoringColorDetails(scoringDataVo.getTotalCountMap(),keyList);
		
		scoringCallColorMapInPercent = extractScoringColorDetailsForPercent(scoringDataVo.getCallCountMapInPercent(), keyList);
		scoringVisitColorMapInPercent = extractScoringColorDetailsForPercent(scoringDataVo.getVisitCountMapInPercent(), keyList);
		scoringAppointmentColorMapInPercent = extractScoringColorDetailsForPercent(scoringDataVo.getAppointmentCountMapInPercent(), keyList);
		scoringNewProspectColorMapInPercent = extractScoringColorDetailsForPercent(scoringDataVo.getNewProspectCountInPercent(), keyList);
		scoringPresentationColorMapInPercent = extractScoringColorDetailsForPercent(scoringDataVo.getPresentationCountMapInPercent(), keyList);
		scoringCloseCaseColorMapInPercent = extractScoringColorDetailsForPercent(scoringDataVo.getCloseCountMapInPercent(),keyList);
		scoringTotalColorMapInPercent = extractScoringColorDetailsForPercent(scoringDataVo.getTotalCountInPercent(), keyList);
		
		if(command.equals(GeneraliConstants.CURRENT_WEEK) || command.equals(GeneraliConstants.PREVIOUS_WEEK)) {
			scoringCallColorMap = filterColorWithoutPreviousMonthData(scoringCallColorMap,command);
			scoringVisitColorMap = filterColorWithoutPreviousMonthData(scoringVisitColorMap,command);
			scoringAppointmentColorMap = filterColorWithoutPreviousMonthData(scoringAppointmentColorMap,command);
			scoringNewProspectColorMap = filterColorWithoutPreviousMonthData(scoringNewProspectColorMap,command);
			scoringPresentationColorMap = filterColorWithoutPreviousMonthData(scoringPresentationColorMap,command);
			scoringCloseCaseColorMap = filterColorWithoutPreviousMonthData(scoringCloseCaseColorMap,command);
			scoringTotalColorMap = filterColorWithoutPreviousMonthData(scoringTotalColorMap,command);
			scoringTotalPercentPerformanceColorMap = filterColorWithoutPreviousMonthData(scoringTotalPercentPerformanceColorMap,command);
			
			scoringCallColorMapInPercent = filterColorWithoutPreviousMonthData(scoringCallColorMapInPercent,command);
			scoringVisitColorMapInPercent = filterColorWithoutPreviousMonthData(scoringVisitColorMapInPercent,command);
			scoringAppointmentColorMapInPercent = filterColorWithoutPreviousMonthData(scoringAppointmentColorMapInPercent,command);
			scoringNewProspectColorMapInPercent = filterColorWithoutPreviousMonthData(scoringNewProspectColorMapInPercent,command);
			scoringPresentationColorMapInPercent = filterColorWithoutPreviousMonthData(scoringPresentationColorMapInPercent,command);
			scoringCloseCaseColorMapInPercent = filterColorWithoutPreviousMonthData(scoringCloseCaseColorMapInPercent,command);
			scoringTotalColorMapInPercent = filterColorWithoutPreviousMonthData(scoringTotalColorMapInPercent,command);
		}
		
		scoringDataVo.setScoringCallColorMap(scoringCallColorMap);
		scoringDataVo.setScoringAppointmentColorMap(scoringAppointmentColorMap);
		scoringDataVo.setScoringVisitColorMap(scoringVisitColorMap);
		scoringDataVo.setScoringNewProspectColorMap(scoringNewProspectColorMap);
		scoringDataVo.setScoringTotalColorMap(scoringTotalColorMap);
		scoringDataVo.setScoringPresentationColorMap(scoringPresentationColorMap);
		scoringDataVo.setScoringCloseCaseColorMap(scoringCloseCaseColorMap);
		scoringDataVo.setScoringTotalPercentPerformanceColorMap(scoringTotalPercentPerformanceColorMap);
		
		scoringDataVo.setScoringCallColorMapInPercent(scoringCallColorMapInPercent);
		scoringDataVo.setScoringAppointmentColorMapInPercent(scoringAppointmentColorMapInPercent);
		scoringDataVo.setScoringVisitColorMapInPercent(scoringVisitColorMapInPercent);
		scoringDataVo.setScoringNewProspectColorMapInPercent(scoringNewProspectColorMapInPercent);
		scoringDataVo.setScoringPresentationColorMapInPercent(scoringPresentationColorMapInPercent);
		scoringDataVo.setScoringCloseCaseColorMapInPercent(scoringCloseCaseColorMapInPercent);
		scoringDataVo.setScoringTotalColorMapInPercent(scoringTotalColorMapInPercent);
	}

	private void getTotalMap(Map<String, Integer> totalMap, SalesActivityDataVo salesActivityVo) {
		int totalCount = 0;
		BigDecimal totalActivityCount = BigDecimal.ZERO;
		BigDecimal benchmarkCount = BigDecimal.ONE;
		BigDecimal percentAchieveTotalCount = BigDecimal.ZERO;
		Set<String> keySet = salesActivityVo.getMonthwiseCallCount().keySet();
		List<String> keyList = new ArrayList<String>(keySet);
		for(int i=0;i<=keyList.size()-1;i++){
			
			int callCnt = 0;
			int visitCnt = 0;
			int appntCnt = 0;
			int newProsCnt = 0;
            int presentationCount = 0;
            int closeCaseCount = 0;
			
			if(salesActivityVo.getMonthwiseCallCount().get(keyList.get(i)) > 0){
				callCnt = salesActivityVo.getMonthwiseCallCount().get(keyList.get(i));
			}
			if(salesActivityVo.getMonthwiseVisitCount().get(keyList.get(i)) > 0){
				visitCnt = salesActivityVo.getMonthwiseVisitCount().get(keyList.get(i));
			}
			if(salesActivityVo.getMonthwiseAppointmentCount().get(keyList.get(i)) > 0){
				appntCnt = salesActivityVo.getMonthwiseAppointmentCount().get(keyList.get(i));
			}
			
			if(salesActivityVo.getNewProspectDetails().get(keyList.get(i)) > 0){
			    newProsCnt = salesActivityVo.getNewProspectDetails().get(keyList.get(i));
		    }
			
			if(salesActivityVo.getMonthwisePresentationCount().get(keyList.get(i)) > 0){
				presentationCount = salesActivityVo.getMonthwisePresentationCount().get(keyList.get(i));
		    }
			
			if(salesActivityVo.getMonthwiseCloseCount().get(keyList.get(i)) > 0){
                closeCaseCount = salesActivityVo.getMonthwiseCloseCount().get(keyList.get(i));
            }
		    
		    totalCount = callCnt + visitCnt+ appntCnt + newProsCnt + presentationCount + closeCaseCount;
		    
		    if(keyList.get(i).equals(GeneraliConstants.TOTAL_ACTIVITY)){
				totalActivityCount = new BigDecimal(totalCount);
			}
			
			if(keyList.get(i).equals(GeneraliConstants.BENCHMARK)){
				benchmarkCount = new BigDecimal(totalCount);
			}
			
			if(keyList.get(i).equals(GeneraliConstants.PERCENT_ACHIEVE)){
				percentAchieveTotalCount = ((totalActivityCount.divide(benchmarkCount, 2, RoundingMode.HALF_UP)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED)).setScale(0, RoundingMode.HALF_UP);
				totalCount =  (int) Double.parseDouble(percentAchieveTotalCount.toString());
			}
			
			totalMap.put(keyList.get(i), totalCount);
		}
	}
	
	private Map<String, String> getScoringTotalMap(ScoringDataVo scoringDataVo) {
		int totalCount = 0;
		BigDecimal totalActivityCount = BigDecimal.ZERO;
		BigDecimal benchmarkCount = BigDecimal.ONE;
		BigDecimal percentAchieveTotalCount = BigDecimal.ZERO;
		Set<String> keySet = scoringDataVo.getCallCountMap().keySet();
		List<String> keyList = new ArrayList<String>(keySet);
		Map<String, String> totalMap = new LinkedHashMap<String,String>();
		for(int i=0;i<=keyList.size()-1;i++){
			
			float callCnt = 0;
			float visitCnt = 0;
			float appntCnt = 0;
			float newProsCnt = 0;
			float presentationCount = 0;
			float closeCaseCount = 0;
			
			if(Float.parseFloat(scoringDataVo.getCallCountMap().get(keyList.get(i))) > 0){
				callCnt = Float.parseFloat(scoringDataVo.getCallCountMap().get(keyList.get(i)));
			}
			if(Float.parseFloat(scoringDataVo.getVisitCountMap().get(keyList.get(i))) > 0){
				visitCnt = Float.parseFloat(scoringDataVo.getVisitCountMap().get(keyList.get(i)));
			}
			if(Float.parseFloat(scoringDataVo.getAppointmentCountMap().get(keyList.get(i))) > 0){
				appntCnt = Float.parseFloat(scoringDataVo.getAppointmentCountMap().get(keyList.get(i)));
			}
			
			if(Float.parseFloat(scoringDataVo.getNewProspectCountMap().get(keyList.get(i))) > 0){
			    newProsCnt = Float.parseFloat(scoringDataVo.getNewProspectCountMap().get(keyList.get(i)));
		    }
			
			if(Float.parseFloat(scoringDataVo.getPresentationCountMap().get(keyList.get(i))) > 0){
                presentationCount = Float.parseFloat(scoringDataVo.getPresentationCountMap().get(keyList.get(i)));
			}
          

			if(Float.parseFloat(scoringDataVo.getCloseCountMap().get(keyList.get(i))) > 0){
              closeCaseCount = Float.parseFloat(scoringDataVo.getCloseCountMap().get(keyList.get(i)));
			}
       
			totalCount = (int) (callCnt + visitCnt+ appntCnt + newProsCnt+presentationCount+closeCaseCount);
			
		    if(keyList.get(i).equals(GeneraliConstants.TOTAL_ACTIVITY)){
				totalActivityCount = new BigDecimal(totalCount);
			}
			
			if(keyList.get(i).equals(GeneraliConstants.BENCHMARK)){
				benchmarkCount = new BigDecimal(totalCount);
			}
			
			if(keyList.get(i).equals(GeneraliConstants.PERCENT_ACHIEVE)){
				percentAchieveTotalCount = ((totalActivityCount.divide(benchmarkCount, 2, RoundingMode.HALF_UP)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED)).setScale(0, RoundingMode.HALF_UP);
				totalCount =  (int) Double.parseDouble(percentAchieveTotalCount.toString());
			}
			
			totalMap.put(keyList.get(i), Integer.toString(totalCount));
		}
		
		return totalMap;
	}
	
	
	private Map<String, String> getScoringTotalMapInPercent(ScoringDataVo scoringDataVo) {
		float totalCount = 0;
		BigDecimal totalActivityCount = BigDecimal.ZERO;
		BigDecimal benchmarkCount = BigDecimal.ONE;
		BigDecimal percentAchieveTotalCount = BigDecimal.ZERO;
		Set<String> keySet = scoringDataVo.getCallCountMapInPercent().keySet();
		List<String> keyList = new ArrayList<String>(keySet);
		Map<String, String> totalMap = new LinkedHashMap<String,String>();
		for(int i=0;i<=keyList.size()-1;i++){
			
			float callCnt = 0;
			float visitCnt = 0;
			float appntCnt = 0;
			float newProsCnt = 0;
			float presentationCount = 0;
			float closeCaseCount = 0;
			
			if(Float.parseFloat(scoringDataVo.getCallCountMapInPercent().get(keyList.get(i))) > 0){
				callCnt = Float.parseFloat(scoringDataVo.getCallCountMapInPercent().get(keyList.get(i)));
			}
			if(Float.parseFloat(scoringDataVo.getVisitCountMapInPercent().get(keyList.get(i))) > 0){
				visitCnt = Float.parseFloat(scoringDataVo.getVisitCountMapInPercent().get(keyList.get(i)));
			}
			if(Float.parseFloat(scoringDataVo.getAppointmentCountMapInPercent().get(keyList.get(i))) > 0){
				appntCnt = Float.parseFloat(scoringDataVo.getAppointmentCountMapInPercent().get(keyList.get(i)));
			}
			
			if(Float.parseFloat(scoringDataVo.getNewProspectCountInPercent().get(keyList.get(i))) > 0){
			    newProsCnt = Float.parseFloat(scoringDataVo.getNewProspectCountInPercent().get(keyList.get(i)));
		    }
			if(keyList.get(i).equals(GeneraliConstants.BENCHMARK)){
			    presentationCount = Float.parseFloat(scoringDataVo.getPresentationCountMapInPercent().get(keyList.get(i)));
			    closeCaseCount = Float.parseFloat(scoringDataVo.getCloseCountMapInPercent().get(keyList.get(i)));
			}
		    
		    totalCount = callCnt + visitCnt+ appntCnt + newProsCnt + presentationCount + closeCaseCount;
		    
		    if(keyList.get(i).equals(GeneraliConstants.TOTAL_ACTIVITY)){
				totalActivityCount = new BigDecimal(totalCount);
			}
			
			if(keyList.get(i).equals(GeneraliConstants.BENCHMARK)){
				benchmarkCount = new BigDecimal(totalCount);
			}
			
			if(keyList.get(i).equals(GeneraliConstants.PERCENT_ACHIEVE)){
				percentAchieveTotalCount = ((totalActivityCount.divide(benchmarkCount, 2, RoundingMode.HALF_UP)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED)).setScale(0, RoundingMode.HALF_UP);
				totalCount =  (int) Double.parseDouble(percentAchieveTotalCount.toString());
			}
			
			totalMap.put(keyList.get(i), Float.toString(totalCount));
		}
		
		return totalMap;
	}

    /**
     * Points to percentage conversion
     * 
     * @param columnCountForScoring
     * @param benchmarkValueListForScoring 
     * @param command
     * @return Map<String, String>
     */
    private Map<String, String> calculatePointstoPercentageConversion(Map<String, String> columnCountForScoring,
            List<BigDecimal> benchmarkValueListForScoring, String command) {
        
        Map<String, String> eachColumncount = new LinkedHashMap<String, String>();
        Set<String> keySet = columnCountForScoring.keySet();
        List<String> keyList = new ArrayList<String>(keySet);
        
        for (int i = 0; i < columnCountForScoring.size() - 1; i++) {
            if(!GeneraliConstants.COLORCODING_EXEMPTIONS_SET.contains(keyList.get(i))){
                BigDecimal eachCountValue = BigDecimal.ZERO;
                if (columnCountForScoring.get(keyList.get(i)).equals(GeneraliConstants.MINUS_ONE_VALUE)) {
                    eachCountValue = new BigDecimal(-1);
                } else {
                    if(command.equals(GeneraliConstants.CURRENT_WEEK) || command.equals(GeneraliConstants.PREVIOUS_WEEK)){
                        eachCountValue = ((new BigDecimal(columnCountForScoring.get(keyList.get(i))).divide(benchmarkValueListForScoring.get(0), 2, RoundingMode.HALF_UP)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED)).setScale(0, RoundingMode.HALF_UP);
                    } else {
                        eachCountValue = ((new BigDecimal(columnCountForScoring.get(keyList.get(i))).divide(benchmarkValueListForScoring.get(i), 2, RoundingMode.HALF_UP)).multiply(GeneraliConstants.PERCENTAGE_HUNDRED)).setScale(0, RoundingMode.HALF_UP);
                    }
                }
                eachColumncount.put(keyList.get(i), eachCountValue.toString());
            }
        }
        return eachColumncount;
    }

    /**
     * Method to return previous week/month data used for Pre. Week/Pre. Month Activity
     * 
     * @param weekDataCount
     * @param totalValue
     * @param command TODO
     * @return totalValue for each row
     */
    private int getTotalCountValue(Map<String, Integer> weekDataCount, int totalValue, String command) {
    	int index = 0;
        for (Map.Entry<String, Integer> weekDataSet : weekDataCount.entrySet()) {
        	if(weekDataSet.getValue() != -1 && !command.equals(GeneraliConstants.CURRENT_WEEK) && !command.equals(GeneraliConstants.PREVIOUS_WEEK)) {
        		totalValue = totalValue + weekDataSet.getValue();
        	}
        	else if(isWeekDayCurrentMonth(command,index) && weekDataSet.getValue() != -1) {
        			totalValue = totalValue + weekDataSet.getValue();
        	}
        	index++;
        }
        return totalValue;
    }
    
	private Boolean isWeekDayCurrentMonth(String command, int index) {
		// TODO Auto-generated method stub
		List<Date> dateRange = getDateRange(command);
		Date start = dateRange.get(0);
		Calendar startDate = Calendar.getInstance();
		startDate.setTime(start);
		Date today = new Date();
		Calendar todayCal = Calendar.getInstance();
		todayCal.setTime(today);
		Boolean flag = false;
		Boolean twoMonthsInAWeekFlag = false;
		int greatestMonth = 0;

		if (startDate.get(7) == 7) {
			startDate.add(Calendar.DAY_OF_WEEK, 2);
		}

		else if (startDate.get(7) == 1) {
			startDate.add(Calendar.DAY_OF_WEEK, 1);
		}

		else {
			startDate.set(Calendar.DAY_OF_WEEK, startDate.getActualMinimum(Calendar.DAY_OF_WEEK));
			startDate.add(Calendar.DAY_OF_WEEK, 1);
		}

		for (int i = 0; i < 5; i++) {
			if (command.equals(GeneraliConstants.PREVIOUS_WEEK)) {
				if (i == 0) {
					greatestMonth = startDate.get(2);
				}
				if (startDate.get(2) > greatestMonth) {
					greatestMonth = startDate.get(2);
					twoMonthsInAWeekFlag = true;
				}
			}
			startDate.add(Calendar.DATE, 1);
		}
		startDate.add(Calendar.DATE, -5);
		for (int i = 0; i < 5; i++) {
			if ((i == index && command.equals(GeneraliConstants.CURRENT_WEEK) && startDate.get(2) == todayCal.get(2))
					|| (i == index && !twoMonthsInAWeekFlag && command.equals(GeneraliConstants.PREVIOUS_WEEK))
					|| ((i == index && twoMonthsInAWeekFlag && startDate.get(2) == greatestMonth) && command.equals(GeneraliConstants.PREVIOUS_WEEK))) {
				flag = true;
			}
			startDate.add(Calendar.DATE, 1);
		}
		return flag;
	}

    private List<BigDecimal> getColumnListForTotalPercentagePerformance(Map<String, String> columnCountForScoring) {
        BigDecimal eachColumnValue = BigDecimal.ZERO;
        List<BigDecimal> columnListForTotPercentPerformance = new ArrayList<BigDecimal>();
        for(Map.Entry<String,String> eachColumnCount:columnCountForScoring.entrySet()){
            if (!eachColumnCount.getKey().equals(GeneraliConstants.TOTAL_POINTS) && !eachColumnCount.getKey().equals(GeneraliConstants.PERCENT_ACHIEVE)
                    && !eachColumnCount.getKey().equals(GeneraliConstants.BENCHMARK)) {
                eachColumnValue = new BigDecimal(eachColumnCount.getValue());
                columnListForTotPercentPerformance.add(eachColumnValue);
            }
        }
        return columnListForTotPercentPerformance;
    }

    /**
     * Method to calculate points for scoring for all modules including total points column 
     * 
     * @param monthwiseCount
     * @param pointsTobeMultiplied
     * @return
     */
    private Map<String, String> calculatePointsForScoring(Map<String, Integer> monthwiseCount, String pointsTobeMultiplied, BigDecimal benchmarkValuesListForScoring) {
        Map<String, String> pointsForScoring = new LinkedHashMap<String, String>();
        BigDecimal totalPoints = BigDecimal.ZERO;
        BigDecimal percentageAchieve = BigDecimal.ZERO;
        
        for (Map.Entry<String, Integer> eachMonthCount : monthwiseCount.entrySet()) {
            BigDecimal eachMonthValue = BigDecimal.ZERO;
            BigDecimal monthValue = BigDecimal.ZERO;
            
            if(eachMonthCount.getValue() == -1){
                monthValue = new BigDecimal(eachMonthCount.getValue());
            } else {
                eachMonthValue = new BigDecimal(eachMonthCount.getValue());
                monthValue = eachMonthValue.multiply(new BigDecimal(pointsTobeMultiplied)).setScale(0, RoundingMode.HALF_UP);
            }
                if (!eachMonthCount.getKey().equals(GeneraliConstants.PRE_WEEKLY) && !eachMonthCount.getKey().equals(GeneraliConstants.PRE_MONTHLY) 
                        && !eachMonthCount.getKey().equals(GeneraliConstants.BENCHMARK) && !eachMonthCount.getKey().equals(GeneraliConstants.PERCENT_ACHIEVE)) {
                    if(eachMonthCount.getKey().equals(GeneraliConstants.TOTAL_ACTIVITY)){
                        pointsForScoring.put(GeneraliConstants.TOTAL_POINTS, monthValue.toString());
                        totalPoints = monthValue;
                    } else {
                        pointsForScoring.put(eachMonthCount.getKey(), monthValue.toString());
                    }
                }
        }
        pointsForScoring.put(GeneraliConstants.BENCHMARK, benchmarkValuesListForScoring.setScale(0, RoundingMode.HALF_UP).toString());
        percentageAchieve = ((totalPoints.multiply(GeneraliConstants.PERCENTAGE_HUNDRED)).divide(benchmarkValuesListForScoring, 2, RoundingMode.HALF_UP)).setScale(0, RoundingMode.HALF_UP);
        pointsForScoring.put(GeneraliConstants.PERCENT_ACHIEVE, percentageAchieve.toString());
        
        return pointsForScoring;
    }

    /*private Map<String, Integer> getCloseCaseDetails(RequestInfo requestInfo, List<String> agentCodesList,
            SalesActivityResponseDataVo salesActivityResponseDataVo, String command) {
        Map<String, Integer> closeCaseWeekCount = new HashMap<String, Integer>();
        List<Transactions> transactionsList;
        SearchCriteria searchCriteria = new SearchCriteria();
        ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
        searchCriteria.setSelectedIds(selectedID);
        List<Date> dateRange = getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.CLOSE_CASE);
        transactionsList = eAppRepository.retrieveMonthlyCloseData(requestInfo, searchCriteria);

        int closeCaseWeek1 = 0;
        int closeCaseWeek2 = 0;
        int closeCaseWeek3 = 0;
        int closeCaseWeek4 = 0;
        int closeCaseWeek5 = 0;
        if (transactionsList != null) {
            for (Transactions transaction : transactionsList) {
                Date creationDate = transaction.getProposal().getCreationDateTime();
                Calendar c = Calendar.getInstance();
                c.setTime(creationDate);
                
                int weekNumber = c.get(4);
                switch(weekNumber){
                    case 1:
                        closeCaseWeek1++;
                        closeCaseWeekCount.put("week1", closeCaseWeek1);
                        break;
                    case 2:
                        closeCaseWeek2++;
                        closeCaseWeekCount.put("week2", closeCaseWeek2);
                        break;
                    case 3:
                        closeCaseWeek3++;
                        closeCaseWeekCount.put("week3", closeCaseWeek3);
                        break;
                    case 4:
                        closeCaseWeek4++;
                        closeCaseWeekCount.put("week4", closeCaseWeek4);
                        break;
                    case 5:
                        closeCaseWeek5++;
                        closeCaseWeekCount.put("week5", closeCaseWeek5);
                        break;
                    }
            }
        }

        return closeCaseWeekCount;
    }

    private Map<String, Integer> getPresentationDetails(RequestInfo requestInfo, List<String> agentCodesList,
            SalesActivityResponseDataVo salesActivityResponseDataVo, String command) {
        Map<String, Integer> presentationWeekCount = new HashMap<String, Integer>();
        List<Transactions> transactionsList;
        SearchCriteria searchCriteria = new SearchCriteria();
        ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
        searchCriteria.setSelectedIds(selectedID);
        List<Date> dateRange = getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.PRESENTATION);
        transactionsList = illustrationRepository.retrieveMonthlyPresentationData(requestInfo, searchCriteria);
        int presentationWeek1 = 0;
        int presentationWeek2 = 0;
        int presentationWeek3 = 0;
        int presentationWeek4 = 0;
        int presentationWeek5 = 0;
        if (transactionsList != null) {
            for (Transactions transaction : transactionsList) {
                Date creationDate = transaction.getProposal().getCreationDateTime();
                Calendar c = Calendar.getInstance();
                c.setTime(creationDate);
                
                int weekNumber = c.get(4);
                switch(weekNumber){
                    case 1:
                        presentationWeek1++;
                        presentationWeekCount.put("week1", presentationWeek1);
                        break;
                    case 2:
                        presentationWeek2++;
                        presentationWeekCount.put("week2", presentationWeek2);
                        break;
                    case 3:
                        presentationWeek3++;
                        presentationWeekCount.put("week3", presentationWeek3);
                        break;
                    case 4:
                        presentationWeek4++;
                        presentationWeekCount.put("week4", presentationWeek4);
                        break;
                    case 5:
                        presentationWeek5++;
                        presentationWeekCount.put("week5", presentationWeek5);
                        break;
                    }
            }
        }

        return presentationWeekCount;
    }*/

    private Map<String, Integer> getWeeklyCloseDetails(RequestInfo requestInfo, List<String> agentCodesList,
            SalesActivityResponseDataVo salesActivityResponseDataVo, String command) {
        SearchCriteria searchCriteria = new SearchCriteria();
        ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
        searchCriteria.setSelectedIds(selectedID);
        List<Date> dateRange = getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.CLOSE_CASE);
        Map<String, Integer> weeklyCloseCaseCount = new HashMap<String, Integer>();
        weeklyCloseCaseCount = retrieveWeekwiseSalesActivityDataCloseCase(requestInfo, searchCriteria);
        return weeklyCloseCaseCount;
    }

   

    private Map<String, Integer> getWeeklyPresentationDetails(RequestInfo requestInfo, List<String> agentCodesList,
            SalesActivityResponseDataVo salesActivityResponseDataVo, String command) {
        SearchCriteria searchCriteria = new SearchCriteria();

        ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
        searchCriteria.setSelectedIds(selectedID);
        List<Date> dateRange = getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.PRESENTATION);
        Map<String, Integer> weeklyPresentationCount = new HashMap<String, Integer>();
        weeklyPresentationCount = retrieveWeekwiseSalesActivityDataForPresentation(requestInfo, searchCriteria);
        return weeklyPresentationCount;
    }

   
    private Map<String, Integer> getMonthwiseCloseCaseDetails(RequestInfo requestInfo, List<String> agentCodesList,
            SalesActivityResponseDataVo salesActivityResponseDataVo) {
        SearchCriteria searchCriteria = new SearchCriteria();
        ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
        searchCriteria.setSelectedIds(selectedID);
        List<Date> dateRange= getDateRange(salesActivityResponseDataVo.getCommand());
        Date startDate = null ;
        Date endDate = null ;
        if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_THREE_MONTHS)){
            startDate = dateRange.get(4);
            endDate = dateRange.get(1);
        }
        else if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_SIX_MONTHS)){
            startDate = dateRange.get(10);
            endDate = dateRange.get(1);
        }
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.CLOSE_CASE);
        searchCriteria.setCommand(salesActivityResponseDataVo.getCommand());
        Map<String,Integer> monthwiseCloseCaseCount = new HashMap<String,Integer>();
        monthwiseCloseCaseCount = retrieveMonthwiseSalesActivityDataForCloseCase(requestInfo, searchCriteria);
        return monthwiseCloseCaseCount;
    }

    private Map<String, Integer> retrieveWeekwiseSalesActivityDataCloseCase(RequestInfo requestInfo,
            SearchCriteria searchCriteria) {

        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
        Map<String, Integer> weeklyCloseCount = new HashMap<String, Integer>();
        weeklyCloseCount = eAppRepository.retrieveWeeklyCount(requestInfo, searchCriteria);
        Integer count = 0;
       Integer mondayCount =0;
       Integer fridayCount =0;
       String displayDateFormat = "EEE dd";
       Date day = new Date();
          Date today = new Date();
       DateFormat df = new SimpleDateFormat(displayDateFormat);
       Calendar c = Calendar.getInstance();
       if (searchCriteria.getStartDate() != null) {
              c.setTime(searchCriteria.getStartDate());
       }
              if(c.get(7) == 7){
                     c.add(Calendar.DAY_OF_WEEK,2);
              }
              
              else if(c.get(7) == 1){
                     c.add(Calendar.DAY_OF_WEEK,1);
              }
              
              else {
                     c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK));
                     c.add(Calendar.DAY_OF_WEEK,1);
              }
              
              String week = df.format(c.getTime());
              LinkedHashMap<String,Integer> finalMap = new LinkedHashMap<String,Integer>();
              //Generate Monday to Friday
              for(int i = 1; i<=5;i++) {
                     day = c.getTime();
                     week = df.format(day);
					 finalMap.put(week, 0);
                     Iterator<Map.Entry<String,Integer>> iter = weeklyCloseCount.entrySet().iterator();
                     while (iter.hasNext()) {
                         Map.Entry<String,Integer> entry = iter.next();
                         String date = entry.getKey();
                           count = entry.getValue();
                           DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                           try {
                                  week = df.format(day);
                                  Date dateValue = formatter.parse(date);
                                  Calendar dateVal = Calendar.getInstance();
                                  dateVal.setTime(dateValue);
                                  //Saturday or Sunday
                                  if (dateVal.get(7) == 1 || dateVal.get(7) == 7) {
                                         if (c.getTime().before(dateValue)) {
                                                fridayCount  += count;
                                                iter.remove();
                                         } else if (c.getTime().after(dateValue)) {
                                                mondayCount += count;
                                                iter.remove();
                                         }
                                  } else if (formatter.format(dateValue).equals(formatter.format(c.getTime()))) {
                                         finalMap.put(week, count);
                                         iter.remove();
                                  }
                           } catch (ParseException e) {
                                  e.printStackTrace();
                           }
                     }
                     //Future dates return -1
                     if(day.after(today)) {
                           finalMap.put(week, -1);
                     }
                     c.add(Calendar.DAY_OF_WEEK,1);
              }
              if(finalMap.entrySet().iterator().hasNext())   {
                     Map.Entry<String,Integer> entry = finalMap.entrySet().iterator().next();
                     String firstkey = entry.getKey();
                     Integer firstvalue =entry.getValue();
                     firstvalue += mondayCount;
                     finalMap.put(firstkey, firstvalue);
                     
                     List<Entry<String,Integer>> entryList = new ArrayList<Map.Entry<String, Integer>>(finalMap.entrySet());
                     Entry<String, Integer> lastEntry = entryList.get(entryList.size()-1);
                     String lastkey = lastEntry.getKey();
                     Integer lastvalue = lastEntry.getValue();
                     if(lastvalue != -1) {
                           lastvalue += fridayCount;
                           finalMap.put(lastkey, lastvalue);
                     }
              }

       
        return finalMap;
    
    }

    private Map<String, Integer> retrieveWeekwiseSalesActivityDataForPresentation(RequestInfo requestInfo,
            SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
       Map<String, Integer> weeklyPresentationCount = new HashMap<String, Integer>();
        weeklyPresentationCount = illustrationRepository.retrieveWeeklyCount(requestInfo, searchCriteria);
        Integer count = 0;
       Integer mondayCount =0;
       Integer fridayCount =0;
       String displayDateFormat = "EEE dd";
       Date day = new Date();
          Date today = new Date();
       DateFormat df = new SimpleDateFormat(displayDateFormat);
       Calendar c = Calendar.getInstance();
       if (searchCriteria.getStartDate() != null) {
              c.setTime(searchCriteria.getStartDate());
       }
              if(c.get(7) == 7){
                     c.add(Calendar.DAY_OF_WEEK,2);
              }
              
              else if(c.get(7) == 1){
                     c.add(Calendar.DAY_OF_WEEK,1);
              }
              
              else {
                     c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK));
                     c.add(Calendar.DAY_OF_WEEK,1);
              }
              
              String week = df.format(c.getTime());
              LinkedHashMap<String,Integer> finalMap = new LinkedHashMap<String,Integer>();
              //Generate Monday to Friday
              for(int i = 1; i<=5;i++) {
                     day = c.getTime();
                     week = df.format(day);
					 finalMap.put(week, 0);
                     Iterator<Map.Entry<String,Integer>> iter = weeklyPresentationCount.entrySet().iterator();
                     while (iter.hasNext()) {
                         Map.Entry<String,Integer> entry = iter.next();
                         String date = entry.getKey();
                           count = entry.getValue();
                           DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                           try {
                                  week = df.format(day);
                                  Date dateValue = formatter.parse(date);
                                  Calendar dateVal = Calendar.getInstance();
                                  dateVal.setTime(dateValue);
                                  //Saturday or Sunday
                                  if (dateVal.get(7) == 1 || dateVal.get(7) == 7) {
                                         if (c.getTime().before(dateValue)) {
                                                fridayCount  += count;
                                                iter.remove();
                                         } else if (c.getTime().after(dateValue)) {
                                                mondayCount += count;
                                                iter.remove();
                                         }
                                  } else if (formatter.format(dateValue).equals(formatter.format(c.getTime()))) {
                                         finalMap.put(week, count);
                                         iter.remove();
                                  }
                           } catch (ParseException e) {
                                  e.printStackTrace();
                           }
                     }
                     //Future dates return -1
                     if(day.after(today)) {
                           finalMap.put(week, -1);
                     }
                     c.add(Calendar.DAY_OF_WEEK,1);
              }
              if(finalMap.entrySet().iterator().hasNext())   {
                     Map.Entry<String,Integer> entry = finalMap.entrySet().iterator().next();
                     String firstkey = entry.getKey();
                     Integer firstvalue =entry.getValue();
                     firstvalue += mondayCount;
                     finalMap.put(firstkey, firstvalue);
                     
                     List<Entry<String,Integer>> entryList = new ArrayList<Map.Entry<String, Integer>>(finalMap.entrySet());
                     Entry<String, Integer> lastEntry = entryList.get(entryList.size()-1);
                     String lastkey = lastEntry.getKey();
                     Integer lastvalue = lastEntry.getValue();
                     if(lastvalue != -1) {
                           lastvalue += fridayCount;
                           finalMap.put(lastkey, lastvalue);
                     }
              }

       
        return finalMap;
    }

    private Map<String, Integer> retrieveMonthwiseSalesActivityDataForCloseCase(RequestInfo requestInfo,
            SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
        Map<String, Integer> monthwiseCloseCount = new HashMap<String, Integer>();
        monthwiseCloseCount = eAppRepository.retrieveMonthwiseCount(requestInfo, searchCriteria);
		Calendar c = Calendar.getInstance();
		LinkedHashMap<String, Integer> countMap = new LinkedHashMap<String, Integer>();
		String week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
		Integer count = 0;
		if (searchCriteria.getCommand().equals(GeneraliConstants.LAST_THREE_MONTHS)) {
			for (int i = 1; i <= 3; i++) {
				c.add(Calendar.MONTH, -1);
				week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
				count = monthwiseCloseCount.get(Integer.toString(c.get(2) + 1));
				if (count == null) {
					count = 0;
				}
				countMap.put(week, count);
			}
		} else if (searchCriteria.getCommand().equals(GeneraliConstants.LAST_SIX_MONTHS)) {
			for (int j = 1; j <= 6; j++) {
				c.add(Calendar.MONTH, -1);
				week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
				count = monthwiseCloseCount.get(Integer.toString(c.get(2) + 1));
				if (count == null) {
					count = 0;
				}
				countMap.put(week, count);
			}
		}
		Map<String,Integer> resultMap = new LinkedHashMap<String,Integer>();
		Set<String> keySet = countMap.keySet();
		List<String> keyList = new ArrayList<String>(keySet);
		for(int i=keyList.size()-1 ; i>=0 ;i--){
			resultMap.put(keyList.get(i), countMap.get(keyList.get(i)));
		}
		
		return resultMap;
    }

    private Map<String, Integer> retrieveMonthwiseSalesActivityDataForPresentation(RequestInfo requestInfo,
            SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
       Map<String, Integer> monthwisePresentationCount = new HashMap<String, Integer>();
        monthwisePresentationCount = illustrationRepository.retrieveMonthwiseCount(requestInfo, searchCriteria);
		Calendar c = Calendar.getInstance();
		LinkedHashMap<String, Integer> countMap = new LinkedHashMap<String, Integer>();
		String week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
		Integer count = 0;
		if (searchCriteria.getCommand().equals(GeneraliConstants.LAST_THREE_MONTHS)) {
			for (int i = 1; i <= 3; i++) {
				c.add(Calendar.MONTH, -1);
				week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
				count = monthwisePresentationCount.get(Integer.toString(c.get(2) + 1));
				if (count == null) {
					count = 0;
				}
				countMap.put(week, count);
			}
		} else if (searchCriteria.getCommand().equals(GeneraliConstants.LAST_SIX_MONTHS)) {
			for (int j = 1; j <= 6; j++) {
				c.add(Calendar.MONTH, -1);
				week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
				count = monthwisePresentationCount.get(Integer.toString(c.get(2) + 1));
				if (count == null) {
					count = 0;
				}
				countMap.put(week, count);
			}
		}
		Map<String,Integer> resultMap = new LinkedHashMap<String,Integer>();
		Set<String> keySet = countMap.keySet();
		List<String> keyList = new ArrayList<String>(keySet);
		for(int i=keyList.size()-1 ; i>=0 ;i--){
			resultMap.put(keyList.get(i), countMap.get(keyList.get(i)));
		}
		
		return resultMap;
	}

    private Map<String, Integer> getMonthwisePresentationDetails(RequestInfo requestInfo, List<String> agentCodesList,
            SalesActivityResponseDataVo salesActivityResponseDataVo) {
        SearchCriteria searchCriteria = new SearchCriteria();
        ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
        searchCriteria.setSelectedIds(selectedID);
        List<Date> dateRange= getDateRange(salesActivityResponseDataVo.getCommand());
        Date startDate = null ;
        Date endDate = null ;
        if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_THREE_MONTHS)){
            startDate = dateRange.get(4);
            endDate = dateRange.get(1);
        }
        else if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_SIX_MONTHS)){
            startDate = dateRange.get(10);
            endDate = dateRange.get(1);
        }
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.PRESENTATION);
        searchCriteria.setCommand(salesActivityResponseDataVo.getCommand());
        Map<String,Integer> monthwisePresentationCount = new HashMap<String,Integer>();
        monthwisePresentationCount = retrieveMonthwiseSalesActivityDataForPresentation(requestInfo, searchCriteria);
        return monthwisePresentationCount;
        
    }

	private Map<String,Integer> getMonthwiseVisitDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo) {
    	SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(salesActivityResponseDataVo.getCommand());
		Date startDate = null ;
        Date endDate = null ;
        if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_THREE_MONTHS)){
        	startDate = dateRange.get(4);
        	endDate = dateRange.get(1);
        }
        else if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_SIX_MONTHS)){
        	startDate = dateRange.get(10);
        	endDate = dateRange.get(1);
        }
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.VISIT);
		searchCriteria.setCommand(salesActivityResponseDataVo.getCommand());
        Map<String,Integer> monthwiseVisitCount = new HashMap<String,Integer>();
        monthwiseVisitCount = retrieveMonthwiseSalesActivityData(requestInfo, searchCriteria);
        return monthwiseVisitCount;
		
	}

	private Map<String,Integer> getMonthwiseCallDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo) {
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(salesActivityResponseDataVo.getCommand());
        Date startDate = null ;
        Date endDate = null ;
        if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_THREE_MONTHS)){
        	startDate = dateRange.get(4);
        	endDate = dateRange.get(1);
        }
        else if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_SIX_MONTHS)){
        	startDate = dateRange.get(10);
        	endDate = dateRange.get(1);
        }
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.CALL);
		searchCriteria.setCommand(salesActivityResponseDataVo.getCommand());
        Map<String,Integer> monthwiseCallCount = new HashMap<String,Integer>();
        monthwiseCallCount = retrieveMonthwiseSalesActivityData(requestInfo, searchCriteria);
        return monthwiseCallCount;
		
	}

	private Map<String,Integer> getMonthwiseAppointmentDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo) {
		
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(salesActivityResponseDataVo.getCommand());
		 Date startDate = null ;
	        Date endDate = null ;
	        if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_THREE_MONTHS)){
	        	startDate = dateRange.get(4);
	        	endDate = dateRange.get(1);
	        }
	        else if(salesActivityResponseDataVo.getCommand().equals(GeneraliConstants.LAST_SIX_MONTHS)){
	        	startDate = dateRange.get(10);
	        	endDate = dateRange.get(1);
	        }
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.APPOINTMENT);
		searchCriteria.setCommand(salesActivityResponseDataVo.getCommand());
        Map<String,Integer> monthwiseAppointmentCount = new HashMap<String,Integer>();
        monthwiseAppointmentCount = retrieveMonthwiseSalesActivityData(requestInfo, searchCriteria);
        return monthwiseAppointmentCount;
		
	}
	
	private Map<String,Integer> getMonthwiseNewProspectDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo){
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(salesActivityResponseDataVo.getCommand());
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setCommand(salesActivityResponseDataVo.getCommand());
        Map<String,Integer> newProspectCount = new HashMap<String,Integer>();
        newProspectCount = retrieveMonthwiseNewProspect(requestInfo, searchCriteria);
        return newProspectCount;
	}


	private Map<String,Integer> getWeeklyAppointmentDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command) {
    	
    	SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
		Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.APPOINTMENT);
        Map<String,Integer> weeklyAppointmentCount = new HashMap<String,Integer>();
        weeklyAppointmentCount = retrieveWeekwiseSalesActivityData(requestInfo, searchCriteria);
        return weeklyAppointmentCount;
		
    	
		
	}

	private Map<String,Integer> getWeeklyCallDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command) {
		
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.CALL);
        Map<String,Integer> weeklyCallCount = new HashMap<String,Integer>();
        weeklyCallCount = retrieveWeekwiseSalesActivityData(requestInfo, searchCriteria);
        return weeklyCallCount;
		
	}
	
	private Map<String,Integer> getWeeklyVisitDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command) {
		
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.VISIT);
        Map<String,Integer> weeklyVisitCount = new HashMap<String,Integer>();
        weeklyVisitCount = retrieveWeekwiseSalesActivityData(requestInfo, searchCriteria);
        return weeklyVisitCount;
		
	}
	
	private Map<String,Integer> getWeekwiseNewProspectDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command){
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        Map<String,Integer> newProspectCount = new HashMap<String,Integer>();
        newProspectCount = retrieveWeekwiseNewProspectData(requestInfo, searchCriteria);
        return newProspectCount;
	}
	
	private Map<String,Integer> retrieveWeekwiseNewProspectData(RequestInfo requestInfo,
			SearchCriteria searchCriteria) {
	      LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
	     Map<String,Integer> weekwiseNewProspectCount = new HashMap<String,Integer>();
	     weekwiseNewProspectCount = lmsRepository.retrieveWeekwiseNewProspect(requestInfo, searchCriteria);
	      Integer count = 0;
	     Integer mondayCount =0;
	     Integer fridayCount =0;
	     String displayDateFormat = "EEE dd";
	     Date day = new Date();
	        Date today = new Date();
	     DateFormat df = new SimpleDateFormat(displayDateFormat);
	     Calendar c = Calendar.getInstance();
	     if (searchCriteria.getStartDate() != null) {
	            c.setTime(searchCriteria.getStartDate());
	     }
	            if(c.get(7) == 7){
	                   c.add(Calendar.DAY_OF_WEEK,2);
	            }
	            
	            else if(c.get(7) == 1){
	                   c.add(Calendar.DAY_OF_WEEK,1);
	            }
	            
	            else {
	                   c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK));
	                   c.add(Calendar.DAY_OF_WEEK,1);
	            }
	            
	            String week = df.format(c.getTime());
	            LinkedHashMap<String,Integer> finalMap = new LinkedHashMap<String,Integer>();
	            //Generate Monday to Friday
	            for(int i = 1; i<=5;i++) {
	                   day = c.getTime();
	                   week = df.format(day);
					 finalMap.put(week, 0);
	                   Iterator<Map.Entry<String,Integer>> iter = weekwiseNewProspectCount.entrySet().iterator();
	                   while (iter.hasNext()) {
	                       Map.Entry<String,Integer> entry = iter.next();
	                       String date = entry.getKey();
	                         count = entry.getValue();
	                         DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	                         try {
	                                week = df.format(day);
	        						  finalMap.put(week, 0);
	                                Date dateValue = formatter.parse(date);
	                                Calendar dateVal = Calendar.getInstance();
	                                dateVal.setTime(dateValue);
	                                //Saturday or Sunday
	                                if (dateVal.get(7) == 1 || dateVal.get(7) == 7) {
	                                       if (c.getTime().before(dateValue)) {
	                                              fridayCount  += count;
	                                              iter.remove();
	                                       } else if (c.getTime().after(dateValue)) {
	                                              mondayCount += count;
	                                              iter.remove();
	                                       }
	                                } else if (formatter.format(dateValue).equals(formatter.format(c.getTime()))) {
	                                       finalMap.put(week, count);
	                                }
	                         } catch (ParseException e) {
	                                e.printStackTrace();
	                         }
	                   }
	                   //Future dates return -1
	                   if(day.after(today)) {
	                         finalMap.put(week, -1);
	                   }
	                   c.add(Calendar.DAY_OF_WEEK,1);
	            }
	            if(finalMap.size() != 0)   {
	                   Map.Entry<String,Integer> entry = finalMap.entrySet().iterator().next();
	                   String firstkey = entry.getKey();
	                   Integer firstvalue =entry.getValue();
	                   firstvalue += mondayCount;
	                   finalMap.put(firstkey, firstvalue);
	                   
	                   List<Entry<String,Integer>> entryList = new ArrayList<Map.Entry<String, Integer>>(finalMap.entrySet());
	                   Entry<String, Integer> lastEntry = entryList.get(entryList.size()-1);
	                   String lastkey = lastEntry.getKey();
	                   Integer lastvalue = lastEntry.getValue();
	                   if(lastvalue != -1) {
	                         lastvalue += fridayCount;
	                         finalMap.put(lastkey, lastvalue);
	                   }
	            }

	     
	      return finalMap;
	  }
	
	private List<String> getAgentCodes(List<AgentDataVo> agentsList) {
        List<String> agentCodesList = new ArrayList<String>();
        for (AgentDataVo agentDataVo : agentsList) {
            agentCodesList.add(agentDataVo.getAgentCode());
        }
        return agentCodesList;
    }

    private HashMap<String, List<AgentDataVo>> getTeamdetailsInEng(List<TeamDetailsDto> teamDetailsDtoList) {
        HashMap<String, List<AgentDataVo>> teamDetailsMapInEng = new HashMap<String, List<AgentDataVo>>();

        for (TeamDetailsDto teamDetailsDto : teamDetailsDtoList) {
            AgentDataVo agentDataVo = new AgentDataVo();
            List<AgentDataVo> agentDataVoList = new ArrayList<AgentDataVo>();

            if (teamDetailsMapInEng.containsKey(teamDetailsDto.getTeamNameInEng())) {
                agentDataVo.setAgentCode(teamDetailsDto.getAgentCode());
                agentDataVo.setAgentName(teamDetailsDto.getAgentName());
                teamDetailsMapInEng.get(teamDetailsDto.getTeamNameInEng()).add(agentDataVo);
            } else {
                agentDataVo.setAgentCode(teamDetailsDto.getAgentCode());
                agentDataVo.setAgentName(teamDetailsDto.getAgentName());
                agentDataVoList.add(agentDataVo);
                teamDetailsMapInEng.put(teamDetailsDto.getTeamNameInEng(), agentDataVoList);
            }
        }
        return teamDetailsMapInEng;
    }

    private HashMap<String, List<AgentDataVo>> getTeamdetailsInThai(List<TeamDetailsDto> teamDetailsDtoList) {
        HashMap<String, List<AgentDataVo>> teamDetailsMapInThai = new HashMap<String, List<AgentDataVo>>();
        
        for (TeamDetailsDto teamDetailsDto : teamDetailsDtoList) {
            AgentDataVo agentDataVo = new AgentDataVo();
            List<AgentDataVo> agentDataVoList = new ArrayList<AgentDataVo>();

            if (teamDetailsMapInThai.containsKey(teamDetailsDto.getTeamNameInThai())) {
                agentDataVo.setAgentCode(teamDetailsDto.getAgentCode());
                agentDataVo.setAgentName(teamDetailsDto.getAgentName());
                teamDetailsMapInThai.get(teamDetailsDto.getTeamNameInThai()).add(agentDataVo);
            } else {
                agentDataVo.setAgentCode(teamDetailsDto.getAgentCode());
                agentDataVo.setAgentName(teamDetailsDto.getAgentName());
                agentDataVoList.add(agentDataVo);
                teamDetailsMapInThai.put(teamDetailsDto.getTeamNameInThai(), agentDataVoList);
            }
        }
        return teamDetailsMapInThai;
    }

    private String validateAgentType(Transactions userTransactions) {
        String positionCode = null;
        String agentType = null;
        if(userTransactions.getGeneraligent()!=null && userTransactions.getGeneraligent().getAgentPositionCode()!=null){
            positionCode = userTransactions.getGeneraligent().getAgentPositionCode();
            if (positionCode.equals(GeneraliConstants.GM_CODE_D1)
                    || positionCode.equals(GeneraliConstants.GM_CODE_D2)
                    || positionCode.equals(GeneraliConstants.GM_CODE_D3)) {
                agentType = GeneraliConstants.GROUP_MANAGER;
            } else if (positionCode.equals(GeneraliConstants.SM_CODE_E1)
                    || positionCode.equals(GeneraliConstants.SM_CODE_E2)
                    || positionCode.equals(GeneraliConstants.SM_CODE_E3)
                    || positionCode.equals(GeneraliConstants.SM_CODE_SM)){
                agentType = GeneraliConstants.SALES_MANAGER;
            } else if (positionCode.equals(GeneraliConstants.AGENT_CODE_PT)
                    || positionCode.equals(GeneraliConstants.AGENT_CODE_I1)
                    || positionCode.equals(GeneraliConstants.AGENT_CODE_I2)
                    || positionCode.equals(GeneraliConstants.AGENT_CODE_I3)
                    || positionCode.equals(GeneraliConstants.AGENT_CODE_FP)
                    || positionCode.equals(GeneraliConstants.AGENT_CODE_CA)){
                agentType = GeneraliConstants.SALES_AGENT;
            }
        }
        return agentType;
    }
    
    /**
     * 
     * Return benchmark calculation
     * 
     * @param startDate
     * @param endDate
     * @param benchmarkTargetForCall
     * @param benchmarkTargetForAppointment
     * @param benchmarkTargetForVisiting
     * @param benchmarkTargetForPresentation
     * @param benchmarkTargetForNewProspect
     * @param benchmarkTargetForSelling
     * @return
     */
    private List<BigDecimal> getBenchmarkCalculations(Date startDate, Date endDate, String benchmarkTargetForCall,
            String benchmarkTargetForAppointment, String benchmarkTargetForVisiting,
            String benchmarkTargetForPresentation, String benchmarkTargetForNewProspect,
            String benchmarkTargetForSelling,int benchMarkAgentCount) {

        int numberOfWorkingDays;
        BigDecimal benchmarkTargetCall, benchmarkTargetAppointment, benchmarkTargetVisiting,
                benchmarkTargetPresentation, benchmarkTargetNewProspect, benchmarkTargetSelling;
        BigDecimal total = BigDecimal.ZERO;

        benchmarkTargetCall = new BigDecimal(benchmarkTargetForCall);
        benchmarkTargetAppointment = new BigDecimal(benchmarkTargetForAppointment);
        benchmarkTargetVisiting = new BigDecimal(benchmarkTargetForVisiting);
        benchmarkTargetPresentation = new BigDecimal(benchmarkTargetForPresentation);
        benchmarkTargetNewProspect = new BigDecimal(benchmarkTargetForNewProspect);
        benchmarkTargetSelling = new BigDecimal(benchmarkTargetForSelling);

        List<BigDecimal> benchmarkValueList = new ArrayList<BigDecimal>();

        numberOfWorkingDays = getNumberOfWorkingDays(startDate, endDate);

        benchmarkValueList.add((benchmarkTargetCall.multiply(new BigDecimal(numberOfWorkingDays))).multiply(new BigDecimal(benchMarkAgentCount)));
        benchmarkValueList.add((benchmarkTargetAppointment.multiply(new BigDecimal(numberOfWorkingDays))).multiply(new BigDecimal(benchMarkAgentCount)));
        benchmarkValueList.add((benchmarkTargetVisiting.multiply(new BigDecimal(numberOfWorkingDays))).multiply(new BigDecimal(benchMarkAgentCount)));
        benchmarkValueList.add((benchmarkTargetPresentation.multiply(new BigDecimal(numberOfWorkingDays))).multiply(new BigDecimal(benchMarkAgentCount)));
        benchmarkValueList.add((benchmarkTargetNewProspect.multiply(new BigDecimal(numberOfWorkingDays))).multiply(new BigDecimal(benchMarkAgentCount)));
        benchmarkValueList.add((benchmarkTargetSelling.multiply(new BigDecimal(numberOfWorkingDays))).multiply(new BigDecimal(benchMarkAgentCount)));
        for (BigDecimal eachBenchmark : benchmarkValueList) {
            total = total.add(eachBenchmark);
        }
        benchmarkValueList.add(total);
        return benchmarkValueList;
    }

    /**
     * 
     * method returns the number of working days alone as per Thai calendar
     * Number of working days excludes Thai holiday, Saturday and Sunday
     *  
     * @param startDate
     * @param endDate
     * @return number of working days
     */
    private int getNumberOfWorkingDays(Date startDate, Date endDate) {
        int numberOfWorkingDays;
        int numberOfDaysWithoutSatSun = 0, thaiHoliday = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calStart = Calendar.getInstance();
        Calendar calEnd = Calendar.getInstance();

        calStart.setTime(startDate);
        calEnd.setTime(endDate);

        while (!calStart.after(calEnd)) {
            List<String> holidayList = getThaiHolidays();
            Date dateToCheck = calStart.getTime();
            String dateStringToCheck = dateFormat.format(dateToCheck);
            if (holidayList.contains(dateStringToCheck)) {
                thaiHoliday++;
            }
            if (calStart.get(7) != 1 && calStart.get(7) != 7) {
                numberOfDaysWithoutSatSun++;
            }
            calStart.add(Calendar.DAY_OF_WEEK, 1);
        }

        if (numberOfDaysWithoutSatSun > thaiHoliday) {
            numberOfWorkingDays = numberOfDaysWithoutSatSun - thaiHoliday;
        } else {
            numberOfWorkingDays = thaiHoliday - numberOfDaysWithoutSatSun;
        }
        return numberOfWorkingDays;
    }
    
    /**
     * Method to get the Thailand Holidays
     * @return holidayList
     */
    private List<String> getThaiHolidays() {
        
        String url = "/thaiHoliday/holiday.json";

        String holidays = new Scanner(this.getClass().getResourceAsStream(url)).useDelimiter("\\Z").next();
        List<String> holidayList = new ArrayList<String>();
        try {
            final JSONObject jsonObj = new JSONObject(holidays);
            final JSONArray jsonArray = jsonObj.getJSONArray("dates");

            for (int i = 0; i < jsonArray.length(); i++) {
                holidayList.add(jsonArray.get(i).toString());
            }

        } catch (ParseException e) {
            LOGGER.error("Error while parsing the holday json " + e);

        }
        return holidayList;
    }
    
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
            }
        return message;
        }
    
	private List<Date> getDateRange(String command) {
		List<Date> weekRange = new ArrayList<Date>();
		Date end = new Date();
		Date start = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(end);
		if (command.equals(GeneraliConstants.CURRENT_WEEK)) {
			// Sunday --- Have to display next week as empty from Monday to
			// Friday
			if (c.get(7) == 1 && c.get(5) != c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
				c.add(Calendar.DATE, 1);
				start = c.getTime();
				c.add(Calendar.DATE, 4);
				end = c.getTime();
				weekRange.add(0, start);
				weekRange.add(1, end);
			}
			// Saturday --- Have to display next week as empty from Monday to
			// Friday
			else if (c.get(7) == 7 && (c.get(5) + 1) != c.getActualMaximum(Calendar.DAY_OF_MONTH)
					&& (c.get(5)) != c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
				c.add(Calendar.DATE, 2);
				start = c.getTime();
				c.add(Calendar.DATE, 4);
				end = c.getTime();
				weekRange.add(0, start);
				weekRange.add(1, end);
			}

			// Saturday - last day of the month
			else if (c.get(7) == 7 && c.get(5) == c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
				end = c.getTime();
				c.add(Calendar.DATE, -7);
				start = c.getTime();
				weekRange.add(0, start);
				weekRange.add(1, end);
			}

			// Sunday - last day of the month
			else if (c.get(7) == 1 && c.get(5) == c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
				end = c.getTime();
				c.add(Calendar.DATE, -8);
				start = c.getTime();
				weekRange.add(0, start);
				weekRange.add(1, end);
			}
			/*
			 * Special case, if Sunday is the last day and Saturday is the last
			 * Saturday of the month
			 */
			else if (c.get(7) == 7 && (c.get(5) + 1) == c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
				end = c.getTime();
				c.add(Calendar.DATE, -7);
				start = c.getTime();
				weekRange.add(0, start);
				weekRange.add(1, end);
			} else {
				// Weekdays
				end = c.getTime();
				c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK));
				c.add(Calendar.DATE, -1);
				start = c.getTime();
				weekRange.add(0, start);
				weekRange.add(1, end);
				List<Date> dateRange = monthEndDateValidator(weekRange);
				weekRange.set(0, dateRange.get(0));
				weekRange.set(1, dateRange.get(1));
			}
		}

		else if (command.equals(GeneraliConstants.PREVIOUS_WEEK)) {
			// weekday
			if (c.get(7) != 1 && c.get(7) != 7) {
				c.add(Calendar.DATE, -7);
				c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK));
				c.add(Calendar.DATE, -1);
				start = c.getTime();
				c.add(Calendar.DATE, 6);
				end = c.getTime();
				weekRange.add(0, start);
				weekRange.add(1, end);
				List<Date> dateRange = monthEndDateValidator(weekRange);
				weekRange.set(0, dateRange.get(0));
				weekRange.set(1, dateRange.get(1));
			}
			// Saturday
			else if (c.get(7) == 7) {
				// Saturday last weekend of month
				if (c.get(5) == c.getActualMaximum(Calendar.DAY_OF_MONTH)
						|| (c.get(5) + 1) == c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
					c.add(Calendar.DATE, -8);
					end = c.getTime();
					c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK));
					c.add(Calendar.DATE, -1);
					start = c.getTime();
					weekRange.add(0, start);
					weekRange.add(1, end);
				} else {
					c.add(Calendar.DATE, -1);
					end = c.getTime();
					c.add(Calendar.DATE, -6);
					start = c.getTime();

					weekRange.add(0, start);
					weekRange.add(1, end);
					List<Date> dateRange = monthEndDateValidator(weekRange);
					weekRange.set(0, dateRange.get(0));
					weekRange.set(1, dateRange.get(1));
				}
			}
			// Sunday
			else if (c.get(7) == 1) {
				// Sunday last weekend of month
				if (c.get(5) == c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
					c.add(Calendar.DATE, -9);
					end = c.getTime();
					c.add(Calendar.DATE, -6);
					start = c.getTime();
					weekRange.add(0, start);
					weekRange.add(1, end);
				} else {
					c.add(Calendar.DATE, -2);
					end = c.getTime();
					c.add(Calendar.DATE, -6);
					start = c.getTime();

					weekRange.add(0, start);
					weekRange.add(1, end);
					List<Date> dateRange = monthEndDateValidator(weekRange);
					weekRange.set(0, dateRange.get(0));
					weekRange.set(1, dateRange.get(1));
				}
			}
		}

		// Returns 1 pair of date range for current month
		else if (command.equals(GeneraliConstants.CURRENT_MONTH)) {
			end = c.getTime();
			c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
			start = c.getTime();
			weekRange.add(0, start);
			weekRange.add(1, end);
		}

		// Returns 1 pair of date range for previous month
		else if (command.equals(GeneraliConstants.PREVIOUS_MONTH)) {
			c.add(Calendar.MONTH, -1);
			// First date of the month
			c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
			start = c.getTime();
			// Last date of the month
			c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
			end = c.getTime();
			weekRange.add(0, start);
			weekRange.add(1, end);
		}

		// Returns 3 pairs of date range for each month
		else if (command.equals(GeneraliConstants.LAST_THREE_MONTHS)) {
			int s = 0;
			for (int k = 0; k < 3; k++) {
				c.add(Calendar.MONTH, -1);
				// First date of the month
				c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
				start = c.getTime();
				// Last date of the month
				c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
				end = c.getTime();
				weekRange.add(s, start);
				s += 1;
				weekRange.add(s, end);
				s += 1;
			}
		}

		// Returns 6 pairs of date range for each month
		else if (command.equals(GeneraliConstants.LAST_SIX_MONTHS)) {
			int s = 0;
			for (int k = 0; k < 6; k++) {
				c.add(Calendar.MONTH, -1);
				// First date of the month
				c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
				start = c.getTime();
				// Last date of the month
				c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
				end = c.getTime();
				weekRange.add(s, start);
				s += 1;
				weekRange.add(s, end);
				s += 1;
			}
		}		return weekRange;
	}
    
	private List<Date> monthEndDateValidator(List<Date> dateList) {
		Calendar c = Calendar.getInstance();
		Date end = dateList.get(1);
		Date start = dateList.get(0);
		c.setTime(end);
		c.add(Calendar.DATE, 1);
		// Saturday last day of week
		if (c.get(5) == c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			end = c.getTime();
		}
		// Sunday last day of week
		else if ((c.get(5) + 1) == c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			c.add(Calendar.DATE, 1);
			end = c.getTime();
		}
		c.setTime(start);
		// Check if Saturday is a first week of the previous month
		if (c.get(5) == c.getActualMaximum(Calendar.DAY_OF_MONTH)
				|| (c.get(5) + 1) == c.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			c.add(Calendar.DATE, 2);
			start = c.getTime();
		}
		dateList.set(0, start);
		dateList.set(1, end);
		return dateList;
	}
	
	private Map<String,Integer> getMonthlyAppointmentDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command) throws BusinessException, ParseException  {
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.APPOINTMENT);
        Map<String,Integer> monthlyAppointmentCount = new LinkedHashMap<String,Integer>();
        monthlyAppointmentCount = retrieveMonthlySalesActivityData(requestInfo, searchCriteria);
        return monthlyAppointmentCount;
    }
	
	private Map<String,Integer> getMonthlyCallDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command) throws BusinessException, ParseException  {
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.CALL);
        Map<String,Integer> monthlyCallCount = new LinkedHashMap<String,Integer>();
        monthlyCallCount = retrieveMonthlySalesActivityData(requestInfo, searchCriteria);
        return monthlyCallCount;
    }
	
	private Map<String,Integer> getMonthlyVisitDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command) throws BusinessException, ParseException  {
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.VISIT);
        Map<String,Integer> monthlyVisitCount = new LinkedHashMap<String,Integer>();
        monthlyVisitCount = retrieveMonthlySalesActivityData(requestInfo, searchCriteria);
        return monthlyVisitCount;
    }
	
	private Map<String,Integer> getMonthlyPresentationDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command) throws BusinessException, ParseException  {
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(GeneraliConstants.PRESENTATION);
        Map<String,Integer> monthlyPresentatioCount = new LinkedHashMap<String,Integer>();
        monthlyPresentatioCount = retrieveMonthlySalesActivityData(requestInfo, searchCriteria);
        return monthlyPresentatioCount;
    }
	
	private Map<String,Integer> getMonthlyEappDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command) throws BusinessException, ParseException  {
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setType(ContextTypeCodeList.EAPP.toString());
        Map<String,Integer> monthlyPresentatioCount = new LinkedHashMap<String,Integer>();
        monthlyPresentatioCount = retrieveMonthlySalesActivityData(requestInfo, searchCriteria);
        return monthlyPresentatioCount;
    }
	
	private Map<String,Integer> retrieveMonthlySalesActivityData(RequestInfo requestInfo, SearchCriteria searchCriteria) throws BusinessException, ParseException {
		LOGGER.trace("Inside LifeEngageSyncImpl retrieveMonthlySalesActivityData : requestInfo" + requestInfo);
	       Map<String,Integer> monthlyCount = new LinkedHashMap<String,Integer>();
	       Map<String,Integer> salesActivityCount = new LinkedHashMap<String,Integer>();
	       salesActivityCount = lmsRepository.retrieveMonthlySalesActivity(requestInfo, searchCriteria);
       	Calendar c = Calendar.getInstance();
		c.setTime(searchCriteria.getStartDate());
       	int iter = 1;
       	Date today = new Date();
       	Integer count =0;
       	DateFormat df = new SimpleDateFormat("MM"+"/"+"dd");
       	
		while(iter <= 5) {
			Iterator<Map.Entry<String,Integer>> iterator = salesActivityCount.entrySet().iterator();
			String weekDate = "Week of " + df.format(c.getTime());
			monthlyCount.put(weekDate, 0);
			while (iterator.hasNext()) {
	            Map.Entry<String,Integer> entry = iterator.next();
	            String weekNumber = entry.getKey();
	            count = entry.getValue();
	            if(c.get(4) == Integer.parseInt(weekNumber)) {
	            	monthlyCount.put(weekDate, count);
	            }
	        }
			if(c.getTime().after(today)) {
				monthlyCount.put(weekDate, -1);
			}
			
			c.add(Calendar.DATE, 7);
			c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK));
			c.add(Calendar.DATE, 1);
			iter++;
		}
		return monthlyCount;
	}
	
    private Map<String,Integer> getMonthlyNewProspectDetails(RequestInfo requestInfo, List<String> agentCodesList,
			SalesActivityResponseDataVo salesActivityResponseDataVo, String command) throws BusinessException, ParseException{
    	List<Transactions> transactionsList;
		Map<String,Integer> newProspectCount = new LinkedHashMap<String,Integer>(); 
		SearchCriteria searchCriteria = new SearchCriteria();
		ArrayList<String> selectedID = new ArrayList<String>(agentCodesList);
		searchCriteria.setSelectedIds(selectedID);
		List<Date> dateRange= getDateRange(command);
        Date startDate = dateRange.get(0);
        Date endDate = dateRange.get(1);
        searchCriteria.setCurrentDate(endDate);
        searchCriteria.setStartDate(startDate);
		transactionsList = retrieveNewProspectData(requestInfo, searchCriteria);
		int leadWeek1 = 0;
		int leadWeek2 = 0;
		int leadWeek3 = 0;
		int leadWeek4 = 0;
		int leadWeek5 = 0;
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
       	int iter = 1;
       	Date today = new Date();
       	List<String> weekHeader = new ArrayList<String>();
		while(iter <= 5) {
			DateFormat df = new SimpleDateFormat("MM"+"/"+"dd");
			String weekDate = "Week of " + df.format(c.getTime());
			weekHeader.add(weekDate);
			if(c.getTime().after(today)) {
				newProspectCount.put(weekDate, -1);
			}
			else {
				newProspectCount.put(weekDate, 0);
			}
			c.add(Calendar.DATE, 7);
			c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK));
			c.add(Calendar.DATE, 1);
			iter++;
		}
		for(Transactions transaction : transactionsList){
						Date creationDate = transaction.getCustomer().getCreationDateTime();
						c.setTime(creationDate);
						int weekNumber = c.get(4);
						switch(weekNumber){
						case 1:
							leadWeek1++;
							newProspectCount.put(weekHeader.get(0), leadWeek1);
							break;
						case 2:
							leadWeek2++;
							newProspectCount.put(weekHeader.get(1), leadWeek2);
							break;
						case 3:
							leadWeek3++;
							newProspectCount.put(weekHeader.get(2), leadWeek3);
							break;
						case 4:
							leadWeek4++;
							newProspectCount.put(weekHeader.get(3), leadWeek4);
							break;
						case 5:
							leadWeek5++;
							newProspectCount.put(weekHeader.get(4), leadWeek5);
							break;
						}
		}
		
        return newProspectCount;
	}
    
    protected Map<String,Integer> retrieveSalesActivityData(final RequestInfo requestInfo, final SearchCriteria searchCriteria)
            throws BusinessException, ParseException {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
        Map<String,Integer> transactionsList = new LinkedHashMap<String,Integer>();
        transactionsList = lmsRepository.retrieveMonthlySalesActivity(requestInfo, searchCriteria);
        return transactionsList;
    }
    
    protected List<Transactions> retrieveNewProspectData(final RequestInfo requestInfo, final SearchCriteria searchCriteria)
            throws BusinessException, ParseException {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
       List<Transactions> transactionsList = new ArrayList<Transactions>();
        transactionsList = lmsRepository.retrieveMonthlyNewProspect(requestInfo, searchCriteria);
        return transactionsList;
    }
    
    private Map<String,Integer> retrieveMonthwiseSalesActivityData(RequestInfo requestInfo,
			SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
       Map<String, Integer> monthwiseCount = new HashMap<String, Integer>();
		monthwiseCount = lmsRepository.retrieveMonthwiseSalesActivity(requestInfo, searchCriteria);
		Calendar c = Calendar.getInstance();
		LinkedHashMap<String, Integer> countMap = new LinkedHashMap<String, Integer>();
		String week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
		Integer count = 0;
		if (searchCriteria.getCommand().equals(GeneraliConstants.LAST_THREE_MONTHS)) {
			for (int i = 1; i <= 3; i++) {
				c.add(Calendar.MONTH, -1);
				week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
				count = monthwiseCount.get(Integer.toString(c.get(2) + 1));
				if (count == null) {
					count = 0;
				}
				countMap.put(week, count);
			}
		} else if (searchCriteria.getCommand().equals(GeneraliConstants.LAST_SIX_MONTHS)) {
			for (int j = 1; j <= 6; j++) {
				c.add(Calendar.MONTH, -1);
				week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
				count = monthwiseCount.get(Integer.toString(c.get(2) + 1));
				if (count == null) {
					count = 0;
				}
				countMap.put(week, count);
			}
		}
		Map<String,Integer> resultMap = new LinkedHashMap<String,Integer>();
		Set<String> keySet = countMap.keySet();
		List<String> keyList = new ArrayList<String>(keySet);
		for(int i=keyList.size()-1 ; i>=0 ;i--){
			resultMap.put(keyList.get(i), countMap.get(keyList.get(i)));
		}
		
		return resultMap;
	}
    
    private Map<String,Integer> retrieveMonthwiseNewProspect(RequestInfo requestInfo,
			SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
       Map<String, Integer> monthwiseCount = new HashMap<String, Integer>();
		monthwiseCount = lmsRepository.retrieveMonthwiseNewProspect(requestInfo, searchCriteria);
		Calendar c = Calendar.getInstance();
		LinkedHashMap<String, Integer> countMap = new LinkedHashMap<String, Integer>();
		String week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
		Integer count = 0;
		if (searchCriteria.getCommand().equals(GeneraliConstants.LAST_THREE_MONTHS)) {
			for (int i = 1; i <= 3; i++) {
				c.add(Calendar.MONTH, -1);
				week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
				count = monthwiseCount.get(Integer.toString(c.get(2) + 1));
				if (count == null) {
					count = 0;
				}
				countMap.put(week, count);
			}
		} else if (searchCriteria.getCommand().equals(GeneraliConstants.LAST_SIX_MONTHS)) {
			for (int j = 1; j <= 6; j++) {
				c.add(Calendar.MONTH, -1);
				week = new SimpleDateFormat("MMM yyyy").format(c.getTime());
				count = monthwiseCount.get(Integer.toString(c.get(2) + 1));
				if (count == null) {
					count = 0;
				}
				countMap.put(week, count);
			}
		}
		Map<String,Integer> resultMap = new LinkedHashMap<String,Integer>();
		Set<String> keySet = countMap.keySet();
		List<String> keyList = new ArrayList<String>(keySet);
		for(int i=keyList.size()-1 ; i>=0 ;i--){
			resultMap.put(keyList.get(i), countMap.get(keyList.get(i)));
		}
		
		return resultMap;
	}
    
    private Map<String,Integer> retrieveWeekwiseSalesActivityData(RequestInfo requestInfo,
			SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
       Map<String,Integer> weekwiseAppointmentCount = new HashMap<String,Integer>();
       weekwiseAppointmentCount = lmsRepository.retrieveWeekwiseSalesActivity(requestInfo, searchCriteria);
        Integer count = 0;
       Integer mondayCount =0;
       Integer fridayCount =0;
       String displayDateFormat = "EEE dd";
       Date day = new Date();
          Date today = new Date();
       DateFormat df = new SimpleDateFormat(displayDateFormat);
       Calendar c = Calendar.getInstance();
       if (searchCriteria.getStartDate() != null) {
              c.setTime(searchCriteria.getStartDate());
       }
              if(c.get(7) == 7){
                     c.add(Calendar.DAY_OF_WEEK,2);
              }
              
              else if(c.get(7) == 1){
                     c.add(Calendar.DAY_OF_WEEK,1);
              }
              
              else {
                     c.set(Calendar.DAY_OF_WEEK, c.getActualMinimum(Calendar.DAY_OF_WEEK));
                     c.add(Calendar.DAY_OF_WEEK,1);
              }
              
              String week = df.format(c.getTime());
              LinkedHashMap<String,Integer> finalMap = new LinkedHashMap<String,Integer>();
              //Generate Monday to Friday
              for(int i = 1; i<=5;i++) {
                     day = c.getTime();
                     week = df.format(day);
					 finalMap.put(week, 0);
                     Iterator<Map.Entry<String,Integer>> iter = weekwiseAppointmentCount.entrySet().iterator();
                     while (iter.hasNext()) {
                         Map.Entry<String,Integer> entry = iter.next();
                         String date = entry.getKey();
                           count = entry.getValue();
                           DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                           try {
                                  week = df.format(day);
                                  Date dateValue = formatter.parse(date);
                                  Calendar dateVal = Calendar.getInstance();
                                  dateVal.setTime(dateValue);
                                  //Saturday or Sunday
                                  if (dateVal.get(7) == 1 || dateVal.get(7) == 7) {
                                         if (c.getTime().before(dateValue)) {
                                                fridayCount  += count;
                                                iter.remove();
                                         } else if (c.getTime().after(dateValue)) {
                                                mondayCount += count;
                                                iter.remove();
                                         }
                                  } else if (formatter.format(dateValue).equals(formatter.format(c.getTime()))) {
                                         finalMap.put(week, count);
                                         iter.remove();
                                  }
                           } catch (ParseException e) {
                                  e.printStackTrace();
                           }
                     }
                     //Future dates return -1
                     if(day.after(today)) {
                           finalMap.put(week, -1);
                     }
                     c.add(Calendar.DAY_OF_WEEK,1);
              }
              if(finalMap.entrySet().iterator().hasNext())   {
                     Map.Entry<String,Integer> entry = finalMap.entrySet().iterator().next();
                     String firstkey = entry.getKey();
                     Integer firstvalue =entry.getValue();
                     firstvalue += mondayCount;
                     finalMap.put(firstkey, firstvalue);
                     
                     List<Entry<String,Integer>> entryList = new ArrayList<Map.Entry<String, Integer>>(finalMap.entrySet());
                     Entry<String, Integer> lastEntry = entryList.get(entryList.size()-1);
                     String lastkey = lastEntry.getKey();
                     Integer lastvalue = lastEntry.getValue();
                     if(lastvalue != -1) {
                           lastvalue += fridayCount;
                           finalMap.put(lastkey, lastvalue);
                     }
              }

       
        return finalMap;
    }
    
	private Map<String, String> extractColorDetails(Map<String, Integer> inputMap, List<String> keyList,
			List<BigDecimal> benchMark, String command,int benchMarkAgentCount) {
		Map<String, String> resultMap = new LinkedHashMap<String, String>();
		String resultColor = "white";
		for (int i = 0; i <= keyList.size() - 1; i++) {
			if (inputMap.get(keyList.get(i)) >= 0) {

				float result = 0;
                if (keyList.get(i).equalsIgnoreCase(GeneraliConstants.BENCHMARK)
                        || keyList.get(i).equalsIgnoreCase(GeneraliConstants.TOTAL_ACTIVITY)
                        || keyList.get(i).equalsIgnoreCase(GeneraliConstants.PRE_WEEKLY)
                        || keyList.get(i).equalsIgnoreCase(GeneraliConstants.PRE_MONTHLY)) {
                    resultColor = "white";
                }
				
				else{
				if (keyList.get(i).equalsIgnoreCase(GeneraliConstants.PERCENT_ACHIEVE)) {
					result = inputMap.get(keyList.get(i)).floatValue();
				} else {
					float count = inputMap.get(keyList.get(i)).floatValue();
					float target = 0;
					if(command.equals(GeneraliConstants.CURRENT_WEEK) || command.equals(GeneraliConstants.PREVIOUS_WEEK)){
					    target = (benchMark.get(0).floatValue()*benchMarkAgentCount);
					} else {
					    target = benchMark.get(i).floatValue()*benchMarkAgentCount;
					}
					result = (count * 100) / target;
				}

				if (result < 60) {
					resultColor = "red";
				}

				else if ((result > 59) && result < 80) {
					resultColor = "yellow";
				}

				else if (result > 79) {
					resultColor = "green";
				}
				}
			}
			else if (inputMap.get(keyList.get(i)) == -1) {
				resultColor = "white";
			} else {
				resultColor = "grey";
			}
			
			resultMap.put(keyList.get(i), resultColor);
		
		}
		return resultMap;
	}
	
	private Map<String, String> extractScoringColorDetails(Map<String, String> inputMap, List<String> keyList,
			List<BigDecimal> benchMark, String command,int benchMarkAgentCount) {
		Map<String, String> resultMap = new LinkedHashMap<String, String>();
		String resultColor = "white";
		for (int i = 0; i <= keyList.size() - 1; i++) {
			if (Float.parseFloat(inputMap.get(keyList.get(i))) >= 0) {

				float result = 0;
                if (GeneraliConstants.COLORCODING_EXEMPTIONS_SET_FOR_SCORING.contains(keyList.get(i))) {
                    resultColor = "white";
                }
				
				else{
    				if (keyList.get(i).equalsIgnoreCase(GeneraliConstants.PERCENT_ACHIEVE)) {
    					result = Float.parseFloat(inputMap.get(keyList.get(i)));
    				} else {
    					float count = Float.parseFloat(inputMap.get(keyList.get(i)));
    					float target = 0;
    					if(command.equals(GeneraliConstants.CURRENT_WEEK) || command.equals(GeneraliConstants.PREVIOUS_WEEK)){
                            target = benchMark.get(0).floatValue()*benchMarkAgentCount;
                        } else {
                            target = benchMark.get(i).floatValue()*benchMarkAgentCount;
                        }
                        result = (count * 100) / target;
    				}
    
    				if (result < 60) {
    					resultColor = "red";
    				}
    
    				else if ((result > 59) && result < 80) {
    					resultColor = "yellow";
    				}
    
    				else if (result > 79) {
    					resultColor = "green";
    				}
				}
			}
			else if (Float.parseFloat(inputMap.get(keyList.get(i))) == -1) {
				resultColor = "white";
			} else {
				resultColor = "grey";
			}
			resultMap.put(keyList.get(i), resultColor);
		}
		return resultMap;
	}
	
	private Map<String, String> extractScoringColorDetailsForPercent(Map<String, String> inputMap, List<String> keyList) {
        Map<String, String> resultMap = new LinkedHashMap<String, String>();
        String resultColor = "white";
        for (int i = 0; i <= keyList.size() - 1; i++) {
            if (Float.parseFloat(inputMap.get(keyList.get(i))) >= 0) {

                float result = 0;
                if (GeneraliConstants.COLORCODING_EXEMPTIONS_SET_FOR_SCORING.contains(keyList.get(i))) {
                    resultColor = "white";
                }
                
                else{
                    result = Float.parseFloat(inputMap.get(keyList.get(i)));
                    if (result < 60) {
                        resultColor = "red";
                    }
    
                    else if ((result > 59) && result < 80) {
                        resultColor = "yellow";
                    }
    
                    else if (result > 79) {
                        resultColor = "green";
                    }
                }
            }
            else if (Float.parseFloat(inputMap.get(keyList.get(i))) == -1) {
                resultColor = "white";
            } else {
                resultColor = "grey";
            }
            resultMap.put(keyList.get(i), resultColor);
        }
        return resultMap;
    }
	
	private Map<String, String> extractTotPercentScoringColorDetails(Map<String, String> inputMap, List<String> keyList) {
		Map<String, String> resultMap = new LinkedHashMap<String, String>();
		String resultColor = "white";
		for (int i = 0; i <= keyList.size() - 1; i++) {
			if (Float.parseFloat(inputMap.get(keyList.get(i))) >= 0) {

				float result = 0;
                if (keyList.get(i).equalsIgnoreCase(GeneraliConstants.BENCHMARK)
                        || keyList.get(i).equalsIgnoreCase(GeneraliConstants.TOTAL_POINTS)
                        || keyList.get(i).equalsIgnoreCase(GeneraliConstants.PERCENT_ACHIEVE)) {
                    resultColor = "white";
                }
				
				else{
					result = Float.parseFloat(inputMap.get(keyList.get(i)));

				if (result < 60) {
					resultColor = "red";
				}

				else if ((result > 59) && result < 80) {
					resultColor = "yellow";
				}

				else if (result > 79) {
					resultColor = "green";
				}
				}
			}
			else if (Float.parseFloat(inputMap.get(keyList.get(i))) == -1) {
				resultColor = "white";
			} else {
				resultColor = "grey";
			}
			
			resultMap.put(keyList.get(i), resultColor);
		
		}
		return resultMap;
	}
    
    private Map<String,String> getAchieveCustom(SalesActivityDataVo salesActivityVo,SalesActivityDataVo salesActivityDataVo,String chooseActivity, List<BigDecimal> benchmarkValuesListForSalesActivity) {
    	Map<String,String> achieveAcitivity = new LinkedHashMap<String, String>();
    	Map<String, Integer> monthCount = new LinkedHashMap<String, Integer>(); 
    	String callAchievePercent = StringUtils.EMPTY;
    	String visitAchievePercent = StringUtils.EMPTY;
    	String apptAchievePercent = StringUtils.EMPTY;
    	String prstAchievePercent = StringUtils.EMPTY;
    	String closeAchievePercent = StringUtils.EMPTY;
    	String newProspectPercent = StringUtils.EMPTY;
    	
    	if(chooseActivity.equals(GeneraliConstants.LAST_THREE_MONTHS) || chooseActivity.equals(GeneraliConstants.LAST_SIX_MONTHS)) {
    		monthCount = salesActivityVo.getMonthwiseCallCount();
    		callAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(0));  
          	          	
          	monthCount = salesActivityVo.getMonthwiseAppointmentCount();
          	apptAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(1));  
          	
          	monthCount = salesActivityVo.getMonthwiseVisitCount();
          	visitAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(2));  
          	
          	monthCount = salesActivityVo.getMonthwisePresentationCount();
          	prstAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(3));  
          	
          	monthCount = salesActivityVo.getMonthwiseCloseCount();
          	closeAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(5));  
          	
          	monthCount = salesActivityVo.getNewProspectDetails();
          	newProspectPercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(4));  
            
    	}else if(chooseActivity.equals(GeneraliConstants.CURRENT_WEEK) || chooseActivity.equals(GeneraliConstants.PREVIOUS_WEEK)) {
    		monthCount = salesActivityVo.getWeekwiseCallCount();
    		callAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(0));  
          	         	
          	monthCount = salesActivityVo.getWeekwiseAppointmentCount();
          	apptAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(1));  
          	
          	monthCount = salesActivityVo.getWeekwiseVisitCount();
          	visitAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(2));  
          	
          	monthCount = salesActivityVo.getWeekwisePresentationCount();
          	prstAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(3));  
          	
          	monthCount = salesActivityVo.getNewProspectDetails();
            newProspectPercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(4));
          	
          	monthCount = salesActivityVo.getWeekwiseCloseCount();
          	closeAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(5));  
    	}else if(chooseActivity.equals(GeneraliConstants.CURRENT_MONTH) || chooseActivity.equals(GeneraliConstants.PREVIOUS_MONTH)) {
    		monthCount = salesActivityVo.getCallWeekCount();
    		callAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(0));  
                   	
          	monthCount = salesActivityVo.getAppointmentWeekCount();
          	apptAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(1));  
                   	
          	monthCount = salesActivityVo.getVisitWeekCount();
          	visitAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(2));  
                   	
          	monthCount = salesActivityVo.getPresentationWeekCount();
          	prstAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(3));  
          	
          	monthCount = salesActivityVo.getNewProspectDetails();
            newProspectPercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(4));
                   	
          	monthCount = salesActivityVo.getCloseCaseWeekCount();
          	closeAchievePercent = getCalc(monthCount,salesActivityDataVo,benchmarkValuesListForSalesActivity.get(5));  
         }
    	
      	achieveAcitivity.put("callCount", callAchievePercent);
      	achieveAcitivity.put("apptCount", apptAchievePercent);
      	achieveAcitivity.put("visitCount", visitAchievePercent);
      	achieveAcitivity.put("prstCount", prstAchievePercent);
      	achieveAcitivity.put("closeCount", closeAchievePercent);
      	achieveAcitivity.put("newProspect", newProspectPercent);
      	
      	return achieveAcitivity;
   }
    
    private String getCalc(Map<String,Integer> commonCount,SalesActivityDataVo salesActivityDataVo,BigDecimal benchMark){
    	BigDecimal totalCount=BigDecimal.ZERO;
    	BigDecimal standardPercent = new BigDecimal(100);
    	BigDecimal totalAchievePercent= ((new BigDecimal(commonCount.get(GeneraliConstants.TOTAL_ACTIVITY)).divide(benchMark,2, RoundingMode.HALF_UP)).multiply(standardPercent)).setScale(0, RoundingMode.HALF_UP);
    	return(Integer.toString(totalAchievePercent.intValue()));
    }
    
    private Map<String,String> filterColorWithoutPreviousMonthData(Map<String,String> colorMap,String command) {
	    List<Date> dateRange= getDateRange(command);
	    int index = 0;
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    Calendar calStart = Calendar.getInstance();
	    Calendar calEnd = Calendar.getInstance();
	    calStart.setTime(dateRange.get(0));
	    calEnd.setTime(dateRange.get(1));
	    List<String> holidayList = getThaiHolidays();
	    if(calStart.get(7) == 7){
	    	calStart.add(Calendar.DAY_OF_WEEK,2);
	     }
	     
	     else if(calStart.get(7) == 1){
	    	 calStart.add(Calendar.DAY_OF_WEEK,1);
	     }
	     
	     else {
	    	 calStart.set(Calendar.DAY_OF_WEEK, calStart.getActualMinimum(Calendar.DAY_OF_WEEK));
	    	 calStart.add(Calendar.DAY_OF_WEEK, 1);
	     }
	    Iterator<Map.Entry<String, String>> entries = colorMap.entrySet().iterator();
	    while (entries.hasNext() && index < 5) {
            	Map.Entry<String,String> entry = entries.next();
        	    String key = entry.getKey();
                String dateStringToCheck = dateFormat.format(calStart.getTime());
                if (holidayList.contains(dateStringToCheck) || calEnd.get(2) != calStart.get(2)) {
                	colorMap.put(key, "grey");
                }
                index++;
                calStart.add(Calendar.DAY_OF_WEEK, 1);
        }
	    return colorMap;
    }

    @Override
    public List<AgentReportVo> getAgentReport(String mode) {
        List<AgentReportVo> objAgentReportlist = new ArrayList<AgentReportVo>();

        try {
            objAgentReportlist = userRepository.getAllAgentDetails();

            ArrayList<String> selectedID = new ArrayList<String>();
            for (AgentReportVo agentVo : objAgentReportlist) {
                selectedID.add(agentVo.getAgentCode());
            }
            // Apply partition to support MS SQL Server IN clause
            List<List<String>> agentSplitList = Lists.partition(selectedID, 1000);

            for (List agent : agentSplitList) {

                List<Date> dateRange = null;
                if (mode.equals("Monthly")) {
                    dateRange = getDateRange(GeneraliConstants.PREVIOUS_MONTH);
                } else if (mode.equals("Weekly")) {
                    // Weekly
                    dateRange = getWeekRange();
                }

                SearchCriteria searchCriteria = new SearchCriteria();
                searchCriteria.setAgentCodes(agent);

                Date startDate = dateRange.get(0);
                Date endDate = dateRange.get(1);
                searchCriteria.setCurrentDate(endDate);
                searchCriteria.setStartDate(startDate);

                Map<String, String> leadCount = new HashMap<String, String>();
                Map<String, String> illustrationCount = new HashMap<String, String>();
                Map<String, String> eAppCount = new HashMap<String, String>();
                Map<String, Map<String, String>> callVisitAppointmentCount = new HashMap<String, Map<String, String>>();

                leadCount = lmsRepository.retrieveMonthlyNewCount(new RequestInfo(), searchCriteria);

                for (Map.Entry<String, String> entry : leadCount.entrySet()) {

                    for (AgentReportVo agentVo : objAgentReportlist) {
                        if (entry.getKey().equals(agentVo.getAgentCode())) {
                            agentVo.setLeadCount(entry.getValue());

                        }
                    }

                }
                illustrationCount =
                        illustrationRepository.getEAppIllustrationCountForReport(new RequestInfo(), searchCriteria);

                for (Map.Entry<String, String> entry : illustrationCount.entrySet()) {

                    for (AgentReportVo agentVo : objAgentReportlist) {
                        if (entry.getKey().equals(agentVo.getAgentCode())) {
                            agentVo.setIllustrationCount(entry.getValue());
                        }
                    }

                }
                eAppCount = eAppRepository.getEAppIllustrationCountForReport(new RequestInfo(), searchCriteria);
                for (Map.Entry<String, String> entry : eAppCount.entrySet()) {

                    for (AgentReportVo agentVo : objAgentReportlist) {
                        if (entry.getKey().equals(agentVo.getAgentCode())) {
                            agentVo.seteAppCount(entry.getValue());
                        }
                    }

                }

                callVisitAppointmentCount = lmsRepository.retrieveLeadStatistics(new RequestInfo(), searchCriteria);
                if (callVisitAppointmentCount.containsKey("Call")) {

                    Map<String, String> callCount = callVisitAppointmentCount.get("Call");
                    for (Map.Entry<String, String> entry : callCount.entrySet()) {

                        for (AgentReportVo agentVo : objAgentReportlist) {
                            if (entry.getKey().equals(agentVo.getAgentCode())) {
                                agentVo.setCallCount(entry.getValue());

                            }
                        }

                    }

                }
                if (callVisitAppointmentCount.containsKey("Visit")) {
                    Map<String, String> visitCount = callVisitAppointmentCount.get("Visit");
                    for (Map.Entry<String, String> entry : visitCount.entrySet()) {

                        for (AgentReportVo agentVo : objAgentReportlist) {
                            if (entry.getKey().equals(agentVo.getAgentCode())) {
                                agentVo.setVisitCount(entry.getValue());

                            }
                        }

                    }

                }
                if (callVisitAppointmentCount.containsKey("Appointment")) {
                    Map<String, String> appointmentCount = callVisitAppointmentCount.get("Appointment");
                    for (Map.Entry<String, String> entry : appointmentCount.entrySet()) {

                        for (AgentReportVo agentVo : objAgentReportlist) {
                            if (entry.getKey().equals(agentVo.getAgentCode())) {
                                agentVo.setAppointmentcount(entry.getValue());

                            }
                        }

                    }

                }

            }

        } catch (Exception e) {
            LOGGER.error("Error in Agent Report Generation " + e.getMessage());
        }
        // TODO Auto-generated method stub
        return objAgentReportlist;
    }
    /** Get date range for last 7 days
     * @return weekRange
     */
    private List<Date> getWeekRange() {
        List<Date> weekRange = new ArrayList<Date>();
        Date end = new Date();
        Date start = new Date();
        Calendar c = Calendar.getInstance();

        c.add(Calendar.DATE, -1);
        end = c.getTime();
        c.add(Calendar.DATE, -6);
        start = c.getTime();
        weekRange.add(0, start);
        weekRange.add(1, end);
        return weekRange;
    }


}
