package com.cognizant.generali.security.constants;

public class GeneraliExceptionCodes {
	
	public static final String ERROR_475 = "475";
	public static final String ERROR_480 = "480";
	public static final String ERROR_481 = "481";
	public static final String ERROR_502 = "502";
				
			
}
