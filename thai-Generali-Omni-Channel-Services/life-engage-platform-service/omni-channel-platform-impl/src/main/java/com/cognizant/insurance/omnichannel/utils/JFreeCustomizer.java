package com.cognizant.insurance.omnichannel.utils;

import java.awt.Color;
import java.awt.Font;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.ui.TextAnchor;
import org.jfree.chart.title.LegendTitle;

import net.sf.jasperreports.engine.JRAbstractChartCustomizer;
import net.sf.jasperreports.engine.JRChart;

public class JFreeCustomizer extends JRAbstractChartCustomizer {
	@Override
	public void customize(JFreeChart chart, JRChart jasperChart) {
		CategoryPlot categoryPlot = chart.getCategoryPlot();
		//To eliminate legend border and position it to top
		LegendTitle legend = chart.getLegend();
		legend.setBorder(0, 0, 0, 0);
		BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();
		// The space between 2 series within a category is controlled by the
		renderer.setMaximumBarWidth(0.20);
		// attribute itemMargin
		renderer.setItemMargin(-0.5);
		categoryPlot.setRenderer(renderer);
		// The space between 2 categories is controlled by the categoryMargin
		// attribute of the CategoryAxis //for the plot
		CategoryAxis domainAxis = categoryPlot.getDomainAxis();
		domainAxis.setCategoryMargin(0.3f);

		renderer.setItemLabelAnchorOffset(1.25);
		//setting properties for Last Entry in the chart.
		Font font = new Font("SansSerif", Font.PLAIN, 8);
		renderer.setSeriesItemLabelsVisible(2, true);
		renderer.setSeriesItemLabelFont(2, font);
		renderer.setSeriesItemLabelPaint(2, new Color(102, 102, 102));
		renderer.setSeriesPositiveItemLabelPosition(2, new ItemLabelPosition(
		ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER));
		categoryPlot.setRangeGridlinesVisible(false);
		categoryPlot.setDomainGridlinesVisible(false);

	}
}
