/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists;

/**
 * Identifies a classification of employees according to their salary grade.
 * 
 * @author 301350
 * 
 * 
 */
public enum SalaryGradeCodeList {
    /**
     * Identifies an employee with salary grade 'Junior'.
     * 
     * 
     * 
     * 
     */
    Junior,
    /**
     * Identifies an employee with salary grade 'Senior'.
     * 
     * 
     * 
     * 
     */
    Senior,
    /**
     * Identifies an employee with salary grade 'Entry level'.
     * 
     * 
     * 
     */
    EntryLevel
}
