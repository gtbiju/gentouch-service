/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.placesubtypes;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.agent.domain.contactandplace.Place;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.CountrySubdivisionTypeCodeList;

/**
 * This concept represents the first level decomposition of a country into
 * subdivisions.
 * 
 * Examples: Canada subdivides into provinces and territories USA subdivides
 * into states, territories, and a district
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("COUNTRY_SUBDIV")
public class CountrySubdivision extends Place {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3736504605550349508L;

    /**
     * See ISO codes definition (ISO 3166-2).
     */
    @OneToOne(cascade=CascadeType.ALL)
    private ExternalCode alphaISOCode;

    /**
     * A code indicating the type of country subdivision (e.g. province, state,
     * etc.).
     * 
     * 
     * 
     */
    @Enumerated
    private CountrySubdivisionTypeCodeList typeCode;

    /**
     * @return the alphaISOCode
     * 
     * 
     */
    public ExternalCode getAlphaISOCode() {
        return alphaISOCode;
    }

    /**
     * @return the typeCode
     * 
     * 
     */
    public CountrySubdivisionTypeCodeList getTypeCode() {
        return typeCode;
    }

    /**
     * @param alphaISOCode
     *            the alphaISOCode to set
     * 
     * 
     */
    public void setAlphaISOCode(final ExternalCode alphaISOCode) {
        this.alphaISOCode = alphaISOCode;
    }

    /**
     * @param typeCode
     *            the typeCode to set
     * 
     * 
     */
    public void
            setTypeCode(final CountrySubdivisionTypeCodeList typeCode) {
        this.typeCode = typeCode;
    }
}
