package com.cognizant.le.model.dto;

public class LoginStatus {

	private boolean invalid;

	private boolean empty;

	private boolean unauthorized;

	/**
	 * @return the unauthorizedUser
	 */
	public boolean isUnauthorized() {
		return unauthorized;
	}

	/**
	 * @param UnauthorizedUser
	 *            the UnauthorizedUser to set
	 */
	public void setUnauthorized(boolean unauthorized) {
		this.unauthorized = unauthorized;
	}

	/**
	 * @return the emptyField
	 */
	public boolean isEmpty() {
		return empty;
	}

	/**
	 * @param emptyField
	 *            the emptyField to set
	 */
	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	/**
	 * @return the invalidUser
	 */
	public boolean isInvalidUser() {
		return invalid;
	}

	/**
	 * @param InvalidUser
	 *            the InvalidUser to set
	 */
	public void setInvalidUser(boolean invalid) {
		this.invalid = invalid;
	}

}
