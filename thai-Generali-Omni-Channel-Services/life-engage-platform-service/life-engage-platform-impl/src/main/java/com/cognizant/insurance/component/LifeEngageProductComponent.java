package com.cognizant.insurance.component;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * The Interface LifeEngageProductComponent.
 */
public interface LifeEngageProductComponent {
	
	/**
	 * Retrieve product
	 *
	 * @param json the input json
	 * @param version 
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String getProduct(RequestInfo requestInfo, String json, String version) throws BusinessException;
	
	/**
	 * Check whether the product is active
	 *
	 * @param json the input json
	 * @param version 
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String isProductActive(RequestInfo requestInfo, String json, String version) throws BusinessException;
	
	/**
	 * Retrieve All products
	 *
	 * @param json the input json
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String getProducts(RequestInfo requestInfo, String json, Boolean activeProducts) throws BusinessException;
	
	/**
	 * Retrieve All active products by filter
	 *
	 * @param json the input json
	 * @param version 
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String getAllOrActiveProductsByFilter(RequestInfo requestInfo, String json, Boolean activeProducts, String version) throws BusinessException;
	
	/**
	 * Retrieve All products by filter
	 *
	 * @param json the input json
	 * @param version 
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String getProductsByFilter(RequestInfo requestInfo, String json, String version) throws BusinessException;
	
	/**
	 * Retrieve All product templates by filter
	 *
	 * @param json the input json
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String getProductTemplatesByFilter(RequestInfo requestInfo, String json) throws BusinessException;

}
