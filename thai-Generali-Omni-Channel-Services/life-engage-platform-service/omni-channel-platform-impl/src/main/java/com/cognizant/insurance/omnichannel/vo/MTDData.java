package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;

/**
 * The Class class MTDData.
 */
public class MTDData implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -127477911741190446L;

    /** The status New. */
    private int new_status;

    /** The status Closed. */
    private int closed_status;

    /** The status FollowUp. */
    private int followUp_status;

    /** The ActivityRatio. */
    private double activityRatio;

    /** The ConversionRate. */
    private long conversionRate;

    /** The APE */
    private long APE;

    /** The Productivity */
    private long productivity;

    /** The CaseSize */
    private long caseSize;

    /**
     * Gets the new status.
     * 
     * @return the new status
     */
    public int getNew_status() {
        return new_status;
    }

    /**
     * Sets the new status.
     * 
     * @param new_status
     *            the new status
     */
    public void setNew_status(int new_status) {
        this.new_status = new_status;
    }

    /**
     * Gets the closed status.
     * 
     * @return the closed status
     */
    public int getClosed_status() {
        return closed_status;
    }

    /**
     * Sets the closed status.
     * 
     * @param closed_status
     *            the new closed status
     */
    public void setClosed_status(int closed_status) {
        this.closed_status = closed_status;
    }

    /**
     * Gets the follow up status.
     * 
     * @return the follow up status
     */
    public int getFollowUp_status() {
        return followUp_status;
    }

    /**
     * Sets the follow up status.
     * 
     * @param followUp_status
     *            the new follow up status
     */
    public void setFollowUp_status(int followUp_status) {
        this.followUp_status = followUp_status;
    }

    /**
     * Gets the activity ratio.
     * 
     * @return the activity ratio
     */
    public double getActivityRatio() {
        return activityRatio;
    }

    /**
     * Sets the activity ratio.
     * 
     * @param activityRatio
     *            the new activity ratio
     */
    public void setActivityRatio(double activityRatio) {
        this.activityRatio = activityRatio;
    }

    /**
     * Gets the conversion rate.
     * 
     * @return the conversion rate
     */
    public long getConversionRate() {
        return conversionRate;
    }

    /**
     * Sets the conversion rate.
     * 
     * @param conversionRate
     *            the new conversion rate
     */
    public void setConversionRate(long conversionRate) {
        this.conversionRate = conversionRate;
    }

    /**
     * Gets the APE.
     * 
     * @return the APE
     */
    public long getAPE() {
        return APE;
    }

    /**
     * Sets the APE.
     * 
     * @param APE
     *            the new APE
     */
    public void setAPE(long aPE) {
        APE = aPE;
    }

    /**
     * Gets the productivity.
     * 
     * @return the productivity
     */
    public long getProductivity() {
        return productivity;
    }

    /**
     * Sets the productivity.
     * 
     * @param productivity
     *            the new productivity
     */
    public void setProductivity(long productivity) {
        this.productivity = productivity;
    }

    /**
     * Gets the case size.
     * 
     * @return the case size
     */
    public long getCaseSize() {
        return caseSize;
    }

    /**
     * Sets the case size.
     * 
     * @param caseSize
     *            the new case size
     */
    public void setCaseSize(long caseSize) {
        this.caseSize = caseSize;
    }

}
