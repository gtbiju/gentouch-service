/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Dec 22, 2015
 */
package com.cognizant.insurance.pushnotification;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class class SNSServices.
 */
@Entity
@Table(name = "AWS_SNS_SERVICES")
public class SNSDetails {

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /** The request json. */
    @Column(name = "REQUEST_JSON")
    private String requestJSON;

    /** The source app id. */
    @Column(name = "SOURCE_APP_ID")
    private String sourceAppID;

    /** The notifier id. */
    @Column(name = "NOTIFIER_ID")
    private String notifierID;

    /** The notifier group id. */
    @Column(name = "NOTIFIER_GROUP_ID")
    private String notifierGroupID;

    /** The message. */
    @Column(name = "MESSAGE")
    private String message;

    /** The device filter. */
    @Column(name = "DEVICE_FILTER")
    private String deviceFilter;

    /** The platform filter. */
    @Column(name = "PLATFORM_FILTER")
    private String platformFilter;

    /** The applicationn name. */
    @Column(name = "APPLICATION_NAME")
    private String applicationName;

    /** The sns status message. */
    @Column(name = "SNS_STATUS_MESSAGE")
    private String snsStatusMessage;
    
    /** The sns status message. */
    @Column(name = "SNS_STATUS")
    private String snsStatus;

    /** The sns request time. */
    @Column(name = "SNS_REQUEST_TIME")
    private Date snsRequestTime;

    /** The sns received time. */
    @Column(name = "SNS_RECEIVED_TIME")
    private Date snsReceivedTime;

    /** The device id. */
    @Column(name = "SNS_DEVICE_ID")
    private String deviceId;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the request json.
     * 
     * @return the request json
     */
    public String getRequestJSON() {
        return requestJSON;
    }

    /**
     * Sets the request json.
     * 
     * @param requestJSON
     *            the request json to set.
     */
    public void setRequestJSON(String requestJSON) {
        this.requestJSON = requestJSON;
    }

    /**
     * Gets the source app id.
     * 
     * @return the source app id
     */
    public String getSourceAppID() {
        return sourceAppID;
    }

    /**
     * Sets the source app id.
     * 
     * @param sourceAppID
     *            the source app id to set.
     */
    public void setSourceAppID(String sourceAppID) {
        this.sourceAppID = sourceAppID;
    }

    /**
     * Gets the notifier id.
     * 
     * @return the notifier id
     */
    public String getNotifierID() {
        return notifierID;
    }

    /**
     * Sets the notifier id.
     * 
     * @param notifierID
     *            the notifier id to set.
     */
    public void setNotifierID(String notifierID) {
        this.notifierID = notifierID;
    }

    /**
     * Gets the notifier group id.
     * 
     * @return the notifier group id
     */
    public String getNotifierGroupID() {
        return notifierGroupID;
    }

    /**
     * Sets the notifier group id.
     * 
     * @param notifierGroupID
     *            the notifier group id to set.
     */
    public void setNotifierGroupID(String notifierGroupID) {
        this.notifierGroupID = notifierGroupID;
    }

    /**
     * Gets the message.
     * 
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     * 
     * @param message
     *            the message to set.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets the device filter.
     * 
     * @return the device filter
     */
    public String getDeviceFilter() {
        return deviceFilter;
    }

    /**
     * Sets the device filter.
     * 
     * @param deviceFilter
     *            the device filter to set.
     */
    public void setDeviceFilter(String deviceFilter) {
        this.deviceFilter = deviceFilter;
    }

    /**
     * Gets the platform filter.
     * 
     * @return the platform filter
     */
    public String getPlatformFilter() {
        return platformFilter;
    }

    /**
     * Sets the platform filter.
     * 
     * @param platformFilter
     *            the platform filter to set.
     */
    public void setPlatformFilter(String platformFilter) {
        this.platformFilter = platformFilter;
    }

    /**
     * Gets the applicationn name.
     * 
     * @return the applicationn name
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Sets the applicationn name.
     *
     * @param applicationName the application name to set.
     */
    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    /**
     * Gets the sns status message.
     * 
     * @return the sns status message
     */
    public String getSnsStatusMessage() {
        return snsStatusMessage;
    }

    /**
     * Sets the sns status message.
     * 
     * @param snsStatusMessage
     *            the sns status message to set.
     */
    public void setSnsStatusMessage(String snsStatusMessage) {
        this.snsStatusMessage = snsStatusMessage;
    }

    /**
     * Gets the sns status.
     *
     * @return the sns status
     */
    public String getSnsStatus() {
        return snsStatus;
    }

    /**
     * Sets the sns status.
     *
     * @param snsStatus the sns status to set.
     */
    public void setSnsStatus(String snsStatus) {
        this.snsStatus = snsStatus;
    }

    /**
     * Gets the sns request time.
     * 
     * @return the sns request time
     */
    public Date getSnsRequestTime() {
        return snsRequestTime;
    }

    /**
     * Sets the sns request time.
     * 
     * @param snsRequestTime
     *            the sns request time to set.
     */
    public void setSnsRequestTime(Date snsRequestTime) {
        this.snsRequestTime = snsRequestTime;
    }

    /**
     * Gets the sns received time.
     * 
     * @return the sns received time
     */
    public Date getSnsReceivedTime() {
        return snsReceivedTime;
    }

    /**
     * Sets the sns received time.
     * 
     * @param snsReceivedTime
     *            the sns received time to set.
     */
    public void setSnsReceivedTime(Date snsReceivedTime) {
        this.snsReceivedTime = snsReceivedTime;
    }

    /**
     * Gets the device id.
     * 
     * @return the device id
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * Sets the device id.
     * 
     * @param deviceId
     *            the device id to set.
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
