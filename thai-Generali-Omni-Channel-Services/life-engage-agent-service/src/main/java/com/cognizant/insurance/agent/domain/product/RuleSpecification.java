/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;

/**
 * The Class RuleSpecification.
 *
 * @author 304007
 */

@Entity
@Table(name = "CORE_PROD_RULE_SPEC")
public class RuleSpecification extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The name. */
    @Column(name="NAME")
    private String name;
    
    /** The version. */
    @Column(name="VERSION")
    private String version;

    /** The input parameters. */
    @Column(name="INPUT_PARAMETERS", columnDefinition = "varchar(max)")
    private String inputParameters;

    /** The output parameters. */
    @Column(name="OUTPUT_PARAMETERS", columnDefinition = "varchar(max)")
    private String outputParameters;

    /** The canbe overwritten indicator. */
    @Column(name="CANBE_OVERWRITTEN_INDICATOR")
    private Boolean canbeOverwrittenIndicator;

    /** The comment on failure. */
    @Column(name="COMMENT_ON_FAILURE")
    private String commentOnFailure;

    /** The actual rule ref. */
    @Column(name="ACTUAL_RULE_REF")
    private String actualRuleRef;
    
    /** The parent product. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "parentProduct_id")
    private ProductSpecification parentProduct;
    
    /** The rule type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "ruleType_ID")
    private RuleType ruleType;
    
    /** Group Name**/
    @Column(name = "GROUP_NAME")
    private String groupName;

    /**
     * The Constructor.
     */
    public RuleSpecification() {
        super();
    }

    /**
     * The Constructor.
     *
     * @return the input parameters
     */
    
    public String getInputParameters() {
        return inputParameters;
    }

    /**
     * Sets the input parameters.
     *
     * @param inputParameters the input parameters
     */
    public void setInputParameters(final String inputParameters) {
        this.inputParameters = inputParameters;
    }

    /**
     * Gets the output parameters.
     *
     * @return the output parameters
     */
    public String getOutputParameters() {
        return outputParameters;
    }

    /**
     * Sets the output parameters.
     *
     * @param outputParameters the output parameters
     */
    public void setOutputParameters(final String outputParameters) {
        this.outputParameters = outputParameters;
    }

    /**
     * Gets the canbe overwritten indicator.
     *
     * @return the canbe overwritten indicator
     */
    public Boolean getCanbeOverwrittenIndicator() {
        return canbeOverwrittenIndicator;
    }

    /**
     * Sets the canbe overwritten indicator.
     *
     * @param canbeOverwrittenIndicator the canbe overwritten indicator
     */
    public void setCanbeOverwrittenIndicator(final Boolean canbeOverwrittenIndicator) {
        this.canbeOverwrittenIndicator = canbeOverwrittenIndicator;
    }

    /**
     * Gets the comment on failure.
     *
     * @return the comment on failure
     */
    public String getCommentOnFailure() {
        return commentOnFailure;
    }

    /**
     * Sets the comment on failure.
     *
     * @param commentOnFailure the comment on failure
     */
    public void setCommentOnFailure(final String commentOnFailure) {
        this.commentOnFailure = commentOnFailure;
    }

    /**
     * Gets the actual rule ref.
     *
     * @return the actual rule ref
     */
    public String getActualRuleRef() {
        return actualRuleRef;
    }

    /**
     * Sets the actual rule ref.
     *
     * @param actualRuleRef the actual rule ref
     */
    public void setActualRuleRef(final String actualRuleRef) {
        this.actualRuleRef = actualRuleRef;
    }

    /**
     * Gets the name.
     *
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The name.
     *
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the version.
     *
     * @return Returns the version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets The version.
     *
     * @param version The version to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * Gets the parentProduct.
     *
     * @return Returns the parentProduct.
     */
    public ProductSpecification getParentProduct() {
        return parentProduct;
    }

    /**
     * Sets The parentProduct.
     *
     * @param parentProduct The parentProduct to set.
     */
    public void setParentProduct(final ProductSpecification parentProduct) {
        this.parentProduct = parentProduct;
    }

    /**
     * Gets the ruleType.
     *
     * @return Returns the ruleType.
     */
    public RuleType getRuleType() {
        return ruleType;
    }

    /**
     * Sets The ruleType.
     *
     * @param ruleType The ruleType to set.
     */
    public void setRuleType(final RuleType ruleType) {
        this.ruleType = ruleType;
    }

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
    
    
}