/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Mar 14, 2013
 */
package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * The Interface interface LookUpservice.
 */

public interface CodeLookUpService {

    /**
     * Gets the look up data.
     * 
     * @param json
     *            the json
     * @return the look up data
     * @throws BusinessException
     *             the business exception
     */
    String getLookUpData(final String json) throws BusinessException;
}
