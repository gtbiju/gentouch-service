/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Mar 14, 2013
 */
package com.cognizant.insurance.agent.domain.codes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

/**
 * The Class class CodeLookUp.
 */

@Entity
@Table(name = "CODE_LOOKUP")
@NamedQueries({
        @NamedQuery(name = "CodeLookUp.fetchLookUpDataForCode", query = "select r.child from CodeRelation r join fetch r.child.localeLookUps l where r.parent.code = :code and l.language= :language and l.country = :country and r.type.name= :typeName"),
        @NamedQuery(name = "CodeLookUp.fetchLookUpData", query = "select c from CodeLookUp c join fetch c.localeLookUps l where l.language= :language and l.country = :country and c.codeType.name= :typeName") })
public class CodeLookUp {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2103441291638341376L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /** The code type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JoinColumn(name = "CODETYPE_ID")
    private CodeTypeLookUp codeType;

    /** The code. */
    @Column(name = "CODE")
    private String code;

    /** The description. */
    @Column(name = "DESCRIPTION",columnDefinition="nvarchar(200)")
    private String description;

    /** The isDefault. */
    @Column(name = "IS_DEFAULT")
    private Integer isDefault;  
    

	/** The locale look ups. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "lookUpCode")
    private Set<CodeLocaleLookUp> localeLookUps;

    /** The parent relations. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parent")
    private Set<CodeRelation> parentRelations;

    /** The child relations. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "child")
    private Set<CodeRelation> childRelations;

    /**
     * Gets the id.
     * 
     * @return Returns the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets The id.
     * 
     * @param id
     *            The id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the code.
     * 
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets The code.
     * 
     * @param code
     *            The code to set.
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The localeLookUps.
     * 
     * @param localeLookUps
     *            The localeLookUps to set.
     */
    public void setLocaleLookUps(Set<CodeLocaleLookUp> localeLookUps) {
        this.localeLookUps = localeLookUps;
    }

    /**
     * Gets the localeLookUps.
     * 
     * @return Returns the localeLookUps.
     */
    public Set<CodeLocaleLookUp> getLocaleLookUps() {
        return localeLookUps;
    }

    /**
     * Gets the parentRelations.
     * 
     * @return Returns the parentRelations.
     */
    public Set<CodeRelation> getParentRelations() {
        return parentRelations;
    }

    /**
     * Sets The parentRelations.
     * 
     * @param parentRelations
     *            The parentRelations to set.
     */
    public void setParentRelations(Set<CodeRelation> parentRelations) {
        this.parentRelations = parentRelations;
    }

    /**
     * Gets the childRelations.
     * 
     * @return Returns the childRelations.
     */
    public Set<CodeRelation> getChildRelations() {
        return childRelations;
    }

    /**
     * Sets The childRelations.
     * 
     * @param childRelations
     *            The childRelations to set.
     */
    public void setChildRelations(Set<CodeRelation> childRelations) {
        this.childRelations = childRelations;
    }

    /**
     * Sets The codeType.
     * 
     * @param codeType
     *            The codeType to set.
     */
    public void setCodeType(CodeTypeLookUp codeType) {
        this.codeType = codeType;
    }

    /**
     * Gets the codeType.
     * 
     * @return Returns the codeType.
     */
    public CodeTypeLookUp getCodeType() {
        return codeType;
    }

	/**
	 * @return the isDefault
	 */
	public Integer getIsDefault() {
		return isDefault;
	}

	/**
	 * @param isDefault the isDefault to set
	 */
	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}
}
