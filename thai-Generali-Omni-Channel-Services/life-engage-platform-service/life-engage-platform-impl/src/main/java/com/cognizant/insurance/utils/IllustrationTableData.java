package com.cognizant.insurance.utils;

import java.util.List;

/**
 * The Class IllustrationTableData.
 */
public class IllustrationTableData {
	
	/** The key1. */
	private List<IllusCode> keys;

	public List<IllusCode> getKeys() {
		return keys;
	}

	public void setKeys(List<IllusCode> keys) {
		this.keys = keys;
	}
	

}
