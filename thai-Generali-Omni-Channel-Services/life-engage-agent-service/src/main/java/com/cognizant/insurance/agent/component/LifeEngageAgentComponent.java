package com.cognizant.insurance.agent.component;

import org.json.JSONArray;

import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.request.vo.RequestInfo;

/**
 * The Interface LifeEngageAgentComponent.
 */
public interface LifeEngageAgentComponent {

	/**
	 * Retrieve agent profile.
	 *
	 * @param requestInfo the request info
	 * @param jsonArray the json array
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String retrieveAgentProfileByCode(RequestInfo requestInfo, JSONArray jsonArray)
			throws BusinessException;

}
