package com.cognizant.insurance.omnichannel.executor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.omnichannel.component.GeneraliEmailComponent;
import com.cognizant.insurance.omnichannel.component.repository.LifeAsiaRepository;

@Component
public class GeneraliFailedSPAJMailExecutor {

    @Autowired
    private LifeAsiaRepository bpmRepository;

    @Autowired
    private GeneraliEmailComponent lifeEngageEmailComponent;

    public void run() {
        try {
            List<String> SPAJs = bpmRepository.getFailedSPAJs();
            List<String> omniSPAJs = new ArrayList<String>();//bpmRepository.getFailedSPAJsFromOmni();
            List<String> failedSPAJs = new ArrayList<String>();
            if (SPAJs.size() > 0) {
                failedSPAJs.addAll(SPAJs);

            }
            if (omniSPAJs.size() > 0) {
                failedSPAJs.addAll(omniSPAJs);
            }

            if (failedSPAJs.size() > 0) {
                lifeEngageEmailComponent.sendFailedSPAJMail(failedSPAJs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
