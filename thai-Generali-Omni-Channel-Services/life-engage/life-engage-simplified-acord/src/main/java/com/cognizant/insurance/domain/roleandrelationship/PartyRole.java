/**
 * 
 */
package com.cognizant.insurance.domain.roleandrelationship;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.contactandplace.ContactPreference;
import com.cognizant.insurance.domain.documentandcommunication.Communication;
import com.cognizant.insurance.domain.documentandcommunication.CommunicationContent;
import com.cognizant.insurance.domain.party.Party;
import com.cognizant.insurance.domain.party.partyname.PartyName;
import com.cognizant.insurance.domain.product.RoleSpecification;

/**
 * This concept is the generalizing concept of all types of party roles.
 * 
 * @author 301350
 * 
 * 
 */
@MappedSuperclass
public abstract class PartyRole extends Role {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3955950103071929339L;

    /**
     * This relationship links a party with one or more party roles. For example, the party "Acme Insurance" may be
     * associated with the party role "Insurer". This represents the party playing a party role. For example, the party
     * role of "Insurer" may be played by the party "Acme Insurance".
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE })
    @Fetch(FetchMode.JOIN)
    private Party playerParty;

    /**
     * This relationship links a party role to a party name for the role.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private PartyName name;

    /**
     * This relationship links a party role to a related contact preference.
     * 
     * 
     * 
     * 
     * 
     */
    @Transient
    private Set<ContactPreference> preferredContactPreference;

    /**
     * This relationship links a party role to a specification in order to identify the designer of the specification.
     * 
     * 
     * 
     * 
     * customized product model
     */
    @Transient
    private Set<RoleSpecification> specificationOfSpecificationIdentifiesDesigningPartyRole;

    /**
     * This relationship represents the sending of a communication to a party playing a role.
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private Set<Communication> receivesCommunication;

    /**
     * Indicates the set of UserSelections made as part of a questionnaire. 
     */ 
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    @BatchSize(size=50)
    private Set<UserSelection> selectedOptions;
    
    
    /**
     * Added for generali vietnam  
     */ 
    @OneToOne(cascade=CascadeType.ALL)
    private CommunicationContent declares;
    
    /**
     * Gets the playerParty.
     * 
     * @return Returns the playerParty.
     */
    public Party getPlayerParty() {
        return playerParty;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public PartyName getName() {
        return name;
    }

    /**
     * Gets the preferredContactPreference.
     * 
     * @return Returns the preferredContactPreference.
     */
    public Set<ContactPreference> getPreferredContactPreference() {
        return preferredContactPreference;
    }

    /**
     * Gets the specificationOfSpecificationIdentifiesDesigningPartyRole.
     * 
     * @return Returns the specificationOfSpecificationIdentifiesDesigningPartyRole.
     */
    public Set<RoleSpecification> getSpecificationOfSpecificationIdentifiesDesigningPartyRole() {
        return specificationOfSpecificationIdentifiesDesigningPartyRole;
    }

    /**
     * Sets The playerParty.
     * 
     * @param playerParty
     *            The playerParty to set.
     */
    public void setPlayerParty(final Party playerParty) {
        this.playerParty = playerParty;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final PartyName name) {
        this.name = name;
    }

    /**
     * Sets The preferredContactPreference.
     * 
     * @param preferredContactPreference
     *            The preferredContactPreference to set.
     */
    public void setPreferredContactPreference(final Set<ContactPreference> preferredContactPreference) {
        this.preferredContactPreference = preferredContactPreference;
    }

    /**
     * Sets The specificationOfSpecificationIdentifiesDesigningPartyRole.
     * 
     * @param specificationOfSpecificationIdentifiesDesigningPartyRole
     *            The specificationOfSpecificationIdentifiesDesigningPartyRole to set.
     */
    public void setSpecificationOfSpecificationIdentifiesDesigningPartyRole(
            final Set<RoleSpecification> specificationOfSpecificationIdentifiesDesigningPartyRole) {
        this.specificationOfSpecificationIdentifiesDesigningPartyRole =
                specificationOfSpecificationIdentifiesDesigningPartyRole;
    }

    /**
     * @return the receivesCommunication
     */
    public Set<Communication> getReceivesCommunication() {
        return receivesCommunication;
    }

    /**
     * @param receivesCommunication
     *            the receivesCommunication to set
     */
    public void setReceivesCommunication(Set<Communication> receivesCommunication) {
        this.receivesCommunication = receivesCommunication;
    }

    /**
     * @return the selectedOptions
     */
    public Set<UserSelection> getSelectedOptions() {
        return selectedOptions;
    }

    /**
     * @param selectedOptions the selectedOptions to set
     */
    public void setSelectedOptions(Set<UserSelection> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

	/**
	 * @return the declares
	 */
	public CommunicationContent getDeclares() {
		return declares;
	}

	/**
	 * @param declares the declares to set
	 */
	public void setDeclares(CommunicationContent declares) {
		this.declares = declares;
	}

   

  
    

}
