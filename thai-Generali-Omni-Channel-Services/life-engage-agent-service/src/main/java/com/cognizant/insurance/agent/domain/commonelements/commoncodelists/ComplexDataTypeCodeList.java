/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * This list defines complex data types. 
 * 
 * @author 301350
 * 
 * 
 */
public enum ComplexDataTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Integer,
    /**
     * 
     * 
     * 
     * 
     */
    CurrencyAmount,
    /**
     * 
     * 
     * 
     * 
     */
    ModFactor,
    /**
     * 
     * 
     * 
     * 
     */
    Percentage,
    /**
     * 
     * 
     * 
     * 
     */
    FormattedText
}
