/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.domain.product;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class PropertySpecification.
 *
 * @author 304007,420118
 */

@Entity
@Table(name = "CORE_PROD_PROPERTIES")
@NamedQueries({
    @NamedQuery(name = "PropertySpecification.pollProperty", query = "select propSpec from PropertySpecification propSpec where propSpec.parentProduct.id = :productId and propSpec.propertyType.id = :propTypeId")
    ,@NamedQuery(name = "PropertySpecification.retrievePropertiesByCode", query = "select propSpec from PropertySpecification propSpec where propSpec.parentProduct.id IN :selectedIds")
    ,@NamedQuery(name = "PropertySpecification.retrievePropertiesByCodeAndVersion", query = "select propSpec from PropertySpecification propSpec where propSpec.productCode = :prodCode and propSpec.version = :version")
})
public class PropertySpecification extends Component {
        
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8730014899225576705L;
    
    /** The name. */
    @Column(name="NAME")
    private String name;   

    /** The default value. */
    @Column(name="DEFAULT_VALUE")
    private String defaultValue;
    
    /** The min value. */
    @Column(name="MIN_VALUE")
    private String minValue;
    
    /** The max value. */
    @Column(name="MAX_VALUE")
    private String maxValue;
    
    /** The optional indicator. */
    @Column(name="OPTIONAL_INDICATOR")
    private String optionalIndicator;
    
    /** The calculated indicator. */
    @Column(name="CALCULATED_INDICATOR")
    private String calculatedIndicator;
    
    /** The constant indicator. */
    @Column(name="CONSTANT_INDICATOR")
    private String constantIndicator;
    
    /** The product id. */
    @Column(name = "PRODUCT_ID")
    private Integer productId;
    
    /** The property values. */  
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE },fetch = FetchType.EAGER, mappedBy="propertySpecification", orphanRemoval = true )
    private Set<PropertyValues> propertyValues;
    
    /** The parent product. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }) 
    private ProductSpecification parentProduct;
    
    /** The property type. */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE }) 
    @JoinColumn(name="propertyType_ID")
    private PropertySpecificationType propertyType;
    
    /** The basic data complete code. */
    @Column(name= "basicDataCompleteCode")
    private Integer basicDataCompleteCode;
    
    /** The context id. */
    @Column(name= "contextId")
    private Integer contextId;
    
    /** The creation date time. */
    @Column(name= "creationDateTime")
    private Date creationDateTime;
    
    /** The modified date time. */
    @Column(name= "modifiedDateTime")
    private Date modifiedDateTime;
    
    /** The trans tracking id. */
    @Column(name= "transTrackingId")
    private String transTrackingId;
    
    /** The transaction id. */
    @Column(name= "transactionId")
    private String transactionId;
    
    /** The type name. */
    @Column(name= "typeName")
    private String typeName;
    
    /** The transaction keys_id. */
    @Column(name= "transactionKeys_id")
    private Long transactionKeys_id;
    
    /**
     * Gets the default value.
     *
     * @return the default value
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the default value.
     *
     * @param defaultValue the default value
     */
    public void setDefaultValue(final String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Gets the min value.
     *
     * @return the min value
     */
    public String getMinValue() {
        return minValue;
    }

    /**
     * Sets the min value.
     *
     * @param minValue the min value
     */
    public void setMinValue(final String minValue) {
        this.minValue = minValue;
    }

    /**
     * Gets the max value.
     *
     * @return the max value
     */
    public String getMaxValue() {
        return maxValue;
    }

    /**
     * Sets the max value.
     *
     * @param maxValue the max value
     */
    public void setMaxValue(final String maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * Gets the optional indicator.
     *
     * @return the optional indicator
     */
    public String getOptionalIndicator() {
        return optionalIndicator;
    }

    /**
     * Sets the optional indicator.
     *
     * @param optionalIndicator the optional indicator
     */
    public void setOptionalIndicator(final String optionalIndicator) {
        this.optionalIndicator = optionalIndicator;
    }

    /**
     * Gets the calculated indicator.
     *
     * @return the calculated indicator
     */
    public String getCalculatedIndicator() {
        return calculatedIndicator;
    }

    /**
     * Sets the calculated indicator.
     *
     * @param calculatedIndicator the calculated indicator
     */
    public void setCalculatedIndicator(final String calculatedIndicator) {
        this.calculatedIndicator = calculatedIndicator;
    }

    /**
     * Gets the constant indicator.
     *
     * @return the constant indicator
     */
    public String getConstantIndicator() {
        return constantIndicator;
    }

    /**
     * Sets the constant indicator.
     *
     * @param constantIndicator the constant indicator
     */
    public void setConstantIndicator(final String constantIndicator) {
        this.constantIndicator = constantIndicator;
    }

    /**
     * Sets The propertyValues.
     *
     * @param propertyValues The propertyValues to set.
     */
    public void setPropertyValues(final Set<PropertyValues> propertyValues) {
        this.propertyValues = propertyValues;
    }

    /**
     * Gets the propertyValues.
     *
     * @return Returns the propertyValues.
     */
    public Set<PropertyValues> getPropertyValues() {
        return propertyValues;
    }

    /**
     * Gets the name.
     *
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The name.
     *
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }


    /**
     * Gets the parentProduct.
     *
     * @return Returns the parentProduct.
     */
    public ProductSpecification getParentProduct() {
        return parentProduct;
    }

    /**
     * Sets The parentProduct.
     *
     * @param parentProduct The parentProduct to set.
     */
    public void setParentProduct(final ProductSpecification parentProduct) {
        this.parentProduct = parentProduct;
    }

    /**
     * Gets the propertyType.
     *
     * @return Returns the propertyType.
     */
    public PropertySpecificationType getPropertyType() {
        return propertyType;
    }

    /**
     * Sets The propertyType.
     *
     * @param propertyType The propertyType to set.
     */
    public void setPropertyType(final PropertySpecificationType propertyType) {
        this.propertyType = propertyType;
    }
    
    /**
	 * Gets the product id.
	 *
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}

	/**
	 * Sets the product id.
	 *
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
    /**
     * Gets the basic data complete code.
     *
     * @return the basic data complete code
     */
    public Integer getBasicDataCompleteCode() {
        return basicDataCompleteCode;
    }


    /**
     * Sets the basic data complete code.
     *
     * @param basicDataCompleteCode the basic data complete code to set.
     */
    public void setBasicDataCompleteCode(Integer basicDataCompleteCode) {
        this.basicDataCompleteCode = basicDataCompleteCode;
    }


    /**
     * Gets the context id.
     *
     * @return the context id
     */
    public Integer getContextId() {
        return contextId;
    }


    /**
     * Sets the context id.
     *
     * @param contextId the context id to set.
     */
    public void setContextId(Integer contextId) {
        this.contextId = contextId;
    }


    /**
     * Gets the creation date time.
     *
     * @return the creation date time
     */
    public Date getCreationDateTime() {
        return creationDateTime;
    }


    /**
     * Sets the creation date time.
     *
     * @param creationDateTime the creation date time to set.
     */
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }


    /**
     * Gets the modified date time.
     *
     * @return the modified date time
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }


    /**
     * Sets the modified date time.
     *
     * @param modifiedDateTime the modified date time to set.
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }


    /**
     * Gets the trans tracking id.
     *
     * @return the trans tracking id
     */
    public String getTransTrackingId() {
        return transTrackingId;
    }


    /**
     * Sets the trans tracking id.
     *
     * @param transTrackingId the trans tracking id to set.
     */
    public void setTransTrackingId(String transTrackingId) {
        this.transTrackingId = transTrackingId;
    }


    /**
     * Gets the transaction id.
     *
     * @return the transaction id
     */
    public String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transaction id.
     *
     * @param transactionId the transaction id to set.
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the type name.
     *
     * @return the type name
     */
    public String getTypeName() {
        return typeName;
    }


    /**
     * Sets the type name.
     *
     * @param typeName the type name to set.
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


    /**
     * Gets the transaction keys_id.
     *
     * @return the transaction keys_id
     */
    public Long getTransactionKeys_id() {
        return transactionKeys_id;
    }


    /**
     * Sets the transaction keys_id.
     *
     * @param transactionKeys_id the transaction keys_id to set.
     */
    public void setTransactionKeys_id(Long transactionKeys_id) {
        this.transactionKeys_id = transactionKeys_id;
    }
}