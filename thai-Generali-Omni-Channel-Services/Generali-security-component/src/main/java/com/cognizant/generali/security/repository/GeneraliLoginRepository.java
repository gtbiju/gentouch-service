package com.cognizant.generali.security.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.generali.security.dao.GeneraliLoginDao;
import com.cognizant.le.security.request.Request;

public class GeneraliLoginRepository {
	
	@Autowired
	private GeneraliLoginDao generaliLoginDao;
	
	@Transactional
	public void updateFailedAttempt(Request<String> userNameRq){
		generaliLoginDao.updateFailedAttempt(userNameRq);
	}
	
	@Transactional
	public void resetAttempts(Request<String> userNameRq){
		generaliLoginDao.resetAttempts(userNameRq);
	}

}
