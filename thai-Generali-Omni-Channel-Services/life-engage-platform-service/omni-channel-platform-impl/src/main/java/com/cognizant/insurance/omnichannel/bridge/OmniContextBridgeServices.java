package com.cognizant.insurance.omnichannel.bridge;

public interface OmniContextBridgeServices {
	public String getValueForCode(String code,String language,String type);
}
