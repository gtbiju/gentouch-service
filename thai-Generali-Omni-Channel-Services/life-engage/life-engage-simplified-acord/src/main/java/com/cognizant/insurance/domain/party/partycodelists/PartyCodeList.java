/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304007
 * @version       : 0.1, Dec 14, 2012
 */
package com.cognizant.insurance.domain.party.partycodelists;

import java.util.Set;

/**
 * This class holds all the code lists corresponding to party.
 */
public class PartyCodeList {
    
    /** The id. */
    private Long id;
    
    /** The code list name. */
    private String codeListName;
    
    /** The code list values. */
    private Set<PartyCodeListValues> codeListValues;

    /**
     * Gets the id.
     *
     * @return Returns the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets The id.
     *
     * @param id The id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the codeListName.
     *
     * @return Returns the codeListName.
     */
    public String getCodeListName() {
        return codeListName;
    }

    /**
     * Sets The codeListName.
     *
     * @param codeListName The codeListName to set.
     */
    public void setCodeListName(final String codeListName) {
        this.codeListName = codeListName;
    }

    /**
     * Gets the codeListValues.
     *
     * @return Returns the codeListValues.
     */
    public Set<PartyCodeListValues> getCodeListValues() {
        return codeListValues;
    }

    /**
     * Sets The codeListValues.
     *
     * @param codeListValues The codeListValues to set.
     */
    public void setCodeListValues(final Set<PartyCodeListValues> codeListValues) {
        this.codeListValues = codeListValues;
    }

}
