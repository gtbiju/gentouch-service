package com.cognizant.insurance.omnichannel.component.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.generali.security.component.GeneraliLoginComponent;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.repository.UserRepository;

public class GeneraliLoginComponentImpl implements GeneraliLoginComponent {
	
	/** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliLoginComponentImpl.class);
    
    @Autowired
    private UserRepository userRepository;

    
    public final boolean validateAgentLicenseExpiry(String agentCode) {

    	GeneraliAgent agent = getAgentDetails(agentCode);

    	SimpleDateFormat sdf = new SimpleDateFormat(GeneraliConstants.DATE_FORMAT);
    	String date = sdf.format(new Date());

    	if(agent != null){
    		if (agent.getLicenseExpireDate().compareTo(date) < 0) {
    			return true;
    		}
    	} else {
    		GeneraliGAOAgent gaoAgent = getGAOAgentDetails(agentCode);		
    		if(gaoAgent != null && gaoAgent.getLicenseExpireDate().compareTo(date) < 0) {
    			return true;
    		}
    	}
    	return false;
    }

	public final boolean validateAgentStatus(String agentCode) {

		GeneraliAgent agent = getAgentDetails(agentCode);

		if(agent != null){
			if (!GeneraliConstants.INFORCE.equals(agent.getAgentStatus())) {
				return true;
			}
		} else {
    		GeneraliGAOAgent gaoAgent = getGAOAgentDetails(agentCode);		
    		if (gaoAgent != null && !GeneraliConstants.INFORCE.equals(gaoAgent.getAgentStatus())) {
				return true;
			}
    	}
		return false;
	}

	private final GeneraliAgent getAgentDetails(String agentCode) {

		LOGGER.debug("METHOD ENTRY : getAgentDetails ");

		LOGGER.debug("getAgentDetails userName>>" +agentCode);

		GeneraliAgent agent = userRepository.agentProfileLoginValidation(agentCode);

		LOGGER.debug("METHOD EXIT :getAgentDetails");
		return agent;
	}	
	
	private final GeneraliGAOAgent getGAOAgentDetails(String agentCode) {

		LOGGER.debug("METHOD ENTRY : getGAOAgentDetails ");

		LOGGER.debug("getGAOAgentDetails userName>>" +agentCode);

		GeneraliGAOAgent agent = userRepository.gaoAgentProfileLoginValidation(agentCode);

		LOGGER.debug("METHOD EXIT :getGAOAgentDetails");
		return agent;
	}	
}
