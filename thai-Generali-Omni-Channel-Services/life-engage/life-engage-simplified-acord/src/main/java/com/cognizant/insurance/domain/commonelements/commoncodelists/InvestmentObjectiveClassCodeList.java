/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of Funds according to their investment objective
 * class.
 * 
 * @author 301350
 * 
 * 
 */
public enum InvestmentObjectiveClassCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    TechnologySharesInEuroMarket,
    /**
     * 
     * 
     * 
     * 
     */
    EuroBonds,
    /**
     * 
     * 
     * 
     * 
     */
    UsdBonds,
    /**
     * 
     * 
     * 
     * 
     */
    SharesInBricMarkets
}
