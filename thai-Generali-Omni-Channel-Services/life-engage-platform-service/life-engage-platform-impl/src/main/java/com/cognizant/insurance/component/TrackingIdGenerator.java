package com.cognizant.insurance.component;

public interface TrackingIdGenerator {
	
	public Long generate(String context);
	public Long generate();
	

}
