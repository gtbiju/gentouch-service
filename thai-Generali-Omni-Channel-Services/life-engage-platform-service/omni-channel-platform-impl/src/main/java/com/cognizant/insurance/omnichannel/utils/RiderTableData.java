package com.cognizant.insurance.omnichannel.utils;

/**
 * @author 471501
 * 
 *         The Class RiderTableData
 * 
 *         This class is a POJO for Rider details in illustration PDF..
 * 
 */
public class RiderTableData {

	private String riderName;

	private String lifeAssured;

	private String sumAssured;

	private String premiumAndPolicyTerm;

	private String yearlyPremium;

	private String totalPremium;

	private String totalPremiumYearly;

	private String totalPremiumHalfYearly;

	private String totalPremiumQuaterly;
	
	private String description;
	
	private String ppt;
	
	private String policyYr;
	
	private boolean riderSingle;
	
	private String mainInsuredPremium;
	
	private String addIns1Premium;
	
	private String addIns2Premium;

	private String addIns3Premium;
	
	private String addIns4Premium;

	private String firstAddInsuredName;
	
	private String secondAddInsuredName;

	private String thirdAddInsuredName;

	private String fourthAddInsuredName;

	/**
	 * @return the riderName
	 */
	public String getRiderName() {
		return riderName;
	}

	/**
	 * @param riderName
	 *            the riderName to set
	 */
	public void setRiderName(String riderName) {
		this.riderName = riderName;
	}

	/**
	 * @return the lifeAssured
	 */
	public String getLifeAssured() {
		return lifeAssured;
	}

	/**
	 * @param lifeAssured
	 *            the lifeAssured to set
	 */
	public void setLifeAssured(String lifeAssured) {
		this.lifeAssured = lifeAssured;
	}

	/**
	 * @return the sumAssured
	 */
	public String getSumAssured() {
		return sumAssured;
	}

	/**
	 * @param sumAssured
	 *            the sumAssured to set
	 */
	public void setSumAssured(String sumAssured) {
		this.sumAssured = sumAssured;
	}

	/**
	 * @return the premiumAndPolicyTerm
	 */
	public String getPremiumAndPolicyTerm() {
		return premiumAndPolicyTerm;
	}

	/**
	 * @param premiumAndPolicyTerm
	 *            the premiumAndPolicyTerm to set
	 */
	public void setPremiumAndPolicyTerm(String premiumAndPolicyTerm) {
		this.premiumAndPolicyTerm = premiumAndPolicyTerm;
	}

	/**
	 * @return the yearlyPremium
	 */
	public String getYearlyPremium() {
		return yearlyPremium;
	}

	/**
	 * @param yearlyPremium
	 *            the yearlyPremium to set
	 */
	public void setYearlyPremium(String yearlyPremium) {
		this.yearlyPremium = yearlyPremium;
	}

	/**
	 * @return the totalPremium
	 */
	public String getTotalPremium() {
		return totalPremium;
	}

	/**
	 * @param totalPremium
	 *            the totalPremium to set
	 */
	public void setTotalPremium(String totalPremium) {
		this.totalPremium = totalPremium;
	}

	/**
	 * @return the totalPremiumYearly
	 */
	public String getTotalPremiumYearly() {
		return totalPremiumYearly;
	}

	/**
	 * @param totalPremiumYearly
	 *            the totalPremiumYearly to set
	 */
	public void setTotalPremiumYearly(String totalPremiumYearly) {
		this.totalPremiumYearly = totalPremiumYearly;
	}

	/**
	 * @return the totalPremiumHalfYearly
	 */
	public String getTotalPremiumHalfYearly() {
		return totalPremiumHalfYearly;
	}

	/**
	 * @param totalPremiumHalfYearly
	 *            the totalPremiumHalfYearly to set
	 */
	public void setTotalPremiumHalfYearly(String totalPremiumHalfYearly) {
		this.totalPremiumHalfYearly = totalPremiumHalfYearly;
	}

	/**
	 * @return the totalPremiumQuaterly
	 */
	public String getTotalPremiumQuaterly() {
		return totalPremiumQuaterly;
	}

	/**
	 * @param totalPremiumQuaterly
	 *            the totalPremiumQuaterly to set
	 */
	public void setTotalPremiumQuaterly(String totalPremiumQuaterly) {
		this.totalPremiumQuaterly = totalPremiumQuaterly;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param riderSingle the riderSingle to set
	 */
	public void setRiderSingle(boolean riderSingle) {
		this.riderSingle = riderSingle;
	}

	/**
	 * @return the riderSingle
	 */
	public boolean isRiderSingle() {
		return riderSingle;
	}

	/**
	 * @param ppt the ppt to set
	 */
	public void setPpt(String ppt) {
		this.ppt = ppt;
	}

	/**
	 * @return the ppt
	 */
	public String getPpt() {
		return ppt;
	}

	/**
	 * @param policyYr the policyYr to set
	 */
	public void setPolicyYr(String policyYr) {
		this.policyYr = policyYr;
	}

	/**
	 * @return the policyYr
	 */
	public String getPolicyYr() {
		return policyYr;
	}

	public String getMainInsuredPremium() {
		return mainInsuredPremium;
	}

	public void setMainInsuredPremium(String mainInsuredPremium) {
		this.mainInsuredPremium = mainInsuredPremium;
	}

	public String getAddIns1Premium() {
		return addIns1Premium;
	}

	public void setAddIns1Premium(String addIns1Premium) {
		this.addIns1Premium = addIns1Premium;
	}

	public String getAddIns2Premium() {
		return addIns2Premium;
	}

	public void setAddIns2Premium(String addIns2Premium) {
		this.addIns2Premium = addIns2Premium;
	}

	public String getAddIns3Premium() {
		return addIns3Premium;
	}

	public void setAddIns3Premium(String addIns3Premium) {
		this.addIns3Premium = addIns3Premium;
	}

	public String getAddIns4Premium() {
		return addIns4Premium;
	}

	public void setAddIns4Premium(String addIns4Premium) {
		this.addIns4Premium = addIns4Premium;
	}

	public String getFirstAddInsuredName() {
		return firstAddInsuredName;
	}

	public void setFirstAddInsuredName(String firstAddInsuredName) {
		this.firstAddInsuredName = firstAddInsuredName;
	}

	public String getSecondAddInsuredName() {
		return secondAddInsuredName;
	}

	public void setSecondAddInsuredName(String secondAddInsuredName) {
		this.secondAddInsuredName = secondAddInsuredName;
	}

	public String getThirdAddInsuredName() {
		return thirdAddInsuredName;
	}

	public void setThirdAddInsuredName(String thirdAddInsuredName) {
		this.thirdAddInsuredName = thirdAddInsuredName;
	}

	public String getFourthAddInsuredName() {
		return fourthAddInsuredName;
	}

	public void setFourthAddInsuredName(String fourthAddInsuredName) {
		this.fourthAddInsuredName = fourthAddInsuredName;
	}

}
