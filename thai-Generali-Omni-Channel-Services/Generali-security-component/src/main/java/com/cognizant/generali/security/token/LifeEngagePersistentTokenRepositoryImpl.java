/**
*
* Copyright 2012, Cognizant
*
* @author        : 300797
* @version       : 0.1, Jul 20, 2015
*/

package com.cognizant.generali.security.token;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.cognizant.le.security.component.LifeEngageValidationComponent;
import com.cognizant.le.security.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.le.security.exception.DaoException;
import com.cognizant.le.security.request.Request;
import com.cognizant.le.security.request.impl.JPARequestImpl;
import com.cognizant.le.security.response.Response;
import com.cognizant.le.security.response.vo.TokenInfo;

/**
* The Class class CustomPersistingTokenRepository.
*/
public class LifeEngagePersistentTokenRepositoryImpl implements PersistentTokenRepository {

   /** The Constant LOGGER. */
   private static final Logger LOGGER = LoggerFactory.getLogger(LifeEngagePersistentTokenRepositoryImpl.class);

   /** The login component. */
   @Autowired
   private LifeEngageValidationComponent validationComponent;

   /** The entity manager. */
   @PersistenceContext(unitName="LE_Platform")
   private EntityManager entityManager;

   /**
    * Will call the service URL $contextRoot/auth/token/save.
    * 
    * @param token
    *            the token
    */
   @Override
   public void createNewToken(PersistentRememberMeToken token) {

       LOGGER.debug("METHOD ENTRY : CustomPersistingTokenRepository.createNewToken with token details : "
               + token.toString());
       TokenInfo tokenInfo = new TokenInfo();
       copyToTokenDetails(token, tokenInfo);
       final Request<TokenInfo> request =
               new JPARequestImpl<TokenInfo>(entityManager, ContextTypeCodeList.SAVE_TOKEN, null);
       request.setType(tokenInfo);
       try {
           validationComponent.saveToken(request);
       } catch (DaoException e) {
           LOGGER.error("Failed to Save TOKEN", e);
       }
       LOGGER.debug("METHOD EXIT : CustomPersistingTokenRepository.createNewToken ");
   }

   /*
    * (non-Javadoc)
    * 
    * @see
    * org.springframework.security.web.authentication.rememberme.PersistentTokenRepository#updateToken(java.lang.String
    * , java.lang.String, java.util.Date)
    */
   @Override
   public void updateToken(String series, String tokenValue, Date lastUsed) {
       LOGGER.debug("METHOD ENTRY : CustomPersistingTokenRepository.updateToken with series: " + series);
       TokenInfo tokenInfo = new TokenInfo();
       copyToTokenDetails(series, lastUsed, tokenInfo);
       final Request<TokenInfo> request =
               new JPARequestImpl<TokenInfo>(entityManager, ContextTypeCodeList.UPDATE_TOKEN, null);
       request.setType(tokenInfo);
       try {
           validationComponent.updateToken(request);
       } catch (DaoException e) {
           LOGGER.error("Failed to update TOKEN", e);
       }
       LOGGER.debug("METHOD EXIT : CustomPersistingTokenRepository.updateToken ");
   }

   /**
    * Will call the service URL $contextRoot/auth/token.
    * 
    * @param seriesId
    *            the series id
    * @return the token for series
    */
   @Override
   public PersistentRememberMeToken getTokenForSeries(String seriesId) {
       LOGGER.info("METHOD ENTRY : CustomPersistingTokenRepository.getTokenDetails with seriesId : " + seriesId);

       PersistentRememberMeToken persistentRememberMeToken = null;
       final Request<String> request = new JPARequestImpl<String>(entityManager, ContextTypeCodeList.SAVE_TOKEN, null);
       request.setType(seriesId);
       try {
           Response<TokenInfo> response = validationComponent.getTokenDetails(request);
           TokenInfo tokenInfo = response.getType();
           persistentRememberMeToken =
                   new PersistentRememberMeToken(tokenInfo.getUsername(), tokenInfo.getSeries(), tokenInfo.getToken(),
                           tokenInfo.getLastUsedAt());
       } catch (DaoException e) {
           LOGGER.error("Failed to get token for series", e);
       }
       LOGGER.debug("METHOD EXIT : CustomPersistingTokenRepository.getTokenForSeries ");

       return persistentRememberMeToken;

   }

   /*
    * (non-Javadoc)
    * 
    * @see
    * org.springframework.security.web.authentication.rememberme.PersistentTokenRepository#removeUserTokens(java.lang
    * .String)
    */
   @Override
   public void removeUserTokens(String username) {
       LOGGER.debug("Do nothing");
   }

   /**
    * Copy to token details.
    * 
    * @param token
    *            the token
    * @param tokenDetails
    *            the token details
    */
   private void copyToTokenDetails(PersistentRememberMeToken token, TokenInfo tokenDetails) {
       tokenDetails.setUsername(token.getUsername());
       tokenDetails.setSeries(token.getSeries());
       tokenDetails.setToken(token.getTokenValue());
       tokenDetails.setLastUsedAt(token.getDate());
   }

   /**
    * Copy to token details.
    * 
    * @param series
    *            the series
    * @param lastUsed
    *            the last used
    * @param tokenDetails
    *            the token details
    */
   private void copyToTokenDetails(String series, Date lastUsed, TokenInfo tokenDetails) {
       tokenDetails.setSeries(series);
       tokenDetails.setLastUsedAt(lastUsed);
   }
}
