package com.cognizant.insurance.domain.agreement;

import java.util.Set;

public class CessTerm {
	
	 /** For cess term from rule. */
    private String premiumCessTerm;
    
    private String benefitCessTerm;
    
    private String riskCessTerm;
    
    private String riderPlanCode;
    
    private Set<CessTerm> riderCessTermSet;

	public Set<CessTerm> getRiderCessTermSet() {
		return riderCessTermSet;
	}

	public void setRiderCessTermSet(Set<CessTerm> riderCessTermSet) {
		this.riderCessTermSet = riderCessTermSet;
	}

	public String getRiskCessTerm() {
		return riskCessTerm;
	}

	public void setRiskCessTerm(String riskCessTerm) {
		this.riskCessTerm = riskCessTerm;
	}

	public String getBenefitCessTerm() {
		return benefitCessTerm;
	}

	public void setBenefitCessTerm(String benefitCessTerm) {
		this.benefitCessTerm = benefitCessTerm;
	}

	public String getPremiumCessTerm() {
		return premiumCessTerm;
	}

	public void setPremiumCessTerm(String premiumCessTerm) {
		this.premiumCessTerm = premiumCessTerm;
	}



	public String getRiderPlanCode() {
		return riderPlanCode;
	}

	public void setRiderPlanCode(String riderPlanCode) {
		this.riderPlanCode = riderPlanCode;
	}

}
