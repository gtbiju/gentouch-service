/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.partyrolerelationshipsubtypes;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.HomeOwnershipCodeList;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.agent.domain.roleandrelationship.PartyRoleRelationship;

/**
 * A set of individuals usually resident at the same address and members of the
 * same nuclear or extended family, who share a common economic interest and who
 * are to be treated for certain purposes (e.g: marketing campaigns) as a unit.
 * 
 * e.g: Smith Household.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class HouseholdRelationship extends PartyRoleRelationship {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -429483123728480233L;

    /**
     * The tenancy status of the main residence. Is derived via the object
     * ownership of a physical object that is a dwelling and where that dwelling
     * is located at, via physical object location, the same place as the
     * residence for the household.
     * 
     * e.g: Own
     * 
     * e.g: Rent
     * 
     * 
     * 
     */
    @Enumerated
    private HomeOwnershipCodeList homeOwnership;

    /**
     * The declared number of adults that are dependent from the household.
     * 
     * 
     * 
     */
    private BigDecimal dependentAdultCount;

    /**
     * The declared number of children that are dependent from the household.
     * 
     * 
     * 
     */
    private BigDecimal dependentChildrenCount;

    /**
     * The birth date of the youngest dependent child.
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date youngestDependentChildBirthDate;

    /**
     * The birth date of the eldest dependent child. 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date oldestDependentChildBirthDate;

    /**
     * The birth date of the youngest dependent adult.
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date youngestDependentAdultBirthDate;

    /**
     * The birth date of the eldest dependent adult. 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date oldestDependentAdultBirthDate;

    /**
     * Indicates whether or not the household contains a couple of whom one is
     * designated as the head of the household as well as at least one child of
     * the couple.
     * 
     * 
     * 
     */
    private Boolean familyIndicator;

    /**
     * The most recent known gross income of this household. Would only be
     * derivable from the gross income of the persons that are the household
     * members, provided this information is available for all members of the
     * household.
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount grossIncomeAmount;

    /**
     * The amount of money that this household can spend after having paid all
     * the fixed expenses such as rent, mortgage repayment and so on. This
     * information should be provided by the household when not all composing
     * elements for calculating the disposable income are available.
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount disposableIncomeAmount;

    /**
     * The name of the household.
     * 
     * e.g. The Adams Family
     * 
     * 
     * 
     */
    private String name;

    /**
     * Gets the homeOwnership.
     * 
     * @return Returns the homeOwnership.
     */
    public HomeOwnershipCodeList getHomeOwnership() {
        return homeOwnership;
    }

    /**
     * Gets the dependentAdultCount.
     * 
     * @return Returns the dependentAdultCount.
     */
    public BigDecimal getDependentAdultCount() {
        return dependentAdultCount;
    }

    /**
     * Gets the dependentChildrenCount.
     * 
     * @return Returns the dependentChildrenCount.
     */
    public BigDecimal getDependentChildrenCount() {
        return dependentChildrenCount;
    }

    /**
     * Gets the youngestDependentChildBirthDate.
     * 
     * @return Returns the youngestDependentChildBirthDate.
     */
    public Date getYoungestDependentChildBirthDate() {
        return youngestDependentChildBirthDate;
    }

    /**
     * Gets the oldestDependentChildBirthDate.
     * 
     * @return Returns the oldestDependentChildBirthDate.
     */
    public Date getOldestDependentChildBirthDate() {
        return oldestDependentChildBirthDate;
    }

    /**
     * Gets the youngestDependentAdultBirthDate.
     * 
     * @return Returns the youngestDependentAdultBirthDate.
     */
    public Date getYoungestDependentAdultBirthDate() {
        return youngestDependentAdultBirthDate;
    }

    /**
     * Gets the oldestDependentAdultBirthDate.
     * 
     * @return Returns the oldestDependentAdultBirthDate.
     */
    public Date getOldestDependentAdultBirthDate() {
        return oldestDependentAdultBirthDate;
    }

    /**
     * Gets the familyIndicator.
     * 
     * @return Returns the familyIndicator.
     */
    public Boolean getFamilyIndicator() {
        return familyIndicator;
    }

    /**
     * Gets the grossIncomeAmount.
     * 
     * @return Returns the grossIncomeAmount.
     */
    public CurrencyAmount getGrossIncomeAmount() {
        return grossIncomeAmount;
    }

    /**
     * Gets the disposableIncomeAmount.
     * 
     * @return Returns the disposableIncomeAmount.
     */
    public CurrencyAmount getDisposableIncomeAmount() {
        return disposableIncomeAmount;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The homeOwnership.
     * 
     * @param homeOwnership
     *            The homeOwnership to set.
     */
    public void
            setHomeOwnership(final HomeOwnershipCodeList homeOwnership) {
        this.homeOwnership = homeOwnership;
    }

    /**
     * Sets The dependentAdultCount.
     * 
     * @param dependentAdultCount
     *            The dependentAdultCount to set.
     */
    public void setDependentAdultCount(
            final BigDecimal dependentAdultCount) {
        this.dependentAdultCount = dependentAdultCount;
    }

    /**
     * Sets The dependentChildrenCount.
     * 
     * @param dependentChildrenCount
     *            The dependentChildrenCount to set.
     */
    public void setDependentChildrenCount(
            final BigDecimal dependentChildrenCount) {
        this.dependentChildrenCount = dependentChildrenCount;
    }

    /**
     * Sets The youngestDependentChildBirthDate.
     * 
     * @param youngestDependentChildBirthDate
     *            The youngestDependentChildBirthDate to set.
     */
    public void setYoungestDependentChildBirthDate(
            final Date youngestDependentChildBirthDate) {
        this.youngestDependentChildBirthDate = youngestDependentChildBirthDate;
    }

    /**
     * Sets The oldestDependentChildBirthDate.
     * 
     * @param oldestDependentChildBirthDate
     *            The oldestDependentChildBirthDate to set.
     */
    public void setOldestDependentChildBirthDate(
            final Date oldestDependentChildBirthDate) {
        this.oldestDependentChildBirthDate = oldestDependentChildBirthDate;
    }

    /**
     * Sets The youngestDependentAdultBirthDate.
     * 
     * @param youngestDependentAdultBirthDate
     *            The youngestDependentAdultBirthDate to set.
     */
    public void setYoungestDependentAdultBirthDate(
            final Date youngestDependentAdultBirthDate) {
        this.youngestDependentAdultBirthDate = youngestDependentAdultBirthDate;
    }

    /**
     * Sets The oldestDependentAdultBirthDate.
     * 
     * @param oldestDependentAdultBirthDate
     *            The oldestDependentAdultBirthDate to set.
     */
    public void setOldestDependentAdultBirthDate(
            final Date oldestDependentAdultBirthDate) {
        this.oldestDependentAdultBirthDate = oldestDependentAdultBirthDate;
    }

    /**
     * Sets The familyIndicator.
     * 
     * @param familyIndicator
     *            The familyIndicator to set.
     */
    public void setFamilyIndicator(final Boolean familyIndicator) {
        this.familyIndicator = familyIndicator;
    }

    /**
     * Sets The grossIncomeAmount.
     * 
     * @param grossIncomeAmount
     *            The grossIncomeAmount to set.
     */
    public void setGrossIncomeAmount(final CurrencyAmount grossIncomeAmount) {
        this.grossIncomeAmount = grossIncomeAmount;
    }

    /**
     * Sets The disposableIncomeAmount.
     * 
     * @param disposableIncomeAmount
     *            The disposableIncomeAmount to set.
     */
    public void setDisposableIncomeAmount(
            final CurrencyAmount disposableIncomeAmount) {
        this.disposableIncomeAmount = disposableIncomeAmount;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

}
