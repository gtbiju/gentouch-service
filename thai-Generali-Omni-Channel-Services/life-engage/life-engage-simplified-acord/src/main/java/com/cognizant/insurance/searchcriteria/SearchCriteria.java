/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Jul 29, 2015
 */
package com.cognizant.insurance.searchcriteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The Class class SearchCriteria.
 */
public class SearchCriteria implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4331778853873528710L;

    /** The command. */
    private String command;

    /** The modes. */
    private ArrayList<String> modes;

    /** The value. */
    private String value;

    /** The agent id. */
    private String agentId;
    
    private String branchId;

    /** The type. */
    private String type;
    
    private String gaoId;
    
    private String branchAdminId;
    
    private String gaoOfficeCode;
    
    private Date startDate;
	
	private Date currentDate;
	
	private Date currentMonth;
	
	private String agentType;

    /** The transTrackingIdList. */
    private List<String> transTrackingIdList;
    
    /** The selectedIds. */
    private ArrayList<String> selectedIds;
    
    private List<String> agentCodes;

    public List<String> getAgentCodes() {
        return agentCodes;
    }

    public void setAgentCodes(List<String> agentCodes) {
        this.agentCodes = agentCodes;
    }

    public List<String> getTransTrackingIdList() {
        return transTrackingIdList;
    }

    public void setTransTrackingIdList(List<String> transTrackingIdList) {
        this.transTrackingIdList = transTrackingIdList;
    }

    /**
     * Gets the command.
     * 
     * @return the command
     */
    public String getCommand() {
        return command;
    }

    /**
     * Sets the command.
     * 
     * @param command
     *            the command to set.
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * Gets the modes.
     * 
     * @return the modes
     */
    public ArrayList<String> getModes() {
        return modes;
    }

    /**
     * Sets the modes.
     * 
     * @param modes
     *            the modes to set.
     */
    public void setModes(ArrayList<String> modes) {
        this.modes = modes;
    }

    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     * 
     * @param value
     *            the value to set.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the agent id.
     * 
     * @return the agent id
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * Sets the agent id.
     * 
     * @param agentId
     *            the agent id to set.
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     * 
     * @param type
     *            the type to set.
     */
    public void setType(String type) {
        this.type = type;
    }
    
    /**
     * Gets the selectedIds.
     *
     * @return Returns the selectedIds.
     */
    public ArrayList<String> getSelectedIds() {
        return selectedIds;
    }

    /**
     * Sets The selectedIds.
     *
     * @param selectedIds The selectedIds to set.
     */
    public void setSelectedIds(ArrayList<String> selectedIds) {
        this.selectedIds = selectedIds;
    }

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param currentDate the currentDate to set
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * @return the currentDate
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentMonth the currentMonth to set
	 */
	public void setCurrentMonth(Date currentMonth) {
		this.currentMonth = currentMonth;
	}

	/**
	 * @return the currentMonth
	 */
	public Date getCurrentMonth() {
		return currentMonth;
	}

	/**
	 * @param agentType the agentType to set
	 */
	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	/**
	 * @return the agentType
	 */
	public String getAgentType() {
		return agentType;
	}

	/**
	 * @param branchId the branchId to set
	 */
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	/**
	 * @return the branchId
	 */
	public String getBranchId() {
		return branchId;
	}

    /**
     * Gets the gaoId.
     *
     * @return Returns the gaoId.
     */
    public String getGaoId() {
        return gaoId;
    }

    /**
     * Sets The gaoId.
     *
     * @param gaoId The gaoId to set.
     */
    public void setGaoId(String gaoId) {
        this.gaoId = gaoId;
    }

    /**
     * Gets the branchAdminId.
     *
     * @return Returns the branchAdminId.
     */
    public String getBranchAdminId() {
        return branchAdminId;
    }

    /**
     * Sets The branchAdminId.
     *
     * @param branchAdminId The branchAdminId to set.
     */
    public void setBranchAdminId(String branchAdminId) {
        this.branchAdminId = branchAdminId;
    }

    /**
     * Gets the gaoOfficeCode.
     *
     * @return Returns the gaoOfficeCode.
     */
    public String getGaoOfficeCode() {
        return gaoOfficeCode;
    }

    /**
     * Sets The gaoOfficeCode.
     *
     * @param gaoOfficeCode The gaoOfficeCode to set.
     */
    public void setGaoOfficeCode(String gaoOfficeCode) {
        this.gaoOfficeCode = gaoOfficeCode;
    }

    /**
     * Gets the serialversionuid.
     *
     * @return Returns the serialversionuid.
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
