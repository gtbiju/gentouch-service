/**
 * 
 *//*
package com.cognizant.le.model.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
//import org.hibernate.Query;
//import org.hibernate.Session;
import org.json.simple.JSONObject;
import com.cognizant.le.model.dto.AppContext;
import com.cognizant.le.model.dto.ContentFile;
import com.cognizant.le.model.dto.ContentFileDTO;
import com.cognizant.le.model.dto.ContentLanguage;
import com.cognizant.le.model.dto.ContentLanguageDTO;
import com.cognizant.le.model.dto.ContentType;
import com.cognizant.le.model.dto.ContentTypeDTO;
//import com.cognizant.le.model.dto.FileBean;
import com.cognizant.le.model.dto.FileInfo;
//import com.cognizant.le.util.HibernateUtil;

*//**
 * @author 291422
 * 
 *//*
public class ContentAdminDAOImpl implements ContentAdminDAO, Serializable {

	*//**
	 * 
	 *//*
	private static final long serialVersionUID = -7501078092966814434L;
	
	static Logger logger = Logger.getLogger(ContentAdminDAOImpl.class);

	*//**
	 * This method is used to insert content.
	 * 
	 *//*
	@Override
	public Boolean insertContent(ContentFileDTO contentFileDTO)
			throws Exception {
		Session session = null;
		boolean isSuccess = true;
		if (contentFileDTO != null) {
			try {
				ContentFile contentFile = new ContentFile();
				ContentType contentType = new ContentType();
				contentType.setId(contentFileDTO.getContentType().getId());
				ContentLanguage contentLanguage = new ContentLanguage();

				if (contentFileDTO.getContentLanguage() == null) {
					List<ContentLanguageDTO> contentDtos = new CommonAdminDAOImpl().retriveLanguage();
					for (ContentLanguageDTO contentLanguageDTO : contentDtos) {
						if (contentLanguageDTO.getLanguage().equalsIgnoreCase(
								"english")) {
							contentLanguage.setId(contentLanguageDTO.getId());
							contentLanguage.setLanguage(contentLanguageDTO
									.getLanguage());
						}
					}
				}

				else {
					contentLanguage.setId(contentFileDTO.getContentLanguage().getId());
				}
				contentFile.setType(contentType);
				contentFile.setFileName(contentFileDTO.getFileName());
				contentFile.setVersion(contentFileDTO.getVersion());
				contentFile.setLastUpdated(contentFileDTO.getLastUpdated());
				contentFile.setPublicURL(contentFileDTO.getPublicURL());
				contentFile.setComments(contentFileDTO.getComments());
				contentFile.setAppContext(contentFileDTO.getAppContexts());
				contentFile.setContentLanguage(contentLanguage);
				contentFile.setIsRequired(contentFileDTO.getIsRequired());
				contentFile.setFileSize(contentFileDTO.getFileSize());
				contentFile.setDescription(contentFileDTO.getFileDesc());
				session = HibernateUtil.getSession();
				session.beginTransaction();
				session.saveOrUpdate(contentFile);
				session.getTransaction().commit();
			} catch (Exception e) {
				session.getTransaction().rollback();
				isSuccess = false;
				throw e;
			} finally {
				if (session != null) {
					session.close();
				}
			}
		}
		return isSuccess;
	}

	*//**
	 * This method is used to Retrive Content from data base.
	 * 
	 *//*

	@Override
	public List<ContentFileDTO> retriveContent(FileInfo fileInfo) throws Exception {

		Session session = null;
		List<ContentFileDTO> contentList = new ArrayList<ContentFileDTO>();
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchSortedContentFiles");
			List<ContentFile> contentFileList = query.list();

			for (ContentFile contentInfo : contentFileList) {
				ContentFileDTO contentFileDTO = new ContentFileDTO();
				contentFileDTO.setFileName(contentInfo.getFileName());

				ContentTypeDTO contentTypeDTO = new ContentTypeDTO();
				contentTypeDTO.setContentType(contentInfo.getContentType()
						.getType());
				contentTypeDTO.setId(contentInfo.getContentType().getId());

				contentFileDTO.setContentType(contentTypeDTO);
				ContentLanguageDTO contentLanguageDTO = new ContentLanguageDTO();
				if (contentInfo.getContentLanguage() != null) {
					contentLanguageDTO.setId(contentInfo.getContentLanguage().getId());
					contentLanguageDTO.setLanguage(contentInfo.getContentLanguage().getLanguage());
					contentFileDTO.setContentLanguage(contentLanguageDTO);
				}
				contentFileDTO.setVersion(contentInfo.getVersion());
				contentFileDTO.setLastUpdated(contentInfo.getLastUpdated());
				contentFileDTO.setAppContexts(contentInfo.getAppContext());
				contentFileDTO.setAppContext(contentInfo.getAppContext());
				contentFileDTO.setIsRequired(contentInfo.getIsRequired());
				String downloadUrl = buildDownloadUrl(contentInfo.getPublicURL(),fileInfo);
				contentFileDTO.setDownloadUrl(downloadUrl);
				contentFileDTO.setFileDesc(contentInfo.getDescription());
				contentFileDTO.setFileSize(contentInfo.getFileSize());
				contentList.add(contentFileDTO);
			}
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return contentList;
	}



	private String buildDownloadUrl(String publicURL, FileInfo fileInfo) {
		StringBuffer downloadURL = new StringBuffer(fileInfo.getServerUrl());
		if(!(fileInfo.getServerUrl().endsWith("/"))) {
			downloadURL.append("/");
		} 
		StringBuilder contextPath= new StringBuilder(fileInfo.getContextPath());
		if(contextPath.charAt(0)=='/'){
			contextPath.deleteCharAt(0);
		}
		downloadURL.append(contextPath);
		return downloadURL+publicURL;
	}

	*//**
	 * This method is used to Retrive file with filter from DB.
	 * 
	 *//*
	@Override
	public List<ContentFileDTO> retriveContent(String filter, FileInfo fileInfo) throws Exception {
		Session session = null;
		List<ContentFileDTO> contentList = new ArrayList<ContentFileDTO>();
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("filteredContentFiles");
			query.setParameter("filter", filter);
			List<ContentFile> contentFileList = query.list();
			for (ContentFile contentFile : contentFileList) {
				ContentFileDTO contentFileDTO = new ContentFileDTO();
				contentFileDTO.setFileName(contentFile.getFileName());

				ContentTypeDTO contentTypeDTO = new ContentTypeDTO();
				contentTypeDTO.setContentType(contentFile.getContentType()
						.getType());
				contentTypeDTO.setId(contentFile.getContentType().getId());

				contentFileDTO.setContentType(contentTypeDTO);
				ContentLanguageDTO contentLanguageDTO = new ContentLanguageDTO();
				if (contentFile.getContentLanguage() != null) {
					contentLanguageDTO.setId(contentFile.getContentLanguage().getId());
					contentLanguageDTO.setLanguage(contentFile.getContentLanguage().getLanguage());
					contentFileDTO.setContentLanguage(contentLanguageDTO);
				} 
				contentFileDTO.setVersion(contentFile.getVersion());
				contentFileDTO.setLastUpdated(contentFile.getLastUpdated());
				contentFileDTO.setAppContexts(contentFile.getAppContext());
				contentFileDTO.setAppContext(contentFile.getAppContext());
				contentFileDTO.setIsRequired(contentFile.getIsRequired());
				String downloadUrl = buildDownloadUrl(contentFile.getPublicURL(),fileInfo);
				contentFileDTO.setDownloadUrl(downloadUrl);
				contentList.add(contentFileDTO);
			}
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return contentList;
	}



	*//**
	 * This method is used to Update the updated file to DB.
	 * 
	 *//*
	@Override
	public Boolean updateContent(ContentFileDTO contentFileDTO,
			FileBean fileBean) throws Exception {

		boolean isSuccess = true;
		Session session = null;

		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchContentFileByFileName");
			query.setParameter("fileName", contentFileDTO.getFileName());
			ContentFile contentFile = (ContentFile) query.list().get(0);
			fileBean.setPrevVersionNumber(contentFile.getVersion());
			contentFile.setVersion(contentFile.getVersion() + 1);
			if (saveFile(fileBean)) {
				contentFile
						.setLastUpdated(new Date(System.currentTimeMillis()));
				contentFile.setComments(contentFileDTO.getComments());
				contentFile.setDescription(contentFileDTO.getFileDesc());
				contentFile.setFileSize(contentFileDTO.getFileSize());
				session.saveOrUpdate(contentFile);
				session.getTransaction().commit();
			} else {
				return false;
			}
		} catch (Exception e) {
			session.getTransaction().rollback();
			isSuccess = false;
			throw e;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return isSuccess;
	}

	*//**
	 * This method is used to check file already exists in DB before insert.
	 * 
	 *//*
	@Override
	public Boolean isContentExists(ContentFileDTO contentFileDTO)
			throws Exception {

		Session session = null;
		Boolean result = false;
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchContentFileByFileName");
			query.setParameter("fileName", contentFileDTO.getFileName());
			if (query.list().size() > 0) {
				result = true;
			}
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return result;
	}

	public Boolean saveFile(FileBean fileBean) throws IOException {
		try {
			File uploadDir;
			uploadDir = new File(fileBean.getContentFolder() + File.separator
					+ fileBean.getPrevVersionNumber());
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}
			File oldVersion = new File(fileBean.getContentFolder(),
					fileBean.getFileName());
			if (oldVersion
					.renameTo(new File(uploadDir, fileBean.getFileName()))) {
				logger.info("File is moved successful!");
			} else {
				logger.info("File is failed to move!");
				return false;
			}

			if (fileBean.getContentFolder() != null
					&& fileBean.getFileName() != null
					&& fileBean.getInputStream() != null) {

				File uploadFile = new File(fileBean.getContentFolder(),
						fileBean.getFileName());
				FileOutputStream fileOutputStream = new FileOutputStream(
						uploadFile);
				int read = 0;
				byte[] bytes = new byte[1024];

				while ((read = fileBean.getInputStream().read(bytes)) != -1) {
					fileOutputStream.write(bytes, 0, read);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public Boolean rollBackContent(ContentFileDTO contentFileDTO)
			throws Exception {
		boolean isSuccess = true;
		Session session = null;

		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchContentFileByFileName");
			query.setParameter("fileName", contentFileDTO.getFileName());
			ContentFile contentFile = (ContentFile) query.list().get(0);
			contentFile.setVersion(contentFileDTO.getVersion());
			contentFile.setLastUpdated(new Date(System.currentTimeMillis()));
			contentFile.setComments(contentFileDTO.getComments());
			session.saveOrUpdate(contentFile);
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			isSuccess = false;
			throw e;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return isSuccess;
	}

	public JSONObject getContentTypes() {
		Session session = null;
		JSONArray finalArray = new JSONArray();
		JSONObject finalObject = new JSONObject();
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchContentTypes");
			List<ContentType> contentTypes = query.list();
			for (ContentType contentType : contentTypes) {
				JSONObject jsonObj = new JSONObject();
				JSONArray jsonArray = new JSONArray();
				String[] extensions;
				if (null != contentType.getExtensions()) {
					extensions = contentType.getExtensions().split(",");
					for (String ext : extensions) {
						jsonArray.put(ext);
					}
				}
				jsonObj.put(new String(contentType.getType()), jsonArray);
				finalArray.put(jsonObj);
			}
			finalObject.put("contentTypes", finalArray);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error("Exception while getting content types >" + e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return finalObject;

	}

	public JSONObject getApplicationContexts() {
		Session session = null;
		List<AppContext> appContexts = new ArrayList<AppContext>();
		JSONObject finalObject = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchContexts");
			appContexts = query.list();
			JSONObject jsonObject = new JSONObject();
			for (AppContext appContext : appContexts) {
				jsonObject.put("id", appContext.getId());
				jsonObject.put("context", appContext.getContext());
				jsonArray.put(jsonObject);
			}
			finalObject.put("appContexts", jsonArray);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error("Exception while getting application context >" + e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return finalObject;
	}

	public JSONObject getContentPreference() {
		Session session = null;
		JSONObject jsonObj = new JSONObject();
		JSONObject finalObject = new JSONObject();
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchContentTypes");
			List<ContentType> contentTypes = query.list();
			for (ContentType contentType : contentTypes) {
				Boolean previewable = contentType.getPreviewable();
				Boolean optionality = contentType.getOptionality();
				JSONObject cntJson = new JSONObject();
				cntJson.put("optionality", optionality);
				cntJson.put("previewable", previewable);
				jsonObj.put(contentType.getId(), cntJson);
			}
			finalObject.put("contentPreference", jsonObj);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error("Exception while getting content types >" + e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return finalObject;

	}

	*//**
	 * This method is used to tag/untag the content file to one or more
	 * application context
	 * 
	 *//*
	@Override
	public Boolean tagUntagContext(ContentFileDTO contentFileDTO)
			throws Exception {

		boolean isSuccess = true;
		Session session = null;

		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			ContentLanguage contentLanguage = null;
				contentLanguage = new ContentLanguage();
				contentLanguage.setId(contentFileDTO.getContentLanguage().getId());
			Query query = session.getNamedQuery("fetchContentFileByFileName");
			query.setParameter("fileName", contentFileDTO.getFileName());
			ContentFile contentFile = (ContentFile) query.list().get(0);
			contentFile.setAppContext(contentFileDTO.getAppContexts());
			contentFile.setContentLanguage(contentLanguage);
			if (null != contentFileDTO.getIsRequired()) {
				contentFile.setIsRequired(contentFileDTO.getIsRequired());
			}
			session.saveOrUpdate(contentFile);
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			isSuccess = false;
			throw e;

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return isSuccess;

	}
}*/