package com.cognizant.insurance.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.component.LifeEngageAgentComponent;
import com.cognizant.insurance.component.repository.AgentProfileRepository;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.vo.AgentProfileResponse;
import com.cognizant.insurance.response.vo.ResponseInfo;
import com.google.common.base.Strings;

/**
 * The Class LifeEngageAgentComponentImpl.
 */
@Service("lifeEngageAgentComponent")
public class LifeEngageAgentComponentImpl implements LifeEngageAgentComponent {

	@Autowired
	AgentProfileRepository agentRepository;
	
	@Autowired
	@Qualifier("agentProfileMapping")
	LifeEngageSmooksHolder agentSmooksHolder;
	
	/** The Constant TRANSACTIONS. */
	private static final String TRANSACTIONS = "Transactions";
	
	/** The Constant TRANSACTION_DATA. */
	private static final String TRANSACTION_DATA = "TransactionData";

	/** The Constant AGENT_ID. */
	private static final String AGENT_ID = "Key11";

	/**
	 * Retrieve agent profile.
	 * 
	 * @param agentID
	 *            the agent id
	 * @return the string
	 * @throws BusinessException
	 *             the business exception
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
	public String retrieveAgentProfileByCode(RequestInfo requestInfo,
			JSONArray jsonArray) throws BusinessException {
		String response = null;
		if (jsonArray.length() > 0) {
			final JSONObject jsonObj = jsonArray
					.getJSONObject(0);
			final String agentID = getStringKeyValue(AGENT_ID,
					jsonObj);
			if (Strings.isNullOrEmpty(agentID)) {
				throwBusinessException(true, "Key11 is null or empty!");
			}
			final Agent agent = agentRepository.retrieveAgentDetailsByCode(requestInfo, agentID);
			final AgentProfileResponse agentResponse = new AgentProfileResponse();
			agentResponse.setResponseInfo(buildResponseInfo(requestInfo));
			agentResponse.setAgent(agent);
			response = agentSmooksHolder.parseBO(agentResponse);
		}	
		return response;
	}
	
	
    /**
     * Builds the response info.
     * 
     * @param requestInfo
     *            the request info
     * @return the response info
     */
    private ResponseInfo buildResponseInfo(final RequestInfo requestInfo) {
        final ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setCreationDateTime(requestInfo.getCreationDateTime());
        responseInfo.setRequestorToken(requestInfo.getRequestorToken());
        responseInfo.setSourceInfoName(requestInfo.getSourceInfoName());
        responseInfo.setTransactionId(requestInfo.getTransactionId());
        responseInfo.setUserName(requestInfo.getUserName());
        return responseInfo;
    }
    
	/**
	 * Gets the string key value.
	 * 
	 * @param key
	 *            the key
	 * @param jsonObj
	 *            the json obj
	 * @return the string key value
	 */
	private static String getStringKeyValue(final String key,
			final JSONObject jsonObj) {
		String result = null;
		if (jsonObj.has(key)) {
			result = jsonObj.getString(key);
		}

		return result;
	}
    

}
