package com.cognizant.insurance.generic.component;

import java.util.List;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.eMail.LifeEngageEmail;

/**
 * The Interface interface EmailComponent.
 *
 */
public interface EmailComponent {

    /**
     * Save email details.
     * 
     * @param request the request
     * @return the response
     */
    <T> Response<Object> save(Request<T> request);

    /**
     * Update email details.
     *
     * @param request the request
     */
    <T> void update(Request<T> request);
    
    /**
     * Retrieve failed email details.
     *
     * @param request the request
     * @param maxAttemptNo the maxAttemptNo
     * @return the response
     */
    Response<List<LifeEngageEmail>> retrieveFailedLifeEngageEmailDetails(Request<LifeEngageEmail> request, String maxAttemptNo);
    
    Response<List<LifeEngageEmail>> retrieveInitialisedLifeEngageEmailDetails(Request<LifeEngageEmail> request);

}
