/**
 * 
 */
package com.cognizant.insurance.agent.domain.party;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.contactandplace.ContactPreference;
import com.cognizant.insurance.agent.domain.extension.Extension;
import com.cognizant.insurance.agent.domain.party.registration.subtypes.TaxRegistration;

/**
 * This is the super type for all organizations and persons.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Party extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7571738761596895495L;    
  

    /**
     * This relationship links a party to a related contact preference. This
     * represents a party's contact preference (telephone, email, etc.).
     * 
     * 
     * 
     */
    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    private Set<ContactPreference> preferredContact;

    /******Newly added for EAp - Start********/
    /**
     * An indicator that specifies if SMS is to be used 
     * as the permanent method of communication. 
     */
    private Boolean communicateViaSMS;
    
    /**
     * An indicator that specifies if email is to be used 
     * as the permanent method of communication. 
     */
    private Boolean communicateViaEmail;

    /**
     * An indicator that specifies if regular mails is to be used 
     * as the permanent method of communication. 
     */
    private Boolean communicateViaPost;
    
    
    /** The includes extension. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    private List<Extension> includesExtension; 
    
    /**
	 * This relationship links a tax registration to the related registrant party to identify a party as the subject of a tax registration.
	 * 
	 * e.g: The tax registration of Jane Doe.
	 * 
	 * e.g: The tax registration of Acme Cleaning Services.
	 */
    @OneToMany(cascade={ CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
	Set<TaxRegistration> relatedTaxRegistrations;
    
    /******Newly added for EAp - End ********/
    
    public Set<TaxRegistration> getRelatedTaxRegistrations() {
		return relatedTaxRegistrations;
	}

	public void setRelatedTaxRegistrations(
			Set<TaxRegistration> relatedTaxRegistrations) {
		this.relatedTaxRegistrations = relatedTaxRegistrations;
	}

	/**
     * Gets the preferredContact.
     * 
     * @return Returns the preferredContact.
     */
    public Set<ContactPreference> getPreferredContact() {
        return preferredContact;
    }

    /**
     * Sets The preferredContact.
     * 
     * @param preferredContact
     *            The preferredContact to set.
     */
    public void setPreferredContact(
            final Set<ContactPreference> preferredContact) {
        this.preferredContact = preferredContact;
    }

    /**
     * @return the communicateViaSMS
     */
    public Boolean getCommunicateViaSMS() {
        return communicateViaSMS;
    }

    /**
     * @param communicateViaSMS the communicateViaSMS to set
     */
    public void setCommunicateViaSMS(Boolean communicateViaSMS) {
        this.communicateViaSMS = communicateViaSMS;
    }

    /**
     * @return the communicateViaEmail
     */
    public Boolean getCommunicateViaEmail() {
        return communicateViaEmail;
    }

    /**
     * @param communicateViaEmail the communicateViaEmail to set
     */
    public void setCommunicateViaEmail(Boolean communicateViaEmail) {
        this.communicateViaEmail = communicateViaEmail;
    }

    /**
     * @return the communicateViaPost
     */
    public Boolean getCommunicateViaPost() {
        return communicateViaPost;
    }

    /**
     * @param communicateViaPost the communicateViaPost to set
     */
    public void setCommunicateViaPost(Boolean communicateViaPost) {
        this.communicateViaPost = communicateViaPost;
    }
    
    /**
	 * Gets the includes extension.
	 *
	 * @return the includes extension
	 */
	public List<Extension> getIncludesExtension() {
		return includesExtension;
	}

	/**
	 * Sets the includes extension.
	 *
	 * @param includesExtensions the new includes extension
	 */
	public void setIncludesExtension(List<Extension> includesExtension) {
		this.includesExtension = includesExtension;
	}

	

}
