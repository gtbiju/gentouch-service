/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */
package com.cognizant.insurance.product.component;

import java.util.List;

import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;

/**
 * The Interface interface ProductComponent.
 */
public interface ProductComponent {


	/**
	 * Gets the All products .
	 * 
	 * @param criteriaRequest
	 *            the criteria request
	 * @return the product
	 */
	Response<List<ProductSpecification>> getProducts(
			Request<ProductSearchCriteria> criteriaRequest);
	
	/**
	 * Gets the Active products .
	 * 
	 * @param request
	 *            the criteria request
	 * @return the product
	 */
	Response<List<ProductSpecification>> getActiveProducts(
			Request<ProductSearchCriteria> request);
	
	

    /**
     * Gets the product by id.
     * 
     * @param requestProductSpec
     *            the product id
     * @return the product by id
     */
    Response<ProductSpecification> getProductById(Request<ProductSearchCriteria> requestProductSpec, final String version);

    /**
     * Gets the product.
     * 
     * @param criteriaRequest
     *            the criteria request
     * @return the product
     */
    Response<ProductSpecification> getProduct(Request<ProductSearchCriteria> criteriaRequest, final String version);

    /**
     * Used to check whether a product is active or not.
     *
     * @param criteriaRequest the criteria request
     * @return the response
     */
    Response<Boolean> isProductActive(Request<ProductSearchCriteria> criteriaRequest, final String version);
    
    /**
     * Get all products by filter : carrierId & ChannelId.
     *
     * @param criteriaRequest the criteria request
     * @return the product
     */
    Response<List<ProductSpecification>> getAllProductsByFilter(Request<ProductSearchCriteria> criteriaRequest, final String version);

    /**
     * Get all active products by filter : carrierId & ChannelId.
     *
     * @param criteriaRequest the criteria request
     * @return the active products by filter
     */
    Response<List<ProductSpecification>> getActiveProductsByFilter(Request<ProductSearchCriteria> criteriaRequest, final String version);

   
    /**
     * Gets the products by filter.
     * 
     * @param criteriaRequest
     *            the criteria request
     * @param version
     * @return the products by filter
     */
    Response<List<ProductSpecification>> getProductsByFilter(Request<ProductSearchCriteria> criteriaRequest, String version);

    /**
     * Gets the product templates by filter.
     * 
     * @param criteriaRequest
     *            the criteria request
     * @return the product templates by filter
     */
    Response<List<ProductTemplateMapping>> getProductTemplatesByFilter(Request<ProductSearchCriteria> criteriaRequest);


}
