/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Sep 20, 2017
 */


package com.cognizant.insurance.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class BroadLobType.
 */
@Entity
@DiscriminatorValue("Broad_Lob")
public class BroadLobType extends Type {

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 946096563299117775L;
	
    /**
     * Instantiates a new broad lob type.
     *
     * @param id the id
     */
    public BroadLobType(final String id) {
        super(id);
    }
    
    /**
     * Instantiates a new broad lob type.
     */
    public BroadLobType(){
    	
    }
	
	
	/** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL }, mappedBy = "lineOfBusiness")
    private Set<ProductSpecification> productSpecifications;

    /**
     * Sets the product specifications.
     *
     * @param productSpecifications the productSpecifications to set
     */
    public void setProductSpecifications(Set<ProductSpecification> productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    /**
     * Gets the product specifications.
     *
     * @return the productSpecifications
     */
    public Set<ProductSpecification> getProductSpecifications() {
        return productSpecifications;
    }
}
