package com.cognizant.insurance.omnichannel.component.impl;

import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.cognizant.generali.encryption.util.EncryptionUtil;
import com.cognizant.insurance.component.VendorAgentComponent;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.google.gson.Gson;

public class GeneraliVendorComponent implements VendorAgentComponent {

    
    private String vendorServiceURL="";
    
    
    private String VendorUrltogetMappedBrances="";

    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    EncryptionUtil encryptionUtil;

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliVendorComponent.class);

    @Override
    public String retrieveAgentProfileByCode(RequestInfo requestInfo, JSONArray jsonArray) throws BusinessException {
        String encryptedString="";
        // Load agent profile offline
        if (vendorServiceURL.endsWith(".json")) {
            return getOffileAgentProfile(jsonArray);
        }
        String profileReq = buildRequest(requestInfo, jsonArray);
        encryptedString=encryptionUtil.encryptString(profileReq);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> httpRequest = new HttpEntity<String>(encryptedString, headers);
        ResponseEntity<String> response;
        String responseBodyString="";
        try {
            response = restTemplate.postForEntity(vendorServiceURL, httpRequest, String.class);
            String responseBodyJson=response.getBody();
            responseBodyString= new Gson().fromJson(responseBodyJson, String.class);
        } catch (RestClientException e) {
            throw new BusinessException("Unable to complete Agent profile Service request - RestClient Exception", e);
        } catch (Exception e) {
            throw new BusinessException("Unable to complete Agent profile Service request", e);
        }
        if (response.getStatusCode().value() != 200) {
            LOGGER.error("Vendor Service error : Status " + response.getStatusCode() + " - " + response.getBody());
            throw new BusinessException(
                    "Unable to complete Agent profile Service request : Failed to process the response");

        }
      
        return  encryptionUtil.decryptString(responseBodyString);
    }

    private String buildRequest(RequestInfo info, JSONArray array) {
        JSONObject requestObj = new JSONObject();
        JSONObject request = new JSONObject();
        JSONObject requestInfo = new JSONObject();
        JSONObject requestPayload = new JSONObject();
        requestObj.put(REQUEST, request);
        request.put(REQUEST_INFO, requestInfo);
        request.put(REQUEST_PAYLOAD, requestPayload);
        requestPayload.put(TRANSACTIONS, array);
        return requestObj.toString();

    }
    
    private String getOffileAgentProfile(JSONArray jsonArray) {
        String response = "";
       
        String agentId = jsonArray.getJSONObject(0).getString("Key11");
        String[] agentIdParts=agentId.split("-");
        String agentType=agentIdParts[0];
        String agentCode=agentIdParts[1];
        String url="/agentprofile/"+agentType+".json";
        response = new Scanner(this.getClass().getResourceAsStream(url)).useDelimiter("\\Z").next();
        response = response.replaceAll("--AgentId--", agentId);
        response = response.replaceAll("--BancaAgentId--", "IOIS-"+agentCode);
        return response;

    }
    
 
    @Override
    public String retrieveAgentsbyBranchCode(RequestInfo requestInfo, JSONArray jsonArray) throws BusinessException {
        String encryptedString="";
        // Load agent profile offline
        if (vendorServiceURL.endsWith(".json")) {
            return getOffileAgentProfile(jsonArray);
        }
        String profileReq = buildRequest(requestInfo, jsonArray);
        encryptedString=encryptionUtil.encryptString(profileReq);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> httpRequest = new HttpEntity<String>(encryptedString, headers);
        ResponseEntity<String> response;
        String responseBodyString="";
        try {
            response = restTemplate.postForEntity(VendorUrltogetMappedBrances, httpRequest, String.class);
            String responseBodyJson=response.getBody();
            responseBodyString= new Gson().fromJson(responseBodyJson, String.class);
        } catch (RestClientException e) {
            throw new BusinessException("Unable to complete Agent profile Service request - RestClient Exception", e);
        } catch (Exception e) {
            throw new BusinessException("Unable to complete Agent profile Service request", e);
        }
        if (response.getStatusCode().value() != 200) {
            LOGGER.error("Vendor Service error : Status " + response.getStatusCode() + " - " + response.getBody());
            throw new BusinessException(
                    "Unable to complete Agent profile Service request : Failed to process the response");

        }
       
        return encryptionUtil.decryptString(responseBodyString);
    }

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayLoad";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

}
