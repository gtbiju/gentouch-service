package com.cognizant.insurance.omnichannel.dao;

import java.util.List;

import com.cognizant.insurance.dao.Dao;
import com.cognizant.insurance.omnichannel.domain.AutoEtrGenerateAudit;

public interface AutoEtrGenerateAuditDao extends Dao  {

	List<AutoEtrGenerateAudit> retrieveAutoEtrGenerateAudit(String eappNo);
}
