--Lead Details
select p.creationDateTime as CreationTime ,p.agentId,ISNULL(p.branchId,''),k.key2 as FirstName,k.key3 as LastName,k.key15  from PartyRoleInRelationship p , TransactionKeys k where p.transactionKeys_id=k.id and p.creationDateTime>='2016-12-05' and p.statusCode not in ('18','19') order by p.id desc

--Lead Count 

select count(id) from PartyRoleInRelationship  where statusCode not in ('18','19')

--Login Count

select count( distinct upper(userId)) from LE_LOGIN_USER_TOKEN with (nolock)

--Login Users

select distinct upper(userId) from LE_LOGIN_USER_TOKEN with (nolock)

--Login Details

select  distinct userId from LE_LOGIN_USER_TOKEN where timestamp >='2016-12-05'

-- Illustration
select Count(1) from Agreement where statusCode not in ('4','7') and creationDateTime>='2016-12-05' and contextId='2'

-- Illustration with Agent Code

select 
       cast(a.modifiedDateTime as Date),
       case a.statusCode  when 0 then'Final'
    when 1 then'Inforce'
    when 2 then'InForceChangePending'  
    when 3 then'Initial' 
    when 4 then'Suspended'  
    when 5 then'Terminated'  
    when 6 then'UnderNegotiation'   
    when 7 then'Cancelled' 
    when 8 then'Proposed'  
    when 9 then'NotMentioned'
    when 10 then'Draft'   
    when 11 then'Completed'   
    when 12 then'Confirmed'  
    when 13 then'ReportSent'    
    when 14 then'New' 
    when 15 then'Submitted'   
    when 16 then'Lapse'   
    when 17 then'Surrender'   
    when 18 then'InProgress'   
    when 19 then'Review'    
    when 20 then'PaymentDone' END  as Status,
	ia.agentId as AgentCode
    
from Agreement as a with (nolock) inner join InsuranceAgreement as ia with (nolock) on a.id=ia.id
where  
       a.statusCode not in (4,7) and
       a.contextId in (2)
     

-- EAPP

select Count(1) from Agreement where statusCode not in ('4','7') and creationDateTime>='2016-12-05' and contextId='4'

-- EAPP with AgentCode
select 
       cast(a.modifiedDateTime as Date),
       case a.statusCode  when 0 then'Final'
    when 1 then'Inforce'
    when 2 then'InForceChangePending'  
    when 3 then'Initial' 
    when 4 then'Suspended'  
    when 5 then'Terminated'  
    when 6 then'UnderNegotiation'   
    when 7 then'Cancelled' 
    when 8 then'Proposed'  
    when 9 then'NotMentioned'
    when 10 then'Draft'   
    when 11 then'Completed'   
    when 12 then'Confirmed'  
    when 13 then'ReportSent'    
    when 14 then'New' 
    when 15 then'Submitted'   
    when 16 then'Lapse'   
    when 17 then'Surrender'   
    when 18 then'InProgress'   
    when 19 then'Review'    
    when 20 then'PaymentDone' END  as Status,
	ia.agentId as AgentCode
    
from Agreement as a with (nolock) inner join InsuranceAgreement as ia with (nolock) on a.id=ia.id
where  
       a.statusCode not in (4,7) and
       a.contextId in (4)
     

-- Illustration and eApp with all status
select 
       case contextId when  2 then 'Illustration' when 4 then 'E-Application'END as AppType,cast(modifiedDateTime as Date),
       case statusCode  when 0 then'Final'
    when 1 then'Inforce'
    when 2 then'InForceChangePending'  
    when 3 then'Initial' 
    when 4 then'Suspended'  
    when 5 then'Terminated'  
    when 6 then'UnderNegotiation'   
    when 7 then'Cancelled' 
    when 8 then'Proposed'  
    when 9 then'NotMentioned'
    when 10 then'Draft'   
    when 11 then'Completed'   
    when 12 then'Confirmed'  
    when 13 then'ReportSent'    
    when 14 then'New' 
    when 15 then'Submitted'   
    when 16 then'Lapse'   
    when 17 then'Surrender'   
    when 18 then'InProgress'   
    when 19 then'Review'    
    when 20 then'PaymentDone' END  as Status,
       count(1)  as Count
from Agreement with (nolock)
where  
       statusCode not in (4,7) and
       contextId in (2,4)
          and modifiedDateTime>='2016-12-03'
group by 
       contextId,statusCode,CAST(modifiedDateTime AS DATE)
          ORDER BY CAST(modifiedDateTime AS DATE)

-- Life Asia Submission

select datepart(dd,CREATIONdate) as 'Date', datepart(MM,CREATIONdate) as 'Month', datePart(YY,CREATIONdate) as 'Year', count(1)
as 'count' from LifeAsiaRequest 
where status='SUBMITTED' 
group by datePart(YY,CREATIONdate),datepart(MM,CREATIONdate),datepart(dd,CREATIONdate)
order by datepart(YY,CREATIONdate),datepart(MM,CREATIONdate),datepart(dd,CREATIONdate)




