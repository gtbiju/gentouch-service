/**
 * 
 */
package com.cognizant.insurance.domain.finance;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.domain.finance.financecodelists.BillingOptionCodeList;
import com.cognizant.insurance.domain.finance.financecodelists.PaymentDueStatusCodeList;

/**
 * An installment is the atomic financial provision of a scheduler. As such, it
 * keeps track of information about the provisions as they relate to the
 * scheduler and acts as a type of association class between the two (i.e., it
 * can conceptually be thought of that way).
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public abstract class Installment extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1380198222313842267L;

    /**
     * The date before which the amount due must be paid.
     * 
     * e.g: The due date for the premium related to the period 12/1/2001 to
     * 12/1/2002 is set to the 12/1/2001. For insurance premiums, the due date
     * normally is equal to the payment due period start date, because most of
     * the financial transactions by an insurance company are due in the
     * beginning of the period over which they are applicable. This is called
     * payments in advance or prenumerando, as opposed to payments in arrears or
     * postnumerando. Postnumerando payments are due at the end of the period
     * over which they are applicable.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date dueDate;

    /**
     * The payment period (the period for which the payment due is created)
     * 
     * e.g: The period start date for the premium due related to the period
     * 12/1/2001 to 12/1/2002 is the 12/1/2001. The period end date for the
     * premium due related to the period 12/1/2001 to 12/1/2002 is the
     * 12/1/2002.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private TimePeriod paymentPeriod;

    /**
     * The current status of a payment due within a life-cycle model.
     * 
     * e.g: Due
     * 
     * e.g: Fully settled
     * 
     * e.g: Partially settled
     * 
     * e.g: Replaced
     * 
     * e.g: Written off
     * 
     * 
     * 
     */
    @Enumerated
    private PaymentDueStatusCodeList statusCode;

    /**
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount netWithSurchargeAmount;

    /**
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)   
    private CurrencyAmount withoutSurchargeAmount;

    /**
     * The billed premium nature is either "basic" or "With extra costs" due to
     * a dunning procedure.
     * 
     * 
     * 
     */
    private String premiumInstallmentType;

    /**
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)   
    private CurrencyAmount netInstallmentPremiumAmount;

    /**
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)   
    private CurrencyAmount netInitialPremiumAmount;

    @Enumerated
    private BillingOptionCodeList billingOptionCode;

    /**
     * Gets the netWithSurchargeAmount.
     * 
     * @return Returns the netWithSurchargeAmount.
     */
    public CurrencyAmount getNetWithSurchargeAmount() {
        return netWithSurchargeAmount;
    }

    /**
     * Gets the withoutSurchargeAmount.
     * 
     * @return Returns the withoutSurchargeAmount.
     */
    public CurrencyAmount getWithoutSurchargeAmount() {
        return withoutSurchargeAmount;
    }

    /**
     * Gets the premiumInstallmentType.
     * 
     * @return Returns the premiumInstallmentType.
     */
    public String getPremiumInstallmentType() {
        return premiumInstallmentType;
    }

    /**
     * Gets the netInstallmentPremiumAmount.
     * 
     * @return Returns the netInstallmentPremiumAmount.
     */
    public CurrencyAmount getNetInstallmentPremiumAmount() {
        return netInstallmentPremiumAmount;
    }

    /**
     * Gets the netInitialPremiumAmount.
     * 
     * @return Returns the netInitialPremiumAmount.
     */
    public CurrencyAmount getNetInitialPremiumAmount() {
        return netInitialPremiumAmount;
    }

    /**
     * Sets The netWithSurchargeAmount.
     * 
     * @param netWithSurchargeAmount
     *            The netWithSurchargeAmount to set.
     */
    public void setNetWithSurchargeAmount(
            final CurrencyAmount netWithSurchargeAmount) {
        this.netWithSurchargeAmount = netWithSurchargeAmount;
    }

    /**
     * Sets The withoutSurchargeAmount.
     * 
     * @param withoutSurchargeAmount
     *            The withoutSurchargeAmount to set.
     */
    public void setWithoutSurchargeAmount(
            final CurrencyAmount withoutSurchargeAmount) {
        this.withoutSurchargeAmount = withoutSurchargeAmount;
    }

    /**
     * Sets The premiumInstallmentType.
     * 
     * @param premiumInstallmentType
     *            The premiumInstallmentType to set.
     */
    public void setPremiumInstallmentType(
            final String premiumInstallmentType) {
        this.premiumInstallmentType = premiumInstallmentType;
    }

    /**
     * Sets The netInstallmentPremiumAmount.
     * 
     * @param netInstallmentPremiumAmount
     *            The netInstallmentPremiumAmount to set.
     */
    public void setNetInstallmentPremiumAmount(
            final CurrencyAmount netInstallmentPremiumAmount) {
        this.netInstallmentPremiumAmount = netInstallmentPremiumAmount;
    }

    /**
     * Sets The netInitialPremiumAmount.
     * 
     * @param netInitialPremiumAmount
     *            The netInitialPremiumAmount to set.
     */
    public void setNetInitialPremiumAmount(
            final CurrencyAmount netInitialPremiumAmount) {
        this.netInitialPremiumAmount = netInitialPremiumAmount;
    }

    
    
    /**
     * Gets the dueDate.
     * 
     * @return Returns the dueDate.
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Gets the paymentPeriod.
     * 
     * @return Returns the paymentPeriod.
     */
    public TimePeriod getPaymentPeriod() {
        return paymentPeriod;
    }

    /**
     * Gets the statusCode.
     * 
     * @return Returns the statusCode.
     */
    public PaymentDueStatusCodeList getStatusCode() {
        return statusCode;
    }

    /**
     * Sets The dueDate.
     * 
     * @param dueDate
     *            The dueDate to set.
     */
    public void setDueDate(final Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Sets The paymentPeriod.
     * 
     * @param paymentPeriod
     *            The paymentPeriod to set.
     */
    public void setPaymentPeriod(final TimePeriod paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    /**
     * Sets The statusCode.
     * 
     * @param statusCode
     *            The statusCode to set.
     */
    public void setStatusCode(final PaymentDueStatusCodeList statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the billingOptionCode
     * 
     * 
     */
    public BillingOptionCodeList getBillingOptionCode() {
        return billingOptionCode;
    }

    /**
     * @param billingOptionCode
     *            the billingOptionCode to set
     * 
     * 
     */
    public void setBillingOptionCode(
            final BillingOptionCodeList billingOptionCode) {
        this.billingOptionCode = billingOptionCode;
    }

}
