package com.cognizant.insurance.omnichannel.component.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.FNARepository;
import com.cognizant.insurance.component.repository.helper.LifeEngageComponentRepositoryHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.goalandneed.dao.GoalAndNeedDao;
import com.cognizant.insurance.goalandneed.dao.impl.GoalAndNeedDaoImpl;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliGoalAndNeedComponent;
import com.cognizant.insurance.omnichannel.utils.GeneraliDateUtil;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;
import com.cognizant.insurance.omnichannel.dao.GeneraliGoalAndNeedDao;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliGoalAndNeedDaoImpl;


public class GeneraliFNARepository extends FNARepository {

    /** The goalAndNeed component. */
    @Autowired
    private GeneraliGoalAndNeedComponent goalAndNeedComponent;
    
    
    /** The conflict resolution. */
    @Value("${le.platform.service.conflictResolution}")
    private String conflictResolution;
    
    /** The source of truth. */
    @Value("${le.platform.service.sourceOfTruth}")
    private String sourceOfTruth;
    
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;
    
    /** The Constant LMS. */
    private static final String FNA = "FNA";
    
    /** The Constant PARTIES. */
    private static final String PARTIES = "parties";

    /** The Constant BASICDETAILS. */
    private static final String BASICDETAILS = "BasicDetails";

    /** The Constant CONTACTDETAILS. */
    private static final String CONTACTDETAILS = "ContactDetails";

    /** The Constant FIRSTNAME. */
    private static final String FIRSTNAME = "firstName";

    /** The Constant LASTNAME. */
    private static final String LASTNAME = "lastName";

    /** The Constant DOB. */
    private static final String DOB = "dob";

    /** The Constant EMAILID. */
    private static final String EMAILID = "emailId";

    /** The Constant CONTACTNUMBER. */
    private static final String CONTACTNUMBER = "homeNumber1";

    /** The date format. */
    public static final String DATEFORMAT_MM_DD_YYYY = "MM-dd-yyyy";

    /** The date format yyyy-mm-dd. */
    public static final String DATEFORMAT_YYYY_MM_DD = "yyyy-MM-dd";

    /** The party type. */
    private static final String partyType = "FNAMyself";
    
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;
    
    @Override
	public Transactions saveFNA(final Transactions transactions, final RequestInfo requestInfo, final String json, final GoalAndNeed responseGoalAndNeed)
            throws ParseException {

        LOGGER.info(" :::: Entering saveFNA in FNARepository 2.....");
        final Request<GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<GoalAndNeed>(entityManager, ContextTypeCodeList.FNA, requestInfo.getTransactionId());
        final GoalAndNeed goalAndNeed = new GoalAndNeed();

        goalAndNeed.setIdentifier(transactions.getFnaId());
        goalAndNeed.setTransTrackingId(transactions.getTransTrackingId());
        goalAndNeedRequest.setType(goalAndNeed);

        final JSONObject transactionJsonObject = new JSONObject(json);
        
        final JSONObject jsonObject;
        if(transactions.getKey40()!=null && transactions.getKey40().equalsIgnoreCase("reassign")){
        	jsonObject = new JSONObject(transactions.getGoalAndNeed().getRequestJSON());
        	 String newAgentStatus = responseGoalAndNeed.getStatus();
             String newLeadId = transactions.getKey22();
        	String status = onFnaUpdate(transactions, requestInfo, jsonObject, responseGoalAndNeed);
            if (!Constants.SUCCESS.equals(status)) {
                transactions.setStatus(Constants.REJECTED);
                return transactions;
            }

        	//setGoalAndNeed(transactions, requestInfo, goalAndNeed, jsonObject);
            // goalAndNeed.setCreationDateTime(currentDate);
        	String newAgentId = transactions.getKey39();
        	String fnaReassignIdentifier = newAgentId+String.valueOf(idGenerator.generate(FNA));
        	transactions.setAgentId(newAgentId);
        	transactions.setKey2(fnaReassignIdentifier);
        	transactions.setProductId(responseGoalAndNeed.getProductId());
        	transactions.setStatus(newAgentStatus);
        	transactions.setCustomerId(newLeadId);
        	goalAndNeed.setIdentifier(fnaReassignIdentifier);
        	goalAndNeed.setCreationDateTime(new Date());
        	goalAndNeed.setStatus(newAgentStatus);
        	goalAndNeed.setLeadId(newLeadId);
        	goalAndNeed.setTransTrackingId(GeneraliConstants.LEADREASSIGN+"-"+newAgentId+"-"+String.valueOf(idGenerator.generate())+"-"+String.valueOf(idGenerator.generate()));
        	TransactionKeys transactionKeys = new TransactionKeys();
        	goalAndNeed.setTransactionKeys(transactionKeys);
        	goalAndNeed.getTransactionKeys().setKey1(transactions.getKey22());
        	goalAndNeed.getTransactionKeys().setKey13(
                     LifeEngageComponentRepositoryHelper.formatDate(goalAndNeed.getCreationDateTime()));
        	
        	setGoalAndNeed(transactions, requestInfo, goalAndNeed, jsonObject);
            goalAndNeedComponent.saveGoalAndNeed(goalAndNeedRequest);
        
         }
        else {
			jsonObject = transactionJsonObject.getJSONObject(Constants.TRANSACTION_DATA);
			if (Constants.UPDATE.equals(transactions.getMode())) {
				String status = onFnaUpdate(transactions, requestInfo, jsonObject, responseGoalAndNeed);
				if (!Constants.SUCCESS.equals(status)) {
					transactions.setStatus(Constants.REJECTED);
					return transactions;
				}
			}
			else {
				setGoalAndNeed(transactions, requestInfo, goalAndNeed, jsonObject);
				// goalAndNeed.setCreationDateTime(currentDate);
				goalAndNeed.getTransactionKeys()
						.setKey13(LifeEngageComponentRepositoryHelper.formatDate(goalAndNeed.getCreationDateTime()));
				goalAndNeedComponent.saveGoalAndNeed(goalAndNeedRequest);
			}
		}

       // if (transactions.getMode().equals("update")) {
           // transactions.setLastSyncDate(goalAndNeed.getModifiedDateTime());
      //  } else if (transactions.getMode().equals("save")) {
         //   transactions.setLastSyncDate(goalAndNeed.getCreationDateTime());
      //  }
        
        LOGGER.trace(" :::: Exiting saveFNA in FNARepository 2 : status >> " + transactions.getStatus());
        LOGGER.trace(" :::: Exiting saveFNA in FNARepository 2 : getLastSyncDate >> " + transactions.getLastSyncDate());
        return transactions;
    }

    /**
     * Retrieve RelatedTransactions for fna.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the list
     */
    public List<Transactions> retrieveRelatedTransactions(RequestInfo requestInfo, SearchCriteria searchCriteria) {

        LOGGER.info(" :::: Entering retrieveByFilter in GeneraliFNARepository ");
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.FNA,
                        requestInfo.getTransactionId());

        searchCriteriatRequest.setType(searchCriteria);
        final Response<List<GoalAndNeed>> searchCriteriatResponse =
                goalAndNeedComponent.retrieveRelatedTransactions(searchCriteriatRequest);
        final List<GoalAndNeed> goalAndNeeds = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(goalAndNeeds)) {
            for (GoalAndNeed goalAndneed : goalAndNeeds) {
                transaction = new Transactions();
                transaction.setGoalAndNeed((GoalAndNeed) goalAndneed);
                transaction.setKey2(goalAndneed.getIdentifier());
                transaction.setCustomerId(goalAndneed.getLeadId());
                transaction.setType(searchCriteria.getType());
                if (goalAndneed.getModifiedDateTime() != null
                        && !(goalAndneed.getModifiedDateTime().toString().equals(""))) {
                    transaction.setLastSyncDate(goalAndneed.getModifiedDateTime());
                } else if (goalAndneed.getCreationDateTime() != null
                        && !("".equals(goalAndneed.getCreationDateTime().toString()))) {
                    transaction.setLastSyncDate(goalAndneed.getCreationDateTime());
                }
                transaction.setModifiedTimeStamp(goalAndneed.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }
        LOGGER.info(" :::: Exiting retrieveByFilter in GeneraliFNARepository : transactionsList >> " + transactionsList);
        return transactionsList;

    }

    /**
     * retrieveFNACountForLMS.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the search count result
     */
    public SearchCountResult retrieveFNACountForLMS(final RequestInfo requestInfo, final SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.FNA,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        final Response<SearchCountResult> searchCountResponse =
                goalAndNeedComponent.getGoalAndNeedCountforLMS(searchCriteriatRequest);

        SearchCountResult searchCountResult = null;
        if (searchCountResponse.getType() != null) {
            searchCountResult = (SearchCountResult) searchCountResponse.getType();

        }
        return searchCountResult;

    }
    
    /**
     * On fna update.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param trnasactionData
     *            the trnasaction data
     * @return the string
     * @throws ParseException
     *             the parse exception
     */
    @Override
    protected String onFnaUpdate(Transactions transactions, RequestInfo requestInfo, JSONObject trnasactionData, GoalAndNeed responseGoalAndNeed)
            throws ParseException {
        final Request<GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<GoalAndNeed>(getEntityManager(), ContextTypeCodeList.FNA, requestInfo.getTransactionId());
        final GoalAndNeed goalAndNeed = new GoalAndNeed();

        goalAndNeed.setIdentifier(transactions.getFnaId());
        goalAndNeedRequest.setType(goalAndNeed);
        List<GoalAndNeed> goalAndNeeds = new ArrayList<GoalAndNeed>();
        if( responseGoalAndNeed != null ){
        	 goalAndNeeds.add((GoalAndNeed) responseGoalAndNeed);
        }else{
        final Response<List<GoalAndNeed>> goalAndNeedResponse =
                goalAndNeedComponent.getGoalAndNeedByIdentifier(goalAndNeedRequest);
        goalAndNeeds = goalAndNeedResponse.getType();
        }
        
        if(transactions.getKey40().equals("reassign")){
	        updateFnaStatusREASSIGN(responseGoalAndNeed, requestInfo,ContextTypeCodeList.FNA,trnasactionData);
	       }
        else{
        for (GoalAndNeed goalAndneed : goalAndNeeds) {
            // Checking whether the server data is latest when source of
            // truth is server, if so reject the client data and send
            // rejected status
            if (Constants.TRUE.equals(conflictResolution) && Constants.SERVER.equals(sourceOfTruth)) {

                if (transactions.getLastSyncDate() != null
                        && !(transactions.getLastSyncDate().toString().equals(GeneraliDateUtil.getDefaultDate()))
                        && ((null != goalAndneed.getModifiedDateTime()) && (goalAndneed.getModifiedDateTime().after(transactions.getLastSyncDate())))) {

                    return Constants.REJECTED;

                }

            }
            setGoalAndNeed(transactions, requestInfo, goalAndneed, trnasactionData);
            goalAndneed.getTransactionKeys()
			.setKey13(LifeEngageComponentRepositoryHelper.formatDate(goalAndneed.getCreationDateTime()));
        }
        }
        return Constants.SUCCESS;
    }
    
    protected void updateFnaStatusREASSIGN(final GoalAndNeed responseGoalAndNeed, final RequestInfo requestInfo,
            final ContextTypeCodeList contextType,JSONObject  transactionData) {
        final Request<GoalAndNeed> request =
                new JPARequestImpl<GoalAndNeed>(entityManager, contextType, requestInfo.getTransactionId());
        responseGoalAndNeed.getTransactionKeys().setKey15(AgreementStatusCodeList.Suspended.toString());
        try {
        	responseGoalAndNeed.setModifiedDateTime(LifeEngageComponentHelper.getCurrentdate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        responseGoalAndNeed.setStatus(AgreementStatusCodeList.Suspended.toString());
        request.setType(responseGoalAndNeed);
        final GeneraliGoalAndNeedDao goalAndNeedDao = new GeneraliGoalAndNeedDaoImpl();
        goalAndNeedDao.updateGoalAndNeedStatusForLeadReassign(request);
    }

    /**
     * Create basic details json objects with details from db.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param goalAndNeed
     *            the goal and need
     * @param jsonObject
     *            the json object
     * @throws ParseException
     *             the parse exception
     */
    protected static void setGoalAndNeed(final Transactions transactions, final RequestInfo requestInfo,
            final GoalAndNeed goalAndNeed, final JSONObject jsonObject) throws ParseException {
        try {
            TransactionKeys transactionKeys = new TransactionKeys();
            boolean isInsured = false;
            final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD, Locale.getDefault());
            goalAndNeed.setTransactionId(requestInfo.getTransactionId());

            Date currentDate = LifeEngageComponentRepositoryHelper.getCurrentdate();
            
            goalAndNeed.setAgentId(transactions.getAgentId());
            goalAndNeed.setUserEmail(requestInfo.getUserEmail());
            goalAndNeed.setModifiedDateTime(currentDate);
           
            if (Constants.UPDATE.equals(transactions.getMode())) {
                transactions.setModifiedTimeStamp(currentDate);
                transactions.setCreationDateTime(goalAndNeed.getCreationDateTime());
                transactions.setLastSyncDate(currentDate);
           } else {
               goalAndNeed.setCreationDateTime(currentDate);
               transactions.setCreationDateTime(currentDate);
               transactions.setModifiedTimeStamp(currentDate);
               transactions.setLastSyncDate(currentDate);   
           }

            if (jsonObject.has(PARTIES)) {
                final JSONArray jsonpartiesObj = jsonObject.getJSONArray(PARTIES);
                JSONObject jsonInsuredObj = new JSONObject();
                if (jsonpartiesObj != null && jsonpartiesObj.length() > 0) {
                    for (int i = 0; i < jsonpartiesObj.length(); i++) {
                        JSONObject partyObj = (JSONObject) jsonpartiesObj.get(i);
                        if (partyObj.getString("type").equalsIgnoreCase(partyType)) {
                            isInsured = true;
                        }
                        if (isInsured) {
                            jsonInsuredObj = partyObj;
                            break;
                        }
                    }
                    if (jsonInsuredObj != null && jsonInsuredObj.length() > 0) {
                        final JSONObject jsonInsuredBasicDetailsObj = jsonInsuredObj.getJSONObject(BASICDETAILS);
                        final JSONObject jsonInsuredContactDetailsObj = jsonInsuredObj.getJSONObject(CONTACTDETAILS);

                        goalAndNeed.setGivenName(LifeEngageComponentHelper.getStringKeyValue(FIRSTNAME,
                                jsonInsuredBasicDetailsObj));
                        goalAndNeed.setSurName(LifeEngageComponentHelper.getStringKeyValue(LASTNAME,
                                jsonInsuredBasicDetailsObj));

                        goalAndNeed.setBirthDate(LifeEngageComponentHelper.getDateKeyValue(dateFormat, DOB,
                                jsonInsuredBasicDetailsObj));

                        goalAndNeed.setEmailAddress(LifeEngageComponentHelper.getStringKeyValue(EMAILID,
                                jsonInsuredContactDetailsObj));
                        goalAndNeed.setContactNumber(LifeEngageComponentHelper.getStringKeyValue(CONTACTNUMBER,
                                jsonInsuredContactDetailsObj));

                        // set values to transaction keys object
                        
                        transactionKeys.setKey6(goalAndNeed.getGivenName() + " " + goalAndNeed.getSurName());
                        transactionKeys.setKey7(jsonInsuredBasicDetailsObj.getString(DOB));
                        transactionKeys.setKey8(goalAndNeed.getContactNumber());
                        transactionKeys.setKey20(goalAndNeed.getEmailAddress());
                        transactionKeys.setKey14(LifeEngageComponentRepositoryHelper.formatDate(goalAndNeed.getModifiedDateTime()));
                    }
                }
            }
            goalAndNeed.setRequestJSON(jsonObject.toString());
            String status = transactions.getStatus().replaceAll("\\s",""); 
            goalAndNeed.setStatus(AgreementStatusCodeList.valueOf(status).toString());
            goalAndNeed.setProductId(transactions.getProductId());
            goalAndNeed.setLeadId(transactions.getCustomerId());
            goalAndNeed.setContextId(ContextTypeCodeList.FNA);

            // set values to transaction keys object
            transactionKeys.setKey1(transactions.getCustomerId());
            transactionKeys.setKey2(transactions.getKey2());
            transactionKeys.setKey11(transactions.getAgentId());
            transactionKeys.setKey15(transactions.getStatus());
            goalAndNeed.setTransactionKeys(transactionKeys);
        } catch (ParseException ex) {
            LOGGER.error(" :::: Error while setting values in goaland need for saving >> " + ex);
        }
    }
}
