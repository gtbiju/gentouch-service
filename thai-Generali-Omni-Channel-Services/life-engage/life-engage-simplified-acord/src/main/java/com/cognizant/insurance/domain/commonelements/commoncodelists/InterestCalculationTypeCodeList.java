/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Interest Calculation Type Identifies how the interest is calculated for an
 * interest bearing investment.
 * 
 * @author 301350
 * 
 * 
 */
public enum InterestCalculationTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * The investment earns simple interest (calculated based on the principal
     * only) at the rate stated.
     * 
     * 
     * 
     */
    Simple,
    /**
     * The investment interest is compounded (calculated based on the principal
     * plus interest).
     * 
     * 
     * 
     */
    Compound,
    /**
     * 
     * 
     * 
     * 
     */
    Other
}
