/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partydetailsubtypes;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.agent.domain.party.PartyDetail;

/**
 * This concept is a generalizing concept providing further details about an
 * organization. Some details regarding a party are not likely to be necessary
 * to perform basic processes about parties. As such, they have been separated
 * from basic party information and identified as details.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class OrganizationDetail extends PartyDetail {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3981381360907291166L;
    /**
     * States the reference period.
     * 
     * e.g. If the financial results are issued every year on the 30 of June, an
     * example of result period is June 30, 2004 to June 30, 2005.
     * 
     * e.g. An organization was founded as a partnership for a given reference
     * period. Later, the organization changed legal status to corporation for a
     * new reference period.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    private TimePeriod referencePeriod;

    /**
     * Gets the referencePeriod.
     * 
     * @return Returns the referencePeriod.
     */
    public TimePeriod getReferencePeriod() {
        return referencePeriod;
    }

    /**
     * Sets The referencePeriod.
     * 
     * @param referencePeriod
     *            The referencePeriod to set.
     */
    public void setReferencePeriod(final TimePeriod referencePeriod) {
        this.referencePeriod = referencePeriod;
    }

}
