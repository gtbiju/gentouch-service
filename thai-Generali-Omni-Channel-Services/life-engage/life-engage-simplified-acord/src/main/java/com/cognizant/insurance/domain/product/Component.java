/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 420118
 * @version       : 0.1, Sep 20, 2017
 */

package com.cognizant.insurance.domain.product;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

/**
 * The Class Component.
 */
@MappedSuperclass
public abstract class Component implements Serializable{

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8535660404426946497L;

	/** The id. */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    /** The version. */
    @Column(name="VERSION")
    private String version;
    
    /** The product code. */
    @Column(name="PRODUCT_CODE")
    private String productCode;
    
    /** The channel type. */
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="channelType_ID")
    private ChannelType channelType;
    
    /** The jurisdiction code. */
    @OneToOne(cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumns({
    	@JoinColumn(name="JURISDICTION_CODE", referencedColumnName = "JURISDICTION_CODE"),
    	@JoinColumn(name ="JURISDICTION_CARRIER", referencedColumnName = "JURISDICTION_CARRIER")
    })
    
    private JurisdictionCode jurisdictionCode;
    
    /** The version status. */
    @Column(name = "VERSION_STATUS")
    private String versionStatus;
    
   
    
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * Gets the channel type.
	 *
	 * @return the channel type
	 */
	public ChannelType getChannelType() {
		return channelType;
	}

	/**
	 * Sets the channel type.
	 *
	 * @param channelType the new channel type
	 */
	public void setChannelType(ChannelType channelType) {
		this.channelType = channelType;
	}

	/**
	 * Gets the version status.
	 *
	 * @return the version status
	 */
	public String getVersionStatus() {
		return versionStatus;
	}

	/**
	 * Sets the version status.
	 *
	 * @param versionStatus the new version status
	 */
	public void setVersionStatus(String versionStatus) {
		this.versionStatus = versionStatus;
	}

	/**
	 * Gets the jurisdiction code.
	 *
	 * @return the jurisdiction code
	 */
	public JurisdictionCode getJurisdictionCode() {
		return jurisdictionCode;
	}

	/**
	 * Sets the jurisdiction code.
	 *
	 * @param jurisdictionCode the new jurisdiction code
	 */
	public void setJurisdictionCode(JurisdictionCode jurisdictionCode) {
		this.jurisdictionCode = jurisdictionCode;
	}

}
