/**
 * 
 */
package com.cognizant.insurance.domain.agreement.agreementcodelists;

/**
 * Identifies a classification of coverage components according to their
 * priority in relation to other coverages/policies. 
 * 
 * @author 301350
 * 
 * 
 */
public enum CoveragePriorityCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Excess,
    /**
     * 
     * 
     * 
     * 
     */
    ContributionPerLimitRatio,
    /**
     * 
     * 
     * 
     * 
     */
    EqualShare,
    /**
     * 
     * 
     * 
     * 
     */
    Primary
}
