/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.placesubtypes;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.agent.domain.contactandplace.Place;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.AdministrativeSubDivisionCodeList;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.MunicipalityTypeCodeList;

/**
 * This concept represents a geographic place recognized by a governmental body,
 * not an administrative subdivision akin to a party/organization. The term
 * "municipality" is a generic term, and can describe any geographic place other
 * than a region, country or first level of country subdivision (refer to the
 * classes [GeographicRegion], [Country] and [CountrySubdivision]).
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("MUNICIPALITY")
public class Municipality extends Place {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6495490706776961748L;

    /**
     * A code indicating the nature of the city with regards to government
     * administrative functions, if applicable.
     * 
     * 
     * 
     */
    @Enumerated
    private AdministrativeSubDivisionCodeList administrativeSubDivisionCode;

    /**
     * A code indicating a type of municipality (e.g. City, Town, etc.).
     * 
     * 
     * 
     */
    @Enumerated
    private MunicipalityTypeCodeList typeCode;

    /**
     * A code utilized as "my" code for a municipality identification.
     * 
     * See also "Usage Note" text.
     * 
     * 
     * 
     */
    @OneToOne (cascade=CascadeType.ALL)
    private ExternalCode assignedCode;

    /**
     * Gets the administrativeSubDivisionCode.
     * 
     * @return Returns the administrativeSubDivisionCode.
     */
    public AdministrativeSubDivisionCodeList
            getAdministrativeSubDivisionCode() {
        return administrativeSubDivisionCode;
    }

    /**
     * Gets the typeCode.
     * 
     * @return Returns the typeCode.
     */
    public MunicipalityTypeCodeList getTypeCode() {
        return typeCode;
    }

    /**
     * Gets the assignedCode.
     * 
     * @return Returns the assignedCode.
     */
    public ExternalCode getAssignedCode() {
        return assignedCode;
    }

    /**
     * Sets The administrativeSubDivisionCode.
     * 
     * @param administrativeSubDivisionCode
     *            The administrativeSubDivisionCode to set.
     */
    public
            void
            setAdministrativeSubDivisionCode(
                    final AdministrativeSubDivisionCodeList administrativeSubDivisionCode) {
        this.administrativeSubDivisionCode = administrativeSubDivisionCode;
    }

    /**
     * Sets The typeCode.
     * 
     * @param typeCode
     *            The typeCode to set.
     */
    public void setTypeCode(final MunicipalityTypeCodeList typeCode) {
        this.typeCode = typeCode;
    }

    /**
     * Sets The assignedCode.
     * 
     * @param assignedCode
     *            The assignedCode to set.
     */
    public void setAssignedCode(final ExternalCode assignedCode) {
        this.assignedCode = assignedCode;
    }

}
