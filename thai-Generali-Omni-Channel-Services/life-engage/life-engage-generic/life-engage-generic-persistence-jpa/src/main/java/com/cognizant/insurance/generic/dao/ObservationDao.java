/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.dao;


import com.cognizant.insurance.dao.Dao;

/**
 * The Interface interface AuditDao.
 *
 * @author 300797
 */
public interface ObservationDao extends Dao {

}
