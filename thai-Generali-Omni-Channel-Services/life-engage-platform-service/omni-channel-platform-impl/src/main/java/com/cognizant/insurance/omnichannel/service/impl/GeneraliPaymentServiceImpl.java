/**
 *
 * Copyright 2016, Cognizant 
 *
 * @author        : 291446
 * @version       : 0.1, Sep 08, 2016
 */
package com.cognizant.insurance.omnichannel.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliManagerialLevelComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliPaymentComponent;
import com.cognizant.insurance.omnichannel.domain.TinyUrlGenerator;
import com.cognizant.insurance.omnichannel.service.GeneraliManagerialLevelService;
import com.cognizant.insurance.omnichannel.service.GeneraliPaymentService;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;

/**
 * The Class class GeneraliPaymentServiceImpl.
 * 
 * @author 291446
 */
@Service("gliPaymentService")
public class GeneraliPaymentServiceImpl implements GeneraliPaymentService {

	@Autowired
    private GeneraliPaymentComponent generaliPaymentComponent;
    
    /** The Constant PaymentOption. */
    private static final String PAYMENTOPTION = "PaymentOption";
    
    private static final String PAYMENTTYPE = "paymentType";

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliPaymentServiceImpl.class);
	
	
	@Override
	@Transactional(rollbackFor = { Exception.class }, readOnly = true)
	public String retrievePaymentService(String json)
			throws BusinessException {
		
		LOGGER.trace("Inside GeneraliPaymentServiceImpl.retrievePaymentService :json " + json);
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        String transactionArray = "{\"Transactions\":[]}";
        String response = "";
        String paymentOption = "";
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
            if (jsonRqArray.length() > 0) {
                for (int i = 0; i < jsonRqArray.length(); i++) {                    
                		final JSONObject jsonObj = jsonRqArray.getJSONObject(i);                   
                    	paymentOption = (String)jsonObj.getString(PAYMENTOPTION);
                	}
                    if(Constants.MANUALBANKING.equalsIgnoreCase(paymentOption) || Constants.POS.equalsIgnoreCase(paymentOption)){
                    	response = generaliPaymentComponent.isRecieptNoExisting(jsonRqArray, requestInfo);
                    }else {
                    	response = generaliPaymentComponent.retrievePaymentService(json);
                    }         	         
              }
        } catch (ParseException e) {
            LOGGER.trace("GeneraliPaymentServiceImpl.retrievePaymentService: Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        LOGGER.trace("Inside GeneraliPaymentServiceImpl retrievePaymentService : jsonObjectRs" + jsonObjectRs.toString());
        return response;
	}
	
	@Override
	public String makePayment(String json)
 throws BusinessException {
		String response = StringUtils.EMPTY;
		String randomId = StringUtils.EMPTY;
		String paymentOption = StringUtils.EMPTY;
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
			RequestInfo requestInfo;
			requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
			LifeEngageEmail saveEmailResponse;

			final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
			final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
			JSONObject jsonObj = null;
			if (jsonRqArray.length() > 0) {
				for (int i = 0; i < jsonRqArray.length(); i++) {
					jsonObj = jsonRqArray.getJSONObject(i);
					paymentOption = jsonObj.getString(Constants.KEY1);
				}
				if (Constants.CARD.equalsIgnoreCase(paymentOption)) {
					response = generaliPaymentComponent.makePaymentViaURL(jsonObj, requestInfo.getTransactionId());
				}

				else if (Constants.SEND_BY_URL.equalsIgnoreCase(paymentOption)) {
					randomId = generaliPaymentComponent.makePaymentViaURL(jsonObj, requestInfo.getTransactionId());
					response = generaliPaymentComponent.retrieveAndSendMail(jsonObj, randomId, requestInfo);
					JSONObject mailObject = new JSONObject(response);
					saveEmailResponse = generaliPaymentComponent.saveGeneraliEmail(mailObject);
					generaliPaymentComponent.triggerEmail(saveEmailResponse.getAgentId(), saveEmailResponse.getType());
				}
			}

		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return response;//randomId;
	}
	
	@Override
	public TinyUrlGenerator makePaymentViaUrl(String uuid,String id)
			 throws BusinessException {
		TinyUrlGenerator finalResponse = null;
					finalResponse = generaliPaymentComponent.makePaymentViaUrl(uuid,id);
					return finalResponse;
				}
	
	/*@Override
	public String getCardPaymentStatus(String refNmber)
			throws BusinessException {
		String response = StringUtils.EMPTY;
		response = generaliPaymentComponent.getCardPaymentStatus(refNmber);
		return response;
		
	}*/
	
	@Override
	public String getUrlPaymentStatus(String json)
			throws BusinessException {
		String response = StringUtils.EMPTY;
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
			RequestInfo requestInfo;
			requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);

			final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
			final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
			JSONObject jsonObj = null;
			if (jsonRqArray.length() > 0) {
				for (int i = 0; i < jsonRqArray.length(); i++) {
					jsonObj = jsonRqArray.getJSONObject(i);
					response = generaliPaymentComponent.getUrlPaymentStatus(jsonObj.toString());
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return response;
	}
	

}
