/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class RuleType.
 * 
 * @author 304007
 */

@Entity
@DiscriminatorValue("Rule")
public class RuleType extends Type {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3324385761400009327L;

    /** The rule specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "ruleType")
    private Set<RuleSpecification> ruleSpecifications;

    /**
     * Gets the rule specifications.
     *
     * @return the ruleSpecifications
     */
    public final Set<RuleSpecification> getRuleSpecifications() {
        return ruleSpecifications;
    }

    /**
     * Sets the rule specifications.
     *
     * @param ruleSpecifications the ruleSpecifications to set
     */
    public final void setRuleSpecifications(Set<RuleSpecification> ruleSpecifications) {
        this.ruleSpecifications = ruleSpecifications;
    }

    /**
     * The Constructor.
     */
    public RuleType() {
        super();
    }

    /**
     * The Constructor.
     * 
     * @param id
     *            the id
     */
    public RuleType(final String id) {
        super(id);
    }
}
