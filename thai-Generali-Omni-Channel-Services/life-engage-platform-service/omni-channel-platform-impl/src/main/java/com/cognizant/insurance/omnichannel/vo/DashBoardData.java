package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;

/**
 * The Class class DashBoardData.
 */
public class DashBoardData implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -127477911741190446L;

    /** The mtdData. */
    private MTDData mtdData;

    /** The ytdData. */
    private YTDData ytdData;

    /**
     * Gets the mtd data.
     * 
     * @return the mtd data
     */
    public MTDData getMtdData() {
        return mtdData;
    }

    /**
     * Sets the mtd data.
     * 
     * @param mtdData
     *            the new mtd data
     */
    public void setMtdData(MTDData mtdData) {
        this.mtdData = mtdData;
    }

    /**
     * Gets the ytd data.
     * 
     * @return the ytd data
     */
    public YTDData getYtdData() {
        return ytdData;
    }

    /**
     * Sets the ytd data.
     * 
     * @param mtdData
     *            the new ytd data
     */
    public void setYtdData(YTDData ytdData) {
        this.ytdData = ytdData;
    }

}
