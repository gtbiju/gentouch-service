package com.cognizant.insurance.omnichannel.component;
import java.util.Set;

import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.request.Request;


public interface UserSelectionComponent {
	
	void saveUserSelection(Request<Set<UserSelection>> request);
	
	void saveUserSelection(Set<UserSelection> selections);
}
