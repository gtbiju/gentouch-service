package com.cognizant.insurance.omnichannel.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.icr.rule.bom.RuleCriteria;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.RulesRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.InputValidationException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.agreement.AddressTranslation;
import com.cognizant.insurance.domain.agreement.AgreementExtension;
import com.cognizant.insurance.domain.agreement.BPMRiderDetails;
import com.cognizant.insurance.domain.agreement.CessTerm;
import com.cognizant.insurance.domain.agreement.Coverage;
import com.cognizant.insurance.domain.agreement.RiderSequence;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.agreementcodelists.CoverageCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.contactandplace.ContactPreference;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.ElectronicContact;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.party.partyname.PersonName;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliDocumentComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliEmailComponent;
import com.cognizant.insurance.omnichannel.component.LifeAsiaComponent;
import com.cognizant.insurance.omnichannel.component.LifeAsiaRequestSubmitter;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliEAppRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliLMSRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliRequirementDocumentFileRepository;
import com.cognizant.insurance.omnichannel.component.repository.LifeAsiaRepository;
import com.cognizant.insurance.omnichannel.component.repository.UserRepository;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaMetaData;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaRequest;
import com.cognizant.insurance.omnichannel.domain.UnderWriterMemoDetails;
import com.cognizant.insurance.omnichannel.jpa.component.LifeAsiaRequestComponent;
import com.cognizant.insurance.omnichannel.lifeasia.searchcriteria.LifeAsiaSearchCriteria;
import com.cognizant.insurance.omnichannel.smooks.OmniSoapSmooksHolder;
import com.cognizant.insurance.omnichannel.utils.MemoApplicationData;
import com.cognizant.insurance.omnichannel.vo.GeneraliTransactions;
import com.cognizant.insurance.omnichannel.vo.LifeAsiaDataVo;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;

@Component
public class LifeAsiaComponentImpl implements LifeAsiaComponent {

	private static final String CESS_TERM = "riderCessTerm";

	private static final String RIDER_RISK_CESS_TERM = "RiderRiskCessTerm";

	private static final String RIDER_BENEFIT_CESS_TERM = "RiderBenefitCessTerm";

	private static final String RIDER_PREMIUM_CESS_TERM = "RiderPremiumCessTerm";

	private static final String RIDER_PLAN_CODE = "RiderPlanCode";

	private static final String BASE_RISK_CESS_TERM = "baseRiskCessTerm";

	private static final String BASE_BENEFIT_CESS_TERM = "baseBenefitCessTerm";

	private static final String BASE_PREMIUM_CESS_TERM = "basePremiumCessTerm";

	private static final String VALIDATION_GROUP = "15";

	private static final String CESS_TERM_RULE = "Cess_Term_Rule";

	@Autowired
	@Qualifier("generateBPMMapping")
	private OmniSoapSmooksHolder generateBPMMapping;
	
	@Autowired
	@Qualifier("eAppSaveMapping")
	private LifeEngageSmooksHolder saveEAppHolder;
	
	@Autowired
	@Qualifier("eAppRetrieveMapping")
	private LifeEngageSmooksHolder retrieveEAppHolder;
	
	@Autowired
	@Qualifier("generateBPMStatusMapping")
	private LifeEngageSmooksHolder generateBPMStatusMapping;
	
	@Autowired
	@Qualifier("generateICMData")
	private OmniSoapSmooksHolder generateICMData;
	
	@Autowired
    GeneraliRequirementDocumentFileRepository generaliRequirementDocumentFileRepository;
	
	/** The rule repository. */
    @Autowired
    private RulesRepository rulesRepository;
	
	@Value("${generali.platform.service.memoEmail.subject}")
	private String memoEmailSubject;
    
	@Value("${le.platform.lifeasia.spajPath}")
	private String fileUploadPath;
	
    @Value( "${le.platform.service.fileUploadPath}" )
    private String reqFileUploadPath;
    
    @Value( "${generali.platform.bpm.fileUploadPath}" )
    private String reqFileUploadBPMPath;
    
    @Value( "${le.platform.las.submissionErrorMessage}" )
    private String lasSubmissionErrorMsg;
    
    @Value( "${generali.platform.bpm.memoUpdate.url}" )
    private String bpmMemoUpdateURL;
    
    @Value("${le.platform.default.previousPolicyTemplateId}")
    private String previousPolicyTemplateId;
    
    @Value("${le.platform.default.eAppTemplate}")
    private String eAppTemplateId;
    
    /** The client id. */
    @Value("${le.platform.service.illustration.clientId}")
    private String clientId;
    
    /** The retrieve memo holder. */
    @Autowired
    @Qualifier("retrieveMemoDetails")
    private LifeEngageSmooksHolder retrieveMemoDetail;
    
	@Autowired
	private LifeAsiaRequestSubmitter bpmRequestSubmitter;
	@Autowired
	private GeneraliEAppRepository eAppRepo;
	@Autowired
	GeneraliLMSRepository lmsRepository;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private LifeAsiaRepository bpmRepository;
	
	@Autowired
	private LifeAsiaRequestComponent lifeAsiaComponent;
	
	@Autowired
	private GeneraliEmailComponent generaliEmailComponent;
	
	@Autowired
    private GeneraliDocumentComponent lifeEngageDocumentComponent;
	
	@PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;
	
	private ContextTypeCodeList contextId;

	public static final Logger LOGGER = LoggerFactory
			.getLogger(LifeAsiaComponentImpl.class);
	
	private static final String LIFE_ASIA_SUCCESS_STATUS="200";
	private static final String META_KEY_LAS_AVL="LAS_IS_LIVE";

	private static final String LAS_SUBMISSION_ERROR = "LAS_ERROR";	
	
	private static final String MAIL_SUBJECT = "mailSubject";
    
    private static final String CC_MAIL_ID = "ccMailIds";
    
    private static final String TO_MAIL_ID = "toMailIds";
    
    private static final String LEAD_NAME = "leadName";
    
    /** The Constant E_APP. */
    private static final String E_APP = "eApp";
    
    private static final Set<String> LAS_IGNORE_STATUS_LIST = new HashSet<String>(Arrays.asList(
		     new String[] {"NS"}
		));
	
	/**
	 * Creating DB entry and request file for the <b>BPM</b>.</br>
	 * Also  creating the <b>ICM</b> meta data files 
	 * 		
	 */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public LifeAsiaRequest createLifeAsiaRequest(String eAppIdentifier) throws BusinessException {
       if (!bpmRepository.isExistingApplication(eAppIdentifier)) {
            LOGGER.info("create Life-Asia request for Application : " + eAppIdentifier);

            //Fetching eApp details from DB
            Transactions transactions = eAppRepo.retrieveEApp(eAppIdentifier, null);
            
            try {
				setRiderDetails(transactions);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				LOGGER.error("Parse failed in rider details section:"
						+ e1.getMessage()+ "	"+e1.getCause());
			}
            //Creating JSON and executing rule for fetching Cess term data
            String ruleResult = executeCessTermRule(transactions);
            //Retrieving base and rider cess term details
            if(ruleResult != null) {
            	try {
            		Set<CessTerm> cessCoverageSet = getCessTermDetails(transactions, ruleResult);
					if(transactions.getCessTerm() != null) {
						transactions.getCessTerm().setRiderCessTermSet(cessCoverageSet);
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					LOGGER.error("Parse failed in cess term rule :"
							+ e.getMessage());
				}
            }
            // Fetching Translated address from DB
            try {
                List<AddressTranslation> addressTranslation =
                        bpmRepository.getTranslatedAddress(new AddressTranslation());
                transactions.setAddressTranslation(addressTranslation);
            } catch (Exception e) {
                LOGGER.error("Error while reading thai address from DB");
            }
            // Fetching agent details from ODS DB
            Transactions agentTransaction = new Transactions();
            agentTransaction.setKey1(transactions.getProposal().getAgentId());
            agentTransaction = userRepository.validateAgentProfile(agentTransaction);
            transactions.setGeneraligent(agentTransaction.getGeneraligent());
            // nofifySuccessfulSale(transactions);
            // Method to update eApp status as Submitted while pushing data to Appain system
           // bpmRepository.updateAgreementWithStauts("Submitted", eAppIdentifier, "", "");

        	LifeAsiaRequest request = createBPMRequestObject(transactions);

        	try {
				createICMMetaData(transactions, false);
			} catch (Exception e) {
				LOGGER.error("BPM Share path file generation failed"
						+ e.getMessage());
			}
            if (request != null && (request.geteAppId() == null || request.geteAppId().isEmpty())) {
                request.seteAppId(eAppIdentifier);
            }
            // submit(request);
            return bpmRepository.save(request);
       } else {
            LOGGER.warn("Application " + eAppIdentifier + " is already Submitted to BPM ");
        }
        return null;
            
    }

	private void setRiderDetails(Transactions transactions) {
		List<RiderSequence> riderSequence = eAppRepo.retrieveRiderSequence();
		BPMRiderDetails riderDetails = new BPMRiderDetails();
		riderDetails.setRiderSequenceList(riderSequence);
		Set<Coverage> coverageSet = transactions.getProposal().getIncludesCoverage();
		Iterator<Coverage> coverageIter = coverageSet.iterator();
		Set<Coverage> riderCoverageSet = new HashSet<Coverage>(); 
		while(coverageIter.hasNext()) {
			Coverage riderCov = coverageIter.next();
			if(riderCov.getCoverageCode().toString().equalsIgnoreCase(CoverageCodeList.Rider.toString())) {
				riderCoverageSet.add(riderCov);
			}
		}
		riderDetails.setIncludesCoverage(riderCoverageSet);
		transactions.setRiderDetails(riderDetails);
	}

	/**
	 * @param transactions
	 * @param ruleResult
	 * @return
	 * @throws ParseException
	 */
	private Set<CessTerm> getCessTermDetails(Transactions transactions, String ruleResult) throws ParseException {
		//Base plan cess data
		JSONObject ruleResultObj = new JSONObject(ruleResult);
		
		String basePremiumCessTerm = ruleResultObj.has(BASE_PREMIUM_CESS_TERM)?ruleResultObj.getString(BASE_PREMIUM_CESS_TERM):StringUtils.EMPTY;
		String baseBenefitCessTerm = ruleResultObj.has(BASE_BENEFIT_CESS_TERM)?ruleResultObj.getString(BASE_BENEFIT_CESS_TERM):StringUtils.EMPTY;
		String baseRiskCessTerm = ruleResultObj.has(BASE_RISK_CESS_TERM)?ruleResultObj.getString(BASE_RISK_CESS_TERM):StringUtils.EMPTY;
		CessTerm objCessTerm =  new CessTerm();	
		transactions.setCessTerm(objCessTerm);
		
		if(transactions.getCessTerm() != null) {
			transactions.getCessTerm().setPremiumCessTerm(basePremiumCessTerm);
			transactions.getCessTerm().setBenefitCessTerm(baseBenefitCessTerm);
			transactions.getCessTerm().setRiskCessTerm(baseRiskCessTerm);
		}
		
		//Rider plan cess data
		Set<CessTerm> cessCoverageSet = new HashSet<CessTerm>();
		CessTerm cessValue;
		JSONArray cessTermArray = ruleResultObj.has(CESS_TERM)?ruleResultObj.getJSONArray(CESS_TERM): new JSONArray();
		
		for(int i=0 ; i < cessTermArray.length(); i++) {
			cessValue = new CessTerm();
			JSONObject cessJsonValue = (JSONObject) (cessTermArray.get(i)!=null?cessTermArray.get(i):new JSONObject());
			String riderPlanCode = cessJsonValue.has(RIDER_PLAN_CODE)?cessJsonValue.getString(RIDER_PLAN_CODE):StringUtils.EMPTY;
			String riderPremiumCessTerm = cessJsonValue.has(RIDER_PREMIUM_CESS_TERM)?cessJsonValue.getString(RIDER_PREMIUM_CESS_TERM):StringUtils.EMPTY;
			String riderBenefitCessTerm = cessJsonValue.has(RIDER_BENEFIT_CESS_TERM)?cessJsonValue.getString(RIDER_BENEFIT_CESS_TERM):StringUtils.EMPTY;
			String riderRiskCessTerm = cessJsonValue.has(RIDER_RISK_CESS_TERM)?cessJsonValue.getString(RIDER_RISK_CESS_TERM):StringUtils.EMPTY;
			cessValue.setRiderPlanCode(riderPlanCode);
			cessValue.setPremiumCessTerm(riderPremiumCessTerm);
			cessValue.setBenefitCessTerm(riderBenefitCessTerm);
			cessValue.setRiskCessTerm(riderRiskCessTerm);
			cessCoverageSet.add(cessValue);
		}
		return cessCoverageSet;
	}

	private String executeCessTermRule(Transactions transactions) {
		//Building request for retrieving cess term data from rule
		JSONObject jsonObject = new JSONObject();
		//Null check need to be implemented
		Person insuredPerson = (Person) transactions.getInsured().getPlayerParty();
		Person payerPerson = (Person) transactions.getPayer().getPlayerParty();
		
		JSONObject insuredObj = new JSONObject();
		insuredObj.put("Age", insuredPerson.getAge());
         
		JSONObject payerObj = new JSONObject();
		payerObj.put("Age", payerPerson.getAge());
         
		jsonObject.put("Insured", insuredObj);
		jsonObject.put("Payer", payerObj);
		
		String basePlanCode = StringUtils.EMPTY;
		String riderPlanCode = StringUtils.EMPTY;
		Set<Coverage> coverageSet;
		JSONArray array = new JSONArray(); 
		if(transactions.getProposal()!= null && transactions.getProposal().getIncludesCoverage() != null) {
			coverageSet = transactions.getProposal().getIncludesCoverage();	 
			Iterator<Coverage> iter = coverageSet.iterator();
			while(iter.hasNext()) {
				Coverage coverageVal = iter.next();
				if(coverageVal.getCoverageCode()!=null && coverageVal.getCoverageCode().equals(CoverageCodeList.BasicCoverage)) {
					basePlanCode = coverageVal.getPlanCode();
				}
				if(coverageVal.getCoverageCode()!=null && coverageVal.getCoverageCode().equals(CoverageCodeList.Rider)) {
					riderPlanCode = coverageVal.getPlanCode();
					array.put(riderPlanCode);
				}
			}
			
			jsonObject.put("basePlanCode", basePlanCode!=null?basePlanCode:StringUtils.EMPTY);
			//Rider
			jsonObject.put("riderplanCode", array!=null?array:new JSONArray());
		}     
		//JSONObject ruleInput = new JSONObject();
		//ruleInput.put("ruleInput", jsonObject);
		String inputJson = jsonObject.toString();
		final String replacedInputJson = inputJson.replaceAll("\\r\\n|\\r|\\n", " ");
		RuleCriteria ruleCriteria = new RuleCriteria();
		ruleCriteria.setRule(CESS_TERM_RULE);
		ruleCriteria.setClientId(clientId);
		ruleCriteria.setCategory(VALIDATION_GROUP);
		String ruleResult = null;
		//Executing rule
		LOGGER.info("Rule cess term request: "+ replacedInputJson);
		try {
		    ruleResult = rulesRepository.executeRule(ruleCriteria, replacedInputJson);
		    LOGGER.info("Rule cess term response: "+ ruleResult);
		} catch (Exception e) {
			LOGGER.error("Failed to return Cess term values from Rule  : "+e.getMessage()+"   "+e.getCause());
		    //throwSystemException(true, "Oops....Failed to return Cess term values from Rule : " + e.getMessage());
		    
		}
		return ruleResult;
	}

	@Override
	@Transactional(rollbackFor = { Exception.class })
	public LifeAsiaRequest createLifeAsiaRequest(Transactions transactions) throws Exception {
		LifeAsiaRequest object = createBPMRequestObject(transactions);
		object.seteAppId(transactions.getKey4());
		return bpmRepository.save(object);
	}

	private LifeAsiaRequest createBPMRequestObject(Transactions transactions) throws BusinessException {
		LOGGER.info("On Life-Asia Component : Start Creating BPM request object : "+transactions.getProposal().getTransactionKeys().getKey21());
		
		createBPMRequestFile(transactions);
		LifeAsiaRequest bpmRequest = new LifeAsiaRequest();
		bpmRequest.setCreationDate(new Date());
		bpmRequest.setAgentId(transactions.getProposal().getAgentId());
		//Application Number
		if(transactions.getProposal().getTransactionKeys()!=null){
			bpmRequest.setSpajNo(transactions.getProposal().getTransactionKeys().getKey21());
		}		
		bpmRequest.setRetryAttempt(0);
		LOGGER.info("On Life-Asia Component : finished the BPM request object :"+transactions.getProposal().getTransactionKeys().getKey21());
		
		return bpmRequest;
	}
	
	private void createBPMRequestFile(String  eAppIdentifer) throws BusinessException{ 
		Transactions transactions = eAppRepo.retrieveEApp(eAppIdentifer, null);
		createBPMRequestFile(transactions);
	}
	/**
	 * Method to write Appain Request JSON in AppServer Physical location
	 * @param transactions
	 * @throws BusinessException
	 */
    private void createBPMRequestFile(Transactions transactions) throws BusinessException {
        File f = new File(fileUploadPath + transactions.getProposal().getTransactionKeys().getKey21() + ".json");
        if (f.exists()) {
            LOGGER.info("File exist for Application NO : " + transactions.getProposal().getTransactionKeys().getKey21());
            return;
        }

        FileWriterWithEncoding fileWriter = null;

        try {
            f.createNewFile();
            fileWriter = new FileWriterWithEncoding(f, "UTF-8");

            StreamResult sr = new StreamResult(fileWriter);
            transactions.setLastSyncDate(new Date());
            generateBPMMapping.parseBO(transactions, sr);
            
            //File destFile = new File(reqFileUploadBPMPath+"Application"+File.separator+transactions.getProposal().getTransactionKeys().getKey21() + ".txt");
            //copyFileUsingStream(f, destFile);
        } catch (IOException e) {
            throw new BusinessException(e);
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                LOGGER.error("Failed Write BPM request " + e.getMessage());
            }
        }
    }
	@Transactional
	public void createICMInfo(String eAppId,boolean isResubmit){
		LOGGER.info("Recived ICM meta creation for Application : "+eAppId+"  with resubmission :"+isResubmit);
		GeneraliTransactions transactions = eAppRepo.retrieveEAppWithMemo(eAppId, null);
		try {
			createICMMetaData(transactions,isResubmit);
		} catch (BusinessException e) {
			LOGGER.error("Failed to Write ICM info "+e.getMessage());
		}
	}
	

    private void createICMMetaData(Transactions transactions,boolean isResubmit) throws BusinessException{
		/** Dirty fix for issue Signature line duplicate in ICM meta file -- Please remove if we fixed the issue from UI**/
		Map<String, String> signatureMap =new HashMap<String, String>();
		JSONArray jsonMemoArray = new JSONArray();
		String applicationNo=transactions.getProposal().getTransactionKeys().getKey21();
		int SPAJNoLength = applicationNo.length();
		String identifier=transactions.getProposal().getIdentifier();
		Set<String> updatedMemoIds = new HashSet<String>();
		File dir=new File(reqFileUploadBPMPath+applicationNo);
        if(!transactions.getProposal().getRequirements().isEmpty()){
		    if (!dir.exists() && !dir.mkdirs()) {
                LOGGER.error("Failed to create directory : " + dir.getPath());
                throwSystemException(true, Constants.FAILED_DIRECTORY_CREATION);
            }
        }
        
        if(applicationNo!=null && SPAJNoLength==GeneraliConstants.SPAJAPPLENGTH) {
        		LOGGER.info("Manual Application Number and Not Copying Eapp and PreviousPdf file ");
        }else if (!isResubmit) {
        		LOGGER.info("Generated Application Number and Copying Eapp and PreviousPdf file ");
            createAndMoveEappPdfToBPM(applicationNo, identifier);
        }
        
		for (Requirement requirement : transactions.getProposal().getRequirements()) {
		    if(isResubmit && requirement.getRequirementType().equals(DocumentTypeCodeList.Memo)){
                dir=new File(reqFileUploadBPMPath+applicationNo+File.separator+"Memo"+File.separator+requirement.getRequirementName());
                if (!dir.exists() && !dir.mkdirs()) {
                    LOGGER.error("Failed to create directory : " + dir.getPath());
                    throwSystemException(true, Constants.FAILED_DIRECTORY_CREATION);
                }
            }
			String subType=requirement.getRequirementSubType();
			if((subType!=null &&  ( requirement.getRequirementName().equals(GeneraliConstants.GAO) )) || (isResubmit && !requirement.getRequirementType().equals(DocumentTypeCodeList.Memo))){
				continue;
			}
            for (Document document : requirement.getRequirementDocuments()) {
                String formId = document.getDocumentTypeCode();
                if (!"Deleted".equals(document.getDocumentStatus())) {
                    for (Document page : document.getPages()) {
                        if (!"Deleted".equals(page.getDocumentStatus())) {
                            if (formId != null && "signature".equalsIgnoreCase(formId)) {
                                if (signatureMap.containsKey(subType)) {
                                    LOGGER.info("ICM Signature already foudnd for :" + subType + " ,application :"
                                            + applicationNo);
                                    continue;
                                } else {
                                    signatureMap.put(subType, "");
                                }
                            }

                            File sourceFile =
                                    new File(reqFileUploadPath + transactions.getProposal().getIdentifier()
                                            + File.separator + "REQUIREMENTS" + File.separator + page.getFileNames());
                            File destFile = null;
                            if (isResubmit && requirement.getRequirementType().equals(DocumentTypeCodeList.Memo)) {
                                if (requirement.getPartyIdentifier().equals("Open")) {
                                    if (document.getModifiableIndicator()) {
                                        String filePath =
                                                reqFileUploadBPMPath + applicationNo + File.separator + "Memo"
                                                        + File.separator + requirement.getRequirementName()
                                                        + File.separator + page.getFileNames();
                                        destFile = new File(filePath);
                                        copyFileUsingStream(sourceFile, destFile);
                                        JSONObject jsonObj = new JSONObject();
                                        jsonObj.put(GeneraliConstants.MEMO_DOC_ID, requirement.getRequirementName());
                                        jsonObj.put(GeneraliConstants.MEMO_CODE, requirement.getRequirementSubType());
                                        jsonObj.put(GeneraliConstants.DOC_NAME, page.getFileNames());
                                        jsonObj.put(GeneraliConstants.DOCLOCATION,
                                                filePath);
                                        jsonMemoArray.put(jsonObj);
                                        updatedMemoIds.add(requirement.getRequirementName());
                                    }
                                }/*
                                  * else
                                  * if(requirement.getPartyIdentifier().equals("Cancelled")||requirement.getPartyIdentifier
                                  * ().equals("Resolved")){ destFile = new
                                  * File(reqFileUploadBPMPath+transactions.getProposal
                                  * ().getIdentifier()+File.separator+requirement
                                  * .getRequirementName()+File.separator+page.getFileNames()); if(destFile.exists()){
                                  * destFile.delete(); } }
                                  */
                            } else if (!isResubmit) {
                            	 
                                destFile =
                                        new File(reqFileUploadBPMPath + applicationNo + File.separator + "App"
                                                + File.separator + page.getFileNames());
                                copyFileUsingStream(sourceFile, destFile);
                            }
                        }
                    }
                }//
            }
		}
    	if(isResubmit){
    	    JSONObject jsonObj = new JSONObject();
    		jsonObj.put(GeneraliConstants.APP_NO, applicationNo);
    		if(transactions.getProposal()!= null && transactions.getProposal().getAgreementExtension() != null 
    		        && transactions.getProposal().getAgreementExtension().getBpmStatus()!= null){
    		    jsonObj.put(GeneraliConstants.APP_STATUS, transactions.getProposal().getAgreementExtension().getBpmStatus());
    		}
    		jsonObj.put(GeneraliConstants.MEMO_DETAILS, jsonMemoArray);
    		JSONArray jsonArray = new JSONArray();
    		jsonArray.put(jsonObj);
    		JSONObject appObject = new JSONObject(); 
    		appObject.put(GeneraliConstants.EAPPDETAILS, jsonArray);
    		sendUpdateToBPM(appObject.toString(),bpmMemoUpdateURL);
    		updateSubmissionDate(updatedMemoIds,applicationNo);
        }
		/*for (AgreementDocument document:transactions.getProposal().getRelatedDocument()) {
			String formId= document.getDocumentTypeCode();
			if(formId!=null&&formId.equalsIgnoreCase("illustration")){
				File pdfFile=new File(reqFileUploadPath+transactions.getProposal().getIdentifier()+File.separator+document.getFileNames());
				if(pdfFile.isFile()&& pdfFile.exists()){
					File destFile = new File(reqFileUploadBPMPath+applicationNo+File.separator+document.getFileNames());
                    copyFileUsingStream(pdfFile, destFile);                        
				}					
			}				
		}*/		
	}
    
    private void createAndMoveEappPdfToBPM(String applicationNo, String identifier) {
        ByteArrayOutputStream byteStream = null;

        byte[] data = null;
        String sourcePath = null;
        File destFile = null;
        File sourceFile = null;
        String eAppFileName = "eApp.pdf";
        String previousPolicyFileName = "PreviousPolicy.pdf";
        try {
            LOGGER.info("BPM eApp PDF creation ->start");
            byteStream =
                    (ByteArrayOutputStream) lifeEngageDocumentComponent.generatePdfGet(identifier, E_APP,
                            eAppTemplateId);
            data = byteStream.toByteArray();
            sourcePath = reqFileUploadPath + identifier + File.separator + "REQUIREMENTS" + File.separator;
            createFile(eAppFileName, data, sourcePath);
            byteStream = null;
            byteStream =
                    (ByteArrayOutputStream) lifeEngageDocumentComponent.generatePdfGet(identifier, E_APP,
                            previousPolicyTemplateId);
            data = null;
            data = byteStream.toByteArray();
            createFile(previousPolicyFileName, data, sourcePath);

            LOGGER.info("BPM eApp PDF creation ->End");
            LOGGER.info("BPM eApp PDF copy ->Start");

            destFile =
                    new File(reqFileUploadBPMPath + applicationNo + File.separator + "App" + File.separator
                            + eAppFileName);
            sourceFile =
                    new File(reqFileUploadPath + identifier + File.separator + "REQUIREMENTS" + File.separator
                            + eAppFileName);
            copyFileUsingStream(sourceFile, destFile);

            destFile =
                    new File(reqFileUploadBPMPath + applicationNo + File.separator + "App" + File.separator
                            + previousPolicyFileName);
            sourceFile =
                    new File(reqFileUploadPath + identifier + File.separator + "REQUIREMENTS" + File.separator
                            + previousPolicyFileName);
            copyFileUsingStream(sourceFile, destFile);

            LOGGER.info("BPM eApp PDF copy ->End");
        } catch (Exception e) {
            LOGGER.error("Failed to move eApp pdfs to BPM" + e.getMessage());
        }

    }

    private void createFile(final String fileName, final byte[] fileByteArray, final String path) throws IOException {

        if (fileName != null) {

            final File file = new File(path + fileName);
            if (file.exists()) {
                if (file.delete()) {
                    FileOutputStream fos = null;
                    try {
                        LOGGER.error("File Deleted: " + file.getPath());
                        final File someFile = new File(path, fileName);
                        someFile.createNewFile();
                        fos = new FileOutputStream(someFile);
                        fos.write(fileByteArray);

                    } finally {
                        fos.flush();
                        fos.close();
                    }
                } else {
                    LOGGER.error("File Deletion Error! : " + file.getPath());
                }
            } else {
                FileOutputStream fos = null;
                try {
                    final File someFile = new File(path, fileName);
                    someFile.createNewFile();
                    fos = new FileOutputStream(someFile);
                    fos.write(fileByteArray);

                } finally {
                    fos.flush();
                    fos.close();
                }
            }
        }
    }

    private void updateSubmissionDate(Set<String> updatedMemoIds,String applicationNo) {
    	Transactions transactions = new Transactions();
    	transactions.setKey21(applicationNo);
    	Response<List<AppianMemoDetails>> memoResponse = bpmRepository.retrieveMemoDetails(transactions,GeneraliConstants.UPDATE);
    	List<AppianMemoDetails> memoList = memoResponse.getType();
    	for(AppianMemoDetails memoDetails:memoList){
    		if(updatedMemoIds.contains(memoDetails.getMemoId())){	    		
					memoDetails.setSubmissionDate(LifeEngageComponentHelper.getCurrentdateString());
					bpmRepository.updateMemoDetails(memoDetails, GeneraliConstants.UPDATE);	    		
    		}
    	}
		
	}

	@Override
    public String sendUpdateToBPM(String json, String url) throws BusinessException{
        LOGGER.info("sendUpdateToBPM memo submission trigger");
        return bpmRequestSubmitter.postJSONToBPM(json,url);
    }
	
  /**
   * Copies a file using the File streams
   * @param source
   * @param dest
   */
  private static void copyFileUsingStream(File source, File dest)throws BusinessException  {
      try {
          FileUtils.copyFile(source, dest);
      }catch (IOException e) {
          throw new BusinessException("Error while sending data to BPM"); 
      }   
    }

	

    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void submit(LifeAsiaRequest bpmReq) {
        String response = null;
        if (bpmReq.getRetryAttempt() > 3) {
            LOGGER.info("Max attempt exceeded " + bpmReq.getSpajNo());
            bpmReq.setStatus("IGNORED");
            bpmRepository.save(bpmReq);
            return;
        }
        bpmReq.setStatus("FAILED");
        try {
            Date d1 = new Date();
            File f = new File(fileUploadPath + bpmReq.getSpajNo() + ".json");
            if (!f.exists()) {
                LOGGER.info("File is not existing.. trying to create new json file  : " + bpmReq.getSpajNo());
                createBPMRequestFile(bpmReq.geteAppId());
            }


            String source = new String(Files.readAllBytes(Paths.get(fileUploadPath + bpmReq.getSpajNo() + ".json")),Charset.forName("UTF-8"));
            //JSONObject jsonObj = new JSONObject();
            //jsonObj.put("AppNo", bpmReq.getSpajNo());
            
            LOGGER.info("File read Completed : " + bpmReq.getSpajNo());
            //LOGGER.info("Data .."+jsonObj.toString());
            bpmReq.setRequestData(source);
            response = bpmRequestSubmitter.submit(source);
            Date d2 = new Date();
            LOGGER.info("Life-Asia Response time : " + (d2.getTime() - d1.getTime()));

            bpmReq.setStatus("SUBMITTED");

        } catch (BusinessException e) {
            LOGGER.error("BPM Submission failed for " + bpmReq.getSpajNo() + " " + e.getMessage());
            response = e.getMessage();
        } catch (Exception e) {
            LOGGER.error("BPM Submission failed for " + bpmReq.getSpajNo() + " " + e.getMessage());
            response = e.getMessage();
        } catch (Throwable e) {
            LOGGER.error("BPM Submission failed " + e.getMessage());
            response = e.getMessage();
        }
        bpmReq.setResponseData(response);
        bpmReq.setRetryAttempt(bpmReq.getRetryAttempt() + 1);
        bpmRepository.save(bpmReq);

        LOGGER.info("Submitting....");
        

    }

	/*
	private void nofifySuccessfulSale(Transactions transactions){
		String eAppId=transactions.getProposal().getIdentifier();
		LOGGER.info("LifeAsia BackFlow: Ready to Nofify Successful Sale for eApp : "+eAppId);		
		Coverage product=null;
		BigDecimal totalIniPremium=null;
		for (Coverage coverage : transactions.getProposal().getIncludesCoverage()) {
			if(coverage.getCoverageCode()== CoverageCodeList.BasicCoverage){
				product=coverage;
				break;
			}
		}
		for (Premium premium : product.getPremium()) {
			if(premium.getNatureCode()==PremiumNatureCodeList.TotalInitialPremium){				
				totalIniPremium=premium.getAmount().getAmount();
			}			
		}
		/* Notify LMS*/
		/*LOGGER.info("LifeAsia BackFlow: notifySuccessfulSale for eApp : "+eAppId +" With LMS tracking Id : "
				+transactions.getProposal().getTransactionKeys().getKey1()+" Proposal No"+transactions.getProposal().getTransactionKeys().getKey21()+
				", Total Premium"+totalIniPremium);
		String lmsUpdateResponse=lmsRepository.notifySuccessfulSale(transactions.getProposal().getTransactionKeys().getKey1(),
				transactions.getProposal().getTransactionKeys().getKey21(),
				totalIniPremium, new Date(), product.getProductCode().getCode());*/
		/*Notify eApp*/
		/*bpmRepository.updateAgreementWithStauts("Submitted", eAppId, "", "");*/
		
		/*LOGGER.info("LifeAsia BackFlow: Completed Successful Sale notification for eApp : "+eAppId +" With LMS tracking Id : "
				+transactions.getProposal().getTransactionKeys().getKey1()+" "+lmsUpdateResponse);
		
	}*/

	@Override
	@Transactional(rollbackFor = { Exception.class })
	public void submit(Number bpmRequestId) {
	    //Fetching Data from LifeAsiaRequest table
		LifeAsiaRequest bpmRequest = bpmRepository.getRequestData(String
				.valueOf(bpmRequestId));
		submit(bpmRequest);

	}

	@Override
	@Transactional
	public String createLifeAsiaRequestString(RequestInfo requestInfo,JSONObject jsonObject) throws BusinessException {
					
		String eAppId= jsonObject.getString("Key4");
		Transactions transactions=null;
		if(eAppId!=null&&!eAppId.isEmpty()){
			transactions=eAppRepo.retrieveEApp(eAppId, "");
		}else{
			transactions =(Transactions) saveEAppHolder.parseJson(jsonObject.toString());
		}
		LifeAsiaRequest bpmRequest=createBPMRequestObject(transactions);
		
		return bpmRequest.getRequestData();
	}
	@Override
	public String submitLifeAsiaWithPayload(String bpmReq) {
		String response = null;
		try {
			 response = bpmRequestSubmitter.submit(bpmReq);
		} catch (BusinessException e) {
			LOGGER.error(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return response;
		
	}
	@Transactional(rollbackFor = { Exception.class },propagation=Propagation.REQUIRES_NEW)
	public void fetchAndUpdateStatus(String agentId){
		Date d1=new Date();
		LOGGER.info("LifeAsia Status Fetch Start for :"+agentId);
		Set<LifeAsiaDataVo> statusChanges;
		try {
			statusChanges = fetchBPMStatusChange(agentId);
			for (LifeAsiaDataVo lifeAsiaDataVo : statusChanges) {
				updateApplication(lifeAsiaDataVo.getDocumentId(), lifeAsiaDataVo.getStatus(), lifeAsiaDataVo.getMessages());			
			}
		} catch (BusinessException e) {
			LOGGER.error("Unable to Update Application Status from Life Asia for agent "+agentId+" : "+e.getMessage());
		}catch (Exception e) {
			LOGGER.error("Unable to Update Application Status from Life Asia for agent "+agentId+" : "+e.getMessage());
		}
		Date d2=new Date();
		LOGGER.info("LifeAsia Status Fetch END  for :"+agentId+" Response Time : "+(d2.getTime()-d1.getTime()));
		
	}
	
	@Transactional(rollbackFor = { Exception.class })
	public Set<LifeAsiaDataVo> fetchBPMStatusChange(String agentId) throws BusinessException{
		List<String> status=new ArrayList<String>();
		Set<LifeAsiaDataVo> lifeAsiaDataVos=new HashSet<LifeAsiaDataVo>();
		/*if(!isLASAvailable()){
			LOGGER.info("Agent :"+agentId+" tried to fetch status from LAS, but LAS is not LIVE ");
			return lifeAsiaDataVos;
		}*/
		
		status.add(GeneraliConstants.SUBMITTED);
		status.add(GeneraliConstants.ERROR);
		status.add(GeneraliConstants.FAILED);
		status.add(GeneraliConstants.CREATED);
		status.add(GeneraliConstants.IGNORED);
		List<String> spajs =bpmRepository.getSubmittedProposals(agentId, status);
		LifeAsiaDataVo dataVo=new LifeAsiaDataVo();
		dataVo.setSpajIds(spajs);
		dataVo.setAgentId(agentId);
		String request=generateBPMStatusMapping.parseBO(dataVo);
		String statusResponse=bpmRequestSubmitter.fetchStatusChanges(request);
		JSONObject statusRes;
		JSONArray proposalStatusArray;
		
		try {
			statusRes = new JSONObject(statusResponse);
			proposalStatusArray=statusRes.getJSONObject("Response").getJSONObject("ResponsePayLoad").
			getJSONArray("Transactions").getJSONObject(0).
			getJSONObject("TransactionData").getJSONArray("ProposalStatusDetails");
			if(proposalStatusArray!=null){
				for (int i=0;i<proposalStatusArray.length();i++) {
					JSONObject proposalStatus=proposalStatusArray.getJSONObject(i);					
					LifeAsiaDataVo vo=new LifeAsiaDataVo();
					vo.setDocumentId(proposalStatus.getString("ApplicationNo"));
					vo.setStatus(proposalStatus.getString("StatusCode"));
					vo.setMessages(proposalStatus.getString("Remarks"));
					lifeAsiaDataVos.add(vo);
				}
			}
		} catch (ParseException e) {
			LOGGER.error("Unable to parse StatusResponse for "+agentId+" "+e.getMessage());
		}catch (NoSuchElementException e) {
			LOGGER.error("Unable to parse NoSuchElementException StatusResponse for "+agentId+" "+e.getMessage());
		}
		return lifeAsiaDataVos;
	}
	
	
	
	@Transactional( rollbackFor = { Exception.class })
	public boolean updateApplication(String bpmDocId,String status,String pendingInfo){
		String eAppidentifier=bpmRepository.getAppIdfromDocId(bpmDocId);		
		if(eAppidentifier!=null&& !eAppidentifier.isEmpty()){
			LOGGER.info("Updating application status of  "+eAppidentifier+" with DocId, status, info  :- "
					+bpmDocId+", "+status+", "+pendingInfo);
			if(status==null||status.isEmpty()||LAS_IGNORE_STATUS_LIST.contains(status.toUpperCase())){
				status=AgreementStatusCodeList.Submitted.name();
			}
				return	bpmRepository.updateAgreementWithStauts(status, eAppidentifier, pendingInfo, bpmDocId,"0");	
			
		}else {
			LOGGER.error("Unable to find the eApp Id from Doc Id "+bpmDocId);
		}
		return false;
	}
	/**
	 * @return <b>true</b> if LAS is Live, else retruen <b>false</b>
	 */
	public boolean isLASAvailable(){
		LifeAsiaMetaData metaData=processLAStatusResponse();
		if(metaData!=null && metaData.getValue()!=null && metaData.getValue().trim().toLowerCase().equals("0")){
			return true;
		}
		return false;
	}
	/**
	 * return Status in JSON Format 
	 * @return <b>statusCode 200</b> if is live else <b>100</b>.</br>
	 *  <b>msg_en</b> - message configured in English.</br>
	 *  <b>msg_vn</b> - message configured in Vietnamese.</br>
	 */
	public String isLASAvailable_withDetails(){
		boolean isLive=false;
		 String msg1="";
		 String msg2="";
		 LifeAsiaMetaData metaData=processLAStatusResponse();
		//If any exception
		 if(metaData==null||metaData.getValue()==null){
			 msg1="Syatem Error; Please contact Genova Support Team"; 
			 msg2=msg1;
		 }
		 //0- LA is up, we can proceed to submit eApp.
		 else if(metaData!=null && metaData.getValue()!=null && metaData.getValue().trim().toLowerCase().equals("0")){
			isLive=true;
			msg1=metaData.getMsg_en();
			msg2=metaData.getMsg_vn();
		}else if(metaData!=null && metaData.getValue()!=null){
			msg1=metaData.getMsg_en();
			msg2=metaData.getMsg_vn();
		}
		 
		JSONObject response=new JSONObject();
		JSONObject status=new JSONObject();
		response.put("status", status);
		if(isLive){			
			status.put("statusCode", 200);
			status.put("msg_en", msg1);
			status.put("msg_vn", msg2);
		}else{
			status.put("statusCode", 100);
			status.put("msg_en", msg1);
			status.put("msg_vn", msg2);
		}
		
		return response.toString();
	}
	

	/**
	 * Method to process response from getLAStatus web service.
	 * @return LifeAsiaMetaData with status code and status messages.
	 */
	public LifeAsiaMetaData processLAStatusResponse() {

		JSONObject statusRes;

		LifeAsiaMetaData metaData = new LifeAsiaMetaData();

		try {
			String lifeAsiaStatusDetail = bpmRequestSubmitter.getLAStatus();
			JSONObject responsJSonObj = new JSONObject(lifeAsiaStatusDetail);
			statusRes = responsJSonObj.getJSONObject("Response")
					.getJSONObject("ResponsePayLoad")
					.getJSONArray("Transactions").getJSONObject(0)
					.getJSONObject("TransactionData")
					.getJSONObject("StatusData");
			if (statusRes != null) {
				metaData.setValue(statusRes.getString("StatusCode"));
				LOGGER.info("LA Status: " + statusRes.getString("StatusCode"));
				JSONObject statusMessage = statusRes
						.getJSONObject("StatusMessage");
				metaData.setMsg_en(statusMessage.getString("en"));
				metaData.setMsg_vn(statusMessage.getString("vt"));

			}
		} catch (ParseException e) {
			LOGGER.error("Unable to parse LA Status Response " , e);
		} catch (NoSuchElementException e) {
			LOGGER.error("Unable to parse NoSuchElementException LA Status Response  "
					,e);
		} catch (BusinessException e) {
			LOGGER.error("Error in Getting respones from Glink LA Status Response  "
					,e);
		}

		return metaData;
	}
	
	@Override
	@Transactional(rollbackFor = { Exception.class }, readOnly = true)
	public String retrieveMemoDetails(RequestInfo requestInfo, JSONObject jsonRqObj) throws InputValidationException {
		String response = StringUtils.EMPTY;
		String spajNo = jsonRqObj.getString(Constants.KEY21);
		Transactions userTransactions = new Transactions();
        Response<List<AppianMemoDetails>> memoDetails = null;
	        try {
			if (spajNo != null && !("").equals(spajNo)) {
			    userTransactions.setKey21(spajNo);
		        String memoType = GeneraliConstants.RETRIEVE;
		        memoDetails = bpmRepository.retrieveMemoDetails(userTransactions,memoType);
			} else {
				throw new SystemException(ErrorConstants.LE_SYNC_ERR_105, GeneraliConstants.SPAJ_NO_MISSING);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to retrieve memo: " + e.getMessage());
		} finally {
			if (memoDetails != null) {
				response = retrieveMemoDetail.parseBO(memoDetails.getType());
			} else {
				response = GeneraliConstants.MEMO_FAILURE_MESSAGE;
			}
		}
		return response;
	}
	
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public String updateMemoDetails(MemoApplicationData application) throws InputValidationException {
		 
		String response = StringUtils.EMPTY;
		Boolean statusFlag = true;
		int memoCount = 0;
		try {
			Transactions userTransactions = new Transactions();
			userTransactions.setKey21(application.getAppNumber());
			LOGGER.info("Update from BPM ->"+application.getAppNumber());
			Response<List<AppianMemoDetails>> responseMemo = bpmRepository.retrieveMemoDetails(userTransactions,GeneraliConstants.UPDATE);
			List<AppianMemoDetails> existingMemoList = responseMemo.getType();
			if(existingMemoList == null){
			    existingMemoList = new ArrayList<AppianMemoDetails>(); 
			}
			
			Set<String> updatedMemos = new HashSet<String>();
			for(AppianMemoDetails appianMemo :application.getMemoList()){
			    boolean memoFlag = true;
			    for(AppianMemoDetails fetchedMemo :existingMemoList){
                    if(fetchedMemo.getMemoId().equals(appianMemo.getMemoId())){
                        memoFlag = false; 
                        fetchedMemo.setAppStatus(appianMemo.getAppStatus());
                        fetchedMemo.setCategory(appianMemo.getCategory());
                        fetchedMemo.setMemoDate(appianMemo.getMemoDate());
                        fetchedMemo.setMemoDesc(appianMemo.getMemoDesc());
                        fetchedMemo.setMemoReason(appianMemo.getMemoReason());
                        appianMemo.setSubmissionDate(fetchedMemo.getSubmissionDate());
                        fetchedMemo.setMemoStatus(appianMemo.getMemoStatus());
                        fetchedMemo.setRemarks(appianMemo.getRemarks());
                        fetchedMemo.setPolicyNumber(appianMemo.getPolicyNumber());
                        bpmRepository.updateMemoDetails(fetchedMemo, Constants.UPDATE);
                        updatedMemos.add(fetchedMemo.getMemoId());
                    }
                }
                if(memoFlag){
                	if(appianMemo.getMemoStatus().equals(GeneraliConstants.OPEN)){
                		memoCount++;
                	}
                    UnderWriterMemoDetails umMemo = bpmRepository.getMemoDescription(appianMemo.getMemoDetails().getMemoCode());
                    appianMemo.setMemoDetails(umMemo);
                    bpmRepository.updateMemoDetails(appianMemo, Constants.SAVE);
                }
			}
			for(AppianMemoDetails fetchedMemo :existingMemoList){
			    if((!updatedMemos.contains(fetchedMemo.getMemoId())) && fetchedMemo.getMemoStatus().equals(GeneraliConstants.OPEN)){
			        fetchedMemo.setMemoDate(application.getMemoDate());
                    String desc = StringUtils.EMPTY;
			        if(fetchedMemo.getMemoDetails().getMemoDescription()!=null){
			            desc = fetchedMemo.getMemoDesc().replaceAll(fetchedMemo.getMemoDetails().getMemoDescription(), StringUtils.EMPTY);
			            fetchedMemo.setMemoDesc(desc);
			        }
			        bpmRepository.updateMemoDetails(fetchedMemo, Constants.UPDATE);
			    }
			}
			LOGGER.info("Update from BPM -> Going to update count and status");
			memoCount = memoCount + getOpenMemoCount(existingMemoList);
			updateEappMemoCountAndStatus(application.getAppStatus(), application.getMemoList(), memoCount);
		} catch (Exception e) {
			statusFlag = false;
			LOGGER.error("Unable to update memo :" + e.getMessage());
		} finally {
			// Response Builder
			JSONObject jsonObject = new JSONObject();
			JSONObject responseJson = new JSONObject();
			JSONObject msgInfo = new JSONObject();
			if (statusFlag) {
				msgInfo.put(GeneraliConstants.MSG_DESCRIPTION, GeneraliConstants.RECORD_SAVED_SUCCESSFULLY);
				msgInfo.put(GeneraliConstants.MSG_CODE, GeneraliConstants.SUCCESS_CODE);
				msgInfo.put(GeneraliConstants.MSG, GeneraliConstants.SUCCESS);
			} else {
				msgInfo.put(GeneraliConstants.MSG_DESCRIPTION, GeneraliConstants.RECORD_SAVE_FAILED);
				msgInfo.put(GeneraliConstants.MSG_CODE, GeneraliConstants.FAILURE_CODE);
				msgInfo.put(GeneraliConstants.MSG, GeneraliConstants.FAILURE);
			}
			responseJson.put(GeneraliConstants.MSGINFO, msgInfo);
			jsonObject.put(GeneraliConstants.RESPONSE, responseJson);
			response = jsonObject.toString();
		}
		return response;
	}
	
	
	private int getOpenMemoCount(List<AppianMemoDetails> existingMemoList) {
		int count = 0;
		 for(AppianMemoDetails fetchedMemo :existingMemoList){
			 if(fetchedMemo.getMemoStatus().equals(GeneraliConstants.OPEN)){
				 count++;
			 }
		 }
		return count;
	}

	@Async
	@Transactional(propagation = Propagation.REQUIRES_NEW,rollbackFor = { Exception.class })
    private void updateEappMemoCountAndStatus(String appStatus, List<AppianMemoDetails> appianMemoList, int memoCount){
	    try{
	        LOGGER.info("updateEappMemoCountAndStatus --> Start");
	        String appNumber = appianMemoList.get(0).getAppNumber();
	        InsuranceAgreement insAgrement = new InsuranceAgreement();
	        TransactionKeys txnKeys = new TransactionKeys();
	        txnKeys.setKey21(appNumber);
	        txnKeys.setKey30(Integer.toString(memoCount));
	        txnKeys.setKey7(appStatus.equals("Pending Collection Memo")?"Memo":appStatus);
	        AgreementExtension agreementExtn = new AgreementExtension();
	        agreementExtn.setBpmStatus(appStatus);
    	    insAgrement.setTransactionKeys(txnKeys);
    	    insAgrement.setAgreementExtension(agreementExtn);
    	    LOGGER.info("updateEappMemoCountAndStatus --> memo count and status - >Start");
    	    eAppRepo.updateEappMemoCountAndStatus(insAgrement,appianMemoList);
    	    LOGGER.info("updateEappMemoCountAndStatus --> memo count and status - >End");
    	    LOGGER.info("updateEappMemoCountAndStatus --> LAS status - >Start");
    	    bpmRepository.updateLifeAsisRqStatus(appNumber,appStatus);
    	    LOGGER.info("updateEappMemoCountAndStatus --> LAS status - >End");
    	    LOGGER.info("AppNumber --> "+appNumber);
    	    LOGGER.info("MemoCount --> "+memoCount);
    	    LOGGER.info("Status --> "+appStatus);
    	    LOGGER.info("updateEappMemoCountAndStatus --> End");
    	    
	    } catch (BusinessException e) {
	        LOGGER.error("Unable to update eApp memo count: " + e.getMessage());
        } catch(DaoException e){
            LOGGER.error("Unable to update eApp memo count: " + e.getMessage());
        } catch(RuntimeException e){
            LOGGER.error("Unable to update eApp memo count: " + e.getMessage());
        }
	    
	}
	
	@Async
	@Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
	public void triggerEappMemoEmail(String appNo){
	    try{
	        Transactions userTransactions = new Transactions();
	        userTransactions.setKey21(appNo);
            Response<List<AppianMemoDetails>> responseMemo = bpmRepository.retrieveMemoDetails(userTransactions,GeneraliConstants.UPDATE);
            if(responseMemo.getType()!= null && !responseMemo.getType().isEmpty()){
                InsuranceAgreement insAgrement = new InsuranceAgreement();
                TransactionKeys txnKeys = new TransactionKeys(); 
                txnKeys.setKey21(appNo);
                insAgrement.setTransactionKeys(txnKeys);
                GeneraliTransactions txn = eAppRepo.retrieveEappTxnByAppNo(insAgrement);
                txn.setAppianMemoDetails(responseMemo.getType());
                JSONObject eAppJsonObj = getEmailJson(txn);
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(eAppJsonObj);
                generaliEmailComponent.updateEmailDetails(new RequestInfo(), jsonArray);
                generaliEmailComponent.triggerEmail(txn.getAgentId(), GeneraliConstants.MEMO);
            }
        } catch (BusinessException e) {
            LOGGER.error("Unable to trigger memo email: " + e.getMessage());
        } catch(DaoException e){
            LOGGER.error("Unable to trigger memo email: " + e.getMessage());
        } catch(RuntimeException e){
            LOGGER.error("Unable to trigger memo email: " + e.getMessage());
        } catch(ParseException e){
            LOGGER.error("Unable to trigger memo email: " + e.getMessage());
        }
       // Map<String,UnderWriterMemoDetails> memoUW = bpmRepository.getUnderWriterMemoDetails(memoCodeSet);
        
	}
	
	private JSONObject getEmailJson(GeneraliTransactions generaliTxn) throws ParseException{
	    String eAppJSON = StringUtils.EMPTY;
	    final List<GeneraliTransactions> transactionsList = new ArrayList<GeneraliTransactions>();
        GeneraliTransactions txn = new GeneraliTransactions();
        //txn = eAppRepo.retrieveEAppWithMemo(generaliTxn.getKey4(), generaliTxn.getTransTrackingId());
        //generaliTxn.setAppianMemoDetails(appianMemoList);
        generaliTxn.getProposal().getTransactionKeys().setKey18(Constants.TRUE);
	    transactionsList.add(generaliTxn);
        eAppJSON = retrieveEAppHolder.parseBO(transactionsList);
        eAppJSON = eAppJSON.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
        JSONObject jsonObj = new JSONObject();
        Person insuredPerson = (Person)generaliTxn.getInsured().getPlayerParty();
        List<PersonName> personNameList = new ArrayList<PersonName>();
        personNameList.addAll(insuredPerson.getName());
        jsonObj.put(LEAD_NAME, personNameList.get(0).getFullName());
        List<ContactPreference> contactPreferenceList = new ArrayList<ContactPreference>();
        contactPreferenceList.addAll(insuredPerson.getPreferredContact());
        for(ContactPreference contactPref : contactPreferenceList){
            if(contactPref.getPreferredContactPoint().getClass().getSimpleName().equals(ElectronicContact.class.getSimpleName())){
                jsonObj.put(TO_MAIL_ID, ((ElectronicContact)contactPref.getPreferredContactPoint()).getEmailAddress());
            }
        }
        Person agentPerson = (Person)generaliTxn.getAgreementProducer().getPlayerParty();
        contactPreferenceList.clear();
        contactPreferenceList.addAll(agentPerson.getPreferredContact());
        for(ContactPreference contactPref : contactPreferenceList){
            if(contactPref.getPreferredContactPoint().getClass().getSimpleName().equals(ElectronicContact.class.getSimpleName())){
                jsonObj.put(CC_MAIL_ID, ((ElectronicContact)contactPref.getPreferredContactPoint()).getEmailAddress());
            }
        }
        jsonObj.put(GeneraliConstants.LANGUAGE,"th");
        memoEmailSubject = memoEmailSubject.replace("[Application number]", generaliTxn.getProposal().getTransactionKeys().getKey21());
        jsonObj.put(MAIL_SUBJECT,memoEmailSubject);
        JSONObject eappObj = new JSONObject(eAppJSON);
        JSONObject  jsonObject = new JSONObject();
        jsonObject = eappObj.getJSONArray(GeneraliConstants.TRANSACTIONS).getJSONObject(0);
        jsonObject.put(Constants.TYPE, GeneraliConstants.MEMO);
        jsonObject.getJSONObject(Constants.TRANSACTION_DATA).put(GeneraliConstants.EMAIL, jsonObj);
        //jsonObject.put("Email", jsonObj);
        
	    return jsonObject;
	}
	
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public String updateAppainStatus(String appNo, String requestJson) throws InputValidationException {
		String responseString = StringUtils.EMPTY;
		Boolean statusFlag = true;
		try {
			// Retrieving LifeAsiaRequest table data with SpajNO
			LifeAsiaSearchCriteria lifeAsiaSearchCriteria = new LifeAsiaSearchCriteria();
			lifeAsiaSearchCriteria.setSpajNo(appNo);
			Request<LifeAsiaSearchCriteria> request = new JPARequestImpl<LifeAsiaSearchCriteria>(entityManager,
					contextId, "");
			request.setType(lifeAsiaSearchCriteria);
			Response<LifeAsiaRequest> response = lifeAsiaComponent.getLifeAsiaRequestByCriteria(request);
			LifeAsiaRequest lifeAsiaRequest = response.getType();
			lifeAsiaRequest.setSpajNo(appNo);
			lifeAsiaRequest.setResponseData(requestJson);
			lifeAsiaRequest.setStatus(GeneraliConstants.ERROR);
			Date responseTime = new Date();
			lifeAsiaRequest.setResponseTime(responseTime);

			bpmRepository.updateAppainStatus(lifeAsiaRequest);
			statusFlag = true;
		} catch (Exception e) {
			statusFlag = false;
			LOGGER.error("Unable to update BPM Response :" + e.getMessage());
		} finally {
			// Response Builder
			JSONObject jsonObject = new JSONObject();
			JSONObject responseJson = new JSONObject();
			JSONObject msgInfo = new JSONObject();
			if (statusFlag) {
				msgInfo.put(GeneraliConstants.MSG_DESCRIPTION, GeneraliConstants.RECORD_SAVED_SUCCESSFULLY);
				msgInfo.put(GeneraliConstants.MSG_CODE, GeneraliConstants.SUCCESS_CODE);
				msgInfo.put(GeneraliConstants.MSG, GeneraliConstants.SUCCESS);
			} else {
				msgInfo.put(GeneraliConstants.MSG_DESCRIPTION, GeneraliConstants.RECORD_SAVE_FAILED);
				msgInfo.put(GeneraliConstants.MSG_CODE, GeneraliConstants.FAILURE_CODE);
				msgInfo.put(GeneraliConstants.MSG, GeneraliConstants.FAILURE);
			}
			responseJson.put(GeneraliConstants.MSGINFO, msgInfo);
			jsonObject.put(GeneraliConstants.RESPONSE, responseJson);
			responseString = jsonObject.toString();
		}
		return responseString;
	}

    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String updateApplicationStatus(MemoApplicationData memoAppData) {
        String responseString = StringUtils.EMPTY;
        boolean successFlag = false;
        LOGGER.info("updateApplicationStatus Entry");
        try{
            InsuranceAgreement insAgrement = new InsuranceAgreement();
            TransactionKeys txnKeys = new TransactionKeys();
            txnKeys.setKey21(memoAppData.getAppNumber());
            //txnKeys.setKey30("0");
            txnKeys.setKey7(memoAppData.getAppStatus().equals("Pending Collection Memo")?"Memo":memoAppData.getAppStatus());
            AgreementExtension agreementExtn = new AgreementExtension();
            agreementExtn.setBpmStatus(memoAppData.getAppStatus());
            insAgrement.setTransactionKeys(txnKeys);
            insAgrement.setAgreementExtension(agreementExtn);

			Transactions userTransactions = new Transactions();
			userTransactions.setKey21(memoAppData.getAppNumber());
			
			Response<List<AppianMemoDetails>> responseMemo = bpmRepository.retrieveMemoDetails(userTransactions,GeneraliConstants.UPDATE);
			List<AppianMemoDetails> existingMemoList = responseMemo.getType();
			if(existingMemoList == null){
			    existingMemoList = new ArrayList<AppianMemoDetails>(); 
			}
			txnKeys.setKey30(Integer.toString(getOpenMemoCount(existingMemoList)));
			
            List<AppianMemoDetails> appianMemoList = Collections.emptyList(); 
            eAppRepo.updateEappMemoCountAndStatus(insAgrement,appianMemoList);
            bpmRepository.updateLifeAsisRqStatus(memoAppData.getAppNumber(),memoAppData.getAppStatus()); 
            successFlag = true;
            LOGGER.info("AppNumber Entry"+memoAppData.getAppNumber());
            LOGGER.info("Status Entry"+memoAppData.getAppStatus());
        }catch(DaoException e){
            successFlag =false;
            LOGGER.error("Unable to update application status :" + e.getMessage());
        }catch(IllegalStateException e){
            successFlag =false;
            LOGGER.error("Unable to update application status :" + e.getMessage());
        }catch(BusinessException e){
            successFlag =false;
            LOGGER.error("Unable to update application status :" + e.getMessage());
        }finally{
         // Response Builder
            JSONObject jsonObject = new JSONObject();
            JSONObject responseJson = new JSONObject();
            JSONObject msgInfo = new JSONObject();
            if (successFlag) {
                msgInfo.put(GeneraliConstants.MSG_DESCRIPTION, GeneraliConstants.RECORD_SAVED_SUCCESSFULLY);
                msgInfo.put(GeneraliConstants.MSG_CODE, GeneraliConstants.SUCCESS_CODE);
                msgInfo.put(GeneraliConstants.MSG, GeneraliConstants.SUCCESS);
            } else {
                msgInfo.put(GeneraliConstants.MSG_DESCRIPTION, GeneraliConstants.RECORD_SAVE_FAILED);
                msgInfo.put(GeneraliConstants.MSG_CODE, GeneraliConstants.FAILURE_CODE);
                msgInfo.put(GeneraliConstants.MSG, GeneraliConstants.FAILURE);
            }
            responseJson.put(GeneraliConstants.MSGINFO, msgInfo);
            jsonObject.put(GeneraliConstants.RESPONSE, responseJson);
            responseString = jsonObject.toString();
        }
        LOGGER.info("updateApplicationStatus Exit");
        return responseString;
        
    }

}
