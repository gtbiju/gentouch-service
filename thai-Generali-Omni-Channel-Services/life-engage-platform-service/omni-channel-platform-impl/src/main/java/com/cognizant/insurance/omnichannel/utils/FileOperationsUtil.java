package com.cognizant.insurance.omnichannel.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Class contains different file operations like 
 * creating and reading files.
 *
 */
@Component("fileOperationsUtil")
public class FileOperationsUtil {
	
		@Value("${le.platform.service.fileUploadPath}")
	    private String fileUploadPath;
		
	    public static final Logger LOGGER = LoggerFactory.getLogger(FileOperationsUtil.class);

	
	public void createFileFromByteArray(final String proposalNumber, final String fileName, final byte[] fileByteArray)
            throws IOException {
		try{
		final File tempDirectory = new File(fileUploadPath + proposalNumber);
		// Directory does'nt exists - create Directory and the files
		if (tempDirectory.exists()) {
			// Directory exists - create the file alone
			createFile(fileName, fileByteArray, tempDirectory);
			// ToDo - add document path in agreementdoc
		} else {
			// Create the parent directory and the sub directory.
			if (tempDirectory.mkdirs()) {
				// Create the file
				createFile(fileName, fileByteArray, tempDirectory);
			} else {
				LOGGER.error("Failed to create directory!");
    }
		}
		
		}catch(Exception e){
			LOGGER.error("Error createFileFromByteArray -- "+e.getMessage());
		}
}
	
	
	
	/**
	 * Method used to create a file
	 */
	public void createFile(final String fileName,final byte[] fileByteArray, final File tempDirectory)
			throws IOException {
		final File someFile = new File(tempDirectory, fileName);
		final FileOutputStream fos = new FileOutputStream(someFile);
		fos.write(fileByteArray);
		fos.flush();
		fos.close();
	}
	
	 /**
	 * Used to read a file from a location
	 */
	public  String getFile(final String fileName, final String proposalNumber,Boolean isImage) {
		 File file = null;
		byte[] bytes;
		byte[] encoded = null;
		String result="";
	
        try {
        	if(isImage){
  			  file = new File(fileUploadPath+ proposalNumber + "/" +"REQUIREMENTS"+"/"+ fileName);
  		}else{
  			  file = new File(fileUploadPath+ proposalNumber + "/" + fileName);
  		}
			 InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			 bytes = IOUtils.toByteArray(inputStream);
			 encoded = Base64.encodeBase64(bytes);
			 result=new String(encoded);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e) {
			LOGGER.error("Error in getting signature in proposalNumber "+proposalNumber,e.getMessage());
		}
		
		return result;
	 }

}
