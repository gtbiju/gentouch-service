package com.cognizant.insurance.omnichannel.utils;

/**
 * @author 471501
 * 
 *         The Class RiderBenefitIllustrationTable.
 * 
 *         This class is a POJO for Benefit illustration for rider table in
 *         illustration PDF.
 */

public class RiderBenefitIllustrationTable {

	/** riderName1. */
	private String firstRiderName;

	/** secondRiderName. */
	private String secondRiderName;

	/** thirdRiderName. */
	private String thirdRiderName;

	/** fourthRiderName. */
	private String fourthRiderName;

	/** The firstSumAssured. */
	private String firstSumAssured;

	/** The secondSumAssured. */
	private String secondSumAssured;

	/** The thirdSumAssured. */
	private String thirdSumAssured;

	/** The fourthSumAssured. */
	private String fourthSumAssured;

	/** The firstDescription. */
	private String firstDescription;
	
	/** The firstDescription. */
	private String secondDescription;
	
	/** The firstDescription. */
	private String thirdDescription;
	
	/** The firstDescription. */
	private String fourthDescription;

	/**
	 * @return the firstRiderName
	 */
	public String getFirstRiderName() {
		return firstRiderName;
	}

	/**
	 * @param firstRiderName the firstRiderName to set
	 */
	public void setFirstRiderName(String firstRiderName) {
		this.firstRiderName = firstRiderName;
	}

	/**
	 * @return the secondRiderName
	 */
	public String getSecondRiderName() {
		return secondRiderName;
	}

	/**
	 * @param secondRiderName the secondRiderName to set
	 */
	public void setSecondRiderName(String secondRiderName) {
		this.secondRiderName = secondRiderName;
	}

	/**
	 * @return the thirdRiderName
	 */
	public String getThirdRiderName() {
		return thirdRiderName;
	}

	/**
	 * @param thirdRiderName the thirdRiderName to set
	 */
	public void setThirdRiderName(String thirdRiderName) {
		this.thirdRiderName = thirdRiderName;
	}

	/**
	 * @return the fourthRiderName
	 */
	public String getFourthRiderName() {
		return fourthRiderName;
	}

	/**
	 * @param fourthRiderName the fourthRiderName to set
	 */
	public void setFourthRiderName(String fourthRiderName) {
		this.fourthRiderName = fourthRiderName;
	}

	/**
	 * @return the firstSumAssured
	 */
	public String getFirstSumAssured() {
		return firstSumAssured;
	}

	/**
	 * @param firstSumAssured the firstSumAssured to set
	 */
	public void setFirstSumAssured(String firstSumAssured) {
		this.firstSumAssured = firstSumAssured;
	}

	/**
	 * @return the secondSumAssured
	 */
	public String getSecondSumAssured() {
		return secondSumAssured;
	}

	/**
	 * @param secondSumAssured the secondSumAssured to set
	 */
	public void setSecondSumAssured(String secondSumAssured) {
		this.secondSumAssured = secondSumAssured;
	}

	/**
	 * @return the thirdSumAssured
	 */
	public String getThirdSumAssured() {
		return thirdSumAssured;
	}

	/**
	 * @param thirdSumAssured the thirdSumAssured to set
	 */
	public void setThirdSumAssured(String thirdSumAssured) {
		this.thirdSumAssured = thirdSumAssured;
	}

	/**
	 * @return the fourthSumAssured
	 */
	public String getFourthSumAssured() {
		return fourthSumAssured;
	}

	/**
	 * @param fourthSumAssured the fourthSumAssured to set
	 */
	public void setFourthSumAssured(String fourthSumAssured) {
		this.fourthSumAssured = fourthSumAssured;
	}

	/**
	 * @return the firstDescription
	 */
	public String getFirstDescription() {
		return firstDescription;
	}

	/**
	 * @param firstDescription the firstDescription to set
	 */
	public void setFirstDescription(String firstDescription) {
		this.firstDescription = firstDescription;
	}

	/**
	 * @return the secondDescription
	 */
	public String getSecondDescription() {
		return secondDescription;
	}

	/**
	 * @param secondDescription the secondDescription to set
	 */
	public void setSecondDescription(String secondDescription) {
		this.secondDescription = secondDescription;
	}

	/**
	 * @return the thirdDescription
	 */
	public String getThirdDescription() {
		return thirdDescription;
	}

	/**
	 * @param thirdDescription the thirdDescription to set
	 */
	public void setThirdDescription(String thirdDescription) {
		this.thirdDescription = thirdDescription;
	}

	/**
	 * @return the fourthDescription
	 */
	public String getFourthDescription() {
		return fourthDescription;
	}

	/**
	 * @param fourthDescription the fourthDescription to set
	 */
	public void setFourthDescription(String fourthDescription) {
		this.fourthDescription = fourthDescription;
	}

}
