package com.cognizant.insurance.omnichannel.utils;

/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Oct 7, 2015
 */
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.omnichannel.component.repository.helper.GeneraliComponentRepositoryHelper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

/**
 * The Class class FNAScriptlet.
 */
public class FNAScriptlet extends JRDefaultScriptlet {

	/** The goal and need. */
	private GoalAndNeed goalAndNeed;

	/** The request json object. */
	private JSONObject requestJSONObject;

	/** The myself json object. */
	private JSONObject myselfJSONObject;

	/** The myself json object1. */
	private JSONObject myselfJSONObject1;

	/** The json party obj. */
	private JSONObject jsonPartyObj;

	/** The json party obj. */
	private JSONObject jsonGoalObj;

	/** The agent. */
	private GeneraliAgent generaliAgent;

	/** The Constant FNA_MY_SELF. */
	private static final String FNA_MY_SELF = "FNAMyself";

	/** The Constant BASICDETAILS. */
	private static final String BASICDETAILS = "BasicDetails";

	/** The Constant LIFESTAGE. */
	private static final String LIFESTAGE = "lifeStage";

	/** The Constant CONTACTDETAILS. */
	private static final String CONTACTDETAILS = "ContactDetails";

	/** The Constant RISKPROFILE. */
	private static final String RISKPROFILE = "riskProfile";

	/** The Constant AGENT. */
	private static final String AGENT = "Agent";

	/** The Constant PARAMETERS. */
	private static final String PARAMETERS = "parameters";

	private static final String RESULT = "result";

	private static final String YEARS = "ปี";

	private static final String EMPTY = "";

	private static final String goalEducation = "Education";

	private static final String goalRtd = "Retirement";

	private static final String goalProtection = "Protection";

	private static final String PM = "บาท";

	private static final String goalHEALTH = "Health";

	private static final String goalSAVINGS = "Savings";

	private Multimap<String, Integer> goalsPriorityMap = ArrayListMultimap.create();

	private List<FamilyDetailsData> familyData = new ArrayList<FamilyDetailsData>();

	private TreeMap<Integer, JSONObject> educationdetails = new TreeMap<Integer, JSONObject>();

	/** The goals array. */
	JSONArray goalsArray;

	private String pdfReqInput;

	private org.json.simple.JSONObject jsonObjectPdfInput;

	private org.json.simple.JSONObject jsonObjectResourceIN;

	private Map<String, Map<String, String>> codeValuesMap;

	private Map<String, String> goalsMap;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FNAScriptlet.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see net.sf.jasperreports.engine.JRDefaultScriptlet#beforeDetailEval()
	 */
	@Override
	public void beforeDetailEval() throws JRScriptletException {

		goalAndNeed = (GoalAndNeed) this.getFieldValue("goalAndNeed");

		generaliAgent = (GeneraliAgent) this.getFieldValue("generaligent");

		String requestJSON = goalAndNeed.getRequestJSON();

		try {

			requestJSONObject = new JSONObject(requestJSON);

			JSONArray partiesArray = requestJSONObject.getJSONArray("parties");
			goalsArray = requestJSONObject.getJSONArray("goals");
			if (partiesArray != null) {
				for (int i = 0; i < partiesArray.length(); i++) {
					jsonPartyObj = partiesArray.getJSONObject(i);
					if (jsonPartyObj.has("type") && FNA_MY_SELF.equals(jsonPartyObj.getString("type"))) {
						myselfJSONObject = jsonPartyObj;
					}

				}
			}

			goalsMap = new HashMap<String, String>();
			goalsMap.put("Health", "สุขภาพ ");
			goalsMap.put("Retirement", "วัยเกษียณ ");
			goalsMap.put("Protection", "ความคุ้มครอง ");
			goalsMap.put("Savings", "การออม ");
			goalsMap.put("Education", "การศึกษาบุตรของ ");

			if (goalsArray != null) {
				for (int i = 0; i < goalsArray.length(); i++) {
					jsonGoalObj = goalsArray.getJSONObject(i);
					// if (jsonGoalObj.has("type") &&
					// FNA_MY_SELF.equals(jsonGoalObj.getString("type"))) {
					myselfJSONObject1 = jsonGoalObj;
					// }
				}
			}

			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");
			if (selectedGoalsArray != null) {
				Integer priority = 0;
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonGoalObj = selectedGoalsArray.getJSONObject(i);
					if (jsonGoalObj.has("priority")) {
						priority = jsonGoalObj.getInt("priority");
					}
					String goalName = checkForValue(jsonGoalObj, "goalName");
					if (!EMPTY.equals(priority) && !EMPTY.equals(goalName)) {
						goalsPriorityMap.put(goalName, priority);
					}

					// Logic to handle multiple Education goals.

					if (goalEducation.equals(goalName)) {

						educationdetails.put(priority, jsonGoalObj);

					}

				}

			}

			if (partiesArray != null) {
				for (int i = 0; i < partiesArray.length(); i++) {
					jsonPartyObj = partiesArray.getJSONObject(i);
					if (!jsonPartyObj.has("type")) {
						FamilyDetailsData familyDetails = new FamilyDetailsData();
						if (jsonPartyObj.has("BasicDetails")) {
							JSONObject value1 = (JSONObject) jsonPartyObj.get("BasicDetails");

							familyDetails.setName(
									value1.get("firstName").toString() + " " + value1.get("lastName").toString());
							if (!("").equals(value1.get("age").toString())) {
								familyDetails.setAge((value1.get("age").toString()) + "  " + YEARS);
							} else {
								familyDetails.setAge(value1.get("age").toString());
							}
							familyDetails.setTitle(value1.has("title") ? value1.get("title").toString() : "");

							familyDetails.setGender(value1.has("gender") ? value1.get("gender").toString() : "");
						}
						if (jsonPartyObj.has("classNameAttached")) {
							familyDetails.setRelationship(jsonPartyObj.get("classNameAttached").toString());
						} else {
							familyDetails.setRelationship(" ");
						}

						familyData.add(familyDetails);
					}

				}
			}

			/*
			 * JSONParser jsonParser = new JSONParser(); try { if (pdfReqInput
			 * == null) pdfReqInput = GeneraliPDFContentReader
			 * .getPDFContent("pdfTemplates/FNAResourceJson.json"); } catch
			 * (Exception e1) { // TODO Auto-generated catch block
			 * e1.printStackTrace(); }
			 * 
			 * 
			 * Object obj1 = jsonParser.parse(pdfReqInput); jsonObjectPdfInput =
			 * (org.json.simple.JSONObject) obj1; jsonObjectResourceIN
			 * =(org.json.simple.JSONObject) jsonObjectPdfInput.get("in");
			 * 
			 */
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} /*
			 * catch (org.json.simple.parser.ParseException e) { // TODO
			 * Auto-generated catch block e.printStackTrace(); }
			 */

	}

	/**
	 * Gets the myself name.
	 *
	 * @return the myself name
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getMyselfName() throws JRScriptletException {

		return ((JSONObject) myselfJSONObject.get(BASICDETAILS)) != null
				? ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("firstName").toString() + " "
						+ ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("lastName")
				: "";

	}

	public String getMyselfNameTitle() throws JRScriptletException {
		String name = "";
		if (myselfJSONObject.get(BASICDETAILS) != null) {
			if (("Male").equals(((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("gender").toString())) {
				name = "นาย   " + ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("firstName").toString() + " "
						+ ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("lastName").toString();
			} else if (("Female").equals(((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("gender").toString())) {
				name = "นาง    " + ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("firstName").toString() + " "
						+ ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("lastName").toString();
			}
		}
		return name;

	}

	public String getChildName() throws JRScriptletException {
		String educationChildName = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("educationChildNameSlider")) {
							educationChildName = value1.get("educationChildNameSlider").toString();
						}
					}
				}
			}
		}
		return educationChildName;
	}

	public String getEducationAge(String p) throws JRScriptletException {
		String educationAge = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has("goalName") && goalEducation.equals(jsonGoalObj.get("goalName").toString())
							&& (p).equals(jsonGoalObj.get("priority"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject result = new JSONObject();
							result = jsonGoalObj.getJSONObject(RESULT);
							educationAge = checkForValue(result, ("educationChildAgeSlider"));

							if (!EMPTY.equals(educationAge)) {
								educationAge = educationAge + "  " + YEARS;
							}

						}
					}
				}
			}

		}
		return educationAge;
	}

	public String getEducationTargetAge(String p) throws JRScriptletException {
		String educationTargetAge = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has("goalName") && goalEducation.equals(jsonGoalObj.get("goalName").toString())
							&& (p).equals(jsonGoalObj.get("priority"))) {
						if (jsonGoalObj.has(RESULT)) {

							JSONObject result = new JSONObject();
							result = jsonGoalObj.getJSONObject(RESULT);
							educationTargetAge = checkForValue(result, ("educationEntryAge1Slider"));

							if (!EMPTY.equals(educationTargetAge)) {
								educationTargetAge = educationTargetAge + "  " + YEARS;
							}
						}
					}
				}
			}

		}
		return educationTargetAge;
	}

	public String getTermToEducation(String p) throws JRScriptletException, ParseException {
		String termToEducation = "";
		/*
		 * String year = ""; Date educationYear =new Date();
		 */
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has("goalName") && goalEducation.equals(jsonGoalObj.get("goalName").toString())
							&& (p).equals(jsonGoalObj.get("priority"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject result = new JSONObject();
							result = jsonGoalObj.getJSONObject(RESULT);
							termToEducation = checkForValue(result, ("termToEducation"));
							/*
							 * year = checkForValue(result, ("myselfDob"));
							 */

							if (!EMPTY.equals(termToEducation)) {
								termToEducation = termToEducation + "  " + YEARS;

							}
							/*
							 * if (!EMPTY.equals(year)) { SimpleDateFormat
							 * format = new SimpleDateFormat("yyyy-MM-dd");
							 * educationYear= format.parse(year); Date
							 * convertedDate = convertDateToThai(educationYear);
							 * Calendar calendar = Calendar.getInstance();
							 * calendar.setTime(convertedDate); int value=
							 * (calendar.get(calendar.YEAR))+Integer.parseInt(
							 * termToEducation);
							 * yearValue=String.valueOf(value); }
							 */
						}
					}
				}
			}
		}

		return termToEducation;
	}

	public String getTotalCost(String p) throws JRScriptletException {
		String totalCost = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has("goalName") && goalEducation.equals(jsonGoalObj.get("goalName").toString())
							&& (p).equals(jsonGoalObj.get("priority"))) {
						if (jsonGoalObj.has(RESULT)) {

							JSONObject result = new JSONObject();
							result = jsonGoalObj.getJSONObject(RESULT);
							totalCost = formatter(checkForValue(result, ("totalFutureCost")));

						}
					}
				}
			}
		}
		return totalCost;
	}

	public String getAvailableSavings(String p) throws JRScriptletException {
		String availableSavings = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has("goalName") && goalEducation.equals(jsonGoalObj.get("goalName").toString())
							&& (p).equals(jsonGoalObj.get("priority"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject result = new JSONObject();
							result = jsonGoalObj.getJSONObject(RESULT);
							availableSavings = formatter(checkForValue(result, ("availableSavings")));

						}
					}
				}
			}
		}
		return availableSavings;
	}

	public Boolean getFamilyDetailsFlag() throws JRScriptletException {
		JSONArray partiesArray = requestJSONObject.getJSONArray("parties");
		Boolean familyFlag = false;
		if (partiesArray.length() > 1) {
			familyFlag = true;
		}
		return familyFlag;
	}

	public String getShortFall() throws JRScriptletException {
		String shortFall = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1.has("needGap")) {
							shortFall = formatter(value1.getString("needGap"));
						}
					}
				}
			}
		}
		return shortFall;
	}

	public String getShortFallEdu(String p) throws JRScriptletException {
		String shortFall = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null && ("Education").equals(jsonGoalObj.get("goalName"))
						&& (p).equals(jsonGoalObj.get("priority"))) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);

						shortFall = formatter(checkForValue(value1, ("needGapfnaReport")));

					}
				}
			}
		}
		return shortFall;
	}

	public String getSurplus() throws JRScriptletException {
		String surplus = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(PARAMETERS)) {
						JSONArray value1 = (JSONArray) jsonGoalObj.getJSONArray(PARAMETERS);
						for (int j = 0; j < value1.length(); j++) {
							jsonGoalObj = value1.getJSONObject(j);
							if (("surplus").equals(jsonGoalObj.get("name").toString())) {
								surplus = jsonGoalObj.get("value").toString();
							}
						}
					}
				}
			}
		}
		return surplus;
	}

	public String getMonthlySavingsEducation(String p) throws JRScriptletException {
		String monthlySavings = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && ("Education").equals(jsonGoalObj.get("goalName"))
							&& (p).equals(jsonGoalObj.get("priority"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);

							monthlySavings = formatter(checkForValue(value1, ("monthlySavingsRequired")));
							if (!EMPTY.equals(monthlySavings)) {
								monthlySavings = monthlySavings + "  " + PM;
							}

						}
					}
				}
			}
		}
		return monthlySavings;
	}

	public String getCurrentAge() throws JRScriptletException {
		String currentAge = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalRtd.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null && value1.has("currentAgeSlider")) {
								currentAge = value1.get("currentAgeSlider").toString();
								if (!EMPTY.equals(currentAge))
									currentAge = currentAge + "  " + YEARS;
							}
						}
					}
				}
			}
		}
		return currentAge;
	}

	public String getRetirementAge() throws JRScriptletException {
		String retirementAge = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalRtd.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null && value1.has("retirementPensionAgeSlider")) {
								retirementAge = value1.get("retirementPensionAgeSlider").toString();
								if (!EMPTY.equals(retirementAge)) {
									retirementAge = retirementAge + "  " + YEARS;
								}
							}
						}
					}
				}
			}
		}
		return retirementAge;
	}

	public String getYearsToRetire() throws JRScriptletException {
		String yearsToRetire = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalRtd.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null) {
								yearsToRetire = checkForValue(value1, ("yearsToRetire").toString());
								if (!EMPTY.equals(yearsToRetire)) {
									yearsToRetire = yearsToRetire + "  " + YEARS;

								}
							}
						}
					}
				}
			}
		}
		return yearsToRetire;
	}

	public String getRetirementFund() throws JRScriptletException {
		String retirementFund = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalRtd.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null) {
								retirementFund = formatter(checkForValue(value1, ("totalAmountReqdForRet")));
							}
						}
					}
				}
			}
		}
		return retirementFund;
	}

	public String getRetirementSavings() throws JRScriptletException {
		String availableSavings = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalRtd.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null && value1.has("futureSavings")) {
								availableSavings = formatter(value1.getString("futureSavings"));
							}
						}
					}
				}
			}
		}
		return availableSavings;
	}

	public String getShortFallRetirement() throws JRScriptletException {
		String shortFallRetirement = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalRtd.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							/*
							 * JSONArray value2 = (JSONArray) value1
							 * .getJSONArray("ruleExecutionOutput"); JSONObject
							 * shortfallValue1 = (JSONObject) (value2.get(0));
							 */
							if (value1.has("futureNeedGapfnaReport")) {
								shortFallRetirement = formatter(value1.getString("futureNeedGapfnaReport"));
							}
						}
					}
				}
			}
		}
		return shortFallRetirement;
	}

	public String getMonthlySavingsRetirement() throws JRScriptletException {
		String monthlySavings = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalRtd.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);

							monthlySavings = formatter(checkForValue(value1, ("monthlySavingsRequired")));
							if (!EMPTY.equals(monthlySavings)) {
								monthlySavings = monthlySavings + "  " + PM;
							}

						}
					}
				}
			}
		}
		return monthlySavings;
	}

	public String getSurplusRetirement() throws JRScriptletException {
		String showSurplus = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalRtd.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null && value1.has("futureNeedGapfnaReport")
									&& value1.get("futureNeedGapfnaReport") != null) {
								showSurplus = formatter(value1.get("futureNeedGapfnaReport").toString());
							}
						}
					}
				}
			}
		}
		return showSurplus;
	}

	public String getTermOfMonthlyExpenses() throws JRScriptletException {
		String termOfMonthlyExpenses = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("protectionDependencyPeriodSlider")) {
							if (value1.has("protectionDependencyPeriodSlider")
									&& !(("").equals(value1.get("protectionDependencyPeriodSlider").toString()))) {
								termOfMonthlyExpenses = value1.get("protectionDependencyPeriodSlider").toString() + "  "
										+ YEARS;
							} else {
								termOfMonthlyExpenses = value1.get("protectionDependencyPeriodSlider").toString();
							}

						}
					}
				}
			}
		}
		return termOfMonthlyExpenses;
	}

	public String getCurrentMontlyExpenses() throws JRScriptletException {
		String currentMontlyExpenses = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("protectionCurrentMonthlyExpensesSlider")) {
							currentMontlyExpenses = formatter(
									value1.get("protectionCurrentMonthlyExpensesSlider").toString());
						}
					}
				}
			}
		}
		return currentMontlyExpenses;
	}

	public String getTotalProtectionCover() throws JRScriptletException {
		String totalProtectionCover = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null && goalProtection.equals(jsonGoalObj.get("goalName"))) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("protectionRequired")) {
							totalProtectionCover = formatter(value1.get("protectionRequired").toString());
						}
					}
				}
			}
		}
		return totalProtectionCover;
	}

	public String getAvailableFund() throws JRScriptletException {
		String availableFund = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null && goalProtection.equals(jsonGoalObj.get("goalName"))) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("totalSavingsValueUnit")) {
							availableFund = formatter(value1.get("totalSavingsValueUnit").toString());
						}
					}
				}
			}
		}
		return availableFund;
	}

	public String getSurplusOrShortfall() throws JRScriptletException {
		String surplusOrShortfall = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null && goalProtection.equals(jsonGoalObj.get("goalName"))) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						/*
						 * JSONArray value2 = (JSONArray)
						 * value1.getJSONArray("ruleExecutionOutput");
						 * JSONObject shortfallValue1=(JSONObject)
						 * (value2.get(0));
						 */
						if (value1.has("needGapfnaReport")) {
							surplusOrShortfall = formatter(value1.getString("needGapfnaReport"));
						}
					}
				}
			}
		}
		return surplusOrShortfall;
	}

	public String getShowSurplus() throws JRScriptletException {
		String showSurplus = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null && goalProtection.equals(jsonGoalObj.get("goalName"))) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("surplus")) {
							showSurplus = formatter(value1.get("surplus").toString());
						}
					}
				}
			}
		}
		return showSurplus;
	}

	public String getShowSurplusEdu(String p) throws JRScriptletException {
		String showSurplus = "";
		String surplus = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has("goalName") && goalEducation.equals(jsonGoalObj.get("goalName").toString())
							&& (p).equals(jsonGoalObj.get("priority"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);

							surplus = formatter(checkForValue(value1, ("needGap").toString()));
							showSurplus = surplus.substring(1);
						}
					}
				}
			}
		}
		return showSurplus;
	}

	public String getAccidentBenefit() throws JRScriptletException {
		String accidentBenefit = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						/*
						 * JSONArray value2 = (JSONArray)
						 * value1.getJSONArray("ruleExecutionOutput");
						 * JSONObject shortfallValue1=(JSONObject)
						 * (value2.get(0));
						 */
						if (value1.has("personalAccidentBenefit")) {
							accidentBenefit = formatter(value1.getString("personalAccidentBenefit"));
						}
					}
				}
			}
		}
		return accidentBenefit;
	}

	public String getChoiceOfRoom() throws JRScriptletException {
		String choiceOfRoom = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						/*
						 * JSONArray value2 = (JSONArray)
						 * value1.getJSONArray("ruleExecutionOutput");
						 * JSONObject shortfallValue1=(JSONObject)
						 * (value2.get(0));
						 */
						if (value1.has("criticalIllness")) {
							choiceOfRoom = formatter(value1.getString("criticalIllness"));
						}
					}
				}
			}
		}
		return choiceOfRoom;
	}

	public String getCriticalIllness() throws JRScriptletException {
		String criticalIllness = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						/*
						 * JSONArray value2 = (JSONArray)
						 * value1.getJSONArray("ruleExecutionOutput");
						 * JSONObject shortfallValue1=(JSONObject)
						 * (value2.get(0));
						 */
						if (value1.has("suggestedCoverAmount")) {
							criticalIllness = formatter(value1.getString("suggestedCoverAmount"));
						}
					}
				}
			}
		}
		return criticalIllness;
	}

	public String getWhatIsYourPlan() throws JRScriptletException {
		String whatIsYourPlan = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("investmentPurposeSlider")) {
							whatIsYourPlan = value1.get("investmentPurposeSlider").toString();
						}
					}
				}
			}
		}
		return whatIsYourPlan;
	}

	public String getHealthCurrentAge() throws JRScriptletException {
		String currentAge = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("currentAgeSlider")) {
							currentAge = value1.get("currentAgeSlider").toString();
						}
					}
				}
			}
		}
		return currentAge;
	}

	public String getHowMuchItWillCost() throws JRScriptletException {
		String howMuchItWillCost = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("investmentCostSlider")) {
							howMuchItWillCost = formatter(value1.get("investmentCostSlider").toString());
						}
					}
				}
			}
		}
		return howMuchItWillCost;
	}

	public String getPlanWouldbeRealize() throws JRScriptletException {
		String planWouldbeRealize = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("investmentRealiseSlider")) {
							planWouldbeRealize = value1.get("investmentRealiseSlider").toString();
						}
					}
				}
			}
		}
		return planWouldbeRealize;
	}

	public String getProjectedCost() throws JRScriptletException {
		String projectedCost = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("amountRequired")) {
							projectedCost = formatter(value1.get("amountRequired").toString());
						}
					}
				}
			}
		}
		return projectedCost;
	}

	public String getAllcoatedForPlan() throws JRScriptletException {
		String allcoatedForPlan = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("projectSavings")) {
							allcoatedForPlan = formatter(value1.get("projectSavings").toString());
						}
					}
				}
			}
		}
		return allcoatedForPlan;
	}

	public String getSavingsSurplusOrShortfall() throws JRScriptletException {
		String surplusOrShortfall = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalSAVINGS.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							/*
							 * JSONArray value2 = (JSONArray) value1
							 * .getJSONArray("ruleExecutionOutput"); JSONObject
							 * shortfallValue1 = (JSONObject) (value2.get(0));
							 */
							if (value1.has("futureNeedGapfnaReport")) {
								surplusOrShortfall = formatter(checkForValue(value1, "futureNeedGapfnaReport"));
							}
						}
					}
				}
			}
		}
		return surplusOrShortfall;
	}

	public String getSavingsMonthlySavings() throws JRScriptletException {
		String surplusOrShortfall = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalSAVINGS.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							/*
							 * JSONArray value2 = (JSONArray) value1
							 * .getJSONArray("ruleExecutionOutput"); JSONObject
							 * shortfallValue1 = (JSONObject) (value2.get(0));
							 */
							if (value1.has("monthlySavingsRequired")) {
								surplusOrShortfall = formatter(checkForValue(value1, "monthlySavingsRequired"));
								if (!EMPTY.equals(surplusOrShortfall)) {
									surplusOrShortfall = surplusOrShortfall + "  " + PM;
								}
							}
						}
					}
				}
			}
		}
		return surplusOrShortfall;
	}

	public String getSavingsShowSurplus() throws JRScriptletException {
		String showSurplus = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalSAVINGS.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null && value1.has("surplus") && value1.get("surplus") != null) {
								showSurplus = formatter(value1.get("surplus").toString());
							}
						}
					}
				}
			}
		}
		return showSurplus;
	}

	/*
	 * public String getWealthTransferAmountOfAsset() throws
	 * JRScriptletException { String wealthTransferAmountOfAsset = ""; JSONArray
	 * goalsArray = requestJSONObject.getJSONArray("selectedGoals"); if
	 * (goalsArray != null) { for (int i = 0; i < goalsArray.length(); i++) {
	 * jsonGoalObj = goalsArray.getJSONObject(i); if (jsonGoalObj != null) { if
	 * (jsonGoalObj.has(RESULT)) { JSONObject value1 = (JSONObject) jsonGoalObj
	 * .get(RESULT); if (value1 != null && value1
	 * .has("wealthTransferAmountOfAssetSlider")) { wealthTransferAmountOfAsset
	 * = formatter(value1.get( "wealthTransferAmountOfAssetSlider")
	 * .toString()); } } } } } return wealthTransferAmountOfAsset; }
	 */

	public String getHealthAnnualRevenue() throws JRScriptletException {
		String healthAnnualRevenue = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("annualRevenueSlider")) {
							healthAnnualRevenue = value1.get("annualRevenueSlider").toString();
							if (!"".equals(healthAnnualRevenue)) {
								healthAnnualRevenue = healthAnnualRevenue + "%";
							}
						}
					}
				}
			}
		}
		return healthAnnualRevenue;
	}

	public String getHealthAnnualExpense() throws JRScriptletException {
		String healthAnnualExpense = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("annualExpensesSlider")) {
							healthAnnualExpense = value1.get("annualExpensesSlider").toString();
						}
					}
				}
			}
		}
		return healthAnnualExpense;
	}

	public String getHealthDeposit() throws JRScriptletException {
		String healthDeposit = "";

		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalHEALTH.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null && value1.has("depositSlider")) {
								healthDeposit = formatter(checkForValue(value1, ("depositSlider")));
							}
						}
					}
				}
			}
		}
		return healthDeposit;
	}

	public String getHealthRequired() throws JRScriptletException {
		String healthRequired = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalHEALTH.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);

							healthRequired = formatter(checkForValue(value1, "healthRequiredSlider"));
						}
					}
				}
			}
		}

		return healthRequired;
	}

	public String getHealthEmployeeBenefit() throws JRScriptletException {
		String employeeBenefit = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalHEALTH.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);

							employeeBenefit = formatter(checkForValue(value1, "employeeBenefitSlider"));
						}
					}
				}
			}
		}

		return employeeBenefit;
	}

	public String getHealthShortfall() throws JRScriptletException {
		String shortfall = "";

		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalHEALTH.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);

							shortfall = formatter(checkForValue(value1, "needGapfnaReport"));

						}
					}
				}
			}
		}
		return shortfall;
	}

	public String getHealthSurplus() throws JRScriptletException {
		String showSurplus = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalHEALTH.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null && value1.has("needGapfnaReport")
									&& value1.get("needGapfnaReport") != null) {
								showSurplus = formatter(value1.get("needGapfnaReport").toString());
							}
						}
					}
				}
			}
		}
		return showSurplus;
	}

	public String getRoomClass() throws JRScriptletException {
		String classType = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (jsonGoalObj != null && goalProtection.equals(jsonGoalObj.get("goalName"))) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							classType = (checkForValue(value1, "protectionChoiceofRoom"));

						}
					}
				}
			}
		}
		return classType;
	}

	public List<FamilyDetailsData> getFamilyDetailsData() throws Throwable {

		JSONArray partiesArray = requestJSONObject.getJSONArray("parties");
		if (partiesArray != null) {
			for (int i = 0; i < partiesArray.length(); i++) {
				jsonPartyObj = partiesArray.getJSONObject(i);
				if (!jsonPartyObj.has("type")) {
					if (jsonPartyObj.has("BasicDetails")) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("BasicDetails");
						FamilyDetailsData familyDetails = new FamilyDetailsData();
						familyDetails
								.setName(value1.get("firstName").toString() + " " + value1.get("lastName").toString());
						if (!("").equals(value1.get("age").toString())) {
							familyDetails.setAge((value1.get("age").toString()) + "  " + YEARS);
						} else {
							familyDetails.setAge(value1.get("age").toString());
						}

						if (jsonPartyObj.has("classNameAttached")) {
							familyDetails.setRelationship(jsonPartyObj.get("classNameAttached").toString());
						} else {
							familyDetails.setRelationship(" ");
						}
						familyData.add(familyDetails);
					}
				}
			}
		}
		return familyData;

	}

	public List<EducationData> getFNADataChartEdu1(String p) throws JRScriptletException {
		List<EducationData> eduData = new ArrayList<EducationData>();
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result")) && (("Education").equals(jsonPartyObj.get("goalName").toString()))
							&& (p).equals(jsonPartyObj.get("priority"))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");

						EducationData FNAEDUData1 = new EducationData();
						if (value1.has("totalFutureCost")) {
							Long fund = checkForKeyValue(value1, "totalFutureCost");
							FNAEDUData1.setTotalCost(fund);
							FNAEDUData1.setCategoryAxisFund(formatter(fund.toString()));
						}
						if (value1.has("FVSavings")) {
							Long savings = checkForKeyValue(value1, "availableSavings");
							FNAEDUData1.setAvailableSavings(savings);
							FNAEDUData1.setCategoryAxisSavings(formatter(savings.toString()));
						}
						if (value1.has("needGapfnaReport")) {
							FNAEDUData1.setShortFall(checkForKeyValue(value1, "needGapfnaReport"));
						}
						eduData.add(FNAEDUData1);
					}
				}

			}
		}
		return eduData;

	}

	public List<RetirementData> getFNADataChartRetirement1() throws JRScriptletException {
		List<RetirementData> retireData = new ArrayList<RetirementData>();
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result"))
							&& (("Retirement").equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");

						RetirementData FNARetireData1 = new RetirementData();
						if (value1.has("totalAmountReqdForRet")) {
							Long rtdFund = checkForKeyValue(value1, "totalAmountReqdForRet");

							FNARetireData1.setRetirementFund(rtdFund);
							FNARetireData1.setCategoryAxisFund(formatter(rtdFund.toString()));
						}
						if (value1.has("futureSavings")) {
							Long rtdSavings = checkForKeyValue(value1, "futureSavings");

							FNARetireData1.setRetrSavings(rtdSavings);
							FNARetireData1.setCategoryAxisSavings(formatter(rtdSavings.toString()));
						}
						if (value1.has("futureNeedGapfnaReport")) {
							Long rtdShortFall = checkForKeyValue(value1, "futureNeedGapfnaReport");
							FNARetireData1.setRetrShorFall(rtdShortFall);
							FNARetireData1.setCategoryAxisShortFall(formatter(rtdShortFall.toString()));
						}
						retireData.add(FNARetireData1);
					}
				}

			}
		}
		return retireData;

	}

	public List<ProtectionData> getFNAProtectionNoShort() throws JRScriptletException {
		List<ProtectionData> protectionData = new ArrayList<ProtectionData>();
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result"))
							&& (("Protection").equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");
						ProtectionData FNAProtectionData1 = new ProtectionData();
						if (value1.has("protectionRequired")) {
							Long fund = checkForKeyValue(value1, "protectionRequired");
							FNAProtectionData1.setProtectionCover(fund);
							FNAProtectionData1.setCategoryAxisFund(formatter(fund.toString()));
						}
						if (value1.has("totalSavingsValueUnit")) {
							Long savings = checkForKeyValue(value1, "totalSavingsValueUnit");
							FNAProtectionData1.setProtectionFund(savings);
							FNAProtectionData1.setCategoryAxisSavings(formatter(savings.toString()));
						}
						protectionData.add(FNAProtectionData1);
					}
				}
			}
		}
		return protectionData;

	}

	public List<ShortFallData> getFNAProtectionShortFall() throws JRScriptletException {
		List<ShortFallData> protectionData = new ArrayList<ShortFallData>();
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result"))
							&& (("Protection").equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");

						ShortFallData FNAShortData1 = new ShortFallData();
						if (value1.has("protectionRequired")) {
							Long fund = checkForKeyValue(value1, "protectionRequired");
							FNAShortData1.setProtectionCoverShort(fund);
							FNAShortData1.setCategoryAxisFund(formatter(fund.toString()));
						}
						if (value1.has("totalSavingsValueUnit")) {
							Long savings = checkForKeyValue(value1, "totalSavingsValueUnit");
							FNAShortData1.setProtectionAvailFund(savings);
							FNAShortData1.setCategoryAxisSavings(formatter(savings.toString()));
						}
						if (value1.has("needGapfnaReport")) {
							FNAShortData1.setProtectionShorFall(checkForKeyValue(value1, "needGapfnaReport"));
						}
						protectionData.add(FNAShortData1);
					}
				}

			}
		}
		return protectionData;

	}

	public List<SavingsData> getFNASavingsData() throws JRScriptletException {
		List<SavingsData> savingsData = new ArrayList<SavingsData>();
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			for (int i = 0; i < selectedGoalsArray.length(); i++) {
				jsonPartyObj = selectedGoalsArray.getJSONObject(i);

				if ((jsonPartyObj.has("result")) && ((goalSAVINGS).equals(jsonPartyObj.get("goalName").toString()))) {
					JSONObject value1 = (JSONObject) jsonPartyObj.get("result");

					SavingsData FNASavingsData1 = new SavingsData();
					if (value1.has("amountRequired")) {
						Long fund = checkForKeyValue(value1, "amountRequired");
						FNASavingsData1.setSavingsAvailCost(fund);
						FNASavingsData1.setCategoryAxisFund(formatter(fund.toString()));
					}
					if (value1.has("projectSavings")) {
						Long savings = checkForKeyValue(value1, "projectSavings");
						FNASavingsData1.setSavingsAvailSavings(savings);
						FNASavingsData1.setCategoryAxisSavings(formatter(savings.toString()));
					}
					if (value1.has("futureNeedGapfnaReport")) {
						FNASavingsData1.setShortFall(checkForKeyValue(value1, "futureNeedGapfnaReport"));
					}
					savingsData.add(FNASavingsData1);
				}
			}

		}

		return savingsData;

	}

	public List<HealthData> getFNAHealthData() throws JRScriptletException {

		List<HealthData> healthData = new ArrayList<HealthData>();
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result")) && (("Health").equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");

						HealthData FNAHealthData1 = new HealthData();
						if (value1.has("healthRequiredSlider") && !("").equals(value1.get("healthRequiredSlider"))) {
							Long fund = checkForKeyValue(value1, "healthRequiredSlider");
							FNAHealthData1.setHealthRequired(fund);
							FNAHealthData1.setCategoryAxisFund(formatter(fund.toString()));
						}
						if (value1.has("employeeBenefitSlider")) {
							Long savings = checkForKeyValue(value1, "employeeBenefitSlider");
							FNAHealthData1.setHealthSavings(savings);
							FNAHealthData1.setCategoryAxisSavings(formatter(savings.toString()));
						}
						if (value1.has("needGapfnaReport")) {
							Long health = checkForKeyValue(value1, "needGapfnaReport");
							FNAHealthData1.SetHealthShortFall(health);
						}
						healthData.add(FNAHealthData1);
					}
				}

			}
		}
		return healthData;

	}

	public List<FNADataChart> getCostOfDelayEducation(String p) {
		List<FNADataChart> costData = new ArrayList<FNADataChart>();
		Long err = (long) 0;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result")) && (("Education").equals(jsonPartyObj.get("goalName").toString()))
							&& (p).equals(jsonPartyObj.get("priority"))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");
						if (value1.has("montlySavingsArr") && value1.has("xAxisarr1")) {
							JSONArray savingsArray = value1.getJSONArray("montlySavingsArr");
							JSONArray xAxisArray = value1.getJSONArray("xAxisarr1");
							if (savingsArray != null && xAxisArray != null) {
								for (int j = 0; j < savingsArray.length(); j++) {

									Object obj1 = savingsArray.get(j);
									Object obj2 = xAxisArray.get(j);
									FNADataChart FNATableData2 = new FNADataChart();
									if (obj1 != null && obj2 != null) {
										try {
											FNATableData2.setSavingsArrayValue(checkForArrayValue(obj1));
											FNATableData2.setXaxisValue(checkForXArrayValue(obj2));
										} catch (Exception e) {
											FNATableData2.setSavingsArrayValue(err);
											FNATableData2.setXaxisValue(err);
										}
									} else {
										FNATableData2.setSavingsArrayValue(err);
										FNATableData2.setXaxisValue(err);
									}
									costData.add(FNATableData2);
								}
							}
						}

					}
				}

			}

		}
		return costData;

	}

	public List<FNADataChart> getCostOfDelayRetirement() {
		List<FNADataChart> costData = new ArrayList<FNADataChart>();
		Long err = (long) 0;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result"))
							&& (("Retirement").equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");
						if (value1.has("montlySavingsArr") && value1.has("xAxisarr1")) {
							JSONArray savingsArray = value1.getJSONArray("montlySavingsArr");

							JSONArray xAxisArray = value1.getJSONArray("xAxisarr1");
							if (savingsArray != null && xAxisArray != null) {
								for (int j = 0; j < savingsArray.length(); j++) {

									Object obj1 = savingsArray.get(j);
									Object obj2 = xAxisArray.get(j);
									FNADataChart FNATableData2 = new FNADataChart();
									if (obj1 != null && obj1 != null) {
										try {
											FNATableData2.setSavingsArrayValue(checkForArrayValue(obj1));
											FNATableData2.setXaxisValue(checkForXArrayValue(obj2));
										} catch (Exception e) {
											FNATableData2.setSavingsArrayValue(err);
											FNATableData2.setXaxisValue(err);
										}
									} else {
										FNATableData2.setSavingsArrayValue(err);
										FNATableData2.setXaxisValue(err);
									}
									costData.add(FNATableData2);
								}
							}
						}

					}
				}

			}

		}
		return costData;

	}

	public List<FNADataChart> getCostOfDelaySavings() {
		Long err = (long) 0;
		List<FNADataChart> costData = new ArrayList<FNADataChart>();
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result"))
							&& ((goalSAVINGS).equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");
						if (value1.has("montlySavingsArr") && value1.has("xAxisarr1")) {
							JSONArray savingsArray = value1.getJSONArray("montlySavingsArr");
							System.out.println("savingsArray " + savingsArray);
							JSONArray xAxisArray = value1.getJSONArray("xAxisarr1");
							System.out.println("xAxisArray " + xAxisArray);
							if (savingsArray != null && xAxisArray != null) {
								for (int j = 0; j < savingsArray.length(); j++) {
									Object obj1 = savingsArray.get(j);
									Object obj2 = xAxisArray.get(j);
									FNADataChart FNATableData2 = new FNADataChart();
									if (obj1 != null && obj2 != null) {
										try {
											FNATableData2.setSavingsArrayValue(checkForArrayValue(obj1));
											FNATableData2.setXaxisValue((checkForXArrayValue(obj2)));
										} catch (Exception e) {
											FNATableData2.setSavingsArrayValue(err);
											FNATableData2.setXaxisValue(err);
										}
									} else {
										FNATableData2.setSavingsArrayValue(err);
										FNATableData2.setXaxisValue(err);
									}
									costData.add(FNATableData2);
									System.out.println("FNATableData2 " + FNATableData2);
									System.out.println("costData " + costData);
								}
							}
						}

					}
				}

			}

		}
		return costData;

	}

	public List<FNADataChart> getCostOfDelayTransfer() {
		Long err = (long) 0;
		List<FNADataChart> costData = new ArrayList<FNADataChart>();
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result")) && (("Health").equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");
						if (value1.has("montlySavingsArr") && value1.has("xAxisarr1")) {
							JSONArray savingsArray = value1.getJSONArray("montlySavingsArr");
							JSONArray xAxisArray = value1.getJSONArray("xAxisarr1");
							if (savingsArray != null && xAxisArray != null) {
								for (int j = 0; j < savingsArray.length(); j++) {

									Object obj1 = savingsArray.get(j);
									Object obj2 = xAxisArray.get(j);
									FNADataChart FNATableData2 = new FNADataChart();
									if (!obj1.equals(null) && obj1 != null && obj2 != null) {
										try {
											FNATableData2.setSavingsArrayValue(checkForArrayValue(obj1));
											FNATableData2.setXaxisValue(checkForXArrayValue(obj2));
										} catch (Exception e) {
											FNATableData2.setSavingsArrayValue(err);
											FNATableData2.setXaxisValue(err);
										}
									} else {
										FNATableData2.setSavingsArrayValue(err);
										FNATableData2.setXaxisValue(err);
									}
									costData.add(FNATableData2);
								}
							}
						}

					}
				}

			}

		}
		return costData;

	}

	public boolean getCostOfEducationFlag(String p) {
		Boolean flag = false;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result")) && (("Education").equals(jsonPartyObj.get("goalName").toString()))
							&& (p).equals(jsonPartyObj.get("priority"))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");
						if (value1.has("surplusShow")) {

							flag = !value1.getBoolean("surplusShow");

						} else if (value1.has("montlySavingsArr")) {
							JSONArray montlySavingsArr = new JSONArray();
							montlySavingsArr = value1.getJSONArray("montlySavingsArr");

							if (montlySavingsArr.length() > 0 && checkForKeyValue(value1, "needGapfnaReport") > 0) {
								flag = true;
							}
						}

					}
				}
			}
		}
		return flag;
	}

	public boolean getCostOfRetirementFlag() {
		Boolean flag = false;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result"))
							&& (("Retirement").equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");
						if (value1.has("surplusShow")) {

							flag = !value1.getBoolean("surplusShow");

						} else if (value1.has("montlySavingsArr")) {
							JSONArray montlySavingsArr = new JSONArray();
							montlySavingsArr = value1.getJSONArray("montlySavingsArr");
							if (montlySavingsArr.length() > 0) {
								flag = true;
							}
						}

					}

				}
			}
		}

		return flag;
	}

	public boolean getCostOfAccumulationFlag() {
		Boolean flag = false;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result"))
							&& ((goalSAVINGS).equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");
						if (value1.has("surplusShow")) {

							flag = !value1.getBoolean("surplusShow");

						} else if (value1.has("montlySavingsArr")) {
							JSONArray montlySavingsArr = new JSONArray();
							montlySavingsArr = value1.getJSONArray("montlySavingsArr");
							if (montlySavingsArr.length() > 0)
								flag = true;
						}

					}
				}
			}
		}
		return flag;
	}

	public boolean getCostOfTransferFlag() throws JRScriptletException {
		Boolean flag = false;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray selectedGoalsArray = requestJSONObject.getJSONArray("selectedGoals");

			if (selectedGoalsArray != null) {
				for (int i = 0; i < selectedGoalsArray.length(); i++) {
					jsonPartyObj = selectedGoalsArray.getJSONObject(i);

					if ((jsonPartyObj.has("result")) && (("Health").equals(jsonPartyObj.get("goalName").toString()))) {
						JSONObject value1 = (JSONObject) jsonPartyObj.get("result");
						if (value1.has("surplusShow")) {

							flag = !value1.getBoolean("surplusShow");

						} else if (value1.has("montlySavingsArr")) {
							JSONArray montlySavingsArr = new JSONArray();
							montlySavingsArr = value1.getJSONArray("montlySavingsArr");
							if (montlySavingsArr.length() > 0 && checkForKeyValue(value1, "needGapfnaReport") > 0) {
								flag = true;
							}
						}

					}
				}
			}
		}
		return flag;
	}

	public List<FNADataChart> getproductRecommended() {
		List<FNADataChart> recommendedData = new ArrayList<FNADataChart>();
		String goalName = "";
		String productName = "";
		String productIterate = "";
		String productRecomAdd = "";
		String iConImage = "";
		int count;
		int countAdd;
		List<String> productCode1 = new ArrayList<String>();
		List<String> productCode2 = new ArrayList<String>();
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray value1 = (JSONArray) requestJSONObject.getJSONArray("selectedGoals");
			if (value1 != null) {
				for (int i = 0; i < value1.length(); i++) {
					productIterate = "";
					productRecomAdd = "";
					String name = "";
					JSONObject goalValue1 = (JSONObject) (value1.get(i));

					JSONArray mainInsured = goalValue1.getJSONArray("fnaReportMainInsured");
					JSONArray additionalInsured = goalValue1.getJSONArray("fnaReportAdditionalInsured");

					FNADataChart goalValues = new FNADataChart();
					if (goalValue1 != null && goalValue1.has("result")) {

						JSONObject result = goalValue1.getJSONObject("result");
						if (result != null && result.has("educationChildNameSlider")) {
							name = result.getString("educationChildNameSlider");
						}
					}
					if (goalValue1 != null && goalValue1.has("goalName") && goalValue1.get(
							"goalName") != null) /*
													 * {
													 * 
													 * if((goalValue1.getString(
													 * "newGoalName").toString()
													 * .contains("Education for"
													 * )) ||
													 * (goalValue1.getString(
													 * "newGoalName").toString()
													 * .
													 * contains("Retirement for"
													 * ))) { String goalOne =
													 * goalValue1.getString(
													 * "newGoalName").toString()
													 * ; String words[] =
													 * goalOne.split(" ");
													 * String firstTwo =
													 * words[0] + " " +
													 * words[1]; String last =
													 * words[words.length - 1];
													 * goalName =
													 * getFromResource(firstTwo)
													 * +" "+last; } else
													 */
					{
						goalName = goalValue1.getString("goalName");
						if (!StringUtils.isEmpty(goalName)) {
							goalName = goalsMap.get(goalName);
						}
					}

					if (!StringUtils.isEmpty(name) && goalName != null) {
						goalName = goalName + name.substring(0, 1).toUpperCase() + name.substring(1);
					}
					// }
					JSONArray product = goalValue1.getJSONArray("productList");
					if (product != null) {

						JSONObject productNameObject = (JSONObject) (product.get(0));
						if (productNameObject != null) {
							productName = checkForValue(productNameObject, "productName");
							iConImage = checkForValue(productNameObject, "productId");
						}
					}

					/*
					 * if (product != null) { for (int l = 0; l <
					 * product.length(); l++) { JSONObject imageValue1 =
					 * (JSONObject) (product .get(l)); if (imageValue1 != null
					 * && imageValue1.has("goalicon") &&
					 * imageValue1.get("goalicon") != null) { iConImage =
					 * (String) imageValue1 .get("goalicon"); } } }
					 */

					if (mainInsured != null) {
						count = 0;
						for (int j = 0; j < mainInsured.length(); j++) {

							JSONObject mainInsuredValue1 = (JSONObject) (mainInsured.get(j));
							if (mainInsuredValue1 != null && mainInsuredValue1.has("productCode")
									&& mainInsuredValue1.get("productCode") != null) {
								productCode1.add(mainInsuredValue1.getString("productCode"));
								productIterate = productIterate + (++count) + ". "
										+ mainInsuredValue1.getString("productCode") + "<br>";
							}
						}
					}
					if (additionalInsured != null) {
						countAdd = 0;
						for (int k = 0; k < additionalInsured.length(); k++) {
							JSONObject additionalInsuredValue1 = (JSONObject) (additionalInsured.get(k));
							if (additionalInsuredValue1 != null && additionalInsuredValue1.has("productCode")
									&& additionalInsuredValue1.get("productCode") != null) {
								productCode2.add(additionalInsuredValue1.getString("productCode"));
								productRecomAdd = productRecomAdd + (++countAdd) + ". "
										+ additionalInsuredValue1.getString("productCode") + "<br>";
							}
						}
					}
					goalValues.setGoalName(goalName);
					goalValues.setProductName(productName);
					goalValues.setImageIcon(iConImage);
					goalValues.setProductCodeMain(productCode1);
					goalValues.setProductCodeAdd(productCode2);
					goalValues.setProductCodeValue(productIterate);
					goalValues.setProductRecomAdd(productRecomAdd);
					recommendedData.add(goalValues);

					if (product != null && product.length() > 1) {

						for (int j = 1; j < product.length(); j++) {
							FNADataChart goalValues1 = new FNADataChart();

							JSONObject productNameObject = (JSONObject) (product.get(j));

							if (productNameObject != null) {
								String productName1 = checkForValue(productNameObject, "productName");
								String iConImage1 = checkForValue(productNameObject, "productId");

								goalValues1.setProductName(productName1);
								goalValues1.setImageIcon(iConImage1);
								recommendedData.add(goalValues1);
							}
						}

					}
				}
			}
		}

		return recommendedData;
	}

	public List<FNADataChart> getSelectedGoals1() {
		List<FNADataChart> goalData = new ArrayList<FNADataChart>();
		String newGoalValue = "";
		String priority = "";
		String goalNamePrint = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("goals");
		String name = "";
		String nameofChild = "";
		JSONObject jsonGoalObjVal;
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				FNADataChart selectedGoal = new FNADataChart();
				jsonGoalObj = goalsArray.getJSONObject(i);

				if (jsonGoalObj != null) {
					if (jsonGoalObj.has(PARAMETERS)) {
						JSONArray value1 = (JSONArray) jsonGoalObj.getJSONArray(PARAMETERS);
						for (int j = 0; j < value1.length(); j++) {
							jsonGoalObjVal = value1.getJSONObject(j);
							if (("Savings").equals(jsonGoalObjVal.get("name").toString())) {
								nameofChild = jsonGoalObjVal.get("value").toString();
							}
						}
					}
				}
				if (jsonGoalObj != null && jsonGoalObj.has("result")) {
					JSONObject result = jsonGoalObj.getJSONObject("result");
					if (result != null && result.has("educationChildNameSlider")) {
						name = result.getString("educationChildNameSlider");
						// name = nameofChild;
					}
				}
				if (jsonGoalObj != null && jsonGoalObj.has("goalName") && jsonGoalObj.get("goalName") != null) {
					if ((jsonGoalObj.get("goalName").toString().contains("Education for"))
							|| (jsonGoalObj.get("goalName").toString().contains("Retirement for"))) {
						String goalOne = jsonGoalObj.get("newGoalName").toString();
						String words[] = goalOne.split(" ");
						String firstTwo = "การศึกษาของเด็กชาย";
						String last = words[words.length - 1];
						newGoalValue = firstTwo + " " + last;
					} else {
						newGoalValue = jsonGoalObj.getString("goalName").toString();
					}

				}
				if (newGoalValue != null && name != null) {
						newGoalValue = goalsMap.get(newGoalValue);
				}

				if (jsonGoalObj != null && jsonGoalObj.has("priority")) {
					priority = jsonGoalObj.getString("priority");
				}
				if (jsonGoalObj != null && jsonGoalObj.has("goalName") && jsonGoalObj.get("goalName") != null) {
					goalNamePrint = jsonGoalObj.getString("goalName");
				}
				selectedGoal.setGoalNamePrint(goalNamePrint);
				selectedGoal.setPriority(priority);
				selectedGoal.setSelectedGoalName(newGoalValue);
				goalData.add(selectedGoal);
			}

		}

		return goalData;
	}

	public List<FNADataChart> getRecommendedGoals() {
		List<FNADataChart> recommendedData = new ArrayList<FNADataChart>();
		String newGoalValue = "";
		String priority = "";
		String goalNamePrint = "";
		if (requestJSONObject.has("unselectedGoals")) {
			JSONArray goalsArray = requestJSONObject.getJSONArray("unselectedGoals");
			if (goalsArray != null) {
				for (int i = 0; i < goalsArray.length(); i++) {
					FNADataChart unselectedGoal = new FNADataChart();
					jsonGoalObj = goalsArray.getJSONObject(i);
					if (jsonGoalObj != null && jsonGoalObj.has("newGoalName")
							&& jsonGoalObj.get("newGoalName") != null) {
						if ((jsonGoalObj.get("newGoalName").toString().contains("Education for"))
								|| (jsonGoalObj.get("newGoalName").toString().contains("Retirement for"))) {
							String goalOne = jsonGoalObj.get("newGoalName").toString();
							String words[] = goalOne.split(" ");
							String firstTwo = words[0] + " " + words[1];
							String last = words[words.length - 1];
							newGoalValue = firstTwo + " " + last;
						} else {
							newGoalValue = jsonGoalObj.getString("newGoalName").toString();
						}
					}
					if (jsonGoalObj != null && jsonGoalObj.has("priority")) {
						priority = jsonGoalObj.getString("priority");
					}
					if (jsonGoalObj != null && jsonGoalObj.has("goalName") && jsonGoalObj.get("goalName") != null) {
						goalNamePrint = jsonGoalObj.getString("goalName");
					}
					unselectedGoal.setGoalNamePrint(goalNamePrint);
					unselectedGoal.setPriority(priority);
					unselectedGoal.setUnSelectedGoal(newGoalValue);
					recommendedData.add(unselectedGoal);
				}

			}
		}
		return recommendedData;
	}

	public boolean getEducationText() {
		Boolean educationText = false;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray value1 = (JSONArray) requestJSONObject.getJSONArray("selectedGoals");
			if (value1 != null) {
				for (int i = 0; i < value1.length(); i++) {
					JSONObject goalValue1 = (JSONObject) (value1.get(i));
					if (goalValue1 != null && goalValue1.has("goalName") && goalValue1.get("goalName") != null) {
						if (("Education").equals(goalValue1.get("goalName").toString())) {
							educationText = true;
						}
					}
				}
			}
		}
		return educationText;
	}

	public boolean getRetirementText() {
		Boolean retirementText = false;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray value1 = (JSONArray) requestJSONObject.getJSONArray("selectedGoals");
			if (value1 != null) {
				for (int i = 0; i < value1.length(); i++) {
					JSONObject goalValue1 = (JSONObject) (value1.get(i));
					if (goalValue1 != null && goalValue1.has("goalName") && goalValue1.get("goalName") != null) {
						if (("Retirement").equals(goalValue1.get("goalName").toString())) {
							retirementText = true;
						}
					}
				}
			}
		}
		return retirementText;
	}

	public boolean getProtectionText() {
		Boolean protectionText = false;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray value1 = (JSONArray) requestJSONObject.getJSONArray("selectedGoals");
			if (value1 != null) {
				for (int i = 0; i < value1.length(); i++) {
					JSONObject goalValue1 = (JSONObject) (value1.get(i));
					if (goalValue1 != null && goalValue1.has("goalName") && goalValue1.get("goalName") != null) {
						if (("Protection").equals(goalValue1.get("goalName").toString())) {
							protectionText = true;
						}
					}
				}
			}
		}
		return protectionText;
	}

	public boolean getSavingsText() {
		Boolean SavingsText = false;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray value1 = (JSONArray) requestJSONObject.getJSONArray("selectedGoals");
			if (value1 != null) {
				for (int i = 0; i < value1.length(); i++) {
					JSONObject goalValue1 = (JSONObject) (value1.get(i));
					if (goalValue1 != null && goalValue1.has("goalName") && goalValue1.get("goalName") != null) {
						if ((goalSAVINGS).equals(goalValue1.get("goalName").toString())) {
							SavingsText = true;
						}
					}
				}
			}
		}
		return SavingsText;
	}

	public boolean getHealthText() {
		Boolean healthTransferText = false;
		if (requestJSONObject.has("selectedGoals")) {
			JSONArray value1 = (JSONArray) requestJSONObject.getJSONArray("selectedGoals");
			if (value1 != null) {
				for (int i = 0; i < value1.length(); i++) {
					JSONObject goalValue1 = (JSONObject) (value1.get(i));
					if (goalValue1 != null && goalValue1.has("goalName") && goalValue1.get("goalName") != null) {
						if (("Health").equals(goalValue1.get("goalName").toString())) {
							healthTransferText = true;
						}
					}
				}
			}
		}
		return healthTransferText;
	}

	public boolean getShowSurplusShow() {
		Boolean showText = false;
		String showSurplus = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (goalProtection.equals(jsonGoalObj.get("goalName").toString())) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null && value1.has("surplusShow")) {
								showText = (Boolean) value1.get("surplusShow");

							} else if (value1 != null && value1.has("surplus")) {
								showSurplus = value1.get("surplusShow").toString();
								if (showSurplus != null) {
									showText = true;
								}
							}
						}
					}
				}
			}
		}
		return showText;
	}

	public boolean getShorFallShow() {
		String surplusOrShortfall = "";
		Boolean showShort = false;
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null) {
					if (goalProtection.equals(jsonGoalObj.get("goalName").toString())) {
						if (jsonGoalObj.has(RESULT)) {
							JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
							if (value1 != null && value1.has("surplusShow")) {
								showShort = !(Boolean) value1.get("surplusShow");

							} else if (value1 != null && value1.has("needGapfnaReport")) {
								surplusOrShortfall = value1.getString("needGapfnaReport");
								if (surplusOrShortfall != null) {
									showShort = true;
								}
							}
						}
					}
				}
			}
		}
		return showShort;
	}

	/**
	 * Gets the fNA completion.
	 *
	 * @return the fNA completion
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 * @throws ParseException
	 */
	public String getFNACompletion() throws JRScriptletException, ParseException {
		// return requestJSONObject.get("fnaCompletion") != null ?
		// requestJSONObject
		// .get("fnaCompletion").toString() : "";
		if (requestJSONObject.get("fnaCompletion") != null) {
			// String date_s =
			// requestJSONObject.get("fnaCompletion").toString();

			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date date = new Date();
			Date thaiDate = convertDateToThai(date);
			String thaiDateAndTime = GeneraliComponentRepositoryHelper.buildDateInThai(thaiDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(thaiDate);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			if (hour < 10)
				hour = 0 + hour;
			int min = cal.get(Calendar.MINUTE);
			if (min < 10)
				min = 0 + min;
			StringBuilder finalDate = new StringBuilder();
			finalDate.append(thaiDateAndTime).append("   เวลา   ").append(Integer.toString(hour)).append(" : ")
					.append(min).append(" น.");

			return finalDate.toString();

		}
		return "";
	}

	/**
	 * Gets the agent name.
	 *
	 * @return the agent name
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getAgentName() throws JRScriptletException {
		JSONObject agent = requestJSONObject.getJSONObject("AgentDetails");
		String agentName = "";
		// return agent.getFullname() != null ? agent.getFullname() : "";
		try {
			agentName = agent.getString("agentName");
		} catch (Exception e) {
			LOGGER.error("Error in getting the agent name ");
		}

		return agentName;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getDescription() throws JRScriptletException {
		String description = "";
		String value = ((JSONObject) requestJSONObject.get(LIFESTAGE)) != null
				? (((JSONObject) requestJSONObject.get(LIFESTAGE)).get("description").toString()) : "";
		if (value.equalsIgnoreCase("single")) {
			description = "โสด";
		} else if (value.equalsIgnoreCase("married")) {
			description = "สมรส";
		} else if (value.equalsIgnoreCase("Married-with-kids")) {
			description = "สมรสและมีบุตร(เด็กเล็ก)";
		} else if (value.equalsIgnoreCase("Married-with-grown-up-kids")) {
			description = "สมรสและมีบุตร (เด็กโต)";
		} else if (value.equalsIgnoreCase("Golden-Age")) {
			description = "ผู้สูงอายุ";
		}
		return description;

	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */

	/**
	 * Gets the address.
	 *
	 * @return the address
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */

	public String getAddress1() throws JRScriptletException {
		StringBuffer address = new StringBuffer();

		if (myselfJSONObject.has("ContactDetails")) {
			JSONObject name1 = (JSONObject) myselfJSONObject.get("ContactDetails");
			if (name1.has("currentAddress")) {
				JSONObject contactJSONObj = (JSONObject) name1.get("currentAddress");
				String addressLine1 = checkForValue(contactJSONObj, ("addressLine1"));
				String addressLine2 = checkForValue(contactJSONObj, ("addressLine2"));
				String city = checkForValue(contactJSONObj, ("city"));
				String state = checkForValue(contactJSONObj, ("state"));
				String zipCode = checkForValue(contactJSONObj, ("zipCode"));
				if (!("").equals(addressLine2)) {

					address.append(checkForValue(contactJSONObj, ("addressLine2")));
				}
				if (!("").equals(addressLine1)) {
					if (!("").equals(addressLine2)) {
						address.append(",\n");
					}
					address.append(checkForValue(contactJSONObj, ("addressLine1")));
					if (!("").equals(zipCode) || !("").equals(city) || !("").equals(state)) {
						address.append(", ");
					}
				}
			}
		}
		return address.toString();

	}

	public String getAddress2() throws JRScriptletException {
		StringBuffer address = new StringBuffer();

		if (myselfJSONObject.has("ContactDetails")) {
			JSONObject name1 = (JSONObject) myselfJSONObject.get("ContactDetails");
			if (name1.has("currentAddress")) {
				JSONObject contactJSONObj = (JSONObject) name1.get("currentAddress");
				String addressLine1 = checkForValue(contactJSONObj, ("addressLine1"));
				String addressLine2 = checkForValue(contactJSONObj, ("addressLine2"));
				String city = checkForValue(contactJSONObj, ("city"));
				String state = checkForValue(contactJSONObj, ("state"));
				String zipCode = checkForValue(contactJSONObj, ("zipCode"));

				if (!("").equals(city)) {

					address.append(checkForValue(contactJSONObj, ("city")));
				}
				if (!("").equals(state)) {
					if (!("").equals(addressLine2) || !("").equals(addressLine1) || !("").equals(city)) {
						address.append(", ");
					}
					address.append(checkForValue(contactJSONObj, ("state")));
				}
				if (!("").equals(zipCode)) {
					if (!("").equals(addressLine2) || !("").equals(addressLine1) || !("").equals(city)
							|| !("").equals(state)) {
						address.append(" ");
					}
					address.append(checkForValue(contactJSONObj, ("zipCode")));
				}
			}
		}
		return address.toString();
	}

	/**
	 * Gets the risk profile.
	 *
	 * @return the risk profile
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getRiskProfile() throws JRScriptletException {
		String risk = "";

		if ((JSONObject) requestJSONObject.get(RISKPROFILE) != null) {
			risk = (((JSONObject) requestJSONObject.get(RISKPROFILE)).get("level").toString());
			if (risk != null && !EMPTY.equals(risk)) {
				risk = risk.substring(0, 1).toUpperCase() + risk.substring(1);
			}
		}
		return risk;

	}

	/**
	 * Gets the contact.
	 *
	 * @return the contact
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getContact() throws JRScriptletException {
		String contact = "";
		if (((JSONObject) myselfJSONObject.get(CONTACTDETAILS)) != null) {
			contact = formatterPhone(
					((JSONObject) myselfJSONObject.get(CONTACTDETAILS)).get("mobileNumber1").toString());
		}
		return contact;

	}

	public String getAgentContact() throws JRScriptletException {
		String contact = "";
		try {
			if (null != generaliAgent) {
				contact = generaliAgent.getZmPhone().replace("-", "").substring(0, 3) + "-" +
						generaliAgent.getZmPhone().replace("-", "").substring(3, 6)
						+ "-" + generaliAgent.getZmPhone().replace("-", "").substring(6);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting the agent contact");
		}
		return contact;
	}

	public String formatterPhone(String number) {
		Integer amount = number.length();
		StringBuffer sb = new StringBuffer();
		if (amount > 3) {
			sb.append(number.substring(0, 3));
			sb.append("-");

		} else {
			sb.append(number);
		}
		if (amount > 5) {
			sb.append(number.substring(3, 6));
			sb.append("-");
			sb.append(number.substring(6));
		} else {
			sb.append(number.substring(5));
		}
		return sb.toString();

	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getEmail() throws JRScriptletException {
		return ((JSONObject) myselfJSONObject.get(CONTACTDETAILS)) != null
				? ((JSONObject) myselfJSONObject.get(CONTACTDETAILS)).get("emailId").toString() : "";
	}

	public String getAgentEmail() throws JRScriptletException {
		String email = "";
		try {
			if (((JSONObject) requestJSONObject.get("AgentDetails")) != null) {
				email = ((JSONObject) requestJSONObject.get("AgentDetails")).get("emailId").toString();
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting the agent email");
		}
		return email;
	}

	/**
	 * Gets the age.
	 *
	 * @return the age
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 * @throws ParseException
	 *             the parse exception
	 */
	public String getAge() throws JRScriptletException, ParseException {
		String age = null;
		;
		if (myselfJSONObject.get(BASICDETAILS) != null) {
			JSONObject basicDetailsJsonObj = (JSONObject) myselfJSONObject.get(BASICDETAILS);

			if (basicDetailsJsonObj.has("age") && !("").equals(basicDetailsJsonObj.get("age").toString())) {
				age = basicDetailsJsonObj.get("age").toString() + "  " + YEARS;
			} else {
				age = basicDetailsJsonObj.get("age").toString();
			}

		}
		return age;
	}

	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public String getImage() {
		String image = "";
		String temptoken = null;
		if (myselfJSONObject.get(BASICDETAILS) != null) {
			JSONObject basicDetailsJsonObj = (JSONObject) myselfJSONObject.get(BASICDETAILS);
			image = basicDetailsJsonObj.getString("photo").toString();
			StringTokenizer token = new StringTokenizer(image, ",");
			while (token.hasMoreTokens()) {
				temptoken = token.nextToken();
			}
			if (temptoken != null) {
				image = temptoken;
			}
		}
		return image;
	}

	public String getAgentImage() {
		String image = "";
		String temptoken = null;
		if (requestJSONObject.get("AgentDetails") != null) {
			JSONObject agentDetailsJsonObj = (JSONObject) requestJSONObject.get("AgentDetails");
			image = agentDetailsJsonObj.getString("photo").toString();
			StringTokenizer token = new StringTokenizer(image, ",");
			while (token.hasMoreTokens()) {
				temptoken = token.nextToken();
			}
			if (temptoken != null) {
				image = temptoken;
			}
		}
		return image;
	}

	public String formatHomeNumber(String number) {
		StringBuffer sb = new StringBuffer();
		int length = number.length();
		if (number.length() > 0) {
			try {
				sb.append(number.substring(0, 3));
				sb.append(" - ");

				sb.append(number.substring(3, 6));
				sb.append(" - ");

				if (length >= 8) {
					sb.append(number.substring(6, 10));
				} else {
					sb.append(number.substring(6, 7));
				}
			}

			catch (Exception e) {
				sb.replace(0, sb.length(), "");

			}
		}
		return sb.toString();

	}

	public String checkForValue(JSONObject obj, String key) {
		String value = "";
		if (obj.has(key)) {
			Object jsonValue = obj.get(key);
			if (jsonValue != null) {
				value = jsonValue.toString();
			}
		}
		return value;
	}

	public String formatter(String number) {
		double amount = 0;
		String result = number;
		try {
			amount = Double.parseDouble(number);
			BigDecimal bd = new BigDecimal(amount);

		} catch (NumberFormatException e) {

		} finally {
			DecimalFormat formatter = new DecimalFormat("");
			// if (amount > 0) {
			result = formatter.format(amount);
			// }

		}

		return result;

	}

	public Long checkForKeyValue(JSONObject obj, String key) {
		Long value = (long) 0;
		BigDecimal divisor = new BigDecimal("1000");
		if (obj.has(key)) {
			Object jsonValue = obj.get(key);
			if (jsonValue != null && !("").equals(jsonValue)) {
				try {

					BigDecimal bd = new BigDecimal(jsonValue.toString());
					value = bd.divide(divisor, 0, BigDecimal.ROUND_HALF_UP).longValue();

				} catch (Exception e) {

				}
			}
		}
		return value;
	}

	public Long checkForArrayValue(Object obj) {
		Long value = (long) 0;
		BigDecimal divisor = new BigDecimal(1000);

		if (obj != null) {
			try {
				BigDecimal bd = new BigDecimal(obj.toString());
				bd = bd.divide(divisor);
				BigDecimal scaled = bd.setScale(0, RoundingMode.HALF_UP);
				value = scaled.longValue();

			} catch (Exception e) {

			}

		}
		return value;
	}

	public Long checkForXArrayValue(Object obj) {
		Long value = (long) 0;

		if (obj != null) {
			try {
				BigDecimal bd = new BigDecimal(obj.toString());
				value = bd.longValue();

			} catch (Exception e) {

			}

		}
		return value;
	}

	public String getChildNameEdu(String p) throws JRScriptletException {
		String educationChildName = "";
		JSONArray goalsArray = requestJSONObject.getJSONArray("selectedGoals");
		if (goalsArray != null) {
			for (int i = 0; i < goalsArray.length(); i++) {
				jsonGoalObj = goalsArray.getJSONObject(i);
				if (jsonGoalObj != null && goalEducation.equals(jsonGoalObj.get("goalName"))
						&& p.equals(jsonGoalObj.get("priority"))) {
					if (jsonGoalObj.has(RESULT)) {
						JSONObject value1 = (JSONObject) jsonGoalObj.get(RESULT);
						if (value1 != null && value1.has("educationChildNameSlider")) {
							String name = value1.get("educationChildNameSlider").toString();
							if (name != null && !(EMPTY).equals(name)) {
								educationChildName = "การศึกษาของ  " + name.substring(0, 1).toUpperCase()
										+ name.substring(1);
							}
							break;
						} else {
							educationChildName = "เพื่อการศึกษา";
						}
					}
				}
			}
		}
		return educationChildName;
	}

	public String getMyselfNameRtd() throws JRScriptletException {

		return ((JSONObject) myselfJSONObject.get(BASICDETAILS)) != null ? "แผนชีวิตในวัยเกษียณของ"
				+ ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("firstName").toString() : " ";

	}

	public String getMyselfNameSavings() throws JRScriptletException {

		return ((JSONObject) myselfJSONObject.get(BASICDETAILS)) != null ? "เป้าหมายด้านการออมของ "
				+ ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("firstName").toString() : " ";

	}

	public String getMyselfNameProtNoShrtfall() throws JRScriptletException {

		return ((JSONObject) myselfJSONObject.get(BASICDETAILS)) != null ? "แผนความคุ้มครองชีวิตของ"
				+ ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("firstName").toString() : " ";

	}

	public String getMyselfNameProtShrtfall() throws JRScriptletException {

		return ((JSONObject) myselfJSONObject.get(BASICDETAILS)) != null ? "แผนความคุ้มครองชีวิตของ"
				+ ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("firstName").toString() : " ";

	}

	public String getMyselfNameHealth() throws JRScriptletException {

		return ((JSONObject) myselfJSONObject.get(BASICDETAILS)) != null
				? "เป้าหมายด้านสุขภาพ " + ((JSONObject) myselfJSONObject.get(BASICDETAILS)).get("firstName").toString()
				: " ";

	}

	public Collection<Integer> checkPriority(String goalName) {

		Collection<Integer> result = null;
		if (goalsPriorityMap.get(goalName) != null) {
			result = goalsPriorityMap.get(goalName);
		}

		return result;

	}

	public Integer getEducationPriority() {

		int result = 0;

		if (checkPriority(goalEducation).toArray().length != 0) {
			result = (Integer) checkPriority(goalEducation).toArray()[0];
		}
		return result;
	}

	public Integer getEducationPriority2() {

		int result = 0;
		if (checkPriority(goalEducation).toArray().length > 1) {
			result = (Integer) checkPriority(goalEducation).toArray()[1];

		}
		return result;
	}

	public Integer getEducationPriority3() {

		int result = 0;
		if (checkPriority(goalEducation).toArray().length > 2) {
			result = (Integer) checkPriority(goalEducation).toArray()[2];
		}
		return result;
	}

	public Integer getEducationPriority4() {

		int result = 0;
		if (checkPriority(goalEducation).toArray().length > 3) {
			result = (Integer) checkPriority(goalEducation).toArray()[3];
		}
		return result;
	}

	public Integer getRtdPriority() {
		Integer result = 0;
		if (checkPriority(goalRtd).toArray().length != 0) {
			result = (Integer) checkPriority(goalRtd).toArray()[0];
		}
		return result;
	}

	public Integer getHealthPriority() {
		Integer result = 0;
		if (checkPriority(goalHEALTH).toArray().length != 0) {
			result = (Integer) (checkPriority(goalHEALTH).toArray())[0];
		}
		return result;
	}

	public Integer getSavingsPriority() {
		Integer result = 0;
		if (checkPriority(goalSAVINGS).toArray().length != 0) {
			result = (Integer) (checkPriority(goalSAVINGS).toArray())[0];
		}
		return result;
	}

	public Integer getProtectionPriority() {
		Integer result = 0;
		if (checkPriority(goalProtection).toArray().length != 0) {
			result = (Integer) (checkPriority(goalProtection).toArray())[0];
		}
		return result;
	}

	public String getName1() {
		String name = "";
		String value = "";
		int age = 0;
		if (familyData.size() > 0) {
			value = familyData.get(0).getName();
		}
		if (value.isEmpty()) {
			name = "-";
		} else {
			String sub = this.getAge1().trim().substring(0, (this.getAge1().length()) - 4);
			age = Integer.parseInt(sub);
			if (age < 16) {
				if (this.getRelation1().equalsIgnoreCase("Son"))
					name = "เด็กชาย" + value;
				else if (this.getRelation1().equalsIgnoreCase("Daughter"))
					name = "เด็กหญิง" + value;

			} else {
				if (this.getRelation1().equalsIgnoreCase("Son"))
					name = "ชาย" + value;
				else if (this.getRelation1().equalsIgnoreCase("Daughter"))
					name = "หญิง" + value;
				else if ((this.getRelation1().equalsIgnoreCase("Spouse")
						&& this.getGender1().equalsIgnoreCase("Female"))
						|| this.getRelation1().equalsIgnoreCase("Mother"))
					name = "นาง" + value;
				else if ((this.getRelation1().equalsIgnoreCase("Spouse") && this.getGender1().equalsIgnoreCase("Male"))
						|| this.getRelation1().equalsIgnoreCase("Father"))
					name = "นาย" + value;
			}

		}
		return name;

	}

	public String getGender1() {
		String gender = "";
		String value = "";
		if (familyData.size() > 0) {
			value = familyData.get(0).getGender();
		}
		if (value.isEmpty()) {
			gender = "-";
		} else {
			gender = value;
		}
		return gender;

	}

	public String getAge1() {
		String age = "";
		String value = "";
		if (familyData.size() > 0) {
			value = familyData.get(0).getAge();
		}
		if (value.isEmpty()) {
			age = "-";
		} else {
			age = value;
		}

		return age;

	}

	public String getRelation1() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 0) {
			value = familyData.get(0).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else
			relationship = value;

		return relationship;
	}

	public String getRelationShip1() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 0) {
			value = familyData.get(0).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else {
			if (value.equalsIgnoreCase("Spouse")) {
				relationship = "คู่สมรส";
			} else if (value.equalsIgnoreCase("Son") || value.equalsIgnoreCase("Daughter")) {
				relationship = "บุตร";
			} else if (value.equalsIgnoreCase("Father")) {
				relationship = "บิดา";
			} else if (value.equalsIgnoreCase("Mother")) {
				relationship = "มารดา";
			} else {
				relationship = value;
			}

		}

		return relationship;
	}

	public String getName2() {
		String name = "";
		String value = "";
		int age = 0;
		if (familyData.size() > 1) {
			value = familyData.get(1).getName();
		}
		if (value.isEmpty()) {
			name = "-";
		} else {
			String sub = this.getAge2().trim().substring(0, (this.getAge2().length()) - 4);
			age = Integer.parseInt(sub);
			if (age < 16) {
				if (this.getRelation2().equalsIgnoreCase("Son"))
					name = "เด็กชาย" + value;
				else if (this.getRelation2().equalsIgnoreCase("Daughter"))
					name = "เด็กหญิง" + value;

			} else {
				if (this.getRelation2().equalsIgnoreCase("Son"))
					name = "ชาย" + value;
				else if (this.getRelation2().equalsIgnoreCase("Daughter"))
					name = "หญิง" + value;
				else if ((this.getRelation2().equalsIgnoreCase("Spouse")
						&& this.getGender2().equalsIgnoreCase("Female"))
						|| this.getRelation2().equalsIgnoreCase("Mother"))
					name = "นาง" + value;
				else if ((this.getRelation2().equalsIgnoreCase("Spouse") && this.getGender2().equalsIgnoreCase("Male"))
						|| this.getRelation2().equalsIgnoreCase("Father"))
					name = "นาย" + value;
			}

		}
		return name;

	}

	public String getGender2() {
		String gender = "";
		String value = "";
		if (familyData.size() > 1) {
			value = familyData.get(1).getGender();
		}
		if (value.isEmpty()) {
			gender = "-";
		} else {
			gender = value;
		}
		return gender;

	}

	public String getAge2() {
		String age = "";
		String value = "";
		if (familyData.size() > 1) {
			value = familyData.get(1).getAge();
		}
		if (value.isEmpty()) {
			age = "-";
		} else {
			age = value;
		}
		return age;

	}

	public String getRelation2() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 1) {
			value = familyData.get(1).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else
			relationship = value;

		return relationship;
	}

	public String getRelationShip2() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 1) {

			value = familyData.get(1).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else {
			if (value.equalsIgnoreCase("Spouse")) {
				relationship = "คู่สมรส";
			} else if (value.equalsIgnoreCase("Son") || value.equalsIgnoreCase("Daughter")) {
				relationship = "บุตร";
			} else if (value.equalsIgnoreCase("Father")) {
				relationship = "บิดา";
			} else if (value.equalsIgnoreCase("Mother")) {
				relationship = "มารดา";
			} else
				relationship = value;

		}

		return relationship;

	}

	public String getName3() {
		String name = "";
		String value = "";
		int age = 0;
		if (familyData.size() > 2) {
			value = familyData.get(2).getName();
		}
		if (value.isEmpty()) {
			name = "-";
		} else {
			String sub = this.getAge3().trim().substring(0, (this.getAge3().length()) - 4);
			age = Integer.parseInt(sub);
			if (age < 16) {
				if (this.getRelation3().equalsIgnoreCase("Son"))
					name = "เด็กชาย" + value;
				else if (this.getRelation3().equalsIgnoreCase("Daughter"))
					name = "เด็กหญิง" + value;

			} else {
				if (this.getRelation3().equalsIgnoreCase("Son"))
					name = "ชาย" + value;
				else if (this.getRelation3().equalsIgnoreCase("Daughter"))
					name = "หญิง" + value;
				else if ((this.getRelation3().equalsIgnoreCase("Spouse")
						&& this.getGender3().equalsIgnoreCase("Female"))
						|| (this.getRelation3().equalsIgnoreCase("Mother")
								|| this.getRelation3().equalsIgnoreCase("มารดา")))
					name = "นาง" + value;
				else if ((this.getRelation3().equalsIgnoreCase("Spouse") && this.getGender3().equalsIgnoreCase("Male"))
						|| this.getRelation3().equalsIgnoreCase("Father"))
					name = "นาย" + value;
			}

		}
		return name;
	}

	public String getGender3() {
		String gender = "";
		String value = "";
		if (familyData.size() > 2) {
			value = familyData.get(2).getGender();
		}
		if (value.isEmpty()) {
			gender = "-";
		} else {
			gender = value;
		}
		return gender;

	}

	public String getAge3() {
		String age = "";
		String value = "";
		if (familyData.size() > 2) {
			value = familyData.get(2).getAge();
		}
		if (value.isEmpty()) {
			age = "-";
		} else {
			age = value;
		}
		return age;

	}

	public String getRelation3() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 2) {
			value = familyData.get(2).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else if (value.equalsIgnoreCase("Father")) {
			relationship = "บิดา";
		} else if (value.equalsIgnoreCase("Mother")) {
			relationship = "มารดา";
		} else
			relationship = value;

		return relationship;
	}

	public String getRelationShip3() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 2) {
			value = familyData.get(2).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else {
			if (value.equalsIgnoreCase("Spouse")) {
				relationship = "คู่สมรส";
			} else if (value.equalsIgnoreCase("Son") || value.equalsIgnoreCase("Daughter")) {
				relationship = "บุตร";
			} else
				relationship = value;

		}

		return relationship;

	}

	public String getName4() {
		String name = "";
		String value = "";
		int age = 0;
		if (familyData.size() > 3) {
			value = familyData.get(3).getName();
		}
		if (value.isEmpty()) {
			name = "-";
		} else {
			String sub = this.getAge4().trim().substring(0, (this.getAge4().length()) - 4);
			age = Integer.parseInt(sub);
			if (age < 16) {
				if (this.getRelation4().equalsIgnoreCase("Son"))
					name = "เด็กชาย" + value;
				else if (this.getRelation4().equalsIgnoreCase("Daughter"))
					name = "เด็กหญิง" + value;

			} else {
				if (this.getRelation4().equalsIgnoreCase("Son"))
					name = "ชาย" + value;
				else if (this.getRelation4().equalsIgnoreCase("Daughter"))
					name = "หญิง" + value;
				else if ((this.getRelation4().equalsIgnoreCase("Spouse")
						&& this.getGender4().equalsIgnoreCase("Female"))
						|| this.getRelation4().equalsIgnoreCase("Mother"))
					name = "นาง" + value;
				else if ((this.getRelation4().equalsIgnoreCase("Spouse") && this.getGender4().equalsIgnoreCase("Male"))
						|| this.getRelation4().equalsIgnoreCase("Father"))
					name = "นาย" + value;
			}

		}
		return name;

	}

	public String getGender4() {
		String gender = "";
		String value = "";
		if (familyData.size() > 3) {
			value = familyData.get(3).getGender();
		}
		if (value.isEmpty()) {
			gender = "-";
		} else {
			gender = value;
		}
		return gender;

	}

	public String getAge4() {
		String age = "";
		String value = "";
		if (familyData.size() > 3) {
			value = familyData.get(3).getAge();
		}
		if (value.isEmpty()) {
			age = "-";
		} else {
			age = value;
		}
		return age;

	}

	public String getRelation4() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 3) {
			value = familyData.get(3).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else
			relationship = value;

		return relationship;
	}

	public String getRelationShip4() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 3) {
			value = familyData.get(3).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else {
			if (value.equalsIgnoreCase("Spouse")) {
				relationship = "คู่สมรส";
			} else if (value.equalsIgnoreCase("Son") || value.equalsIgnoreCase("Daughter")) {
				relationship = "บุตร";
			} else if (value.equalsIgnoreCase("Father")) {
				relationship = "บิดา";
			} else if (value.equalsIgnoreCase("Mother")) {
				relationship = "มารดา";
			} else
				relationship = value;

		}

		return relationship;

	}

	public String getName5() {
		String name = "";
		String value = "";
		int age = 0;
		if (familyData.size() > 4) {
			value = familyData.get(4).getName();
		}
		if (value.isEmpty()) {
			name = "-";
		} else {
			String sub = this.getAge5().trim().substring(0, (this.getAge5().length()) - 4);
			age = Integer.parseInt(sub);
			if (age < 16) {
				if (this.getRelation5().equalsIgnoreCase("Son"))
					name = "เด็กชาย" + value;
				else if (this.getRelation5().equalsIgnoreCase("Daughter"))
					name = "เด็กหญิง" + value;

			} else {
				if (this.getRelation5().equalsIgnoreCase("Son"))
					name = "ชาย" + value;
				else if (this.getRelation5().equalsIgnoreCase("Daughter"))
					name = "หญิง" + value;
				else if ((this.getRelation5().equalsIgnoreCase("Spouse")
						&& this.getGender5().equalsIgnoreCase("Female"))
						|| this.getRelation5().equalsIgnoreCase("Mother"))
					name = "นาง" + value;
				else if ((this.getRelation5().equalsIgnoreCase("Spouse") && this.getGender5().equalsIgnoreCase("Male"))
						|| this.getRelation5().equalsIgnoreCase("Father"))
					name = "นาย" + value;
			}

		}
		return name;
	}

	public String getGender5() {
		String gender = "";
		String value = "";
		if (familyData.size() > 4) {
			value = familyData.get(4).getGender();
		}
		if (value.isEmpty()) {
			gender = "-";
		} else {
			gender = value;
		}
		return gender;

	}

	public String getAge5() {
		String age = "";
		String value = "";
		if (familyData.size() > 4) {
			value = familyData.get(4).getAge();
		}
		if (value.isEmpty()) {
			age = "-";
		} else {
			age = value;
		}
		return age;

	}

	public String getRelation5() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 4) {
			value = familyData.get(4).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else
			relationship = value;

		return relationship;
	}

	public String getRelationShip5() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 4) {
			value = familyData.get(4).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else {
			if (value.equalsIgnoreCase("Spouse")) {
				relationship = "คู่สมรส";
			} else if (value.equalsIgnoreCase("Son") || value.equalsIgnoreCase("Daughter")) {
				relationship = "บุตร";
			} else if (value.equalsIgnoreCase("Father")) {
				relationship = "บิดา";
			} else if (value.equalsIgnoreCase("Mother")) {
				relationship = "มารดา";
			} else
				relationship = value;

		}

		return relationship;

	}

	public String getName6() {
		String name = "";
		String value = "";
		int age = 0;
		if (familyData.size() > 5) {
			value = familyData.get(5).getName();
		}
		if (value.isEmpty()) {
			name = "-";
		} else {
			String sub = this.getAge6().trim().substring(0, (this.getAge6().length()) - 4);
			age = Integer.parseInt(sub);
			if (age < 16) {
				if (this.getRelation6().equalsIgnoreCase("Son"))
					name = "เด็กชาย" + value;
				else if (this.getRelation6().equalsIgnoreCase("Daughter"))
					name = "เด็กหญิง" + value;

			} else {
				if (this.getRelation6().equalsIgnoreCase("Son"))
					name = "ชาย" + value;
				else if (this.getRelation6().equalsIgnoreCase("Daughter"))
					name = "หญิง" + value;
				else if ((this.getRelation6().equalsIgnoreCase("Spouse")
						&& this.getGender6().equalsIgnoreCase("Female"))
						|| this.getRelation6().equalsIgnoreCase("Mother"))
					name = "นาง" + value;
				else if ((this.getRelation6().equalsIgnoreCase("Spouse") && this.getGender6().equalsIgnoreCase("Male"))
						|| this.getRelation6().equalsIgnoreCase("Father"))
					name = "นาย" + value;
			}

		}
		return name;

	}

	public String getGender6() {
		String gender = "";
		String value = "";
		if (familyData.size() > 5) {
			value = familyData.get(5).getGender();
		}
		if (value.isEmpty()) {
			gender = "-";
		} else {
			gender = value;
		}
		return gender;

	}

	public String getAge6() {
		String age = "";
		String value = "";
		if (familyData.size() > 5) {
			value = familyData.get(5).getAge();
		}
		if (value.isEmpty()) {
			age = "-";
		} else {
			age = value;
		}
		return age;

	}

	public String getRelation6() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 5) {
			value = familyData.get(5).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else
			relationship = value;

		return relationship;
	}

	public String getRelationShip6() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 5) {
			value = familyData.get(5).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else {
			if (value.equalsIgnoreCase("Spouse")) {
				relationship = "คู่สมรส";
			} else if (value.equalsIgnoreCase("Son") || value.equalsIgnoreCase("Daughter")) {
				relationship = "บุตร";
			} else if (value.equalsIgnoreCase("Father")) {
				relationship = "บิดา";
			} else if (value.equalsIgnoreCase("Mother")) {
				relationship = "มารดา";
			} else
				relationship = value;

		}

		return relationship;

	}

	public String getName7() {
		String name = "";
		String value = "";
		int age = 0;
		if (familyData.size() > 6) {
			value = familyData.get(6).getName();
		}
		if (value.isEmpty()) {
			name = "-";
		} else {
			String sub = this.getAge7().trim().substring(0, (this.getAge7().length()) - 4);
			age = Integer.parseInt(sub);
			if (age < 16) {
				if (this.getRelation7().equalsIgnoreCase("Son"))
					name = "เด็กชาย" + value;
				else if (this.getRelation7().equalsIgnoreCase("Daughter"))
					name = "เด็กหญิง" + value;

			} else {
				if (this.getRelation7().equalsIgnoreCase("Son"))
					name = "ชาย" + value;
				else if (this.getRelation7().equalsIgnoreCase("Daughter"))
					name = "หญิง" + value;
				else if ((this.getRelation7().equalsIgnoreCase("Spouse")
						&& this.getGender7().equalsIgnoreCase("Female"))
						|| this.getRelation7().equalsIgnoreCase("Mother"))
					name = "นาง" + value;
				else if ((this.getRelation7().equalsIgnoreCase("Spouse") && this.getGender7().equalsIgnoreCase("Male"))
						|| this.getRelation7().equalsIgnoreCase("Father"))
					name = "นาย" + value;
			}

		}
		return name;

	}

	public String getGender7() {
		String gender = "";
		String value = "";
		if (familyData.size() > 6) {
			value = familyData.get(6).getGender();
		}
		if (value.isEmpty()) {
			gender = "-";
		} else {
			gender = value;
		}
		return gender;

	}

	public String getAge7() {
		String age = "";
		String value = "";
		if (familyData.size() > 6) {
			value = familyData.get(6).getAge();
		}
		if (value.isEmpty()) {
			age = "-";
		} else {
			age = value;
		}
		return age;

	}

	public String getRelation7() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 6) {
			value = familyData.get(6).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else
			relationship = value;

		return relationship;
	}

	public String getRelationShip7() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 6) {
			value = familyData.get(6).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else {
			if (value.equalsIgnoreCase("Spouse")) {
				relationship = "คู่สมรส";
			} else if (value.equalsIgnoreCase("Son") || value.equalsIgnoreCase("Daughter")) {
				relationship = "บุตร";
			} else if (value.equalsIgnoreCase("Father")) {
				relationship = "บิดา";
			} else if (value.equalsIgnoreCase("Mother")) {
				relationship = "มารดา";
			} else
				relationship = value;

		}

		return relationship;
	}

	public String getName8() {
		String name = "";
		String value = "";
		int age = 0;
		if (familyData.size() > 7) {
			value = familyData.get(7).getName();
		}
		if (value.isEmpty()) {
			name = "-";
		} else {
			String sub = this.getAge8().trim().substring(0, (this.getAge8().length()) - 4);
			age = Integer.parseInt(sub);
			if (age < 16) {
				if (this.getRelation8().equalsIgnoreCase("Son"))
					name = "เด็กชาย" + value;
				else if (this.getRelation8().equalsIgnoreCase("Daughter"))
					name = "เด็กหญิง" + value;

			} else {
				if (this.getRelation8().equalsIgnoreCase("Son"))
					name = "ชาย" + value;
				else if (this.getRelation8().equalsIgnoreCase("Daughter"))
					name = "หญิง" + value;
				else if ((this.getRelation8().equalsIgnoreCase("Spouse")
						&& this.getGender8().equalsIgnoreCase("Female"))
						|| this.getRelation8().equalsIgnoreCase("Mother"))
					name = "นาง" + value;
				else if ((this.getRelation8().equalsIgnoreCase("Spouse") && this.getGender8().equalsIgnoreCase("Male"))
						|| this.getRelation8().equalsIgnoreCase("Father"))
					name = "นาย" + value;
			}

		}
		return name;

	}

	public String getGender8() {
		String gender = "";
		String value = "";
		if (familyData.size() > 8) {
			value = familyData.get(8).getGender();
		}
		if (value.isEmpty()) {
			gender = "-";
		} else {
			gender = value;
		}
		return gender;

	}

	public String getAge8() {
		String age = "";
		String value = "";
		if (familyData.size() > 7) {
			value = familyData.get(7).getAge();
		}
		if (value.isEmpty()) {
			age = "-";
		} else {
			age = value;
		}
		return age;

	}

	public String getRelation8() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 7) {
			value = familyData.get(7).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else
			relationship = value;

		return relationship;
	}

	public String getRelationShip8() {
		String relationship = "";
		String value = "";
		if (familyData.size() > 7) {
			value = familyData.get(7).getRelationship();
		}
		if (value.isEmpty()) {
			relationship = "-";
		} else {
			if (value.equals("Spouse")) {
				relationship = "คู่สมรส";
			} else if (value.equals("Son") || value.equals("Daughter")) {
				relationship = "บุตร";
			} else if (value.equalsIgnoreCase("Father")) {
				relationship = "บิดา";
			} else if (value.equalsIgnoreCase("Mother")) {
				relationship = "มารดา";
			} else
				relationship = value;

		}

		return relationship;

	}

	/*
	 * public String getName9() { String name = ""; String value=""; if
	 * (familyData.size() > 8) { value = familyData.get(8).getName(); }
	 * if(value.isEmpty()) { name="-"; } else { name=value; } return name;
	 * 
	 * }
	 * 
	 * public String getAge9() { String age = ""; String value=""; if
	 * (familyData.size() > 8) { age = familyData.get(8).getAge(); }
	 * if(value.isEmpty()) { age="-"; } else { age=value; } return age;
	 * 
	 * }
	 * 
	 * public String getRelationShip9() { String relationship = ""; String
	 * value=""; if (familyData.size() > 8) { relationship =
	 * familyData.get(8).getRelationship(); } if(value.isEmpty()) {
	 * relationship="-"; } else { relationship=value; } return relationship;
	 * 
	 * }
	 * 
	 * public String getName10() { String name = ""; String value=""; if
	 * (familyData.size() > 9) { name = familyData.get(9).getName(); }
	 * if(value.isEmpty()) { name="-"; } else { name=value; } return name;
	 * 
	 * }
	 * 
	 * public String getAge10() { String age = ""; if (familyData.size() > 9) {
	 * age = familyData.get(9).getAge(); } return age;
	 * 
	 * }
	 * 
	 * public String getRelationShip10() { String relationship = ""; if
	 * (familyData.size() > 9) { relationship =
	 * familyData.get(9).getRelationship(); } return relationship;
	 * 
	 * }
	 */

	public Integer getCountOfFamilyMembers() {
		int count = 0;
		count = familyData.size();
		return count;

	}

	/*
	 * public String getFromResource(String key){ String value=key; try{
	 * if(jsonObjectResourceIN.containsKey(key)){ value=
	 * jsonObjectResourceIN.get(key).toString(); } } catch(Exception e){
	 * LOGGER.error("Error in getting value from resource json for Key "+ key);
	 * } return value; }
	 */
	public String getDate() throws ParseException {
		/* SimpleDateFormat dt1 = new SimpleDateFormat("dd MM yyyy"); */
		Date modifiedDate = goalAndNeed.getModifiedDateTime();
		Date insuredDobInThai = convertDateToThai(modifiedDate);
		String thaiDate = GeneraliComponentRepositoryHelper.buildDateInThai(insuredDobInThai);
		return thaiDate;
	}

	private Date convertDateToThai(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, 543);
		Date outputDate = calendar.getTime();
		return outputDate;
	}

	public String getSalutation() throws JRScriptletException {
		String salutation = "";
		if (myselfJSONObject.get(BASICDETAILS) != null) {
			JSONObject basicDetailsJsonObj = (JSONObject) myselfJSONObject.get(BASICDETAILS);
			if (basicDetailsJsonObj.has("title") && !("").equals(basicDetailsJsonObj.get("title"))) {
				salutation = basicDetailsJsonObj.get("title").toString();
			}
		}
		return salutation;
	}
}