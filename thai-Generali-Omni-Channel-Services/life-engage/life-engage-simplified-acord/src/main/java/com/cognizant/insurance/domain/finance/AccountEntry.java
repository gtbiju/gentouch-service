/**
 * 
 */
package com.cognizant.insurance.domain.finance;

import java.util.Date;
import java.util.Set;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.finance.financecodelists.EntryCategoryCodeList;
import com.cognizant.insurance.domain.finance.financecodelists.PaymentBasisReasonCodeList;

/**
 * The AccountEntry concept represents an atomic transaction or alteration in an
 * Account and allows for current and historical positions of an Account.
 * 
 * All changes to an account, either credit or debit, need to be represented by
 * an account entry. This means that an account balance or position can only be
 * changed by adding new account entries to an account.
 * 
 * @author 301350
 * 
 * 
 */
public class AccountEntry extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7087678925138364548L;

    /**
     * The type of transaction that generated an account entry.
     * 
     * 
     * 
     */
    private PaymentBasisReasonCodeList transactionTypeCode;

    /**
     * The amount of money of the account entry. The identification of whether
     * the amount is a debit or a credit is defined in the Debit credit
     * indicator.
     * 
     * 
     * 
     */
    private CurrencyAmount accountEntryAmount;

    /**
     * Type of the AccountEntry.
     * 
     * 
     * 
     */
    private EntryCategoryCodeList accountEntryTypeCode;

    /**
     * A free-text statement used to explain the meaning of the account entry.
     * 
     * 
     * 
     */
    private String description;

    /**
     * The date at which the account entry is posted to its account.
     * 
     * 
     * 
     */
    private Date postedDate;

    /**
     * The date from which the amount of money of this account entry can be
     * accounted for.
     * 
     * 
     * 
     */
    private Date valueDate;

    /**
     * Each insurer has its own strategies to split amount (on coverages,
     * commissions, taxes, investment supports ...). This composition allows for
     * the decomposition of an account entry into sub amounts.
     * 
     * 
     * 
     * 
     * 
     */
    private Set<AccountEntry> splitInto;

    /**
     * Gets the transactionTypeCode.
     * 
     * @return Returns the transactionTypeCode.
     */
    public PaymentBasisReasonCodeList getTransactionTypeCode() {
        return transactionTypeCode;
    }

    /**
     * Gets the accountEntryAmount.
     * 
     * @return Returns the accountEntryAmount.
     */
    public CurrencyAmount getAccountEntryAmount() {
        return accountEntryAmount;
    }

    /**
     * Gets the accountEntryTypeCode.
     * 
     * @return Returns the accountEntryTypeCode.
     */
    public EntryCategoryCodeList getAccountEntryTypeCode() {
        return accountEntryTypeCode;
    }

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the postedDate.
     * 
     * @return Returns the postedDate.
     */
    public Date getPostedDate() {
        return postedDate;
    }

    /**
     * Gets the valueDate.
     * 
     * @return Returns the valueDate.
     */
    public Date getValueDate() {
        return valueDate;
    }

    /**
     * Gets the splitInto.
     * 
     * @return Returns the splitInto.
     */
    public Set<AccountEntry> getSplitInto() {
        return splitInto;
    }

    /**
     * Sets The transactionTypeCode.
     * 
     * @param transactionTypeCode
     *            The transactionTypeCode to set.
     */
    public void setTransactionTypeCode(
            final PaymentBasisReasonCodeList transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }

    /**
     * Sets The accountEntryAmount.
     * 
     * @param accountEntryAmount
     *            The accountEntryAmount to set.
     */
    public void setAccountEntryAmount(
            final CurrencyAmount accountEntryAmount) {
        this.accountEntryAmount = accountEntryAmount;
    }

    /**
     * Sets The accountEntryTypeCode.
     * 
     * @param accountEntryTypeCode
     *            The accountEntryTypeCode to set.
     */
    public void setAccountEntryTypeCode(
            final EntryCategoryCodeList accountEntryTypeCode) {
        this.accountEntryTypeCode = accountEntryTypeCode;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The postedDate.
     * 
     * @param postedDate
     *            The postedDate to set.
     */
    public void setPostedDate(final Date postedDate) {
        this.postedDate = postedDate;
    }

    /**
     * Sets The valueDate.
     * 
     * @param valueDate
     *            The valueDate to set.
     */
    public void setValueDate(final Date valueDate) {
        this.valueDate = valueDate;
    }

    /**
     * Sets The splitInto.
     * 
     * @param splitInto
     *            The splitInto to set.
     */
    public void setSplitInto(final Set<AccountEntry> splitInto) {
        this.splitInto = splitInto;
    }

}
