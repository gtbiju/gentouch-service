package com.cognizant.insurance.agent.domain.commonelements.complexdatatypes;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Used to indicate a type of enumerated value (i.e. code) that is not managed
 * (i.e. enumerated) in the ACORD Information Model.
 * 
 * At this time, ExternalCode has a single child attribute used to model the
 * value of the code. It has been modeled this way in order to allow for
 * additional attribution in future versions of the Information Model
 */

@Entity
public class ExternalCode {

    /** The id. */
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    /**
     * The code value from the external code list.
     */
    @Basic(fetch=FetchType.EAGER)
    private String code;

    /**
     * The URL utilized to identify the source of a code list when there is a
     * need to override the URL (if any) documented in the Information Model.
     * This allows ACORD to define a standard source and allows implementers to
     * define and use their own.
     */
    private String sourceURL;

    /**
     * Gets the code.
     * 
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the sourceURL.
     * 
     * @return Returns the sourceURL.
     */
    public String getSourceURL() {
        return sourceURL;
    }

    /**
     * Sets The code.
     * 
     * @param code
     *            The code to set.
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets The sourceURL.
     * 
     * @param sourceURL
     *            The sourceURL to set.
     */
    public void setSourceURL(final String sourceURL) {
        this.sourceURL = sourceURL;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }
}
