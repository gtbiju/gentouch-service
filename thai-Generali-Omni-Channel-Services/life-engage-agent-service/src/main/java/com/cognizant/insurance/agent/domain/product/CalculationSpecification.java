/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;

/**
 * The Class CalculationSpecification.
 * 
 * @author 304007
 */

@Entity
@Table(name = "CORE_PROD_CALC_SPEC")
public class CalculationSpecification extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8799760309347458010L;

    /** The name. */
    @Column(name = "NAME")
    private String name;

    /** The version. */
    @Column(name = "VERSION")
    private String version;

    /** The input parameters. */
    @Column(name = "INPUT_PARAMETERS", columnDefinition = "varchar(max)")
    private String inputParameters;

    /** The output parameters. */
    @Column(name = "OUTPUT_PARAMETERS", columnDefinition = "varchar(max)")
    private String outputParameters;

    /** The actual rule ref. */
    @Column(name = "ACTUAL_RULE_REF")
    private String actualRuleRef;

    /** The calculation type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "calculationType_ID")
    private CalculationType calculationType;

    /** The parent product. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "parentProduct_id")
    private ProductSpecification parentProductId;
    
    /** Group Name**/
    @Column(name = "GROUP_NAME")
    private String groupName;

    /**
     * @return the name
     */
    public final String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * @return the version
     */
    public final String getVersion() {
        return version;
    }

    /**
     * @param version
     *            the version to set
     */
    public final void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the inputParameters
     */
    public final String getInputParameters() {
        return inputParameters;
    }

    /**
     * @param inputParameters
     *            the inputParameters to set
     */
    public final void setInputParameters(String inputParameters) {
        this.inputParameters = inputParameters;
    }

    /**
     * @return the outputParameters
     */
    public final String getOutputParameters() {
        return outputParameters;
    }

    /**
     * @param outputParameters
     *            the outputParameters to set
     */
    public final void setOutputParameters(String outputParameters) {
        this.outputParameters = outputParameters;
    }

    /**
     * @return the actualRuleRef
     */
    public final String getActualRuleRef() {
        return actualRuleRef;
    }

    /**
     * @param actualRuleRef
     *            the actualRuleRef to set
     */
    public final void setActualRuleRef(String actualRuleRef) {
        this.actualRuleRef = actualRuleRef;
    }

    /**
     * @return the calculationType
     */
    public CalculationType getCalculationType() {
        return calculationType;
    }

    /**
     * @param calculationType
     *            the calculationType to set
     */
    public void setCalculationType(CalculationType calculationType) {
        this.calculationType = calculationType;
    }

    /**
     * @return the parentProductId
     */
    public final ProductSpecification getParentProductId() {
        return parentProductId;
    }

    /**
     * @param parentProductId
     *            the parentProductId to set
     */
    public final void setParentProductId(ProductSpecification parentProductId) {
        this.parentProductId = parentProductId;
    }

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
    
}
