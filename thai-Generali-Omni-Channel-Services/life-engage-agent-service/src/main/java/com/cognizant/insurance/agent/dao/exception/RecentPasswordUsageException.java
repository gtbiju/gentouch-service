package com.cognizant.insurance.agent.dao.exception;

public class RecentPasswordUsageException extends Exception {

	public RecentPasswordUsageException(String message){
		super(message);
	}
}
