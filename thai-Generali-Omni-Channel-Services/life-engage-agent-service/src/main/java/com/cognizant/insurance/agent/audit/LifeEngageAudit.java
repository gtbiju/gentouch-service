/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.agent.audit;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class class LifeEngageAudit.
 *
 * @author 300797
 */
@Entity
public class LifeEngageAudit {
    /** The id. */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    /** The user name. */
    private String userName;
    
    /** The creation dateand time. */
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDateandTime;
    
    /** The source info name. */
    private String sourceInfoName;
    
    /** The transaction id. */
    private String transactionId;
    
    /** The request json. */
    @Lob
    private String requestJson;

    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.EAGER,mappedBy="lifeEngageAudit")
    private Set<LifeEngagePayloadAudit> lifeEngagePayloadAudits;
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }
    
    /**
     * Sets the id.
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Gets the user name.
     *
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }
    
    /**
     * Sets the user name.
     *
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    /**
     * Gets the creation dateand time.
     *
     * @return the creationDateandTime
     */
    public Date getCreationDateandTime() {
        return creationDateandTime;
    }
    
    /**
     * Sets the creation dateand time.
     *
     * @param creationDateandTime the creationDateandTime to set
     */
    public void setCreationDateandTime(Date creationDateandTime) {
        this.creationDateandTime = creationDateandTime;
    }
    
    /**
     * Gets the source info name.
     *
     * @return the sourceInfoName
     */
    public String getSourceInfoName() {
        return sourceInfoName;
    }
    
    /**
     * Sets the source info name.
     *
     * @param sourceInfoName the sourceInfoName to set
     */
    public void setSourceInfoName(String sourceInfoName) {
        this.sourceInfoName = sourceInfoName;
    }
    
    /**
     * Gets the transaction id.
     *
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }
    
    /**
     * Sets the transaction id.
     *
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    
    /**
     * Gets the request json.
     *
     * @return the requestJson
     */
    public String getRequestJson() {
        return requestJson;
    }
    
    /**
     * Sets the request json.
     *
     * @param requestJson the requestJson to set
     */
    public void setRequestJson(String requestJson) {
        this.requestJson = requestJson;
    }

    /**
     * @param lifeEngagePayloadAudits the lifeEngagePayloadAudits to set
     */
    public void setLifeEngagePayloadAudits(Set<LifeEngagePayloadAudit> lifeEngagePayloadAudits) {
        this.lifeEngagePayloadAudits = lifeEngagePayloadAudits;
    }

    /**
     * @return the lifeEngagePayloadAudits
     */
    public Set<LifeEngagePayloadAudit> getLifeEngagePayloadAudits() {
        return lifeEngagePayloadAudits;
    }
  
    
}
