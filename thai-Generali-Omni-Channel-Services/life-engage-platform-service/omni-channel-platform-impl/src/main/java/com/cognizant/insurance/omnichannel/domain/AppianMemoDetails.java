package com.cognizant.insurance.omnichannel.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "APPIAN_MEMO_DETAILS")
public class AppianMemoDetails {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "applicationNumber")
    private String appNumber;
    
    @Column(name = "applicationStatus", columnDefinition="nvarchar(100)")
    private String appStatus;
    
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "memoCode", referencedColumnName = "DESCITEM")
    private UnderWriterMemoDetails memoDetails;
    
    @Column(name = "category", columnDefinition="nvarchar(100)")
    private String category;
    
    @Column(name = "remarks", columnDefinition="nvarchar(1000)")
    private String remarks;
    
    @Column(name = "memoDescription", columnDefinition="NVARCHAR(MAX)")
    private String memoDesc;
    
    @Column(name = "memoDate")
    private String memoDate;
    
    @Column(name = "memoStatus", columnDefinition="nvarchar(200)")
    private String memoStatus;
    
    @Column(name = "memoId", columnDefinition="nvarchar(200)")
    private String memoId;
    
    @Column(name = "submissionDate")
    private String submissionDate;

    @Column(name = "memoReason")
    private String memoReason;
    
    @Column(name = "details", columnDefinition="nvarchar(1000)")
    private String details;
    
    private String policyNumber;

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppNumber() {
        return appNumber;
    }

    public void setAppNumber(String appNumber) {
        this.appNumber = appNumber;
    }

    public String getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(String appStatus) {
        this.appStatus = appStatus;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getMemoDesc() {
        return memoDesc;
    }

    public void setMemoDesc(String memoDesc) {
        this.memoDesc = memoDesc;
    }

    public String getMemoDate() {
        return memoDate;
    }

    public void setMemoDate(String memoDate) {
        this.memoDate = memoDate;
    }

    public String getMemoStatus() {
        return memoStatus;
    }

    public void setMemoStatus(String memoStatus) {
        this.memoStatus = memoStatus;
    }

    /**
     * Gets the memodetails.
     *
     * @return Returns the memodetails.
     */
    public UnderWriterMemoDetails getMemoDetails() {
        return memoDetails;
    }

    /**
     * Sets The memodetails.
     *
     * @param memodetails The memodetails to set.
     */
    public void setMemoDetails(UnderWriterMemoDetails memoDetails) {
        this.memoDetails = memoDetails;
    }

    /**
     * Gets the submissionDate.
     *
     * @return Returns the submissionDate.
     */
    public String getSubmissionDate() {
        return submissionDate;
    }

    /**
     * Sets The submissionDate.
     *
     * @param submissionDate The submissionDate to set.
     */
    public void setSubmissionDate(String submissionDate) {
        this.submissionDate = submissionDate;
    }

    /**
     * Gets the memoReason.
     *
     * @return Returns the memoReason.
     */
    public String getMemoReason() {
        return memoReason;
    }

    /**
     * Sets The memoReason.
     *
     * @param memoReason The memoReason to set.
     */
    public void setMemoReason(String memoReason) {
        this.memoReason = memoReason;
    }

    /**
     * Gets the memoId.
     *
     * @return Returns the memoId.
     */
    public String getMemoId() {
        return memoId;
    }

    /**
     * Sets The memoId.
     *
     * @param memoId The memoId to set.
     */
    public void setMemoId(String memoId) {
        this.memoId = memoId;
    }

    /**
     * Gets the details.
     *
     * @return Returns the details.
     */
    public String getDetails() {
        return details;
    }

    /**
     * Sets The details.
     *
     * @param details The details to set.
     */
    public void setDetails(String details) {
        this.details = details;
    }
    
}
