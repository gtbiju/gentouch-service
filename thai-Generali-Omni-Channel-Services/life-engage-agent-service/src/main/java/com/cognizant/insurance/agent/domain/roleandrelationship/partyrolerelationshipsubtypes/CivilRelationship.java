/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.partyrolerelationshipsubtypes;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.agent.domain.roleandrelationship.PartyRoleRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.Spouse;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CivilStatusCodeList;

/**
 * This represents a relationship between spouses. The spouses may or may not
 * reside in the same household.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class CivilRelationship extends PartyRoleRelationship {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3732866708326016097L;

    /**
     * Civil status of the person.
     * 
     * 
     * 
     */
	@Enumerated
    private CivilStatusCodeList statusCode;

    /**
     * The date associated with the civil status. 
     * 
     * 
     * 
     */
   @Temporal(TemporalType.DATE)
    private Date civilRelationshipSstatusDate;

    /**
     * This relationship link a spouse to a related civil relationship.
     * 
     * In the event a spouse no longer has an association with the other spouse
     * (divorce, widowed, etc.), only one spouse needs to be associated with the
     * relationship.
     * 
     * 
     * 
     * 
     * 
     */
   @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
   @JoinTable(name = "SPOUSE_CIVILRELATIONSHIP",
            joinColumns = { @JoinColumn(name = "CIVILRELATIONSHIP_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "SPOUSE_ID", referencedColumnName = "Id") })
    private Set<Spouse> spouse;

    /**
     * The name of the civil relationship.
     * 
     * e.g. Mr. and Mrs. John and Jane Doe
     * 
     * NOTE: In addition to serving as the name of the civil relationship, this
     * may also have relevance beyond the relationship. For example, a personal
     * auto insurance policy may be issued in the names of a husband and wife
     * and they explicitly request the policy declarations page display the
     * named insured as "Mr. and Mrs. John and Jane Doe" (declarations name).
     * The declarations name is not specific to either person, or any lone
     * party, and is technically not a party name - it is a name utilized for
     * print or a similar function.
     * 
     * 
     * 
     */
    private String name;

    /**
     * Gets the statusCode.
     * 
     * @return Returns the statusCode.
     */
    public CivilStatusCodeList getStatusCode() {
        return statusCode;
    }

    /**
     * Gets the spouse.
     * 
     * @return Returns the spouse.
     */
    public Set<Spouse> getSpouse() {
        return spouse;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The statusCode.
     * 
     * @param statusCode
     *            The statusCode to set.
     */
    public void setStatusCode(final CivilStatusCodeList statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Sets The spouse.
     * 
     * @param spouse
     *            The spouse to set.
     */
    public void setSpouse(final Set<Spouse> spouse) {
        this.spouse = spouse;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets The civilRelationshipSstatusDate.
     *
     * @param civilRelationshipSstatusDate The civilRelationshipSstatusDate to set.
     */
    public void setCivilRelationshipSstatusDate(
            final Date civilRelationshipSstatusDate) {
        this.civilRelationshipSstatusDate = civilRelationshipSstatusDate;
    }

    /**
     * Gets the civilRelationshipSstatusDate.
     *
     * @return Returns the civilRelationshipSstatusDate.
     */
    public Date getCivilRelationshipSstatusDate() {
        return civilRelationshipSstatusDate;
    }
}