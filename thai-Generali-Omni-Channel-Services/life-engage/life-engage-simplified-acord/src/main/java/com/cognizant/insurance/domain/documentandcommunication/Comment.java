/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 325754
 * @version       : 0.1, Oct 26, 2015
 */
package com.cognizant.insurance.domain.documentandcommunication;

import javax.persistence.Entity;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;

/**
 * @author 325754
 * 
 */
@Entity
public class Comment extends InformationModelObject {

    /**
     * 
     */
    private static final long serialVersionUID = -3237822575657216460L;

    private String comment;

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

}
