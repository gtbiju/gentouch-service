package com.cognizant.le.model.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//import org.hibernate.annotations.Filters;
@Entity
@Table(name = "NWS_USER")
public class User {
	@Id
	@Column(name = "UID")
	private String uid;
	@Column(name ="UserMarkedAsDeleted")
	private String userMarkedAsDeleted;
	private String mnylStatus;
	private String mnylMyAgentRoleCode;
	private String mnylAgentEmail;
	private String mnylBossID;
	private String title; 
	private String givenName;
	private String mnylAgentCode;
	
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}



	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getMnylStatus() {
		return mnylStatus;
	}

	public void setMnylStatus(String mnylStatus) {
		this.mnylStatus = mnylStatus;
	}

	public String getMnylMyAgentRoleCode() {
		return mnylMyAgentRoleCode;
	}

	public void setMnylMyAgentRoleCode(String mnylMyAgentRoleCode) {
		this.mnylMyAgentRoleCode = mnylMyAgentRoleCode;
	}

	public String getMnylAgentEmail() {
		return mnylAgentEmail;
	}

	public void setMnylAgentEmail(String mnylAgentEmail) {
		this.mnylAgentEmail = mnylAgentEmail;
	}

	public String getMnylBossID() {
		return mnylBossID;
	}

	public void setMnylBossID(String mnylBossID) {
		this.mnylBossID = mnylBossID;
	}

	public String getUserMarkedAsDeleted() {
		return userMarkedAsDeleted;
	}

	public void setUserMarkedAsDeleted(String userMarkedAsDeleted) {
		this.userMarkedAsDeleted = userMarkedAsDeleted;
	}

	public String getMnylAgentCode() {
		return mnylAgentCode;
	}

	public void setMnylAgentCode(String mnylAgentCode) {
		this.mnylAgentCode = mnylAgentCode;
	}

}
