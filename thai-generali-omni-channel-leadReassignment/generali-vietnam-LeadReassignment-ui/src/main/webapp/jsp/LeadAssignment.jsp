<%@page import="java.util.ArrayList"%>
<%@page import="com.cognizant.le.model.dto.AgentResponseDTO"%>
<%@page import="com.cognizant.le.model.dto.MappedBranch"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@ page import="com.cognizant.le.model.dto.AgentDTO"%>
<%@ page import="java.util.ResourceBundle"%>
<%
    ResourceBundle resource = ResourceBundle.getBundle("resource");
%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<link rel="stylesheet" href="css/css_reset.css" />
<link rel="stylesheet" href="css/styles_workbench.css" />
<title>Lead Re-Assignment</title>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery.fixedheadertable.min.js"></script>

<script>
var json = '${agentDetailsJSON}';
var parsedJson = JSON.parse(json);
var selectedAgentName='';
var selectedBranchName='';
var recordsSelected = [];
var reassignAgentName = '';
var allVal = [];
var selectedRecords = {agentId:""};
var transactionArray = [];
var terminatedAgentsArray=[];
var activeAgentsArray=[];
var thaiYearDifference = 543;

$(document).ready(function() {
	//$('#agentStatus').on('change',function() {
	$('#agent_details_table tbody').empty();
	$('#agentname').show();
	$('#agent_name').show();
	//$('#branchname').empty();
	$('#searchButtonGroup').hide();
	$('#reassignBtn').hide(); 
	//var a=$(this).val();
	var option='';
	//var option1='';
	option = '<option value="'+ "" + '">' + "" + '</option>';
	//option1 = '<option value="'+ "" + '">' + "" + '</option>';
	$('#agent_name').show();
	if(parsedJson.Response.ResponsePayLoad.Transactions.length>0) {
	  transactionArray=  parsedJson.Response.ResponsePayLoad.Transactions;
	 	//if(a=="Active") {
			$.each(transactionArray, function( index, value ) {
				if(value.TransactionData.TerminatedAgentDetails.length>0){
					terminatedAgentsArray = value.TransactionData.TerminatedAgentDetails;
					$.each(terminatedAgentsArray, function( index, value ) {
				if(value.status=="inactive"){
					var agentCode = value.agentCode;
					var agentName = value.agentName; 
					var agentDetails = agentCode + "-" + agentName;
					option += '<option value="'+ agentDetails + '">' + agentDetails + '</option>';
					}
				});
				}
				});
     	/* } else if(a=="Inactive"){
					$.each(transactionArray, function( index, value ) {
					if(value.TransactionData.AgentDetails.status=="Inactive" || value.TransactionData.AgentDetails.status=="Terminated"){
					var agentCode = value.TransactionData.AgentDetails.agentCode;
					var agentName = value.TransactionData.AgentDetails.agentName; 
					var agentDetails = agentCode + "-" + agentName;
					option += '<option value="'+ agentDetails + '">' + agentDetails + '</option>';
				}
		        });
        } */
	}/*  else {
		$('#ErrorPopup').show();
		$('#page_loader').show();
		$('body').css('overflow','hidden');
		$('#ErrorPopup #modalPopupHeader').html("Warning");
		$('#ErrorPopup #errorMessage').html("No Agents Found");
	} */
	$('#agentname').append(option);
	//$('#branchname').append(option1);
//});
	
	
	$('#agentname').on('change',function() {
    $('#agent_details_table tbody').empty();	
	var b=$('#agentname option:selected').text();
	selectedAgentName = b;
	var agent=$('#agentname option:selected').text();
	//var branch=$('#branchname option:selected').text();	
	//var status=$('#agentStatus option:selected').text();
	$('#reassignBtn').hide();
	if(agent!=""){
	$('#searchButtonGroup').show();
	$('#reassignBtn').show();
	} else{
		$('#searchButtonGroup').hide();
	}
		//$('#branchname').empty();
	
 /* var option='';	
	option = '<option value="'+ "" + '">' + "" + '</option>';
	var code = b.split('-');
	 $('#branchName').show();
	$.each(transactionArray, function( index, value ) {
		if(value.TransactionData.AgentDetails.agentCode==code[0]){
			var mappedBranches=value.TransactionData.AgentDetails.mappedBranches;
			$.each(mappedBranches, function( index, value ) {
			var branchCode = value.branchCode;
			var branchName = value.branchName; 
			var branchDetails = branchCode + "-" + branchName;
		option += '<option value="'+ branchDetails + '">' + branchDetails + '</option>';
			});
		}
	});	
       $('#branchname').append(option); */
	});	
	
	/* $('#branchname').on('change',function() {
	    $('#agent_details_table tbody').empty();	
        $('#reassignBtn').hide();		
		selectedBranchName = $('#branchname option:selected').text();		
		var branch=$('#branchname option:selected').text();	
		var agent=$('#agentname option:selected').text();
		var status=$('#agentStatus option:selected').text();
	}); */
	
	//$('#reassignAgentStatus').on('change',function() {
		//var a=$(this).val();
		/* $('#reassignAgentNameWrapper').hide();
		$('#reassignAgentName').empty();
		//var a=$(this).val();
		$('#proceedBtn').attr('disabled','disabled');
		var option='';
		//var option1='';
		option = '<option value="'+ "" + '">' + "" + '</option>';
		//if($('#reassignAgentStatus option:selected').text()!="")
		//{
		$('#reassignAgentNameWrapper').show();
		var agent=$('#reassignAgentName option:selected').text();
		//var status=$('#reassignAgentStatus option:selected').text();
		//if(a=="Active") {
			$.each(transactionArray, function( index, value ) {
				if(value.TransactionData.ActiveAgentDetails.length>0){
					activeAgentsArray = value.TransactionData.ActiveAgentDetails;
					$.each(activeAgentsArray, function( index, value ) {
				if(value.status=="active"){
					var agentCode = value.agentCode;
					var agentName = value.agentName; 
					var agentDetails = agentCode + "-" + agentName;
					option += '<option value="'+ agentDetails + '">' + agentDetails + '</option>';
					}
				});
				}
				}); */
	    //}
	//$('#reassignAgentName').append(option);
	//}
	//});
	$('#reassignAgentName').on('change',function() {	
	var b=$(this).val();
	reassignAgentName = b;	
	var agent=$('#reassignAgentName option:selected').text();
	//var status=$('#reassignAgentStatus option:selected').text();
	if(agent!=""){
		$('#proceedBtn').removeAttr("disabled");
	}
	else{
		$('#proceedBtn').prop('disabled','disabled');
	}
	//$('#reassignBranchName').empty();
	});

	$('#searchBtn').click(function(e){
					$('#loadingmessage').show();
					$('#reassignBtn').prop('disabled','disabled');  
					$('input[name="listingCheckboxSelectAll"]').prop('checked', false); 
					$('#proceedBtn').attr('disabled','disabled');
					//var selBranchName = $('#branchname option:selected').text();
					var selAgentName = $('#agentname option:selected').text();
						$('.lead_assign_table').show();
						$('#reassignBtn').show();
						/* if($('.lead_assign_table').children('.fht-table-wrapper').length ==0){
							$('.agent_details_table').fixedHeaderTable({height:265});
						} */
						$('.lead_assign_table').show();
						$('#agent_details_table tbody').empty();
							$.ajax({
							type: 'post',
							url: 'LeadListing',
							dataType: 'JSON',
							 data: { 
									  agentname: selAgentName,
									  //branchname: selBranchName
									},
							success: function(response) {
								$('#loadingmessage').hide();
								if(response.Response.ResponseInfo.isValidSession == false){
									$('#loadingmessage').hide();
									$('#InvalidSessionErrorPopup').show();
									$('#page_loader').show();
									$('body').css('overflow','hidden');
									$('#InvalidSessionErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									$('#InvalidSessionErrorPopup #invalidSessionErrorMessage').html("<%=new String(resource.getString("sessionTimeOut").getBytes( "ISO-8859-1" ),"UTF-8")%>");
								} else {
								if(response.Response.ResponsePayLoad.Transactions.length>0)
									{
										var transactionArray = response.Response.ResponsePayLoad.Transactions;										
										var followUpDate = "";
										var dateFormat = [];
										var correctedDate = "";
										var date = "";
										var time = "";
										var dateTime = [];
										$.each(transactionArray, function( index, value ) 
											{
												var firstName = value.Key2;
												var lastName = value.Key3;
												var fullName = value.TransactionData.CustomerDetails.leadFullName;
												followUpDate = value.TransactionData.CustomerDetails.creationDateTime;
												var action=value.TransactionData.CustomerDetails.mobileNumber;
												
												if(followUpDate!=""){
													dateTime = followUpDate.split(" ");
													date = dateTime[0];
													time = dateTime[1];
													dateFormat = date.split("/");
													
													dateFormat[2] = parseInt(dateFormat[2]) + thaiYearDifference;
													
													correctedDate = dateFormat[1]+"-"+dateFormat[0]+"-"+dateFormat[2];
													followUpDate = correctedDate+"  "+time;
													
												}
												
												var status = value.TransactionData.CustomerDetails.fnaCompleted;
												var createdByAgent=value.TransactionData.CustomerDetails.salesIllustration;
												var eApps=value.TransactionData.CustomerDetails.eapp;
												var agentId = value.TransactionData.CustomerDetails.identifier;
												var agentCreation = 'N';
												if(value.Key11!="" && value.Key25!="" && value.TransactionData.AllocationType!="") {
                                                   agentCreation = "N";
												} else if(value.Key11!="" && value.Key25!="" && value.TransactionData.AllocationType=="") {
													agentCreation = "N";
												} else if(value.Key11!="" && value.Key25=="" && value.TransactionData.AllocationType!="") {
													agentCreation = "N";
												} else {
												   agentCreation = "Y";
												}
												var row = $("<tr />");
												$("#agent_details_table").append(row); 
												row.append($("<td><input  type="+"checkbox"+" id="+"listingCheckbox"+[index]+" class="+"icheckbox"+" name="+"listingCheckbox"+" value="+ agentId +"></input><label for="+"listingCheckbox"+[index]+"></label></td>"));
												row.append($("<td>" + fullName + "</td>"));
												row.append($("<td>" + correctedDate+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+ time + "</td>"));
												row.append($("<td>" + action + "</td>"));
												row.append($("<td>" + status + "</td>"));
												row.append($("<td>" + createdByAgent + "</td>"));
												row.append($("<td>" + eApps + "</td>"));
												//row.append($("<td>" + agentCreation + "</td>"));
												$("#agent_details_table").append(row); 	
							
											});	         
									} }/*else {
										$('#ErrorPopup').show();
										$('#page_loader').show();
										$('body').css('overflow','hidden');
										$('#ErrorPopup #modalPopupHeader').html("Warning");
										$('#ErrorPopup #errorMessage').html("No leads under this agent");
									}*/
							},
							error: function(data) {
								if(data.responseText!="") {
									$('#loadingmessage').hide();
									$('#InvalidSessionErrorPopup').show();
									$('#page_loader').show();
									$('body').css('overflow','hidden');
									$('#InvalidSessionErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									$('#InvalidSessionErrorPopup #invalidSessionErrorMessage').html("<%=new String(resource.getString("sessionTimeOut").getBytes( "ISO-8859-1" ),"UTF-8")%>");
								} else {
									$('#loadingmessage').hide();
									$('#ErrorPopup').show();
									$('#page_loader').show();
									$('body').css('overflow','hidden');
									$('#ErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									$('#ErrorPopup #errorMessage').html("<%=new String(resource.getString("unexpectedErrorMessage").getBytes( "ISO-8859-1" ),"UTF-8")%>");
								}
							}
						});	
			});
	    $('#reassignBtn').click(function() {
						var dynamic_height = $('body').height();
						$('#page_loader').css('height',dynamic_height).fadeIn(300);
						$('body').css('overflow','hidden');
						$('#reassignPopUp').show();
						$('#reassignAgentName').empty();
						$('#reassignAgentNameWrapper').show();
						
						$("#reassignAgentName option:selected").prop("selected", false);
						$("#reassignAgentName option:nth-child(1)").prop("selected", true);
						
						$('#proceedBtn').attr('disabled','disabled');						
						
						var selAgentName = $('#agentname option:selected').text();
						$.ajax({
								type: 'post',
								url: 'RetrieveActiveAgents',
								dataType: 'JSON',
								 data: { 
										  agentname: selAgentName
										},
								success: function(response) {
									$('#loadingmessage').hide();
									if(response.Response.ResponseInfo.isValidSession == false){
										$('#loadingmessage').hide();
										$('#InvalidSessionErrorPopup').show();
										$('#page_loader').show();
										$('body').css('overflow','hidden');
										$('#InvalidSessionErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
										$('#InvalidSessionErrorPopup #invalidSessionErrorMessage').html("<%=new String(resource.getString("sessionTimeOut").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									} else {
									      if(response.Response.ResponsePayLoad.Transactions.length>0) {
									    	  
											var transactionArray = response.Response.ResponsePayLoad.Transactions;
											
											var option='';													
                                            option = '<option value="'+ "" + '">' + "" + '</option>';											
											
												$.each(transactionArray, function( index, value ) {
													if(value.TransactionData.ActiveAgentDetails.length>0){
														activeAgentsArray = value.TransactionData.ActiveAgentDetails;
														
														$.each(activeAgentsArray, function( index, value ) {
													
														if(value.status=="active") {
															var agentCode = value.agentCode;
															var agentName = value.agentName; 
															var agentDetails = agentCode + "-" + agentName;
															option += '<option value="'+ agentDetails + '">' + agentDetails + '</option>';
														}
													});
													}
													});
										    
										    $('#reassignAgentName').append(option);
											
												         
										} 
									}
								},
								error: function(data) {
									if(data.responseText!="") {
										$('#loadingmessage').hide();
										$('#InvalidSessionErrorPopup').show();
										$('#page_loader').show();
										$('body').css('overflow','hidden');
										$('#InvalidSessionErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
										$('#InvalidSessionErrorPopup #invalidSessionErrorMessage').html("<%=new String(resource.getString("sessionTimeOut").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									} else {
										$('#loadingmessage').hide();
										$('#ErrorPopup').show();
										$('#page_loader').show();
										$('body').css('overflow','hidden');
										$('#ErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
										$('#ErrorPopup #errorMessage').html("<%=new String(resource.getString("unexpectedErrorMessage").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									}
								}
							});
									
	             });
						
						function updateSelectedRecords(){
							var allVal = [];	
							$('input[name="listingCheckbox"]:checked').each(function() {
							   selectedRecords.agentId = $(this).val();
							   allVal.push($(this).val());
							  });
							 		recordsSelected = allVal;
							 		if(recordsSelected.length > 0){
										$('#reassignBtn').removeAttr('disabled'); 
							 		}else{
										$('#reassignBtn').prop('disabled','disabled'); 
							 		}
							}
						
						$(document).on('click','input[name="listingCheckbox"]',function(){
							updateSelectedRecords();
							
						});
						
						$('#cancelBtn').click(function() {
							$('#proceedBtn').attr('disabled','disabled');							
							/* $('#reassignAgentNameWrapper').hide();
							$('#reassignAgentName').empty();
							var option = '<option value="'+ "" + '">' + "" + '</option>';
							$('#reassignAgentName').append(option); */
							
							$('#reassignPopUp').hide();
							$('#page_loader').css("display", "none");
							$('body').css('overflow','auto');
						});
						
	$('#proceedBtn').click(function() {
							$('#loadingmessage').show();
							var requestJSON = JSON.stringify(recordsSelected);
							$('#reassignPopUp').hide();
							$('.lead_assign_table').show();
							$('#reassignBtn').removeAttr("disabled");
							$.ajax({
							type: 'post',
							url: 'LeadReassign',
							dataType: 'JSON',
							 data: { 
									  reassignAgentName: reassignAgentName,
									  recordsSelected:recordsSelected
									},
							success: function(response) {
								$('#loadingmessage').hide();
								if(response.isValidSession == false)
									{
										$('#loadingmessage').hide();
										$('#InvalidSessionErrorPopup').show();
										$('#page_loader').show();
										$('body').css('overflow','hidden');
										$('#InvalidSessionErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
										$('#InvalidSessionErrorPopup #invalidSessionErrorMessage').html("<%=new String(resource.getString("sessionTimeOut").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									} else {
										if(response.StatusData) {
												if(response.StatusData.StatusCode == "100" && response.StatusData.Status == "SUCCESS"){
													$('#Popup').show();
													$('#modalPopupHeader').html("<%=new String(resource.getString("successVT").getBytes( "ISO-8859-1" ),"UTF-8")%>");
													$('#message').html("<%=new String(resource.getString("reassignSuccessMessage").getBytes( "ISO-8859-1" ),"UTF-8")%>");
											     } else{
											    	 	$('#Popup').show();
														$('#modalPopupHeader').html("<%=new String(resource.getString("error").getBytes( "ISO-8859-1" ),"UTF-8")%>");
														$('#message').html("<%=new String(resource.getString("saveFailureMessage").getBytes( "ISO-8859-1" ),"UTF-8")%>");
														
												    }
										    } else{
										    	$('#Popup').show();
												$('#modalPopupHeader').html("<%=new String(resource.getString("error").getBytes( "ISO-8859-1" ),"UTF-8")%>");
												$('#message').html("<%=new String(resource.getString("saveFailureMessage").getBytes( "ISO-8859-1" ),"UTF-8")%>");
										    }	
									}
							},
							error: function(data) {
								if(data.responseText!="") {
									$('#loadingmessage').hide();
									$('#InvalidSessionErrorPopup').show();
									$('#page_loader').show();
									$('body').css('overflow','hidden');
									$('#InvalidSessionErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									$('#InvalidSessionErrorPopup #invalidSessionErrorMessage').html("<%=new String(resource.getString("sessionTimeOut").getBytes( "ISO-8859-1" ),"UTF-8")%>");
								} else {
									$('#loadingmessage').hide();
									$('#ErrorPopup').show();
									$('#page_loader').show();
									$('body').css('overflow','hidden');
									$('#ErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									$('#ErrorPopup #errorMessage').html("<%=new String(resource.getString("unexpectedErrorMessage").getBytes( "ISO-8859-1" ),"UTF-8")%>");
								}								
							}
						});		
							/* $('#page_loader').css("display","none"); */
		});
		$('#successOKbtn').click(function() {
							$('#loadingmessage').show();
							$('#proceedBtn').attr('disabled','disabled');
							$('input[name="listingCheckboxSelectAll"]').prop('checked', false); 
							/* $('#reassignAgentName').empty();
							$('#reassignAgentNameWrapper').hide();
							var option = '<option value="'+ "" + '">' + "" + '</option>';
							$('#reassignAgentName').append(option);
							var selBranchName = $('#branchname option:selected').text(); */
							var selAgentName = $('#agentname option:selected').text();
							recordsSelected = '';
							allVal = '';
							$('#Popup').hide();
							$('#agent_details_table tbody').empty();
							$.ajax({
							type: 'post',
							url: 'LeadListing',
							dataType: 'JSON',
							 data: { 
									  agentname : selAgentName,
									 // branchname : selBranchName
									},
							success: function(response) {
								$('#loadingmessage').hide();
								$('#Popup').hide();
								$('#reassignBtn').prop('disabled','disabled');  
								if(response.Response.ResponseInfo.isValidSession == false){
									$('#loadingmessage').hide();
									$('#InvalidSessionErrorPopup').show();
									$('#page_loader').show();
									$('body').css('overflow','hidden');
									$('#InvalidSessionErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									$('#InvalidSessionErrorPopup #invalidSessionErrorMessage').html("<%=new String(resource.getString("sessionTimeOut").getBytes( "ISO-8859-1" ),"UTF-8")%>");
								} else {
								var followUpDate = "";
								var dateFormat = [];
								var correctedDate = "";
								var date = "";
								var time = "";
								var dateTime = [];
								
								if(response.Response.ResponsePayLoad.Transactions.length>0){
								var transactionArray = response.Response.ResponsePayLoad.Transactions;								
								$.each(transactionArray, function( index, value ) 
									{
						
										var firstName = value.Key2;
										var lastName = value.Key3;
										var fullName = value.TransactionData.CustomerDetails.leadFullName;
										followUpDate = value.TransactionData.CustomerDetails.creationDateTime;
										var action=value.TransactionData.CustomerDetails.mobileNumber;
										if(followUpDate!=""){
											dateTime = followUpDate.split(" ");
											date = dateTime[0];
											time = dateTime[1];
											dateFormat = date.split("/");
											
											dateFormat[2] = parseInt(dateFormat[2]) + thaiYearDifference;
										
											correctedDate = dateFormat[1]+"-"+dateFormat[0]+"-"+dateFormat[2];
											followUpDate = correctedDate+"     "+time;
										}
										
										var status = value.TransactionData.CustomerDetails.fnaCompleted;
										var createdByAgent=value.TransactionData.CustomerDetails.salesIllustration;
										var eApps=value.TransactionData.CustomerDetails.eapp;
										var agentId = value.TransactionData.CustomerDetails.identifier;
										var agentCreation = 'N';
										if(value.Key11!="" && value.Key25!="" && value.TransactionData.AllocationType!="") {
                                            agentCreation = "N";
										} else if(value.Key11!="" && value.Key25!="" && value.TransactionData.AllocationType=="") {
											agentCreation = "N";
										} else if(value.Key11!="" && value.Key25=="" && value.TransactionData.AllocationType!="") {
											agentCreation = "N";
										} else {
											 agentCreation = "Y";
										}
										var row = $("<tr />")
										$("#agent_details_table").append(row); 
										
										row.append($("<td><input  type="+"checkbox"+" id="+"listingCheckbox"+[index]+" class="+"icheckbox"+" name="+"listingCheckbox"+" value="+ agentId +"></input><label for="+"listingCheckbox"+[index]+"></label></td>"));
										row.append($("<td>" + fullName + "</td>"));
										row.append($("<td>" + correctedDate+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+ time + "</td>"));
									    row.append($("<td>" + action + "</td>"));
										row.append($("<td>" + status + "</td>"));
										row.append($("<td>" + createdByAgent + "</td>"));
										row.append($("<td>" + eApps + "</td>"));
										//row.append($("<td>" + agentCreation + "</td>"));
										$("#agent_details_table").append(row); 	
					
									});	  
								} else {
									$('#reassignPopUp').hide();
								}
								}

							},
							error: function(data) {
								if(data.responseText!="") {
									$('#loadingmessage').hide();
									$('#InvalidSessionErrorPopup').show();
									$('#page_loader').show();
									$('body').css('overflow','hidden');
									$('#InvalidSessionErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									$('#InvalidSessionErrorPopup #invalidSessionErrorMessage').html("<%=new String(resource.getString("sessionTimeOut").getBytes( "ISO-8859-1" ),"UTF-8")%>");
								} else {
									$('#loadingmessage').hide();
									$('#reassignPopUp').hide();
									$('#reassignBtn').prop('disabled','disabled'); 
									$('#ErrorPopup').show();
									$('#page_loader').show();
									$('body').css('overflow','hidden');
									$('#ErrorPopup #modalPopupHeader').html("<%=new String(resource.getString("warning").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									$('#ErrorPopup #errorMessage').html("<%=new String(resource.getString("unexpectedErrorMessage").getBytes( "ISO-8859-1" ),"UTF-8")%>");
								}
							}
						});	
						
							$('#page_loader').css("display", "none");
							$('body').css('overflow','auto');
						});
						
					$('#errorOKbtn').click(function() {
							recordsSelected = '';
							allVal = '';
							$('#ErrorPopup').hide();
							$('#agent_details_table tbody').empty();
							//$('#reassignAgentNameWrapper').hide();
							$('#page_loader').css("display", "none");
							$('body').css('overflow','auto');
						});
					$('#invalidSessionOkButton').click(function() {
						document.location.href="<%=request.getContextPath()%>/Logout";
					});
						$(document).on('change','.fht-thead input[name="listingCheckboxSelectAll"]',function () {	
							$('input[name="listingCheckbox"]').prop('checked', $(this).prop("checked"));
								updateSelectedRecords();
						});
				});
</script>
<script type="text/javascript">
	function logoutClick(){
		document.location.href="<%=request.getContextPath()%>/Logout";
	}
</script>
</head>
<body>
<div class="page-content landing-page">
	<div class="logo_header">
		<img src="img/logo-main.png" alt="logo-main" class="mainLogo">
		<div class="lead_assign_heading"><%=new String(resource.getString("leadReassignment").getBytes( "ISO-8859-1" ),"UTF-8") %></div> <img
			src="img/log-out.png" onClick="logoutClick()" alt="logo-main"
			class="mainendLogo"> 

		<!-- <span
			class="header_title">LifeEngage Configurator</span>-->
	</div>
	<ul class="menu-bar">
		<li><a href="#" class="">Upload</a>
		</li>
		<li class="active"><a href="#" class=""><%=new String(resource.getString("leadReassignment").getBytes( "ISO-8859-1" ),"UTF-8") %></a>
		</li>
	</ul>
	<div class="bg_white">
		<span><%=new String(resource.getString("leadReassignment").getBytes( "ISO-8859-1" ),"UTF-8") %><span>
	</div>
	
	<div class="lead_assign_div">
	
		<div class="field-wrapper">
			<div class="clearfix">
			<%-- <div class="agent_status">
					<span><%=new String(resource.getString("agentStatus").getBytes( "ISO-8859-1" ),"UTF-8") %><span class="mandatory">*</span></span>
					 <div class="select-arrow">
						<select id="agentStatus">
							<option selected></option>
							<option>Active</option>
							<option>Inactive</option>
						</select> 
					</div>
				</div> --%>
				<div class="agent_name" id="agent_name">
					<span><%=new String(resource.getString("agentName").getBytes( "ISO-8859-1" ),"UTF-8") %><span class="mandatory">*</span></span>
					<div class="select-arrow">
					    <select id="agentname" name="agentname">
					    </select>
					</div>
				</div>
			</div>
			<%-- <div class="clearfix">
			  	<div id="branchName" class="branch_name pull-left">
					<span><%=new String(resource.getString("branchName").getBytes( "ISO-8859-1" ),"UTF-8") %></span> 
					<div class="select-arrow">
						<select id="branchname" name="branchname">
						</select>
					</div>
				</div> --%>

				<div id="searchButtonGroup" class="branch_name">
					<button class="red-btn " id="searchBtn" ><%=new String(resource.getString("search").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
					<button class="white-btn" type="button" id="reassignBtn" disabled><%=new String(resource.getString("reAssign").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
				</div>
			<!-- </div> -->
		</div>

		<div class="lead_assign_table displayNone">
			<table class="agent_details_table" id="agent_details_table" name="agent_details_table">
				<thead>
					<tr id="agent_details_topic">
						<th class="center-align"><input type="checkbox" id="listingCheckboxSelectAll" class="icheckbox" name="listingCheckboxSelectAll"/><label for="listingCheckboxSelectAll"></label></th>						
						<th><%=new String(resource.getString("leadName").getBytes( "ISO-8859-1" ),"UTF-8") %></th>
						<th><%=new String(resource.getString("followUpDate").getBytes( "ISO-8859-1" ),"UTF-8") %></th>
						<th><%=new String(resource.getString("action").getBytes( "ISO-8859-1" ),"UTF-8") %></th>
						<th><%=new String(resource.getString("status").getBytes( "ISO-8859-1" ),"UTF-8") %></th>
						<th><%=new String(resource.getString("createdByAgent").getBytes( "ISO-8859-1" ),"UTF-8") %></th>
						<th><%=new String(resource.getString("eApps").getBytes( "ISO-8859-1" ),"UTF-8") %></th>
					</tr>
				</thead>
				<tbody>
			    </tbody>
			</table>
		</div>
	</div>


	<div class="modal_pop_up agtupd_content_pop_up displayNone"
		id="reassignPopUp">
		<p class="modal_pop_up_header"><%=new String(resource.getString("reAssigneeDetails").getBytes( "ISO-8859-1" ),"UTF-8") %></p>
		<div class="blankContainerInner">
			<div>
				<div class="re_assignee_details">
					<%-- <div class="re_assignee_status_part">
						<span><%=new String(resource.getString("agentStatus").getBytes( "ISO-8859-1" ),"UTF-8") %><span class="mandatory">*</span>
						</span>
						<div class="select-arrow">				
							<select id="reassignAgentStatus">
							<option selected></option>
							<option>Active</option>
							</select>
						</div>
					</div> --%>
					<div class="re_assignee_agent_part" id="reassignAgentNameWrapper">
						<span><%=new String(resource.getString("agentName").getBytes( "ISO-8859-1" ),"UTF-8") %><span class="mandatory">*</span>
						</span>
						<div class="select-arrow reassignAgent">	
						<select id="reassignAgentName">
									<option selected></option>
						</select>
						</div>
					</div>
				</div>
			</div>
			<div class="btn-wrapper top-btm-mrg-10">
				<button class="pop_up_upload_btn ok-btn cancelBtn" id="cancelBtn"><%=new String(resource.getString("cancel").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
				<button class="pop_up_upload_btn ok-btn" id="proceedBtn" disabled><%=new String(resource.getString("proceed").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
			</div>
		</div>
	</div>

	<div class="modal_pop_up agtupd_content_pop_up error_popup" id="Popup">
		<div class="pop_up_img">
		    <img src="img/infored.png" width="25">
		</div>
			<div class="center-align msgfont">
				<span id="message"></span>
			</div>
			<div class="btn-wrapper closeBtn">
				<button class="pop_up_upload_btn ok-btn close_btn" id="successOKbtn"><%=new String(resource.getString("ok").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
			</div>
	</div>
	
	<div class="modal_pop_up agtupd_content_pop_up error_popup" id="ErrorPopup">
		<div class="pop_up_img">
		    <img src="img/infored.png" width="25">
		</div>		
			<div class="center-align msgfont">
				<span id="errorMessage"></span>
			</div>
			<div class="btn-wrapper closeBtn">
				<button class="pop_up_upload_btn ok-btn close_btn" id="errorOKbtn"><%=new String(resource.getString("ok").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
			</div>		
	</div>
	
	<div class="modal_pop_up agtupd_content_pop_up error_popup" id="InvalidSessionErrorPopup">
		<div class="pop_up_img">
		    <img src="img/infored.png" width="25">
		</div>
			<div class="center-align msgfont">
				<span id="invalidSessionErrorMessage"></span>
			</div>
			<div class="btn-wrapper closeBtn">
				<button class="pop_up_upload_btn ok-btn close_btn" id="invalidSessionOkButton"><%=new String(resource.getString("ok").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
			</div>
	</div>
	
	
	<div id="loadingmessage" style="display:none;">
		<div class="loaderWrapper">
				<img src="img/brspin.gif"/>		
				<span><%=new String(resource.getString("showLoadingMessage").getBytes( "ISO-8859-1" ),"UTF-8") %></span>
		</div>
	</div>
	
	<div id="page_loader"></div>
	
	<div class="footer_part">
		<p class="copyright_label">©2017 Generali Life Assurance (Thailand) Plc</p>
		<span class="version">Version No:1.1</span>
	</div>
</div>
</body>
</html>