package com.cognizant.insurance.omnichannel.component;

import java.text.ParseException;

import java.util.List;
import java.util.Map;
import com.cognizant.insurance.component.LifeEngageEmailComponent;
import com.cognizant.insurance.core.exception.BusinessException;


public interface GeneraliEmailComponent extends LifeEngageEmailComponent {
	
	 public String triggerBulkUploadEmail(String agentID,String emailId,String type,Map<String, String> excelDataList) throws ParseException, BusinessException;
	 public void sendFailedSPAJMail(List<String> SPAJs);
	 public void sendAgentReport(String fileName, String mode);
	
}
