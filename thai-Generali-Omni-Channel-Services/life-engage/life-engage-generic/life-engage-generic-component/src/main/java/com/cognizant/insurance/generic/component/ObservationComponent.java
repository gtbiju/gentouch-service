/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.component;

import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;

/**
 * The Interface interface ObservationComponent.
 *
 * @author 300797
 */
public interface ObservationComponent {

    /**
     * Save Observation.
     *
     * @param <T> the generic type
     * @param request the request
     * @return the response
     */
    <T> Response<Object> saveObservations(Request<T> request);
   
}
