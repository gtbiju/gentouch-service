/**
 * 
 */
package com.cognizant.insurance.domain.finance.financecodelists;

/**
 * Defines the possible billing options applicable for a contract (Agency,
 * Direct ...).
 * 
 * @author 301350
 * 
 * 
 */
public enum BillingOptionCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Agency,
    /**
     * 
     * 
     * 
     * 
     */
    Direct
}
