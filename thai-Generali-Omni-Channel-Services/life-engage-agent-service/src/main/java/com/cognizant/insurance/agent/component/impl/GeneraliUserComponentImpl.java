package com.cognizant.insurance.agent.component.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.agent.component.GeneraliUserComponent;
import com.cognizant.insurance.agent.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.constants.ErrorConstants;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.core.exception.SystemException;
import com.cognizant.insurance.agent.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.agent.dao.exception.InputValidationException;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.repository.AuditRepository;
import com.cognizant.insurance.agent.repository.LMSRepository;
import com.cognizant.insurance.agent.repository.UserRepository;
import com.cognizant.insurance.agent.request.vo.RequestInfo;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.searchcriteria.SearchCountResult;
import com.cognizant.insurance.agent.searchcriteria.SearchCriteria;
import com.cognizant.insurance.agent.searchcriteria.SearchCriteriaResponse;
import com.cognizant.insurance.generali.security.entity.PasswordHistory;

@Component
public class GeneraliUserComponentImpl implements GeneraliUserComponent {
	
	private static final String LE_TXN_MANAGER = "le_txn_manager";
	
	   /** The Constant FNA. */
    private static final String FNA = "FNA";

    /** The Constant LMS. */
    private static final String LMS = "LMS";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";
	
	/** The repository. */
    @Autowired
    private UserRepository userRepository;
    
    /** The LMS Repository*/
    @Autowired
    private LMSRepository lmsRepository;
    
    /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;
   
    /** The validate customer. */
    @Autowired
    @Qualifier("registerUser")
    private LifeEngageSmooksHolder registerUser;
   
    //** The validate user. *//*
    @Autowired
    @Qualifier("createUser")
    private LifeEngageSmooksHolder createUser;
    
    /** The retrieve by count holder. */
    @Autowired
    @Qualifier("retrieveByCountMapping")
    private LifeEngageSmooksHolder retrieveByCountHolder;
    
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;

    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliUserComponentImpl.class);

    @Override
	@Transactional(value = LE_TXN_MANAGER)
	public String register(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException {
    			
        Transactions userTransactions = new Transactions();
              
        return validateRegistrationDetails(requestInfo, jsonObject, userTransactions);         
	}
    
    @Override
   	@Transactional(value = LE_TXN_MANAGER)
   	public String forgetPassword(RequestInfo requestInfo, JSONObject jsonObject) {
       			
           Transactions userTransactions = new Transactions();
          
           return validateRegistrationDetails(requestInfo, jsonObject, userTransactions);         
   	}

	private String validateRegistrationDetails(RequestInfo requestInfo, JSONObject jsonObject,
			                              Transactions userTransactions) {
		
		String agentCode = jsonObject.getString(Constants.KEY1);
        String licenseNumber = jsonObject.getString(Constants.KEY2);
        String identificationId = jsonObject.getString(Constants.KEY3);        
        String agentDob = jsonObject.getString(Constants.KEY4);
        String isReset = jsonObject.getString(Constants.KEY8);
        
        userTransactions.setKey1(agentCode);
        userTransactions.setKey2(licenseNumber);
        userTransactions.setKey3(identificationId);
        userTransactions.setKey4(agentDob);
        userTransactions.setKey8(isReset);
        
        
        Transactions transactions = userRepository.validateAgentProfile(userTransactions);        
        //auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), jsonObject.toString());
        return getResponse(userTransactions, transactions);
	}
	
	private String getResponse(Transactions userTransactions, Transactions transactions) {		

    	String response = StringUtils.EMPTY;
		String responseMsg = StringUtils.EMPTY;
		String responseCode = StringUtils.EMPTY;
		String status = StringUtils.EMPTY;
		
		ArrayList<String> responses = new ArrayList<String>();
		
		    //Common Error Code (201) for AgentCode,LicenseNo,IdentificationId & DOB validations
			status = Constants.FAILURE;
			responseCode = Constants.INVALID_REGISTRATION_ERROR_CODE;
			
			if (transactions.getAgent() == null) {				
				responseMsg = Constants.INVALID_AGENT_CODE_ERROR_MSG;			
				
			} else {	
				
				responses = validateAgentRegistrationDetails(transactions.getAgent(), responses, userTransactions);
				
				StringBuilder sb = new StringBuilder();
				
				for (String fRes : responses) {
				    sb.append(fRes);
				    sb.append("-");
				}
				
				responseMsg=sb.toString();		
				
				if(responseMsg.isEmpty()) {
					responseMsg = validateAgentLicenseStatus(transactions.getAgent(), responseMsg);
				}
			} 
			
			Response<GeneraliTHUser> user = userRepository.validateLoggedInUser(userTransactions);
			
			if(user.getType() == null && !userTransactions.getKey8().isEmpty()
					                  && userTransactions.getKey8().equals(Constants.RESET)) {		    
			     responseMsg = Constants.USER_NOT_FOUND;
			}
						
			if(responseMsg.isEmpty()) {
				status = Constants.SUCCESS;
				responseCode = Constants.SUCCESS_CODE;
		    }
			
		LifeEngageComponentHelper.generateResponseStatusObj(transactions, status, responseMsg, responseCode);
		response = registerUser.parseBO(transactions);
		return response;	
	}	
	
	private ArrayList<String> validateAgentRegistrationDetails(GeneraliAgent agent, ArrayList<String> responses, Transactions userTransactions) {
		
		if (!agent.getLicenseNumber().equals(userTransactions.getKey2())) {
			responses.add(Constants.INVALID_LICENSE_NUMBER_ERROR_MSG);
		} 
		
		if (!agent.getIdentificationId().equals(userTransactions.getKey3())) {
			responses.add(Constants.INVALID_IDENTIFICATION_ID_ERROR_MSG);
		} 		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date dob = null;
		
		try {
			dob = sdf.parse(userTransactions.getKey4());
			
		} catch (ParseException e) {			
			e.printStackTrace();
		}			
		
		if (!agent.getAgentDob().equals(dateFormat.format(dob))) {
			responses.add(Constants.INVALID_AGENT_DOB_ERROR_MSG);
		}
		
		return responses;
	}
	
	private String validateAgentLicenseStatus(GeneraliAgent agent, String responseMsg) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

		String date = sdf.format(new Date());
		
		if (agent.getLicenseExpireDate().compareTo(date) < 0) {
			
			responseMsg = Constants.LICENSE_DATE_EXPIRED_ERROR_MSG;
		   
		} else if(!agent.getAgentStatus().equals(Constants.AGENT_STATUS_INFORCE)) {
			
			responseMsg = Constants.AGENT_NOTACTIVE_EXPIRED_ERROR_MSG;			
		}
		return responseMsg;
	}
	
	@Override
	@Transactional(value = LE_TXN_MANAGER)
	public String createPassword(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException {
						
		   String response = StringUtils.EMPTY;
	       String responseMsg = StringUtils.EMPTY;
	       String responseCode = Constants.FAILURE_CODE;
	       String status = Constants.FAILURE;
	       
	       Transactions transactions = new Transactions();
	       StandardPasswordEncoder encoder = new StandardPasswordEncoder();
	       
	       String agentCode = jsonObject.getString(Constants.KEY1);
	       String password = jsonObject.getString(Constants.KEY5);
	       String email = jsonObject.getString(Constants.KEY6)!=null ? jsonObject.getString(Constants.KEY6) : StringUtils.EMPTY;     
	       
	         if (agentCode!=null && password!=null) {
	        	 
	        	transactions.setKey1(agentCode);
	   	        transactions.setKey5(encoder.encode(password));
	   	        transactions.setKey6(email);
	   	        
	   	        Response<GeneraliTHUser> user = userRepository.validateLoggedInUser(transactions);
	   	     
	   	        if(user.getType() != null) {
	   	    	   resetPassword(requestInfo, jsonObject);
	   	    	   
			    } else {	   	      
	   	           userRepository.createPassword(transactions);
			    }
	   	        
		        status = Constants.SUCCESS;
		        responseCode = Constants.SUCCESS_CODE;
	          
		        LifeEngageComponentHelper.generateResponseStatusObj(transactions, status, responseMsg, responseCode);
	            response = createUser.parseBO(transactions);
	           }
	      return response;
	  }
	
	@Override
	@Transactional(value = LE_TXN_MANAGER)
	public String resetPassword(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException {
						
		   String response = StringUtils.EMPTY;
	       String responseMsg = StringUtils.EMPTY;
	       String responseCode = Constants.FAILURE_CODE;
	       String status = Constants.FAILURE;
	       
	       Transactions transactions = new Transactions();
	       StandardPasswordEncoder encoder = new StandardPasswordEncoder();
	       
	       String agentCode = jsonObject.getString(Constants.KEY1);
	       String password = jsonObject.getString(Constants.KEY5);
	       String email = jsonObject.getString(Constants.KEY6)!=null ? jsonObject.getString(Constants.KEY6) : StringUtils.EMPTY;     
	       
	         if (agentCode!=null && password!=null) {	        	
	        	 
	        	transactions.setKey1(agentCode);
	   	        transactions.setKey5(encoder.encode(password));
	   	        transactions.setKey6(email);
	   	      
	   	        Response<GeneraliTHUser> user = userRepository.validateLoggedInUser(transactions);
	   	        
	   	        if(user.getType() != null) {
	   	        
	   	        Response<List<PasswordHistory>> passwordResponse = userRepository.isThisRecentPassword(user.getType());
	   	        	   	        	 
	 			   if(!isPasswordMatched(encoder, passwordResponse, password)) {
	 				   transactions.setGeneraliThUser(user.getType());
	 				   userRepository.resetPassword(transactions);
	 				   status = Constants.SUCCESS;
	 			       responseCode = Constants.SUCCESS_CODE;
	 			   } 
	   	        }
	   	        
		        LifeEngageComponentHelper.generateResponseStatusObj(transactions, status, responseMsg, responseCode);
		       //auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), jsonObject.toString(), lifeEngageAudit);
	            response = createUser.parseBO(transactions);	           
	         }
	        return response;
	    }
	
	private boolean isPasswordMatched(StandardPasswordEncoder encoder, Response<List<PasswordHistory>> passwordResponse, String currentPassword) {
		
		for (PasswordHistory history : passwordResponse.getType()) {
			if(encoder.matches(currentPassword, history.getPassword())) {
				return true;
			}
		}
		return false;		
	}
	
	@Override
	@Transactional(value = LE_TXN_MANAGER)
	public String retrieveLeads(RequestInfo requestInfo,JSONArray jsonRqArray)throws InputValidationException {
		
			String response = StringUtils.EMPTY;
			String responseMsg = StringUtils.EMPTY;
			String responseCode = Constants.INVALID_REGISTRATION_ERROR_CODE;
			String status = Constants.FAILURE;
			
			Transactions transactions = new Transactions();
	        SearchCriteriaResponse searchCriteriaResponse = new SearchCriteriaResponse();
			
			try{
				if (jsonRqArray != null) {
	                for (int i = 0; i < jsonRqArray.length(); i++) {                    
	                    final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);
	                    JSONObject jsonTransactionData = null;
	                    SearchCriteria searchCriteria = new SearchCriteria();
	                    String agentId = jsonTransactionsObj.getString(Constants.KEY11);
	                    if (agentId != null && !("").equals(agentId)) {
	                        ArrayList<String> modesList = new ArrayList<String>();
	                        if (!(jsonTransactionsObj.isNull(Constants.TRANSACTION_DATA))) {
	                            jsonTransactionData = jsonTransactionsObj.getJSONObject(Constants.TRANSACTION_DATA);
	                            final JSONObject searchCriteriaRqJson = jsonTransactionData.getJSONObject(Constants.SEARCHCRITERIAREQUEST);
	                            JSONArray jsonModesArray = searchCriteriaRqJson.getJSONArray(Constants.MODES);
	                            for (int j = 0; j < jsonModesArray.length(); j++) {
	                                modesList.add((String) jsonModesArray.get(j));
	                            }
	                            String command = searchCriteriaRqJson.getString(Constants.COMMAND);
	                            searchCriteria.setCommand(command);
	                            searchCriteria.setValue(searchCriteriaRqJson.getString(Constants.VALUE));
	                            searchCriteria.setAgentId(agentId);
	                            searchCriteria.setModes(modesList);
	                            if ((Constants.LANDING_DASHBOARD_COUNT).equals(command)) {
	                                searchCriteriaResponse = retrieveByCount(requestInfo, searchCriteria);
	                            		}
	                        		}
	                        	}else {
	                        	throw new SystemException(ErrorConstants.LE_SYNC_ERR_105, Constants.AGENTID_MISSING);
	                            /*throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, Constants.AGENTID_MISSING);*/
	                        	}
	                		}
	                	}
					}             
			catch (SystemException e) {
	            LOGGER.error("SystemException", e);
	            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
	                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),ErrorConstants.LE_SYNC_ERR_105);
	            }else{
	                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),e.getErrorCode());
	            	 }
	        } catch (Exception e) {
	            LOGGER.error("Unable to retrieve count by each module :" + e.getMessage());
	            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
	                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
	        }		
			finally {
	            response = retrieveByCountHolder.parseBO(transactions);
	        }
			return response;
		}
	
	public SearchCriteriaResponse retrieveByCount(final RequestInfo requestInfo, final SearchCriteria searchCriteria) throws BusinessException, ParseException {
				LOGGER.trace("Inside LifeEngageSearchCriteriaComponentImpl retrieveByCount : requestInfo" + requestInfo);
				SearchCountResult searchCountResult = null;
				SearchCriteriaResponse searchCriteriaResponse = new SearchCriteriaResponse();
				List<SearchCountResult> searchCountResultList = new ArrayList<SearchCountResult>();
				ArrayList<String> modeList = searchCriteria.getModes();

				if (modeList.size() > 0) {
					for (String mode : modeList) {
						/*if (E_APP.equals(mode)) {
							searchCriteria.setType(mode);
							searchCountResult = eAppRepository.retrieveByCountEApp(requestInfo, searchCriteria);
							searchCountResultList.add(searchCountResult);
						} else if (ILLUSTRATION.equals(mode)) {
							searchCriteria.setType(mode);
							searchCountResult = illustrationRepository.retrieveByCountIllustration(requestInfo, searchCriteria);
							searchCountResultList.add(searchCountResult);
						} else if (FNA.equals(mode)) {
							searchCriteria.setType(mode);
							searchCountResult = fnaRepository.retrieveByCountFNA(requestInfo, searchCriteria);
							searchCountResultList.add(searchCountResult);
						} else*/ if (LMS.equals(mode)) {
							searchCriteria.setType(mode);
							searchCountResult = lmsRepository.retrieveByCountLMS(requestInfo, searchCriteria);
							searchCountResultList.add(searchCountResult);
						}
					}
				}
				searchCriteriaResponse.setModeResults(searchCountResultList);
				return searchCriteriaResponse;
	}
	
	/**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        	}
        return message;
    	}
 }
