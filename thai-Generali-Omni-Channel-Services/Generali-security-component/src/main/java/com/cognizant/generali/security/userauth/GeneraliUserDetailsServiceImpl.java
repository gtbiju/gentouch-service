package com.cognizant.generali.security.userauth;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.cognizant.generali.security.constants.GeneraliExceptionCodes;
import com.cognizant.generali.security.constants.GeneraliExceptionMessages;
import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.generali.security.exception.AccountLockedException;
import com.cognizant.generali.security.exception.PasswordExpiredException;
import com.cognizant.le.security.component.LifeEngageLoginComponent;
import com.cognizant.le.security.constants.ExceptionCodes;
import com.cognizant.le.security.constants.ExceptionMessages;
import com.cognizant.le.security.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.le.security.entity.Role;
import com.cognizant.le.security.entity.User;
import com.cognizant.le.security.exception.DaoException;
import com.cognizant.le.security.exception.UserInactiveException;
import com.cognizant.le.security.exception.UserNotFoundException;
import com.cognizant.le.security.request.Request;
import com.cognizant.le.security.request.impl.JPARequestImpl;
import com.cognizant.le.security.response.Response;
import com.cognizant.le.security.userauth.LifeEngageUserDetailsImpl;


public class GeneraliUserDetailsServiceImpl implements UserDetailsService {
	/** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliUserDetailsServiceImpl.class);

  
	 /** The login component. */
    @Autowired
    private LifeEngageLoginComponent loginComponent;
    
 
	 /** The entity manager. */
    @PersistenceContext(unitName="LE_Platform")
    private EntityManager entityManager;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LOGGER.info("METHOD ENTRY : loadUserByUsername "+ username);
		
		LifeEngageUserDetailsImpl userDetails = new LifeEngageUserDetailsImpl();

		try {
			final Request<String> request =
	            new JPARequestImpl<String>(entityManager,ContextTypeCodeList.LOGIN,null);
	    	request.setType(username);
	    	
			Response<User> response = loginComponent.getUserDetails(request);			
			User user = response.getType();
			if(user == null) {
			    throw new UserNotFoundException(ExceptionCodes.ERROR_471, ExceptionMessages.USER_NOT_FOUND);
			} else {
			    if (null == user.getIsActive() || !user.getIsActive()) {
			        throw new UserInactiveException(ExceptionCodes.ERROR_472, ExceptionMessages.INACTIVE_USER); 
			    } if (user instanceof GeneraliTHUser){
			    	GeneraliTHUser generaliThUser = (GeneraliTHUser)user;
			    	if(generaliThUser.isPasswordLocked()){
			    		throw new AccountLockedException(GeneraliExceptionCodes.ERROR_502,GeneraliExceptionMessages.ACCOUNT_LOCKED);
			    	} else if (generaliThUser.getPasswordExpiryDate().before(new Date())){
			    		throw new PasswordExpiredException(GeneraliExceptionCodes.ERROR_475,GeneraliExceptionMessages.PASSWORD_EXPIRED);
			    	}
			    }
			    
			    userDetails.setUsername(user.getUserId());
			    userDetails.setPassword(user.getPassword());
			    List<SimpleGrantedAuthority> roles = new ArrayList<SimpleGrantedAuthority>();
			    if (user.getRoles() != null) {
			        for (Role role : user.getRoles()) {
			            roles.add(new SimpleGrantedAuthority(role.getRoleName()));
			        }
			    }
			    userDetails.setAuthorities(roles);
			}
		} catch (DaoException e) {
		    LOGGER.error("Failed to load userBy username ", e);
		}       
        LOGGER.debug("METHOD EXIT : loadUserByUsername ");
        return userDetails;
    }
}
