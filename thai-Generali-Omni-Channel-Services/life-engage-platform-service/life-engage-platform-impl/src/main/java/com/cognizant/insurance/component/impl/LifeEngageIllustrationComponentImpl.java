/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 21, 2013
 */
package com.cognizant.insurance.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.icr.rule.bom.RuleCriteria;
import com.cognizant.insurance.audit.IllustrationResponsePayLoad;
import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngageIllustrationComponent;
import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.IllustrationRepository;
import com.cognizant.insurance.component.repository.RulesRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;

// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageIllustrationComponentImpl.
 * 
 * @author 300797
 */
@Service("lifeEngageIllustrationComponent")
public class LifeEngageIllustrationComponentImpl implements LifeEngageIllustrationComponent {

    /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;

    /** The save illustration holder. */
    @Autowired
    @Qualifier("illustrationSaveMapping")
    private LifeEngageSmooksHolder saveIllustrationHolder;

    /** The save illustration holder. */
    @Autowired
    @Qualifier("illustrationRetrieveMapping")
    private LifeEngageSmooksHolder retrieveIllustrationHolder;

    /** The retrieve illustration list holder. */
    @Autowired
    @Qualifier("illustrationRetrieveListMapping")
    private LifeEngageSmooksHolder retrieveIllustrationListHolder;

    /** The generate illustration holder. */
    @Autowired
    @Qualifier("generateIllustrationMapping")
    private LifeEngageSmooksHolder generateIllustrationHolder;

    /** The Constant SUCCESS_MESSAGE_SAVE. */
    private static final String SUCCESS_MESSAGE_SAVE = "Illustration Saved";

    /** The Constant SUCCESS_MESSAGE_UPDATE. */
    private static final String SUCCESS_MESSAGE_UPDATE = "Illustration Updated";

    /** The Constant REJECTED_MESSAGE. */
    private static final String REJECTED_MESSAGE = "Illustration Rejected";

    /** The Constant ILLUSTRATION. */
    private static final String ILLUSTRATION = "Illustration";

    /** The illustration repository. */
    @Autowired
    private IllustrationRepository illustrationRepository;

    /** The rule repository. */
    @Autowired
    private RulesRepository rulesRepository;

    /** The id generator. */
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;

    /** The client id. */
    @Value("${le.platform.service.illustration.clientId}")
    private String clientId;

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageIllustrationComponentImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageIllustrationComponent#saveIllustration(java.lang.String,
     * com.cognizant.insurance.request.vo.RequestInfo, com.cognizant.insurance.audit.LifeEngageAudit)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String saveIllustration(final String json, String jsonIllustrationOutput, final RequestInfo requestInfo,
            final LifeEngageAudit lifeEngageAudit) {
        String eAppResponse = null;
        Transactions transactions = new Transactions();
        String successMessage = "";
        String offlineIdentifier = null;
        Boolean isDeleteRequest = Boolean.FALSE;

        try {
            LifeEngageComponentHelper.parseTrasactionDetails(json, transactions, false);
            // Store to a temp variable to set it back to response.
            offlineIdentifier = transactions.getOfflineIdentifier();
            // Do not process eApp data if already done
            if (auditRepository.isTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                throwSystemException(true, ErrorConstants.LE_SYNC_ERR_102, Constants.PROCESSED_TRANSACTION);
            }
            if (transactions.getStatus() != null
                    && transactions.getStatus().equals(AgreementStatusCodeList.Cancelled.toString())) {
                isDeleteRequest = Boolean.TRUE;
            }
            transactions = (Transactions) saveIllustrationHolder.parseJson(json);
            if (isDeleteRequest) {
                transactions.setStatus(AgreementStatusCodeList.Cancelled.toString());
            }
         // Fetching agreement By transTracking Id - Fix for duplicate Illustration when key3 not returned properly
            InsuranceAgreement agreement = null;
            if (null != transactions.getTransTrackingId()) {
                agreement =
                    illustrationRepository.getIllustrationAgreementByTransTrackingId(transactions,
                                 requestInfo.getTransactionId());
            }
            if (agreement != null  || StringUtils.isNotEmpty(transactions.getIllustrationId())) {
                if(agreement != null){
                    transactions.setIllustrationId(String.valueOf(agreement.getIdentifier()));
                }
                transactions.setMode(Constants.STATUS_UPDATE);
                successMessage = SUCCESS_MESSAGE_UPDATE;
            } else {
                transactions.setIllustrationId(String.valueOf(idGenerator.generate(ILLUSTRATION)));
                transactions.setMode(Constants.STATUS_SAVE);
                successMessage = SUCCESS_MESSAGE_SAVE;
            }

            onBeforeSave(transactions, requestInfo, jsonIllustrationOutput);

            final String proposalstatus = illustrationRepository.saveIllustration(transactions, requestInfo, agreement);

            if (Constants.REJECTED.equals(proposalstatus)) {
                successMessage = REJECTED_MESSAGE;
            }
            LifeEngageComponentHelper.createResponseStatus(transactions, proposalstatus, successMessage, null);

            transactions.setOfflineIdentifier(offlineIdentifier);

            auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit,"");
            eAppResponse = saveIllustrationHolder.parseBO(transactions);
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_104);
            transactions.setOfflineIdentifier(offlineIdentifier);
            eAppResponse = saveIllustrationHolder.parseBO(transactions);

        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_103);
                transactions.setOfflineIdentifier(offlineIdentifier);
            }

            else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
                transactions.setOfflineIdentifier(offlineIdentifier);
            }
            eAppResponse = saveIllustrationHolder.parseBO(transactions);
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
                transactions.setOfflineIdentifier(offlineIdentifier);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
                transactions.setOfflineIdentifier(offlineIdentifier);
            }
            eAppResponse = saveIllustrationHolder.parseBO(transactions);

        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_100);
            transactions.setOfflineIdentifier(offlineIdentifier);
            eAppResponse = saveIllustrationHolder.parseBO(transactions);

        }

        return eAppResponse;
    }

    /**
     * On before save.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param jsonIllustrationOutput
     *            the json illustration output
     * @throws ParseException
     *             the parse exception
     */
    protected void onBeforeSave(Transactions transactions, RequestInfo requestInfo, String jsonIllustrationOutput)
            throws ParseException {
        /* May illustrationOutput can be null while saving before showing the output */
        IllustrationResponsePayLoad illustrationResponsePayLoad = null;
        if (jsonIllustrationOutput != null) {
            illustrationResponsePayLoad = new IllustrationResponsePayLoad();
            illustrationResponsePayLoad.setResponseJson(jsonIllustrationOutput);
            illustrationResponsePayLoad.setIdentifier(Integer.parseInt(transactions.getIllustrationId()));
            illustrationResponsePayLoad.setAgreement(transactions.getProposal());
            Set<IllustrationResponsePayLoad> illustrationResponsePayLoads = new HashSet<IllustrationResponsePayLoad>();
            illustrationResponsePayLoads.add(illustrationResponsePayLoad);
            transactions.getProposal().setIllustrationResponse(illustrationResponsePayLoads);
        }

        transactions.setCreationDateTime(LifeEngageComponentHelper.getCurrentdate());
        transactions.setModifiedTimeStamp(LifeEngageComponentHelper.getCurrentdate());
    }

    /**
     * Will execute in between Save and freemarker parsing.
     * 
     * @param transactions
     *            the transactions
     */
    protected void onAfterSave(Transactions transactions) {

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageIllustrationComponent#retrieveIllustration(com.cognizant.insurance
     * .request.vo.RequestInfo, org.json.JSONObject)
     */
    @Override
    public String retrieveIllustration(final RequestInfo requestInfo, final JSONObject jsonObj)
            throws BusinessException, ParseException {
        String response = null;
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        if (jsonObj.has(LifeEngageComponentHelper.KEY_3) && jsonObj.getString(LifeEngageComponentHelper.KEY_3) != null
                && !"".equals(jsonObj.getString(LifeEngageComponentHelper.KEY_3))
                && !jsonObj.getString(LifeEngageComponentHelper.KEY_3).equals("null")) {
            final Transactions requestPayload = (Transactions) retrieveIllustrationHolder.parseJson(jsonObj.toString());

            try {
                transaction = illustrationRepository.retrieveIllustration(requestPayload, requestInfo);
            } catch (Exception e) {
                LOGGER.error("Unable to retrieve Illustration :" + e.getMessage());
                transaction = requestPayload;
                LifeEngageComponentHelper.createResponseStatus(transaction, Constants.FAILURE, e.getMessage() + " : "
                        + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

            }

            LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), transaction, true);
            transactionsList.add(transaction);
        }
        response = retrieveIllustrationHolder.parseBO(transactionsList);
        response = response.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageIllustrationComponent#retrieveAllIllustration(com.cognizant.insurance
     * .request.vo.RequestInfo, org.json.JSONObject)
     */
    @Override
    public String retrieveAllIllustration(final RequestInfo requestInfo, final JSONObject jsonObj)
            throws BusinessException, ParseException {
        List<Transactions> transactionlist = new ArrayList<Transactions>();

        final Transactions transactions = (Transactions) retrieveIllustrationHolder.parseJson(jsonObj.toString());
        String eAppMapToReturn = "{\"Transactions\":[]}";
        boolean fullFetch = true;
        try{
        if (jsonObj.has(LifeEngageComponentHelper.KEY_10)
                && jsonObj.getString(LifeEngageComponentHelper.KEY_10) != null) {
            if (LifeEngageComponentHelper.FULL_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                // Retrieve All details for all proposals for an agent
                transactionlist = illustrationRepository.retrieveAll(transactions, requestInfo, true);
            } else if (LifeEngageComponentHelper.BASIC_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                // Retrieve All details for all proposals for an agent
                transactionlist = illustrationRepository.retrieveAll(transactions, requestInfo, false);
                fullFetch = false;
            }
        }
        LifeEngageComponentHelper.buildTransactionsList(transactionlist, jsonObj);
        if (fullFetch) {
            eAppMapToReturn = retrieveIllustrationHolder.parseBO(transactionlist);
        } else {
            eAppMapToReturn = retrieveIllustrationListHolder.parseBO(transactionlist);
        }}
        catch(Exception e){
        	LOGGER.error("Error in retrieving illustration data",e);
        }
        eAppMapToReturn = eAppMapToReturn.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
        return eAppMapToReturn;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageIllustrationComponent#runIllustration(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Deprecated
    @Override
    public String runIllustration(final JSONObject jsonObjectTransactionData) {

        final String illustrationRuleName = jsonObjectTransactionData.getString(Constants.ILLUSTRATION_RULE_NAME);
        final String validationRuleName = jsonObjectTransactionData.getString(Constants.VALIDATION_RULE_INPUT);
        final String illustrationRuleGroup = jsonObjectTransactionData.getString(Constants.ILLUSTRATION_RULE_GROUP);
        final String validationRuleGroup = jsonObjectTransactionData.getString(Constants.VALIDATION_RULE_GROUP);
        final JSONObject jsonObjectRuleInput = jsonObjectTransactionData.getJSONObject(Constants.RULE_INPUT);
        final String inputJson = jsonObjectRuleInput.toString();

        final String replacedInputJson = inputJson.replaceAll("\\r\\n|\\r|\\n", " ");
        RuleCriteria ruleCriteria = new RuleCriteria();
        ruleCriteria.setRule(validationRuleName);
        ruleCriteria.setClientId(clientId);
        ruleCriteria.setCategory(validationRuleGroup);
        String ruleResult = null;
        try {
            ruleResult = rulesRepository.executeRule(ruleCriteria, replacedInputJson);
        } catch (Exception e) {
            throwSystemException(true, "Failed to satisfy validate Illustration : " + getExceptionMessage(e));
        }
        try {
            final JSONObject jsonObject = new JSONObject(ruleResult);
            if (jsonObject.getBoolean("isSuccess")) {
                ruleCriteria = new RuleCriteria();
                ruleCriteria.setRule(illustrationRuleName);
                ruleCriteria.setClientId(clientId);
                ruleCriteria.setCategory(illustrationRuleGroup);
                ruleResult = null;
                try {
                    ruleResult = rulesRepository.executeRule(ruleCriteria, replacedInputJson);
                } catch (Exception e) {
                    throwSystemException(true, "Failed to satisfy run Illustration : " + getExceptionMessage(e));
                }
            }
        } catch (ParseException e) {
            throwSystemException(true, "Failed to parse Json Object : " + getExceptionMessage(e));
        }

        return ruleResult;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageIllustrationComponent#executeRule(org.json.JSONObject)
     */
    @Override
    public String executeRule(JSONObject jsonObjectTransactionData) {
        final String ruleName = jsonObjectTransactionData.getString(Constants.RULE_NAME);
        final String ruleGroup = jsonObjectTransactionData.getString(Constants.RULE_GROUP);
        final JSONObject jsonObjectRuleInput = jsonObjectTransactionData.getJSONObject(Constants.RULE_INPUT);
        final String inputJson = jsonObjectRuleInput.toString();

        final String replacedInputJson = inputJson.replaceAll("\\r\\n|\\r|\\n", " ");
        RuleCriteria ruleCriteria = new RuleCriteria();
        ruleCriteria.setRule(ruleName);
        ruleCriteria.setClientId(clientId);
        ruleCriteria.setCategory(ruleGroup);
        String ruleResult = null;
        try {
            ruleResult = rulesRepository.executeRule(ruleCriteria, replacedInputJson);
        } catch (Exception e) {
            LOGGER.error("LifeEngageIllustrationComponentImpl.executeRule : Exception" + e.toString());
            throwSystemException(true, "Failed to execute rule : " + getExceptionMessage(e), e);
        }
        return ruleResult;
    }

    /**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }

    /**
     * Gets the audit repository.
     * 
     * @return the auditRepository
     */
    public final AuditRepository getAuditRepository() {
        return auditRepository;
    }

    /**
     * Sets the audit repository.
     * 
     * @param auditRepository
     *            the auditRepository to set
     */
    public final void setAuditRepository(final AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    /**
     * Gets the save illustration holder.
     * 
     * @return the saveIllustrationHolder
     */
    public final LifeEngageSmooksHolder getSaveIllustrationHolder() {
        return saveIllustrationHolder;
    }

    /**
     * Sets the save illustration holder.
     * 
     * @param saveIllustrationHolder
     *            the saveIllustrationHolder to set
     */
    public final void setSaveIllustrationHolder(final LifeEngageSmooksHolder saveIllustrationHolder) {
        this.saveIllustrationHolder = saveIllustrationHolder;
    }

    /**
     * Gets the retrieve illustration holder.
     * 
     * @return the retrieveIllustrationHolder
     */
    public final LifeEngageSmooksHolder getRetrieveIllustrationHolder() {
        return retrieveIllustrationHolder;
    }

    /**
     * Sets the retrieve illustration holder.
     * 
     * @param retrieveIllustrationHolder
     *            the retrieveIllustrationHolder to set
     */
    public final void setRetrieveIllustrationHolder(final LifeEngageSmooksHolder retrieveIllustrationHolder) {
        this.retrieveIllustrationHolder = retrieveIllustrationHolder;
    }

    /**
     * Gets the retrieve illustration list holder.
     * 
     * @return the retrieveIllustrationListHolder
     */
    public final LifeEngageSmooksHolder getRetrieveIllustrationListHolder() {
        return retrieveIllustrationListHolder;
    }

    /**
     * Sets the retrieve illustration list holder.
     * 
     * @param retrieveIllustrationListHolder
     *            the retrieveIllustrationListHolder to set
     */
    public final void setRetrieveIllustrationListHolder(final LifeEngageSmooksHolder retrieveIllustrationListHolder) {
        this.retrieveIllustrationListHolder = retrieveIllustrationListHolder;
    }

    /**
     * Gets the illustration repository.
     * 
     * @return the illustrationRepository
     */
    public final IllustrationRepository getIllustrationRepository() {
        return illustrationRepository;
    }

    /**
     * Sets the illustration repository.
     * 
     * @param illustrationRepository
     *            the illustrationRepository to set
     */
    public final void setIllustrationRepository(final IllustrationRepository illustrationRepository) {
        this.illustrationRepository = illustrationRepository;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageIllustrationComponent#generateIllustration(org.json.JSONObject)
     */
    @Override
    public String generateIllustration(final JSONObject jsonData) throws ParseException {
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        JSONObject jsonObjectTransactionData = jsonData.getJSONObject(Constants.TRANSACTION_DATA);
        Transactions transaction = new Transactions();
        String illustrationId = jsonData.getString("Key3");
        final String illustrationRuleName = jsonObjectTransactionData.getString(Constants.ILLUSTRATION_RULE_NAME);
        final String validationRuleName = jsonObjectTransactionData.getString(Constants.VALIDATION_RULE_INPUT);
        final String illustrationRuleGroup = jsonObjectTransactionData.getString(Constants.ILLUSTRATION_RULE_GROUP);
        final String validationRuleGroup = jsonObjectTransactionData.getString(Constants.VALIDATION_RULE_GROUP);
        final JSONObject jsonObjectRuleInput = jsonObjectTransactionData.getJSONObject(Constants.RULE_INPUT);
        final String inputJson = jsonObjectRuleInput.toString();
        final String replacedInputJson = inputJson.replaceAll("\\r\\n|\\r|\\n", " ");
        RuleCriteria ruleCriteria = new RuleCriteria();
        ruleCriteria.setRule(validationRuleName);
        ruleCriteria.setClientId(clientId);
        ruleCriteria.setCategory(validationRuleGroup);
        String ruleResult = null;

        try {
            try {
                ruleResult = rulesRepository.executeRule(ruleCriteria, replacedInputJson);
            } catch (Exception e) {
                throw new BusinessException("Failed to satisfy validate Illustration : " + getExceptionMessage(e));
            }
            try {
                final JSONObject jsonObject = new JSONObject(ruleResult);
                if (jsonObject.getBoolean("isSuccess")) {
                    ruleCriteria = new RuleCriteria();
                    ruleCriteria.setRule(illustrationRuleName);
                    ruleCriteria.setClientId(clientId);
                    ruleCriteria.setCategory(illustrationRuleGroup);
                    ruleResult = null;
                    try {
                        ruleResult = rulesRepository.executeRule(ruleCriteria, replacedInputJson);
                    } catch (Exception e) {
                        throwSystemException(true, "Failed to satisfy run Illustration : " + getExceptionMessage(e));
                    }
                }
                transaction.setRuleOutput(ruleResult);
                final StatusData statusData = new StatusData();
                statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
                statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE);
                transaction.setSyncStatus(statusData.getStatus());
                transaction.setStatusData(statusData);
                transaction.setIllustrationId(illustrationId);
            } catch (ParseException e) {
                throw new BusinessException(e);
            }
        } catch (Exception e) {
            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.FAILURE);
            statusData.setStatusCode(LifeEngageComponentHelper.FAILURE_CODE);
            String errorMsg = e.getMessage() + " " + getExceptionMessage(e);
            // TODO removing " to avoid JSON parse error
            errorMsg = errorMsg.replaceAll("\"", "&quot;");
            statusData.setStatusMessage(errorMsg);
            transaction.setStatusData(statusData);
            transaction.setSyncStatus(statusData.getStatus());
            transaction.setSyncError(statusData.getStatusMessage());
        }
        transactionsList.add(transaction);
        LifeEngageComponentHelper.buildTransactionsList(transactionsList, jsonData);
        ruleResult = generateIllustrationHolder.parseBO(transactionsList);

        return ruleResult;
    }
    
    
}
