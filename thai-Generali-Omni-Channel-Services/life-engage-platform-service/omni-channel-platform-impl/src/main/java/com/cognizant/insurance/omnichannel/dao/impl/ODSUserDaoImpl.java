package com.cognizant.insurance.omnichannel.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.agent.PasswordHistory;
import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.dao.ODSUserDao;
import com.cognizant.insurance.omnichannel.dataresults.dto.TeamDetailsDto;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;

public class ODSUserDaoImpl extends DaoImpl implements ODSUserDao {

	@Override
	public Response<GeneraliAgent> validateAgentProfile(Request<Transactions> request) {
		Response<GeneraliAgent> response=new ResponseImpl<GeneraliAgent>();
		JPARequestImpl<Transactions> requestImpl=(JPARequestImpl<Transactions>) request;
		String queryString ="select agent from GeneraliAgent agent where agent.agentCode =:agentCode";
		
		Query query=requestImpl.getEntityManager().createQuery(queryString);
		query.setParameter("agentCode", request.getType().getKey1());
		List<Object> resultList=query.getResultList();
		if(resultList!=null && resultList.size() >0){
			response.setType((GeneraliAgent) resultList.get(0));
		}
		return response;
	}	
	
	@Override
	public Response<GeneraliTHUser> validateLoggedInUser(Request<Transactions> request) {
		Response<GeneraliTHUser> response=new ResponseImpl<GeneraliTHUser>();
		JPARequestImpl<Transactions> requestImpl=(JPARequestImpl<Transactions>) request;
		String queryString ="select user from GeneraliTHUser user where user.userId =:userId";
		
		Query query=requestImpl.getEntityManager().createQuery(queryString);
		query.setParameter("userId", request.getType().getKey1());
		List<Object> resultList=query.getResultList();
		if(resultList!=null && resultList.size() >0){
			response.setType((GeneraliTHUser) resultList.get(0));
		}
		return response;
	}
	
	@Override
	public Response<List<PasswordHistory>> getRecentPasswords(Request<String> userRequest, Request<Integer> historyRequest) {
		Response<List<PasswordHistory>> response = new ResponseImpl<List<PasswordHistory>>();
		List<PasswordHistory> histories=new ArrayList<PasswordHistory>();
		response.setType(histories);
        JPARequestImpl<String> requestImpl = (JPARequestImpl<String>) userRequest;
        Query query = requestImpl.getEntityManager().createQuery(
                            "select password from PasswordHistory password  where password.userId=:userId order by password.createdOn DESC");
        query.setParameter("userId", userRequest.getType());
        query.setMaxResults(historyRequest.getType());
        List<PasswordHistory> list = query.getResultList();
        if (CollectionUtils.isNotEmpty(list)) {
            histories.addAll(list);
        } 
        return response;
	}
	
	@Override
	public Response<PasswordHistory> getOldPasswordHistory(Request<String> userRequest) {
		Response<PasswordHistory> response = new ResponseImpl<PasswordHistory>();		
        JPARequestImpl<String> requestImpl = (JPARequestImpl<String>) userRequest;
        Query query = requestImpl.getEntityManager().createQuery(
                            "select password from PasswordHistory password  where password.userId=:userId order by password.createdOn ASC");
        query.setParameter("userId", userRequest.getType());
        List<PasswordHistory> resultList = query.getResultList();
        if (resultList!=null && resultList.size() > 5) {
            response.setType(resultList.get(0));
        } 
        return response;
	}
	
	@Override
    public Response<List<TeamDetailsDto>> getTeamDetails(Request<Transactions> request) {
        Response<List<TeamDetailsDto>> response=new ResponseImpl<List<TeamDetailsDto>>();
        JPARequestImpl<Transactions> requestImpl=(JPARequestImpl<Transactions>) request;
        
        String queryAppenderCode = StringUtils.EMPTY;
        String queryAppenderTeam = StringUtils.EMPTY;
        String queryAppenderAgent = StringUtils.EMPTY;
        boolean teamFlag = false;
        boolean agentFlag = false;
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String currentDate = sdf.format(new Date());
        
        if(request.getType().getKey2().equals(GeneraliConstants.GROUP_MANAGER)){
            queryAppenderCode = " agent.gmCode = ?1";
        } else if (request.getType().getKey2().equals(GeneraliConstants.SALES_MANAGER)){
            queryAppenderCode = " agent.smCode = ?1";
        } else if (request.getType().getKey2().equals(GeneraliConstants.SALES_AGENT)){
            queryAppenderCode = " agent.agentCode = ?1";
        }
        
        if (request.getType().getKey3() != null) {
            if (request.getType().getKey5()!= null && request.getType().getKey5().equals(GeneraliConstants.LANGUAGE_THAI)) {
                queryAppenderTeam = " and agent.teamThai = ?2";
            } else {
                queryAppenderTeam = " and agent.teamEng = ?2";
            }
            teamFlag = true;
        }

        if (request.getType().getKey4() != null) {
            queryAppenderAgent = " and agent.agentCode = ?3";
            agentFlag = true;
        }
        
        String queryString ="select new com.cognizant.insurance.omnichannel.dataresults.dto.TeamDetailsDto"
                + " (agent.agentCode,"
                + " agent.agentName,"
                + " agent.teamThai,"
                + " agent.teamEng)"
                + " from GeneraliAgent agent where "
                + queryAppenderCode
                + queryAppenderTeam
                + queryAppenderAgent
                + " and agent.agentStatus = ?4"
                + " and agent.licenseExpireDate >= "
                + currentDate
                + " order by agent.agentCode";
        
        Query query=requestImpl.getEntityManager().createQuery(queryString);
        query.setParameter(1, request.getType().getKey1());
        if(teamFlag == true){
            query.setParameter(2, request.getType().getKey3());
        }
        if(agentFlag == true){
            query.setParameter(3, request.getType().getKey4());
        }
        	query.setParameter(4,GeneraliConstants.INFORCE_AGENT);
        List<TeamDetailsDto> resultList=query.getResultList();
        if(resultList!=null && resultList.size() >0){
            response.setType(resultList);
        }
        return response;
    }
	
	@Override
	public Response<GeneraliGAOAgent> validateGAOAgentProfile(Request<Transactions> request) {
		Response<GeneraliGAOAgent> response=new ResponseImpl<GeneraliGAOAgent>();
		JPARequestImpl<Transactions> requestImpl=(JPARequestImpl<Transactions>) request;
		String queryString ="select agent from GeneraliGAOAgent agent where agent.agentCode =:agentCode";
		
		Query query=requestImpl.getEntityManager().createQuery(queryString);
		query.setParameter("agentCode", request.getType().getKey1());
		List<Object> resultList=query.getResultList();
		if(resultList!=null && resultList.size() >0){
			response.setType((GeneraliGAOAgent) resultList.get(0));
		}
		return response;
	}

    @Override
    public Response<List<GeneraliAgent>> getAllAgentDetails(Request<Transactions> request) {
        Response<List<GeneraliAgent>> response = new ResponseImpl<List<GeneraliAgent>>();
        JPARequestImpl<Transactions> requestImpl = (JPARequestImpl<Transactions>) request;
        String queryString = "select agent from GeneraliAgent agent where agent.agentStatus ='INFORCE'";
        Query query = requestImpl.getEntityManager().createQuery(queryString);
        List<GeneraliAgent> resultList = query.getResultList();
        if (resultList != null && resultList.size() > 0) {
            response.setType(resultList);
        }
        return response;
    }
}
