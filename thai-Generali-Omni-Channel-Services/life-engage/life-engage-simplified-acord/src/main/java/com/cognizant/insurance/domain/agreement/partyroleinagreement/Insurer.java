/**
 * 
 */
package com.cognizant.insurance.domain.agreement.partyroleinagreement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;

import com.cognizant.insurance.domain.commonelements.commoncodelists.InsurerRejectionReasonCodeList;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;

/**
 * A party (e.g. person or organization) that undertakes to provide insurance
 * services in exchange for the payment of premiums.
 * 
 * SOURCE: Barron's
 * 
 * A party who offers protection to the insured in a financial services
 * agreement.
 * 
 * NOTE: When an insurer shares the risk sustained under an insurance policy
 * with another insurer, each is also known as a co-insurer.
 * 
 * e.g: Acme Insurance Company as the insurer of an endowment policy. <!--
 * end-UML-doc -->
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("INSURER")
public class Insurer extends PartyRoleInAgreement {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6285984510970944387L;

    /**
     * The reason given by the insurer for his rejection of the conditions
     * stated in the financial services agreement.
     * 
     * e.g: High risk
     * 
     * e.g: History of fraud
     * 
     * 
     * 
     */
    @Enumerated
    private InsurerRejectionReasonCodeList rejectionReasonCode;

    /**
     * Written line: the maximum amount of insurance that an insurer has agreed
     * to accept when initialling a slip; it may be more than the amount
     * actually insured by an individual insurer if the broker obtains more than
     * 100% cover for the risk, in which case each insurer's liability will be
     * reduced proportionately (written down) to a closed line or signed line.
     * 
     * 
     * 
     */
    private Float writtenLinePercentage;

    /**
     * The amount for which an insurer becomes liable in the event of a slip
     * being over- subscribed (see written line). Syn: Closed line.
     * 
     * 
     * 
     */
    private Float signedLinePercentage;

    /**
     * Indicates the insurer is the leader (aperitor).
     * 
     * Note: Co-insurance refers to the insurance, usually of large risks, by
     * two or more direct insurers as a means of spreading the risk. <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    private Boolean leaderIndicator;

    /**
     * Indicates if the management of the coinsurance is made by the aperitor.
     * 
     * 
     * 
     */
    private Boolean managementByAperitorIndicator;

    /**
     * Gets the rejectionReasonCode.
     * 
     * @return Returns the rejectionReasonCode.
     */
    public InsurerRejectionReasonCodeList getRejectionReasonCode() {
        return rejectionReasonCode;
    }

    /**
     * Gets the writtenLinePercentage.
     * 
     * @return Returns the writtenLinePercentage.
     */
    public Float getWrittenLinePercentage() {
        return writtenLinePercentage;
    }

    /**
     * Gets the signedLinePercentage.
     * 
     * @return Returns the signedLinePercentage.
     */
    public Float getSignedLinePercentage() {
        return signedLinePercentage;
    }

    /**
     * Gets the leaderIndicator.
     * 
     * @return Returns the leaderIndicator.
     */
    public Boolean getLeaderIndicator() {
        return leaderIndicator;
    }

    /**
     * Gets the managementByAperitorIndicator.
     * 
     * @return Returns the managementByAperitorIndicator.
     */
    public Boolean getManagementByAperitorIndicator() {
        return managementByAperitorIndicator;
    }

    /**
     * Sets The rejectionReasonCode.
     * 
     * @param rejectionReasonCode
     *            The rejectionReasonCode to set.
     */
    public void setRejectionReasonCode(
            final InsurerRejectionReasonCodeList rejectionReasonCode) {
        this.rejectionReasonCode = rejectionReasonCode;
    }

    /**
     * Sets The writtenLinePercentage.
     * 
     * @param writtenLinePercentage
     *            The writtenLinePercentage to set.
     */
    public void
            setWrittenLinePercentage(final Float writtenLinePercentage) {
        this.writtenLinePercentage = writtenLinePercentage;
    }

    /**
     * Sets The signedLinePercentage.
     * 
     * @param signedLinePercentage
     *            The signedLinePercentage to set.
     */
    public void setSignedLinePercentage(final Float signedLinePercentage) {
        this.signedLinePercentage = signedLinePercentage;
    }

    /**
     * Sets The leaderIndicator.
     * 
     * @param leaderIndicator
     *            The leaderIndicator to set.
     */
    public void setLeaderIndicator(final Boolean leaderIndicator) {
        this.leaderIndicator = leaderIndicator;
    }

    /**
     * Sets The managementByAperitorIndicator.
     * 
     * @param managementByAperitorIndicator
     *            The managementByAperitorIndicator to set.
     */
    public void setManagementByAperitorIndicator(
            final Boolean managementByAperitorIndicator) {
        this.managementByAperitorIndicator = managementByAperitorIndicator;
    }

}
