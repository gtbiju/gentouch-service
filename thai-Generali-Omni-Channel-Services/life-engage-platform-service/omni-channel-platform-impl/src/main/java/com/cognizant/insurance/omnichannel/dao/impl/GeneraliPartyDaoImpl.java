/*
 * Name : GeneraliPartyDaoImpl
 * Created on : 21-07-2015
 */
package com.cognizant.insurance.omnichannel.dao.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Query;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationInteractionTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationStatusCodeList;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.dao.GeneraliPartyDao;
import com.cognizant.insurance.omnichannel.vo.AgentData;
import com.cognizant.insurance.omnichannel.vo.ManagerialLevelUserDetails;
import com.cognizant.insurance.omnichannel.vo.SelectedAgentData;
import com.cognizant.insurance.party.dao.impl.PartyDaoImpl;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public class GeneraliPartyDaoImpl extends PartyDaoImpl implements GeneraliPartyDao {
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliPartyDaoImpl.class);

    /** The Constant FNA. */
    private static final String FNA = "FNA";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";
    
    /** The Constant GET_LMS_BY_IDENTIFIER. */

    private static final String GET_LMS_BY_BRANCH_IDENTIFIER = "select cus from Customer cus where cus.branchId =?1 "
            + "and cus.statusCode in ?2 and cus.modifiedDateTime>=?3 " + "order by cus.creationDateTime asc";

    /** The Constant GET_LMS_BY_IDENTIFIER. */
    private static final String GET_LMS_BY_AGENT_IDENTIFIER = "select cus from Customer cus where cus.agentId =?1 "
            + "and cus.statusCode in ?2 and cus.modifiedDateTime>=?3 " + "order by cus.creationDateTime asc";

    /** The Constant GET_LMS_BY_REASSIGNKEY_IDENTIFIER. */
    private static final String GET_LMS_BY_REASSIGNKEY_IDENTIFIER =
            "select cus from Customer cus where cus.agentId =?1 "
                    + "and cus.branchId =?2 and cus.statusCode in ?3 and cus.modifiedDateTime>=?4 "
                    + "order by cus.creationDateTime asc";

    /** The Constant GET_LMS_BY_IDENTIFIER. */
    private static final String GET_LMS_BY_IDENTIFIER = "select cus from Customer cus where cus.identifier =?1 "
            + "and cus.statusCode in ?2";

    /*
     * Implementing retrieveLMSDuplicateCustomers based on agentId and contact number and statusCode not in
     */
    public List<Customer> retrieveLMSDuplicateCustomers(final Request<PartyRoleInRelationship> customerRequest,
            final Transactions transactions, final Request<List<CustomerStatusCodeList>> statusRequest,
            final Request<List<TelephoneTypeCodeList>> telephoneTypeRequest) {
        // TODO Auto-generated method stub

        String query =
                "select customer from Customer customer join fetch customer.playerParty party join fetch party.name name join fetch party.preferredContact pc join fetch pc.preferredContactPoint pcp where customer.agentId=?1 and pcp.fullNumber=?2 and customer.statusCode not in ?3";
        String agentIdOrRmIdentityProof = transactions.getAgentId();

        if (transactions.getBranchId() != null && !"".equalsIgnoreCase(transactions.getBranchId())
                && transactions.getKey4() != null
                && !GeneraliConstants.BULK_AGENCY.equals(transactions.getAllocationType())) {
            /* Branch login duplicate check with branch id and customer mobile number in key4 */
            query =
                    "select customer from Customer customer join fetch customer.relatedCustomers rc join fetch rc.playerParty rcplayerParty join fetch  customer.playerParty party "
                            + "join fetch party.name name join fetch party.preferredContact pc join fetch pc.preferredContactPoint pcp where "
                            + " rcplayerParty.identityProof=?1 and pcp.fullNumber=?2 and customer.statusCode not in ?3";
            agentIdOrRmIdentityProof = transactions.getRmIdentityProof();
        }

        List<Object> customerList =
                findUsingQuery(customerRequest, query, agentIdOrRmIdentityProof, transactions.getKey4(),
                        statusRequest.getType());

        final List<Customer> customers = new ArrayList<Customer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object customerObj : customerList) {
                customers.add((Customer) customerObj);
            }
        }
        return customers;
    }
    /**
     * Method to retrieve Additional Insured
     * @param agreementRequest
     * @return response
     */
    public Response<Set<Insured>> retrieveInsuredWithAdditionalInsured(Request<Agreement> agreementRequest) {
        Response<Set<Insured>> response = new ResponseImpl<Set<Insured>>();
        if (agreementRequest.getType() != null) {
            String query = "select ins from Insured ins where ins.isPartyRoleIn.id =?1";
            List<Object> insuredList = findUsingQuery(agreementRequest, query, agreementRequest.getType().getId());

            if (CollectionUtils.isNotEmpty(insuredList)) {
                Set<Insured> insureds = new HashSet<Insured>();
                for (Object object : insuredList) {
                    insureds.add((Insured) object);
                }

                response.setType(insureds);
            }
        }
        return response;
    }

    /**
     * Method getRelatedTransactionCountForLMS
     * 
     * @param searchCriteriatRequest
     * @param statusRequest
     * @return searchCountResponse
     */
    @Override
    public Response<List<SearchCountResult>> getRelatedTransactionCountForLMS(
            final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<List<SearchCountResult>> searchCountResponse = new ResponseImpl<List<SearchCountResult>>();
        List<Object> searchCriteriaList = null;
        String query;
        List<SearchCountResult> searchCountResultList = new ArrayList<SearchCountResult>();
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();

            /**
             * Query to fetch FNA Count
             */
            query =
                    "select count(goalAndNeed.identifier),transactionKeys.key1 from GoalAndNeed goalAndNeed where goalAndNeed.agentId =?1 "
                            + "and goalAndNeed.contextId =?2 and goalAndNeed.transactionKeys.key1 IN (?3) and goalAndNeed.status not in  ('Cancelled','Draft') group by transactionKeys.key1";

            searchCriteriaList =
                    findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(), ContextTypeCodeList.FNA,
                            searchCriteria.getTransTrackingIdList());

            if (CollectionUtils.isNotEmpty(searchCriteriaList)) {

                createSearchCountResponse(searchCriteriaList, searchCountResultList, FNA);
            } 

            /**
             * Query to fetch illustration Count
             */
            query =
                    "select count(insAgr.identifier), insAgr.transactionKeys.key1 from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                            + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 and "
                            + createOrClause("insAgr.transactionKeys.key1", searchCriteria.getTransTrackingIdList()) + "group by insAgr.transactionKeys.key1";
            searchCriteriaList =
                    findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(), statusRequest.getType(),
                            ContextTypeCodeList.ILLUSTRATION);

            if (CollectionUtils.isNotEmpty(searchCriteriaList)) {

                createSearchCountResponse(searchCriteriaList, searchCountResultList, ILLUSTRATION);
            }

            /**
             * Query to fetch eApp Count
             */
            query =
                    "select count(distinct insAgr.identifier), insAgr.transactionKeys.key1 from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                            + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 and "
                            + createOrClause("insAgr.transactionKeys.key1", searchCriteria.getTransTrackingIdList()) + "group by insAgr.transactionKeys.key1";
            searchCriteriaList =
                    findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(), statusRequest.getType(),
                            ContextTypeCodeList.EAPP);

            if (CollectionUtils.isNotEmpty(searchCriteriaList)) {

                createSearchCountResponse(searchCriteriaList, searchCountResultList, E_APP);
            }

        }

        searchCountResponse.setType(searchCountResultList);
        return searchCountResponse;
    }
    /**
     * Method to create to dynamic SQL statement with 'Or' clause [Replacement of 'in']
     * @param placeHolder
     * @param actualDatas
     * @return query
     */
    public String createOrClause(String placeHolder, List<String> actualDatas) {

        int count = 0;
        int size = actualDatas.size();
        StringBuilder query = new StringBuilder();
        query.append("( ");
        for (String actualData : actualDatas) {
            count++;
            query.append(placeHolder + "='" + actualData + "'");
            if (count < size) {
                query.append(" or ");
            }
        }
        query.append(") ");
        return query.toString();

    }
 
    /**
     * Method createSearchCountResponse
     * 
     * @param searchCriteriaList
     * @param searchCountResultList
     * @param contexType
     */
    private void createSearchCountResponse(List<Object> searchCriteriaList,
            List<SearchCountResult> searchCountResultList, String contexType) {

        for (Object searchCriteria : searchCriteriaList) {
            final SearchCountResult searchCountResult = new SearchCountResult();

            Object[] values = (Object[]) searchCriteria;

            Long count = (Long) values[0];
            String transTrackingId = (String) values[1];

            searchCountResult.setCount(String.valueOf(count));
            searchCountResult.setMode(contexType);
            searchCountResult.setTransTrackingId(transTrackingId);
            searchCountResultList.add(searchCountResult);
        }

    }
    /**
     * Method to retrieve Agent
     * @param agreementRequest
     * @return response
     */
    public Response<AgreementProducer> retrieveAgent(Request<Agreement> agreementRequest) {
        Response<AgreementProducer> response = new ResponseImpl<AgreementProducer>();
        if (agreementRequest.getType() != null) {
            String query = "select agent from AgreementProducer Agent where Agent.isPartyRoleIn.id =?1";
            List<Object> proposerList = findUsingQuery(agreementRequest, query, agreementRequest.getType().getId());
            if (CollectionUtils.isNotEmpty(proposerList)) {
                response.setType((AgreementProducer) proposerList.get(0));
            }
        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.party.dao.PartyDao#retrieveCustomers(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    /**
     * Modifeied for Generali Vietnam. Changed logic to retrive Lead created under branch Users (Relationship Manager)
     * 
     */

    @Override
    public List<Customer> retrieveCustomers(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest, Request<Integer> limitRequest) {
        String query;
        List<Object> customerList = new ArrayList<Object>();
        String identifier1;
        String identifier2;
        final Customer customer = (Customer) customerRequest.getType();

        if (customer.getAllocationType() != null && !customer.getAllocationType().equals("")) {

            if (customer.getBranchId() != null && !customer.getBranchId().equals("")) {
                identifier1 = customer.getAgentId();
                identifier2 = customer.getBranchId();
                query = GET_LMS_BY_REASSIGNKEY_IDENTIFIER;

                customerList =
                        findUsingQuery(customerRequest, query, identifier1, identifier2, statusRequest.getType(),
                                customer.getLastSyncDate());

            } else {
                identifier1 = customer.getAgentId();
                query = GET_LMS_BY_AGENT_IDENTIFIER;

                customerList =
                        findUsingQuery(customerRequest, query, identifier1, statusRequest.getType(),
                                customer.getLastSyncDate());

            }

        } else if (customer.getBranchId() != null && !customer.getBranchId().equals("")) {
            String identifier = customer.getBranchId();
            query = GET_LMS_BY_BRANCH_IDENTIFIER;

            if (limitRequest != null && limitRequest.getType() != null && limitRequest.getType() > 0) {
                customerList =
                        findUsingQueryWithLimit(customerRequest, query, limitRequest.getType(), identifier,
                                statusRequest.getType(), customer.getLastSyncDate());
            } else {
                customerList =
                        findUsingQuery(customerRequest, query, customer.getAgentId(), statusRequest.getType(),
                                customer.getLastSyncDate());
            }

        } else {
            String identifier = customer.getAgentId();
            query = GET_LMS_BY_AGENT_IDENTIFIER;
            if (limitRequest != null && limitRequest.getType() != null && limitRequest.getType() > 0) {
                customerList =
                        findUsingQueryWithLimit(customerRequest, query, limitRequest.getType(), identifier,
                                statusRequest.getType(), customer.getLastSyncDate());
            } else {
                customerList =
                        findUsingQuery(customerRequest, query, customer.getAgentId(), statusRequest.getType(),
                                customer.getLastSyncDate());
            }
        }

        final List<Customer> customers = new ArrayList<Customer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object customerObj : customerList) {
                customers.add((Customer) customerObj);
            }
        }
        return customers;
    }

    /**
     * Method to retrieve Customer
     * @param identifierRequest
     * @param statusRequest
     * @return response
     */
    @Override
    public Response<Customer> retrieveCustomer(Request<Customer> identifierRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        Response<Customer> response = new ResponseImpl<Customer>();
        if (identifierRequest.getType() != null) {
            final Customer customer = (Customer) identifierRequest.getType();
            if (customer.getIdentifier() != null && !("".equals(customer.getIdentifier()))) {
                final List<Customer> customerObjList =
                        findByQuery(identifierRequest, GET_LMS_BY_IDENTIFIER, customer.getIdentifier(),
                                statusRequest.getType());
                final List<Customer> customers = new ArrayList<Customer>();
                if (CollectionUtils.isNotEmpty(customerObjList)) {
                    for (Object customerObj : customerObjList) {
                        customers.add((Customer) customerObj);
                    }
                } else {
                    throw new DaoException("No LMS with identifier : " + customer.getIdentifier());
                }
                response.setType((Customer) customers.get(0));

            }
        }
        return response;
    }
    /**
     * Method to get Lead Count
     * @param agentCodeRequst
     * @param statusRequest
     * @return agenyCodeList
     */
    @Override
    public List<String> getAgentLeadCount(final Request<List<String>> agentCodeRequst,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        List<Object> customerList;
        List<String> agentCodeListInput = agentCodeRequst.getType();
        List<String> agenyCodeList = new ArrayList<String>();
        Map<String, Long> agentDataMap = new HashMap<String, Long>();
        try {
            String query =
                    "select agentId,count(customer.identifier) from Customer customer where customer.agentId in ?1  and customer.statusCode in ?2  group by customer.agentId";
            customerList = findUsingQuery(agentCodeRequst, query, agentCodeRequst.getType(), statusRequest.getType());

            for (Object customerObj : customerList) {

                Object[] valuesOuter = (Object[]) customerObj;
                String agentIdOuter = (String) valuesOuter[0];
                Long countTotal = (Long) valuesOuter[1];
                agentDataMap.put(agentIdOuter, countTotal);
                if (countTotal.intValue() < 50) {

                    agenyCodeList.add(agentIdOuter);
                }
            }
            for (String agentcode : agentCodeListInput) {
                if (!agenyCodeList.contains(agentcode) && !agentDataMap.containsKey(agentcode)) {

                    agenyCodeList.add(agentcode);
                }

            }
        } catch (Exception e) {
            LOGGER.error("error", e);
        }
        return agenyCodeList;

    }
    /**
     * Method to get YTD
     * @param agentCodeRequst
     * @param statusRequestClosed
     * @param statusRequestTotal
     * @return customerTestvoYTD
     * 
     */
    @Override
    public List<AgentData> getYTD(final Request<List<AgentData>> agentCodeRequst,
            final Request<List<CustomerStatusCodeList>> statusRequestClosed,
            final Request<List<CustomerStatusCodeList>> statusRequestTotal) {

        List<AgentData> agentClosedDataList = new ArrayList<AgentData>();
        List<AgentData> agentTotalDataList = new ArrayList<AgentData>();
        final List<AgentData> customerTestvoYTD = new ArrayList<AgentData>();
        try {
            List<String> agentCodeList = new ArrayList<String>();
            List<AgentData> agentDatalist = new ArrayList<AgentData>();
            AgentData agentData = new AgentData();
            if (agentCodeRequst != null) {
                agentDatalist = agentCodeRequst.getType();
                if (agentDatalist != null && agentDatalist.size() > 0) {
                    agentData = agentDatalist.get(0);
                }
            }
            List<String> agentCodeListInput = new ArrayList<String>();
            for (AgentData agentdata1 : agentDatalist) {
                agentCodeListInput.add(agentdata1.getAgentCode());
            }

            List<Object> customerListClosed;
            List<Object> customerListTotal;
            Map<String, Long> agentClosedMap = new HashMap<String, Long>();
            Map<String, Long> agentTotalMap = new HashMap<String, Long>();

            String query1 =
                    "select cus1.agentId, count(cus1.identifier) from Customer cus1 where cus1.agentId in ?1 and cus1.statusCode in ?2 and cus1.creationDateTime<=?3 and cus1.creationDateTime>=?4 group by cus1.agentId order by cus1.agentId";

            String query2 =
                    "select cus2.agentId, count(cus2.identifier) from Customer cus2 where cus2.agentId in ?1 and cus2.statusCode NOT in ?2 and cus2.creationDateTime<=?3 and cus2.creationDateTime>=?4 group by cus2.agentId order by cus2.agentId";

            customerListClosed =
                    findUsingQuery(agentCodeRequst, query1, agentCodeListInput, statusRequestClosed.getType(),
                            agentData.getCurrentDate(), agentData.getStartDate());

            customerListTotal =
                    findUsingQuery(agentCodeRequst, query2, agentCodeListInput, statusRequestTotal.getType(),
                            agentData.getCurrentDate(), agentData.getStartDate());

            final List<Customer> customers = new ArrayList<Customer>();

            for (Object customerObjTotal : customerListTotal) {
                Object[] valuesOuter = (Object[]) customerObjTotal;
                String agentIdOuter = (String) valuesOuter[0];
                Long countTotal = (Long) valuesOuter[1];
                agentTotalMap.put(agentIdOuter, countTotal);

            }

            for (Object customerObjClosed : customerListClosed) {
                Object[] valuesOuter = (Object[]) customerObjClosed;
                String agentIdOuter = (String) valuesOuter[0];
                Long countTotal = (Long) valuesOuter[1];
                agentClosedMap.put(agentIdOuter, countTotal);

            }

            for (AgentData agentDetails : agentDatalist) {
                String agentCode = agentDetails.getAgentCode();
                if (agentTotalMap.containsKey(agentCode)) {
                    agentDetails.setCountofTotalLeads(agentTotalMap.get(agentCode));
                } else {
                    agentDetails.setCountofTotalLeads(1);
                }
                if (agentClosedMap.containsKey(agentCode)) {
                    agentDetails.setContofClosedleads(agentClosedMap.get(agentCode));
                } else {
                    agentDetails.setContofClosedleads(0);
                }
            }

            for (AgentData agentDetails : agentDatalist) {

                BigDecimal countofClosedLeads = new BigDecimal(agentDetails.getContofClosedleads());
                BigDecimal countofTotalLeads = new BigDecimal(agentDetails.getCountofTotalLeads());
                // Bug UAT 95- Fix
                agentDetails.setYTD(countofClosedLeads.divide(countofTotalLeads, 5, BigDecimal.ROUND_HALF_UP));

            }

            Collections.sort(agentDatalist);

            BigDecimal maxYTD = new BigDecimal(0);
            if (agentDatalist != null && agentDatalist.size() > 0) {
                maxYTD = agentDatalist.get(0).getYTD();

            }
            for (AgentData agentDetails : agentDatalist) {

                customerTestvoYTD.add(agentDetails);

            }
        } catch (Exception e) {
            LOGGER.error("ERROR IN ytd", e);
        }

        return customerTestvoYTD;
    }
    /**
     * Method to get Agent Status Count
     * @param managerialLevelUserDetailsRequest
     * @param statusListNew
     * @param statusListClosed
     * @param statusListFollowUp
     * @return managerialLevelUserDetailsObject
     */
    @Override
    public ManagerialLevelUserDetails getAgentStatusCount(
            Request<ManagerialLevelUserDetails> managerialLevelUserDetailsRequest,
            List<CustomerStatusCodeList> statusListNew, List<CustomerStatusCodeList> statusListClosed,
            List<CustomerStatusCodeList> statusListFollowUp) {

        ManagerialLevelUserDetails managerialLevelUserDetailsObj = new ManagerialLevelUserDetails();
        final ManagerialLevelUserDetails managerialLevelUserDetailsObject = new ManagerialLevelUserDetails();
        ManagerialLevelUserDetails managerialLevelUserDetailsAgentObject;
        Map<String, ManagerialLevelUserDetails> managerialLevelUserDetailsMap =
                new HashMap<String, ManagerialLevelUserDetails>();
        final List<ManagerialLevelUserDetails> managerialLevelUserDetailsList =
                new ArrayList<ManagerialLevelUserDetails>();
        int newCount_YTD = 0;
        int newCount_MTD = 0;
        int closedCount_YTD = 0;
        int closedCount_MTD = 0;
        int followUpCount_YTD = 0;
        int followUpCount_MTD = 0;
        int activeAgentsCount_YTD = 0;
        int activeAgentsCount_MTD = 0;
        int totalAgentsCount_YTD = 0;
        int totalAgentsCount_MTD = 0;
        BigDecimal activityRatio_YTD = new BigDecimal(0);
        BigDecimal activityRatio_MTD = new BigDecimal(0);
        BigDecimal conversionRate_YTD = new BigDecimal(0);
        BigDecimal conversionRate_MTD = new BigDecimal(0);
        BigDecimal APE_value_YTD = new BigDecimal(0);
        BigDecimal APE_value_MTD = new BigDecimal(0);
        BigDecimal productivity_YTD = new BigDecimal(0);
        BigDecimal productivity_MTD = new BigDecimal(0);
        BigDecimal caseSize_YTD = new BigDecimal(0);
        BigDecimal caseSize_MTD = new BigDecimal(0);

        try {
            if (managerialLevelUserDetailsRequest != null) {
                managerialLevelUserDetailsObj = managerialLevelUserDetailsRequest.getType();

                ArrayList<SelectedAgentData> selectedAgentIdData = managerialLevelUserDetailsObj.getSelectedAgentIds();
                List<String> selectedAgentIds = new ArrayList<String>();

                for (SelectedAgentData objSelectedData : selectedAgentIdData) {
                    selectedAgentIds.add(objSelectedData.getAgentId());
                    managerialLevelUserDetailsAgentObject = new ManagerialLevelUserDetails();
                    managerialLevelUserDetailsAgentObject.setUserId(objSelectedData.getAgentId());
                    managerialLevelUserDetailsAgentObject.setUserName(objSelectedData.getAgentName());
                    if(objSelectedData.getMobileNumber() != null ) {
                    	managerialLevelUserDetailsAgentObject.setMobileNumber(objSelectedData.getMobileNumber());
                    }
                    managerialLevelUserDetailsAgentObject.setNew_YTD(0);
                    managerialLevelUserDetailsAgentObject.setClosed_YTD(0);
                    managerialLevelUserDetailsAgentObject.setFollowUp_YTD(0);
                    managerialLevelUserDetailsAgentObject.setNew_MTD(0);
                    managerialLevelUserDetailsAgentObject.setClosed_MTD(0);
                    managerialLevelUserDetailsAgentObject.setFollowUp_MTD(0);
                    managerialLevelUserDetailsMap.put(objSelectedData.getAgentId(),
                            managerialLevelUserDetailsAgentObject);

                }
                totalAgentsCount_YTD = getTotalAgentCountYTD(selectedAgentIdData);
                totalAgentsCount_MTD = getTotalAgentCountMTD(selectedAgentIdData);

                List<Object> customerListNewYTD;
                List<Object> customerListClosedYTD;
                List<Object> customerListFollowUpYTD;

                List<Object> customerListNewMTD;
                List<Object> customerListClosedMTD;
                List<Object> customerListFollowUpMTD;

                List<Object> customerListAPEYTD;
                List<Object> customerListAPEMTD;
                List<Object> customerListActiveAgentYTD;
                
                List<Object> customerListActiveAgentMTD;
                

                // Calling queries for calculating YTD
                String queryStatus_YTD =
                        "select cus.agentId, count(cus.identifier) from Customer cus where cus.agentId in ?1 and cus.statusCode in ?2 and cus.modifiedDateTime<?3 and cus.modifiedDateTime>=?4 group by cus.agentId";

                String queryStatusNew_YTD =
                    "select cus.agentId, count(cus.identifier) from Customer cus where cus.agentId in ?1 and cus.statusCode in ?2 and cus.creationDateTime<?3 and cus.creationDateTime>=?4 group by cus.agentId";
                
                customerListNewYTD =
                        findUsingQuery(managerialLevelUserDetailsRequest, queryStatusNew_YTD, selectedAgentIds,
                                statusListNew, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getStartDate());

                customerListClosedYTD =
                        findUsingQuery(managerialLevelUserDetailsRequest, queryStatus_YTD, selectedAgentIds,
                                statusListClosed, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getStartDate());

                customerListFollowUpYTD =
                        findUsingQuery(managerialLevelUserDetailsRequest, queryStatus_YTD, selectedAgentIds,
                                statusListFollowUp, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getStartDate());

              /*  String queryAPE_YTD =
                        "select cus.agentId, Sum(isnull(cast(cus.transactionKeys.key24 as long),0)) from Customer cus where cus.agentId in ?1 and cus.statusCode in ?2 and cus.modifiedDateTime<?3 and cus.modifiedDateTime>=?4 group by cus.agentId";*/

              
                
                String queryAPE_YTD =
                        "select customer.agentId, ISNULL(sum(cp.premium.amount),0) from Customer customer join customer.purchasedProducts cp "
                                + " where cp.premium.amount is not null and customer.agentId in ?1 and customer.statusCode in ?2 "
                                + "and cp.submissionDate<?3 and cp.submissionDate>=?4 group by " + "customer.agentId";

                customerListAPEYTD =
                        findUsingQuery(managerialLevelUserDetailsRequest, queryAPE_YTD, selectedAgentIds,
                                statusListClosed, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getStartDate());

                // Calling queries for calculating MTD
                String queryStatus_MTD =
                        "select cus.agentId, count(cus.identifier) from Customer cus where cus.agentId in ?1 and cus.statusCode in ?2 and cus.modifiedDateTime<?3 and cus.modifiedDateTime>=?4 group by cus.agentId";
                
                String queryStatusClosed_MTD =
                        "select customer.agentId, customer.identifier,cp.submissionDate from Customer customer join customer.purchasedProducts cp where cp.premium.amount is not null and customer.agentId in ?1 and customer.statusCode in ?2 and customer.modifiedDateTime<?3 and customer.modifiedDateTime>=?4 order by  customer.identifier,cp.submissionDate";
                
                String queryStatusNew_MTD =
                    "select cus.agentId, count(cus.identifier) from Customer cus where cus.agentId in ?1 and cus.statusCode in ?2 and cus.creationDateTime<?3 and cus.creationDateTime>=?4 group by cus.agentId";

                String queryActiveAgent_MTD =
                   // "select cus.agentId, count(cus.identifier) from Customer cus where cus.agentId in ?1 and cus.statusCode in ?2 and cus.modifiedDateTime<?3 and cus.modifiedDateTime>=?4 group by cus.agentId";
                    "select cus.agentId, count(cus.identifier) from Customer cus join cus.purchasedProducts cp where cus.agentId in ?1 and cus.statusCode in ?2 and cp.submissionDate<?3 and cp.submissionDate>=?4 group by cus.agentId";
            
                customerListActiveAgentMTD =

                        findUsingQuery(managerialLevelUserDetailsRequest, queryActiveAgent_MTD, selectedAgentIds,
                                statusListClosed, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getCurrentMonth());
                
                customerListNewMTD =
                        findUsingQuery(managerialLevelUserDetailsRequest, queryStatusNew_MTD, selectedAgentIds,
                                statusListNew, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getCurrentMonth());

                customerListClosedMTD =
                        findUsingQuery(managerialLevelUserDetailsRequest, queryStatusClosed_MTD, selectedAgentIds,
                                statusListClosed, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getCurrentMonth());

                customerListFollowUpMTD =
                        findUsingQuery(managerialLevelUserDetailsRequest, queryStatus_MTD, selectedAgentIds,
                                statusListFollowUp, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getCurrentMonth());

              
                
                
                String queryAPE_MTD =
                        "select customer.agentId, ISNULL(Sum(cp.premium.amount),0) from Customer customer join customer.purchasedProducts cp "
                                + " where cp.premium.amount is not null and customer.agentId in ?1 and customer.statusCode in ?2 "
                                + "and cp.submissionDate<?3 and cp.submissionDate>=?4 group by " + "customer.agentId";
                 

                customerListAPEMTD =
                        findUsingQuery(managerialLevelUserDetailsRequest, queryAPE_MTD, selectedAgentIds,
                                statusListClosed, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getCurrentMonth());

                   String activeAgentCountYTD =
                        "select count(cus.identifier), cus.agentId ,DATENAME(MONTH ,cp.submissionDate) from Customer cus join cus.purchasedProducts cp where cus.agentId in ?1 and cus.statusCode in ?2 and cp.submissionDate<?3 and cp.submissionDate>=?4 group by  cus.agentId,DATENAME(MONTH,cp.submissionDate) order by cus.agentId";
                customerListActiveAgentYTD =
                        findUsingQuery(managerialLevelUserDetailsRequest, activeAgentCountYTD, selectedAgentIds,
                                statusListClosed, managerialLevelUserDetailsObj.getCurrentDate(),
                                managerialLevelUserDetailsObj.getStartDate());
                activeAgentsCount_YTD = customerListActiveAgentYTD.size();

                // for getting new status YTD
                if (customerListNewYTD.size() > 0) {
                    for (Object customerObjNew : customerListNewYTD) {
                        Object[] valuesOuter = (Object[]) customerObjNew;
                        String agentIdOuter = (String) valuesOuter[0];
                        Long countNewlong = (Long) valuesOuter[1];
                        String countNewStr = countNewlong.toString();
                        int newCountnew = Integer.parseInt(countNewStr);
                        newCount_YTD = newCount_YTD + newCountnew;
                        managerialLevelUserDetailsAgentObject = managerialLevelUserDetailsMap.get(agentIdOuter);
                        managerialLevelUserDetailsAgentObject.setUserId(agentIdOuter);
                        managerialLevelUserDetailsAgentObject.setNew_YTD(newCountnew);
                        managerialLevelUserDetailsMap.put(agentIdOuter, managerialLevelUserDetailsAgentObject);
                    }
                    managerialLevelUserDetailsObject.setNew_YTD(newCount_YTD);
                }
                // for getting closed status YTD
                if (customerListClosedYTD.size() > 0) {
                    for (Object customerObjClosed : customerListClosedYTD) {
                        Object[] valuesOuter = (Object[]) customerObjClosed;
                        String agentIdOuter = (String) valuesOuter[0];
                        Long countClosedlong = (Long) valuesOuter[1];
                        String countClosedStr = countClosedlong.toString();
                        int countClosed = Integer.parseInt(countClosedStr);

                        closedCount_YTD = closedCount_YTD + countClosed;
                        managerialLevelUserDetailsAgentObject = managerialLevelUserDetailsMap.get(agentIdOuter);
                        if ("".equals(managerialLevelUserDetailsAgentObject)
                                || null == managerialLevelUserDetailsAgentObject) {
                            managerialLevelUserDetailsAgentObject = new ManagerialLevelUserDetails();
                            managerialLevelUserDetailsAgentObject.setUserId(agentIdOuter);
                        }
                        managerialLevelUserDetailsAgentObject.setClosed_YTD(countClosed);
                        managerialLevelUserDetailsMap.put(agentIdOuter, managerialLevelUserDetailsAgentObject);

                    }
                    managerialLevelUserDetailsObject.setClosed_YTD(closedCount_YTD);
                    managerialLevelUserDetailsObject.setActiveAgents_YTD(activeAgentsCount_YTD);
                }

                // for getting followup status YTD
                if (customerListFollowUpYTD.size() > 0) {
                    for (Object customerObjFollowUp : customerListFollowUpYTD) {
                        Object[] valuesOuter = (Object[]) customerObjFollowUp;
                        String agentIdOuter = (String) valuesOuter[0];
                        Long countFollowUplong = (Long) valuesOuter[1];
                        String countFollowUpStr = countFollowUplong.toString();
                        int countFollowUp = Integer.parseInt(countFollowUpStr);
                        followUpCount_YTD = followUpCount_YTD + countFollowUp;
                        managerialLevelUserDetailsAgentObject = managerialLevelUserDetailsMap.get(agentIdOuter);
                        if ("".equals(managerialLevelUserDetailsAgentObject)
                                || null == managerialLevelUserDetailsAgentObject) {
                            managerialLevelUserDetailsAgentObject = new ManagerialLevelUserDetails();
                            managerialLevelUserDetailsAgentObject.setUserId(agentIdOuter);
                        }
                        managerialLevelUserDetailsAgentObject.setFollowUp_YTD(countFollowUp);
                        managerialLevelUserDetailsMap.put(agentIdOuter, managerialLevelUserDetailsAgentObject);
                    }
                    managerialLevelUserDetailsObject.setFollowUp_YTD(followUpCount_YTD);
                }

                // for getting APE YTD
                if (customerListAPEYTD.size() > 0) {
                    for (Object customerObjAPE : customerListAPEYTD) {
                        Object[] valuesOuter = (Object[]) customerObjAPE;
                        String agentIdOuter = (String) valuesOuter[0];
                        BigDecimal ape_YTDStr = (BigDecimal) valuesOuter[1];
                        APE_value_YTD = APE_value_YTD.add(ape_YTDStr);
                        managerialLevelUserDetailsAgentObject = managerialLevelUserDetailsMap.get(agentIdOuter);
                        if ("".equals(managerialLevelUserDetailsAgentObject)
                                || null == managerialLevelUserDetailsAgentObject) {
                            managerialLevelUserDetailsAgentObject = new ManagerialLevelUserDetails();
                            managerialLevelUserDetailsAgentObject.setUserId(agentIdOuter);
                        }
                        managerialLevelUserDetailsAgentObject.setAPE_YTD(ape_YTDStr);
                        managerialLevelUserDetailsMap.put(agentIdOuter, managerialLevelUserDetailsAgentObject);
                    }
                    managerialLevelUserDetailsObject.setAPE_YTD(APE_value_YTD);
                }

                // for getting new status MTD
                if (customerListNewMTD.size() > 0) {
                    for (Object customerObjNew : customerListNewMTD) {
                        Object[] valuesOuter = (Object[]) customerObjNew;
                        String agentIdOuter = (String) valuesOuter[0];
                        Long countNewlong = (Long) valuesOuter[1];
                        String countNewStr = countNewlong.toString();
                        int newCountnew = Integer.parseInt(countNewStr);
                        newCount_MTD = newCount_MTD + newCountnew;
                        managerialLevelUserDetailsAgentObject = managerialLevelUserDetailsMap.get(agentIdOuter);
                        if ("".equals(managerialLevelUserDetailsAgentObject)
                                || null == managerialLevelUserDetailsAgentObject) {
                            managerialLevelUserDetailsAgentObject = new ManagerialLevelUserDetails();
                            managerialLevelUserDetailsAgentObject.setUserId(agentIdOuter);
                        }
                        managerialLevelUserDetailsAgentObject.setNew_MTD(newCountnew);
                        managerialLevelUserDetailsMap.put(agentIdOuter, managerialLevelUserDetailsAgentObject);
                    }
                    managerialLevelUserDetailsObject.setNew_MTD(newCount_MTD);
                }
                // for getting closed status MTD
                Map<String, Date> identifierMap = new HashMap<String, Date>();
                Map<String, Integer> agentCountMap = new HashMap<String, Integer>();
            
               
                if (customerListClosedMTD.size() > 0) {
                    for (Object customerObjClosed : customerListClosedMTD) {
                        Object[] valuesOuter = (Object[]) customerObjClosed;
                        String agentIdOuter = (String) valuesOuter[0];
                        String identifier = (String) valuesOuter[1];
                        Date submissionDate = (Date)valuesOuter[2];
                        int cnt = 0;
                        
                   
                        
                        if (identifierMap.containsKey(identifier)) {
                            
                          //Skipping records with same identifier  
                       

                        } else {
                          
                            identifierMap.put(identifier, submissionDate);
                            if (!submissionDate.before(managerialLevelUserDetailsObj.getCurrentMonth())) {
                               
                           
                                
                                if (agentCountMap.containsKey(agentIdOuter)) {

                                    cnt = cnt + agentCountMap.get(agentIdOuter)+1;
                                    agentCountMap.put(agentIdOuter, cnt);
                                } else {
                                    agentCountMap.put(agentIdOuter, 1);
                                }

                            }
                            
                        }

                
                    }
                    
                    Iterator agentCountMapEntries = agentCountMap.entrySet().iterator();
                    while (agentCountMapEntries.hasNext()) {
                        Entry thisEntry = (Entry) agentCountMapEntries.next();
                        String agentIdKey = (String) thisEntry.getKey();
                        Integer agentValue = (Integer) thisEntry.getValue();
                        closedCount_MTD = closedCount_MTD + agentValue;
                        managerialLevelUserDetailsAgentObject = managerialLevelUserDetailsMap.get(agentIdKey);
                        if ("".equals(managerialLevelUserDetailsAgentObject)
                                || null == managerialLevelUserDetailsAgentObject) {
                            managerialLevelUserDetailsAgentObject = new ManagerialLevelUserDetails();
                            managerialLevelUserDetailsAgentObject.setUserId(agentIdKey);
                        }
                        managerialLevelUserDetailsAgentObject.setClosed_MTD(agentValue);
                        managerialLevelUserDetailsMap.put(agentIdKey, managerialLevelUserDetailsAgentObject);
                    }
                                        
                    activeAgentsCount_MTD = customerListActiveAgentMTD.size();
                    //Needs to be changed 
                    
                    
                    
                    
                    
                    managerialLevelUserDetailsObject.setClosed_MTD(closedCount_MTD);
                    managerialLevelUserDetailsObject.setActiveAgents_MTD(activeAgentsCount_MTD);
                }

                // for getting followup status MTD
                if (customerListFollowUpMTD.size() > 0) {
                    for (Object customerObjFollowUp : customerListFollowUpMTD) {
                        Object[] valuesOuter = (Object[]) customerObjFollowUp;
                        String agentIdOuter = (String) valuesOuter[0];
                        Long countFollowUplong = (Long) valuesOuter[1];
                        String countFollowUpStr = countFollowUplong.toString();
                        int countFollowUp = Integer.parseInt(countFollowUpStr);
                        followUpCount_MTD = followUpCount_MTD + countFollowUp;
                        managerialLevelUserDetailsAgentObject = managerialLevelUserDetailsMap.get(agentIdOuter);
                        if ("".equals(managerialLevelUserDetailsAgentObject)
                                || null == managerialLevelUserDetailsAgentObject) {
                            managerialLevelUserDetailsAgentObject = new ManagerialLevelUserDetails();
                            managerialLevelUserDetailsAgentObject.setUserId(agentIdOuter);
                        }
                        managerialLevelUserDetailsAgentObject.setFollowUp_MTD(countFollowUp);
                        managerialLevelUserDetailsMap.put(agentIdOuter, managerialLevelUserDetailsAgentObject);
                    }
                    managerialLevelUserDetailsObject.setFollowUp_MTD(followUpCount_MTD);
                }

                // for getting APE MTD
                if (customerListAPEMTD.size() > 0) {
                    for (Object customerObjAPE : customerListAPEMTD) {
                        Object[] valuesOuter = (Object[]) customerObjAPE;
                        String agentIdOuter = (String) valuesOuter[0];
                        BigDecimal ape_MTDStr = (BigDecimal) valuesOuter[1];
                        APE_value_MTD = APE_value_MTD.add(ape_MTDStr);
                        managerialLevelUserDetailsAgentObject = managerialLevelUserDetailsMap.get(agentIdOuter);
                        if ("".equals(managerialLevelUserDetailsAgentObject)
                                || null == managerialLevelUserDetailsAgentObject) {
                            managerialLevelUserDetailsAgentObject = new ManagerialLevelUserDetails();
                            managerialLevelUserDetailsAgentObject.setUserId(agentIdOuter);
                        }
                        managerialLevelUserDetailsAgentObject.setAPE_MTD(ape_MTDStr);
                        managerialLevelUserDetailsMap.put(agentIdOuter, managerialLevelUserDetailsAgentObject);
                    }
                    managerialLevelUserDetailsObject.setAPE_MTD(APE_value_MTD);
                }

                Iterator mapEntries = managerialLevelUserDetailsMap.entrySet().iterator();
   			 while(mapEntries.hasNext()) {
   				 Entry thisEntry = (Entry) mapEntries.next();
   				 String agentIdKey = (String) thisEntry.getKey();
   				 ManagerialLevelUserDetails agentValue = (ManagerialLevelUserDetails) thisEntry.getValue();
   				 Integer totalLeadsSumup_YTD = agentValue.getNew_YTD() + agentValue.getClosed_YTD() + agentValue.getFollowUp_YTD();
   				 Integer totalLeadsSumup_MTD = agentValue.getNew_MTD() + agentValue.getClosed_MTD() + agentValue.getFollowUp_MTD();
   				 if(totalLeadsSumup_YTD>0) {
   					 BigDecimal agentConversionRate_YTD = new BigDecimal((float)agentValue.getClosed_YTD()/totalLeadsSumup_YTD*100).setScale(2,BigDecimal.ROUND_HALF_UP);
   					 agentValue.setConversionRate_YTD(agentConversionRate_YTD);
   				 } else {
   					 BigDecimal agentConversionRate_YTD = null ;
   					 agentValue.setConversionRate_YTD(agentConversionRate_YTD);
   				 } 
   				 if(totalLeadsSumup_MTD>0) {
   					 BigDecimal agentConversionRate_MTD = new BigDecimal((float)agentValue.getClosed_MTD()/totalLeadsSumup_MTD*100).setScale(2,BigDecimal.ROUND_HALF_UP);
   					 agentValue.setConversionRate_MTD(agentConversionRate_MTD);
   				 } else {
   					 BigDecimal agentConversionRate_MTD = null;
   					 agentValue.setConversionRate_MTD(agentConversionRate_MTD);
   				 }
   				 agentValue.setTotalLeads_YTD(totalLeadsSumup_YTD);
   				 agentValue.setTotalLeads_MTD(totalLeadsSumup_MTD);
   				
   				 
   				 managerialLevelUserDetailsList.add(agentValue);
   			 }
   			 //settign each agent list to managerialLevelUserDetails object
   			 managerialLevelUserDetailsObject.setUserDetails(managerialLevelUserDetailsList);
   			 
   			 //calculating YTD values like totalagents,activeagents,activityRatio,conversionrate,caseSize,productivity
   			 //TODO ************************************Activity Ratio
   			 //totalAgentsCount_YTD = newCount_YTD + closedCount_YTD + followUpCount_YTD;
   			 managerialLevelUserDetailsObject.setTotalAgents_YTD(totalAgentsCount_YTD);		
   			 int totalLeads_YTD = newCount_YTD + closedCount_YTD + followUpCount_YTD;
   			 managerialLevelUserDetailsObject.setTotalLeads_YTD(totalLeads_YTD);
   			 if(totalLeads_YTD>0 && closedCount_YTD>0) {
   				 conversionRate_YTD = new BigDecimal((float)closedCount_YTD/totalLeads_YTD*100).setScale(2,BigDecimal.ROUND_HALF_UP);
   			 }			 
   			 if(totalAgentsCount_YTD>0) {
   				 activityRatio_YTD = new BigDecimal((float)activeAgentsCount_YTD/totalAgentsCount_YTD*100).setScale(2,BigDecimal.ROUND_HALF_UP);				
   				 productivity_YTD =  APE_value_YTD.divide(new BigDecimal(totalAgentsCount_YTD), 2, BigDecimal.ROUND_HALF_UP);
   			 } 	
   			 if(closedCount_YTD>0) {
   				 caseSize_YTD = APE_value_YTD.divide(new BigDecimal(closedCount_YTD), 2, BigDecimal.ROUND_HALF_UP);
   			 } 			 
   			 
   			//calculating MTD values like totalagents,activeagents,activityRatio,conversionrate,caseSize,productivity
   			 
   			 //totalAgentsCount_MTD = newCount_MTD + closedCount_MTD + followUpCount_MTD;
   			 managerialLevelUserDetailsObject.setTotalAgents_MTD(totalAgentsCount_MTD);		
   			 int totalLeads_MTD = newCount_MTD + closedCount_MTD + followUpCount_MTD;
   			 managerialLevelUserDetailsObject.setTotalLeads_MTD(totalLeads_MTD);
   			 if(totalLeads_MTD>0 && closedCount_MTD>0) {
   				 conversionRate_MTD = new BigDecimal((float)closedCount_MTD/totalLeads_MTD*100).setScale(2,BigDecimal.ROUND_HALF_UP);
   			 }
   			 if(totalAgentsCount_MTD>0) {
   				 activityRatio_MTD = new BigDecimal((float)activeAgentsCount_MTD/totalAgentsCount_MTD*100).setScale(2,BigDecimal.ROUND_HALF_UP);
   				 productivity_MTD = APE_value_MTD.divide(new BigDecimal(totalAgentsCount_MTD), 2, BigDecimal.ROUND_HALF_UP);
   			 }
   			 if(closedCount_MTD>0) {
   				 caseSize_MTD = APE_value_MTD.divide(new BigDecimal(closedCount_MTD), 2, BigDecimal.ROUND_HALF_UP);
   			 }
   			 
   			 			 
   			//setting YTD calculated values to managerialLevelUserDetails object
   			 
   			 managerialLevelUserDetailsObject.setActivityRatio_YTD(activityRatio_YTD);
   			 managerialLevelUserDetailsObject.setConversionRate_YTD(conversionRate_YTD);
   			 managerialLevelUserDetailsObject.setProductivity_YTD(productivity_YTD);
   			 managerialLevelUserDetailsObject.setCaseSize_YTD(caseSize_YTD);
   			
   			 //setting MTD calculated values to managerialLevelUserDetails object
   			 
   			 managerialLevelUserDetailsObject.setActivityRatio_MTD(activityRatio_MTD);
   			 managerialLevelUserDetailsObject.setConversionRate_MTD(conversionRate_MTD);
   			 managerialLevelUserDetailsObject.setProductivity_MTD(productivity_MTD);
   			 managerialLevelUserDetailsObject.setCaseSize_MTD(caseSize_MTD);
   			 
   		} 
   			
   		} catch(Exception e) {
   			LOGGER.error("ERROR IN getAgentStatusCount:GeneraliPartyDaoIlmpl",e);
   		}
   		
   		return managerialLevelUserDetailsObject;
   	}
    /**
     * Method to get RetrieveFilterCount
     * @param searchCriteriaRequest
     * @param statusList
     * @param statusListClosed
     * @return searchCountResponse
     */
    @Override
    public Response<SearchCountResult> getRetrieveFilterCount(Request<SearchCriteria> searchCriteriaRequest,
            List<CustomerStatusCodeList> statusList, List<CustomerStatusCodeList> statusListClosed) {
        final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
        final SearchCountResult searchCountResult = new SearchCountResult();
        List<Object> searchCriteriaList = null;
        List<Object> customerListAPEYTD = new ArrayList<Object>();
        List<Object> customerListAPEMTD = new ArrayList<Object>();
        List<Object> customerListClosedYTD = new ArrayList<Object>();
        List<Object> customerListClosedMTD = new ArrayList<Object>();
        List<Object> customerListLeadAssignedYTD = new ArrayList<Object>();
        List<Object> customerListLeadAssignedMTD = new ArrayList<Object>();
        String type = null;
        BigDecimal totalAPE = new BigDecimal("0");
        BigDecimal totalAPEMTD = new BigDecimal("0");
        String query;
        if (searchCriteriaRequest != null && searchCriteriaRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriaRequest.getType();
            type = searchCriteria.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select count(customer.identifier),customer.statusCode from Customer customer where customer.agentId =?1 "
                                + "and customer.statusCode in ?2 and "
                                + "(customer.transactionKeys.key2 like ?3 or customer.transactionKeys.key4 like ?3) "
                                + "group by customer.statusCode";
                searchCriteriaList =
                        findUsingQuery(searchCriteriaRequest, query, searchCriteria.getAgentId(), statusList, "%"
                                + searchCriteria.getValue() + "%");

            } else {
                query =
                        "select count(customer.identifier), customer.statusCode from Customer customer where customer.agentId =?1 "
                                + "and customer.statusCode in ?2 " + "group by customer.statusCode";

                searchCriteriaList =
                        findUsingQuery(searchCriteriaRequest, query, searchCriteria.getAgentId(), statusList);
            }

           

            String queryAPE_YTD =
                    "select ISNULL(sum(cp.premium.amount),0) from Customer customer join customer.purchasedProducts cp "
                            + " where cp.premium.amount is not null and customer.agentId=?1 and customer.statusCode in ?2 "
                            + "and cp.submissionDate<?3 and cp.submissionDate>=?4 ";


            customerListAPEYTD =
                    findUsingQuery(searchCriteriaRequest, queryAPE_YTD, searchCriteria.getAgentId(), statusListClosed,
                            searchCriteria.getCurrentDate(), searchCriteria.getStartDate());

           

            
            String queryAPE_MTD =
                    "select ISNULL(sum(cp.premium.amount),0) from Customer customer join customer.purchasedProducts cp "
                            + " where cp.premium.amount is not null and customer.agentId=?1 and customer.statusCode in ?2 "
                            + "and cp.submissionDate<?3 and cp.submissionDate>=?4 ";
        

            customerListAPEMTD =
                    findUsingQuery(searchCriteriaRequest, queryAPE_MTD, searchCriteria.getAgentId(), statusListClosed,
                            searchCriteria.getCurrentDate(), searchCriteria.getCurrentMonth());

            String queryLeadAssigned_YTD =
                    "select count(cus.identifier) from Customer cus where cus.agentId =?1 "
                            + "and cus.statusCode in ?2 and cus.creationDateTime<?3 and cus.creationDateTime>=?4 "
                            + "and (cus.branchId IS NOT NULL and cus.branchId<>'')";

            customerListLeadAssignedYTD =
                    findUsingQuery(searchCriteriaRequest, queryLeadAssigned_YTD, searchCriteria.getAgentId(),
                            statusList, searchCriteria.getCurrentDate(), searchCriteria.getStartDate());

            String queryLeadAssigned_MTD =
                    "select count(cus.identifier) from Customer cus where cus.agentId =?1"
                            + " and cus.statusCode in ?2 and cus.creationDateTime<?3 and cus.creationDateTime>=?4 and "
                            + "(cus.branchId IS NOT NULL and cus.branchId<>'')";

            customerListLeadAssignedMTD =
                    findUsingQuery(searchCriteriaRequest, queryLeadAssigned_MTD, searchCriteria.getAgentId(),
                            statusList, searchCriteria.getCurrentDate(), searchCriteria.getCurrentMonth());

            String queryStatus_YTD =
                    "select count(cus.identifier) from Customer cus where cus.agentId =?1 "
                            + "and cus.statusCode in ?2 and cus.creationDateTime<?3 and cus.creationDateTime>=?4";

            customerListClosedYTD =
                    findUsingQuery(searchCriteriaRequest, queryStatus_YTD, searchCriteria.getAgentId(),
                            statusListClosed, searchCriteria.getCurrentDate(), searchCriteria.getStartDate());

            String queryStatus_MTD =
                    "select count(cus.identifier) from Customer cus where cus.agentId =?1 "
                            + "and cus.statusCode in ?2 and cus.creationDateTime<?3 and cus.creationDateTime>=?4";

            customerListClosedMTD =
                    findUsingQuery(searchCriteriaRequest, queryStatus_MTD, searchCriteria.getAgentId(),
                            statusListClosed, searchCriteria.getCurrentDate(), searchCriteria.getCurrentMonth());

        }

        if (customerListAPEYTD.size() > 0) {
            for (Object customerObjAPE : customerListAPEYTD) {
            	String ape_YTDStr = customerObjAPE.toString();
                totalAPE = totalAPE.add(new BigDecimal(ape_YTDStr));
                
                totalAPE=totalAPE.setScale(0);
            }
            searchCountResult.setAPE_YTD(totalAPE.toString());
        }

        if (customerListAPEMTD.size() > 0) {
            for (Object customerObjAPE : customerListAPEMTD) {
                String ape_MTDStr = customerObjAPE.toString();
                totalAPEMTD = totalAPEMTD.add(new BigDecimal(ape_MTDStr));
                
                totalAPEMTD = totalAPEMTD.setScale(0);
            }

            searchCountResult.setAPE_MTD(totalAPEMTD.toString());
        }

        if (customerListLeadAssignedYTD.size() > 0) {
            for (Object customerObjLeadAssigned : customerListLeadAssignedYTD) {
                String assigned_YTD = customerObjLeadAssigned.toString();
                searchCountResult.setLeadAssigned_YTD(assigned_YTD);
            }
        }

        if (customerListLeadAssignedMTD.size() > 0) {
            for (Object customerObjLeadAssigned : customerListLeadAssignedMTD) {
                String assigned_MTD = customerObjLeadAssigned.toString();
                searchCountResult.setLeadAssigned_MTD(assigned_MTD);
            }
        }

        if (customerListClosedYTD.size() > 0) {
            for (Object customerObjLeadClosed : customerListClosedYTD) {
                String closed_YTD = customerObjLeadClosed.toString();
                searchCountResult.setClosed_YTD(closed_YTD);
            }
        }

        if (customerListClosedMTD.size() > 0) {
            for (Object customerObjLeadClosed : customerListClosedMTD) {
                String closed_MTD = customerObjLeadClosed.toString();
                searchCountResult.setClosed_MTD(closed_MTD);
            }
        }

        final List<SearchCountResult> subModeResults = new ArrayList<SearchCountResult>();

        SearchCountResult submode;
        BigDecimal totalModCount = new BigDecimal("0");
        BigDecimal closedCount = new BigDecimal("0");
        BigDecimal newCount = new BigDecimal("0");
        BigDecimal followUpCount = new BigDecimal("0");
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
           
            for (Object searchCriteria : searchCriteriaList) {
                Object[] values = (Object[]) searchCriteria;
                Long countStr = (Long) values[0];
                String count = countStr.toString();
                CustomerStatusCodeList mode = (CustomerStatusCodeList) values[1];

                if (mode.equals(CustomerStatusCodeList.Successful_Sale)) {
                    closedCount = new BigDecimal(count);
                    totalModCount = totalModCount.add(closedCount);
                } else if (mode.equals(CustomerStatusCodeList.New)) {
                    newCount = new BigDecimal(count);
                    totalModCount = totalModCount.add(newCount);
                } else if (mode.equals(CustomerStatusCodeList.App_Submitted)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.Pending_Documents)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.Others)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.ToBeCalled)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.Meeting_Fixed)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.Non_Contactable)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else {
                    totalModCount = totalModCount.add(new BigDecimal(count));
                }
            }
        
            searchCountResult.setNewCount(newCount.toString());
            searchCountResult.setClosedCount(closedCount.toString());
            searchCountResult.setFollowUpCount(followUpCount.toString());
            searchCountResult.setSubModeResults(subModeResults);
        }

        searchCountResult.setCount(String.valueOf(totalModCount));
        searchCountResult.setMode(type);
        searchCountResponse.setType(searchCountResult);

        return searchCountResponse;
    }

    /**
     * Method to get retrieveFilterCustomers
     * @param searchCriteriatRequest
     * @param statusRequest
     * @return customers
     */
    @Override
    public List<Customer> retrieveFilterCustomers(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<CustomerStatusCodeList>> statusRequest) {
        List<Object> customerList = null;
        String query;
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select cus from Customer cus where cus.agentId =?1 and cus.statusCode in ?2 and "
                                + "(cus.transactionKeys.key2 like ?3 or cus.transactionKeys.key4 like ?3) order by cus.modifiedDateTime Desc";
                customerList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType(), "%" + searchCriteria.getValue() + "%");
            } else if (searchCriteria.getBranchId() != null && !("".equals(searchCriteria.getBranchId()))) {
                query = "select cus from Customer cus where cus.statusCode in ?1 and cus.branchId =?2 order by cus.modifiedDateTime Desc";
                customerList =
                        findUsingQuery(searchCriteriatRequest, query, statusRequest.getType(),
                                searchCriteria.getBranchId());
            } else {
                query = "select cus from Customer cus where cus.agentId in ?1 and cus.statusCode in ?2 order by cus.modifiedDateTime Desc";
                customerList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType());
            }
        }

        final List<Customer> customers = new ArrayList<Customer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object customerObj : customerList) {
                customers.add((Customer) customerObj);
            }
        }
        return customers;
    }
    
    @Override
    public Map<String,Integer> retrieveMonthlySalesActivityCustomers(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<CustomerStatusCodeList>> statusRequest, Request<List<CommunicationStatusCodeList>> communicationStatusRequest,
            Request<List<CommunicationInteractionTypeCodeList>> communicationInteractionCodeRequest) {
        List<Object[]> customerList = null;
        String query;
        SimpleDateFormat dateFormatter = new SimpleDateFormat(GeneraliConstants.DATEFORMAT_YYYYMMDD);
        
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            List<String> agentIDList = searchCriteria.getSelectedIds();
            
            StringBuilder agentCode = new StringBuilder();
            for (String agentId : agentIDList) {
                agentCode.append("'").append(agentId).append("'").append(",");

            }
            agentCode.deleteCharAt(agentCode.length() - 1); 
            
			if (!searchCriteria.getSelectedIds().isEmpty()
					&& searchCriteria.getType().equals(GeneraliConstants.CALL)) {
				query = "select DATEDIFF(week,0,c.communicationStatusDate) - (DATEDIFF(week,0,DATEADD(dd, -DAY(c.communicationStatusDate)+1, c.communicationStatusDate))-1) as redDate,count(*) as reqCount from PartyRoleInRelationship cus "
						+ "join PartyRoleInRelationship_Communication pc on cus.id = pc.PartyRoleInRelationship_id join communication_communication cc on cc.communication_id = pc.receivesCommunication_id "
						+ "join communication c on c.id=cc.followsUp_id where cus.agentId in (" +agentCode.toString() + ") and cus.statusCode = '20' and c.communicationStatusCode in ('23', '24', '25') and c.interactionType = '4' and "
						+ "c.communicationStatusDate between ?1 and ?2 group by DATEDIFF(week,0,c.communicationStatusDate) - (DATEDIFF(week,0,DATEADD(dd, -DAY(c.communicationStatusDate)+1, c.communicationStatusDate))-1) "
						+ "order by DATEDIFF(week,0,c.communicationStatusDate) - (DATEDIFF(week,0,DATEADD(dd, -DAY(c.communicationStatusDate)+1, c.communicationStatusDate))-1)";
				customerList =
                		findUsingNativeQuery(searchCriteriatRequest, query,
                		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
            } 
            
            else if (!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(GeneraliConstants.VISIT)) {
            	query = "select DATEDIFF(week,0,c.communicationStatusDate) - (DATEDIFF(week,0,DATEADD(dd, -DAY(c.communicationStatusDate)+1, c.communicationStatusDate))-1) as redDate,count(*) as reqCount from "+ "PartyRoleInRelationship cus "
            			+"join PartyRoleInRelationship_Communication pc on cus.id = pc.PartyRoleInRelationship_id join communication_communication cc on cc.communication_id = pc.receivesCommunication_id "
            			+"join communication c on c.id=cc.followsUp_id where cus.agentId in (" +agentCode.toString() + ") and cus.statusCode = '20' and c.communicationStatusCode ='27' and " +"c.interactionType = '5' and "
            			+"c.communicationStatusDate between ?1 and ?2 group by DATEDIFF(week,0,c.communicationStatusDate) - (DATEDIFF(week,0,DATEADD(dd, -DAY(c.communicationStatusDate)+1, c.communicationStatusDate))-1) "
            			+"order by DATEDIFF(week,0,c.communicationStatusDate) - (DATEDIFF(week,0,DATEADD(dd, -DAY(c.communicationStatusDate)+1, c.communicationStatusDate))-1)";
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
                } 
			
            else if (!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(GeneraliConstants.APPOINTMENT)) {
            	query = "select DATEDIFF(week,0,c.communicationStatusDate) - (DATEDIFF(week,0,DATEADD(dd, -DAY(c.communicationStatusDate)+1, c.communicationStatusDate))-1) as redDate,count(*) as reqCount from PartyRoleInRelationship cus "
						+ "join PartyRoleInRelationship_Communication pc on cus.id = pc.PartyRoleInRelationship_id join communication_communication cc on cc.communication_id = pc.receivesCommunication_id "
						+ "join communication c on c.id=cc.followsUp_id where cus.agentId in (" +agentCode.toString() + ") and cus.statusCode = '20' and c.interactionType = '6' and "
						+ "c.communicationStatusDate between ?1 and ?2 group by DATEDIFF(week,0,c.communicationStatusDate) - (DATEDIFF(week,0,DATEADD(dd, -DAY(c.communicationStatusDate)+1, c.communicationStatusDate))-1) "
						+ "order by DATEDIFF(week,0,c.communicationStatusDate) - (DATEDIFF(week,0,DATEADD(dd, -DAY(c.communicationStatusDate)+1, c.communicationStatusDate))-1)";;
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
                } 
			
            else if (!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(GeneraliConstants.PRESENTATION)) {
            	query = "select DATEDIFF(week,0,CONVERT(VARCHAR(10),agr.creationDateTime,126)) - (DATEDIFF(week,0,DATEADD(dd, -DAY(CONVERT(VARCHAR(10),agr.creationDateTime,126))+1, CONVERT(VARCHAR(10),agr.creationDateTime,126)))-1) as redDate,count(agr.id) as reqCount  "
                        + "from Agreement agr inner join InsuranceAgreement ig on (agr.id=ig.id) where ig.agentId in (" +agentCode.toString() + ")  and agr.statusCode in ('10','11') "
                        + "and agr.contextId = '2' and CONVERT(VARCHAR(10),agr.creationDateTime,126) between ?1 and ?2 group by DATEDIFF(week,0,CONVERT(VARCHAR(10),agr.creationDateTime,126)) - (DATEDIFF(week,0,DATEADD(dd, -DAY(CONVERT(VARCHAR(10),agr.creationDateTime,126))+1, CONVERT(VARCHAR(10),agr.creationDateTime,126)))-1) "
                        +"order by DATEDIFF(week,0,CONVERT(VARCHAR(10),agr.creationDateTime,126)) - (DATEDIFF(week,0,DATEADD(dd, -DAY(CONVERT(VARCHAR(10),agr.creationDateTime,126))+1, CONVERT(VARCHAR(10),agr.creationDateTime,126)))-1)";
            	customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query, dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
                }
			
            else if (!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(ContextTypeCodeList.EAPP.toString())) {
            	query = "select DATEDIFF(week,0,CONVERT(VARCHAR(10),agr.creationDateTime,126)) - (DATEDIFF(week,0,DATEADD(dd, -DAY(CONVERT(VARCHAR(10),agr.creationDateTime,126))+1, CONVERT(VARCHAR(10),agr.creationDateTime,126)))-1) as redDate,count(agr.id) as reqCount  "
                        + "from Agreement agr inner join InsuranceAgreement ig on (agr.id=ig.id) where ig.agentId in (" +agentCode.toString() + ")  and agr.statusCode = '15' "
                        + "and agr.contextId = '4' and CONVERT(VARCHAR(10),agr.creationDateTime,126) between ?1 and ?2 group by DATEDIFF(week,0,CONVERT(VARCHAR(10),agr.creationDateTime,126)) - (DATEDIFF(week,0,DATEADD(dd, -DAY(CONVERT(VARCHAR(10),agr.creationDateTime,126))+1, CONVERT(VARCHAR(10),agr.creationDateTime,126)))-1) "
                        +"order by DATEDIFF(week,0,CONVERT(VARCHAR(10),agr.creationDateTime,126)) - (DATEDIFF(week,0,DATEADD(dd, -DAY(CONVERT(VARCHAR(10),agr.creationDateTime,126))+1, CONVERT(VARCHAR(10),agr.creationDateTime,126)))-1)";
            	customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query, dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
                }
            
        }


        Map<String,Integer> monthlyCount = new LinkedHashMap<String,Integer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object[] customerObj : customerList) {
            	monthlyCount.put(customerObj[0].toString(), (Integer)customerObj[1]);
            }
        }
        return monthlyCount;
    }
    
    @Override
    public List<Customer> retrieveMonthlyNewProspect(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<CustomerStatusCodeList>> statusRequest) {
        List<Object> customerList = null;
        String query;
        
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            List<String> agentIDList = searchCriteria.getSelectedIds();
			if (!searchCriteria.getSelectedIds().isEmpty()) {
				query = "select cus from Customer cus where cus.agentId in ?1 and cus.statusCode in "
						+ GeneraliConstants.SALES_ACTIVITY_COUNT_STATUS
						+ " and cus.creationDateTime between ?2 and ?3";
				customerList = findUsingQuery(searchCriteriatRequest, query, agentIDList, searchCriteria.getStartDate(),
						searchCriteria.getCurrentDate());
            } 
            
            
        }


        final List<Customer> customers = new ArrayList<Customer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object customerObj : customerList) {
                customers.add((Customer) customerObj);
            }
        }
        return customers;
    }
    
    @Override
    public Map<String,Integer> retrieveMonthwiseSalesActivityCustomers(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<CustomerStatusCodeList>> statusRequest) {
        List<Object[]> customerList = null;
        String query;
        SimpleDateFormat dateFormatter = new SimpleDateFormat(GeneraliConstants.DATEFORMAT_YYYYMMDD);
        
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            List<String> agentIDList = searchCriteria.getSelectedIds();
			
			StringBuilder agentCode = new StringBuilder();
            for (String agentId : agentIDList) {
                agentCode.append("'").append(agentId).append("'").append(",");

            }
            agentCode.deleteCharAt(agentCode.length() - 1); 
			
            if(!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(GeneraliConstants.APPOINTMENT)){
        		query ="select DATEPART(MONTH, CONVERT(VARCHAR(10),cus.modifiedDateTime,126)) as redDate,count(*) as reqCount "
        				+ "from PartyRoleInRelationship cus join PartyRoleInRelationship_Communication pc on cus.id = pc.PartyRoleInRelationship_id"
        				+ " join communication_communication cc on cc.communication_id = pc.receivesCommunication_id join communication c on c.id=cc.followsUp_id "
        				+ "where cus.agentId in (" +agentCode.toString() + ") and cus.statusCode = '20' "
        				+ "and c.interactionType = '6' and cus.modifiedDateTime between ?1 and ?2 "
        				+ " group by DATEPART(MONTH, CONVERT(VARCHAR(10),cus.modifiedDateTime,126)) "
        				+ "order by DATEPART(MONTH, CONVERT(VARCHAR(10),cus.modifiedDateTime,126))";
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
        	}
        	
        	else if(!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(GeneraliConstants.CALL)){
        		query ="select DATEPART(MONTH, c.communicationStatusDate) as redDate,count(*) as reqCount "
        				+ "from PartyRoleInRelationship cus join PartyRoleInRelationship_Communication pc on cus.id = pc.PartyRoleInRelationship_id "
        				+ "join communication_communication cc on cc.Communication_id = pc.receivesCommunication_id join communication c on c.id=cc.followsUp_id "
        				+ "where cus.agentId in (" +agentCode.toString() + ") and cus.statusCode ='20' "
        				+ "and c.interactionType = '4' "
        				+ "and c.communicationStatusCode in ('23', '24', '25') "
        				+ "and c.communicationStatusDate between ?1 and ?2 "
        				+ "group by DATEPART(MONTH, c.communicationStatusDate) "
        				+ "order by DATEPART(MONTH, c.communicationStatusDate)";
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
        	}
        	
        	else if(!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(GeneraliConstants.VISIT)){
        		query ="select DATEPART(MONTH, c.communicationStatusDate) as redDate,count(*) as reqCount "
        				+ "from PartyRoleInRelationship cus join PartyRoleInRelationship_Communication pc on cus.id = pc.PartyRoleInRelationship_id "
        				+ "join communication_communication cc on cc.Communication_id = pc.receivesCommunication_id join communication c on c.id=cc.followsUp_id "
        				+ "where cus.agentId in (" +agentCode.toString() + ") and cus.statusCode ='20' and c.interactionType = '5' "
        				+ "and c.communicationStatusCode = '27' "
        				+ "and c.communicationStatusDate between ?1 and ?2 "
        				+ "group by DATEPART(MONTH, c.communicationStatusDate) "
        				+ "order by DATEPART(MONTH, c.communicationStatusDate)";
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
        	}


        //final List<Customer> customers = new ArrayList<Customer>();
        Map<String,Integer> monthwiseCount = new HashMap<String,Integer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object[] customerObj : customerList) {
            	monthwiseCount.put(customerObj[0].toString(), (Integer)customerObj[1]);
            }
        }
        return monthwiseCount;
    }
    
    @Override
    public Map<String,Integer> retrieveWeekwiseSalesActivityCustomers(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<CustomerStatusCodeList>> statusRequest) {
        List<Object[]> customerList = null;
        String query;
        SimpleDateFormat dateFormatter = new SimpleDateFormat(GeneraliConstants.DATEFORMAT_YYYYMMDD);
        
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            List<String> agentIDList = searchCriteria.getSelectedIds();
			
			StringBuilder agentCode = new StringBuilder();
            for (String agentId : agentIDList) {
                agentCode.append("'").append(agentId).append("'").append(",");

            }
            agentCode.deleteCharAt(agentCode.length() - 1); 
			
            if(!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(GeneraliConstants.APPOINTMENT)){
        		query ="select c.communicationStatusDate as redDate,count(*) as reqCount from PartyRoleInRelationship cus join PartyRoleInRelationship_Communication pc "
        				+ "on cus.id = pc.PartyRoleInRelationship_id join communication_communication cc on cc.Communication_id = pc.receivesCommunication_id "
        				+ "join communication c on c.id = cc.followsUp_id "
        				+ "where cus.agentId in (" +agentCode.toString() + ") and cus.statusCode = '20' and c.interactionType = '6' " 
        				+ "and c.communicationStatusDate between ?1 and ?2 "
        				+ "group by c.communicationStatusDate order by c.communicationStatusDate";
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
        	}
        	
        	else if(!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(GeneraliConstants.CALL)){
        		query ="select c.communicationStatusDate as redDate,count(*) as reqCount from PartyRoleInRelationship cus join PartyRoleInRelationship_Communication pc "
        				+ "on cus.id = pc.PartyRoleInRelationship_id join communication_communication cc on cc.Communication_id = pc.receivesCommunication_id "
        				+ "join communication c on c.id = cc.followsUp_id "
        				+ "where cus.agentId in (" +agentCode.toString() + ") and cus.statusCode = '20' and c.interactionType = '4' "
        				+ "and c.communicationStatusCode in ('23', '24', '25') " 
        				+ "and c.communicationStatusDate between ?1 and ?2 "
        				+ "group by c.communicationStatusDate order by c.communicationStatusDate";
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
        	}
        	
        	else if(!searchCriteria.getSelectedIds().isEmpty() && searchCriteria.getType().equals(GeneraliConstants.VISIT)){
        		query ="select c.communicationStatusDate as redDate,count(*) as reqCount from PartyRoleInRelationship cus join PartyRoleInRelationship_Communication pc "
        				+ "on cus.id = pc.PartyRoleInRelationship_id join communication_communication cc on cc.Communication_id = pc.receivesCommunication_id "
        				+ "join communication c on c.id = cc.followsUp_id "
        				+ "where cus.agentId in (" +agentCode.toString() + ") and cus.statusCode = '20' and c.interactionType = '5' " 
        				+ "and c.communicationStatusCode = '27' " 
        				+ "and c.communicationStatusDate between ?1 and ?2 "
        				+ "group by c.communicationStatusDate order by c.communicationStatusDate";
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
        	}


        //final List<Customer> customers = new ArrayList<Customer>();
        Map<String,Integer> monthwiseCount = new HashMap<String,Integer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object[] customerObj : customerList) {
            	monthwiseCount.put(customerObj[0].toString(), (Integer)customerObj[1]);
            }
        }
        return monthwiseCount;
    }
    
    @Override
    public Map<String,Integer> retrieveNewProspect(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<CustomerStatusCodeList>> statusRequest) {
        List<Object[]> customerList = null;
        String query;
        SimpleDateFormat dateFormatter = new SimpleDateFormat(GeneraliConstants.DATEFORMAT_YYYYMMDD);
        
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            List<String> agentIDList = searchCriteria.getSelectedIds();
			
			StringBuilder agentCode = new StringBuilder();
            for (String agentId : agentIDList) {
                agentCode.append("'").append(agentId).append("'").append(",");

            }
            agentCode.deleteCharAt(agentCode.length() - 1); 
			
            if(!searchCriteria.getSelectedIds().isEmpty()){
        		query ="select CONVERT(VARCHAR(10),creationDateTime,126) as creationDate,count(*) as leadCount " 
        				+ "from partyRoleInRelationship where agentId in (" +agentCode.toString() + ") "
        				+ "and statusCode = '20' "
        				+ "and CONVERT(VARCHAR(10),creationDateTime,126) between ?1 and ?2 "
        				+ "group by CONVERT(VARCHAR(10),creationDateTime,126) order by CONVERT(VARCHAR(10),creationDateTime,126)";
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
        	}


        //final List<Customer> customers = new ArrayList<Customer>();
        Map<String,Integer> weekwiseCount = new HashMap<String,Integer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object[] customerObj : customerList) {
            	weekwiseCount.put(customerObj[0].toString(), (Integer)customerObj[1]);
            }
        }
        return weekwiseCount;
    }
    
    @Override
    public Map<String,Integer> retrieveMonthwiseNewProspect(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<CustomerStatusCodeList>> statusRequest) {
        List<Object[]> customerList = null;
        String query;
        SimpleDateFormat dateFormatter = new SimpleDateFormat(GeneraliConstants.DATEFORMAT_YYYYMMDD);
        
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            List<String> agentIDList = searchCriteria.getSelectedIds();
			
			StringBuilder agentCode = new StringBuilder();
            for (String agentId : agentIDList) {
                agentCode.append("'").append(agentId).append("'").append(",");

            }
            agentCode.deleteCharAt(agentCode.length() - 1);
			
            if(!searchCriteria.getSelectedIds().isEmpty()){
        		query ="select DATEPART(MONTH, CONVERT(VARCHAR(10),creationDateTime,126)) as creationDate,count(*) as leadCount " 
        				+"from partyRoleInRelationship where agentId in (" +agentCode.toString() + ") and statusCode = '20' and CONVERT(VARCHAR(10),creationDateTime,126) between ?1 " 
        				+"and ?2 group by DATEPART(MONTH, CONVERT(VARCHAR(10),creationDateTime,126)) order by DATEPART(MONTH, CONVERT(VARCHAR(10),creationDateTime,126))";
                    customerList =
                    		findUsingNativeQuery(searchCriteriatRequest, query,
                    		        dateFormatter.format(searchCriteria.getStartDate()),dateFormatter.format(searchCriteria.getCurrentDate()));
        	}


        //final List<Customer> customers = new ArrayList<Customer>();
        Map<String,Integer> monthwiseCount = new HashMap<String,Integer>();
        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object[] customerObj : customerList) {
            	monthwiseCount.put(customerObj[0].toString(), (Integer)customerObj[1]);
            }
        }
        return monthwiseCount;
    }
    
    protected List<Object[]> findUsingNativeQuery(Request request, String queryString, Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createNativeQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<Object[]>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
    /**
     * Method to get customer with TransTrackingId
     * @param trackIdRequest
     * @param statusRequest
     * @return response
     */
    public Response<Customer> retrieveCustomerWithTransTrackId(Request<String> trackIdRequest,
            Request<List<CustomerStatusCodeList>> statusRequest) {
        Response<Customer> response = new ResponseImpl<Customer>();
        Customer customer = null;
        String query = "select cus from Customer cus where cus.transTrackingId =?1 and cus.statusCode in ?2";
        List<Object> customerList = findUsingQuery(trackIdRequest, query, trackIdRequest.getType(),statusRequest.getType());
        if (CollectionUtils.isNotEmpty(customerList)) {
            customer = (Customer) customerList.get(0);
        }
        response.setType(customer);
        return response;
    }
    /**
     * Method to get getRetrieveFilterCountBranchUser
     * @param searchCriteriaRequest
     * @param statusRequest
     * @param statusListClosed
     * @return searchCountResponse
     */
    @Override
    public Response<SearchCountResult> getRetrieveFilterCountBranchUser(Request<SearchCriteria> searchCriteriaRequest,
            List<CustomerStatusCodeList> statusRequest, List<CustomerStatusCodeList> statusListClosed) {
        final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
        final SearchCountResult searchCountResult = new SearchCountResult();
        List<Object> searchCriteriaList = null;
        List<Object> customerListAPEYTD = new ArrayList<Object>();
        List<Object> customerListAPEMTD = new ArrayList<Object>();
        List<Object> customerListClosedYTD = new ArrayList<Object>();
        List<Object> customerListClosedMTD = new ArrayList<Object>();
        List<Object> customerListLeadAssignedYTD = new ArrayList<Object>();
        List<Object> customerListLeadAssignedMTD = new ArrayList<Object>();
        BigDecimal apeYTD = new BigDecimal("0");
        BigDecimal apeMTD = new BigDecimal("0");
        BigDecimal leadAssignedYTD = new BigDecimal("0");
        BigDecimal leadAssignedMTD = new BigDecimal("0");
        BigDecimal leadClosedYTD = new BigDecimal("0");
        BigDecimal leadClosedMTD = new BigDecimal("0");

        String type = null;
        String query;
        if (searchCriteriaRequest != null && searchCriteriaRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriaRequest.getType();
            type = searchCriteria.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select count(customer.identifier),customer.statusCode from Customer customer where "
                                + "customer.statusCode in ?1 and customer.branchId=?2 and "
                                + "(customer.transactionKeys.key2 like ?3 or customer.transactionKeys.key4 like ?3) "
                                + "group by customer.statusCode";
                searchCriteriaList =
                        findUsingQuery(searchCriteriaRequest, query, statusRequest, searchCriteria.getBranchId(), "%"
                                + searchCriteria.getValue() + "%");

            } else {
                query =
                        "select count(customer.identifier), customer.statusCode from Customer customer where "
                                + "customer.statusCode in ?1 and customer.branchId=?2 "
                                + "group by customer.statusCode";

                searchCriteriaList =
                        findUsingQuery(searchCriteriaRequest, query, statusRequest, searchCriteria.getBranchId());
            }
            
            String queryAPE_YTD =
                    "select ISNULL(sum(cp.premium.amount),0) from Customer customer join customer.purchasedProducts cp "
                            + " where cp.premium.amount is not null and customer.branchId=?1 and customer.statusCode in ?2 "
                            + "and cp.submissionDate<=?3 and cp.submissionDate>=?4 ";

            customerListAPEYTD =
                    findUsingQuery(searchCriteriaRequest, queryAPE_YTD, searchCriteria.getBranchId(), statusListClosed,
                            searchCriteria.getCurrentDate(), searchCriteria.getStartDate());

            String queryAPE_MTD =
                    "select ISNULL(sum(cp.premium.amount),0) from Customer customer join customer.purchasedProducts cp "
                            + " where cp.premium.amount is not null and customer.branchId=?1 and customer.statusCode in ?2 "
                            + "and cp.submissionDate<=?3 and cp.submissionDate>=?4 ";


            customerListAPEMTD =
                    findUsingQuery(searchCriteriaRequest, queryAPE_MTD, searchCriteria.getBranchId(), statusListClosed,
                            searchCriteria.getCurrentDate(), searchCriteria.getCurrentMonth());

            String queryLeadAssigned_YTD =
                    "select count(cus.identifier) from Customer cus where cus.statusCode in ?1 and cus.creationDateTime<?2 and cus.creationDateTime>=?3 and cus.branchId=?4";

            customerListLeadAssignedYTD =
                    findUsingQuery(searchCriteriaRequest, queryLeadAssigned_YTD, statusRequest,
                            searchCriteria.getCurrentDate(), searchCriteria.getStartDate(),
                            searchCriteria.getBranchId());

            String queryLeadAssigned_MTD =
                    "select count(cus.identifier) from Customer cus where cus.statusCode in ?1 and cus.creationDateTime<?2 and cus.creationDateTime>=?3 and cus.branchId=?4";

            customerListLeadAssignedMTD =
                    findUsingQuery(searchCriteriaRequest, queryLeadAssigned_MTD, statusRequest,
                            searchCriteria.getCurrentDate(), searchCriteria.getCurrentMonth(),
                            searchCriteria.getBranchId());

            String queryStatus_YTD =
                    "select count(cus.identifier) from Customer cus where cus.statusCode in ?1 and cus.creationDateTime<?2 and cus.creationDateTime>=?3 and cus.branchId=?4";

            customerListClosedYTD =
                    findUsingQuery(searchCriteriaRequest, queryStatus_YTD, statusListClosed,
                            searchCriteria.getCurrentDate(), searchCriteria.getStartDate(),
                            searchCriteria.getBranchId());

            String queryStatus_MTD =
                    "select count(cus.identifier) from Customer cus where cus.statusCode in ?1 and cus.creationDateTime<?2 and cus.creationDateTime>=?3 and cus.branchId=?4";

            customerListClosedMTD =
                    findUsingQuery(searchCriteriaRequest, queryStatus_MTD, statusListClosed,
                            searchCriteria.getCurrentDate(), searchCriteria.getCurrentMonth(),
                            searchCriteria.getBranchId());

        }

        if (customerListAPEYTD.size() > 0) {
            for (Object customerObjAPE : customerListAPEYTD) {
                String ape_YTDStr = customerObjAPE.toString();
               
                apeYTD = apeYTD.add(new BigDecimal(ape_YTDStr));
                apeYTD = apeYTD.setScale(0);
            }

            searchCountResult.setAPE_YTD(apeYTD.toString());
        }

        if (customerListAPEMTD.size() > 0) {
            for (Object customerObjAPE : customerListAPEMTD) {
                String ape_MTDStr = customerObjAPE.toString();
                // apeMTD = apeMTD + ape_MTD;
                apeMTD = apeMTD.add(new BigDecimal(ape_MTDStr));
                apeMTD = apeMTD.setScale(0);
            }

            searchCountResult.setAPE_MTD(apeMTD.toString());
        }

        if (customerListLeadAssignedYTD.size() > 0) {
            for (Object customerObjLeadAssigned : customerListLeadAssignedYTD) {
                String assigned_YTD = customerObjLeadAssigned.toString();
                leadAssignedYTD = leadAssignedYTD.add(new BigDecimal(assigned_YTD));
                searchCountResult.setLeadAssigned_YTD(leadAssignedYTD.toString());
            }
        }

        if (customerListLeadAssignedMTD.size() > 0) {
            for (Object customerObjLeadAssigned : customerListLeadAssignedMTD) {
                String assigned_MTD = customerObjLeadAssigned.toString();
                leadAssignedMTD = leadAssignedMTD.add(new BigDecimal(assigned_MTD));
                searchCountResult.setLeadAssigned_MTD(leadAssignedMTD.toString());
            }
        }

        if (customerListClosedYTD.size() > 0) {
            for (Object customerObjLeadClosed : customerListClosedYTD) {
                String closed_YTD = customerObjLeadClosed.toString();
                leadClosedYTD = leadClosedYTD.add(new BigDecimal(closed_YTD));
                searchCountResult.setClosed_YTD(leadClosedYTD.toString());
            }
        }

        if (customerListClosedMTD.size() > 0) {
            for (Object customerObjLeadClosed : customerListClosedMTD) {
                String closed_MTD = customerObjLeadClosed.toString();
                leadClosedMTD = leadClosedMTD.add(new BigDecimal(closed_MTD));
                searchCountResult.setClosed_MTD(leadClosedMTD.toString());
            }
        }

        final List<SearchCountResult> subModeResults = new ArrayList<SearchCountResult>();

        SearchCountResult submode;
        BigDecimal totalModCount = new BigDecimal("0");
        BigDecimal closedCount = new BigDecimal("0");
        BigDecimal newCount = new BigDecimal("0");
        BigDecimal followUpCount = new BigDecimal("0");
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
           
            for (Object searchCriteria : searchCriteriaList) {
                Object[] values = (Object[]) searchCriteria;
                Long count = (Long) values[0];
                CustomerStatusCodeList mode = (CustomerStatusCodeList) values[1];
                if (mode.equals(CustomerStatusCodeList.Successful_Sale)) {
                    closedCount = new BigDecimal(count);
                    totalModCount = totalModCount.add(closedCount);
                } else if (mode.equals(CustomerStatusCodeList.New)) {
                    newCount = new BigDecimal(count);
                    totalModCount = totalModCount.add(newCount);
                } else if (mode.equals(CustomerStatusCodeList.App_Submitted)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.Pending_Documents)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.Others)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.ToBeCalled)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.Meeting_Fixed)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else if (mode.equals(CustomerStatusCodeList.Non_Contactable)) {
                    followUpCount = followUpCount.add(new BigDecimal(count));
                    totalModCount = totalModCount.add(new BigDecimal(count));
                } else {
                    totalModCount = totalModCount.add(new BigDecimal(count));
                }
            }
           
            searchCountResult.setNewCount(newCount.toString());
            searchCountResult.setClosedCount(closedCount.toString());
            searchCountResult.setFollowUpCount(followUpCount.toString());
            searchCountResult.setSubModeResults(subModeResults);
        }

        searchCountResult.setCount(String.valueOf(totalModCount));
        searchCountResult.setMode(type);
        searchCountResponse.setType(searchCountResult);

        return searchCountResponse;
    }

    /**
     * Method to get Total Agent Count - YTD
     * 
     * @param selectedAgentIdData
     * @return count
     */
    public int getTotalAgentCountYTD(ArrayList<SelectedAgentData> selectedAgentIdData) {
        String pattern = "yyyy-MM-dd";
        DateFormat dateFormat = null;
        Date startDateOfYear = null;
        Calendar cal = null;

        Date currentDate = null;
        String strCurrentDate = null;
        int count = 0;
        try {
            dateFormat = new SimpleDateFormat(pattern);

            // Get current Date - Start
            currentDate = new Date();
            strCurrentDate = dateFormat.format(currentDate);
            currentDate = dateFormat.parse(strCurrentDate);
            // Get current Date - End

            // Get the start date of the current year - Start
            cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.DAY_OF_YEAR, 1);
            String formatted = dateFormat.format(cal.getTime());
            startDateOfYear = dateFormat.parse(formatted);

            // Get the start date of the current year - End

            for (SelectedAgentData objSelectedAgents : selectedAgentIdData) {

                String strJoiningDate = null;
                String strTerminationDate = null;
                Date joiningDate = null;
                Date terminationDate = null;
                if (null != objSelectedAgents.getJoiningDate()) {
                    strJoiningDate = dateFormat.format(objSelectedAgents.getJoiningDate());
                    // Joining Date
                    joiningDate = dateFormat.parse(strJoiningDate);

                    if (null != objSelectedAgents.getTerminationDate()) {
                        strTerminationDate = dateFormat.format(objSelectedAgents.getTerminationDate());
                        // Termination Date
                        terminationDate = dateFormat.parse(strTerminationDate);
                    }

                    Calendar startDate = Calendar.getInstance();
                    Calendar endDate = Calendar.getInstance();
                    int diff = 0;
                    if (joiningDate.before(startDateOfYear)) {

                        startDate.setTime(startDateOfYear);
                        endDate.setTime(currentDate);
                        if (null != terminationDate) {
                            if (terminationDate.before(startDateOfYear)) {
                                diff = 0;
                            } else {
                                endDate.setTime(terminationDate);
                                diff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH) + 1;
                            }
                        } else {

                            diff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH) + 1;
                        }
                        count = count + diff;

                    } else {

                        startDate.setTime(joiningDate);
                        endDate.setTime(currentDate);

                        if (null != terminationDate) {
                            if (terminationDate.before(startDateOfYear)) {
                                diff = 0;
                            } else {
                                endDate.setTime(terminationDate);
                                diff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH) + 1;
                            }
                        } else {
                            diff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH) + 1;
                        	}

                        count = count + diff;
                    }
                }
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception while calculating TotalAgentCountYTD", e);
        }
        return count;
    }
    
    /**
   	 * Method to get Total Agent Count - MTD 
   	 * @param selectedAgentIdData
   	 * @return count
   	 */
       public int getTotalAgentCountMTD(ArrayList<SelectedAgentData> selectedAgentIdData) {
           String pattern = "yyyy-MM-dd";
           DateFormat dateFormat = null;
           Date startDateOfMonth = null;
           Calendar cal = null;
          
           Date currentDate = null;
           String strCurrentDate = null;
           int count = 0;
           try {
               dateFormat = new SimpleDateFormat(pattern);

               // Get current Date - Start
               currentDate = new Date();
               strCurrentDate = dateFormat.format(currentDate);
               currentDate = dateFormat.parse(strCurrentDate);
               // Get current Date - End

               // Get the start date of the current month - Start
               cal = Calendar.getInstance();
               int month = cal.get(Calendar.MONTH);
               cal.set(Calendar.MONTH, month);
               cal.set(Calendar.DAY_OF_MONTH, 1);
               String formatted = dateFormat.format(cal.getTime());
               startDateOfMonth = dateFormat.parse(formatted);

               // Get the start date of the current year - End

               for (SelectedAgentData objSelectedAgents : selectedAgentIdData) {
                   
                   String strJoiningDate = null;
                   String strTerminationDate = null;
                   Date joiningDate = null;
                   Date terminationDate = null;
                   if (null != objSelectedAgents.getJoiningDate()) {
                       strJoiningDate = dateFormat.format(objSelectedAgents.getJoiningDate());
                       //Joining Date
                       joiningDate = dateFormat.parse(strJoiningDate);
                       
                       if (null != objSelectedAgents.getTerminationDate()) {
                           strTerminationDate = dateFormat.format(objSelectedAgents.getTerminationDate());
                           //Termination Date
                           terminationDate = dateFormat.parse(strTerminationDate);
                       }

                       Calendar startDate = Calendar.getInstance();
                       Calendar endDate = Calendar.getInstance();
                       int diff = 0 ;
                       if (joiningDate.before(startDateOfMonth)) {

                           startDate.setTime(startDateOfMonth);
                           endDate.setTime(currentDate);
                           if (null != terminationDate) {
                               if (terminationDate.before(startDateOfMonth)) {
                                   diff = 0;
                               } else {
                                   endDate.setTime(terminationDate);
                                   diff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH) + 1;
                               }
                           } else {

                               diff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH) + 1;
                           }
                           count = count + diff;
                          
                       } else {

                           startDate.setTime(joiningDate);
                           endDate.setTime(currentDate);

                           if (null != terminationDate) {
                               if (terminationDate.before(startDateOfMonth)) {
                               	diff = 0;
                               } else {
                                   endDate.setTime(terminationDate);
                                   diff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH) + 1;
                               }
                           }else{
                           	diff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH) + 1;
                           }
                           
                           count = count + diff;
                          
                       }
                   }
                  

               }
           

           } catch (Exception e) {
               LOGGER.error("Exception while calculating TotalAgentCountYTD",e);
           }
           return count;
       }
     
       @Override
       public Response<SearchCountResult> getCustomerCount(Request<SearchCriteria> searchCriteriatRequest,
               final Request<List<CustomerStatusCodeList>> statusRequest) {
           final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
           List<Object> searchCriteriaList = null;
           List<Object>  appointmentList = null ;
           String type = null;
           String query;
           String queryForCommunication;
           if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
               SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
               type = searchCriteria.getType();
               if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                   query =
                           "select count(customer.identifier),customer.statusCode from Customer customer where customer.agentId =?1 "
                                   + "and customer.statusCode in ?2 and "
                                   + "(customer.transactionKeys.key2 like ?3 or customer.transactionKeys.key4 like ?3 or customer.transactionKeys.key26 like ?3) "
                                   + "group by customer.statusCode";
                   
                   queryForCommunication = "select count(customer.identifier),test.interactionType from Customer customer join customer.receivesCommunication test join test.followsUp followup where followup.interactionType= '6' " 
      						+ "and customer.agentId =?1 and "
      						+ "(customer.transactionKeys.key2 like ?2 or customer.transactionKeys.key4 like ?2 or customer.transactionKeys.key26 like ?2) "
      						+ "and MONTH(followup.communicationStatusDate) = MONTH(GETDATE()) and YEAR(followup.communicationStatusDate) = YEAR(GETDATE()) and customer.statusCode=20 " + "group by test.interactionType";
                   
                   searchCriteriaList = findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                   statusRequest.getType(), "%" + searchCriteria.getValue() + "%");
                   
                   appointmentList = findUsingQuery(searchCriteriatRequest, queryForCommunication,searchCriteria.getAgentId(), "%" + searchCriteria.getValue() + "%");

               } else {
                   query =
                           "select count(customer.identifier), customer.statusCode from Customer customer where customer.agentId =?1 "
                                   + "and customer.statusCode in ?2 and YEAR(customer.creationDateTime) = YEAR(GETDATE())"
                           		+ "and MONTH(customer.creationDateTime) = MONTH(GETDATE())" + "group by customer.statusCode";
                   
                   queryForCommunication = "select count(customer.identifier),test.interactionType from Customer customer join customer.receivesCommunication test join test.followsUp followup where followup.interactionType= '6' " 
                   						+ "and customer.agentId =?1 "
                   						+ "and YEAR(followup.communicationStatusDate) = YEAR(GETDATE()) "
                   						+ "and MONTH(followup.communicationStatusDate) = MONTH(GETDATE()) "
                   						+ "and customer.statusCode=20 " + "group by test.interactionType";

                   searchCriteriaList = findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),statusRequest.getType());
                   appointmentList = findUsingQuery(searchCriteriatRequest, queryForCommunication,searchCriteria.getAgentId());
               }
           }
           final List<SearchCountResult> subModeResults = new ArrayList<SearchCountResult>();
           final SearchCountResult searchCountResult = new SearchCountResult();
           SearchCountResult submode;
           Integer totalModCount = 0;
           if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
               for (Object searchCriteria : searchCriteriaList) {
                   Object[] values = (Object[]) searchCriteria;
                   Long count = (Long) values[0];
                   CustomerStatusCodeList mode = (CustomerStatusCodeList) values[1];
                   submode = new SearchCountResult();
                   submode.setCount(count.toString());
                   submode.setMode(mode.toString());
                   subModeResults.add(submode);
                   totalModCount += Integer.valueOf(submode.getCount());
               }
               
           }
           if(CollectionUtils.isNotEmpty(appointmentList)) {
        	   for(Object appointmentSearchCriteria : appointmentList ){
        		   Object[] values = (Object[]) appointmentSearchCriteria;
        		   Long countOfAppointment = (Long) values[0];
        		   CommunicationInteractionTypeCodeList mode= (CommunicationInteractionTypeCodeList) values[1];
            	   searchCountResult.setNoOfAppointments(countOfAppointment.toString());
            	   submode = new SearchCountResult();
                   submode.setCount(countOfAppointment.toString());
                   submode.setMode(mode.toString());
                   subModeResults.add(submode);
        	   }
           }
           searchCountResult.setSubModeResults(subModeResults);
           searchCountResult.setCount(String.valueOf(totalModCount));
           searchCountResult.setMode(type);
           searchCountResponse.setType(searchCountResult);
           return searchCountResponse;
       }

    @Override
    public Map<String, String> retrieveMonthlyNewCount(Request<SearchCriteria> searchCriteriatRequest) {
        Map<String, String> customerMap = new HashMap<String, String>();
        String query;
        List<Object> customerList = new ArrayList<Object>();
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            List<String> agentIDList = searchCriteria.getAgentCodes();
            if (!searchCriteria.getAgentCodes().isEmpty()) {
                query =
                        "select cus.agentId,count(cus.identifier) from Customer cus where  cus.agentId in ?1 and cus.statusCode in "
                                + GeneraliConstants.SALES_ACTIVITY_COUNT_STATUS
                                + " and cus.creationDateTime between ?2 and ?3  group by cus.agentId";
                customerList =
                        findUsingQuery(searchCriteriatRequest, query, agentIDList, searchCriteria.getStartDate(),
                                searchCriteria.getCurrentDate());
            }

        }

        if (CollectionUtils.isNotEmpty(customerList)) {
            for (Object customerObj : customerList) {
                Object[] values = (Object[]) customerObj;
                String agentCode = values[0].toString();
                String count = values[1].toString();
                customerMap.put(agentCode, count);
            }

        }
        return customerMap;
    }

    @Override
    public Map<String, Map<String, String>> retrieveLeadStatistics(Request<SearchCriteria> searchCriteriatRequest) {
        List<Object[]> customerList = null;
        Map<String, Map<String, String>> customerMap = new HashMap<String, Map<String, String>>();
        Map<String, String> callMap = new HashMap<String, String>();
        Map<String, String> visitMap = new HashMap<String, String>();
        Map<String, String> appointmentMap = new HashMap<String, String>();
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            List<String> agentIDList = searchCriteria.getAgentCodes();
            StringBuilder agentCode = new StringBuilder();
            String query = null;
            for (String agentId : agentIDList) {
                agentCode.append("'").append(agentId).append("'").append(",");

            }
            agentCode.deleteCharAt(agentCode.length() - 1);
            query =
                    "select cus.agentId,count(cus.identifier) from PartyRoleInRelationship cus join PartyRoleInRelationship_Communication pc on cus.id = pc.PartyRoleInRelationship_id "
                            + "join communication_communication cc on cc.communication_id = pc.receivesCommunication_id join communication c on c.id=cc.followsUp_id where cus.agentId in ("
                            + agentCode.toString()
                            + ") and cus.statusCode = '20' and c.communicationStatusCode in ('23', '24', '25') and c.interactionType = '4' and c.communicationStatusDate between ?1 and ?2 group by cus.agentId";
            customerList =
                    findUsingNativeQuery(searchCriteriatRequest, query, searchCriteria.getStartDate(),
                            searchCriteria.getCurrentDate());

            if (CollectionUtils.isNotEmpty(customerList)) {
                for (Object customerObj : customerList) {
                    Object[] values = (Object[]) customerObj;
                    String agentId = values[0].toString();
                    String count = values[1].toString();
                    callMap.put(agentId, count);
                }

            }
            customerMap.put("Call", callMap);
            query =
                    "select cus.agentId,count(cus.identifier) from PartyRoleInRelationship cus join PartyRoleInRelationship_Communication pc on cus.id = pc.PartyRoleInRelationship_id join communication_communication cc on cc.communication_id = pc.receivesCommunication_id join communication c on c.id=cc.followsUp_id where cus.agentId in ("
                            + agentCode.toString()
                            + ") and cus.statusCode = '20' and c.communicationStatusCode ='27' and c.interactionType = '5' and  c.communicationStatusDate between ?1 and ?2 group by cus.agentId";

            customerList = null;
            customerList =
                    findUsingNativeQuery(searchCriteriatRequest, query, searchCriteria.getStartDate(),
                            searchCriteria.getCurrentDate());
            if (CollectionUtils.isNotEmpty(customerList)) {
                for (Object customerObj : customerList) {
                    Object[] values = (Object[]) customerObj;
                    String agentId = values[0].toString();
                    String count = values[1].toString();
                    visitMap.put(agentId, count);
                }

            }
            customerMap.put("Visit", visitMap);

            query =
                    "select cus.agentId,count(cus.identifier) from PartyRoleInRelationship cus join PartyRoleInRelationship_Communication pc on cus.id = pc.PartyRoleInRelationship_id join communication_communication cc on cc.communication_id = pc.receivesCommunication_id join communication c on c.id=cc.followsUp_id where cus.agentId in ("
                            + agentCode.toString()
                            + ") and cus.statusCode = '20' and c.interactionType = '6' and c.communicationStatusDate between ?1 and ?2 group by cus.agentId";

            customerList = null;
            customerList =
                    findUsingNativeQuery(searchCriteriatRequest, query, searchCriteria.getStartDate(),
                            searchCriteria.getCurrentDate());
            if (CollectionUtils.isNotEmpty(customerList)) {
                for (Object customerObj : customerList) {
                    Object[] values = (Object[]) customerObj;
                    String agentId = values[0].toString();
                    String count = values[1].toString();
                    appointmentMap.put(agentId, count);
                }

            }
            customerMap.put("Appointment", appointmentMap);
        }
        return customerMap;
    }
}
