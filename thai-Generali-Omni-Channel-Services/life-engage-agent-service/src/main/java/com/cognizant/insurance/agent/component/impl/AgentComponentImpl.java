package com.cognizant.insurance.agent.component.impl;

import com.cognizant.insurance.agent.domain.agent.Agent;
import com.cognizant.insurance.agent.component.AgentComponent;
import com.cognizant.insurance.agent.dao.AgentDao;
import com.cognizant.insurance.agent.dao.impl.AgentDaoImpl;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.response.impl.ResponseImpl;

/**
 * The Class AgentComponentImpl.
 */
public class AgentComponentImpl implements AgentComponent {
	
	

	/* (non-Javadoc)
	 * @see com.cognizant.insurance.generic.component.AgentComponent#retrieveAgentByCode(com.cognizant.insurance.request.Request)
	 */
	@Override
	public Response<GeneraliAgent> retrieveAgentByCode(Request<GeneraliAgent> agentRequest) {
		final AgentDao agentDao = new AgentDaoImpl();
		Response<GeneraliAgent> agentResponse = new ResponseImpl<GeneraliAgent>();
		agentResponse.setType(agentDao.retrieveAgentByCode(agentRequest));
		return agentResponse;
	}

}
