/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 20, 2012
 */
package com.cognizant.insurance.product.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;

import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.product.dao.ProductDao;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;

/**
 * The Class class ProductDaoImpl.
 */
public class ProductDaoImpl extends DaoImpl implements ProductDao {
    /** The Constant CARRIER_CODE. */
    private static final String CARRIER_CODE = "carrierCode";

    /** The Constant PRODUCT_SPECIFICATION_GET_PRODUCT. */
    private static final String PRODUCT_SPECIFICATION_GET_PRODUCT = "productSpecification.getProduct";

    /** The Constant PRODUCT_SPECIFICATION_GET_PRODUCT_BY_ID. */
    private static final String PRODUCT_SPECIFICATION_GET_PRODUCT_BY_ID = "productSpecification.getProductById";

    /** The Constant PRODUCT_SPECIFICATION_GET_PRODUCT_BY_ID. */
    private static final String PRODUCT_SPECIFICATION_GET_PRODUCTS_BY_IDS = "productSpecification.getProductsByIds";

    /** The Constant PRODUCT_SPECIFICATION_FIND_BY_CARRIER_CODE. */
    private static final String PRODUCT_SPECIFICATION_FIND_BY_CARRIER_CODE = "ProductSpecification.findByCarrierCode";

    /** The Constant PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCTS. */
    private static final String PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCTS = "ProductSpecification.findActiveProducts";
    
    /** The Constant PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCTS. */
    private static final String PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCT_BY_ID = "ProductSpecification.findActiveProductByID";
    
    /** The Constant PRODUCT_SPECIFICATION_FIND_ACTV_PRODUCTS_BYFLTR. */
    private static final String PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCTS_BY_FILTER = "ProductSpecification.findActiveProductsByFilter";
    
    /** The Constant PRODUCT_SPECIFICATION_FIND_PRODUCTS_BYFILTER. */
    private static final String PRODUCT_SPECIFICATION_FIND_ALL_PRODUCTS_BY_FILTER = "ProductSpecification.findAllProductsByFilter";

    /** The Constant PRODUCT_ID. */
    private static final String PRODUCT_ID = "productId";

    /** The Constant PRODUCT_CODE. */
    private static final String PRODUCT_CODE = "productCode";
    
	private static final String PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCTS_BY_FILTERV2 = "ProductSpecification.V2findActiveProductsByFilter";
	
    private static final String PRODUCT_SPECIFICATION_FIND_ALL_PRODUCTS_BY_FILTERV2 = "ProductSpecification.V2findAllProductsByFilter";
	
    private static final String PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCT_BY_IDV2 = "ProductSpecification.V2findActiveProductByID";
	
    private static final String PRODUCT_SPECIFICATION_GET_PRODUCTV2 = "productSpecification.V2getProduct";
    
    private static final String PRODUCT_SPECIFICATION_GET_PRODUCT_BY_IDV2 = "productSpecification.V2getProductById";
    
    private static final String PRODUCT_SPECIFICATION_GET_PRODUCTS_BY_IDSV2 = "productSpecification.V2getProductsByIds";
    
    private static final String JUR_CD= "jurisdictionCode";
    
    private static final String CARRIER_ID = "carrierId";
    
    private static final String CHANNEL_ID ="channelId";
    
    private static final String PROD_SPN_DATE = "productSuspensionDate";
    
    private static final String PROD_WDRL_DATE = "productWithdrawalDate";
    
    private static final String PROD_TRM_DATE = "productTerminationDate";
    
    private static final String VERSION_ONE = "1";
    
    private static final String VERSION_TWO = "2";

	private static final String PROD_ID = "prodId";


    protected List<Object> findUsingQueryParameterList(final Request request, final String queryString,
            final Map<String, Object> configParamMap) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (configParamMap != null && configParamMap.size() > 0) {
                for (Map.Entry<String, Object> params : configParamMap.entrySet()) {
                    query.setParameter(params.getKey(), params.getValue());
                }
            }
            return (List<Object>) query.getResultList();

        } catch (Exception e) {
            throw new DaoException(e);
        }

    }
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.product.dao.ProductDao#getProducts(com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<List<ProductSpecification>> getProducts(final Request<ProductSearchCriteria> productRequest) {

        final Response<List<ProductSpecification>> response = new ResponseImpl<List<ProductSpecification>>();
        final ProductSearchCriteria productSearchCriteria = productRequest.getType();

        final Query query =
                ((JPARequestImpl<ProductSearchCriteria>) productRequest).getEntityManager().createNamedQuery(
                        PRODUCT_SPECIFICATION_FIND_BY_CARRIER_CODE);
        query.setParameter("carrierId", productSearchCriteria.getCarrierCode());

        List<ProductSpecification> prodList = (List<ProductSpecification>) query.getResultList();

        response.setType(prodList);
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.product.dao.ProductDao#getActiveProducts(com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<List<ProductSpecification>> getActiveProducts(
            final Request<ProductSearchCriteria> productRequest) {

        final Response<List<ProductSpecification>> response = new ResponseImpl<List<ProductSpecification>>();
        final ProductSearchCriteria productSearchCriteria = productRequest.getType();

        final Query query =
                ((JPARequestImpl<ProductSearchCriteria>) productRequest).getEntityManager().createNamedQuery(
                        PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCTS);
        query.setParameter(CARRIER_ID, productSearchCriteria.getCarrierCode());
        query.setParameter(PROD_SPN_DATE, productSearchCriteria.getCheckDate());
        query.setParameter(PROD_WDRL_DATE, productSearchCriteria.getCheckDate());
        query.setParameter(PROD_TRM_DATE, productSearchCriteria.getCheckDate());
        List<ProductSpecification> prodList = (List<ProductSpecification>) query.getResultList();
        response.setType(prodList);
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.product.dao.ProductDao#getProduct(com.cognizant.insurance.request.Request)
     */
    @Override
    public final ProductSpecification getProduct(final Request<ProductSearchCriteria> criteriaRequest, final String version) {
        ProductSpecification product = null;
        try {
            final ProductSearchCriteria criteria = criteriaRequest.getType();
            final EntityManager entityManager =
                    ((JPARequestImpl<ProductSearchCriteria>) criteriaRequest).getEntityManager();

            Query query = null;
            //Two named queries are used since, the previous model, doesnt have jurisdiction code in it
            if(null == version || (null!=version && version.equals(VERSION_ONE))){
	            query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_GET_PRODUCT);
	            query.setParameter(PRODUCT_CODE, criteria.getCode());
	            query.setParameter(CARRIER_CODE, criteria.getCarrierCode());
            }else if(null!=version && version.equals(VERSION_TWO)){
            	query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_GET_PRODUCTV2);
                query.setParameter(PRODUCT_CODE, criteria.getCode());
                query.setParameter(CARRIER_CODE, criteria.getCarrierCode());
                query.setParameter(JUR_CD, criteria.getJurisdictionCode());
                query.setParameter(CHANNEL_ID, criteria.getChannelId());
            }
            final List<ProductSpecification> specs = (List<ProductSpecification>) query.getResultList();
            if (CollectionUtils.isNotEmpty(specs)) {
                product = specs.get(0);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return product;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.product.dao.ProductDao#getProductById(com.cognizant.insurance.request.Request)
     */
    @Override
    public final ProductSpecification getProductById(final Request<ProductSearchCriteria> criteriaRequest, final String version) {
        ProductSpecification product = null;
        try {
        	final ProductSearchCriteria criteria = criteriaRequest.getType();
            final EntityManager entityManager = ((JPARequestImpl<ProductSearchCriteria>) criteriaRequest).getEntityManager();
            Query query = null;
            if((null!=version && version.equals(VERSION_TWO))){
                query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_GET_PRODUCT_BY_IDV2);
                query.setParameter(PROD_ID,criteria.getProductId());
                query.setParameter(JUR_CD, criteria.getJurisdictionCode());
                query.setParameter(CHANNEL_ID, criteria.getChannelId());
            }else if(null == version ||  (null!=version && version.equals(VERSION_ONE))){
            	query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_GET_PRODUCT_BY_ID);
            	query.setParameter(PRODUCT_ID, criteria.getId());
            }
            final List<ProductSpecification> specs = (List<ProductSpecification>) query.getResultList();
            if (CollectionUtils.isNotEmpty(specs)) {
                product = specs.get(0);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return product;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.product.dao.ProductDao#getProductsByIds(com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<List<ProductSpecification>> getProductsByFilter(
            final Request<ProductSearchCriteria> criteriaRequest, final String version) {
        final Response<List<ProductSpecification>> response = new ResponseImpl<List<ProductSpecification>>();
        final ProductSearchCriteria criteria = criteriaRequest.getType();
        Query query = null;
        final EntityManager entityManager =
                ((JPARequestImpl<ProductSearchCriteria>) criteriaRequest).getEntityManager();
        if(null == version || (null!=version && version.equals(VERSION_ONE))){
        	query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_GET_PRODUCTS_BY_IDS);
        	query.setParameter(PRODUCT_ID, criteria.getProductIds());
        }else if(null!=version && version.equals(VERSION_TWO)){
        	query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_GET_PRODUCTS_BY_IDSV2);
        	query.setParameter(PRODUCT_ID, criteria.getProductIds());
            query.setParameter(JUR_CD, criteria.getJurisdictionCode());
            query.setParameter(CHANNEL_ID, criteria.getChannelId());
        }
        final List<ProductSpecification> prodList = (List<ProductSpecification>) query.getResultList();
        response.setType(prodList);

        return response;
    }

    /* (non-Javadoc)
     * @see com.cognizant.insurance.product.dao.ProductDao#isProductActive(com.cognizant.insurance.request.Request)
     */
    @Override
    public final Boolean isProductActive(final Request<ProductSearchCriteria> criteriaRequest, final String version) {
    
    	 Boolean isProductActive = Boolean.FALSE;
         try {
        	 final ProductSearchCriteria criteria = criteriaRequest.getType();
             final EntityManager entityManager =
                     ((JPARequestImpl<ProductSearchCriteria>) criteriaRequest).getEntityManager();

             Query query = null;
             if(null == version || (null!=version && version.equals(VERSION_ONE))){
                 query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCT_BY_ID);
                 query.setParameter(PROD_SPN_DATE, criteria.getCheckDate());
                 query.setParameter(PROD_WDRL_DATE, criteria.getCheckDate());
                 query.setParameter(PROD_TRM_DATE, criteria.getCheckDate());
                 query.setParameter(PRODUCT_ID, criteria.getId());
                 query.setParameter(CARRIER_ID, criteria.getCarrierCode());
             }else if(null!=version && version.equals(VERSION_TWO)){
                 query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCT_BY_IDV2);
                 query.setParameter(PROD_SPN_DATE, criteria.getCheckDate());
                 query.setParameter(PROD_WDRL_DATE, criteria.getCheckDate());
                 query.setParameter(PROD_TRM_DATE, criteria.getCheckDate());
                 query.setParameter(PRODUCT_ID, criteria.getId().intValue()); //the field product_id
                 query.setParameter(CARRIER_ID, criteria.getCarrierCode());
                 query.setParameter(JUR_CD, criteria.getJurisdictionCode());
                 query.setParameter(CHANNEL_ID, criteria.getChannelId());
             }
             final List<ProductSpecification> specs = (List<ProductSpecification>) query.getResultList();
             if (CollectionUtils.isNotEmpty(specs)) {
            	 isProductActive = Boolean.TRUE;
             }
         } catch (Exception e) {
             throw new DaoException(e);
         }

         return isProductActive;
    }
    
    /*
     * (non-Javadoc)
     * 
     * Get all products by filter : carrierId & ChannelId
     * 
     * @see com.cognizant.insurance.product.dao.ProductDao#getProductsByFilter(com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<List<ProductSpecification>> getAllProductsByFilter(
            final Request<ProductSearchCriteria> productRequest, final String version) {

        final Response<List<ProductSpecification>> response = new ResponseImpl<List<ProductSpecification>>();
        final ProductSearchCriteria productSearchCriteria = productRequest.getType();

        Query query = null;
        if(null == version || (null!=version && version.equals(VERSION_ONE))){
	        query =
                ((JPARequestImpl<ProductSearchCriteria>) productRequest).getEntityManager().createNamedQuery(
                		PRODUCT_SPECIFICATION_FIND_ALL_PRODUCTS_BY_FILTER);
	        query.setParameter(CARRIER_ID, productSearchCriteria.getCarrierCode());
	        query.setParameter(CHANNEL_ID, productSearchCriteria.getChannelId());
        }else if(null!=version && version.equals(VERSION_TWO)){
            query =
                    ((JPARequestImpl<ProductSearchCriteria>) productRequest).getEntityManager().createNamedQuery(
                    		PRODUCT_SPECIFICATION_FIND_ALL_PRODUCTS_BY_FILTERV2);
            query.setParameter(CARRIER_ID, productSearchCriteria.getCarrierCode());
            query.setParameter(CHANNEL_ID, productSearchCriteria.getChannelId());
            query.setParameter("lob", productSearchCriteria.getLobId());
            query.setParameter(JUR_CD, productSearchCriteria.getJurisdictionCode());
            query.setParameter("version", productSearchCriteria.getVersion());
            query.setParameter("subLob", productSearchCriteria.getSubLob());
            query.setParameter("broadLob", productSearchCriteria.getBroadLob());
        }
        
        List<ProductSpecification> prodList = (List<ProductSpecification>) query.getResultList();
        response.setType(prodList);
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * Get all active products by filter : carrierId & ChannelId
     * 
     * @see com.cognizant.insurance.product.dao.ProductDao#getActiveProductsByFilter(com.cognizant.insurance.request.Request)
     */
    @Override
    public final Response<List<ProductSpecification>> getActiveProductsByFilter(
            final Request<ProductSearchCriteria> productRequest, final String version) {

        final Response<List<ProductSpecification>> response = new ResponseImpl<List<ProductSpecification>>();
        final ProductSearchCriteria productSearchCriteria = productRequest.getType();

        Query query = null;
        if(null == version || (null!=version && version.equals(VERSION_ONE))){
	        query =
	                ((JPARequestImpl<ProductSearchCriteria>) productRequest).getEntityManager().createNamedQuery(
	                        PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCTS_BY_FILTER);
	        query.setParameter(CARRIER_ID, productSearchCriteria.getCarrierCode());
	        query.setParameter(CHANNEL_ID, productSearchCriteria.getChannelId());
	        query.setParameter(PROD_SPN_DATE, productSearchCriteria.getCheckDate());
	        query.setParameter(PROD_WDRL_DATE, productSearchCriteria.getCheckDate());
	        query.setParameter(PROD_TRM_DATE, productSearchCriteria.getCheckDate());
        }else if(null!=version && version.equals(VERSION_TWO)){
        	query =
                ((JPARequestImpl<ProductSearchCriteria>) productRequest).getEntityManager().createNamedQuery(
                		PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCTS_BY_FILTERV2);
	        query.setParameter(CARRIER_ID, productSearchCriteria.getCarrierCode());
	        query.setParameter(CHANNEL_ID, productSearchCriteria.getChannelId());
	        query.setParameter(PROD_SPN_DATE, productSearchCriteria.getCheckDate());
	        query.setParameter(PROD_WDRL_DATE, productSearchCriteria.getCheckDate());
	        query.setParameter(PROD_TRM_DATE, productSearchCriteria.getCheckDate());
	        query.setParameter("lob", productSearchCriteria.getLobId());
	        query.setParameter(JUR_CD, productSearchCriteria.getJurisdictionCode());
	        query.setParameter("version", productSearchCriteria.getVersion());
	        query.setParameter("subLob", productSearchCriteria.getSubLob());
	        query.setParameter("broadLob", productSearchCriteria.getBroadLob());
        }	
        List<ProductSpecification> prodList = (List<ProductSpecification>) query.getResultList();
        response.setType(prodList);
        return response;
    }

    /* (non-Javadoc)
     * @see com.cognizant.insurance.product.dao.ProductDao#getProductTemplatesByFilter(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<ProductTemplateMapping>> getProductTemplatesByFilter(
            Request<ProductSearchCriteria> criteriaRequest) {
        final Response<List<ProductTemplateMapping>> response = new ResponseImpl<List<ProductTemplateMapping>>();
        final ProductSearchCriteria productSearchCriteria = criteriaRequest.getType();
        Map<String, Object> configParamMap = new HashMap<String, Object>();
        configParamMap.put(PRODUCT_ID, productSearchCriteria.getId());
        configParamMap.put("carrierId", productSearchCriteria.getCarrierCode());
        String query = "SELECT templateMapping " +
        		       "FROM " +
        		       "ProductTemplateMapping templateMapping " +
        		       "WHERE " +
        		           "templateMapping.productSpecification.id = :productId and " +
        		           "templateMapping.productSpecification.carrier.id = :carrierId";
        if(productSearchCriteria.getTemplateType()!= null && !productSearchCriteria.getTemplateType().equals("")) {
            configParamMap.put("templateType", productSearchCriteria.getTemplateType());
            query = query + " and templateMapping.templateType.id = :templateType";
        }
        if(productSearchCriteria.getTemplateLanguage()!= null && !productSearchCriteria.getTemplateLanguage().equals("")) {
            configParamMap.put("templateLanguage", productSearchCriteria.getTemplateLanguage());
            query = query + " and templateMapping.productTemplates.language = :templateLanguage";
        }
        if(productSearchCriteria.getTemplateCountryCode()!= null && !productSearchCriteria.getTemplateCountryCode().equals("")) {
            configParamMap.put("templateCountry", productSearchCriteria.getTemplateCountryCode());
            query = query + " and templateMapping.productTemplates.countryCode = :templateCountry";
        }
        List<Object> productTemplateMappingList = findUsingQueryParameterList(criteriaRequest, query, configParamMap);
        
        final List<ProductTemplateMapping> productTemplateMappings = new ArrayList<ProductTemplateMapping>();
        if (CollectionUtils.isNotEmpty(productTemplateMappingList)) {
            for (Object agreement : productTemplateMappingList) {
                productTemplateMappings.add((ProductTemplateMapping) agreement);
            }
        }
        response.setType(productTemplateMappings);
        return response;
    }
    
    
    
    @Override
    public final Response<List<ProductSpecification>> getActiveProductsByFilterV2(
            final Request<ProductSearchCriteria> productRequest) {

        final Response<List<ProductSpecification>> response = new ResponseImpl<List<ProductSpecification>>();
        final ProductSearchCriteria productSearchCriteria = productRequest.getType();

        final Query query =
                ((JPARequestImpl<ProductSearchCriteria>) productRequest).getEntityManager().createNamedQuery(
                		PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCTS_BY_FILTERV2);
        query.setParameter("carrierId", productSearchCriteria.getCarrierCode());
        query.setParameter("channelId", productSearchCriteria.getChannelId());
        query.setParameter("productSuspensionDate", productSearchCriteria.getCheckDate());
        query.setParameter("productWithdrawalDate", productSearchCriteria.getCheckDate());
        query.setParameter("productTerminationDate", productSearchCriteria.getCheckDate());
        query.setParameter("lob", productSearchCriteria.getLobId());
        query.setParameter("jurisdictionCode", productSearchCriteria.getJurisdictionCode());
        query.setParameter("version", productSearchCriteria.getVersion());
        query.setParameter("subLob", productSearchCriteria.getSubLob());
        query.setParameter("broadLob", productSearchCriteria.getBroadLob());
        
        List<ProductSpecification> prodList = (List<ProductSpecification>) query.getResultList();
        response.setType(prodList);
        return response;
    }

    
    @Override
    public final Response<List<ProductSpecification>> getAllProductsByFilterV2(
            final Request<ProductSearchCriteria> productRequest) {

        final Response<List<ProductSpecification>> response = new ResponseImpl<List<ProductSpecification>>();
        final ProductSearchCriteria productSearchCriteria = productRequest.getType();

        final Query query =
                ((JPARequestImpl<ProductSearchCriteria>) productRequest).getEntityManager().createNamedQuery(
                		PRODUCT_SPECIFICATION_FIND_ALL_PRODUCTS_BY_FILTERV2);
        query.setParameter("carrierId", productSearchCriteria.getCarrierCode());
        query.setParameter("channelId", productSearchCriteria.getChannelId());
        query.setParameter("lob", productSearchCriteria.getLobId());
        query.setParameter("jurisdictionCode", productSearchCriteria.getJurisdictionCode());
        query.setParameter("version", productSearchCriteria.getVersion());
        query.setParameter("subLob", productSearchCriteria.getSubLob());
        query.setParameter("broadLob", productSearchCriteria.getBroadLob());
        
        List<ProductSpecification> prodList = (List<ProductSpecification>) query.getResultList();
        response.setType(prodList);
        return response;
    }
    
    
    @Override
    public final Boolean isProductActiveV2(final Request<ProductSearchCriteria> criteriaRequest) {
    
    	 Boolean isProductActive = Boolean.FALSE;
         try {
        	 final ProductSearchCriteria criteria = criteriaRequest.getType();
             final EntityManager entityManager =
                     ((JPARequestImpl<ProductSearchCriteria>) criteriaRequest).getEntityManager();

             final Query query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_FIND_ACTIVE_PRODUCT_BY_IDV2);
             query.setParameter("productSuspensionDate", criteria.getCheckDate());
             query.setParameter("productWithdrawalDate", criteria.getCheckDate());
             query.setParameter("productTerminationDate", criteria.getCheckDate());
             query.setParameter(PRODUCT_ID, criteria.getId()); //the field product_id
             query.setParameter("carrierId", criteria.getCarrierCode());
             query.setParameter("jurisdictionCode", criteria.getJurisdictionCode());
             query.setParameter("channelId", criteria.getChannelId());
             final List<ProductSpecification> specs = (List<ProductSpecification>) query.getResultList();
             if (CollectionUtils.isNotEmpty(specs)) {
            	 isProductActive = Boolean.TRUE;
             }
         } catch (Exception e) {
             throw new DaoException(e);
         }

         return isProductActive;
    }
    
    
    @Override
    public final ProductSpecification getProductV2(final Request<ProductSearchCriteria> criteriaRequest) {
        ProductSpecification product = null;
        try {
            final ProductSearchCriteria criteria = criteriaRequest.getType();
            final EntityManager entityManager =
                    ((JPARequestImpl<ProductSearchCriteria>) criteriaRequest).getEntityManager();

            final Query query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_GET_PRODUCTV2);
            query.setParameter(PRODUCT_CODE, criteria.getCode());
            query.setParameter(CARRIER_CODE, criteria.getCarrierCode());
            query.setParameter("jurisdictionCode", criteria.getJurisdictionCode());
            query.setParameter("channelId", criteria.getChannelId());
            
            final List<ProductSpecification> specs = (List<ProductSpecification>) query.getResultList();
            if (CollectionUtils.isNotEmpty(specs)) {
                product = specs.get(0);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return product;
    }
    
    
    @Override
    public final ProductSpecification getProductByIdV2(final Request<ProductSearchCriteria> criteriaRequest) {
        ProductSpecification product = null;
        try {
        	final ProductSearchCriteria criteria = criteriaRequest.getType();
            final EntityManager entityManager = ((JPARequestImpl<ProductSearchCriteria>) criteriaRequest).getEntityManager();
            final Query query = entityManager.createNamedQuery(PRODUCT_SPECIFICATION_GET_PRODUCT_BY_IDV2);
            query.setParameter(PRODUCT_ID, criteria.getId());
            query.setParameter("jurisdictionCode", criteria.getJurisdictionCode());
            query.setParameter("channelId", criteria.getChannelId());
            final List<ProductSpecification> specs = (List<ProductSpecification>) query.getResultList();
            if (CollectionUtils.isNotEmpty(specs)) {
                product = specs.get(0);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }

        return product;
    }
    



}
