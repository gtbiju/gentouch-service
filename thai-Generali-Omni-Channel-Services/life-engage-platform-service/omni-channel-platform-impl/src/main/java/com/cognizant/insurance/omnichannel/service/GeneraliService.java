package com.cognizant.insurance.omnichannel.service;

import com.cognizant.insurance.core.exception.BusinessException;

public interface GeneraliService {

	public String generateSPAJNo(String requestJson) throws BusinessException;
	public String isSpajExisting(String requestJson) throws BusinessException;
	public String generateBPMEmailPdf(String requestJson);
	public void generateICMInfo(String eAppId);
	public String isLASAvailable();
	String validateAgentCode(String requestJson) throws BusinessException;
	public void resubmit(String eAppId);
	public String retrieveMemoDetails(String requestJson) throws BusinessException;
	public String updateMemoDetails(String requestJson) throws BusinessException;
	public String updateAppainStatus(String requestJson) throws BusinessException;
	public String generateEtr(String requestJson)throws BusinessException;
    public String lockOrUnlockCase(String json);
    String sendUpdateToBPM(String json);
}
