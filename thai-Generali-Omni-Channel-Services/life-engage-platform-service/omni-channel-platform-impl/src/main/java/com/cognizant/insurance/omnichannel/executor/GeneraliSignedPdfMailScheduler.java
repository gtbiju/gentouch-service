package com.cognizant.insurance.omnichannel.executor;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.component.LifeEngageEmailComponent;
import com.cognizant.insurance.eMail.LifeEngageEmail;




@Component
public class GeneraliSignedPdfMailScheduler {
	
	/** The Constant ID. */
    private static final String ID = "id";
    
    /** The Constant EMAIL_JSON. */
    private static final String EMAIL_JSON = "emailJson";
    
    /** The Constant ATTEMPT_NUMBER. */
    private static final String ATTEMPT_NUMBER = "attemptNo";
    
    private static final String CURRENT_DATE="currentDate";  
    
	@Autowired
	LifeEngageEmailComponent lifeEngageEmailComponent;
	
	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	private Job jobToFetchSignedPDFAndSendMail;
	
	/** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliSignedPdfMailScheduler.class);

	public void run() {

	    try {
	    	 List<LifeEngageEmail> lifeEngageEmails = lifeEngageEmailComponent.fetchAllInitializedSignedEmail("eApp");
				
			 for(LifeEngageEmail email : lifeEngageEmails){
				 Integer AttemptNumber = Integer.parseInt(email.getAttemptNumber())+1;
				 jobLauncher.run(
						 jobToFetchSignedPDFAndSendMail,
							new JobParametersBuilder().addLong(ID, email.getId()).addString(EMAIL_JSON, email.getEmailValues()).addString(ATTEMPT_NUMBER, AttemptNumber.toString()).addDate(CURRENT_DATE, new Date()).toJobParameters());
				
			 }
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	LOGGER.error("Schedular Trigger Failed ", e);
	    }

	  }
	
	
}
