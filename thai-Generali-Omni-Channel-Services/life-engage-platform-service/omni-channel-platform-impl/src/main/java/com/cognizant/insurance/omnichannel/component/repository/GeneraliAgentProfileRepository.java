package com.cognizant.insurance.omnichannel.component.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cognizant.insurance.agent.AgentAchievement;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliAgentAddress;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.component.repository.AgentProfileRepository;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.omnichannel.component.GeneraliAgentComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;


/**
 * The Class AgentProfileRepository.
 */
@Repository
public class GeneraliAgentProfileRepository extends AgentProfileRepository{

	/** The entity manager. */
	@PersistenceContext(unitName="ODS_unit")
	private EntityManager entityManager;
	
	/** The LE entity manager. */
	@PersistenceContext(unitName="LE_Platform")
	private EntityManager leEntityManager;

	/** The agent component. */
	@Autowired
	private GeneraliAgentComponent agentComponent;

	/**
	 * Retrieve agent details by code.
	 * 
	 * @param requestInfo
	 *            the request info
	 * @param agentID
	 *            the agent id
	 * @return the agent
	 */
	/*public final GeneraliAgent retrieveAgentDetailsByCode(
			final RequestInfo requestInfo, final String agentID) {
		final Request<GeneraliAgent> agentRequest = new JPARequestImpl<GeneraliAgent>(entityManager, ContextTypeCodeList.AGENT,
				requestInfo.getTransactionId());
		final GeneraliAgent agent = new GeneraliAgent();
		agent.setAgentCode(agentID);
		agentRequest.setType(agent);
		Response<GeneraliAgent> response = generaliAgentComponent.retrieveAgentByCode(agentRequest);
		return response.getType();
	}
	*/
	public final GeneraliAgent retrieveAgentODSDetailsByCode(
			final RequestInfo requestInfo, final String agentID) {
		final Request<GeneraliAgent> agentRequest = new JPARequestImpl<GeneraliAgent>(entityManager, ContextTypeCodeList.AGENT,
				requestInfo.getTransactionId());
		final GeneraliAgent agent = new GeneraliAgent();
		agent.setAgentCode(agentID);
		agentRequest.setType(agent);
		Response<GeneraliAgent> response = agentComponent.retrieveAgentByODSCode(agentRequest);
		return response.getType();
	}

	public final GeneraliGAOAgent retrieveGAOAgentODSDetailsByCode(
			final RequestInfo requestInfo, final String agentID) {
		final Request<GeneraliGAOAgent> agentRequest = new JPARequestImpl<GeneraliGAOAgent>(entityManager, ContextTypeCodeList.AGENT,
				requestInfo.getTransactionId());
		final GeneraliGAOAgent agent = new GeneraliGAOAgent();
		agent.setAgentCode(agentID);
		agentRequest.setType(agent);
		Response<GeneraliGAOAgent> response = agentComponent.retrieveGAOAgentByODSCode(agentRequest);
		return response.getType();
	}

	public final GeneraliAgentAddress retrieveGAOAddress(
			final RequestInfo requestInfo, final String gaoCode) {
		final Request<GeneraliAgentAddress> agentRequest = new JPARequestImpl<GeneraliAgentAddress>(entityManager, ContextTypeCodeList.AGENT,
				requestInfo.getTransactionId());
		final GeneraliAgentAddress agentAddr = new GeneraliAgentAddress();
		agentAddr.setCode(gaoCode);
		agentRequest.setType(agentAddr);
		Response<GeneraliAgentAddress> response = agentComponent.retrieveGAOAddress(agentRequest);
		return response.getType();
	}
	
	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the new entity manager
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public GeneraliAgentComponent getAgentComponent() {
		return agentComponent;
	}

	public void setAgentComponent(GeneraliAgentComponent agentComponent) {
		this.agentComponent = agentComponent;
	}

    public final List<ProductSpecification> retrieveAgentODSDetailsByProductCode(final RequestInfo requestInfo, final ProductSpecification prodType) {
        final Request<ProductSpecification> agentProductRequest = new JPARequestImpl<ProductSpecification>(leEntityManager, ContextTypeCodeList.AGENT,
                requestInfo.getTransactionId());
        agentProductRequest.setType(prodType);
        Response<List<ProductSpecification>> response = agentComponent.retrieveProductsByCode(agentProductRequest);
        return response.getType();
    }
    
    public final List<GeneraliAgent> retrieveAgentODSDetailsForReassign(
			final RequestInfo requestInfo, final String agentID,String agentStatus) {
		final Request<GeneraliAgent> agentRequest = new JPARequestImpl<GeneraliAgent>(entityManager, ContextTypeCodeList.AGENT,
				requestInfo.getTransactionId());
		final GeneraliAgent agent = new GeneraliAgent();
		agent.setAgentCode(agentID);
		agent.setAgentStatus(agentStatus);
		agentRequest.setType(agent);
		List<GeneraliAgent> response = agentComponent.retrieveAgentByODSForReassign(agentRequest);
		return response;
	}
    
    public final List<GeneraliAgent> retrieveActiveAgentCodeToReassign(
			final RequestInfo requestInfo, final String agentID,String agentStatus) {
		final Request<GeneraliAgent> agentRequest = new JPARequestImpl<GeneraliAgent>(entityManager, ContextTypeCodeList.AGENT,
				requestInfo.getTransactionId());
		final GeneraliAgent agent = new GeneraliAgent();
		agent.setAgentCode(agentID);
		agent.setAgentStatus(agentStatus);
		agentRequest.setType(agent);
		List<GeneraliAgent> response = agentComponent.retrieveActiveAgentByODSToReassign(agentRequest);
		return response;
	}
    
    public final List<Customer> retrieveLeads(
			final RequestInfo requestInfo, final String agentID) {
		final Request<Customer> agentRequest = new JPARequestImpl<Customer>(leEntityManager, ContextTypeCodeList.LMS,
				requestInfo.getTransactionId());
		//final GeneraliAgent agent = new GeneraliAgent();
		final Customer customer = new Customer();
		customer.setAgentId(agentID);
		agentRequest.setType(customer);
		List<Customer> response = agentComponent.retrieveLeads(agentRequest);
		return response;
	}
    
    public final Integer retrieveIllustrationCount(final RequestInfo requestInfo, final String transTrackingID) {
		final Request<Customer> agentRequest = new JPARequestImpl<Customer>(leEntityManager, ContextTypeCodeList.LMS,
				requestInfo.getTransactionId());
		final Customer customer = new Customer();
		customer.setTransTrackingId(transTrackingID);
		agentRequest.setType(customer);
		return agentComponent.retrieveIllustrationCount(agentRequest);
	}
    
    public final Integer retrieveEappCount(final RequestInfo requestInfo, final String transTrackingID) {
    	final Request<Customer> agentRequest = new JPARequestImpl<Customer>(leEntityManager, ContextTypeCodeList.LMS,
				requestInfo.getTransactionId());
		final Customer customer = new Customer();
		customer.setTransTrackingId(transTrackingID);
		agentRequest.setType(customer);
		return agentComponent.retrieveEappCount(agentRequest);
	}
    
    public final Integer retrieveFnaCount(final RequestInfo requestInfo, final String transTrackingID) {
    	final Request<Customer> agentRequest = new JPARequestImpl<Customer>(leEntityManager, ContextTypeCodeList.LMS,
				requestInfo.getTransactionId());
		final Customer customer = new Customer();
		customer.setTransTrackingId(transTrackingID);
		agentRequest.setType(customer);
		return agentComponent.retrieveFnaCount(agentRequest);
	}
    
    public List<GoalAndNeed> retrieveGoalAndNeed(final RequestInfo requestInfo, final String transTrackingId){
    	final Request<GoalAndNeed> agentRequest = new JPARequestImpl<GoalAndNeed>(leEntityManager, ContextTypeCodeList.FNA,
				requestInfo.getTransactionId());
    	GoalAndNeed goalAndNeed = new GoalAndNeed();
    	goalAndNeed.setLeadId(transTrackingId);
    	agentRequest.setType(goalAndNeed);
    	List<GoalAndNeed> result = agentComponent.retrieveGoalAndNeed(agentRequest);
		return result;
    	
    }
    
	public List<Agreement> retrieveAgreement(final RequestInfo requestInfo, final String identifier) {
		final Request<InsuranceAgreement> agentRequest = new JPARequestImpl<InsuranceAgreement>(leEntityManager,
				ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
		final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
		TransactionKeys transactionKeys = new TransactionKeys();
		transactionKeys.setKey28(identifier);
		insuranceAgreement.setTransactionKeys(transactionKeys);
		agentRequest.setType(insuranceAgreement);
		List<Agreement> result = agentComponent.retrieveAgreement(agentRequest);
		return result;
	}
    
    public String retrieveLeadIdForFna(final RequestInfo requestInfo, final String identifier){
    	String leadId = StringUtils.EMPTY;
    	final Request<Customer> agentRequest = new JPARequestImpl<Customer>(leEntityManager, ContextTypeCodeList.LMS,
				requestInfo.getTransactionId());
		final Customer customer = new Customer();
		customer.setIdentifier(identifier);
		agentRequest.setType(customer);
		leadId = agentComponent.retrieveLeadIdForFna(agentRequest);
    	return leadId;
    }
    
    public List<InsuranceAgreement> retrieveIllustration(final RequestInfo requestInfo, final String transTrackingId){
    	final Request<InsuranceAgreement> agentRequest = new JPARequestImpl<InsuranceAgreement>(leEntityManager, ContextTypeCodeList.EAPP,
				requestInfo.getTransactionId());
    	final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
    	TransactionKeys transkeys = new TransactionKeys();
    	transkeys.setKey28(transTrackingId);
    	insuranceAgreement.setTransactionKeys(transkeys);
    	agentRequest.setType(insuranceAgreement);
    	List<InsuranceAgreement> result = agentComponent.retrieveIllustration(agentRequest);
		return result;
    }

	public EntityManager getLeEntityManager() {
		return leEntityManager;
	}

	public void setLeEntityManager(EntityManager leEntityManager) {
		this.leEntityManager = leEntityManager;
	}

	public final List<AgentAchievement> retrieveAgentAchievementDetails(final RequestInfo requestInfo, final String agentID) {
		final Request<AgentAchievement> agentAchievementRequest = new JPARequestImpl<AgentAchievement>(entityManager, ContextTypeCodeList.AGENT,
                requestInfo.getTransactionId());
		final AgentAchievement agentAchievement = new AgentAchievement();
		agentAchievement.setAgentCode(agentID);
		agentAchievementRequest.setType(agentAchievement);
		 Response<List<AgentAchievement>> response = agentComponent.retrieveAchievements(agentAchievementRequest);
		return response.getType();
	}

}
