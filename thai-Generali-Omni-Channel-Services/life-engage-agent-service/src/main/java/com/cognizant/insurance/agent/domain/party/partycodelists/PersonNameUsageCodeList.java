/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partycodelists;

/**
 * A code list representing various types of names applicable only to persons.
 * 
 * @author 301350
 * 
 * 
 */
public enum PersonNameUsageCodeList {
    /**
     *
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    BirthName,
    /**
     *
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    MarriedName,
    /**
     * Also called: nom de plume an author's pseudonym.
     * 
     * SOURCE: http://dictionary.reference.com/browse/pen+name
     * 
     * 
     * 
     * 
     * 
     * 
     */
    PenName,
    /**
     * The pseudonym of an actor.
     * 
     * SOURCE: http://dictionary.reference.com/browse/stage+name
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    StageName,
    /**
     * Often considered the last legal name prior to first marriage.
     * 
     * 
     * 
     */
    MaidenName,
    /**
     * 
     * 
     * 
     * 
     */
    Nickname
}
