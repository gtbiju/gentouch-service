package com.cognizant.insurance.omnichannel.vo;

import java.util.ArrayList;

public class PaymentBO {
	
	 /** The paymentType. */
    private ArrayList<String> paymentType;
    
    private String recieptNo;
    
    private String gapAmount;
    
    private String messageCode;
    
    private String message;

	/**
	 * @return the paymentType
	 */
	public ArrayList<String> getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(ArrayList<String> paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the recieptNo
	 */
	public String getRecieptNo() {
		return recieptNo;
	}

	/**
	 * @param recieptNo the recieptNo to set
	 */
	public void setRecieptNo(String recieptNo) {
		this.recieptNo = recieptNo;
	}

	/**
	 * @return the gapAmount
	 */
	public String getGapAmount() {
		return gapAmount;
	}

	/**
	 * @param gapAmount the gapAmount to set
	 */
	public void setGapAmount(String gapAmount) {
		this.gapAmount = gapAmount;
	}

	/**
	 * @return the messageCode
	 */
	public String getMessageCode() {
		return messageCode;
	}

	/**
	 * @param messageCode the messageCode to set
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
    

	
}
