package com.cognizant.insurance.omnichannel.component.repository;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.agreement.component.AgreementComponent;
import com.cognizant.insurance.component.repository.DocumentRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliAgreementComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.response.Response;

public class GeneraliDocumentRepository extends DocumentRepository {
	
	/** The agreement component. */
	@Autowired
	private GeneraliAgreementComponent agreementComponent;

	@Override
	protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(
			String operation) {
		List<AgreementStatusCodeList> statusCodeLists = new ArrayList<AgreementStatusCodeList>();
		if (Constants.UPDATE.equals(operation)) {
			statusCodeLists.add(AgreementStatusCodeList.Draft);
			statusCodeLists.add(AgreementStatusCodeList.Completed);
			statusCodeLists.add(AgreementStatusCodeList.Confirmed);
			statusCodeLists.add(AgreementStatusCodeList.Submitted);
			statusCodeLists.add(AgreementStatusCodeList.New);
		}
		else if (GeneraliConstants.SUBMITTED.equals(operation)) {
			statusCodeLists.add(AgreementStatusCodeList.Submitted);
		}
		return statusCodeLists;
	}

    /**
     * The method is to persist base64 pdf for Email and BPM service
     * 
     * @param agreementDocuments
     * @param proposalnumber
     */
    public void saveBPMBase64Pdf(Set<AgreementDocument> agreementDocuments, String proposalnumber) {

        // get the Agreement object with proposal number
    	
       final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(GeneraliConstants.SUBMITTED);
    	
        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(getEntityManager(), ContextTypeCodeList.EAPP, null);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(proposalnumber);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(getEntityManager(), ContextTypeCodeList.EAPP, null);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementsResponse =
                agreementComponent.getInsuranceAgreement(agreementRequest, statusRequest);
        if (agreementsResponse.getType() == null) {
            throwSystemException(true, Constants.NO_PROPOSAL_EXIST);
        } else {
            final InsuranceAgreement agreement = (InsuranceAgreement) agreementsResponse.getType();

            if (null != agreement.getRelatedDocument()) {

                Set<AgreementDocument> existingDocument = agreement.getRelatedDocument();
                // Appending BPMPDF Document to existing Document

                existingDocument.addAll(agreementDocuments);

                agreement.setRelatedDocument(existingDocument);

                // updating the agreement object
                agreementRequest.setType(agreement);
                agreementComponent.mergeAgreement(agreementRequest);

            }

        }

    }
    /**
     * Method to get Base64 pdf document
     * @param proposalNumber
     * @return
     */
    public Set<AgreementDocument> getBase64DocumentDetils(String proposalNumber) {
        // TODO Auto-generated method stub
                // get the Agreement object with proposal number
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.UPDATE);
        final Request<Agreement> agreementRequest =
            new JPARequestImpl<Agreement>(getEntityManager(), ContextTypeCodeList.EAPP, null);
        
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(proposalNumber);
        
        final Set<AgreementDocument> agreementDocuments = new HashSet<AgreementDocument>();
        final AgreementDocument agreementDocument = new AgreementDocument();
      
        agreementDocument.setTypeCode(DocumentTypeCodeList.BPMPdf);
        agreementDocuments.add(agreementDocument);
        insuranceAgreement.setRelatedDocument(agreementDocuments);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(getEntityManager(), ContextTypeCodeList.EAPP,
                        null);
        statusRequest.setType(statusList);

        final Response<Set<AgreementDocument>> agreementDocumentResponse =
            agreementComponent.getBase64DocumentDetils(agreementRequest, statusRequest);

        
        return agreementDocumentResponse.getType();
    }
}
