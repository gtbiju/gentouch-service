package com.cognizant.insurance.agent.service.impl;


import static com.cognizant.insurance.agent.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.agent.audit.LifeEngageAudit;
import com.cognizant.insurance.agent.component.LifeEngageAuditComponent;
import com.cognizant.insurance.agent.component.LifeEngageLMSComponent;
import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.constants.GeneraliConstants;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.core.exception.InputValidationException;
import com.cognizant.insurance.agent.core.exception.SystemException;
import com.cognizant.insurance.agent.request.vo.RequestInfo;
import com.cognizant.insurance.agent.service.LifeEngageLMSService;
import com.cognizant.insurance.agent.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.agent.service.helper.LifeEngageValidationHelper;


@Component
@Service
public class LifeEngageLMSServiceImpl implements LifeEngageLMSService {
	
	 public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageLMSServiceImpl.class);
	 
	 private static final String TRANSACTION_ARRAY = "{\"Transactions\":[]}";
	 
	 /** The Constant REQUEST. */
	    private static final String REQUEST = "Request";

	    /** The Constant REQUEST_INFO. */
	    private static final String REQUEST_INFO = "RequestInfo";

	    /** The Constant TRANSACTIONS. */
	    private static final String TRANSACTIONS = "Transactions";
	    
	    /** The Constant REQUEST_PAYLOAD. */
	    private static final String REQUEST_PAYLOAD = "RequestPayload";
	    
	    /** The Constant LMS. */
	    private static final String LMS = "LMS";
	    
	    /** The Constant TYPE. */
	    private static final String TYPE = "Type";
	    
	 
	 /** The lms limit. */
	 @Value("${le.platform.service.lms.fetchLimit}")
	 protected Integer lmsLimit;
	 
	 
	 /* Added for security_optimizations */
	 @Value("${le.platform.security.validationPattern}")
	 private String validationPattern;
	 
	 
	 /** The life engage e LMS component. */
	 @Autowired
	 private LifeEngageLMSComponent lifeEngageLMSComponent;
	 
	 @Autowired
	 private LifeEngageAuditComponent lifeEngageAuditComponent;
	 

	@Override
	public String save(String json) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String createLead(String json) throws BusinessException {
        LOGGER.trace("Inside LifeEngageSyncServiceImpl.Save :json " + json);
        JSONObject jsonObject;
        String id =  "";
        final JSONObject jsonObjectRs = new JSONObject();     
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            final JSONArray jsonRsArray = new JSONArray();
            //LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
            if (jsonRqArray.length() > 0) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    
                    final JSONObject jsonObj = jsonRqArray.getJSONObject(i);                   
                    
                    //String emailFlag = (String)jsonObj.getString(KEY18);    
                    
                    String response = null;
                    /* Added for security_optimizations */
                    //LifeEngageValidationHelper.validateUser(jsonObj);
                    /* Added for security_optimizations */
                    try {
                    	/* Added for security_optimizations */
                    	//LifeEngageValidationHelper.validateInputString(jsonObj.toString(),validationPattern);
                    	/* Added for security_optimizations */
                    	/*if (E_APP.equals(jsonObj.getString(TYPE))) {
                            response =
                                    lifeEngageEAppComponent.saveEApp(jsonObj.toString(), requestInfo, lifeEngageAudit);
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                savelifeEngageEmail(jsonObj, response);
                            }
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.SaveEApp response :response " + response);
                            
                        } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
                             Getting IllustartionOutput object from json 
                            JSONObject jsonTransactionData = null;
                            String jsonIllustrationOutput = null;
                            if (!(jsonObj.isNull(TRANSACTIONDATA))) {
                                jsonTransactionData = jsonObj.getJSONObject(TRANSACTIONDATA);
                                if (!(jsonTransactionData.isNull(ILLUSTRATION_OUTPUT))) {
                                    jsonIllustrationOutput =
                                            jsonTransactionData.getJSONObject(ILLUSTRATION_OUTPUT).toString();
                                }
                            }
                             EndOf Getting IllustartionOutput object from json 
                            response =
                                    lifeEngageIllustrationComponent.saveIllustration(jsonObj.toString(),
                                            jsonIllustrationOutput, requestInfo, lifeEngageAudit);
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                savelifeEngageEmail(jsonObj, response);
                            }  
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveIllustration response :response "
                                    + response);
                        } else if (FNA.equals(jsonObj.getString(TYPE))) {
                            response = lifeEngageFNAComponent.saveFNA(jsonObj.toString(), requestInfo, lifeEngageAudit);
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                savelifeEngageEmail(jsonObj, response);
                            } 
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveFNA response :response " + response);
                        } else if (OBSERVATION.equals(jsonObj.getString(TYPE))) {
                            response =
                                    lifeEngageEAppComponent.saveToSyncObservation(jsonObj.toString(), requestInfo,
                                            lifeEngageAudit);
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveToSyncObservation response :response "
                                    + response);
                        } else*/ if (LMS.equals(jsonObj.getString(TYPE))) {
                            response = lifeEngageLMSComponent.saveLMS(jsonObj.toString(), requestInfo, lifeEngageAudit);
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveToSyncObservation response :response "
                                    + response);
                        }
                        jsonRsArray.put(new JSONObject(response));
                    } catch (SystemException e) {
                        jsonRsArray.put(new JSONObject(e.getMessage()));
                        /* Added for security_optimizations */
                    } /*catch(InputValidationException e){
                    	JSONObject jsonResponse= new JSONObject();
                    	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
                    	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
                    	jsonResponse.put("statusMessage", e.getMessage());                    	
                    	jsonRsArray.put(new JSONObject().put("StatusData", jsonResponse));
                    } */catch (Exception e) {
                    	LOGGER.error("Exception", e);
                    }
                    /* Added for security_optimizations */     
                    
                }
            }

            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.Save : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }

	@Override
	public String retrieve(String json) throws BusinessException {
		 LOGGER.trace("LifeEngageLMSServiceImpl.retrieve:  json" + json);
	        JSONObject jsonObject;
	        JSONObject jsonObjectRs = new JSONObject();
	        String eAppResponse = "";
	        try {
	            jsonObject = new JSONObject(json);
	            final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
	            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
	            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
	            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
	            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
	            eAppResponse = retrieve(requestInfo, jsonRqArray);
	            LOGGER.trace("LifeEngageLMSServiceImpl.retrieve:  eAppResponse" + eAppResponse);
	            jsonObjectRs = new JSONObject(eAppResponse);
	        } catch (ParseException e) {
	            LOGGER.trace("LifeEngageLMSServiceImpl.retrieve: Parse Exception" + e.toString());
	            throwBusinessException(true, e.getMessage());
	        }

	        return jsonObjectRs.toString();
	}

	private String retrieve(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException, ParseException {
		
		 JSONObject jsonObject;
	     JSONArray jsonRsArray = new JSONArray();
	     final JSONObject jsonObjectRs = new JSONObject();
	     if (jsonRqArray.length() > 0) {
	    	 final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
	    	 /* Added for security_optimizations */
	            //LifeEngageValidationHelper.validateUser(jsonObj);
	            try{
	            	LifeEngageValidationHelper.validateInputString(jsonObj.toString(),validationPattern);
	            	 if (jsonObj.has(Constants.TYPE) && jsonObj.getString(Constants.TYPE) != null && !"".equals(jsonObj.getString(Constants.TYPE))
	                         && !jsonObj.getString(Constants.TYPE).equals("null")) {
	            		 String transactionArray = "{\"Transactions\":[]}";
	            		 if (LMS.equals(jsonObj.getString(Constants.TYPE))) {
	                         transactionArray = lifeEngageLMSComponent.retrieveLMS(requestInfo, jsonObj);
	                     }
	            		 jsonObject = new JSONObject(transactionArray);
	                     jsonRsArray = jsonObject.getJSONArray(GeneraliConstants.TRANSACTIONS);
	            	 }

	            }catch(InputValidationException e){
	            	JSONObject jsonResponse= new JSONObject();
		        	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
		        	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
		        	jsonResponse.put("statusMessage", e.getMessage());                    	
		        	jsonRsArray.put(new JSONObject().put("StatusData", jsonResponse));	
	            }
	         }
	        LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
	        return jsonObjectRs.toString();
	}

	@Override
	public String retrieveCustomerList(String json) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveObservations(String json) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStatus(String json) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String retrieveAll(String json) throws BusinessException {
		  LOGGER.trace("Inside LifeEngageLMSServiceImpl RetrieveAll : json" + json);
	        JSONObject jsonObjectRs = new JSONObject();
	        String eAppResponse = null;
	        try {
	            final JSONObject jsonObject = new JSONObject(json);
	            final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
	            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
	            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
	            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
	            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
	            eAppResponse = retrieveAll(requestInfo, jsonRqArray);
	            LOGGER.trace("Inside LifeEngageLMSServiceImpl RetrieveAll : " + eAppResponse);
	            jsonObjectRs = new JSONObject(eAppResponse);
	        } catch (ParseException e) {
	            throwBusinessException(true, e.getMessage());
	        }
		return jsonObjectRs.toString();
	}

	private String retrieveAll(final RequestInfo requestInfo, final JSONArray jsonRqArray) throws BusinessException,ParseException {
		LOGGER.trace("Inside LifeEngageLMSServiceImpl RetrieveAll : requestInfo" + requestInfo);
        JSONObject jsonObject;
        JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        Integer chunkSize = null;
        if (jsonRqArray.length() > 0) {
        	final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
        	 if (jsonObj.has(Constants.TYPE) && jsonObj.getString(Constants.TYPE) != null && !"".equals(jsonObj.getString(Constants.TYPE))
	                    && !jsonObj.getString(Constants.TYPE).equals("null")) {
        		 String transactionArray = TRANSACTION_ARRAY;
        		 if (LMS.equals(jsonObj.getString(Constants.TYPE))) {
	                    transactionArray = lifeEngageLMSComponent.retrieveAllLMS(requestInfo, jsonObj);
	                    chunkSize = lmsLimit;
	                } 
        		 LOGGER.trace("Inside LifeEngageLMSServiceImpl RetrieveAll : transactionArray" + transactionArray);
	                transactionArray = transactionArray.replaceAll("<br>", "\\\\n");
	                jsonObject = new JSONObject(transactionArray);
	                jsonRsArray = jsonObject.getJSONArray(GeneraliConstants.TRANSACTIONS);
        	 }
        }
        LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, chunkSize);
        LOGGER.trace("Inside LifeEngageLMSServiceImpl RetrieveAll : jsonObjectRs" + jsonObjectRs.toString());
        return jsonObjectRs.toString();
	}
	
	/**
     * Retrieve by filter.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly = true)
    public String retrieveByFilter(String json) throws BusinessException {
        LOGGER.trace("Inside LifeEngageSyncServiceImpl.retrieveByFilter :json " + json);
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        JSONArray jsonRsArray = new JSONArray();
        String transactionArray = "{\"Transactions\":[]}";
        try {
        	/* Added for security_optimizations */
        	//LifeEngageValidationHelper.validateInputString(json.toString(),validationPattern);
        	/* Added for security_optimizations */
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            transactionArray = lifeEngageLMSComponent.retrieveByFilter(requestInfo, jsonRqArray);
            jsonObject = new JSONObject(transactionArray);
            jsonRsArray = jsonObject.getJSONArray(TRANSACTIONS);

            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.retrieveByFilterCount: Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
            /* Added for security_optimizations */
        } /*catch (InputValidationException e) {
        	JSONObject jsonResponse= new JSONObject();
         	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
         	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
         	jsonResponse.put("statusMessage", e.getMessage());                    	
         	jsonObjectRs.put("StatusData", jsonResponse);
        }*/
        	/* Added for security_optimizations */
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveByFilterCount : jsonObjectRs" + jsonObjectRs.toString());
        return jsonObjectRs.toString();
    }

	@Override
	public String delete(String json) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

}
