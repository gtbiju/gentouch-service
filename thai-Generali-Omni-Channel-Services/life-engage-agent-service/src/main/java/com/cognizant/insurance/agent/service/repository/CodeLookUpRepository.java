/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Mar 15, 2013
 */
package com.cognizant.insurance.agent.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cognizant.insurance.agent.domain.codes.CodeLookUp;



/**
 * The Interface interface CodeLookUpRepository.
 */
public interface CodeLookUpRepository extends JpaRepository<CodeLookUp, Long>, CodeLookUpRepositoryCusotm {

}
