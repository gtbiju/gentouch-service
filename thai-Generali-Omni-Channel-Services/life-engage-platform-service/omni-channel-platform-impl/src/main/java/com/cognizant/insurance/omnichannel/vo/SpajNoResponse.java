package com.cognizant.insurance.omnichannel.vo;

import java.util.List;

import com.cognizant.insurance.response.vo.ResponseInfo;

public class SpajNoResponse {

	private ResponseInfo responseInfo;
	private List<String> spajNos;
	private String agentId;
	private String Key16;

	public ResponseInfo getResponseInfo() {
		return responseInfo;
	}

	public void setResponseInfo(ResponseInfo responseInfo) {
		this.responseInfo = responseInfo;
	}

	public List<String> getSpajNos() {
		return spajNos;
	}

	public void setSpajNos(List<String> formatedProposalNos) {
		this.spajNos = formatedProposalNos;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public void setKey16(String key16) {
		Key16 = key16;
	}

	public String getKey16() {
		return Key16;
	}

}
