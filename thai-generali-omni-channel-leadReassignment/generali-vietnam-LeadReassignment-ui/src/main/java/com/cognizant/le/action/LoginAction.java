package com.cognizant.le.action;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cognizant.le.manager.AgentServiceManager;
import com.cognizant.le.model.dto.LoginStatus;




/**
 * Servlet implementation class LoginAction
 */
@WebServlet("/LoginAction")
public class LoginAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(LoginAction.class);
	
	private WebApplicationContext springContext;
	
	@Autowired
	AgentServiceManager service;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 *//*
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}*/

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.debug("Method Entry : LoginAction.doPost");
		//boolean validUser = false;
		boolean isReAssign = false;
		boolean isUnauthorized = false;
		JSONObject agentDetailsJSON = new JSONObject();
		
		LoginStatus loginProfile= new LoginStatus();
		
		/*Properties properties = new Properties();
		String propertiesFile = getServletContext().getInitParameter("CONFIG_PROPERTIES");
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream stream = classLoader.getResourceAsStream("/" + propertiesFile);
		if(null == stream) {
			stream = classLoader.getResourceAsStream(propertiesFile);
		}
		properties.load(stream);*/
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		//String userAction = (String) request.getAttribute("userAction");
		
	    HttpSession session = request.getSession(true);
	    
	    //String defaultLanguage = properties.getProperty("DEFAULT_LANGUAGE");
	    
		/*if((null==defaultLanguage) || "".equals(defaultLanguage.trim())) {
			LOGGER.error("Property DEFAULT_LANGUAGE is not defined");
		} 
		session.setAttribute("DEFAULT_LANGUAGE", defaultLanguage);*/

	    try {
	    	String userType="";	    	
	    	if(null==username||"".equals(username)||null==password||"".equals(password)){
	    		loginProfile.setEmpty(true);
	    		loginProfile.setInvalidUser(true);
	    		request.setAttribute("status", loginProfile);
	    		request.getRequestDispatcher("/index.jsp").forward(request, response);
	    	}
	    	else{
	    		
	    			userType = service.isValidUser(username, password);
	    		
	    		if(userType.equalsIgnoreCase("rm_reassign")){
	    			isReAssign = true;
	    			agentDetailsJSON = service.getAgentDetails(username, "terminateAgents");
	    		}else if(userType.equalsIgnoreCase("User Terminated or Suspended")){
	    			isUnauthorized = true;
	    		}
	    		if(isReAssign){
						request.getSession().setAttribute("Login", "success");
						session.setAttribute("username", username);
						session.setAttribute("userType", userType);				
						request.setAttribute("agentDetailsJSON", agentDetailsJSON);
						request.getRequestDispatcher("jsp/LeadAssignment.jsp").forward(request, response);				
					} else if(isUnauthorized){
						loginProfile.setEmpty(false);
						loginProfile.setInvalidUser(false);
						loginProfile.setUnauthorized(true);
						request.setAttribute("status", loginProfile);
				        request.getRequestDispatcher("/index.jsp").forward(request, response);	
					} else {			
						loginProfile.setEmpty(false);
			    		loginProfile.setInvalidUser(true);
			    		loginProfile.setUnauthorized(false);
						request.setAttribute("status", loginProfile);
				        request.getRequestDispatcher("/index.jsp").forward(request, response);		       
						}
	       }
		} catch (Exception e) {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
			LOGGER.error("Exception in LoginAction.doPost "+e);
		}
		LOGGER.debug("Method Exit : LoginAction.doPost");		
	}
	
	 public void init(ServletConfig config)throws ServletException {
				super.init(config);
				 springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
			        final AutowireCapableBeanFactory beanFactory = springContext.getAutowireCapableBeanFactory();
			        beanFactory.autowireBean(this);	   
		  }
}
