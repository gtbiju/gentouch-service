/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commonclasses;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;

/**
 * This concept is defined as the super type for all specification concepts (e.g. blueprints / designs).
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "SPECIFICATION_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class Specification extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4082713499649513644L;

    /**
     * The word or phrase used to identify (but not uniquely) the specification.
     * 
     * 
     * 
     */
    private String name;

    /**
     * A free-text statement used to explain the meaning of the specification.
     * 
     * 
     * 
     */
    private String description;

    /**
     * Identifies the version of a specification.
     * 
     * 
     * 
     */
    private String version;

    /**
     * A name that is unique used to define the kind of element this specification is describing.
     * 
     * 
     * 
     */
    private String kindOfElementName;

    /**
     * This attribute can be used as an alias resulting from the different hierarchy level names or organization of the
     * sub-components of the product definition.
     * 
     * 
     * 
     */
    private String shortName;

    /**
     * This attribute specifies the version date.
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date versionDate;

    /**
     * The date on which the first manufactured item was manufactured based on
     * the model specification.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date firstManufacturedDate;

    /**
     * The date at which the last object of this model specification has been
     * manufactured.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date lastManufacturedDate;

    /**
     * The suggested purchase price of this particular model specification,
     * based on the features fitted as standard. 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private CurrencyAmount listPriceNewAmount;

    /**
     * The classification of this model specification that identifies the family
     * or range to which it belongs.
     * 
     * e.g: Bayer Asperin for drugs.
     * 
     * e.g: Ford Mondeo, Opel Vectra, Ford Taurus for cars.
     * 
     * 
     * 
     */
    private String make;

    /**
     * The classification of this particular model specification that
     * distinguishes it from other models in the same range (make).
     * 
     * e.g: EcoWash 1100 for a Zanussi washing-machine.
     * 
     * 
     * 
     */
    private String model;

    /**
     * The year for this model specification, and is part of the unique
     * identification of the model. Note that model specifications may change
     * from year to year.
     * 
     * e.g: options than the 1995 model.
     * 
     * 
     * 
     */
    private Integer modelYear;

    /**
     * The approximate value at a given point in time for a physical object,
     * based on the specification defined for this model specification.
     * 
     * e.g: Determine the current value of a 1989 SAAB 2.0l. as of today. <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private CurrencyAmount estimatedMarketAmount;

    /**
     * The classification of a collection of produced items based on a common
     * set of construction characteristics such as construction specification,
     * place of construction and period of construction. Series are subgroups of
     * models.
     * 
     * 
     * 
     */
    private String series;

    /**
     * Gets the firstManufacturedDate.
     * 
     * @return Returns the firstManufacturedDate.
     */
    public Date getFirstManufacturedDate() {
        return firstManufacturedDate;
    }

    /**
     * Gets the lastManufacturedDate.
     * 
     * @return Returns the lastManufacturedDate.
     */
    public Date getLastManufacturedDate() {
        return lastManufacturedDate;
    }

    /**
     * Gets the listPriceNewAmount.
     * 
     * @return Returns the listPriceNewAmount.
     */
    public CurrencyAmount getListPriceNewAmount() {
        return listPriceNewAmount;
    }

    /**
     * Gets the make.
     * 
     * @return Returns the make.
     */
    public String getMake() {
        return make;
    }

    /**
     * Gets the model.
     * 
     * @return Returns the model.
     */
    public String getModel() {
        return model;
    }

    /**
     * Gets the modelYear.
     * 
     * @return Returns the modelYear.
     */
    public Integer getModelYear() {
        return modelYear;
    }

    /**
     * Gets the estimatedMarketAmount.
     * 
     * @return Returns the estimatedMarketAmount.
     */
    public CurrencyAmount getEstimatedMarketAmount() {
        return estimatedMarketAmount;
    }

    /**
     * Gets the series.
     * 
     * @return Returns the series.
     */
    public String getSeries() {
        return series;
    }

    /**
     * Sets The firstManufacturedDate.
     * 
     * @param firstManufacturedDate
     *            The firstManufacturedDate to set.
     */
    public void setFirstManufacturedDate(
            final Date firstManufacturedDate) {
        this.firstManufacturedDate = firstManufacturedDate;
    }

    /**
     * Sets The lastManufacturedDate.
     * 
     * @param lastManufacturedDate
     *            The lastManufacturedDate to set.
     */
    public void
            setLastManufacturedDate(final Date lastManufacturedDate) {
        this.lastManufacturedDate = lastManufacturedDate;
    }

    /**
     * Sets The listPriceNewAmount.
     * 
     * @param listPriceNewAmount
     *            The listPriceNewAmount to set.
     */
    public void setListPriceNewAmount(final CurrencyAmount listPriceNewAmount) {
        this.listPriceNewAmount = listPriceNewAmount;
    }

    /**
     * Sets The make.
     * 
     * @param make
     *            The make to set.
     */
    public void setMake(final String make) {
        this.make = make;
    }

    /**
     * Sets The model.
     * 
     * @param model
     *            The model to set.
     */
    public void setModel(final String model) {
        this.model = model;
    }

    /**
     * Sets The modelYear.
     * 
     * @param modelYear
     *            The modelYear to set.
     */
    public void setModelYear(final Integer modelYear) {
        this.modelYear = modelYear;
    }

    /**
     * Sets The estimatedMarketAmount.
     * 
     * @param estimatedMarketAmount
     *            The estimatedMarketAmount to set.
     */
    public void setEstimatedMarketAmount(
            final CurrencyAmount estimatedMarketAmount) {
        this.estimatedMarketAmount = estimatedMarketAmount;
    }

    /**
     * Sets The series.
     * 
     * @param series
     *            The series to set.
     */
    public void setSeries(final String series) {
        this.series = series;
    }

    
    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the version.
     * 
     * @return Returns the version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Gets the kindOfElementName.
     * 
     * @return Returns the kindOfElementName.
     */
    public String getKindOfElementName() {
        return kindOfElementName;
    }

    /**
     * Gets the shortName.
     * 
     * @return Returns the shortName.
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The version.
     * 
     * @param version
     *            The version to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * Sets The kindOfElementName.
     * 
     * @param kindOfElementName
     *            The kindOfElementName to set.
     */
    public void setKindOfElementName(final String kindOfElementName) {
        this.kindOfElementName = kindOfElementName;
    }

    /**
     * Sets The shortName.
     * 
     * @param shortName
     *            The shortName to set.
     */
    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return the versionDate
     */
    public Date getVersionDate() {
        return versionDate;
    }

    /**
     * @param versionDate
     *            the versionDate to set
     */
    public void setVersionDate(final Date versionDate) {
        this.versionDate = versionDate;
    }

}
