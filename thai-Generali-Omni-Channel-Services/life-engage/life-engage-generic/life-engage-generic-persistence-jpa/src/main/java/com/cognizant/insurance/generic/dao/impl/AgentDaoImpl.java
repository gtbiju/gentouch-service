package com.cognizant.insurance.generic.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.generic.dao.AgentDao;
import com.cognizant.insurance.request.Request;

public class AgentDaoImpl extends DaoImpl implements AgentDao {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory
			.getLogger(AgentDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cognizant.insurance.generic.dao.AgentDao#retrieveAgentByCode(com.
	 * cognizant.insurance.request.Request)
	 */
	@Override
	public Agent retrieveAgentByCode(Request<Agent> request) {
		Agent retrievedAgent = null;
		Agent inpAgent = request.getType();
		String query = "SELECT agent FROM Agent agent WHERE agent.agentCode = ?1";
		List<Agent> agentsList = findByQuery(request, query,
				inpAgent.getAgentCode());
		if (agentsList.size() > 0) {
			retrievedAgent = agentsList.get(0);
		}
		return retrievedAgent;
	}
}
