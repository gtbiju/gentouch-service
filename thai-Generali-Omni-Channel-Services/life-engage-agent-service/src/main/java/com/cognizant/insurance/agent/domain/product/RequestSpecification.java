/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;

/**
 * The Class RequestSpecification.
 *
 * @author 304007
 */

//@Entity
//@Table(name = "core_prod_request_spec")

public class RequestSpecification extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 267096574533854643L;

    /** The name. */
    private String name;
    
    /** The version. */
    private String version;

    /** The is allowed for. */
    private Boolean isAllowedFor;
    
    /** The includes. */
    private String includes;
    
    /** The request type. */
    private RequestType requestType;
    
    /** The parent product. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private ProductSpecification parentProduct;

    /**
     * The Constructor.
     */
    public RequestSpecification() {
        super();
    }

    /**
     * Gets the is allowed for.
     *
     * @return the checks if is allowed for
     */
    public Boolean getIsAllowedFor() {
        return isAllowedFor;
    }

    /**
     * Sets the is allowed for.
     *
     * @param isAllowedFor the checks if is allowed for
     */
    public void setIsAllowedFor(final Boolean isAllowedFor) {
        this.isAllowedFor = isAllowedFor;
    }

    /**
     * Gets the includes.
     *
     * @return the includes
     */
    public String getIncludes() {
        return includes;
    }

    /**
     * Sets the includes.
     *
     * @param includes the includes
     */
    public void setIncludes(final String includes) {
        this.includes = includes;
    }

    /**
     * Gets the name.
     *
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The name.
     *
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the version.
     *
     * @return Returns the version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets The version.
     *
     * @param version The version to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * Gets the requestType.
     *
     * @return Returns the requestType.
     */
    public RequestType getRequestType() {
        return requestType;
    }

    /**
     * Sets The requestType.
     *
     * @param requestType The requestType to set.
     */
    public void setRequestType(final RequestType requestType) {
        this.requestType = requestType;
    }

    /**
     * Gets the parentProduct.
     *
     * @return Returns the parentProduct.
     */
    public ProductSpecification getParentProduct() {
        return parentProduct;
    }

    /**
     * Sets The parentProduct.
     *
     * @param parentProduct The parentProduct to set.
     */
    public void setParentProduct(final ProductSpecification parentProduct) {
        this.parentProduct = parentProduct;
    }
}
