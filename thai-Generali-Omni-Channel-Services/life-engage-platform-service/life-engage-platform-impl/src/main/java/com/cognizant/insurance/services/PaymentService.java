/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * The Interface interface PaymentService.
 */
public interface PaymentService {

    /**
     * Make payment.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String makePayment(String json) throws BusinessException;

    /**
     * Gets the payment options.
     * 
     * @param json
     *            the json
     * @return the payment options
     * @throws BusinessException
     *             the business exception
     */
    String getPaymentOptions(String json) throws BusinessException;
}
