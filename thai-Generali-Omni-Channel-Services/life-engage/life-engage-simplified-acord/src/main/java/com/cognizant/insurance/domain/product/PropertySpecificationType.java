/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class PropertySpecificationType.
 *
 * @author 304007
 */

@Entity
@DiscriminatorValue("Property")
public class PropertySpecificationType extends Type {

    /**
     *
     */
    private static final long serialVersionUID = -7048572048054466313L;

    /**
     * The Constructor.
     */
    public PropertySpecificationType() {
        super();
    }

    /**
     * The Constructor.
     *
     * @param id the id
     */
    public PropertySpecificationType(final String id) {
        super(id);
    }

    /** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "propertyType")
    private Set<PropertySpecification> propertySpecifications;

    /**
     * @param propertySpecifications the propertySpecifications to set
     */
    public void setPropertySpecifications(Set<PropertySpecification> propertySpecifications) {
        this.propertySpecifications = propertySpecifications;
    }

    /**
     * @return the propertySpecifications
     */
    public Set<PropertySpecification> getPropertySpecifications() {
        return propertySpecifications;
    }
}
