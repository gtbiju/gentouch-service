/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 325754
 * @version       : 0.1, Oct 5, 2015
 */
package com.cognizant.insurance.life.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LifeEngageSyncService;
import com.cognizant.insurance.services.RequirementFileUploadService;
import com.cognizant.insurance.utils.Base64Utils;

/**
 * @author 325754
 * 
 */

@ContextConfiguration("/service-context-test.xml")
public class TestLifeEnagageRequirementDocFileService extends AbstractJUnit4SpringContextTests {
    /** The document service. */
    @Autowired
    RequirementFileUploadService requirementFileUploadService;

    /** The response. */
    private MockHttpServletResponse response;

    /** The data source. */
    @Autowired
    private DataSource dataSource;

    /** The life engage sync service. */
    @Autowired
    LifeEngageSyncService lifeEngageSyncService;

    /** The Constant KEY6. */
    private static final String KEY6 = "Key6";

    /** The Constant KEY7. */
    private static final String KEY7 = "Key7";

    /** The Constant KEY15. */
    private static final String KEY15 = "Key15";

    /** The Constant KEY4. */
    private static final String KEY4 = "Key4";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant STATUS. */
    private static final String STATUS = "Status";

    /** The Constant STATUS_CODE. */
    private static final String STATUS_CODE = "StatusCode";

    /** The Constant STATUS_DATA. */
    private static final String STATUS_DATA = "StatusData";

    /** The Constant STATUS_MESSAGE. */
    private static final String STATUS_MESSAGE = "StatusMessage";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";

    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";

    @Value("${le.platform.service.fileUploadPath}")
    private String fileUploadPath;

    @Value("${le.platform.service.slash}")
    private String fileSeparator;

    @Test
    public void testUploadRequirement() throws IOException, BusinessException, ParseException {
        String agentId;
        final SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Calendar calendar = new GregorianCalendar();
        final String currentDate = newDt.format(calendar.getTime());
        agentId = "Agent1";
        String productTypeFirst = "Term";
        String insFstNmeFst = "JohansUplaodDoc";
        String insuredLastNameFirst = "Nems";
        String proposerFirstNameFirst = "Johns";
        String proposerLastNameFirst = "Nemaas";
        String productFirstproductname = "lifeEngage";
        String statusCode100 = "100";
        String statusSuccess = "SUCCESS";
        String statusMessage = "Proposal Saved";
        String type = "eApp";

        // creating the eapp save request
        String saveRequest = getRequest("eAppRequest/eAppServiceRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", productFirstproductname)
                    .replace("{4}", productTypeFirst)
                    .replace("{5}", insuredLastNameFirst)
                    .replace("{6}", insFstNmeFst)
                    .replace("{7}", proposerLastNameFirst)
                    .replace("{8}", proposerFirstNameFirst)
                    .replace("{9}", "")
                    .replace("{10}", "")
                    .replace("{11}", "Saved")
                    .replace("{12}", "Saved")
                    .replace("{13}", "Saved");

        // Saving eapp Request
        String responseJson = lifeEngageSyncService.save(saveRequest);

        assertNotNull(responseJson);
        JSONArray jsonRsTransactionsArray =
                new JSONObject(responseJson).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        // assertEquals("Initial", jsonObj.getString(KEY15));
        String eappIdFirst = jsonObj.getString(KEY4);
        assertNotNull(eappIdFirst);
        jsonObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(jsonObj);
        assertEquals(statusCode100, jsonObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, jsonObj.getString(STATUS));
        assertEquals(statusMessage, jsonObj.getString(STATUS_MESSAGE));

        // Retrieve eApp and see if document and requirement are saved

        String retrieveRequest = getRequest("eAppRequest/eAppServiceRq_Retrieve.json");
        retrieveRequest = retrieveRequest.replace("{1}", currentDate).replace("{2}", eappIdFirst);
        // System.out.println(retrieveRequest);
        String retrieveResponseJson = lifeEngageSyncService.retrieve(retrieveRequest);
        // System.out.println(retrieveResponseJson);
        assertNotNull(retrieveResponseJson);

        // uploading first Requirement doc file object - status Saved

        String uploadRequirementDocFileRequest1 = getRequest("requirementRequest/UploadSingleRequirementDocFile.json");
        String workingDir = System.getProperty("user.dir");
        String doc1 = Base64Utils.encodeFileToBase64Binary(workingDir + "/src/test/resources/documentRequest/img1.jpg");
        uploadRequirementDocFileRequest1 =
                uploadRequirementDocFileRequest1.replace("{1}", eappIdFirst)
                    .replace("{2}", "ReqName1_4f7b67a5-5870-429f-80e1-6a351725b299")
                    .replace("{3}", "docname1_4f7b67a5-5870-429f-80e1-6a351725b299")
                    .replace("{4}", "Saved")
                    .replace("{5}", "testfile.jpg")
                    .replace("{6}", doc1);

        // System.out.println(uploadRequirementDocFileRequest1);
        String uploadRequirementResponse1 =
                requirementFileUploadService.uploadRequirementDocFile(uploadRequirementDocFileRequest1);
        assertNotNull(uploadRequirementResponse1);

        jsonRsTransactionsArray =
                new JSONObject(uploadRequirementResponse1).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertNotNull(jsonObj);
        jsonObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(jsonObj);
        assertEquals(statusCode100, jsonObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, jsonObj.getString(STATUS));
        assertEquals("Successfully uploaded document file", jsonObj.getString(STATUS_MESSAGE));

        String retrieveRequestafterfileUpload = getRequest("eAppRequest/eAppServiceRq_Retrieve.json");
        retrieveRequestafterfileUpload =
                retrieveRequestafterfileUpload.replace("{1}", currentDate).replace("{2}", eappIdFirst);
        // System.out.println(retrieveRequest);
        String retrieveResponseafterfileUpload = lifeEngageSyncService.retrieve(retrieveRequestafterfileUpload);
        assertNotNull(retrieveResponseafterfileUpload);

        /*
         * String uploadRequirementDocFileRequest2 =
         * getRequest("requirementRequest/UploadSingleRequirementDocFile.json"); System.out.println("eappIdFirst ==== "
         * + eappIdFirst); uploadRequirementDocFileRequest2 = uploadRequirementDocFileRequest2.replace("{1}",
         * eappIdFirst) .replace("{2}", "ReqName1_4f7b67a5-5870-429f-80e1-6a351725b299") .replace("{3}",
         * "docname1_4f7b67a5-5870-429f-80e1-6a351725b299") .replace("{4}", "Deleted") .replace("{5}", "testfile.jpg");
         * 
         * System.out.println(uploadRequirementDocFileRequest2); String uploadRequirementResponse2 =
         * requirementFileUploadService.uploadRequirementDocFile(uploadRequirementDocFileRequest2);
         * System.out.println(uploadRequirementResponse2); assertNotNull(uploadRequirementResponse2);
         * 
         * jsonRsTransactionsArray = new JSONObject(uploadRequirementResponse1).getJSONObject(RESPONSE)
         * .getJSONObject(RESPONSE_PAYLOAD) .getJSONArray(TRANSACTIONS); assertEquals(1,
         * jsonRsTransactionsArray.length()); jsonObj = jsonRsTransactionsArray.getJSONObject(0);
         * assertNotNull(jsonObj); jsonObj = jsonObj.getJSONObject(STATUS_DATA); assertNotNull(jsonObj);
         * assertEquals(statusCode100, jsonObj.getString(STATUS_CODE)); assertEquals(statusSuccess,
         * jsonObj.getString(STATUS)); assertEquals("Successfully uploaded document file",
         * jsonObj.getString(STATUS_MESSAGE));
         */

        String retrieveRequestafterfileUploadforDelete = getRequest("eAppRequest/eAppServiceRq_Retrieve.json");
        retrieveRequestafterfileUploadforDelete =
                retrieveRequestafterfileUploadforDelete.replace("{1}", currentDate).replace("{2}", eappIdFirst);
        // System.out.println(retrieveRequest);
        String retrieveResponseafterfileUploadDelete = lifeEngageSyncService.retrieve(retrieveRequestafterfileUpload);
        assertNotNull(retrieveResponseafterfileUploadDelete);

        String retrieveFileRequest = getRequest("requirementRequest/getSingleRequirementDocFile.json");
        retrieveFileRequest =
                retrieveFileRequest.replace("{1}", eappIdFirst)
                    .replace("{2}", "ReqName1_4f7b67a5-5870-429f-80e1-6a351725b299")
                    .replace("{3}", "docname1_4f7b67a5-5870-429f-80e1-6a351725b299")
                    .replace("{4}", "testfile.jpg");

        // System.out.println(retrieveFileRequest);
        String retrieveFileResponse = requirementFileUploadService.getRequirementDocumentFile(retrieveFileRequest);
        assertNotNull(retrieveFileResponse);

        String retrieveFileListRequest = getRequest("requirementRequest/getRequirementDocFilesList.json");
        retrieveFileListRequest =
                retrieveFileListRequest.replace("{1}", eappIdFirst)
                    .replace("{2}", "ReqName1_4f7b67a5-5870-429f-80e1-6a351725b299")
                    .replace("{3}", "docname1_4f7b67a5-5870-429f-80e1-6a351725b299");

        String retrieveFileListResponse =
                requirementFileUploadService.getRequirementDocFilesList(retrieveFileListRequest);
        System.out.println(retrieveFileListResponse);
        assertNotNull(retrieveFileListResponse);

    }

    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(new String(line.getBytes(), "UTF-8"));
        }

        return sb.toString();
    }

}
