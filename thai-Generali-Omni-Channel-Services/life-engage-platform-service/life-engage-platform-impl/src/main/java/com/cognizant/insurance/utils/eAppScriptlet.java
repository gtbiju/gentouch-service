package com.cognizant.insurance.utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import java.util.StringTokenizer;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.Coverage;
import com.cognizant.insurance.domain.agreement.agreementpremium.Premium;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Appointee;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.commonelements.commonclasses.GLISelectedOption;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.contactandplace.ContactPoint;
import com.cognizant.insurance.domain.contactandplace.ContactPreference;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.AddressNatureCodeList;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.ElectronicContact;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.PostalAddressContact;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.TelephoneCallContact;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.City;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.Country;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.CountryElement;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.CountrySubdivision;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.extension.Extension;
import com.cognizant.insurance.domain.finance.FinancialScheduler;
import com.cognizant.insurance.domain.finance.paymentmethodsubtypes.CreditCardPayment;
import com.cognizant.insurance.domain.party.Party;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.party.partycodelists.EducationLevelCodeList;
import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;
import com.cognizant.insurance.domain.party.partyname.PersonName;
import com.cognizant.insurance.domain.party.persondetailsubtypes.EducationDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.IncomeDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.OccupationDetail;
import com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes.EmploymentRelationship;

// TODO: Auto-generated Javadoc
/**
 * The Class eAppScriptlet.
 */
public class eAppScriptlet extends JRDefaultScriptlet {

	/** The person names. */
	private Set<PersonName> personNames;

	/** The person. */
	private Person person;

	/** The person details. */
	private Set<PersonDetail> personDetails;

	/** The income detail. */
	private IncomeDetail incomeDetail;

	/** The countries. */
	private Set<Country> countries;

	/** The contact preferences. */
	private Set<ContactPreference> contactPreferences;

	/** The postal address contact. */
	private PostalAddressContact postalAddressContact;

	/** The education detail. */
	private EducationDetail educationDetail;

	/** The permanent address. */
	private PostalAddressContact permanentAddress;

	/** The occupation detail. */
	private OccupationDetail occupationDetail;

	/** The home1 contact. */
	private TelephoneCallContact home1Contact;

	/** The home2 contact. */
	private TelephoneCallContact home2Contact;

	/** The electronic contact. */
	private ElectronicContact electronicContact;

	/** Declaration Questions set**/
	private Set<UserSelection> declationQuestions;
	private String agentSignDate;
	private String agentSignPlace;
	private Date spajSignDate;
	private String SpajSignPlace;
	private String spajAccountHolderName;
	private String spajBankName;
	private String spajBranchName;
	private String spajAccountNumber;

	/** The card detail. */
	private String insuredIdCardType;
	private String insuredIdCardNumber;

	/** Height and Weight. */
	private BigDecimal insuredHeight;
	private BigDecimal insuredWeight;

	private Set<UserSelection> insuredUserSelections;
	private String insuredHasHeartcirulatoryBloodDisorder;
	private String insuredHasAsthmaPnuemonia;
	private String insuredHasHepatitisGastrointestinal;
	private String insuredHasGenitoUrinary;
	private String insuredMetabolicAndEndorcrineDisorder;
	private String insuredHasNeurologicalAndPsychiatricDisorder;
	private String insuredHasHIVOrBoneDisorder;
	private String insuredHasSpinalDisorder;
	private String insuredHasCancerMelanoma;
	private String insuredCurrentlyPregnent;
	private String insuredOtherDiseases;
	private String insuredReceivedMedTreatment;
	private String insuredHasUsedTobacco;
	private String insuredDrugsOrNarcotics;
	private String insuredHazardousActivities;
	private String insuredFamilyMembersDiagWithDiseases;
	private Integer insuredTobaccoSticks;

	/** The appointee. */
	private Appointee appointee;

	/** The appointee person. */
	private Person appointeePerson;

	/** The appointee person names. */
	private Set<PersonName> appointeePersonNames;

	/** The appointee contact preferences. */
	private Set<ContactPreference> appointeeContactPreferences;

	/** The appointee countries. */
	private Set<Country> appionteeCountries;

	/** The appointee postal address contact. */
	private PostalAddressContact appointeePostalAddressContact;

	/** The appointee permanent address. */
	private PostalAddressContact appointeePermanentAddress;

	/** The appointee home1 contact. */
	private TelephoneCallContact appointeeHome1Contact;

	/** The appointee home2 contact. */
	private TelephoneCallContact appointeeHome2Contact;

	/** The appointee electronic contact. */
	private ElectronicContact appointeeElectronicContact;

	/** The appointee agreement Documents. */
	public Set<Document> appionteeDocuments;

	//--------------------------//
	/** The proposer. */
	private AgreementHolder proposer;

	/** The proposer person. */
	private Person proposerPerson;

	/** The proposer person names. */
	private Set<PersonName> proposerPersonNames;

	/** The proposer dob. */
	private Date proposerdob;
	private Date insuredDob;

	String proposerFullName;

	/** The proposer person details. */
	private Set<PersonDetail> proposerPersonDetails;

	/** The proposer contact preferences. */
	private Set<ContactPreference> proposerContactPreferences;

	/** The proposer countries. */
	private Set<Country> proposerCountries;

	/** The proposer countries. */
	private Country proposerResidenceCountry;
	private Country insuredResidenceCountry;

	/** The proposer electronic contact. */
	private ElectronicContact proposerElectronicContact;

	/** The proposer postal address contact. */
	private PostalAddressContact proposerPostalAddressContact;

	/** The proposer permanent address. */
	private PostalAddressContact proposerPermanentAddress;

	private TelephoneCallContact telephoneCallContact;

	private String insuredMobileNumber1;
	private String insuredMobileNumber2;

	private String insuredHomeNumber1;
	private String insuredHomeNumber2;

	private String proposerMobileNumber1;
	private String proposerMobileNumber2;

	private String proposerHomeNumber1;
	private String proposerHomeNumber2;

	private String relationshipWithProposer = "";
	private String insuredReasonForInsurance;
	/** The proposer occupation detail. */
	private OccupationDetail proposerOccupationDetail;

	/** The proposer card detail. */
	private String proposerIdCardType;
	private String proposerIdCardNumber;

	IncomeDetail proposerIncomeDetail;
	EducationDetail proposerEducationDetail;
	String proposerPaymentMethod;


	/** The additional insured list. */
	private Set<Insured> additionalInsuredes;

	Insured firstAddInsured;
	Insured secondAddInsured;
	Insured thirdAddInsured;
	Insured fourthAddInsured;
	private Person firstAddInsuredPerson;
	private Person secondAddInsuredPerson;
	private Person thirdAddInsuredPerson;
	private Person fourthAddInsuredPerson;
	private Set<UserSelection> firstAddInsuredUserSelections;
	private Set<UserSelection> secondAddInsuredUserSelections;
	private Set<UserSelection> thirdAddInsuredUserSelections;
	private Set<UserSelection> fourthAddInsuredUserSelections;


	private Date firstAddInsuredDob;
	private Set<PersonName> firstAddInsuredPersonNames;
	private Set<PersonDetail> firstAddInsuredPersonDetails;
	//private Set<ContactPreference> firstAddInsuredContactPreferences;
	private Set<Country> firstAddInsuredCountries;
	private Country firstAddInsuredResidenceCountry;
	private OccupationDetail firstAddInsuredOccupationDetail;
	private String firstAddInsuredRelationWithInsured;
	private BigDecimal firstAddInsuredHeight;
	private BigDecimal firstAddInsuredWeight;
	private String firstAddInsuredIdCardType;
	private String firstAddInsuredIdCardNumber;
	private String firstAddInsuredHasHeartcirulatoryBloodDisorder;
	private String firstAddInsuredHasAsthmaPnuemonia;
	private String firstAddInsuredHasHepatitisGastrointestinal;
	private String firstAddInsuredHasGenitoUrinary;
	private String firstAddInsuredMetabolicAndEndorcrineDisorder;
	private String firstAddInsuredHasNeurologicalAndPsychiatricDisorder;
	private String firstAddInsuredHasHIVOrBoneDisorder;
	private String firstAddInsuredHasSpinalDisorder;
	private String firstAddInsuredHasCancerMelanoma;
	private String firstAddInsuredCurrentlyPregnent;
	private String firstAddInsuredOtherDiseases;
	private String firstAddInsuredReceivedMedTreatment;
	private String firstAddInsuredHasUsedTobacco;
	private String firstAddInsuredDrugsOrNarcotics;
	private String firstAddInsuredHazardousActivities;
	private String firstAddInsuredFamilyMembersDiagWithDiseases;
	private String firstAddInsuredPregnentDescription;

	Date secondAddInsuredDob;
	private Set<PersonName> secondAddInsuredPersonNames;
	private Set<PersonDetail> secondAddInsuredPersonDetails;
	//private Set<ContactPreference> secondAddInsuredContactPreferences;
	private Set<Country> secondAddInsuredCountries;
	Country secondAddInsuredResidenceCountry;
	OccupationDetail secondAddInsuredOccupationDetail;
	String secondAddInsuredRelationWithInsured;
	private String secondAddInsuredIdCardType;
	private String secondAddInsuredIdCardNumber;
	private BigDecimal secondAddInsuredHeight;
	private BigDecimal secondAddInsuredWeight;
	private String secondAddInsuredHasHeartcirulatoryBloodDisorder;
	private String secondAddInsuredHasAsthmaPnuemonia;
	private String secondAddInsuredHasHepatitisGastrointestinal;
	private String secondAddInsuredHasGenitoUrinary;
	private String secondAddInsuredMetabolicAndEndorcrineDisorder;
	private String secondAddInsuredHasNeurologicalAndPsychiatricDisorder;
	private String secondAddInsuredHasHIVOrBoneDisorder;
	private String secondAddInsuredHasSpinalDisorder;
	private String secondAddInsuredHasCancerMelanoma;
	private String secondAddInsuredCurrentlyPregnent;
	private String secondAddInsuredOtherDiseases;
	private String secondAddInsuredReceivedMedTreatment;
	private String secondAddInsuredHasUsedTobacco;
	private String secondAddInsuredDrugsOrNarcotics;
	private String secondAddInsuredHazardousActivities;
	private String secondAddInsuredFamilyMembersDiagWithDiseases;
	private String secondAddInsuredPregnentDescription;


	Date thirdAddInsuredDob;
	private Set<PersonName> thirdAddInsuredPersonNames;
	private Set<PersonDetail> thirdAddInsuredPersonDetails;
	//private Set<ContactPreference> thirdAddInsuredContactPreferences;
	private Set<Country> thirdAddInsuredCountries;
	Country thirdAddInsuredResidenceCountry;
	OccupationDetail thirdAddInsuredOccupationDetail;
	String thirdAddInsuredRelationWithInsured;
	private String thirdAddInsuredIdCardType;
	private String thirdAddInsuredIdCardNumber;
	private BigDecimal thirdAddInsuredHeight;
	private BigDecimal thirdAddInsuredWeight;
	private String thirdAddInsuredHasHeartcirulatoryBloodDisorder;
	private String thirdAddInsuredHasAsthmaPnuemonia;
	private String thirdAddInsuredHasHepatitisGastrointestinal;
	private String thirdAddInsuredHasGenitoUrinary;
	private String thirdAddInsuredMetabolicAndEndorcrineDisorder;
	private String thirdAddInsuredHasNeurologicalAndPsychiatricDisorder;
	private String thirdAddInsuredHasHIVOrBoneDisorder;
	private String thirdAddInsuredHasSpinalDisorder;
	private String thirdAddInsuredHasCancerMelanoma;
	private String thirdAddInsuredCurrentlyPregnent;
	private String thirdAddInsuredOtherDiseases;
	private String thirdAddInsuredReceivedMedTreatment;
	private String thirdAddInsuredHasUsedTobacco;
	private String thirdAddInsuredDrugsOrNarcotics;
	private String thirdAddInsuredHazardousActivities;
	private String thirdAddInsuredFamilyMembersDiagWithDiseases;
	private String thirdAddInsuredPregnentDescription;



	Date fourthAddInsuredDob;
	private Set<PersonName> fourthAddInsuredPersonNames;
	private Set<PersonDetail> fourthAddInsuredPersonDetails;
	//private Set<ContactPreference> fourthAddInsuredContactPreferences;
	private Set<Country> fourthAddInsuredCountries;
	Country fourthAddInsuredResidenceCountry;
	OccupationDetail fourthAddInsuredOccupationDetail;
	String fourthAddInsuredRelationWithInsured;
	private String fourthAddInsuredIdCardType;
	private String fourthAddInsuredIdCardNumber;
	private BigDecimal fourthAddInsuredHeight;
	private BigDecimal fourthAddInsuredWeight;
	private String fourthAddInsuredHasHeartcirulatoryBloodDisorder;
	private String fourthAddInsuredHasAsthmaPnuemonia;
	private String fourthAddInsuredHasHepatitisGastrointestinal;
	private String fourthAddInsuredHasGenitoUrinary;
	private String fourthAddInsuredMetabolicAndEndorcrineDisorder;
	private String fourthAddInsuredHasNeurologicalAndPsychiatricDisorder;
	private String fourthAddInsuredHasHIVOrBoneDisorder;
	private String fourthAddInsuredHasSpinalDisorder;
	private String fourthAddInsuredHasCancerMelanoma;
	private String fourthAddInsuredCurrentlyPregnent;
	private String fourthAddInsuredOtherDiseases;
	private String fourthAddInsuredReceivedMedTreatment;
	private String fourthAddInsuredHasUsedTobacco;
	private String fourthAddInsuredDrugsOrNarcotics;
	private String fourthAddInsuredHazardousActivities;
	private String fourthAddInsuredFamilyMembersDiagWithDiseases;
	private String fourthAddInsuredPregnentDescription;


	String insurancePurposeBusiness;
	String insurancePurposeDesc;
	String insurancePurposeEducation;
	String insurancePurposeInvestment;
	String insurancePurposePensionFund;
	String insurancePurposeProtection;
	private String insurancePurposeOther;

	String creditCardType;
	String creditCardName;
	Long creditCardNumber;
	Date creditCardExpiryDate;
	String accountHolderName;
	String bankName;
	String branchName;
	Long accountNumber;

	private PremiumPayer payer;
	/** Premium Payer**/
	private String premiumPayer;
	/** The payer person details. */
	private Set<PersonDetail> payerPersonDetails;
	/** The payer contact preferences. */
	private Set<ContactPreference> payerContactPreferences;
	private PostalAddressContact payerPermanentAddress;
	private Person payerPerson;
	private Set<PersonName> payerPersonNames;
	private IncomeDetail payerIncomeDetail;

	/** The beneficiary list. */

	private Person firstBeneficiaryPerson;
	private Set<PersonName> firstBeneficiaryPersonNames;
	private Date firstBeneficiaryDOB;
	private String firstBeneficiaryGender;
	private Float firstBeneficiarySharePercentage;
	private String firstBeneficiaryRelationWithInsured;

	private Person secondBeneficiaryPerson;
	private Set<PersonName> secondBeneficiaryPersonNames;
	private Date secondBeneficiaryDOB;
	private String secondBeneficiaryGender;
	private Float secondBeneficiarySharePercentage;
	private String secondBeneficiaryRelationWithInsured;

	private Person thirdBeneficiaryPerson;
	private Set<PersonName> thirdBeneficiaryPersonNames;
	private Date thirdBeneficiaryDOB;
	private String thirdBeneficiaryGender;
	private Float thirdBeneficiarySharePercentage;
	private String thirdBeneficiaryRelationWithInsured;

	private Person fourthBeneficiaryPerson;
	private Set<PersonName> fourthBeneficiaryPersonNames;
	private Date fourthBeneficiaryDOB;
	private String fourthBeneficiaryGender;
	private Float fourthBeneficiarySharePercentage;
	private String fourthBeneficiaryRelationWithInsured;

	private Person fifthBeneficiaryPerson;
	private Set<PersonName> fifthBeneficiaryPersonNames;
	private Date fifthBeneficiaryDOB;
	private String fifthBeneficiaryGender;
	private Float fifthBeneficiarySharePercentage;
	private String fifthBeneficiaryRelationWithInsured;


	private String empty="";


	private Set<UserSelection> proposerUserSelections;
	String proposerFatcaQues1;
	String proposerFatcaQues2;

	private Set<UserSelection> firstBeneficiarySelectedOptions;
	private Set<UserSelection> secondBeneficiarySelectedOptions;
	private Set<UserSelection> thirdBeneficiarySelectedOptions;
	private Set<UserSelection> fourthBeneficiarySelectedOptions;
	private Set<UserSelection> fifthBeneficiarySelectedOptions;
	Boolean beneficiaryFatcaQues1;
	Boolean beneficiaryFatcaQues2;

	String spajNumber;
	String illustrationNumber;
	String agentCode;

	String firstAddinsuredLostWeight;
	String firstAddinsuredLostWeightReason;
	String secondAddinsuredLostWeight;
	String secondAddinsuredLostWeightReason;
	String thirdAddinsuredLostWeight;
	String thirdAddinsuredLostWeightReason;
	String fourthAddinsuredLostWeight;
	String fourthAddinsuredLostWeightReason;

	private String insuredLostWeight;
	private String insuredLostWeightReason;
	private String insuredPregnentDescription;


	private AgreementProducer agent;
	private Person agentPerson;
	private Set<ContactPreference> agentContactPreference;
	private String agencyName;
	private TelephoneCallContact agencyPhoneContact;
	private String agentMobileNo;
	private String agentLandNo;
	private String agentName;

	/** Previous Policy */
	private Set<Insured> insuredPreviousPolicyList;
	private String firstCompanyName;
	private String firstPrevInsuredName;
	private String firstFinalDecision;
	private String firstTypeOfInsurance;
	private BigDecimal firstPrevSumAssured;
	private String firstPolicyStatus;

	private String secondCompanyName;
	private String secondPrevInsuredName;
	private String secondFinalDecision;
	private String secondTypeOfInsurance;
	private BigDecimal secondPrevSumAssured;
	private String secondPolicyStatus;

	private String thirdCompanyName;
	private String thirdPrevInsuredName;
	private String thirdFinalDecision;
	private String thirdTypeOfInsurance;
	private BigDecimal thirdPrevSumAssured;
	private String thirdPolicyStatus;

	private String fourthCompanyName;
	private String fourthPrevInsuredName;
	private String fourthFinalDecision;
	private String fourthTypeOfInsurance;
	private BigDecimal fourthPrevSumAssured;
	private String fourthPolicyStatus;

	private String fifthCompanyName;
	private String fifthPrevInsuredName;
	private String fifthFinalDecision;
	private String fifthTypeOfInsurance;
	private BigDecimal fifthPrevSumAssured;
	private String fifthPolicyStatus;

	private String gemilangHealthQuest1;
	private String gemilangHealthQuest2;
	private String gemilangHealthQuest3;

	private Boolean isPropDiffFromInsured;

	private String firstAddInsuredReasonForInsurance;
	private String secondAddInsuredReasonForInsurance;
	private String thirdAddInsuredReasonForInsurance;
	private String fourthAddInsuredReasonForInsurance;

	private String description;

	private Integer firstAddInsuredTobaccoSticks;
	private Integer secondAddInsuredTobaccoSticks;
	private Integer thirdAddInsuredTobaccoSticks;
	private Integer fourthAddInsuredTobaccoSticks;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(eAppScriptlet.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see net.sf.jasperreports.engine.JRDefaultScriptlet#beforeReportInit()
	 */
	public void beforeDetailEval() {
		try {
			com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured insured = (com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured) this
					.getFieldValue("insured");

			appointee = (com.cognizant.insurance.domain.agreement.partyroleinagreement.Appointee) this
					.getFieldValue("appointee");
			additionalInsuredes = (Set<Insured>) this
					.getFieldValue("additionalInsuredes");

			try{
				agent =  (AgreementProducer) this.getFieldValue("agreementProducer");
			}catch(Exception e){
				LOGGER.error("Error while getting fieldValue agent "+e.getMessage());
			}

			try{
				insuredPreviousPolicyList =  (Set<Insured>) this.getFieldValue("insuredPreviousPolicyList");
			}catch(Exception e){
				LOGGER.error("Error while getting fieldValue insuredPreviousPolicyList "+e.getMessage());
			}
			try{
				if (null != insured) {
					person = (Person) insured.getPlayerParty();
					relationshipWithProposer = insured.getRelationshipWithProposer();
					insuredReasonForInsurance = insured.getReasonForInsurance();
					description = insured.getDescription();
				}
			}catch(Exception e){
				LOGGER.error("Error  "+e.getMessage());
			}
			
			try{
				if (null != appointee) {
					appointeePerson = (Person) appointee.getPlayerParty();
				}
			}catch(Exception e){
				LOGGER.error("Error  appointeePerson"+e.getMessage());
			}
			
			try{
			if (null != appointeePerson) {
				appointeePersonNames = appointeePerson.getName();
				appointeeContactPreferences = appointeePerson
						.getPreferredContact();
				appionteeCountries = appointeePerson.getNationalityCountry();
			}
			}catch(Exception e){
				LOGGER.error("Error  appointeePerson"+e.getMessage());
			}
			
			try{
			if (null != person) {
				personNames = person.getName();
				personDetails = person.getDetail();
				contactPreferences = person.getPreferredContact();
				countries = person.getNationalityCountry();
				insuredResidenceCountry = person.getResidenceCountry();
				insuredDob = person.getBirthDate();
				insuredIdCardType = person.getNationalIdType();
				insuredIdCardNumber = person.getNationalId();
				if(person.getHeight()!= null && !"".equals(person.getHeight())){
					insuredHeight = person.getHeight().getValue();
				}
				if(person.getWeight()!= null && !"".equals(person.getWeight())){
					insuredWeight = person.getWeight().getValue();
				}
			}
			}catch(Exception e){
				LOGGER.error("Error  appointeePerson"+e.getMessage());
			}
			
			if (null != personDetails) {
				for (PersonDetail personDetail : personDetails) {
					if (personDetail instanceof IncomeDetail) {
						incomeDetail = (IncomeDetail) personDetail;
					} else if (personDetail instanceof EducationDetail) {
						educationDetail = (EducationDetail) personDetail;
					} else if (personDetail instanceof OccupationDetail) {
						occupationDetail = (OccupationDetail) personDetail;
					}
				}
			}


			if (null != contactPreferences) {
				for (ContactPreference contactPreference : contactPreferences) {
					ContactPoint contactPoint = contactPreference
							.getPreferredContactPoint();
					if (contactPoint instanceof PostalAddressContact) {
						if (((PostalAddressContact) contactPoint)
								.getAddressNatureCode().equals(
										AddressNatureCodeList.Current)) {
							postalAddressContact = (PostalAddressContact) contactPoint;
						} else if (((PostalAddressContact) contactPoint)
								.getAddressNatureCode().equals(
										AddressNatureCodeList.Permanent)) {
							permanentAddress = (PostalAddressContact) contactPoint;
						}

					} else if (contactPoint instanceof TelephoneCallContact) {
						if (null != contactPreference.getPriorityLevel()) {
							if ((contactPreference.getPriorityLevel() == 1)) {
								home1Contact = (TelephoneCallContact) contactPoint;
							} else if (contactPreference.getPriorityLevel() == 2) {
								home2Contact = (TelephoneCallContact) contactPoint;
							}
						}
					} else if (contactPoint instanceof ElectronicContact) {
						electronicContact = (ElectronicContact) contactPoint;
					}
				}
			}


			com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument document =
					(com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument) this.getFieldValue("document");

			if (null != document) {
				appionteeDocuments = document.getPages();

			}
			//--------------------------------//
			try {
				proposer = (com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder) this
						.getFieldValue("proposer");
				proposerFatcaQues1="No";
				proposerFatcaQues2="No";
				if (null != proposer) {
					proposerPerson = (Person) proposer.getPlayerParty();
					proposerUserSelections = proposer.getSelectedOptions();
					isPropDiffFromInsured = proposer.getProposerDifferentFromInsuredIndicator();
				}
				if(proposerUserSelections != null && proposerUserSelections.size() >0){
					for(UserSelection selection:proposerUserSelections){
						if(selection != null && selection.getQuestionnaireType() != null){
							if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
								if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
										selection.getSelectedOption() == true){
									proposerFatcaQues1 = "Yes";
								}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
										selection.getSelectedOption() == true){
									proposerFatcaQues2 = "Yes";
								}
							}
						}
					}
				}

				if (null != proposerPerson) {
					proposerdob = proposerPerson.getBirthDate();
					proposerPersonNames = proposerPerson.getName();
					proposerPersonDetails = proposerPerson.getDetail();
					proposerContactPreferences = proposerPerson.getPreferredContact();
					proposerCountries = proposerPerson.getNationalityCountry();
					proposerResidenceCountry = proposerPerson.getResidenceCountry();
					proposerIdCardType = proposerPerson.getNationalIdType();
					proposerIdCardNumber = proposerPerson.getNationalId();
				}

				if (null != proposerPersonDetails) {
					for (PersonDetail personDetail : proposerPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							proposerIncomeDetail = (IncomeDetail) personDetail;
						} else if (personDetail instanceof EducationDetail) {
							proposerEducationDetail = (EducationDetail) personDetail;
						} else
							if (personDetail instanceof OccupationDetail) {
								proposerOccupationDetail = (OccupationDetail) personDetail;
							}
					}
				}
			}
			catch(Exception e){
				LOGGER.error("ScriptletSample Error :while getting field value proposer"+ e);
			}
			try{
				payer = (PremiumPayer) this.getFieldValue("payer");
				//premiumPayer = payer.getPartyType();
				payerPerson=(Person) payer.getPlayerParty();
				if (payerPerson != null) {
					payerPersonNames = payerPerson.getName();
					payerPersonDetails = payerPerson.getDetail();
					payerContactPreferences=payerPerson.getPreferredContact();
				}
				if (null != payerPersonDetails) {
					for (PersonDetail personDetail : payerPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							payerIncomeDetail = (IncomeDetail) personDetail;
						}
					}
				}
			} catch(Exception e){
				LOGGER.error("ScriptletSample Error :while getting field value payer"+ e);
			}

			beneficiaryFatcaQues1=false;
			beneficiaryFatcaQues2=false;
			Set<Beneficiary> beneficiaries=null;
			try{
				beneficiaries = (Set<Beneficiary>) this.getFieldValue("beneficiaries");

				Beneficiary firstBeneficiary = null;
				Beneficiary secondBeneficiary = null;
				Beneficiary thirdBeneficiary = null;
				Beneficiary fourthBeneficiary = null;
				Beneficiary fifthBeneficiary = null;

				if(beneficiaries != null){
					for(Beneficiary beneficiary:beneficiaries){
						if(beneficiary != null ){
							for(Extension benExtension : beneficiary.getPlayerParty().getIncludesExtension()){
								if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("1")){
									firstBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("2")){
									secondBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("3")){
									thirdBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("4")){
									fourthBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("5")){
									fifthBeneficiary = beneficiary;
								}
							}
						}
					}

					if(firstBeneficiary != null){
						firstBeneficiarySelectedOptions=firstBeneficiary.getSelectedOptions();
						firstBeneficiaryPerson = (Person)firstBeneficiary.getPlayerParty();
						if(firstBeneficiaryPerson != null){
							firstBeneficiaryPersonNames = firstBeneficiaryPerson.getName();
						}
						if(firstBeneficiarySelectedOptions != null && firstBeneficiarySelectedOptions.size() >0){
							for(UserSelection selection:firstBeneficiarySelectedOptions){
								if(selection != null && selection.getQuestionnaireType() != null){
									if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
										if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
												selection.getSelectedOption() == true){
											beneficiaryFatcaQues1 = selection.getSelectedOption();
										}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
												selection.getSelectedOption() == true){
											beneficiaryFatcaQues2 = selection.getSelectedOption();
										}
									}
								}
							}
						}
						firstBeneficiaryDOB = firstBeneficiaryPerson.getBirthDate();
						firstBeneficiaryGender = firstBeneficiaryPerson.getGenderCode().name();
						firstBeneficiarySharePercentage = firstBeneficiary.getSharePercentage();
						firstBeneficiaryRelationWithInsured = firstBeneficiary.getRelationWithInsured();
					}

					if(secondBeneficiary != null){
						secondBeneficiarySelectedOptions=secondBeneficiary.getSelectedOptions();
						secondBeneficiaryPerson = (Person)secondBeneficiary.getPlayerParty();
						if(secondBeneficiaryPerson != null){
							secondBeneficiaryPersonNames = secondBeneficiaryPerson.getName();
						}
						secondBeneficiaryDOB = secondBeneficiaryPerson.getBirthDate();
						secondBeneficiaryGender = secondBeneficiaryPerson.getGenderCode().name();
						secondBeneficiarySharePercentage = secondBeneficiary.getSharePercentage();
						secondBeneficiaryRelationWithInsured = secondBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(secondBeneficiarySelectedOptions != null && secondBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:secondBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null && 
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();
											}
										}
									}
								}
							}
						}
					}

					if(thirdBeneficiary != null){
						thirdBeneficiarySelectedOptions=thirdBeneficiary.getSelectedOptions();
						thirdBeneficiaryPerson = (Person)thirdBeneficiary.getPlayerParty();
						if(thirdBeneficiaryPerson != null){
							thirdBeneficiaryPersonNames = thirdBeneficiaryPerson.getName();
						}
						thirdBeneficiaryDOB = thirdBeneficiaryPerson.getBirthDate();
						thirdBeneficiaryGender = thirdBeneficiaryPerson.getGenderCode().name();
						thirdBeneficiarySharePercentage = thirdBeneficiary.getSharePercentage();
						thirdBeneficiaryRelationWithInsured = thirdBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(thirdBeneficiarySelectedOptions != null && thirdBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:thirdBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null && 
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();
											}
										}
									}
								}
							}
						}
					}

					if(fourthBeneficiary != null){
						fourthBeneficiarySelectedOptions=fourthBeneficiary.getSelectedOptions();
						fourthBeneficiaryPerson = (Person)fourthBeneficiary.getPlayerParty();
						if(fourthBeneficiaryPerson != null){
							fourthBeneficiaryPersonNames = fourthBeneficiaryPerson.getName();
						}

						fourthBeneficiaryDOB = fourthBeneficiaryPerson.getBirthDate();
						fourthBeneficiaryGender = fourthBeneficiaryPerson.getGenderCode().name();
						fourthBeneficiarySharePercentage = fourthBeneficiary.getSharePercentage();
						fourthBeneficiaryRelationWithInsured = fourthBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(fourthBeneficiarySelectedOptions != null && fourthBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:fourthBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null && 
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();

											}

										}
									}
								}
							}
						}
					}

					if(fifthBeneficiary != null){
						fifthBeneficiarySelectedOptions=fifthBeneficiary.getSelectedOptions();
						fifthBeneficiaryPerson = (Person)fifthBeneficiary.getPlayerParty();
						if(fifthBeneficiaryPerson != null){
							fifthBeneficiaryPersonNames = fifthBeneficiaryPerson.getName();
						}
						fifthBeneficiaryDOB = fifthBeneficiaryPerson.getBirthDate();
						fifthBeneficiaryGender = fifthBeneficiaryPerson.getGenderCode().name();
						fifthBeneficiarySharePercentage = fifthBeneficiary.getSharePercentage();
						fifthBeneficiaryRelationWithInsured = fifthBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(fifthBeneficiarySelectedOptions != null && fifthBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:fifthBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();

											}
										}
									}
								}
							}
						}
					}

				}
			}catch (Exception e) {
				LOGGER.error("ScriptletSample Error : populating beneficiaries:",e);
			}


			try {
				InsuranceAgreement insuranceAgreement = (InsuranceAgreement) this.getFieldValue("proposal");
				try{
					if(insuranceAgreement != null){
						TransactionKeys transactionKey = insuranceAgreement.getTransactionKeys();
						if(transactionKey != null){
							spajNumber = transactionKey.getKey21();
							illustrationNumber = transactionKey.getKey24();
							//agentCode = transactionKey.getKey11();
						}
					}
				}catch(Exception e){
					LOGGER.error("Error while getting keys "+e.getMessage());
				}


				if(insuranceAgreement != null && insuranceAgreement.getAgreementExtension() != null){
					insurancePurposeBusiness = insuranceAgreement.getAgreementExtension().getInsurancePurposeBusiness();
					insurancePurposeDesc = insuranceAgreement.getAgreementExtension().getInsurancePurposeDesc();
					insurancePurposeEducation = insuranceAgreement.getAgreementExtension().getInsurancePurposeEducation();
					insurancePurposeInvestment = insuranceAgreement.getAgreementExtension().getInsurancePurposeInvestment();
					insurancePurposePensionFund = insuranceAgreement.getAgreementExtension().getInsurancePurposePensionFund();
					insurancePurposeProtection = insuranceAgreement.getAgreementExtension().getInsurancePurposeProtection();
					insurancePurposeOther = insuranceAgreement.getAgreementExtension().getInsurancePurposeOther();
					declationQuestions=insuranceAgreement.getAgreementExtension().getSelectedOptions();
					agentSignDate = insuranceAgreement.getAgreementExtension().getAgentSignDate();
					agentSignPlace = insuranceAgreement.getAgreementExtension().getAgentSignPlace();
					spajSignDate = insuranceAgreement.getAgreementExtension().getSpajDate();
					SpajSignPlace = insuranceAgreement.getAgreementExtension().getSpajSignPlace();
					spajAccountHolderName = insuranceAgreement.getAgreementExtension().getSpajDeclarationName1();
					spajBankName = insuranceAgreement.getAgreementExtension().getSpajDeclarationName2();
					spajAccountNumber = insuranceAgreement.getAgreementExtension().getSpajDeclarationName3();
					spajBranchName = insuranceAgreement.getAgreementExtension().getSpajDeclarationName4();
					premiumPayer = insuranceAgreement.getAgreementExtension().getPremiumPayerType();
				}

				if(insuranceAgreement != null){
					for(Premium premium	:insuranceAgreement.getAgreementPremium()){
						if(("RenewalPremium").equals(premium.getNatureCode().name())){
							for(FinancialScheduler financialScheduler :premium.getAttachedFinancialScheduler()){
								proposerPaymentMethod =  financialScheduler.getPaymentMethodCode().name();
								CreditCardPayment creditCardPayment= (CreditCardPayment) financialScheduler.getPaymentMeans();
								creditCardName=creditCardPayment.getCardholderName().getFullName();
								creditCardType = creditCardPayment.getCreditCardTypeCode().name();
								creditCardNumber = creditCardPayment.getCardIdentifier();
								creditCardExpiryDate =  creditCardPayment.getExpiryDate();
								//accountHolderName = payer.getPartyType();
								/*for(Party owner :(financialScheduler.getRelatedBankAccount().getOwner())){
									Person ownerPrson=(Person)owner;
									for(PersonName accountPersonName:ownerPrson.getName()){
										accountHolderName = payer.getPartyType();
									}
								}*/
								bankName = financialScheduler.getRelatedBankAccount().getBankName();
								branchName = financialScheduler.getRelatedBankAccount().getBranchName();
								accountNumber=financialScheduler.getRelatedBankAccount().getAccountIdentifier();
							}

						}
					}
				}

			}catch(Exception e){
				LOGGER.error("ScriptletSample Error :while getting field value proposer"+ e);
			}

			try{
				if(agent != null){
					agentPerson = (Person)agent.getPlayerParty();
					agentCode = agent.getPartyIdentifier();
					if(agentPerson != null){
						agentContactPreference = agentPerson.getPreferredContact();
					}

				}

				if(agentContactPreference != null){

					if (null != agentContactPreference) {
						for (ContactPreference contactPreference : agentContactPreference) {
							ContactPoint contactPoint = contactPreference.getPreferredContactPoint();
							if (contactPoint instanceof PostalAddressContact) {
								PostalAddressContact agentAgncyDetails = (PostalAddressContact)contactPoint;
								agencyName = agentAgncyDetails.getAddressLine1();
							} else if (contactPoint instanceof TelephoneCallContact) {
								agencyPhoneContact = (TelephoneCallContact) contactPoint;
								if(agencyPhoneContact.getTypeCode().name().
										equalsIgnoreCase("Mobile")){
									agentMobileNo = agencyPhoneContact.getFullNumber();
								}else if(agencyPhoneContact.getTypeCode().name().
										equalsIgnoreCase("Home")){
									agentLandNo = agencyPhoneContact.getFullNumber();
								}
							}
						}
					}
				}
			}catch(Exception e){
				LOGGER.error("Error while getting agent details : "+e.getMessage());
			}

			try{

				for(Insured additionalInsured :additionalInsuredes){
					if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("1")){
						firstAddInsured = additionalInsured;
					}else if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("2")){
						secondAddInsured = additionalInsured;
					}else if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("3")){
						thirdAddInsured = additionalInsured;
					}else if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("4")){
						fourthAddInsured = additionalInsured;
					}
				}

				if (null != firstAddInsured) {
					firstAddInsuredReasonForInsurance = firstAddInsured.getReasonForInsurance();
					firstAddInsuredPerson = (Person) firstAddInsured.getPlayerParty();
					firstAddInsuredRelationWithInsured = firstAddInsured.getRelationshipWithProposer();
					firstAddInsuredUserSelections =  firstAddInsured.getSelectedOptions();
					if(firstAddInsuredUserSelections != null && firstAddInsuredUserSelections.size() > 0){
						for(UserSelection addUserselections:firstAddInsuredUserSelections){
							if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Health")){
								if(addUserselections.getQuestionId() == 1){
									if(addUserselections.getSelectedOption()){
										firstAddinsuredLostWeight = "Yes";
										firstAddinsuredLostWeightReason = addUserselections.getDetailedInfo();
										/*for(GLISelectedOption optn : addUserselections.getSelectedOptions()){
											firstAddinsuredLostWeightReason = optn.getSelectedOptionValue();
										}*/
									}else{
										firstAddinsuredLostWeight = "No";
									}

								}else if(addUserselections.getQuestionId() == 2 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHasHeartcirulatoryBloodDisorder = "Yes";
									}else{
										firstAddInsuredHasHeartcirulatoryBloodDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 3 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHasAsthmaPnuemonia = "Yes";
									}else{
										firstAddInsuredHasAsthmaPnuemonia = "No";
									}
								}else if(addUserselections.getQuestionId() == 4 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHasHepatitisGastrointestinal = "Yes";
									}else{
										firstAddInsuredHasHepatitisGastrointestinal = "No";
									}
								}else if(addUserselections.getQuestionId() == 5 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHasGenitoUrinary = "Yes";
									}else{
										firstAddInsuredHasGenitoUrinary = "No";
									}
								}else if(addUserselections.getQuestionId() == 6 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredMetabolicAndEndorcrineDisorder = "Yes";
									}else{
										firstAddInsuredMetabolicAndEndorcrineDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 7 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHasNeurologicalAndPsychiatricDisorder = "Yes";
									}else{
										firstAddInsuredHasNeurologicalAndPsychiatricDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 8 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHasHIVOrBoneDisorder ="Yes";
									}else{
										firstAddInsuredHasHIVOrBoneDisorder ="No";
									}
								}else if(addUserselections.getQuestionId() == 9 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHasSpinalDisorder ="Yes";
									}else{
										firstAddInsuredHasSpinalDisorder ="No";
									}
								}else if(addUserselections.getQuestionId() == 10 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHasCancerMelanoma ="Yes";
									}else{
										firstAddInsuredHasCancerMelanoma ="No";
									}
								}else if(addUserselections.getQuestionId() == 13 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredCurrentlyPregnent ="Yes";
										firstAddInsuredPregnentDescription = ""+addUserselections.getUnit();
									}else{
										firstAddInsuredCurrentlyPregnent ="No";
									}
								}else if(addUserselections.getQuestionId() == 11 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredOtherDiseases ="Yes";
									}else{
										firstAddInsuredOtherDiseases ="No";
									}
								}else if(addUserselections.getQuestionId() == 12 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredReceivedMedTreatment ="Yes";
									}else{
										firstAddInsuredReceivedMedTreatment ="No";
									}
								}
							}else if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Lifestyle")){
								if(addUserselections.getQuestionId() == 1 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHasUsedTobacco = "Yes";
										firstAddInsuredTobaccoSticks = addUserselections.getUnit();
									}else{
										firstAddInsuredHasUsedTobacco = "No";
									}
								}else if(addUserselections.getQuestionId() == 2 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredDrugsOrNarcotics = "Yes";
									}else{
										firstAddInsuredDrugsOrNarcotics = "No";
									}
								}else if (addUserselections.getQuestionId() == 3 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredHazardousActivities = "Yes";
									}else{
										firstAddInsuredHazardousActivities = "No";
									}
								}
							}

							else if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Other")){
								if (addUserselections.getQuestionId() == 1 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										firstAddInsuredFamilyMembersDiagWithDiseases = "Yes";
									}else{
										firstAddInsuredFamilyMembersDiagWithDiseases = "No";
									}
								}
							}

						}
					}
				}
				if (null != secondAddInsured) {
					secondAddInsuredReasonForInsurance = secondAddInsured.getReasonForInsurance();
					secondAddInsuredPerson = (Person) secondAddInsured.getPlayerParty();
					secondAddInsuredPerson = (Person) secondAddInsured.getPlayerParty();
					secondAddInsuredRelationWithInsured = secondAddInsured.getRelationshipWithProposer();
					secondAddInsuredUserSelections =  secondAddInsured.getSelectedOptions();
					if(secondAddInsuredUserSelections != null && secondAddInsuredUserSelections.size() > 0){
						for(UserSelection addUserselections:secondAddInsuredUserSelections){
							if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Health")){
								if(addUserselections.getQuestionId() == 1){
									if(addUserselections.getSelectedOption()){
										secondAddinsuredLostWeight = "Yes";
										secondAddinsuredLostWeightReason = addUserselections.getDetailedInfo();
										/*for(GLISelectedOption optn : addUserselections.getSelectedOptions()){
											secondAddinsuredLostWeightReason = optn.getSelectedOptionValue();
										}*/
									}else{
										secondAddinsuredLostWeight = "No";
									}

								}else if(addUserselections.getQuestionId() == 2 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHasHeartcirulatoryBloodDisorder = "Yes";
									}else{
										secondAddInsuredHasHeartcirulatoryBloodDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 3 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHasAsthmaPnuemonia = "Yes";
									}else{
										secondAddInsuredHasAsthmaPnuemonia = "No";
									}
								}else if(addUserselections.getQuestionId() == 4 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHasHepatitisGastrointestinal = "Yes";
									}else{
										secondAddInsuredHasHepatitisGastrointestinal = "No";
									}
								}else if(addUserselections.getQuestionId() == 5 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHasGenitoUrinary = "Yes";
									}else{
										secondAddInsuredHasGenitoUrinary = "No";
									}
								}else if(addUserselections.getQuestionId() == 6 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredMetabolicAndEndorcrineDisorder = "Yes";
									}else{
										secondAddInsuredMetabolicAndEndorcrineDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 7 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHasNeurologicalAndPsychiatricDisorder = "Yes";
									}else{
										secondAddInsuredHasNeurologicalAndPsychiatricDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 8 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHasHIVOrBoneDisorder ="Yes";
									}else{
										secondAddInsuredHasHIVOrBoneDisorder ="No";
									}
								}else if(addUserselections.getQuestionId() == 9 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHasSpinalDisorder ="Yes";
									}else{
										secondAddInsuredHasSpinalDisorder ="No";
									}
								}else if(addUserselections.getQuestionId() == 10 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHasCancerMelanoma ="Yes";
									}else{
										secondAddInsuredHasCancerMelanoma ="No";
									}
								}else if(addUserselections.getQuestionId() == 13 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredCurrentlyPregnent ="Yes";
										secondAddInsuredPregnentDescription = ""+addUserselections.getUnit();
									}else{
										secondAddInsuredCurrentlyPregnent ="No";
									}
								}else if(addUserselections.getQuestionId() == 11 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredOtherDiseases ="Yes";
									}else{
										secondAddInsuredOtherDiseases ="No";
									}
								}else if(addUserselections.getQuestionId() == 12 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredReceivedMedTreatment ="Yes";
									}else{
										secondAddInsuredReceivedMedTreatment ="No";
									}
								}
							}else if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Lifestyle")){
								if(addUserselections.getQuestionId() == 1 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHasUsedTobacco = "Yes";
										secondAddInsuredTobaccoSticks = addUserselections.getUnit();
									}else{
										secondAddInsuredHasUsedTobacco = "No";
									}
								}else if(addUserselections.getQuestionId() == 2 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredDrugsOrNarcotics = "Yes";
									}else{
										secondAddInsuredDrugsOrNarcotics = "No";
									}
								}else if (addUserselections.getQuestionId() == 3 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredHazardousActivities = "Yes";
									}else{
										secondAddInsuredHazardousActivities = "No";
									}
								}
							}

							else if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Other")){
								if (addUserselections.getQuestionId() == 1 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										secondAddInsuredFamilyMembersDiagWithDiseases = "Yes";
									}else{
										secondAddInsuredFamilyMembersDiagWithDiseases = "No";
									}
								}
							}

						}
					}
				}
				if (null != thirdAddInsured) {
					thirdAddInsuredReasonForInsurance = thirdAddInsured.getReasonForInsurance();
					thirdAddInsuredPerson = (Person) thirdAddInsured.getPlayerParty();
					thirdAddInsuredRelationWithInsured = thirdAddInsured.getRelationshipWithProposer();
					thirdAddInsuredUserSelections =  thirdAddInsured.getSelectedOptions();
					if(thirdAddInsuredUserSelections != null && thirdAddInsuredUserSelections.size() > 0){
						for(UserSelection addUserselections:thirdAddInsuredUserSelections){
							if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Health")){

								if(addUserselections.getQuestionId() == 1){
									if(addUserselections.getSelectedOption()){
										thirdAddinsuredLostWeight = "Yes";
										thirdAddinsuredLostWeightReason = addUserselections.getDetailedInfo();
										/*for(GLISelectedOption optn : addUserselections.getSelectedOptions()){
											thirdAddinsuredLostWeightReason = optn.getSelectedOptionValue();
										}*/
									}else{
										thirdAddinsuredLostWeight = "No";
									}

								}else if(addUserselections.getQuestionId() == 2 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHasHeartcirulatoryBloodDisorder = "Yes";
									}else{
										thirdAddInsuredHasHeartcirulatoryBloodDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 3 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHasAsthmaPnuemonia = "Yes";
									}else{
										thirdAddInsuredHasAsthmaPnuemonia = "No";
									}
								}else if(addUserselections.getQuestionId() == 4 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHasHepatitisGastrointestinal = "Yes";
									}else{
										thirdAddInsuredHasHepatitisGastrointestinal = "No";
									}
								}else if(addUserselections.getQuestionId() == 5 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHasGenitoUrinary = "Yes";
									}else{
										thirdAddInsuredHasGenitoUrinary = "No";
									}
								}else if(addUserselections.getQuestionId() == 6 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredMetabolicAndEndorcrineDisorder = "Yes";
									}else{
										thirdAddInsuredMetabolicAndEndorcrineDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 7 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHasNeurologicalAndPsychiatricDisorder = "Yes";
									}else{
										thirdAddInsuredHasNeurologicalAndPsychiatricDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 8 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHasHIVOrBoneDisorder ="Yes";
									}else{
										thirdAddInsuredHasHIVOrBoneDisorder ="No";
									}
								}else if(addUserselections.getQuestionId() == 9 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHasSpinalDisorder ="Yes";
									}else{
										thirdAddInsuredHasSpinalDisorder ="No";
									}
								}else if(addUserselections.getQuestionId() == 10 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHasCancerMelanoma ="Yes";
									}else{
										thirdAddInsuredHasCancerMelanoma ="No";
									}
								}else if(addUserselections.getQuestionId() == 13 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredCurrentlyPregnent ="Yes";
										thirdAddInsuredPregnentDescription = ""+addUserselections.getUnit();
									}else{
										thirdAddInsuredCurrentlyPregnent ="No";
									}
								}else if(addUserselections.getQuestionId() == 11 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredOtherDiseases ="Yes";
									}else{
										thirdAddInsuredOtherDiseases ="No";
									}
								}else if(addUserselections.getQuestionId() == 12 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredReceivedMedTreatment ="Yes";
									}else{
										thirdAddInsuredReceivedMedTreatment ="No";
									}
								}
							}else if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Lifestyle")){
								if(addUserselections.getQuestionId() == 1 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHasUsedTobacco = "Yes";
										thirdAddInsuredTobaccoSticks = addUserselections.getUnit();
									}else{
										thirdAddInsuredHasUsedTobacco = "No";
									}
								}else if(addUserselections.getQuestionId() == 2 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredDrugsOrNarcotics = "Yes";
									}else{
										thirdAddInsuredDrugsOrNarcotics = "No";
									}
								}else if (addUserselections.getQuestionId() == 3 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredHazardousActivities = "Yes";
									}else{
										thirdAddInsuredHazardousActivities = "No";
									}
								}
							}

							else if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Other")){
								if (addUserselections.getQuestionId() == 1 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										thirdAddInsuredFamilyMembersDiagWithDiseases = "Yes";
									}else{
										thirdAddInsuredFamilyMembersDiagWithDiseases = "No";
									}
								}
							}

						}
					}
				}
				if (null != fourthAddInsured) {
					fourthAddInsuredReasonForInsurance = fourthAddInsured.getReasonForInsurance();
					fourthAddInsuredPerson = (Person) fourthAddInsured.getPlayerParty();
					fourthAddInsuredRelationWithInsured = fourthAddInsured.getRelationshipWithProposer();
					fourthAddInsuredUserSelections =  fourthAddInsured.getSelectedOptions();
					if(fourthAddInsuredUserSelections != null && fourthAddInsuredUserSelections.size() > 0){
						for(UserSelection addUserselections:fourthAddInsuredUserSelections){
							if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Health")){
								if(addUserselections.getQuestionId() == 1){
									if(addUserselections.getSelectedOption()){
										fourthAddinsuredLostWeight = "Yes";
										fourthAddinsuredLostWeightReason = addUserselections.getDetailedInfo();
										/*for(GLISelectedOption optn : addUserselections.getSelectedOptions()){
											fourthAddinsuredLostWeightReason = optn.getSelectedOptionValue();
										}*/
									}else{
										fourthAddinsuredLostWeight = "No";
									}

								}
								else if(addUserselections.getQuestionId() == 2 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHasHeartcirulatoryBloodDisorder = "Yes";
									}else{
										fourthAddInsuredHasHeartcirulatoryBloodDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 3 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHasAsthmaPnuemonia = "Yes";
									}else{
										fourthAddInsuredHasAsthmaPnuemonia = "No";
									}
								}else if(addUserselections.getQuestionId() == 4 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHasHepatitisGastrointestinal = "Yes";
									}else{
										fourthAddInsuredHasHepatitisGastrointestinal = "No";
									}
								}else if(addUserselections.getQuestionId() == 5 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHasGenitoUrinary = "Yes";
									}else{
										fourthAddInsuredHasGenitoUrinary = "No";
									}
								}else if(addUserselections.getQuestionId() == 6 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredMetabolicAndEndorcrineDisorder = "Yes";
									}else{
										fourthAddInsuredMetabolicAndEndorcrineDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 7 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHasNeurologicalAndPsychiatricDisorder = "Yes";
									}else{
										fourthAddInsuredHasNeurologicalAndPsychiatricDisorder = "No";
									}
								}else if(addUserselections.getQuestionId() == 8 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHasHIVOrBoneDisorder ="Yes";
									}else{
										fourthAddInsuredHasHIVOrBoneDisorder ="No";
									}
								}else if(addUserselections.getQuestionId() == 9 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHasSpinalDisorder ="Yes";
									}else{
										fourthAddInsuredHasSpinalDisorder ="No";
									}
								}else if(addUserselections.getQuestionId() == 10 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHasCancerMelanoma ="Yes";
									}else{
										fourthAddInsuredHasCancerMelanoma ="No";
									}
								}else if(addUserselections.getQuestionId() == 13 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredCurrentlyPregnent ="Yes";
										fourthAddInsuredPregnentDescription = ""+addUserselections.getUnit();
									}else{
										fourthAddInsuredCurrentlyPregnent ="No";
									}
								}else if(addUserselections.getQuestionId() == 11 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredOtherDiseases ="Yes";
									}else{
										fourthAddInsuredOtherDiseases ="No";
									}
								}else if(addUserselections.getQuestionId() == 12 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredReceivedMedTreatment ="Yes";
									}else{
										fourthAddInsuredReceivedMedTreatment ="No";
									}
								}
							}else if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Lifestyle")){
								if(addUserselections.getQuestionId() == 1 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHasUsedTobacco = "Yes";
										fourthAddInsuredTobaccoSticks = addUserselections.getUnit();
									}else{
										fourthAddInsuredHasUsedTobacco = "No";
									}
								}else if(addUserselections.getQuestionId() == 2 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredDrugsOrNarcotics = "Yes";
									}else{
										fourthAddInsuredDrugsOrNarcotics = "No";
									}
								}else if (addUserselections.getQuestionId() == 3 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredHazardousActivities = "Yes";
									}else{
										fourthAddInsuredHazardousActivities = "No";
									}
								}
							}

							else if(addUserselections != null &&
									addUserselections.getQuestionnaireType().name().equalsIgnoreCase("Other")){
								if (addUserselections.getQuestionId() == 1 && addUserselections.getSelectedOption() != null){
									if(addUserselections.getSelectedOption() == true){
										fourthAddInsuredFamilyMembersDiagWithDiseases = "Yes";
									}else{
										fourthAddInsuredFamilyMembersDiagWithDiseases = "No";
									}
								}
							}

						}
					}
				}

				if (null != firstAddInsuredPerson) {
					firstAddInsuredDob = firstAddInsuredPerson.getBirthDate();
					firstAddInsuredPersonNames = firstAddInsuredPerson.getName();
					firstAddInsuredPersonDetails = firstAddInsuredPerson.getDetail();
					//firstAddInsuredContactPreferences = firstAddInsuredPerson.getPreferredContact();
					firstAddInsuredCountries = firstAddInsuredPerson.getNationalityCountry();
					firstAddInsuredResidenceCountry = firstAddInsuredPerson.getResidenceCountry();
					firstAddInsuredIdCardType = firstAddInsuredPerson.getNationalIdType();
					firstAddInsuredIdCardNumber = firstAddInsuredPerson.getNationalId();
					firstAddInsuredHeight = firstAddInsuredPerson.getHeight().getValue();
					firstAddInsuredWeight = firstAddInsuredPerson.getWeight().getValue();
				}
				if (null != firstAddInsuredPersonDetails) {
					for (PersonDetail personDetail : firstAddInsuredPersonDetails) {
						if (personDetail instanceof OccupationDetail) {
							firstAddInsuredOccupationDetail = (OccupationDetail) personDetail;
						}
					}
				}

				if (null != secondAddInsuredPerson) {
					secondAddInsuredDob = secondAddInsuredPerson.getBirthDate();
					secondAddInsuredPersonNames = secondAddInsuredPerson.getName();
					secondAddInsuredPersonDetails = secondAddInsuredPerson.getDetail();
					//secondAddInsuredContactPreferences = secondAddInsuredPerson.getPreferredContact();
					secondAddInsuredCountries = secondAddInsuredPerson.getNationalityCountry();
					secondAddInsuredResidenceCountry = secondAddInsuredPerson.getResidenceCountry();
					secondAddInsuredIdCardType = secondAddInsuredPerson.getNationalIdType();
					secondAddInsuredIdCardNumber = secondAddInsuredPerson.getNationalId();
					secondAddInsuredHeight = secondAddInsuredPerson.getHeight().getValue();
					secondAddInsuredWeight = secondAddInsuredPerson.getWeight().getValue();
				}

				if (null != secondAddInsuredPersonDetails) {
					for (PersonDetail personDetail : secondAddInsuredPersonDetails) {
						if (personDetail instanceof OccupationDetail) {
							secondAddInsuredOccupationDetail = (OccupationDetail) personDetail;
						}
					}
				}

				if (null != thirdAddInsuredPerson) {
					thirdAddInsuredDob = thirdAddInsuredPerson.getBirthDate();
					thirdAddInsuredPersonNames = thirdAddInsuredPerson.getName();
					thirdAddInsuredPersonDetails = thirdAddInsuredPerson.getDetail();
					//thirdAddInsuredContactPreferences = thirdAddInsuredPerson.getPreferredContact();
					thirdAddInsuredCountries = thirdAddInsuredPerson.getNationalityCountry();
					thirdAddInsuredResidenceCountry = thirdAddInsuredPerson.getResidenceCountry();
					thirdAddInsuredIdCardType = thirdAddInsuredPerson.getNationalIdType();
					thirdAddInsuredIdCardNumber = thirdAddInsuredPerson.getNationalId();
					thirdAddInsuredHeight = thirdAddInsuredPerson.getHeight().getValue();
					thirdAddInsuredWeight = thirdAddInsuredPerson.getWeight().getValue();
				}

				if (null != thirdAddInsuredPersonDetails) {
					for (PersonDetail personDetail : thirdAddInsuredPersonDetails) {
						if (personDetail instanceof OccupationDetail) {
							thirdAddInsuredOccupationDetail = (OccupationDetail) personDetail;
						}
					}
				}

				if (null != fourthAddInsuredPerson) {
					fourthAddInsuredDob = fourthAddInsuredPerson.getBirthDate();
					fourthAddInsuredPersonNames = fourthAddInsuredPerson.getName();
					fourthAddInsuredPersonDetails = fourthAddInsuredPerson.getDetail();
					//fourthAddInsuredContactPreferences = fourthAddInsuredPerson.getPreferredContact();
					fourthAddInsuredCountries = fourthAddInsuredPerson.getNationalityCountry();
					fourthAddInsuredResidenceCountry = fourthAddInsuredPerson.getResidenceCountry();
					fourthAddInsuredIdCardType = fourthAddInsuredPerson.getNationalIdType();
					fourthAddInsuredIdCardNumber = fourthAddInsuredPerson.getNationalId();
					fourthAddInsuredHeight = fourthAddInsuredPerson.getHeight().getValue();
					fourthAddInsuredWeight = fourthAddInsuredPerson.getWeight().getValue();
				}

				if (null != fourthAddInsuredPersonDetails) {
					for (PersonDetail personDetail : fourthAddInsuredPersonDetails) {
						if (personDetail instanceof OccupationDetail) {
							fourthAddInsuredOccupationDetail = (OccupationDetail) personDetail;
						}
					}
				}
			}catch(Exception ex){
				LOGGER.error("Error in additional insured "+ex.getMessage());
			}

			try{
				if(insured != null){
					insuredUserSelections =  insured.getSelectedOptions();
					if(insuredUserSelections != null && insuredUserSelections.size() > 0){
						for(UserSelection insUserselections:insuredUserSelections){
							if(insUserselections != null &&
									insUserselections.getQuestionnaireType().name().equalsIgnoreCase("Health")){

								if(insUserselections.getSelectedOption()!=null && insUserselections.getQuestionId() == 1){
									if(insUserselections.getSelectedOption()==true){
										insuredLostWeight = "Yes";
										insuredLostWeightReason=insUserselections.getDetailedInfo();
										//for(GLISelectedOption optn : insUserselections.getSelectedOptions()){
										//insuredLostWeightReason = optn.getSelectedOptionValue();
										//}
									}else{
										insuredLostWeight ="No";
									}
								}else if(insUserselections.getQuestionId() == 2 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasHeartcirulatoryBloodDisorder = "Yes";
									}else{
										insuredHasHeartcirulatoryBloodDisorder = "No";
									}
								}else if(insUserselections.getQuestionId() == 3 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasAsthmaPnuemonia = "Yes";
									}else{
										insuredHasAsthmaPnuemonia = "No";
									}
								}else if(insUserselections.getQuestionId() == 4 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasHepatitisGastrointestinal = "Yes";
									}else{
										insuredHasHepatitisGastrointestinal = "No";
									}
								}else if(insUserselections.getQuestionId() == 5 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasGenitoUrinary = "Yes";
									}else{
										insuredHasGenitoUrinary = "No";
									}
								}else if(insUserselections.getQuestionId() == 6 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredMetabolicAndEndorcrineDisorder = "Yes";
									}else{
										insuredMetabolicAndEndorcrineDisorder = "No";
									}
								}else if(insUserselections.getQuestionId() == 7 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasNeurologicalAndPsychiatricDisorder = "Yes";
									}else{
										insuredHasNeurologicalAndPsychiatricDisorder = "No";
									}
								}else if(insUserselections.getQuestionId() == 8 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasHIVOrBoneDisorder ="Yes";
									}else{
										insuredHasHIVOrBoneDisorder ="No";
									}
								}else if(insUserselections.getQuestionId() == 9 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasSpinalDisorder ="Yes";
									}else{
										insuredHasSpinalDisorder ="No";
									}
								}else if(insUserselections.getQuestionId() == 10 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasCancerMelanoma ="Yes";
									}else{
										insuredHasCancerMelanoma ="No";
									}
								}else if(insUserselections.getQuestionId() == 13 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredCurrentlyPregnent ="Yes";
										insuredPregnentDescription = ""+insUserselections.getUnit();
									}else{
										insuredCurrentlyPregnent ="No";
									}
								}else if(insUserselections.getQuestionId() == 11 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredOtherDiseases ="Yes";
									}else{
										insuredOtherDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 12 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredReceivedMedTreatment ="Yes";
									}else{
										insuredReceivedMedTreatment ="No";
									}
								}else if(insUserselections.getQuestionId() == 14 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										gemilangHealthQuest1 ="Yes";
									}else{
										gemilangHealthQuest1 ="No";
									}
								}else if(insUserselections.getQuestionId() == 15 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										gemilangHealthQuest2 ="Yes";
									}else{
										gemilangHealthQuest2 ="No";
									}
								}else if(insUserselections.getQuestionId() == 16 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										gemilangHealthQuest3 ="Yes";
									}else{
										gemilangHealthQuest3 ="No";
									}
								}
							}else if(insUserselections != null &&
									insUserselections.getQuestionnaireType() != null && insUserselections.getQuestionnaireType().name().equalsIgnoreCase("Lifestyle")){
								if(insUserselections.getQuestionId() == 1 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasUsedTobacco = "Yes";
										insuredTobaccoSticks = insUserselections.getUnit();
										/*for(GLISelectedOption optn : insUserselections.getSelectedOptions()){
											insuredTobaccoSticks = optn.getSelectedOptionValue();
										}*/
									}else{
										insuredHasUsedTobacco = "No";
									}
								}else if(insUserselections.getQuestionId() == 2 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredDrugsOrNarcotics = "Yes";
									}else{
										insuredDrugsOrNarcotics = "No";
									}
								}else if (insUserselections.getQuestionId() == 3 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHazardousActivities = "Yes";
									}
									else{
										insuredHazardousActivities = "No";
									}
								}
							}
							else if(insUserselections != null &&
									insUserselections.getQuestionnaireType() != null && insUserselections.getQuestionnaireType().name().equalsIgnoreCase("Other")){
								if (insUserselections.getQuestionId() == 1 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredFamilyMembersDiagWithDiseases = "Yes";
									}else{
										insuredFamilyMembersDiagWithDiseases = "No";
									}
								}
							}
						}
					}
				}
			}catch(Exception e){
				LOGGER.error("Error in getting insured userselection"+e.getMessage());
				e.printStackTrace();
			}

			/** Previous Policy */

			try{
				if(insuredPreviousPolicyList != null && insuredPreviousPolicyList.size() >0){
					LOGGER.error("insuredPreviousPolicyList  "+insuredPreviousPolicyList.size());
					int count = 0;
					for(Insured ins:insuredPreviousPolicyList){
						if(count == 0){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									firstCompanyName = agmnt.getInsurerName();
									firstPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										firstPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										firstFinalDecision = agmnt.getAgreementExtension().getRemark();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											firstTypeOfInsurance = coverage.getMarketingName();
											firstPrevSumAssured = coverage.getSumAssured().getAmount();
										}

									}
								}
							}
						}else if(count == 1){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									secondCompanyName = agmnt.getInsurerName();
									secondPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										secondPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										secondFinalDecision = agmnt.getAgreementExtension().getRemark();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											secondTypeOfInsurance = coverage.getMarketingName();
											secondPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}else if(count == 2){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									thirdCompanyName = agmnt.getInsurerName();
									thirdPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										thirdPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										thirdFinalDecision = agmnt.getAgreementExtension().getRemark();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											thirdTypeOfInsurance = coverage.getMarketingName();
											thirdPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}else if(count == 3){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									fourthCompanyName = agmnt.getInsurerName();
									fourthPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										fourthPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										fourthFinalDecision = agmnt.getAgreementExtension().getRemark();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											fourthTypeOfInsurance = coverage.getMarketingName();
											fourthPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}else if(count == 4){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									fifthCompanyName = agmnt.getInsurerName();
									fifthPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										fifthPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										fifthFinalDecision = agmnt.getAgreementExtension().getRemark();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											fifthTypeOfInsurance = coverage.getMarketingName();
											fifthPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}
						count++;
					}
				}
			}catch(Exception e){
				LOGGER.warn("Error in getting previous policy details"+e);
			}
		} catch (Exception e) {
			LOGGER.warn("Error in beforeDetailEval"+e);
		}
	}
	//****************************************************************************//

	public String populateAppointeeESignature() {
		String sign = null;
		String temptoken = null;
		Document documentDetail = null;
		LOGGER.info("appionteeDocuments ---- " + appionteeDocuments);
		// appointee signature detail
		if (appionteeDocuments != null) {
			try{
				for (Document agreementDocument : appionteeDocuments) {
					if (agreementDocument != null) {
						documentDetail = agreementDocument;
						if (("AgentSignature").equals(agreementDocument
								.getDocumentTypeCode())) {
							LOGGER.info("Type Code is Signature ---"
									+ documentDetail.getDocumentTypeCode());
							sign = documentDetail.getBase64string();
							StringTokenizer token = new StringTokenizer(sign, ",");
							while (token.hasMoreTokens()) {
								temptoken = token.nextToken();
							}
							if (temptoken != null) {
								sign = temptoken;
							}
						}
					}

				}
			}catch(Exception e){
				LOGGER.error("Error in getting  agent sign for spaj " + spajNumber);
			}
		}
		//System.out.println("Signature for PDF is " + sign);
		return sign;
	}

	public String populatClientSignature() {
		String sign = null;
		String temptoken = null;
		Document documentDetail = null;
		LOGGER.info("appionteeDocuments ---- " + appionteeDocuments);
		// appointee signature detail
		if (appionteeDocuments != null) {
			try{
				for (Document agreementDocument : appionteeDocuments) {
					if (agreementDocument != null) {
						documentDetail = agreementDocument;
						if (("SPAJSignature").equals(agreementDocument
								.getDocumentTypeCode())) {
							LOGGER.info("Type Code is Signature ---"
									+ documentDetail.getDocumentTypeCode());
							sign = documentDetail.getBase64string();
							StringTokenizer token = new StringTokenizer(sign, ",");
							while (token.hasMoreTokens()) {
								temptoken = token.nextToken();
							}
							if (temptoken != null) {
								sign = temptoken;
							}
						}
					}

				}
			}
			catch(Exception e){
				LOGGER.error("Error in getting  client sign for spaj " + spajNumber);
			}
		}
		//System.out.println("Signature for PDF is " + sign);
		return sign;
	}
	public void populateCP() {
		for (ContactPreference contactPreference : contactPreferences) {
			ContactPoint contactPoint = contactPreference
					.getPreferredContactPoint();
			if (contactPoint instanceof PostalAddressContact) {
				if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Current)) {
					postalAddressContact = (PostalAddressContact) contactPoint;
				} else if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Permanent)) {
					permanentAddress = (PostalAddressContact) contactPoint;
				}

			}else if (contactPoint instanceof TelephoneCallContact) {
				if (null != contactPreference.getPriorityLevel()) {
					if ((contactPreference.getPriorityLevel() == 1)) {
						telephoneCallContact = (TelephoneCallContact) contactPoint;
						if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
							insuredMobileNumber1 = telephoneCallContact.getFullNumber();
						} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
							insuredHomeNumber1 = telephoneCallContact.getFullNumber();
						}
					} else if (contactPreference.getPriorityLevel() == 2) {
						telephoneCallContact = (TelephoneCallContact) contactPoint;
						if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
							insuredMobileNumber2 = telephoneCallContact.getFullNumber();
						} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
							insuredHomeNumber2 = telephoneCallContact.getFullNumber();
						}
					}
				}
			} else if (contactPoint instanceof ElectronicContact) {
				electronicContact = (ElectronicContact) contactPoint;
			}
		}
	}

	public void populateAppointeeCP() {
		// appointee contact details
		for (ContactPreference contactPreference1 : appointeeContactPreferences) {
			ContactPoint contactPoint = contactPreference1
					.getPreferredContactPoint();
			if (contactPoint instanceof PostalAddressContact) {
				if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Current)) {
					appointeePostalAddressContact = (PostalAddressContact) contactPoint;
				} else if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Permanent)) {
					appointeePermanentAddress = (PostalAddressContact) contactPoint;
				}

			} else if (contactPoint instanceof TelephoneCallContact) {
				if ((contactPreference1.getPriorityLevel() == 1)) {
					appointeeHome1Contact = (TelephoneCallContact) contactPoint;
				} else if (contactPreference1.getPriorityLevel() == 2) {
					appointeeHome2Contact = (TelephoneCallContact) contactPoint;
				}
			} else if (contactPoint instanceof ElectronicContact) {
				appointeeElectronicContact = (ElectronicContact) contactPoint;
			}
		}

	}
	/** proposer contact details */
	public void populateProposerCP() {
		try {
			for (ContactPreference contactPreference : proposerContactPreferences) {
				ContactPoint contactPoint = contactPreference.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Current)) {
						proposerPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Permanent)) {
						proposerPermanentAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
					if (null != contactPreference.getPriorityLevel()) {
						if ((contactPreference.getPriorityLevel() == 1)) {
							telephoneCallContact = (TelephoneCallContact) contactPoint;
							if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
								proposerMobileNumber1 = telephoneCallContact.getFullNumber();
							} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
								proposerHomeNumber1 = telephoneCallContact.getFullNumber();
							}
						} else if (contactPreference.getPriorityLevel() == 2) {
							telephoneCallContact = (TelephoneCallContact) contactPoint;
							if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
								proposerMobileNumber2 = telephoneCallContact.getFullNumber();
							} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
								proposerHomeNumber2 = telephoneCallContact.getFullNumber();
							}
						}
					}
				} else if (contactPoint instanceof ElectronicContact) {
					proposerElectronicContact = (ElectronicContact) contactPoint;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :proposerContactPreferences"+ e.getStackTrace());
		}
	}

	/**
	 * Gets the middle name.
	 *
	 * @return the middle name
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getMiddleName() throws JRScriptletException {
		String name = null;
		for (PersonName personName : personNames) {
			name = personName.getMiddleName();
		}
		return name;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		String title = "";
		for (PersonName personName : personNames) {
			title = null!=personName.getTitle()? personName.getTitle():"";
		}
		return title;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName personName : personNames) {
			name = personName.getGivenName();
		}
		return name;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getLastName() throws JRScriptletException {
		String name = null;
		for (PersonName personName : personNames) {
			name = personName.getSurname();
		}
		return name;
	}

	public String getInsuredFirstAndLastName(){
		String insuredFullName = "";
		if(isPropDiffFromInsured){
			try{
				insuredFullName = getFirstName()+" "+getLastName();
			}catch(Exception e){
				LOGGER.error("Error getInsuredFirstAndLastName");
			}
		}
		return insuredFullName;
	}
	/**
	 * Gets the annual income.
	 *
	 * @return the annual income
	 */
	public String getAnnualIncome() {
		if (incomeDetail != null) {
			CurrencyAmount currencyAmount = incomeDetail.getGrossAmount();
			return currencyAmount.getAmount().toString();
		} else {
			return "0";
		}
	}

	/**
	 * Gets the nationality.
	 *
	 * @return the nationality
	 */
	public String getNationality() {
		String nationality = "";
		if(isPropDiffFromInsured){
			for (Country country : countries) {
				nationality = country.getName();
			}
		}
		return nationality;
	}

	/**
	 * Gets the address line1.
	 *
	 * @return the address line1
	 */
	public String getAddressLine1() {
		String aLine1 = "";
		if(isPropDiffFromInsured){
			aLine1 = postalAddressContact.getAddressLine1();
		}
		return aLine1;
	}

	/**
	 * Gets the address line2.
	 *
	 * @return the address line2
	 */
	public String getAddressLine2() {
		String aLine2 = "";
		if(isPropDiffFromInsured){
			aLine2 = postalAddressContact.getAddressLine2();
		}
		return aLine2;
	}

	/**
	 * Gets the address line3.
	 *
	 * @return the address line3
	 */
	public String getAddressLine3() {
		String aLine3 = null;
		aLine3 = postalAddressContact.getAddressLine3();
		return aLine3;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		String city = "";
		if(isPropDiffFromInsured){
			Set<CountryElement> countryElements = postalAddressContact
					.getIncludedCountryElement();
			for (CountryElement countryElement : countryElements) {
				if (countryElement instanceof City) {
					return ((City) countryElement).getName();
				}
			}
		}
		return city;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		String state = "";
		if(isPropDiffFromInsured){
			CountrySubdivision subDiv = postalAddressContact
					.getPostalCountrySubdivision();
			return subDiv.getName();
		}
		return state;
	}

	/**
	 * Gets the zip code.
	 *
	 * @return the zip code
	 */
	public String getZipCode() {
		if(isPropDiffFromInsured){
			if (null != postalAddressContact
					&& null != postalAddressContact.getPostalPostCode()
					&& null != postalAddressContact.getPostalPostCode()
					.getAssignedCode()) {
				return (postalAddressContact.getPostalPostCode().getAssignedCode()
						.getCode());
			}
		}
		return "";
	}

	/**
	 * Gets the education qual.
	 *
	 * @return the education qual
	 */
	public String getEducationQual() {
		EducationLevelCodeList codeList = educationDetail
				.getEducationLevelCode();
		return codeList.name();
	}

	/**
	 * Gets the perm addr1.
	 *
	 * @return the perm addr1
	 */
	public String getPermAddr1() {
		return permanentAddress.getAddressLine1();
	}

	/**
	 * Gets the perm addr2.
	 *
	 * @return the perm addr2
	 */
	public String getPermAddr2() {
		return permanentAddress.getAddressLine2();
	}

	/**
	 * Gets the perm addr3.
	 *
	 * @return the perm addr3
	 */
	public String getPermAddr3() {
		return permanentAddress.getAddressLine3();
	}

	/**
	 * Gets the perm city.
	 *
	 * @return the perm city
	 */
	public String getPermCity() {
		Set<CountryElement> countryElements = permanentAddress
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}

	/**
	 * Gets the perm zip code.
	 *
	 * @return the perm zip code
	 */
	public String getPermZipCode() {
		if (null != permanentAddress
				&& null != permanentAddress.getPostalPostCode()
				&& null != permanentAddress.getPostalPostCode()
				.getAssignedCode()) {
			return (permanentAddress.getPostalPostCode().getAssignedCode()
					.getCode());
		}
		return null;
	}

	/**
	 * Gets the perm state.
	 *
	 * @return the perm state
	 */
	public String getPermState() {
		CountrySubdivision subDiv = permanentAddress
				.getPostalCountrySubdivision();
		return subDiv.getName();
	}

	/**
	 * Gets the designation.
	 *
	 * @return the designation
	 */
	public String getDesignation() {
		return occupationDetail.getOccupationClass();
	}

	/**
	 * Gets the nature of work.
	 *
	 * @return the nature of work
	 */
	public String getNatureOfWork() {
		return occupationDetail.getNatureofWork();
	}

	/**
	 * Gets the home num1.
	 *
	 * @return the home num1
	 */
	public String getHomeNum1() {
		return home1Contact.getFullNumber();
	}

	/**
	 * Gets the home num2.
	 *
	 * @return the home num2
	 */
	public String getHomeNum2() {
		return home2Contact.getFullNumber();
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		if(isPropDiffFromInsured){
			return electronicContact.getEmailAddress();
		}
		return "";
	}

	public String getIdCardType() {
		String insuredCardType = "";
		if(getIsPropDiffFromInsured()){
			return insuredIdCardType;
		}
		return insuredCardType;
	}

	public String getIdCardNumber() {
		String  idCardNumber = "";
		if(isPropDiffFromInsured){
			return insuredIdCardNumber;
		}
		return idCardNumber;
	}
	public String getInsuredResidenceCountry(){
		String resCountry = "";
		if(isPropDiffFromInsured){
			if(insuredResidenceCountry != null){
				resCountry = insuredResidenceCountry.getName();
			}
		}
		return resCountry;
	}

	public Date getInsuredDob() {
		return insuredDob;
	}

	public String getInsuredCompanyName() {
		String insuredCompany = "";
		if(isPropDiffFromInsured){
			if (occupationDetail != null) {
				insuredCompany=occupationDetail.getNameofInstitution();
			}
		}
		return insuredCompany;
	}
	public String getRelationshipWithProposer() {
		return relationshipWithProposer;
	}

	public String getOccupation() {
		String occupation = "";
		if(isPropDiffFromInsured){
			occupation = occupationDetail.getDescriptionOthers();
		}
		return occupation;
	}
	public String getOccupationIndustry() {
		String occupation = "";
		if(isPropDiffFromInsured){
			occupation = occupationDetail.getNatureofWorkValue();
			try{
				if(occupation != null && !"".equals(occupation)){
					if(occupation.equalsIgnoreCase("Others")){
						for(EmploymentRelationship relationship :occupationDetail.getProvidingEmployment()){
							occupation = relationship.getBusinessUnit();
						}
					}
					if(occupation != null && !"".equals(occupation) && occupation.contains("&amp;")){
						occupation = occupation.replace("&amp;","&");
					}
				}
			}catch(Exception e){
				LOGGER.error("getProposerOccupation .. "+e.getMessage());
			}
		}
		return occupation;
	}

	public String getJobDescription() {
		String jobDescription = "";
		if(isPropDiffFromInsured){
			for(EmploymentRelationship empRelationship:occupationDetail.getProvidingEmployment()){
				jobDescription = empRelationship.getJobDescription();
			}
		}
		return jobDescription;
	}

	public String getInsuredMobileNumber() {
		if(isPropDiffFromInsured){
			populateCP();
			return insuredMobileNumber1;
		}
		return "";
	}
	public String getInsuredHomeNumber() {
		if(isPropDiffFromInsured){
			populateCP();
			return insuredHomeNumber1;
		}
		return "";
	}

	public String getInsuredHasHeartcirulatoryBloodDisorder() {
		return insuredHasHeartcirulatoryBloodDisorder;
	}

	public String getInsuredHasAsthmaPnuemonia() {
		return insuredHasAsthmaPnuemonia;
	}

	public String getInsuredHasHepatitisGastrointestinal() {
		return insuredHasHepatitisGastrointestinal;
	}

	public String getInsuredHasGenitoUrinary() {
		return insuredHasGenitoUrinary;
	}

	public String getInsuredMetabolicAndEndorcrineDisorder() {
		return insuredMetabolicAndEndorcrineDisorder;
	}

	public String getInsuredHasNeurologicalAndPsychiatricDisorder() {
		return insuredHasNeurologicalAndPsychiatricDisorder;
	}

	public String getInsuredHasHIVOrBoneDisorder() {
		return insuredHasHIVOrBoneDisorder;
	}

	public String getInsuredHasSpinalDisorder() {
		return insuredHasSpinalDisorder;
	}

	public String getInsuredHasCancerMelanoma() {
		return insuredHasCancerMelanoma;
	}

	public String getInsuredCurrentlyPregnent() {
		return insuredCurrentlyPregnent;
	}

	public String getInsuredOtherDiseases() {
		return insuredOtherDiseases;
	}

	public String getInsuredReceivedMedTreatment() {
		return insuredReceivedMedTreatment;
	}

	public String getInsuredHasUsedTobacco() {
		return insuredHasUsedTobacco;
	}

	public String getInsuredDrugsOrNarcotics() {
		return insuredDrugsOrNarcotics;
	}

	public String getInsuredHazardousActivities() {
		return insuredHazardousActivities;
	}

	public String getInsuredFamilyMembersDiagWithDiseases() {
		return insuredFamilyMembersDiagWithDiseases;
	}


	/** appointee details*/

	/**
	 * Gets the appointee middle name.
	 *
	 * @return the appointee middle name
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getAppointeeMiddleName() throws JRScriptletException {
		String name = null;
		for (PersonName appointeePersonName : appointeePersonNames) {
			name = appointeePersonName.getMiddleName();
		}
		return name;
	}

	/**
	 * Gets the appointee first name.
	 *
	 * @return the appointee first name
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getAppointeeFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName appointeePersonName : appointeePersonNames) {
			name = appointeePersonName.getGivenName();
		}
		return name;
	}

	/**
	 * Gets the appointee last name.
	 *
	 * @return the appointee last name
	 * @throws JRScriptletException
	 *             the jR scriptlet exception
	 */
	public String getAppointeeLastName() throws JRScriptletException {
		String name = null;
		for (PersonName appointeePersonName : appointeePersonNames) {
			name = appointeePersonName.getSurname();
		}
		return name;
	}

	/**
	 * Gets the appointee title.
	 *
	 * @return the appointee title
	 */
	public String getAppointeeTitle() {
		String title = null;
		for (PersonName appointeePersonName : appointeePersonNames) {
			title = appointeePersonName.getTitle();
		}
		return title;
	}

	/**
	 * Gets the appointee nationality.
	 *
	 * @return the appointee nationality
	 */
	public String getAppointeeNationality() {
		String nationality = null;
		for (Country country : appionteeCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	/**
	 * Gets the appointee address line1.
	 *
	 * @return the appointee address line1
	 */
	public String getAppointeeAddressLine1() {
		populateAppointeeCP();
		String aLine1 = null;
		aLine1 = appointeePostalAddressContact.getAddressLine1();
		return aLine1;
	}

	/**
	 * Gets the address line2.
	 *
	 * @return the address line2
	 */
	public String getAppointeeAddressLine2() {
		populateAppointeeCP();
		String aLine2 = null;
		aLine2 = appointeePostalAddressContact.getAddressLine2();
		return aLine2;
	}

	/**
	 * Gets the address line3.
	 *
	 * @return the address line3
	 */
	public String getAppointeeAddressLine3() {
		populateAppointeeCP();
		String aLine3 = null;
		aLine3 = appointeePostalAddressContact.getAddressLine3();
		return aLine3;
	}

	/**
	 * Gets the appointee perm addr1.
	 *
	 * @return the appointee perm addr1
	 */
	public String getAppointeePermAddr1() {
		return appointeePermanentAddress.getAddressLine1();
	}

	/**
	 * Gets the perm addr2.
	 *
	 * @return the perm addr2
	 */
	public String getAppointeePermAddr2() {
		return appointeePermanentAddress.getAddressLine2();
	}

	/**
	 * Gets the perm addr3.
	 *
	 * @return the perm addr3
	 */
	public String getAppointeePermAddr3() {
		return appointeePermanentAddress.getAddressLine3();
	}

	/**
	 * Gets the appointee home num1.
	 *
	 * @return the appointee home num1
	 */
	public String getAppointeeHomeNum1() {
		return appointeeHome1Contact.getFullNumber();
	}

	/**
	 * Gets the home num2.
	 *
	 * @return the home num2
	 */
	public String getAppointeeHomeNum2() {
		return appointeeHome2Contact.getFullNumber();
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getAppointeeEmail() {
		return appointeeElectronicContact.getEmailAddress();
	}

	/**
	 * Gets the appointee city.
	 *
	 * @return the appointee city
	 */
	public String getAppointeeCity() {
		populateAppointeeCP();
		Set<CountryElement> countryElements = appointeePostalAddressContact
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getAppointeeState() {
		populateAppointeeCP();
		CountrySubdivision subDiv = appointeePostalAddressContact
				.getPostalCountrySubdivision();
		return subDiv.getName();
	}

	/**
	 * Gets the zip code.
	 *
	 * @return the zip code
	 */
	public String getAppointeeZipCode() {
		populateAppointeeCP();
		if (null != appointeePostalAddressContact
				&& null != appointeePostalAddressContact.getPostalPostCode()
				&& null != appointeePostalAddressContact.getPostalPostCode()
				.getAssignedCode()) {
			return (appointeePostalAddressContact.getPostalPostCode()
					.getAssignedCode().getCode());
		}
		return null;
	}

	/**
	 * Gets the appointee perm city.
	 *
	 * @return the appointee perm city
	 */
	public String getAppointeePermCity() {
		populateAppointeeCP();
		Set<CountryElement> countryElements = appointeePermanentAddress
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}

	/**
	 * Gets the perm zip code.
	 *
	 * @return the perm zip code
	 */
	public String getAppointeePermZipCode() {
		populateAppointeeCP();
		if (null != appointeePermanentAddress
				&& null != appointeePermanentAddress.getPostalPostCode()
				&& null != appointeePermanentAddress.getPostalPostCode()
				.getAssignedCode()) {
			return (appointeePermanentAddress.getPostalPostCode()
					.getAssignedCode().getCode());
		}
		return null;
	}

	/**
	 * Gets the perm state.
	 *
	 * @return the perm state
	 */
	public String getAppointeePermState() {
		populateAppointeeCP();
		CountrySubdivision subDiv = appointeePermanentAddress
				.getPostalCountrySubdivision();
		return subDiv.getName();
	}

	/**
	 * Gets the spouse name.
	 *
	 * @return the spouse name
	 */
	public String getSpouseName() {
		return person.getSpouseOrParentName().getFullName();
	}

	/**
	 * Gets the marital status.
	 *
	 * @return the marital status
	 */
	public String getMaritalStatus() {
		String status = "";
		if(isPropDiffFromInsured){
			return person.getMaritalStatusCode().name();
		}
		return status;
	}

	/**
	 * Gets the gender.
	 *
	 * @return the gender
	 */
	public String getGender() {
		String gender ="";
		if(isPropDiffFromInsured){
			return person.getGenderCode().name();
		}
		return gender;
	}

	public String getAppointeeGender() {
		return appointeePerson.getGenderCode().name();
	}

	/** Proposer detail */

	public String getProposerFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName proposerPersonName : proposerPersonNames) {
			name = proposerPersonName.getGivenName();
		}
		return name;
	}

	public String getProposerLastName() throws JRScriptletException {
		String name = null;
		for (PersonName proposerPersonName : proposerPersonNames) {
			name = proposerPersonName.getSurname();
		}
		return name;
	}


	public String getProposerFirstAndLastName(){
		try {
			proposerFullName = getProposerFirstName()+" "+getProposerLastName();
		} catch (Exception e) {
			LOGGER.error("Error while retrieving ProposerFirstAndLastName "+e.getMessage());
		}
		return proposerFullName;

	}

	public String getProposerGender() {
		String gender = null;
		try {
			if (proposerPerson.getGenderCode() != null) {
				gender = proposerPerson.getGenderCode().name();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving getProposerGender "+e.getMessage());
		}

		return gender;
	}

	public Date getProposerdob() {
		if(proposerdob!= null && !"".equals(proposerdob)){
			return proposerdob;
		}
		return proposerdob;
	}

	public String getProposerNationality() {
		String nationality = null;
		for (Country country : proposerCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	public String getProposerEmail() {
		return proposerElectronicContact.getEmailAddress();
	}
	public String getProposerMobileNumber() {
		return proposerMobileNumber1;
	}
	public String getProposerHomeNumber() {
		return proposerHomeNumber1;
	}

	public String getProposerAddressLine1() {
		populateProposerCP();
		String addrLine1 = null;
		/*if(proposerPostalAddressContact!=null&&proposerPostalAddressContact.getPostalMunicipality()!=null){
			addrLine1 = proposerPostalAddressContact.getPostalMunicipality().getName();
		}*/
		addrLine1 = proposerPostalAddressContact.getAddressLine1();
		return addrLine1;
	}


	public String getProposerAddressLine2() {
		populateProposerCP();
		String addrLine2 = null;
		addrLine2 = proposerPostalAddressContact.getAddressLine2();
		return addrLine2;
	}

	public String getProposerCity() {
		populateProposerCP();
		Set<CountryElement> countryElements = proposerPostalAddressContact
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}

	public String getProposerState() {
		populateProposerCP();
		CountrySubdivision subDiv = proposerPostalAddressContact
				.getPostalCountrySubdivision();
		return subDiv.getName();
	}

	public String getProposerCountry() {
		populateProposerCP();
		Country country = proposerPostalAddressContact.getPostalCountry();
		if (country != null) {
			return country.getName();
		}
		return null;
	}


	public String getProposerZipCode() {
		populateProposerCP();
		if (null != proposerPostalAddressContact
				&& null != proposerPostalAddressContact.getPostalPostCode()
				&& null != proposerPostalAddressContact.getPostalPostCode()
				.getAssignedCode()) {
			return (proposerPostalAddressContact.getPostalPostCode()
					.getAssignedCode().getCode());
		}
		return null;
	}

	/** proposer permanent address */
	public String getProposerPermAddr1() {
		/*if(proposerPermanentAddress!=null&&proposerPermanentAddress.getPostalMunicipality()!=null){
			return proposerPermanentAddress.getPostalMunicipality().getName();
		}
		else{
			return null;
		}*/
		return proposerPermanentAddress.getAddressLine1();
	}

	public String getProposerPermAddr2() {
		return proposerPermanentAddress.getAddressLine2();
	}

	public String getProposerPermCity() {
		populateProposerCP();
		Set<CountryElement> countryElements = proposerPermanentAddress
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}

	public String getProposerPermState() {
		populateProposerCP();
		CountrySubdivision subDiv = proposerPermanentAddress
				.getPostalCountrySubdivision();
		return subDiv.getName();
	}

	public String getProposerPermCountry() {
		populateProposerCP();
		Country country = proposerPermanentAddress.getPostalCountry();
		if (country != null) {
			return country.getName();
		}
		return null;
	}
	public String getProposerPermZipCode() {
		populateProposerCP();
		if (null != proposerPermanentAddress
				&& null != proposerPermanentAddress.getPostalPostCode()
				&& null != proposerPermanentAddress.getPostalPostCode()
				.getAssignedCode()) {
			return (proposerPermanentAddress.getPostalPostCode()
					.getAssignedCode().getCode());
		}
		return null;
	}

	/** Residence country taken as birth country */

	public String getProposerResidenceCountry(){
		String resCountry = null;
		if(proposerResidenceCountry != null){
			resCountry = proposerResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getProposerMaritalStatus() {
		if (proposerPerson != null) {
			if (proposerPerson.getMaritalStatusCode() != null) {
				return proposerPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getProposerCompanyName() {
		String proposerCompanyName = null;
		proposerCompanyName = proposerOccupationDetail.getNameofInstitution();
		return proposerCompanyName;
	}

	public String getProposerIndustry() {
		String proposerOccupation = null;
		proposerOccupation = proposerOccupationDetail.getNatureofWorkValue();
		try{
			if(proposerOccupation != null && !"".equals(proposerOccupation)){
				if(proposerOccupation.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :proposerOccupationDetail.getProvidingEmployment()){
						proposerOccupation = relationship.getBusinessUnit();
					}
				}
				if(proposerOccupation.contains("&amp;")){
					proposerOccupation = proposerOccupation.replace("&amp;","&");
				}
			}
		}catch(Exception e){
			LOGGER.error("getProposerOccupation .. "+e.getMessage());
		}
		return proposerOccupation;
	}
	public String getProposerOccupation() {
		String proposerOccupation = null;
		try{
			proposerOccupation = proposerOccupationDetail.getDescriptionOthers();
		}catch(Exception e){
			LOGGER.error("getProposerOccupation .. "+e.getMessage());
		}
		return proposerOccupation;
	}


	public String getProposerPosition() {
		String proposerPosition = null;
		for(EmploymentRelationship empRelationship: proposerOccupationDetail.getProvidingEmployment()){
			proposerPosition = empRelationship.getOccupationTypeCode();
		}
		return proposerPosition;
	}
	public String getInsuredPosition() {
		String insuredPosition = "";
		if(isPropDiffFromInsured){
			for(EmploymentRelationship empRelationship: occupationDetail.getProvidingEmployment()){
				insuredPosition = empRelationship.getOccupationTypeCode();
			}
		}
		return insuredPosition;
	}

	public String getProposerJobDescription() {
		String proposerJobDescription = null;
		for(EmploymentRelationship empRelationship:proposerOccupationDetail.getProvidingEmployment()){
			proposerJobDescription = empRelationship.getJobDescription();
		}
		return proposerJobDescription;
	}

	public String getProposerIdCardType() {
		return proposerIdCardType;
	}

	public String getProposerIdCardNumber() {
		return proposerIdCardNumber;
	}

	/** Additional insured */

	/** First additional insured */

	public Date getFirstAddInsuredDob() {
		return firstAddInsuredDob;
	}

	public String getFirstAddInsuredFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : firstAddInsuredPersonNames) {
			name = addInsuredPersonName.getGivenName();
		}
		return name;
	}

	public String getFirstAddInsuredLastName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : firstAddInsuredPersonNames) {
			name = addInsuredPersonName.getSurname();
		}
		return name;
	}


	public String getFirstAddInsuredFirstAndLastName(){
		String firstAddInsuredFullName = null;
		try {
			firstAddInsuredFullName = getFirstAddInsuredFirstName()+" "+getFirstAddInsuredLastName();
		} catch (Exception e) {
			LOGGER.error("Error getFirstAddInsuredFirstAndLastName "+e.getMessage());
		}
		return firstAddInsuredFullName;

	}
	public String getFirstAddInsuredGender() {
		return firstAddInsuredPerson.getGenderCode().name();
	}

	public String getFirstAddInsuredNationality() {
		String nationality = null;
		for (Country country : firstAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	/** Residence country taken as birth country */
	public String getFirstAddInsuredResidenceCountry(){
		String resCountry = null;
		if(firstAddInsuredResidenceCountry != null){
			resCountry = firstAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getFirstAddInsuredMaritalStatus() {
		if (firstAddInsuredPerson != null) {
			if (firstAddInsuredPerson.getMaritalStatusCode() != null) {
				return firstAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getFirstAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = firstAddInsuredOccupationDetail
				.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getFirstAddInsuredOccupation() {
		String firstAddInsuredOccupation = null;
		firstAddInsuredOccupation = firstAddInsuredOccupationDetail
				.getDescriptionOthers();
		return firstAddInsuredOccupation;
	}

	public String getFirstAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for (EmploymentRelationship employmentRelationship : firstAddInsuredOccupationDetail
				.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship
			.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getFirstAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for (EmploymentRelationship employmentRelationship : firstAddInsuredOccupationDetail
				.getProvidingEmployment()) {
			firstAddInsuredJobDescription = employmentRelationship
					.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}

	public String getFirstAddInsuredLineOfBusiness() {
		String firstAddInsuredLineOfBusiness = null;
		firstAddInsuredLineOfBusiness = firstAddInsuredOccupationDetail
				.getNatureofWorkValue();
		try{
			if(firstAddInsuredLineOfBusiness != null && !"".equals(firstAddInsuredLineOfBusiness)){
				if(firstAddInsuredLineOfBusiness.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :firstAddInsuredOccupationDetail.getProvidingEmployment()){
						firstAddInsuredLineOfBusiness = relationship.getBusinessUnit();
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("firstAddInsuredLineOfBusiness "+e.getMessage());
		}
		return firstAddInsuredLineOfBusiness;
	}
	public String getFirstAddInsuredRelationWithInsured() {
		return firstAddInsuredRelationWithInsured;
	}

	public String getFirstAddInsuredIdCardType() {
		return firstAddInsuredIdCardType;
	}

	public String getFirstAddInsuredIdCardNumber() {
		return firstAddInsuredIdCardNumber;
	}

	public BigDecimal getFirstAddInsuredHeight() {
		return firstAddInsuredHeight;
	}

	public BigDecimal getFirstAddInsuredWeight() {
		return firstAddInsuredWeight;
	}

	public String getFirstAddInsuredHasHeartcirulatoryBloodDisorder() {
		return firstAddInsuredHasHeartcirulatoryBloodDisorder;
	}

	public String getFirstAddInsuredHasAsthmaPnuemonia() {
		return firstAddInsuredHasAsthmaPnuemonia;
	}

	public String getFirstAddInsuredHasHepatitisGastrointestinal() {
		return firstAddInsuredHasHepatitisGastrointestinal;
	}

	public String getFirstAddInsuredHasGenitoUrinary() {
		return firstAddInsuredHasGenitoUrinary;
	}

	public String getFirstAddInsuredMetabolicAndEndorcrineDisorder() {
		return firstAddInsuredMetabolicAndEndorcrineDisorder;
	}

	public String getFirstAddInsuredHasNeurologicalAndPsychiatricDisorder() {
		return firstAddInsuredHasNeurologicalAndPsychiatricDisorder;
	}

	public String getFirstAddInsuredHasHIVOrBoneDisorder() {
		return firstAddInsuredHasHIVOrBoneDisorder;
	}

	public String getFirstAddInsuredHasSpinalDisorder() {
		return firstAddInsuredHasSpinalDisorder;
	}

	public String getFirstAddInsuredHasCancerMelanoma() {
		return firstAddInsuredHasCancerMelanoma;
	}

	public String getFirstAddInsuredCurrentlyPregnent() {
		return firstAddInsuredCurrentlyPregnent;
	}

	public String getFirstAddInsuredOtherDiseases() {
		return firstAddInsuredOtherDiseases;
	}

	public String getFirstAddInsuredReceivedMedTreatment() {
		return firstAddInsuredReceivedMedTreatment;
	}

	public String getFirstAddInsuredHasUsedTobacco() {
		return firstAddInsuredHasUsedTobacco;
	}

	public String getFirstAddInsuredDrugsOrNarcotics() {
		return firstAddInsuredDrugsOrNarcotics;
	}

	public String getFirstAddInsuredHazardousActivities() {
		return firstAddInsuredHazardousActivities;
	}

	public String getFirstAddInsuredFamilyMembersDiagWithDiseases() {
		return firstAddInsuredFamilyMembersDiagWithDiseases;
	}

	/** Second additional insured */

	public Date getSecondAddInsuredDob() {
		return secondAddInsuredDob;
	}
	public String getSecondAddInsuredRelationWithInsured() {
		return secondAddInsuredRelationWithInsured;
	}

	public String getSecondAddInsuredFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : secondAddInsuredPersonNames) {
			name = addInsuredPersonName.getGivenName();
		}
		return name;
	}

	public String getSecondAddInsuredLastName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : secondAddInsuredPersonNames) {
			name = addInsuredPersonName.getSurname();
		}
		return name;
	}
	public String getSecondAddInsuredFirstAndLastName(){
		String secondAddInsuredFullName = null;
		try {
			secondAddInsuredFullName = getSecondAddInsuredFirstName()+" "+getSecondAddInsuredLastName();
		} catch (Exception e) {
			LOGGER.error("Error getSecondAddInsuredFirstAndLastName "+e.getMessage());
		}
		return secondAddInsuredFullName;

	}

	public String getSecondAddInsuredGender() {
		return secondAddInsuredPerson.getGenderCode().name();
	}

	public String getSecondAddInsuredMaritalStatus() {
		if (secondAddInsuredPerson != null) {
			if (secondAddInsuredPerson.getMaritalStatusCode() != null) {
				return secondAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getSecondAddInsuredNationality() {
		String nationality = null;
		for (Country country : secondAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	public String getSecondAddInsuredResidenceCountry(){
		String resCountry = null;
		if(secondAddInsuredResidenceCountry != null){
			resCountry = secondAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getSecondAddInsuredIdCardType() {
		return secondAddInsuredIdCardType;
	}

	public String getSecondAddInsuredIdCardNumber() {
		return secondAddInsuredIdCardNumber;
	}

	public BigDecimal getSecondAddInsuredHeight() {
		return secondAddInsuredHeight;
	}

	public BigDecimal getSecondAddInsuredWeight() {
		return secondAddInsuredWeight;
	}

	public String getSecondAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = secondAddInsuredOccupationDetail.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getSecondAddInsuredOccupation() {
		String firstAddInsuredOccupation = null;
		firstAddInsuredOccupation = secondAddInsuredOccupationDetail.getDescriptionOthers();
		return firstAddInsuredOccupation;
	}

	public String getSecondAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for(EmploymentRelationship employmentRelationship:  secondAddInsuredOccupationDetail.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getSecondAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for(EmploymentRelationship employmentRelationship : secondAddInsuredOccupationDetail.getProvidingEmployment()){
			firstAddInsuredJobDescription=employmentRelationship.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}
	public String getSecondAddInsuredLineOfBusiness() {
		String secondAddInsuredLineOfBusiness = null;
		secondAddInsuredLineOfBusiness = secondAddInsuredOccupationDetail.getNatureofWorkValue();
		try{
			if(secondAddInsuredLineOfBusiness != null && !"".equals(secondAddInsuredLineOfBusiness)){
				if(secondAddInsuredLineOfBusiness.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :secondAddInsuredOccupationDetail.getProvidingEmployment()){
						secondAddInsuredLineOfBusiness = relationship.getBusinessUnit();
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("getSecondAddInsuredLineOfBusiness "+e.getMessage());
		}
		return secondAddInsuredLineOfBusiness;
	}

	public String getSecondAddInsuredHasHeartcirulatoryBloodDisorder() {
		return secondAddInsuredHasHeartcirulatoryBloodDisorder;
	}

	public String getSecondAddInsuredHasAsthmaPnuemonia() {
		return secondAddInsuredHasAsthmaPnuemonia;
	}

	public String getSecondAddInsuredHasHepatitisGastrointestinal() {
		return secondAddInsuredHasHepatitisGastrointestinal;
	}

	public String getSecondAddInsuredHasGenitoUrinary() {
		return secondAddInsuredHasGenitoUrinary;
	}

	public String getSecondAddInsuredMetabolicAndEndorcrineDisorder() {
		return secondAddInsuredMetabolicAndEndorcrineDisorder;
	}

	public String getSecondAddInsuredHasNeurologicalAndPsychiatricDisorder() {
		return secondAddInsuredHasNeurologicalAndPsychiatricDisorder;
	}

	public String getSecondAddInsuredHasHIVOrBoneDisorder() {
		return secondAddInsuredHasHIVOrBoneDisorder;
	}

	public String getSecondAddInsuredHasSpinalDisorder() {
		return secondAddInsuredHasSpinalDisorder;
	}

	public String getSecondAddInsuredHasCancerMelanoma() {
		return secondAddInsuredHasCancerMelanoma;
	}

	public String getSecondAddInsuredCurrentlyPregnent() {
		return secondAddInsuredCurrentlyPregnent;
	}

	public String getSecondAddInsuredOtherDiseases() {
		return secondAddInsuredOtherDiseases;
	}

	public String getSecondAddInsuredReceivedMedTreatment() {
		return secondAddInsuredReceivedMedTreatment;
	}

	public String getSecondAddInsuredHasUsedTobacco() {
		return secondAddInsuredHasUsedTobacco;
	}

	public String getSecondAddInsuredDrugsOrNarcotics() {
		return secondAddInsuredDrugsOrNarcotics;
	}

	public String getSecondAddInsuredHazardousActivities() {
		return secondAddInsuredHazardousActivities;
	}

	public String getSecondAddInsuredFamilyMembersDiagWithDiseases() {
		return secondAddInsuredFamilyMembersDiagWithDiseases;
	}

	/** Third additional insured */

	public Date getThirdAddInsuredDob() {
		return thirdAddInsuredDob;
	}
	public String getThirdAddInsuredFirstName() throws JRScriptletException {
		String name = "";
		for (PersonName addInsuredPersonName : thirdAddInsuredPersonNames) {
			name = addInsuredPersonName.getGivenName();
		}
		return name;
	}

	public String getThirdAddInsuredLastName() throws JRScriptletException {
		String name = "";
		for (PersonName addInsuredPersonName : thirdAddInsuredPersonNames) {
			name = addInsuredPersonName.getSurname();
		}
		return name;
	}
	public String getThirdAddInsuredFirstAndLastName(){
		String thirdAddInsuredFullName = null;
		try {
			thirdAddInsuredFullName = getThirdAddInsuredFirstName()+" "+getThirdAddInsuredLastName();
		} catch (Exception e) {
			LOGGER.error("Error getThirdAddInsuredFirstAndLastName "+e.getMessage());
		}
		return thirdAddInsuredFullName;

	}

	public String getThirdAddInsuredGender() {
		return thirdAddInsuredPerson.getGenderCode().name();
	}

	public String getThirdAddInsuredMaritalStatus() {
		if (thirdAddInsuredPerson != null) {
			if (thirdAddInsuredPerson.getMaritalStatusCode() != null) {
				return thirdAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getThirdAddInsuredNationality() {
		String nationality = null;
		for (Country country : thirdAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	public String getThirdAddInsuredResidenceCountry(){
		String resCountry = null;
		if(thirdAddInsuredResidenceCountry != null){
			resCountry = thirdAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getThirdAddInsuredIdCardType() {
		return thirdAddInsuredIdCardType;
	}

	public String getThirdAddInsuredIdCardNumber() {
		return thirdAddInsuredIdCardNumber;
	}

	public BigDecimal getThirdAddInsuredHeight() {
		return thirdAddInsuredHeight;
	}

	public BigDecimal getThirdAddInsuredWeight() {
		return thirdAddInsuredWeight;
	}

	public String getThirdAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = thirdAddInsuredOccupationDetail.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getThirdAddInsuredOccupation() {
		String firstAddInsuredOccupation = null;
		firstAddInsuredOccupation = thirdAddInsuredOccupationDetail.getDescriptionOthers();
		return firstAddInsuredOccupation;
	}

	public String getThirdAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for(EmploymentRelationship employmentRelationship:  thirdAddInsuredOccupationDetail.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getThirdAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for(EmploymentRelationship employmentRelationship : thirdAddInsuredOccupationDetail.getProvidingEmployment()){
			firstAddInsuredJobDescription=employmentRelationship.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}
	public String getThirdAddInsuredLineOfBusiness() {
		String thirdAddInsuredLineOfBusiness = null;
		thirdAddInsuredLineOfBusiness = thirdAddInsuredOccupationDetail.getNatureofWorkValue();
		try{
			if(thirdAddInsuredLineOfBusiness != null && !"".equals(thirdAddInsuredLineOfBusiness)){
				if(thirdAddInsuredLineOfBusiness.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :thirdAddInsuredOccupationDetail.getProvidingEmployment()){
						thirdAddInsuredLineOfBusiness = relationship.getBusinessUnit();
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("getThirdAddInsuredLineOfBusiness .. "+e.getMessage());
		}
		return thirdAddInsuredLineOfBusiness;
	}
	public String getThirdAddInsuredRelationWithInsured() {
		return thirdAddInsuredRelationWithInsured;
	}

	public String getThirdAddInsuredHasHeartcirulatoryBloodDisorder() {
		return thirdAddInsuredHasHeartcirulatoryBloodDisorder;
	}

	public String getThirdAddInsuredHasAsthmaPnuemonia() {
		return thirdAddInsuredHasAsthmaPnuemonia;
	}

	public String getThirdAddInsuredHasHepatitisGastrointestinal() {
		return thirdAddInsuredHasHepatitisGastrointestinal;
	}

	public String getThirdAddInsuredHasGenitoUrinary() {
		return thirdAddInsuredHasGenitoUrinary;
	}

	public String getThirdAddInsuredMetabolicAndEndorcrineDisorder() {
		return thirdAddInsuredMetabolicAndEndorcrineDisorder;
	}

	public String getThirdAddInsuredHasNeurologicalAndPsychiatricDisorder() {
		return thirdAddInsuredHasNeurologicalAndPsychiatricDisorder;
	}

	public String getThirdAddInsuredHasHIVOrBoneDisorder() {
		return thirdAddInsuredHasHIVOrBoneDisorder;
	}

	public String getThirdAddInsuredHasSpinalDisorder() {
		return thirdAddInsuredHasSpinalDisorder;
	}

	public String getThirdAddInsuredHasCancerMelanoma() {
		return thirdAddInsuredHasCancerMelanoma;
	}

	public String getThirdAddInsuredCurrentlyPregnent() {
		return thirdAddInsuredCurrentlyPregnent;
	}

	public String getThirdAddInsuredOtherDiseases() {
		return thirdAddInsuredOtherDiseases;
	}

	public String getThirdAddInsuredReceivedMedTreatment() {
		return thirdAddInsuredReceivedMedTreatment;
	}

	public String getThirdAddInsuredHasUsedTobacco() {
		return thirdAddInsuredHasUsedTobacco;
	}

	public String getThirdAddInsuredDrugsOrNarcotics() {
		return thirdAddInsuredDrugsOrNarcotics;
	}

	public String getThirdAddInsuredHazardousActivities() {
		return thirdAddInsuredHazardousActivities;
	}

	public String getThirdAddInsuredFamilyMembersDiagWithDiseases() {
		return thirdAddInsuredFamilyMembersDiagWithDiseases;
	}

	/** Fourth additional insured */

	public Date getFourthAddInsuredDob() {
		return fourthAddInsuredDob;
	}
	public String getFourthAddInsuredFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : fourthAddInsuredPersonNames) {
			name = addInsuredPersonName.getGivenName();
		}
		return name;
	}

	public String getFourthAddInsuredLastName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : fourthAddInsuredPersonNames) {
			name = addInsuredPersonName.getSurname();
		}
		return name;
	}
	public String getFourthAddInsuredFirstAndLastName(){
		String fourthAddInsuredFullName = null;
		try {
			fourthAddInsuredFullName = getFourthAddInsuredFirstName()+" "+getFourthAddInsuredLastName();
		} catch (Exception e) {
			LOGGER.error("Error getFourthAddInsuredFirstAndLastName "+e.getMessage());
		}
		return fourthAddInsuredFullName;

	}

	public String getFourthAddInsuredGender() {
		return fourthAddInsuredPerson.getGenderCode().name();
	}

	public String getFourthAddInsuredMaritalStatus() {
		if (fourthAddInsuredPerson != null) {
			if (fourthAddInsuredPerson.getMaritalStatusCode() != null) {
				return fourthAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getFourthAddInsuredNationality() {
		String nationality = null;
		for (Country country : fourthAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	public String getFourthAddInsuredResidenceCountry(){
		String resCountry = null;
		if(fourthAddInsuredResidenceCountry != null){
			resCountry = fourthAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getFourthAddInsuredIdCardType() {
		return fourthAddInsuredIdCardType;
	}

	public String getFourthAddInsuredIdCardNumber() {
		return fourthAddInsuredIdCardNumber;
	}

	public BigDecimal getFourthAddInsuredHeight() {
		return fourthAddInsuredHeight;
	}

	public BigDecimal getFourthAddInsuredWeight() {
		return fourthAddInsuredWeight;
	}

	public String getFourthAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = fourthAddInsuredOccupationDetail.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getFourthAddInsuredOccupation() {
		String firstAddInsuredOccupation = null;
		firstAddInsuredOccupation = fourthAddInsuredOccupationDetail.getDescriptionOthers();
		return firstAddInsuredOccupation;
	}

	public String getFourthAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for(EmploymentRelationship employmentRelationship:  fourthAddInsuredOccupationDetail.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getFourthAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for(EmploymentRelationship employmentRelationship : fourthAddInsuredOccupationDetail.getProvidingEmployment()){
			firstAddInsuredJobDescription=employmentRelationship.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}
	public String getFourthAddInsuredLineOfBusiness() {
		String fourthAddInsuredLineOfBusiness = null;
		fourthAddInsuredLineOfBusiness = fourthAddInsuredOccupationDetail.getNatureofWorkValue();
		try{
			if(fourthAddInsuredLineOfBusiness != null && !"".equals(fourthAddInsuredLineOfBusiness)){
				if(fourthAddInsuredLineOfBusiness.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :fourthAddInsuredOccupationDetail.getProvidingEmployment()){
						fourthAddInsuredLineOfBusiness = relationship.getBusinessUnit();
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("getFourthAddInsuredLineOfBusiness .. "+e.getMessage());
		}
		return fourthAddInsuredLineOfBusiness;
	}
	public String getFourthAddInsuredRelationWithInsured() {
		return fourthAddInsuredRelationWithInsured;
	}

	public String getFourthAddInsuredHasHeartcirulatoryBloodDisorder() {
		return fourthAddInsuredHasHeartcirulatoryBloodDisorder;
	}

	public String getFourthAddInsuredHasAsthmaPnuemonia() {
		return fourthAddInsuredHasAsthmaPnuemonia;
	}

	public String getFourthAddInsuredHasHepatitisGastrointestinal() {
		return fourthAddInsuredHasHepatitisGastrointestinal;
	}

	public String getFourthAddInsuredHasGenitoUrinary() {
		return fourthAddInsuredHasGenitoUrinary;
	}

	public String getFourthAddInsuredMetabolicAndEndorcrineDisorder() {
		return fourthAddInsuredMetabolicAndEndorcrineDisorder;
	}

	public String getFourthAddInsuredHasNeurologicalAndPsychiatricDisorder() {
		return fourthAddInsuredHasNeurologicalAndPsychiatricDisorder;
	}

	public String getFourthAddInsuredHasHIVOrBoneDisorder() {
		return fourthAddInsuredHasHIVOrBoneDisorder;
	}

	public String getFourthAddInsuredHasSpinalDisorder() {
		return fourthAddInsuredHasSpinalDisorder;
	}

	public String getFourthAddInsuredHasCancerMelanoma() {
		return fourthAddInsuredHasCancerMelanoma;
	}

	public String getFourthAddInsuredCurrentlyPregnent() {
		return fourthAddInsuredCurrentlyPregnent;
	}

	public String getFourthAddInsuredOtherDiseases() {
		return fourthAddInsuredOtherDiseases;
	}

	public String getFourthAddInsuredReceivedMedTreatment() {
		return fourthAddInsuredReceivedMedTreatment;
	}

	public String getFourthAddInsuredHasUsedTobacco() {
		return fourthAddInsuredHasUsedTobacco;
	}

	public String getFourthAddInsuredDrugsOrNarcotics() {
		return fourthAddInsuredDrugsOrNarcotics;
	}

	public String getFourthAddInsuredHazardousActivities() {
		return fourthAddInsuredHazardousActivities;
	}

	public String getFourthAddInsuredFamilyMembersDiagWithDiseases() {
		return fourthAddInsuredFamilyMembersDiagWithDiseases;
	}

	/** Beneficiary */
	/** First beneficiary */

	public String getFirstBeneficiaryFirstName() throws JRScriptletException {
		String firstBeneficiaryFirstName = "";
		for (PersonName firstBeneficiaryPersonName : firstBeneficiaryPersonNames) {
			firstBeneficiaryFirstName = firstBeneficiaryPersonName.getGivenName();
		}
		return firstBeneficiaryFirstName;
	}

	public String getFirstBeneficiaryLastName() throws JRScriptletException {
		String firstBeneficiaryLastName = "";
		for (PersonName firstBeneficiaryPersonName : firstBeneficiaryPersonNames) {
			firstBeneficiaryLastName = firstBeneficiaryPersonName.getSurname();
		}
		return firstBeneficiaryLastName;
	}

	public String getFirstBeneficiaryFirstAndLastName(){
		String firstBeneficiaryFullName = "";
		try {
			if (getFirstBeneficiaryLastName()!=null){
				firstBeneficiaryFullName = getFirstBeneficiaryFirstName()+" "+getFirstBeneficiaryLastName();
			}else{
				firstBeneficiaryFullName = getFirstBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving BeneficiaryFirstAndLastName "+e.getMessage());
		}
		return firstBeneficiaryFullName;
	}

	public Date getFirstBeneficiaryDob() {
		return firstBeneficiaryDOB;
	}

	public String getFirstBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(firstBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFirstBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getFirstBeneficiarySharePercentage() {
		return firstBeneficiarySharePercentage;
	}

	public String getFirstBeneficiaryRelationWithInsured() {
		String rel = "";
		if(firstBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(firstBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getFirstBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if(firstBeneficiaryDobValue != null && !"".equalsIgnoreCase(firstBeneficiaryDobValue)){
				year = firstBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first beneficiary year of birth ");
		}
		return year;
	}

	public String getFirstBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if(firstBeneficiaryDobValue != null && !"".equalsIgnoreCase(firstBeneficiaryDobValue)){
				month = firstBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first beneficiary month of birth ");
		}
		return month;
	}

	public String getFirstBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if(firstBeneficiaryDobValue != null && !"".equalsIgnoreCase(firstBeneficiaryDobValue)){
				date = firstBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first beneficiary day of birth ");
		}
		return date;
	}

	/** Second beneficiary */

	public String getSecondBeneficiaryFirstName() throws JRScriptletException {
		String secondBeneficiaryFirstName = "";
		for (PersonName secondBeneficiaryPersonName : secondBeneficiaryPersonNames) {
			secondBeneficiaryFirstName = secondBeneficiaryPersonName.getGivenName();
		}
		return secondBeneficiaryFirstName;
	}

	public String getSecondBeneficiaryLastName() throws JRScriptletException {
		String secondBeneficiaryLastName = "";
		for (PersonName secondBeneficiaryPersonName : secondBeneficiaryPersonNames) {
			secondBeneficiaryLastName = secondBeneficiaryPersonName.getSurname();
		}
		return secondBeneficiaryLastName;
	}

	public String getSecondBeneficiaryFirstAndLastName(){
		String secondBeneficiaryFullName = "";
		try {
			if (getSecondBeneficiaryLastName()!=null){
				secondBeneficiaryFullName = getSecondBeneficiaryFirstName()+" "+getSecondBeneficiaryLastName();
			}else{
				secondBeneficiaryFullName = getSecondBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving SecondBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return secondBeneficiaryFullName;
	}

	public Date getSecondBeneficiaryDob() {
		return secondBeneficiaryDOB;
	}

	public String getSecondBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(secondBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFirstBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getSecondBeneficiarySharePercentage() {
		return secondBeneficiarySharePercentage;
	}

	public String getSecondBeneficiaryRelationWithInsured() {
		String rel = "";
		if(secondBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(secondBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getSecondBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if(secondBeneficiaryDobValue != null && !"".equalsIgnoreCase(secondBeneficiaryDobValue)){
				year = secondBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second beneficiary year of birth ");
		}
		return year;
	}

	public String getSecondBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if(secondBeneficiaryDobValue != null && !"".equalsIgnoreCase(secondBeneficiaryDobValue)){
				month = secondBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second beneficiary month of birth ");
		}
		return month;
	}

	public String getSecondBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if(secondBeneficiaryDobValue != null && !"".equalsIgnoreCase(secondBeneficiaryDobValue)){
				date = secondBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second beneficiary day of birth ");
		}
		return date;
	}

	/** Third beneficiary */

	public String getThirdBeneficiaryFirstName() throws JRScriptletException {
		String thirdBeneficiaryFirstName = null;
		for (PersonName thirdBeneficiaryPersonName : thirdBeneficiaryPersonNames) {
			thirdBeneficiaryFirstName = thirdBeneficiaryPersonName.getGivenName();
		}
		return thirdBeneficiaryFirstName;
	}

	public String getThirdBeneficiaryLastName() throws JRScriptletException {
		String thirdBeneficiaryLastName = null;
		for (PersonName thirdBeneficiaryPersonName : thirdBeneficiaryPersonNames) {
			thirdBeneficiaryLastName = thirdBeneficiaryPersonName.getSurname();
		}
		return thirdBeneficiaryLastName;
	}

	public String getThirdBeneficiaryFirstAndLastName(){
		String thirdBeneficiaryFullName = null;
		try {
			if (getThirdBeneficiaryLastName()!=null){
				thirdBeneficiaryFullName = getThirdBeneficiaryFirstName()+" "+getThirdBeneficiaryLastName();
			}else{
				thirdBeneficiaryFullName = getThirdBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving ThirdBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return thirdBeneficiaryFullName;
	}

	public Date getThirdBeneficiaryDob() {
		return thirdBeneficiaryDOB;
	}

	public String getThirdBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(thirdBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFirstBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getThirdBeneficiarySharePercentage() {
		return thirdBeneficiarySharePercentage;
	}

	public String getThirdBeneficiaryRelationWithInsured() {
		String rel = "";
		if(thirdBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(thirdBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getThirdBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if(thirdBeneficiaryDobValue != null && !"".equalsIgnoreCase(thirdBeneficiaryDobValue)){
				year = thirdBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third beneficiary year of birth ");
		}
		return year;
	}

	public String getThirdBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if(thirdBeneficiaryDobValue != null && !"".equalsIgnoreCase(thirdBeneficiaryDobValue)){
				month = thirdBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third beneficiary month of birth ");
		}
		return month;
	}

	public String getThirdBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if(thirdBeneficiaryDobValue != null && !"".equalsIgnoreCase(thirdBeneficiaryDobValue)){
				date = thirdBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third beneficiary day of birth ");
		}
		return date;
	}

	/** Fourth beneficiary */

	public String getFourthBeneficiaryFirstName() throws JRScriptletException {
		String fourthBeneficiaryFirstName = "";
		for (PersonName fourthBeneficiaryPersonName : fourthBeneficiaryPersonNames) {
			fourthBeneficiaryFirstName = fourthBeneficiaryPersonName.getGivenName();
		}
		return fourthBeneficiaryFirstName;
	}

	public String getFourthBeneficiaryLastName() throws JRScriptletException {
		String fourthBeneficiaryLastName = "";
		for (PersonName fourthBeneficiaryPersonName : fourthBeneficiaryPersonNames) {
			fourthBeneficiaryLastName = fourthBeneficiaryPersonName.getSurname();
		}
		return fourthBeneficiaryLastName;
	}

	public String getFourthBeneficiaryFirstAndLastName(){
		String fourthBeneficiaryFullName = "";
		try {
			if (getFourthBeneficiaryLastName()!=null){
				fourthBeneficiaryFullName = getFourthBeneficiaryFirstName()+" "+getFourthBeneficiaryLastName();
			}else{
				fourthBeneficiaryFullName = getFourthBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving FourthBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return fourthBeneficiaryFullName;
	}

	public Date getFourthBeneficiaryDob() {
		return fourthBeneficiaryDOB;
	}

	public String getFourthBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(fourthBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFourthBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getFourthBeneficiarySharePercentage() {
		return fourthBeneficiarySharePercentage;
	}

	public String getFourthBeneficiaryRelationWithInsured() {
		String rel = "";
		if(fourthBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(fourthBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getFourthBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if(fourthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fourthBeneficiaryDobValue)){
				year = fourthBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth beneficiary year of birth ");
		}
		return year;
	}

	public String getFourthBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if(fourthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fourthBeneficiaryDobValue)){
				month = fourthBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth beneficiary month of birth ");
		}
		return month;
	}

	public String getFourthBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if(fourthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fourthBeneficiaryDobValue)){
				date = fourthBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth beneficiary day of birth ");
		}
		return date;
	}

	/** Fifth beneficiary */

	public String getFifthBeneficiaryFirstName() throws JRScriptletException {
		String fifthBeneficiaryFirstName = "";
		for (PersonName fifthBeneficiaryPersonName : fifthBeneficiaryPersonNames) {
			fifthBeneficiaryFirstName = fifthBeneficiaryPersonName.getGivenName();
		}
		return fifthBeneficiaryFirstName;
	}

	public String getFifthBeneficiaryLastName() throws JRScriptletException {
		String fifthBeneficiaryLastName = "";
		for (PersonName fifthBeneficiaryPersonName : fifthBeneficiaryPersonNames) {
			fifthBeneficiaryLastName = fifthBeneficiaryPersonName.getSurname();
		}
		return fifthBeneficiaryLastName;
	}

	public String getFifthBeneficiaryFirstAndLastName(){
		String fifthBeneficiaryFullName = "";
		try {
			if (getFifthBeneficiaryLastName()!=null){
				fifthBeneficiaryFullName = getFifthBeneficiaryFirstName()+" "+getFifthBeneficiaryLastName();
			}else{
				fifthBeneficiaryFullName = getFifthBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving FifthBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return fifthBeneficiaryFullName;
	}

	public Date getFifthBeneficiaryDob() {
		return fifthBeneficiaryDOB;
	}

	public String getFifthBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(fifthBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFifthBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getFifthBeneficiarySharePercentage() {
		return fifthBeneficiarySharePercentage;
	}

	public String getFifthBeneficiaryRelationWithInsured() {
		String rel = "";
		if(fifthBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(fifthBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getFifthBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date fifthBeneficiaryDob = getFifthBeneficiaryDob();
			String fifthBeneficiaryDobValue = fifthBeneficiaryDob.toString();
			if(fifthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fifthBeneficiaryDobValue)){
				year = fifthBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fifth beneficiary year of birth ");
		}
		return year;
	}

	public String getFifthBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date fifthBeneficiaryDob = getFifthBeneficiaryDob();
			String fifthBeneficiaryDobValue = fifthBeneficiaryDob.toString();
			if(fifthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fifthBeneficiaryDobValue)){
				month = fifthBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fifth beneficiary month of birth ");
		}
		return month;
	}

	public String getFifthBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date fifthBeneficiaryDob = getFifthBeneficiaryDob();
			String fifthBeneficiaryDobValue = fifthBeneficiaryDob.toString();
			if(fifthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fifthBeneficiaryDobValue)){
				date = fifthBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fifth beneficiary day of birth ");
		}
		return date;
	}

	/** Financial Data */

	public String getInsurancePurposeBusiness() {
		return insurancePurposeBusiness;
	}

	public String getInsurancePurposeDesc() {
		return insurancePurposeDesc;
	}

	public String getInsurancePurposeEducation() {
		return insurancePurposeEducation;
	}

	public String getInsurancePurposeInvestment() {
		return insurancePurposeInvestment;
	}

	public String getInsurancePurposePensionFund() {
		return insurancePurposePensionFund;
	}

	public String getInsurancePurposeProtection() {
		return insurancePurposeProtection;
	}

	public String getInsurancePurposeOther() {
		return insurancePurposeOther;
	}
	public String getProposerSourceOfIncome(){
		String proposerSourceOfIncome = null;
		proposerSourceOfIncome = proposerIncomeDetail.getTypeCode().name();
		return proposerSourceOfIncome;
	}

	public String getProposerSourceOfIncomeDescription(){
		String proposerSourceOfIncomeDesc = null;
		proposerSourceOfIncomeDesc = proposerIncomeDetail.getDescription();
		return proposerSourceOfIncomeDesc;
	}
	public String getPremiumPayerSourceOfIncomeDescription(){
		String proposerSourceOfIncomeDesc = null;
		proposerSourceOfIncomeDesc = payerIncomeDetail.getDescription();
		return proposerSourceOfIncomeDesc;
	}
	public String getPremiumPayerSourceOfIncome(){
		String premiumPayerSourceOfIncome = null;
		premiumPayerSourceOfIncome = payerIncomeDetail.getTypeCode().name();
		return premiumPayerSourceOfIncome;
	}


	public String getProposerGrossIncome(){
		String proposerGrossIncome = null;
		proposerGrossIncome = proposerIncomeDetail.getGrossIncome();
		return proposerGrossIncome;
	}

	public String getProposerGrossIncomeDescription(){
		String proposerGrossIncomeDesc = null;
		proposerGrossIncomeDesc = proposerIncomeDetail.getGrossIncomeDesc();
		return proposerGrossIncomeDesc;
	}
	public String getPayerGrossIncome(){
		String payerGrossIncome = null;
		if(payerIncomeDetail.getGrossIncome() != null && !"".equals(payerIncomeDetail.getGrossIncome())){
			payerGrossIncome = payerIncomeDetail.getGrossIncome().trim();
		}
		return payerGrossIncome;
	}

	public String getPayerGrossIncomeDescription(){
		String payerGrossIncome = null;
		payerGrossIncome = payerIncomeDetail.getGrossIncomeDesc();
		return payerGrossIncome;
	}

	public String getProposerPaymentMethod(){
		return proposerPaymentMethod;
	}
	public String getCreditCardType() {
		if (creditCardType == "Unknown" || creditCardType == null)
		{
			creditCardType="";

		}else{
			return creditCardType;
		}
		return creditCardType;
	}
	public Long getCreditCardNumber() {
		return creditCardNumber;
	}
	public String getCreditCardName() {
		return creditCardName;
	}
	public String getAccountHolderName() {
		accountHolderName="";
		if(proposerPaymentMethod.equalsIgnoreCase("DebitAccount")){
			accountHolderName = payer.getPartyType();
		}else{
			accountHolderName="";
		}
		return accountHolderName;
	}
	public String getBankName() {
		return bankName;
	}
	public String getBranchName() {
		return branchName;
	}
	public String getAccountNumber() {
		if(accountNumber == null || accountNumber == 0L)
		{
			String acno="";
			return acno;
		}else{
			return accountNumber.toString();
		}
	}
	public String getSpajNumber() {
		return spajNumber;
	}
	public String getIllustrationNumber() {
		return illustrationNumber;
	}


	//* Credit card payment details* //

	/*public String getCreditCardAccHolderName(){
		String creditCardAccHolderName = null;

	}*/

	public String getProposerYearOfBirth(){
		String year = null;
		try{
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if(propDobValue != null && !"".equalsIgnoreCase(propDobValue)){
				year = propDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting proposer year of birth ");
		}
		return year;
	}

	public String getProposerMonthOfBirth(){
		String month = null;
		try{
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if(propDobValue != null && !"".equalsIgnoreCase(propDobValue)){
				month = propDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting proposer month of birth ");
		}
		return month;
	}

	public String getProposerDayOfBirth(){
		String date = null;
		try{
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if(propDobValue != null && !"".equalsIgnoreCase(propDobValue)){
				date = propDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting proposer month of birth ");
		}
		return date;
	}

	public String getInsuredYearOfBirth(){
		String year = "";
		if(isPropDiffFromInsured){
			try{
				Date insDob = getInsuredDob();
				String insDobValue = insDob.toString();
				if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
					year = insDobValue.substring(0, 4);
				}
			}catch(Exception e){
				LOGGER.error("Error while getting proposer year of birth ");
			}
		}
		return year;
	}

	public String getInsuredMonthOfBirth(){
		String month = "";
		if(isPropDiffFromInsured){
			try{
				Date insDob = getInsuredDob();
				String insDobValue = insDob.toString();
				if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
					month = insDobValue.substring(5, 7);
				}
			}catch(Exception e){
				LOGGER.error("Error while getting insured month of birth ");
			}
		}
		return month;
	}

	public String getInsuredDayOfBirth(){
		String date = "";
		if(isPropDiffFromInsured){
			try{
				Date insDob = getInsuredDob();
				String insDobValue = insDob.toString();
				if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
					date = insDobValue.substring(8, 10);
				}
			}catch(Exception e){
				LOGGER.error("Error while getting insured day of birth ");
			}
		}
		return date;
	}

	public String getFirstAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getFirstAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first addinsured year of birth ");
		}
		return year;
	}

	public String getFirstAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getFirstAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first addinsured month of birth ");
		}
		return month;
	}

	public String getFirstAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getFirstAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first addinsured day of birth ");
		}
		return date;
	}
	public String getSecondAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getSecondAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second addinsured year of birth ");
		}
		return year;
	}

	public String getSecondAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getSecondAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second addinsured month of birth ");
		}
		return month;
	}

	public String getSecondAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getSecondAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second addinsured day of birth ");
		}
		return date;
	}
	public String getThirdAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getThirdAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third addinsured year of birth ");
		}
		return year;
	}

	public String getThirdAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getThirdAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third addinsured month of birth ");
		}
		return month;
	}

	public String getThirdAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getThirdAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third addinsured day of birth ");
		}
		return date;
	}
	public String getFourthAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getFourthAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth addinsured year of birth ");
		}
		return year;
	}

	public String getFourthAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getFourthAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth addinsured month of birth ");
		}
		return month;
	}

	public String getFourthAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getFourthAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth addinsured day of birth ");
		}
		return date;
	}

	public String getInsuredRelationWithProposer(){
		String relationshipWithProp = "";
		if(getIsPropDiffFromInsured()){
			return relationshipWithProposer;
		}
		return relationshipWithProp;
	}

	public String getPremiumPayer(){
		return premiumPayer;
	}
	public String getCreditCardDayOfExpiry(){
		Date dobValue = creditCardExpiryDate;
		String ccDayofExpiry="";
		if(proposerPaymentMethod.equalsIgnoreCase("CreditCard")){
			if(dobValue != null && dobValue.equals("1900-01-01")){
				try{
					splitDate(dobValue,8,10);
				}catch(Exception e){
					LOGGER.error("Error while getting credit card day of expiry ");
				}
			}
		}
		else {
			ccDayofExpiry="";
		}
		return ccDayofExpiry;
	}

	public String getCreditCardMonthOfExpiry(){
		Date dobValue = creditCardExpiryDate;
		String ccMonthofExpiry="";
		if(proposerPaymentMethod.equalsIgnoreCase("CreditCard")){
			if(dobValue != null && !dobValue.equals("1900-01-01")){
				try{
					ccMonthofExpiry=splitDate(dobValue,5,7);
				}catch(Exception e){
					LOGGER.error("Error while getting credit card month of expiry ");
				}
			}
		}
		else {
			ccMonthofExpiry="";
		}
		return ccMonthofExpiry;
	}
	public String getCreditCardYearOfExpiry(){
		Date dobValue = creditCardExpiryDate;
		String ccYearofExpiry="";
		if(proposerPaymentMethod.equalsIgnoreCase("CreditCard")){
			if(dobValue != null && !dobValue.equals("1900-01-01")){
				try{
					ccYearofExpiry=splitDate(dobValue,0,4);
				}catch(Exception e){
					LOGGER.error("Error while getting credit year month of expiry ");
				}
			}
		}
		else {
			ccYearofExpiry="";
		}
		return ccYearofExpiry;
	}
	public String splitDate(Date date, int firstIndex , int lastIndex) throws Exception{
		String dobValue = date.toString();
		String dateSplit="";

		if(dobValue != null && !"".equalsIgnoreCase(dobValue)){
			dateSplit = dobValue.substring(firstIndex, lastIndex);
		}

		return dateSplit;
	}

	public String getDecalrationQuestionAnswer(String Number) {
		String answer="";
		try{

			for(UserSelection question : declationQuestions)
			{
				if(Number.equals(question.getQuestionId().toString()) && ("Basic").equals(question.getQuestionnaireType().name()) ){
					if(true==question.getSelectedOption())
					{
						answer="true";

					}
					else if(false==question.getSelectedOption()){
						answer="false";
					}
					break;
				}
			}


		}catch(Exception e){
			LOGGER.error("Error while getting decalartion answer option of question "+Number);
		}
		return answer;

	}

	public String getDecalrationQuestionAnswerDetail(String Number) {
		String answer="";
		try{
			for(UserSelection question : declationQuestions)
			{
				if(Number.equals("7")){
					if(question.getQuestionId() != null && question.getDetailedInfo() != null && question.getQuestionId().toString().equals("7")){
						answer=question.getDetailedInfo();
					}
				}else if(Number.equals(question.getQuestionId().toString()) && ("Basic").equals(question.getQuestionnaireType().name())){
					if(true==question.getSelectedOption()){
						/*if(!empty.equals(question.getDetailedInfo())){
							answer=question.getDetailedInfo()+","+" ";
						}*/
						if(question.getDetailedInfo() != null){
							answer=question.getDetailedInfo()+",";
						}
						for(GLISelectedOption answerOption : question.getSelectedOptions()){
							/*if(!empty.equals(answerOption.getSelectedOptionValue())){
								answer+=answerOption.getSelectedOptionValue()+ ","+" ";
							}*/
							if(answerOption.getSelectedOptionValue() != null){
								answer+=answerOption.getSelectedOptionValue()+ ",";
							}
						}
						String words[] = answer.split(",");
						answer = "";
						for(int i=0;i<words.length;i++){
							if(!words[i].equals("")){
								answer += words[i]+",";
							}
						}
						answer=answer.substring(0, answer.length()-1);
						break;
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("Error while getting decalartion answer detail of question "+Number);
		}
		return answer;

	}
	public String getFirstAddinsuredLostWeight() {
		return firstAddinsuredLostWeight;
	}

	public String getFirstAddinsuredLostWeightReason() {
		return firstAddinsuredLostWeightReason;
	}
	public String getProposerFacta1(){
		return proposerFatcaQues1;
	}

	public String getProposerFacta2(){
		return proposerFatcaQues2;
	}

	public Boolean getBeneficiaryFacta1(){
		return beneficiaryFatcaQues1;
	}

	public Boolean getBeneficiaryFacta2(){
		return beneficiaryFatcaQues2;
	}

	public String getSecondAddinsuredLostWeight() {
		return secondAddinsuredLostWeight;
	}

	public String getSecondAddinsuredLostWeightReason() {
		return secondAddinsuredLostWeightReason;
	}

	public String getThirdAddinsuredLostWeight() {
		return thirdAddinsuredLostWeight;
	}

	public String getThirdAddinsuredLostWeightReason() {
		return thirdAddinsuredLostWeightReason;
	}

	public String getFourthAddinsuredLostWeight() {
		return fourthAddinsuredLostWeight;
	}

	public String getFourthAddinsuredLostWeightReason() {
		return fourthAddinsuredLostWeightReason;
	}
	public BigDecimal getInsuredHeight() {
		return insuredHeight;
	}
	public BigDecimal getInsuredWeight() {
		return insuredWeight;
	}
	public String getInsuredLostWeight() {
		return insuredLostWeight;
	}
	public String getInsuredLostWeightReason() {
		return insuredLostWeightReason;
	}
	public String getAgentCode() {
		return agentCode;
	}

	/** Previous Policy */

	public String getFirstCompanyName() {
		return firstCompanyName;
	}

	public String getFirstPrevInsuredName() {
		return firstPrevInsuredName;
	}

	public String getFirstFinalDecision() {
		return firstFinalDecision;
	}

	public String getFirstTypeOfInsurance() {
		return firstTypeOfInsurance;
	}

	public String getFirstPolicyStatus() {
		return firstPolicyStatus;
	}

	public BigDecimal getFirstPrevSumAssured() {
		return firstPrevSumAssured;
	}

	public String getSecondCompanyName() {
		return secondCompanyName;
	}

	public String getSecondPrevInsuredName() {
		return secondPrevInsuredName;
	}

	public String getSecondFinalDecision() {
		return secondFinalDecision;
	}

	public String getSecondTypeOfInsurance() {
		return secondTypeOfInsurance;
	}

	public String getSecondPolicyStatus() {
		return secondPolicyStatus;
	}

	public BigDecimal getSecondPrevSumAssured() {
		return secondPrevSumAssured;
	}

	public String getThirdCompanyName() {
		return thirdCompanyName;
	}

	public String getThirdPrevInsuredName() {
		return thirdPrevInsuredName;
	}

	public String getThirdFinalDecision() {
		return thirdFinalDecision;
	}

	public String getThirdTypeOfInsurance() {
		return thirdTypeOfInsurance;
	}

	public String getThirdPolicyStatus() {
		return thirdPolicyStatus;
	}

	public BigDecimal getThirdPrevSumAssured() {
		return thirdPrevSumAssured;
	}

	public String getFourthCompanyName() {
		return fourthCompanyName;
	}

	public String getFourthPrevInsuredName() {
		return fourthPrevInsuredName;
	}

	public String getFourthFinalDecision() {
		return fourthFinalDecision;
	}

	public String getFourthTypeOfInsurance() {
		return fourthTypeOfInsurance;
	}

	public String getFourthPolicyStatus() {
		return fourthPolicyStatus;
	}

	public BigDecimal getFourthPrevSumAssured() {
		return fourthPrevSumAssured;
	}

	public String getFifthCompanyName() {
		return fifthCompanyName;
	}

	public String getFifthPrevInsuredName() {
		return fifthPrevInsuredName;
	}

	public String getFifthFinalDecision() {
		return fifthFinalDecision;
	}

	public String getFifthTypeOfInsurance() {
		return fifthTypeOfInsurance;
	}

	public String getFifthPolicyStatus() {
		return fifthPolicyStatus;
	}

	public BigDecimal getFifthPrevSumAssured() {
		return fifthPrevSumAssured;
	}
	public String getAgencyName() {
		return agencyName;
	}
	public String getAgentMobileNo() {
		return agentMobileNo;
	}
	public String getAgentLandNo() {
		return agentLandNo;
	}
	public String getAgentName() {
		String agentName = null;
		try{
			if(agentPerson != null){
				for (PersonName personName : agentPerson.getName()) {
					agentName = personName.getFullName();
				}
			}
		}catch(Exception e){
			LOGGER.error("-- getAgentName -- "+e.getMessage());
		}
		return agentName;
	}

	public String getAgentSignDate() {
		return agentSignDate;
	}

	public String getAgentYearOfBirth(){
		String year = null;
		try{
			String ageDobValue = getAgentSignDate();
			if(ageDobValue != null && !"".equalsIgnoreCase(ageDobValue)){
				year = ageDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting agent year of birth ");
		}
		return year;
	}

	public String getAgentMonthOfBirth(){
		String month = null;
		try{
			String agenDobValue = getAgentSignDate();
			if(agenDobValue != null && !"".equalsIgnoreCase(agenDobValue)){
				month = agenDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting agent month of birth ");
		}
		return month;
	}

	public String getAgentDayOfBirth(){
		String date = null;
		try{
			String agenDobValue = getAgentSignDate();
			if(agenDobValue != null && !"".equalsIgnoreCase(agenDobValue)){
				date = agenDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting agent day of birth ");
		}
		return date;
	}

	public String getAgentSignPlace() {
		return agentSignPlace;
	}

	public Date getSpajSignDate() {
		return spajSignDate;
	}

	public String getSpajYearOfBirth(){
		String year = null;
		try{
			Date spYear = getSpajSignDate();
			String spDobValue = spYear.toString();
			if(spDobValue != null && !"".equalsIgnoreCase(spDobValue)){
				year = spDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting spaj year of birth ");
		}
		return year;
	}

	public String getSpajMonthOfBirth(){
		String month = null;
		try{
			Date spMonth = getSpajSignDate();
			String spDobValue = spMonth.toString();
			if(spDobValue != null && !"".equalsIgnoreCase(spDobValue)){
				month = spDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting spaj month of birth ");
		}
		return month;
	}

	public String getSpajDayOfBirth(){
		String date = null;
		try{
			Date spDob = getSpajSignDate();
			String spDobValue = spDob.toString();
			if(spDobValue != null && !"".equalsIgnoreCase(spDobValue)){
				date = spDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting spaj day of birth ");
		}
		return date;
	}

	public String getSpajSignPlace() {
		return SpajSignPlace;
	}

	public String getSpajAccountHolderName() {
		return spajAccountHolderName;
	}

	public String getSpajBankName() {
		return spajBankName;
	}

	public String getSpajBranchName() {
		return spajBranchName;
	}

	public String getSpajAccountNumber() {
		return spajAccountNumber;
	}
	public String getGemilangHealthQuest1() {
		return gemilangHealthQuest1;
	}
	public String getGemilangHealthQuest2() {
		return gemilangHealthQuest2;
	}
	public String getGemilangHealthQuest3() {
		return gemilangHealthQuest3;
	}
	public Boolean getIsPropDiffFromInsured() {
		return isPropDiffFromInsured;
	}

	public String getInsuredReasonForInsurance() {
		return insuredReasonForInsurance;
	}
	public String getFirstAddInsuredReasonForInsurance() {
		return firstAddInsuredReasonForInsurance;
	}
	public String getSecondAddInsuredReasonForInsurance() {
		return secondAddInsuredReasonForInsurance;
	}
	public String getThirdAddInsuredReasonForInsurance() {
		return thirdAddInsuredReasonForInsurance;
	}
	public String getFourthAddInsuredReasonForInsurance() {
		return fourthAddInsuredReasonForInsurance;
	}
	public String getDescription() {
		return description;
	}

	public String getInsuredPregnentDescription(){
		return insuredPregnentDescription;
	}

	public Integer getInsuredTobaccoSticks(){
		return insuredTobaccoSticks;
	}

	public Integer getFirstAddInsuredTobaccoSticks(){
		return firstAddInsuredTobaccoSticks;
	}

	public Integer getSecondAddInsuredTobaccoSticks(){
		return secondAddInsuredTobaccoSticks;
	}

	public Integer getThirdAddInsuredTobaccoSticks(){
		return thirdAddInsuredTobaccoSticks;
	}

	public Integer getFourthAddInsuredTobaccoSticks(){
		return fourthAddInsuredTobaccoSticks;
	}

	public String getFirstAddInsuredPregnentDescription(){
		return firstAddInsuredPregnentDescription;
	}
	public String getSecondAddInsuredPregnentDescription(){
		return secondAddInsuredPregnentDescription;
	}
	public String getThirdAddInsuredPregnentDescription(){
		return thirdAddInsuredPregnentDescription;
	}
	public String getFourthAddInsuredPregnentDescription(){
		return fourthAddInsuredPregnentDescription;
	}
	public String getBenRelationInBahsa(String relation){
		String rel = "";
		try{
			if(relation != null && !"".equals(relation)){
				if(relation.equalsIgnoreCase("Husband")){
					rel = "Suami";
				}else if(relation.equalsIgnoreCase("Son")){
					rel = "Anak Laki Laki";
				}else if(relation.equalsIgnoreCase("Father")){
					rel = "Ayah";
				}else if(relation.equalsIgnoreCase("Wife")){
					rel = "Istri";
				}else if(relation.equalsIgnoreCase("Daughter")){
					rel = "Anak Perempuan";
				}else if(relation.equalsIgnoreCase("Mother")){
					rel = "Ibu";
				}else if(relation.equalsIgnoreCase("Siblings")){
					rel = "Saudara Kandung";
				}else if(relation.equalsIgnoreCase("Others")){
					rel = "Lainnya";
				}
			}
		}catch(Exception e){
			LOGGER.error("Error in getBenRelationInBahsa :"+e.getMessage());
		}
		return rel;
	}

	public String getBenGender(String gender){
		String gen = "";
		if(gender != null){
			if(gender.equalsIgnoreCase("Male")){
				gen = "Pria";
			}else if(gender.equalsIgnoreCase("Female")){
				gen = "Wanita";
			}
		}
		return gen;
	}

}