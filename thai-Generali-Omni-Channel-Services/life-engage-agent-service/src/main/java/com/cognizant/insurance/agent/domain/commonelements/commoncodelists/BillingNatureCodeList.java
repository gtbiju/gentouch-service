/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * A code list that identifies the nature of the billing (policy bill, account
 * bill).
 * 
 * @author 301350
 * 
 * 
 */
public enum BillingNatureCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    AccountBill,
    /**
     * 
     * 
     * 
     * 
     */
    PolicyBill
}
