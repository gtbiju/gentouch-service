/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.contactpointsubtypes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.domain.contactandplace.ContactPoint;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneNetworkTypeCodeList;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;

/**
 * This concept represents a telephone as a means of contact.
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */


@Entity
public class TelephoneCallContact extends ContactPoint {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -453126016517665568L;

    /**
     * This attribute defines the type of phone number (fax, cell ...).
     * 
     * 
     * 
     */
    @Enumerated
    private TelephoneTypeCodeList typeCode;

    /**
     * A code identifying a type of telephone network contact.
     * 
     * 
     * 
     */
    @Enumerated
    private TelephoneNetworkTypeCodeList networkTypeCode;

    /** The TTY indicator. */
    private Boolean ttyIndicator;
    
    /**
     * A code identifying the applicable country calling code.
     * 
     * Reference: http://en.wikipedia.org/wiki/List_of_country_calling_codes
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    private ExternalCode countryCode;

    /**
     * A trunk prefix is the number to be dialed in a domestic telephone call,
     * preceding any necessary area codes and subscriber numbers. When the
     * number is called from overseas, the trunk prefix is omitted by the
     * caller. In most countries, such as Australia, Germany and the United
     * Kingdom, the trunk prefix is 0. In the North American Numbering Plan it
     * is 1.
     * 
     * For example, assume a call is to be made to someone in the Australian
     * state of Queensland. A caller from outside Australia would dial the
     * international access number (international call prefix) of the
     * originating country (00 for many countries, 011 from NANP areas), then
     * dial the country code (in this case 61), omit the trunk prefix, then dial
     * the area code (7), and then the local number: 00 61 7 3333 3333.
     * 
     * Calling interstate (e.g. from Western Australia), a caller would not dial
     * an international access number or country code, but dial the trunk prefix
     * then the area code, followed by the telephone number: 0 7 3333 3333
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Trunk_prefix
     * 
     * 
     * 
     */
    private String trunkPrefix;

    /**
     * Area codes are also known as Numbering Plan Areas (NPAs) and formerly
     * known as STD codes in the UK. These are necessary (for the most part)
     * only when dialed from outside the code area, from mobile phones, and
     * (especially within North America) from within overlay plans. Area codes
     * usually indicate geographical areas within one country that are covered
     * by perhaps hundreds of telephone exchanges, although the correlation to
     * geographical area is becoming obsolete.
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Area_code#Area_code 
     *
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private ExternalCode areaCode;

    /**
     * The extension of a telephone number, typically used in business, and
     * often found following the full telephone number.
     * 
     * e.g. 1 800 123 4567 ext. 890
     * 
     * 
     * 
     * 
     * 
     */
    @Column(name = "extension", columnDefinition = "NVARCHAR(MAX)")
    private String extension;

    /**
     * The full/complete telephone number. 
     * 
     * 
     * 
     */
    private String fullNumber;
    
    /** The invalid indicator. */
    private Boolean invalidIndicator;

    
    /**
     * The local number (or subscriber number) must always be dialed in its
     * entirety. The first few digits in the local number typically indicate
     * smaller geographical areas or individual telephone exchanges. In mobile
     * networks they may indicate a network provider in case the area code does
     * not. Callers from a number with a given area/country code usually do not
     * need to (but optionally may) include the particular area/country code in
     * the number dialed, which enables shorter "dial strings" to be used.
     * Devices that dial phone numbers automatically can include the full number
     * with area and access codes, since there is no additional annoyance
     * related to dialing extra digits.
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Local_number#Local_number
     * 
     * 
     * Telex messages are routed by addressing them to a telex address, e.g.,
     * "14910 ERIC S", where 14910 is the subscriber number, ERIC is an
     * abbreviation for the subscriber's name (in this case Telefonaktiebolaget
     * L.M. Ericsson in Sweden) and S is the country code. Solutions also exist
     * for the automatic routing of messages to different telex terminals within
     * a subscriber organization, by using different terminal identities, e.g.,
     * "+T148".
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Telex
     * 
     * 
     * 
     */
    private String localNumber;

    
    /**
     * A single, formal string representation of the address for the resource.
     * On a given network type, this Integer provides enough information to be
     * able to locate the associated address point. For example, on a telephone
     * network, this is the string commonly thought of as the "Phone Number."
     * 
     * USAGE NOTE: The format of the Integer is driven either by the convention
     * of the implementer or specified in the definition of the sub-class. E.g.
     * The UniformResourceLocation.identifier has a very specific format.
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Uniform_resource_identifier <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    private Integer identifier;

    /**
     * @return the identifier
     * 
     * 
     */
    public Integer getIdentifier() {

        return identifier;

    }

    /**
     * @param identifier
     *            the Integer to set
     * 
     * 
     */
    public void setIdentifier(final Integer identifier) {

        this.identifier = identifier;

    }
    
    /**
     * @return the localNumber
     * 
     * 
     */
    public String getLocalNumber() {

        return localNumber;

    }

    /**
     * @param localNumber
     *            the localNumber to set
     * 
     * 
     */
    public void setLocalNumber(final String localNumber) {

        this.localNumber = localNumber;

    }
    
    /**
     * Gets the countryCode.
     * 
     * @return Returns the countryCode.
     */
    public ExternalCode getCountryCode() {
        return countryCode;
    }

    /**
     * Gets the trunkPrefix.
     * 
     * @return Returns the trunkPrefix.
     */
    public String getTrunkPrefix() {
        return trunkPrefix;
    }

    /**
     * Gets the areaCode.
     * 
     * @return Returns the areaCode.
     */
    public ExternalCode getAreaCode() {
        return areaCode;
    }

    /**
     * Gets the extension.
     * 
     * @return Returns the extension.
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Gets the fullNumber.
     * 
     * @return Returns the fullNumber.
     */
    public String getFullNumber() {
        return fullNumber;
    }

    /**
     * Sets The countryCode.
     * 
     * @param countryCode
     *            The countryCode to set.
     */
    public void setCountryCode(final ExternalCode countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * Sets The trunkPrefix.
     * 
     * @param trunkPrefix
     *            The trunkPrefix to set.
     */
    public void setTrunkPrefix(final String trunkPrefix) {
        this.trunkPrefix = trunkPrefix;
    }

    /**
     * Sets The areaCode.
     * 
     * @param areaCode
     *            The areaCode to set.
     */
    public void setAreaCode(final ExternalCode areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * Sets The extension.
     * 
     * @param extension
     *            The extension to set.
     */
    public void setExtension(final String extension) {
        this.extension = extension;
    }

    /**
     * Sets The fullNumber.
     * 
     * @param fullNumber
     *            The fullNumber to set.
     */
    public void setFullNumber(final String fullNumber) {
        this.fullNumber = fullNumber;
    }
	/**
	 * @param invalidIndicator the invalidIndicator to set
	 */
	public void setInvalidIndicator(final Boolean invalidIndicator) {
		this.invalidIndicator = invalidIndicator;
	}

	/**
	 * @return the invalidIndicator
	 */
	public Boolean getInvalidIndicator() {
		return invalidIndicator;
	}


    /**
     * Gets the network type code.
     * 
     * @return the networkTypeCode
     */
    public TelephoneNetworkTypeCodeList getNetworkTypeCode() {

        return networkTypeCode;

    }



    /**
     * @return the ttyIndicator
     */
    public Boolean getTtyIndicator() {
        
        return ttyIndicator;
    }

    /**
     * Gets the type code.
     * 
     * @return the typeCode
     */
    public TelephoneTypeCodeList getTypeCode() {

        return typeCode;
    }

    /**
     * Sets the network type code.
     * 
     * @param networkTypeCode
     *            the networkTypeCode to set
     */
    public void setNetworkTypeCode(
            final TelephoneNetworkTypeCodeList networkTypeCode) {
        
        this.networkTypeCode = networkTypeCode;
    }



    /**
     * @param ttyIndicator
     *            the ttyIndicator to set
     */
    public void setTtyIndicator(final Boolean ttyIndicator) {
        
        this.ttyIndicator = ttyIndicator;
    }

    /**
     * Sets the type code.
     * 
     * @param typeCode
     *            the typeCode to set
     */
    public void setTypeCode(final TelephoneTypeCodeList typeCode) {
        
        this.typeCode = typeCode;
    }
}
