package com.cognizant.insurance.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.cognizant.insurance.audit.IllustrationResponsePayLoad;
import com.cognizant.insurance.domain.agreement.Coverage;
import com.cognizant.insurance.domain.agreement.agreementcodelists.CoverageCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.party.partyname.PersonName;

// TODO: Auto-generated Javadoc
/**
 * The Class IllustrationScriptlet.
 */
public class IllustrationScriptlet extends JRDefaultScriptlet {
	
	/** The json object. */
	private JSONObject jsonObject;
	
	/** The agreement. */
	private InsuranceAgreement agreement;
	
	/** The payer. */
	private PremiumPayer payer;
	
	/** The insured. */
	private Insured insured;
	
	/** The payer person. */
	private Person payerPerson;
	
	/** The insured person. */
	private Person insuredPerson;
	
	/** The appointee agreement Documents. */
	private AgreementDocument agreementDocument;
	
	/** The appointee agreement Documents. */
    public Set<Document> appionteeDocuments;
    
    public Coverage basicCoverage;
    
	/* (non-Javadoc)
	 * @see net.sf.jasperreports.engine.JRDefaultScriptlet#beforeDetailEval()
	 */
	public void beforeDetailEval() throws JRScriptletException {
		agreement =  (InsuranceAgreement) this.getFieldValue("proposal");
		payer = (PremiumPayer) this.getFieldValue("payer");
		insured = (Insured) this.getFieldValue("insured");		
		
		agreementDocument = (AgreementDocument) this.getFieldValue("document");
		if (null != agreementDocument) {
            appionteeDocuments = agreementDocument.getPages();
        }
		payerPerson = (Person) payer.getPlayerParty();
		insuredPerson = (Person) insured.getPlayerParty();
		for (Coverage includesCoverage : agreement.getIncludesCoverage()) {
			
			if(includesCoverage.getCoverageCode()==CoverageCodeList.BasicCoverage){
				basicCoverage=includesCoverage;
				break;
			}
		}
		
		Set<IllustrationResponsePayLoad> respSet= agreement.getIllustrationResponse();
		String json = null;
		for (IllustrationResponsePayLoad illustrationResponsePayLoad : respSet){
			json = illustrationResponsePayLoad.getResponseJson();
		}
		JSONParser jsonParser = new JSONParser();
		try {
			Object obj = jsonParser.parse(json);
			jsonObject = (JSONObject) obj;
			System.out.println(jsonObject.get("PayorDetails"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Gets the ph name.
	 *
	 * @return the PH name
	 * @throws JRScriptletException the JR scriptlet exception
	 */
	public String getPHName() throws JRScriptletException {
		String name = null;
		for(PersonName pName:payerPerson.getName()){
			name = pName.getGivenName();
		}
		
		return name;
	}
	
	/**
	 * Gets the ph age.
	 *
	 * @return the PH age
	 * @throws JRScriptletException the JR scriptlet exception
	 */
	public String getPHAge() throws JRScriptletException {
		Calendar dob = Calendar.getInstance();
		dob.setTime(payerPerson.getBirthDate());
		Calendar today = Calendar.getInstance();
		Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
		age--;
		
		return age.toString();
	}
	
	/**
	 * Gets the ph sex.
	 *
	 * @return the PH sex
	 * @throws JRScriptletException the JR scriptlet exception
	 */
	public String getPHSex() throws JRScriptletException {
		return payerPerson.getGenderCode().toString();
	}
	
	/**
	 * Gets the in name.
	 *
	 * @return the IN name
	 * @throws JRScriptletException the JR scriptlet exception
	 */
	public String getINName() throws JRScriptletException {
		String name = null;
		for(PersonName pName:insuredPerson.getName()){
			name = pName.getGivenName();
		}
		
		return name;
	}
	
	/**
	 * Gets the in age.
	 *
	 * @return the IN age
	 * @throws JRScriptletException the JR scriptlet exception
	 */
	public String getINAge() throws JRScriptletException {
		Calendar dob = Calendar.getInstance();
		dob.setTime(insuredPerson.getBirthDate());
		Calendar today = Calendar.getInstance();
		Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
		age--;
		
		return age.toString();
	}
	
	/**
	 * Gets the in sex.
	 *
	 * @return the IN sex
	 * @throws JRScriptletException the JR scriptlet exception
	 */
	public String getINSex() throws JRScriptletException {
		return insuredPerson.getGenderCode().toString();
	}
	
	/**
	 * Gets the guaranteed mat sa.
	 *
	 * @return the guaranteed mat sa
	 *//*
	public String getGuaranteedMatSA(){
		return round((Double) ((JSONObject)jsonObject.get("quotesTable")).get("guaranteedMaturityValue"),0);
		
	}
	
	*//**
	 * Gets the ppt.
	 *
	 * @return the ppt
	 *//*
	public String getPPT(){
		return ((JSONObject)jsonObject.get("PolicyDetails"))!=null?((JSONObject)jsonObject.get("PolicyDetails")).get("premiumPayingTerm").toString():"-NA-";
		
	}
	
	*//**
	 * Gets the bonus opt.
	 *
	 * @return the bonus opt
	 *//*
	public String getBonusOpt(){
		return ((JSONObject)jsonObject.get("PolicyDetails"))!=null?((JSONObject)jsonObject.get("PolicyDetails")).get("bonusOption").toString():"-NA-";
		
	}
	
	*//**
	 * Gets the payment mode.
	 *
	 * @return the payment mode
	 *//*
	public String getPaymentMode(){
		return ((JSONObject)jsonObject.get("PolicyDetails"))!=null?((JSONObject)jsonObject.get("PolicyDetails")).get("premiumMode").toString():"-NA-";
	}
	
	*//**
	 * Gets the state.
	 *
	 * @return the state
	 *//*
	public String getState(){
		return ((JSONObject)jsonObject.get("InsuredDetails"))!=null?((JSONObject)jsonObject.get("InsuredDetails")).get("state").toString():"-NA-";
	}
	
	*//**
	 * Gets the first year basic modal premium.
	 *
	 * @return the first year basic modal premium
	 *//*
	public String getFirstYearBasicModalPremium(){
		return (jsonObject.get("firstYearBasicModalPremium")!=null?round(new Double(jsonObject.get("firstYearBasicModalPremium").toString()),0).toString():"-NA-");
	}
	
	*//**
	 * Gets the service tax_ b p_ rp.
	 *
	 * @return the service tax_ b p_ rp
	 *//*
	public String getServiceTax_BP_RP(){
		return (jsonObject.get("serviceTax_BP_RP")!=null?round(new Double(jsonObject.get("serviceTax_BP_RP").toString()),0).toString():"-NA-");
	}
	
	*//**
	 * Gets the total modal premium inclusive s tand cess.
	 *
	 * @return the total modal premium inclusive s tand cess
	 *//*
	public String getTotalModalPremiumInclusiveSTandCess(){
		return (jsonObject.get("totalModalPremiumInclusiveSTandCess")!=null?round(new Double(jsonObject.get("totalModalPremiumInclusiveSTandCess").toString()),0).toString():"-NA-");
	}
	
	*//**
	 * Gets the total annual premium exclusive s tand cess.
	 *
	 * @return the total annual premium exclusive s tand cess
	 *//*
	public String getTotalAnnualPremiumExclusiveSTandCess(){
		return (jsonObject.get("totalAnnualPremiumExclusiveSTandCess")!=null?round(new Double(jsonObject.get("totalAnnualPremiumExclusiveSTandCess").toString()),0).toString():"-NA-");

		
	}
	
	*//**
	 * Gets the renewal basic modal premium.
	 *
	 * @return the renewal basic modal premium
	 *//*
	public String getRenewalBasicModalPremium(){
		return (jsonObject.get("renewalBasicModalPremium")!=null?round(new Double(jsonObject.get("renewalBasicModalPremium").toString()),0).toString():"-NA-");

		
	}
	
	*//**
	 * Gets the s t_ bp renewal and rider premium renewal.
	 *
	 * @return the s t_ bp renewal and rider premium renewal
	 *//*
	public String getST_BPRenewalAndRiderPremiumRenewal(){
		return (jsonObject.get("ST_BPRenewalAndRiderPremiumRenewal")!=null?round(new Double(jsonObject.get("ST_BPRenewalAndRiderPremiumRenewal").toString()),0).toString():"-NA-");
	}
	
	
	*//**
	 * Gets the total modal premium inclusive st renewal and cess.
	 *
	 * @return the total modal premium inclusive st renewal and cess
	 *//*
	public String getTotalModalPremiumInclusiveSTRenewalAndCess(){
		return (jsonObject.get("totalModalPremiumInclusiveSTRenewalAndCess")!=null?round(new Double(jsonObject.get("totalModalPremiumInclusiveSTRenewalAndCess").toString()),0).toString():"-NA-");
	}
	
	
	*//**
	 * Gets the total annual premium exclusive st renewal and cess.
	 *
	 * @return the total annual premium exclusive st renewal and cess
	 *//*
	public String getTotalAnnualPremiumExclusiveSTRenewalAndCess(){
		return (jsonObject.get("totalAnnualPremiumExclusiveSTRenewalAndCess")!=null?round(new Double(jsonObject.get("totalAnnualPremiumExclusiveSTRenewalAndCess").toString()),0).toString():"-NA-");

	}
	
	*//**
	 * Gets the total annual premium inclusive rider premium and st renewal.
	 *
	 * @return the total annual premium inclusive rider premium and st renewal
	 *//*
	public String getTotalAnnualPremiumInclusiveRiderPremiumAndSTRenewal(){
		return (jsonObject.get("totalAnnualPremiumInclusiveRiderPremiumAndSTRenewal")!=null?round(new Double(jsonObject.get("totalAnnualPremiumInclusiveRiderPremiumAndSTRenewal").toString()),0).toString():"-NA-");
	}
	
	
	*//**
	 * Gets the product name.
	 *
	 * @return the product name
	 * @throws JRScriptletException the jR scriptlet exception
	 *//*
	public String getProductName() throws JRScriptletException {
		return basicCoverage!=null?basicCoverage.getMarketingName():"";
	}
	
	*//**
	 * Gets the total annual premium inclusive rider premium and st.
	 *
	 * @return the total annual premium inclusive rider premium and st
	 *//*
	public String getTotalAnnualPremiumInclusiveRiderPremiumAndST(){
		return (jsonObject.get("totalAnnualPremiumInclusiveRiderPremiumAndST")!=null?round(new Double(jsonObject.get("totalAnnualPremiumInclusiveRiderPremiumAndST").toString()),0).toString():"-NA-");
	}
	
	public String getESignature() {
		String sign = "";
        String temptoken = null;
        Document documentDetail = null;
        System.out.println("appionteeDocuments ---- " + appionteeDocuments);
        // appointee signature detail
        if (appionteeDocuments != null) {
            for (Document agreementDocument : appionteeDocuments) {
                System.out.println("In Loop---->" + agreementDocument.getTypeCode()+"  " + agreementDocument.getBase64string() + " " + agreementDocument.getFileNames());
                if (agreementDocument != null) {
                    documentDetail = agreementDocument;
                    System.out.println("Type Code is Signature ---" + documentDetail.getBase64string());
                    sign = documentDetail.getBase64string();
                    StringTokenizer token = new StringTokenizer(sign, ",");
                    while (token.hasMoreTokens()) {
                        temptoken = token.nextToken();
                    }
                    if (temptoken != null) {
                        sign = temptoken;
                    }
                }
            }
        }
        System.out.println("Signature for PDF is " + sign);
        return sign;    
    }
	
	
	public List<IllustrationTableData> getIllustrationDataFour(){
		List<IllustrationTableData> illustrationTableDatas = new ArrayList<IllustrationTableData>();
		JSONArray jsonArray = (JSONArray) jsonObject.get("illustrationTableData4");
		illustrationTableDatas  = getIllusData(jsonArray);
		return illustrationTableDatas;
		
	}
	
	
	public List<IllustrationTableData> getIllustrationDataEight(){
		List<IllustrationTableData> illustrationTableDatas = new ArrayList<IllustrationTableData>();
		JSONArray jsonArray = (JSONArray) jsonObject.get("illustrationTableData8");
		illustrationTableDatas = getIllusData(jsonArray);
		return illustrationTableDatas;
		
	}
	
	
	public List<IllustrationTableData> getIllustrationDataSimple(){
		List<IllustrationTableData> illustrationTableDatas = new ArrayList<IllustrationTableData>();
		JSONArray jsonArray = (JSONArray) jsonObject.get("illustrationTableData");
		illustrationTableDatas = getIllusData(jsonArray);
		return illustrationTableDatas;
		
	}
	
	
	
	*//**
	 * Gets the illus data.
	 *
	 * @return the illus data
	 *//*
	public List<IllustrationTableData> getIllusData(JSONArray jsonArray){
		List<IllustrationTableData> illustrationTableDatas = new ArrayList<IllustrationTableData>();
		
		Map<String,String> tableHeaders = new HashMap<String, String>();
		
		tableHeaders.put("policyYear", "Policy Year");
		tableHeaders.put("ageOfLifeAssured", "Age");
		tableHeaders.put("annualPremium", "Annual Premium");
		tableHeaders.put("premiumAllocationCharge", "Premium Allocation Charge");
		tableHeaders.put("allocatedPremium", "Amount Available for Investment(out of premium)");
		tableHeaders.put("policyAdminCharge_4", "Policy Administration Charge");
		tableHeaders.put("riskCharge_4", "Risk Charges");
		tableHeaders.put("fundMgmtCharge_4", "Fund Management Charge");
		tableHeaders.put("otherCharge_4", "Other Charges");
		tableHeaders.put("totalCharges_4", "Total Charge");
		tableHeaders.put("serviceTax_4", "Service Tax");
		tableHeaders.put("fundValuebeforeFMC_4", "Fund Value Before FMC");
		tableHeaders.put("fundValueEOY_4", "Fund Value (EOY)");
		tableHeaders.put("partialWithdrawalEOY_4", "Partial Withdrawal EOY");
		tableHeaders.put("loyaltyAdditions_4", "Loyalty Additions to fund (if any)");
		tableHeaders.put("surrenderValueEOY_4", "Surrender Value (EOY)");
		tableHeaders.put("deathBenefitEOY_4", "Death Benefit (EOY)");
		tableHeaders.put("maturityBenefit_4", "Vesting Benefit");
		tableHeaders.put("commissions_4", "Commissions/Brokerage if payable");
		
		//FYPP
		tableHeaders.put("guaranteeCharge_4", "Guarantee Charge");
		tableHeaders.put("topupPremium", "Top Up Premium");
		tableHeaders.put("guaranteedVestingBenefit_4", "Guaranteed Vesting Benefit");		
		tableHeaders.put("riderCharge_4", "Rider Charges");
		
		//WLPS
		tableHeaders.put("terminalBonus_8", "Terminal Bonus");
		tableHeaders.put("deathBenefit_8", "Death Benefit");
		tableHeaders.put("maturityBenefit_8", "Vesting Benefit");
		tableHeaders.put("cashBonus_8", "Cash Bonus");
		tableHeaders.put("otherCharge_8", "Other Charges");
		tableHeaders.put("partialWithdrawalEOY_8", "Partial Withdrawal EOY");
		tableHeaders.put("surrenderValuePUA_4", "Surrender Value PUA");
		tableHeaders.put("cashBonus_4", "Cash Bonus");
		tableHeaders.put("deathBenefit_4", "Death Benefit");
		tableHeaders.put("terminalBonus_4", "Terminal Bonus");
		tableHeaders.put("guaranteed_Surr_Value", "Guaranteed Surrender Value");
		tableHeaders.put("survival_maturityBenefit", "Survival Maturity Benefit");
		tableHeaders.put("deathBenefitEOY_8", "Death Benefit (EOY)");
		tableHeaders.put("netOutlay_8", "Net Outlay");
		tableHeaders.put("surrenderValuePUA_8", "Surrender Value PUA");
		tableHeaders.put("netOutlay_4", "Net Outlay");
		tableHeaders.put("specialSurrenderValue_8", "Special Surrender Value");
		tableHeaders.put("deathBenefit", "Death Benefit");
		tableHeaders.put("specialSurrenderValue_4", "Special Surrender Value");
		
		tableHeaders.put("policyAdminCharge_8", "Policy Administration Charge");
		tableHeaders.put("fundValueEOY_8", "Fund Value (EOY)");
		tableHeaders.put("surrenderValueEOY_8", "Surrender Value (EOY)");
		tableHeaders.put("commissions_8", "Commissions/Brokerage if payable");
		tableHeaders.put("riskCharge_8", "Risk Charges");
		tableHeaders.put("deathBenefitEOY", "Death Benefit (EOY)");
		tableHeaders.put("loyaltyAdditions_8", "Loyalty Additions to fund (if any)");
		tableHeaders.put("totalCharges_8", "Total Charge");
		tableHeaders.put("serviceTax_8", "Service Tax");
		tableHeaders.put("fundValuebeforeFMC_8", "Fund Value Before FMC");
		tableHeaders.put("fundMgmtCharge_8", "Fund Management Charge");
		tableHeaders.put("guaranteeCharge_8", "Guarantee Charge");
		tableHeaders.put("guaranteedVestingBenefit_8", "Guaranteed Vesting Benefit");
		tableHeaders.put("riderCharge_8", "Rider Charges");
		
		
		for (Object obj : jsonArray){
			IllustrationTableData illustrationTableData = new  IllustrationTableData();
			List<IllusCode> illusCodes = new ArrayList<IllusCode>();
			JSONObject jsonObj = (JSONObject) obj;
			Set<String> jsonKeys= jsonObj.keySet();
			for(String jsonKey : jsonKeys){
				IllusCode illusCode = new IllusCode();
				String output = "";
				if(tableHeaders.containsKey(jsonKey)){
					output = tableHeaders.get(jsonKey);
				}
				else{
					output = jsonKey.replaceAll("(\\p{Ll})(\\p{Lu})","$1 $2").toUpperCase();
				}
				illusCode.setKey(output);
				illusCode.setValue((jsonObj.get(jsonKey)!=null?round(new Double(jsonObj.get(jsonKey).toString()),0).toString():""));
				if(illusCode.getKey().equals("Policy Year")){
					IllusCode firstEl = illusCodes.get(0);
					illusCodes.set(0, illusCode);
					illusCodes.add(firstEl);
				}else if(illusCode.getKey().equals("Age")){
					IllusCode firstEl = illusCodes.get(1);
					illusCodes.set(1, illusCode);
					illusCodes.add(firstEl);
				}else if(illusCode.getKey().equals("Annual Premium")){
					IllusCode firstEl = illusCodes.get(2);
					illusCodes.set(2, illusCode);
					illusCodes.add(firstEl);
				}else if(illusCode.getKey().equals("Premium Allocation Charge")){
					IllusCode firstEl = illusCodes.get(3);
					illusCodes.set(3, illusCode);
					illusCodes.add(firstEl);
				}else if(illusCode.getKey().equals("Amount Available for Investment(out of premium)")){
					IllusCode firstEl = illusCodes.get(4);
					illusCodes.set(4, illusCode);
					illusCodes.add(firstEl);
				}				
				else{
				illusCodes.add(illusCode);
				}
			}
			illustrationTableData.setKeys(illusCodes);
			illustrationTableDatas.add(illustrationTableData);
		}

		return illustrationTableDatas;
	}
	
	
	
	*//**
	 * Gets the graph.
	 *
	 * @return the graph
	 *//*
	public List<BarChart> getGraph(){
		JSONArray jsonArray = null;
		if(basicCoverage.getProductCode().getCode().equals("24")){
			jsonArray = (JSONArray) jsonObject.get("illustrationTableData8");
		}else if(basicCoverage.getProductCode().getCode().equals("36")) {
			jsonArray = (JSONArray) jsonObject.get("illustrationTableData4");
		}else{
			jsonArray = (JSONArray) jsonObject.get("illustrationTableData");
		}
		List<BarChart> graphs = new ArrayList<BarChart>();
		for (Object obj : jsonArray){
			JSONObject jsonObj = (JSONObject) obj;
			Set<String> jsonKeys= jsonObj.keySet();
			BarChart barChart = new BarChart();
			for(String jsonKey : jsonKeys){
				if(jsonKey.equals("policyYear")){
					barChart.setPolicyYear(((Long) jsonObj.get(jsonKey)));
				}else if(jsonKey.equals("deathBenefitEOY_4")){
					Object jsonObject =jsonObj.get(jsonKey);
					if(jsonObject instanceof Long){
						barChart.setDeathBenefit((Long)jsonObj.get(jsonKey));
					}else if(jsonObject instanceof Double){
						barChart.setDeathBenefit(Long.parseLong(round((Double)jsonObj.get(jsonKey),0)));
					}
					
					
					
				}
			}
			graphs.add(barChart);
		}
		
		return graphs;
	}
	*/
	
	/*
	/**
	 * Gets the committed premium.
	 *
	 * @return the committed premium
	 *//*
	public String getCommittedPremium(){
		return null!=basicCoverage?(null!=basicCoverage.getCommittedPremium()? basicCoverage.getCommittedPremium().toString():""):"";
	}
	
	
	*//**
	 * Gets the annual premium.
	 *
	 * @return the annual premium
	 *//*
	public String getAnnualPremium(){
		return (jsonObject.get("annualPremium")!=null?round(new Double(jsonObject.get("annualPremium").toString()),0).toString():"-NA-");
	}
	
	*//**
	 * Gets the cumulative premium.
	 *
	 * @return the cumulative premium
	 *//*
	public String getCumulativePremium(){
		return (jsonObject.get("modalPremium")!=null?round(new Double(jsonObject.get("modalPremium").toString()),0).toString():"-NA-");
	}
	
	*//**
	 * Gets the cumulative annual premium.
	 *
	 * @return the cumulative annual premium
	 *//*
	public String getCumulativeAnnualPremium(){
		return (jsonObject.get("cumulativeAnnualPremium")!=null?round(new Double(jsonObject.get("cumulativeAnnualPremium").toString()),0).toString():"-NA-");
	}
	
	*//**
	 * Gets the vesting age.
	 *
	 * @return the vesting age
	 *//*
	public String getVestingAge(){
		return null!=basicCoverage?(null!=basicCoverage.getVestingAge()? basicCoverage.getVestingAge().toString():""):"";
	}
	
	*//**
	 * Gets the modal premium.
	 *
	 * @return the modal premium
	 *//*
	public String getModalPremium(){
		return (jsonObject.get("modalPremium")!=null?round(new Double(jsonObject.get("modalPremium").toString()),0).toString():"-NA-");
	}
	
	*//**
	 * Gets the fY premium.
	 *
	 * @return the fY premium
	 *//*
	public String getFYPremium(){
		return (jsonObject.get("firstYearBasicAnnualPremium")!=null?round(new Double(jsonObject.get("firstYearBasicAnnualPremium").toString()),0).toString():"-NA-");
	}
	
	*//**
	 * Gets the rN premium.
	 *
	 * @return the rN premium
	 *//*
	public String getRNPremium(){
		return (jsonObject.get("renewalBasicAnnualPremium")!=null?round(new Double(jsonObject.get("renewalBasicAnnualPremium").toString()),0).toString():"-NA-");
	}
	
	
	

	*//**
	 * Gets the fY rider premium.
	 *
	 * @return the fY rider premium
	 *//*
	public String getFYRiderPremium(){
		return (jsonObject.get("firstYearPayorRiderPremiumSummary")!=null?round(new Double(jsonObject.get("firstYearPayorRiderPremiumSummary").toString()),0).toString():"-NA-");
	}
	

	*//**
	 * Gets the rN rider premium.
	 *
	 * @return the rN rider premium
	 *//*
	public String getRNRiderPremium(){
		return (jsonObject.get("renewalPayorRiderPremiumSummary")!=null?round(new Double(jsonObject.get("renewalPayorRiderPremiumSummary").toString()),0).toString():"-NA-");
	}
	
	
	
	*//**
	 * Gets the face amt.
	 *
	 * @return the face amt
	 *//*
	public String getFaceAmt(){
		return null!=agreement.getSumAssured()? agreement.getSumAssured().getAmount().toString():"";
	}
	
	*//**
	 * Gets the bonus opt wlps.
	 *
	 * @return the bonus opt wlps
	 *//*
	public String getBonusOptWLPS(){
		return null!=basicCoverage?(null!=basicCoverage.getBonusOption()? basicCoverage.getBonusOption():""):"";	
		
	}
	
	*//**
	 * Gets the prem type.
	 *
	 * @return the prem type
	 *//*
	public String getPremType(){
		return null!=basicCoverage?(null!=basicCoverage.getPremiumType()? basicCoverage.getPremiumType():""):"";	
	}
	
	
	*//**
	 * Gets the premium paying term.
	 *
	 * @return the premium paying term
	 *//*
	public String getPremiumPayingTerm(){
		return null!=basicCoverage?(null!=basicCoverage.getPremiumPayingTerm()? basicCoverage.getPremiumPayingTerm().toString():""):"";	

	}
	
	*//**
	 * Gets the policy term wlps.
	 *
	 * @return the policy term wlps
	 *//*
	public String getPolicyTermWLPS(){
		return null!=basicCoverage?(null!=basicCoverage.getPolicyTerm()? basicCoverage.getPolicyTerm().toString():""):"";	

	}
	
	*//**
	 * Gets the sPS age.
	 *
	 * @return the sPS age
	 *//*
	public String getSPSAge(){
		return ((JSONObject)jsonObject.get("InsuredDetails"))!=null?((JSONObject)jsonObject.get("InsuredDetails")).get("age").toString():"-NA-";
	}
	
	
	
	*//**
	 * Round.
	 *
	 * @param value the value
	 * @param places the places
	 * @return the string
	 *//*
	public static String round(double value, int places) {
	
		DecimalFormat df = new DecimalFormat("#");
		df.setRoundingMode(RoundingMode.DOWN);
		 String s = df.format(value);
		 
		 return s;
	}
	
	*//**
	 * First index of ucl.
	 *
	 * @param str the str
	 * @return the int
	 *//*
	public int firstIndexOfUCL(String str) {        
	    for(int i=0; i<str.length(); i++) {
	        if(Character.isUpperCase(str.charAt(i))) {
	            return i;
	        }
	    }
	    return -1;
	}*/
	
	public String getRegularPremium(){
		//return ((JSONObject)jsonObject.get("PolicyDetails"))!=null?((JSONObject)jsonObject.get("PolicyDetails")).get("regularBasicPremium").toString():"-NA-";
		return "780000";
	}
	
	public String getRegularTopUp(){
		//return ((JSONObject)jsonObject.get("PolicyDetails"))!=null?((JSONObject)jsonObject.get("PolicyDetails")).get("regularTopUp").toString():"-NA-";
		return "420000";
	}
	
	public String getPremFrequency(){
		return null!=basicCoverage?(null!=basicCoverage.getPremiumFrequency()? basicCoverage.getPremiumFrequency().name():""):"";
		
	}
	
	public String getSumAssured(){
		//return ((String)jsonObject.get("sumAssured"))!=null?((String)jsonObject.get("sumAssured")):"-NA-";
		return "2500000";
	}
	
	public String getCoverageTerm(){
		//return ((String)jsonObject.get("sumAssured"))!=null?((String)jsonObject.get("sumAssured")):"-NA-";
		return "-NA-";
	}
	
	public String getCurrency(){
		//return ((String)jsonObject.get("sumAssured"))!=null?((String)jsonObject.get("sumAssured")):"-NA-";
		return "-NA-";
	}
	
	public String getEquityFund(){
		//return ((JSONObject)jsonObject.get("Funds"))!=null?((JSONObject)jsonObject.get("Funds")).get("equity").toString():"-NA-";
		return "35%";
	}
	
	public String getFixedIncomeFund(){
		//return ((JSONObject)jsonObject.get("Funds"))!=null?((JSONObject)jsonObject.get("Funds")).get("fixedIncome").toString():"-NA-";
		return "30%";
	}
	
	public String getMoneyMarketFund(){
		//return ((JSONObject)jsonObject.get("Funds"))!=null?((JSONObject)jsonObject.get("Funds")).get("moneyMarket").toString():"-NA-";
		return "35%";
	}
	
	// Rider tables
	
	public String getPolicyTerm(){
		//return ((String)jsonObject.get("policyTerm"))!=null?((String)jsonObject.get("policyTerm")):"-NA-";
		return "72";
	}
	
	public String getGemilangMaxCoverAge(){
		//return ((String)jsonObject.get("gemilangMaxCoverAge"))!=null?((String)jsonObject.get("gemilangMaxCoverAge")):"-NA-";
		return "99";
	}
	
	public String getGemilangMonthlyPremium(){
		//return ((String)jsonObject.get("monthlyPremium"))!=null?((String)jsonObject.get("monthlyPremium")):"-NA-";
		return "100000";
	}
	
	public String getGemilangTotalPremium(){
		//return ((String)jsonObject.get("totalPremium"))!=null?((String)jsonObject.get("totalPremium")):"-NA-";
		return "200000";
	}
	
	public String getAdbMaxCoverAge(){
		//return ((String)jsonObject.get("adbMaxCoverAge"))!=null?((String)jsonObject.get("adbMaxCoverAge")):"-NA-";
		return "70";
	}
	
	public String getAdbSumAssured(){
		//return ((String)jsonObject.get("adbSumAssured"))!=null?((String)jsonObject.get("adbSumAssured")):"-NA-";
		return "65000000";
	}
	
	public String getAdbMonthlyPremium(){
		//return ((String)jsonObject.get("adbMonthlyPremium"))!=null?((String)jsonObject.get("adbMonthlyPremium")):"-NA-";
		return "300000";
	}
	
	public String getCiAccMaxCoverAge(){
		//return ((String)jsonObject.get("ciAccMaxCoverAge"))!=null?((String)jsonObject.get("ciAccMaxCoverAge")):"-NA-";
		return "70";
	}
	
	public String getCiAccSumAssured(){
		//return ((String)jsonObject.get("ciAccSumAssured"))!=null?((String)jsonObject.get("ciAccSumAssured")):"-NA-";
		return "60000000";
	}
	
	public String getCiAccMonthlyPremium(){
		//return ((String)jsonObject.get("ciAccMonthlyPremium"))!=null?((String)jsonObject.get("ciAccMonthlyPremium")):"-NA-";
		return "400000";
	}
	
	public Integer getInsuredAge() throws JRScriptletException {
		Calendar dob = Calendar.getInstance();
		dob.setTime(insuredPerson.getBirthDate());
		Calendar today = Calendar.getInstance();
		Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
		age--;
		
		return age;
	}
	
	public String getSumInsuredLabel() throws JRScriptletException{
		
		Integer insuredAge = getInsuredAge();
		if(insuredAge >= 18){
			return "Usia >= 18 (delapan belas) tahun";
		}
		else{
			return "Usia < 18 (delapan belas) tahun";
		}
		
	}
	
	public String getCriticalIllness() throws JRScriptletException{
		
		Integer insuredAge = getInsuredAge();
		if(insuredAge >= 18){
			return ((JSONObject)jsonObject.get("constantMaxSumInsuredTable1"))!=null?((JSONObject)jsonObject.get("constantMaxSumInsuredTable1")).get("sumInsured").toString():"-NA-";
		}
		else{
			return ((JSONObject)jsonObject.get("constantMaxSumInsuredTable2"))!=null?((JSONObject)jsonObject.get("constantMaxSumInsuredTable2")).get("sumInsured").toString():"-NA-";
		}
	}
	
	public String getDeathByAccident() throws JRScriptletException{
		
		Integer insuredAge = getInsuredAge();
		if(insuredAge >= 18){
			return ((JSONObject)jsonObject.get("constantMaxSumInsuredTable1"))!=null?((JSONObject)jsonObject.get("constantMaxSumInsuredTable1")).get("maxSumInsured").toString():"-NA-";
		}
		else{
			return ((JSONObject)jsonObject.get("constantMaxSumInsuredTable2"))!=null?((JSONObject)jsonObject.get("constantMaxSumInsuredTable2")).get("maxSumInsured").toString():"-NA-";
		}
	}
	
	public Long checkForKey(JSONObject obj, String key) {
		Long value = null;
		if (obj.containsKey(key)) {
			Object jsonValue = obj.get(key);
			if (jsonValue instanceof Double) {
				value = ((Double) jsonValue).longValue();
			}else if (jsonValue instanceof String){
				value = Long.parseLong((String)jsonValue);
			}
			else  {
				value = (Long) jsonValue;
			} 
		}
		return value;

	}
	
	public List<IllusDataGenli> getIllusDataGli() {
		JSONArray jsonArray = null;
		List<IllusDataGenli> illusData = new ArrayList<IllusDataGenli>();
		jsonArray = (JSONArray) jsonObject.get("illustrationTableData");
		for (Object obj : jsonArray) {
			JSONObject jsonObj = (JSONObject) obj;
			if (jsonObj.get("policyYear") != null) {
				IllusDataGenli illustrationTableData = new IllusDataGenli();
				illustrationTableData.setPolicyYear(checkForKey(jsonObj,
						"policyYear").intValue());
				illustrationTableData.setAgeOfLifeAssured(checkForKey(jsonObj,
						"ageOfLifeAssured").intValue());
				illustrationTableData.setAnnualPremium(checkForKey(jsonObj,
						"annualPremium"));
				illustrationTableData.setTopUp(checkForKey(jsonObj, "topUp"));
				illustrationTableData.setWithdrawalAmount(checkForKey(jsonObj,
						"withdrawalAmount"));
				illustrationTableData.setPolicyValueLow(checkForKey(jsonObj,
						"policyValueLow"));
				illustrationTableData.setPolicyValueMed(checkForKey(jsonObj,
						"policyValueMed"));
				illustrationTableData.setPolicyValueHigh(checkForKey(jsonObj,
						"policyValueHigh"));
				illustrationTableData.setStringdeathBenefit(checkForKey(
						jsonObj, "deathBenefit"));
				illusData.add(illustrationTableData);
			}
		}
		return illusData;
	}
	
	public List<IllusFundDataGli> getIllusFundDataGli() {
		JSONArray jsonArray = null;
		List<IllusFundDataGli> illusData = new ArrayList<IllusFundDataGli>();
		jsonArray = (JSONArray) jsonObject.get("illustrationTableFundData");
		for (Object obj : jsonArray) {
			JSONObject jsonObj = (JSONObject) obj;
			if (jsonObj.get("policyYear") != null) {
				IllusFundDataGli illustrationTableData = new IllusFundDataGli();
				illustrationTableData.setPolicyYear(checkForKey(jsonObj,
						"policyYear").intValue());
				illustrationTableData.setAgeOfLifeAssured(checkForKey(jsonObj,
						"ageOfLifeAssured").intValue());
				illustrationTableData.setMoneyMarketLow(checkForKey(jsonObj,
						"moneyMarketLow"));
				illustrationTableData.setMoneyMarketMed(checkForKey(jsonObj,
						"moneyMarketMed"));
				illustrationTableData.setMoneyMarketHigh(checkForKey(jsonObj,
						"moneyMarketHigh"));
				illustrationTableData.setEquityMarketLow(checkForKey(jsonObj,
						"equityMarketLow"));
				illustrationTableData.setEquityMarketMed(checkForKey(jsonObj,
						"equityMarketMed"));
				illustrationTableData.setEquityMarketHigh(checkForKey(jsonObj,
						"equityMarketHigh"));
				illustrationTableData.setFixedMarketLow(checkForKey(jsonObj,
						"fixedMarketLow"));
				illustrationTableData.setFixedMarketMed(checkForKey(jsonObj,
						"fixedMarketMed"));
				illustrationTableData.setFixedMarketHigh(checkForKey(jsonObj,
						"fixedMarketHigh"));
				illustrationTableData.setDeathBenefit(checkForKey(jsonObj,
						"deathBenefit"));
				illusData.add(illustrationTableData);
			}
		}
		return illusData;
	}
	
	public List<IllusDataGenli> getIllusDataChartGli() {
		JSONArray jsonArray = null;
		List<IllusDataGenli> illusData = new ArrayList<IllusDataGenli>();
		jsonArray = (JSONArray) jsonObject.get("illustrationTableData");
		for(int i=1; i<=20; i++){
			for (Object obj : jsonArray) {
				JSONObject jsonObj = (JSONObject) obj;
				if (jsonObj.get("policyYear") != null) {
					if(i == checkForKey(jsonObj,"policyYear").intValue()){
						IllusDataGenli illustrationTableData = new IllusDataGenli();
						illustrationTableData.setPolicyYear(checkForKey(jsonObj,
								"policyYear").intValue());
						illustrationTableData.setPolicyValueLow(checkForKey(jsonObj,
								"policyValueLow"));
						illustrationTableData.setPolicyValueMed(checkForKey(jsonObj,
								"policyValueMed"));
						illustrationTableData.setPolicyValueHigh(checkForKey(jsonObj,
								"policyValueHigh"));
						illusData.add(illustrationTableData);
					}
				}
			}
		}
		return illusData;
	}
	
	public String getPercentageSumIns1(){
		return ((JSONObject)jsonObject.get("constantSumInsuredPercentageTable"))!=null?((JSONObject)jsonObject.get("constantSumInsuredPercentageTable")).get("fistSumInsured").toString()+ "%":"-NA-";
	}
	
	public String getPercentageSumIns2(){
		return ((JSONObject)jsonObject.get("constantSumInsuredPercentageTable"))!=null?((JSONObject)jsonObject.get("constantSumInsuredPercentageTable")).get("secondSumInsured").toString()+ "%":"-NA-";
	}
	
	public String getPercentageSumIns3(){
		return ((JSONObject)jsonObject.get("constantSumInsuredPercentageTable"))!=null?((JSONObject)jsonObject.get("constantSumInsuredPercentageTable")).get("thirdSumInsured").toString()+ "%":"-NA-";
	}
	
	public String getPercentageSumIns4(){
		return ((JSONObject)jsonObject.get("constantSumInsuredPercentageTable"))!=null?((JSONObject)jsonObject.get("constantSumInsuredPercentageTable")).get("fourthSumInsured").toString()+ "%":"-NA-";
	}
	
	public String getPercentageSumIns5(){
		return ((JSONObject)jsonObject.get("constantSumInsuredPercentageTable"))!=null?((JSONObject)jsonObject.get("constantSumInsuredPercentageTable")).get("fifthSumInsured").toString()+ "%":"-NA-";
	}

	public List<IllusAllocationData> getRegularAllocation(){
		List<IllusAllocationData> illusAllocationData = new ArrayList<IllusAllocationData>();
			JSONObject jsonObj = (JSONObject) jsonObject.get("constantRegularTopUpAllocationTable");;
			if (jsonObj.get("basicPremiumInveAllocYear1") != null) {
				
				IllusAllocationData illusData = new IllusAllocationData();
				illusData.setBasicPremiumInveAllocYear1(checkForKey(jsonObj,
				"basicPremiumInveAllocYear1").intValue()+ "%");
				illusData.setBasicPremiumAcquFeeYear1(checkForKey(jsonObj,
				"basicPremiumAcquFeeYear1").intValue()+ "%");
				illusData.setTopUpInveAllocYear1(checkForKey(jsonObj,
				"topUpInveAllocYear1").intValue()+ "%");
				illusData.setTopUpAcquFeeYear1(checkForKey(jsonObj,
				"topUpAcquFeeYear1").intValue()+ "%");
				
				illusData.setBasicPremiumInveAllocYear2(checkForKey(jsonObj,
				"basicPremiumInveAllocYear2").intValue()+ "%");
				illusData.setBasicPremiumAcquFeeYear2(checkForKey(jsonObj,
				"basicPremiumAcquFeeYear2").intValue()+ "%");
				illusData.setTopUpInveAllocYear2(checkForKey(jsonObj,
				"topUpInveAllocYear2").intValue()+ "%");
				illusData.setTopUpAcquFeeYear2(checkForKey(jsonObj,
				"topUpAcquFeeYear2").intValue()+ "%");
				
				illusData.setBasicPremiumInveAllocYear3(checkForKey(jsonObj,
				"basicPremiumInveAllocYear3").intValue()+ "%");
				illusData.setBasicPremiumAcquFeeYear3(checkForKey(jsonObj,
				"basicPremiumAcquFeeYear3").intValue()+ "%");
				illusData.setTopUpInveAllocYear3(checkForKey(jsonObj,
				"topUpInveAllocYear3").intValue()+ "%");
				illusData.setTopUpAcquFeeYear3(checkForKey(jsonObj,
				"topUpAcquFeeYear3").intValue()+ "%");
				
				illusData.setBasicPremiumInveAllocYear4(checkForKey(jsonObj,
				"basicPremiumInveAllocYear4").intValue()+ "%");
				illusData.setBasicPremiumAcquFeeYear4(checkForKey(jsonObj,
				"basicPremiumAcquFeeYear4").intValue()+ "%");
				illusData.setTopUpInveAllocYear4(checkForKey(jsonObj,
				"topUpInveAllocYear4").intValue()+ "%");
				illusData.setTopUpAcquFeeYear4(checkForKey(jsonObj,
				"topUpAcquFeeYear4").intValue()+ "%");
				
				illusData.setBasicPremiumInveAllocYear5(checkForKey(jsonObj,
				"basicPremiumInveAllocYear5").intValue()+ "%");
				illusData.setBasicPremiumAcquFeeYear5(checkForKey(jsonObj,
				"basicPremiumAcquFeeYear5").intValue()+ "%");
				illusData.setTopUpInveAllocYear5(checkForKey(jsonObj,
				"topUpInveAllocYear5").intValue()+ "%");
				illusData.setTopUpAcquFeeYear5(checkForKey(jsonObj,
				"topUpAcquFeeYear5").intValue()+ "%");
				
				illusData.setBasicPremiumInveAllocYear6(checkForKey(jsonObj,
				"basicPremiumInveAllocYear6").intValue()+ "%");
				illusData.setBasicPremiumAcquFeeYear6(checkForKey(jsonObj,
				"basicPremiumAcquFeeYear6").intValue()+ "%");
				illusData.setTopUpInveAllocYear6(checkForKey(jsonObj,
				"topUpInveAllocYear6").intValue()+ "%");
				illusData.setTopUpAcquFeeYear6(checkForKey(jsonObj,
				"topUpAcquFeeYear6").intValue()+ "%");
				
				illusAllocationData.add(illusData);
			}
		
		return illusAllocationData;
	}
	
	public String getPremAllocation(){
		return ((JSONObject)jsonObject.get("constantSingleTopUpAllocaionTable"))!=null?((JSONObject)jsonObject.get("constantSingleTopUpAllocaionTable")).get("singleTopUpInveAllocation").toString()+ "%":"-NA-";
	}
	
	public String getPremAcquisition(){
		return ((JSONObject)jsonObject.get("constantSingleTopUpAllocaionTable"))!=null?((JSONObject)jsonObject.get("constantSingleTopUpAllocaionTable")).get("singleTopUpAllocFee").toString()+ "%":"-NA-";
	}
	
	public String getESignature() {
		String sign = "";
        String temptoken = null;
        Document documentDetail = null;
        System.out.println("appionteeDocuments ---- " + appionteeDocuments);
        // appointee signature detail
        if (appionteeDocuments != null) {
            for (Document agreementDocument : appionteeDocuments) {
                System.out.println("In Loop---->" + agreementDocument.getTypeCode()+"  " + agreementDocument.getBase64string() + " " + agreementDocument.getFileNames());
                if (agreementDocument != null) {
                    documentDetail = agreementDocument;
                    System.out.println("Type Code is Signature ---" + documentDetail.getBase64string());
                    sign = documentDetail.getBase64string();
                    StringTokenizer token = new StringTokenizer(sign, ",");
                    while (token.hasMoreTokens()) {
                        temptoken = token.nextToken();
                    }
                    if (temptoken != null) {
                        sign = temptoken;
                    }
                }
            }
        }
        System.out.println("Signature for PDF is " + sign);
        return sign;    
    }
	
	public String getAgentId(){
		return agreement.getAgentId();
	}
	
	/*public List<IllusCode> getQstn(){
        //List<IllustrationTableData> illustrationTableDatas = new ArrayList<IllustrationTableData>();

              //IllustrationTableData illustrationTableData = new  IllustrationTableData();
              List<IllusCode> illusCodes = new ArrayList<IllusCode>();

                    IllusCode illusCode = new IllusCode();

                    illusCode.setId("1");
                    illusCode.setQuestion("Kapan pertama kali Anda di diagnosa hypertensi ? When was your first diagnosed with raised blood pressure?");
                    illusCode.setAnswer("July 1st week");
                    illusCodes.add(illusCode);

                    IllusCode illusCode1 = new IllusCode();

                    illusCode1.setId("2");
                    illusCode1.setQuestion("Berapa tekanan darah Anda saat itu ? / Do you know what your blood pressure was at that time?");
                    illusCode1.setAnswer("Yes");
                   illusCodes.add(illusCode1);

                    IllusCode illusCode3 = new IllusCode();

                    illusCode3.setId("3");
                    illusCode3.setQuestion("Apakah Anda mengetahui tekanan darah Anda saat ini / Do you know what your blood pressure was on this occasion?");
                    illusCode3.setAnswer("Yes");
                    illusCodes.add(illusCode3);

              //illustrationTableData.setKeys(illusCodes);
             // illustrationTableDatas.add(illustrationTableData);


        return illusCodes;
  }
*/
	
	public JSONObject getJsonObject() {
		return jsonObject;
	}


	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}


	public InsuranceAgreement getAgreement() {
		return agreement;
	}


	public void setAgreement(InsuranceAgreement agreement) {
		this.agreement = agreement;
	}


	public PremiumPayer getPayer() {
		return payer;
	}


	public void setPayer(PremiumPayer payer) {
		this.payer = payer;
	}


	public Insured getInsured() {
		return insured;
	}


	public void setInsured(Insured insured) {
		this.insured = insured;
	}


	public Person getPayerPerson() {
		return payerPerson;
	}


	public void setPayerPerson(Person payerPerson) {
		this.payerPerson = payerPerson;
	}


	public Person getInsuredPerson() {
		return insuredPerson;
	}


	public void setInsuredPerson(Person insuredPerson) {
		this.insuredPerson = insuredPerson;
	}


	public AgreementDocument getAgreementDocument() {
		return agreementDocument;
	}


	public void setAgreementDocument(AgreementDocument agreementDocument) {
		this.agreementDocument = agreementDocument;
	}


	public Set<Document> getAppionteeDocuments() {
		return appionteeDocuments;
	}


	public void setAppionteeDocuments(Set<Document> appionteeDocuments) {
		this.appionteeDocuments = appionteeDocuments;
	}
	
}
