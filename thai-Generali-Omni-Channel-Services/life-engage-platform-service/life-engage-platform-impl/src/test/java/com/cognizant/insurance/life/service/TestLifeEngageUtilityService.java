package com.cognizant.insurance.life.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LifeEngageUtilityService;

@ContextConfiguration("/service-context-test.xml")
public class TestLifeEngageUtilityService extends AbstractJUnit4SpringContextTests {

    @Autowired
    LifeEngageUtilityService lifeEngageUtilityService;

    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant STATUS. */
    private static final String STATUS = "Status";

    /** The Constant STATUS_CODE. */
    private static final String STATUS_CODE = "StatusCode";

    /** The Constant STATUS_DATA. */
    private static final String STATUS_DATA = "StatusData";

    /** The Constant STATUS_MESSAGE. */
    private static final String STATUS_MESSAGE = "StatusMessage";

    @Test
    public void testSaveDataWipeAudit() throws IOException, BusinessException, ParseException {
        String responseJson = null;
        String statusCode100 = "100";
        String statusSuccess = "SUCCESS";
        String statusMessage = "Success";

        String Request = getRequest("dataWipe/datawipeAudit-request.json");
        responseJson = lifeEngageUtilityService.saveDataWipeAudit(Request);
        System.out.println(responseJson);

        assertNotNull(responseJson);
        JSONObject jsonObject = new JSONObject(responseJson);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        JSONObject statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));

    }

    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(new String(line.getBytes(), "UTF-8"));
        }

        return sb.toString();
    }

}
