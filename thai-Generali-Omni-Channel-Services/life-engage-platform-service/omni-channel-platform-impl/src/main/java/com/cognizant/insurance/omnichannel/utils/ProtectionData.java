package com.cognizant.insurance.omnichannel.utils;

public class ProtectionData {

    private Long protectionFund;

    private Long protectionCover;

    private String categoryAxisSavings;

    private String categoryAxisFund;
	public Long getProtectionFund() {
		return protectionFund;
	}
	public String getCategoryAxisSavings() {
        return categoryAxisSavings;
    }
    public void setCategoryAxisSavings(String categoryAxisSavings) {
        this.categoryAxisSavings = categoryAxisSavings;
    }
    public String getCategoryAxisFund() {
        return categoryAxisFund;
    }
    public void setCategoryAxisFund(String categoryAxisFund) {
        this.categoryAxisFund = categoryAxisFund;
    }
    public void setProtectionFund(Long protectionFund) {
		this.protectionFund = protectionFund;
	}
	public Long getProtectionCover() {
		return protectionCover;
	}
	public void setProtectionCover(Long protectionCover) {
		this.protectionCover = protectionCover;
	}

}
