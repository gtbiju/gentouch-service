/**
 * 
 */
package com.cognizant.insurance.component.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.LifeEngageProductComponent;
import com.cognizant.insurance.component.repository.ProductRepository;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.vo.ResponseBo;
import com.cognizant.insurance.response.vo.ResponseInfo;
import com.cognizant.insurance.response.vo.ResponsePayload;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;



/**
 * The Class LifeEngageProductComponentImpl.
 */
@Service("lifeEngageProductComponent")
public class LifeEngageProductComponentImpl implements
		LifeEngageProductComponent {

	/** The e app repository. */
	@Autowired
	private ProductRepository productRepository;
	
	/** The get products holder. */
	@Autowired
	@Qualifier("getProductsServiceMapping")
	private LifeEngageSmooksHolder getProductsHolder;
	
	/** The get products holder. */
	@Autowired
	@Qualifier("getProductServiceMapping")
	private LifeEngageSmooksHolder getProductHolder;

	/** Is Product Active Holder. */
    @Autowired
    @Qualifier("isProductActiveServiceMapping")
    private LifeEngageSmooksHolder isProductActiveHolder;
    
    /** The get products holder. */
    @Autowired
    @Qualifier("getProductsByFilterMapping")
    private LifeEngageSmooksHolder getProductsByFilterHolder;
    
    /** The get products holder. */
    @Autowired
    @Qualifier("getProductsDetailsByFilterMapping")
    private LifeEngageSmooksHolder getProductsDetailsByFilterHolder;
    
    /** The getproduct templates by filter holder. */
    @Autowired
    @Qualifier("getproductTemplatesByFilterMapping")
    private LifeEngageSmooksHolder getproductTemplatesByFilterHolder;
    
    private static final String VERSION_TWO = "2";
	
	/* (non-Javadoc)
	 * @see com.cognizant.insurance.component.LifeEngageProductComponent#getProduct(java.lang.String)
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public String getProduct(RequestInfo requestInfo, String json, String version) throws BusinessException {
		
		String parseJson = "";
		final ProductSearchCriteria productSearchCriteria =
            (ProductSearchCriteria) getProductHolder.parseJson(json);
	    if(productSearchCriteria.getCheckDate()== null) {
	        productSearchCriteria.setCheckDate(new Date());
	    }
	    if(productSearchCriteria.getValidityDate()== null) {
	        productSearchCriteria.setValidityDate(new Date());
	    }
	    if(version.equals(VERSION_TWO)){
	    	 productSearchCriteria.setProductId(productSearchCriteria.getId().intValue());
	    }
	   
	    productSearchCriteria.setIsAllDataRequired(Boolean.TRUE);
	    
	    final ResponseBo responseBo = new ResponseBo();
	    final ResponseInfo responseInfo = createResponseInfo(requestInfo);
	    responseBo.setResponseInfo(responseInfo);
	    final ResponsePayload responsePayLoad = new ResponsePayload();
	    
		    final ProductSpecification productSpecification = productRepository.getProduct(productSearchCriteria, version);
		    if (productSpecification != null) {
		        final List<ProductSpecification> prodSpecList = new ArrayList<ProductSpecification>();
		        prodSpecList.add(productSpecification);
		        responsePayLoad.setProductSpecification(prodSpecList);
		    }
	   
	    responsePayLoad.setProductSearchCriteria(productSearchCriteria);
	    responseBo.setResponsePayload(responsePayLoad);
	    parseJson = getProductHolder.parseBO(responseBo);
		return parseJson;
	}

	/* (non-Javadoc)
	 * @see com.cognizant.insurance.component.LifeEngageProductComponent#isProductActive(java.lang.String)
	 */
	@Override
	public String isProductActive(RequestInfo requestInfo, String json, String version) throws BusinessException {

		String parseJson = "";
		final ProductSearchCriteria productSearchCriteria =
             (ProductSearchCriteria) isProductActiveHolder.parseJson(json);
    
	    final ResponseBo responseBo = new ResponseBo();
	    final ResponseInfo responseInfo = createResponseInfo(requestInfo);
	    responseBo.setResponseInfo(responseInfo);
	    final ResponsePayload responsePayLoad = new ResponsePayload();
	 
	    	Boolean isProductActive = productRepository.isProductActive(productSearchCriteria, version);
		    if (isProductActive) {
			      final List<ProductSpecification> prodSpecList = new ArrayList<ProductSpecification>();
			      ProductSpecification productSpecification = new ProductSpecification();
			      productSpecification.setId(productSearchCriteria.getId());
			      prodSpecList.add(productSpecification);
			      responsePayLoad.setProductSpecification(prodSpecList);
		    }
		 
	    responseBo.setResponsePayload(responsePayLoad);
	    parseJson = isProductActiveHolder.parseBO(responseBo);
	     
		return parseJson;
	}

	/* (non-Javadoc)
	 * @see com.cognizant.insurance.component.LifeEngageProductComponent#getProducts(java.lang.String)
	 */
	@Override
	public String getProducts(RequestInfo requestInfo, String json, Boolean activeProducts) throws BusinessException {
		
		 String parseJson = "";
		 final ProductSearchCriteria productSearchCriteria =
             (ProductSearchCriteria) getProductsHolder.parseJson(json);
	     Response<List<ProductSpecification>> response ;
	     final ResponseBo responseBo = new ResponseBo();
	     final ResponseInfo responseInfo = createResponseInfo(requestInfo);
	     responseBo.setResponseInfo(responseInfo);
	     final ResponsePayload responsePayLoad = new ResponsePayload();
	     //Get All or active products based on LOB - Life or Motor
			 response = productRepository.getProducts(productSearchCriteria, activeProducts);
		     final List<ProductSpecification> prodSpecList = response.getType();
		     responsePayLoad.setProductSpecification(prodSpecList);
	     responseBo.setResponsePayload(responsePayLoad);
	     parseJson = getProductsHolder.parseBO(responseBo);
	     
		return parseJson;
	}

	/* (non-Javadoc)
	 * @see com.cognizant.insurance.component.LifeEngageProductComponent#getAllOrActiveProductsByFilter(java.lang.String)
	 */
	@Override
	public String getAllOrActiveProductsByFilter(RequestInfo requestInfo, String json, Boolean activeProducts, String version)
			throws BusinessException {
		
		String parseJson = "";
		final ProductSearchCriteria productSearchCriteria =
            (ProductSearchCriteria) getProductsByFilterHolder.parseJson(json);
	    Response<List<ProductSpecification>> response;
	    final ResponseBo responseBo = new ResponseBo();
	    final ResponseInfo responseInfo = createResponseInfo(requestInfo);
	    responseBo.setResponseInfo(responseInfo);
	    final ResponsePayload responsePayLoad = new ResponsePayload();
	    
	  
	         response = productRepository.getAllOrActiveProductsByFilter(productSearchCriteria, activeProducts, version);
	         final List<ProductSpecification> prodSpecList = response.getType();
	         responsePayLoad.setProductSpecification(prodSpecList);
      
	    responseBo.setResponsePayload(responsePayLoad);
	    parseJson = getProductsByFilterHolder.parseBO(responseBo);
		return parseJson;
	}

	/* (non-Javadoc)
	 * @see com.cognizant.insurance.component.LifeEngageProductComponent#getProductsByFilter(java.lang.String)
	 */
	@Override
	public String getProductsByFilter(RequestInfo requestInfo, String json, String version) throws BusinessException {

		String parseJson = "";
		final ProductSearchCriteria productSearchCriteria =
            (ProductSearchCriteria) getProductsDetailsByFilterHolder.parseJson(json);
		final ResponseBo responseBo = new ResponseBo();
        final ResponseInfo responseInfo = createResponseInfo(requestInfo);
        responseBo.setResponseInfo(responseInfo);
        final ResponsePayload responsePayLoad = new ResponsePayload();
        
        // to get required details of list of products
      
	        Response<List<ProductSpecification>> response = productRepository.getProductsByFilter(productSearchCriteria, version);
	        final List<ProductSpecification> prodSpecList = response.getType();
	        responsePayLoad.setProductSpecification(prodSpecList);
        responsePayLoad.setProductSearchCriteria(productSearchCriteria);
        responseBo.setResponsePayload(responsePayLoad);
        parseJson = getProductsDetailsByFilterHolder.parseBO(responseBo);
		return parseJson;
	}

	/* (non-Javadoc)
	 * @see com.cognizant.insurance.component.LifeEngageProductComponent#getProductTemplatesByFilter(java.lang.String)
	 */
	@Override
	public String getProductTemplatesByFilter(RequestInfo requestInfo, String json)
			throws BusinessException {
		
		String parseJson = "";
		final ProductSearchCriteria productSearchCriteria =
            (ProductSearchCriteria) getproductTemplatesByFilterHolder.parseJson(json);
      
	        Response<List<ProductTemplateMapping>> response = productRepository.getProductTemplatesByFilter(productSearchCriteria);
	        final List<ProductTemplateMapping> productTemplateMappings = response.getType();
	        productSearchCriteria.setTemplatemapping(productTemplateMappings);
       
        final ResponsePayload responsePayLoad = new ResponsePayload();
     
        responsePayLoad.setProductSearchCriteria(productSearchCriteria);
        final ResponseBo responseBo = new ResponseBo();
        final ResponseInfo responseInfo = createResponseInfo(requestInfo);
        responseBo.setResponseInfo(responseInfo);
        responseBo.setResponsePayload(responsePayLoad);
        parseJson = getproductTemplatesByFilterHolder.parseBO(responseBo);
		return parseJson;
	}
	
	/**
     * Creates the response info.
     *
     * @param requestInfo the request info
     * @return the response info
     */
     private ResponseInfo createResponseInfo(RequestInfo requestInfo) {
         final ResponseInfo responseInfo = new ResponseInfo();
         responseInfo.setCreationDateTime(requestInfo.getCreationDateTime());
       responseInfo.setRequestorToken(requestInfo.getRequestorToken());
       responseInfo.setSourceInfoName(requestInfo.getSourceInfoName());
       responseInfo.setTransactionId(requestInfo.getTransactionId());
       responseInfo.setUserName(requestInfo.getUserName());

       return responseInfo;
     }

}
