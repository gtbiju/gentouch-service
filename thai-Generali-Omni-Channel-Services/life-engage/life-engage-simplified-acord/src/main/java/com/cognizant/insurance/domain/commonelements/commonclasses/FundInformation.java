/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 301350
 * @version       : 0.1, Aug 08, 2013
 */
package com.cognizant.insurance.domain.commonelements.commonclasses;

import javax.persistence.Entity;
import com.cognizant.insurance.domain.commonelements.InformationModelObject;

/**
 * The Class FundInformation. Newly added for EApp - 301350, 081Aug2013. Not part
 * of ACORD.
 */

@Entity
public class FundInformation extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8254159605449610889L;

    /** The name of the fund/portfolio. */
    private String name;

    /** The allocaton percentage. */
    private Long allocatonPercentage;

    /** The bonusOrDividend option. */
    private String bonusOrDividend;

    /** The PayoutOrSettlement option. */
    private String payoutOrSettlement;

    private String fundCode;
    
    private Integer fundId;

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the allocatonPercentage.
     * 
     * @return Returns the allocatonPercentage.
     */
    public final Long getAllocatonPercentage() {
        return allocatonPercentage;
    }

    /**
     * Gets the bonusOrDividend.
     * 
     * @return Returns the bonusOrDividend.
     */
    public final String getBonusOrDividend() {
        return bonusOrDividend;
    }

    /**
     * Gets the payoutOrSettlement.
     * 
     * @return Returns the payoutOrSettlement.
     */
    public final String getPayoutOrSettlement() {
        return payoutOrSettlement;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * Sets The allocatonPercentage.
     * 
     * @param allocatonPercentage
     *            The allocatonPercentage to set.
     */
    public final void setAllocatonPercentage(Long allocatonPercentage) {
        this.allocatonPercentage = allocatonPercentage;
    }

    /**
     * Sets The bonusOrDividend.
     * 
     * @param bonusOrDividend
     *            The bonusOrDividend to set.
     */
    public final void setBonusOrDividend(String bonusOrDividend) {
        this.bonusOrDividend = bonusOrDividend;
    }

    /**
     * Sets The payoutOrSettlement.
     * 
     * @param payoutOrSettlement
     *            The payoutOrSettlement to set.
     */
    public final void setPayoutOrSettlement(String payoutOrSettlement) {
        this.payoutOrSettlement = payoutOrSettlement;
    }

	/**
	 * @param fundId the fundId to set
	 */
	public void setFundId(Integer fundId) {
		this.fundId = fundId;
	}

	/**
	 * @return the fundId
	 */
	public Integer getFundId() {
		return fundId;
	}
}
