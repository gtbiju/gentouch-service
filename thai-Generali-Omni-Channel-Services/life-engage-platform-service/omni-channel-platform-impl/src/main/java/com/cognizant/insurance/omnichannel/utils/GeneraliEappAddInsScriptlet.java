package com.cognizant.insurance.omnichannel.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.icr.pdf.util.Constants;
import com.cognizant.icr.pdf.util.PropertyFileReader;
import com.cognizant.insurance.domain.agreement.Coverage;
import com.cognizant.insurance.domain.agreement.agreementpremium.Premium;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.commonelements.commonclasses.FamilyHealthHistory;
import com.cognizant.insurance.domain.commonelements.commonclasses.GLISelectedOption;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.contactandplace.ContactPoint;
import com.cognizant.insurance.domain.contactandplace.ContactPreference;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.AddressNatureCodeList;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.ElectronicContact;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.PostalAddressContact;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.TelephoneCallContact;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.City;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.Country;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.CountryElement;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.extension.Extension;
import com.cognizant.insurance.domain.finance.FinancialScheduler;
import com.cognizant.insurance.domain.finance.paymentmethodsubtypes.CreditCardPayment;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;
import com.cognizant.insurance.domain.party.partyname.PersonName;
import com.cognizant.insurance.domain.party.persondetailsubtypes.EducationDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.IncomeDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.OccupationDetail;
import com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes.EmploymentRelationship;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.bridge.OmniContextBridge;
import com.cognizant.insurance.utils.FileUtils;

/**
 * @author 444873 
 * Class for eApp pdf data mapping
 *
 */
public class GeneraliEappAddInsScriptlet extends JRDefaultScriptlet{

	private Set<PersonName> personNames;
	private Person person;
	private Set<PersonDetail> personDetails;
	private IncomeDetail incomeDetail;
	private Set<Country> countries;
	private Set<ContactPreference> contactPreferences;
	private PostalAddressContact postalAddressContact;
	private PostalAddressContact permanentAddress;
	private OccupationDetail occupationDetail;
	private TelephoneCallContact home1Contact;
	private ElectronicContact electronicContact;
	
	private String insuredMobileNumber1;
	private String insuredHomeNumber1;
	private String insuredOfficeNumber;

	/** Declaration Questions set**/
	private Set<UserSelection> declationQuestions;
	private String agentSignDate;
	private String agentSignPlace;
	private Date spajSignDate;
	private String SpajSignPlace;

	private String identityNo;
	private Date insuredIdentityDate;
	private String insuredIdentityPlace;

	private BigDecimal insuredHeight;
	private BigDecimal insuredWeight;

	/** The proposer. */
	private AgreementHolder proposer;
	private Person proposerPerson;
	private Set<PersonName> proposerPersonNames;

	private Date proposerdob;
	private Date insuredDob;

	String proposerFullName;
	private Set<PersonDetail> proposerPersonDetails;
	private Set<ContactPreference> proposerContactPreferences;
	private Set<Country> proposerCountries;
	private Country proposerResidenceCountry;
	private Country insuredResidenceCountry;
	private ElectronicContact proposerElectronicContact;
	private PostalAddressContact proposerPostalAddressContact;
	private PostalAddressContact proposerPermanentAddress;
	private TelephoneCallContact telephoneCallContact;
	private String proposerMobileNumber1;
	private String proposerHomeNumber1;
	private String proposerOfficeNumber;

	private String relationshipWithProposer = "";
	private OccupationDetail proposerOccupationDetail;
	private String proposerIdentityProof;
	private String proposerIdentityDate;
	private String proposerIdentityPlace;
	private String proposerIdentityNo;

	IncomeDetail proposerIncomeDetail;
	EducationDetail proposerEducationDetail;
	String proposerPaymentMethod;


	/** The additional insured list. */
	private Set<Insured> additionalInsuredes;

	Insured firstAddInsured;
	Insured secondAddInsured;
	Insured thirdAddInsured;
	Insured fourthAddInsured;
	private Person firstAddInsuredPerson;
	private Person secondAddInsuredPerson;
	private Person thirdAddInsuredPerson;
	private Person fourthAddInsuredPerson;
	


	private Date firstAddInsuredDob;
	private Set<PersonName> firstAddInsuredPersonNames;
	private Set<PersonDetail> firstAddInsuredPersonDetails;
	private Set<Country> firstAddInsuredCountries;
	private Country firstAddInsuredResidenceCountry;
	private OccupationDetail firstAddInsuredOccupationDetail;
	private EducationDetail firstAddInsuredEducationDetail;
	private IncomeDetail firstAddInsuredIncomeDetail;
	private String firstAddInsuredRelationWithProposer;
	private BigDecimal firstAddInsuredHeight;
	private BigDecimal firstAddInsuredWeight;
	private String firstAddInsuredIdCardType;
	private String firstAddInsuredIdCardNumber;

	Date secondAddInsuredDob;
	private Set<PersonName> secondAddInsuredPersonNames;
	private Set<PersonDetail> secondAddInsuredPersonDetails;
	private Set<Country> secondAddInsuredCountries;
	Country secondAddInsuredResidenceCountry;
	private OccupationDetail secondAddInsuredOccupationDetail;
	private EducationDetail secondAddInsuredEducationDetail;
	private IncomeDetail secondAddInsuredIncomeDetail;
	String secondAddInsuredRelationWithProposer;
	private String secondAddInsuredIdCardType;
	private String secondAddInsuredIdCardNumber;
	private BigDecimal secondAddInsuredHeight;
	private BigDecimal secondAddInsuredWeight;


	Date thirdAddInsuredDob;
	private Set<PersonName> thirdAddInsuredPersonNames;
	private Set<PersonDetail> thirdAddInsuredPersonDetails;
	private Set<Country> thirdAddInsuredCountries;
	Country thirdAddInsuredResidenceCountry;
	OccupationDetail thirdAddInsuredOccupationDetail;
	private EducationDetail thirdAddInsuredEducationDetail;
	private IncomeDetail thirdAddInsuredIncomeDetail;
	String thirdAddInsuredRelationWithProposer;
	private String thirdAddInsuredIdCardType;
	private String thirdAddInsuredIdCardNumber;
	private BigDecimal thirdAddInsuredHeight;
	private BigDecimal thirdAddInsuredWeight;

	Date fourthAddInsuredDob;
	private Set<PersonName> fourthAddInsuredPersonNames;
	private Set<PersonDetail> fourthAddInsuredPersonDetails;
	private Set<Country> fourthAddInsuredCountries;
	Country fourthAddInsuredResidenceCountry;
	private OccupationDetail fourthAddInsuredOccupationDetail;
	private EducationDetail fourthAddInsuredEducationDetail;
	private IncomeDetail fourthAddInsuredIncomeDetail;
	String fourthAddInsuredRelationWithProposer;
	private String fourthAddInsuredIdCardType;
	private String fourthAddInsuredIdCardNumber;
	private BigDecimal fourthAddInsuredHeight;
	private BigDecimal fourthAddInsuredWeight;

	private Date firstAddInsuredIdentityDate;
	private String firstAddInsuredIdentityPlace;
	private Date secondAddInsuredIdentityDate;
	private String secondAddInsuredIdentityPlace;
	private Date thirdAddInsuredIdentityDate;
	private String thirdAddInsuredIdentityPlace;
	private Date fourthAddInsuredIdentityDate;
	private String fourthAddInsuredIdentityPlace;
	
	/** The beneficiary list. */

	private Person firstBeneficiaryPerson;
	private Set<PersonName> firstBeneficiaryPersonNames;
	private Date firstBeneficiaryDOB;
	private String firstBeneficiaryGender;
	private Float firstBeneficiarySharePercentage;
	private String firstBeneficiaryRelationWithInsured;

	private Person secondBeneficiaryPerson;
	private Set<PersonName> secondBeneficiaryPersonNames;
	private Date secondBeneficiaryDOB;
	private String secondBeneficiaryGender;
	private Float secondBeneficiarySharePercentage;
	private String secondBeneficiaryRelationWithInsured;
	
	private Person thirdBeneficiaryPerson;
	private Set<PersonName> thirdBeneficiaryPersonNames;
	private Date thirdBeneficiaryDOB;
	private String thirdBeneficiaryGender;
	private Float thirdBeneficiarySharePercentage;
	private String thirdBeneficiaryRelationWithInsured;

	private Person fourthBeneficiaryPerson;
	private Set<PersonName> fourthBeneficiaryPersonNames;
	private Date fourthBeneficiaryDOB;
	private String fourthBeneficiaryGender;
	private Float fourthBeneficiarySharePercentage;
	private String fourthBeneficiaryRelationWithInsured;

	
	private String firstBeneficiaryTypeOfIdentity;
	private String secondBeneficiaryTypeOfIdentity;
	private String thirdBeneficiaryTypeOfIdentity;
	private String fourthBeneficiaryTypeOfIdentity;


	private Set<UserSelection> proposerUserSelections;
	String proposerFatcaQues1;
	String proposerFatcaQues2;

	private Set<UserSelection> firstBeneficiarySelectedOptions;
	private Set<UserSelection> secondBeneficiarySelectedOptions;
	private Set<UserSelection> thirdBeneficiarySelectedOptions;
	private Set<UserSelection> fourthBeneficiarySelectedOptions;
	private Set<UserSelection> fifthBeneficiarySelectedOptions;
	Boolean beneficiaryFatcaQues1;
	Boolean beneficiaryFatcaQues2;

	String spajNumber;
	String illustrationNumber;
	String agentCode;

	
	private String insuredIdentityType;
	private String proposerIdentityProofType;

	/** Previous Policy */
	private Set<Insured> insuredPreviousPolicyList;
	private String firstCompanyName;
	private String firstPrevInsuredName;
	private String firstFinalDecision;
	private String firstTypeOfInsurance;
	private BigDecimal firstPrevSumAssured;
	private String firstPolicyStatus;

	private String secondCompanyName;
	private String secondPrevInsuredName;
	private String secondFinalDecision;
	private String secondTypeOfInsurance;
	private BigDecimal secondPrevSumAssured;
	private String secondPolicyStatus;

	private String thirdCompanyName;
	private String thirdPrevInsuredName;
	private String thirdFinalDecision;
	private String thirdTypeOfInsurance;
	private BigDecimal thirdPrevSumAssured;
	private String thirdPolicyStatus;

	private String fourthCompanyName;
	private String fourthPrevInsuredName;
	private String fourthFinalDecision;
	private String fourthTypeOfInsurance;
	private BigDecimal fourthPrevSumAssured;
	private String fourthPolicyStatus;


	private String	firstPrePolicyType;
	private String	secondPrePolicyType;
	private String	thirdPrePolicyType;
	private String	fourthPrePolicyType;
	
	private Date secondPrePolicyEffectiveDate;
	private Date firstPrePolicyEffectiveDate;
	private Date thirdPrePolicyEffectiveDate;
	private Date fourthPrePolicyEffectiveDate;
	
	private Boolean isPropDiffFromInsured;
	
	private Set<ContactPreference> firstAddnInsuredContactPreferences;
	private Set<ContactPreference> secondAddnInsuredContactPreferences;
	private Set<ContactPreference> thirdAddnInsuredContactPreferences;
	private Set<ContactPreference> fourthAddnInsuredContactPreferences;
	
	private ElectronicContact firstAddnInsuredElectronicContact;
	private ElectronicContact secondAddnInsuredElectronicContact;
	private ElectronicContact thirdAddnInsuredElectronicContact;
	private ElectronicContact fourthAddnInsuredElectronicContact;


	private PostalAddressContact firstAddnInsuredPostalAddressContact;
	private PostalAddressContact firstAddnInsuredPermanentAddress;
	private String firstAddnInsuredMobileNumber1;
	private String firstAddnInsuredHomeNumber1;
	private String firstAddnInsuredOfficeNumber;

	private PostalAddressContact secondAddnInsuredPostalAddressContact;
	private PostalAddressContact secondAddnInsuredPermanentAddress;
	private String secondAddnInsuredMobileNumber1;
	private String secondAddnInsuredHomeNumber1;
	private String secondAddnInsuredOfficeNumber;
	
	private PostalAddressContact thirdAddnInsuredPostalAddressContact;
	private PostalAddressContact thirdAddnInsuredPermanentAddress;
	private String thirdAddnInsuredMobileNumber1;
	private String thirdAddnInsuredHomeNumber1;
	private String thirdAddnInsuredOfficeNumber;
	
	private PostalAddressContact fourthAddnInsuredPostalAddressContact;
	private PostalAddressContact fourthAddnInsuredPermanentAddress;
	private String fourthAddnInsuredMobileNumber1;
	private String fourthAddnInsuredHomeNumber1;
	private String fourthAddnInsuredOfficeNumber;

	private String firstAddInsuredTypeOfIdentity;
	private String secondAddInsuredTypeOfIdentity;
	private String thirdAddInsuredTypeOfIdentity;
	private String fourthAddInsuredTypeOfIdentity;
	
	private String firstAddInsuredIdentityNo;
	private String secondAddInsuredIdentityNo;
	private String thirdAddInsuredIdentityNo;
	private String fourthAddInsuredIdentityNo;
	
	private Set<UserSelection> insuredUserSelections;
	private String insuredHasExistingPolicies;
	private String insuredFamilyMembersDiagWithDiseases;
	
	private Set<FamilyHealthHistory> familyHealthHistories;
	private Integer firstFamilyMemAgeAtDeath;
	private Integer firstFamilyMemAge;
	private String firstFamilyMemCauseOfDeath;
	private String firstFamilyMemRelation;
	
	private Integer secondFamilyMemAgeAtDeath;
	private Integer secondFamilyMemAge;
	private String secondFamilyMemCauseOfDeath;
	private String secondFamilyMemRelation;

	private Integer thirdFamilyMemAgeAtDeath;
	private Integer thirdFamilyMemAge;
	private String thirdFamilyMemCauseOfDeath;
	private String thirdFamilyMemRelation;
	
	private Set<FamilyHealthHistory> familyAddFirstHealthHistories;
	private Integer firstAddFirstFamilyMemAgeAtDeath;
	private Integer firstAddFirstFamilyMemAge;
	private String firstAddFirstFamilyMemCauseOfDeath;
	private String firstAddFirstFamilyMemRelation;
	
	private Integer secondAddFirstFamilyMemAgeAtDeath;
	private Integer secondAddFirstFamilyMemAge;
	private String secondAddFirstFamilyMemCauseOfDeath;
	private String secondAddFirstFamilyMemRelation;

	private Integer thirdAddFirstFamilyMemAgeAtDeath;
	private Integer thirdAddFirstFamilyMemAge;
	private String thirdAddFirstFamilyMemCauseOfDeath;
	private String thirdAddFirstFamilyMemRelation;
	
	private Set<FamilyHealthHistory> familyAddSecondHealthHistories;
	private Integer firstAddSecondFamilyMemAgeAtDeath;
	private Integer firstAddSecondFamilyMemAge;
	private String firstAddSecondFamilyMemCauseOfDeath;
	private String firstAddSecondFamilyMemRelation;
	
	private Integer secondAddSecondFamilyMemAgeAtDeath;
	private Integer secondAddSecondFamilyMemAge;
	private String secondAddSecondFamilyMemCauseOfDeath;
	private String secondAddSecondFamilyMemRelation;

	private Integer thirdAddSecondFamilyMemAgeAtDeath;
	private Integer thirdAddSecondFamilyMemAge;
	private String thirdAddSecondFamilyMemCauseOfDeath;
	private String thirdAddSecondFamilyMemRelation;
	
	private Set<FamilyHealthHistory> familyAddThirdHealthHistories;
	private Integer firstAddThirdFamilyMemAgeAtDeath;
	private Integer firstAddThirdFamilyMemAge;
	private String firstAddThirdFamilyMemCauseOfDeath;
	private String firstAddThirdFamilyMemRelation;
	
	private Integer secondAddThirdFamilyMemAgeAtDeath;
	private Integer secondAddThirdFamilyMemAge;
	private String secondAddThirdFamilyMemCauseOfDeath;
	private String secondAddThirdFamilyMemRelation;

	private Integer thirdAddThirdFamilyMemAgeAtDeath;
	private Integer thirdAddThirdFamilyMemAge;
	private String thirdAddThirdFamilyMemCauseOfDeath;
	private String thirdAddThirdFamilyMemRelation;
	
	private Set<FamilyHealthHistory> familyAddFourthHealthHistories;
	private Integer firstAddFourthFamilyMemAgeAtDeath;
	private Integer firstAddFourthFamilyMemAge;
	private String firstAddFourthFamilyMemCauseOfDeath;
	private String firstAddFourthFamilyMemRelation;
	
	private Integer secondAddFourthFamilyMemAgeAtDeath;
	private Integer secondAddFourthFamilyMemAge;
	private String secondAddFourthFamilyMemCauseOfDeath;
	private String secondAddFourthFamilyMemRelation;

	private Integer thirdAddFourthFamilyMemAgeAtDeath;
	private Integer thirdAddFourthFamilyMemAge;
	private String thirdAddFourthFamilyMemCauseOfDeath;
	private String thirdAddFourthFamilyMemRelation;
	
	private String insuredHaveSuspendedPolicies;
	private String insuredHaveSuspendedPoliciesDetails;
	private String insuredHaveDeclainedPolicies;
	private String insuredHaveDeclainedPoliciesDetails;
	private String insuredHasEngagedSports;
	private String insuredHasEngagedSportsDetails;
	private String insuredHasLostWeight;
	private String insuredHasLostWeightDetails;
	private String insuredHasRecevingMedTreatment;
	private String insuredHasRecevingMedTreatmentDetails;
	private String insuredHasUsedTobacco;
	private String insuredHasUsedTobaccoDetails;
	private String insuredHasUsedDrugs;
	private String insuredHasUsedDrugsDetails;
	private String insuredHasUsedAlcoholicBev;
	private String insuredHasUsedAlcoholicBevDetails;
	private String insuredHasUndergoneDiagnosis;
	private String insuredHasUndergoneDiagnosisDetails;
	private String insuredHasIntensionToTravelDetails;
	private String insuredHasIntensionToTravel;
	private String insuredHasCardioDiseases;
	private String insuredHasCardioDiseasesDetails;
	private String insuredHasRespiratoryDiseases;
	private String insuredHasRespiratoryDiseasesDetails;
	private String insuredHasDigestiveDiseases;
	private String insuredHasDigestiveDiseasesDetails;
	private String insuredHasUrinaryDiseases;
	private String insuredHasUrinaryDiseasesDetails;
	private String insuredHasNervousDiseases;
	private String insuredHasNervousDiseasesDetails;
	private String insuredHasEndocrineDiseases;
	private String insuredHasEndocrineDiseasesDetails;
	private String insuredHasSkinDiseases;
	private String insuredHasSkinDiseasesDetails;
	private String insuredHasHaematologicDiseases;
	private String insuredHasHaematologicDiseasesDetails;
	private String insuredHasENTDiseases;
	private String insuredHasENTDiseasesDetails;
	private String insuredHasTumourOrCancer;
	private String insuredHasTumourOrCancerDetails;
	private String insuredHasDisability;
	private String insuredHasDisabilityDetails;
	private String insuredHasSexuallyTransmittedDisease;
	private String insuredHasSexuallyTransmittedDiseaseDetails;
	private String insuredHasWeightAtBirth;
	private String insuredHasPshycoAbnormality;
	private String insuredHasPshycoAbnormalityDetails;
	private String insuredIsPregnant;
	private String insuredIsPregnantDetails;
	private String insuredHasPregComplication;
	private String insuredHasPregComplicationDetails;
	private String insuredHasDoctor;
	private String insuredHasDoctorDetails;
	
	private String addInsuredOneHaveSuspendedPolicies;
	private String addInsuredOneHaveSuspendedPoliciesDetails = "";
	private String addInsuredOneHaveDeclainedPolicies;
	private String addInsuredOneHaveDeclainedPoliciesDetails= "";
	private String addInsuredOneHasEngagedSports;
	private String addInsuredOneHasEngagedSportsDetails= "";
	private String addInsuredOneHasLostWeight;
	private String addInsuredOneHasLostWeightDetails= "";
	private String addInsuredOneHasRecevingMedTreatment;
	private String addInsuredOneHasRecevingMedTreatmentDetails= "";
	private String addInsuredOneHasUsedTobacco;
	private String addInsuredOneHasUsedTobaccoDetails= "";
	private String addInsuredOneHasUsedDrugs;
	private String addInsuredOneHasUsedDrugsDetails= "";
	private String addInsuredOneHasUsedAlcoholicBev;
	private String addInsuredOneHasUsedAlcoholicBevDetails= "";
	private String addInsuredOneHasUndergoneDiagnosis;
	private String addInsuredOneHasUndergoneDiagnosisDetails= "";
	private String addInsuredOneHasIntensionToTravelDetails= "";
	private String addInsuredOneHasIntensionToTravel;
	private String addInsuredOneHasCardioDiseases;
	private String addInsuredOneHasCardioDiseasesDetails= "";
	private String addInsuredOneHasRespiratoryDiseases;
	private String addInsuredOneHasRespiratoryDiseasesDetails= "";
	private String addInsuredOneHasDigestiveDiseases;
	private String addInsuredOneHasDigestiveDiseasesDetails= "";
	private String addInsuredOneHasUrinaryDiseases;
	private String addInsuredOneHasUrinaryDiseasesDetails= "";
	private String addInsuredOneHasNervousDiseases;
	private String addInsuredOneHasNervousDiseasesDetails= "";
	private String addInsuredOneHasEndocrineDiseases;
	private String addInsuredOneHasEndocrineDiseasesDetails= "";
	private String addInsuredOneHasSkinDiseases;
	private String addInsuredOneHasSkinDiseasesDetails= "";
	private String addInsuredOneHasHaematologicDiseases;
	private String addInsuredOneHasHaematologicDiseasesDetails= "";
	private String addInsuredOneHasENTDiseases;
	private String addInsuredOneHasENTDiseasesDetails= "";
	private String addInsuredOneHasTumourOrCancer;
	private String addInsuredOneHasTumourOrCancerDetails= "";
	private String addInsuredOneHasDisability;
	private String addInsuredOneHasDisabilityDetails= "";
	private String addInsuredOneHasSexuallyTransmittedDisease;
	private String addInsuredOneHasSexuallyTransmittedDiseaseDetails= "";
	private String addInsuredOneHasWeightAtBirth;
	private String addInsuredOneHasPshycoAbnormality;
	private String addInsuredOneHasPshycoAbnormalityDetails= "";
	private String addInsuredOneIsPregnant;
	private String addInsuredOneIsPregnantDetails= "";
	private String addInsuredOneHasPregComplication;
	private String addInsuredOneHasPregComplicationDetails= "";
	private String addInsuredOneHasDoctor;
	private String addInsuredOneHasDoctorDetails= "";
	
	private Set<UserSelection> addInsuredOneUserSelections;
	private String addInsuredHasExistingPolicies;
	private String addInsuredFamilyMembersDiagWithDiseases;
	
	private String acrQuest1;
	private String acrQuest1Details;
	private String acrQuest2;
	private String acrQuest2Details;
	private String acrQuest3;
	private String acrQuest3Details;
	private String acrQuest4;
	private String acrQuest4Details;
	private String acrQuest5;
	private String acrQuest5Details;
	private String acrQuest6;
	private String acrQuest6Details;
	private String acrQuesr6Opt1;
	private String acrQuesr6Opt2;
	private String acrQuesr6Opt3;
	private String acrQuesr6Opt4;
	private String acrQuesr6Opt5;
	private String acrQuest7;		
	private String acrQuest7Details;
	private String acrQuesr7Opt1;
	private String acrQuesr7Opt2;
	private String acrQuesr7Opt3;
	private String acrQuesr7Opt4;
	private String acrQuesr7Opt5;
	private String acrQuest8;
	private String acrQuest8Details;
	private String acrQuest9;
	private String acrQuest9Details;
	private String acrQuest10;
	private String acrQuest10Details;
	private String acrQuest11;
	private String acrQuest11Details;
	private String acrQuest12;
	private String acrQuest12Details;
	private String acrQuesr12Opt1;
	private String acrQuesr12Opt2;
	private String acrQuesr12Opt3;
	private String acrQuesr12Opt4;
	private String acrQuesr12Opt5;
	private String acrQuesr12Opt6;
	private String acrQuest13;
	private String acrQuest13Details;
	private String acrQuest14;
	private String acrQuest14Details;
	private String acrQuesr14Opt1;
	private String acrQuesr14Opt2;
	private String acrQuesr14Opt3;
	private String acrQuesr14Opt4;
	private String acrQuesr14Opt5;
	private String acrQuest15;
	private String acrQuest15Details;
	private String acrQuest16;
	private String acrQuest16Details;
	private String acrQuesr16Opt1;
	private String acrQuesr16Opt2;
	private String acrQuesr16Opt3;
	private String acrQuesr16Opt4;
	private String acrQuesr16Opt5;
	private String acrQuesr16Opt6;
	private String acrQuesr16Opt7;
	private String acrQuest17;
	private String acrQuest17Details;
	private String acrQuest18;
	private String acrQuest18Details;
	private String acrQuest19;
	private String acrQuest19Details;
	private String acrQuest20;
	private String acrQuest20Details;
	private String acrQuesr20Opt1;
	private String acrQuesr20Opt2;
	private String acrQuesr20Opt3;
	private String acrQuesr20Opt4;
	private String acrQuesr20Opt5;
	private String acrQuesr20Opt6;
	private String acrQuest21;
	private String acrQuest21Details;
	private String acrQuest22;
	private String acrQuest22Details;
	
	private AgreementProducer agent;
	private Person agentPerson;
	
	/** The proposal. */
    private InsuranceAgreement proposal;
    
    private String productName;
    private BigDecimal productSumAssured;
    private Integer productPolicyTerm;
    private Integer productPremiumTerm;
    
    private String firstRiderName;
	 private String secondRiderName;
	 private String thirdRiderName;
	 private String fourthRiderName;
	 private String fifthRiderName;
	 private String sixthRiderName;
	 private String seventhRiderName;
	 private String eightthRiderName;
	 private String ninthRiderName;
	 
	 private BigDecimal firstRiderSumAssured;
	 private BigDecimal secondRiderSumAssured;
	 private BigDecimal thirdRiderSumAssured;
	 private BigDecimal fourthRiderSumAssured;
	 private BigDecimal fifthRiderSumAssured;
	 private BigDecimal sixthRiderSumAssured;
	 private BigDecimal seventhRiderSumAssured;
	 private BigDecimal eightthRiderSumAssured;
	 private BigDecimal ninthRiderSumAssured;

	 private Integer firstRiderTerm;
	 private Integer secondRiderTerm;
	 private Integer thirdRiderTerm;
	 private Integer fourthRiderTerm;
	 private Integer fifthRiderTerm;
	 private Integer sixthRiderTerm;
	 private Integer seventhRiderTerm;
	 private Integer eightthRiderTerm;
	 private Integer ninthRiderTerm;
	 
	 private BigDecimal firstRiderInitialPremium;
	 private BigDecimal secondRiderInitialPremium;
	 private BigDecimal thirdRiderInitialPremium;
	 private BigDecimal fourthRiderInitialPremium;
	 private BigDecimal fifthRiderInitialPremium;
	 private BigDecimal sixthRiderInitialPremium;
	 private BigDecimal seventhRiderInitialPremium;
	 private BigDecimal eightthRiderInitialPremium;
	 private BigDecimal ninthRiderInitialPremium;
	 
	 private String productPremiumFrequency;
	 private BigDecimal totalInitialPremium;
	 
	 private String receiptNumber;
	 
	 private Insured additionalInsured;
	 
	 public String insuredDeclarationPlace;
	 public String proposerDeclarationPlace;
	 public String additionalInsDeclarationPlace;
	 
	 public Date insuredDeclarationDateTime;
	 public Date proposerDeclarationDateTime;
	 public Date additionalInsDeclarationDateTime;
	 
	 private String addInsuredOneHasUrinaryDiseasesDetailsOptions = "";
	 private String addInsuredOneHasEndocrineDiseasesDetailsOptions = "";
	 
	 public static String pdfReqInput;
	 private JSONObject jsonObjectPdfInput;
	 
	  /** The appointee agreement Documents. */
		public Set<Document> appionteeDocuments;
	 
	private static final Logger LOGGER = LoggerFactory.getLogger(GeneraliEappPDFScriptlet.class);

	
	public void beforeDetailEval() {
		try {
			com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured insured = (com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured) this
					.getFieldValue("insured");
			additionalInsuredes = (Set<Insured>) this.getFieldValue("additionalInsuredes");
			try{
				if (null != insured) {
					person = (Person) insured.getPlayerParty();
					relationshipWithProposer = insured.getRelationshipWithProposer();
				}
			}catch(Exception e){
				LOGGER.error("Error while getting insured Person "+e.getMessage());
			}
			
			try{
				agent =  (AgreementProducer) this.getFieldValue("agreementProducer");
			}catch(Exception e){
				LOGGER.error("Error while getting fieldValue agent "+e.getMessage());
			}

			try{
				additionalInsured =  (Insured) this.getFieldValue("additionalInsured");
			}catch(Exception e){
				LOGGER.error("Error while getting fieldValue additional insured "+e.getMessage());
			}

			
			try{
				if (null != person) {
					personNames = person.getName();
					personDetails = person.getDetail();
					contactPreferences = person.getPreferredContact();
					countries = person.getNationalityCountry();
					insuredResidenceCountry = person.getResidenceCountry();
					insuredDob = person.getBirthDate();
					if(person.getHeight()!= null && !"".equals(person.getHeight())){
						insuredHeight = person.getHeight().getValue();
					}
					if(person.getWeight()!= null && !"".equals(person.getWeight())){
						insuredWeight = person.getWeight().getValue();
					}
					insuredIdentityType = person.getIdentityProof();
					insuredIdentityDate = person.getIdentityDate();
					insuredIdentityPlace =person.getIdentityPlace();
					identityNo = person.getNationalId();
				}
				}catch(Exception e){
					LOGGER.error("Error  appointeePerson"+e.getMessage());
				}
			
			
			
		

			try{
				insuredPreviousPolicyList =  (Set<Insured>) this.getFieldValue("insuredPreviousPolicyList");
			}catch(Exception e){
				LOGGER.error("Error while getting fieldValue insuredPreviousPolicyList "+e.getMessage());
			}
			
			
			
			
			
			
			
			
			if (null != personDetails) {
				for (PersonDetail personDetail : personDetails) {
					if (personDetail instanceof IncomeDetail) {
						incomeDetail = (IncomeDetail) personDetail;
					} else if (personDetail instanceof OccupationDetail) {
						occupationDetail = (OccupationDetail) personDetail;
					}
				}
			}


			if (null != contactPreferences) {
				for (ContactPreference contactPreference : contactPreferences) {
					ContactPoint contactPoint = contactPreference
							.getPreferredContactPoint();
					if (contactPoint instanceof PostalAddressContact) {
						if (((PostalAddressContact) contactPoint)
								.getAddressNatureCode().equals(
										AddressNatureCodeList.Current)) {
							postalAddressContact = (PostalAddressContact) contactPoint;
						} else if (((PostalAddressContact) contactPoint)
								.getAddressNatureCode().equals(
										AddressNatureCodeList.Permanent)) {
							permanentAddress = (PostalAddressContact) contactPoint;
						}

					} else if (contactPoint instanceof TelephoneCallContact) {
						if (null != contactPreference.getPriorityLevel()) {
							if ((contactPreference.getPriorityLevel() == 1)) {
								home1Contact = (TelephoneCallContact) contactPoint;
							}
						}
					} else if (contactPoint instanceof ElectronicContact) {
						electronicContact = (ElectronicContact) contactPoint;
					}
				}
			}


			com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument document =
					(com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument) this.getFieldValue("document");
			if (null != document) {
				appionteeDocuments = document.getPages();

				}
			
			//--------------------------------//
			try {
				proposer = (com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder) this
						.getFieldValue("proposer");
				proposerFatcaQues1="No";
				proposerFatcaQues2="No";
				if (null != proposer) {
					proposerPerson = (Person) proposer.getPlayerParty();
					proposerUserSelections = proposer.getSelectedOptions();
					isPropDiffFromInsured = proposer.getProposerDifferentFromInsuredIndicator();
				}
				if(proposerUserSelections != null && proposerUserSelections.size() >0){
					for(UserSelection selection:proposerUserSelections){
						if(selection != null && selection.getQuestionnaireType() != null){
							if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
								if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
										selection.getSelectedOption() == true){
									proposerFatcaQues1 = "Yes";
								}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
										selection.getSelectedOption() == true){
									proposerFatcaQues2 = "Yes";
								}
							}
						}
					}
				}

				if (null != proposerPerson) {
					proposerdob = proposerPerson.getBirthDate();
					proposerPersonNames = proposerPerson.getName();
					proposerPersonDetails = proposerPerson.getDetail();
					proposerContactPreferences = proposerPerson.getPreferredContact();
					proposerCountries = proposerPerson.getNationalityCountry();
					proposerResidenceCountry = proposerPerson.getResidenceCountry();
					proposerIdentityProofType = proposerPerson.getIdentityProof();
					proposerIdentityPlace = proposerPerson.getIdentityPlace();
					proposerIdentityNo = proposerPerson.getNationalId();
				}

				if (null != proposerPersonDetails) {
					for (PersonDetail personDetail : proposerPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							proposerIncomeDetail = (IncomeDetail) personDetail;
						} else if (personDetail instanceof EducationDetail) {
							proposerEducationDetail = (EducationDetail) personDetail;
						} else
							if (personDetail instanceof OccupationDetail) {
								proposerOccupationDetail = (OccupationDetail) personDetail;
							}
					}
				}
			}
			catch(Exception e){
				LOGGER.error("ScriptletSample Error :while getting field value proposer"+ e);
			}
			

			beneficiaryFatcaQues1=false;
			beneficiaryFatcaQues2=false;
			Set<Beneficiary> beneficiaries=null;
			try{
				beneficiaries = (Set<Beneficiary>) this.getFieldValue("beneficiaries");

				Beneficiary firstBeneficiary = null;
				Beneficiary secondBeneficiary = null;
				Beneficiary thirdBeneficiary = null;
				Beneficiary fourthBeneficiary = null;
				Beneficiary fifthBeneficiary = null;

				if(beneficiaries != null){
					for(Beneficiary beneficiary:beneficiaries){
						if(beneficiary != null ){
							for(Extension benExtension : beneficiary.getPlayerParty().getIncludesExtension()){
								if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("1")){
									firstBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("2")){
									secondBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("3")){
									thirdBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("4")){
									fourthBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("5")){
									fifthBeneficiary = beneficiary;
								}
							}
						}
					}

					if(firstBeneficiary != null){
						firstBeneficiarySelectedOptions=firstBeneficiary.getSelectedOptions();
						firstBeneficiaryPerson = (Person)firstBeneficiary.getPlayerParty();
						if(firstBeneficiaryPerson != null){
							firstBeneficiaryPersonNames = firstBeneficiaryPerson.getName();
							firstBeneficiaryTypeOfIdentity = firstBeneficiaryPerson.getIdentityProof();
						}
						if(firstBeneficiarySelectedOptions != null && firstBeneficiarySelectedOptions.size() >0){
							for(UserSelection selection:firstBeneficiarySelectedOptions){
								if(selection != null && selection.getQuestionnaireType() != null){
									if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
										if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
												selection.getSelectedOption() == true){
											beneficiaryFatcaQues1 = selection.getSelectedOption();
										}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
												selection.getSelectedOption() == true){
											beneficiaryFatcaQues2 = selection.getSelectedOption();
										}
									}
								}
							}
						}
						firstBeneficiaryDOB = firstBeneficiaryPerson.getBirthDate();
						firstBeneficiaryGender = firstBeneficiaryPerson.getGenderCode().name();
						firstBeneficiarySharePercentage = firstBeneficiary.getSharePercentage();
						firstBeneficiaryRelationWithInsured = firstBeneficiary.getRelationWithInsured();
					}

					if(secondBeneficiary != null){
						secondBeneficiarySelectedOptions=secondBeneficiary.getSelectedOptions();
						secondBeneficiaryPerson = (Person)secondBeneficiary.getPlayerParty();
						if(secondBeneficiaryPerson != null){
							secondBeneficiaryPersonNames = secondBeneficiaryPerson.getName();
							secondBeneficiaryTypeOfIdentity = secondBeneficiaryPerson.getIdentityProof();
						}
						secondBeneficiaryDOB = secondBeneficiaryPerson.getBirthDate();
						secondBeneficiaryGender = secondBeneficiaryPerson.getGenderCode().name();
						secondBeneficiarySharePercentage = secondBeneficiary.getSharePercentage();
						secondBeneficiaryRelationWithInsured = secondBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(secondBeneficiarySelectedOptions != null && secondBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:secondBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null && 
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();
											}
										}
									}
								}
							}
						}
					}
					
					if(thirdBeneficiary != null){
						thirdBeneficiarySelectedOptions=thirdBeneficiary.getSelectedOptions();
						thirdBeneficiaryPerson = (Person)thirdBeneficiary.getPlayerParty();
						if(thirdBeneficiaryPerson != null){
							thirdBeneficiaryPersonNames = thirdBeneficiaryPerson.getName();
							thirdBeneficiaryTypeOfIdentity = thirdBeneficiaryPerson.getIdentityProof();

						}
						thirdBeneficiaryDOB = thirdBeneficiaryPerson.getBirthDate();
						thirdBeneficiaryGender = thirdBeneficiaryPerson.getGenderCode().name();
						thirdBeneficiarySharePercentage = thirdBeneficiary.getSharePercentage();
						thirdBeneficiaryRelationWithInsured = thirdBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(thirdBeneficiarySelectedOptions != null && thirdBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:thirdBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null && 
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();
											}
										}
									}
								}
							}
						}
					}

					if(fourthBeneficiary != null){
						fourthBeneficiarySelectedOptions=fourthBeneficiary.getSelectedOptions();
						fourthBeneficiaryPerson = (Person)fourthBeneficiary.getPlayerParty();
						if(fourthBeneficiaryPerson != null){
							fourthBeneficiaryPersonNames = fourthBeneficiaryPerson.getName();
							fourthBeneficiaryTypeOfIdentity = thirdBeneficiaryPerson.getIdentityProof();
						}

						fourthBeneficiaryDOB = fourthBeneficiaryPerson.getBirthDate();
						fourthBeneficiaryGender = fourthBeneficiaryPerson.getGenderCode().name();
						fourthBeneficiarySharePercentage = fourthBeneficiary.getSharePercentage();
						fourthBeneficiaryRelationWithInsured = fourthBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(fourthBeneficiarySelectedOptions != null && fourthBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:fourthBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null && 
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();

											}

										}
									}
								}
							}
						}
					}

					
					
					
					
					
				}
			}catch (Exception e) {
				LOGGER.error("ScriptletSample Error : populating beneficiaries:",e);
			}


			try {
				InsuranceAgreement insuranceAgreement = (InsuranceAgreement) this.getFieldValue("proposal");
				proposal = insuranceAgreement;
				try{
					if(insuranceAgreement != null){
						TransactionKeys transactionKey = insuranceAgreement.getTransactionKeys();
						if(transactionKey != null){
							spajNumber = transactionKey.getKey21();
							illustrationNumber = transactionKey.getKey24();
							agentCode = transactionKey.getKey11();
						}
					}
				}catch(Exception e){
					LOGGER.error("Error while getting keys "+e.getMessage());
				}


				if(insuranceAgreement != null && insuranceAgreement.getAgreementExtension() != null){
					declationQuestions=insuranceAgreement.getAgreementExtension().getSelectedOptions();
					agentSignDate = insuranceAgreement.getAgreementExtension().getAgentSignDate();
					agentSignPlace = insuranceAgreement.getAgreementExtension().getAgentSignPlace();
					spajSignDate = insuranceAgreement.getAgreementExtension().getSpajDate();
					SpajSignPlace = insuranceAgreement.getAgreementExtension().getSpajSignPlace();
				}

				if(insuranceAgreement != null){
					for(Premium premium	:insuranceAgreement.getAgreementPremium()){
						if(("RenewalPremium").equals(premium.getNatureCode().name())){
							for(FinancialScheduler financialScheduler :premium.getAttachedFinancialScheduler()){
								proposerPaymentMethod =  financialScheduler.getPaymentMethodCode().name();
								CreditCardPayment creditCardPayment= (CreditCardPayment) financialScheduler.getPaymentMeans();
							}

						}
					}
				}
				/* Product Details*/
				if(insuranceAgreement != null){
					if(insuranceAgreement.getIncludesCoverage() != null && insuranceAgreement.getIncludesCoverage().size() >0){
						for(Coverage coverage:insuranceAgreement.getIncludesCoverage()){
							int riderCount = 0;
							if(coverage.getCoverageCode() != null && coverage.getCoverageCode().equals("BasicCoverage")){
								if(coverage.getProductType() != null){
									productName = coverage.getProductType().name();
								}
								if(coverage.getSumAssured() != null){
									productSumAssured = coverage.getSumAssured().getAmount();
								}
								productPolicyTerm = coverage.getPolicyTerm();
								productPremiumTerm = coverage.getPremiumTerm();
								if(coverage.getPremiumFrequency() != null){
									productPremiumFrequency = coverage.getPremiumFrequency().name();
								}
								if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
									for(Premium premium:coverage.getPremium()){
										if(premium.getNatureCode() != null && 
												premium.getNatureCode().name().equalsIgnoreCase("TotalInitialPremium")){
											if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
												totalInitialPremium = premium.getAmount().getAmount();
											}
												
										}
									}
								}
							}else if(coverage.getCoverageCode()!= null && coverage.getCoverageCode().equals("Rider")){
								if(riderCount == 0){
									firstRiderName = coverage.getName();
									if(coverage.getSumAssured() != null){
										firstRiderSumAssured = coverage.getSumAssured().getAmount();
									}
									firstRiderTerm = coverage.getRiderTerm();
									if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
										for(Premium premium:coverage.getPremium()){
											if(premium.getNatureCode() != null && 
													premium.getNatureCode().name().equalsIgnoreCase("InitialPremium")){
												if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
													firstRiderInitialPremium = premium.getAmount().getAmount();
												}
													
											}
										}
									}
								}else if(riderCount == 1){
									secondRiderName = coverage.getName();
									if(coverage.getSumAssured() != null){
										secondRiderSumAssured = coverage.getSumAssured().getAmount();
									}
									secondRiderTerm = coverage.getRiderTerm();
									if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
										for(Premium premium:coverage.getPremium()){
											if(premium.getNatureCode() != null && 
													premium.getNatureCode().name().equalsIgnoreCase("InitialPremium")){
												if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
													secondRiderInitialPremium = premium.getAmount().getAmount();
												}
													
											}
										}
									}
								}else if(riderCount == 2){
									thirdRiderName = coverage.getName();
									if(coverage.getSumAssured() != null){
										thirdRiderSumAssured = coverage.getSumAssured().getAmount();
									}
									thirdRiderTerm = coverage.getRiderTerm();
									if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
										for(Premium premium:coverage.getPremium()){
											if(premium.getNatureCode() != null && 
													premium.getNatureCode().name().equalsIgnoreCase("InitialPremium")){
												if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
													thirdRiderInitialPremium = premium.getAmount().getAmount();
												}
													
											}
										}
									}
								}else if(riderCount == 3){
									fourthRiderName = coverage.getName();
									if(coverage.getSumAssured() != null){
										fourthRiderSumAssured = coverage.getSumAssured().getAmount();
									}
									fourthRiderTerm = coverage.getRiderTerm();
									if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
										for(Premium premium:coverage.getPremium()){
											if(premium.getNatureCode() != null && 
													premium.getNatureCode().name().equalsIgnoreCase("InitialPremium")){
												if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
													fourthRiderInitialPremium = premium.getAmount().getAmount();
												}
													
											}
										}
									}
								}else if(riderCount == 4){
									fifthRiderName = coverage.getName();
									if(coverage.getSumAssured() != null){
										fifthRiderSumAssured = coverage.getSumAssured().getAmount();
									}
									fifthRiderTerm = coverage.getRiderTerm();
									if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
										for(Premium premium:coverage.getPremium()){
											if(premium.getNatureCode() != null && 
													premium.getNatureCode().name().equalsIgnoreCase("InitialPremium")){
												if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
													fifthRiderInitialPremium = premium.getAmount().getAmount();
												}
													
											}
										}
									}
								}else if(riderCount == 5){
									sixthRiderName = coverage.getName();
									if(coverage.getSumAssured() != null){
										sixthRiderSumAssured = coverage.getSumAssured().getAmount();
									}
									sixthRiderTerm = coverage.getRiderTerm();
									if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
										for(Premium premium:coverage.getPremium()){
											if(premium.getNatureCode() != null && 
													premium.getNatureCode().name().equalsIgnoreCase("InitialPremium")){
												if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
													sixthRiderInitialPremium = premium.getAmount().getAmount();
												}
													
											}
										}
									}
								}else if(riderCount == 6){
									seventhRiderName = coverage.getName();
									if(coverage.getSumAssured() != null){
										seventhRiderSumAssured = coverage.getSumAssured().getAmount();
									}
									seventhRiderTerm = coverage.getRiderTerm();
									if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
										for(Premium premium:coverage.getPremium()){
											if(premium.getNatureCode() != null && 
													premium.getNatureCode().name().equalsIgnoreCase("InitialPremium")){
												if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
													seventhRiderInitialPremium = premium.getAmount().getAmount();
												}
													
											}
										}
									}
								}else if(riderCount == 7){
									eightthRiderName = coverage.getName();
									if(coverage.getSumAssured() != null){
										eightthRiderSumAssured = coverage.getSumAssured().getAmount();
									}
									eightthRiderTerm = coverage.getRiderTerm();
									if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
										for(Premium premium:coverage.getPremium()){
											if(premium.getNatureCode() != null && 
													premium.getNatureCode().name().equalsIgnoreCase("InitialPremium")){
												if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
													eightthRiderInitialPremium = premium.getAmount().getAmount();
												}
													
											}
										}
									}
								}else if(riderCount == 8){
									ninthRiderName = coverage.getName();
									if(coverage.getSumAssured() != null){
										ninthRiderSumAssured = coverage.getSumAssured().getAmount();
									}
									ninthRiderTerm = coverage.getRiderTerm();
									
									if(coverage.getPremium() != null && coverage.getPremium().size() > 0){
										for(Premium premium:coverage.getPremium()){
											if(premium.getNatureCode() != null && 
													premium.getNatureCode().name().equalsIgnoreCase("InitialPremium")){
												if(premium.getAmount() != null && premium.getAmount().getAmount() != null){
													ninthRiderInitialPremium = premium.getAmount().getAmount();
												}
													
											}
										}
								}
								}
							}
						}
					}
				}
				/* Payment Details*/
			try{
				if(insuranceAgreement != null){
					if(insuranceAgreement.getAgreementPremium() != null && insuranceAgreement.getAgreementPremium().size() >0){
						for(Premium premium :insuranceAgreement.getAgreementPremium()){
							if(premium.getNatureCode() != null && premium.getNatureCode().name().equalsIgnoreCase("RegularPremium")){
								if(premium.getAttachedFinancialScheduler()!= null && premium.getAttachedFinancialScheduler().size() >0){
									for(FinancialScheduler scheduler :premium.getAttachedFinancialScheduler()){
										receiptNumber = scheduler.getTransactionRefNumber();
									}
								}
							}
						}
					}
				}
			}catch(Exception e){
				LOGGER.error("ScriptletSample Error :while getting payment details"+ e);
			}
				

			}catch(Exception e){
				LOGGER.error("ScriptletSample Error :while getting field value proposer"+ e);
			}

			
			try{
				firstAddInsured = additionalInsured;
				/*for(Insured additionalInsured :additionalInsuredes){
					if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("1")){
						firstAddInsured = additionalInsured;
					}else if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("2")){
						secondAddInsured = additionalInsured;
					}else if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("3")){
						thirdAddInsured = additionalInsured;
					}else if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("4")){
						fourthAddInsured = additionalInsured;
					}
				}*/

				if (null != firstAddInsured) {
					firstAddInsuredPerson = (Person) firstAddInsured.getPlayerParty();
					firstAddInsuredRelationWithProposer = firstAddInsured.getRelationshipWithProposer();
				}
				if (null != secondAddInsured) {
					secondAddInsuredPerson = (Person) secondAddInsured.getPlayerParty();
					secondAddInsuredRelationWithProposer = secondAddInsured.getRelationshipWithProposer();
				}
				if (null != thirdAddInsured) {
					thirdAddInsuredPerson = (Person) thirdAddInsured.getPlayerParty();
					thirdAddInsuredRelationWithProposer = thirdAddInsured.getRelationshipWithProposer();
				}
				if (null != fourthAddInsured) {
					fourthAddInsuredPerson = (Person) fourthAddInsured.getPlayerParty();
					fourthAddInsuredRelationWithProposer = fourthAddInsured.getRelationshipWithProposer();
				}

				if (null != firstAddInsuredPerson) {
					firstAddInsuredDob = firstAddInsuredPerson.getBirthDate();
					firstAddInsuredPersonNames = firstAddInsuredPerson.getName();
					firstAddInsuredPersonDetails = firstAddInsuredPerson.getDetail();
					firstAddInsuredCountries = firstAddInsuredPerson.getNationalityCountry();
					firstAddInsuredResidenceCountry = firstAddInsuredPerson.getResidenceCountry();
					if(firstAddInsuredPerson.getHeight()!= null && !"".equals(firstAddInsuredPerson.getHeight())){
						firstAddInsuredHeight = firstAddInsuredPerson.getHeight().getValue();
					}
					if(firstAddInsuredPerson.getWeight()!= null && !"".equals(firstAddInsuredPerson.getWeight())){
						firstAddInsuredWeight = firstAddInsuredPerson.getWeight().getValue();
					}
					firstAddInsuredIdentityDate = firstAddInsuredPerson.getIdentityDate();
					firstAddInsuredIdentityPlace = firstAddInsuredPerson.getIdentityPlace();
					firstAddnInsuredContactPreferences = firstAddInsuredPerson.getPreferredContact();
					firstAddInsuredTypeOfIdentity = firstAddInsuredPerson.getIdentityProof();
					firstAddInsuredIdentityNo = firstAddInsuredPerson.getNationalId();
				}
				
				if (null != firstAddInsuredPersonDetails) {
					for (PersonDetail personDetail : firstAddInsuredPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							firstAddInsuredIncomeDetail = (IncomeDetail) personDetail;
						} else if (personDetail instanceof EducationDetail) {
							firstAddInsuredEducationDetail = (EducationDetail) personDetail;
						} else
							if (personDetail instanceof OccupationDetail) {
								firstAddInsuredOccupationDetail = (OccupationDetail) personDetail;
							}
					}
				}
				

				if (null != secondAddInsuredPerson) {
					secondAddInsuredDob = secondAddInsuredPerson.getBirthDate();
					secondAddInsuredPersonNames = secondAddInsuredPerson.getName();
					secondAddInsuredPersonDetails = secondAddInsuredPerson.getDetail();
					secondAddInsuredCountries = secondAddInsuredPerson.getNationalityCountry();
					secondAddInsuredResidenceCountry = secondAddInsuredPerson.getResidenceCountry();
					secondAddInsuredIdCardType = secondAddInsuredPerson.getNationalIdType();
					secondAddInsuredIdCardNumber = secondAddInsuredPerson.getNationalId();
					if(secondAddInsuredPerson.getHeight()!= null && !"".equals(secondAddInsuredPerson.getHeight())){
						secondAddInsuredHeight = secondAddInsuredPerson.getHeight().getValue();
					}
					if(secondAddInsuredPerson.getWeight()!= null && !"".equals(secondAddInsuredPerson.getWeight())){
						secondAddInsuredWeight = secondAddInsuredPerson.getWeight().getValue();
					}
					secondAddInsuredIdentityDate = secondAddInsuredPerson.getIdentityDate();
					secondAddInsuredIdentityPlace = secondAddInsuredPerson.getIdentityPlace();
					secondAddnInsuredContactPreferences = secondAddInsuredPerson.getPreferredContact();
					secondAddInsuredTypeOfIdentity = secondAddInsuredPerson.getIdentityProof();
					secondAddInsuredIdentityNo = secondAddInsuredPerson.getNationalId();
				}

				
				if (null != secondAddInsuredPersonDetails) {
					for (PersonDetail personDetail : secondAddInsuredPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							secondAddInsuredIncomeDetail = (IncomeDetail) personDetail;
						} else if (personDetail instanceof EducationDetail) {
							secondAddInsuredEducationDetail = (EducationDetail) personDetail;
						} else
							if (personDetail instanceof OccupationDetail) {
								secondAddInsuredOccupationDetail = (OccupationDetail) personDetail;
							}
					}
				}
				
				
				if (null != thirdAddInsuredPerson) {
					thirdAddInsuredDob = thirdAddInsuredPerson.getBirthDate();
					thirdAddInsuredPersonNames = thirdAddInsuredPerson.getName();
					thirdAddInsuredPersonDetails = thirdAddInsuredPerson.getDetail();
					thirdAddInsuredCountries = thirdAddInsuredPerson.getNationalityCountry();
					thirdAddInsuredResidenceCountry = thirdAddInsuredPerson.getResidenceCountry();
					if(thirdAddInsuredPerson.getHeight()!= null && !"".equals(thirdAddInsuredPerson.getHeight())){
						thirdAddInsuredHeight = thirdAddInsuredPerson.getHeight().getValue();
					}
					if(thirdAddInsuredPerson.getWeight()!= null && !"".equals(thirdAddInsuredPerson.getWeight())){
						thirdAddInsuredWeight = thirdAddInsuredPerson.getWeight().getValue();
					}
					thirdAddInsuredIdentityDate = thirdAddInsuredPerson.getIdentityDate();
					thirdAddInsuredIdentityPlace = thirdAddInsuredPerson.getIdentityPlace();
					thirdAddnInsuredContactPreferences = thirdAddInsuredPerson.getPreferredContact();
					thirdAddInsuredTypeOfIdentity = thirdAddInsuredPerson.getIdentityProof();
					thirdAddInsuredIdentityNo = thirdAddInsuredPerson.getNationalId();
				}

				
				if (null != thirdAddInsuredPersonDetails) {
					for (PersonDetail personDetail : thirdAddInsuredPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							thirdAddInsuredIncomeDetail = (IncomeDetail) personDetail;
						} else if (personDetail instanceof EducationDetail) {
							thirdAddInsuredEducationDetail = (EducationDetail) personDetail;
						} else
							if (personDetail instanceof OccupationDetail) {
								thirdAddInsuredOccupationDetail = (OccupationDetail) personDetail;
							}
					}
				}

				if (null != fourthAddInsuredPerson) {
					fourthAddInsuredDob = fourthAddInsuredPerson.getBirthDate();
					fourthAddInsuredPersonNames = fourthAddInsuredPerson.getName();
					fourthAddInsuredPersonDetails = fourthAddInsuredPerson.getDetail();
					fourthAddInsuredCountries = fourthAddInsuredPerson.getNationalityCountry();
					fourthAddInsuredResidenceCountry = fourthAddInsuredPerson.getResidenceCountry();
					if(fourthAddInsuredPerson.getHeight()!= null && !"".equals(fourthAddInsuredPerson.getHeight())){
						fourthAddInsuredHeight = thirdAddInsuredPerson.getHeight().getValue();
					}
					if(fourthAddInsuredPerson.getWeight()!= null && !"".equals(fourthAddInsuredPerson.getWeight())){
						fourthAddInsuredWeight = thirdAddInsuredPerson.getWeight().getValue();
					}
					fourthAddInsuredIdentityDate = fourthAddInsuredPerson.getIdentityDate();
					fourthAddInsuredIdentityPlace = fourthAddInsuredPerson.getIdentityPlace();
					fourthAddnInsuredContactPreferences = fourthAddInsuredPerson.getPreferredContact();
					fourthAddInsuredTypeOfIdentity = fourthAddInsuredPerson.getIdentityProof();
					fourthAddInsuredIdentityNo = fourthAddInsuredPerson.getNationalId();
				}

				
				if (null != fourthAddInsuredPersonDetails) {
					for (PersonDetail personDetail : fourthAddInsuredPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							fourthAddInsuredIncomeDetail = (IncomeDetail) personDetail;
						} else if (personDetail instanceof EducationDetail) {
							fourthAddInsuredEducationDetail = (EducationDetail) personDetail;
						} else
							if (personDetail instanceof OccupationDetail) {
								fourthAddInsuredOccupationDetail = (OccupationDetail) personDetail;
							}
					}
				}
				
			}catch(Exception ex){
				LOGGER.error("Error in additional insured "+ex.getMessage());
			}
			
			try {
				if (pdfReqInput == null)
					pdfReqInput = GeneraliPDFContentReader.getPDFContent("pdfTemplates/eAppPdfResourceJson.json");
			} catch (Exception e) {
				LOGGER.error("error pdf json."+ e.getMessage());
			}
			
			JSONParser jsonParser = new JSONParser();
			try {
				Object obj1 = jsonParser.parse(pdfReqInput);
				jsonObjectPdfInput = (JSONObject) obj1;
			} catch (Exception e) {
				LOGGER.error("Out of beforeDetailEval " + e.getMessage());
			} 
		
			/** Previous Policy */

			try{
				if(insuredPreviousPolicyList != null && insuredPreviousPolicyList.size() >0){
					LOGGER.error("insuredPreviousPolicyList  "+insuredPreviousPolicyList.size());
					int count = 0;
					for(Insured ins:insuredPreviousPolicyList){						
						if(count == 0){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									firstCompanyName = agmnt.getInsurerName();
									firstPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										firstPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										firstPrePolicyType = agmnt.getAgreementExtension().getRemark();
										firstPrePolicyEffectiveDate = agmnt.getAgreementExtension().getSpajDate();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											firstTypeOfInsurance = coverage.getMarketingName();
											firstPrevSumAssured = coverage.getSumAssured().getAmount();
										}

									}
								}
							}
						}else if(count == 1){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									secondCompanyName = agmnt.getInsurerName();
									secondPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										secondPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										secondPrePolicyType = agmnt.getAgreementExtension().getRemark();
										secondPrePolicyEffectiveDate = agmnt.getAgreementExtension().getSpajDate();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											secondTypeOfInsurance = coverage.getMarketingName();
											secondPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}else if(count == 2){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									thirdCompanyName = agmnt.getInsurerName();
									thirdPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										thirdPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										thirdPrePolicyType = agmnt.getAgreementExtension().getRemark();
										thirdPrePolicyEffectiveDate = agmnt.getAgreementExtension().getSpajDate();

										for(Coverage coverage : agmnt.getIncludesCoverage()){
											thirdTypeOfInsurance = coverage.getMarketingName();
											thirdPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}else if(count == 3){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									fourthCompanyName = agmnt.getInsurerName();
									fourthPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										fourthPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										fourthPrePolicyType = agmnt.getAgreementExtension().getRemark();
										fourthPrePolicyEffectiveDate = agmnt.getAgreementExtension().getSpajDate();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											fourthTypeOfInsurance = coverage.getMarketingName();
											fourthPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}else if(count == 4){}
						count++;
					}
				}
			}catch(Exception e){
				LOGGER.warn("Error in getting previous policy details"+e);
			}
			/** Insured User selection */
			
			try{
				if(insured != null){
					insuredUserSelections =  insured.getSelectedOptions();
					if(insuredUserSelections != null && insuredUserSelections.size() > 0){
						for(UserSelection insUserselections:insuredUserSelections){
							if(insUserselections != null &&
									insUserselections.getQuestionnaireType().name().equalsIgnoreCase("Health")){

								if(insUserselections.getSelectedOption()!=null && insUserselections.getQuestionId() == 1){
									if(insUserselections.getSelectedOption()==true){
										insuredHaveSuspendedPolicies = "Yes";
										insuredHaveSuspendedPoliciesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHaveSuspendedPolicies ="No";
									}
								}else if(insUserselections.getQuestionId() == 2 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHaveDeclainedPolicies = "Yes";
										insuredHaveDeclainedPoliciesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHaveDeclainedPolicies = "No";
									}
								}else if(insUserselections.getQuestionId() == 3 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasEngagedSports = "Yes";
										insuredHasEngagedSportsDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasEngagedSports = "No";
									}
								}else if(insUserselections.getQuestionId() == 4 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasIntensionToTravel = "Yes";
										insuredHasIntensionToTravelDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasIntensionToTravel = "No";
									}
								}else if(insUserselections.getQuestionId() == 5 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasLostWeight = "Yes";
										insuredHasLostWeightDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasLostWeight = "No";
									}
								}else if(insUserselections.getQuestionId() == 6 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasRecevingMedTreatment = "Yes";
										insuredHasRecevingMedTreatment = insUserselections.getDetailedInfo();
									}else{
										insuredHasRecevingMedTreatment = "No";
									}
								}else if(insUserselections.getQuestionId() == 7 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasUsedTobacco = "Yes";
										insuredHasUsedTobaccoDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasUsedTobacco = "No";
									}
								}else if(insUserselections.getQuestionId() == 8 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasUsedDrugs = "Yes";
										insuredHasUsedDrugsDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasUsedDrugs = "No";
									}
								}else if(insUserselections.getQuestionId() == 9 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasUsedAlcoholicBev ="Yes";
										insuredHasUsedAlcoholicBevDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasUsedAlcoholicBev ="No";
									}
								}else if(insUserselections.getQuestionId() == 10 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasUndergoneDiagnosis ="Yes";
										insuredHasUndergoneDiagnosisDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasUndergoneDiagnosis ="No";
									}
								}else if(insUserselections.getQuestionId() == 11 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasCardioDiseases ="Yes";
										insuredHasCardioDiseasesDetails  = insUserselections.getDetailedInfo();
									}else{
										insuredHasCardioDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 12 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasRespiratoryDiseases ="Yes";
										insuredHasRespiratoryDiseasesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasRespiratoryDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 13 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasDigestiveDiseases ="Yes";
										insuredHasDigestiveDiseasesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasDigestiveDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 14 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasUrinaryDiseases ="Yes";
										insuredHasUrinaryDiseasesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasUrinaryDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 15 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasNervousDiseases ="Yes";
										insuredHasNervousDiseasesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasNervousDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 16 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasEndocrineDiseases ="Yes";
										insuredHasEndocrineDiseasesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasEndocrineDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 17 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasSkinDiseases ="Yes";
										insuredHasSkinDiseasesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasSkinDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 18 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasHaematologicDiseases ="Yes";
										insuredHasHaematologicDiseasesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasHaematologicDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 19 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasENTDiseases ="Yes";
										insuredHasENTDiseasesDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasENTDiseases ="No";
									}
								}else if(insUserselections.getQuestionId() == 20 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasTumourOrCancer ="Yes";
										insuredHasTumourOrCancerDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasTumourOrCancer ="No";
									}
								}else if(insUserselections.getQuestionId() == 21 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasDisability ="Yes";
										insuredHasDisabilityDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasDisability ="No";
									}
								}else if(insUserselections.getQuestionId() == 22 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasSexuallyTransmittedDisease ="Yes";
										insuredHasSexuallyTransmittedDiseaseDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasSexuallyTransmittedDisease ="No";
									}
								}else if(insUserselections.getQuestionId() == 23 && insUserselections.getSelectedOption() != null){
										insuredHasWeightAtBirth = insUserselections.getDetailedInfo();
								}else if(insUserselections.getQuestionId() == 24 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasPshycoAbnormality ="Yes";
										insuredHasPshycoAbnormalityDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasPshycoAbnormality ="No";
									}
								}else if(insUserselections.getQuestionId() == 25 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredIsPregnant ="Yes";
										insuredIsPregnantDetails = insUserselections.getDetailedInfo();
									}else{
										insuredIsPregnant ="No";
									}
								}else if(insUserselections.getQuestionId() == 26 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasPregComplication ="Yes";
										insuredHasPregComplicationDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasPregComplication ="No";
									}
								}else if(insUserselections.getQuestionId() == 27 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasDoctor ="Yes";
										insuredHasDoctorDetails = insUserselections.getDetailedInfo();
									}else{
										insuredHasDoctor ="No";
									}
								}
							}else if(insUserselections != null &&
									insUserselections.getQuestionnaireType() != null && insUserselections.getQuestionnaireType().name().equalsIgnoreCase("Lifestyle")){
								if(insUserselections.getQuestionId() == 101 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredHasExistingPolicies = "Yes";
									}else{
										insuredHasExistingPolicies = "No";
									}
								}else if(insUserselections.getQuestionId() == 2 && insUserselections.getSelectedOption() != null){
								/*	if(insUserselections.getSelectedOption() == true){
										insuredDrugsOrNarcotics = "Yes";
									}else{
										insuredDrugsOrNarcotics = "No";
									}*/
								}else if (insUserselections.getQuestionId() == 3 && insUserselections.getSelectedOption() != null){
									/*if(insUserselections.getSelectedOption() == true){
										insuredHazardousActivities = "Yes";
									}
									else{
										insuredHazardousActivities = "No";
									}*/
								}
							}
							else if(insUserselections != null &&
									insUserselections.getQuestionnaireType() != null && insUserselections.getQuestionnaireType().name().equalsIgnoreCase("Other")){
								if (insUserselections.getQuestionId() == 201 && insUserselections.getSelectedOption() != null){
									if(insUserselections.getSelectedOption() == true){
										insuredFamilyMembersDiagWithDiseases = "Yes";
									}else{
										insuredFamilyMembersDiagWithDiseases = "No";
									}
								}
							}
						}
				}
				}
			}catch(Exception e){
				LOGGER.error("Error in getting insured userselection"+e.getMessage());
				e.printStackTrace();
			}
			
			/* Insured family health history*/
			if(insured != null){
				familyHealthHistories = insured.getFamilyHealthHistory();
				if(familyHealthHistories != null && familyHealthHistories.size() >0){
					int count = 0;
					for(FamilyHealthHistory familyHealthHistory:familyHealthHistories){
						if(count == 0){
							firstFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							firstFamilyMemAge= familyHealthHistory.getPresentAgeIfAlive();
							firstFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							firstFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if (count == 1){
							secondFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							secondFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							secondFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							secondFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if(count == 2){
							thirdFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							thirdFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							thirdFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							thirdFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}
					}
				}
			}
		try{
			/*First Additional Insured family health history*/
			if(firstAddInsured != null){
				familyAddFirstHealthHistories = firstAddInsured.getFamilyHealthHistory();
				if(familyAddFirstHealthHistories != null && familyAddFirstHealthHistories.size() >0){
					int count = 0;
					for(FamilyHealthHistory familyHealthHistory:familyAddFirstHealthHistories){
						if(familyHealthHistory != null){
							count = familyHealthHistory.getOrderId();
						if(count == 1){
							firstAddFirstFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							firstAddFirstFamilyMemAge= familyHealthHistory.getPresentAgeIfAlive();
							firstAddFirstFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							firstAddFirstFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if (count == 2){
							secondAddFirstFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							secondAddFirstFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							secondAddFirstFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							secondAddFirstFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if(count == 3){
							thirdAddFirstFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							thirdAddFirstFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							thirdAddFirstFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							thirdAddFirstFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}
						}
					}					
				}
			}
			
			/*Second Additional Insured family health history*/
			if(secondAddInsured != null){
				familyAddSecondHealthHistories = secondAddInsured.getFamilyHealthHistory();
				if(familyAddSecondHealthHistories != null && familyAddSecondHealthHistories.size() >0){
					int count = 0;
					for(FamilyHealthHistory familyHealthHistory:familyAddSecondHealthHistories){
						if(count == 0){
							firstAddSecondFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							firstAddSecondFamilyMemAge= familyHealthHistory.getPresentAgeIfAlive();
							firstAddSecondFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							firstAddSecondFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if (count == 1){
							secondAddSecondFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							secondAddSecondFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							secondAddSecondFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							secondAddSecondFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if(count == 2){
							thirdAddSecondFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							thirdAddSecondFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							thirdAddSecondFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							thirdAddSecondFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}
					}
				}
			}
			
			/*Third Additional Insured family health history*/
			if(thirdAddInsured != null){
				familyAddThirdHealthHistories = thirdAddInsured.getFamilyHealthHistory();
				if(familyAddThirdHealthHistories != null && familyAddThirdHealthHistories.size() >0){
					int count = 0;
					for(FamilyHealthHistory familyHealthHistory:familyAddThirdHealthHistories){
						if(count == 0){
							firstAddThirdFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							firstAddThirdFamilyMemAge= familyHealthHistory.getPresentAgeIfAlive();
							firstAddThirdFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							firstAddThirdFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if (count == 1){
							secondAddThirdFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							secondAddThirdFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							secondAddThirdFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							secondAddThirdFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if(count == 2){
							thirdAddThirdFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							thirdAddThirdFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							thirdAddThirdFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							thirdAddThirdFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}
					}
				}
			}
			
			/*Fourth Additional Insured family health history*/
			if(fourthAddInsured != null){
				familyAddFourthHealthHistories = fourthAddInsured.getFamilyHealthHistory();
				if(familyAddFourthHealthHistories != null && familyAddFourthHealthHistories.size() >0){
					int count = 0;
					for(FamilyHealthHistory familyHealthHistory:familyAddFourthHealthHistories){
						if(count == 0){
							firstAddFourthFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							firstAddFourthFamilyMemAge= familyHealthHistory.getPresentAgeIfAlive();
							firstAddFourthFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							firstAddFourthFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if (count == 1){
							secondAddFourthFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							secondAddFourthFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							secondAddFourthFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							secondAddFourthFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}else if(count == 2){
							thirdAddFourthFamilyMemAgeAtDeath = familyHealthHistory.getAgeAtDeath();
							thirdAddFourthFamilyMemAge = familyHealthHistory.getPresentAgeIfAlive();
							thirdAddFourthFamilyMemCauseOfDeath = familyHealthHistory.getCauseOfDeathOrIllness();
							thirdAddFourthFamilyMemRelation = familyHealthHistory.getFamilyMember();
						}
					}
				}
			}
			
			// Additional Insured One User Selection
			
			try{
				if(firstAddInsured != null){
					addInsuredOneUserSelections = firstAddInsured.getSelectedOptions();
					if(addInsuredOneUserSelections != null && addInsuredOneUserSelections.size() > 0){
						for(UserSelection addInsUserselections:addInsuredOneUserSelections){
							if(addInsUserselections != null &&
									addInsUserselections.getQuestionnaireType().name().equalsIgnoreCase("Health")){

								if(addInsUserselections.getSelectedOption()!=null && addInsUserselections.getQuestionId() == 1010){
									if(addInsUserselections.getSelectedOption()==true){
										addInsuredOneHaveSuspendedPolicies = "Yes";
										addInsuredOneHaveSuspendedPoliciesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHaveSuspendedPoliciesDetails != null && !"".equalsIgnoreCase(addInsuredOneHaveSuspendedPoliciesDetails)){
											addInsuredOneHaveSuspendedPoliciesDetails = "\n"+"3a) " +addInsuredOneHaveSuspendedPoliciesDetails;
										}
									}else{
										addInsuredOneHaveSuspendedPolicies ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1003 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHaveDeclainedPolicies = "Yes";
										addInsuredOneHaveDeclainedPoliciesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHaveDeclainedPoliciesDetails != null && !"".equalsIgnoreCase(addInsuredOneHaveDeclainedPoliciesDetails)){
											addInsuredOneHaveDeclainedPoliciesDetails = "\n"+"b) "+addInsuredOneHaveDeclainedPoliciesDetails;
										}
									}else{
										addInsuredOneHaveDeclainedPolicies = "No";
									}
								}else if(addInsUserselections.getQuestionId() == 1004 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasEngagedSports = "Yes";
										addInsuredOneHasEngagedSportsDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasEngagedSportsDetails != null && !"".equalsIgnoreCase(addInsuredOneHasEngagedSportsDetails)){
											addInsuredOneHasEngagedSportsDetails = "\n"+"4) "+addInsuredOneHasEngagedSportsDetails;
										}
									}else{
										addInsuredOneHasEngagedSports = "No";
									}
								}else if(addInsUserselections.getQuestionId() == 1012 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasIntensionToTravel = "Yes";
										addInsuredOneHasIntensionToTravelDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasIntensionToTravelDetails != null && !"".equalsIgnoreCase(addInsuredOneHasIntensionToTravelDetails)){
											addInsuredOneHasIntensionToTravelDetails = "\n"+"5) "+addInsuredOneHasIntensionToTravelDetails;
										}
									}else{
										addInsuredOneHasIntensionToTravel = "No";
									}
								}else if(addInsUserselections.getQuestionId() == 1005 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasLostWeight = "Yes";
										addInsuredOneHasLostWeightDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasLostWeightDetails != null && !"".equalsIgnoreCase(addInsuredOneHasLostWeightDetails)){
											addInsuredOneHasLostWeightDetails = "\n"+"6) "+addInsuredOneHasLostWeightDetails;
										}
									}else{
										addInsuredOneHasLostWeight = "No";
									}
								}else if(addInsUserselections.getQuestionId() == 1006 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasRecevingMedTreatment = "Yes";
										addInsuredOneHasRecevingMedTreatmentDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasRecevingMedTreatmentDetails != null && !"".equalsIgnoreCase(addInsuredOneHasRecevingMedTreatmentDetails)){
											addInsuredOneHasRecevingMedTreatmentDetails = "\n"+"7) "+addInsuredOneHasRecevingMedTreatmentDetails;
										}
									}else{
										addInsuredOneHasRecevingMedTreatment = "No";
									}
								}else if(addInsUserselections.getQuestionId() == 1013 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasUsedTobacco = "Yes";
										addInsuredOneHasUsedTobaccoDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasUsedTobaccoDetails != null && !"".equalsIgnoreCase(addInsuredOneHasUsedTobaccoDetails)){
											addInsuredOneHasUsedTobaccoDetails = "\n"+"8) "+addInsuredOneHasUsedTobaccoDetails;
										}
									}else{
										addInsuredOneHasUsedTobacco = "No";
									}
								}else if(addInsUserselections.getQuestionId() == 1007 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasUsedDrugs = "Yes";
										addInsuredOneHasUsedDrugsDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasUsedDrugsDetails != null && !"".equalsIgnoreCase(addInsuredOneHasUsedDrugsDetails)){
											addInsuredOneHasUsedDrugsDetails = "\n"+"9) "+addInsuredOneHasUsedDrugsDetails;
										}
									}else{
										addInsuredOneHasUsedDrugs = "No";
									}
								}else if(addInsUserselections.getQuestionId() == 1008 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasUsedAlcoholicBev ="Yes";
										addInsuredOneHasUsedAlcoholicBevDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasUsedAlcoholicBevDetails != null && !"".equalsIgnoreCase(addInsuredOneHasUsedAlcoholicBevDetails)){
											addInsuredOneHasUsedAlcoholicBevDetails = "\n"+"10) "+addInsuredOneHasUsedAlcoholicBevDetails;
										}
									}else{
										addInsuredOneHasUsedAlcoholicBev ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1009 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasUndergoneDiagnosis ="Yes";
										addInsuredOneHasUndergoneDiagnosisDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasUndergoneDiagnosisDetails != null && !"".equalsIgnoreCase(addInsuredOneHasUndergoneDiagnosisDetails)){
											addInsuredOneHasUndergoneDiagnosisDetails = "\n"+"11) "+addInsuredOneHasUndergoneDiagnosisDetails;
										}
									}else{
										addInsuredOneHasUndergoneDiagnosis ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1014 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasCardioDiseases ="Yes";
										addInsuredOneHasCardioDiseasesDetails  = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasCardioDiseasesDetails != null && !"".equalsIgnoreCase(addInsuredOneHasCardioDiseasesDetails)){
											addInsuredOneHasCardioDiseasesDetails = "\n"+"12 a) "+addInsuredOneHasCardioDiseasesDetails;
										}
									}else{
										addInsuredOneHasCardioDiseases ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1015 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasRespiratoryDiseases ="Yes";
										addInsuredOneHasRespiratoryDiseasesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasRespiratoryDiseasesDetails != null && !"".equalsIgnoreCase(addInsuredOneHasRespiratoryDiseasesDetails)){
											addInsuredOneHasRespiratoryDiseasesDetails = "\n"+"12 b) "+addInsuredOneHasRespiratoryDiseasesDetails;
										}
									}else{
										addInsuredOneHasRespiratoryDiseases ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1016 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasDigestiveDiseases ="Yes";
										addInsuredOneHasDigestiveDiseasesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasDigestiveDiseasesDetails != null && !"".equalsIgnoreCase(addInsuredOneHasDigestiveDiseasesDetails)){
											addInsuredOneHasDigestiveDiseasesDetails = "\n"+"12 c) "+addInsuredOneHasDigestiveDiseasesDetails;
										}
									}else{
										addInsuredOneHasDigestiveDiseases ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1017 && addInsUserselections.getSelectedOption() != null){
									
									addInsuredOneHasUrinaryDiseasesDetailsOptions = "";
									StringBuilder sb=new StringBuilder();  
									String vietValue = "";
									
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasUrinaryDiseases ="Yes";
										addInsuredOneHasUrinaryDiseasesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasUrinaryDiseasesDetails != null && !"".equalsIgnoreCase(addInsuredOneHasUrinaryDiseasesDetails)){
											addInsuredOneHasUrinaryDiseasesDetails = "\n"+"12 d) "+addInsuredOneHasUrinaryDiseasesDetails;
										}
										if(addInsUserselections.getSelectedOptions() != null && addInsUserselections.getSelectedOptions().size() >0){
											for(GLISelectedOption option:addInsUserselections.getSelectedOptions()){
												if(option != null && option.getSelectedOptionValue() != null
															&& !"".equalsIgnoreCase(option.getSelectedOptionValue()) && 
															option.getSelectedOptionValue().equalsIgnoreCase("Yes")){
													
													if(option.getSelectedOptionKey() != null && !"".equalsIgnoreCase(option.getSelectedOptionKey())){
														if (jsonObjectPdfInput.containsKey(option.getSelectedOptionKey())) {
															vietValue = jsonObjectPdfInput.get(option.getSelectedOptionKey()).toString();
														}
												}
													
													sb.append(vietValue);
													sb.append(",");
												}
											}
										}
										if(sb != null && sb.length() >0){
											addInsuredOneHasUrinaryDiseasesDetailsOptions = sb.toString().substring(0, sb.length() -1);
										}
									}else{
										addInsuredOneHasUrinaryDiseases ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1018 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasNervousDiseases ="Yes";
										addInsuredOneHasNervousDiseasesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasNervousDiseasesDetails != null && !"".equalsIgnoreCase(addInsuredOneHasNervousDiseasesDetails)){
											addInsuredOneHasNervousDiseasesDetails = "\n"+"12 e) "+addInsuredOneHasNervousDiseasesDetails;
										}
									}else{
										addInsuredOneHasNervousDiseases ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1019 && addInsUserselections.getSelectedOption() != null){
									StringBuilder sb=new StringBuilder(); 
									String vietValue = "";
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasEndocrineDiseases ="Yes";
										addInsuredOneHasEndocrineDiseasesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasEndocrineDiseasesDetails != null && !"".equalsIgnoreCase(addInsuredOneHasEndocrineDiseasesDetails)){
											addInsuredOneHasEndocrineDiseasesDetails = "\n"+"12 f) "+addInsuredOneHasEndocrineDiseasesDetails;
										}
										if(addInsUserselections.getSelectedOptions() != null && addInsUserselections.getSelectedOptions().size() >0){
											for(GLISelectedOption option:addInsUserselections.getSelectedOptions()){
												if(option != null && option.getSelectedOptionValue() != null
															&& !"".equalsIgnoreCase(option.getSelectedOptionValue()) && 
															option.getSelectedOptionValue().equalsIgnoreCase("Yes")){
													
													if(option.getSelectedOptionKey() != null && !"".equalsIgnoreCase(option.getSelectedOptionKey())){
														if (jsonObjectPdfInput.containsKey(option.getSelectedOptionKey())) {
															vietValue = jsonObjectPdfInput.get(option.getSelectedOptionKey()).toString();
														}
												}
													
													sb.append(vietValue);
													sb.append(",");
												}
											}
										}
										if(sb != null && sb.length() >0){
											addInsuredOneHasEndocrineDiseasesDetailsOptions = sb.toString().substring(0, sb.length() -1);
										}
									}else{
										addInsuredOneHasEndocrineDiseases ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1020 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasSkinDiseases ="Yes";
										addInsuredOneHasSkinDiseasesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasSkinDiseasesDetails != null && !"".equalsIgnoreCase(addInsuredOneHasSkinDiseasesDetails)){
											addInsuredOneHasSkinDiseasesDetails = "\n"+"12 g) "+addInsuredOneHasSkinDiseasesDetails;
										}
									}else{
										addInsuredOneHasSkinDiseases ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1021 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasHaematologicDiseases ="Yes";
										addInsuredOneHasHaematologicDiseasesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasHaematologicDiseasesDetails != null && !"".equalsIgnoreCase(addInsuredOneHasHaematologicDiseasesDetails)){
											addInsuredOneHasHaematologicDiseasesDetails = "\n"+"12 h) "+addInsuredOneHasHaematologicDiseasesDetails;
										}
									}else{
										addInsuredOneHasHaematologicDiseases ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1022 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasENTDiseases ="Yes";
										addInsuredOneHasENTDiseasesDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasENTDiseasesDetails != null && !"".equalsIgnoreCase(addInsuredOneHasENTDiseasesDetails)){
											addInsuredOneHasENTDiseasesDetails = "\n"+"12 i) "+addInsuredOneHasENTDiseasesDetails;
										}
									}else{
										addInsuredOneHasENTDiseases ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1023 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasTumourOrCancer ="Yes";
										addInsuredOneHasTumourOrCancerDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasTumourOrCancerDetails != null && !"".equalsIgnoreCase(addInsuredOneHasTumourOrCancerDetails)){
											addInsuredOneHasTumourOrCancerDetails = "\n"+"12 j) "+addInsuredOneHasTumourOrCancerDetails;
										}
									}else{
										addInsuredOneHasTumourOrCancer ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1024 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasDisability ="Yes";
										addInsuredOneHasDisabilityDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasDisabilityDetails != null && !"".equalsIgnoreCase(addInsuredOneHasDisabilityDetails)){
											addInsuredOneHasDisabilityDetails = "\n"+"12 k) "+addInsuredOneHasDisabilityDetails;
										}
									}else{
										addInsuredOneHasDisability ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1025 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasSexuallyTransmittedDisease ="Yes";
										addInsuredOneHasSexuallyTransmittedDiseaseDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasSexuallyTransmittedDiseaseDetails != null && !"".equalsIgnoreCase(addInsuredOneHasSexuallyTransmittedDiseaseDetails)){
											addInsuredOneHasSexuallyTransmittedDiseaseDetails = "\n"+"12 l) "+addInsuredOneHasSexuallyTransmittedDiseaseDetails;
										}
									}else{
										addInsuredOneHasSexuallyTransmittedDisease ="No";
									}
								}else if(addInsUserselections.getQuestionId() == 1026){
										addInsuredOneHasWeightAtBirth = addInsUserselections.getDetailedInfo();
								}else if(addInsUserselections.getQuestionId() == 1027 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasPshycoAbnormality ="Yes";
										addInsuredOneHasPshycoAbnormalityDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasPshycoAbnormalityDetails != null && !"".equalsIgnoreCase(addInsuredOneHasPshycoAbnormalityDetails)){
											addInsuredOneHasPshycoAbnormalityDetails = "\n"+"13 b) "+addInsuredOneHasPshycoAbnormalityDetails;
										}
									}else if (addInsUserselections.getSelectedOption() == false){
										addInsuredOneHasPshycoAbnormality ="No";
									}
									else{
										addInsuredOneHasPshycoAbnormality ="";
									}
								}else if(addInsUserselections.getQuestionId() == 1028 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneIsPregnant ="Yes";
										addInsuredOneIsPregnantDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneIsPregnantDetails != null && !"".equalsIgnoreCase(addInsuredOneIsPregnantDetails)){
											addInsuredOneIsPregnantDetails = "\n"+"14 a) "+addInsuredOneIsPregnantDetails;
										}
									}else if (addInsUserselections.getSelectedOption() == false){
										addInsuredOneIsPregnant ="No";
									}
									else{
										addInsuredOneIsPregnant ="";
									}
								}else if(addInsUserselections.getQuestionId() == 1029 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasPregComplication ="Yes";
										addInsuredOneHasPregComplicationDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasPregComplicationDetails != null && !"".equalsIgnoreCase(addInsuredOneHasPregComplicationDetails)){
											addInsuredOneHasPregComplicationDetails = "\n"+"14 b) "+addInsuredOneHasPregComplicationDetails;
										}
									}else if(addInsUserselections.getSelectedOption() == false){
										addInsuredOneHasPregComplication ="No";
									}
									else{
										addInsuredOneHasPregComplication ="";
									}
								}else if(addInsUserselections.getQuestionId() == 1011 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredOneHasDoctor ="Yes";
										addInsuredOneHasDoctorDetails = addInsUserselections.getDetailedInfo();
										if(addInsuredOneHasDoctorDetails != null && !"".equalsIgnoreCase(addInsuredOneHasDoctorDetails)){
											addInsuredOneHasDoctorDetails = "\n"+"15 ) "+addInsuredOneHasDoctorDetails;
										}
									}else{
										addInsuredOneHasDoctor ="No";
									}
								}
							}else if(addInsUserselections != null &&
									addInsUserselections.getQuestionnaireType() != null && addInsUserselections.getQuestionnaireType().name().equalsIgnoreCase("Lifestyle")){
								if(addInsUserselections.getQuestionId() == 1001 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredHasExistingPolicies = "Yes";
									}else{
										addInsuredHasExistingPolicies = "No";
									}
								}else if(addInsUserselections.getQuestionId() == 2 && addInsUserselections.getSelectedOption() != null){
								/*	if(insUserselections.getSelectedOption() == true){
										insuredDrugsOrNarcotics = "Yes";
									}else{
										insuredDrugsOrNarcotics = "No";
									}*/
								}else if (addInsUserselections.getQuestionId() == 3 && addInsUserselections.getSelectedOption() != null){
									/*if(insUserselections.getSelectedOption() == true){
										insuredHazardousActivities = "Yes";
									}
									else{
										insuredHazardousActivities = "No";
									}*/
								}
							}
							else if(addInsUserselections != null &&
									addInsUserselections.getQuestionnaireType() != null && addInsUserselections.getQuestionnaireType().name().equalsIgnoreCase("Other")){
								if (addInsUserselections.getQuestionId() == 1002 && addInsUserselections.getSelectedOption() != null){
									if(addInsUserselections.getSelectedOption() == true){
										addInsuredFamilyMembersDiagWithDiseases = "Yes";
									}else{
										addInsuredFamilyMembersDiagWithDiseases = "No";
									}
								}
							}
						}
				}
				}
			}catch(Exception e){
				LOGGER.error("Error in getting additional insured one userselection"+e.getMessage());
				e.printStackTrace();
			}
					
			/* Declaration For ACR*/
			if(proposal != null && proposal.getAgreementExtension() != null &&
					 proposal.getAgreementExtension().getSelectedOptions() != null){
				for(UserSelection selection : proposal.getAgreementExtension().getSelectedOptions()){
					if(selection != null &&
							selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
						if(selection.getSelectedOption()!=null && selection.getQuestionId() == 1){
							if(selection.getSelectedOption()==true){
								acrQuest1 = "Yes";
								acrQuest1Details = selection.getDetailedInfo();
							}else{
								acrQuest1 ="No";
							}
						}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest2 = "Yes";
								acrQuest2Details = selection.getDetailedInfo();
							}else{
								acrQuest2 = "No";
							}
						}else if(selection.getQuestionId() == 3 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest3 = "Yes";
								acrQuest3Details = selection.getDetailedInfo();
							}else{
								acrQuest3 = "No";
							}
						}else if(selection.getQuestionId() == 4 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest4 = "Yes";
								acrQuest4Details = selection.getDetailedInfo();
							}else{
								acrQuest4 = "No";
							}
						}else if(selection.getQuestionId() == 5 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest5 = "Yes";
								acrQuest5Details = selection.getDetailedInfo();
							}else{
								acrQuest5 = "No";
							}
						}else if(selection.getQuestionId() == 6 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest6 = "Yes";
								acrQuest6Details = selection.getDetailedInfo();
								if(selection.getSelectedOptions() != null){
									for(GLISelectedOption option:selection.getSelectedOptions()){
										if(option != null && option.getSelectedOptionId() ==1){
											acrQuesr6Opt1 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==2){
											acrQuesr6Opt2 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==3){
											acrQuesr6Opt3 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==4){
											acrQuesr6Opt4 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==5){
											acrQuesr6Opt5 = option.getSelectedOptionValue();
										}
									}
								}
							}else{
								acrQuest6 = "No";
							}
						}else if(selection.getQuestionId() == 7 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest7 = "Yes";
								acrQuest7Details = selection.getDetailedInfo();
								if(selection.getSelectedOptions() != null){
									for(GLISelectedOption option:selection.getSelectedOptions()){
										if(option != null && option.getSelectedOptionId() ==1){
											acrQuesr7Opt1 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==2){
											acrQuesr7Opt2 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==3){
											acrQuesr7Opt3 = option.getSelectedOptionValue();
										}
									}
								}
							}else{
								acrQuest7 = "No";
							}
						}else if(selection.getQuestionId() == 8 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest8 = "Yes";
								acrQuest8Details = selection.getDetailedInfo();
							}else{
								acrQuest8 = "No";
							}
						}else if(selection.getQuestionId() == 9 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest9 = "Yes";
								acrQuest9Details = selection.getDetailedInfo();
							}else{
								acrQuest9 = "No";
							}
						}else if(selection.getQuestionId() == 10 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest10 = "Yes";
								acrQuest10Details = selection.getDetailedInfo();
							}else{
								acrQuest10 = "No";
							}
						}else if(selection.getQuestionId() == 11 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest11 = "Yes";
								acrQuest11Details = selection.getDetailedInfo();
							}else{
								acrQuest11 = "No";
							}
						}else if(selection.getQuestionId() == 12 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest12 = "Yes";
								acrQuest12Details = selection.getDetailedInfo();
								if(selection.getSelectedOptions() != null){
									for(GLISelectedOption option:selection.getSelectedOptions()){
										if(option != null && option.getSelectedOptionId() ==1){
											acrQuesr12Opt1 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==2){
											acrQuesr12Opt2 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==3){
											acrQuesr12Opt3 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==4){
											acrQuesr12Opt4 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==5){
											acrQuesr12Opt5 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==6){
											acrQuesr12Opt6 = option.getSelectedOptionValue();
										}
									}
								}
							}else{
								acrQuest12 = "No";
							}
						}else if(selection.getQuestionId() == 13 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest13 = "Yes";
								acrQuest13Details = selection.getDetailedInfo();
							}else{
								acrQuest13 = "No";
							}
						}else if(selection.getQuestionId() == 14 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest14 = "Yes";
								acrQuest14Details = selection.getDetailedInfo();
							}else{
								acrQuest14 = "No";
							}
						}else if(selection.getQuestionId() == 16 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest16 = "Yes";
								acrQuest16Details = selection.getDetailedInfo();
								if(selection.getSelectedOptions() != null){
									for(GLISelectedOption option:selection.getSelectedOptions()){
										if(option != null && option.getSelectedOptionId() ==1){
											acrQuesr16Opt1 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==2){
											acrQuesr16Opt2 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==3){
											acrQuesr16Opt3 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==4){
											acrQuesr16Opt4 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==5){
											acrQuesr16Opt5 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==6){
											acrQuesr16Opt6 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==7){
											acrQuesr16Opt7 = option.getSelectedOptionValue();
										}
									}
								}
							}else{
								acrQuest16 = "No";
							}
						}else if(selection.getQuestionId() == 17 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest17 = "Yes";
								acrQuest17Details = selection.getDetailedInfo();
							}else{
								acrQuest17 = "No";
							}
						}else if(selection.getQuestionId() == 18 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest18 = "Yes";
								acrQuest18Details = selection.getDetailedInfo();
							}else{
								acrQuest18 = "No";
							}
						}else if(selection.getQuestionId() == 19 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest19 = "Yes";
								acrQuest19Details = selection.getDetailedInfo();
							}else{
								acrQuest19 = "No";
							}
						}else if(selection.getQuestionId() == 20 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest20 = "Yes";
								acrQuest20Details = selection.getDetailedInfo();
								if(selection.getSelectedOptions() != null){
									for(GLISelectedOption option:selection.getSelectedOptions()){
										if(option != null && option.getSelectedOptionId() ==1){
											acrQuesr20Opt1 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==2){
											acrQuesr20Opt2 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==3){
											acrQuesr20Opt3 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==4){
											acrQuesr20Opt4 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==5){
											acrQuesr20Opt5 = option.getSelectedOptionValue();
										}else if (option != null && option.getSelectedOptionId() ==6){
											setAcrQuesr20Opt6(option.getSelectedOptionValue());
										}
									}
								}
							}else{
								acrQuest20 = "No";
							}
						}else if(selection.getQuestionId() == 21 && selection.getSelectedOption() != null){
							if(selection.getSelectedOption() == true){
								acrQuest21 = "Yes";
								acrQuest21Details = selection.getDetailedInfo();
							}else{
								acrQuest21 = "No";
							}
						}
					}
				}
			}
			}catch(Exception e){
				LOGGER.error("-- Declaration questions--");
			}
			
			
			/* Agent Details*/
			try{
				if(agent != null){
					agentPerson = (Person)agent.getPlayerParty();
					//agentCode = agent.getPartyIdentifier();
				}
			}catch(Exception e){
				LOGGER.error("Error while getting agent details : "+e.getMessage());
			}
			
			try{
				if(insured != null && insured.getDeclares() != null){
					insuredDeclarationDateTime = insured.getDeclares().getActualTimePeriod();
					insuredDeclarationPlace = insured.getDeclares().getName();
				}	
			}catch(Exception e){
				LOGGER.error("Error while getting insured declaration details : "+e.getMessage());
			}
			
			try{
				if(proposer != null && proposer.getDeclares() != null){
					proposerDeclarationDateTime = proposer.getDeclares().getActualTimePeriod();
					proposerDeclarationPlace = proposer.getDeclares().getName();
				}	
			}catch(Exception e){
				LOGGER.error("Error while getting proposer declaration details : "+e.getMessage());
			}
			
			try{
				if(firstAddInsured != null && firstAddInsured.getDeclares() != null){
					additionalInsDeclarationDateTime = firstAddInsured.getDeclares().getActualTimePeriod();
					additionalInsDeclarationPlace = firstAddInsured.getDeclares().getName();
				}	
			}catch(Exception e){
				LOGGER.error("Error while getting proposer declaration details : "+e.getMessage());
			}
			
		} catch (Exception e) {
			LOGGER.warn("Error in beforeDetailEval"+e);
		}
		
	}
	//****************************************************************************//
	
	public void populateCP() {
		for (ContactPreference contactPreference : contactPreferences) {
			ContactPoint contactPoint = contactPreference
					.getPreferredContactPoint();
			if (contactPoint instanceof PostalAddressContact) {
				if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Current)) {
					postalAddressContact = (PostalAddressContact) contactPoint;
				} else if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Permanent)) {
					permanentAddress = (PostalAddressContact) contactPoint;
				}

			}else if (contactPoint instanceof TelephoneCallContact) {
						telephoneCallContact = (TelephoneCallContact) contactPoint;
						if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
							insuredMobileNumber1 = telephoneCallContact.getFullNumber();
						} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
							insuredHomeNumber1 = telephoneCallContact.getFullNumber();
						}else if(telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Business)){
							insuredOfficeNumber  = telephoneCallContact.getFullNumber();
						}
			} else if (contactPoint instanceof ElectronicContact) {
				electronicContact = (ElectronicContact) contactPoint;
			}
		}
	}
	
	/** proposer contact details */
	public void populateProposerCP() {
		try {
			for (ContactPreference contactPreference : proposerContactPreferences) {
				ContactPoint contactPoint = contactPreference.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Current)) {
						proposerPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Permanent)) {
						proposerPermanentAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
							telephoneCallContact = (TelephoneCallContact) contactPoint;
							if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
								proposerMobileNumber1 = telephoneCallContact.getFullNumber();
							} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
								proposerHomeNumber1 = telephoneCallContact.getFullNumber();
							}else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Business)) {
								proposerOfficeNumber = telephoneCallContact.getFullNumber();
							}
				} else if (contactPoint instanceof ElectronicContact) {
					proposerElectronicContact = (ElectronicContact) contactPoint;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :proposerContactPreferences"+ e.getStackTrace());
		}
	}
	
	public void populateFirstAddnInsuredCP() {
		try {
			for (ContactPreference contactPreference : firstAddnInsuredContactPreferences) {
				ContactPoint contactPoint = contactPreference.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Current)) {
						firstAddnInsuredPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Permanent)) {
						firstAddnInsuredPermanentAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
							telephoneCallContact = (TelephoneCallContact) contactPoint;
							if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
								firstAddnInsuredMobileNumber1 = telephoneCallContact.getFullNumber();
							} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
								firstAddnInsuredHomeNumber1 = telephoneCallContact.getFullNumber();
							}else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Business)) {
								firstAddnInsuredOfficeNumber = telephoneCallContact.getFullNumber();
							}
				} else if (contactPoint instanceof ElectronicContact) {
					firstAddnInsuredElectronicContact = (ElectronicContact) contactPoint;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :proposerContactPreferences"+ e.getStackTrace());
		}
	}
	
	
	public void populateSecondAddnInsuredCP() {
		try {
			for (ContactPreference contactPreference : secondAddnInsuredContactPreferences) {
				ContactPoint contactPoint = contactPreference.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Current)) {
						secondAddnInsuredPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Permanent)) {
						secondAddnInsuredPermanentAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
							telephoneCallContact = (TelephoneCallContact) contactPoint;
							if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
								secondAddnInsuredMobileNumber1 = telephoneCallContact.getFullNumber();
							} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
								secondAddnInsuredHomeNumber1 = telephoneCallContact.getFullNumber();
							}else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Business)) {
								secondAddnInsuredOfficeNumber = telephoneCallContact.getFullNumber();
							}
				} else if (contactPoint instanceof ElectronicContact) {
					secondAddnInsuredElectronicContact = (ElectronicContact) contactPoint;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :proposerContactPreferences"+ e.getStackTrace());
		}
	}
	
	public void populateThirdAddnInsuredCP() {
		try {
			for (ContactPreference contactPreference : thirdAddnInsuredContactPreferences) {
				ContactPoint contactPoint = contactPreference.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Current)) {
						thirdAddnInsuredPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Permanent)) {
						thirdAddnInsuredPermanentAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
							telephoneCallContact = (TelephoneCallContact) contactPoint;
							if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
								thirdAddnInsuredMobileNumber1 = telephoneCallContact.getFullNumber();
							} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
								thirdAddnInsuredHomeNumber1 = telephoneCallContact.getFullNumber();
							}else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Business)) {
								thirdAddnInsuredOfficeNumber = telephoneCallContact.getFullNumber();
							}
				} else if (contactPoint instanceof ElectronicContact) {
					thirdAddnInsuredElectronicContact = (ElectronicContact) contactPoint;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :proposerContactPreferences"+ e.getStackTrace());
		}
	}
	
	public void populateFourthAddnInsuredCP() {
		try {
			for (ContactPreference contactPreference : fourthAddnInsuredContactPreferences) {
				ContactPoint contactPoint = contactPreference.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Current)) {
						fourthAddnInsuredPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Permanent)) {
						fourthAddnInsuredPermanentAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
							telephoneCallContact = (TelephoneCallContact) contactPoint;
							if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
								fourthAddnInsuredMobileNumber1 = telephoneCallContact.getFullNumber();
							} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
								fourthAddnInsuredHomeNumber1 = telephoneCallContact.getFullNumber();
							}else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Business)) {
								fourthAddnInsuredOfficeNumber = telephoneCallContact.getFullNumber();
							}
				} else if (contactPoint instanceof ElectronicContact) {
					fourthAddnInsuredElectronicContact = (ElectronicContact) contactPoint;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :proposerContactPreferences"+ e.getStackTrace());
		}
	}
	
	/* Main Insured Details*/
	
	public String getInsuredFullName() throws JRScriptletException {
		String name = null;
		String fname = null;
		String lname = null;
		for (PersonName personName : personNames) {
			fname = personName.getGivenName();
			lname = personName.getSurname();
			name = createFullName(fname, lname);
			if (name != null && !"".equals(name)) {
				name = name.toUpperCase();
			}
		}
		return name;
	}
	public Date getInsuredDob() {
		return insuredDob;
	}
	public String getInsuredYearOfBirth(){
		String year = "";
		if(isPropDiffFromInsured){
			try{
				Date insDob = getInsuredDob();
				String insDobValue = insDob.toString();
				if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
					year = insDobValue.substring(0, 4);
				}
			}catch(Exception e){
				LOGGER.error("Error while getting proposer year of birth ");
			}
		}
		return year;
	}

	public String getInsuredMonthOfBirth(){
		String month = "";
		if(isPropDiffFromInsured){
			try{
				Date insDob = getInsuredDob();
				String insDobValue = insDob.toString();
				if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
					month = insDobValue.substring(5, 7);
				}
			}catch(Exception e){
				LOGGER.error("Error while getting insured month of birth ");
			}
		}
		return month;
	}

	public String getInsuredDayOfBirth(){
		String date = "";
		if(isPropDiffFromInsured){
			try{
				Date insDob = getInsuredDob();
				String insDobValue = insDob.toString();
				if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
					date = insDobValue.substring(8, 10);
				}
			}catch(Exception e){
				LOGGER.error("Error while getting insured day of birth ");
			}
		}
		return date;
	}
	
	public String getInsuredGender() {
		return person.getGenderCode().name();
	}
	
	public String getInsuredNationality() {
		String nationality = "";
			for (Country country : countries) {
				nationality = country.getName();
			}
		return nationality;
	}
	
	
	public String getInsuredMaritalStatus() {
		return person.getMaritalStatusCode().name();
	}
	
	public BigDecimal getInsuredHeight() {
		return insuredHeight;
	}
	public BigDecimal getInsuredWeight() {
		return insuredWeight;
	}
	
	public String getInsuredEmail() {
		return electronicContact.getEmailAddress();
	}
	
	public String getInsuredMobileNumber() {
			populateCP();
			return insuredMobileNumber1;
	}
	
	public String getInsuredOfficeNumber() {
		return insuredOfficeNumber;
	}

	public String getInsuredHomeNumber() {
			populateCP();
			return insuredHomeNumber1;
	}
	public String getIdentityNo() {
		return identityNo;
	}

	public Date getInsuredIdentityDate() {
		return insuredIdentityDate;
	}

	public String getInsuredIdentityPlace() {
		return insuredIdentityPlace;
	}
	
	public String getInsuredPerStreetName() {
		return permanentAddress.getStreetName();
	}
	public String getInsuredPerHouseNo() {
		return permanentAddress.getBoxNumber();
	}
	
	public String getInsuredPermWard() {
		return permanentAddress.getAddressLine2();
	}
	public String getInsuredPermDistrict() {
		return permanentAddress.getAddressLine1();
	}
	
	public String getInsuredPermCity() {
		Set<CountryElement> countryElements = permanentAddress
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}
	
	public String getInsuredCurrentStreetName() {
		return postalAddressContact.getStreetName();
	}
	public String getInsuredCurrentHouseNo() {
		return postalAddressContact.getBoxNumber();
	}
	
	public String getInsuredCurrentWard() {
		return postalAddressContact.getAddressLine2();
	}
	public String getInsuredCurrentDistrict() {
		return postalAddressContact.getAddressLine1();
	}
	
	public String getInsuredCurrentCity() {
		Set<CountryElement> countryElements = postalAddressContact
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}
	
	public String getInsuredNameOfComapny() {
		return occupationDetail.getNameofInstitution();
	}

	public String getInsuredAddressOfComapny() {
		return postalAddressContact.getAddressLine3();
	}

	public String getInsuredOccupation() {
		return occupationDetail.getOccupationClass();
	}

	
	public String getInsuredNatureOfWork() {
		return occupationDetail.getNatureofWork();
	}
	
	public String getInsuredAnnualIncome() {
		return incomeDetail.getGrossIncome();
	}
	public String getInsuredBirthPlace(){
		String resCountry = "";
			if(insuredResidenceCountry != null){
				resCountry = insuredResidenceCountry.getName();
			}
		return resCountry;
	}
	
	public String getInsuredRelationshipWithProposer() {
		return relationshipWithProposer;
	}
	
	
	
	/*-------  Proposer Details -----------*/

	public String getProposerFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName proposerPersonName : proposerPersonNames) {
			name = proposerPersonName.getGivenName();
		}
		return name;
	}

	public String getProposerLastName() throws JRScriptletException {
		String name = null;
		for (PersonName proposerPersonName : proposerPersonNames) {
			name = proposerPersonName.getSurname();
		}
		return name;
	}


	public String getProposerFirstAndLastName(){
		try {
			proposerFullName = getProposerFirstName()+" "+getProposerLastName();
			if(proposerFullName != null && !"".equalsIgnoreCase(proposerFullName)){
				proposerFullName.toUpperCase();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving ProposerFirstAndLastName "+e.getMessage());
		}
		return proposerFullName;

	}

	public String getProposerGender() {
		String gender = null;
		try {
			if (proposerPerson.getGenderCode() != null) {
				gender = proposerPerson.getGenderCode().name();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving getProposerGender "+e.getMessage());
		}

		return gender;
	}

	public Date getProposerdob() {
		if(proposerdob!= null && !"".equals(proposerdob)){
			return proposerdob;
		}
		return proposerdob;
	}

	public String getProposerNationality() {
		String nationality = null;
		for (Country country : proposerCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	public String getProposerEmail() {
		return proposerElectronicContact.getEmailAddress();
	}
	public String getProposerMobileNumber() {
		return proposerMobileNumber1;
	}
	public String getProposerHomeNumber() {
		return proposerHomeNumber1;
	}
	
	public String getProposerOfficeNumber() {
		return proposerOfficeNumber;
	}

	public String getProposerIdentityProof() {
		return proposerIdentityProof;
	}

	public String getProposerIdentityDate() {
		return proposerIdentityDate;
	}

	public String getProposerIdentityPlace() {
		return proposerIdentityPlace;
	}

	public String getProposerMaritalStatus() {
			if (proposerPerson.getMaritalStatusCode() != null) {
				return proposerPerson.getMaritalStatusCode().name();
			}
		return null;
	}
	
	public String getProposerBirthPlace(){
		String resCountry = "";
			if(proposerResidenceCountry != null){
				resCountry = proposerResidenceCountry.getName();
			}
		return resCountry;
	}
	

	public String getProposerPerStreetName() {
		return proposerPermanentAddress.getStreetName();
	}
	public String getProposerPermWard() {
		return proposerPermanentAddress.getAddressLine2();
	}
	public String getProposerPerHouseNo() {
		return proposerPermanentAddress.getBoxNumber();
	}
	public String getProposerPermDistrict() {
		return proposerPermanentAddress.getAddressLine1();
	}
	
	public String getProposerCurrentDistrict() {
		return proposerPermanentAddress.getAddressLine1();
	}
	public String getProposerCurrentStreetName() {
		return proposerPostalAddressContact.getStreetName();
	}
	public String getProposerCurrentWard() {
		return proposerPostalAddressContact.getAddressLine2();
	}
	public String getProposerCurrentHouseNo() {
		return proposerPostalAddressContact.getBoxNumber();
	}
	
	public String getProposerPermCity() {
		populateProposerCP();
		Set<CountryElement> countryElements = proposerPermanentAddress
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}
	public String getProposerCurrentCity() {
		Set<CountryElement> countryElements = proposerPostalAddressContact
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}
	
	public String getProposerNameOfComapny() {
		return proposerOccupationDetail.getNameofInstitution();
	}
	
	public String getProposerNatureOfWork() {
		return proposerOccupationDetail.getNatureofWork();
	}
	
	public String getProposerAddressOfComapny() {
		return proposerPostalAddressContact.getAddressLine3();
	}
	public String getProposerOccupation() {
		return proposerOccupationDetail.getOccupationClass();
	}
	public String getProposerAnnualIncome() {
		return proposerIncomeDetail.getGrossIncome();
	}
	
	/*-------  Additional insured Details -----------*/
	/** First additional insured */

	public String getFirstAddInsuredFullName() throws JRScriptletException {
		String name = null;
		String fname=null;
		String lname=null;
		for (PersonName personName : firstAddInsuredPersonNames) {
			fname = personName.getGivenName();				
			lname = personName.getSurname();
			name=createFullName(fname, lname);
		}
		return name;
	}
	
	public Date getFirstAddInsuredDob() {
		return firstAddInsuredDob;
	}
	
	public String getFirstAddInsuredGender() {
		return firstAddInsuredPerson.getGenderCode().name();
	}
	
	public String getFirstAddInsuredRelationWithProposer() {
		String relationShip = "";
		if(firstAddInsuredRelationWithProposer != null && !"".equalsIgnoreCase(firstAddInsuredRelationWithProposer)){
			if (jsonObjectPdfInput.containsKey(firstAddInsuredRelationWithProposer)) {
				relationShip = jsonObjectPdfInput.get(firstAddInsuredRelationWithProposer).toString();
			}
		}
		return relationShip;
	}

	public String getFirstAddInsuredMaritalStatus() {
		if (firstAddInsuredPerson != null) {
			if (firstAddInsuredPerson.getMaritalStatusCode() != null) {
				return firstAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}
	
	public String getFirstAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getFirstAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first addinsured year of birth ");
		}
		return year;
	}

	public String getFirstAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getFirstAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first addinsured month of birth ");
		}
		return month;
	}

	public String getFirstAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getFirstAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first addinsured day of birth ");
		}
		return date;
	}


	public String getFirstAddInsuredNationality() {
		String nationality = null;
		String newC = "";
		try{
			for (Country country : firstAddInsuredCountries) {
				nationality = country.getName();
			}
			newC = OmniContextBridge.services().getValueForCode(nationality, "en", "NATIONALITY");
		}catch(Exception e){
		LOGGER.error("-- getFirstAddInsuredNationality -- "+e.getMessage());
		}
		return newC;
		}
	
	public Date getFirstAddInsuredIdentityDate() {
		return firstAddInsuredIdentityDate;
	}
	public String getFirstAddInsuredIdentityPlace() {
		return firstAddInsuredIdentityPlace;
	}
	
	public BigDecimal getFirstAddInsuredHeight() {
		return firstAddInsuredHeight;
	}

	public BigDecimal getFirstAddInsuredWeight() {
		return firstAddInsuredWeight;
	}
	
	public String getFirstAddInsuredNatureOfWork() {
		String firstAddInsuredNatureOfWork = null;
		firstAddInsuredNatureOfWork = firstAddInsuredOccupationDetail.getNatureofWork();
		return firstAddInsuredNatureOfWork;
	}
	

	public String getFirstAddInsuredNameOfComapny() {
		return firstAddInsuredOccupationDetail.getNameofInstitution();
	}
	
	public String getFirstAddnInsuredMobileNumber1() {
		populateFirstAddnInsuredCP();
		return firstAddnInsuredMobileNumber1;
	}
	public String getFirstAddnInsuredHomeNumber1() {
		populateFirstAddnInsuredCP();
		return firstAddnInsuredHomeNumber1;
	}
	public String getFirstAddnInsuredOfficeNumber() {
		populateFirstAddnInsuredCP();
		return firstAddnInsuredOfficeNumber;
	}
	
	public String getFirstAddInsuredAddressOfComapny() {
		return firstAddnInsuredPostalAddressContact.getAddressLine3();
	}
	public String getFirstAddInsuredOccupation() {
		String occupation = "";
		String addInsuredOccupation = "";
		try{
			occupation = firstAddInsuredOccupationDetail.getOccupationClass();
			addInsuredOccupation = OmniContextBridge.services().getValueForCode(occupation, "vt", "OCCUPATION_LIST");
		}catch(Exception e){
			LOGGER.error("Error .. getFirstAddInsuredOccupation "+e.getMessage());
		}
		return addInsuredOccupation;
	}
	
	public String getFirstAddInsuredAnnualIncome() {
		String annualIncome ="";
		try{
			if(firstAddInsuredIncomeDetail.getGrossIncome() !=null){
				annualIncome = formatter(firstAddInsuredIncomeDetail.getGrossIncome(), "vn");
			}			
		}catch(Exception e){
			LOGGER.error("Error .. getFirstAddInsuredAnnualIncome "+e.getMessage());
		}
		return annualIncome;
	}
	
	public String getFirstAddInsuredEmail() {
		populateFirstAddnInsuredCP();
		return firstAddnInsuredElectronicContact.getEmailAddress();
	}
	
	public String getFirstAddInsuredIdentityNo(){
		return firstAddInsuredIdentityNo;
	}
	
	public String getFirstAddInsuredPermWard() {
		String place = "";
		String addInsuredPermWard = "";
		try{
			place = firstAddnInsuredPostalAddressContact.getAddressLine2();
			addInsuredPermWard = OmniContextBridge.services().getValueForCode(place, "en", "WARD");
		}catch(Exception e){
			LOGGER.error("Error .. getFirstAddInsuredPermWard "+e.getMessage());
		}
		return addInsuredPermWard;
	}
	
	public String getFirstAddInsuredPermDistrict() {
		String place = "";
		String addInsuredPermDistrict = "";
		try{
			place = firstAddnInsuredPostalAddressContact.getAddressLine1();
			addInsuredPermDistrict = OmniContextBridge.services().getValueForCode(place, "en", "DISTRICT");
		}catch(Exception e){
			LOGGER.error("Error .. getFirstAddInsuredPermDistrict "+e.getMessage());
		}
		return addInsuredPermDistrict;
	}
	
	public String getAddInsuredPermCity() {
		String place = "";
		String addInsuredPermCity = "";
		Set<CountryElement> countryElements = firstAddnInsuredPostalAddressContact
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				place = ((City) countryElement).getName();
			}
		}
		try{
			if(place != null){
				addInsuredPermCity = OmniContextBridge.services().getValueForCode(place, "en", "STATE");
			}
		}catch(Exception e){
			LOGGER.error("Error .. getAddInsuredPermCity "+e.getMessage());
		}
		
		return addInsuredPermCity;
	}
	
	public String getFirstAddInsuredPermanentAddress() {
		if (firstAddInsuredPerson != null) {
			populateFirstAddnInsuredCP();
			if(firstAddnInsuredPostalAddressContact !=null){
				String houseNo = firstAddnInsuredPostalAddressContact.getBoxNumber();
				String street=firstAddnInsuredPostalAddressContact.getStreetName();
				if(street==null){
					street="";
				}
				String ward = getFirstAddInsuredPermWard();
				String district = getFirstAddInsuredPermDistrict();
				String city = getAddInsuredPermCity();
				String address = houseNo+","+street+","+ward+","+district+","+city;
				return address;
			}	
		}
		return null;
	}
	
	/*second additional insured*/
	
	public String getSecondAddInsuredFullName() throws JRScriptletException {
		String name = null;
		String fname=null;
		String lname=null;
		for (PersonName personName : secondAddInsuredPersonNames) {
			fname = personName.getGivenName();				
			lname = personName.getSurname();
			name=createFullName(fname, lname);
		}
		return name;
	}
	public Date getSecondAddInsuredDob() {
		return secondAddInsuredDob;
	}
	public String getSecondAddInsuredRelationWithProposer() {
		return secondAddInsuredRelationWithProposer;
	}
	public String getSecondAddnInsuredMobileNumber1() {
		return secondAddnInsuredMobileNumber1;
	}
	public String getSecondAddnInsuredHomeNumber1() {
		return secondAddnInsuredHomeNumber1;
	}
	public String getSecondAddnInsuredOfficeNumber() {
		return secondAddnInsuredOfficeNumber;
	}
	public String getSecondAddInsuredEmail() {
		return secondAddnInsuredElectronicContact.getEmailAddress();
	}
	
	public String getSecondAddInsuredAnnualIncome() {
		return secondAddInsuredIncomeDetail.getGrossIncome();
	}
	public String getSecondAddInsuredGender() {
		return secondAddInsuredPerson.getGenderCode().name();
	}
	public String getSecondAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getSecondAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second addinsured year of birth ");
		}
		return year;
	}

	public String getSecondAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getSecondAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second addinsured month of birth ");
		}
		return month;
	}

	public String getSecondAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getSecondAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second addinsured day of birth ");
		}
		return date;
	}
	public Date getSecondAddInsuredIdentityDate() {
		return secondAddInsuredIdentityDate;
	}
	public String getSecondAddInsuredIdentityPlace() {
		return secondAddInsuredIdentityPlace;
	}
	public String getSecondAddInsuredNationality() {
		String nationality = null;
		for (Country country : secondAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}
	public BigDecimal getSecondAddInsuredHeight() {
		return secondAddInsuredHeight;
	}

	public BigDecimal getSecondAddInsuredWeight() {
		return secondAddInsuredWeight;
	}
	
	public String getSecondAddInsuredAddressOfComapny() {
		return secondAddnInsuredPostalAddressContact.getAddressLine3();
	}
	
	public String getSecondAddInsuredNameOfComapny() {
		return secondAddInsuredOccupationDetail.getNameofInstitution();
	}
	public String getSecondAddInsuredNatureOfWork() {
		String secondAddInsuredNatureOfWork = null;
		secondAddInsuredNatureOfWork = secondAddInsuredOccupationDetail.getNatureofWorkValue();
		return secondAddInsuredNatureOfWork;
	}
	public String getSecondAddInsuredOccupation() {
		return secondAddInsuredOccupationDetail.getOccupationClass();
	}
	
	public String getSecondAddInsuredIdentityNo(){
		return secondAddInsuredIdentityNo;
	}
	
	public String getSecondAddInsuredPermanentAddress() {
		if (secondAddInsuredPerson != null) {
			if (secondAddInsuredPerson.getBirthAddress() != null) {
				String houseNo = secondAddInsuredPerson.getBirthAddress().getBoxNumber();
				String ward = secondAddInsuredPerson.getBirthAddress().getAddressLine2();
				String district = secondAddInsuredPerson.getBirthAddress().getAddressLine1();
				String address = houseNo+" "+ward+" "+district;
				return address;
			}
		}
		return null;
	}
	
	/*Third additional insured*/
	public String getThirdAddInsuredFullName() throws JRScriptletException {
		String name = null;
		String fname=null;
		String lname=null;
		for (PersonName personName : thirdAddInsuredPersonNames) {
			fname = personName.getGivenName();				
			lname = personName.getSurname();
			name=createFullName(fname, lname);
		}
		return name;
	}
	
	public Date getThirdAddInsuredDob() {
		return thirdAddInsuredDob;
	}
	public String getThirdAddInsuredRelationWithProposer() {
		return thirdAddInsuredRelationWithProposer;
	}
	
	public String getThirdAddnInsuredMobileNumber1() {
		return thirdAddnInsuredMobileNumber1;
	}
	public String getThirdAddnInsuredHomeNumber1() {
		return thirdAddnInsuredHomeNumber1;
	}
	public String getThirdAddnInsuredOfficeNumber() {
		return thirdAddnInsuredOfficeNumber;
	}
	
	public String getThirdAddInsuredAddressOfComapny() {
		return thirdAddnInsuredPostalAddressContact.getAddressLine3();
	}
	
	public String getThirdAddInsuredEmail() {
		return thirdAddnInsuredElectronicContact.getEmailAddress();
	}
	public String getThirdAddInsuredAnnualIncome() {
		return thirdAddInsuredIncomeDetail.getGrossIncome();
	}
	public String getThirdAddInsuredGender() {
		return thirdAddInsuredPerson.getGenderCode().name();
	}
	public String getThirdAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getThirdAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third addinsured year of birth ");
		}
		return year;
	}

	public String getThirdAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getThirdAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third addinsured month of birth ");
		}
		return month;
	}

	public String getThirdAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getThirdAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third addinsured day of birth ");
		}
		return date;
	}
	
	public Date getThirdAddInsuredIdentityDate() {
		return thirdAddInsuredIdentityDate;
	}
	public String getThirdAddInsuredIdentityPlace() {
		return thirdAddInsuredIdentityPlace;
	}
	
	public String getThirdAddInsuredNationality() {
		String nationality = null;
		for (Country country : thirdAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}
	public BigDecimal getThirdAddInsuredHeight() {
		return thirdAddInsuredHeight;
	}

	public BigDecimal getThirdAddInsuredWeight() {
		return thirdAddInsuredWeight;
	}
	public String getThirdAddInsuredNameOfComapny() {
		return thirdAddInsuredOccupationDetail.getNameofInstitution();
	}
	public String getThirdAddInsuredNatureOfWork() {
		String thirdAddInsuredNatureOfWork = null;
		thirdAddInsuredNatureOfWork = thirdAddInsuredOccupationDetail.getNatureofWorkValue();
		return thirdAddInsuredNatureOfWork;
	}
	public String getThirdAddInsuredOccupation() {
		return thirdAddInsuredOccupationDetail.getOccupationClass();
	}
	
	public String getThirdAddInsuredIdentityNo(){
		return thirdAddInsuredIdentityNo;
	}
	public String getThirdAddInsuredPermanentAddress() {
		if (thirdAddInsuredPerson != null) {
			if (thirdAddInsuredPerson.getBirthAddress() != null) {
				String houseNo = thirdAddInsuredPerson.getBirthAddress().getBoxNumber();
				String ward = thirdAddInsuredPerson.getBirthAddress().getAddressLine2();
				String district = thirdAddInsuredPerson.getBirthAddress().getAddressLine1();
				String address = houseNo+" "+ward+" "+district;
				return address;
			}
		}
		return null;
	}
	
	/*fourth additional insured*/
	public String getFourthAddInsuredFullName() throws JRScriptletException {
		String name = null;
		String fname=null;
		String lname=null;
		for (PersonName personName : fourthAddInsuredPersonNames) {
			fname = personName.getGivenName();				
			lname = personName.getSurname();
			name=createFullName(fname, lname);
		}
		return name;
	}
	public Date getFourthAddInsuredDob() {
		return fourthAddInsuredDob;
	}

	public String getFourthAddInsuredGender() {
		return fourthAddInsuredPerson.getGenderCode().name();
	}

	public String getFourthAddInsuredMaritalStatus() {
		if (fourthAddInsuredPerson != null) {
			if (fourthAddInsuredPerson.getMaritalStatusCode() != null) {
				return fourthAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}
	public String getFourthAddnInsuredMobileNumber1() {
		return fourthAddnInsuredMobileNumber1;
	}
	public String getFourthAddnInsuredHomeNumber1() {
		return fourthAddnInsuredHomeNumber1;
	}
	public String getFourthAddnInsuredOfficeNumber() {
		return fourthAddnInsuredOfficeNumber;
	}

	public String getFourthAddInsuredAddressOfComapny() {
		return fourthAddnInsuredPostalAddressContact.getAddressLine3();
	}
	
	public String getFourthAddInsuredNationality() {
		String nationality = null;
		for (Country country : fourthAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}
	
	public String getFourthAddInsuredRelationWithProposer() {
		return fourthAddInsuredRelationWithProposer;
	}
	
	public String getFourthAddInsuredEmail() {
		return fourthAddnInsuredElectronicContact.getEmailAddress();
	}
	
	public String getFourthAddInsuredAnnualIncome() {
		return fourthAddInsuredIncomeDetail.getGrossIncome();
	}
	

	public String getFourthAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getFourthAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth addinsured year of birth ");
		}
		return year;
	}

	public String getFourthAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getFourthAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth addinsured month of birth ");
		}
		return month;
	}

	public String getFourthAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getFourthAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth addinsured day of birth ");
		}
		return date;
	}
	
	public Date getFourthAddInsuredIdentityDate() {
		return fourthAddInsuredIdentityDate;
	}
	public String getFourthAddInsuredIdentityPlace() {
		return fourthAddInsuredIdentityPlace;
	}
	public BigDecimal getFourthAddInsuredHeight() {
		return fourthAddInsuredHeight;
	}

	public BigDecimal getFourthAddInsuredWeight() {
		return fourthAddInsuredWeight;
	}
	
	public String getFourthAddInsuredNameOfComapny() {
		return fourthAddInsuredOccupationDetail.getNameofInstitution();
	}
	public String getFourthAddInsuredNatureOfWork() {
		String fourthAddInsuredNatureOfWork = null;
		fourthAddInsuredNatureOfWork = thirdAddInsuredOccupationDetail.getNatureofWorkValue();
		return fourthAddInsuredNatureOfWork;
	}
	public String getFourthAddInsuredOccupation() {
		return fourthAddInsuredOccupationDetail.getOccupationClass();
	}
	
	public String getFourthAddInsuredIdentityNo(){
		return fourthAddInsuredIdentityNo;
	}
	
	public String getFourthAddInsuredPermanentAddress() {
		if (fourthAddInsuredPerson != null) {
			if (fourthAddInsuredPerson.getBirthAddress() != null) {
				String houseNo = fourthAddInsuredPerson.getBirthAddress().getBoxNumber();
				String ward = fourthAddInsuredPerson.getBirthAddress().getAddressLine2();
				String district = fourthAddInsuredPerson.getBirthAddress().getAddressLine1();
				String address = houseNo+" "+ward+" "+district;
				return address;
			}
		}
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	/** Residence country taken as birth country */
	public String getFirstAddInsuredResidenceCountry(){
		String resCountry = null;
		String addInsuredBirthPlace="";
		try{
			if(firstAddInsuredResidenceCountry != null){
				resCountry = firstAddInsuredResidenceCountry.getName();
			}
			addInsuredBirthPlace = OmniContextBridge.services().getValueForCode(resCountry, "en", "NATIONALITY");
		}catch(Exception e){
		LOGGER.error("Error .. getAddInsuredBirthPlace "+e.getMessage());
		}
		return addInsuredBirthPlace;
		}



	public String getFirstAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = firstAddInsuredOccupationDetail
				.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	

	public String getFirstAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for (EmploymentRelationship employmentRelationship : firstAddInsuredOccupationDetail
				.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship
			.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getFirstAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for (EmploymentRelationship employmentRelationship : firstAddInsuredOccupationDetail
				.getProvidingEmployment()) {
			firstAddInsuredJobDescription = employmentRelationship
					.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}

	

	

	
	/** Second additional insured */

	



	public String getSecondAddInsuredMaritalStatus() {
		if (secondAddInsuredPerson != null) {
			if (secondAddInsuredPerson.getMaritalStatusCode() != null) {
				return secondAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	

	public String getSecondAddInsuredResidenceCountry(){
		String resCountry = null;
		if(secondAddInsuredResidenceCountry != null){
			resCountry = secondAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getSecondAddInsuredIdCardType() {
		return secondAddInsuredIdCardType;
	}

	public String getSecondAddInsuredIdCardNumber() {
		return secondAddInsuredIdCardNumber;
	}

	

	public String getSecondAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = secondAddInsuredOccupationDetail.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getSecondAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for(EmploymentRelationship employmentRelationship:  secondAddInsuredOccupationDetail.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getSecondAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for(EmploymentRelationship employmentRelationship : secondAddInsuredOccupationDetail.getProvidingEmployment()){
			firstAddInsuredJobDescription=employmentRelationship.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}
	
		/** Third additional insured */

	
	
	

	public String getThirdAddInsuredMaritalStatus() {
		if (thirdAddInsuredPerson != null) {
			if (thirdAddInsuredPerson.getMaritalStatusCode() != null) {
				return thirdAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	

	public String getThirdAddInsuredResidenceCountry(){
		String resCountry = null;
		if(thirdAddInsuredResidenceCountry != null){
			resCountry = thirdAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getThirdAddInsuredIdCardType() {
		return thirdAddInsuredIdCardType;
	}

	public String getThirdAddInsuredIdCardNumber() {
		return thirdAddInsuredIdCardNumber;
	}

	

	public String getThirdAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = thirdAddInsuredOccupationDetail.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}


	public String getThirdAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for(EmploymentRelationship employmentRelationship:  thirdAddInsuredOccupationDetail.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getThirdAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for(EmploymentRelationship employmentRelationship : thirdAddInsuredOccupationDetail.getProvidingEmployment()){
			firstAddInsuredJobDescription=employmentRelationship.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}
	
	

	
	/** Fourth additional insured */

	


	public String getFourthAddInsuredResidenceCountry(){
		String resCountry = null;
		if(fourthAddInsuredResidenceCountry != null){
			resCountry = fourthAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getFourthAddInsuredIdCardType() {
		return fourthAddInsuredIdCardType;
	}

	public String getFourthAddInsuredIdCardNumber() {
		return fourthAddInsuredIdCardNumber;
	}

	

	public String getFourthAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = fourthAddInsuredOccupationDetail.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getFourthAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for(EmploymentRelationship employmentRelationship:  fourthAddInsuredOccupationDetail.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getFourthAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for(EmploymentRelationship employmentRelationship : fourthAddInsuredOccupationDetail.getProvidingEmployment()){
			firstAddInsuredJobDescription=employmentRelationship.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}

	

		/** Beneficiary */
	/** First beneficiary */

	public String getFirstBeneficiaryFirstName() throws JRScriptletException {
		String firstBeneficiaryFirstName = "";
		for (PersonName firstBeneficiaryPersonName : firstBeneficiaryPersonNames) {
			firstBeneficiaryFirstName = firstBeneficiaryPersonName.getGivenName();
		}
		return firstBeneficiaryFirstName;
	}

	public String getFirstBeneficiaryLastName() throws JRScriptletException {
		String firstBeneficiaryLastName = "";
		for (PersonName firstBeneficiaryPersonName : firstBeneficiaryPersonNames) {
			firstBeneficiaryLastName = firstBeneficiaryPersonName.getSurname();
		}
		return firstBeneficiaryLastName;
	}

	public String getFirstBeneficiaryFirstAndLastName(){
		String firstBeneficiaryFullName = "";
		try {
			if (getFirstBeneficiaryLastName()!=null){
				firstBeneficiaryFullName = getFirstBeneficiaryFirstName()+" "+getFirstBeneficiaryLastName();
			}else{
				firstBeneficiaryFullName = getFirstBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving BeneficiaryFirstAndLastName "+e.getMessage());
		}
		return firstBeneficiaryFullName;
	}

	public Date getFirstBeneficiaryDob() {
		return firstBeneficiaryDOB;
	}

	public String getFirstBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(firstBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFirstBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getFirstBeneficiarySharePercentage() {
		return firstBeneficiarySharePercentage;
	}

	public String getFirstBeneficiaryRelationWithInsured() {
		String rel = "";
		if(firstBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(firstBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getFirstBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if(firstBeneficiaryDobValue != null && !"".equalsIgnoreCase(firstBeneficiaryDobValue)){
				year = firstBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first beneficiary year of birth ");
		}
		return year;
	}

	public String getFirstBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if(firstBeneficiaryDobValue != null && !"".equalsIgnoreCase(firstBeneficiaryDobValue)){
				month = firstBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first beneficiary month of birth ");
		}
		return month;
	}

	public String getFirstBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if(firstBeneficiaryDobValue != null && !"".equalsIgnoreCase(firstBeneficiaryDobValue)){
				date = firstBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first beneficiary day of birth ");
		}
		return date;
	}

	/** Second beneficiary */

	public String getSecondBeneficiaryFirstName() throws JRScriptletException {
		String secondBeneficiaryFirstName = "";
		for (PersonName secondBeneficiaryPersonName : secondBeneficiaryPersonNames) {
			secondBeneficiaryFirstName = secondBeneficiaryPersonName.getGivenName();
		}
		return secondBeneficiaryFirstName;
	}

	public String getSecondBeneficiaryLastName() throws JRScriptletException {
		String secondBeneficiaryLastName = "";
		for (PersonName secondBeneficiaryPersonName : secondBeneficiaryPersonNames) {
			secondBeneficiaryLastName = secondBeneficiaryPersonName.getSurname();
		}
		return secondBeneficiaryLastName;
	}

	public String getSecondBeneficiaryFirstAndLastName(){
		String secondBeneficiaryFullName = "";
		try {
			if (getSecondBeneficiaryLastName()!=null){
				secondBeneficiaryFullName = getSecondBeneficiaryFirstName()+" "+getSecondBeneficiaryLastName();
			}else{
				secondBeneficiaryFullName = getSecondBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving SecondBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return secondBeneficiaryFullName;
	}

	public Date getSecondBeneficiaryDob() {
		return secondBeneficiaryDOB;
	}

	public String getSecondBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(secondBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFirstBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getSecondBeneficiarySharePercentage() {
		return secondBeneficiarySharePercentage;
	}

	public String getSecondBeneficiaryRelationWithInsured() {
		String rel = "";
		if(secondBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(secondBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getSecondBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if(secondBeneficiaryDobValue != null && !"".equalsIgnoreCase(secondBeneficiaryDobValue)){
				year = secondBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second beneficiary year of birth ");
		}
		return year;
	}

	public String getSecondBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if(secondBeneficiaryDobValue != null && !"".equalsIgnoreCase(secondBeneficiaryDobValue)){
				month = secondBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second beneficiary month of birth ");
		}
		return month;
	}

	public String getSecondBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if(secondBeneficiaryDobValue != null && !"".equalsIgnoreCase(secondBeneficiaryDobValue)){
				date = secondBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second beneficiary day of birth ");
		}
		return date;
	}

	/** Third beneficiary */

	public String getThirdBeneficiaryFirstName() throws JRScriptletException {
		String thirdBeneficiaryFirstName = null;
		for (PersonName thirdBeneficiaryPersonName : thirdBeneficiaryPersonNames) {
			thirdBeneficiaryFirstName = thirdBeneficiaryPersonName.getGivenName();
		}
		return thirdBeneficiaryFirstName;
	}

	public String getThirdBeneficiaryLastName() throws JRScriptletException {
		String thirdBeneficiaryLastName = null;
		for (PersonName thirdBeneficiaryPersonName : thirdBeneficiaryPersonNames) {
			thirdBeneficiaryLastName = thirdBeneficiaryPersonName.getSurname();
		}
		return thirdBeneficiaryLastName;
	}

	public String getThirdBeneficiaryFirstAndLastName(){
		String thirdBeneficiaryFullName = null;
		try {
			if (getThirdBeneficiaryLastName()!=null){
				thirdBeneficiaryFullName = getThirdBeneficiaryFirstName()+" "+getThirdBeneficiaryLastName();
			}else{
				thirdBeneficiaryFullName = getThirdBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving ThirdBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return thirdBeneficiaryFullName;
	}

	public Date getThirdBeneficiaryDob() {
		return thirdBeneficiaryDOB;
	}

	public String getThirdBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(thirdBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFirstBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getThirdBeneficiarySharePercentage() {
		return thirdBeneficiarySharePercentage;
	}

	public String getThirdBeneficiaryRelationWithInsured() {
		String rel = "";
		if(thirdBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(thirdBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getThirdBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if(thirdBeneficiaryDobValue != null && !"".equalsIgnoreCase(thirdBeneficiaryDobValue)){
				year = thirdBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third beneficiary year of birth ");
		}
		return year;
	}

	public String getThirdBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if(thirdBeneficiaryDobValue != null && !"".equalsIgnoreCase(thirdBeneficiaryDobValue)){
				month = thirdBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third beneficiary month of birth ");
		}
		return month;
	}

	public String getThirdBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if(thirdBeneficiaryDobValue != null && !"".equalsIgnoreCase(thirdBeneficiaryDobValue)){
				date = thirdBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third beneficiary day of birth ");
		}
		return date;
	}

	/** Fourth beneficiary */

	public String getFourthBeneficiaryFirstName() throws JRScriptletException {
		String fourthBeneficiaryFirstName = "";
		for (PersonName fourthBeneficiaryPersonName : fourthBeneficiaryPersonNames) {
			fourthBeneficiaryFirstName = fourthBeneficiaryPersonName.getGivenName();
		}
		return fourthBeneficiaryFirstName;
	}

	public String getFourthBeneficiaryLastName() throws JRScriptletException {
		String fourthBeneficiaryLastName = "";
		for (PersonName fourthBeneficiaryPersonName : fourthBeneficiaryPersonNames) {
			fourthBeneficiaryLastName = fourthBeneficiaryPersonName.getSurname();
		}
		return fourthBeneficiaryLastName;
	}

	public String getFourthBeneficiaryFirstAndLastName(){
		String fourthBeneficiaryFullName = "";
		try {
			if (getFourthBeneficiaryLastName()!=null){
				fourthBeneficiaryFullName = getFourthBeneficiaryFirstName()+" "+getFourthBeneficiaryLastName();
			}else{
				fourthBeneficiaryFullName = getFourthBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving FourthBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return fourthBeneficiaryFullName;
	}

	public Date getFourthBeneficiaryDob() {
		return fourthBeneficiaryDOB;
	}

	public String getFourthBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(fourthBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFourthBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getFourthBeneficiarySharePercentage() {
		return fourthBeneficiarySharePercentage;
	}

	public String getFourthBeneficiaryRelationWithInsured() {
		String rel = "";
		if(fourthBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(fourthBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getFourthBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if(fourthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fourthBeneficiaryDobValue)){
				year = fourthBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth beneficiary year of birth ");
		}
		return year;
	}

	public String getFourthBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if(fourthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fourthBeneficiaryDobValue)){
				month = fourthBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth beneficiary month of birth ");
		}
		return month;
	}

	public String getFourthBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if(fourthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fourthBeneficiaryDobValue)){
				date = fourthBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth beneficiary day of birth ");
		}
		return date;
	}


	/** Financial Data */

	public String getProposerSourceOfIncome(){
		String proposerSourceOfIncome = null;
		proposerSourceOfIncome = proposerIncomeDetail.getTypeCode().name();
		return proposerSourceOfIncome;
	}

	public String getProposerSourceOfIncomeDescription(){
		String proposerSourceOfIncomeDesc = null;
		proposerSourceOfIncomeDesc = proposerIncomeDetail.getDescription();
		return proposerSourceOfIncomeDesc;
	}


	public String getProposerGrossIncome(){
		String proposerGrossIncome = null;
		proposerGrossIncome = proposerIncomeDetail.getGrossIncome();
		return proposerGrossIncome;
	}

	public String getProposerGrossIncomeDescription(){
		String proposerGrossIncomeDesc = null;
		proposerGrossIncomeDesc = proposerIncomeDetail.getGrossIncomeDesc();
		return proposerGrossIncomeDesc;
	}
	
	public String getProposerPaymentMethod(){
		return proposerPaymentMethod;
	}
	
	
	
	public String getSpajNumber() {
		return spajNumber;
	}
	public String getIllustrationNumber() {
		return illustrationNumber;
	}


	

	public String getProposerYearOfBirth(){
		String year = null;
		try{
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if(propDobValue != null && !"".equalsIgnoreCase(propDobValue)){
				year = propDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting proposer year of birth ");
		}
		return year;
	}

	public String getProposerMonthOfBirth(){
		String month = null;
		try{
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if(propDobValue != null && !"".equalsIgnoreCase(propDobValue)){
				month = propDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting proposer month of birth ");
		}
		return month;
	}

	public String getProposerDayOfBirth(){
		String date = null;
		try{
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if(propDobValue != null && !"".equalsIgnoreCase(propDobValue)){
				date = propDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting proposer month of birth ");
		}
		return date;
	}

	
	
	
	
			public String getInsuredRelationWithProposer(){
		String relationshipWithProp = "";
		if(getIsPropDiffFromInsured()){
			return relationshipWithProposer;
		}
		return relationshipWithProp;
	}

	
	

	
	
	public String splitDate(Date date, int firstIndex , int lastIndex) throws Exception{
		String dobValue = date.toString();
		String dateSplit="";

		if(dobValue != null && !"".equalsIgnoreCase(dobValue)){
			dateSplit = dobValue.substring(firstIndex, lastIndex);
		}

		return dateSplit;
	}

	public String getDecalrationQuestionAnswer(String Number) {
		String answer="";
		try{

			for(UserSelection question : declationQuestions)
			{
				if(Number.equals(question.getQuestionId().toString()) && ("Basic").equals(question.getQuestionnaireType().name()) ){
					if(true==question.getSelectedOption())
					{
						answer="true";

					}
					else if(false==question.getSelectedOption()){
						answer="false";
					}
					break;
				}
			}


		}catch(Exception e){
			LOGGER.error("Error while getting decalartion answer option of question "+Number);
		}
		return answer;

	}

	
	
	public String getProposerFacta1(){
		return proposerFatcaQues1;
	}

	public String getProposerFacta2(){
		return proposerFatcaQues2;
	}

	public Boolean getBeneficiaryFacta1(){
		return beneficiaryFatcaQues1;
	}

	public Boolean getBeneficiaryFacta2(){
		return beneficiaryFatcaQues2;
	}

	
	
	public String getAgentCode() {
		return agentCode;
	}

	/** Previous Policy */

	public String getFirstCompanyName() {
		return firstCompanyName;
	}

	public String getFirstPrevInsuredName() {
		return firstPrevInsuredName;
	}

	public String getFirstFinalDecision() {
		return firstFinalDecision;
	}

	public String getFirstTypeOfInsurance() {
		return firstTypeOfInsurance;
	}

	public String getFirstPolicyStatus() {
		return firstPolicyStatus;
	}

	public BigDecimal getFirstPrevSumAssured() {
		return firstPrevSumAssured;
	}

	public String getSecondCompanyName() {
		return secondCompanyName;
	}

	public String getSecondPrevInsuredName() {
		return secondPrevInsuredName;
	}

	public String getSecondFinalDecision() {
		return secondFinalDecision;
	}

	public String getSecondTypeOfInsurance() {
		return secondTypeOfInsurance;
	}

	public String getSecondPolicyStatus() {
		return secondPolicyStatus;
	}

	public BigDecimal getSecondPrevSumAssured() {
		return secondPrevSumAssured;
	}

	public String getThirdCompanyName() {
		return thirdCompanyName;
	}

	public String getThirdPrevInsuredName() {
		return thirdPrevInsuredName;
	}

	public String getThirdFinalDecision() {
		return thirdFinalDecision;
	}

	public String getThirdTypeOfInsurance() {
		return thirdTypeOfInsurance;
	}

	public String getThirdPolicyStatus() {
		return thirdPolicyStatus;
	}

	public BigDecimal getThirdPrevSumAssured() {
		return thirdPrevSumAssured;
	}

	public String getFourthCompanyName() {
		return fourthCompanyName;
	}

	public String getFourthPrevInsuredName() {
		return fourthPrevInsuredName;
	}

	public String getFourthFinalDecision() {
		return fourthFinalDecision;
	}

	public String getFourthTypeOfInsurance() {
		return fourthTypeOfInsurance;
	}

	public String getFourthPolicyStatus() {
		return fourthPolicyStatus;
	}

	public BigDecimal getFourthPrevSumAssured() {
		return fourthPrevSumAssured;
	}

		public String getAgentSignDate() {
		return agentSignDate;
	}

		public String getFirstPrePolicyType() {
			return firstPrePolicyType;
		}
		public String getSecondPrePolicyType() {
			return secondPrePolicyType;
		}
		public String getThirdPrePolicyType() {
			return thirdPrePolicyType;
		}
		public String getFourthPrePolicyType() {
			return fourthPrePolicyType;
		}
		public Date getSecondPrePolicyEffectiveDate() {
			return secondPrePolicyEffectiveDate;
		}
		public Date getFirstPrePolicyEffectiveDate() {
			return firstPrePolicyEffectiveDate;
		}
		public Date getThirdPrePolicyEffectiveDate() {
			return thirdPrePolicyEffectiveDate;
		}
		public Date getFourthPrePolicyEffectiveDate() {
			return fourthPrePolicyEffectiveDate;
		}

	

	public String getAgentSignPlace() {
		return agentSignPlace;
	}

	public Date getSpajSignDate() {
		return spajSignDate;
	}

	
	

	public String getSpajSignPlace() {
		return SpajSignPlace;
	}

	

	

	

	
	public Boolean getIsPropDiffFromInsured() {
		return isPropDiffFromInsured;
	}

	
	
	

		public String getBenRelationInBahsa(String relation){
		String rel = "";
		try{
			if(relation != null && !"".equals(relation)){
				if(relation.equalsIgnoreCase("Husband")){
					rel = "Suami";
				}else if(relation.equalsIgnoreCase("Son")){
					rel = "Anak Laki Laki";
				}else if(relation.equalsIgnoreCase("Father")){
					rel = "Ayah";
				}else if(relation.equalsIgnoreCase("Wife")){
					rel = "Istri";
				}else if(relation.equalsIgnoreCase("Daughter")){
					rel = "Anak Perempuan";
				}else if(relation.equalsIgnoreCase("Mother")){
					rel = "Ibu";
				}else if(relation.equalsIgnoreCase("Siblings")){
					rel = "Saudara Kandung";
				}else if(relation.equalsIgnoreCase("Others")){
					rel = "Lainnya";
				}
			}
		}catch(Exception e){
			LOGGER.error("Error in getBenRelationInBahsa :"+e.getMessage());
		}
		return rel;
	}

	public String getBenGender(String gender){
		String gen = "";
		if(gender != null){
			if(gender.equalsIgnoreCase("Male")){
				gen = "Pria";
			}else if(gender.equalsIgnoreCase("Female")){
				gen = "Wanita";
			}
		}
		return gen;
	}
	public String getInsuredHasExistingPolicies() {
		return insuredHasExistingPolicies;
	}
	public String getInsuredFamilyMembersDiagWithDiseases() {
		return insuredFamilyMembersDiagWithDiseases;
	}
	
	public Integer getFirstFamilyMemAgeAtDeath() {
		return firstFamilyMemAgeAtDeath;
	}
	public Integer getFirstFamilyMemAge() {
		return firstFamilyMemAge;
	}
	public String getFirstFamilyMemCauseOfDeath() {
		return firstFamilyMemCauseOfDeath;
	}
	public String getFirstFamilyMemRelation() {
		return firstFamilyMemRelation;
	}
	public Integer getSecondFamilyMemAgeAtDeath() {
		return secondFamilyMemAgeAtDeath;
	}
	public Integer getSecondFamilyMemAge() {
		return secondFamilyMemAge;
	}
	public String getSecondFamilyMemCauseOfDeath() {
		return secondFamilyMemCauseOfDeath;
	}
	public String getSecondFamilyMemRelation() {
		return secondFamilyMemRelation;
	}
	public Integer getThirdFamilyMemAgeAtDeath() {
		return thirdFamilyMemAgeAtDeath;
	}
	public Integer getThirdFamilyMemAge() {
		return thirdFamilyMemAge;
	}
	public String getThirdFamilyMemCauseOfDeath() {
		return thirdFamilyMemCauseOfDeath;
	}
	public String getThirdFamilyMemRelation() {
		return thirdFamilyMemRelation;
	}
	
	public String getAddInsuredHaveSuspendedPolicies() {
		return addInsuredOneHaveSuspendedPolicies;
	}
	public String getAddInsuredHaveSuspendedPoliciesDetails() {
		return addInsuredOneHaveSuspendedPoliciesDetails;
	}
	public String getAddInsuredHaveDeclainedPolicies() {
		return addInsuredOneHaveDeclainedPolicies;
	}
	public String getAddInsuredHaveDeclainedPoliciesDetails() {
		return addInsuredOneHaveDeclainedPoliciesDetails;
	}
	public String getAddInsuredHasEngagedSports() {
		return addInsuredOneHasEngagedSports;
	}
	public String getAddInsuredHasEngagedSportsDetails() {
		return addInsuredOneHasEngagedSportsDetails;
	}
	public String getAddInsuredHasIntensionToTravel() {
		return addInsuredOneHasIntensionToTravel;
	}
	public String getAddInsuredHasIntensionToTravelDetails() {
		return addInsuredOneHasIntensionToTravelDetails;
	}
	
	public String getAddInsuredHasLostWeight() {
		return addInsuredOneHasLostWeight;
	}
	public String getAddInsuredHasLostWeightDetails() {
		return addInsuredOneHasLostWeightDetails;
	}
	public String getAddInsuredHasRecevingMedTreatment() {
		return addInsuredOneHasRecevingMedTreatment;
	}
	public String getAddInsuredHasRecevingMedTreatmentDetails() {
		return addInsuredOneHasRecevingMedTreatmentDetails;
	}
	public String getAddInsuredHasUsedTobacco() {
		return addInsuredOneHasUsedTobacco;
	}
	public String getAddInsuredHasUsedTobaccoDetails() {
		return addInsuredOneHasUsedTobaccoDetails;
	}
	public String getAddInsuredHasUsedDrugs() {
		return addInsuredOneHasUsedDrugs;
	}
	public String getAddInsuredHasUsedDrugsDetails() {
		return addInsuredOneHasUsedDrugsDetails;
	}
	public String getAddInsuredHasUsedAlcoholicBev() {
		return addInsuredOneHasUsedAlcoholicBev;
	}
	public String getAddInsuredHasUsedAlcoholicBevDetails() {
		return addInsuredOneHasUsedAlcoholicBevDetails;
	}
	public String getAddInsuredHasUndergoneDiagnosis() {
		return addInsuredOneHasUndergoneDiagnosis;
	}
	public String getAddInsuredHasUndergoneDiagnosisDetails() {
		return addInsuredOneHasUndergoneDiagnosisDetails;
	}
	
	public String getAddInsuredFamilyMembersDiagWithDiseases(){
		return addInsuredFamilyMembersDiagWithDiseases;
    }
	
	
	//Additional Insured One FamilyHistory
	public Integer getAddFirstFamilyMemAgeAtDeath() {
		return firstAddFirstFamilyMemAgeAtDeath;
	}
	public Integer getAddFirstFamilyMemAge() {
		return firstAddFirstFamilyMemAge;
	}
	public String getAddFirstFamilyMemCauseOfDeath() {
		return firstAddFirstFamilyMemCauseOfDeath;
	}
	public String getAddFirstFamilyMemRelation() {
		String relationShip = "";
		if(firstAddFirstFamilyMemRelation != null && !"".equalsIgnoreCase(firstAddFirstFamilyMemRelation)){
			if (jsonObjectPdfInput.containsKey(firstAddFirstFamilyMemRelation)) {
				relationShip = jsonObjectPdfInput.get(firstAddFirstFamilyMemRelation).toString();
			}
		}
		return relationShip;
	}
	
	public Integer getSecondAddFirstFamilyMemAgeAtDeath() {
		return secondAddFirstFamilyMemAgeAtDeath;
	}
	public Integer getSecondAddFirstFamilyMemAge() {
		return secondAddFirstFamilyMemAge;
	}
	public String getSecondAddFirstFamilyMemCauseOfDeath() {
		return secondAddFirstFamilyMemCauseOfDeath;
	}
	public String getSecondAddFirstFamilyMemRelation() {
		String relationShip = "";
		if(secondAddFirstFamilyMemRelation != null && !"".equalsIgnoreCase(secondAddFirstFamilyMemRelation)){
			if (jsonObjectPdfInput.containsKey(secondAddFirstFamilyMemRelation)) {
				relationShip = jsonObjectPdfInput.get(secondAddFirstFamilyMemRelation).toString();
			}
		}
		return relationShip;
	}
	
	public Integer getThirdAddFirstFamilyMemAgeAtDeath() {
		return thirdAddFirstFamilyMemAgeAtDeath;
	}
	public Integer getThirdAddFirstFamilyMemAge() {
		return thirdAddFirstFamilyMemAge;
	}
	public String getThirdAddFirstFamilyMemCauseOfDeath() {
		return thirdAddFirstFamilyMemCauseOfDeath;
	}
	public String getThirdAddFirstFamilyMemRelation() {
		String relationShip = "";
		if(thirdAddFirstFamilyMemRelation != null && !"".equalsIgnoreCase(thirdAddFirstFamilyMemRelation)){
			if (jsonObjectPdfInput.containsKey(thirdAddFirstFamilyMemRelation)) {
				relationShip = jsonObjectPdfInput.get(thirdAddFirstFamilyMemRelation).toString();
			}
		}
		return relationShip;
	}
	
	//Additional Insured Two FamilyHistory
	public Integer getAddTwoFirstFamilyMemAgeAtDeath() {
		return firstAddSecondFamilyMemAgeAtDeath;
	}
	public Integer getAddTwoFirstFamilyMemAge() {
		return firstAddSecondFamilyMemAge;
	}
	public String getAddTwoFirstFamilyMemCauseOfDeath() {
		return firstAddSecondFamilyMemCauseOfDeath;
	}
	public String getAddTwoFirstFamilyMemRelation() {
		return firstAddSecondFamilyMemRelation;
	}
	
	public Integer getSecondAddTwoFirstFamilyMemAgeAtDeath() {
		return secondAddSecondFamilyMemAgeAtDeath;
	}
	public Integer getSecondAddTwoFirstFamilyMemAge() {
		return secondAddSecondFamilyMemAge;
	}
	public String getSecondAddTwoFirstFamilyMemCauseOfDeath() {
		return secondAddSecondFamilyMemCauseOfDeath;
	}
	public String getSecondAddTwoFirstFamilyMemRelation() {
		return secondAddSecondFamilyMemRelation;
	}
	
	public Integer getThirdAddTwoFirstFamilyMemAgeAtDeath() {
		return thirdAddSecondFamilyMemAgeAtDeath;
	}
	public Integer getThirdAddTwoFirstFamilyMemAge() {
		return thirdAddSecondFamilyMemAge;
	}
	public String getThirdAddTwoFirstFamilyMemCauseOfDeath() {
		return thirdAddSecondFamilyMemCauseOfDeath;
	}
	public String getThirdAddTwoFirstFamilyMemRelation() {
		return thirdAddSecondFamilyMemRelation;
	}
	
	//Additional Insured Three FamilyHistory
	public Integer getAddThreeFirstFamilyMemAgeAtDeath() {
		return firstAddThirdFamilyMemAgeAtDeath;
	}
	public Integer getAddThreeFirstFamilyMemAge() {
		return firstAddThirdFamilyMemAge;
	}
	public String getAddThreeFirstFamilyMemCauseOfDeath() {
		return firstAddThirdFamilyMemCauseOfDeath;
	}
	public String getAddThreeFirstFamilyMemRelation() {
		return firstAddThirdFamilyMemRelation;
	}
	
	public Integer getSecondAddThreeFirstFamilyMemAgeAtDeath() {
		return secondAddThirdFamilyMemAgeAtDeath;
	}
	public Integer getSecondAddThreeFirstFamilyMemAge() {
		return secondAddThirdFamilyMemAge;
	}
	public String getSecondAddThreeFirstFamilyMemCauseOfDeath() {
		return secondAddThirdFamilyMemCauseOfDeath;
	}
	public String getSecondAddThreeFirstFamilyMemRelation() {
		return secondAddThirdFamilyMemRelation;
	}
	
	public Integer getThirdAddThreeFirstFamilyMemAgeAtDeath() {
		return thirdAddThirdFamilyMemAgeAtDeath;
	}
	public Integer getThirdAddThreeFirstFamilyMemAge() {
		return thirdAddThirdFamilyMemAge;
	}
	public String getThirdAddThreeFirstFamilyMemCauseOfDeath() {
		return thirdAddThirdFamilyMemCauseOfDeath;
	}
	public String getThirdAddThreeFirstFamilyMemRelation() {
		return thirdAddThirdFamilyMemRelation;
	}
	
	//Additional Insured Four FamilyHistory
	public Integer getAddFourFirstFamilyMemAgeAtDeath() {
		return firstAddFourthFamilyMemAgeAtDeath;
	}
	public Integer getAddFourFirstFamilyMemAge() {
		return firstAddFourthFamilyMemAge;
	}
	public String getAddFourFirstFamilyMemCauseOfDeath() {
		return firstAddFourthFamilyMemCauseOfDeath;
	}
	public String getAddFourFirstFamilyMemRelation() {
		return firstAddFourthFamilyMemRelation;
	}
	
	public Integer getSecondAddFourFirstFamilyMemAgeAtDeath() {
		return secondAddFourthFamilyMemAgeAtDeath;
	}
	public Integer getSecondAddFourFirstFamilyMemAge() {
		return secondAddFourthFamilyMemAge;
	}
	public String getSecondAddFourFirstFamilyMemCauseOfDeath() {
		return secondAddFourthFamilyMemCauseOfDeath;
	}
	public String getSecondAddFourFirstFamilyMemRelation() {
		return secondAddFourthFamilyMemRelation;
	}
	
	public Integer getThirdAddFourFirstFamilyMemAgeAtDeath() {
		return thirdAddFourthFamilyMemAgeAtDeath;
	}
	public Integer getThirdAddFourFirstFamilyMemAge() {
		return thirdAddFourthFamilyMemAge;
	}
	public String getThirdAddFourFirstFamilyMemCauseOfDeath() {
		return thirdAddFourthFamilyMemCauseOfDeath;
	}
	public String getThirdAddFourFirstFamilyMemRelation() {
		return thirdAddFourthFamilyMemRelation;
	}
	
	
	/* ACR Section*/
	public String getAcrQuest1() {
		return acrQuest1;
	}
	public String getAcrQuest1Details() {
		return acrQuest1Details;
	}
	public String getAcrQuest2() {
		return acrQuest2;
	}
	public String getAcrQuest2Details() {
		return acrQuest2Details;
	}
	public String getAcrQuest3() {
		return acrQuest3;
	}
	public String getAcrQuest3Details() {
		return acrQuest3Details;
	}
	public String getAcrQuest4() {
		return acrQuest4;
	}
	public String getAcrQuest4Details() {
		return acrQuest4Details;
	}
	public String getAcrQuest5() {
		return acrQuest5;
	}
	public String getAcrQuest5Details() {
		return acrQuest5Details;
	}
	public String getAcrQuest6() {
		return acrQuest6;
	}
	public String getAcrQuest6Details() {
		return acrQuest6Details;
	}
	public String getAcrQuest7() {
		return acrQuest7;
	}
	public String getAcrQuest7Details() {
		return acrQuest7Details;
	}
	public String getAcrQuest8() {
		return acrQuest8;
	}
	public String getAcrQuest8Details() {
		return acrQuest8Details;
	}
	public String getAcrQuest9() {
		return acrQuest9;
	}
	public String getAcrQuest9Details() {
		return acrQuest9Details;
	}
	public String getAcrQuest10() {
		return acrQuest10;
	}
	public String getAcrQuest10Details() {
		return acrQuest10Details;
	}
	public String getAcrQuest11() {
		return acrQuest11;
	}
	public String getAcrQuest11Details() {
		return acrQuest11Details;
	}
	public String getAcrQuest12() {
		return acrQuest12;
	}
	public String getAcrQuest12Details() {
		return acrQuest12Details;
	}
	public String getAcrQuest13() {
		return acrQuest13;
	}
	public String getAcrQuest13Details() {
		return acrQuest13Details;
	}
	public String getAcrQuest14() {
		return acrQuest14;
	}
	public String getAcrQuest14Details() {
		return acrQuest14Details;
	}
	public String getAcrQuest15() {
		return acrQuest15;
	}
	public String getAcrQuest15Details() {
		return acrQuest15Details;
	}
	public String getAcrQuest16() {
		return acrQuest16;
	}
	public String getAcrQuest16Details() {
		return acrQuest16Details;
	}
	public String getAcrQuest17() {
		return acrQuest17;
	}
	public String getAcrQuest17Details() {
		return acrQuest17Details;
	}
	public String getAcrQuest18() {
		return acrQuest18;
	}
	public String getAcrQuest18Details() {
		return acrQuest18Details;
	}
	public String getAcrQuest19() {
		return acrQuest19;
	}
	public String getAcrQuest19Details() {
		return acrQuest19Details;
	}
	public String getAcrQuest20() {
		return acrQuest20;
	}
	public String getAcrQuest20Details() {
		return acrQuest20Details;
	}
	public String getAcrQuest21() {
		return acrQuest21;
	}
	public String getAcrQuest21Details() {
		return acrQuest21Details;
	}
	public String getAcrQuest22() {
		return acrQuest22;
	}
	public String getAcrQuest22Details() {
		return acrQuest22Details;
	}
	public String getAddInsuredHasCardioDiseases() {
		return addInsuredOneHasCardioDiseases;
	}
	public String getAddInsuredHasCardioDiseasesDetails() {
		return addInsuredOneHasCardioDiseasesDetails;
	}
	public String getAddInsuredHasRespiratoryDiseases() {
		return addInsuredOneHasRespiratoryDiseases;
	}
	public String getAddInsuredHasRespiratoryDiseasesDetails() {
		return addInsuredOneHasRespiratoryDiseasesDetails;
	}
	public String getAddInsuredHasDigestiveDiseases() {
		return addInsuredOneHasDigestiveDiseases;
	}
	public String getAddInsuredHasDigestiveDiseasesDetails() {
		return addInsuredOneHasDigestiveDiseasesDetails;
	}
	public String getAddInsuredHasUrinaryDiseases() {
		return addInsuredOneHasUrinaryDiseases;
	}
	public String getAddInsuredHasUrinaryDiseasesDetails() {
		return addInsuredOneHasUrinaryDiseasesDetails;
	}
	public String getAddInsuredUrinaryDisOptions(){
		return addInsuredOneHasUrinaryDiseasesDetailsOptions;
	}
	public String getAddInsuredHasNervousDiseases() {
		return addInsuredOneHasNervousDiseases;
	}
	public String getAddInsuredHasNervousDiseasesDetails() {
		return addInsuredOneHasNervousDiseasesDetails;
	}
	public String getAddInsuredHasEndocrineDiseases() {
		return addInsuredOneHasEndocrineDiseases;
	}
	public String getAddInsuredHasEndocrineDiseasesDetails() {
		return addInsuredOneHasEndocrineDiseasesDetails;
	}
	public String getAddInsuredHasEndocrineDisOptions() {
		return addInsuredOneHasEndocrineDiseasesDetailsOptions;
	}
	public String getAddInsuredHasSkinDiseases() {
		return addInsuredOneHasSkinDiseases;
	}
	public String getAddInsuredHasSkinDiseasesDetails() {
		return addInsuredOneHasSkinDiseasesDetails;
	}
	public String getAddInsuredHasHaematologicDiseases() {
		return addInsuredOneHasHaematologicDiseases;
	}
	public String getAddInsuredHasHaematologicDiseasesDetails() {
		return addInsuredOneHasHaematologicDiseasesDetails;
	}
	public String getAddInsuredHasENTDiseases() {
		return addInsuredOneHasENTDiseases;
	}
	public String getAddInsuredHasENTDiseasesDetails() {
		return addInsuredOneHasENTDiseasesDetails;
	}
	public String getAddInsuredHasTumourOrCancer() {
		return addInsuredOneHasTumourOrCancer;
	}
	public String getAddInsuredHasTumourOrCancerDetails() {
		return addInsuredOneHasTumourOrCancerDetails;
	}
	public String getAddInsuredHasDisability() {
		return addInsuredOneHasDisability;
	}
	public String getAddInsuredHasDisabilityDetails() {
		return addInsuredOneHasDisabilityDetails;
	}
	public String getAddInsuredHasSexuallyTransmittedDisease() {
		return addInsuredOneHasSexuallyTransmittedDisease;
	}
	public String getAddInsuredHasSexuallyTransmittedDiseaseDetails() {
		return addInsuredOneHasSexuallyTransmittedDiseaseDetails;
	}
	public String getAddInsuredHasWeightAtBirth() {
		return addInsuredOneHasWeightAtBirth;
	}
	public String getAddInsuredHasPshycoAbnormality() {
		return addInsuredOneHasPshycoAbnormality;
	}
	public String getAddInsuredHasPshycoAbnormalityDetails() {
		return addInsuredOneHasPshycoAbnormalityDetails;
	}
	public String getAddInsuredIsPregnant() {
		return addInsuredOneIsPregnant;
	}
	public String getAddInsuredIsPregnantDetails() {
		return addInsuredOneIsPregnantDetails;
	}
	public String getAddInsuredHasPregComplication() {
		return addInsuredOneHasPregComplication;
	}
	public String getAddInsuredHasPregComplicationDetails() {
		return addInsuredOneHasPregComplicationDetails;
	}
	public String getAddInsuredHasDoctor() {
		return addInsuredOneHasDoctor;
	}
	public String getAddInsuredHasDoctorDetails() {
		return addInsuredOneHasDoctorDetails;
	}
	public String getAgentName() {
		String agentName = null;
		try{
			if(agentPerson != null){
				for (PersonName personName : agentPerson.getName()) {
					agentName = personName.getFullName();
				}
			}
		}catch(Exception e){
			LOGGER.error("-- getAgentName -- "+e.getMessage());
		}
		return agentName;
	}	
	
	public String getProductName() {
		return productName;
	}
	public BigDecimal getProductSumAssured() {
		return productSumAssured;
	}
	public Integer getProductPolicyTerm() {
		return productPolicyTerm;
	}
	public Integer getProductPremiumTerm() {
		return productPremiumTerm;
	}
	public String getFirstRiderName() {
		return firstRiderName;
	}
	public String getSecondRiderName() {
		return secondRiderName;
	}
	public String getThirdRiderName() {
		return thirdRiderName;
	}
	public String getFourthRiderName() {
		return fourthRiderName;
	}
	public String getFifthRiderName() {
		return fifthRiderName;
	}
	public String getSixthRiderName() {
		return sixthRiderName;
	}
	public String getSeventhRiderName() {
		return seventhRiderName;
	}
	public String getEightthRiderName() {
		return eightthRiderName;
	}
	public String getNinthRiderName() {
		return ninthRiderName;
	}
	public BigDecimal getFirstRiderSumAssured() {
		return firstRiderSumAssured;
	}
	public BigDecimal getSecondRiderSumAssured() {
		return secondRiderSumAssured;
	}
	public BigDecimal getThirdRiderSumAssured() {
		return thirdRiderSumAssured;
	}
	public BigDecimal getFourthRiderSumAssured() {
		return fourthRiderSumAssured;
	}
	public BigDecimal getFifthRiderSumAssured() {
		return fifthRiderSumAssured;
	}
	public BigDecimal getSixthRiderSumAssured() {
		return sixthRiderSumAssured;
	}
	public BigDecimal getSeventhRiderSumAssured() {
		return seventhRiderSumAssured;
	}
	public BigDecimal getEightthRiderSumAssured() {
		return eightthRiderSumAssured;
	}
	public BigDecimal getNinthRiderSumAssured() {
		return ninthRiderSumAssured;
	}
	public Integer getFirstRiderTerm() {
		return firstRiderTerm;
	}
	public Integer getSecondRiderTerm() {
		return secondRiderTerm;
	}
	public Integer getThirdRiderTerm() {
		return thirdRiderTerm;
	}
	public Integer getFourthRiderTerm() {
		return fourthRiderTerm;
	}
	public Integer getFifthRiderTerm() {
		return fifthRiderTerm;
	}
	public Integer getSixthRiderTerm() {
		return sixthRiderTerm;
	}
	public Integer getSeventhRiderTerm() {
		return seventhRiderTerm;
	}
	public Integer getEightthRiderTerm() {
		return eightthRiderTerm;
	}
	public Integer getNinthRiderTerm() {
		return ninthRiderTerm;
	}
	public String getProductPremiumFrequency() {
		return productPremiumFrequency;
	}
	public String readCheckMark() {
		byte[] bytes;
		byte[] encoded = null;
		InputStream imageInputStream =	this.getClass().getResourceAsStream("/pdfTemplates/Tick.jpg");
		try {
			 bytes = IOUtils.toByteArray(imageInputStream);
			 encoded = Base64.encodeBase64(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String(encoded);
	}
	
	public String getImageLocation() {
		return PropertyFileReader.fetchProperty(GeneraliConstants.PDF_IMAGE_LOC,
				GeneraliConstants.CONFIG_PROPERTY_FILE);
	}

	/**
	 * Encodes the byte array into base64 string
	 * 
	 * @param imageByteArray
	 *            - byte array
	 * @return String a {@link java.lang.String}
	 */
	public String encodeImage(String fileName) {
		File file = new File(fileName);
		byte[] bytes;
		byte[] encoded = null;
		try {
			bytes = FileUtils.loadFile(file);
			encoded = Base64.encodeBase64(bytes);
		} catch (IOException e) {
			LOGGER.error(" Error while encode image : "+e.getMessage());
		}
		return new String(encoded);
	}
	
	/**
	 * populateAddSignatureESignature
	 * 
	 * @return
	 */
	public String populateAddSignatureESignature() {
		String sign = null;
		String temptoken = null;
		Document documentDetail = null;
		String signatureType = "AdditionalInsuredOneSignature";
		LOGGER.info("appionteeDocuments ---- " + appionteeDocuments);

		if (additionalInsured != null) {

			if (additionalInsured.getPartyId() != null
					&& additionalInsured.getPartyId().equalsIgnoreCase("1")) {
				signatureType = "AdditionalInsuredOneSignature";
			} else if (additionalInsured.getPartyId() != null
					&& additionalInsured.getPartyId().equalsIgnoreCase("2")) {
				signatureType = "AdditionalInsuredTwoSignature";
			} else if (additionalInsured.getPartyId() != null
					&& additionalInsured.getPartyId().equalsIgnoreCase("3")) {
				signatureType = "AdditionalInsuredThreeSignature";
			} else if (additionalInsured.getPartyId() != null
					&& additionalInsured.getPartyId().equalsIgnoreCase("4")) {
				signatureType = "AdditionalInsuredFourSignature";
			}
		}
		if (appionteeDocuments != null) {
			try {
				for (Document agreementDocument : appionteeDocuments) {
					if (agreementDocument != null) {
						documentDetail = agreementDocument;
						if (signatureType.equals(agreementDocument
								.getSignatureType())) {
							LOGGER.info("Type Code is Signature ---"
									+ documentDetail.getSignatureType());
							sign = documentDetail.getBase64string();
							StringTokenizer token = new StringTokenizer(sign,
									",");
							while (token.hasMoreTokens()) {
								temptoken = token.nextToken();
							}
							if (temptoken != null) {
								sign = temptoken;
							}
						}
					}

				}
			} catch (Exception e) {
				LOGGER.error("Error in getting sign for spaj " + spajNumber
						+ "type : " + signatureType);
			}
		}
		return sign;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}
	
	public String getInsuredIdentityType() {
		return insuredIdentityType;
	}
	public String getProposerIdentityProofType() {
		return proposerIdentityProofType;
	}
	public String getProposerIdentityNo() {
		return proposerIdentityNo;
	}
	public String getFirstAddInsuredTypeOfIdentity() {
		return firstAddInsuredTypeOfIdentity;
	}
	public String getSecondAddInsuredTypeOfIdentity() {
		return secondAddInsuredTypeOfIdentity;
	}
	public String getThirdAddInsuredTypeOfIdentity() {
		return thirdAddInsuredTypeOfIdentity;
	}
	public String getFourthAddInsuredTypeOfIdentity() {
		return fourthAddInsuredTypeOfIdentity;
	}
	public BigDecimal getTotalInitialPremium() {
		return totalInitialPremium;
	}
	public String getFirstBeneficiaryTypeOfIdentity() {
		return firstBeneficiaryTypeOfIdentity;
	}
	public String getSecondBeneficiaryTypeOfIdentity() {
		return secondBeneficiaryTypeOfIdentity;
	}
	public String getThirdBeneficiaryTypeOfIdentity() {
		return thirdBeneficiaryTypeOfIdentity;
	}
	public String getFourthBeneficiaryTypeOfIdentity() {
		return fourthBeneficiaryTypeOfIdentity;
	}

	public BigDecimal getFirstRiderInitialPremium() {
		return firstRiderInitialPremium;
	}
	public BigDecimal getSecondRiderInitialPremium() {
		return secondRiderInitialPremium;
	}
	public BigDecimal getThirdRiderInitialPremium() {
		return thirdRiderInitialPremium;
	}
	public BigDecimal getFourthRiderInitialPremium() {
		return fourthRiderInitialPremium;
	}
	public BigDecimal getFifthRiderInitialPremium() {
		return fifthRiderInitialPremium;
	}
	public BigDecimal getSixthRiderInitialPremium() {
		return sixthRiderInitialPremium;
	}
	public BigDecimal getSeventhRiderInitialPremium() {
		return seventhRiderInitialPremium;
	}
	public BigDecimal getEightthRiderInitialPremium() {
		return eightthRiderInitialPremium;
	}
	public BigDecimal getNinthRiderInitialPremium() {
		return ninthRiderInitialPremium;
	}

	/**
	 * @return the acrQuesr6Opt1
	 */
	public String getAcrQuesr6Opt1() {
		return acrQuesr6Opt1;
	}

	/**
	 * @param acrQuesr6Opt1 the acrQuesr6Opt1 to set
	 */
	public void setAcrQuesr6Opt1(String acrQuesr6Opt1) {
		this.acrQuesr6Opt1 = acrQuesr6Opt1;
	}

	/**
	 * @return the acrQuesr6Opt2
	 */
	public String getAcrQuesr6Opt2() {
		return acrQuesr6Opt2;
	}

	/**
	 * @param acrQuesr6Opt2 the acrQuesr6Opt2 to set
	 */
	public void setAcrQuesr6Opt2(String acrQuesr6Opt2) {
		this.acrQuesr6Opt2 = acrQuesr6Opt2;
	}

	/**
	 * @return the acrQuesr6Opt3
	 */
	public String getAcrQuesr6Opt3() {
		return acrQuesr6Opt3;
	}

	/**
	 * @param acrQuesr6Opt3 the acrQuesr6Opt3 to set
	 */
	public void setAcrQuesr6Opt3(String acrQuesr6Opt3) {
		this.acrQuesr6Opt3 = acrQuesr6Opt3;
	}

	/**
	 * @return the acrQuesr6Opt4
	 */
	public String getAcrQuesr6Opt4() {
		return acrQuesr6Opt4;
	}

	/**
	 * @param acrQuesr6Opt4 the acrQuesr6Opt4 to set
	 */
	public void setAcrQuesr6Opt4(String acrQuesr6Opt4) {
		this.acrQuesr6Opt4 = acrQuesr6Opt4;
	}

	/**
	 * @return the acrQuesr6Opt5
	 */
	public String getAcrQuesr6Opt5() {
		return acrQuesr6Opt5;
	}

	/**
	 * @param acrQuesr6Opt5 the acrQuesr6Opt5 to set
	 */
	public void setAcrQuesr6Opt5(String acrQuesr6Opt5) {
		this.acrQuesr6Opt5 = acrQuesr6Opt5;
	}

	/**
	 * @return the acrQuesr7Opt1
	 */
	public String getAcrQuesr7Opt1() {
		return acrQuesr7Opt1;
	}

	/**
	 * @param acrQuesr7Opt1 the acrQuesr7Opt1 to set
	 */
	public void setAcrQuesr7Opt1(String acrQuesr7Opt1) {
		this.acrQuesr7Opt1 = acrQuesr7Opt1;
	}

	/**
	 * @return the acrQuesr7Opt2
	 */
	public String getAcrQuesr7Opt2() {
		return acrQuesr7Opt2;
	}

	/**
	 * @param acrQuesr7Opt2 the acrQuesr7Opt2 to set
	 */
	public void setAcrQuesr7Opt2(String acrQuesr7Opt2) {
		this.acrQuesr7Opt2 = acrQuesr7Opt2;
	}

	/**
	 * @return the acrQuesr7Opt3
	 */
	public String getAcrQuesr7Opt3() {
		return acrQuesr7Opt3;
	}

	/**
	 * @param acrQuesr7Opt3 the acrQuesr7Opt3 to set
	 */
	public void setAcrQuesr7Opt3(String acrQuesr7Opt3) {
		this.acrQuesr7Opt3 = acrQuesr7Opt3;
	}

	/**
	 * @return the acrQuesr7Opt4
	 */
	public String getAcrQuesr7Opt4() {
		return acrQuesr7Opt4;
	}

	/**
	 * @param acrQuesr7Opt4 the acrQuesr7Opt4 to set
	 */
	public void setAcrQuesr7Opt4(String acrQuesr7Opt4) {
		this.acrQuesr7Opt4 = acrQuesr7Opt4;
	}

	/**
	 * @return the acrQuesr7Opt5
	 */
	public String getAcrQuesr7Opt5() {
		return acrQuesr7Opt5;
	}

	/**
	 * @param acrQuesr7Opt5 the acrQuesr7Opt5 to set
	 */
	public void setAcrQuesr7Opt5(String acrQuesr7Opt5) {
		this.acrQuesr7Opt5 = acrQuesr7Opt5;
	}

	/**
	 * @return the acrQuesr12Opt1
	 */
	public String getAcrQuesr12Opt1() {
		return acrQuesr12Opt1;
	}

	/**
	 * @param acrQuesr12Opt1 the acrQuesr12Opt1 to set
	 */
	public void setAcrQuesr12Opt1(String acrQuesr12Opt1) {
		this.acrQuesr12Opt1 = acrQuesr12Opt1;
	}

	/**
	 * @return the acrQuesr12Opt2
	 */
	public String getAcrQuesr12Opt2() {
		return acrQuesr12Opt2;
	}

	/**
	 * @param acrQuesr12Opt2 the acrQuesr12Opt2 to set
	 */
	public void setAcrQuesr12Opt2(String acrQuesr12Opt2) {
		this.acrQuesr12Opt2 = acrQuesr12Opt2;
	}

	/**
	 * @return the acrQuesr12Opt3
	 */
	public String getAcrQuesr12Opt3() {
		return acrQuesr12Opt3;
	}

	/**
	 * @param acrQuesr12Opt3 the acrQuesr12Opt3 to set
	 */
	public void setAcrQuesr12Opt3(String acrQuesr12Opt3) {
		this.acrQuesr12Opt3 = acrQuesr12Opt3;
	}

	/**
	 * @return the acrQuesr12Opt4
	 */
	public String getAcrQuesr12Opt4() {
		return acrQuesr12Opt4;
	}

	/**
	 * @param acrQuesr12Opt4 the acrQuesr12Opt4 to set
	 */
	public void setAcrQuesr12Opt4(String acrQuesr12Opt4) {
		this.acrQuesr12Opt4 = acrQuesr12Opt4;
	}

	/**
	 * @return the acrQuesr12Opt5
	 */
	public String getAcrQuesr12Opt5() {
		return acrQuesr12Opt5;
	}

	/**
	 * @param acrQuesr12Opt5 the acrQuesr12Opt5 to set
	 */
	public void setAcrQuesr12Opt5(String acrQuesr12Opt5) {
		this.acrQuesr12Opt5 = acrQuesr12Opt5;
	}

	/**
	 * @return the acrQuesr14Opt1
	 */
	public String getAcrQuesr14Opt1() {
		return acrQuesr14Opt1;
	}

	/**
	 * @param acrQuesr14Opt1 the acrQuesr14Opt1 to set
	 */
	public void setAcrQuesr14Opt1(String acrQuesr14Opt1) {
		this.acrQuesr14Opt1 = acrQuesr14Opt1;
	}

	/**
	 * @return the acrQuesr14Opt2
	 */
	public String getAcrQuesr14Opt2() {
		return acrQuesr14Opt2;
	}

	/**
	 * @param acrQuesr14Opt2 the acrQuesr14Opt2 to set
	 */
	public void setAcrQuesr14Opt2(String acrQuesr14Opt2) {
		this.acrQuesr14Opt2 = acrQuesr14Opt2;
	}

	/**
	 * @return the acrQuesr14Opt3
	 */
	public String getAcrQuesr14Opt3() {
		return acrQuesr14Opt3;
	}

	/**
	 * @param acrQuesr14Opt3 the acrQuesr14Opt3 to set
	 */
	public void setAcrQuesr14Opt3(String acrQuesr14Opt3) {
		this.acrQuesr14Opt3 = acrQuesr14Opt3;
	}

	/**
	 * @return the acrQuesr14Opt4
	 */
	public String getAcrQuesr14Opt4() {
		return acrQuesr14Opt4;
	}

	/**
	 * @param acrQuesr14Opt4 the acrQuesr14Opt4 to set
	 */
	public void setAcrQuesr14Opt4(String acrQuesr14Opt4) {
		this.acrQuesr14Opt4 = acrQuesr14Opt4;
	}

	/**
	 * @return the acrQuesr14Opt5
	 */
	public String getAcrQuesr14Opt5() {
		return acrQuesr14Opt5;
	}

	/**
	 * @param acrQuesr14Opt5 the acrQuesr14Opt5 to set
	 */
	public void setAcrQuesr14Opt5(String acrQuesr14Opt5) {
		this.acrQuesr14Opt5 = acrQuesr14Opt5;
	}

	/**
	 * @return the acrQuesr16Opt1
	 */
	public String getAcrQuesr16Opt1() {
		return acrQuesr16Opt1;
	}

	/**
	 * @param acrQuesr16Opt1 the acrQuesr16Opt1 to set
	 */
	public void setAcrQuesr16Opt1(String acrQuesr16Opt1) {
		this.acrQuesr16Opt1 = acrQuesr16Opt1;
	}

	/**
	 * @return the acrQuesr16Opt2
	 */
	public String getAcrQuesr16Opt2() {
		return acrQuesr16Opt2;
	}

	/**
	 * @param acrQuesr16Opt2 the acrQuesr16Opt2 to set
	 */
	public void setAcrQuesr16Opt2(String acrQuesr16Opt2) {
		this.acrQuesr16Opt2 = acrQuesr16Opt2;
	}

	/**
	 * @return the acrQuesr16Opt3
	 */
	public String getAcrQuesr16Opt3() {
		return acrQuesr16Opt3;
	}

	/**
	 * @param acrQuesr16Opt3 the acrQuesr16Opt3 to set
	 */
	public void setAcrQuesr16Opt3(String acrQuesr16Opt3) {
		this.acrQuesr16Opt3 = acrQuesr16Opt3;
	}

	/**
	 * @return the acrQuesr16Opt4
	 */
	public String getAcrQuesr16Opt4() {
		return acrQuesr16Opt4;
	}

	/**
	 * @param acrQuesr16Opt4 the acrQuesr16Opt4 to set
	 */
	public void setAcrQuesr16Opt4(String acrQuesr16Opt4) {
		this.acrQuesr16Opt4 = acrQuesr16Opt4;
	}

	/**
	 * @return the acrQuesr16Opt5
	 */
	public String getAcrQuesr16Opt5() {
		return acrQuesr16Opt5;
	}

	/**
	 * @param acrQuesr16Opt5 the acrQuesr16Opt5 to set
	 */
	public void setAcrQuesr16Opt5(String acrQuesr16Opt5) {
		this.acrQuesr16Opt5 = acrQuesr16Opt5;
	}

	/**
	 * @return the acrQuesr20Opt1
	 */
	public String getAcrQuesr20Opt1() {
		return acrQuesr20Opt1;
	}

	/**
	 * @param acrQuesr20Opt1 the acrQuesr20Opt1 to set
	 */
	public void setAcrQuesr20Opt1(String acrQuesr20Opt1) {
		this.acrQuesr20Opt1 = acrQuesr20Opt1;
	}

	/**
	 * @return the acrQuesr20Opt2
	 */
	public String getAcrQuesr20Opt2() {
		return acrQuesr20Opt2;
	}

	/**
	 * @param acrQuesr20Opt2 the acrQuesr20Opt2 to set
	 */
	public void setAcrQuesr20Opt2(String acrQuesr20Opt2) {
		this.acrQuesr20Opt2 = acrQuesr20Opt2;
	}

	/**
	 * @return the acrQuesr20Opt3
	 */
	public String getAcrQuesr20Opt3() {
		return acrQuesr20Opt3;
	}

	/**
	 * @param acrQuesr20Opt3 the acrQuesr20Opt3 to set
	 */
	public void setAcrQuesr20Opt3(String acrQuesr20Opt3) {
		this.acrQuesr20Opt3 = acrQuesr20Opt3;
	}

	/**
	 * @return the acrQuesr20Opt4
	 */
	public String getAcrQuesr20Opt4() {
		return acrQuesr20Opt4;
	}

	/**
	 * @param acrQuesr20Opt4 the acrQuesr20Opt4 to set
	 */
	public void setAcrQuesr20Opt4(String acrQuesr20Opt4) {
		this.acrQuesr20Opt4 = acrQuesr20Opt4;
	}

	/**
	 * @return the acrQuesr20Opt5
	 */
	public String getAcrQuesr20Opt5() {
		return acrQuesr20Opt5;
	}

	/**
	 * @param acrQuesr20Opt5 the acrQuesr20Opt5 to set
	 */
	public void setAcrQuesr20Opt5(String acrQuesr20Opt5) {
		this.acrQuesr20Opt5 = acrQuesr20Opt5;
	}

	/**
	 * @param acrQuesr12Opt6 the acrQuesr12Opt6 to set
	 */
	public void setAcrQuesr12Opt6(String acrQuesr12Opt6) {
		this.acrQuesr12Opt6 = acrQuesr12Opt6;
	}

	/**
	 * @return the acrQuesr12Opt6
	 */
	public String getAcrQuesr12Opt6() {
		return acrQuesr12Opt6;
	}

	/**
	 * @param acrQuesr20Opt6 the acrQuesr20Opt6 to set
	 */
	public void setAcrQuesr20Opt6(String acrQuesr20Opt6) {
		this.acrQuesr20Opt6 = acrQuesr20Opt6;
	}

	/**
	 * @return the acrQuesr20Opt6
	 */
	public String getAcrQuesr20Opt6() {
		return acrQuesr20Opt6;
	}
	
	public Date dateValue(){		
		return new Date();				
	}
	
	public String populateESignature(String signatureType) {
		String sign = null;
		String temptoken = null;
		Document documentDetail = null;
		LOGGER.info("appionteeDocuments ---- " + appionteeDocuments);
		// appointee signature detail
		if (appionteeDocuments != null) {
			try{
				for (Document agreementDocument : appionteeDocuments) {
					if (agreementDocument != null) {
						documentDetail = agreementDocument;
						if (signatureType.equals(agreementDocument
								.getSignatureType())) {
							LOGGER.info("Type Code is Signature ---"
									+ documentDetail.getSignatureType());
							sign = documentDetail.getBase64string();
							StringTokenizer token = new StringTokenizer(sign, ",");
							while (token.hasMoreTokens()) {
								temptoken = token.nextToken();
							}
							if (temptoken != null) {
								sign = temptoken;
							}
						}
					}

				}
			}catch(Exception e){
				LOGGER.error("Error in getting sign for spaj " + spajNumber +"tyep : "+signatureType);
			}
		}
		return sign;
	}
	
	public String insuredDate(){
		String newDateString = null;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if(insuredDeclarationDateTime != null){
				newDateString = sdf.format(insuredDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting insuredDeclarationDateTime ", e);
		}
		return newDateString;			
	}
	
	public String insuredSignedTime(){
		String newDateString = null;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			if(insuredDeclarationDateTime != null){
				newDateString = sdf.format(insuredDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting insuredDeclarationTime ", e);
		}
		return newDateString;
	}
	
	public String proposerDate(){	
		String newDateString = null;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if(proposerDeclarationDateTime != null){
				newDateString = sdf.format(proposerDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting proposerDeclarationDateTime ", e);
		}
		return newDateString;		
	}
		
	public String proposerSignedTime(){
		String newDateString = null;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			if(proposerDeclarationDateTime != null){
				newDateString = sdf.format(proposerDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting proposerDeclarationTime ", e);
		}
		return newDateString;
	}
	
	public String addInsDate(){
		String newDateString = null;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			if(additionalInsDeclarationDateTime != null){
				newDateString = sdf.format(additionalInsDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting additionalInsDeclarationDateTime ", e);
		}
		return newDateString;
	}
		
	public String addInsTime(){
		String newDateString = null;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			if(additionalInsDeclarationDateTime != null){
				newDateString = sdf.format(additionalInsDeclarationDateTime);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting additionalInsDeclarationTime ", e);
		}
		return newDateString;
	}
	
	public String getInsuredSignedPlace() {
		String insuredPlace = "";
		try{
			if(insuredDeclarationPlace != null){
				insuredPlace = OmniContextBridge.services().getValueForCode(insuredDeclarationPlace, "en", "STATE");
			}
		}catch(Exception e){
			LOGGER.error("Error .. getInsuredSignedPlace "+e.getMessage());
		}
		
		return insuredPlace;
	}
	
	public String getProposerSignedPlace() {
		String proposerPlace = "";
		try{
			if(proposerDeclarationPlace != null){
				proposerPlace = OmniContextBridge.services().getValueForCode(proposerDeclarationPlace, "en", "STATE");
			}
		}catch(Exception e){
			LOGGER.error("Error .. getProposerSignedPlace "+e.getMessage());
		}
		
		return proposerPlace;
	}
	
	public String getAddInsSignedPlace() {
		String addInsuredPlace = "";
		try{
			if(additionalInsDeclarationPlace != null){
				addInsuredPlace = OmniContextBridge.services().getValueForCode(additionalInsDeclarationPlace, "en", "STATE");
			}
		}catch(Exception e){
			LOGGER.error("Error .. getAddInsSignedPlace "+e.getMessage());
		}
		
		return addInsuredPlace;
	}
	
	public String formatter(String number, String language) {
		double amount = 0;
		BigDecimal bd = new BigDecimal("0");
		String result = number;

		try {
			bd = new BigDecimal(number);
			amount = Double.parseDouble(number);
		} catch (NumberFormatException e) {
			LOGGER.error("Error in formatting value " + number , e);
		} finally {

			DecimalFormat formatter = (DecimalFormat) NumberFormat
					.getInstance(Locale.US);
			DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
			//if ("vn".equals(language)) {
				symbols.setGroupingSeparator('.');
		//	} else {
			//	symbols.setGroupingSeparator(',');
		//	}
			formatter.setDecimalFormatSymbols(symbols);
			if (amount > 0) {
				result = formatter.format(bd);
			}
		}
		return result;
	}
	public String bigDecimalformatter(BigDecimal number) {
		String amount = "";
		try{
			DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
			DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
			symbols.setGroupingSeparator('.');
			formatter.setDecimalFormatSymbols(symbols);
			amount = (formatter.format(number.longValue()));

		}catch(Exception e){
			LOGGER.error("Error in bigDecimalformatter " + number , e);
		}
		return amount;
	}
	public String createFullName(String firstName, String lastName) {
		String fullname = "";
		if (firstName != null) {
			StringBuffer sbFullname = new StringBuffer(firstName);
			try {
				if (lastName != null) {

					sbFullname = sbFullname.append(" ");
					sbFullname = sbFullname.append(lastName);

				}
			} catch (Exception e) {
				LOGGER.error("Error in concatinating full name, First Name: "
						+ firstName + "Last Name : " + lastName, e.getMessage());
			}
			fullname = sbFullname.toString();
		}
		return fullname;
	}

}
