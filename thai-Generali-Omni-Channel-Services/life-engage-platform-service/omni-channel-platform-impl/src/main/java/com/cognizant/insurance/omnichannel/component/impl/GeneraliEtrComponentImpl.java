package com.cognizant.insurance.omnichannel.component.impl;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.component.GeneraliEtrComponent;
import com.cognizant.insurance.omnichannel.utils.GeneraliAsymmetricEncryptionUtil;

@Component
public class GeneraliEtrComponentImpl implements GeneraliEtrComponent {

	@Value("${generali.platform.payment.fetchEtrURL}")
	private String fetchEtrURL;

	@Autowired
	GeneraliAsymmetricEncryptionUtil asymmetricEncryption;

	public static final String X_AGENT_CODE = "X-Agent-Code";

	public static final String JSON_DATA = "json_data";

	
	@Autowired
	private RestTemplate restTemplate;

	public static final Logger LOGGER = LoggerFactory
			.getLogger(GeneraliEtrComponentImpl.class);

	/**
	 * Retrieves the Etr Url .
	 *
	 * @param json
	 * 
	 * @return the reponse from external service
	 * @throws IOException
	 * @throws SignatureException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */

    public String retrieveEtr(JSONObject json, String agentId) throws BusinessException, ParseException,
            InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, IOException {
        LOGGER.info("E-TR Service Start");
        String result = "";
        ResponseEntity<String> response;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);        
        
        String encryptedJson = asymmetricEncryption.encrypt(agentId);
		System.out.println("The encryptedJson string " + encryptedJson);
        headers.add(X_AGENT_CODE, agentId + ":" + encryptedJson);
        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
        parameters.add(JSON_DATA, json.toString());
        System.out.println("The Json string " + json.toString());
        try {
            response =
                    restTemplate.postForEntity(fetchEtrURL, new HttpEntity<MultiValueMap<String, Object>>(parameters,
                            headers), String.class);
            result = response.getBody();
			System.out.println("The retrieveEtr value is "+result);
        } catch (RestClientException e) {
            LOGGER.error("Error while communication to E-TR System :" + e.getMessage());
            throw new BusinessException("Unable to fetch E-tr Number - RestClient Exception", e);
        } catch (Exception e) {
            throw new BusinessException("Unable to complete fetch E-tr Service request", e);
        }
        System.out.println("After call the Service and Status code is = " + response.getStatusCode().value());
        if (response.getStatusCode().value() != 200) {
            LOGGER.error("Vendor Service error : Status " + response.getStatusCode() + " - " + response.getBody());
            throw new BusinessException(
                    "Unable to complete fetch E-tr Service request : Failed to process the response");

        }

        LOGGER.info("E-TR Service End");
        return result;
    }

}
