/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 28, 2013
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.cognizant.insurance.component.LifeEngageDocumentComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.services.DocumentService;
import com.cognizant.insurance.services.vo.FileUploadForm;
import com.cognizant.insurance.utils.Base64Utils;

/**
 * The Class class DocumentServiceImpl.
 * 
 * @author 300797
 */
@Service("documentService")
public class DocumentServiceImpl implements DocumentService {
	
	/** The life engage document component. */
	@Autowired
    private LifeEngageDocumentComponent lifeEngageDocumentComponent;

    /** The document upload holder. */
    @Autowired
    @Qualifier("documentUploadMapping")
    private LifeEngageSmooksHolder documentUploadHolder;    
  

    /** The document upload holder. */
    @Autowired
    @Qualifier("uploadDocumentMapping")
    private LifeEngageSmooksHolder uploadDocumentHolder;    
    
    /** The get document holder. */
    @Autowired
    @Qualifier("getDocumentMapping")
    private LifeEngageSmooksHolder getDocumentHolder;  


    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(DocumentServiceImpl.class);

    /** The uploaded file path. */    
     /** The uploaded file path. */
  
     private static final String UPLOADED_FILE_PATH = "/apps/lifeengage/dev/lifeengageservices/eApp_FileUploads/";

    /** The Constant SAVE_ERROR_MESSAGE. */
    private static final String SAVE_ERROR_MESSAGE = "Error while saving file";

    /** The Constant PROPOSAL_NUMBER. */
    private static final String PROPOSAL_NUMBER = "proposalNumber";

    /** The Constant ERROR. */
    private static final String ERROR = "Error";

    /** The Constant HEADER_KEY. */
    private static final String HEADER_KEY = "Content-Disposition";

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";
    
    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";
    
    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";
    
    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";
    
    /** The Constant E_APP. */
    private static final String E_APP = "eApp";
    
	/** The Constant ILLUSTRATION. */
	private static final String ILLUSTRATION = "illustration";

    private static final String FNA = "FNA";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";
    
	/** The Constant TYPE. */
	private static final String KEY4 = "Key4";

	/** The Constant TransactionData. */
	private static final String TRANSACTIONDATA = "TransactionData";
	
	/** The Constant TYPE. */
	private static final String KEY3 = "Key3";
	
	/** The Constant PRODUCTDETAILS. */
	private static final String PRODUCTDETAILS = "ProductDetails";
	
	/** The Constant PRODUCTCODE. */
	private static final String PRODUCTCODE = "productCode";
	private static final String TEMPLATES="templates";
	private static final String E_APP_PDF_TEMPALTE="eAppPdfTemplate";
	private static final String ILLUSTRATION_PDF_TEMPLATE="illustrationPdfTemplate";
    /** The Constant "Product". */
    private static final String PRODUCT = "Product";
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.DocumentService#uploadDocument(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public  String uploadDocument(final String inputJsonObj) throws BusinessException {
    	
    	LOGGER.trace("Inside DocumentServiceImpl : uploadDocument" + inputJsonObj);
        Transactions transactions = new Transactions();
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        RequestInfo requestInfo = null;
        try {
            JSONObject jsonObject = new JSONObject(inputJsonObj);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);

            if (jsonRqArray.length() > 0) {

                final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
                LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), transactions,false);
                transactions = (Transactions) uploadDocumentHolder.parseJson(inputJsonObj);
                try {
                	lifeEngageDocumentComponent.uploadDocuments(transactions, requestInfo.getTransactionId(),
                				transactions.getDocument());
                    transactions.setStatusData(getStatusDatObject(LifeEngageComponentHelper.SUCCESS,
                            LifeEngageComponentHelper.SUCCESS_CODE, "Successfully uploaded document"));

                } catch (IOException e) {
                    LOGGER.error("IO Exception : ", e);
                    transactions.setStatusData(getStatusDatObject(LifeEngageComponentHelper.FAILURE,
                            LifeEngageComponentHelper.FAILURE_CODE, e.getMessage() + " : " + getExceptionMessage(e)));
                } catch (Exception e) {
                    LOGGER.error("Exception : ", e);
                    transactions.setStatusData(getStatusDatObject(LifeEngageComponentHelper.FAILURE,
                            LifeEngageComponentHelper.FAILURE_CODE, e.getMessage() + " : " + getExceptionMessage(e)));
                }
                String eAppResponse = uploadDocumentHolder.parseBO(transactions);
                jsonRsArray.put(new JSONObject(eAppResponse));
                LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

            }
        } catch (ParseException e) {
            LOGGER.error("Parse Exception : ", e);
            throwBusinessException(true, e.getMessage(), e);
        }

        return jsonObjectRs.toString();
    }

	@Transactional(rollbackFor = { Exception.class })
	public  String uploadDocumentForPdf(final String inputJsonObj)
			throws BusinessException {
		return uploadDocument(inputJsonObj);
	}

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.DocumentService#getDocument(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public  String getDocument(final String inputJsonObj) throws BusinessException {
        Transactions transactions = new Transactions();
        RequestInfo requestInfo = null;
        JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        try {
            JSONObject jsonObject = new JSONObject(inputJsonObj);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);

            if (jsonRqArray.length() > 0) {
                final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
                LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), transactions,false);
                transactions = (Transactions) getDocumentHolder.parseJson(jsonObj.toString());
                if (transactions.getProposalNumber() == null || transactions.getDocument() == null
                        || transactions.getDocument().getName() == null) {
                    throwBusinessException(true, "Key4 and Document name is mandatory");
                } else {
                
                    String documentName =
                            jsonObj.getJSONObject("TransactionData")
                                .getJSONObject("Documents")
                                .getString("documentName");
                    AgreementDocument agreementDocument;
                    try {
                        agreementDocument =
                        		lifeEngageDocumentComponent.getDocument(transactions, documentName, requestInfo.getTransactionId());
                        transactions.setDocument(agreementDocument);
                    } catch (IOException e) {
                        LOGGER.error("IO Exception : ", e);
                        transactions.setStatusData(getStatusDatObject(LifeEngageComponentHelper.FAILURE,
                                LifeEngageComponentHelper.FAILURE_CODE, e.getMessage() + " : " + getExceptionMessage(e)));                        
                    }
                    transactionsList.add(transactions);
                    String transactionArray = getDocumentHolder.parseBO(transactionsList);
                    jsonObject = new JSONObject(transactionArray);
                    jsonRsArray = jsonObject.getJSONArray(TRANSACTIONS);
                    LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
                }
            }
        } catch (ParseException e) {
            LOGGER.error("Parse Exception : ", e);
            throwBusinessException(true, e.getMessage(), e);
        }
        return jsonObjectRs.toString();
    }

    /**
     * Gets the status dat object.
     * 
     * @param status
     *            the status
     * @param statusCode
     *            the status code
     * @param statusMessage
     *            the status message
     * @return the status dat object
     */
    protected StatusData getStatusDatObject(final String status, final String statusCode, final String statusMessage) {
        final StatusData statusData = new StatusData();
        statusData.setStatus(status);
        statusData.setStatusCode(statusCode);
        statusData.setStatusMessage(statusMessage);

        return statusData;
    }
    
    /**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.DocumentService#uploadFiles(java.lang.String)
     */
    @Override
    public  String uploadFiles(final String inputJsonObj) throws BusinessException {

        final Map<String, String> fileUploadStatus = new HashMap<String, String>();
        try {
            final JSONObject jsonObject = new JSONObject(inputJsonObj);
            final String proposalNumber = jsonObject.getString(PROPOSAL_NUMBER);

            final JSONArray jsonFilesArray = jsonObject.getJSONArray("files");
            if (jsonFilesArray.length() > 0) {
                for (int i = 0; i < jsonFilesArray.length(); i++) {
                    final JSONObject fileObj = jsonFilesArray.getJSONObject(i);
                    final String fileName = fileObj.getString("fileName");
                    String fileContent = fileObj.getString("fileContent");
                    final String pattern = "data(.*?)\\;base64,";
                    fileContent = fileContent.replaceAll(pattern, "");
                    // Converting a Base64 String into byte array
                    final byte[] decodedBytes = Base64.decodeBase64(fileContent);

                    if (StringUtils.isEmpty(proposalNumber)) {
                        LOGGER.error("Error while saving file : Proposal Number Empty");
                        fileUploadStatus.put(fileName, "Failed");
                    } else {
                        try {
                            fileUploadStatus.put(fileName, "Success");
                            fileUploadStatus.put(PROPOSAL_NUMBER, proposalNumber);
                            createFileFromByteArray(proposalNumber, fileName, decodedBytes);
                        } catch (FileNotFoundException e) {
                            LOGGER.error(SAVE_ERROR_MESSAGE);
                            fileUploadStatus.put(fileName, "Failed");
                        } catch (IOException e) {
                            LOGGER.error(SAVE_ERROR_MESSAGE);
                            fileUploadStatus.put(fileName, "Failed");
                        }
                    }
                }
            }
        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
        }
        return documentUploadHolder.parseBO(fileUploadStatus);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.services.DocumentService#saveFiles(com.cognizant.insurance.services.vo.FileUploadForm)
     */
    @Override
    public  String saveFiles(final FileUploadForm uploadForm) {
        final String proposalNumber = uploadForm.getProposalNumber();
        final CommonsMultipartFile[] files = uploadForm.getFiles();
        String outPut = "Success";

        if (files != null && files.length != 0) {
            for (MultipartFile file : files) {
                if (!file.isEmpty() && !proposalNumber.isEmpty()) {
                    try {
                        final File tempDirectory = new File(UPLOADED_FILE_PATH + proposalNumber);
                        // Directory does'nt exists - create Directory and the files
                        if (tempDirectory.exists()) {
                            // Directory exists - create the file alone
                            createFile(file, tempDirectory);
                            // ToDo - add document path in agreementdoc
                        } else {
                            // Create the parent directory and the sub directory.
                            if (tempDirectory.mkdirs()) {
                                // Create the file
                                createFile(file, tempDirectory);
                            } else {
                                LOGGER.error("Failed to create directory!");
                            }
                        }

                    } catch (IOException e) {
                        LOGGER.error(SAVE_ERROR_MESSAGE);
                        outPut = ERROR;
                        break;
                    }

                } else {
                    LOGGER.error(SAVE_ERROR_MESSAGE);
                    outPut = ERROR;
                    break;
                }
            }
        }

        return outPut;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.DocumentService#getFile(java.lang.String, java.lang.String,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public  void getFile(final String fileName, final String proposalNumber, final HttpServletRequest request,
            final HttpServletResponse response) {

        final ServletContext context = RequestContextUtils.getWebApplicationContext(request).getServletContext();       
        String mimeType = context.getMimeType(UPLOADED_FILE_PATH + "/" + proposalNumber + "/" + fileName);

        if (mimeType == null) {
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream";
        }
        try {
            // get your file as InputStream         
            final File file = new File(UPLOADED_FILE_PATH + "/" + proposalNumber + "/" + fileName);
            final InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

            // set content attributes for the response
            response.setContentType(mimeType);
            response.setContentLength((int) file.length());

            // set headers for the response
            final String headerKey = HEADER_KEY;
            final String headerValue = String.format("attachment; filename=\"%s\"", file.getName());
            response.setHeader(headerKey, headerValue);

            // copy it to response's OutputStream
            IOUtils.copy(inputStream, response.getOutputStream());

            inputStream.close();
            response.flushBuffer();

        } catch (IOException ex) {
            LOGGER.error("Error writing file to output stream. Filename was '" + fileName + "'");
            throw new SystemException(Constants.OUTPUT_STREAM_IO_ERROR, ex);
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.DocumentService#deleteFile(java.lang.String)
     */
    @Override
    public  String deleteFile(final String inputJsonObj) throws BusinessException {
        JSONObject jsonObject;
        final Map<String, String> fileDeleteStatus = new HashMap<String, String>();

        try {
            jsonObject = new JSONObject(inputJsonObj);
            final String proposalNumber = jsonObject.getString(PROPOSAL_NUMBER);
            final String fileName = jsonObject.getString("fileName");
            // get the file           
            final File file = new File(UPLOADED_FILE_PATH + "/" + proposalNumber + "/" + fileName);
            fileDeleteStatus.put(fileName, "Success");
            fileDeleteStatus.put(PROPOSAL_NUMBER, proposalNumber);
            if (file.exists()) {
                if (!file.delete()) {
                    LOGGER.error("File Deletion Error!");
                    fileDeleteStatus.put(fileName, ERROR);
                }
            } else {
                LOGGER.error("File Does not exists!");
                fileDeleteStatus.put(fileName, ERROR);
            }
        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
        }
        return documentUploadHolder.parseBO(fileDeleteStatus);
    }


    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.DocumentService#generatePDF_GET(java.lang.String, java.lang.String,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly=true)
    public  void
            generatePdfGet(final String proposalnumber, final String type, final String templateId, final HttpServletResponse response)
                    throws BusinessException, ParseException {

        InputStream inputStream = null;
        ByteArrayOutputStream byteStream = null;
		String fileName = "";

        try {
            if (E_APP.equals(type)) {
                // get your file as InputStream
				byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
						.generatePdfGet(proposalnumber, type,templateId);
				inputStream = new ByteArrayInputStream(byteStream.toByteArray());
				fileName = "EappProposal_" + proposalnumber + ".pdf";
			} else if (ILLUSTRATION.equals(type)) {
				
				byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
						.generateIllustrationPdf(proposalnumber, type ,templateId);
                inputStream = new ByteArrayInputStream(byteStream.toByteArray());
				fileName = "IllustrationProposal_" + proposalnumber + ".pdf";
            } else if (FNA.equals(type)) {
                byteStream =
                        (ByteArrayOutputStream) lifeEngageDocumentComponent.generateFNAReport(proposalnumber, type,
                                templateId);
                inputStream = new ByteArrayInputStream(byteStream.toByteArray());
                fileName = "FNAReport_" + proposalnumber + ".pdf";
            }

            // set content attributes for the response
            response.setContentType(Constants.TYPE_APPLICATION_PDF);
            response.setContentLength(byteStream.toByteArray().length);

            // set headers for the response
            final String headerKey = HEADER_KEY;
			final String headerValue = String.format(
					"attachment; filename=\"%s\"", fileName);
            response.setHeader(headerKey, headerValue);

            // copy it to response's OutputStream
            IOUtils.copy(inputStream, response.getOutputStream());

            inputStream.close();
            response.flushBuffer();

        } catch (Exception ex) {
            LOGGER.error("Error writing file to output stream. Filename was '" + "'");
            throw new SystemException(Constants.OUTPUT_STREAM_IO_ERROR, ex);
        }
    }
     /**
     * Generate base 64 string pdf for eApp and illustration.
     * 
     * @param json 
     *            the json objct
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
	@Override
	@Transactional(rollbackFor = { Exception.class }, readOnly = true)
	public  String generateBase64StringPdf(final String json)
			throws BusinessException {
		String base64pdf = "";
		String type = "";
		String proposalNumber = "";
		JSONObject jsonObject;
		
		final JSONObject jsonObjectRs = new JSONObject();

		try {
			jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonRqArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final JSONArray jsonRsArray = new JSONArray();
			ByteArrayOutputStream byteStream = null;
			if (jsonRqArray.length() > 0) {
				for (int i = 0; i < jsonRqArray.length(); i++) {
					JSONObject jsonObj = jsonRqArray.getJSONObject(i);
					JSONObject responseJsonObj = new JSONObject();
					type = jsonObj.getString(TYPE);
					proposalNumber = (String) jsonObj.get(KEY4);
					try {
						if (E_APP.equals(jsonObj.getString(TYPE))) {
							JSONObject jsonTransactionData = null;
							String templateId = "";
							if (!(jsonObj.isNull(TRANSACTIONDATA))) {
								jsonTransactionData = jsonObj
										.getJSONObject(TRANSACTIONDATA);
								if (!(jsonTransactionData.isNull(PRODUCT))&&
										jsonTransactionData.getJSONObject(PRODUCT).has(TEMPLATES)&&
										jsonTransactionData.getJSONObject(PRODUCT).getJSONObject(TEMPLATES).has(E_APP_PDF_TEMPALTE)) {
									JSONObject productJsonObj = jsonTransactionData.getJSONObject(PRODUCT);
									templateId = productJsonObj.getJSONObject(TEMPLATES).getString(E_APP_PDF_TEMPALTE);
								}
							}
							// get your file as InputStream
							byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
									.generatePdfGet(proposalNumber, type,templateId);

						} else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
							/* Getting IllustartionOutput object from json */
							JSONObject jsonTransactionData = null;
							String templateId = "";
							if (!(jsonObj.isNull(TRANSACTIONDATA))) {
								jsonTransactionData = jsonObj
										.getJSONObject(TRANSACTIONDATA);
								if (!(jsonTransactionData.isNull("Product"))&&
										jsonTransactionData.getJSONObject(PRODUCT).has(TEMPLATES)&&
										jsonTransactionData.getJSONObject(PRODUCT).getJSONObject(TEMPLATES).has(ILLUSTRATION_PDF_TEMPLATE)) {
									JSONObject productJsonObj = jsonTransactionData.getJSONObject(PRODUCT);
									templateId = productJsonObj.getJSONObject(TEMPLATES).getString(ILLUSTRATION_PDF_TEMPLATE);
								}
							}
							proposalNumber = (String) jsonObj.get(KEY3);
							byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
									.generateIllustrationPdf(proposalNumber,
											type,templateId);
						}
						byte[] data = byteStream.toByteArray();
						base64pdf = Base64Utils.encode(data);
						responseJsonObj.put("base64pdf", base64pdf);
						jsonRsArray.put(responseJsonObj);
					} catch (SystemException e) {
						jsonRsArray.put(new JSONObject(e.getMessage()));
					}
				}
			}
			LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo,
					jsonRsArray, jsonObjectRs, null);
		} catch (ParseException e) {
			throwBusinessException(true, e.getMessage());
		}
		return jsonObjectRs.toString();
	}

    /**
     * Creates the file.
     * 
     * @param file
     *            the file
     * @param tempDirectory
     *            the temp directory
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void createFile(final MultipartFile file, final File tempDirectory) throws IOException {

        final File someFile = new File(tempDirectory, file.getOriginalFilename());
        final FileOutputStream fos = new FileOutputStream(someFile);
        fos.write(file.getBytes());
        fos.flush();
        fos.close();
    }

    /**
     * Creates the file from byte array.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param fileName
     *            the file name
     * @param fileByteArray
     *            the file byte array
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void
            createFileFromByteArray(final String proposalNumber, final String fileName, final byte[] fileByteArray)
                    throws IOException {
        final File tempDirectory = new File(UPLOADED_FILE_PATH + proposalNumber);
        // Directory does'nt exists - create Directory and the files
        if (tempDirectory.exists()) {
            // Directory exists - create the file alone
            createFile(fileName, fileByteArray, tempDirectory);
            // ToDo - add document path in agreementdoc
        } else {
            // Create the parent directory and the sub directory.
            if (tempDirectory.mkdirs()) {
                // Create the file
                createFile(fileName, fileByteArray, tempDirectory);
            } else {
                LOGGER.error("Failed to create directory!");

            }
        }
    }

    /**
     * Creates the file.
     * 
     * @param fileName
     *            the file name
     * @param fileByteArray
     *            the file byte array
     * @param tempDirectory
     *            the temp directory
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void createFile(final String fileName, final byte[] fileByteArray, final File tempDirectory)
            throws IOException {
        final File someFile = new File(tempDirectory, fileName);
        final FileOutputStream fos = new FileOutputStream(someFile);
        fos.write(fileByteArray);
        fos.flush();
        fos.close();
    }

    /**
     * Gets the document upload holder.
     * 
     * @return the documentUploadHolder
     */
    public final LifeEngageSmooksHolder getDocumentUploadHolder() {
        return documentUploadHolder;
    }

    /**
     * Sets the document upload holder.
     * 
     * @param documentUploadHolder
     *            the documentUploadHolder to set
     */
    public final void setDocumentUploadHolder(final LifeEngageSmooksHolder documentUploadHolder) {
        this.documentUploadHolder = documentUploadHolder;
    }
}
