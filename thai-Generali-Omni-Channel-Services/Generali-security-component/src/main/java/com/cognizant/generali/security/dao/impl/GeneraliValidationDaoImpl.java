/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Feb 18, 2015
 */
package com.cognizant.generali.security.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.generali.security.entity.GeneraliUserToken;
import com.cognizant.le.security.dao.ValidationDao;
import com.cognizant.le.security.entity.User;
import com.cognizant.le.security.entity.UserToken;
import com.cognizant.le.security.exception.DaoException;
import com.cognizant.le.security.request.Request;
import com.cognizant.le.security.request.impl.JPARequestImpl;
import com.cognizant.le.security.response.vo.TokenInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class class ValidationDaoImpl.
 */
public class GeneraliValidationDaoImpl implements ValidationDao {

    /** The Constant USER_GET_TOKEN. */
    private static final String GENERALI_USER_GET_TOKEN = "GeneraliUserToken.fetchTokenDetails";

    /** The Constant USER_FETCH_USER_DETAILS. */
    private static final String USER_FETCH_USER_DETAILS = "User.fetchUserDetails";

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliValidationDaoImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#save(com.cognizant.icr.request.Request)
     */
    
    public final <T> void save(final Request<User> userRq) {
        try {
            LOGGER.debug("METHOD ENTRY : ValidationDaoImpl.save ");
            final EntityManager entityManager = ((JPARequestImpl<User>) userRq).getEntityManager();

            entityManager.persist(userRq.getType());
            LOGGER.debug("METHOD EXIT : ValidationDaoImpl.save ");
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#merge(com.cognizant.icr.request.Request)
     */
    
    public final <T> void merge(final Request<TokenInfo> tokenInfoRq) {
        try {
            LOGGER.debug("METHOD ENTRY : ValidationDaoImpl.merge ");

            final EntityManager entityManager = ((JPARequestImpl<TokenInfo>) tokenInfoRq).getEntityManager();

            TokenInfo tokenInfo = tokenInfoRq.getType();

           
                GeneraliUserToken userToken = new GeneraliUserToken();
                userToken.setTimeStamp(tokenInfo.getLastUsedAt());
                userToken.setToken(tokenInfo.getToken());
                userToken.setSeriesId(tokenInfo.getSeries());
                userToken.setUserId(tokenInfo.getUsername());
                entityManager.merge(userToken);
          

            LOGGER.debug("METHOD EXIT : ValidationDaoImpl.merge ");

        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.login.dao.ValidationDao#getTokenDetails(com.cognizant.login.request.Request)
     */
    
    public final GeneraliUserToken getTokenDetails(final Request<String> seriesRq) {
        LOGGER.debug("METHOD ENTRY : ValidationDaoImpl.getTokenDetails ");
        GeneraliUserToken userToken = null;
        try {
            final EntityManager entityManager = ((JPARequestImpl<String>) seriesRq).getEntityManager();

            final Query query = entityManager.createNamedQuery(GENERALI_USER_GET_TOKEN);
            query.setParameter("seriesId", seriesRq.getType());

            List<GeneraliUserToken> userTokenList = (List<GeneraliUserToken>) query.getResultList();
            if (CollectionUtils.isNotEmpty(userTokenList)) {
                for (GeneraliUserToken userObj : userTokenList) {
                    userToken = userObj;
                }
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
        LOGGER.debug("METHOD EXIT : ValidationDaoImpl.getTokenDetails ");
        return userToken;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.security.dao.ValidationDao#updateToken(com.cognizant.le.security.request.Request)
     */
    
    public void updateToken(Request<TokenInfo> tokenInfo) {
        LOGGER.debug("METHOD ENTRY : ValidationDaoImpl.updateToken ");
        try {
            final EntityManager entityManager = ((JPARequestImpl<TokenInfo>) tokenInfo).getEntityManager();

            final Query query = entityManager.createNamedQuery(GENERALI_USER_GET_TOKEN);
            query.setParameter("seriesId", tokenInfo.getType().getSeries());

            List<GeneraliUserToken> userTokenList = (List<GeneraliUserToken>) query.getResultList();
            if (CollectionUtils.isNotEmpty(userTokenList)) {
            	GeneraliUserToken userToken = userTokenList.get(0);
                userToken.setTimeStamp(tokenInfo.getType().getLastUsedAt());
                entityManager.merge(userToken);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
}
