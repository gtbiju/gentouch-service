/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * @author 291433
 *
 */
public enum PolicyIssueSubTypeCodeList {

	/**
	 * 
	 */
	Other,
	/**
	 * 
	 */
	Unknown,
	/**
	 * Policies require a completed medical
	 * questionnaire and medical exam performed
	 * by a medical doctor. A full health history,
	 * along with height and weight, blood
	 * pressure and blood and urine samples are
	 * collected. Any pre-existing conditions as
	 * defined by the contract would not be
	 * covered by the policy.
	 */
	FullMedical,
	/**
	 * Policies require a more extensive health
	 * history than simplified issue policies. No
	 * medical or paramedical exam is required.
	 */
	Non_Medical,
	/**
	 * Policies require a full paramedical exam. A
	 * full health history, along with height and
	 * weight, blood pressure and blood and urine
	 * samples are collected.
	 */
	Paramedical,
}
