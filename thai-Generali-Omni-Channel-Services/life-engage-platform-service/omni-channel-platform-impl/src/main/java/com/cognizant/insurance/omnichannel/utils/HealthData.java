package com.cognizant.insurance.omnichannel.utils;

public class HealthData {

	private Long healthShortFall;
	
	private Long healthRequired;
	
	private Long healthSavings;
	
	private String categoryAxisSavings;
    
    public String getCategoryAxisSavings() {
        return categoryAxisSavings;
    }

    public void setCategoryAxisSavings(String categoryAxisSavings) {
        this.categoryAxisSavings = categoryAxisSavings;
    }

    public String getCategoryAxisFund() {
        return categoryAxisFund;
    }

    public void setCategoryAxisFund(String categoryAxisFund) {
        this.categoryAxisFund = categoryAxisFund;
    }

    private String categoryAxisFund;

	public Long getHealthShortFall() {
		return healthShortFall;
	}

	public void SetHealthShortFall(Long healthShortFall) {
		this.healthShortFall = healthShortFall;
	}

	public Long getHealthRequired() {
		return healthRequired;
	}

	public void setHealthRequired(Long healthRequired) {
		this.healthRequired = healthRequired;
	}

	public Long getHealthSavings() {
		return healthSavings;
	}

	public void setHealthSavings(Long healthSavings) {
		this.healthSavings = healthSavings;
	}
}
