/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.cognizant.insurance.agent.domain.agreement.agreementcodelists.CoverageCodeList;
import com.cognizant.insurance.agent.domain.agreement.agreementcodelists.CoveragePriorityCodeList;
import com.cognizant.insurance.agent.domain.agreement.agreementpremium.Premium;
import com.cognizant.insurance.agent.domain.commonelements.commonclasses.FundInformation;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.DeductibleAppliesToCodeList;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.DeductibleTypeCodeList;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.LimitAppliesToCodeList;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.PaymentFrequencyCodeList;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.PolicyProductTypeCodeList;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;

/**
 * This concept defines the deductible characteristics of the selected coverage.
 * It also defines the key characteristics of a benefit limit regarding a
 * coverage or the whole contract (Probable maximum loss).
 * 
 * Stop loss = maximum amount whatever the number of claims. 
 * 
 * In an insurance policy, the deductible (North American term) or excess (UK
 * term) is the portion of any claim that is not covered by the insurance
 * provider. It is the amount of expenses that must be paid out of pocket before
 * an insurer will cover any expenses. It is normally quoted as a fixed quantity
 * and is a part of most policies covering losses to the policy holder. The
 * deductible must be paid by the insured, before the benefits of the policy can
 * apply. Typically, a general rule is: the higher the deductible, the lower the
 * premium, and vice versa.
 * 
 * In a typical automobile insurance policy, a deductible will apply to claims
 * arising from damage to or loss of the policy holder's own vehicle, whether
 * this damage/loss is caused by accidents for which the holder is responsible,
 * or vandalism and theft. Third-party liability coverage generally has no
 * deductible, since the third party will likely attempt to recover any loss,
 * however small, for which the policy holder is liable.
 * 
 * Most health insurance policies and some travel insurance policies have
 * deductibles as well. The type of health insurance deductibles can also vary,
 * as individual amounts and family amounts. Major medical insurance policies
 * are known for often having a deductible which does not cover the cost of
 * routine visits (e.g., to a doctor's office).
 * 
 * For example, a person might have an auto insurance policy with a $500
 * deductible on collision coverage. If this person were in an accident that did
 * $800 worth of damage to the car, then the insurance company would pay him or
 * her $300. The insured is responsible for the first $500 of damage (the
 * deductible), and the insurance company pays the balance. In industrial risks
 * it is also common for the deductible to be expressed as a Float of the loss,
 * often though not always, with a minimum and maximum amount, for example 10%
 * of loss minimum 1,500USD max 5,000USD. Therefore in the event of a claim
 * totaling 25,000USD the applicable deductible is 2,500 (i.e. 10% of the loss),
 * meaning that the assured would receive an indemnity payment from the insurer
 * of 22,500USD. If the claim only amounts to 7,500USD then the applicable
 * deductible is 1,500USD and not 10% of the loss, since 10% is below the
 * minimum deductible level. Similarly, in this instance, for losses above
 * 50,000USD the deductible will never be more than 5,000USD. Deductibles can
 * also differ depending on the cause of the claim: the same policy can contain
 * varying deductibles which are applied to loss or damage arising from theft,
 * fire, natural perils, etc.
 * 
 * For example a plastics factory may have a deductible of 10,000 for theft,
 * 50,000 for natural perils and 100,000 for fire.
 * 
 * There are two types of deductibles: - Straight deductible: A straight
 * deductible is a deductible that applies to each separate loss, and is the
 * type of deductible in most personal lines of insurance. For instance, if,
 * during the course of a year, you are involved in 2 separate auto accidents,
 * then the deductible will be subtracted for each accident. - Aggregate
 * deductible: Commercial insurance often has an aggregate deductible, which is
 * the total deductible for a given policy period. For instance, if a business
 * has property insurance with an aggregate deductible of $10,000, then the
 * business will have to cover the 1st $10,000 worth of losses for each policy
 * period - usually a year. So if, during the course of a year, a business has 3
 * separate losses of $2,000, then $6,000, then $4,000, the business will be
 * have to cover $10,000 of its losses, but will receive $2,000 from its
 * insurance company for the last loss. Any more losses that occur within the
 * same policy period will be fully covered. So if the business has another loss
 * of $3,000, it will receive the full amount from its insurer. An aggregate
 * deductible makes more sense in commercial insurance, since businesses can
 * have many separate losses during the course of a year, but would be unusual
 * for an individual to experience multiple losses within a year.
 * 
 * A deductible should never be confused with a franchise, the latter
 * representing a threshold which needs to be exceeded in order for the insurer
 * to be liable for the entirety of the claim. In other words, with a franchise
 * of 20,000USD a claim of 19,900 USD is borne entirely by the assured whereas
 * in the event of a claim totaling 20,500 USD the insurer would be liable for
 * the whole amount.
 * 
 * @author 301350  - This class was created combining CoverageLimit, 
 * CoverageDeductible and CoverageComponent  - eApp Requirement. 
 * 
 * 
 */

@Entity
public class Coverage extends Agreement {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4468085382189088597L;
    
    /**
     * This relationship links the premium related to a specific coverage with
     * the coverage component defined on the policy.
     * 
     * e.g: Glass breakage coverage premium.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    private Set<Premium> premium;
    

    /**
     * Amount of the coverage deductible. 
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount deductibleAmount;

    /**
     * Indicates whether the deductible applies per claim or for all the
     * occurred claims. There are two types of deductibles: - Straight
     * deductible: A straight deductible is a deductible that applies to each
     * separate loss, and is the type of deductible in most personal lines of
     * insurance. For instance, if, during the course of a year, you are
     * involved in 2 separate auto accidents, then the deductible will be
     * subtracted for each accident. - Aggregate deductible: Commercial
     * insurance often has an aggregate deductible, which is the total
     * deductible for a given policy period. For instance, if a business has
     * property insurance with an aggregate deductible of $10,000, then the
     * business will have to cover the 1st $10,000 worth of losses for each
     * policy period - usually a year. So if, during the course of a year, a
     * business has 3 separate losses of $2,000, then $6,000, then $4,000, the
     * business will be have to cover $10,000 of its losses, but will receive
     * $2,000 from its insurance company for the last loss. Any more losses that
     * occur within the same policy period will be fully covered. So if the
     * business has another loss of $3,000, it will receive the full amount from
     * its insurer. An aggregate deductible makes more sense in commercial
     * insurance, since businesses can have many separate losses during the
     * course of a year, but would be unusual for an individual to experience
     * multiple losses within a year.
     * 
     * 
     * 
     */
    private Boolean perClaimApplicationIndicator;

    /**
     * Float of the coverage deductible. 
     * 
     * 
     * 
     */
    private Float deductiblePercentage;

    /** The deductible calculation base. */
    private String deductibleCalculationBase;
 
    /**
     * A code identifying the type of deductible. 
     * 
     * 
     * 
     */
    @Enumerated
    private DeductibleTypeCodeList deductibleTypeCode;

    /**
     * A code identifying to what the deductible applies.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private DeductibleAppliesToCodeList deductibleAppliesToCode;

    ////////////////Coverage Limit Fields Start/////////////

    /**
     * Maximum amount that may reach the benefit in case it gets paid. Can also
     * be called "high deductible".
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount coverageLimitAmount;

    /**
     * Describes the payment frequency for the coverage benefit.
     * 
     * 
     * 
     */
    @Enumerated
    private PaymentFrequencyCodeList paymentFrequencyCode;


    /**
     * Limit of the benefit expressed in percentage. 
     * 
     * 
     * 
     */
    private Float coverageLimitPercentage;

    /** The coverage limit calculation base. */
    private String coverageLimitCalculationBase;

    /**
     * Maximum amount that may reach the benefit per year in case it gets paid.
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount annualLimitAmount;
  
    /**
     * A code identifying the concept to which the limit applies.
     * 
     * e.g. Per Accident, Per Vehicle
     * 
     * 
     * 
     */
    @Enumerated
    private LimitAppliesToCodeList coverageLimitAppliesToCode;

  ////////////////// CoverageLimit fields ends ////////////////////
    
  ////////////////// CoverageComponent Fields starts /////////////////////////
    
    /**
     * A number which represents the exposure on a coverage based on a defined measurement (e.g. square footage,
     * payroll, sales, units, employee count, etc.). The basis for this number can be found in product component.
     * 
     * e.g: 10 (Full time employees )
     * 
     * e.g: 10, 000 (n $100's of insurance)
     * 
     * e.g: 100,000 (in sales )
     * 
     * e.g: 1500 (square feet)
     * 
     * 
     * 
     */
    @ManyToOne(cascade=CascadeType.ALL)
    private CurrencyAmount exposureAmount;

    /**
     * Time following the eligibility date during which the insured may apply for insurance without evidence of
     * insurability.
     * 
     * e.g: 31 days.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private TimePeriod eligibilityDuration;

    /**
     * The period of time after the start date of the coverage component during which the insured is not covered. Any
     * loss occurring before the end of the waiting period will not be subject to a benefit.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private TimePeriod waitingPeriodDuration;

    /**
     * Period following the termination date of the policy or coverage in which a claim can still be accepted, provided
     * the claimed loss has occurred during the coverage period.
     * 
     * e.g: 2 years
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private TimePeriod extendedReportingDuration;

    /**
     * Start date of policy coverage chosen as a date in the past preceding the policy inception date.
     * 
     * e.g: 1 june 2004 where inception date is 1 march 2005
     * 
     * 
     * 
     * 
     */

    private Date retroactiveStartDate;

    /**
     * Indicates the priority of the coverage in relation to other insurance policies that cover the same insured for
     * the same risks.
     * 
     * e.g: Contribution per limit ratio
     * 
     * e.g: Equal share
     * 
     * e.g: Excess
     * 
     * e.g: Primary
     * 
     * 
     * 
     */
    @Enumerated
    private CoveragePriorityCodeList coveragePriorityCode;

    /**
     * A code representing a coverage.
     * 
     * 
     * 
     */
    @Enumerated
    private CoverageCodeList coverageCode;
    
    /** ****Newly added for EApp - Start *******. */

    /** The sum assured. */
    @ManyToOne (cascade={CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE})
    private CurrencyAmount sumAssured;
    
    /** The sum assured. */
    @ManyToOne (cascade={CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE})
    private CurrencyAmount sumInsured;
    
    /** The policy term. */
    private Integer riderTerm;
    /******Newly added for EApp - End ********/

    /******Newly added for Illustration - Start ********/
    
    /** The required. */
    private Boolean required;
    
    /** The emr required. */
    private Boolean emrRequired;

    /** The emr. */
    private Float emr;
    
    /** The emr duration. */
    private Integer emrDuration;
    
    /** The emr Underwriting code. */
    private String emrUWCode;

    
    /** The flat extra required. */
    private Boolean flatExtraRequired;

    /** The flat extra rate. */
    private Float flatExtraRate;

    /** The flat extra duration. */
    private Integer flatExtraDuration;
    

    /** The flat extra duration. */
    private String flatExtraUWCode;
    
    
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private CurrencyAmount maxRegularTopUp;
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
    private CurrencyAmount minRegularTopUp;
    
    private String minProtection;
    private String maxProtection;
    
    private Integer premiumHolidayStart;
    private String healthUnitsPlanType;
    private String isDisabled;
    /**
	 * A company-specific code indicating the product classification of the
	 * agreement. The source of this code list is the individual insurer.
	 * 
	 * 
	 * 
	 */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
	private ExternalCode productCode;

	/** The product family. */
	private String productFamily;

	/**
	 * Product type is specified.
	 */
	@Enumerated
	private PolicyProductTypeCodeList productType;

	/**
	 * Short name specified.
	 */

	private String shortName;

	/**
	 * MarketingName indicates the version marketing name associated with a
	 * product. MarketingName is the carriers marketing name associated with the
	 * ProductVersionCode.
	 */

	private String marketingName;
	
	/** The minimum sum assured. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
	private CurrencyAmount minsumAssured;

	/** The maximum sum assured. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
	private CurrencyAmount maxsumAssured;
	
	/** The Fund Information. */
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REFRESH, CascadeType.REMOVE })
	private Set<FundInformation> fundInformation;
	
	/** The premium type. */
	private String premiumType;
	
	/*
	 * Created for Generali Gemilang Product
	 */
	/** The premium Package option. */
	private String packageOption;


	/** The premium paying term. */
	private Integer premiumPayingTerm;
	
	/** The premium term. */
	private Integer premiumTerm;

	/** The policy term. */
	private Integer policyTerm;
	

	/** The Cover Multiple. */
	private Integer coverMultiple;

	/** The name of the Dividend Adjustment. */
	private String dividendAdjustment;
	

	/** The committedPremium. */
	private Integer committedPremium;

	/** The premium frequency. */
	@Enumerated
	private PaymentFrequencyCodeList premiumFrequency;

	/** The surrender option. */
	private Boolean surrenderOption;

	/** The bonus option. */
	private String bonusOption;

	/** The premium paying term. */
	private Integer vestingAge;

	/** The premium paying term. */
	private String productGroup;
	/** The div type. */
	private String divType;
	
	/** The year Of Issue . */
	private Integer yearOfIssue;
	
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    private Set<TopUp> includesTopUp;
	
	/*
	 * Risk for Generali
	 */
    private Boolean autoBalancing;

    private String riderCode;

    private String insuredType;
    
    //Generali Vietnam
    
    private String premiumAmount;

    public String getBpmCode() {
        return bpmCode;
    }

    public void setBpmCode(String bpmCode) {
        this.bpmCode = bpmCode;
    }



    private String insured;

    private String uniqueRiderName;
    
    private String planName;
    
    private String bpmCode;

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getRiderCode() {
        return riderCode;
    }

    public void setRiderCode(String riderCode) {
        this.riderCode = riderCode;
    }

    public String getInsuredType() {
        return insuredType;
    }

    public void setInsuredType(String insuredType) {
        this.insuredType = insuredType;
    }

    public String getInsured() {
        return insured;
    }

    public void setInsured(String insured) {
        this.insured = insured;
    }

    public String getUniqueRiderName() {
        return uniqueRiderName;
    }

    public void setUniqueRiderName(String uniqueRiderName) {
        this.uniqueRiderName = uniqueRiderName;
    }

    public Boolean getAutoBalancing() {
        return autoBalancing;
    }

	public void setAutoBalancing(Boolean autoBalancing) {
		this.autoBalancing = autoBalancing;
	}

	public Integer getAutoBalancingValue() {
		return autoBalancingValue;
	}

	public void setAutoBalancingValue(Integer autoBalancingValue) {
		this.autoBalancingValue = autoBalancingValue;
	}

	public Boolean getAutoTrading() {
		return autoTrading;
	}

	public void setAutoTrading(Boolean autoTrading) {
		this.autoTrading = autoTrading;
	}

	public Boolean getTargetReturn() {
		return targetReturn;
	}

	public void setTargetReturn(Boolean targetReturn) {
		this.targetReturn = targetReturn;
	}

	public Integer getTargetReturnValue() {
		return targetReturnValue;
	}

	public void setTargetReturnValue(Integer targetReturnValue) {
		this.targetReturnValue = targetReturnValue;
	}

	public Boolean getProfitClimbing() {
		return profitClimbing;
	}

	public void setProfitClimbing(Boolean profitClimbing) {
		this.profitClimbing = profitClimbing;
	}

	public Integer getProfitClimbingValue() {
		return profitClimbingValue;
	}

	public void setProfitClimbingValue(Integer profitClimbingValue) {
		this.profitClimbingValue = profitClimbingValue;
	}

	public Boolean getCutLoss_TargetLoss() {
		return cutLoss_TargetLoss;
	}

	public void setCutLoss_TargetLoss(Boolean cutLoss_TargetLoss) {
		this.cutLoss_TargetLoss = cutLoss_TargetLoss;
	}

	public Integer getCutLoss_TargetLossValue() {
		return cutLoss_TargetLossValue;
	}

	public void setCutLoss_TargetLossValue(Integer cutLoss_TargetLossValue) {
		this.cutLoss_TargetLossValue = cutLoss_TargetLossValue;
	}

	public Boolean getAutoReentry() {
		return autoReentry;
	}

	public void setAutoReentry(Boolean autoReentry) {
		this.autoReentry = autoReentry;
	}

	public Integer getAutoReentryValue() {
		return autoReentryValue;
	}

	public void setAutoReentryValue(Integer autoReentryValue) {
		this.autoReentryValue = autoReentryValue;
	}

	public Boolean getBounceBack() {
		return bounceBack;
	}

	public void setBounceBack(Boolean bounceBack) {
		this.bounceBack = bounceBack;
	}

	

	private Integer autoBalancingValue;
    
    private Boolean autoTrading;
    
    private Boolean targetReturn;
    private Integer targetReturnValue;
    
    private Boolean profitClimbing;
    private Integer profitClimbingValue;
    
    private Boolean cutLoss_TargetLoss;
    private Integer cutLoss_TargetLossValue;
    
    private Boolean autoReentry;
    private Integer autoReentryValue;
    
    private Boolean bounceBack;
   
    
    private String bounceBackValues;
    
    private Integer maxCoverageAge;
    /**
     * ****Newly added for Illustration - End *******.
     *
     * @return the exposure amount
     */


    public String getPremiumType() {
		return premiumType;
	}

	public void setPremiumType(String premiumType) {
		this.premiumType = premiumType;
	}

	public Integer getPremiumPayingTerm() {
		return premiumPayingTerm;
	}

	public void setPremiumPayingTerm(Integer premiumPayingTerm) {
		this.premiumPayingTerm = premiumPayingTerm;
	}

	/**
     * Gets the exposureAmount.
     * 
     * @return Returns the exposureAmount.
     */
    public CurrencyAmount getExposureAmount() {
        return exposureAmount;
    }

    /**
     * Gets the eligibilityDuration.
     * 
     * @return Returns the eligibilityDuration.
     */
    public TimePeriod getEligibilityDuration() {
        return eligibilityDuration;
    }

    /**
     * Gets the waitingPeriodDuration.
     * 
     * @return Returns the waitingPeriodDuration.
     */
    public TimePeriod getWaitingPeriodDuration() {
        return waitingPeriodDuration;
    }

    /**
     * Gets the extendedReportingDuration.
     * 
     * @return Returns the extendedReportingDuration.
     */
    public TimePeriod getExtendedReportingDuration() {
        return extendedReportingDuration;
    }

    /**
     * Gets the retroactiveStartDate.
     * 
     * @return Returns the retroactiveStartDate.
     */
    public Date getRetroactiveStartDate() {
        return retroactiveStartDate;
    }

    /**
     * Gets the coveragePriorityCode.
     * 
     * @return Returns the coveragePriorityCode.
     */
    public CoveragePriorityCodeList getCoveragePriorityCode() {
        return coveragePriorityCode;
    }

    /**
     * Gets the coverageCode.
     * 
     * @return Returns the coverageCode.
     */
    public CoverageCodeList getCoverageCode() {
        return coverageCode;
    }

    /**
     * Sets The exposureAmount.
     * 
     * @param exposureAmount
     *            The exposureAmount to set.
     */
    public void setExposureAmount(final CurrencyAmount exposureAmount) {
        this.exposureAmount = exposureAmount;
    }

    /**
     * Sets The eligibilityDuration.
     * 
     * @param eligibilityDuration
     *            The eligibilityDuration to set.
     */
    public void setEligibilityDuration(final TimePeriod eligibilityDuration) {
        this.eligibilityDuration = eligibilityDuration;
    }

    /**
     * Sets The waitingPeriodDuration.
     * 
     * @param waitingPeriodDuration
     *            The waitingPeriodDuration to set.
     */
    public void setWaitingPeriodDuration(final TimePeriod waitingPeriodDuration) {
        this.waitingPeriodDuration = waitingPeriodDuration;
    }

    /**
     * Sets The extendedReportingDuration.
     * 
     * @param extendedReportingDuration
     *            The extendedReportingDuration to set.
     */
    public void setExtendedReportingDuration(final TimePeriod extendedReportingDuration) {
        this.extendedReportingDuration = extendedReportingDuration;
    }

    /**
     * Sets The retroactiveStartDate.
     * 
     * @param retroactiveStartDate
     *            The retroactiveStartDate to set.
     */
    public void setRetroactiveStartDate(final Date retroactiveStartDate) {
        this.retroactiveStartDate = retroactiveStartDate;
    }

    /**
     * Sets The coveragePriorityCode.
     * 
     * @param coveragePriorityCode
     *            The coveragePriorityCode to set.
     */
    public void setCoveragePriorityCode(final CoveragePriorityCodeList coveragePriorityCode) {
        this.coveragePriorityCode = coveragePriorityCode;
    }

    /**
     * Sets The coverageCode.
     * 
     * @param coverageCode
     *            The coverageCode to set.
     */
    public void setCoverageCode(final CoverageCodeList coverageCode) {
        this.coverageCode = coverageCode;
    }
    
    
  ////////////////// CoverageComponent Fields ends ////////////////////////////  
    
    
    /**
     * Gets the deductibleAmount.
     * 
     * @return Returns the deductibleAmount.
     */
    public CurrencyAmount getDeductibleAmount() {
        return deductibleAmount;
    }

    /**
     * Gets the perClaimApplicationIndicator.
     * 
     * @return Returns the perClaimApplicationIndicator.
     */
    public Boolean getPerClaimApplicationIndicator() {
        return perClaimApplicationIndicator;
    }

    /**
     * Gets the deductiblePercentage.
     * 
     * @return Returns the deductiblePercentage.
     */
    public Float getDeductiblePercentage() {
        return deductiblePercentage;
    }

    /**
     * Gets the deductibleCalculationBase.
     * 
     * @return Returns the deductibleCalculationBase.
     */
    public String getDeductibleCalculationBase() {
        return deductibleCalculationBase;
    }

    /**
     * Gets the deductibleTypeCode.
     * 
     * @return Returns the deductibleTypeCode.
     */
    public DeductibleTypeCodeList getDeductibleTypeCode() {
        return deductibleTypeCode;
    }

    /**
     * Gets the deductibleAppliesToCode.
     * 
     * @return Returns the deductibleAppliesToCode.
     */
    public DeductibleAppliesToCodeList getDeductibleAppliesToCode() {
        return deductibleAppliesToCode;
    }

    /**
     * Sets The deductibleAmount.
     * 
     * @param deductibleAmount
     *            The deductibleAmount to set.
     */
    public void setDeductibleAmount(final CurrencyAmount deductibleAmount) {
        this.deductibleAmount = deductibleAmount;
    }

    /**
     * Sets The perClaimApplicationIndicator.
     * 
     * @param perClaimApplicationIndicator
     *            The perClaimApplicationIndicator to set.
     */
    public void setPerClaimApplicationIndicator(
            final Boolean perClaimApplicationIndicator) {
        this.perClaimApplicationIndicator = perClaimApplicationIndicator;
    }

    /**
     * Sets The deductiblePercentage.
     * 
     * @param deductiblePercentage
     *            The deductiblePercentage to set.
     */
    public void setDeductiblePercentage(final Float deductiblePercentage) {
        this.deductiblePercentage = deductiblePercentage;
    }

    /**
     * Sets The deductibleCalculationBase.
     * 
     * @param deductibleCalculationBase
     *            The deductibleCalculationBase to set.
     */
    public void setDeductibleCalculationBase(
            final String deductibleCalculationBase) {
        this.deductibleCalculationBase = deductibleCalculationBase;
    }

    /**
     * Sets The deductibleTypeCode.
     * 
     * @param deductibleTypeCode
     *            The deductibleTypeCode to set.
     */
    public void setDeductibleTypeCode(
            final DeductibleTypeCodeList deductibleTypeCode) {
        this.deductibleTypeCode = deductibleTypeCode;
    }

    /**
     * Sets The deductibleAppliesToCode.
     * 
     * @param deductibleAppliesToCode
     *            The deductibleAppliesToCode to set.
     */
    public void setDeductibleAppliesToCode(
            final DeductibleAppliesToCodeList deductibleAppliesToCode) {
        this.deductibleAppliesToCode = deductibleAppliesToCode;
    }

    /**
     * Gets the coverageLimitAmount.
     * 
     * @return Returns the coverageLimitAmount.
     */
    public CurrencyAmount getCoverageLimitAmount() {
        return coverageLimitAmount;
    }

    /**
     * Gets the paymentFrequencyCode.
     * 
     * @return Returns the paymentFrequencyCode.
     */
    public PaymentFrequencyCodeList getPaymentFrequencyCode() {
        return paymentFrequencyCode;
    }


    /**
     * Gets the coverageLimitPercentage.
     * 
     * @return Returns the coverageLimitPercentage.
     */
    public Float getCoverageLimitPercentage() {
        return coverageLimitPercentage;
    }

    /**
     * Gets the coverageLimitCalculationBase.
     * 
     * @return Returns the coverageLimitCalculationBase.
     */
    public String getCoverageLimitCalculationBase() {
        return coverageLimitCalculationBase;
    }

    /**
     * Gets the annualLimitAmount.
     * 
     * @return Returns the annualLimitAmount.
     */
    public CurrencyAmount getAnnualLimitAmount() {
        return annualLimitAmount;
    }

    /**
     * Gets the coverageLimitAppliesToCode.
     * 
     * @return Returns the coverageLimitAppliesToCode.
     */
    public LimitAppliesToCodeList getCoverageLimitAppliesToCode() {
        return coverageLimitAppliesToCode;
    }

    /**
     * Sets The coverageLimitAmount.
     * 
     * @param coverageLimitAmount
     *            The coverageLimitAmount to set.
     */
    public void setCoverageLimitAmount(final CurrencyAmount coverageLimitAmount) {
        this.coverageLimitAmount = coverageLimitAmount;
    }

    /**
     * Sets The paymentFrequencyCode.
     * 
     * @param paymentFrequencyCode
     *            The paymentFrequencyCode to set.
     */
    public void setPaymentFrequencyCode(
            final PaymentFrequencyCodeList paymentFrequencyCode) {
        this.paymentFrequencyCode = paymentFrequencyCode;
    }

    /**
     * Sets The coverageLimitPercentage.
     * 
     * @param coverageLimitPercentage
     *            The coverageLimitPercentage to set.
     */
    public void setCoverageLimitPercentage(
            final Float coverageLimitPercentage) {
        this.coverageLimitPercentage = coverageLimitPercentage;
    }

    /**
     * Sets The coverageLimitCalculationBase.
     * 
     * @param coverageLimitCalculationBase
     *            The coverageLimitCalculationBase to set.
     */
    public void setCoverageLimitCalculationBase(
            final String coverageLimitCalculationBase) {
        this.coverageLimitCalculationBase = coverageLimitCalculationBase;
    }

    /**
     * Sets The annualLimitAmount.
     * 
     * @param annualLimitAmount
     *            The annualLimitAmount to set.
     */
    public void setAnnualLimitAmount(final CurrencyAmount annualLimitAmount) {
        this.annualLimitAmount = annualLimitAmount;
    }

    /**
     * Sets The coverageLimitAppliesToCode.
     * 
     * @param coverageLimitAppliesToCode
     *            The coverageLimitAppliesToCode to set.
     */
    public void setCoverageLimitAppliesToCode(
            final LimitAppliesToCodeList coverageLimitAppliesToCode) {
        this.coverageLimitAppliesToCode = coverageLimitAppliesToCode;
    }

    /**
     * Gets the premium.
     * 
     * @return Returns the premium.
     */
    public Set<Premium> getPremium() {
        return premium;
    }
    

    /**
     * Sets The premium.
     * 
     * @param premium
     *            The premium to set.
     */
    public void setPremium(final Set<Premium> premium) {
        this.premium = premium;
    }

    /**
     * Gets the sumAssured.
     * 
     * @return Returns the sumAssured.
     */
    public final CurrencyAmount getSumAssured() {
        return sumAssured;
    }
    

    /**
     * Sets The sumAssured.
     * 
     * @param sumAssured
     *            The sumAssured to set.
     */
    public final void setSumAssured(final CurrencyAmount sumAssured) {
        this.sumAssured = sumAssured;
    }

    /**
     * @return the required
     */
    public Boolean getRequired() {
        return required;
    }

    /**
     * @param required
     *            the required to set
     */
    public void setRequired(final Boolean required) {
        this.required = required;
    }

    /**
     * @return the emrRequired
     */
    public Boolean getEmrRequired() {
        return emrRequired;
    }

    /**
     * @param emrRequired
     *            the emrRequired to set
     */
    public void setEmrRequired(final Boolean emrRequired) {
        this.emrRequired = emrRequired;
    }

    /**
     * @return the emr
     */
    public Float getEmr() {
        return emr;
    }

    /**
     * @param emr
     *            the emr to set
     */
    public void setEmr(final Float emr) {
        this.emr = emr;
    }

    /**
     * @return the emrDuration
     */
    public Integer getEmrDuration() {
        return emrDuration;
    }

    /**
     * @param emrDuration
     *            the emrDuration to set
     */
    public void setEmrDuration(final Integer emrDuration) {
        this.emrDuration = emrDuration;
    }

    /**
     * @return the flatExtraRequired
     */
    public Boolean getFlatExtraRequired() {
        return flatExtraRequired;
    }

    /**
     * @param flatExtraRequired
     *            the flatExtraRequired to set
     */
    public void setFlatExtraRequired(final Boolean flatExtraRequired) {
        this.flatExtraRequired = flatExtraRequired;
    }

    /**
     * @return the flatExtraRate
     */
    public Float getFlatExtraRate() {
        return flatExtraRate;
    }

    /**
     * @param flatExtraRate
     *            the flatExtraRate to set
     */
    public void setFlatExtraRate(final Float flatExtraRate) {
        this.flatExtraRate = flatExtraRate;
    }

    /**
     * @return the flatExtraDuration
     */
    public Integer getFlatExtraDuration() {
        return flatExtraDuration;
    }

    /**
     * @param flatExtraDuration
     *            the flatExtraDuration to set
     */
    public void setFlatExtraDuration(final Integer flatExtraDuration) {
        this.flatExtraDuration = flatExtraDuration;
    }

    /**
     * Gets the riderTerm.
     * 
     * @return Returns the riderTerm.
     */
    public final Integer getRiderTerm() {
        return riderTerm;
    }

    /**
     * Sets The riderTerm.
     * 
     * @param riderTerm
     *            The riderTerm to set.
     */
    public final void setRiderTerm(final Integer riderTerm) {
        this.riderTerm = riderTerm;
    }

	public ExternalCode getProductCode() {
		return productCode;
	}

	public void setProductCode(ExternalCode productCode) {
		this.productCode = productCode;
	}

	public String getProductFamily() {
		return productFamily;
	}

	public void setProductFamily(String productFamily) {
		this.productFamily = productFamily;
	}

	public PolicyProductTypeCodeList getProductType() {
		return productType;
	}

	public void setProductType(PolicyProductTypeCodeList productType) {
		this.productType = productType;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getMarketingName() {
		return marketingName;
	}

	public void setMarketingName(String marketingName) {
		this.marketingName = marketingName;
	}

	public CurrencyAmount getMinsumAssured() {
		return minsumAssured;
	}

	public void setMinsumAssured(CurrencyAmount minsumAssured) {
		this.minsumAssured = minsumAssured;
	}

	public CurrencyAmount getMaxsumAssured() {
		return maxsumAssured;
	}

	public void setMaxsumAssured(CurrencyAmount maxsumAssured) {
		this.maxsumAssured = maxsumAssured;
	}

	public Set<FundInformation> getFundInformation() {
		return fundInformation;
	}

	public void setFundInformation(Set<FundInformation> fundInformation) {
		this.fundInformation = fundInformation;
	}

	public Integer getPremiumTerm() {
		return premiumTerm;
	}

	public void setPremiumTerm(Integer premiumTerm) {
		this.premiumTerm = premiumTerm;
	}

	public Integer getPolicyTerm() {
		return policyTerm;
	}

	public void setPolicyTerm(Integer policyTerm) {
		this.policyTerm = policyTerm;
	}

	public Integer getCoverMultiple() {
		return coverMultiple;
	}

	public void setCoverMultiple(Integer coverMultiple) {
		this.coverMultiple = coverMultiple;
	}

	public String getDividendAdjustment() {
		return dividendAdjustment;
	}

	public void setDividendAdjustment(String dividendAdjustment) {
		this.dividendAdjustment = dividendAdjustment;
	}

	public Integer getCommittedPremium() {
		return committedPremium;
	}

	public void setCommittedPremium(Integer committedPremium) {
		this.committedPremium = committedPremium;
	}

	public PaymentFrequencyCodeList getPremiumFrequency() {
		return premiumFrequency;
	}

	public void setPremiumFrequency(PaymentFrequencyCodeList premiumFrequency) {
		this.premiumFrequency = premiumFrequency;
	}

	public Boolean getSurrenderOption() {
		return surrenderOption;
	}

	public void setSurrenderOption(Boolean surrenderOption) {
		this.surrenderOption = surrenderOption;
	}

	public String getBonusOption() {
		return bonusOption;
	}

	public void setBonusOption(String bonusOption) {
		this.bonusOption = bonusOption;
	}

	public Integer getVestingAge() {
		return vestingAge;
	}

	public void setVestingAge(Integer vestingAge) {
		this.vestingAge = vestingAge;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	public String getDivType() {
		return divType;
	}

	public void setDivType(String divType) {
		this.divType = divType;
	}

	public Integer getYearOfIssue() {
		return yearOfIssue;
	}

	public void setYearOfIssue(Integer yearOfIssue) {
		this.yearOfIssue = yearOfIssue;
	}    
	public void setIncludesTopUp(Set<TopUp> includesTopUp) {
		this.includesTopUp = includesTopUp;
	}

	public Set<TopUp> getIncludesTopUp() {
		return includesTopUp;
	}

	public void setEmrUWCode(String emrUWCode) {
		this.emrUWCode = emrUWCode;
	}

	public String getEmrUWCode() {
		return emrUWCode;
	}

	public void setFlatExtraUWCode(String flatExtraUWCode) {
		this.flatExtraUWCode = flatExtraUWCode;
	}

	public String getFlatExtraUWCode() {
		return flatExtraUWCode;
	}

	public void setPackageOption(String packageOption) {
		this.packageOption = packageOption;
	}

	public String getPackageOption() {
		return packageOption;
	}

	public void setMaxRegularTopUp(CurrencyAmount maxRegularTopUp) {
		this.maxRegularTopUp = maxRegularTopUp;
	}

	public CurrencyAmount getMaxRegularTopUp() {
		return maxRegularTopUp;
	}

	public void setMinRegularTopUp(CurrencyAmount minRegularTopUp) {
		this.minRegularTopUp = minRegularTopUp;
	}

	public CurrencyAmount getMinRegularTopUp() {
		return minRegularTopUp;
	}

	public void setMinProtection(String minProtection) {
		this.minProtection = minProtection;
	}

	public String getMinProtection() {
		return minProtection;
	}

	

	public void setPremiumHolidayStart(Integer premiumHolidayStart) {
		this.premiumHolidayStart = premiumHolidayStart;
	}

	public Integer getPremiumHolidayStart() {
		return premiumHolidayStart;
	}

	
	

	public void setMaxCoverageAge(Integer maxCoverageAge) {
		this.maxCoverageAge = maxCoverageAge;
	}

	public Integer getMaxCoverageAge() {
		return maxCoverageAge;
	}

	public void setSumInsured(CurrencyAmount sumInsured) {
		this.sumInsured = sumInsured;
	}

	public CurrencyAmount getSumInsured() {
		return sumInsured;
	}

	public void setHealthUnitsPlanType(String healthUnitsPlanType) {
		this.healthUnitsPlanType = healthUnitsPlanType;
	}

	public String getHealthUnitsPlanType() {
		return healthUnitsPlanType;
	}

	public void setMaxProtection(String maxProtection) {
		this.maxProtection = maxProtection;
	}

	public String getMaxProtection() {
		return maxProtection;
	}

	public void setBounceBackValues(String bounceBackValues) {
		this.bounceBackValues = bounceBackValues;
	}

	public String getBounceBackValues() {
		return bounceBackValues;
	}

    public void setIsDisabled(String isDisabled) {
        this.isDisabled = isDisabled;
    }

    public String getIsDisabled() {
        return isDisabled;
    }

	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public String getPremiumAmount() {
		return premiumAmount;
	}
    
}
