package com.cognizant.insurance.agent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Agent details will be stored.
 * 
 * 
 */
@Entity
@Table(name = "GAO_Address")
public class GeneraliAgentAddress {	
		
	@Id
	@Column(name = "Code_GAO")
	private String code;
	
	@Column(name = "NGAO", columnDefinition = "NVARCHAR(MAX)")
	private String nGao;
	
	@Column(name = "GAO_Name", columnDefinition = "NVARCHAR(MAX)")
	private String name;
	
	@Column(name = "Zone")
	private String zone;
	
	@Column(name = "State", columnDefinition = "NVARCHAR(MAX)")
	private String state;
	
	@Column(name = "Address", columnDefinition = "NVARCHAR(MAX)")
	private String address;

	@Column(name = "Effective_Date")
	private String effectiveDate;

	@Column(name = "Owner", columnDefinition = "NVARCHAR(MAX)")
	private String owner;

	@Column(name = "Owner_Mobile")
	private String ownerMobile;
	
	@Column(name = "Email")
	private String email;
	
	@Column(name = "Center", columnDefinition = "NVARCHAR(MAX)")
	private String center;
	
	@Column(name = "Center_Tel")
	private String centerTel;
	
	@Column(name = "Terminated_Date")
	private String terminationDate;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOwnerMobile() {
		return ownerMobile;
	}

	public void setOwnerMobile(String ownerMobile) {
		this.ownerMobile = ownerMobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCenter() {
		return center;
	}

	public void setCenter(String center) {
		this.center = center;
	}

	public String getCenterTel() {
		return centerTel;
	}

	public void setCenterTel(String centerTel) {
		this.centerTel = centerTel;
	}

	public String getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getnGao() {
		return nGao;
	}

	public void setnGao(String nGao) {
		this.nGao = nGao;
	}	
}
