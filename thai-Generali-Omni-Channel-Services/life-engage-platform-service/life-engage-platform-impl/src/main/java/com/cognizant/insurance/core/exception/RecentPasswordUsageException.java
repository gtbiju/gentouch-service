package com.cognizant.insurance.core.exception;

public class RecentPasswordUsageException extends Exception {

	public RecentPasswordUsageException(String message){
		super(message);
	}
}
