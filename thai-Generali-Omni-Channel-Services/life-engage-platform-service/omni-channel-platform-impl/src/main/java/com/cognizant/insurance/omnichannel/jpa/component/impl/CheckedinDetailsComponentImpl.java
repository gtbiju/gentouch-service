/**
 * 
 */
package com.cognizant.insurance.omnichannel.jpa.component.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cognizant.insurance.omnichannel.dao.CheckedinDetailsDoa;
import com.cognizant.insurance.omnichannel.dao.impl.CheckedinDetailsDoaImpl;
import com.cognizant.insurance.omnichannel.domain.CheckinData;
import com.cognizant.insurance.omnichannel.domain.CheckinData.Status;
import com.cognizant.insurance.omnichannel.jpa.component.CheckedinDetailsComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;

/**
 * @author 397850
 *
 */
@Component
public class CheckedinDetailsComponentImpl implements CheckedinDetailsComponent {

	@Override
	public Response<CheckinData> save(Request<CheckinData> request) {
		
		CheckedinDetailsDoa dao=new CheckedinDetailsDoaImpl();
		dao.save(request);
		Response<CheckinData> response = new ResponseImpl<CheckinData>();
		response.setType(request.getType());
		return response;
	}

	@Override
	public CheckinData retriveCheckedinDetails(Request<CheckinData> checkedinRequest,String agentId,
			Status status) {
		CheckinData data= new CheckinData();
		CheckedinDetailsDoa dao=new CheckedinDetailsDoaImpl();
		data=dao.retriveCheckedinDetails(checkedinRequest,agentId,status);
		
		return data;
		
	}

	@Override
	public void update(Request<CheckinData> checkedinRequest,
			CheckinData retirevedData) {
		CheckedinDetailsDoa dao=new CheckedinDetailsDoaImpl();
		dao.update(checkedinRequest,retirevedData);
		
	}

	@Override
	public List<CheckinData> retireveCheckinList(Request<CheckinData> checkedinRequest,
			Status status,Request<List<String>> statusRequest) {
		List<CheckinData> checkinList=new ArrayList<CheckinData>();
		CheckedinDetailsDoa dao=new CheckedinDetailsDoaImpl();
		checkinList=dao.retireveCheckinList(checkedinRequest,status,statusRequest);
		return checkinList;
	}

	@Override
	public void updateCheckinDetails(Request<CheckinData> checkedinRequest, Date date) {
		
		CheckedinDetailsDoa dao=new CheckedinDetailsDoaImpl();
		dao.updateCheckinDetails(checkedinRequest,date);
	}

}
