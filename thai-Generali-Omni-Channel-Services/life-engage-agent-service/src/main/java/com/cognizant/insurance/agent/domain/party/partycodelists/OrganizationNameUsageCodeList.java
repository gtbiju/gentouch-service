/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partycodelists;

/**
 * A code list representing various types of names for parties other than
 * persons.
 * 
 * @author 301350
 * 
 * 
 */
public enum OrganizationNameUsageCodeList {
    /**
     * Also known as "business name".
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    CompanyName,
    /**
     * The stock trading symbol or similar designation/name associated with a
     * stock exchange.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    TradingName,
    /**
     * 
     * 
     * 
     * 
     */
    DoingBusinessAsNameDBA
}
