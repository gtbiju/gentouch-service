ALTER TABLE LE_LOGIN_USER ADD INDEX (USER_ID);

CREATE TABLE `AGENT` (
  `agentCode` varchar(255) NOT NULL,
  `briefWriteUp` varchar(255) DEFAULT NULL,
  `employeeId` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `name_of_excelsheet` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `uploaded_timestamp` datetime DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `emp_type` varchar(50) DEFAULT NULL,
  `supervisor_code` varchar(255) DEFAULT NULL,
  `office` varchar(20) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `agent_group` varchar(20) DEFAULT NULL,
  `yoe` decimal(3,0) DEFAULT NULL,
  `business_sourced` decimal(20,0) DEFAULT NULL,
  `num_of_service` decimal(20,0) DEFAULT NULL,
  `license_num` varchar(30) DEFAULT NULL,
  `license_issue_date` date DEFAULT NULL,
  `license_exp_date` date DEFAULT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `mob_num` decimal(20,0) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `agentCode_2` (`agentCode`),
  KEY `supervisor_code` (`supervisor_code`),
  KEY `agentCode` (`agentCode`),
  KEY `FK_AGENT_CODES` (`userid`),
  CONSTRAINT `FK_AGENT_CODES` FOREIGN KEY (`userid`) REFERENCES `LE_LOGIN_USER` (`USER_ID`),
  CONSTRAINT `FK_AGENT_SUPER_CODES` FOREIGN KEY (`supervisor_code`) REFERENCES `AGENT` (`agentCode`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;


CREATE TABLE `ACHIEVEMENT` (
  `agent_id` int(11) NOT NULL,
  `achievement_code` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `id` int(11) DEFAULT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `agent_id` (`agent_id`),
  CONSTRAINT `FK_ACHIEVEMENT_AGENT` FOREIGN KEY (`agent_id`) REFERENCES `AGENT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;