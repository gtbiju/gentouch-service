/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.agreementcodelists;

/**
 * A code list identifying various types of premium calculations.
 * 
 * @author 301350
 * 
 * 
 */
public enum PremiumCalculationCodeList {
    /**
     * The premium amount including taxes, surcharges and any other fees
     * included in the price of the agreement. 
     * 
     * 
     * 
     */
    GrossPremium,
    /**
     * The premium amount including taxes, surcharges and any other fees
     * excluding the price of the agreement.
     * 
     * 
     * 
     */
    NetPremium
}
