/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of Funds according to their investment scope.
 * 
 * @author 301350
 * 
 * 
 */
public enum InvestmentScopeCodeList {
    /**
     * Identifies a Fund with investment scope 'Bond'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Bond,
    /**
     * Identifies a Fund with investment scope 'Bond + Share + Cash'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    BondAndShareAndCash,
    /**
     * Identifies a Fund with investment scope 'Cash'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Cash,
    /**
     * Identifies a Fund with investment scope 'Cash + Bond'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    CashAndBond,
    /**
     * Identifies a Fund with investment scope 'Mixed'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Mixed,
    /**
     * Identifies a Fund with investment scope 'Mutual fund'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    MutualFund,
    /**
     * Identifies a Fund with investment scope 'Property'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Property,
    /**
     * Identifies a Fund with investment scope 'Share (equity)'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Share
}
