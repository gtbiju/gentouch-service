/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.domain.product;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class RoleSpecType.
 *
 * @author 304007
 */

@Entity
@DiscriminatorValue("Role_Spec")
public class RoleSpecType extends Type {

    /**
     *
     */
    private static final long serialVersionUID = -8902369242942697667L;

    /**
     * The Constructor.
     */
    public RoleSpecType() {
        super();
    }

    /**
     * The Constructor.
     *
     * @param id the id
     */
    public RoleSpecType(final String id) {
        super(id);
    }
}
