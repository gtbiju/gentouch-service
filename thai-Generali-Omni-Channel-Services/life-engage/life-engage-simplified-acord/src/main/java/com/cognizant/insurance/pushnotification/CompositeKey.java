/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Jan 8, 2016
 */
package com.cognizant.insurance.pushnotification;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The Class class CompositeKey.
 */
@Embeddable
public class CompositeKey implements Serializable {

    /** The push msg registered id. */
    @Column(name = "SNS_PUSH_MSG_REGISTERED_ID")
    private String pushMsgRegisteredID;;

    /** The mobile app bundle id. */
    @Column(name = "SNS_MOBILEAPP_BUNDLE_ID")
    private String mobileAppBundleID;

    /** The agent id. */
    @Column(name = "AGENT_ID")
    private String agentID;

    /**
     * Gets the push msg registered id.
     * 
     * @return the push msg registered id
     */
    public String getPushMsgRegisteredID() {
        return pushMsgRegisteredID;
    }

    /**
     * Sets the push msg registered id.
     * 
     * @param pushMsgRegisteredID
     *            the push msg registered id to set.
     */
    public void setPushMsgRegisteredID(String pushMsgRegisteredID) {
        this.pushMsgRegisteredID = pushMsgRegisteredID;
    }

    /**
     * Gets the mobile app bundle id.
     * 
     * @return the mobile app bundle id
     */
    public String getMobileAppBundleID() {
        return mobileAppBundleID;
    }

    /**
     * Sets the mobile app bundle id.
     * 
     * @param mobileAppBundleID
     *            the mobile app bundle id to set.
     */
    public void setMobileAppBundleID(String mobileAppBundleID) {
        this.mobileAppBundleID = mobileAppBundleID;
    }

    /**
     * Gets the agent id.
     * 
     * @return the agent id
     */
    public String getAgentID() {
        return agentID;
    }

    /**
     * Sets the agent id.
     * 
     * @param agentID
     *            the agent id to set.
     */
    public void setAgentID(String agentID) {
        this.agentID = agentID;
    }

}
