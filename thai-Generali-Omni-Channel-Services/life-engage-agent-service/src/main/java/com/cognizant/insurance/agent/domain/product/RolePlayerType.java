/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304007
 * @version       : 0.1, Feb 4, 2013
 */
package com.cognizant.insurance.agent.domain.product;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class class RolePlayerType.
 */

@Entity
@DiscriminatorValue("Role")
public class RolePlayerType extends Type {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6493518462169824980L;
    /**
     * The Constructor.
     */
    public RolePlayerType() {
        super();
    }

    /**
     * The Constructor.
     *
     * @param id the id
     */
    public RolePlayerType(final String id) {
        super(id);
    }
}
