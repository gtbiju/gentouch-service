package com.cognizant.le.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cognizant.le.manager.LeadManager;
import com.cognizant.le.model.dto.PropertyFileSource;

/**
 * Call AgentRetrieveAction
 * 
 * @author 481774
 * 
 */
@WebServlet("/AgentRetrieveAction")
public class AgentRetrieveAction extends HttpServlet {

    private static final long serialVersionUID = 6833439222439684636L;

    private static Logger LOGGER = Logger.getLogger(LoginAction.class);

    private WebApplicationContext springContext;

    @Autowired
    LeadManager leadListingService;

    public AgentRetrieveAction() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        LOGGER.debug("Method Entry : AgentRetrieveAction.doPost");

        String leadListingJSON = "";
        Boolean isValidSession = false;
        //Properties properties = new Properties();
        /*String propertiesFile = getServletContext().getInitParameter("CONFIG_PROPERTIES");
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream stream = classLoader.getResourceAsStream("/" + propertiesFile);
        if (null == stream) {
            stream = classLoader.getResourceAsStream(propertiesFile);
        }
        properties.load(stream);*/
        String agentId = request.getParameter("agentname");
        //String branchId = request.getParameter("branchname");
        String[] agentCodeParts = null;
        if (null != agentId) {
            agentCodeParts = agentId.split("-");
        }

        String agentCode = agentCodeParts[0];
        //String[] branchCodeParts = null;
        /*if (null != branchId) {
            branchCodeParts = branchId.split("-");
        }*/

        //String branchCode = branchCodeParts[0];

        HttpSession session = request.getSession(true);

        /*String defaultLanguage = properties.getProperty("DEFAULT_LANGUAGE");

        if ((null == defaultLanguage) || "".equals(defaultLanguage.trim())) {
            LOGGER.error("Property DEFAULT_LANGUAGE is not defined");
        }
        session.setAttribute("DEFAULT_LANGUAGE", defaultLanguage);*/

        try {
            leadListingJSON = leadListingService.getLeads(agentCode);
            JSONObject jsonObject = new JSONObject(leadListingJSON);
            if (session != null) {

                isValidSession = true;
            }
            jsonObject.getJSONObject("Response").getJSONObject("ResponseInfo").put("isValidSession", isValidSession);
            leadListingJSON = jsonObject.toString();
            request.setAttribute("leadListingJSON", leadListingJSON);
            response.getWriter().print(leadListingJSON);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        LOGGER.debug("Method Exit : LoginAction.doPost");
    }

    /**
     * Servlet init
     * 
     * @param config
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        final AutowireCapableBeanFactory beanFactory = springContext.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
    }

}
