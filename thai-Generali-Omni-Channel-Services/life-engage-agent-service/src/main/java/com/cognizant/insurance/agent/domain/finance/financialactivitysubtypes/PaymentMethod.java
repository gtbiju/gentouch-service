/**
 * 
 */
package com.cognizant.insurance.agent.domain.finance.financialactivitysubtypes;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.FinancialMediumTypeCodeList;

/**
 * As the name implies, the PaymentMethod describes the means of making payments
 * for both money in and out activities.
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PAYMENT_METHOD_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class PaymentMethod extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2927307502101677674L;
    /**
     * Specifies the payment means. Example: check or cash. This can be
     * considered a short cut since the payment means can be deduced by the type
     * of the specialization used for Payment.
     * 
     * 
     * 
     */
    @Enumerated
    private FinancialMediumTypeCodeList paymentFormCode;

    /**
     * @return the paymentFormCode
     * 
     * 
     */
    public FinancialMediumTypeCodeList getPaymentFormCode() {
        return paymentFormCode;
    }

    /**
     * @param paymentFormCode
     *            the paymentFormCode to set
     * 
     * 
     */
    public void setPaymentFormCode(
            final FinancialMediumTypeCodeList paymentFormCode) {
        this.paymentFormCode = paymentFormCode;
    }
}
