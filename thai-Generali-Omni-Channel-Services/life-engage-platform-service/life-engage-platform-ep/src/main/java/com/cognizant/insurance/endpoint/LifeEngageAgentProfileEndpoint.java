package com.cognizant.insurance.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.text.ParseException;
import com.cognizant.insurance.service.helper.LifeEngageValidationHelper;
import org.json.JSONArray;
import org.json.JSONObject;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.AgentProfileService;

/**
 * The Class LifeEngageAgentProfileEndpoint.
 */
@Controller
@RequestMapping("/agentProfileService")
public class LifeEngageAgentProfileEndpoint{
	
	/** The agent profile service. */
	@Autowired
	AgentProfileService agentProfileService;
	
	@Autowired
    private LifeEngageValidationHelper lifeEngageValidationHelper;
	
    /**
     * Retrieve agent.
     *
     * @param json the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveAgentProfile", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String retrieveAgent(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
            	validateAgent(json);
                return agentProfileService.retrieveAgentByCode(json);
            }
        }.execute(json);
    }
    
    /**
	  * This method is used to validate the agent
	 * @param json
	 */
	public void validateAgent(String json) {
	    	JSONObject jsonObject;
	    	try {
	    		jsonObject = new JSONObject(json);
	    		final JSONObject jsonRequestObj = jsonObject.getJSONObject("Request");
	            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject("RequestInfo");
	            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject("RequestPayload");
	            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray("Transactions");
	            if(jsonRqArray.length() > 0){
	            	lifeEngageValidationHelper.validateUser(jsonRqArray.getJSONObject(0));
	            }
	    	} catch (ParseException e) {
	    		e.printStackTrace();
	    	}   
	    }

}
