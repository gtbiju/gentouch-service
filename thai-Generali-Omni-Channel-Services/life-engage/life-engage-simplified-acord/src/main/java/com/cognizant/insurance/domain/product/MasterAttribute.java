/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Sep 20, 2017
 */

package com.cognizant.insurance.domain.product;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cognizant.insurance.domain.product.Carrier;
import com.cognizant.insurance.domain.product.PropertySpecificationType;

// TODO: Auto-generated Javadoc
/**
 * The Class MasterAttribute.
 */
@Entity
@NamedQueries({
	 @NamedQuery(name = "MasterAttribute.retrieveAllMasterAttributes", query = "select mstAttribute from MasterAttribute mstAttribute where mstAttribute.parentComponent IS NULL and (:carrierCode is null or mstAttribute.carrier.id = :carrierCode)")
})

@Table(name = "MST_PROD_PROPERTIES")
public class MasterAttribute {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	/** The name. */
	@Column(name="NAME")
	private String name;
	
    /** The property type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL })
	@JoinColumn(name = "propertyType_ID")
	private PropertySpecificationType propertyType;
	
	/** The default value. */
	@Column(name="DEFAULT_VALUE")
	private String defaultValue;
	
	/** The min value. */
	@Column(name="MIN_VALUE")
	private String minValue;
	
	/** The max value. */
	@Column(name="MAX_VALUE")
	private String maxValue;
	
	/** The optional indicator. */
	@Column(name="OPTIONAL_INDICATOR")
	private boolean optionalIndicator;
	
	/** The calculated indicator. */
	@Column(name="CALCULATED_INDICATOR")
	private boolean calculatedIndicator;
	
	/** The constant indicator. */
	@Column(name="CONSTANT_INDICATOR")
	private boolean constantIndicator;
	
	/** The parent component. */
	@ManyToOne
	@JoinColumn(name="parentComponent_id")
	private MasterComponent parentComponent;
	
	/** The carrier. imp */
    @ManyToOne(cascade = { CascadeType.REFRESH })
    private Carrier carrier;
   
	
    /** The master attribute values. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE },fetch = FetchType.EAGER, mappedBy="property" )
	private List<MasterAttributeValues> masterAttributeValues = new ArrayList<MasterAttributeValues>();

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the property type.
	 *
	 * @return the property type
	 */
	public PropertySpecificationType getPropertyType() {
		return propertyType;
	}

	/**
	 * Sets the property type.
	 *
	 * @param propertyType the new property type
	 */
	public void setPropertyType(PropertySpecificationType propertyType) {
		this.propertyType = propertyType;
	}

	/**
	 * Gets the default value.
	 *
	 * @return the default value
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Sets the default value.
	 *
	 * @param defaultValue the new default value
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * Gets the min value.
	 *
	 * @return the min value
	 */
	public String getMinValue() {
		return minValue;
	}

	/**
	 * Sets the min value.
	 *
	 * @param minValue the new min value
	 */
	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}

	/**
	 * Gets the max value.
	 *
	 * @return the max value
	 */
	public String getMaxValue() {
		return maxValue;
	}

	/**
	 * Sets the max value.
	 *
	 * @param maxValue the new max value
	 */
	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * Checks if is optional indicator.
	 *
	 * @return true, if is optional indicator
	 */
	public boolean isOptionalIndicator() {
		return optionalIndicator;
	}

	/**
	 * Sets the optional indicator.
	 *
	 * @param optionalIndicator the new optional indicator
	 */
	public void setOptionalIndicator(boolean optionalIndicator) {
		this.optionalIndicator = optionalIndicator;
	}

	/**
	 * Checks if is calculated indicator.
	 *
	 * @return true, if is calculated indicator
	 */
	public boolean isCalculatedIndicator() {
		return calculatedIndicator;
	}

	/**
	 * Sets the calculated indicator.
	 *
	 * @param calculatedIndicator the new calculated indicator
	 */
	public void setCalculatedIndicator(boolean calculatedIndicator) {
		this.calculatedIndicator = calculatedIndicator;
	}

	/**
	 * Checks if is constant indicator.
	 *
	 * @return true, if is constant indicator
	 */
	public boolean isConstantIndicator() {
		return constantIndicator;
	}

	/**
	 * Sets the constant indicator.
	 *
	 * @param constantIndicator the new constant indicator
	 */
	public void setConstantIndicator(boolean constantIndicator) {
		this.constantIndicator = constantIndicator;
	}

	/**
	 * Gets the parent component.
	 *
	 * @return the parent component
	 */
	public MasterComponent getParentComponent() {
		return parentComponent;
	}

	/**
	 * Sets the parent component.
	 *
	 * @param parentComponent the new parent component
	 */
	public void setParentComponent(MasterComponent parentComponent) {
		this.parentComponent = parentComponent;
	}

	/**
	 * Gets the master attribute values.
	 *
	 * @return the master attribute values
	 */
	public List<MasterAttributeValues>  getMasterAttributeValues() {
		return masterAttributeValues;
	}

	/**
	 * Sets the master attribute values.
	 *
	 * @param masterAttributeValues the new master attribute values
	 */
	public void setMasterAttributeValues(List<MasterAttributeValues>  masterAttributeValues) {
		this.masterAttributeValues = masterAttributeValues;
	}

	/**
	 * Gets the carrier.
	 *
	 * @return the carrier
	 */
	public Carrier getCarrier() {
		return carrier;
	}

	/**
	 * Sets the carrier.
	 *
	 * @param carrier the carrier to set
	 */
	public void setCarrier(Carrier carrier) {
		this.carrier = carrier;
	}
}
