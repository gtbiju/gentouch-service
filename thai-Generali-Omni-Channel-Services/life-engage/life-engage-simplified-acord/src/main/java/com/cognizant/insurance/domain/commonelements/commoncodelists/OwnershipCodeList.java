/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * It indicates the ownership status of the insured/applicant.
 * 
 * @author 301350
 * 
 * 
 */
public enum OwnershipCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Leased,
    /**
     * 
     * 
     * 
     * 
     */
    Owned,
    /**
     * 
     * 
     * 
     * 
     */
    Rented
}
