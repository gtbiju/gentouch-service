package com.cognizant.le.model.dto;


import java.util.List;


public class AgentProfile {
	
     List<AgentDTO> agents;
	
	
	int totalAgents;
	
	int successAgents;
	
	int failAgents;
	
	int invalidAgent;
	
	int totalAchievments;
	
	int successAchievments;
	
	int failAchievments;
	
	int invalidAchievments;
	
	boolean agentValid;
	
	public boolean isAgentValid() {
		return agentValid;
	}

	public void setAgentValid(boolean agentValid) {
		this.agentValid = agentValid;
	}

	public boolean isAchvValid() {
		return achvValid;
	}

	public void setAchvValid(boolean achvValid) {
		this.achvValid = achvValid;
	}

	boolean achvValid;
	

	public int getTotalAgents() {
		return totalAgents;
	}

	public void setTotalAgents(int totalAgents) {
		this.totalAgents = totalAgents;
	}

	public int getSuccessAgents() {
		return successAgents;
	}

	public void setSuccessAgents(int successAgents) {
		this.successAgents = successAgents;
	}

	public int getFailAgents() {
		return failAgents;
	}

	public void setFailAgents(int failAgents) {
		this.failAgents = failAgents;
	}

	public int getInvalidAgent() {
		return invalidAgent;
	}

	public void setInvalidAgent(int invalidAgent) {
		this.invalidAgent = invalidAgent;
	}

	public int getTotalAchievments() {
		return totalAchievments;
	}

	public void setTotalAchievments(int totalAchievments) {
		this.totalAchievments = totalAchievments;
	}

	public int getSuccessAchievments() {
		return successAchievments;
	}

	public void setSuccessAchievments(int successAchievments) {
		this.successAchievments = successAchievments;
	}

	public int getFailAchievments() {
		return failAchievments;
	}

	public void setFailAchievments(int failAchievments) {
		this.failAchievments = failAchievments;
	}

	public int getInvalidAchievments() {
		return invalidAchievments;
	}

	public void setInvalidAchievments(int invalidAchievments) {
		this.invalidAchievments = invalidAchievments;
	}

	
	public List<AgentDTO> getAgents() {
		return agents;
	}

	public void setAgents(List<AgentDTO> agents) {
		this.agents = agents;
	}
	
	public  String toString()
	{
	    return "Total Agents" + this.totalAgents+",, "+"successAgents:"
	                            +this.successAgents +",, "+"failAgents:"
	                            +this.failAgents+",, " +"invalidAgent:"+this.invalidAgent+",, "+"totalAchievments:"
	                            +this.totalAchievments+",, "+"successAchievments:"
	                            +this.successAchievments+",, "+"failAchievments:"
	                            +this.failAchievments+",, "+"invalidAchievments:"
	                            +this.invalidAchievments+",, "+"agentValid:"
	                            +this.agentValid;
	}

	

}
