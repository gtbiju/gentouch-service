package com.cognizant.insurance.agent.dao.exception;

public class InputValidationException extends Exception {

	 /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8007658416476273000L;

    /** The error code. */
    private String errorCode;

    /**
     * Instantiates a new business exception.
     */
    public InputValidationException() {
        super();
    }

   
    /**
     * Instantiates a new business exception.
     *
     * @param errorCode the error code
     * @param message the message
     */
    public InputValidationException(final String errorCode, final String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * Instantiates a new business exception.
     * 
     * @param message
     *            the message
     */
    public InputValidationException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new business exception.
     * 
     * @param cause
     *            the cause
     */
    public InputValidationException(final Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new business exception.
     * 
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public InputValidationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new business exception.
     * 
     * @param errorCode
     *            the error code
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public InputValidationException(final String errorCode, final String message, final Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    /**
     * Sets The errorCode.
     * 
     * @param errorCode
     *            The errorCode to set.
     */
    public final void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Gets the errorCode.
     * 
     * @return Returns the errorCode.
     */
    public final String getErrorCode() {
        return errorCode;
    }
}
