/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.icr.pdf.repository.impl;

import java.io.File;

import net.sf.jasperreports.engine.JRException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.icr.pdf.repository.DatabaseRepository;

/**
 * The Class class DatabaseRepositoryImpl.
 */
public class DatabaseRepositoryImpl implements DatabaseRepository {

    /** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseRepositoryImpl.class);

    /**
     * Gets the template.
     * 
     * @param templateName
     *            the template name
     * @return the template
     * @throws JRException
     *             the jR exception
     */
    public final File getTemplate(final String templateName) throws JRException {
        LOGGER.debug("Method Entry : DatabaseRepository-> getTemplate");

        LOGGER.debug("Method Exit : DatabaseRepository-> getTemplate");
        return null;
    }

    /**
     * Gets the template name.
     * 
     * @param templateId
     *            the template id
     * @return the template name
     */
    public final String getTemplateName(final String templateId) {
        LOGGER.debug("Method Entry : DatabaseRepository-> getTemplateName");

        LOGGER.debug("Method Exit : DatabaseRepository-> getTemplateName");
        return null;
    }

}
