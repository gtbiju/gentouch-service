package com.cognizant.insurance.agent;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "AGENT_X_ACHIEVEMENT")
public class AgentAchievement {
	
	@Id
	@Column(name = "ID")
	private int id;
	
	@Column(name = "AGENT_CODE", columnDefinition = "VARCHAR(255)")
	private String agentCode;
	
	@Column(name = "POSITION",columnDefinition = "CHAR(2)")
	private String position;
	
	@Column(name = "SUB_POS",columnDefinition = "CHAR(2)")
	private String subPosition;
	
	@Column(name = "ACHIEVE_DATE",columnDefinition = "CHAR(8)")
	private String achieveDate;
	
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "ACHIEVE_NO", referencedColumnName = "ACHIEVE_NO")
	private AgentAchievementDetails agentAchievementDetails;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getSubPosition() {
		return subPosition;
	}

	public void setSubPosition(String subPosition) {
		this.subPosition = subPosition;
	}

	public String getAchieveDate() {
		return achieveDate;
	}

	public void setAchieveDate(String achieveDate) {
		this.achieveDate = achieveDate;
	}

	public AgentAchievementDetails getAgentAchievementDetails() {
		return agentAchievementDetails;
	}

	public void setAgentAchievementDetails(AgentAchievementDetails agentAchievementDetails) {
		this.agentAchievementDetails = agentAchievementDetails;
	}

}
