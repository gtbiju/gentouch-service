/**
 * 
 */
package com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.agreement.FinancialServicesAgreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.BroadLineOfBusinessCodeList;
import com.cognizant.insurance.domain.agreement.agreementcodelists.InsuranceAgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.agreementcodelists.LineOfBusinessCodeList;
import com.cognizant.insurance.domain.commonelements.commoncodelists.PolicyIssueSubTypeCodeList;
import com.cognizant.insurance.domain.commonelements.commoncodelists.PolicyIssueTypeCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.finance.FinancialProvision;

/**
 * An agreement (individual agreement, commercial agreement or group agreement)
 * between an insurer and a policyholder based on a product specification.
 * 
 * The agreement specifies the obligation of the insurer to pay benefits and of
 * the premium payer to pay premiums.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class InsuranceAgreement extends FinancialServicesAgreement {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6086314970478430147L;

	/**
	 * Indicates whether or not the insurance policy is a renewal. If not, it
	 * indicates that the insurance policy is in its first renewal period.
	 * 
	 * 
	 * 
	 */
	private Boolean renewalIndicator;

	/**
	 * The current status of the insurance policy within the life-cycle model.
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	@Enumerated
	private InsuranceAgreementStatusCodeList insuranceAgreementStatusCode;

	/**
	 * A code identifying the broad line of business classification.
	 * 
	 * 
	 * 
	 */
	@Enumerated
	private BroadLineOfBusinessCodeList broadLineOfBusinessCode;

	/**
	 * A code identifying the line of business classification.
	 * 
	 * 
	 * 
	 */
	@Enumerated
	private LineOfBusinessCodeList lineOfBusinessCode;

	

	/**
	 * On Policy, IssueSubType is used with IssueType to provide additional
	 * detail about the type of underwriting that was applied to the policy.
	 */
	@Enumerated
	private PolicyIssueTypeCodeList issueType;

	/**
	 * Issue Sub Type is specified.
	 */
	@Enumerated
	private PolicyIssueSubTypeCodeList issueSubType;

	/**
	 * For a Life Policy, use also the NetSurrValueAmt, CashValueAmt,
	 * DeathBenefitAmt, SurrenderChargeAmt, NetDeathBenefitAmt to fully specify
	 * and clarify the values available under the policy.
	 */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE }, fetch = FetchType.LAZY)
	private CurrencyAmount policyValue;

	/**
	 * Policy Value Last Year.
	 */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE }, fetch = FetchType.LAZY)
	private CurrencyAmount policyValueLastYear;

	/**
	 * Date of the download.
	 */
	@Temporal(TemporalType.DATE)
	private Date downloadDate;

	/**
	 * For policies that had lapsed, but were reinstated, this indicates the
	 * reinstatement date.
	 */
	@Temporal(TemporalType.DATE)
	private Date reinstatementDate;

	/**
	 * For universal policies, this includes the date to which Cost of Insurance
	 * charges are paid for Universal policies.
	 */
	@Temporal(TemporalType.DATE)
	private Date paidToDate;

	/** ****Newly added for EApp/BI- End *******. */

	/** The sum assured. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
	private CurrencyAmount sumAssured;

	

	/** The maturity benefit. */
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.MERGE, CascadeType.REMOVE }, fetch = FetchType.LAZY)
	private CurrencyAmount maturityBenefit;



	/** The name of the Insurere. */
	private String insurerName;

	/** The previous policy indicator. */
	private Boolean previousPolicyIndicator;



	/** The agent id. */
	private String agentId;

	/** The STPStatus. */
	private String stpstatus;


	/** The declaration signed. */
	private Boolean declarationSigned;

	/** The Final Submit. */
	private Boolean isFinalSubmit;

	/** The last sync date. */
	private Date lastSyncDate;

	/**
	 * ****Newly added for EApp/BI - End *******.
	 * 
	 */

	/** ****Newly added for Illustration/BI- Start *******. */


	
	/** PFD template id for the agreement **/
	private String eappTemplateId;
	private String eappInputTemplateId;
	private String illustrationTemplateId;

	@OneToOne(cascade=CascadeType.ALL)
	private FinancialProvision refundDetails;
	/**
	 * ****Newly added for Illustration/BI- End *******.
	 * 
	 */

	public String getIllustrationTemplateId() {
		return illustrationTemplateId;
	}

	public void setIllustrationTemplateId(String illustrationTemplateId) {
		this.illustrationTemplateId = illustrationTemplateId;
	}
	
	public String getEappTemplateId() {
		return eappTemplateId;
	}

	public void setEappTemplateId(String eappTemplateId) {
		this.eappTemplateId = eappTemplateId;
	}

	/**
	 * Gets the broadLineOfBusinessCode.
	 * 
	 * @return Returns the broadLineOfBusinessCode.
	 */
	public BroadLineOfBusinessCodeList getBroadLineOfBusinessCode() {
		return broadLineOfBusinessCode;
	}

	/**
	 * Gets the download date.
	 * 
	 * @return the downloadDate
	 */
	public Date getDownloadDate() {
		return downloadDate;
	}

	/**
	 * Gets the insuranceAgreementStatusCode.
	 * 
	 * @return Returns the insuranceAgreementStatusCode.
	 */
	public InsuranceAgreementStatusCodeList getInsuranceAgreementStatusCode() {
		return insuranceAgreementStatusCode;
	}

	/**
	 * Gets the issue sub type.
	 * 
	 * @return the issueSubType
	 */
	public PolicyIssueSubTypeCodeList getIssueSubType() {
		return issueSubType;
	}

	/**
	 * Gets the issue type.
	 * 
	 * @return the issueType
	 */
	public PolicyIssueTypeCodeList getIssueType() {
		return issueType;
	}

	/**
	 * Gets the lineOfBusinessCode.
	 * 
	 * @return Returns the lineOfBusinessCode.
	 */
	public LineOfBusinessCodeList getLineOfBusinessCode() {
		return lineOfBusinessCode;
	}

	
	/**
	 * Gets the paid to date.
	 * 
	 * @return the paidToDate
	 */
	public Date getPaidToDate() {
		return paidToDate;
	}

	/**
	 * Gets the policy value.
	 * 
	 * @return the policyValue
	 */
	public CurrencyAmount getPolicyValue() {
		return policyValue;
	}

	/**
	 * Gets the policy value last year.
	 * 
	 * @return the policyValueLastYear
	 */
	public CurrencyAmount getPolicyValueLastYear() {
		return policyValueLastYear;
	}

	


	/**
	 * Gets the reinstatement date.
	 * 
	 * @return the reinstatementDate
	 */
	public Date getReinstatementDate() {
		return reinstatementDate;
	}

	/**
	 * Gets the renewalIndicator.
	 * 
	 * @return Returns the renewalIndicator.
	 */

	public Boolean getRenewalIndicator() {
		return renewalIndicator;
	}

	

	/**
	 * Sets The broadLineOfBusinessCode.
	 * 
	 * @param broadLineOfBusinessCode
	 *            The broadLineOfBusinessCode to set.
	 */
	public void setBroadLineOfBusinessCode(
			final BroadLineOfBusinessCodeList broadLineOfBusinessCode) {
		this.broadLineOfBusinessCode = broadLineOfBusinessCode;
	}

	/**
	 * Sets the download date.
	 * 
	 * @param downloadDate
	 *            the downloadDate to set
	 */
	public void setDownloadDate(final Date downloadDate) {
		this.downloadDate = downloadDate;
	}

	/**
	 * Sets The insuranceAgreementStatusCode.
	 * 
	 * @param insuranceAgreementStatusCode
	 *            The insuranceAgreementStatusCode to set.
	 */
	public void setInsuranceAgreementStatusCode(
			final InsuranceAgreementStatusCodeList insuranceAgreementStatusCode) {
		this.insuranceAgreementStatusCode = insuranceAgreementStatusCode;
	}

	/**
	 * Sets the issue sub type.
	 * 
	 * @param issueSubType
	 *            the issueSubType to set
	 */
	public void setIssueSubType(final PolicyIssueSubTypeCodeList issueSubType) {
		this.issueSubType = issueSubType;
	}

	/**
	 * Sets the issue type.
	 * 
	 * @param issueType
	 *            the issueType to set
	 */
	public void setIssueType(final PolicyIssueTypeCodeList issueType) {
		this.issueType = issueType;
	}

	/**
	 * Sets The lineOfBusinessCode.
	 * 
	 * @param lineOfBusinessCode
	 *            The lineOfBusinessCode to set.
	 */
	public void setLineOfBusinessCode(
			final LineOfBusinessCodeList lineOfBusinessCode) {
		this.lineOfBusinessCode = lineOfBusinessCode;
	}

	

	/**
	 * Sets the paid to date.
	 * 
	 * @param paidToDate
	 *            the paidToDate to set
	 */
	public void setPaidToDate(final Date paidToDate) {
		this.paidToDate = paidToDate;
	}

	/**
	 * Sets the policy value.
	 * 
	 * @param policyValue
	 *            the policyValue to set
	 */
	public void setPolicyValue(final CurrencyAmount policyValue) {
		this.policyValue = policyValue;
	}

	/**
	 * Sets the policy value last year.
	 * 
	 * @param policyValueLastYear
	 *            the policyValueLastYear to set
	 */
	public void setPolicyValueLastYear(final CurrencyAmount policyValueLastYear) {
		this.policyValueLastYear = policyValueLastYear;
	}

	

	/**
	 * Sets the reinstatement date.
	 * 
	 * @param reinstatementDate
	 *            the reinstatementDate to set
	 */
	public void setReinstatementDate(final Date reinstatementDate) {
		this.reinstatementDate = reinstatementDate;
	}

	/**
	 * Sets The renewalIndicator.
	 * 
	 * @param renewalIndicator
	 *            The renewalIndicator to set.
	 */
	public void setRenewalIndicator(final Boolean renewalIndicator) {
		this.renewalIndicator = renewalIndicator;
	}

	

	/**
	 * Gets the sumAssured.
	 * 
	 * @return Returns the sumAssured.
	 */
	public CurrencyAmount getSumAssured() {
		return sumAssured;
	}

	/**
	 * Sets The sumAssured.
	 * 
	 * @param sumAssured
	 *            The sumAssured to set.
	 */
	public void setSumAssured(final CurrencyAmount sumAssured) {
		this.sumAssured = sumAssured;
	}

	/**
	 * Gets the maturityBenefit.
	 * 
	 * @return Returns the maturityBenefit.
	 */
	public CurrencyAmount getMaturityBenefit() {
		return maturityBenefit;
	}

	/**
	 * Sets The maturityBenefit.
	 * 
	 * @param maturityBenefit
	 *            The maturityBenefit to set.
	 */
	public void setMaturityBenefit(final CurrencyAmount maturityBenefit) {
		this.maturityBenefit = maturityBenefit;
	}

	

	/**
	 * Gets the insurerName.
	 * 
	 * @return Returns the insurerName.
	 */
	public final String getInsurerName() {
		return insurerName;
	}

	/**
	 * Sets The insurerName.
	 * 
	 * @param insurerName
	 *            The insurerName to set.
	 */
	public final void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}

	/**
	 * Gets the previousPolicyIndicator.
	 * 
	 * @return Returns the previousPolicyIndicator.
	 */
	public final Boolean getPreviousPolicyIndicator() {
		return previousPolicyIndicator;
	}

	/**
	 * Sets The previousPolicyIndicator.
	 * 
	 * @param previousPolicyIndicator
	 *            The previousPolicyIndicator to set.
	 */
	public final void setPreviousPolicyIndicator(Boolean previousPolicyIndicator) {
		this.previousPolicyIndicator = previousPolicyIndicator;
	}
	

	/**
	 * Gets the agent id.
	 * 
	 * @return the agent id
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * Sets the agent id.
	 * 
	 * @param agentId
	 *            the new agent id
	 */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	/**
	 * Gets the declaration signed.
	 * 
	 * @return the declaration signed
	 */
	public Boolean getDeclarationSigned() {
		return declarationSigned;
	}

	/**
	 * Sets the declaration signed.
	 * 
	 * @param declarationSigned
	 *            the declaration signed to set.
	 */
	public void setDeclarationSigned(Boolean declarationSigned) {
		this.declarationSigned = declarationSigned;
	}

	/**
	 * Gets the Final Submit.
	 * 
	 * @return the final submit
	 */
	public Boolean getIsFinalSubmit() {
		return isFinalSubmit;
	}

	/**
	 * Sets the Final Submit.
	 * 
	 * @param isFinalSubmit
	 *            the checks if is final submit to set.
	 */
	public void setIsFinalSubmit(Boolean isFinalSubmit) {
		this.isFinalSubmit = isFinalSubmit;
	}

	/**
	 * Gets the STP Status.
	 * 
	 * @return the STP Status
	 */
	public String getStpstatus() {
		return stpstatus;
	}

	/**
	 * Sets the STP Status.
	 * 
	 * @param stpstatus
	 *            the stpstatus to set.
	 */
	public void setStpstatus(String stpstatus) {
		this.stpstatus = stpstatus;
	}

	/**
	 * Gets the last sync date.
	 * 
	 * @return the last sync date
	 */
	public Date getLastSyncDate() {
		return lastSyncDate;
	}

	/**
	 * Sets the last sync date.
	 * 
	 * @param lastSyncDate
	 *            the last sync date to set.
	 */
	public void setLastSyncDate(Date lastSyncDate) {
		this.lastSyncDate = lastSyncDate;
	}

	

	public String getEappInputTemplateId() {
		return eappInputTemplateId;
	}

	public void setEappInputTemplateId(String eappInputTemplateId) {
		this.eappInputTemplateId = eappInputTemplateId;
	}

	public FinancialProvision getRefundDetails() {
		return refundDetails;
	}

	public void setRefundDetails(FinancialProvision refundDetails) {
		this.refundDetails = refundDetails;
	}

}
