/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 356551
 * @version       : 0.1, Mar 10, 2016
 */
package com.cognizant.insurance.component;

import com.cognizant.insurance.core.exception.BusinessException;

// TODO: Auto-generated Javadoc
/**
 * The Interface interface LifeEngageEncryptionKeyComponent.
 */
public interface LifeEngageEncryptionKeyComponent {

	/**
	 * Fetch key.
	 *
	 * @return the string
	 * @throws BusinessException the business exception
	 */
	String fetchKey()
    throws BusinessException;

   
}
