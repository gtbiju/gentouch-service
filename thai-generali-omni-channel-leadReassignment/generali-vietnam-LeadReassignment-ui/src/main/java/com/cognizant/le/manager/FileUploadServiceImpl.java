package com.cognizant.le.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cognizant.le.model.dao.FileUploadDao;

@Component
public class FileUploadServiceImpl implements FileUploadService{

	@Autowired
	FileUploadDao fileUploadDao;
	
	public void fileUploadService(String fileName, String type ,String user) throws Exception{
		fileUploadDao.uploadFile(fileName, type ,user);
		
		
	}
}
