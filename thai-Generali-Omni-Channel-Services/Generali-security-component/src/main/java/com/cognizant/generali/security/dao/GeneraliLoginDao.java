package com.cognizant.generali.security.dao;

import com.cognizant.le.security.entity.User;
import com.cognizant.le.security.request.Request;

public interface GeneraliLoginDao {
	
	public User getUserDetails(Request<String> userNameRq);
	
	public void updateFailedAttempt(Request<String> userNameRq);	
	 
    public void resetAttempts(Request<String> userNameRq);
}
