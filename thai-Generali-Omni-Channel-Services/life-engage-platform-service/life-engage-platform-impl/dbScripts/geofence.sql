-- Alter Query for Vietnamese support
alter table PartyRoleInRelationship alter column cityValue nvarchar (200);
alter table PartyRoleInRelationship alter column countryValue nvarchar (200);
alter table PartyRoleInRelationship alter column stateValue nvarchar (200);
alter table PartyRoleInRelationship alter column wardValue nvarchar (200);