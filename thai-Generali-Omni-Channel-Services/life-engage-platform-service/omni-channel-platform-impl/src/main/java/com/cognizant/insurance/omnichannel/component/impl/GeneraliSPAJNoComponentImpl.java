package com.cognizant.insurance.omnichannel.component.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tools.ant.BuildException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.omnichannel.component.GeneraliSPAJNoComponent;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliSPAJNoRepository;
import com.cognizant.insurance.omnichannel.vo.SpajNoResponse;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.vo.ResponseInfo;

@Component
public class GeneraliSPAJNoComponentImpl implements GeneraliSPAJNoComponent {

	/** The Constant REQUEST. */
	private static final String REQUEST = "Request";

	/** The Constant REQUEST_INFO. */
	private static final String REQUEST_INFO = "RequestInfo";

	/** The Constant REQUEST_PAYLOAD. */
	private static final String REQUEST_PAYLOAD = "RequestPayload";

	/** The Constant TRANSACTIONS. */
	private static final String TRANSACTIONS = "Transactions";
	
	/** The Constant KEY25. */
    public static final String KEY21 = "Key21";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GeneraliSPAJNoComponentImpl.class);

	@Autowired
	private GeneraliSPAJNoRepository spajRepository;

	@Autowired
	@Qualifier("spajNoMapping")
	private LifeEngageSmooksHolder spajNoMapping;

	@Override
	@Transactional(rollbackFor = { Exception.class })
	public String generateSPAJNo(JSONArray jsonTransactionArray,
			RequestInfo requestInfo) throws BusinessException {
		String response = "";
		JSONObject request = jsonTransactionArray.getJSONObject(0);
		String agentId = request.getString("Key11");
		try {
			List<String> spajlist = spajRepository.generateSpajNos(agentId);
			SpajNoResponse spajNoResponse = new SpajNoResponse();
			spajNoResponse.setAgentId(agentId);
			
			spajNoResponse.setSpajNos(spajlist);
			spajNoResponse.setResponseInfo(buildResponseInfo(requestInfo));
			response = spajNoMapping.parseBO(spajNoResponse);
		} catch (ParseException e) {
			throw new BusinessException(e);
		}
		return response;
	}
	
	

	private ResponseInfo buildResponseInfo(final RequestInfo requestInfo) {
		final ResponseInfo responseInfo = new ResponseInfo();
		responseInfo.setCreationDateTime(requestInfo.getCreationDateTime());
		responseInfo.setRequestorToken(requestInfo.getRequestorToken());
		responseInfo.setSourceInfoName(requestInfo.getSourceInfoName());
		responseInfo.setTransactionId(requestInfo.getTransactionId());
		responseInfo.setUserName(requestInfo.getUserName());
		return responseInfo;
	}

	@Override
	public String isSpajExisting(JSONArray jsonTransactionArray,
			RequestInfo requestInfo) throws BusinessException {
		JSONObject request = jsonTransactionArray.getJSONObject(0);
		String responseData = "";
        try {
            String spajNo = request.getString(KEY21);           
            String eappNo = request.getString(Constants.KEY4);
            SpajNoResponse spajNoResponse = new SpajNoResponse();
            
	        boolean response = spajRepository.isSpajExisting(spajNo, requestInfo, eappNo);            
	
            if (response == true) {
                spajNoResponse.setKey16(Constants.EXISTS);
            } else {
                spajNoResponse.setKey16(Constants.NOT_EXISTS);
            }
            
            spajNoResponse.setResponseInfo(buildResponseInfo(requestInfo));
            responseData = spajNoMapping.parseBO(spajNoResponse);
        } catch (Exception e) {
			LOGGER.error("Key21 is empty " + e);
		}
		return responseData;
	}

}
