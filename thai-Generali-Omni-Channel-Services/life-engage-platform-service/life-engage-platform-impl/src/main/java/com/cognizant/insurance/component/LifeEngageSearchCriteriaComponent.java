/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Jul 31, 2015
 */
package com.cognizant.insurance.component;

import java.text.ParseException;

import org.json.JSONArray;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

// TODO: Auto-generated Javadoc
/**
 * The Interface interface LifeEngageSearchCriteriaComponent.
 */
public interface LifeEngageSearchCriteriaComponent {

    /**
     * Retrieve by filter count.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonRqArray
     *            the json rq array
     * @return the search count result
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveByFilterCount(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException,
            ParseException;

    /**
     * Retrieve by filter.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonRqArray
     *            the json rq array
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveByFilter(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException, ParseException;
    
    /**
     * Retrieve by ids.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonRqArray
     *            the json rq array
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveByIds(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException, ParseException;
}
