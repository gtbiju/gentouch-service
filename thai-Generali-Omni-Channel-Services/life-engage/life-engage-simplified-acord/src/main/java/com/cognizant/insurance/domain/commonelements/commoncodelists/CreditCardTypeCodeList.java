/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Credit Card Type.
 * 
 * @author 301350
 * 
 * 
 */
public enum CreditCardTypeCodeList {
    /**
     * American Express.
     * 
     * 
     * 
     */
    AmericanExpress,
    /**
     * Bank Card.
     * 
     * 
     * 
     */
    BankCard,
    /**
     * Diner's Club.
     * 
     * 
     * 
     */
    DinersClub,
    /**
     * Discover.
     * 
     * 
     * 
     */
    Discover,
    /**
     * Master Card.
     * 
     * 
     * 
     */
    Master,
    /**
     * Other.
     * 
     * 
     * 
     */
    Other,
    /**
     * Unknown.
     * 
     * 
     * 
     */
    Unknown,
    /**
     * Visa.
     * 
     * 
     * 
     */
    Visa,
    /**
     * BCACard
     * 
     */
    BCACard
}
