package com.cognizant.insurance.omnichannel.utils;

/**
 * @author 471501
 * 
 *         The Class RiderTableData
 * 
 *         This class is a POJO for Rider details in illustration PDF..
 * 
 */
public class RiderDetailsTableData {

	private String riderCode;
	private String sumInsured;
	private String standardPremium;
	private String additionalPremium;
	private String premiumPeriod;
	private String totalPremiumOfRider;
	private String totalPremium;
	
	
	public String getRiderCode() {
		return riderCode;
	}
	public void setRiderCode(String riderCode) {
		this.riderCode = riderCode;
	}
	
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getStandardPremium() {
		return standardPremium;
	}
	public void setStandardPremium(String standardPremium) {
		this.standardPremium = standardPremium;
	}
	public String getAdditionalPremium() {
		return additionalPremium;
	}
	public void setAdditionalPremium(String additionalPremium) {
		this.additionalPremium = additionalPremium;
	}
	public String getPremiumPeriod() {
		return premiumPeriod;
	}
	public void setPremiumPeriod(String premiumPeriod) {
		this.premiumPeriod = premiumPeriod;
	}
	public String getTotalPremiumOfRider() {
		return totalPremiumOfRider;
	}
	public void setTotalPremiumOfRider(String totalPremiumOfRider) {
		this.totalPremiumOfRider = totalPremiumOfRider;
	}
	public String getTotalPremium() {
		return totalPremium;
	}
	public void setTotalPremium(String totalPremium) {
		this.totalPremium = totalPremium;
	}
	
	

	

}
