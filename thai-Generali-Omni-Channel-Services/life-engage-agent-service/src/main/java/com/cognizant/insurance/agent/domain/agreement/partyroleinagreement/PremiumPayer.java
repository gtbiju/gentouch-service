/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.partyroleinagreement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;

/**
 * A party who pays the premium due under an agreement.
 * 
 * e.g: John Smith, as premium payer for a term life insurance covering his son.
 * 
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("PREMIUMPAYER")
public class PremiumPayer extends PartyRoleInAgreement {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3670841693453257802L;
    
    
    /****** newly added for EApp - Start ****/
    /**
     * Indicates the relationship of the PremiumPayer with Insured. 
     */    
    private String relationWithInsured;
    
    /**
     * Indicates if the premium payer is different from the Insured. 
     */   
    private boolean payorDifferentFromInsuredIndicator;

    /**
     * Added for generali - Premium Payer.
     * 
     */
    private String partyType;
    
    private String actualRelationship;

    public String getPartyType() {
        return partyType;
    }

    public void setPartyType(String partyType) {
        this.partyType = partyType;
    }

    /**
     * Gets the relationWithInsured.
     *
     * @return Returns the relationWithInsured.
     */
    public final String getRelationWithInsured() {
        return relationWithInsured;
    }

    /**
     * Gets the payorDifferentFromInsuredIndicator.
     *
     * @return Returns the payorDifferentFromInsuredIndicator.
     */
    public final boolean isPayorDifferentFromInsuredIndicator() {
        return payorDifferentFromInsuredIndicator;
    }

    /**
     * Sets The relationWithInsured.
     *
     * @param relationWithInsured The relationWithInsured to set.
     */
    public final void setRelationWithInsured(String relationWithInsured) {
        this.relationWithInsured = relationWithInsured;
    }

    /**
     * Sets The payorDifferentFromInsuredIndicator.
     *
     * @param payorDifferentFromInsuredIndicator The payorDifferentFromInsuredIndicator to set.
     */
    public final void setPayorDifferentFromInsuredIndicator(
            boolean payorDifferentFromInsuredIndicator) {
        this.payorDifferentFromInsuredIndicator =
                payorDifferentFromInsuredIndicator;
    }

	/**
	 * @param actualRelationship the actualRelationship to set
	 */
	public void setActualRelationship(String actualRelationship) {
		this.actualRelationship = actualRelationship;
	}

	/**
	 * @return the actualRelationship
	 */
	public String getActualRelationship() {
		return actualRelationship;
	}

    
    
    /****** newly added for EApp - End ****/
    
    
    
}
