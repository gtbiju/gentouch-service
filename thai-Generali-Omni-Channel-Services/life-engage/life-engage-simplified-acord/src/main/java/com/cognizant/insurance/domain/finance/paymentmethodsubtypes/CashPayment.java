/**
 * 
 */
package com.cognizant.insurance.domain.finance.paymentmethodsubtypes;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.finance.financialactivitysubtypes.PaymentMethod;

/**
 * This concept states a cash payment .
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("CASH")
public class CashPayment extends PaymentMethod {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 105159734484877146L;
    
    private String etrNumber;
    private String etrGeneration;
    private String etrGenerationMode;
    
    public String getEtrGenerationMode() {
		return etrGenerationMode;
	}
	public void setEtrGenerationMode(String etrGenerationMode) {
		this.etrGenerationMode = etrGenerationMode;
	}
	@Temporal(TemporalType.DATE)
    private Date etrDate;
    
	public String getEtrNumber() {
		return etrNumber;
	}
	public void setEtrNumber(String etrNumber) {
		this.etrNumber = etrNumber;
	}
	public Date getEtrDate() {
		return etrDate;
	}
	public void setEtrDate(Date etrDate) {
		this.etrDate = etrDate;
	}
	public String getEtrGeneration() {
		return etrGeneration;
	}
	public void setEtrGeneration(String etrGeneration) {
		this.etrGeneration = etrGeneration;
	}
		
}
