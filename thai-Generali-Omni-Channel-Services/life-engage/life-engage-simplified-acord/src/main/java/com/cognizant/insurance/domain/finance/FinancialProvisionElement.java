/**
 * 
 */
package com.cognizant.insurance.domain.finance;

import java.util.Date;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;

/**
 * The definition of an individual part of a money provision element. The money
 * provision element parts represent the finest level of decomposition of money
 * and are used as input for creating account entries.
 * 
 * e.g: The decomposition of a claim payment into benefits and tax.
 * 
 * e.g: The decomposition of a loan reimbursement into principal and interest.
 * If the monthly repayment is constant, the loan reimbursement is represented
 * as a series of cash flows. For every individual payment, a pair of principal
 * and interest is created.
 * 
 * e.g: The decomposition of a premium. The single premium is expressed as a
 * single cash flow. It is decomposed into pure premium, commission recovery,
 * charge and tax (all these are subtypes of money provision element part.
 * 
 * e.g: The price of a service provided under the terms of a provider agreement
 * expressed as a price.
 * 
 * @author 301350
 * 
 * 
 */
public class FinancialProvisionElement extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2588992455599372083L;

    /**
     * The amount for the money provision element part (used for decomposing a
     * money provision element).
     * 
     * 
     * 
     */
    private CurrencyAmount amount;

    /**
     * The date on which the money provision element part is due. In the case of
     * a single cash flow, this date corresponds to the single cash flow due
     * date. In the case of a series of cash flows, this date is used to express
     * to which payment the money provision element part corresponds.
     * 
     * 
     * 
     */
    private Date dueDate;

    /**
     * Indicates that the amount is an estimate.
     * 
     * e.g: False
     * 
     * e.g: True
     * 
     * 
     * 
     */
    private Boolean estimateIndicator;


    /**
     * A composition mechanism whereby a parent money provision element is
     * decomposed into multiple component money provision element s.
     * 
     * e.g: The decomposition of a premium. The single premium is expressed as a
     * single cash flow. It is decomposed into pure premium, commission
     * recovery, charge and tax (all these are subtypes of money provision
     * element part.
     * 
     * 
     * 
     * 
     * 
     */
    private FinancialProvision isIncludedIn;

    /**
     * Considered fiscal year.
     * 
     * 
     * 
     */
    private Integer fiscalYear;

    /**
     * Gets the amount.
     * 
     * @return Returns the amount.
     */
    public CurrencyAmount getAmount() {
        return amount;
    }

    /**
     * Gets the dueDate.
     * 
     * @return Returns the dueDate.
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Gets the estimateIndicator.
     * 
     * @return Returns the estimateIndicator.
     */
    public Boolean getEstimateIndicator() {
        return estimateIndicator;
    }

    /**
     * Gets the isIncludedIn.
     * 
     * @return Returns the isIncludedIn.
     */
    public FinancialProvision getIsIncludedIn() {
        return isIncludedIn;
    }

    /**
     * Gets the fiscalYear.
     * 
     * @return Returns the fiscalYear.
     */
    public Integer getFiscalYear() {
        return fiscalYear;
    }

    /**
     * Sets The amount.
     * 
     * @param amount
     *            The amount to set.
     */
    public void setAmount(final CurrencyAmount amount) {
        this.amount = amount;
    }

    /**
     * Sets The dueDate.
     * 
     * @param dueDate
     *            The dueDate to set.
     */
    public void setDueDate(final Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Sets The estimateIndicator.
     * 
     * @param estimateIndicator
     *            The estimateIndicator to set.
     */
    public void setEstimateIndicator(final Boolean estimateIndicator) {
        this.estimateIndicator = estimateIndicator;
    }

    /**
     * Sets The isIncludedIn.
     * 
     * @param isIncludedIn
     *            The isIncludedIn to set.
     */
    public void setIsIncludedIn(final FinancialProvision isIncludedIn) {
        this.isIncludedIn = isIncludedIn;
    }

    /**
     * Sets The fiscalYear.
     * 
     * @param fiscalYear
     *            The fiscalYear to set.
     */
    public void setFiscalYear(final Integer fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

}
