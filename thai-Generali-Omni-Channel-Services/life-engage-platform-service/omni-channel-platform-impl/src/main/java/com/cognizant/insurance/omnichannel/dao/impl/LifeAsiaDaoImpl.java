package com.cognizant.insurance.omnichannel.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;

import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.domain.agreement.AddressTranslation;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.dao.LifeAsiaDao;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaMetaData;
import com.cognizant.insurance.omnichannel.domain.LifeAsiaRequest;
import com.cognizant.insurance.omnichannel.domain.UnderWriterMemoDetails;
import com.cognizant.insurance.omnichannel.lifeasia.searchcriteria.LifeAsiaSearchCriteria;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;

public class LifeAsiaDaoImpl extends DaoImpl implements LifeAsiaDao {
	

	
	private static final int NO_RETRY_ATTEMPT = 3;

	@Override
    public Response<LifeAsiaRequest> getLifeAsiaRequestByCriteria(
            Request<LifeAsiaSearchCriteria> criteria) {
        String query;
        Response<LifeAsiaRequest> response = new ResponseImpl<LifeAsiaRequest>();
        LifeAsiaSearchCriteria bpmCriteria = criteria.getType();
        if (bpmCriteria != null) {
            query = "select lifeAsiaRequest from LifeAsiaRequest lifeAsiaRequest";
            Query qry = buildQueryWihCriteria(criteria, criteria.getType(),
                    query);
            Object bpmReqList = null;
            if(CollectionUtils.isNotEmpty(qry.getResultList())){
                bpmReqList =  qry.getResultList().get(0);
            }
            response.setType((LifeAsiaRequest) bpmReqList);
           
        }
        return response;

    }
	
	public Response<LifeAsiaMetaData> getLifeAsiaMetaData(Request<String> request){
		String query;
		Response<LifeAsiaMetaData> response = new ResponseImpl<LifeAsiaMetaData>();
		if (request.getType() != null) {
			query = "select bpmMeta from LifeAsiaMetaData bpmMeta where "
					+ "bpmMeta.typeCode = ?1";
			final List<Object> bpmMetaList = findUsingQuery(request, query,request.getType());
			if (CollectionUtils.isNotEmpty(bpmMetaList)) {

				response.setType((LifeAsiaMetaData) bpmMetaList.get(0));
			}
		}
		return response;
	}

	public Response<LifeAsiaRequest> getLifeAsiaRequest(Request<String> request) {
		String query;
		Response<LifeAsiaRequest> response = new ResponseImpl<LifeAsiaRequest>();
		if (request.getType() != null) {
			query = "select bpmReq from LifeAsiaRequest bpmReq where "
					+ "bpmReq.id = ?1";
			final List<Object> bpmReqList = findUsingQuery(request, query,
					Long.valueOf(request.getType()));
			if (CollectionUtils.isNotEmpty(bpmReqList)) {

				response.setType((LifeAsiaRequest) bpmReqList.get(0));
			}
		}
		return response;

	}

	@Override
	public Response<List<LifeAsiaRequest>> getLifeAsiaRequests(
			Request<LifeAsiaSearchCriteria> criteria) {
		String query;
		Response<List<LifeAsiaRequest>> response = new ResponseImpl<List<LifeAsiaRequest>>();
		LifeAsiaSearchCriteria bpmCriteria = criteria.getType();
		if (bpmCriteria != null) {
			query = "select bpmReq from LifeAsiaRequest bpmReq where bpmReq.spajNo = ?1"
					+ " and bpmReq.status in ?2"
					+ " and bpmReq.creationDate= ?3"
					+ " and bpmreq.documentId = ?4";

			final List<Object> bpmReqList = findUsingQueryByLimit(criteria,
					query, bpmCriteria.getLimit(), bpmCriteria.getSpajNo(),
					bpmCriteria.getStatuses(), bpmCriteria.getFromDate(),
					bpmCriteria.getDocumentId());
			final List<LifeAsiaRequest> bpmRequests = new ArrayList<LifeAsiaRequest>();
			if (CollectionUtils.isNotEmpty(bpmReqList)) {
				for (Object agreement : bpmReqList) {
					bpmRequests.add((LifeAsiaRequest) agreement);
				}
				response.setType(bpmRequests);
			}
		}
		return response;

	}

	@Override
	public Response<List<Number>> getLifeAsiaRequestsIds(
			Request<LifeAsiaSearchCriteria> criteria) {
		String query;
		Response<List<Number>> response = new ResponseImpl<List<Number>>();
		LifeAsiaSearchCriteria lifeAsiaCriteria = criteria.getType();
		if (lifeAsiaCriteria != null) {
			query = "select lifeAsiaRequest.id from LifeAsiaRequest lifeAsiaRequest";

			Query qry = buildQueryWihCriteria(criteria, criteria.getType(),
					query);
			List<Object> lifeAsiaReqList = qry.getResultList();
			final List<Number> lifeAsiaRequests = new ArrayList<Number>();
			if (CollectionUtils.isNotEmpty(lifeAsiaReqList)) {
				for (Object agreement : lifeAsiaReqList) {
					lifeAsiaRequests.add((Number) agreement);
				}
				response.setType(lifeAsiaRequests);
			}
		}
		return response;
	}

	private Query buildQueryWihCriteria(final Request request,
			LifeAsiaSearchCriteria criteria, String baseQuery) {
		String qry = null;System.out.println();
		String query = baseQuery;
		Map<String, Object> params=new HashMap<String, Object>();
		if (criteria.getDocumentId() != null) {
			qry = " where lifeAsiaRequest.documentId=:docId";			
			params.put("docId", criteria.getDocumentId());
		}
		if (criteria.getSpajNo() != null) {
			if (qry == null) {
				qry = " where lifeAsiaRequest.spajNo = :spajNo";
			} else {
				qry = " and lifeAsiaRequest.spajNo = :spajNo";
			}
			
			params.put("spajNo", criteria.getSpajNo());
		}
		if (criteria.geteAppId() != null) {
			if (qry == null) {
				qry = " where lifeAsiaRequest.eAppId = :eAppId";
			} else {
				qry = " and lifeAsiaRequest.eAppId = :eAppId";
			}
			
			params.put("eAppId", criteria.geteAppId());
		}
		if (criteria.getFromDate() != null) {
			if (qry == null) {
				qry = " where lifeAsiaRequest.creationDate = :creationDate";
			} else {
				qry += " and lifeAsiaRequest.creationDate = :creationDate";
			}
			
			params.put("creationDate", criteria.getFromDate());
		}
		if (criteria.getStatuses() != null && criteria.getStatuses().size() > 0) {
			if (qry == null) {
				qry = " where lifeAsiaRequest.status in :statuses";
			} else {
				qry += " and lifeAsiaRequest.status in :statuses";
			}
			params.put("statuses", criteria.getStatuses());
		}

		if (qry != null && !qry.isEmpty()) {
			query = query + qry;
		}

		final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
		final Query queryObj = jpaRequestImpl.getEntityManager().createQuery(
				query);
		
		for (String key : params.keySet()) {
			queryObj.setParameter(key, params.get(key));
		}
		if (criteria.getLimit() >= 0) {
			queryObj.setMaxResults(criteria.getLimit());
		}
		return queryObj;
	}

	protected List<Object> findUsingQuery(final Request request,
			final String queryString, Object... params) {
		try {
			final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
			final Query query = jpaRequestImpl.getEntityManager().createQuery(
					queryString);
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					query.setParameter(i + 1, params[i]);
				}
			}

			return (List<Object>) query.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	protected List<Object> findUsingQueryByLimit(final Request request,
			final String queryString, int maxResult, Object... params) {
		try {
			final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
			final Query query = jpaRequestImpl.getEntityManager().createQuery(
					queryString);
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					query.setParameter(i + 1, params[i]);
				}
			}
			return (List<Object>) query.setMaxResults(maxResult)
					.getResultList();
		} catch (Exception e) {
			throw new DaoException(e);
		}
	}

	@Override
	public Response<String> getEappId(Request<LifeAsiaSearchCriteria> request) {
		String query;
		String eAppId=null;
		Response<String> response = new ResponseImpl<String>();
		LifeAsiaSearchCriteria bpmCriteria = request.getType();
		if (bpmCriteria != null) {
			query = "select lifeAsiaRequest.eAppId from LifeAsiaRequest lifeAsiaRequest ";

			Query qry = buildQueryWihCriteria(request, request.getType(),
					query);
			List<Object> bpmReqList = qry.getResultList();
			if (CollectionUtils.isNotEmpty(bpmReqList)) {
				eAppId=(String) bpmReqList.get(0);
			}
		}
		response.setType(eAppId);
		return response;
	}

    @Override
    public Response<List<String>> getFailedSPAJs(Request<LifeAsiaSearchCriteria> request,int timeRange) {
        String query;
        
        List<String>statusList= new ArrayList<String>();
        statusList.add("ERROR");
        query =
                "select distinct bpmReq.spajNo,key.key11,bpmReq.creationDate  from LifeAsiaRequest bpmReq , TransactionKeys key where " +
                "bpmReq.spajNo=key.key21 and  bpmReq.creationDate >= DATEADD(minute, -"+timeRange+", GETDATE()) and " +
                		"bpmReq.status in ?1 and bpmReq.retryAttempt > "+NO_RETRY_ATTEMPT;        
        List<Object> res = findUsingQuery(request, query,statusList);
        Response<List<String>> response = new ResponseImpl<List<String>>();
        List<String> SPAJs = new ArrayList<String>();
        if (CollectionUtils.isNotEmpty(res)) {

            for (Object SPAJ : res) {
                Object[] values = (Object[]) SPAJ;
                SPAJs.add(values[0]+"##"+values[1]+"##"+values[2]+"##"+"Omni-LifeAsia");
            }

        }
        response.setType(SPAJs);
        return response;
    }
    @Override
    public Response<List<String>> getFailedSPAJsFromOmni(Request<LifeAsiaSearchCriteria> request) {

        String query;
        query =
                "select agr.transactionKeys.key21,agr.transactionKeys.key11,agr.modifiedDateTime from Agreement agr   where "
                        + "   agr.statusCode=15 and agr.contextId=4 and  CONVERT(datetime, agr.transactionKeys.key14) >= DATEADD(day, -1, GETDATE()) and agr.transactionKeys.key21 not in (select  bpq.spajNo from LifeAsiaRequest bpq )";
        List<Object> res = findUsingQuery(request, query);
        Response<List<String>> response = new ResponseImpl<List<String>>();
        List<String> SPAJs = new ArrayList<String>();
        if (CollectionUtils.isNotEmpty(res)) {

            for (Object SPAJ : res) {
                Object[] values = (Object[]) SPAJ;
                SPAJs.add(values[0] + "##" + values[1] + "##" + values[2]+"##"+"Tab-Omni");
            }

        }
        response.setType(SPAJs);
        return response;

    }

	@Override
	public Response<List<String>> getProposalsSubmitted(
			Request<String> agentRequest, Request<List<String>> statusRequest) {
		Response<List<String>> response=new ResponseImpl<List<String>>();
		List<String> spajs=new ArrayList<String>();
		String query="Select lar.spajNo  from LifeAsiaRequest lar ";
		String sub="";
		Map<String, Object> params=new HashMap<String, Object>();
		if(agentRequest.getType()!=null && !agentRequest.getType().isEmpty()){
			sub=sub+" lar.agentId =:agentId";
			params.put("agentId", agentRequest.getType());
		}
		if(statusRequest.getType()!=null && !statusRequest.getType().isEmpty()){
			if(!sub.trim().isEmpty()){
				sub=sub+" and lar.status in :statuses";
			}else{
				sub=sub+" lar.status in :statuses";
			}			
			params.put("statuses", statusRequest.getType());
		}
		if(sub!=null&& !sub.trim().isEmpty()){
			query=query+" where "+sub;
		}
		final Query qry = ((JPARequestImpl<String>)agentRequest).getEntityManager().createQuery(
				query);
		for (String key : params.keySet()) {
			qry.setParameter(key, params.get(key));			
		}
		List<Object> results=qry.getResultList();
		if(CollectionUtils.isNotEmpty(results)){
			for (Object object : results) {
				spajs.add((String) object);
			}			
		}
		response.setType(spajs);
		return response;
	}
	
	@Override
	public Response<List<AppianMemoDetails>> getMemoDetails(Request<Transactions> request,String type) {
		Response<List<AppianMemoDetails>> response = new ResponseImpl<List<AppianMemoDetails>>();
		JPARequestImpl<Transactions> requestImpl = (JPARequestImpl<Transactions>) request;
		String queryString = "";
		List<AppianMemoDetails> resultList = new ArrayList<AppianMemoDetails>();
		List<Object> resultObjList = new ArrayList<Object>();
		if(type.equalsIgnoreCase(GeneraliConstants.UPDATE)) {
			queryString = "select memo from AppianMemoDetails memo where memo.appNumber =:appNumber";
			Query query = requestImpl.getEntityManager().createQuery(queryString);
			query.setParameter("appNumber", request.getType().getKey21());
			resultObjList = query.getResultList();
		}
		else if(type.equalsIgnoreCase(GeneraliConstants.RETRIEVE)){
			queryString = "select memo from AppianMemoDetails memo where memo.appNumber =:appNumber";
			Query query = requestImpl.getEntityManager().createQuery(queryString);
			query.setParameter("appNumber", request.getType().getKey21());
			resultObjList = query.getResultList();
		}
		
		if(CollectionUtils.isNotEmpty(resultObjList)){
            for (Object object : resultObjList) {
                resultList.add((AppianMemoDetails) object);
            }           
        }
		response.setType(resultList);
		return response;
	}
	
	public UnderWriterMemoDetails getMemoDescription(Request<String> request) {
		JPARequestImpl<String> requestImpl = (JPARequestImpl<String>) request;
		UnderWriterMemoDetails umMemo = new UnderWriterMemoDetails();
		String queryString = "select uwmemo from UnderWriterMemoDetails uwmemo where uwmemo.memoCode =:memoCode";
		Query query = requestImpl.getEntityManager().createQuery(queryString);
		query.setParameter("memoCode", request.getType());

		if(CollectionUtils.isNotEmpty(query.getResultList())){
		    umMemo = (UnderWriterMemoDetails) query.getResultList().get(0);
		}		
		return umMemo;
	}

    @Override
    public Response<List<AddressTranslation>> getTranslatedAddress(Request<AddressTranslation> request) {
        JPARequestImpl<AddressTranslation> requestImpl = (JPARequestImpl<AddressTranslation>) request;
        Response<List<AddressTranslation>> response = new ResponseImpl<List<AddressTranslation>>();
        List<AddressTranslation> resultList = new ArrayList<AddressTranslation>();
       
        String queryString = "select address from AddressTranslation address ";
        Query query = requestImpl.getEntityManager().createQuery(queryString);
        resultList = query.getResultList();
        response.setType(resultList);
        return response;
    }

   /* @Override
    public Map<String, UnderWriterMemoDetails> getUnderWriterMemoDetails(Request<Set<String>> request) {
        JPARequestImpl<Set<String>> requestImpl = (JPARequestImpl<Set<String>>) request;
        Map<String, UnderWriterMemoDetails> memoDesc = new HashMap<String, UnderWriterMemoDetails>();
        List<UnderWriterMemoDetails> memoList = new ArrayList<UnderWriterMemoDetails>();
        String queryString = "select uwmemo from UnderWriterMemoDetails uwmemo where uwmemo.memoCode in :memoCodeList";
        Query query = requestImpl.getEntityManager().createQuery(queryString);
        query.setParameter("memoCodeList", request.getType());
        memoList = query.getResultList();
        for(UnderWriterMemoDetails memoObj : memoList){
            memoDesc.put(memoObj.getMemoCode(), memoObj);
        }
        return memoDesc;
    }*/
	
}
