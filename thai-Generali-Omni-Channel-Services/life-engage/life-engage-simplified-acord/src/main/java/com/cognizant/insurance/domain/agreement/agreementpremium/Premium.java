/**
 * 
 */
package com.cognizant.insurance.domain.agreement.agreementpremium;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.cognizant.insurance.domain.agreement.agreementcodelists.PremiumCalculationCodeList;
import com.cognizant.insurance.domain.agreement.agreementcodelists.PremiumNatureCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.finance.FinancialProvision;
import com.cognizant.insurance.domain.finance.Installment;

/**
 * The price for coverage, benefit, and the like described in the agreement or
 * agreement component, and typically associated with a specific period of time.
 * 
 * For example, an insurance policy premium is the price for coverage of a
 * particular risk or set of risks described in the agreement, during a specific
 * period of time.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Premium extends FinancialProvision {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5984126596448657351L;

    /**
     * The amount of premium.
     * 
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne (cascade={CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE})
    private CurrencyAmount amount;

    /**
     * Indicates the nature of the premium. Designed to distinguish subtypes of
     * premiums.
     * 
     * 
     * 
     */
    @Enumerated
    private PremiumNatureCodeList natureCode;

    /**
     * This relationship links a premium with the related installment plan.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE})
    private Set<Installment> resultingInstallment;

    /**
     * A code indicating the type of premium calculation.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private PremiumCalculationCodeList calculationMethodCode;

    /**
     * Gets the amount.
     * 
     * @return Returns the amount.
     */
    public CurrencyAmount getAmount() {
        return amount;
    }

    /**
     * Gets the natureCode.
     * 
     * @return Returns the natureCode.
     */
    public PremiumNatureCodeList getNatureCode() {
        return natureCode;
    }

    /**
     * Gets the resultingInstallment.
     * 
     * @return Returns the resultingInstallment.
     */
    public Set<Installment> getResultingInstallment() {
        return resultingInstallment;
    }

    /**
     * Gets the calculationMethodCode.
     * 
     * @return Returns the calculationMethodCode.
     */
    public PremiumCalculationCodeList getCalculationMethodCode() {
        return calculationMethodCode;
    }

    /**
     * Sets The amount.
     * 
     * @param amount
     *            The amount to set.
     */
    public void setAmount(final CurrencyAmount amount) {
        this.amount = amount;
    }

    /**
     * Sets The natureCode.
     * 
     * @param natureCode
     *            The natureCode to set.
     */
    public void setNatureCode(final PremiumNatureCodeList natureCode) {
        this.natureCode = natureCode;
    }

    /**
     * Sets The resultingInstallment.
     * 
     * @param resultingInstallment
     *            The resultingInstallment to set.
     */
    public void setResultingInstallment(
            final Set<Installment> resultingInstallment) {
        this.resultingInstallment = resultingInstallment;
    }

    /**
     * Sets The calculationMethodCode.
     * 
     * @param calculationMethodCode
     *            The calculationMethodCode to set.
     */
    public void setCalculationMethodCode(
            final PremiumCalculationCodeList calculationMethodCode) {
        this.calculationMethodCode = calculationMethodCode;
    }

}
