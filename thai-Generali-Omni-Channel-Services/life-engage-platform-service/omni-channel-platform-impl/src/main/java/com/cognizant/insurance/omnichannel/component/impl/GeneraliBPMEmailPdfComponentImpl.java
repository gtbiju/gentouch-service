package com.cognizant.insurance.omnichannel.component.impl;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.json.JSONArray;
import com.cognizant.insurance.component.LifeEngageEmailComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.EmailRepository;
import com.cognizant.insurance.component.repository.ProductRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliBPMEmailPdfComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliDocumentComponent;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliDocumentRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliIllustrationRepository;
import com.cognizant.insurance.omnichannel.component.repository.LifeAsiaRepository;
import com.cognizant.insurance.omnichannel.utils.AdditionalQuestionnaireUtil;
import com.cognizant.insurance.omnichannel.utils.FileOperationsUtil;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;
import com.cognizant.insurance.utils.Base64Utils;

@Component
public class GeneraliBPMEmailPdfComponentImpl implements
		GeneraliBPMEmailPdfComponent {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory
			.getLogger(GeneraliBPMEmailPdfComponentImpl.class);

	

	/** The Constant E_APP. */
	private static final String E_APP = "eApp";

	/** The Constant TransactionData. */
	private static final String TRANSACTIONDATA = "TransactionData";

	/** The Constant Email. */
	private static final String EMAIL = "Email";

	/** The Constant EMAIL_ID. */
	private static final String EMAIL_ID = "toMailIds";

	/** The Constant CC_MAIL_ID. */
	private static final String CC_MAIL_ID = "ccMailIds";

	/** The Constant LANGUAGE **/
	private static final String LANGUAGE = "language";

	/** The Constant TYPE. */
	private static final String TYPE = "Type";

	/** The Constant LEAD_NAME **/
	private static final String LEAD_NAME = "leadName";

	/** The Constant SPAJ_NUMBER. */
	private static final String SPAJ_NUMBER = "spajNumber";

	/** The Constant Key4. */
	private static final String KEY_4 = "Key4";

	/** The Constant PRODUCT. */
	private static final String PRODUCT = "Product";

	/** The Constant PRODUCT_DETAILS. */
	private static final String PRODUCT_DETAILS = "ProductDetails";

	/** The Constant PRODUCTCODE */
	private static final String PRODUCT_CODE = "productCode";

	/** The Constant Key24. */
	private static final String KEY24 = "Key24";

	/** The Constant Key21. */
	private static final String KEY21 = "Key21";

	/** The Constant illustration. */
	private static final String ILLUSTRATION = "illustration";

	/** The Constant IN. */
	private static final String IN = "in";
	
	/** The Constant IN. */
	private static final String VT = "vt";

	/** The Constant EN. */
	private static final String EN = "en";

	/** The Constant MAIL_SUBJECT. */
	private static final String MAIL_SUBJECT = "mailSubject";

	/** The Constant TEMPLATE_IN **/
	private static final String TEMPLATE_IN = "template_in";

	/** The Constant TEMPLATE_EN **/
	private static final String TEMPLATE_EN = "template_en";
	
	/** The Constant TEMPLATE_EN **/
	private static final String TEMPLATE_VT = "template_vt";

	/** The Constant TEMPLATE_ILLUSTRATION_COPY to attch with **/
	private static final String TEMPLATE_ILLUSTRATION_COPY = "template_illustration_copy";

	/** The Constant AdditionalQuestionnaire_case. */
	private static final String IS_ADDITIONAL_QUESTIONNAIRE_CASE = "isAdditionalQuestionnaireCase";

	/** The Constant CC_EMAIL_ID. */
	private static final String CC_EMAIL_ID = "ccMailIds";

	/** The Constant EMAIL_FROM_ADDRESS. */
	private static final String EMAIL_FROM_ADDRESS = "fromMailId";

	/** The Constant AGENT ID. */
	private static final String AGENT_ID = "agentId";

	/** The Constant ID. */
	private static final String ID = "id";

	/** The Constant ILLUSTRATION_ID. */
	private static final String ILLUSTRATION_ID = "illustrationId";

	/** The Constant INITIALISED. */
	private static final String INITIALISED = "initialised";

	/** The Constant EMAIL_TEMPLATE. */
	private static final String EMAIL_TEMPLATE = "emailTemplate";

	/** The Constant DAY **/
	private static final String DAY = "day";

	/** The Constant DATE **/
	private static final String DATE = "date";

	/** The Constant TIME **/
	private static final String TIME = "time";

	/** The Constant AGENT_NAME **/
	private static final String AGENT_NAME = "agentName";

	/** The Constant AGENT_EMAIL_ID **/
	private static final String AGENT_EMAIL_ID = "agentEmailId";
	/** The Constant PDF. */
	private static final String PDF = ".pdf";
	
	/** The Constant Key3. */
	private static final String KEY3 = "Key3";
	
	private static final String KEY11 = "Key11";
	
	private static final String ACR = "Acr";


	@Autowired
	private EmailRepository emailRepository;

	@Autowired
	private GeneraliDocumentComponent lifeEngageDocumentComponent;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private AdditionalQuestionnaireUtil additionalQuestionnaireUtil;

	@Autowired
	private LifeEngageEmailComponent lifeEngageEmailComponent;

	@Autowired
	LifeAsiaProcessEngine engine;

	@Autowired
	private GeneraliDocumentRepository documentRepository;
	
    @Autowired
    private GeneraliIllustrationRepository illustrationRepository;
    
	@Autowired
	private LifeAsiaRepository bpmRepository;
	
	@Autowired
	private FileOperationsUtil fileOperationsUtil;
	
	@Value("${generali.platform.service.eAppEmail.subject}")
	private String eAppEmailSubject;
	
	private static final String INSURED = "Insured";
    private static final String PROPOSER = "Proposer";
    private static final String ADD_INSURED = "AdditionalInsured";
    private static final String BASIC_DETAILS = "BasicDetails";
    
    private static final String PROP_SIGNED_DATE = "proposalSignedDate";
    private static final String LIFE_ASSURED_NAMES = "lifeAssuredNames";
    
	@Transactional(rollbackFor = { Exception.class })
	public void triggerBPMRequest(String eAppId) {

		engine.createAndSentLifeAsiaRequest(eAppId);

	}

	@Transactional(rollbackFor = { Exception.class })
	public void saveGeneraliEmail(final JSONObject jsonObj) {

		try {
			LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
			final JSONObject emailObject = new JSONObject();
			final JSONObject emailTemplateObj = new JSONObject();

			String agentId = (String) jsonObj.getString(Constants.KEY11);

			final JSONObject emailObj = jsonObj.getJSONObject(TRANSACTIONDATA)
					.getJSONObject(EMAIL);
			String toMailIds = (String) emailObj.getString(EMAIL_ID);
			String ccMailIds = (String) emailObj.getString(CC_MAIL_ID);

			String fromMailId = "";
			String language = "";
			if (emailObj.has(LANGUAGE)) {
				language = (String) emailObj.getString(LANGUAGE);
			}
			
			String agentName = "";
			String agentEmailId = "";
			String leadName = "";
			String spajNumber = "";
			String type = "";
			String id = "";
			String illustrationId = "";
			String additionalQuestionnaireCase = "false";
			String template_en = "";
			String template_vt= "";
			String template_illustration_copy = "";
			String mailSubject = "";
			type = jsonObj.getString(TYPE);
			//JSONObject mainInsuredObj = null;
			//JSONObject proposerObj  = null;
			//JSONObject agentObj  = null;

			//Response<List<ProductTemplateMapping>> templateResponse = null;
			//Response<List<ProductTemplateMapping>> templateResponseIllusCopy = null;
			//List<ProductTemplateMapping> templateMappings = null;
			
			//String insuredesNames = "";
			//String addInsuredFirstName = "";
			//String mainInsuredFirstName = "";
			String proposerSignDate = "";
			//SimpleDateFormat sdfInput= new SimpleDateFormat("yyyy-MM-dd HH:mm");
			//SimpleDateFormat sdfOutput= new SimpleDateFormat("dd/MM/yyyy HH:mm");
			//StringBuilder insuredesNamesList = new StringBuilder();  
			org.json.JSONArray addInsuredArray = null;
			if (E_APP.equals(jsonObj.getString(TYPE))) {
				if (emailObj.has(LEAD_NAME)) {
					leadName = (String) emailObj.getString(LEAD_NAME);
				}
				if("undefined".equals(leadName)){
					leadName="";
				}
				
				id = (String) jsonObj.getString(KEY_4);
				//illustrationId = (String) (jsonObj.getString(KEY24));
				
				/*if (illustrationId.equals("")) {
					String transTrackingId = "";
					transTrackingId = (String) (jsonObj.getString(KEY3));
					if ((null != transTrackingId && !transTrackingId.equals(""))) {
						illustrationId = illustrationRepository
								.getIllustrationId(transTrackingId,jsonObj.getString(KEY11));
					}

				}*/
				spajNumber = (String) jsonObj.getString(KEY21);

				/*templateResponse = productRepository
						.getProductTemplatesByFilter(buildProductSearchCriteria(
								jsonObj, E_APP));
				templateResponseIllusCopy = productRepository
						.getProductTemplatesByFilter(buildProductSearchCriteria(
								jsonObj, ILLUSTRATION));
				if (additionalQuestionnaireUtil
						.isAdditionalInsuredPresent(jsonObj)) {

					additionalQuestionnaireCase = "true";
				}*/
				
				mailSubject = eAppEmailSubject.replace("[Customer’s Name]", leadName);
				//if (jsonObj.has(INSURED)) {
					/*mainInsuredObj =  jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(INSURED);
					if (mainInsuredObj.has(BASIC_DETAILS)) {
						if(mainInsuredObj.getJSONObject(BASIC_DETAILS).has("fullName")){
							insuredesNames = mainInsuredObj.getJSONObject(BASIC_DETAILS).getString("fullName");
							if(GeneraliConstants.JSON_EMPTY.equals(insuredesNames)){
								if(mainInsuredObj.getJSONObject(BASIC_DETAILS).has("lastName")||mainInsuredObj.getJSONObject(BASIC_DETAILS).has("firstName")){
									String firstName=mainInsuredObj.getJSONObject(BASIC_DETAILS).getString("firstName");
									String lastName=mainInsuredObj.getJSONObject(BASIC_DETAILS).getString("lastName");
									insuredesNames=firstName+" "+lastName;
								}
							}
						}
					}*/
			
			
				//if (jsonObj.has(ADD_INSURED)) {
					
				//if (jsonObj.has(PROPOSER)) {
					/*proposerObj =  jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(PROPOSER);
					if (proposerObj.has("Declaration")) {
						if(proposerObj.getJSONObject("Declaration").has("dateandTime")){
							proposerSignDate = proposerObj.getJSONObject("Declaration").getString("dateandTime");
							if(proposerSignDate!=null && ! GeneraliConstants.JSON_EMPTY.equals(proposerSignDate)){
								try{
								Date formatedDate=sdfInput.parse(proposerSignDate);
								proposerSignDate=sdfOutput.format(formatedDate);
								}
								catch(Exception e){
									LOGGER.error("Error in parsing proposal submission date " +proposerSignDate+" of agreement "+spajNumber );
								}
								
							}
						}
					}*/
				//}
					//if(GeneraliConstants.JSON_EMPTY.equals(leadName)){
						
						/*if (proposerObj.has(BASIC_DETAILS)) {
							if(proposerObj.getJSONObject(BASIC_DETAILS).has("lastName")||proposerObj.getJSONObject(BASIC_DETAILS).has("firstName")){
								String firstName=proposerObj.getJSONObject(BASIC_DETAILS).getString("firstName");
								String lastName=proposerObj.getJSONObject(BASIC_DETAILS).getString("lastName");
									leadName = firstName+ " "+lastName;
							}
						}*/
					//}
					/*agentObj =  jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject("AgentInfo");
					if (agentObj.has("agentName")) {
							agentName = agentObj.getString("agentName");
					}*/
			}

			/*if (null != templateResponse) {
				templateMappings = templateResponse.getType();
			}
			if (templateMappings != null && !templateMappings.isEmpty()) {
				for (ProductTemplateMapping productTemplateMapping : templateMappings) {
					if (EN.equals(productTemplateMapping.getProductTemplates()
							.getLanguage())) {
						template_en = productTemplateMapping
								.getProductTemplates().getTemplateId();
					} else if (VT.equals(productTemplateMapping
							.getProductTemplates().getLanguage())) {
						template_vt = productTemplateMapping
								.getProductTemplates().getTemplateId();
					}
				}

			}

			if (null != templateResponseIllusCopy) {
				templateMappings = templateResponseIllusCopy.getType();
			}
			if (templateMappings != null && !templateMappings.isEmpty()) {
				for (ProductTemplateMapping productTemplateMapping : templateMappings) {
					if (IN.equals(productTemplateMapping.getProductTemplates()
							.getLanguage())) {
						template_illustration_copy = productTemplateMapping
								.getProductTemplates().getTemplateId();
					}
				}

			}*/


			emailObject.put(MAIL_SUBJECT, mailSubject);
			emailObject.put(TEMPLATE_VT, template_vt);
			emailObject.put(TEMPLATE_EN, template_en);
			emailObject.put(TEMPLATE_ILLUSTRATION_COPY,
					template_illustration_copy);
			emailObject.put(IS_ADDITIONAL_QUESTIONNAIRE_CASE,
					additionalQuestionnaireCase);
			emailObject.put(TYPE, type);
			emailObject.put(EMAIL_ID, toMailIds);
			emailObject.put(CC_EMAIL_ID, ccMailIds);
			emailObject.put(EMAIL_FROM_ADDRESS, fromMailId);
			emailObject.put(AGENT_ID, agentId);
			emailObject.put(LANGUAGE, language);

			if (!id.isEmpty()) {
				emailTemplateObj.put(LEAD_NAME, leadName);
				emailTemplateObj.put(AGENT_NAME, agentName);
				emailTemplateObj.put(AGENT_EMAIL_ID, agentEmailId);
				emailTemplateObj.put(SPAJ_NUMBER, spajNumber);
				emailObject.put(ILLUSTRATION_ID, illustrationId);
				emailTemplateObj.put(PROP_SIGNED_DATE, proposerSignDate);
				//emailTemplateObj.put(LIFE_ASSURED_NAMES, insuredesNames);
				emailTemplateObj.put(AGENT_ID, agentId);
				emailTemplateObj.put(DATE, "");
				emailTemplateObj.put(TIME, "");
				emailTemplateObj.put(DAY, "");
				emailObject.put(ID, id);
				emailObject.put(EMAIL_TEMPLATE, emailTemplateObj);
				lifeEngageEmail.setSendTime(LifeEngageComponentHelper
						.getCurrentdate());
				lifeEngageEmail.setEmailValues(emailObject.toString());
				lifeEngageEmail.setAttemptNumber("1");
				lifeEngageEmail.setStatus(INITIALISED);
				lifeEngageEmail.setAgentId(agentId);
				lifeEngageEmail.setType(type);
				lifeEngageEmail = emailRepository
						.savelifeEngageEmail(lifeEngageEmail);

			}
		} catch (ParseException e) {
			LOGGER.error("**Error while Saving Email Data **" + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("**Error while Saving Email Data**" + e.getMessage());
		}

	}

	@Transactional(rollbackFor = { Exception.class })
	public void triggerEmail(String agentId, String type) {
		try {
			// Triggering Generali Email
			
			List<LifeEngageEmail> lifeEngageEmails = emailRepository
					.getInitialisedLifeEngageEmails(agentId, type);
			for (LifeEngageEmail email : lifeEngageEmails) {
				String emailJson = null;
				String statusFlag = null;
				emailJson = email.getEmailValues();
				final JSONObject emailJSONObj = new JSONObject(emailJson);
				final JSONObject templateObj = emailJSONObj
						.getJSONObject(EMAIL_TEMPLATE);
				StatusData statusData = lifeEngageEmailComponent.sendEmail(
						emailJson, "", templateObj.toString(), "");

				statusFlag = statusData.getStatus();
				final LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
				lifeEngageEmail.setSendTime(LifeEngageComponentHelper
						.getCurrentdate());
				lifeEngageEmail.setId(email.getId());
				lifeEngageEmail.setEmailValues(emailJson);
				lifeEngageEmail.setStatus(statusFlag);
				lifeEngageEmail.setAttemptNumber("1");
				lifeEngageEmail.setStatusMessage(statusData.getStatusMessage());
				lifeEngageEmail.setBatchJobId(null);
				lifeEngageEmail.setType(type);
				lifeEngageEmail.setAgentId(agentId);
				if (Constants.SUCCESS.equals(statusFlag)) {
					statusFlag = Constants.SUCCESS;
					emailRepository.updatelifeEngageEmail(lifeEngageEmail);
				} else {
					statusFlag = Constants.FAILURE;
					emailRepository.updatelifeEngageEmail(lifeEngageEmail);
				}
			}

		} catch (Exception e) {
			LOGGER.error("Error while triggering Email " + e.getMessage());
		}
	}

	private ProductSearchCriteria buildProductSearchCriteria(JSONObject object,
			String type) {
		ProductSearchCriteria criteria = new ProductSearchCriteria();
		final JSONObject productObj = object.getJSONObject(TRANSACTIONDATA)
				.getJSONObject(PRODUCT);
		String productCode = productObj.getJSONObject(PRODUCT_DETAILS)
				.getString(PRODUCT_CODE);
		String templateTypeCode = "";
		if (ILLUSTRATION.equals(type)) {
			templateTypeCode = GeneraliConstants.ILLUSTRATION_PDF_TEMPLATE_TYPE;
		} else if (E_APP.equals(type)) {
			templateTypeCode = GeneraliConstants.EAPP_PDF_TEMPLATE_TYPE;
		}else if (ACR.equals(type)) {
			templateTypeCode = GeneraliConstants.ACR_PDF_TEMPLATE_TYPE;
		}
		criteria.setId(Long.parseLong(productCode));
		criteria.setCarrierCode(GeneraliConstants.CARRIER_CODE);
		criteria.setTemplateType(templateTypeCode);
		return criteria;
	}

	@Transactional(rollbackFor = { Exception.class }, readOnly = true)
	public Set<AgreementDocument> generateBPMPdf(JSONObject inputJson) {

		Response<List<ProductTemplateMapping>> templateResponse = null;
		Response<List<ProductTemplateMapping>> templateResponseIllusCopy = null;
		Response<List<ProductTemplateMapping>> templateResponseACR = null;

		List<ProductTemplateMapping> templateMappings = null;
		final Set<AgreementDocument> agreementDocuments = new HashSet<AgreementDocument>();

		try {

			final String id = (String) inputJson.getString(KEY_4);
			final String type = inputJson.getString(TYPE);
			String template_vt_id = "";
			String acrTemplateId = "";
			String template_en = "";
			String template_illustration_copy = "";
			String isAdditionalInsuredPresent = "false";
			//String illustrationId = "";
			String illustrationId = (String) (inputJson.getString(KEY24));
			templateResponse = productRepository
					.getProductTemplatesByFilter(buildProductSearchCriteria(
							inputJson, E_APP));
			templateResponseIllusCopy = productRepository
					.getProductTemplatesByFilter(buildProductSearchCriteria(
							inputJson, ILLUSTRATION));
			
			templateResponseACR = productRepository
			.getProductTemplatesByFilter(buildProductSearchCriteria(
					inputJson, ACR));

			if (null != templateResponse) {
				templateMappings = templateResponse.getType();
			}
			//for eApp pdf
			if (templateMappings != null && !templateMappings.isEmpty()) {
				for (ProductTemplateMapping productTemplateMapping : templateMappings) {
					if (EN.equals(productTemplateMapping.getProductTemplates()
							.getLanguage())) {
						template_en = productTemplateMapping
								.getProductTemplates().getTemplateId();
					} else if (VT.equals(productTemplateMapping
							.getProductTemplates().getLanguage())) {
						template_vt_id = productTemplateMapping
								.getProductTemplates().getTemplateId();
					}
				}

			}

			//for additionalInsured pdf
			if (null != templateResponseACR) {
				templateMappings = templateResponseACR.getType();
			}
			if (templateMappings != null && !templateMappings.isEmpty()) {
				for (ProductTemplateMapping productTemplateMapping : templateMappings) {
					if (VT.equals(productTemplateMapping.getProductTemplates()
							.getLanguage())) {
						acrTemplateId = productTemplateMapping
								.getProductTemplates().getTemplateId();
					}
				}

			}
			
			//for illustration pdf
			if (null != templateResponseIllusCopy) {
				templateMappings = templateResponseIllusCopy.getType();
			}
			if (templateMappings != null && !templateMappings.isEmpty()) {
				for (ProductTemplateMapping productTemplateMapping : templateMappings) {
					if (VT.equals(productTemplateMapping.getProductTemplates()
							.getLanguage())) {
						template_illustration_copy = productTemplateMapping
								.getProductTemplates().getTemplateId();
					}
				}

			}
			if (additionalQuestionnaireUtil
					.isAdditionalInsuredPresent(inputJson)) {
				isAdditionalInsuredPresent = "true";
			}

			ByteArrayOutputStream byteStream = null;

			byte[] data = null;
			String base64SPAJ = null;
			String base64Questionnaire = null;
			String base64Illustration = null;

			if (E_APP.equals(type)) {

				// SPAJ PDF
				byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
						.generatePdfGet(id, type, template_vt_id);
				
				if (null != byteStream) {
					String fileName = "Proposal_"+id+PDF;
					data = byteStream.toByteArray();
					base64SPAJ = Base64Utils.encode(data);
					AgreementDocument spajDocument = new AgreementDocument();
					spajDocument.setBase64string(base64SPAJ);
					spajDocument.setTypeCode(DocumentTypeCodeList.BPMPdf);
					spajDocument.setName(GeneraliConstants.MAIN_INSURED_PDF + PDF);
					spajDocument.setFileNames(fileName);   
					spajDocument.setDocumentTypeCode("Proposal");  
					spajDocument.setContentType(GeneraliConstants.MAIN_INSURED_PDF);       
					agreementDocuments.add(spajDocument);
					try{
						
						fileOperationsUtil.createFileFromByteArray(id,fileName,data);
					}catch(Exception e){
						LOGGER.error("-- Error while writing mail insured pdf "+e.getMessage());
					}
					byteStream = null;
					data = null;
				}
				if ("true".equals(isAdditionalInsuredPresent)) {
					 Set<AgreementDocument> agreementDocumentsPdf = new HashSet<AgreementDocument>();
					// Additional insured PDF

				/*	byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
							.generatePdfGet(id, type, acrTemplateId);*/
					agreementDocumentsPdf = lifeEngageDocumentComponent.getAdditionalInsuredPdf(id,type,GeneraliConstants.ADD_INSURED_PDF_ID);
					if(agreementDocumentsPdf != null && agreementDocumentsPdf.size() >0){
						for(AgreementDocument doc:agreementDocumentsPdf){
							agreementDocuments.add(doc);
						}
					}
				
				}
				if ((null == illustrationId || ("").equals(illustrationId))) {
					String transTrackingId = "";
					transTrackingId = (String) (inputJson.getString(KEY3));
					String agentId= (String) (inputJson.getString(KEY11));
					if ((null != transTrackingId && !transTrackingId.equals(""))) {
						illustrationId = illustrationRepository
								.getIllustrationId(transTrackingId,agentId);

					}
				}
				if ((null != illustrationId && !illustrationId.equals(""))) {
					// Illustration PDF
					byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
							.generateIllustrationPdf(illustrationId,
									ILLUSTRATION, template_illustration_copy);
					if (null != byteStream) {
						String fileName = "Illustration_"+id+PDF;
						data = byteStream.toByteArray();
						base64Illustration = Base64Utils.encode(data);

						AgreementDocument questionnaireDocument = new AgreementDocument();
						questionnaireDocument
								.setBase64string(base64Illustration);
						questionnaireDocument
								.setTypeCode(DocumentTypeCodeList.BPMPdf);
						questionnaireDocument
								.setName(GeneraliConstants.BPM_ILLUSTRATION
										+ PDF);
						questionnaireDocument.setFileNames(fileName);  
						questionnaireDocument.setDocumentTypeCode("Illustration"); 
						questionnaireDocument.setContentType("Illustration");      
						agreementDocuments.add(questionnaireDocument);

						try{
							
							fileOperationsUtil.createFileFromByteArray(id,fileName,data);
						}catch(Exception e){
							LOGGER.error("-- Error while writing mif (!id.isEmpty()) {ail insured pdf ",e);
						}
						
						byteStream = null;
						data = null;
					}
				}
				if ((null != acrTemplateId && !acrTemplateId.equals(""))) {
					// ACR PDF
					byteStream = (ByteArrayOutputStream) lifeEngageDocumentComponent
							.generatePdfGet(id, type, acrTemplateId);
					
					if (null != byteStream) {
						String fileName = "ACR_"+id+PDF;
						data = byteStream.toByteArray();
						base64SPAJ = Base64Utils.encode(data);
						AgreementDocument spajDocument = new AgreementDocument();
						spajDocument.setBase64string(base64SPAJ);
						spajDocument.setTypeCode(DocumentTypeCodeList.BPMPdf);
						spajDocument.setName(GeneraliConstants.ACR_PDF + PDF);
						spajDocument.setFileNames(fileName);
						spajDocument.setDocumentTypeCode("ACR");
						spajDocument.setContentType("ACR");
						agreementDocuments.add(spajDocument);
						try{
							
							fileOperationsUtil.createFileFromByteArray(id,fileName,data);
						}catch(Exception e){
							LOGGER.error("-- Error while writing mail insured pdf "+e.getMessage());
						}
						byteStream = null;
						data = null;
					}
				}
			}

		} catch (Exception e) {
			LOGGER.error(" ERROR IN generateBPMPdf   !!!" + e.getMessage());

		}
		return agreementDocuments;
	}

	@Transactional(rollbackFor = { Exception.class })
	public void saveBPMPdf(Set<AgreementDocument> agreementDocuments,
			String eAppId) {
		if (agreementDocuments!=null&& agreementDocuments.size() > 0) {
			documentRepository.saveBPMBase64Pdf(agreementDocuments, eAppId);
		}
	}
	@Transactional(rollbackFor = { Exception.class })
	public void updateEappBPMStatus(String eAppId,String status,String checkDevice){
		bpmRepository.updateAgreementWithStauts(status, eAppId, "","",checkDevice);
	}
}
