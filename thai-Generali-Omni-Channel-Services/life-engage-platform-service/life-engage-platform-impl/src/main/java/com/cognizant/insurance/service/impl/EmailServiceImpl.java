/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.LifeEngageEmailComponent;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.services.EmailService;

/**
 * The Class class LookUpServiceImpl.
 * 
 * @author 300797
 */
@Service("emailService")
public class EmailServiceImpl implements EmailService {
	
	@Autowired
	private LifeEngageEmailComponent lifeEngageEmailComponent;
	
	/** The Constant EMAIL_TEMPLATE. */
    private static final String EMAIL_TEMPLATE = "emailTemplate";
    
    /** The Constant PDF_NAME. */
    private static final String PDF_NAME = "pdfName";
    
    /** The Constant EMAIL_TEMPLATE. */
    private static final String TEMPLATE_NAME = "templateName";
    
    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";
    
    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";
    
    /** The Constant KEY18. */
    private static final String KEY11 = "Key11";
    
    /** The Constant TYPE. */
    private static final String TYPE = "Type";
    
    /** The Constant EMAIL_BODY. */
    private static final String EMAIL_BODY = "emailBody";
    
    /** The Constant EMAIL_ATTACHMENTS. */
    private static final String EMAIL_ATTACHMENTS = "emailAttachments";
    
    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";
    
    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);
	
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#save(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String sendEmail(final String json) throws BusinessException {
    	JSONObject emailObj;
    	String statusFlag="";
		try {
			emailObj = new JSONObject(json);
	    	String templateName = emailObj.getString(TEMPLATE_NAME);
	    	String pdfName = emailObj.getString(PDF_NAME);
	    	final JSONObject templateObj = emailObj.getJSONObject(EMAIL_TEMPLATE);
	    	StatusData statusData = lifeEngageEmailComponent.sendEmail(json,templateName,templateObj.toString(),pdfName);
	    	statusFlag = statusData.getStatus();
	    	
		} catch (ParseException e) {
		    LOGGER.trace("Parse exceptionwhile sending email" + e);
		}
        return statusFlag;
    }
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final void triggerEmail(final String json) throws BusinessException {
    	JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(json);
		
	    	final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
	        final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
	        final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
	        if (jsonRqArray.length() > 0) {
	            for (int i = 0; i < jsonRqArray.length(); i++) {
	                final JSONObject jsonObj = jsonRqArray.getJSONObject(i);
	                String agentId = (String)jsonObj.getString(KEY11);
                	                
	                String type = (String) jsonObj.getString(TYPE);
                
	                lifeEngageEmailComponent.triggerEmail(agentId, type);
	            }
	        }
	        
		} catch (ParseException e) {
		    LOGGER.trace("Parse exceptionwhile trigger email" + e);
		}
    	
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.EmailService#updateEmailDetails(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String updateEmailDetails(String json) throws BusinessException {
        LOGGER.trace("Inside EmailServiceImpl.updateEmailDetails :json " + json);
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        JSONArray jsonRsArray = new JSONArray();
        String transactionArray = "{\"Transactions\":[]}";
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);

            transactionArray = lifeEngageEmailComponent.updateEmailDetails(requestInfo, jsonRqArray);
            jsonObject = new JSONObject(transactionArray);
            jsonRsArray = jsonObject.getJSONArray(TRANSACTIONS);

            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("EmailServiceImpl.updateEmailDetails: Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        LOGGER.trace("Inside EmailServiceImpl updateEmailDetails : jsonObjectRs" + jsonObjectRs.toString());
        return jsonObjectRs.toString();

    }
    
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.EmailService#sendEmailWithAttachment(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String sendEmailWithAttachment(final String json) throws BusinessException {
    	
    	LOGGER.trace("Inside EmailServiceImpl.sendEmailWithAttachment :json " + json);
    	
    	JSONObject emailObj;
    	JSONObject jsonObjectRs = new JSONObject();
    	String statusFlag="";
    	
		try {
			emailObj = new JSONObject(json);
	    	String emailBody = emailObj.getString(EMAIL_BODY);
	    	JSONArray jsonEmailAttachmentsArr = emailObj.getJSONArray(EMAIL_ATTACHMENTS); 
	    	
	    	StatusData statusData = lifeEngageEmailComponent.sendEmailWithAttachment(json, emailBody, jsonEmailAttachmentsArr);
	    	statusFlag = jsonObjectRs.put(RESPONSE, statusData.getStatus()).toString();
	    	
		} catch (ParseException e) {
			 LOGGER.trace("EmailServiceImpl.sendEmailWithAttachment: Parse Exception" + e.toString());
	         throwBusinessException(true, e.getMessage());
		}
		 LOGGER.trace("Inside EmailServiceImpl sendEmailWithAttachment : statusFlag" + statusFlag);
    	return statusFlag;
    }
    
}
