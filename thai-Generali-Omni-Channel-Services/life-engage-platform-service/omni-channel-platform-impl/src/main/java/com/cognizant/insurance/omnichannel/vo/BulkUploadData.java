package com.cognizant.insurance.omnichannel.vo;

import java.util.Date;

/**
 * BulkUploadData
 * @author 390229
 *
 */
public class BulkUploadData {
	
	/** Customer First name */
	private String customerFirstName;
	
	/** Customer Last Name **/
	private String customerLastName;
	
	/** Customer Gender **/
	private String gender;
	
	/** Customer Mobile Number **/
	private String mobileNumber;
	
	/** Customer National Id **/
	private String nationalID;
	
	/** Customer Email Address **/
	private String emailAddress;
	
	/** Customer Address **/
	private String address;
	
	/** Customer State **/
	private String state;
	
	/** Customer Nationality **/
	private String nationality;
	
	/** Customer Date Of Birth **/
	private Date dob;
	
	/** Customer Potential **/
	private String potential;
	
	/** Customer Source **/
	private String source;
	
	/** Customer occupation **/
	private String occupation;
	
	/** Customer Action **/
	private String action;
	
	/** Customer Next meeting date **/
	private Date nextmeetingDate;
	
	/** Customer Agent Code **/
	private String agentCode;
	
	/** Customer Next Meeting Time **/
	private String nextMeetingTime;
	
	/** Validation Error messages **/
	private String remarks;
	
	/** Customer Status **/
	private Boolean status;
	
	/**Customer Meeting date and time **/
	private String meetingDateAndTime;

	/** Customer City **/
	private String city;
	
	/** Customer annual Income **/
	private String annualIncome;
	
	/** Customer Need **/
	private String need;
	
	/** Customer Branch **/
	private String branch;
	
	/** Customer Bank **/
	private String bank;
	
	/** Refferer Name **/
	private String refName;
	
	/** Referer Mobile Number **/
	private String refMobileNumber;
	
	private Date currentDate;
	private String transTrackingId;
	private String dobString;
	private String nextmeetingDateString;
	private String agentName;
	
	//value to save in drop down
	private String stateValue;
	private String occupationValue;
	private String nationalityValue;
	private String incomeValue;
	private String needValue;
	private String agentModified;
	


	
	//To Highlight invalid cells in Excel
	private boolean isCustomerFirstNameInValid;
	private boolean isCustomerLastNameInValid;
	private boolean isGenderInValid;
	private boolean isMobileNumberInValid;
	private boolean isNationalIDInValid;
	private boolean isEmailAddresInValid;
	private boolean isAddressInValid;
	private boolean isStateInValid;
	private boolean isNationalityInValid;
	private boolean isDOBInValid;
	private boolean isPotentialInValid;
	private boolean isSourceInValid;
	private boolean isOccupationInValid;
	private boolean isActionInValid;
	private boolean isNextMeetinDateInValid;
	private boolean isNextMeetingTimeInValid;
	private boolean isAgentCodeInValid;
	private boolean isCityInValid;
	private boolean isAnnualIncomeInValid;
	private boolean isNeedInValid;
	private boolean isBranchInValid;
	private boolean isBankInValid;
	private boolean isRefNameInValid;
	private boolean isRefMobileNumberInValid;
	
	private String user;
	private String stateCode;
	private String bankCode;
	private String branchCode;
	private String stateName;
	private String bankName;


	private String allocatedBranchCode;
	private String allocatedBranchName;
	
	public String channelType;
	
	


	public boolean isCustomerNameInValid() {
		return isCustomerFirstNameInValid;
	}

	public void setCustomerNameInValid(boolean isCustomerNameInValid) {
		this.isCustomerFirstNameInValid = isCustomerNameInValid;
	}

	public boolean isGenderInValid() {
		return isGenderInValid;
	}

	public void setGenderInValid(boolean isGenderInValid) {
		this.isGenderInValid = isGenderInValid;
	}

	public boolean isMobileNumberInValid() {
		return isMobileNumberInValid;
	}

	public void setMobileNumberInValid(boolean isMobileNumberInValid) {
		this.isMobileNumberInValid = isMobileNumberInValid;
	}

	public boolean isNationalIDInValid() {
		return isNationalIDInValid;
	}

	public void setNationalIDInValid(boolean isNationalIDInValid) {
		this.isNationalIDInValid = isNationalIDInValid;
	}

	public boolean isEmailAddresInValid() {
		return isEmailAddresInValid;
	}

	public void setEmailAddresInValid(boolean isEmailAddresInValid) {
		this.isEmailAddresInValid = isEmailAddresInValid;
	}

	public boolean isAddressInValid() {
		return isAddressInValid;
	}

	public void setAddressInValid(boolean isAddressInValid) {
		this.isAddressInValid = isAddressInValid;
	}

	public boolean isStateInValid() {
		return isStateInValid;
	}

	public void setStateInValid(boolean isStateInValid) {
		this.isStateInValid = isStateInValid;
	}

	public boolean isNationalityInValid() {
		return isNationalityInValid;
	}

	public void setNationalityInValid(boolean isNationalityInValid) {
		this.isNationalityInValid = isNationalityInValid;
	}

	public boolean isDOBInValid() {
		return isDOBInValid;
	}

	public void setDOBInValid(boolean isDOBInValid) {
		this.isDOBInValid = isDOBInValid;
	}

	public boolean isPotentialInValid() {
		return isPotentialInValid;
	}

	public void setPotentialInValid(boolean isPotentialInValid) {
		this.isPotentialInValid = isPotentialInValid;
	}

	public boolean isSourceInValid() {
		return isSourceInValid;
	}

	public void setSourceInValid(boolean isSourceInValid) {
		this.isSourceInValid = isSourceInValid;
	}

	public boolean isOccupationInValid() {
		return isOccupationInValid;
	}

	public void setOccupationInValid(boolean isOccupationInValid) {
		this.isOccupationInValid = isOccupationInValid;
	}

	public boolean isActionInValid() {
		return isActionInValid;
	}

	public void setActionInValid(boolean isActionInValid) {
		this.isActionInValid = isActionInValid;
	}

	public boolean isNextMeetinDateInValid() {
		return isNextMeetinDateInValid;
	}

	public void setNextMeetinDateInValid(boolean isNextMeetinDateInValid) {
		this.isNextMeetinDateInValid = isNextMeetinDateInValid;
	}

	public boolean isNextMeetingTimeInValid() {
		return isNextMeetingTimeInValid;
	}

	public void setNextMeetingTimeInValid(boolean isNextMeetingTimeInValid) {
		this.isNextMeetingTimeInValid = isNextMeetingTimeInValid;
	}

	public boolean getIsAgentCodeInValid() {
		return isAgentCodeInValid;
	}

	public void setIsAgentCodeInValid(boolean isAgentCodeInValid) {
		this.isAgentCodeInValid = isAgentCodeInValid;
	}

	public boolean isCityInValid() {
		return isCityInValid;
	}

	public void setCityInValid(boolean isCityInValid) {
		this.isCityInValid = isCityInValid;
	}

	public boolean isAnnualIncomeInValid() {
		return isAnnualIncomeInValid;
	}

	public void setAnnualIncomeInValid(boolean isAnnualIncomeInValid) {
		this.isAnnualIncomeInValid = isAnnualIncomeInValid;
	}

	public boolean isNeedInValid() {
		return isNeedInValid;
	}

	public void setNeedInValid(boolean isNeedInValid) {
		this.isNeedInValid = isNeedInValid;
	}

	public boolean isBranchInValid() {
		return isBranchInValid;
	}

	public void setBranchInValid(boolean isBranchInValid) {
		this.isBranchInValid = isBranchInValid;
	}

	public boolean isBankInValid() {
		return isBankInValid;
	}

	public void setBankInValid(boolean isBankInValid) {
		this.isBankInValid = isBankInValid;
	}

	public boolean isRefNameInValid() {
		return isRefNameInValid;
	}

	public void setRefNameInValid(boolean isRefNameInValid) {
		this.isRefNameInValid = isRefNameInValid;
	}

	public boolean isRefMobileNumberInValid() {
		return isRefMobileNumberInValid;
	}

	public void setRefMobileNumberInValid(boolean isRefMobileNumberInValid) {
		this.isRefMobileNumberInValid = isRefMobileNumberInValid;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAnnualIncome() {
		return annualIncome;
	}

	public void setAnnualIncome(String annualIncome) {
		this.annualIncome = annualIncome;
	}

	public String getNeed() {
		return need;
	}

	public void setNeed(String need) {
		this.need = need;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBank() {
		return bank;
	}

	public String getMeetingDateAndTime() {
		return meetingDateAndTime;
	}

	public void setMeetingDateAndTime(String meetingDateAndTime) {
		this.meetingDateAndTime = meetingDateAndTime;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getRefName() {
		return refName;
	}

	public void setRefName(String refName) {
		this.refName = refName;
	}

	

	
	

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getNationalID() {
		return nationalID;
	}

	public void setNationalID(String nationalID) {
		this.nationalID = nationalID;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getPotential() {
		return potential;
	}

	public void setPotential(String potential) {
		this.potential = potential;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Date getNextmeetingDate() {
		return nextmeetingDate;
	}

	public void setNextmeetingDate(Date nextmeetingDate) {
		this.nextmeetingDate = nextmeetingDate;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getNextMeetingTime() {
		return nextMeetingTime;
	}

	public void setNextMeetingTime(String nextMeetingTime) {
		this.nextMeetingTime = nextMeetingTime;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setStatus(Boolean isCustomerDataValid) {
		this.status = isCustomerDataValid;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setRefMobileNumber(String refMobileNumber) {
		this.refMobileNumber = refMobileNumber;
	}

	public String getRefMobileNumber() {
		return refMobileNumber;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setTransTrackingId(String transTrackingId) {
		this.transTrackingId = transTrackingId;
	}

	public String getTransTrackingId() {
		return transTrackingId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setDobString(String dobString) {
		this.dobString = dobString;
	}

	public String getDobString() {
		return dobString;
	}

	public void setNextmeetingDateString(String nextmeetingDateString) {
		this.nextmeetingDateString = nextmeetingDateString;
	}

	public String getNextmeetingDateString() {
		return nextmeetingDateString;
	}

	public void setAllocatedBranchCode(String allocatedBranchCode) {
		this.allocatedBranchCode = allocatedBranchCode;
	}

	public String getAllocatedBranchCode() {
		return allocatedBranchCode;
	}

	public void setAllocatedBranchName(String allocatedBranchName) {
		this.allocatedBranchName = allocatedBranchName;
	}

	public String getAllocatedBranchName() {
		return allocatedBranchName;
	}
	
	public String getStateValue() {
		return stateValue;
	}

	public void setStateValue(String stateValue) {
		this.stateValue = stateValue;
	}

	public String getOccupationValue() {
		return occupationValue;
	}

	public void setOccupationValue(String occupationValue) {
		this.occupationValue = occupationValue;
	}

	public String getNationalityValue() {
		return nationalityValue;
	}

	public void setNationalityValue(String nationalityValue) {
		this.nationalityValue = nationalityValue;
	}

	public void setAgentCodeInValid(boolean isAgentCodeInValid) {
		this.isAgentCodeInValid = isAgentCodeInValid;
	}
	/**
	 * @return the incomeValue
	 */
	public String getIncomeValue() {
		return incomeValue;
	}

	/**
	 * @param incomeValue the incomeValue to set
	 */
	public void setIncomeValue(String incomeValue) {
		this.incomeValue = incomeValue;
	}

	/**
	 * @return the needValue
	 */
	public String getNeedValue() {
		return needValue;
	}

	/**
	 * @param needValue the needValue to set
	 */
	public void setNeedValue(String needValue) {
		this.needValue = needValue;
	}

	public void setAgentModified(String agentModified) {
		this.agentModified = agentModified;
	}

	public String getAgentModified() {
		return agentModified;
	}
	
	/**
	 * @return the channelType
	 */
	public String getChannelType() {
		return channelType;
	}

	/**
	 * @param channelType the channelType to set
	 */
	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	/**
	 * @param agentName the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param customerLastName the customerLastName to set
	 */
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	/**
	 * @return the customerLastName
	 */
	public String getCustomerLastName() {
		return customerLastName;
	}

	/**
	 * @param isCustomerLastNameInValid the isCustomerLastNameInValid to set
	 */
	public void setCustomerLastNameInValid(boolean isCustomerLastNameInValid) {
		this.isCustomerLastNameInValid = isCustomerLastNameInValid;
	}

	/**
	 * @return the isCustomerLastNameInValid
	 */
	public boolean isCustomerLastNameInValid() {
		return isCustomerLastNameInValid;
	}

	/**
	 * @param customerFirstName the customerFirstName to set
	 */
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	/**
	 * @return the customerFirstName
	 */
	public String getCustomerFirstName() {
		return customerFirstName;
	}
}
