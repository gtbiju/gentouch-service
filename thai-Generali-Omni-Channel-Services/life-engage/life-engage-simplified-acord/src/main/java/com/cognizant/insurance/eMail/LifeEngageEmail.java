package com.cognizant.insurance.eMail;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class LifeEngageEmail {
	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	/** The email values. */
	@Lob
	private String emailValues;

	/** The status. */
	private String status;
	
	/** The attempt Number. */
	private String attemptNumber;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendTime;
	
	/** The status Message. */
	private String statusMessage;
	
	/** The batch job id. */
	private Long batchJobId;
	
	/** The agent Id. */
	private String agentId;
	
	/** The type. */
    private String type;

	
	/**
     * Gets the id.
     *
     * @return the id
     */
	public Long getId() {
		return id;
	}
	
	/**
     * Sets the id.
     *
     * @param id the id to set
     */
	public void setId(Long id) {
		this.id = id;
	}

	/**
     * Gets the email values.
     *
     * @return the email values
     */
	public String getEmailValues() {
		return emailValues;
	}
	
	/**
     * Sets the email values.
     *
     * @param email values the email values to set
     */
	public void setEmailValues(String emailValues) {
		this.emailValues = emailValues;
	}
	
	/**
     * Gets the status.
     *
     * @return the status
     */
	public String getStatus() {
		return status;
	}
	
	/**
     * Sets the status.
     *
     * @param status the status to set
     */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
     * Gets the attempt number.
     *
     * @return the attempt number
     */
	public String getAttemptNumber() {
		return attemptNumber;
	}
	
	/**
     * Sets the attempt number.
     *
     * @param attempt number the attempt number to set
     */
	public void setAttemptNumber(String attemptNumber) {
		this.attemptNumber = attemptNumber;
	}
	
	/**
     * Gets the send Time.
     *
     * @return the send Time
     */
	public Date getSendTime() {
		return sendTime;
	}
	
	/**
     * Sets the send time.
     *
     * @param sendTime the sendTime to set
     */
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	/**
     * Gets the status message.
     *
     * @return the status message
     */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
     * Sets the status Message.
     *
     * @param statusMessage the statusMessage to set
     */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/**
     * Gets the batch Job Id.
     *
     * @return the batch Job Id
     */
	public Long getBatchJobId() {
		return batchJobId;
	}

	/**
     * Sets the batch Job Id.
     *
     * @param batchJobId the batchJobId to set
     */
	public void setBatchJobId(Long batchJobId) {
		this.batchJobId = batchJobId;
	}

	/**
     * Gets the agent Id.
     *
     * @return the agent Id.
     */
	public String getAgentId() {
		return agentId;
	}

	/**
     * Sets the agent Id.
     *
     * @param agent Id the agent Id to set
     */
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	
	 /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     * 
     * @param type
     *            the type to set.
     */
    public void setType(String type) {
        this.type = type;
    }

	
	
}

