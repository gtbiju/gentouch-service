/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 262471
 * @version       : 0.1, Feb 14, 2013
 */
package com.cognizant.insurance.core.helper;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * The Class class LoggingInterceptor.
 */
@Aspect
@Component
public class LoggingInterceptor {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LoggingInterceptor.class);

    /**
     * After excecuting method.
     * 
     * @param joinPoint
     *            the join point
     */
    @Before("execution(* com.cognizant.icr..*.*(..))")
    public final void beforeExcecutingMethod(final JoinPoint joinPoint) {
        LOGGER.info("Inside " + joinPoint.getTarget().getClass().getName() + ", Entering "
                + joinPoint.getSignature().getName());
    }

    /**
     * Before excecuting method.
     * 
     * @param joinPoint
     *            the join point
     */
    @After("execution(* com.cognizant.icr..*.*(..))")
    public final void afterExcecutingMethod(final JoinPoint joinPoint) {
        LOGGER.info("Inside " + joinPoint.getTarget().getClass().getName() + ", Exiting "
                + joinPoint.getSignature().getName());
    }

}
