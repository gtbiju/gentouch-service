/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.contactpointsubtypes;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.contactandplace.ContactPoint;

/**
 * This abstract super-type contains attributes relevant to electronic contacts
 * such as texting, messaging, or email. 
 * 
 * @author 301350
 * 
 * As part of E-App developement removed the abstract
 *
 */

@Entity

public class ElectronicContact extends ContactPoint {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5951573102940974678L;
    
    /**
     * This relationship links an messaging contact with the identifying Uniform
     * Resource Location.
     * 
     * A single, formal string representation of the address for the resource.
     * On a given network type, this Integer provides enough information to be
     * able to locate the associated address point. For example, on a telephone
     * network, this is the string commonly thought of as the "Phone Number."
     * 
     * USAGE NOTE: The format of the Integer is driven either by the convention
     * of the implementer or specified in the definition of the sub-class. E.g.
     * The UniformResourceLocation.identifier has a very specific format.
     * 
     * SOURCE: http://en.wikipedia.org/wiki/Uniform_resource_identifier
     * 
     */   
    private Integer uniformResourceLocationIdentifier;

    /**
     * Defines the email address.
     * 
     * 
     * 
     * 
     * 
     */
    @Column(columnDefinition="nvarchar(120)")
    private String emailAddress;
    
    /** The undeliverable indicator. */
    private Boolean undeliverableIndicator;
    
    /** The attachment ind. */
    private Boolean attachmentInd;
    
    /**
	 * Gets the attachment ind.
	 *
	 * @return the attachmentInd
	 */
	public Boolean getAttachmentInd() {
		return attachmentInd;
	}

    /**
     * Gets the email address.
     *
     * @return the emailAddress
     */
    public String getEmailAddress() {

        return emailAddress;

    }

    /**
     * Gets the undeliverable indicator.
     *
     * @return the undeliverableIndicator
     */
	public Boolean getUndeliverableIndicator() {
		return undeliverableIndicator;
	}
    
    /**
	 * Sets the attachment ind.
	 *
	 * @param attachmentInd the attachmentInd to set
	 */
	public void setAttachmentInd(final Boolean attachmentInd) {
		this.attachmentInd = attachmentInd;
	}

	/**
     * Sets the email address.
     *
     * @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(final String emailAddress) {

        this.emailAddress = emailAddress;

    }

	/**
     * Sets the undeliverable indicator.
     *
     * @param undeliverableIndicator the undeliverableIndicator to set
     */
	public void setUndeliverableIndicator(final Boolean undeliverableIndicator) {
		this.undeliverableIndicator = undeliverableIndicator;
	}
    
    /**
     * @return the uniformResourceLocationIdentifier
     * 
     * 
     */
    public Integer getUniformResourceLocationIdentifier() {
        return uniformResourceLocationIdentifier;
    }

    /**
     * @param uniformResourceLocationIdentifier
     *            the uniformResourceLocationIdentifier to set
     * 
     * 
     */
    public void setUniformResourceLocationIdentifier(
            final Integer uniformResourceLocationIdentifier) {
        this.uniformResourceLocationIdentifier = uniformResourceLocationIdentifier;
    }

}
