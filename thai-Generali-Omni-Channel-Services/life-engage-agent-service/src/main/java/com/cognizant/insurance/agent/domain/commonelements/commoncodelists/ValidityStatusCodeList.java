/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Validity Status.
 * 
 * @author 301350
 * 
 * 
 */
public enum ValidityStatusCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Valid,
    /**
     * 
     * 
     * 
     * 
     */
    Notvalid
}
