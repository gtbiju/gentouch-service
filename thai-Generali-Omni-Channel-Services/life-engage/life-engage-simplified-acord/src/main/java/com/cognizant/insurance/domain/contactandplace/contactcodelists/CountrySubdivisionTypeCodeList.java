/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.contactcodelists;

/**
 * A code list indicating types of country subdivisions.
 * 
 * 
 * @author 301350
 * 
 * 
 */
public enum CountrySubdivisionTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    District,
    /**
     * 
     * 
     * 
     * 
     */
    Province,
    /**
     * 
     * 
     * 
     * 
     */
    State,
    /**
     * 
     * 
     * 
     * 
     */
    Territory,
    /**
     * 
     * 
     * 
     * 
     */
    Arkansas
}
