/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 19, 2012
 */
package com.cognizant.insurance.dao.exception;

/**
 * The Class DaoException.
 */
public class DaoException extends RuntimeException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3436739202417608836L;

    /** The error code. */
    private String errorCode;

    /** The error msg. */
    private String errorMessage;

    /**
     * The Constructor.
     *
     * @param msg
     *            the msg
     */
    public DaoException(final String msg) {
        super(msg);
        errorMessage = msg;
    }

    /**
     * The Constructor.
     *
     * @param cause
     *            the cause
     */
    public DaoException(final Throwable cause) {
        super(cause);
    }

    /**
     * The Constructor.
     *
     * @param msg
     *            the msg
     * @param cause
     *            the cause
     */
    public DaoException(final String msg, final Throwable cause) {
        super(msg, cause);
        errorMessage = msg;
    }

    /**
     * The Constructor.
     *
     * @param errCode
     *            the err code
     * @param msg
     *            the msg
     */
    public DaoException(final String errCode, final String msg) {
        super(msg);
        errorMessage = msg;
        errorCode = errCode;
    }

    /**
     * Gets the errorCode.
     *
     * @return Returns the errorCode.
     */
    public final String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets The errorCode.
     *
     * @param errorCode The errorCode to set.
     */
    public final void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Gets the errorMessage.
     *
     * @return Returns the errorMessage.
     */
    public final String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets The errorMessage.
     *
     * @param errorMessage The errorMessage to set.
     */
    public final void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
