package com.cognizant.insurance.generic.dao;


import java.util.List;
import com.cognizant.insurance.dao.Dao;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.eMail.LifeEngageEmail;

/**
 * The Interface interface EmailDao.
 *
 */
public interface EmailDao extends Dao {

	 /**
     * Retrieve failed email details.
     *
     * @param request the request
     * 
     * @param maxAttemptNo the maxAttemptNo
     * 
     * @return the response
     */
    List<LifeEngageEmail> retrieveFailedLifeEngageEmailDetails(Request<LifeEngageEmail> request, String maxAttemptNo);
    
    /**
     * Retrieve initialised email details.
     *
     * @param request the request
     * 
     * @param agentId the agentId
     * 
     * @return the response
     */
    List<LifeEngageEmail> retrieveInitialisedLifeEngageEmailDetails(Request<LifeEngageEmail> request);
}
