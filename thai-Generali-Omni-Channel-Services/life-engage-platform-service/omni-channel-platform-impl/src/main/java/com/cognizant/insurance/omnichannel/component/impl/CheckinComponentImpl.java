/**
 * 
 */
package com.cognizant.insurance.omnichannel.component.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.CheckinComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliPushNotificationsComponent;
import com.cognizant.insurance.omnichannel.component.repository.CheckinRepository;
import com.cognizant.insurance.omnichannel.domain.CheckinData;
import com.cognizant.insurance.omnichannel.vo.CheckinResponse;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * @author 397850
 *
 */
public class CheckinComponentImpl implements CheckinComponent {
	
	public static final String CHECKINDETAILS="CheckInDetails";
	
	public static final String TRANSACTIONDATA="TransactionData";
	
	public static final String AGENTID="AgentID";
	
	public static final String CHECKEDINBRANCHID="CheckedinBranch";
	
	 /** The Constant DATEFORMAT_YYYY_MM_DD_HH_MM_SS. */
    public static final String DATEFORMAT_YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
	
	
	@Autowired
	private CheckinRepository checkinRepository;
	
	@Autowired
	private TaskExecutor pushNotificationThreadPool;
	 
	 @Autowired
	 private GeneraliPushNotificationsComponent pushNotificationsComponent;
	 
	 @Autowired
	 @Qualifier("checkinDetailsMapping")
	 private LifeEngageSmooksHolder retrieveCheckinListHolder;
	 
	    /** pushNotififcation message */
	 @Value("${le.platform.service.checkin.message}")
	 private String pushNotififcationmessageAgentCheckin;
	
	public String checkinDetailsSave(CheckinData checkinData,
			RequestInfo requestInfo,Boolean sendNotification, List<String> pushNotificationList,String typeVariableObj) throws ParseException{
		
		String response = "";
		HashMap<String, Integer> pushNotificationAgentMap = new HashMap<String, Integer>();
		String message="";
		String dateTimeString="";
		DateFormat df = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM);
		
		//if agent is checked in at another branch,then the agent has to be checked out of that branch before being checked into the new branch
		checkinRepository.retriveAndUpdateCheckedinDetails(checkinData);
		
		response=checkinRepository.checkinDetailsSave(checkinData);
		dateTimeString= df.format(checkinData.getCheckinDateTime());
		message=pushNotififcationmessageAgentCheckin.replace("<Agent Name>", checkinData.getAgentName()).replace("<Branch name>", checkinData.getBranchName()).replace("<Check-in Date and Time>", dateTimeString);
		
		if(response.equalsIgnoreCase("Success") && sendNotification){
			
			for(int i=0; i<pushNotificationList.size(); i++){
				pushNotificationAgentMap.put(pushNotificationList.get(i), 0);
			}
			   pushNotificationThreadPool.execute(new CreatePushNotificationTask(pushNotificationAgentMap,requestInfo,typeVariableObj,message));
			
		}
				
		return response;	
		
	}
	
	@Override
	public String retrieveDetails(RequestInfo requestInfo,List<String> agentList, List<CheckinResponse> checkinResponseList) {
		
		List<CheckinData> checkinList=new ArrayList<CheckinData>();
		String response="";
		
		checkinList=checkinRepository.retireveCheckinList(requestInfo,agentList,checkinResponseList);
		
		for(int i=0;i<checkinResponseList.size();i++){
			
			for(int j=0;j<checkinList.size();j++){
				if(checkinList.get(j).getAgentId().equalsIgnoreCase(checkinResponseList.get(i).getAgentCode())){
					checkinResponseList.get(i).setCheckinData(checkinList.get(j));
				}
			}
			
		}
		
		response=retrieveCheckinListHolder.parseBO(checkinResponseList);
		
		return response;
	}
	

	@Override
	public void updateCheckinDetails() throws ParseException {
		
		List<Date> startAndEndDayList = null;
		 startAndEndDayList = LifeEngageComponentHelper.getCurrentDateLimit();
		
		checkinRepository.updateCheckinDetails(startAndEndDayList.get(1));
		
	}
	

	private class CreatePushNotificationTask implements Runnable {
		private HashMap<String, Integer> agentPushNotificationMapMap;
		private RequestInfo requestInfo;
		private String typeConstant;
		private String message;
		public CreatePushNotificationTask(HashMap<String, Integer> agentMap,
				RequestInfo requestInfo, String type, String message) {
			super();
			this.requestInfo=requestInfo;
			this.agentPushNotificationMapMap=agentMap;
			this.typeConstant=type;
			this.message=message;
		}

		@Override
		public void run() {
			
			
			 if (GeneraliConstants.AGENTCHECKIN.equals(typeConstant)){
				pushNotificationsComponent.pushNotifications(agentPushNotificationMapMap,requestInfo,GeneraliConstants.AGENTCHECKIN,message);
			}
		}
	}


}
