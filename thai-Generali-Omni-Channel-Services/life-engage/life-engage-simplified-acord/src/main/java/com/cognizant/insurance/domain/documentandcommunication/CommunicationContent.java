/**
 * 
 */
package com.cognizant.insurance.domain.documentandcommunication;

import javax.persistence.Entity;


/**
 * The breakdown of a communication into specified and free form content
 * elements. The physical materialization of the communication content, in any
 * format, electronic or physical, is expressed as a document.
 * 
 * e.g: A claim form filled in by John Smith.
 * 
 * @author 301350
 * 
 * 
 */
@Entity
public class CommunicationContent extends Communication {
    /**
     * A text reference that aids the identification and understanding of this
     * communication content.
     * 
     * e.g: complaint
     * 
     * e.g: direct debit authorization.
     * 
     * e.g: Policy document
     * 
     * 
     * 
     */
    private String name;

    /**
     * A free-text statement used to explain the meaning of this communication
     * content. The description may include the purpose and usage of the
     * communication content as well as other aspects.
     * 
     * 
     * 
     */
    private String description;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

   
}
