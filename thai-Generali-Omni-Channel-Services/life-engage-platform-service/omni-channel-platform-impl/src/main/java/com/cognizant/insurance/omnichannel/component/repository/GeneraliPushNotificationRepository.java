package com.cognizant.insurance.omnichannel.component.repository;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.repository.PushNotificationRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.pushnotification.ApplicationDetails;
import com.cognizant.insurance.pushnotification.CompositeKey;
import com.cognizant.insurance.pushnotification.DeviceRegister;
import com.cognizant.insurance.pushnotification.SNSDetails;
import com.cognizant.insurance.request.vo.RequestInfo;


public class GeneraliPushNotificationRepository extends PushNotificationRepository{
	

    //private String pushNotififcationmessage="khách hàng vừa được giới thiệu đến anh/chị.";
    
    /** pushNotififcation message */
    @Value("${le.platform.service.applicationName}")
    private String applicationName;
	
    /** pushNotififcation message */
    @Value("${le.platform.service.message}")
    private String pushNotififcationmessageBanca;
    
    /** pushNotififcation message */
    @Value("${le.platform.service.messageAgency}")
    private String pushNotififcationmessageAgency;
    
    /** pushNotififcation message */
    @Value("${le.platform.service.messageReAssign}")
    private String pushNotififcationmessageReAssign;
    
    
	/**
     * Push notifications.
     * 
     * @param requestInfo
     *            the request info
	 * @param message 
     * @param transactions
     *            the transactions
     * @param json
     *            the json
     * @return true, if successful
     * @throws ParseException
     *             the parse exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Transactional(rollbackFor = { Exception.class },propagation=Propagation.REQUIRES_NEW)
	public boolean sendPushNotification(String notifierId,String type,int count ,RequestInfo requestInfo, String message) throws ParseException, IOException{
       LOGGER.info("Inside sendPushNotification");
		Map<String, List<DeviceRegister>> deviceIdMap = new HashMap<String, List<DeviceRegister>>();
		DeviceRegister deviceRegister = new DeviceRegister();
		CompositeKey key = new CompositeKey();
		String pushNotififcationmessage="";
		key.setAgentID(notifierId);
		deviceRegister.setKey(key);
		deviceRegister.setApplicationName(applicationName);
		deviceRegister.setPlatformtype("ALL");

		ApplicationDetails applnDetails = new ApplicationDetails();
		applnDetails.setApplicationName(applicationName);
		applnDetails.setPlatformType("ALL");
		

		SNSDetails snsServices = new SNSDetails();
		snsServices.setSnsRequestTime(new java.util.Date());
		snsServices.setNotifierID(notifierId);
		if(GeneraliConstants.TYPE_MARKCOMPLETE.equals(type) || GeneraliConstants.TYPE_REASSIGN.equals(type)){
			pushNotififcationmessage=message;
		}
		snsServices.setMessage(pushNotififcationmessage);
		
		
		List<ApplicationDetails> applicationDetailsList = getApplicationDetails(
				requestInfo, applnDetails);
		if (applicationDetailsList.isEmpty()) {
		
			/*throwSystemException(true, Constants.ERROR_CODE_400,
					Constants.INVALID_APPLICATION_INFORMATION);*/
			LOGGER.error("Invalid Application Details"+applicationName);
			snsServices.setSnsStatus(Constants.FAILURE);
			snsServices.setSnsStatusMessage("Invalid Application Details"+applicationName+ "AgentId: " +notifierId);
			savePushNotificationDetails(requestInfo, snsServices);
		}

		List<DeviceRegister> deviceList = getDeviceDetails(requestInfo,
				deviceRegister);

		

		if (deviceList.isEmpty()) {
		/*	throwSystemException(true, Constants.ERROR_CODE_400,
					Constants.INVALID_DEVICE_INFORMATION);*/
			LOGGER.error("Unregiestered Device "+applicationName+ " AgentId: " +notifierId);
			snsServices.setSnsStatus(Constants.FAILURE);
			snsServices.setSnsStatusMessage("Invalid Device Details"+applicationName+ "AgentId: " +notifierId);
			savePushNotificationDetails(requestInfo, snsServices);
		} else {
			deviceIdMap = getDeviceDetailsMap(deviceList);

		}
		 LOGGER.trace("GOT APP and device details");
		successcount = 0;
		errorCount = 0;
		for (ApplicationDetails applicationDetails : applicationDetailsList) {
			platformApplicationArn = null;
			List<DeviceRegister> deviceRegList = deviceIdMap
					.get(applicationDetails.getPlatformType());
			if (deviceRegList!=null&&deviceRegList.size() > 0) {
				sendNotifications(requestInfo, deviceRegList,
						applicationDetails, snsServices);
			}
		}
	        
	        LOGGER.info("Exit sendPushNotification");
		return getStatusMessage();

	}

    /**
     * Gets the status message.
     * 
     * @return the status message
     */
    private boolean getStatusMessage() {
        boolean isSuccess = false;
        if (successcount > 0 && errorCount == 0) {
            isSuccess = true;
        } else if (errorCount > 0 && successcount == 0) {
            //throwSystemException(true, Constants.ERROR_CODE_402, Constants.NOTIFICATION_ERROR);
        } else if (successcount > 0 && errorCount > 0) {
            isSuccess = false;
        }
        return isSuccess;
    }


}
