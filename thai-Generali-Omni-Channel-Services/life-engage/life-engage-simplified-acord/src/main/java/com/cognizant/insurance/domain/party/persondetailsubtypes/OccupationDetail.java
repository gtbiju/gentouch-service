/**
 * 
 */
package com.cognizant.insurance.domain.party.persondetailsubtypes;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;
import com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes.EmploymentRelationship;

/**
 * This concept provides details about the occupation of a person.
 * 
 * Note: This is not the same as the actual employment of a person (for example,
 * a person still has an occupation even though he or she may be unemployed).
 * Someone may be a seller of insurance, regardless of whether the person is an
 * employee of an insurance company, an insurance broker, or self-employed. It
 * is also possible for parties to have more than one occupation at a given
 * point in time.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("OCCUPATIONDETAIL")
public class OccupationDetail extends PersonDetail {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3151169974358167546L;

    /**
     * This attribute describes the person's occupation (e.g. cook, mechanic,
     * etc.).
     * 
     * Note: This is not the same as the professional title, this is a general
     * work description.
     * 
     * 
     * 
     */
    private String occupationClass;

    /**
     * Defines if the person is going to retire.
     * 
     * 
     * 
     * 
     * 
     */
    private Boolean anticipatedRetirementIndicator;

    /**
     * Date when the person anticipates to retire. This attribute is not
     * expected to be filled if anticipatedRetirement is false.
     * 
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date anticipatedRetirementDate;

    /**
     * Retirement date of the person.
     * 
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date retirementDate;

    /**
     * This relationship links occupation detail to an employment relationship.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<EmploymentRelationship> providingEmployment;

    /**
     * Indicates the person is currently employed.
     * 
     * 
     * 
     * 
     * 
     */
    private Boolean currentEmploymentIndicator;

    /**
     * Indicates this occupation is the person's primary occupation.
     * 
     * 
     * 
     * 
     * 
     */
    private Boolean primaryOccupationIndicator;

    /**
     * The title of the person's occupation.
     * 
     * Note: This is not synonymous with "job title" which is associated with
     * employment.
     * 
     * e.g. A person's occupation may be classified as "chef" and might also
     * have a professional title of "Master Chef". The same person's job title
     * might be "Culinary Director".
     * 
     * 
     * 
     */
    private String professionalTitle;

    /******Newly added for EAp - Start********/
    
    /**
     * Indicates the name of the institution
     */
    @Column(columnDefinition="nvarchar(50)")
    private String nameofInstitution; 
    
    /**
     * Indicates the nature of Work
     */
    @Column(columnDefinition="nvarchar(500)")
    private String natureofWork; 
    
    @Column(columnDefinition="nvarchar(400)")
    private String occupationDescription;
    
    @Column(columnDefinition="nvarchar(400)")
    private String occupationDescriptionValue;
    
    /**
     * Description of the Occupation
     */
    @Column(columnDefinition="nvarchar(300)")
    private String description;
    
    /** Added for generali */
    private String natureofWorkValue;
    
    @Column(columnDefinition="nvarchar(300)")
    private String occupationCategoryValue;
    
    private String descriptionOthers;
    
    @Column(columnDefinition="nvarchar(300)")
    private String occupationCategory;
    
    public String getOccupationDescription() {
		return occupationDescription;
	}

	public void setOccupationDescription(String occupationDescription) {
		this.occupationDescription = occupationDescription;
	}

	public String getOccupationDescriptionValue() {
		return occupationDescriptionValue;
	}

	public void setOccupationDescriptionValue(String occupationDescriptionValue) {
		this.occupationDescriptionValue = occupationDescriptionValue;
	}

	public String getNatureofWork() {
		return natureofWork;
	}

	public void setNatureofWork(String natureofWork) {
		this.natureofWork = natureofWork;
	}

	//Added for Thailand
    @Column(columnDefinition="nvarchar(300)")
    private String industry;
    
    @Column(columnDefinition="nvarchar(300)")
    private String job;
    
    @Column(columnDefinition="nvarchar(300)")
    private String subJob;
    
    @Column(columnDefinition="nvarchar(300)")
    private String industryValue;
    
    @Column(columnDefinition="nvarchar(300)")
    private String descriptionValue;
    
    @Column(columnDefinition="nvarchar(300)")
    private String jobValue;
    
    @Column(columnDefinition="nvarchar(300)")
    private String subJobValue;
    
    @Column(columnDefinition="nvarchar(300)")
    private String occupationClassValue;
     

    /******Newly added for EAp - End********/
    
    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }
    
    
    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getSubJob() {
        return subJob;
    }

    public void setSubJob(String subJob) {
        this.subJob = subJob;
    }

    public String getIndustryValue() {
        return industryValue;
    }

    public void setIndustryValue(String industryValue) {
        this.industryValue = industryValue;
    }

    public String getDescriptionValue() {
        return descriptionValue;
    }

    public void setDescriptionValue(String descriptionValue) {
        this.descriptionValue = descriptionValue;
    }

    public String getJobValue() {
        return jobValue;
    }

    public void setJobValue(String jobValue) {
        this.jobValue = jobValue;
    }

    public String getSubJobValue() {
        return subJobValue;
    }

    public void setSubJobValue(String subJobValue) {
        this.subJobValue = subJobValue;
    }

    public String getOccupationClassValue() {
        return occupationClassValue;
    }

    public void setOccupationClassValue(String occupationClassValue) {
        this.occupationClassValue = occupationClassValue;
    }

    public String getOccupationCategoryValue() {
        return occupationCategoryValue;
    }

    public void setOccupationCategoryValue(String occupationCategoryValue) {
        this.occupationCategoryValue = occupationCategoryValue;
    }

    /**
     * Gets the occupationClass.
     * 
     * @return Returns the occupationClass.
     */
    
    
    public String getOccupationClass() {
        return occupationClass;
    }

    public String getNatureofWorkValue() {
		return natureofWorkValue;
	}

	public void setNatureofWorkValue(String natureofWorkValue) {
		this.natureofWorkValue = natureofWorkValue;
	}

	/**
     * Gets the anticipatedRetirementIndicator.
     * 
     * @return Returns the anticipatedRetirementIndicator.
     */
    public Boolean getAnticipatedRetirementIndicator() {
        return anticipatedRetirementIndicator;
    }

    /**
     * Gets the anticipatedRetirementDate.
     * 
     * @return Returns the anticipatedRetirementDate.
     */
    public Date getAnticipatedRetirementDate() {
        return anticipatedRetirementDate;
    }

    /**
     * Gets the retirementDate.
     * 
     * @return Returns the retirementDate.
     */
    public Date getRetirementDate() {
        return retirementDate;
    }

    /**
     * Gets the providingEmployment.
     * 
     * @return Returns the providingEmployment.
     */
    public Set<EmploymentRelationship> getProvidingEmployment() {
        return providingEmployment;
    }

    /**
     * Gets the currentEmploymentIndicator.
     * 
     * @return Returns the currentEmploymentIndicator.
     */
    public Boolean getCurrentEmploymentIndicator() {
        return currentEmploymentIndicator;
    }

    /**
     * Gets the primaryOccupationIndicator.
     * 
     * @return Returns the primaryOccupationIndicator.
     */
    public Boolean getPrimaryOccupationIndicator() {
        return primaryOccupationIndicator;
    }

    /**
     * Gets the professionalTitle.
     * 
     * @return Returns the professionalTitle.
     */
    public String getProfessionalTitle() {
        return professionalTitle;
    }

    /**
     * Sets The occupationClass.
     * 
     * @param occupationClass
     *            The occupationClass to set.
     */
    public void setOccupationClass(final String occupationClass) {
        this.occupationClass = occupationClass;
    }

    /**
     * Sets The anticipatedRetirementIndicator.
     * 
     * @param anticipatedRetirementIndicator
     *            The anticipatedRetirementIndicator to set.
     */
    public void setAnticipatedRetirementIndicator(
            final Boolean anticipatedRetirementIndicator) {
        this.anticipatedRetirementIndicator = anticipatedRetirementIndicator;
    }

    /**
     * Sets The anticipatedRetirementDate.
     * 
     * @param anticipatedRetirementDate
     *            The anticipatedRetirementDate to set.
     */
    public void setAnticipatedRetirementDate(
            final Date anticipatedRetirementDate) {
        this.anticipatedRetirementDate = anticipatedRetirementDate;
    }

    /**
     * Sets The retirementDate.
     * 
     * @param retirementDate
     *            The retirementDate to set.
     */
    public void setRetirementDate(final Date retirementDate) {
        this.retirementDate = retirementDate;
    }

    /**
     * Sets The providingEmployment.
     * 
     * @param providingEmployment
     *            The providingEmployment to set.
     */
    public void setProvidingEmployment(
            final Set<EmploymentRelationship> providingEmployment) {
        this.providingEmployment = providingEmployment;
    }

    /**
     * Sets The currentEmploymentIndicator.
     * 
     * @param currentEmploymentIndicator
     *            The currentEmploymentIndicator to set.
     */
    public void setCurrentEmploymentIndicator(
            final Boolean currentEmploymentIndicator) {
        this.currentEmploymentIndicator = currentEmploymentIndicator;
    }

    /**
     * Sets The primaryOccupationIndicator.
     * 
     * @param primaryOccupationIndicator
     *            The primaryOccupationIndicator to set.
     */
    public void setPrimaryOccupationIndicator(
            final Boolean primaryOccupationIndicator) {
        this.primaryOccupationIndicator = primaryOccupationIndicator;
    }

    /**
     * Sets The professionalTitle.
     * 
     * @param professionalTitle
     *            The professionalTitle to set.
     */
    public void setProfessionalTitle(final String professionalTitle) {
        this.professionalTitle = professionalTitle;
    }

    /**
     * @return the nameofInstitution
     */
    public String getNameofInstitution() {
        return nameofInstitution;
    }

    /**
     * @param nameofInstitution the nameofInstitution to set
     */
    public void setNameofInstitution(String nameofInstitution) {
        this.nameofInstitution = nameofInstitution;
    }

    
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

	public String getDescriptionOthers() {
		return descriptionOthers;
	}

	public void setDescriptionOthers(String descriptionOthers) {
		this.descriptionOthers = descriptionOthers;
	}

	public String getOccupationCategory() {
		return occupationCategory;
	}

	public void setOccupationCategory(String occupationCategory) {
		this.occupationCategory = occupationCategory;
	}
    

}
