/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 23, 2015
 */
package com.cognizant.insurance.generic.component.impl;

import java.util.List;

import com.cognizant.insurance.generic.component.PushNotificationComponent;
import com.cognizant.insurance.generic.dao.PushNotificationDao;
import com.cognizant.insurance.generic.dao.impl.PushNotificationDaoImpl;
import com.cognizant.insurance.pushnotification.ApplicationDetails;
import com.cognizant.insurance.pushnotification.DeviceRegister;
import com.cognizant.insurance.pushnotification.SNSDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;

/**
 * The Class class PushNotificationComponentImpl.
 */
public class PushNotificationComponentImpl implements PushNotificationComponent {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.component.PushNotificationComponent#registerDevice(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public void registerDevice(Request<DeviceRegister> request) {

        PushNotificationDao pushNotificationDao = new PushNotificationDaoImpl();
        pushNotificationDao.save(request);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.component.PushNotificationComponent#isDeviceIdExists(com.cognizant.insurance.
     * request.Request)
     */
    @Override
    public boolean isDeviceIdExists(Request<DeviceRegister> request) {

        PushNotificationDao pushNotificationDao = new PushNotificationDaoImpl();
        return pushNotificationDao.isDeviceIdExists(request);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.component.PushNotificationComponent#unRegisterDevice(com.cognizant.insurance.
     * request.Request)
     */
    @Override
    public void unRegisterDevice(Request<DeviceRegister> request) {

        PushNotificationDao pushNotificationDao = new PushNotificationDaoImpl();
        pushNotificationDao.unRegisterDevice(request);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.component.PushNotificationComponent#getDeviceDetails(com.cognizant.insurance.
     * request.Request)
     */
    @Override
    public Response<List<DeviceRegister>> getDeviceDetails(Request<DeviceRegister> request) {

        PushNotificationDao pushNotificationDao = new PushNotificationDaoImpl();
        return pushNotificationDao.getDeviceDetails(request);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.component.PushNotificationComponent#getApplicationCredentials(com.cognizant.insurance
     * .request.Request)
     */
    @Override
    public Response<List<ApplicationDetails>> getApplicationDetails(Request<ApplicationDetails> request) {

        PushNotificationDao pushNotificationDao = new PushNotificationDaoImpl();
        return pushNotificationDao.getApplicationDetails(request);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.generic.component.PushNotificationComponent#savePushNotificationDetails(com.cognizant
     * .insurance.request.Request)
     */
    @Override
    public void savePushNotificationDetails(Request<SNSDetails> snsServiceRq) {

        PushNotificationDao pushNotificationDao = new PushNotificationDaoImpl();
        pushNotificationDao.save(snsServiceRq);

    }

}
