package com.cognizant.insurance.omnichannel.component.repository;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.omnichannel.domain.GeneraliSPAJNo;
import com.cognizant.insurance.omnichannel.jpa.component.SpajNoComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;

public class GeneraliSPAJNoRepository {
	@PersistenceContext(unitName="LE_Platform")
	private EntityManager entityManager;

	@Autowired
	private SpajNoComponent spajNoComponent;

	public List<String> generateSpajNos(String agentId)
			throws ParseException {
		if (agentId == null || agentId.isEmpty()) {
			throw new IllegalArgumentException("Agent is missing");
		}
		List<String> spajNos = new ArrayList<String>();
		final Request<GeneraliSPAJNo> spajRequest = new JPARequestImpl<GeneraliSPAJNo>(
				entityManager, null, "");
			GeneraliSPAJNo generaliSPAJNo = new GeneraliSPAJNo();
			generaliSPAJNo.setAgentId(agentId);
			generaliSPAJNo.setAllocationDate(LifeEngageComponentHelper
					.getCurrentdate());
			spajRequest.setType(generaliSPAJNo);
			spajNoComponent.save(spajRequest);
			spajNos.add(generaliSPAJNo.getSpajNo());
		return spajNos;

	}
	public boolean isSpajExisting(String spajNo, RequestInfo requestInfo,
			String identifier) {
		
		final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor();
		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				entityManager, ContextTypeCodeList.EAPP,
				requestInfo.getTransactionId());
		statusRequest.setType(statusList);
		final Request<InsuranceAgreement> spajRequest = new JPARequestImpl<InsuranceAgreement>(
				entityManager, null, "");
		InsuranceAgreement agreement = new InsuranceAgreement();
		agreement.setContextId(ContextTypeCodeList.EAPP);
		// agreement.setIdentifier=spajNo;
		spajRequest.setType(agreement);
		Boolean isSpajExists = spajNoComponent.isSpajExisting(spajRequest,
				spajNo, statusRequest, identifier);
		return isSpajExists;
	}

	protected List<AgreementStatusCodeList> getAgreementStatusCodesFor() {
		List<AgreementStatusCodeList> statusCodeLists = new ArrayList<AgreementStatusCodeList>();
		 statusCodeLists.add(AgreementStatusCodeList.PendingSubmission);
         statusCodeLists.add(AgreementStatusCodeList.Submitted);

		return statusCodeLists;
	}
}
