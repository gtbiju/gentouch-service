/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304007
 * @version       : 0.1, Jan 31, 2013
 */
package com.cognizant.insurance.agent.searchcriteria;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.cognizant.insurance.agent.domain.product.CalculationType;
import com.cognizant.insurance.agent.domain.product.ProductCollaterals;
import com.cognizant.insurance.agent.domain.product.ProductTemplateMapping;

/**
 * The Class ProductSearchCriteria.
 */
public class ProductSearchCriteria implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 15945587189639988L;

    /** The id. */
    private Long id;

    /** The code. */
    private String code;

    /** The lob id. */
    private String lobId;

    /** The family id. */
    private String categoryId;

    /** The name. */
    private String name;

    /** The carrier code. */
    private Long carrierCode;

    /** The product Date. */
    private Date checkDate;

    /** The channel Id. */
    private String channelId;

    /** The calculation types. */
    private List<CalculationType> calculationTypes;

    /** The collaterals *. */
    private List<ProductCollaterals> collaterals;

    /** The templates_mapping *. */
    private List<ProductTemplateMapping> templatemapping;

    /** The is all data required. */
    private Boolean isAllDataRequired;

    /** The is properties required. */
    private Boolean isPropertiesRequired;

    /** The is riders required. */
    private Boolean isRidersRequired;

    /** The is plans required. */
    private Boolean isPlansRequired;

    /** The is channels required. */
    private Boolean isChannelsRequired;

    /** The is funds required. */
    private Boolean isFundsRequired;

    /** The is rule spec required. */
    private Boolean isRuleSpecRequired;

    /** The is calc spec required. */
    private Boolean isCalcSpecRequired;

    /** The is collaterals required. */
    private Boolean isCollateralsRequired;

    /** The is templates required. */
    private Boolean isTemplatesRequired;

    /** The is offline db required. */
    private Boolean isOfflineDBRequired;

    /** The is inactive rider required. */
    private Boolean isInactiveRiderRequired;

    /** The is inactive plans required. */
    private Boolean isInactivePlansRequired;

    /** The product ids. */
    private List<Long> productIds;
    /** The template language. */
    private String templateLanguage;

    /** The template type. */
    private String templateType;

    /** The template country code. */
    private String templateCountryCode;
    /**
     * Gets the id.
     * 
     * @return Returns the id.
     */
    public final Long getId() {
        return id;
    }

    /**
     * Sets The id.
     * 
     * @param id
     *            The id to set.
     */
    public final void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the code.
     * 
     * @return Returns the code.
     */
    public final String getCode() {
        return code;
    }

    /**
     * Sets The code.
     * 
     * @param code
     *            The code to set.
     */
    public final void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the lobId.
     * 
     * @return Returns the lobId.
     */
    public final String getLobId() {
        return lobId;
    }

    /**
     * Sets The lobId.
     * 
     * @param lobId
     *            The lobId to set.
     */
    public final void setLobId(final String lobId) {
        this.lobId = lobId;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets The categoryId.
     * 
     * @param categoryId
     *            The categoryId to set.
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Gets the categoryId.
     * 
     * @return Returns the categoryId.
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * Sets The calculationTypes.
     * 
     * @param calculationTypes
     *            The calculationTypes to set.
     */
    public void setCalculationTypes(final List<CalculationType> calculationTypes) {
        this.calculationTypes = calculationTypes;
    }

    /**
     * Gets the calculationTypes.
     * 
     * @return Returns the calculationTypes.
     */
    public List<CalculationType> getCalculationTypes() {
        return calculationTypes;
    }

    /**
     * Sets The carrierCode.
     * 
     * @param carrierCode
     *            The carrierCode to set.
     */
    public void setCarrierCode(final Long carrierCode) {
        this.carrierCode = carrierCode;
    }

    /**
     * Gets the carrierCode.
     * 
     * @return Returns the carrierCode.
     */
    public Long getCarrierCode() {
        return carrierCode;
    }

    /**
     * Gets the collaterals.
     * 
     * @return the collaterals
     */
    public List<ProductCollaterals> getCollaterals() {
        return collaterals;
    }

    /**
     * Sets the collaterals.
     * 
     * @param collaterals
     *            the collaterals to set
     */
    public void setCollaterals(List<ProductCollaterals> collaterals) {
        this.collaterals = collaterals;
    }

    /**
     * Gets the templatemapping.
     * 
     * @return the templatemapping
     */
    public List<ProductTemplateMapping> getTemplatemapping() {
        return templatemapping;
    }

    /**
     * Sets the templatemapping.
     * 
     * @param templatemapping
     *            the templatemapping to set.
     */
    public void setTemplatemapping(List<ProductTemplateMapping> templatemapping) {
        this.templatemapping = templatemapping;
    }

    /**
     * Gets the check date.
     * 
     * @return the checkDate
     */
    public Date getCheckDate() {
        return checkDate;
    }

    /**
     * Sets the check date.
     * 
     * @param checkDate
     *            the checkDate to set
     */
    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    /**
     * Gets the channel id.
     * 
     * @return the channelId
     */
    public String getChannelId() {
        return channelId;
    }

    /**
     * Sets the channel id.
     * 
     * @param channelId
     *            the channelId to set
     */
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    /**
     * Gets the checks if is all data required.
     * 
     * @return the isAllDataRequired
     */
    public Boolean getIsAllDataRequired() {
        return isAllDataRequired;
    }

    /**
     * Sets the checks if is all data required.
     * 
     * @param isAllDataRequired
     *            the isAllDataRequired to set
     */
    public void setIsAllDataRequired(Boolean isAllDataRequired) {
        this.isAllDataRequired = isAllDataRequired;
    }

    /**
     * Gets the checks if is properties required.
     * 
     * @return the isPropertiesRequired
     */
    public Boolean getIsPropertiesRequired() {
        return isPropertiesRequired;
    }

    /**
     * Sets the checks if is properties required.
     * 
     * @param isPropertiesRequired
     *            the isPropertiesRequired to set
     */
    public void setIsPropertiesRequired(Boolean isPropertiesRequired) {
        this.isPropertiesRequired = isPropertiesRequired;
    }

    /**
     * Gets the checks if is riders required.
     * 
     * @return the isRidersRequired
     */
    public Boolean getIsRidersRequired() {
        return isRidersRequired;
    }

    /**
     * Sets the checks if is riders required.
     * 
     * @param isRidersRequired
     *            the isRidersRequired to set
     */
    public void setIsRidersRequired(Boolean isRidersRequired) {
        this.isRidersRequired = isRidersRequired;
    }

    /**
     * Gets the checks if is plans required.
     * 
     * @return the isPlansRequired
     */
    public Boolean getIsPlansRequired() {
        return isPlansRequired;
    }

    /**
     * Sets the checks if is plans required.
     * 
     * @param isPlansRequired
     *            the isPlansRequired to set
     */
    public void setIsPlansRequired(Boolean isPlansRequired) {
        this.isPlansRequired = isPlansRequired;
    }

    /**
     * Gets the checks if is channels required.
     * 
     * @return the isChannelsRequired
     */
    public Boolean getIsChannelsRequired() {
        return isChannelsRequired;
    }

    /**
     * Sets the checks if is channels required.
     * 
     * @param isChannelsRequired
     *            the isChannelsRequired to set
     */
    public void setIsChannelsRequired(Boolean isChannelsRequired) {
        this.isChannelsRequired = isChannelsRequired;
    }

    /**
     * Gets the checks if is funds required.
     * 
     * @return the isFundsRequired
     */
    public Boolean getIsFundsRequired() {
        return isFundsRequired;
    }

    /**
     * Sets the checks if is funds required.
     * 
     * @param isFundsRequired
     *            the isFundsRequired to set
     */
    public void setIsFundsRequired(Boolean isFundsRequired) {
        this.isFundsRequired = isFundsRequired;
    }

    /**
     * Gets the checks if is rule spec required.
     * 
     * @return the isRuleSpecRequired
     */
    public Boolean getIsRuleSpecRequired() {
        return isRuleSpecRequired;
    }

    /**
     * Sets the checks if is rule spec required.
     * 
     * @param isRuleSpecRequired
     *            the isRuleSpecRequired to set
     */
    public void setIsRuleSpecRequired(Boolean isRuleSpecRequired) {
        this.isRuleSpecRequired = isRuleSpecRequired;
    }

    /**
     * Gets the checks if is calc spec required.
     * 
     * @return the isCalcSpecRequired
     */
    public Boolean getIsCalcSpecRequired() {
        return isCalcSpecRequired;
    }

    /**
     * Sets the checks if is calc spec required.
     * 
     * @param isCalcSpecRequired
     *            the isCalcSpecRequired to set
     */
    public void setIsCalcSpecRequired(Boolean isCalcSpecRequired) {
        this.isCalcSpecRequired = isCalcSpecRequired;
    }

    /**
     * Gets the checks if is collaterals required.
     * 
     * @return the isCollateralsRequired
     */
    public Boolean getIsCollateralsRequired() {
        return isCollateralsRequired;
    }

    /**
     * Sets the checks if is collaterals required.
     * 
     * @param isCollateralsRequired
     *            the isCollateralsRequired to set
     */
    public void setIsCollateralsRequired(Boolean isCollateralsRequired) {
        this.isCollateralsRequired = isCollateralsRequired;
    }

    /**
     * Gets the checks if is templates required.
     * 
     * @return the isTemplatesRequired
     */
    public Boolean getIsTemplatesRequired() {
        return isTemplatesRequired;
    }

    /**
     * Sets the checks if is templates required.
     * 
     * @param isTemplatesRequired
     *            the isTemplatesRequired to set
     */
    public void setIsTemplatesRequired(Boolean isTemplatesRequired) {
        this.isTemplatesRequired = isTemplatesRequired;
    }

    /**
     * Gets the checks if is offline db required.
     * 
     * @return the isOfflineDBRequired
     */
    public Boolean getIsOfflineDBRequired() {
        return isOfflineDBRequired;
    }

    /**
     * Sets the checks if is offline db required.
     * 
     * @param isOfflineDBRequired
     *            the isOfflineDBRequired to set
     */
    public void setIsOfflineDBRequired(Boolean isOfflineDBRequired) {
        this.isOfflineDBRequired = isOfflineDBRequired;
    }

    /**
     * Gets the checks if is inactive rider required.
     * 
     * @return the isInactiveRiderRequired
     */
    public Boolean getIsInactiveRiderRequired() {
        return isInactiveRiderRequired;
    }

    /**
     * Sets the checks if is inactive rider required.
     * 
     * @param isInactiveRiderRequired
     *            the isInactiveRiderRequired to set
     */
    public void setIsInactiveRiderRequired(Boolean isInactiveRiderRequired) {
        this.isInactiveRiderRequired = isInactiveRiderRequired;
    }

    /**
     * Gets the checks if is inactive plans required.
     * 
     * @return the isInactivePlansRequired
     */
    public Boolean getIsInactivePlansRequired() {
        return isInactivePlansRequired;
    }

    /**
     * Sets the checks if is inactive plans required.
     * 
     * @param isInactivePlansRequired
     *            the isInactivePlansRequired to set
     */
    public void setIsInactivePlansRequired(Boolean isInactivePlansRequired) {
        this.isInactivePlansRequired = isInactivePlansRequired;
    }

    /**
     * Gets the product ids.
     * 
     * @return the productIds
     */
    public List<Long> getProductIds() {
        return productIds;
    }

    /**
     * Sets the product ids.
     * 
     * @param productIds
     *            the productIds to set
     */
    public void setProductIds(List<Long> productIds) {
        this.productIds = productIds;
    }

    /**
     * Gets the template language.
     *
     * @return the templateLanguage
     */
    public String getTemplateLanguage() {
        return templateLanguage;
    }

    /**
     * Sets the template language.
     *
     * @param templateLanguage the templateLanguage to set
     */
    public void setTemplateLanguage(String templateLanguage) {
        this.templateLanguage = templateLanguage;
    }

    /**
     * Gets the template type.
     *
     * @return the templateType
     */
    public String getTemplateType() {
        return templateType;
    }

    /**
     * Sets the template type.
     *
     * @param templateType the templateType to set
     */
    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    /**
     * Gets the template country code.
     *
     * @return the templateCountryCode
     */
    public String getTemplateCountryCode() {
        return templateCountryCode;
    }

    /**
     * Sets the template country code.
     *
     * @param templateCountryCode the templateCountryCode to set
     */
    public void setTemplateCountryCode(String templateCountryCode) {
        this.templateCountryCode = templateCountryCode;
    }

}
