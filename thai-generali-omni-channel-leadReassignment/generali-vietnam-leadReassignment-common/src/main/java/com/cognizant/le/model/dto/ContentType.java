package com.cognizant.le.model.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class ContentType.
 */
@NamedQueries({
	@NamedQuery(
	name = "fetchContentTypes",
	query = "from ContentType"
	)
})

@Entity
@Table(name = "CONTENT_TYPES")
public class ContentType {

	/** The id. */
	@Id
	@Column(name="Id")
	private Integer id;
	
	/** The type. */
	@Column(name="Type")
	private String type;
	
	/** The priority. */
	@Column(name="priority")
	private Integer priority;
	
	/** The extensions. */
	@Column(name="extensions")
	private String extensions;
	
	/** The optionality. */
	@Column(name="optionality")
	private Boolean optionality;
	
	/** The previewable. */
	@Column(name="previewable")
	private Boolean previewable;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the priority.
	 *
	 * @return the priority
	 */
	public Integer getPriority() {
		return priority;
	}

	/**
	 * Gets the extensions.
	 *
	 * @return the extensions
	 */
	public String getExtensions() {
		return extensions;
	}

	/**
	 * Sets the priority.
	 *
	 * @param priority the new priority
	 */
	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	/**
	 * Sets the extensions.
	 *
	 * @param extensions the new extensions
	 */
	public void setExtensions(String extensions) {
		this.extensions = extensions;
	}

	/**
	 * Gets the optionality.
	 *
	 * @return the optionality
	 */
	public Boolean getOptionality() {
		return optionality;
	}

	/**
	 * Sets the optionality.
	 *
	 * @param optionality the new optionality
	 */
	public void setOptionality(Boolean optionality) {
		this.optionality = optionality;
	}

	/**
	 * Gets the previewable.
	 *
	 * @return the previewable
	 */
	public Boolean getPreviewable() {
		return previewable;
	}

	/**
	 * Sets the previewable.
	 *
	 * @param previewable the new previewable
	 */
	public void setPreviewable(Boolean previewable) {
		this.previewable = previewable;
	}	
}
