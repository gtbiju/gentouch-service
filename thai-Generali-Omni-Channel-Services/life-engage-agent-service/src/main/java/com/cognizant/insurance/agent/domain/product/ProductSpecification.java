/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;

/**
 * The Class ProductSpecification.
 * 
 * @author 304007
 */

@Entity
@NamedQueries({
    @NamedQuery(name = "ProductSpecification.findByCarrierCode", query = "select prodSpecification from ProductSpecification prodSpecification where prodSpecification.carrier.id = :carrierId and prodSpecification.productType.id = 812")
    ,@NamedQuery(name = "ProductSpecification.findActiveProducts", query = "select prodSpecification from ProductSpecification prodSpecification where prodSpecification.carrier.id = :carrierId and prodSpecification.suspensionDate >= :productSuspensionDate and withdrawalDate >= :productWithdrawalDate and terminationDate >= :productTerminationDate and marketableIndicator = 1" )
    ,@NamedQuery(name = "productSpecification.getProductById", query = "select p from ProductSpecification p where p.id =:productId order by p.id ASC")
    ,@NamedQuery(name = "productSpecification.getProductsByIds", query = "select p from ProductSpecification p where p.id in :productId order by p.id ASC")
    ,@NamedQuery(name = "productSpecification.getProduct", query = "select p from ProductSpecification p where p.productCode =:productCode and p.carrier.id =:carrierCode order by p.id ASC ")
    ,@NamedQuery(name = "ProductSpecification.findActiveProductByID", query = "select p from ProductSpecification p where p.carrier.id = :carrierId and p.suspensionDate >= :productSuspensionDate and p.withdrawalDate >= :productWithdrawalDate and p.terminationDate >= :productTerminationDate and p.marketableIndicator = 1 and p.id =:productId and p.productType.id = 812" )
    ,@NamedQuery(name= "ProductSpecification.findActiveProductsByFilter", query = "select prodSpecification from ProductSpecification prodSpecification join prodSpecification.productChannels channel where prodSpecification.carrier.id = :carrierId and prodSpecification.suspensionDate >= :productSuspensionDate and prodSpecification.withdrawalDate >= :productWithdrawalDate and prodSpecification.terminationDate >= :productTerminationDate and prodSpecification.marketableIndicator = 1 and channel.channelType.id = :channelId" )
    ,@NamedQuery(name= "ProductSpecification.findAllProductsByFilter", query = "select prodSpecification from ProductSpecification prodSpecification join prodSpecification.productChannels channel where prodSpecification.carrier.id = :carrierId and prodSpecification.productType.id = 812 and channel.channelType.id = :channelId")
    })
@Table(name = "CORE_PRODUCT_COMPONENT")
public class ProductSpecification extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7136100978621736164L;
    
    private static final String DATETIMEDEFAULTNULL = "datetime DEFAULT NULL";

    /** The name. */
    @Column(name = "NAME")
    private String name;
    
    /** The localname. */
    @Column(name = "LOCAL_NAME")
    private String localName;
    
    /** The business description. */
    @Column(name = "BUSINESS_DESCRIPTION", columnDefinition = "varchar(max)")
    private String businessDescription;

    /** The formal definition. */
    @Column(name = "FORMAL_DEFINITION", columnDefinition = "varchar(max)")
    private String formalDefinition;

    /** The type. imp */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "productType_ID")
    private ProductType productType;

    /** The version. */
    @Column(name = "VERSION")
    private String version;

    /** The version date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "VERSION_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date versionDate;

    /** The carrier. imp */
    @ManyToOne(cascade = { CascadeType.REFRESH })
    private Carrier carrier;

    /** The product code. */
    @Column(name = "PRODUCT_CODE")
    private String productCode;

    /** The investment type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "investmentType_ID")
    private InvestmentType investmentType;

    /** The channel type. imp */

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "channelType_ID")
    private ChannelType channelType;

    /** The cover type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "coverType_ID")
    private CoverType coverType;

    /** The plan code. */
    @Column(name = "PLAN_CODE")
    private String planCode;

    /** The plan name. */
    @Column(name = "PLAN_NAME")
    private String planName;

    /** The product group. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "PRODUCT_GROUP")
    private ProductGroupType productGroup;

    /** The auto include at issue. */
    @Column(name = "AUTO_INCLUDE_AT_ISSUE")
    private Boolean autoIncludeAtIssue;

    /** The short name. */
    @Column(name = "SHORT_NAME")
    private String shortName;

    /** The marketable name. */
    @Column(name = "MARKETABLE_NAME")
    private String marketableName;

    /** The family. imp */

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "category_ID")
    private CategoryType category;

    /** The line of business. */

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "lineOfBusiness_ID")
    private LobType lineOfBusiness;

    /** The marketable indicator. */
    @Column(name = "MARKETABLE_INDICATOR")
    private Boolean marketableIndicator;

    /** The launch date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "LAUNCH_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date launchDate;

    /** The suspension date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "SUSPENSION_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date suspensionDate;

    /** The termination date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "TERMINATION_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date terminationDate;

    /** The withdrawal date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "WITHDRAWAL_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date withdrawalDate;

    /** The parent product. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "parentProduct_id")
    private ProductSpecification parentProduct;

    /** The child products. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parentProduct")
    private Set<ProductSpecification> childProducts;

    /** The child products. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "productSpecification")
    private Set<ProductChannel> productChannels;

    /** The collaterals. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "productId")
    private Set<ProductCollaterals> prodCollaterals;

    /** The templates. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "productSpecification")
    private Set<ProductTemplateMapping> prodTemplatesMapping;

    /** The calculations. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parentProductId")
    private Set<CalculationSpecification> calculations;

    /** The properties. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE },fetch = FetchType.LAZY, mappedBy = "parentProduct")
    private Set<PropertySpecification> properties;

    /** The rules. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "parentProduct")
    private Set<RuleSpecification> rules;

    /** The roles. */
    // @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @Transient
    private Set<RoleSpecification> roles;

    /** The requests. */
    // @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @Transient
    private Set<RequestSpecification> requests;

    /** The marketable indicator. */
    @Column(name = "PUSH_SELL_INDICATOR")
    private Boolean pushSellIndicator;

    /** The marketable indicator. */
    @Column(name = "WEIGHTAGE")
    private BigDecimal productWeightage;

    /** The child products. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "productSpecification")
    private Set<ProductFundMapping> productFundMappings;

    /**
     * The Constructor.
     */
    public ProductSpecification() {
        super();
    }

    /**
     * Gets the product code.
     * 
     * @return the product code
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the product code.
     * 
     * @param productCode
     *            the product code
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * Gets the short name.
     * 
     * @return the short name
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Sets the short name.
     * 
     * @param shortName
     *            the short name
     */
    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }

    /**
     * Gets the marketable name.
     * 
     * @return the marketable name
     */
    public String getMarketableName() {
        return marketableName;
    }

    /**
     * Sets the marketable name.
     * 
     * @param marketableName
     *            the marketable name
     */
    public void setMarketableName(final String marketableName) {
        this.marketableName = marketableName;
    }

    /**
     * Sets The localName.
     *
     * @param localName The localName to set.
     */
    public void setLocalName(String localName) {
        this.localName = localName;
    }

    /**
     * Gets the localName.
     *
     * @return Returns the localName.
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * Gets the line of business.
     * 
     * @return the line of business
     */
    public LobType getLineOfBusiness() {
        return lineOfBusiness;
    }

    /**
     * Sets the line of business.
     * 
     * @param lineOfBusiness
     *            the line of business
     */
    public void setLineOfBusiness(final LobType lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }

    /**
     * Gets the marketable indicator.
     * 
     * @return the marketable indicator
     */
    public Boolean getMarketableIndicator() {
        return marketableIndicator;
    }

    /**
     * Sets the marketable indicator.
     * 
     * @param marketableIndicator
     *            the marketable indicator
     */
    public void setMarketableIndicator(final Boolean marketableIndicator) {
        this.marketableIndicator = marketableIndicator;
    }

    /**
     * Gets the launch date.
     * 
     * @return the launch date
     */
    public Date getLaunchDate() {
        return launchDate;
    }

    /**
     * Sets the launch date.
     * 
     * @param launchDate
     *            the launch date
     */
    public void setLaunchDate(final Date launchDate) {
        this.launchDate = launchDate;
    }

    /**
     * Gets the suspension date.
     * 
     * @return the suspension date
     */
    public Date getSuspensionDate() {
        return suspensionDate;
    }

    /**
     * Sets the suspension date.
     * 
     * @param suspensionDate
     *            the suspension date
     */
    public void setSuspensionDate(final Date suspensionDate) {
        this.suspensionDate = suspensionDate;
    }

    /**
     * Gets the termination date.
     * 
     * @return the termination date
     */
    public Date getTerminationDate() {
        return terminationDate;
    }

    /**
     * Sets the termination date.
     * 
     * @param terminationDate
     *            the termination date
     */
    public void setTerminationDate(final Date terminationDate) {
        this.terminationDate = terminationDate;
    }

    /**
     * Gets the withdrawal date.
     * 
     * @return the withdrawal date
     */
    public Date getWithdrawalDate() {
        return withdrawalDate;
    }

    /**
     * Sets the withdrawal date.
     * 
     * @param withdrawalDate
     *            the withdrawal date
     */
    public void setWithdrawalDate(final Date withdrawalDate) {
        this.withdrawalDate = withdrawalDate;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }
    
    /**
     * Gets the businessDescription.
     * 
     * @return Returns the businessDescription.
     */
    public String getBusinessDescription() {
        return businessDescription;
    }

    /**
     * Sets The businessDescription.
     * 
     * @param businessDescription
     *            The businessDescription to set.
     */
    public void setBusinessDescription(final String businessDescription) {
        this.businessDescription = businessDescription;
    }

    /**
     * Gets the formalDefinition.
     * 
     * @return Returns the formalDefinition.
     */
    public String getFormalDefinition() {
        return formalDefinition;
    }

    /**
     * Sets The formalDefinition.
     * 
     * @param formalDefinition
     *            The formalDefinition to set.
     */
    public void setFormalDefinition(final String formalDefinition) {
        this.formalDefinition = formalDefinition;
    }

    /**
     * Gets the version.
     * 
     * @return Returns the version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets The version.
     * 
     * @param version
     *            The version to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * Gets the versionDate.
     * 
     * @return Returns the versionDate.
     */
    public Date getVersionDate() {
        return versionDate;
    }

    /**
     * Sets The versionDate.
     * 
     * @param versionDate
     *            The versionDate to set.
     */
    public void setVersionDate(final Date versionDate) {
        this.versionDate = versionDate;
    }

    /**
     * Gets the planCode.
     * 
     * @return Returns the planCode.
     */
    public String getPlanCode() {
        return planCode;
    }

    /**
     * Sets The planCode.
     * 
     * @param planCode
     *            The planCode to set.
     */
    public void setPlanCode(final String planCode) {
        this.planCode = planCode;
    }

    /**
     * Gets the planName.
     * 
     * @return Returns the planName.
     */
    public String getPlanName() {
        return planName;
    }

    /**
     * Sets The planName.
     * 
     * @param planName
     *            The planName to set.
     */
    public void setPlanName(final String planName) {
        this.planName = planName;
    }

    /**
     * Gets the productGroup.
     * 
     * @return Returns the productGroup.
     */
    public ProductGroupType getProductGroup() {
        return productGroup;
    }

    /**
     * Sets The productGroup.
     * 
     * @param productGroup
     *            The productGroup to set.
     */
    public void setProductGroup(final ProductGroupType productGroup) {
        this.productGroup = productGroup;
    }

    /**
     * Gets the productType.
     * 
     * @return Returns the productType.
     */
    public ProductType getProductType() {
        return productType;
    }

    /**
     * Sets The productType.
     * 
     * @param productType
     *            The productType to set.
     */
    public void setProductType(final ProductType productType) {
        this.productType = productType;
    }

    /**
     * Gets the investmentType.
     * 
     * @return Returns the investmentType.
     */
    public InvestmentType getInvestmentType() {
        return investmentType;
    }

    /**
     * Sets The investmentType.
     * 
     * @param investmentType
     *            The investmentType to set.
     */
    public void setInvestmentType(final InvestmentType investmentType) {
        this.investmentType = investmentType;
    }

    /**
     * Gets the channelType.
     * 
     * @return Returns the channelType.
     */
    public ChannelType getChannelType() {
        return channelType;
    }

    /**
     * Sets The channelType.
     * 
     * @param channelType
     *            The channelType to set.
     */
    public void setChannelType(final ChannelType channelType) {
        this.channelType = channelType;
    }

    /**
     * Gets the coverType.
     * 
     * @return Returns the coverType.
     */
    public CoverType getCoverType() {
        return coverType;
    }

    /**
     * Sets The coverType.
     * 
     * @param coverType
     *            The coverType to set.
     */
    public void setCoverType(final CoverType coverType) {
        this.coverType = coverType;
    }

    /**
     * Gets the childProducts.
     * 
     * @return Returns the childProducts.
     */
    public Set<ProductSpecification> getChildProducts() {
        return childProducts;
    }

    /**
     * Sets The childProducts.
     * 
     * @param childProducts
     *            The childProducts to set.
     */
    public void setChildProducts(final Set<ProductSpecification> childProducts) {
        this.childProducts = childProducts;
    }

    /**
     * Gets the calculations.
     * 
     * @return Returns the calculations.
     */
    public Set<CalculationSpecification> getCalculations() {
        return calculations;
    }

    /**
     * Sets The calculations.
     * 
     * @param calculations
     *            The calculations to set.
     */
    public void setCalculations(final Set<CalculationSpecification> calculations) {
        this.calculations = calculations;
    }

    /**
     * Gets the properties.
     * 
     * @return Returns the properties.
     */
    public Set<PropertySpecification> getProperties() {
        return properties;
    }

    /**
     * Sets The properties.
     * 
     * @param properties
     *            The properties to set.
     */
    public void setProperties(final Set<PropertySpecification> properties) {
        this.properties = properties;
    }

    /**
     * Gets the rules.
     * 
     * @return Returns the rules.
     */
    public Set<RuleSpecification> getRules() {
        return rules;
    }

    /**
     * Sets The rules.
     * 
     * @param rules
     *            The rules to set.
     */
    public void setRules(final Set<RuleSpecification> rules) {
        this.rules = rules;
    }

    /**
     * Gets the roles.
     * 
     * @return Returns the roles.
     */
    public Set<RoleSpecification> getRoles() {
        return roles;
    }

    /**
     * Sets The roles.
     * 
     * @param roles
     *            The roles to set.
     */
    public void setRoles(final Set<RoleSpecification> roles) {
        this.roles = roles;
    }

    /**
     * Gets the requests.
     * 
     * @return Returns the requests.
     */
    public Set<RequestSpecification> getRequests() {
        return requests;
    }

    /**
     * Sets The requests.
     * 
     * @param requests
     *            The requests to set.
     */
    public void setRequests(final Set<RequestSpecification> requests) {
        this.requests = requests;
    }

    /**
     * Gets the category.
     * 
     * @return Returns the category.
     */
    public CategoryType getCategory() {
        return category;
    }

    /**
     * Sets The category.
     * 
     * @param category
     *            The category to set.
     */
    public void setCategory(CategoryType category) {
        this.category = category;
    }

    /**
     * Sets The autoIncludeAtIssue.
     * 
     * @param autoIncludeAtIssue
     *            The autoIncludeAtIssue to set.
     */
    public void setAutoIncludeAtIssue(Boolean autoIncludeAtIssue) {
        this.autoIncludeAtIssue = autoIncludeAtIssue;
    }

    /**
     * Gets the autoIncludeAtIssue.
     * 
     * @return Returns the autoIncludeAtIssue.
     */
    public Boolean getAutoIncludeAtIssue() {
        return autoIncludeAtIssue;
    }

    /**
     * Sets The carrier.
     * 
     * @param carrier
     *            the carrier to set.
     */
    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    /**
     * Gets the carrier.
     * 
     * @return Returns the carrier.
     */
    public Carrier getCarrier() {
        return carrier;
    }

    /**
     * Gets the pushSellIndicator.
     * 
     * @return Returns the pushSellIndicator.
     */
    public Boolean getPushSellIndicator() {
        return pushSellIndicator;
    }

    /**
     * Sets The pushSellIndicator.
     * 
     * @param pushSellIndicator
     *            The pushSellIndicator to set.
     */
    public void setPushSellIndicator(Boolean pushSellIndicator) {
        this.pushSellIndicator = pushSellIndicator;
    }

    /**
     * Gets the productWeightage.
     * 
     * @return Returns the productWeightage.
     */
    public BigDecimal getProductWeightage() {
        return productWeightage;
    }

    /**
     * Sets The productWeightage.
     * 
     * @param productWeightage
     *            The productWeightage to set.
     */
    public void setProductWeightage(BigDecimal productWeightage) {
        this.productWeightage = productWeightage;
    }

    /**
     * Gets the prodCollaterals.
     * 
     * @return the prodCollaterals
     */
    public Set<ProductCollaterals> getProdCollaterals() {
        return prodCollaterals;
    }

    /**
     * Sets the prodCollaterals.
     * 
     * @param prodCollaterals
     *            the prodCollaterals to set
     */
    public void setProdCollaterals(Set<ProductCollaterals> prodCollaterals) {
        this.prodCollaterals = prodCollaterals;
    }

    /**
     * Gets the prodTemplatesMapping.
     * 
     * @return the prodTemplatesMapping
     */
    public Set<ProductTemplateMapping> getProdTemplatesMapping() {
        return prodTemplatesMapping;
    }

    /**
     * Sets the prodTemplatesMapping.
     * 
     * @param prodTemplatesMapping
     *            the prodTemplatesMapping to set
     */
    public void setProdTemplatesMapping(Set<ProductTemplateMapping> prodTemplatesMapping) {
        this.prodTemplatesMapping = prodTemplatesMapping;
    }

    /**
     * Sets the parent product.
     * 
     * @param parentProduct
     *            the parentProduct to set
     */
    public void setParentProduct(ProductSpecification parentProduct) {
        this.parentProduct = parentProduct;
    }

    /**
     * Gets the parent product.
     * 
     * @return the parentProduct
     */
    public ProductSpecification getParentProduct() {
        return parentProduct;
    }

    /**
     * Gets the product channels.
     * 
     * @return the productChannels
     */
    public final Set<ProductChannel> getProductChannels() {
        return productChannels;
    }

    /**
     * Sets the product channels.
     * 
     * @param productChannels
     *            the productChannels to set
     */
    public final void setProductChannels(Set<ProductChannel> productChannels) {
        this.productChannels = productChannels;
    }

    /**
     * Gets the product fund mappings.
     * 
     * @return the productFundMappings
     */
    public final Set<ProductFundMapping> getProductFundMappings() {
        return productFundMappings;
    }

    /**
     * Sets the product fund mappings.
     * 
     * @param productFundMappings
     *            the productFundMappings to set
     */
    public final void setProductFundMappings(Set<ProductFundMapping> productFundMappings) {
        this.productFundMappings = productFundMappings;
    }

    
   

}
