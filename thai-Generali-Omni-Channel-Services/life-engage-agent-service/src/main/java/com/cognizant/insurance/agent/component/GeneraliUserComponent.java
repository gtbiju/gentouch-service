package com.cognizant.insurance.agent.component;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.dao.exception.InputValidationException;
import com.cognizant.insurance.agent.dao.exception.RecentPasswordUsageException;
import com.cognizant.insurance.agent.request.vo.RequestInfo;


public interface GeneraliUserComponent {

    public String register(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException;	

	public String createPassword(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException;	

	public String forgetPassword(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException;
	
	public String retrieveLeads(RequestInfo requestInfo,JSONArray jsonRqArray)throws InputValidationException;
	
	public String resetPassword(RequestInfo requestInfo, JSONObject jsonObject) throws BusinessException, RecentPasswordUsageException;	
	
}
