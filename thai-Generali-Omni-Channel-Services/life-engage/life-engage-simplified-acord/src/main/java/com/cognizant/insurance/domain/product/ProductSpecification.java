/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.domain.product;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


// TODO: Auto-generated Javadoc
/**
 * The Class ProductSpecification.
 * 
 * @author 304007,420118
 */

@Entity
@NamedQueries({
    @NamedQuery(name = "ProductSpecification.findByCarrierCode", query = "select prodSpecification from ProductSpecification prodSpecification where prodSpecification.carrier.id = :carrierId and prodSpecification.productType.id = 812")
    ,@NamedQuery(name = "ProductSpecification.findActiveProducts", query = "select prodSpecification from ProductSpecification prodSpecification where prodSpecification.carrier.id = :carrierId and prodSpecification.suspensionDate >= :productSuspensionDate and withdrawalDate >= :productWithdrawalDate and terminationDate >= :productTerminationDate and marketableIndicator = 1" )
    ,@NamedQuery(name = "productSpecification.getProductById", query = "select p from ProductSpecification p where p.id =:productId order by p.id ASC")
    ,@NamedQuery(name = "productSpecification.getProductsByIds", query = "select p from ProductSpecification p where p.id in :productId order by p.id ASC")
    ,@NamedQuery(name = "productSpecification.getProductsByProductIds", query = "select p from ProductSpecification p where p.productId in :productId")
    ,@NamedQuery(name = "productSpecification.getProduct", query = "select p from ProductSpecification p where p.productCode =:productCode and p.carrier.id =:carrierCode order by p.id ASC ")
    ,@NamedQuery(name = "ProductSpecification.findActiveProductByID", query = "select p from ProductSpecification p where p.carrier.id = :carrierId and p.suspensionDate >= :productSuspensionDate and p.withdrawalDate >= :productWithdrawalDate and p.terminationDate >= :productTerminationDate and p.marketableIndicator = 1 and p.id =:productId and p.productType.id = 812" )
    ,@NamedQuery(name= "ProductSpecification.findActiveProductsByFilter", query = "select prodSpecification from ProductSpecification prodSpecification join prodSpecification.productChannels channel where prodSpecification.carrier.id = :carrierId and prodSpecification.suspensionDate >= :productSuspensionDate and prodSpecification.withdrawalDate >= :productWithdrawalDate and prodSpecification.terminationDate >= :productTerminationDate and prodSpecification.marketableIndicator = 1 and channel.channelType.id = :channelId" )
    ,@NamedQuery(name= "ProductSpecification.findAllProductsByFilter", query = "select prodSpecification from ProductSpecification prodSpecification join prodSpecification.productChannels channel where prodSpecification.carrier.id = :carrierId and prodSpecification.productType.id = 812 and channel.channelType.id = :channelId")
    ,@NamedQuery(name= "ProductSpecification.getProductTemplatesByFilter", query ="SELECT templateMapping FROM ProductTemplateMapping templateMapping  WHERE templateMapping.productSpecification.id = :productId and templateMapping.productSpecification.carrier.id = :carrierId and ((:templateCountry is not null and templateMapping.productTemplates.countryCode = :templateCountry)or (:templateCountry is null )) and ((:templateType is not null and templateMapping.templateType.id = :templateType)or (:templateType is null )) and ((:templateLanguage is not null and templateMapping.productTemplates.language = :templateLanguage)or (:templateLanguage is null )) ")
    ,@NamedQuery(name = "ProductSpecification.retrieveComponetsByFilter", query = "select p from ProductSpecification p where p.productCode =:code and p.version = :version")
    ,@NamedQuery(name = "ProductSpecification.retrieveComponetsByIDFilter", query = "select p from ProductSpecification p where p.productCode =:code and p.version = :version and p.parentProduct.id in :parentProdIdList")
    ,@NamedQuery(name = "ProductSpecification.retrieveProductsByCode", query = "select prod from ProductSpecification prod where prod.productCode = :productCode and prod.productType.description = :productType group by prod.productCode,prod.version order by prod.versionDate desc")
    ,@NamedQuery(name = "ProductSpecification.retrieveProductsByStatus", query = "select prod from ProductSpecification prod where prod.versionStatus in :versionStatusList and prod.productType.description = :productType group by prod.productCode,prod.version order by prod.versionDate desc")
    ,@NamedQuery(name = "ProductSpecification.retrieveHigherVersion", query = "select prod.version from ProductSpecification prod where prod.productCode = :productCode")
    ,@NamedQuery(name= "ProductSpecification.V2findActiveProductsByFilter", query = "select prodSpecification from ProductSpecification prodSpecification where prodSpecification.carrier.id = :carrierId and prodSpecification.suspensionDate >= :productSuspensionDate and prodSpecification.withdrawalDate >= :productWithdrawalDate and prodSpecification.terminationDate >= :productTerminationDate and prodSpecification.marketableIndicator = 1 and prodSpecification.channelType.id = :channelId and (:lob is null or  prodSpecification.lineOfBusiness.id = :lob) and (:jurisdictionCode is null or prodSpecification.jurisdictionCode.jurisdictionId.code = :jurisdictionCode) and (:version is null or prodSpecification.version = :version) and (:subLob is null or prodSpecification.lobSubCode.id = :subLob) and (:broadLob is null or prodSpecification.broadLobType.id = :broadLob) and prodSpecification.versionStatus='Approved'" )
    ,@NamedQuery(name= "ProductSpecification.V2findAllProductsByFilter", query = "select prodSpecification from ProductSpecification prodSpecification where prodSpecification.carrier.id = :carrierId and prodSpecification.productType.id = '812' and prodSpecification.channelType.id = :channelId and (:lob is null or  prodSpecification.lineOfBusiness.id = :lob) and (:jurisdictionCode is null or prodSpecification.jurisdictionCode.jurisdictionId.code = :jurisdictionCode) and (:version is null or prodSpecification.version = :version) and (:subLob is null or prodSpecification.lobSubCode.id = :subLob) and (:broadLob is null or prodSpecification.broadLobType.id = :broadLob) and prodSpecification.versionStatus='Approved'")
    ,@NamedQuery(name = "ProductSpecification.V2findActiveProductByID", query = "select p from ProductSpecification p where p.carrier.id = :carrierId and p.suspensionDate >= :productSuspensionDate and p.withdrawalDate >= :productWithdrawalDate and p.terminationDate >= :productTerminationDate and p.marketableIndicator = 1 and p.productId =:productId and p.productType.id = 812 and (:jurisdictionCode is null or p.jurisdictionCode.jurisdictionId.code = :jurisdictionCode) and (:channelId  is null or p.channelType.id = :channelId) and p.versionStatus='Approved'" )
    ,@NamedQuery(name = "productSpecification.V2getProduct", query = "select p from ProductSpecification p where p.productCode =:productCode and p.carrier.id =:carrierCode and (:jurisdictionCode is null or p.jurisdictionCode.jurisdictionId.code = :jurisdictionCode) and (:channelId is null or p.channelType.id = :channelId) and p.versionStatus='Approved' order by p.id ASC ")
    ,@NamedQuery(name = "productSpecification.V2getProductById", query = "select p from ProductSpecification p where p.productId =:prodId and (:jurisdictionCode is null or p.jurisdictionCode.jurisdictionId.code = :jurisdictionCode) and (:channelId is null or p.channelType.id = :channelId) and p.versionStatus='Approved' order by p.id ASC")
    ,@NamedQuery(name = "productSpecification.V2getProductsByIds", query = "select p from ProductSpecification p where p.id in :productId and (:jurisdictionCode is null or p.jurisdictionCode.jurisdictionId.code = :jurisdictionCode) and (:channelId is null or p.channelType.id = :channelId) and p.versionStatus='Approved' order by p.id ASC")
    })
@Table(name = "CORE_PRODUCT_COMPONENT")
public class ProductSpecification extends Component {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7136100978621736164L;
    
    /** The Constant DATETIMEDEFAULTNULL. */
    private static final String DATETIMEDEFAULTNULL = "datetime DEFAULT NULL";

    /** The name. */
    @Column(name = "NAME")
    private String name;
    
    /** The localname. */
    @Column(name = "LOCAL_NAME")
    private String localName;
    
    /** The business description. */
    @Column(name = "BUSINESS_DESCRIPTION", columnDefinition = "varchar(max)")
    private String businessDescription;

    /** The formal definition. */
    @Column(name = "FORMAL_DEFINITION", columnDefinition = "varchar(max)")
    private String formalDefinition;

    /** The type. imp */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "productType_ID")
    private ProductType productType;

    /** The version date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "VERSION_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date versionDate;

    /** The carrier. imp */
    @ManyToOne(cascade = { CascadeType.REFRESH })
    private Carrier carrier;

    /** The investment type. */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "investmentType_ID")
    private InvestmentType investmentType;

    /** The cover type. */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "coverType_ID")
    private CoverType coverType;

    /** The plan code. */
    @Column(name = "PLAN_CODE")
    private String planCode;

    /** The plan name. */
    @Column(name = "PLAN_NAME")
    private String planName;

    /** The product group. */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "PRODUCT_GROUP")
    private ProductGroupType productGroup;

    /** The auto include at issue. */
    @Column(name = "AUTO_INCLUDE_AT_ISSUE")
    private Boolean autoIncludeAtIssue;

    /** The short name. */
    @Column(name = "SHORT_NAME")
    private String shortName;

    /** The marketable name. */
    @Column(name = "MARKETABLE_NAME")
    private String marketableName;

    /** The family. imp */

    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "category_ID")
    private CategoryType category;

    /** The line of business. */

    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "lineOfBusiness_ID")
    private LobType lineOfBusiness;

    /** The marketable indicator. */
    @Column(name = "MARKETABLE_INDICATOR")
    private Boolean marketableIndicator;

    /** The launch date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "LAUNCH_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date launchDate;

    /** The suspension date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "SUSPENSION_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date suspensionDate;

    /** The termination date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "TERMINATION_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date terminationDate;

    /** The withdrawal date. */
    @Temporal(TemporalType.DATE)
    @Column(name = "WITHDRAWAL_DATE", columnDefinition = DATETIMEDEFAULTNULL)
    private Date withdrawalDate;

    /** The product id. */
    @Column(name = "PRODUCT_ID")
    private Integer productId;
    
    /** The parent product. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    @JoinColumn(name = "parentProduct_id")
    private ProductSpecification parentProduct;

    /** The child products. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE }, fetch = FetchType.LAZY, mappedBy = "parentProduct", orphanRemoval= true)
    private Set<ProductSpecification> childProducts;

    /** The child products. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "productSpecification")
    private Set<ProductChannel> productChannels;

    /** The collaterals. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "productId")
    private Set<ProductCollaterals> prodCollaterals;

    /** The templates. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "productSpecification")
    private Set<ProductTemplateMapping> prodTemplatesMapping;

    /** The calculations. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "parentProductId")
    private Set<CalculationSpecification> calculations;

    /** The properties. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL },fetch = FetchType.LAZY, mappedBy = "parentProduct")
    private Set<PropertySpecification> properties;

    /** The rules. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "parentProduct")
    private Set<RuleSpecification> rules;

    /** The roles. */
    // @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @Transient
    private Set<RoleSpecification> roles;

    /** The requests. */
    // @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @Transient
    private Set<RequestSpecification> requests;

    /** The marketable indicator. */
    @Column(name = "PUSH_SELL_INDICATOR")
    private Boolean pushSellIndicator;

    /** The marketable indicator. */
    @Column(name = "WEIGHTAGE")
    private BigDecimal productWeightage;

    /** The child products. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "productSpecification")
    private Set<ProductFundMapping> productFundMappings;
   
    
    /** The distribution channel name. */
    @Column(name = "DIST_CHANNEL_NAME")
    private String distributionChannelName;
    
    /** The distribution id. */
    @Column(name = "DIST_ID")
    private String distributionId;
    
    
    /** The comp sub types. */
    @ManyToOne(cascade = {  CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "COMP_SUBTYPES")
    private ComponentSubTypes compSubTypes;
    
    
    /** The broad lob type. */
    @ManyToOne(cascade = {  CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "BROAD_LOB_CODE")
    private BroadLobType broadLobType;
    
    /** The lob sub code. */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "LOB_SUB_CODE")
    private LobSubType lobSubCode;
    
    /** The effective date. */
    @Column(name = "EFFECTIVE_DATE")
    private Date effectiveDate;
    
    /** The expiration date. */
    @Column(name = "EXPIRATION_DATE")
    private Date expirationDate;
    
    /** The created by. */
    @Column(name= "CREATED_BY")
    private String createdBy;
    
    /** The comment. */
    @Column(name= "COMMENT")
    private String comment;
    
    /** The basic data complete code. */
    @Column(name= "basicDataCompleteCode")
    private Integer basicDataCompleteCode;
    
    /** The context id. */
    @Column(name= "contextId")
    private Integer contextId;
    
    /** The creation date time. */
    @Column(name= "creationDateTime")
    private Date creationDateTime;
    
    /** The modified date time. */
    @Column(name= "modifiedDateTime")
    private Date modifiedDateTime;
    
    /** The trans tracking id. */
    @Column(name= "transTrackingId")
    private String transTrackingId;
    
    /** The transaction id. */
    @Column(name= "transactionId")
    private String transactionId;
    
    /** The type name. */
    @Column(name= "typeName")
    private String typeName;
    
    /** The transaction keys_id. */
    @Column(name= "transactionKeys_id")
    private Long transactionKeys_id;
    
    /**
     * The Constructor.
     */
    public ProductSpecification() {
        super();
    }


    /**
     * Gets the short name.
     * 
     * @return the short name
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * Sets the short name.
     * 
     * @param shortName
     *            the short name
     */
    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }

    /**
     * Gets the marketable name.
     * 
     * @return the marketable name
     */
    public String getMarketableName() {
        return marketableName;
    }

    /**
     * Sets the marketable name.
     * 
     * @param marketableName
     *            the marketable name
     */
    public void setMarketableName(final String marketableName) {
        this.marketableName = marketableName;
    }

    /**
     * Sets The localName.
     *
     * @param localName The localName to set.
     */
    public void setLocalName(String localName) {
        this.localName = localName;
    }

    /**
     * Gets the localName.
     *
     * @return Returns the localName.
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * Gets the line of business.
     * 
     * @return the line of business
     */
    public LobType getLineOfBusiness() {
        return lineOfBusiness;
    }

    /**
     * Sets the line of business.
     * 
     * @param lineOfBusiness
     *            the line of business
     */
    public void setLineOfBusiness(final LobType lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }

    /**
     * Gets the marketable indicator.
     * 
     * @return the marketable indicator
     */
    public Boolean getMarketableIndicator() {
        return marketableIndicator;
    }

    /**
     * Sets the marketable indicator.
     * 
     * @param marketableIndicator
     *            the marketable indicator
     */
    public void setMarketableIndicator(final Boolean marketableIndicator) {
        this.marketableIndicator = marketableIndicator;
    }

    /**
     * Gets the launch date.
     * 
     * @return the launch date
     */
    public Date getLaunchDate() {
        return launchDate;
    }

    /**
     * Sets the launch date.
     * 
     * @param launchDate
     *            the launch date
     */
    public void setLaunchDate(final Date launchDate) {
        this.launchDate = launchDate;
    }

    /**
     * Gets the suspension date.
     * 
     * @return the suspension date
     */
    public Date getSuspensionDate() {
        return suspensionDate;
    }

    /**
     * Sets the suspension date.
     * 
     * @param suspensionDate
     *            the suspension date
     */
    public void setSuspensionDate(final Date suspensionDate) {
        this.suspensionDate = suspensionDate;
    }

    /**
     * Gets the termination date.
     * 
     * @return the termination date
     */
    public Date getTerminationDate() {
        return terminationDate;
    }

    /**
     * Sets the termination date.
     * 
     * @param terminationDate
     *            the termination date
     */
    public void setTerminationDate(final Date terminationDate) {
        this.terminationDate = terminationDate;
    }

    /**
     * Gets the withdrawal date.
     * 
     * @return the withdrawal date
     */
    public Date getWithdrawalDate() {
        return withdrawalDate;
    }

    /**
     * Sets the withdrawal date.
     * 
     * @param withdrawalDate
     *            the withdrawal date
     */
    public void setWithdrawalDate(final Date withdrawalDate) {
        this.withdrawalDate = withdrawalDate;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }
    
    /**
     * Gets the businessDescription.
     * 
     * @return Returns the businessDescription.
     */
    public String getBusinessDescription() {
        return businessDescription;
    }

    /**
     * Sets The businessDescription.
     * 
     * @param businessDescription
     *            The businessDescription to set.
     */
    public void setBusinessDescription(final String businessDescription) {
        this.businessDescription = businessDescription;
    }

    /**
     * Gets the formalDefinition.
     * 
     * @return Returns the formalDefinition.
     */
    public String getFormalDefinition() {
        return formalDefinition;
    }

    /**
     * Sets The formalDefinition.
     * 
     * @param formalDefinition
     *            The formalDefinition to set.
     */
    public void setFormalDefinition(final String formalDefinition) {
        this.formalDefinition = formalDefinition;
    }
    
    /**
     * Gets the versionDate.
     * 
     * @return Returns the versionDate.
     */
    public Date getVersionDate() {
        return versionDate;
    }

    /**
     * Sets The versionDate.
     * 
     * @param versionDate
     *            The versionDate to set.
     */
    public void setVersionDate(final Date versionDate) {
        this.versionDate = versionDate;
    }

    /**
     * Gets the planCode.
     * 
     * @return Returns the planCode.
     */
    public String getPlanCode() {
        return planCode;
    }

    /**
     * Sets The planCode.
     * 
     * @param planCode
     *            The planCode to set.
     */
    public void setPlanCode(final String planCode) {
        this.planCode = planCode;
    }

    /**
     * Gets the planName.
     * 
     * @return Returns the planName.
     */
    public String getPlanName() {
        return planName;
    }

    /**
     * Sets The planName.
     * 
     * @param planName
     *            The planName to set.
     */
    public void setPlanName(final String planName) {
        this.planName = planName;
    }

    /**
     * Gets the productGroup.
     * 
     * @return Returns the productGroup.
     */
    public ProductGroupType getProductGroup() {
        return productGroup;
    }

    /**
     * Sets The productGroup.
     * 
     * @param productGroup
     *            The productGroup to set.
     */
    public void setProductGroup(final ProductGroupType productGroup) {
        this.productGroup = productGroup;
    }

    /**
     * Gets the productType.
     * 
     * @return Returns the productType.
     */
    public ProductType getProductType() {
        return productType;
    }

    /**
     * Sets The productType.
     * 
     * @param productType
     *            The productType to set.
     */
    public void setProductType(final ProductType productType) {
        this.productType = productType;
    }

    /**
     * Gets the investmentType.
     * 
     * @return Returns the investmentType.
     */
    public InvestmentType getInvestmentType() {
        return investmentType;
    }

    /**
     * Sets The investmentType.
     * 
     * @param investmentType
     *            The investmentType to set.
     */
    public void setInvestmentType(final InvestmentType investmentType) {
        this.investmentType = investmentType;
    }

    /**
     * Gets the coverType.
     * 
     * @return Returns the coverType.
     */
    public CoverType getCoverType() {
        return coverType;
    }

    /**
     * Sets The coverType.
     * 
     * @param coverType
     *            The coverType to set.
     */
    public void setCoverType(final CoverType coverType) {
        this.coverType = coverType;
    }

    /**
     * Gets the childProducts.
     * 
     * @return Returns the childProducts.
     */
    public Set<ProductSpecification> getChildProducts() {
        return childProducts;
    }

    /**
     * Sets The childProducts.
     * 
     * @param childProducts
     *            The childProducts to set.
     */
    public void setChildProducts(final Set<ProductSpecification> childProducts) {
        this.childProducts = childProducts;
    }

    /**
     * Gets the calculations.
     * 
     * @return Returns the calculations.
     */
    public Set<CalculationSpecification> getCalculations() {
        return calculations;
    }

    /**
     * Sets The calculations.
     * 
     * @param calculations
     *            The calculations to set.
     */
    public void setCalculations(final Set<CalculationSpecification> calculations) {
        this.calculations = calculations;
    }

    /**
     * Gets the properties.
     * 
     * @return Returns the properties.
     */
    public Set<PropertySpecification> getProperties() {
        return properties;
    }

    /**
     * Sets The properties.
     * 
     * @param properties
     *            The properties to set.
     */
    public void setProperties(final Set<PropertySpecification> properties) {
        this.properties = properties;
    }

    /**
     * Gets the rules.
     * 
     * @return Returns the rules.
     */
    public Set<RuleSpecification> getRules() {
        return rules;
    }

    /**
     * Sets The rules.
     * 
     * @param rules
     *            The rules to set.
     */
    public void setRules(final Set<RuleSpecification> rules) {
        this.rules = rules;
    }

    /**
     * Gets the roles.
     * 
     * @return Returns the roles.
     */
    public Set<RoleSpecification> getRoles() {
        return roles;
    }

    /**
     * Sets The roles.
     * 
     * @param roles
     *            The roles to set.
     */
    public void setRoles(final Set<RoleSpecification> roles) {
        this.roles = roles;
    }

    /**
     * Gets the requests.
     * 
     * @return Returns the requests.
     */
    public Set<RequestSpecification> getRequests() {
        return requests;
    }

    /**
     * Sets The requests.
     * 
     * @param requests
     *            The requests to set.
     */
    public void setRequests(final Set<RequestSpecification> requests) {
        this.requests = requests;
    }

    /**
     * Gets the category.
     * 
     * @return Returns the category.
     */
    public CategoryType getCategory() {
        return category;
    }

    /**
     * Sets The category.
     * 
     * @param category
     *            The category to set.
     */
    public void setCategory(CategoryType category) {
        this.category = category;
    }

    /**
     * Sets The autoIncludeAtIssue.
     * 
     * @param autoIncludeAtIssue
     *            The autoIncludeAtIssue to set.
     */
    public void setAutoIncludeAtIssue(Boolean autoIncludeAtIssue) {
        this.autoIncludeAtIssue = autoIncludeAtIssue;
    }

    /**
     * Gets the autoIncludeAtIssue.
     * 
     * @return Returns the autoIncludeAtIssue.
     */
    public Boolean getAutoIncludeAtIssue() {
        return autoIncludeAtIssue;
    }

    /**
     * Sets The carrier.
     * 
     * @param carrier
     *            the carrier to set.
     */
    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    /**
     * Gets the carrier.
     * 
     * @return Returns the carrier.
     */
    public Carrier getCarrier() {
        return carrier;
    }

    /**
     * Gets the pushSellIndicator.
     * 
     * @return Returns the pushSellIndicator.
     */
    public Boolean getPushSellIndicator() {
        return pushSellIndicator;
    }

    /**
     * Sets The pushSellIndicator.
     * 
     * @param pushSellIndicator
     *            The pushSellIndicator to set.
     */
    public void setPushSellIndicator(Boolean pushSellIndicator) {
        this.pushSellIndicator = pushSellIndicator;
    }

    /**
     * Gets the productWeightage.
     * 
     * @return Returns the productWeightage.
     */
    public BigDecimal getProductWeightage() {
        return productWeightage;
    }

    /**
     * Sets The productWeightage.
     * 
     * @param productWeightage
     *            The productWeightage to set.
     */
    public void setProductWeightage(BigDecimal productWeightage) {
        this.productWeightage = productWeightage;
    }

    /**
     * Gets the prodCollaterals.
     * 
     * @return the prodCollaterals
     */
    public Set<ProductCollaterals> getProdCollaterals() {
        return prodCollaterals;
    }

    /**
     * Sets the prodCollaterals.
     * 
     * @param prodCollaterals
     *            the prodCollaterals to set
     */
    public void setProdCollaterals(Set<ProductCollaterals> prodCollaterals) {
        this.prodCollaterals = prodCollaterals;
    }

    /**
     * Gets the prodTemplatesMapping.
     * 
     * @return the prodTemplatesMapping
     */
    public Set<ProductTemplateMapping> getProdTemplatesMapping() {
        return prodTemplatesMapping;
    }

    /**
     * Sets the prodTemplatesMapping.
     * 
     * @param prodTemplatesMapping
     *            the prodTemplatesMapping to set
     */
    public void setProdTemplatesMapping(Set<ProductTemplateMapping> prodTemplatesMapping) {
        this.prodTemplatesMapping = prodTemplatesMapping;
    }

    /**
     * Sets the parent product.
     * 
     * @param parentProduct
     *            the parentProduct to set
     */
    public void setParentProduct(ProductSpecification parentProduct) {
        this.parentProduct = parentProduct;
    }

    /**
     * Gets the parent product.
     * 
     * @return the parentProduct
     */
    public ProductSpecification getParentProduct() {
        return parentProduct;
    }

    /**
     * Gets the product channels.
     * 
     * @return the productChannels
     */
    public final Set<ProductChannel> getProductChannels() {
        return productChannels;
    }

    /**
     * Sets the product channels.
     * 
     * @param productChannels
     *            the productChannels to set
     */
    public final void setProductChannels(Set<ProductChannel> productChannels) {
        this.productChannels = productChannels;
    }

    /**
     * Gets the product fund mappings.
     * 
     * @return the productFundMappings
     */
    public final Set<ProductFundMapping> getProductFundMappings() {
        return productFundMappings;
    }

    /**
     * Sets the product fund mappings.
     * 
     * @param productFundMappings
     *            the productFundMappings to set
     */
    public final void setProductFundMappings(Set<ProductFundMapping> productFundMappings) {
        this.productFundMappings = productFundMappings;
    }


	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public Integer getProductId() {
		return productId;
	}


	/**
	 * Sets the product id.
	 *
	 * @param productId the new product id
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}



	/**
	 * Gets the distribution channel name.
	 *
	 * @return the distribution channel name
	 */
	public String getDistributionChannelName() {
		return distributionChannelName;
	}


	/**
	 * Sets the distribution channel name.
	 *
	 * @param distributionChannelName the new distribution channel name
	 */
	public void setDistributionChannelName(String distributionChannelName) {
		this.distributionChannelName = distributionChannelName;
	}


	/**
	 * Gets the distribution id.
	 *
	 * @return the distribution id
	 */
	public String getDistributionId() {
		return distributionId;
	}


	/**
	 * Sets the distribution id.
	 *
	 * @param distributionId the new distribution id
	 */
	public void setDistributionId(String distributionId) {
		this.distributionId = distributionId;
	}


	/**
	 * Gets the comp sub types.
	 *
	 * @return the comp sub types
	 */
	public ComponentSubTypes getCompSubTypes() {
		return compSubTypes;
	}


	/**
	 * Sets the comp sub types.
	 *
	 * @param compSubTypes the new comp sub types
	 */
	public void setCompSubTypes(ComponentSubTypes compSubTypes) {
		this.compSubTypes = compSubTypes;
	}


	/**
	 * Gets the broad lob type.
	 *
	 * @return the broad lob type
	 */
	public BroadLobType getBroadLobType() {
		return broadLobType;
	}


	/**
	 * Sets the broad lob type.
	 *
	 * @param broadLobType the new broad lob type
	 */
	public void setBroadLobType(BroadLobType broadLobType) {
		this.broadLobType = broadLobType;
	}


	/**
	 * Gets the lob sub code.
	 *
	 * @return the lob sub code
	 */
	public LobSubType getLobSubCode() {
		return lobSubCode;
	}


	/**
	 * Sets the lob sub code.
	 *
	 * @param lobSubCode the new lob sub code
	 */
	public void setLobSubCode(LobSubType lobSubCode) {
		this.lobSubCode = lobSubCode;
	}


	/**
	 * Gets the effective date.
	 *
	 * @return the effective date
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}


	/**
	 * Sets the effective date.
	 *
	 * @param effectiveDate the new effective date
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}


	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}


	/**
	 * Gets the created by.
	 *
	 * @return the created by
	 */
	public String getCreatedBy() {
		return createdBy;
	}


	/**
	 * Sets the created by.
	 *
	 * @param createdBy the new created by
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	/**
	 * Gets the comment.
	 *
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}


	/**
	 * Sets the comment.
	 *
	 * @param comment the comment to set.
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}


    /**
     * Gets the basic data complete code.
     *
     * @return the basic data complete code
     */
    public Integer getBasicDataCompleteCode() {
        return basicDataCompleteCode;
    }


    /**
     * Sets the basic data complete code.
     *
     * @param basicDataCompleteCode the basic data complete code to set.
     */
    public void setBasicDataCompleteCode(Integer basicDataCompleteCode) {
        this.basicDataCompleteCode = basicDataCompleteCode;
    }


    /**
     * Gets the context id.
     *
     * @return the context id
     */
    public Integer getContextId() {
        return contextId;
    }


    /**
     * Sets the context id.
     *
     * @param contextId the context id to set.
     */
    public void setContextId(Integer contextId) {
        this.contextId = contextId;
    }


    /**
     * Gets the creation date time.
     *
     * @return the creation date time
     */
    public Date getCreationDateTime() {
        return creationDateTime;
    }


    /**
     * Sets the creation date time.
     *
     * @param creationDateTime the creation date time to set.
     */
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }


    /**
     * Gets the modified date time.
     *
     * @return the modified date time
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }


    /**
     * Sets the modified date time.
     *
     * @param modifiedDateTime the modified date time to set.
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }


    /**
     * Gets the trans tracking id.
     *
     * @return the trans tracking id
     */
    public String getTransTrackingId() {
        return transTrackingId;
    }


    /**
     * Sets the trans tracking id.
     *
     * @param transTrackingId the trans tracking id to set.
     */
    public void setTransTrackingId(String transTrackingId) {
        this.transTrackingId = transTrackingId;
    }


    /**
     * Gets the transaction id.
     *
     * @return the transaction id
     */
    public String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transaction id.
     *
     * @param transactionId the transaction id to set.
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the type name.
     *
     * @return the type name
     */
    public String getTypeName() {
        return typeName;
    }


    /**
     * Sets the type name.
     *
     * @param typeName the type name to set.
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


    /**
     * Gets the transaction keys_id.
     *
     * @return the transaction keys_id
     */
    public Long getTransactionKeys_id() {
        return transactionKeys_id;
    }


    /**
     * Sets the transaction keys_id.
     *
     * @param transactionKeys_id the transaction keys_id to set.
     */
    public void setTransactionKeys_id(Long transactionKeys_id) {
        this.transactionKeys_id = transactionKeys_id;
    }
   

}
