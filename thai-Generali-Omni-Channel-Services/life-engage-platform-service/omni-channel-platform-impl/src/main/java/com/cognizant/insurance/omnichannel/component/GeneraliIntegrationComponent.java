package com.cognizant.insurance.omnichannel.component;

import com.cognizant.insurance.request.vo.Transactions;

public interface GeneraliIntegrationComponent {
	
	public Transactions sentPolicyDetails(Transactions transactions);

}
