/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 325754
 * @version       : 0.1, Oct 30, 2015
 */
package com.cognizant.insurance.domain.documentandcommunication;

import javax.persistence.Entity;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;

/**
 * @author 325754
 * 
 */
@Entity
public class DocumentOption extends InformationModelObject {

    /**
     * 
     */
    private static final long serialVersionUID = 5902947087624648389L;

    private String optionTypeCode;

    /**
     * @return the optionTypeCode
     */
    public String getOptionTypeCode() {
        return optionTypeCode;
    }

    /**
     * @param optionTypeCode
     *            the optionTypeCode to set
     */
    public void setOptionTypeCode(String optionTypeCode) {
        this.optionTypeCode = optionTypeCode;
    }

}
