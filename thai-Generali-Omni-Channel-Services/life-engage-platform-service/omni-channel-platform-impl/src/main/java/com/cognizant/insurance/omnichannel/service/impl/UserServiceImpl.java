package com.cognizant.insurance.omnichannel.service.impl;

import java.text.ParseException;
import java.util.NoSuchElementException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.omnichannel.component.GeneraliUserComponent;
import com.cognizant.insurance.component.LifeEngageAuditComponent;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.InputValidationException;
import com.cognizant.insurance.core.exception.RecentPasswordUsageException;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.omnichannel.service.UserService;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;

@Component
public class UserServiceImpl implements UserService {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
    private LifeEngageAuditComponent lifeEngageAuditComponent;
	
	@Autowired
    private GeneraliUserComponent generaliUserComponent;
	
	@Override
	public String register(String inputJSON){
		
		String validationResult = "[]";
        final JSONObject jsonObjectRs = new JSONObject();
        try {
            final JSONObject jsonObject = new JSONObject(inputJSON);
            
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
            
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            //final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, inputJSON); 
            
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
            
            validationResult = generaliUserComponent.register(requestInfo, jsonRqArray.getJSONObject(0));
            
            JSONArray jsonRsArray = new JSONArray(validationResult);
            
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
            LOGGER.trace("Finish LifeEngage register ServiceImpl: " + validationResult);
            
            validationResult = jsonObjectRs.toString();
            
        } catch (ParseException e) {
            LOGGER.error("Failed to get registration Details with ParseException Error:" + e.getMessage());
            validationResult = "Failed to get registration Details with ParseException Response:" + e.getMessage();
        } catch (BusinessException e) {
            LOGGER.error("Failed to get registration Details with BusinessException Error:" + e.getMessage());
            validationResult = "Failed to get registration Details with BusinessException Response:" + e.getMessage();
        }
        return validationResult; 	
	}
	
	/* 
	 * Same functionaility as register used for forgetPassword
	 */
	@Override
	public String forgetPassword(String inputJSON) {
		
		String validationResult = "[]";
        final JSONObject jsonObjectRs = new JSONObject();
        try {
            final JSONObject jsonObject = new JSONObject(inputJSON);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
            
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            //final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, inputJSON);
            
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
            
            validationResult = generaliUserComponent.forgetPassword(requestInfo, jsonRqArray.getJSONObject(0));
            
            JSONArray jsonRsArray = new JSONArray(validationResult);
            
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
            LOGGER.trace("Finish LifeEngage forgetPassword ServiceImpl: " + validationResult);
            
            validationResult = jsonObjectRs.toString();
            
        } catch (ParseException e) {
            LOGGER.error("Failed to get forgetPassword details with ParseException Error:" + e.getMessage());
            validationResult = "Failed to get forgetPassword details with ParseException Response:" + e.getMessage();
        } catch (BusinessException e) {
            LOGGER.error("Failed to get forgetPassword details with BusinessException Error:" + e.getMessage());
            validationResult = "Failed to get forgetPassword details with BusinessException Response:" + e.getMessage();
        }
        return validationResult;
	}
	

	@Override
	public String createPassword(String inputJSON) {
		
		String validationResult = "[]";
        final JSONObject jsonObjectRs = new JSONObject();
        
        try {
            final JSONObject jsonObject = new JSONObject(inputJSON);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
            
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            //final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, inputJSON);
            
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
            
            validationResult = generaliUserComponent.createPassword(requestInfo, jsonRqArray.getJSONObject(0));
            
            JSONArray jsonRsArray = new JSONArray(validationResult);
            
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
            LOGGER.trace("Finish LifeEngage createPassword ServiceImpl: " + validationResult);
            
            validationResult = jsonObjectRs.toString();
            
        } catch (ParseException e) {
            LOGGER.error("Failed to create password Details with ParseException Error:" + e.getMessage());
            validationResult = "Failed to create password Details with ParseException Response:" + e.getMessage();
        } catch (BusinessException e) {
            LOGGER.error("Failed to create password Details with BusinessException Error:" + e.getMessage());
            validationResult = "Failed to create password Details with BusinessException Response:" + e.getMessage();
        }      
        
        return validationResult;
	}
	
	@Override
	public String resetPassword(String inputJSON) {
		
		String validationResult = "[]";
        final JSONObject jsonObjectRs = new JSONObject();
        
        try {
            final JSONObject jsonObject = new JSONObject(inputJSON);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
            
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            //final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, inputJSON);
            
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
            
            validationResult = generaliUserComponent.resetPassword(requestInfo, jsonRqArray.getJSONObject(0));
            
            JSONArray jsonRsArray = new JSONArray(validationResult);
            
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
            LOGGER.trace("Finish LifeEngage resetPassword ServiceImpl: " + validationResult);
            
            validationResult = jsonObjectRs.toString();
            
        } catch (ParseException e) {
            LOGGER.error("Failed to reset password Details with ParseException Error:" + e.getMessage());
            validationResult = "Failed to reset password Details with ParseException Response:" + e.getMessage();
        } catch (BusinessException e) {
            LOGGER.error("Failed to reset password Details with BusinessException Error:" + e.getMessage());
            validationResult = "Failed to reset password Details with BusinessException Response:" + e.getMessage();
        } catch (NoSuchElementException e) {
        	LOGGER.error("Failed to reset password Details with NoSuchElementException Error:" + e.getMessage());
			validationResult = "Failed to reset password Details with NoSuchElementException Response:" + e.getMessage();
		} catch (RecentPasswordUsageException e) {
			LOGGER.error("Failed to reset password Details with RecentPasswordUsageException Error:" + e.getMessage());
			validationResult = "Failed to reset password Details with RecentPasswordUsageException Response:" + e.getMessage();
		}      
        
        return validationResult;
	}
	
	@Override
	public String retrieveLeadList(String inputJSON){
		
		final JSONObject jsonObjectRs = new JSONObject();
        String transactionArray = "{\"Transactions\":[]}";
		String validationResult = "[]";
    
       try {
        final JSONObject jsonObject = new JSONObject(inputJSON);
        final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
        final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
        final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
        //final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, inputJSON);
        final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
        final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
        
        transactionArray = generaliUserComponent.retrieveLeads(requestInfo, jsonRqArray);
        JSONArray jsonRsArray = new JSONArray();
        jsonRsArray.put(new JSONObject(transactionArray));
        LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
        validationResult=jsonObjectRs.toString();
        
    } catch (ParseException e) {
        LOGGER.error("Failed to retrieve the details with ParseException Error:" + e.getMessage());
        validationResult = "Failed to retrieve the details with ParseException Response:" + e.getMessage();
    } catch (InputValidationException e) {
    	JSONObject jsonResponse= new JSONObject();
     	jsonResponse.put("statusCode", /*LifeEngageValidationHelper.FAILURE_CODE*/"400");
     	jsonResponse.put("status", /*LifeEngageValidationHelper.FAILURE*/"FAILURE");                    	
     	jsonResponse.put("statusMessage", e.getMessage());                    	
     	jsonObjectRs.put("StatusData", jsonResponse);			
	}    
    
    return validationResult;
	}
}
