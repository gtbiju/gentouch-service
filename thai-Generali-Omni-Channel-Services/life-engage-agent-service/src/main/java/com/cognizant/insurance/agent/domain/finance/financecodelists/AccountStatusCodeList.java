/**
 * 
 */
package com.cognizant.insurance.agent.domain.finance.financecodelists;

/**
 * This list defines the status of an account. 
 * 
 * @author 301350
 * 
 * 
 */
public enum AccountStatusCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Active,
    /**
     * 
     * 
     * 
     * 
     */
    Expired,
    /**
     * 
     * 
     * 
     * 
     */
    Suspended,
    /**
     * 
     * 
     * 
     * 
     */
    Temporary,
    /**
     * 
     * 
     * 
     * 
     */
    Pending,
    /**
     * 
     * 
     * 
     * 
     */
    Rejected,
    /**
     * 
     * 
     * 
     * 
     */
    Reinstated,
    /**
     * 
     * 
     * 
     * 
     */
    Unknown
}
