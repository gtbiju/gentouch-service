package com.cognizant.le.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.cognizant.le.model.dto.PropertyFileSource;

@Repository
public class FileUploadDaoImpl implements FileUploadDao{

	@Autowired
	PropertyFileSource propertyFileSource;
	
	@Autowired
	RestTemplate restTemplate;
	
	public void uploadFile(String fileName, String type ,String user) throws Exception{
		
		
		String UPLOAD_REQUEST=propertyFileSource.getUploadRequestTemplate();
		String UPLOAD_URL=propertyFileSource.getUploadUlrl();
		
		String OMNI_UPLOAD_REQUEST = UPLOAD_REQUEST.replace("{FileName}", fileName).replace("{Type}", type).replace("{user}", user);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> httpRequest = new HttpEntity<String>(
				OMNI_UPLOAD_REQUEST, headers);
		ResponseEntity<String> response = restTemplate.postForEntity(
				UPLOAD_URL, httpRequest, String.class);
		
	}
}
