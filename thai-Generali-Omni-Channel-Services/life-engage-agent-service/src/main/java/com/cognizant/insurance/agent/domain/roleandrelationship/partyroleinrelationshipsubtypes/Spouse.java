/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolerelationshipsubtypes.CivilRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CivilRelationNatureCodeList;

/**
 * This concept represents a spouse (husband or wife) as related to a civil
 * relationship.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Spouse extends PartyRoleInRelationship {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2331555000483286250L;

    /**
     * A code indicating the nature of a spouse in relation to another spouse in
     * the context of a civil relationship. 
     * 
     * 
     * 
     */
	@Enumerated
    private CivilRelationNatureCodeList civilRelationNatureCode;

    /**
     * This relationship link a spouse to a related civil relationship.
     * 
     * In the event a spouse no longer has an association with the other spouse
     * (divorce, widowed, etc.), only one spouse needs to be associated with the
     * relationship.
     * 
     * 
     * 
     * 
     * 
     */	
   @OneToMany(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
   @JoinTable(name = "SPOUSE_CIVILRELATIONSHIP",
            joinColumns = { @JoinColumn(name = "SPOUSE_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "CIVILRELATIONSHIP_ID", referencedColumnName = "Id") })
    private Set<CivilRelationship> includingCivilRelationship;

    /**
     * Gets the civilRelationNatureCode.
     * 
     * @return Returns the civilRelationNatureCode.
     */
    public CivilRelationNatureCodeList getCivilRelationNatureCode() {
        return civilRelationNatureCode;
    }

    /**
     * Gets the includingCivilRelationship.
     * 
     * @return Returns the includingCivilRelationship.
     */
    public Set<CivilRelationship> getIncludingCivilRelationship() {
        return includingCivilRelationship;
    }

    /**
     * Sets The civilRelationNatureCode.
     * 
     * @param civilRelationNatureCode
     *            The civilRelationNatureCode to set.
     */
    public void setCivilRelationNatureCode(
            final CivilRelationNatureCodeList civilRelationNatureCode) {
        this.civilRelationNatureCode = civilRelationNatureCode;
    }

    /**
     * Sets The includingCivilRelationship.
     * 
     * @param includingCivilRelationship
     *            The includingCivilRelationship to set.
     */
    public void setIncludingCivilRelationship(
            final Set<CivilRelationship> includingCivilRelationship) {
        this.includingCivilRelationship = includingCivilRelationship;
    }

}
