/*
 * Name : GeneraliPartyComponentImpl
 * Created on : 21-07-2015
 */
package com.cognizant.insurance.omnichannel.component.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Communication;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationInteractionTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationStatusCodeList;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
import com.cognizant.insurance.omnichannel.component.GeneraliPartyComponent;
import com.cognizant.insurance.omnichannel.dao.GeneraliPartyDao;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliPartyDaoImpl;
import com.cognizant.insurance.omnichannel.vo.AgentData;
import com.cognizant.insurance.omnichannel.vo.ManagerialLevelUserDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.RequestImpl;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;
import com.cognizant.insurance.party.component.impl.PartyComponentImpl;
import com.cognizant.insurance.party.dao.PartyDao;
import com.cognizant.insurance.party.dao.impl.PartyDaoImpl;

public class GeneraliPartyComponentImpl extends PartyComponentImpl implements GeneraliPartyComponent {

    @Autowired
    private GeneraliPartyDao partyDao;

    /*
     * Implementing retrieveDuplicateLeadList based on agent id , contact number and status code NOT IN
     */
    @Override
    public Response<List<Customer>> retrieveDuplicateLeadList(final Request<PartyRoleInRelationship> customerRequest,
            final Transactions transactions, final Request<List<CustomerStatusCodeList>> statusRequest,
            final Request<List<TelephoneTypeCodeList>> telephoneTypeRequest) {
        // TODO Auto-generated method stub

        final List<Customer> customers =
                partyDao.retrieveLMSDuplicateCustomers(customerRequest, transactions, statusRequest,
                        telephoneTypeRequest);
        final Response<List<Customer>> response = new ResponseImpl<List<Customer>>();
        if (!customers.isEmpty()) {
            response.setType(customers);
        }
        return response;
    }

    public Response<Set<Insured>> retrieveAdditionalInsureds(Request<Agreement> agreementRequest) {

        return partyDao.retrieveInsuredWithAdditionalInsured(agreementRequest);
    }

    public Response<List<SearchCountResult>> getRelatedTransactionCountForLMS(
            final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {

        return partyDao.getRelatedTransactionCountForLMS(searchCriteriatRequest, statusRequest);
    }
    
    public Response<AgreementProducer> retrieveAgent(Request<Agreement> agreementRequest) {

        return partyDao.retrieveAgent(agreementRequest);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveCustomers(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<Customer>> retrieveCustomers(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest, Request<Integer> limitRequest) {
      
        final List<Customer> customers = partyDao.retrieveCustomers(customerRequest, statusRequest, limitRequest);

        final Response<List<Customer>> response = new ResponseImpl<List<Customer>>();
        if (!customers.isEmpty()) {
            response.setType(customers);
        }
        return response;
    }

    /**
     * retrieveAgentwith50lessFollowUps
     */
    @Override
    public Response<List<String>> retrieveAgentwith50lessFollowUps(final Request<List<String>> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
      
        final List<String> agentCodes = partyDao.getAgentLeadCount(customerRequest, statusRequest);

        final Response<List<String>> response = new ResponseImpl<List<String>>();
        if (!agentCodes.isEmpty()) {
            response.setType(agentCodes);
        }
        return response;
    }

    /**
     * retrieveAgentwithMaximumYTD
     */
    @Override
    public Response<List<AgentData>> retrieveAgentwithMaximumYTD(final Request<List<AgentData>> agentDataRequest,
            final Request<List<CustomerStatusCodeList>> statusClosedRequest, final Request<List<CustomerStatusCodeList>> statusRequest) {
    
    	List<AgentData> agentDataList = new ArrayList<AgentData>();
    	List <String>agentCodeList= new ArrayList<String>();
    	if(agentDataRequest!=null){
    		agentDataList=agentDataRequest.getType();
    	}
    	for (AgentData agentData:agentDataList){
    		agentCodeList.add(agentData.getAgentCode());
    	}
    	  final List<AgentData> agentCodes = partyDao.getYTD(agentDataRequest, statusClosedRequest,statusRequest);

        final Response<List<AgentData>> response = new ResponseImpl<List<AgentData>>();
        response.setType(agentCodes);
       
        return response;
    }
    /*
     * **IMP** 'statusRequest' is not considered for now 
     * 
     * */
    public Response<Customer> retrieveCustomerWithTransTrackingId(Request<String> trackId,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        final GeneraliPartyDao partyDao = new GeneraliPartyDaoImpl();
        return partyDao.retrieveCustomerWithTransTrackId(trackId, statusRequest);
    }

	@Override
	public Response<ManagerialLevelUserDetails> retrieveByStatusForManagerialLevel(
			Request<ManagerialLevelUserDetails> managerialLevelUserDetailsRequest,
			List<CustomerStatusCodeList> statusListNew,
			List<CustomerStatusCodeList> statusListClosed,
			List<CustomerStatusCodeList> statusListFollowUp) {
		
		final ManagerialLevelUserDetails managerialLevelUserDetailsCountResp = partyDao.getAgentStatusCount(managerialLevelUserDetailsRequest, statusListNew, statusListClosed, statusListFollowUp);
		
		final Response<ManagerialLevelUserDetails> response = new ResponseImpl<ManagerialLevelUserDetails>();
		response.setType(managerialLevelUserDetailsCountResp);
		
		return response;
	}
	
	@Override
	public Response<SearchCountResult> getCustomerCountRetrieveFilter(
			Request<SearchCriteria> searchCriteriaRequest,
			List<CustomerStatusCodeList> statusRequest,		
			List<CustomerStatusCodeList> statusListClosed) {
		Response<SearchCountResult> searchCriteriaReq = new ResponseImpl<SearchCountResult>();
		
		SearchCriteria searchCriteriaObj = searchCriteriaRequest.getType();
		
		if("BranchUser".equals(searchCriteriaObj.getAgentType())) {
			searchCriteriaReq = partyDao.getRetrieveFilterCountBranchUser(searchCriteriaRequest, statusRequest, statusListClosed);
		} else if(("Agency".equals(searchCriteriaObj.getAgentType())) || ("IOIS".equals(searchCriteriaObj.getAgentType()))) {
			searchCriteriaReq = partyDao.getRetrieveFilterCount(searchCriteriaRequest, statusRequest, statusListClosed);
		}
		
		return searchCriteriaReq;
	}

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveFilterCustomers(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.Request)
     */
    
	@Override
	public Response<List<Customer>> retrieveFilterCustomers(
			final Request<SearchCriteria> searchCriteriatRequest,
			final Request<List<CustomerStatusCodeList>> statusRequest) {
		final List<Customer> customers = partyDao.retrieveFilterCustomers(
				searchCriteriatRequest, statusRequest);
		final Response<List<Customer>> response = new ResponseImpl<List<Customer>>();
		if (!customers.isEmpty()) {
			response.setType(customers);
		}
		return response;
	}
	
	@Override
	public Map<String,Integer> retrieveMonthlySalesActivityCustomers(
			final Request<SearchCriteria> searchCriteriatRequest,
			final Request<List<CustomerStatusCodeList>> statusRequest,
			final Request<List<CommunicationStatusCodeList>> communicationStatusRequest,
			final Request<List<CommunicationInteractionTypeCodeList>> communicationInteractionCodeRequest) {
		final Map<String,Integer> customers = partyDao.retrieveMonthlySalesActivityCustomers(
				searchCriteriatRequest, statusRequest, communicationStatusRequest, communicationInteractionCodeRequest);
		/*final Response<List<Customer>> response = new ResponseImpl<List<Customer>>();
		if (!customers.isEmpty()) {
			response.setType(customers);
		}*/
		return customers;
	}
	
	@Override
	public Response<List<Customer>> retrieveMonthlyNewProspect(
			final Request<SearchCriteria> searchCriteriatRequest,
			final Request<List<CustomerStatusCodeList>> statusRequest) {
		final List<Customer> customers = partyDao.retrieveMonthlyNewProspect(
				searchCriteriatRequest, statusRequest);
		final Response<List<Customer>> response = new ResponseImpl<List<Customer>>();
		if (!customers.isEmpty()) {
			response.setType(customers);
		}
		return response;
	}
	
	@Override
	public Map<String,Integer> retrieveMonthwiseSalesActivityCustomers(
			final Request<SearchCriteria> searchCriteriatRequest,
			final Request<List<CustomerStatusCodeList>> statusRequest) {
		final Map<String,Integer> customers = partyDao.retrieveMonthwiseSalesActivityCustomers(
				searchCriteriatRequest, statusRequest);
		
		return customers;
	}
	
	@Override
	public Map<String,Integer> retrieveWeekwiseSalesActivityCustomers(
			final Request<SearchCriteria> searchCriteriatRequest,
			final Request<List<CustomerStatusCodeList>> statusRequest) {
		final Map<String,Integer> customers = partyDao.retrieveWeekwiseSalesActivityCustomers(
				searchCriteriatRequest, statusRequest);
		
		return customers;
	}
	
	@Override
	public Map<String,Integer> retrieveWeekwiseNewProspect(
			final Request<SearchCriteria> searchCriteriatRequest,
			final Request<List<CustomerStatusCodeList>> statusRequest) {
		final Map<String,Integer> customers = partyDao.retrieveNewProspect(
				searchCriteriatRequest, statusRequest);
		
		return customers;
	}
	
	@Override
	public Map<String,Integer> retrieveMonthwiseNewProspect(
			final Request<SearchCriteria> searchCriteriatRequest,
			final Request<List<CustomerStatusCodeList>> statusRequest) {
		final Map<String,Integer> customers = partyDao.retrieveMonthwiseNewProspect(
				searchCriteriatRequest, statusRequest);
		
		return customers;
	}
    
	@Override
    public Response<SearchCountResult> getCustomerCount(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        final Response<SearchCountResult> searchCountResponse =
                partyDao.getCustomerCount(searchCriteriatRequest, statusRequest);
        return searchCountResponse;
    }
	
    @Override
    public Map<String, String> retrieveMonthlyNewCount(final Request<SearchCriteria> searchCriteriatRequest) {
        final Map<String, String> response = partyDao.retrieveMonthlyNewCount(searchCriteriatRequest);

        return response;
    }
    @Override
    public Map<String, Map<String, String>> retrieveLeadStatistics(final Request<SearchCriteria> searchCriteriatRequest) {
        final Map<String, Map<String, String>> response = partyDao.retrieveLeadStatistics(searchCriteriatRequest);

        return response;
    }
}
