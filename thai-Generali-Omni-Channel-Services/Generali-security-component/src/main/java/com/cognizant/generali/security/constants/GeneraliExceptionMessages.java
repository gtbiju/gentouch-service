package com.cognizant.generali.security.constants;

public class GeneraliExceptionMessages {
	
	public static final String ACCOUNT_LOCKED = "AccountLocked";
	public static final String PASSWORD_EXPIRED = "Your password has expired. Please change the password to login";
}
