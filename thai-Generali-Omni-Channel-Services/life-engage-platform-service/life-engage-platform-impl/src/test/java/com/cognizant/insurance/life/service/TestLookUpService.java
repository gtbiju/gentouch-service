/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 22, 2013
 */
package com.cognizant.insurance.life.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LookUpService;

// TODO: Auto-generated Javadoc
/**
 * The Class class TestLookUpService.
 * 
 * @author 300797
 */
@ContextConfiguration("/service-context-test.xml")
public class TestLookUpService extends AbstractJUnit4SpringContextTests {

    /** The look up service. */
    @Autowired
    private LookUpService lookUpService;

    /** The data source. */
    @Autowired
    private DataSource dataSource;

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /**
     * Gets the country details.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public final void getCountryDetails() throws IOException, BusinessException, ParseException {

        String responseJSON =
                lookUpService.getCountrySpecificDetails(getRequest("loopUpRequest/counrtyLookUpServiceRq.json"));
        // System.out.println(responseJSON);
        assertNotNull(responseJSON);
        JSONObject jsonObject = new JSONObject(responseJSON);
        JSONObject jsonResponseObj = jsonObject.getJSONObject("Response");
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonRsTransactionData = jsonRsTransactionsArray.getJSONObject(0).getJSONObject("TransactionData");
        assertNotNull(jsonRsTransactionData);
        JSONObject jsonCountryObj = jsonRsTransactionData.getJSONObject("Country");
        assertNotNull(jsonCountryObj);
        assertEquals("India", jsonCountryObj.getString("Name"));
        JSONArray jsonStateArray = jsonCountryObj.getJSONArray("States");
        assertEquals(4, jsonStateArray.length());
        for (int i = 0; i < jsonStateArray.length(); i++) {
            if ("Maharashtra".equals(jsonStateArray.getJSONObject(i).getString("Name"))) {
                JSONArray jsonCityArray = jsonStateArray.getJSONObject(i).getJSONArray("Cities");
                assertEquals(2, jsonCityArray.length());
                assertTrue(jsonCityArray.join(",").contains("Mumbai"));
                assertTrue(jsonCityArray.join(",").contains("Pune"));
            } else if ("TamilNadu".equals(jsonStateArray.getJSONObject(i).getString("Name"))) {
                JSONArray jsonCityArray = jsonStateArray.getJSONObject(i).getJSONArray("Cities");
                assertEquals(3, jsonCityArray.length());
                assertTrue(jsonCityArray.join(",").contains("Chennai"));
                assertTrue(jsonCityArray.join(",").contains("Coimbatore"));
                assertTrue(jsonCityArray.join(",").contains("Salem"));
            } else if ("Karnataka".equals(jsonStateArray.getJSONObject(i).getString("Name"))) {
                JSONArray jsonCityArray = jsonStateArray.getJSONObject(i).getJSONArray("Cities");
                assertEquals(2, jsonCityArray.length());
                assertTrue(jsonCityArray.join(",").contains("Bangalore"));
                assertTrue(jsonCityArray.join(",").contains("Mysore"));
            } else if ("Kerala".equals(jsonStateArray.getJSONObject(i).getString("Name"))) {
                JSONArray jsonCityArray = jsonStateArray.getJSONObject(i).getJSONArray("Cities");
                assertEquals(3, jsonCityArray.length());
                assertTrue(jsonCityArray.join(",").contains("Kochi"));
                assertTrue(jsonCityArray.join(",").contains("Trivandrum"));
                assertTrue(jsonCityArray.join(",").contains("Calicut"));
            }

        }

    }

    /**
     * Gets the country or country sub division list.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public final void getCountryOrCountrySubDivisionList() throws IOException, BusinessException, ParseException {

        String responseJSON =
                lookUpService.getCountryOrCountrySubDivisionList(getRequest(
                        "loopUpRequest/counrtyOrCountrySubDivisionListingServiceRq.json"));
        // System.out.println(responseJSON);
        assertNotNull(responseJSON);
        JSONObject jsonObject = new JSONObject(responseJSON);
        JSONObject jsonResponseObj = jsonObject.getJSONObject("Response");
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonRsTransactionData = jsonRsTransactionsArray.getJSONObject(0).getJSONObject("TransactionData");
        assertNotNull(jsonRsTransactionData);
        JSONArray jsonListArray = jsonRsTransactionData.getJSONArray("List");
        assertEquals(2, jsonListArray.length());
        assertEquals("1", jsonListArray.getJSONObject(0).getString("id"));
        assertEquals("India", jsonListArray.getJSONObject(0).getString("name"));
        assertEquals("16", jsonListArray.getJSONObject(1).getString("id"));
        assertEquals("USA", jsonListArray.getJSONObject(1).getString("name"));
    }

    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(final String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(line);
        }

        return sb.toString();
    }

    /**
     * Sets the up.
     */
    @Before
    public final void setUp() {

        try {

            QueryRunner qRunner = new QueryRunner(dataSource);
            qRunner.update("DROP TABLE if exists COUNTRY_LOOKUP;");
            qRunner.update("CREATE TABLE COUNTRY_LOOKUP (identifier bigint(20) " 
                    + "NOT NULL AUTO_INCREMENT,name varchar(25) "
                    + "DEFAULT NULL, type varchar(25) DEFAULT NULL," 
                    + "parentID bigint(20) DEFAULT NULL,PRIMARY KEY(identifier))");

            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) " 
            		+ "values(1,'India','Country',0);");

            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(2,'Kerala','State',1);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(3,'TamilNadu','State',1);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(4,'Karnataka','State',1);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(5,'Maharashtra','State',1);");

            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(6,'Kochi','City',2);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(7,'Trivandrum','City',2);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(8,'Calicut','City',2);");

            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(9,'Chennai','City',3);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(10,'Coimbatore','City',3);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(11,'Salem','City',3);");

            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(12,'Bangalore','City',4);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(13,'Mysore','City',4);");

            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(14,'Mumbai','City',5);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(15,'Pune','City',5);");

            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(16,'USA','Country',0);");

            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(17,'Arizona','State',16);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(18,'Minnesota','State',16);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(19,'Florida','State',16);");

            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(20,'Pheonix','City',17);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(21,'Minneapolis','City',18);");
            qRunner.update("INSERT INTO COUNTRY_LOOKUP(identifier, name, type, parentID) "
                    + "values(22,'Tampa','City',19);");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the look up service.
     * 
     * @return the lookUpService
     */
    public final LookUpService getLookUpService() {
        return lookUpService;
    }

    /**
     * Sets the look up service.
     * 
     * @param lookUpService
     *            the lookUpService to set
     */
    public final void setLookUpService(final LookUpService lookUpService) {
        this.lookUpService = lookUpService;
    }

    /**
     * Gets the data source.
     * 
     * @return the dataSource
     */
    public final DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Sets the data source.
     * 
     * @param dataSource
     *            the dataSource to set
     */
    public final void setDataSource(final DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
