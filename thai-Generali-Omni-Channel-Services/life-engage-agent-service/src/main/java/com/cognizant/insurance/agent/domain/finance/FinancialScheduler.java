/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Feb 7, 2013
 */

package com.cognizant.insurance.agent.domain.finance;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.FinancialMediumTypeCodeList;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.PaymentFrequencyCodeList;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.agent.domain.finance.bankaccount.BankAccount;
import com.cognizant.insurance.agent.domain.finance.financialactivitysubtypes.PaymentMethod;

/**
 * The definition of how financial transactions are handled based on the information available on money provisions and
 * its components (money provision element and money provision element part).
 * 
 * While money provisions only represent the definition of the amounts and their composition, the money scheduler
 * defines how the money will be paid, when it will be paid and who will pay and receive it.
 * 
 * A money scheduler is also responsible for creating the account entires that represent the necessary details of the
 * created financial transactions. These details are derived from the money provision element parts that are attached to
 * the money provisions for this money scheduler.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class FinancialScheduler extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6392483330277880406L;

    /**
     * The way the money will be paid by the payer to the receiver for this money scheduler.
     * 
     * e.g: Bank transfer
     * 
     * e.g: Cash
     * 
     * e.g: Cheque
     * 
     * e.g: Credit card billing
     * 
     * e.g: Direct debit
     * 
     * e.g: Offset
     * 
     * e.g: Payroll deduction
     * 
     * 
     * 
     */
    @Enumerated
    private FinancialMediumTypeCodeList paymentMethodCode;

    /**
     * Describes the payment frequency for the financial scheduler.
     * 
     * 
     * 
     */
    @Enumerated
    private PaymentFrequencyCodeList paymentFrequencyCode;

    /**
     * The day and month that are used to calculate the payment due dates for the payments and payment dues created by
     * the money scheduler.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date anniversaryDate;

    /**
     * The date at which the first amount of money is due for this money scheduler. This first amount of money itself is
     * derived from the elements of the money provisions related to this money scheduler.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date firstDueDate;

    /**
     * A textual statement used to explain the meaning of this money scheduler.
     * 
     * 
     * 
     */
    private String description;

    /**
     * The time period at which the money scheduler is active.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private TimePeriod effectivePeriod;

    /**
     * This corresponds to the number of payments expected to cover the initial policy loan amount.
     * 
     * 
     * 
     */
    private BigDecimal dueCollectionDateCount;

    /**
     * For each contract premium, there is a reference period that can be considered, as a coverage period, which is the
     * counterpart of the contract premium payment
     * 
     * e.g. with a monthly payment, the reference period is 1 month.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private TimePeriod referencePeriod;

    /**
     * Status effect date of remittance option.
     * 
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date statusEffectDate;

    /**
     * This relationship links a financial scheduler with a related bank account.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private BankAccount relatedBankAccount;

    /**
     * This relationship links a financial scheduler to a payment method. Payment means like cash, check, credit card,
     * direct debit, etc.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private PaymentMethod paymentMeans;

    /**
     * The day of the month associated with the financial schedule, often associated with recurring events (e.g. day of
     * month due, day of month paid, etc.).
     * 
     * 
     * 
     */
    private Integer dayOfMonth;
    
    /**
     * Added for generali
     */
    private String transactionRefNumber;

    public String getTransactionRefNumber() {
        return transactionRefNumber;
    }

    public void setTransactionRefNumber(String transactionRefNumber) {
        this.transactionRefNumber = transactionRefNumber;
    }

    /**
     * Gets the paymentMethodCode.
     * 
     * @return Returns the paymentMethodCode.
     */
    public FinancialMediumTypeCodeList getPaymentMethodCode() {
        return paymentMethodCode;
    }

    /**
     * Gets the paymentFrequencyCode.
     * 
     * @return Returns the paymentFrequencyCode.
     */
    public PaymentFrequencyCodeList getPaymentFrequencyCode() {
        return paymentFrequencyCode;
    }

    /**
     * Gets the anniversaryDate.
     * 
     * @return Returns the anniversaryDate.
     */
    public Date getAnniversaryDate() {
        return anniversaryDate;
    }

    /**
     * Gets the firstDueDate.
     * 
     * @return Returns the firstDueDate.
     */
    public Date getFirstDueDate() {
        return firstDueDate;
    }

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the effectivePeriod.
     * 
     * @return Returns the effectivePeriod.
     */
    public TimePeriod getEffectivePeriod() {
        return effectivePeriod;
    }

    /**
     * Gets the dueCollectionDateCount.
     * 
     * @return Returns the dueCollectionDateCount.
     */
    public BigDecimal getDueCollectionDateCount() {
        return dueCollectionDateCount;
    }

    /**
     * Gets the referencePeriod.
     * 
     * @return Returns the referencePeriod.
     */
    public TimePeriod getReferencePeriod() {
        return referencePeriod;
    }

    /**
     * Gets the statusEffectDate.
     * 
     * @return Returns the statusEffectDate.
     */
    public Date getStatusEffectDate() {
        return statusEffectDate;
    }

    /**
     * Gets the relatedBankAccount.
     * 
     * @return Returns the relatedBankAccount.
     */
    public BankAccount getRelatedBankAccount() {
        return relatedBankAccount;
    }

    /**
     * Gets the paymentMeans.
     * 
     * @return Returns the paymentMeans.
     */
    public PaymentMethod getPaymentMeans() {
        return paymentMeans;
    }

    /**
     * Gets the dayOfMonth.
     * 
     * @return Returns the dayOfMonth.
     */
    public Integer getDayOfMonth() {
        return dayOfMonth;
    }

    /**
     * Sets The paymentMethodCode.
     * 
     * @param paymentMethodCode
     *            The paymentMethodCode to set.
     */
    public void setPaymentMethodCode(final FinancialMediumTypeCodeList paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }

    /**
     * Sets The paymentFrequencyCode.
     * 
     * @param paymentFrequencyCode
     *            The paymentFrequencyCode to set.
     */
    public void setPaymentFrequencyCode(final PaymentFrequencyCodeList paymentFrequencyCode) {
        this.paymentFrequencyCode = paymentFrequencyCode;
    }

    /**
     * Sets The anniversaryDate.
     * 
     * @param anniversaryDate
     *            The anniversaryDate to set.
     */
    public void setAnniversaryDate(final Date anniversaryDate) {
        this.anniversaryDate = anniversaryDate;
    }

    /**
     * Sets The firstDueDate.
     * 
     * @param firstDueDate
     *            The firstDueDate to set.
     */
    public void setFirstDueDate(final Date firstDueDate) {
        this.firstDueDate = firstDueDate;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The effectivePeriod.
     * 
     * @param effectivePeriod
     *            The effectivePeriod to set.
     */
    public void setEffectivePeriod(final TimePeriod effectivePeriod) {
        this.effectivePeriod = effectivePeriod;
    }

    /**
     * Sets The dueCollectionDateCount.
     * 
     * @param dueCollectionDateCount
     *            The dueCollectionDateCount to set.
     */
    public void setDueCollectionDateCount(final BigDecimal dueCollectionDateCount) {
        this.dueCollectionDateCount = dueCollectionDateCount;
    }

    /**
     * Sets The referencePeriod.
     * 
     * @param referencePeriod
     *            The referencePeriod to set.
     */
    public void setReferencePeriod(final TimePeriod referencePeriod) {
        this.referencePeriod = referencePeriod;
    }

    /**
     * Sets The statusEffectDate.
     * 
     * @param statusEffectDate
     *            The statusEffectDate to set.
     */
    public void setStatusEffectDate(final Date statusEffectDate) {
        this.statusEffectDate = statusEffectDate;
    }

    /**
     * Sets The relatedBankAccount.
     * 
     * @param relatedBankAccount
     *            The relatedBankAccount to set.
     */
    public void setRelatedBankAccount(final BankAccount relatedBankAccount) {
        this.relatedBankAccount = relatedBankAccount;
    }

    /**
     * Sets The paymentMeans.
     * 
     * @param paymentMeans
     *            The paymentMeans to set.
     */
    public void setPaymentMeans(final PaymentMethod paymentMeans) {
        this.paymentMeans = paymentMeans;
    }

    /**
     * Sets The dayOfMonth.
     * 
     * @param dayOfMonth
     *            The dayOfMonth to set.
     */
    public void setDayOfMonth(final Integer dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

}
