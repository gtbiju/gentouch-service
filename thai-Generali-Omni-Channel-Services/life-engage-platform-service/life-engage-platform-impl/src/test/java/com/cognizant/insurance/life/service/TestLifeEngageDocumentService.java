/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 28, 2014
 */
package com.cognizant.insurance.life.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.DocumentService;
import com.cognizant.insurance.services.LifeEngageSyncService;
import com.cognizant.insurance.utils.Base64Utils;

/**
 * The Class class TestLifeEngageDocumentService.
 */
@ContextConfiguration("/service-context-test.xml")
public class TestLifeEngageDocumentService extends AbstractJUnit4SpringContextTests {

    /** The document service. */
    @Autowired
    DocumentService documentService;

    /** The response. */
    private MockHttpServletResponse response;

    /** The data source. */
    @Autowired
    private DataSource dataSource;

    /** The life engage sync service. */
    @Autowired
    LifeEngageSyncService lifeEngageSyncService;

    /** The Constant KEY6. */
    private static final String KEY6 = "Key6";

    /** The Constant KEY7. */
    private static final String KEY7 = "Key7";
    
    /** The Constant KEY15. */
    private static final String KEY15 = "Key15";

    /** The Constant KEY4. */
    private static final String KEY4 = "Key4";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant STATUS. */
    private static final String STATUS = "Status";

    /** The Constant STATUS_CODE. */
    private static final String STATUS_CODE = "StatusCode";

    /** The Constant STATUS_DATA. */
    private static final String STATUS_DATA = "StatusData";

    /** The Constant STATUS_MESSAGE. */
    private static final String STATUS_MESSAGE = "StatusMessage";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";

    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";

    @Value( "${le.platform.service.fileUploadPath}" )
    private String fileUploadPath;
    
    /**
     * Test generate pdf get.
     * 
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public void testGeneratePdfGet() throws IOException, BusinessException, ParseException {
        // stprule();
        String agentId;
        final SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Calendar calendar = new GregorianCalendar();
        final String currentDate = newDt.format(calendar.getTime());
        agentId = "Agent1";
        String productTypeFirst = "Term";
        String insFstNmeFst = "JohansGetPDF";
        String insuredLastNameFirst = "Nems";
        String proposerFirstNameFirst = "Johns";
        String proposerLastNameFirst = "Nemaas";
        String productFirstproductname = "lifeEngage";
        String eappStatusInitial = "Initial";
        String statusCode100 = "100";
        String statusSuccess = "SUCCESS";
        String statusMessage = "Proposal Saved";

        // creating the first eapp save request
        String saveRequest1 = getRequest("eAppRequest/eAppServiceRq.json");
        String saveRequest =
                saveRequest1.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", productFirstproductname)
                    .replace("{4}", productTypeFirst)
                    .replace("{5}", insuredLastNameFirst)
                    .replace("{6}", insFstNmeFst)
                    .replace("{7}", proposerLastNameFirst)
                    .replace("{8}", proposerFirstNameFirst)
                    .replace("{9}", "")
                    .replace("{10}", "");

        // Saving first eapp Request
        String responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        JSONObject jsonObject = new JSONObject(responseJson);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(eappStatusInitial, jsonObj.getString(KEY15));
        String eappIdFirst = jsonObj.getString(KEY4);
        assertNotNull(eappIdFirst);
        JSONObject statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));

        response = new MockHttpServletResponse();
        documentService.generatePdfGet(eappIdFirst, "eApp", "", response);
        assertEquals("application/pdf", response.getContentType());
        assertTrue(response.getContentLength() > 0);
        assertEquals("%PDF-1.4", response.getContentAsString().substring(0, 8));

    }

    @Test
    public void testUploadDocument() throws IOException, BusinessException, ParseException {
        String agentId;
        final SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Calendar calendar = new GregorianCalendar();
        final String currentDate = newDt.format(calendar.getTime());
        agentId = "Agent1";
        String productTypeFirst = "Term";
        String insFstNmeFst = "JohansUplaodDoc";
        String insuredLastNameFirst = "Nems";
        String proposerFirstNameFirst = "Johns";
        String proposerLastNameFirst = "Nemaas";
        String productFirstproductname = "lifeEngage";
        String statusCode100 = "100";
        String statusSuccess = "SUCCESS";
        String statusMessage = "Proposal Saved";
        String type = "eApp";

        // creating the eapp save request
        String saveRequest = getRequest("eAppRequest/eAppServiceRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", productFirstproductname)
                    .replace("{4}", productTypeFirst)
                    .replace("{5}", insuredLastNameFirst)
                    .replace("{6}", insFstNmeFst)
                    .replace("{7}", proposerLastNameFirst)
                    .replace("{8}", proposerFirstNameFirst)
                    .replace("{9}", "")
                    .replace("{10}", "");

        // Saving eapp Request
        String responseJson = lifeEngageSyncService.save(saveRequest);
        
        assertNotNull(responseJson);
        JSONArray jsonRsTransactionsArray =
                new JSONObject(responseJson).getJSONObject(RESPONSE).getJSONObject(RESPONSE_PAYLOAD).getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals("Initial", jsonObj.getString(KEY15));
        String eappIdFirst = jsonObj.getString(KEY4);
        assertNotNull(eappIdFirst);
        jsonObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(jsonObj);
        assertEquals(statusCode100, jsonObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, jsonObj.getString(STATUS));
        assertEquals(statusMessage, jsonObj.getString(STATUS_MESSAGE));

        // uploading first document object
        String uploadDocumentRequest = getRequest("documentRequest/UploadDocumentRequest.json");
        String workingDir = System.getProperty("user.dir");
        String doc1 = Base64Utils.encodeFileToBase64Binary(workingDir+"/src/test/resources/documentRequest/img1.jpg");
        String doc2 = Base64Utils.encodeFileToBase64Binary(workingDir+"/src/test/resources/documentRequest/img2.jpg");
        uploadDocumentRequest =
                uploadDocumentRequest.replace("{1}", eappIdFirst)
                    .replace("{2}", "doc1")
                    .replace("{3}",doc1)
                    .replace("{4}", "doc2")
                    .replace("{5}",doc2)
                    .replace("{6}", "agent102_527663_1379929960951")
        			.replace("{7}", "Saved"); 
        // System.out.println(uploadDocumentRequest);
        String uploadDocumentResponse = documentService.uploadDocument(uploadDocumentRequest);
        // System.out.println(uploadDocumentResponse);
        assertNotNull(uploadDocumentResponse);

        jsonRsTransactionsArray =
                new JSONObject(uploadDocumentResponse).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(jsonObj);
        assertEquals(statusCode100, jsonObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, jsonObj.getString(STATUS));
        assertEquals("Successfully uploaded document", jsonObj.getString(STATUS_MESSAGE));

        final File tempDirectory =
                new File(fileUploadPath + eappIdFirst);
        assertTrue(tempDirectory.exists());
        String[] fileList = tempDirectory.list();
        assertEquals(2, fileList.length);
        assertTrue(Arrays.asList(fileList).contains("doc1"));
        assertTrue(Arrays.asList(fileList).contains("doc2"));

        // Retrieving the saved eapp
        String retrieveRequest = getRequest("eAppRequest/eAppServiceRq_Retrieve.json");
        retrieveRequest = retrieveRequest.replace("{1}", currentDate).replace("{2}", eappIdFirst);
        // System.out.println(retrieveRequest);
        String retrieveResponseJson = lifeEngageSyncService.retrieve(retrieveRequest);
        // System.out.println(retrieveResponseJson);
        assertNotNull(retrieveResponseJson);
        jsonRsTransactionsArray =
                new JSONObject(retrieveResponseJson).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        assertEquals(eappIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY4));
        assertEquals("Final", jsonRsTransactionsArray.getJSONObject(0).getString(KEY15));
        assertEquals(type, jsonRsTransactionsArray.getJSONObject(0).getString(TYPE));

        assertEquals(1, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Upload")
            .getJSONArray("Documents")
            .length());
        assertEquals("agent102_527663_1379929960951",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(0)
                    .getString("documentName"));
        assertEquals(2, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Upload")
            .getJSONArray("Documents")
            .getJSONObject(0)
            .getJSONArray("pages")
            .length());
        assertEquals("doc1",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(0)
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("fileName"));
        assertEquals("doc2",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(0)
                    .getJSONArray("pages")
                    .getJSONObject(1)
                    .getString("fileName"));

        // uploading first document object with different pages
        String doc3 = Base64Utils.encodeFileToBase64Binary(workingDir+"/src/test/resources/documentRequest/img3.jpg");
        uploadDocumentRequest = getRequest("documentRequest/UploadDocumentRequest.json");
        uploadDocumentRequest = 
                uploadDocumentRequest.replace("{1}", eappIdFirst)
                    .replace("{2}", "doc1")
                    .replace("{3}","")
                    .replace("{4}", "doc3")
                    .replace("{5}", doc3)
                    .replace("{6}", "agent102_527663_1379929960951");
        // System.out.println(uploadDocumentRequest);
        uploadDocumentResponse = documentService.uploadDocument(uploadDocumentRequest);
        // System.out.println(uploadDocumentResponse);
        assertNotNull(uploadDocumentResponse);

        jsonRsTransactionsArray =
                new JSONObject(uploadDocumentResponse).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertNotNull(jsonObj);
        jsonObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(jsonObj);
        assertEquals(statusCode100, jsonObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, jsonObj.getString(STATUS));
        assertEquals("Successfully uploaded document", jsonObj.getString(STATUS_MESSAGE));

        assertTrue(tempDirectory.exists());
        fileList = tempDirectory.list();
        assertEquals(2, fileList.length);
        assertTrue(Arrays.asList(fileList).contains("doc1"));
        assertTrue(Arrays.asList(fileList).contains("doc3"));

        // Retrieving the saved eapp and confirming that pages are upto date
        retrieveRequest = getRequest("eAppRequest/eAppServiceRq_Retrieve.json");
        retrieveRequest = retrieveRequest.replace("{1}", currentDate).replace("{2}", eappIdFirst);
        // System.out.println(retrieveRequest);
        retrieveResponseJson = lifeEngageSyncService.retrieve(retrieveRequest);
        // System.out.println(retrieveResponseJson);
        assertNotNull(retrieveResponseJson);
        jsonRsTransactionsArray =
                new JSONObject(retrieveResponseJson).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        assertEquals(eappIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY4));
        assertEquals("Final", jsonRsTransactionsArray.getJSONObject(0).getString(KEY15));
        assertEquals(type, jsonRsTransactionsArray.getJSONObject(0).getString(TYPE));

        assertEquals(1, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Upload")
            .getJSONArray("Documents")
            .length());
        assertEquals("agent102_527663_1379929960951",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(0)
                    .getString("documentName"));
        assertEquals(2, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Upload")
            .getJSONArray("Documents")
            .getJSONObject(0)
            .getJSONArray("pages")
            .length());
        assertEquals("doc1",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(0)
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("fileName"));
        assertEquals("doc3",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(0)
                    .getJSONArray("pages")
                    .getJSONObject(1)
                    .getString("fileName"));

        // uploading second document object
        uploadDocumentRequest = getRequest("documentRequest/UploadDocumentRequest.json");
        String doc10 = Base64Utils.encodeFileToBase64Binary(workingDir+"/src/test/resources/documentRequest/img1.jpg");
        String doc11 = Base64Utils.encodeFileToBase64Binary(workingDir+"/src/test/resources/documentRequest/img2.jpg");
        uploadDocumentRequest =
                uploadDocumentRequest.replace("{1}", eappIdFirst)
                    .replace("{2}", "doc10")
                    .replace("{3}", doc10)
                    .replace("{4}", "doc11")
                    .replace("{5}", doc11)
                    .replace("{6}", "agent102_527663_1379929960952");
        // System.out.println(uploadDocumentRequest);
        uploadDocumentResponse = documentService.uploadDocument(uploadDocumentRequest);
        // System.out.println(uploadDocumentResponse);
        assertNotNull(uploadDocumentResponse);

        jsonRsTransactionsArray =
                new JSONObject(uploadDocumentResponse).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertNotNull(jsonObj);
        jsonObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(jsonObj);
        assertEquals(statusCode100, jsonObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, jsonObj.getString(STATUS));
        assertEquals("Successfully uploaded document", jsonObj.getString(STATUS_MESSAGE));

        assertTrue(tempDirectory.exists());
        fileList = tempDirectory.list();
        assertEquals(4, fileList.length);
        assertTrue(Arrays.asList(fileList).contains("doc1"));
        assertTrue(Arrays.asList(fileList).contains("doc3"));
        assertTrue(Arrays.asList(fileList).contains("doc10"));
        assertTrue(Arrays.asList(fileList).contains("doc11"));

        // Retrieving the saved eapp and confirming that documents and pages are upto date
        retrieveRequest = getRequest("eAppRequest/eAppServiceRq_Retrieve.json");
        retrieveRequest = retrieveRequest.replace("{1}", currentDate).replace("{2}", eappIdFirst);
        // System.out.println(retrieveRequest);
        retrieveResponseJson = lifeEngageSyncService.retrieve(retrieveRequest);
        // System.out.println(retrieveResponseJson);
        assertNotNull(retrieveResponseJson);
        jsonRsTransactionsArray =
                new JSONObject(retrieveResponseJson).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        assertEquals(eappIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY4));
        assertEquals("Final", jsonRsTransactionsArray.getJSONObject(0).getString(KEY15));
        assertEquals(type, jsonRsTransactionsArray.getJSONObject(0).getString(TYPE));

        assertEquals(2, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Upload")
            .getJSONArray("Documents")
            .length());
        assertEquals("agent102_527663_1379929960951",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(0)
                    .getString("documentName"));
        assertEquals(2, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Upload")
            .getJSONArray("Documents")
            .getJSONObject(0)
            .getJSONArray("pages")
            .length());
        assertEquals("doc1",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(0)
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("fileName"));
        assertEquals("doc3",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(0)
                    .getJSONArray("pages")
                    .getJSONObject(1)
                    .getString("fileName"));

        assertEquals("agent102_527663_1379929960952",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(1)
                    .getString("documentName"));
        assertEquals(2, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject("Upload")
            .getJSONArray("Documents")
            .getJSONObject(1)
            .getJSONArray("pages")
            .length());
        assertEquals("doc10",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(1)
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("fileName"));
        assertEquals("doc11",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Upload")
                    .getJSONArray("Documents")
                    .getJSONObject(1)
                    .getJSONArray("pages")
                    .getJSONObject(1)
                    .getString("fileName"));

        // get first document object
        String getDocumentRequest = getRequest("documentRequest/getDocumentRequest.json");
        getDocumentRequest =
                getDocumentRequest.replace("{1}", eappIdFirst).replace("{2}", "agent102_527663_1379929960951");
        // System.out.println(getDocumentRequest);
        String getDocumentResponse = documentService.getDocument(getDocumentRequest);
        // System.out.println(getDocumentResponse);
        assertNotNull(getDocumentResponse);
        jsonRsTransactionsArray =
                new JSONObject(getDocumentResponse).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        assertEquals(eappIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY4));
        assertEquals("agent102_527663_1379929960951",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getString("documentName"));
        assertEquals(2,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .length());
        assertEquals("doc1",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("fileName"));
        assertEquals(doc1,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("base64string"));
        assertEquals("doc3",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(1)
                    .getString("fileName"));
        assertEquals(doc3,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(1)
                    .getString("base64string"));

        // get second document object
        getDocumentRequest = getRequest("documentRequest/getDocumentRequest.json");
        getDocumentRequest =
                getDocumentRequest.replace("{1}", eappIdFirst).replace("{2}", "agent102_527663_1379929960952");
        getDocumentResponse = documentService.getDocument(getDocumentRequest);
        // System.out.println(getDocumentResponse);
        assertNotNull(getDocumentResponse);
        jsonRsTransactionsArray =
                new JSONObject(getDocumentResponse).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        assertEquals(eappIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY4));
        assertEquals("agent102_527663_1379929960952",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getString("documentName"));
        assertEquals(2,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .length());
        assertEquals("doc10",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("fileName"));
        assertEquals(doc10,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("base64string"));
        assertEquals("doc11",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(1)
                    .getString("fileName"));
        assertEquals(doc11,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(1)
                    .getString("base64string"));

        // uploading third document object(signature)
        String signatureContent = "[{\"lx\":140,\"ly\":45,\"mx\":140,\"my\":44},{\"lx\":136,\"ly\":47,\"mx\":136,\"my\":46}]";
        uploadDocumentRequest = getRequest("documentRequest/UploadDocumentRequest_signature.json");
        uploadDocumentRequest =
                uploadDocumentRequest.replace("{1}", eappIdFirst)
                    .replace("{2}", "doc21")
                    .replace(
                            "{3}", StringEscapeUtils.escapeJava(signatureContent))                    
                    .replace("{4}", "agent102_527663_1379929960953");
        // System.out.println(uploadDocumentRequest);
        uploadDocumentResponse = documentService.uploadDocument(uploadDocumentRequest);
        // System.out.println(uploadDocumentResponse);
        assertNotNull(uploadDocumentResponse);

        jsonRsTransactionsArray =
                new JSONObject(uploadDocumentResponse).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertNotNull(jsonObj);
        jsonObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(jsonObj);
        assertEquals(statusCode100, jsonObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, jsonObj.getString(STATUS));
        assertEquals("Successfully uploaded document", jsonObj.getString(STATUS_MESSAGE));

        assertTrue(tempDirectory.exists());
        fileList = tempDirectory.list();
        assertEquals(5, fileList.length);
        assertTrue(Arrays.asList(fileList).contains("doc1"));
        assertTrue(Arrays.asList(fileList).contains("doc3"));
        assertTrue(Arrays.asList(fileList).contains("doc10"));
        assertTrue(Arrays.asList(fileList).contains("doc11"));
        assertTrue(Arrays.asList(fileList).contains("doc21"));

        // get third document object (signature)
        getDocumentRequest = getRequest("documentRequest/getDocumentRequest.json");
        getDocumentRequest =
                getDocumentRequest.replace("{1}", eappIdFirst).replace("{2}", "agent102_527663_1379929960953");
        // System.out.println(getDocumentRequest);
        getDocumentResponse = documentService.getDocument(getDocumentRequest);
        // System.out.println(getDocumentResponse);
        assertNotNull(getDocumentResponse);
        jsonRsTransactionsArray =
                new JSONObject(getDocumentResponse).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        assertEquals(eappIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY4));
        assertEquals("agent102_527663_1379929960953",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getString("documentName"));
        assertEquals(1,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .length());
        assertEquals("doc21",
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("fileName"));
        assertEquals(signatureContent,
                jsonRsTransactionsArray.getJSONObject(0)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject("Documents")
                    .getJSONArray("pages")
                    .getJSONObject(0)
                    .getString("fileContent"));
        
        
        //Deleting the first document
        String deleteDocRequest = getRequest("documentRequest/UploadDocumentRequest.json");
        workingDir = System.getProperty("user.dir");
        doc1 = Base64Utils.encodeFileToBase64Binary(workingDir+"/src/test/resources/documentRequest/img1.jpg");
        doc2 = Base64Utils.encodeFileToBase64Binary(workingDir+"/src/test/resources/documentRequest/img2.jpg");
        deleteDocRequest =
        	deleteDocRequest.replace("{1}", eappIdFirst)
                    .replace("{2}", "doc1")
                    .replace("{3}",doc1)
                    .replace("{4}", "doc2")
                    .replace("{5}",doc2)
                    .replace("{6}", "agent102_527663_1379929960951")
        			.replace("{7}", "Deleted"); 
        String deleteDocResponse = documentService.uploadDocument(deleteDocRequest);
        assertNotNull(deleteDocResponse);

        jsonRsTransactionsArray =
                new JSONObject(deleteDocResponse).getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(jsonObj);
        assertEquals(statusCode100, jsonObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, jsonObj.getString(STATUS));
        assertEquals("Successfully uploaded document", jsonObj.getString(STATUS_MESSAGE));

        fileList=tempDirectory.list();
        assertTrue(tempDirectory.exists());
        assertEquals(3, fileList.length);
        assertTrue(!Arrays.asList(fileList).contains("doc1"));
        assertTrue(!Arrays.asList(fileList).contains("doc2"));
        
        
//        for (String fileName : fileList) {
//            final File file = new File(tempDirectory.getPath() + "\\" + fileName);
//            assertTrue(file.delete());
//        }
//        assertTrue(tempDirectory.delete());

    }

    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(new String(line.getBytes(), "UTF-8"));
        }

        return sb.toString();
    }

    /**
     * Stprule.
     */
    @Before
    public void stprule() {

        try {

            QueryRunner quryrnr = new QueryRunner(dataSource);

            quryrnr.update("Drop table IF EXISTS `RULE_TABLE`;");

            quryrnr.update("CREATE TABLE RULE_TABLE (Id int(11) NOT NULL AUTO_INCREMENT,RuleName varchar(100) DEFAULT NULL,RuleDesc varchar(8000) DEFAULT NULL,Category varchar(20) DEFAULT NULL,ClientId varchar(20) DEFAULT NULL,PRIMARY KEY (Id))");

            quryrnr.update("INSERT INTO RULE_TABLE (RuleName, RuleDesc, Category, ClientId) VALUES ('STP_InitValidation','function STP_InitValidation(inoutMap){var STPStatus = \"Pass\"; updateJSON(inoutMap, \"STPStatus\", STPStatus);ValidationResults = {}; updateJSON(inoutMap, \"ValidationResults\", ValidationResults);return JSON.stringify(inoutMap);}','STPRule', 'INS');");

            quryrnr.update("INSERT INTO RULE_TABLE (RuleName, RuleDesc, Category, ClientId) VALUES ('STPRules', 'function STPRules(inoutMap) {STP_InitValidation(inoutMap); LowerAgeLimit(inoutMap); UpperAgeLimit(inoutMap); MinSumAssuredLimit(inoutMap); MaxSumAssuredLimit(inoutMap); AnnualIncomeLimit(inoutMap); IsNonMedicalCase(inoutMap); return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO RULE_TABLE (RuleName, RuleDesc, Category, ClientId) VALUES ('LowerAgeLimit', 'function LowerAgeLimit(inoutMap) {dobStr = (inoutMap.TransactionData.Insured.BasicDetails.dob).replace(/-/g,\"/\"); age = getAge(dobStr); if (Number(age) < Number(inoutMap.TransactionData.Product.ProductDetails.minInsuredAge) ) {inoutMap.ValidationResults.LowerAgeLimit = (\"Insured Age less than allowed.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO RULE_TABLE (RuleName, RuleDesc, Category, ClientId) VALUES ('UpperAgeLimit', 'function UpperAgeLimit(inoutMap) {dobStr = (inoutMap.TransactionData.Insured.BasicDetails.dob).replace(/-/g,\"/\"); age = getAge(dobStr); if (Number(age) > Number(inoutMap.TransactionData.Product.ProductDetails.maxInsuredAge) ) {inoutMap.ValidationResults.UpperAgeLimit = (\"Insured Age greater than allowed.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO RULE_TABLE (RuleName, RuleDesc, Category, ClientId) VALUES ('MinSumAssuredLimit', 'function MinSumAssuredLimit(inoutMap) {sumAssured = (inoutMap.TransactionData.Product.ProductDetails.sumAssured); if (Number(sumAssured) < Number(inoutMap.TransactionData.Product.ProductDetails.minSumAssured)){inoutMap.ValidationResults.MinSumAssuredLimit = (\"SumAssured Less than Minimum Sum Assured for the Product.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO RULE_TABLE (RuleName, RuleDesc, Category, ClientId) VALUES ('MaxSumAssuredLimit', 'function MaxSumAssuredLimit(inoutMap) {sumAssured = (inoutMap.TransactionData.Product.ProductDetails.sumAssured); if (Number(sumAssured) > Number(inoutMap.TransactionData.Product.ProductDetails.maxSumAssured)){inoutMap.ValidationResults.MaxSumAssuredLimit = (\"SumAssured Greater than Maximum Sum Assured for the Product.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO RULE_TABLE (RuleName, RuleDesc, Category, ClientId) VALUES ('AnnualIncomeLimit', 'function AnnualIncomeLimit(inoutMap) {annualIncome = (inoutMap.TransactionData.Insured.IncomeDetails.annualIncome); if (Number(annualIncome) < 10000){inoutMap.ValidationResults.AnnualIncomeLimit = (\"Annual Income lesser than limit.\");inoutMap.STPStatus = \"Declined\";} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");

            quryrnr.update("INSERT INTO RULE_TABLE (RuleName, RuleDesc, Category, ClientId) VALUES ('IsNonMedicalCase', 'function IsNonMedicalCase(inoutMap) {nonMedicalCase = (inoutMap.TransactionData.Questionnaire.HealthDetails.isNonMedicalCase); if (nonMedicalCase == \"true\"){inoutMap.ValidationResults.IsNonMedicalCase = (\"Non Medical Case.\");} else {inoutMap.ValidationResults.IsNonMedicalCase = (\"Medical Case.\");} return JSON.stringify(inoutMap);}', 'STPRule', 'INS')");
            quryrnr.update("INSERT INTO CORE_PROD_TEMPLATES(template_id, country_code, language, template_name, version) VALUES (1, NULL, 'en', 'report7.jasper', '1.0'),(321, NULL, 'en', 'eAppJasperTemplate.jasper', '1.0');");

        } catch (SQLException e) {
            // System.out.println("=======================================> Error :" + e.getMessage());
        }

    }

    /**
     * Cleanup.
     */
    @After
    public void cleanup() {
        try {
            QueryRunner queryRunner = new QueryRunner(dataSource);
            queryRunner.update("DELETE FROM `RULE_TABLE`;");
            queryRunner.update("DELETE FROM `CORE_PROD_TEMPLATES`;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
