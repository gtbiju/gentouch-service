package com.cognizant.insurance.omnichannel.component;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationInteractionTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.CommunicationStatusCodeList;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
import com.cognizant.insurance.omnichannel.vo.AgentData;
import com.cognizant.insurance.omnichannel.vo.ManagerialLevelUserDetails;
import com.cognizant.insurance.party.component.PartyComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public interface GeneraliPartyComponent extends PartyComponent  {

	Response<Set<Insured>> retrieveAdditionalInsureds(Request<Agreement> request);
	
	 /**
     * Gets the customer count.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the customer count
     */
    Response<List<SearchCountResult>> getRelatedTransactionCountForLMS(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);
    Response<AgreementProducer> retrieveAgent(Request<Agreement> agreementRequest);

	Response<List<String>> retrieveAgentwith50lessFollowUps(
			Request<List<String>> customerRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);


	/*Response<List<String>> retrieveAgentwith50MaximumYTD(
			Request<List<String>> customerRequest,
			Request<List<CustomerStatusCodeList>> statusClosedRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);*/
	
	Response<List<AgentData>> retrieveAgentwithMaximumYTD(
			Request<List<AgentData>> agentDataRequest,
			Request<List<CustomerStatusCodeList>> statusClosedRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);
	public Response<Customer> retrieveCustomerWithTransTrackingId(Request<String> trackId,
            final Request<List<CustomerStatusCodeList>> statusRequest);

	Response<ManagerialLevelUserDetails> retrieveByStatusForManagerialLevel(
			Request<ManagerialLevelUserDetails> managerialLevelUserDetailsRequest,
			List<CustomerStatusCodeList> statusListNew,
			List<CustomerStatusCodeList> statusListClosed,
			List<CustomerStatusCodeList> statusListFollowUp);

	Response<SearchCountResult> getCustomerCountRetrieveFilter(
			Request<SearchCriteria> searchCriteriatRequest,
			List<CustomerStatusCodeList> statusRequest, List<CustomerStatusCodeList> statusListNew);

	Map<String,Integer> retrieveMonthlySalesActivityCustomers(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest, Request<List<CommunicationStatusCodeList>> communicationStatusRequest, 
			Request<List<CommunicationInteractionTypeCodeList>> communicationInteractionCodeRequest);

	Map<String,Integer> retrieveMonthwiseSalesActivityCustomers(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

	Map<String, Integer> retrieveWeekwiseSalesActivityCustomers(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

	Map<String, Integer> retrieveWeekwiseNewProspect(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

	Map<String, Integer> retrieveMonthwiseNewProspect(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

	Response<List<Customer>> retrieveMonthlyNewProspect(Request<SearchCriteria> searchCriteriatRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

    Map<String, String> retrieveMonthlyNewCount(Request<SearchCriteria> searchCriteriatRequest);

    Map<String, Map<String, String>> retrieveLeadStatistics(Request<SearchCriteria> searchCriteriatRequest);

	
	

}
