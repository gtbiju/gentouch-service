/**
 * 
 */
package com.cognizant.insurance.omnichannel.scheduler;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.omnichannel.component.CheckinComponent;

/**
 * @author 397850
 *
 */
@Component
@EnableScheduling
public class GeneraliCheckoutScheduler {
	
	@Autowired
	private CheckinComponent checkinComponent;
	
	
	@Scheduled(cron = "${le.platform.service.checkout.schedule}")
	public void runScheduler(){
		
		try {
			checkinComponent.updateCheckinDetails();
		} catch (ParseException e) {
			
		}
		
		
		
		
	}

}
