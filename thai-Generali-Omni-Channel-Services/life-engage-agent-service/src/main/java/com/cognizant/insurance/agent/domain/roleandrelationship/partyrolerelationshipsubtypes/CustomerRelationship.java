package com.cognizant.insurance.agent.domain.roleandrelationship.partyrolerelationshipsubtypes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.agent.domain.roleandrelationship.PartyRoleRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;

@Entity
public class CustomerRelationship extends PartyRoleRelationship {

	/**
	 * 
	 */
	private static final long  serialVersionUID = 3732866708326016097L;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
	public Customer relatedCustomer;


	public Customer getRelatedCustomer() {
		return relatedCustomer;
	}


	public void setRelatedCustomer(Customer relatedCustomer) {
		this.relatedCustomer = relatedCustomer;
	}
	
	
	

}
