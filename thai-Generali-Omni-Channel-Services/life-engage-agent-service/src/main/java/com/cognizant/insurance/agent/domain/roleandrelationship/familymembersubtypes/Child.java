/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.familymembersubtypes;

import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.FamilyMember;

/**
 * This concept represents a child.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Child extends FamilyMember {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7065826017464529764L;
}
