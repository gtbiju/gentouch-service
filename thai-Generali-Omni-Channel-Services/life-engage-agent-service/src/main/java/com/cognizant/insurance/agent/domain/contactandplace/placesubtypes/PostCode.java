/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.placesubtypes;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.agent.domain.contactandplace.Place;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.PostalSubSystemCodeList;

/**
 * A postal code (known in various countries as a post code, postcode, or ZIP
 * code) is a series of letters and/or digits appended to a postal address for
 * the purpose of sorting mail.
 * 
 * SOURCE: http://en.wikipedia.org/wiki/Post_code
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("POSTCODE")
public class PostCode extends Place {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7381378927614664830L;

    /**
     * Value of the post code.
     * 
     * REFERENCE: http://en.wikipedia.org/wiki/Postal_code
     * 
     * USA SOURCE: http://www.usps.com/
     * 
     * 
     * 
     * 
     * 
     * 
     */
    @OneToOne (cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    private ExternalCode assignedCode;

    /**
     * Value of the postCode extension.
     * 
     * 
     * 
     * 
     * 
     */
    private String assignedCodeExtension;

    /**
     * Indicates the type of postal sub-system (if applicable).
     * 
     * e.g. CEDEX in France
     * 
     * 
     * 
     */
    @Enumerated
    private PostalSubSystemCodeList subSystemTypeCode;

    /**
     * Gets the assignedCode.
     * 
     * @return Returns the assignedCode.
     */
    public ExternalCode getAssignedCode() {
        return assignedCode;
    }

    /**
     * Gets the assignedCodeExtension.
     * 
     * @return Returns the assignedCodeExtension.
     */
    public String getAssignedCodeExtension() {
        return assignedCodeExtension;
    }

    /**
     * Gets the subSystemTypeCode.
     * 
     * @return Returns the subSystemTypeCode.
     */
    public PostalSubSystemCodeList getSubSystemTypeCode() {
        return subSystemTypeCode;
    }

    /**
     * Sets The assignedCode.
     * 
     * @param assignedCode
     *            The assignedCode to set.
     */
    public void setAssignedCode(final ExternalCode assignedCode) {
        this.assignedCode = assignedCode;
    }

    /**
     * Sets The assignedCodeExtension.
     * 
     * @param assignedCodeExtension
     *            The assignedCodeExtension to set.
     */
    public void setAssignedCodeExtension(
            final String assignedCodeExtension) {
        this.assignedCodeExtension = assignedCodeExtension;
    }

    /**
     * Sets The subSystemTypeCode.
     * 
     * @param subSystemTypeCode
     *            The subSystemTypeCode to set.
     */
    public void setSubSystemTypeCode(
            final PostalSubSystemCodeList subSystemTypeCode) {
        this.subSystemTypeCode = subSystemTypeCode;
    }
}
