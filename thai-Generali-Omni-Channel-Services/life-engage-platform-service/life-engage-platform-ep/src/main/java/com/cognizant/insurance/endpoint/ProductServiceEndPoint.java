/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LifeEngageProductService;

/**
 * The Class class LifeEngageSyncEndPoint.
 * 
 * @author 180790
 */
@Controller
@RequestMapping("/lifeEngageProductService")
public class ProductServiceEndPoint {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageSyncEndPoint.class);

    public static final String VERSION_TWO = "2";
    
    public static final String VERSION_ONE = "1";
    
    /** The life engage product service. */
    @Autowired
    private LifeEngageProductService lifeEngageProductService;

    /**
     * Retrieve all products.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/getProduct", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getProduct(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getProduct(json, VERSION_ONE);

            }
        }.execute(json);
    }

    /**
     * Used to check whether a product is active or not
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/isProductActive", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String isProductActive(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.isProductActive(json, VERSION_ONE);

            }
        }.execute(json);
    }

    /**
     * Retrieve all products basic details.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/getProducts", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getProducts(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getProducts(json);

            }
        }.execute(json);
    }

    /**
     * Retrieve all products basic details.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/getActiveProducts", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getActiveProducts(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getActiveProducts(json);

            }
        }.execute(json);
    }
    
    /**
     * This method retrieves all (both active and inactive) products for the given filter condition.
     * 
     * @param json
     *            the json :Filter params
     *            	- carrierId
	 *				- channelId
     * @return the string
     */
    @RequestMapping(value = "/getAllProductsByFilter", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getAllProductsByFilter(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getAllProductsByFilter(json, VERSION_ONE);

            }
        }.execute(json);
    }
    
    /**
     * Retrieve all active products for the given filter condition.
     * 
     * @param json
     *            the json :Filter params
     *            	- carrierId
	 *				- channelId
     * @return the string
     */
    @RequestMapping(value = "/getAllActiveProductsByFilter", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getAllActiveProductsByFilter(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getAllActiveProductsByFilter(json, VERSION_ONE);

            }
        }.execute(json);
    }

    /**
     * Retrieve products with specified details in filter param.
     * 
     * @param json
     *          - Products Ids : ids of the products that we need the details
     *          - Filter params
     *             -- all : true/false (used to get all details about the product);
     *                   OR
     *             -- Combination of following properties
     *                  -- properties : true/false (Used to identify whether properties are required or not. Default value is false)
     *                  -- riders : true/false (Used to identify whether riders are required or not. Default value is false)
     *                  -- plans: true/false (Used to identify whether plans are required or not. Default value is false)
     *                  -- channels: true/false (Used to identify whether channels are required or not. Default value is false)
     *                  -- funds: true/false (Used to identify whether funds are required or not. Default value is false)
     *                  -- ruleSpec: true/false (Used to identify whether ruleSpec are required or not. Default value is false)
     *                  -- calcSpec: true/false (Used to identify whether calcSpec are required or not. Default value is false)
     *                  -- collaterals: true/false (Used to identify whether collaterals are required or not. Default value is false)
     *                  -- templates: true/false (Used to identify whether templates are required or not. Default value is false)
     *                  -- offlineDB: true/false (Used to identify whether offlineDB are required or not. Default value is false)
     *    
     *              -- inActiveRiders : true/false (used to identify whether inactive rider is required or not. 
     *                      This is relevant only if either "all" OR "property" option is true. Default value is false)
     *              -- inActivePlans : true/false (used to identify whether inactive plan is required or not. 
     *                      This is relevant only if either "all" OR "plans" option is true. Default value is false)
     * @return the string
     */
    @RequestMapping(value = "/getProductsByFilter", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getProductsByFilter(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getProductsByFilter(json, VERSION_ONE);

            }
        }.execute(json);
    }
    
    /**
     * Retrieve product templates with specified details in filter param.
     * 
     * @param json
     *          - Id : product id
     *          - carrierCode : carrier code
     *          - Filter params
     *             -- Combination of following properties
     *                  -- templateType : template type
     *                  -- templateLanguage : language
     *                  -- templateCountryCode: template country code
     * @return the string
     */
    @RequestMapping(value = "/getProductTemplatesByFilter", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String getProductTemplatesByFilter(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getProductTemplatesByFilter(json);

            }
        }.execute(json);
    }
    
    
    /**
     * Used to check whether a product is active or not
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = {"/V2/isProductActive"}, method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String isProductActiveV2(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.isProductActive(json, VERSION_TWO);

            }
        }.execute(json);
    }
    
    
    @RequestMapping(value = {"/V2/getAllProductsByFilter"}, method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getAllProductsByFilterV2(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getAllProductsByFilter(json, VERSION_TWO);

            }
        }.execute(json);
    }
    
    @RequestMapping(value = {"/V2/getAllActiveProductsByFilter"}, method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getAllActiveProductsByFilterV2(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getAllActiveProductsByFilter(json, VERSION_TWO);

            }
        }.execute(json);
    }
    
    @RequestMapping(value = {"/V2/getProduct"}, method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getProductV2(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getProduct(json, VERSION_TWO);

            }
        }.execute(json);
    }
    
    
    @RequestMapping(value = {"/V2/getProducts"}, method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getProductsV2(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getProducts(json);

            }
        }.execute(json);
    }

   

    @RequestMapping(value = {"/V2/getActiveProducts"} ,method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getActiveProductsV2(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getActiveProducts(json);

            }
        }.execute(json);
    }
    
    
    @RequestMapping(value = {"/V2/getProductsByFilter"}, method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getProductsByFilterV2(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageProductService.getProductsByFilter(json, VERSION_TWO);

            }
        }.execute(json);
    }

    
    
    /**
     * @return the lifeEngageProductService
     */
    public final LifeEngageProductService getLifeEngageProductService() {
        return lifeEngageProductService;
    }

    /**
     * @param lifeEngageProductService
     *            the lifeEngageProductService to set
     */
    public final void setLifeEngageProductService(final LifeEngageProductService lifeEngageProductService) {
        this.lifeEngageProductService = lifeEngageProductService;
    }

}
