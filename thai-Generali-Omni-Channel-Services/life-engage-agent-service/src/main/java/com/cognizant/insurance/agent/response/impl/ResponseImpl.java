/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304007
 * @version       : 0.1, Nov 28, 2012
 */
package com.cognizant.insurance.agent.response.impl;

import com.cognizant.insurance.agent.response.Response;

/**
 * The Class class JPAResponseImpl.
 * 
 * @param <T>
 *            the generic type
 */
public class ResponseImpl<T> implements Response<T> {

    /** The type. */
    private T type;

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.response.Response#setType(java.lang.Object)
     */
    @Override
    public final void setType(final T type) {
        this.type = type;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.response.Response#getType()
     */
    @Override
    public final T getType() {
        return type;
    }

}
