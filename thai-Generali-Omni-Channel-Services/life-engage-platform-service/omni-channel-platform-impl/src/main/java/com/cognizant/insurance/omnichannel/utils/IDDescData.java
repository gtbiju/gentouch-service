package com.cognizant.insurance.omnichannel.utils;
/**
 * 
 * @author 390229
 * Class for doing ID and Desc of tables
 */
public class IDDescData {
	
	private String id;
	
	private String description;

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	

}
