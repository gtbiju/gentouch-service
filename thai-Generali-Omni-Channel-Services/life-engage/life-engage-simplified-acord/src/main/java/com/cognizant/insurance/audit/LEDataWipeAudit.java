package com.cognizant.insurance.audit;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class LEDataWipeAudit {
    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /** The user name. */
    private String userName;

    /** The creation dateand time. */
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataWipeDateAndTime;

    @Enumerated
    private DataWipeReasonCodeList reason;


    private String deviceID;

    private String deviceType;

    private String osVersion;

    private String deviceModel;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the dataWipeDateAndTime
     */
    public Date getDataWipeDateAndTime() {
        return dataWipeDateAndTime;
    }

    /**
     * @param dataWipeDateAndTime
     *            the dataWipeDateAndTime to set
     */
    public void setDataWipeDateAndTime(Date dataWipeDateAndTime) {
        this.dataWipeDateAndTime = dataWipeDateAndTime;
    }

    /**
     * @return the reason
     */
    public DataWipeReasonCodeList getReason() {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(DataWipeReasonCodeList reason) {
        this.reason = reason;
    }

    /**
     * @return the deviceID
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * @param deviceID
     *            the deviceID to set
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     * @return the deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType
     *            the deviceType to set
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return the osVersion
     */
    public String getOsVersion() {
        return osVersion;
    }

    /**
     * @param osVersion
     *            the osVersion to set
     */
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    /**
     * @return the deviceModel
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * @param deviceModel
     *            the deviceModel to set
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

}
