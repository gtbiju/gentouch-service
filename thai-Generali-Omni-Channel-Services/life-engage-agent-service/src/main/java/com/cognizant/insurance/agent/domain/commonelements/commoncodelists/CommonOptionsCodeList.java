/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Common Options.
 * 
 * @author 301350
 * 
 * 
 */
public enum CommonOptionsCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Included,
    /**
     * 
     * 
     * 
     * 
     */
    Suspended,
    /**
     * 
     * 
     * 
     * 
     */
    Excluded,
    /**
     * 
     * 
     * 
     * 
     */
    Reinstated,
    /**
     * 
     * 
     * 
     * 
     */
    Rejected
}
