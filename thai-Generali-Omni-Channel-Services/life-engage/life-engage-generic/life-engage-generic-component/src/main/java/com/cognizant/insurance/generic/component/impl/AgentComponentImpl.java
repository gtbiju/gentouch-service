package com.cognizant.insurance.generic.component.impl;

import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.generic.component.AgentComponent;
import com.cognizant.insurance.generic.dao.AgentDao;
import com.cognizant.insurance.generic.dao.impl.AgentDaoImpl;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;

/**
 * The Class AgentComponentImpl.
 */
public class AgentComponentImpl implements AgentComponent {
	
	

	/* (non-Javadoc)
	 * @see com.cognizant.insurance.generic.component.AgentComponent#retrieveAgentByCode(com.cognizant.insurance.request.Request)
	 */
	@Override
	public Response<Agent> retrieveAgentByCode(Request<Agent> agentRequest) {
		final AgentDao agentDao = new AgentDaoImpl();
		Response<Agent> agentResponse = new ResponseImpl<Agent>();
		agentResponse.setType(agentDao.retrieveAgentByCode(agentRequest));
		return agentResponse;
	}

}
