/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.icr.pdf.component;

import java.io.ByteArrayOutputStream;
import java.util.List;

import com.cognizant.icr.pdf.file.FileNameGenerator;
import com.cognizant.icr.pdf.repository.FileRepository;
import com.cognizant.icr.pdf.request.PDFRequest;
import com.cognizant.icr.pdf.response.ResponseBean;

/**
 * The Interface PDFComponent.
 *
 * @param <T> the generic type
 */
public interface PDFComponent<T> {


	/**
	 * Generate pdf.
	 *
	 * @param pdfRequest the pdf request
	 * @return the list
	 */
	List<ResponseBean> generatePdf(PDFRequest<T> pdfRequest);
	
	
	/**
	 * Creates the pdf.
	 *
	 * @param pdfRequest the pdf request
	 * @return the file output stream
	 */
	ByteArrayOutputStream createPdf(PDFRequest<T> pdfRequest);
	
	
	/**
	 * Gets the file name generator.
	 *
	 * @return the file name generator
	 */
	FileNameGenerator getFileNameGenerator();
	
	/**
	 * Sets the file name generator.
	 *
	 * @param fileNameGenerator the file name generator to set.
	 */
	void setFileNameGenerator(FileNameGenerator fileNameGenerator);
	
	/**
	 * Gets the file repository.
	 *
	 * @return the file repository
	 */
	FileRepository getFileRepository();
	
	/**
	 * Sets the file repository.
	 *
	 * @param fileRepository the file repository to set.
	 */
	void setFileRepository(FileRepository fileRepository);
	
	/**
	 * Creates the encrypted pdf.
	 *
	 * @param pdfRequestImpl the pdf request impl
	 * @return the byte array output stream
	 */
	ByteArrayOutputStream createEncryptedPdf(final PDFRequest<T> pdfRequestImpl);
	
	/**
	 * Generate encrypted pdf.
	 *
	 * @param pdfRequestImpl the pdf request impl
	 * @return the list
	 */
	List<ResponseBean> generateEncryptedPdf(final PDFRequest<T> pdfRequestImpl);
	
}
