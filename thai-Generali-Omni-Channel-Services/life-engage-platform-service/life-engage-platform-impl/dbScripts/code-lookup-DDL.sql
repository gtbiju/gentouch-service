/****** Object:  Table [dbo].[CODE_LOCALE_LOOKUP]    Script Date: 11/16/2016 9:53:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CODE_LOCALE_LOOKUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[COUNTRY] [varchar](255) NULL,
	[LANGUAGE] [varchar](255) NULL,
	[VALUE] [nvarchar](500) NULL,
	[LOOKUPCODE_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CODE_LOOKUP]    Script Date: 11/16/2016 9:53:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CODE_LOOKUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[CODE] [varchar](255) NULL,
	[DESCRIPTION] [nvarchar](500) NULL,
	[IS_DEFAULT] [int] NULL,
	[CODETYPE_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CODE_RELATION]    Script Date: 11/16/2016 9:53:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CODE_RELATION](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[CHILD_ID] [bigint] NULL,
	[PARENT_ID] [bigint] NULL,
	[TYPE_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CODE_TYPE_LOOKUP]    Script Date: 11/16/2016 9:53:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CODE_TYPE_LOOKUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](255) NULL,
	[CARRIER_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CODE_LOOKUP]  WITH CHECK ADD  CONSTRAINT [FKE401E00C37C1D23A] FOREIGN KEY([CODETYPE_ID])
REFERENCES [dbo].[CODE_TYPE_LOOKUP] ([id])
GO
ALTER TABLE [dbo].[CODE_LOOKUP] CHECK CONSTRAINT [FKE401E00C37C1D23A]
GO
ALTER TABLE [dbo].[CODE_TYPE_LOOKUP]  WITH CHECK ADD  CONSTRAINT [FK2C4EE6CD778CA37D] FOREIGN KEY([CARRIER_ID])
REFERENCES [dbo].[CORE_CARRIER] ([id])
GO
ALTER TABLE [dbo].[CODE_TYPE_LOOKUP] CHECK CONSTRAINT [FK2C4EE6CD778CA37D]
GO
