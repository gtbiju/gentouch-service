/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 19, 2012
 */
package com.cognizant.insurance.response;

/**
 * The Interface interface Response.
 * 
 * @param <T>
 *            the generic type
 */
public interface Response<T> {
    /**
     * Sets the type.
     * 
     * @param type
     *            the type to set.
     */
    void setType(T type);

    /**
     * Gets the type.
     * 
     * @return the type
     */
    T getType();

}
