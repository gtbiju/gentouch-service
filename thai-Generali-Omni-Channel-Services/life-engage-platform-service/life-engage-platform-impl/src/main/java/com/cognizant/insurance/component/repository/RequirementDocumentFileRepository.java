/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 325754
 * @version       : 0.1, Oct 6, 2015
 */
package com.cognizant.insurance.component.repository;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;
import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.agreement.component.AgreementComponent;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.utils.Base64Utils;

/**
 * @author 325754
 * 
 */
public class RequirementDocumentFileRepository {
	/** The entity manager. */
	@PersistenceContext(unitName="LE_Platform")
	private EntityManager entityManager;

	/** The agreement component. */
	@Autowired
	private AgreementComponent agreementComponent;

	/** The Constant E_APP. */
	private static final String E_APP = "eApp";

	/** The Constant ILLUSTRATION. */
	private static final String ILLUSTRATION = "illustration";

	private static final String CONTENT_TYPE_BASE64 = "base64";

	private static final String PATTERN_REPLACE_BASE64 = "data(.*?)\\;base64,";

	private static final String REQUIREMENTSFOLDER = "REQUIREMENTS";

	private static final String DELETEDSTATUS = "Deleted";

	private static final String SAVEDSTATUS = "Saved";

	@Value("${le.platform.service.slash}")
	private String pathSeparator;

	@Value("${le.platform.service.fileUploadPath}")
	private String fileUploadPath;

	private static final String EMPTY_STRING = "";

	private static final String BASE64PREPEND = "data:image/png;base64,";

	/** The logger. */
	public static final Logger LOGGER = LoggerFactory
			.getLogger(RequirementDocumentFileRepository.class);

	/**
	 * Creates the one page in documents.
	 * 
	 * @param document
	 *            the document
	 * @param tempDirectory
	 *            the temp directory
	 * @param documentTypeCode
	 *            the document type code
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	protected void createOnePageInDocument(final Document page,
			final File tempDirectory) throws IOException {
		if (page != null) {
			byte[] decodedBytes = null;
			final String fileName = page.getFileNames();
			// if (null != page.getContentType() &&
			// page.getContentType().equals(CONTENT_TYPE_BASE64)) {
			String fileContent = page.getBase64string();
			fileContent = fileContent.replaceAll(PATTERN_REPLACE_BASE64, "");
			fileContent = fileContent.replaceAll("\n", "");
			decodedBytes = Base64.decodeBase64(fileContent);
			// } else {
			// String fileContent = page.getFileContent();
			// decodedBytes = fileContent.getBytes();
			// }
			if (decodedBytes != null) {
				createFile(fileName, decodedBytes, tempDirectory);
			}

		}
	}

	/**
	 * Creates the file.
	 * 
	 * @param fileName
	 *            the file name
	 * @param fileByteArray
	 *            the file byte array
	 * @param tempDirectory
	 *            the temp directory
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	protected void createFile(final String fileName,
			final byte[] fileByteArray, final File tempDirectory)
			throws IOException {

		if (fileName != null) {

			final File file = new File(tempDirectory.getPath() + pathSeparator
					+ fileName);
			if (file.exists()) {
				if (file.delete()) {
                    FileOutputStream fos = null;
                    try {
                        LOGGER.error("File Deleted: " + file.getPath());
                        final File someFile = new File(tempDirectory, fileName);
                        fos = new FileOutputStream(someFile);
                        fos.write(fileByteArray);

                    } finally {
                        fos.flush();
                        fos.close();
                    }
				} else {
					LOGGER.error("File Deletion Error! : " + file.getPath());
				}
			} else {
			    FileOutputStream fos=null;
                try {
                    final File someFile = new File(tempDirectory, fileName);
                    fos = new FileOutputStream(someFile);
                    fos.write(fileByteArray);

                } finally {
                    fos.flush();
                    fos.close();
                }
			}
		}
	}

	/**
	 * Creates the directory.
	 * 
	 * @param tempDirectory
	 *            the temp directory
	 */
	protected void createDirectory(final File tempDirectory) {
		if (!tempDirectory.exists() && !tempDirectory.mkdirs()) {
			LOGGER.error("Failed to create directory : "
					+ tempDirectory.getPath());
			throwSystemException(true, Constants.FAILED_DIRECTORY_CREATION);
		}
	}

	/**
	 * Delete unwanted files.
	 * 
	 * @param documents
	 *            the documents
	 * @param tempDirectory
	 *            the temp directory
	 */
	protected void deleteUnwantedFiles(final Set<String> documents,
			final File tempDirectory) {
		for (String documentName : documents) {
			final File file = new File(tempDirectory.getPath() + pathSeparator
					+ documentName);
			if (!file.delete()) {
				LOGGER.error("File Deletion Error! : " + file.getPath());
			}
		}
	}

	/**
	 * @param transactions
	 * @throws IOException
	 */
	public void saveRequirementDocFile(Transactions transaction,
			String transactionId) throws BusinessException, IOException {
		ContextTypeCodeList contextTypeCodeList = null;
		Document docToBeSaved = null;
		Document pageToSave = null;
		// Requirement requirementToSave = null;
		if (CollectionUtils.isNotEmpty(transaction.getRequirements())) {
			for (Requirement requirementInRequest : transaction
					.getRequirements()) {
				// requirementToSave = requirementInRequest;-
				List<AgreementDocument> documents = requirementInRequest
						.getRequirementDocuments();
				if (CollectionUtils.isNotEmpty(documents)) {
					docToBeSaved = documents.get(0);
					if (null != docToBeSaved
							&& CollectionUtils.isNotEmpty(docToBeSaved
									.getPages())) {
						for (Document doc : docToBeSaved.getPages()) {
							pageToSave = doc;
							break;
						}
					}
					break;
				} else {
					throwBusinessException(true, Constants.FILE_DETAILS_MISSING);
				}
			}
		} else {
			throwBusinessException(true, Constants.REQUIREMENT_DETAILS_MISSING);
		}
		List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();
		String type = transaction.getType();
		String id = EMPTY_STRING;
		if (E_APP.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.EAPP;
			id = transaction.getProposalNumber();
			statusList = getAgreementStatusCodesFor(type);

		} else if (ILLUSTRATION.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.ILLUSTRATION;
			id = transaction.getIllustrationId();
			statusList = getAgreementStatusCodesFor(type);
		}

		// get the Agreement object with proposal number final

		final Request<Agreement> agreementRequest = new JPARequestImpl<Agreement>(
				entityManager, contextTypeCodeList, transactionId);
		final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
		insuranceAgreement.setIdentifier(id);
		agreementRequest.setType(insuranceAgreement);

		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				entityManager, contextTypeCodeList, transactionId);
		statusRequest.setType(statusList);
		insuranceAgreement.setRequirements(transaction.getRequirements());
		// final Response<Agreement> agreementsResponse =
		// agreementComponent.getInsuranceAgreement
		// ForReqDocUpdate(agreementRequest, statusRequest);
		final File tempDirectory = new File(fileUploadPath + id + pathSeparator
				+ REQUIREMENTSFOLDER);
		createDirectory(tempDirectory);
		if (pageToSave.getDocumentStatus().equals(SAVEDSTATUS)) {
			createOnePageInDocument(pageToSave, tempDirectory);
		} else if (pageToSave.getDocumentStatus().equals(DELETEDSTATUS)) {
			deletePageOfDocument(pageToSave, tempDirectory);
		}
		agreementComponent.updateInsuranceAgreementWithRequirementDocUpdate(
				agreementRequest, statusRequest);
		
	}

	/**
	 * @param docToBeSaved
	 * @param tempDirectory
	 */
	protected void deletePageOfDocument(Document pageToDelete, File tempDirectory) {
		final String fileName = pageToDelete.getFileNames();
		if (fileName != null) {
			final File file = new File(tempDirectory.getPath() + pathSeparator
					+ fileName);
			if (file.exists()) {
				if (file.delete()) {
					LOGGER.error("File Deleted: " + file.getPath());
				} else {
					LOGGER.error("File Deletion Error! : " + file.getPath());
				}
			} else {
				LOGGER.error("File Not exists Error! : " + file.getPath());
			}
		}
	}

	/**
	 * @param transactions
	 * @param transactionId
	 * @throws BusinessException
	 * @throws IOException
	 */
	public Requirement getRequirementDocFileForPDF(Transactions transaction,
			String transactionId) throws BusinessException, IOException {
		ContextTypeCodeList contextTypeCodeList = null;
		Requirement requirement = new Requirement();
		Set<Requirement> requirementSignature = new HashSet<Requirement>();

		if (CollectionUtils.isNotEmpty(transaction.getProposal()
				.getRequirements())) {
			for (Requirement requirementInRequest : transaction.getProposal()
					.getRequirements()) {

				if (requirementInRequest.getRequirementType().toString()
						.equals(DocumentTypeCodeList.Signature.toString())) {
					requirement = requirementInRequest;
					List<AgreementDocument> documents = requirementInRequest
							.getRequirementDocuments();
					if (CollectionUtils.isEmpty(documents)) {
						throwBusinessException(true,
								Constants.DOCUMENT_DETAILS_MISSING);
					} else {
						if (CollectionUtils
								.isEmpty(documents.get(0).getPages())) {
							// To Do : Need to fix this issue
							// throwBusinessException(true,
							// Constants.FILE_DETAILS_MISSING);
						}
					}
					requirementSignature.add(requirementInRequest);
					break;
				}
			}
		} else {
			throwBusinessException(true, Constants.REQUIREMENT_DETAILS_MISSING);
		}
		List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();
		String type = transaction.getType();
		String id = EMPTY_STRING;
		if (E_APP.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.EAPP;
			id = transaction.getProposalNumber();
			statusList = getAgreementStatusCodesFor(type);

		} else if (ILLUSTRATION.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.ILLUSTRATION;
			id = transaction.getIllustrationId();
			statusList = getAgreementStatusCodesFor(type);
		}

		final Request<Agreement> agreementRequest = new JPARequestImpl<Agreement>(
				entityManager, contextTypeCodeList, transactionId);
		final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
		insuranceAgreement.setIdentifier(id);
		agreementRequest.setType(insuranceAgreement);

		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				entityManager, contextTypeCodeList, transactionId);
		statusRequest.setType(statusList);
		insuranceAgreement.setRequirements(requirementSignature);

		final Response<Document> agreementRequirementDocFileResponse = agreementComponent
				.getRequirementDocFile(agreementRequest, statusRequest);
		addFileContent(agreementRequirementDocFileResponse.getType(), id);
		if (null != requirement.getRequirementDocuments()) {
			for (Document doc : requirement.getRequirementDocuments()) {
				if (!CollectionUtils.isEmpty(doc.getPages())) {
					doc.getPages().clear();
					doc.getPages().add(
							agreementRequirementDocFileResponse.getType());
				}
			}
		}

		return requirement;
	}

	/**
	 * @param transactions
	 * @param transactionId
	 * @throws BusinessException
	 * @throws IOException
	 */
	public Requirement getRequirementDocFile(Transactions transaction,
			String transactionId) throws BusinessException, IOException {
		ContextTypeCodeList contextTypeCodeList = null;
		Requirement requirement = new Requirement();
		if (CollectionUtils.isNotEmpty(transaction.getRequirements())) {
			for (Requirement requirementInRequest : transaction
					.getRequirements()) {
				requirement = requirementInRequest;
				List<AgreementDocument> documents = requirementInRequest
						.getRequirementDocuments();
				if (CollectionUtils.isEmpty(documents)) {
					throwBusinessException(true,
							Constants.DOCUMENT_DETAILS_MISSING);
				} else {
					if (CollectionUtils.isEmpty(documents.get(0).getPages())) {
						throwBusinessException(true,
								Constants.FILE_DETAILS_MISSING);
					}
				}
			}
		} else {
			throwBusinessException(true, Constants.REQUIREMENT_DETAILS_MISSING);
		}
		List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();
		String type = transaction.getType();
		String id = EMPTY_STRING;
		if (E_APP.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.EAPP;
			id = transaction.getProposalNumber();
			statusList = getAgreementStatusCodesFor(type);
		} else if (ILLUSTRATION.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.ILLUSTRATION;
			id = transaction.getIllustrationId();
			statusList = getAgreementStatusCodesFor(type);
		}

		final Request<Agreement> agreementRequest = new JPARequestImpl<Agreement>(
				entityManager, contextTypeCodeList, transactionId);
		final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
		insuranceAgreement.setIdentifier(id);
		agreementRequest.setType(insuranceAgreement);

		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				entityManager, contextTypeCodeList, transactionId);
		statusRequest.setType(statusList);
		insuranceAgreement.setRequirements(transaction.getRequirements());

		final Response<Document> agreementRequirementDocFileResponse = agreementComponent
				.getRequirementDocFile(agreementRequest, statusRequest);
		addFileContent(agreementRequirementDocFileResponse.getType(), id);
		for (Document doc : requirement.getRequirementDocuments()) {
			doc.getPages().clear();
			doc.getPages().add(agreementRequirementDocFileResponse.getType());
		}
		return requirement;
	}

	/**
	 * @param agreementDocument
	 * @throws IOException
	 * 
	 *             Changed visibility to protected.
	 */
	protected void addFileContent(Document agreementDocument,
			String proposalNumber) throws IOException {
		final String documentPath = fileUploadPath + proposalNumber
				+ pathSeparator + REQUIREMENTSFOLDER + pathSeparator;
		if (null != agreementDocument) {
			if (new File(documentPath + agreementDocument.getFileNames())
					.exists()) {
				agreementDocument.setBase64string((BASE64PREPEND)
						.concat(Base64Utils
								.encodeFileToBase64Binary(documentPath
										+ agreementDocument.getFileNames())));
			}
		}
	}

	/**
	 * @param responseTransactions
	 * @param transactionId
	 * @return
	 */
	public Requirement getRequirementDocFileList(Transactions transaction,
			String transactionId) throws BusinessException, IOException {
		ContextTypeCodeList contextTypeCodeList = null;
		Requirement requirement = new Requirement();
		if (CollectionUtils.isNotEmpty(transaction.getRequirements())) {
			for (Requirement requirementInRequest : transaction
					.getRequirements()) {
				requirement = requirementInRequest;
				List<AgreementDocument> documents = requirementInRequest
						.getRequirementDocuments();
				if (CollectionUtils.isEmpty(documents)) {
					throwBusinessException(true,
							Constants.DOCUMENT_DETAILS_MISSING);
				}
			}
		} else {
			throwBusinessException(true, Constants.REQUIREMENT_DETAILS_MISSING);
		}
		List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();

		String type = transaction.getType();
		String id = EMPTY_STRING;
		if (E_APP.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.EAPP;
			id = transaction.getProposalNumber();
			statusList = getAgreementStatusCodesFor(type);

		} else if (ILLUSTRATION.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.ILLUSTRATION;
			id = transaction.getIllustrationId();
			statusList = getAgreementStatusCodesFor(type);
		}

		final Request<Agreement> agreementRequest = new JPARequestImpl<Agreement>(
				entityManager, contextTypeCodeList, transactionId);
		final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
		insuranceAgreement.setIdentifier(id);
		agreementRequest.setType(insuranceAgreement);

		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				entityManager, contextTypeCodeList, transactionId);
		statusRequest.setType(statusList);
		insuranceAgreement.setRequirements(transaction.getRequirements());

		final Response<AgreementDocument> agreementRequirementDocFileResponse = agreementComponent
				.getRequirementDocFileList(agreementRequest, statusRequest);
		AgreementDocument docToReturn = new AgreementDocument();
		docToReturn.setName(agreementRequirementDocFileResponse.getType()
				.getName());
		Set<Document> pages = new HashSet<Document>();
		docToReturn.setPages(pages);
		for (Document doc : agreementRequirementDocFileResponse.getType()
				.getPages()) {
			if (!doc.getDocumentStatus().equalsIgnoreCase(DELETEDSTATUS)) {
				addFileContent(doc, id);
				docToReturn.getPages().add(doc);
			}
		}
		List<AgreementDocument> requirementDocuments = new ArrayList<AgreementDocument>();
		requirementDocuments.add(docToReturn);
		requirement.setRequirementDocuments(requirementDocuments);
		return requirement;
	}

	protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(
			String type) {

		List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();

		if (E_APP.equals(type)) {
			statusList.add(AgreementStatusCodeList.New);
			statusList.add(AgreementStatusCodeList.Submitted);
			statusList.add(AgreementStatusCodeList.Confirmed);
			// statusList.add(AgreementStatusCodeList.InProgress);
			// statusList.add(AgreementStatusCodeList.Final);
			// statusList.add(AgreementStatusCodeList.Review);

		} else if (ILLUSTRATION.equals(type)) {
			statusList.add(AgreementStatusCodeList.Draft);
			statusList.add(AgreementStatusCodeList.Completed);
			statusList.add(AgreementStatusCodeList.Confirmed);

			// statusList.add(AgreementStatusCodeList.InProgress);
			// statusList.add(AgreementStatusCodeList.Final);
		}

		return statusList;

	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * @param transactions
	 * @throws IOException
	 */
	public void saveRequirement(Transactions transaction, String transactionId)
			throws BusinessException, IOException {
		ContextTypeCodeList contextTypeCodeList = null;
		Document docToBeSaved = null;
		if (CollectionUtils.isNotEmpty(transaction.getRequirements())) {
			for (Requirement requirementInRequest : transaction
					.getRequirements()) {
				List<AgreementDocument> documents = requirementInRequest
						.getRequirementDocuments();
				if (CollectionUtils.isNotEmpty(documents)) {
					docToBeSaved = documents.get(0);
				} else {
					throwBusinessException(true, Constants.FILE_DETAILS_MISSING);
				}
			}
		} else {
			throwBusinessException(true, Constants.REQUIREMENT_DETAILS_MISSING);
		}
		List<AgreementStatusCodeList> statusList = new ArrayList<AgreementStatusCodeList>();
		String type = transaction.getType();
		String id = EMPTY_STRING;
		if (E_APP.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.EAPP;
			id = transaction.getProposalNumber();
			statusList = getAgreementStatusCodesFor(type);

		} else if (ILLUSTRATION.equals(type)) {
			contextTypeCodeList = ContextTypeCodeList.ILLUSTRATION;
			id = transaction.getIllustrationId();
			statusList = getAgreementStatusCodesFor(type);
		}

		// get the Agreement object with proposal number final

		final Request<Agreement> agreementRequest = new JPARequestImpl<Agreement>(
				entityManager, contextTypeCodeList, transactionId);
		final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
		insuranceAgreement.setIdentifier(id);
		agreementRequest.setType(insuranceAgreement);

		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				entityManager, contextTypeCodeList, transactionId);
		statusRequest.setType(statusList);
		insuranceAgreement.setRequirements(transaction.getRequirements());
		agreementComponent.updateInsuranceAgreementWithRequirement(
				agreementRequest, statusRequest);
	}
}
