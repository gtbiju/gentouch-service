/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 301350
 * @version       : 0.1, Aug 26, 2013
 */
package com.cognizant.insurance.endpoint;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.DocumentService;
import com.cognizant.insurance.services.vo.FileUploadForm;

/**
 * The Class class DocumentServiceEndPoint.
 */
@Controller
@RequestMapping("/documentService")
public class DocumentServiceEndPoint {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageSyncEndPoint.class);

    /** The document service. */
    @Autowired
    private DocumentService documentService;

    /**
     * Gets the file.
     * 
     * @param fileName
     *            the file name
     * @param proposalNumber
     *            the proposal number
     * @param request
     *            the request
     * @param response
     *            the response
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/file", method = RequestMethod.GET)
    public final void getFile(@RequestParam("fileName") final String fileName,
            @RequestParam("proposalNumber") final String proposalNumber, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        documentService.getFile(fileName, proposalNumber, request, response);
    }

    /**
     * Saves the files to a predefined location.
     * 
     * @param uploadForm
     *            the upload form
     * @return the string
     */
    @RequestMapping(value = "/form", method = RequestMethod.POST, produces = "text/plain")
    @ResponseBody
    public final String saveFiles(final FileUploadForm uploadForm) {
        return documentService.saveFiles(uploadForm);
    }

    /**
     * Upload files.
     * 
     * @param inputJsonObj
     *            the input BASE64 encoded String
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String uploadFiles(@RequestBody final String inputJsonObj) throws BusinessException {
        return documentService.uploadFiles(inputJsonObj);
    }
    
    /**
     * Upload document.
     * 
     * @param inputJsonObj
     *            the input json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    @RequestMapping(value = "/uploadDoc", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String uploadDocument(@RequestBody final String inputJsonObj) throws BusinessException {
        return documentService.uploadDocument(inputJsonObj);
    }


    /**
     * Upload files.
     * 
     * @param inputJsonObj
     *            the input BASE64 encoded String
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    @RequestMapping(value = "/getDoc", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final
            String getDocument(@RequestBody final String inputJsonObj) throws BusinessException {
        return documentService.getDocument(inputJsonObj);
    }

    /**
     * Delete file.
     * 
     * @param inputJsonObj
     *            the input json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    @RequestMapping(value = "/deleteFile", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String deleteFile(@RequestBody final String inputJsonObj) throws BusinessException {
        return documentService.deleteFile(inputJsonObj);
    }

    /**
     * Generate pdf.
     * 
     * @param inputJsonObj
     *            the input json obj     
     * @param response
     *            the response
     * @return the string           
     * @throws BusinessException
     *             the business exception
     * @throws ParseException 
     */
    @RequestMapping(value = "/getpdf/{proposalnumber}/{type}/{templateId}", method = RequestMethod.GET)

    public final void generatePDF(@PathVariable("proposalnumber") String proposalnumber, @PathVariable("type") String type, @PathVariable("templateId") String templateId, final HttpServletResponse response) throws BusinessException, ParseException {
        documentService.generatePdfGet(proposalnumber, type, templateId, response);

    }
     
    /**
     * Generate base64 string pdf.
     * 
     * @param inputJsonObj
     *            the inputJsonObj
     * @return the string           
     * @throws BusinessException
     *             the business exception
     */
    @RequestMapping(value = "/getbase64stringpdf",  method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String generateBase64StringPDF(@RequestBody final String inputJsonObj) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException{
                return documentService.generateBase64StringPdf(inputJsonObj);
            }
        }.execute(inputJsonObj);

    }
    
    /**
     * Gets the document service.
     * 
     * @return the documentService
     */
    public final DocumentService getDocumentService() {
        return documentService;
    }

    /**
     * Sets the document service.
     * 
     * @param documentService
     *            the documentService to set
     */
    public final void setDocumentService(final DocumentService documentService) {
        this.documentService = documentService;
    }
}
