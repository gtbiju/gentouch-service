/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Feb 22, 2013
 */
package com.cognizant.insurance.agent.domain.product;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class class RequestType.
 */

@Entity
@DiscriminatorValue("Request_Type")
public class RequestType extends Type {

    /**
     *
     */
    private static final long serialVersionUID = -8920761754279222687L;

}
