package com.cognizant.insurance.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class IllusDataGenli.
 */
public class IllusAllocationData {
	
	/** The basicPremiumInveAllocYear1. */
	private String basicPremiumInveAllocYear1;
	
	/** The basicPremiumAcquFeeYear1. */
	private String basicPremiumAcquFeeYear1;
	
	/** The topUpInveAllocYear1. */
	private String topUpInveAllocYear1;
	
	/** The topUpAcquFeeYear1. */
	private String topUpAcquFeeYear1;
	
	/** The basicPremiumInveAllocYear2. */
	private String basicPremiumInveAllocYear2;
	
	/** The basicPremiumAcquFeeYear2. */
	private String basicPremiumAcquFeeYear2;
	
	/** The topUpInveAllocYear2. */
	private String topUpInveAllocYear2;
	
	/** The topUpAcquFeeYear2. */
	private String topUpAcquFeeYear2;
	
	/** The basicPremiumInveAllocYear3. */
	private String basicPremiumInveAllocYear3;
	
	/** The basicPremiumAcquFeeYear3. */
	private String basicPremiumAcquFeeYear3;
	
	/** The topUpInveAllocYear3. */
	private String topUpInveAllocYear3;
	
	/** The topUpAcquFeeYear3. */
	private String topUpAcquFeeYear3;
	
	/** The basicPremiumInveAllocYear4. */
	private String basicPremiumInveAllocYear4;
	
	/** The basicPremiumAcquFeeYear4. */
	private String basicPremiumAcquFeeYear4;
	
	/** The topUpInveAllocYear4. */
	private String topUpInveAllocYear4;
	
	/** The topUpAcquFeeYear4. */
	private String topUpAcquFeeYear4;
	
	/** The basicPremiumInveAllocYear5. */
	private String basicPremiumInveAllocYear5;
	
	/** The basicPremiumAcquFeeYear5. */
	private String basicPremiumAcquFeeYear5;
	
	/** The topUpInveAllocYear5. */
	private String topUpInveAllocYear5;
	
	/** The topUpAcquFeeYear5. */
	private String topUpAcquFeeYear5;
	
	/** The basicPremiumInveAllocYear6. */
	private String basicPremiumInveAllocYear6;
	
	/** The basicPremiumAcquFeeYear6. */
	private String basicPremiumAcquFeeYear6;
	
	/** The topUpInveAllocYear6. */
	private String topUpInveAllocYear6;
	
	/** The topUpAcquFeeYear6. */
	private String topUpAcquFeeYear6;

	/**
	 * @return the basicPremiumInveAllocYear1
	 */
	public String getBasicPremiumInveAllocYear1() {
		return basicPremiumInveAllocYear1;
	}

	/**
	 * @param basicPremiumInveAllocYear1 the basicPremiumInveAllocYear1 to set
	 */
	public void setBasicPremiumInveAllocYear1(String basicPremiumInveAllocYear1) {
		this.basicPremiumInveAllocYear1 = basicPremiumInveAllocYear1;
	}

	/**
	 * @return the basicPremiumAcquFeeYear1
	 */
	public String getBasicPremiumAcquFeeYear1() {
		return basicPremiumAcquFeeYear1;
	}

	/**
	 * @param basicPremiumAcquFeeYear1 the basicPremiumAcquFeeYear1 to set
	 */
	public void setBasicPremiumAcquFeeYear1(String basicPremiumAcquFeeYear1) {
		this.basicPremiumAcquFeeYear1 = basicPremiumAcquFeeYear1;
	}

	/**
	 * @return the topUpInveAllocYear1
	 */
	public String getTopUpInveAllocYear1() {
		return topUpInveAllocYear1;
	}

	/**
	 * @param topUpInveAllocYear1 the topUpInveAllocYear1 to set
	 */
	public void setTopUpInveAllocYear1(String topUpInveAllocYear1) {
		this.topUpInveAllocYear1 = topUpInveAllocYear1;
	}

	/**
	 * @return the topUpAcquFeeYear1
	 */
	public String getTopUpAcquFeeYear1() {
		return topUpAcquFeeYear1;
	}

	/**
	 * @param topUpAcquFeeYear1 the topUpAcquFeeYear1 to set
	 */
	public void setTopUpAcquFeeYear1(String topUpAcquFeeYear1) {
		this.topUpAcquFeeYear1 = topUpAcquFeeYear1;
	}

	/**
	 * @return the basicPremiumInveAllocYear2
	 */
	public String getBasicPremiumInveAllocYear2() {
		return basicPremiumInveAllocYear2;
	}

	/**
	 * @param basicPremiumInveAllocYear2 the basicPremiumInveAllocYear2 to set
	 */
	public void setBasicPremiumInveAllocYear2(String basicPremiumInveAllocYear2) {
		this.basicPremiumInveAllocYear2 = basicPremiumInveAllocYear2;
	}

	/**
	 * @return the basicPremiumAcquFeeYear2
	 */
	public String getBasicPremiumAcquFeeYear2() {
		return basicPremiumAcquFeeYear2;
	}

	/**
	 * @param basicPremiumAcquFeeYear2 the basicPremiumAcquFeeYear2 to set
	 */
	public void setBasicPremiumAcquFeeYear2(String basicPremiumAcquFeeYear2) {
		this.basicPremiumAcquFeeYear2 = basicPremiumAcquFeeYear2;
	}

	/**
	 * @return the topUpInveAllocYear2
	 */
	public String getTopUpInveAllocYear2() {
		return topUpInveAllocYear2;
	}

	/**
	 * @param topUpInveAllocYear2 the topUpInveAllocYear2 to set
	 */
	public void setTopUpInveAllocYear2(String topUpInveAllocYear2) {
		this.topUpInveAllocYear2 = topUpInveAllocYear2;
	}

	/**
	 * @return the topUpAcquFeeYear2
	 */
	public String getTopUpAcquFeeYear2() {
		return topUpAcquFeeYear2;
	}

	/**
	 * @param topUpAcquFeeYear2 the topUpAcquFeeYear2 to set
	 */
	public void setTopUpAcquFeeYear2(String topUpAcquFeeYear2) {
		this.topUpAcquFeeYear2 = topUpAcquFeeYear2;
	}

	/**
	 * @return the basicPremiumInveAllocYear3
	 */
	public String getBasicPremiumInveAllocYear3() {
		return basicPremiumInveAllocYear3;
	}

	/**
	 * @param basicPremiumInveAllocYear3 the basicPremiumInveAllocYear3 to set
	 */
	public void setBasicPremiumInveAllocYear3(String basicPremiumInveAllocYear3) {
		this.basicPremiumInveAllocYear3 = basicPremiumInveAllocYear3;
	}

	/**
	 * @return the basicPremiumAcquFeeYear3
	 */
	public String getBasicPremiumAcquFeeYear3() {
		return basicPremiumAcquFeeYear3;
	}

	/**
	 * @param basicPremiumAcquFeeYear3 the basicPremiumAcquFeeYear3 to set
	 */
	public void setBasicPremiumAcquFeeYear3(String basicPremiumAcquFeeYear3) {
		this.basicPremiumAcquFeeYear3 = basicPremiumAcquFeeYear3;
	}

	/**
	 * @return the topUpInveAllocYear3
	 */
	public String getTopUpInveAllocYear3() {
		return topUpInveAllocYear3;
	}

	/**
	 * @param topUpInveAllocYear3 the topUpInveAllocYear3 to set
	 */
	public void setTopUpInveAllocYear3(String topUpInveAllocYear3) {
		this.topUpInveAllocYear3 = topUpInveAllocYear3;
	}

	/**
	 * @return the topUpAcquFeeYear3
	 */
	public String getTopUpAcquFeeYear3() {
		return topUpAcquFeeYear3;
	}

	/**
	 * @param topUpAcquFeeYear3 the topUpAcquFeeYear3 to set
	 */
	public void setTopUpAcquFeeYear3(String topUpAcquFeeYear3) {
		this.topUpAcquFeeYear3 = topUpAcquFeeYear3;
	}

	/**
	 * @return the basicPremiumInveAllocYear4
	 */
	public String getBasicPremiumInveAllocYear4() {
		return basicPremiumInveAllocYear4;
	}

	/**
	 * @param basicPremiumInveAllocYear4 the basicPremiumInveAllocYear4 to set
	 */
	public void setBasicPremiumInveAllocYear4(String basicPremiumInveAllocYear4) {
		this.basicPremiumInveAllocYear4 = basicPremiumInveAllocYear4;
	}

	/**
	 * @return the basicPremiumAcquFeeYear4
	 */
	public String getBasicPremiumAcquFeeYear4() {
		return basicPremiumAcquFeeYear4;
	}

	/**
	 * @param basicPremiumAcquFeeYear4 the basicPremiumAcquFeeYear4 to set
	 */
	public void setBasicPremiumAcquFeeYear4(String basicPremiumAcquFeeYear4) {
		this.basicPremiumAcquFeeYear4 = basicPremiumAcquFeeYear4;
	}

	/**
	 * @return the topUpInveAllocYear4
	 */
	public String getTopUpInveAllocYear4() {
		return topUpInveAllocYear4;
	}

	/**
	 * @param topUpInveAllocYear4 the topUpInveAllocYear4 to set
	 */
	public void setTopUpInveAllocYear4(String topUpInveAllocYear4) {
		this.topUpInveAllocYear4 = topUpInveAllocYear4;
	}

	/**
	 * @return the topUpAcquFeeYear4
	 */
	public String getTopUpAcquFeeYear4() {
		return topUpAcquFeeYear4;
	}

	/**
	 * @param topUpAcquFeeYear4 the topUpAcquFeeYear4 to set
	 */
	public void setTopUpAcquFeeYear4(String topUpAcquFeeYear4) {
		this.topUpAcquFeeYear4 = topUpAcquFeeYear4;
	}

	/**
	 * @return the basicPremiumInveAllocYear5
	 */
	public String getBasicPremiumInveAllocYear5() {
		return basicPremiumInveAllocYear5;
	}

	/**
	 * @param basicPremiumInveAllocYear5 the basicPremiumInveAllocYear5 to set
	 */
	public void setBasicPremiumInveAllocYear5(String basicPremiumInveAllocYear5) {
		this.basicPremiumInveAllocYear5 = basicPremiumInveAllocYear5;
	}

	/**
	 * @return the basicPremiumAcquFeeYear5
	 */
	public String getBasicPremiumAcquFeeYear5() {
		return basicPremiumAcquFeeYear5;
	}

	/**
	 * @param basicPremiumAcquFeeYear5 the basicPremiumAcquFeeYear5 to set
	 */
	public void setBasicPremiumAcquFeeYear5(String basicPremiumAcquFeeYear5) {
		this.basicPremiumAcquFeeYear5 = basicPremiumAcquFeeYear5;
	}

	/**
	 * @return the topUpInveAllocYear5
	 */
	public String getTopUpInveAllocYear5() {
		return topUpInveAllocYear5;
	}

	/**
	 * @param topUpInveAllocYear5 the topUpInveAllocYear5 to set
	 */
	public void setTopUpInveAllocYear5(String topUpInveAllocYear5) {
		this.topUpInveAllocYear5 = topUpInveAllocYear5;
	}

	/**
	 * @return the topUpAcquFeeYear5
	 */
	public String getTopUpAcquFeeYear5() {
		return topUpAcquFeeYear5;
	}

	/**
	 * @param topUpAcquFeeYear5 the topUpAcquFeeYear5 to set
	 */
	public void setTopUpAcquFeeYear5(String topUpAcquFeeYear5) {
		this.topUpAcquFeeYear5 = topUpAcquFeeYear5;
	}

	/**
	 * @return the basicPremiumInveAllocYear6
	 */
	public String getBasicPremiumInveAllocYear6() {
		return basicPremiumInveAllocYear6;
	}

	/**
	 * @param basicPremiumInveAllocYear6 the basicPremiumInveAllocYear6 to set
	 */
	public void setBasicPremiumInveAllocYear6(String basicPremiumInveAllocYear6) {
		this.basicPremiumInveAllocYear6 = basicPremiumInveAllocYear6;
	}

	/**
	 * @return the basicPremiumAcquFeeYear6
	 */
	public String getBasicPremiumAcquFeeYear6() {
		return basicPremiumAcquFeeYear6;
	}

	/**
	 * @param basicPremiumAcquFeeYear6 the basicPremiumAcquFeeYear6 to set
	 */
	public void setBasicPremiumAcquFeeYear6(String basicPremiumAcquFeeYear6) {
		this.basicPremiumAcquFeeYear6 = basicPremiumAcquFeeYear6;
	}

	/**
	 * @return the topUpInveAllocYear6
	 */
	public String getTopUpInveAllocYear6() {
		return topUpInveAllocYear6;
	}

	/**
	 * @param topUpInveAllocYear6 the topUpInveAllocYear6 to set
	 */
	public void setTopUpInveAllocYear6(String topUpInveAllocYear6) {
		this.topUpInveAllocYear6 = topUpInveAllocYear6;
	}

	/**
	 * @return the topUpAcquFeeYear6
	 */
	public String getTopUpAcquFeeYear6() {
		return topUpAcquFeeYear6;
	}

	/**
	 * @param topUpAcquFeeYear6 the topUpAcquFeeYear6 to set
	 */
	public void setTopUpAcquFeeYear6(String topUpAcquFeeYear6) {
		this.topUpAcquFeeYear6 = topUpAcquFeeYear6;
	}
	
	
	
	
}
