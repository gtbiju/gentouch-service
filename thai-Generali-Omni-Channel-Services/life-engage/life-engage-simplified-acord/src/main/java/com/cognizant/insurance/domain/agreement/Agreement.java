/**
 * 
 */
package com.cognizant.insurance.domain.agreement;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;



import com.cognizant.insurance.audit.IllustrationResponsePayLoad;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.commoncodelists.StatusReasonCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.Country;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.extension.Extension;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.roleandrelationship.PartyRoleRelationship;

/**
 * A mutual understanding between two or more parties, each committing
 * themselves to fulfill one or more obligations.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Agreement extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8159393184541999601L;

    /**
     * The unique Integer assigned to the agreement, policy, or submission,
     * being referenced. If required for a self-insurance agreement, the
     * self-insured license or contract number. This is commonly referred to as
     * the "policy number" for insurance agreements.
     * 
     * 
     * 
     * 
     * 
     * 
     */
    private String identifier;

    /**
     * Name of the agreement.
     * 
     * 
     * 
     */
    private String name;

    /**
     * Agreement issue date : date at which the agreement is produced.
     * 
     * 
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date issueDate;

    /**
     * Duration period of the agreement.
     * 
     * Inception date (aka Start Date) = Commencement date of the contract.
     * Expiration date (aka End Date) = Date of cessation of the agreement, when
     * the time period for which it was written has ended.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch=FetchType.LAZY)
    private TimePeriod effectivePeriod;

    /**
     * The date on which a review of the agreement is planned.
     * 
     * 
     * 
     */
    private Date reviewDate;

    /**
     * Represents the versioning information for this instance of an agreement.
     * 
     * 
     * 
     */
    private String version;

    /**
     * The current status of the agreement within the life-cycle model.
     * 
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private AgreementStatusCodeList statusCode;

    /**
     * The date on which the agreement's latest status became effective.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date statusDate;

    /**
     * The additional information to explain the latest status of the agreement.
     * 
     * e.g: Not eligible (if agreement rejected)
     * 
     * e.g: Not taken up (if agreement rejected)
     * 
     * 
     * 
     */
    @Enumerated
    private StatusReasonCodeList statusReasonCode;

    /**
     * Indicates the code identifying the currency selected by the customer to
     * be used on any monetary information provided to the customer.
     * 
     * 
     * SOURCE: ISO-4217
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE }, fetch= FetchType.LAZY)
    private ExternalCode currencyCode;

   /**
     * This relationship links agreements when one is acting as a replacement
     * for the other.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    @JoinTable(name = "AGREEMENT_REPLACEDAGREEMENT",
            joinColumns = { @JoinColumn(name = "AGREEMENT_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "REPLACEDAGREEMENT_ID", referencedColumnName = "Id") })
    private Set<Agreement> replacesAgreements;

    /**
     * This relationship links related agreements.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    @JoinTable(name = "AGREEMENT_REFERENCEDAGREEMENT",
            joinColumns = { @JoinColumn(name = "AGREEMENT_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "REFERENCEDAGREEMENT_ID", referencedColumnName = "Id") })
    private Set<Agreement> referencedAgreement;

   /**
     * This relationship links any type of agreement with a related document.
     * 
     * e.g: Personal homeowners policy coverage form.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
     private Set<AgreementDocument> relatedDocument;

    /**
     * A composition mechanism whereby a parent is made up of one or more child
     * agreement components.
     * 
     * e.g: The individual agreement Automobile Policy includes the agreement
     * components of Liability Coverage and Glass Breakage Option.
     * 
     * 
     * 
     * 
     * 
     */  
      @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
      private Set<Coverage> includesCoverage;  

      /** The includes extension. */
      @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
      private List<Extension> includesExtension; 
      
      

    /**
     * This relationship links an agreement with its corresponding product
     * specification.
     * 
     * 
     * 
     * 
     * customized product model
     */
     @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch=FetchType.LAZY)
     private ProductSpecification isBasedOn;

    /**
     * This relationship links an agreement to its related business
     * relationship.
     * 
     * e.g. The insurance policy is related to the business relationship between
     * the producer and customer. BusinessRelationship of the agreement <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
      @Transient   
    private Set<PartyRoleRelationship> governedPartyRoleRelationship;

    /**
     * Nation issuing the agreement.
     */
     @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE , CascadeType.REMOVE}, fetch=FetchType.LAZY)
    private Country issueNation;
     
     /**
      * For IllustrationResponsePayLoad relationship with agreement
      */
     @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "agreement")
     private Set<IllustrationResponsePayLoad> illustrationResponse;
     
     /**
      * Agreement Extension
      */
     @OneToOne(cascade={CascadeType.ALL})
     private AgreementExtension agreementExtension;
     
     @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
     private Set<Requirement> requirements;
     
   /**
     * Gets the currencyCode.
     * 
     * @return Returns the currencyCode.
     */
    public ExternalCode getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Gets the effectivePeriod.
     * 
     * @return Returns the effectivePeriod.
     */
    public TimePeriod getEffectivePeriod() {
        return effectivePeriod;
    }

    /**
     * Gets the governedPartyRoleRelationship.
     * 
     * @return Returns the governedPartyRoleRelationship.
     */
    public Set<PartyRoleRelationship> getGovernedPartyRoleRelationship() {
        return governedPartyRoleRelationship;
    }

    /**
     * Gets the identifier.
     * 
     * @return Returns the identifier.
     */
    public String getIdentifier() {
        return identifier;
    }

   /**
     * Gets the isBasedOn.
     * 
     * @return Returns the isBasedOn.
     */
    public ProductSpecification getIsBasedOn() {
        return isBasedOn;
    }

    /**
     * Gets the issueDate.
     * 
     * @return Returns the issueDate.
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * @return the issueNation
     */
    public Country getIssueNation() {
        return issueNation;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the referencedAgreement.
     * 
     * @return Returns the referencedAgreement.
     */
    public Set<Agreement> getReferencedAgreement() {
        return referencedAgreement;
    }

   /**
     * Gets the relatedDocument.
     * 
     * @return Returns the relatedDocument.
     */
    public Set<AgreementDocument> getRelatedDocument() {
        return relatedDocument;
    }

    /**
     * Gets the replaced.
     * 
     * @return Returns the replaced.
     */
    public Set<Agreement> getReplacesAgreements() {
        return replacesAgreements;
    }

    /**
     * Gets the reviewDate.
     * 
     * @return Returns the reviewDate.
     */
    public Date getReviewDate() {
        return reviewDate;
    }

    /**
     * Gets the statusCode.
     * 
     * @return Returns the statusCode.
     */
    public AgreementStatusCodeList getStatusCode() {
        return statusCode;
    }

    /**
     * Gets the statusDate.
     * 
     * @return Returns the statusDate.
     */
    public Date getStatusDate() {
        return statusDate;
    }

    /**
     * Gets the statusReasonCode.
     * 
     * @return Returns the statusReasonCode.
     */
    public StatusReasonCodeList getStatusReasonCode() {
        return statusReasonCode;
    }
   
    /**
     * Gets the version.
     * 
     * @return Returns the version.
     */
    public String getVersion() {
        return version;
    }
    
    /**
     * Sets The currencyCode.
     * 
     * @param currencyCode
     *            The currencyCode to set.
     */
    public void setCurrencyCode(final ExternalCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * Sets The effectivePeriod.
     * 
     * @param effectivePeriod
     *            The effectivePeriod to set.
     */
    public void setEffectivePeriod(final TimePeriod effectivePeriod) {
        this.effectivePeriod = effectivePeriod;
    }

    /**
     * Sets The governedPartyRoleRelationship.
     * 
     * @param governedPartyRoleRelationship
     *            The governedPartyRoleRelationship to set.
     */
    public void setGovernedPartyRoleRelationship(
            final Set<PartyRoleRelationship> governedPartyRoleRelationship) {
        this.governedPartyRoleRelationship = governedPartyRoleRelationship;
    }

    /**
     * Sets The identifier.
     * 
     * @param identifier
     *            The Integer to set.
     */
    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

    /**
     * Sets The isBasedOn.
     * 
     * @param isBasedOn
     *            The isBasedOn to set.
     */
    public void setIsBasedOn(final ProductSpecification isBasedOn) {
        this.isBasedOn = isBasedOn;
    }

    /**
     * Sets The issueDate.
     * 
     * @param issueDate
     *            The issueDate to set.
     */
    public void setIssueDate(final Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * @param issueNation
     *            the issueNation to set.
     */
    public void setIssueNation(final Country issueNation) {
        this.issueNation = issueNation;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets The referencedAgreement.
     * 
     * @param referencedAgreement
     *            The referencedAgreement to set.
     */
    public void setReferencedAgreement(
            final Set<Agreement> referencedAgreement) {
        this.referencedAgreement = referencedAgreement;
    }
   
    /**
     * Sets The relatedDocument.
     * 
     * @param relatedDocument
     *            The relatedDocument to set.
     */
     public void setRelatedDocument(
            final Set<AgreementDocument> relatedDocument) {
        this.relatedDocument = relatedDocument;
    }

    /**
     * Sets The replacesAgreements.
     * 
     * @param replacesAgreements
     *            The replaced to set.
     */
    public void setReplacesAgreements(
            final Set<Agreement> replacesAgreements) {
        this.replacesAgreements = replacesAgreements;
    }

    /**
     * Sets The reviewDate.
     * 
     * @param reviewDate
     *            The reviewDate to set.
     */
    public void setReviewDate(final Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    /**
     * Sets The statusCode.
     * 
     * @param statusCode
     *            The statusCode to set.
     */
    public void setStatusCode(final AgreementStatusCodeList statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Sets The statusDate.
     * 
     * @param statusDate
     *            The statusDate to set.
     */
    public void setStatusDate(final Date statusDate) {
        this.statusDate = statusDate;
    }

    /**
     * Sets The statusReasonCode.
     * 
     * @param statusReasonCode
     *            The statusReasonCode to set.
     */
    public void setStatusReasonCode(
            final StatusReasonCodeList statusReasonCode) {
        this.statusReasonCode = statusReasonCode;
    }

    /**
     * Sets The version.
     * 
     * @param version
     *            The version to set.
     */
    public void setVersion(final String version) {
        this.version = version;
    }

	/**
	 * Gets the includes coverage.
	 *
	 * @return the includes coverage
	 */
	public Set<Coverage> getIncludesCoverage() {
		return includesCoverage;
	}

	/**
	 * Sets the includes coverage.
	 *
	 * @param includesCoverage the new includes coverage
	 */
	public void setIncludesCoverage(Set<Coverage> includesCoverage) {
		this.includesCoverage = includesCoverage;
	}

	/**
	 * @param illustrationResponse the illustrationResponse to set
	 */
	public void setIllustrationResponse(Set<IllustrationResponsePayLoad> illustrationResponse) {
		this.illustrationResponse = illustrationResponse;
	}

	/**
	 * @return the illustrationResponse
	 */
	public Set<IllustrationResponsePayLoad> getIllustrationResponse() {
		return illustrationResponse;
	}

	/**
	 * Gets the includes extension.
	 *
	 * @return the includes extension
	 */
	public List<Extension> getIncludesExtension() {
		return includesExtension;
	}

	/**
	 * Sets the includes extension.
	 *
	 * @param includesExtension the new includes extension
	 */
	public void setIncludesExtension(List<Extension> includesExtension) {
		this.includesExtension = includesExtension;
	}

	

	public AgreementExtension getAgreementExtension() {
		return agreementExtension;
	}

	public void setAgreementExtension(AgreementExtension agreementExtension) {
		this.agreementExtension = agreementExtension;
	}
	  /**
     * @return the requirements
     */
    public Set<Requirement> getRequirements() {
        return requirements;
    }

    /**
     * @param requirements
     *            the requirements to set
     */
    public void setRequirements(Set<Requirement> requirements) {
        this.requirements = requirements;
    }

}
