package com.cognizant.insurance.utils;

public class EAppPreviousPolicy {

	/** Name of Insured. */

	private String insuredName;

	/** Insurance Company. */

	private String insuranceCompany;

	/** Type of Insurance. */

	private String insuranceType;

	/** Final Decision. */

	private String decision;

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getDecision() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision = decision;
	}

}
