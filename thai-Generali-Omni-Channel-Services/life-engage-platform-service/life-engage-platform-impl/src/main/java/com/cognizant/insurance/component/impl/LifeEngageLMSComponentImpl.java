/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngageLMSComponent;
import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.LMSRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;

/**
 * The Class class LifeEngageLMSComponentImpl.
 * 
 * @author 300797
 */
@Service("lifeEngageLMSComponent")
public class LifeEngageLMSComponentImpl implements LifeEngageLMSComponent {

    /** The save LMS holder. */
    @Autowired
    @Qualifier("lmsSaveMapping")
    private LifeEngageSmooksHolder saveLMSHolder;

    /** The retrieve LMS holder. */
    @Autowired
    @Qualifier("lmsRetrieveMapping")
    private LifeEngageSmooksHolder retrieveLMSHolder;

    /** The retrieve LMS holder. */
    @Autowired
    @Qualifier("lmsRetrieveListMapping")
    private LifeEngageSmooksHolder retrieveLMSListHolder;

    /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;

    /** The e app repository. */
    @Autowired
    private LMSRepository lmsRepository;

    /** The id generator. */
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;

    /** The Constant SUCCESS_MESSAGE_SAVE. */
    private static final String SUCCESS_MESSAGE_SAVE = "Lead Saved";

    /** The Constant SUCCESS_MESSAGE_UPDATE. */
    private static final String SUCCESS_MESSAGE_UPDATE = "Lead Updated";

    /** The Constant REJECTED_MESSAGE. */
    private static final String REJECTED_MESSAGE = "Lead Rejected";

    /** The Constant LMS. */
    private static final String LMS = "LMS";

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageLMSComponentImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageLMSComponent#saveLMS(java.lang.String,
     * com.cognizant.insurance.request.vo.RequestInfo, com.cognizant.insurance.audit.LifeEngageAudit)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String saveLMS(final String json, final RequestInfo requestInfo, final LifeEngageAudit lifeEngageAudit) {

        LOGGER.trace("Inside LifeEngageEApp component saveEApp json: json" + json);
        String lmsResponse;
        Transactions transactions = new Transactions();
        String successMessage;
        String offlineIdentifier = null;
        try {
            LifeEngageComponentHelper.parseLMSTrasactionDetails(json, transactions);
            // Store to a temp variable to set it back to response.
            offlineIdentifier = transactions.getOfflineIdentifier();
            // Do not process eApp data if already done
            if (auditRepository.isTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                throwSystemException(true, ErrorConstants.LE_SYNC_ERR_102, Constants.PROCESSED_TRANSACTION);
            }
            transactions = (Transactions) saveLMSHolder.parseJson(json);

			/**
			 * Update flow was depending on lmsid alone. Instead will verify
			 * whether the data exist for transTrackingId and AgentId
			 * combination. If so , considered as update flow. Bug fix when
			 * multple save hapenning for same record with same transTrackingId,
			 * in data gets saved in backend, but not getting updated in UI and
			 * hence the same record comes again withour fnaId
			 **/

			Transactions responseTransactions = new Transactions();
			Customer customer = new Customer();
			if (StringUtils.isNotEmpty(transactions.getKey1())
					|| StringUtils
							.isNotEmpty(transactions.getTransTrackingId())) {
				responseTransactions = lmsRepository.retrieveLMS(transactions,
						requestInfo.getTransactionId(), requestInfo);
			}
			if (responseTransactions != null
					&& responseTransactions.getCustomer() != null
					&& responseTransactions.getCustomer().getIdentifier() != null) {
				customer = responseTransactions.getCustomer();
				if (StringUtils.isEmpty(transactions.getKey1())) {
					transactions.setKey1(responseTransactions.getCustomer()
							.getIdentifier());
				}
				transactions.setMode(Constants.STATUS_UPDATE);
				successMessage = SUCCESS_MESSAGE_UPDATE;
			} else {
            	
                transactions.setKey1(transactions.getAgentId()+String.valueOf(idGenerator.generate(LMS)));
                transactions.setMode(Constants.STATUS_SAVE);
                successMessage = SUCCESS_MESSAGE_SAVE;
            }

            onBeforeSave(transactions);

            final String proposalstatus = lmsRepository.saveLMS(transactions, requestInfo,customer);
            if (Constants.REJECTED.equals(proposalstatus)) {
                successMessage = REJECTED_MESSAGE;
            }
            onAfterSave(transactions);

            LifeEngageComponentHelper.createResponseStatus(transactions, proposalstatus, successMessage, null);

            transactions.setOfflineIdentifier(offlineIdentifier);

            auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit,"");
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
			if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						Constants.FAILURE, e.getMessage() + " : "
								+ getExceptionMessage(e),
						ErrorConstants.LE_SYNC_ERR_105);
			} else if (ErrorConstants.LE_SYNC_ERR_102.equals(e.getErrorCode())) {
				LifeEngageComponentHelper.createResponseStatus(transactions, LifeEngageComponentHelper.SUCCESS, SUCCESS_MESSAGE_SAVE, null);

			} else {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						Constants.FAILURE, e.getMessage() + " : "
								+ getExceptionMessage(e), e.getErrorCode());
			}
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                        + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_103);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                        + getExceptionMessage(e), e.getErrorCode());
            }
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_104);
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (ParseException e) {
            LOGGER.error("ParseException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_101);
            lmsResponse = saveLMSHolder.parseBO(transactions);
        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
            lmsResponse = saveLMSHolder.parseBO(transactions);

        }
        LOGGER.trace("Inside LifeEngageEApp component eAppResponse : eAppResponse" + lmsResponse);
        return lmsResponse;
    }

    /**
     * On before save.
     * 
     * @param transactions
     *            the transactions
     * @throws ParseException
     *             the parse exception
     */
    protected void onBeforeSave(Transactions transactions) throws ParseException {
        transactions.setCreationDateTime(LifeEngageComponentHelper.getCurrentdate());
        transactions.setModifiedTimeStamp(LifeEngageComponentHelper.getCurrentdate());
    }

    /**
     * On after save.
     * 
     * @param transactions
     *            the transactions
     */
    protected void onAfterSave(Transactions transactions) {

    }

    /**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageLMSComponent#retrieveLMS(com.cognizant.insurance.request.vo.RequestInfo
     * , org.json.JSONObject)
     */
    @Override
    public String retrieveLMS(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
            ParseException {
        String response = null;
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        if (jsonObj.has(LifeEngageComponentHelper.KEY_1) && jsonObj.getString(LifeEngageComponentHelper.KEY_1) != null
                && !"".equals(jsonObj.getString(LifeEngageComponentHelper.KEY_1))
                && !jsonObj.getString(LifeEngageComponentHelper.KEY_1).equals(Constants.NULL)) {
            final Transactions requestPayload = (Transactions) retrieveLMSHolder.parseJson(jsonObj.toString());

            Transactions responseTransactions = null;
            try {
                responseTransactions =
                        lmsRepository.retrieveLMS(requestPayload, requestInfo.getTransactionId(), requestInfo);
            } catch (Exception e) {
                LOGGER.error("Unable to retrieve LMS :" + e.getMessage());
                responseTransactions = requestPayload;
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage()
                        + " : " + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

            }
            LOGGER.trace("Inside LifeEngageEApp component retrieveEApp : responseTransactions" + responseTransactions);
            LifeEngageComponentHelper.parseLMSTrasactionDetails(jsonObj.toString(), responseTransactions);
            transactionsList.add(responseTransactions);
            LOGGER.trace("LifeEngageEAppComponent.retrieveEApp: responseTransactions" + responseTransactions);
        }
        response = retrieveLMSHolder.parseBO(transactionsList);
        response = response.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageLMSComponent#retrieveAllLMS(com.cognizant.insurance.request.vo.
     * RequestInfo, org.json.JSONObject)
     */
    @Override
    public String retrieveAllLMS(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
            ParseException {

        final Transactions transactions = (Transactions) retrieveLMSHolder.parseJson(jsonObj.toString());
        boolean fullFetch = true;
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        if (jsonObj.has(LifeEngageComponentHelper.KEY_10)
                && jsonObj.getString(LifeEngageComponentHelper.KEY_10) != null) {
            if (LifeEngageComponentHelper.FULL_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                // Retrieve All details for all proposals for an agent
                transactionsList = lmsRepository.retrieveAll(transactions, requestInfo, true);
                LOGGER.trace("Inside LifeEngageLMSComponentImpl RetrieveAllLMS - FullDetails: transactionsList"
                        + transactionsList.size());
            } else if (LifeEngageComponentHelper.BASIC_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                // Retrieve Only the basic details required for listing
                transactions.setProposalNumber(null);
                transactionsList = lmsRepository.retrieveAll(transactions, requestInfo, false);
                LOGGER.trace("Inside LifeEngageLMSComponentImpl RetrieveAllLMS : transactionsList"
                        + transactionsList.size());
                fullFetch = false;
            }
        }
        LifeEngageComponentHelper.buildTransactionsList(transactionsList, jsonObj);
        String eAppMapToReturn = "{\"Transactions\":[]}";
        if (fullFetch) {
            eAppMapToReturn = retrieveLMSHolder.parseBO(transactionsList);
        } else {
            eAppMapToReturn = retrieveLMSListHolder.parseBO(transactionsList);
        }
        eAppMapToReturn = eAppMapToReturn.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
        return eAppMapToReturn;
    }

}
