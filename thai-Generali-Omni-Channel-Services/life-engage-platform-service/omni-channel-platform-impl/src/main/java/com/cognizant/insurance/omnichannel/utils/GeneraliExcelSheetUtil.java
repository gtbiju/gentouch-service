package com.cognizant.insurance.omnichannel.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.vo.AgentReportVo;
import com.cognizant.insurance.omnichannel.vo.BulkUploadData;

/**
 * The class GeneraliExcelSheetUtil
 * Class for handing Excel operations
 * Reading excel
 * Validating input
 * Writing excel 
 * @author 390229
 *
 */
@Component("generaliExcelSheetUtil")
public class GeneraliExcelSheetUtil {

	/** Bulk upload Agency Template */
	@Value("${le.platform.service.agencyExcelTemplate}")
	public String agencyExcelFileName;

	/** Bulk upload Banca Template */
	@Value("${le.platform.service.bancaExcelTemplate}")
	public String bancaExcelFileName;

	/** Bulk upload Folder path */
	@Value("${le.platform.service.folderPath}")
	public String folderPath;

	public static final Logger LOGGER = LoggerFactory
			.getLogger(GeneraliExcelSheetUtil.class);
	
	/** The Constant DATE_FORMAT **/
    private static final String DATE_FORMAT_WITHOUT_SLASH = "yyyyMMdd";
    
    /** The Constant TIME_FORMAT **/
    private static final String TIME_FORMAT_WITHOUT_SECONDS = "HH_mm_ss";
    
    private static String[] columns = { "Agent Code", "Agent Name", "Position", "SM Name", "GM Name", "Team Name", "New Leads",
            "Calls", "Appointments", "Visits", "Illustrations (Draft+Completed)", "E-App (Draft+Submitted)" };
    

	/**
	 * Method to read an Excel file
	 * 
	 * @param fileName
	 * @return
	 */
	public Sheet retrieveFile(String fileName) {
		Sheet sheet = null; // sheet can be used as common for XSSF and HSSF
		try {
			String fileExtn = getFileExtension(fileName);
			Workbook wb_xssf; // Declare XSSF WorkBook
			Workbook wb_hssf; // Declare HSSF WorkBook

			if (fileExtn.equalsIgnoreCase("xlsx")) {
				wb_xssf = new XSSFWorkbook(fileName);
				sheet = wb_xssf.getSheetAt(0);
			}
			if (fileExtn.equalsIgnoreCase("xls")) {
				FileInputStream myInput;
				myInput = new FileInputStream(fileName);
				POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
				wb_hssf = new HSSFWorkbook(myFileSystem);

				sheet = wb_hssf.getSheetAt(0);
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("FILE NOT FOUND FILENAME" + fileName);
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error("Invalid format " + fileName);
			e.printStackTrace();
		}

		return sheet;

	}

	/**
	 * Method to get file extension
	 * 
	 * @param fname2
	 * @return
	 */
	private String getFileExtension(String fname2) {
		String fileName = fname2;
		String ext = "";
		int mid = fileName.lastIndexOf(".");
		ext = fileName.substring(mid + 1, fileName.length());
		return ext;
	}

	/**
	 * Method to write to Output Excel files
	 * 
	 * @param fileName
	 * @param bulkUploadDataList
	 * @param type
	 * @param fileType
	 * @return
	 */
	public Workbook writeToExcelFile(String fileName,
			List<BulkUploadData> bulkUploadDataList, String type,
			String fileType) {
		Workbook wb_Output = null;
		Sheet sheet = null;
		if (GeneraliConstants.TYPE_BANCA.equals(type)) {
			fileName = fileName + "_" + fileType + ".xlsx";
		} else {
			fileName = fileName + ".xlsx";
		}
		try {
			wb_Output = new XSSFWorkbook(fileName);
			sheet = wb_Output.getSheetAt(0);
			Iterator<Row> rows = sheet.rowIterator();
			while (rows.hasNext()) {
				Row myRow = (Row) rows.next();
				if (myRow.getRowNum() == 0) {
					break;
				}
			}
			
			int rowCount = 0;
			CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
			cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
			cellStyle.setWrapText(true);
			cellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
			cellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
			cellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
			cellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);

			CellStyle cellStyleInvalid = sheet.getWorkbook().createCellStyle();
			cellStyleInvalid.cloneStyleFrom(cellStyle);
			cellStyleInvalid.setFillPattern(CellStyle.SOLID_FOREGROUND);
			cellStyleInvalid.setFillForegroundColor(IndexedColors.YELLOW
					.getIndex());

			CellStyle cellDateCellStyle = sheet.getWorkbook().createCellStyle();
			cellDateCellStyle.cloneStyleFrom(cellStyle);
			CreationHelper createHelper = wb_Output.getCreationHelper();
			cellDateCellStyle.setDataFormat(createHelper.createDataFormat()
					.getFormat("dd/mm/yyyy"));

			XSSFFont font = (XSSFFont) wb_Output.createFont();
			font.setFontHeightInPoints((short) 11);
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			font.setColor(XSSFFont.COLOR_RED);
			cellStyleInvalid.setFont(font);

			for (BulkUploadData bulkUploadData : bulkUploadDataList) {

				if (bulkUploadData != null) {

					Row row = sheet.createRow(++rowCount);

					int columnCount = 0;

					if (row.getRowNum() == 0) {

						continue;
					} else {
						if (GeneraliConstants.TYPE_AGENCY.equals(type)) {

							while (columnCount <= 17) {

								Cell cell = row.createCell(columnCount++);

								try {
									if (cell.getColumnIndex() == 0) {
										applyCellStyle(
												bulkUploadData
														.isCustomerNameInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getCustomerFirstName());

									}
									
									//Modified as part of CR
									else if (cell.getColumnIndex() == 1) {
										applyCellStyle(
												bulkUploadData
														.isCustomerLastNameInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getCustomerLastName());

									}


									else if (cell.getColumnIndex() == 2) {
										applyCellStyle(
												bulkUploadData
														.isGenderInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getGender());

									}

									else if (cell.getColumnIndex() == 3) {
										applyCellStyle(
												bulkUploadData
														.isMobileNumberInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getMobileNumber());

									}

									else if (cell.getColumnIndex() == 4) {
										applyCellStyle(
												bulkUploadData
														.isNationalIDInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getNationalID());

									}

									else if (cell.getColumnIndex() == 5) {
										applyCellStyle(
												bulkUploadData
														.isEmailAddresInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getEmailAddress());

									}

									else if (cell.getColumnIndex() == 6) {
										applyCellStyle(
												bulkUploadData
														.isAddressInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getAddress());

									}

									else if (cell.getColumnIndex() == 7) {
										applyCellStyle(
												bulkUploadData.isStateInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getState());

									}

									else if (cell.getColumnIndex() == 8) {
										applyCellStyle(
												bulkUploadData
														.isNationalityInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getNationality());

									} else if (cell.getColumnIndex() == 9) {
										applyCellStyle(
												bulkUploadData.isDOBInValid(),
												cellDateCellStyle,
												cellStyleInvalid, cell);
										cell.setCellValue(bulkUploadData
												.getDobString());

									} else if (cell.getColumnIndex() == 10) {
										applyCellStyle(
												bulkUploadData
														.isPotentialInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getPotential());

									} else if (cell.getColumnIndex() == 11) {
										applyCellStyle(
												bulkUploadData
														.isSourceInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getSource());

									} else if (cell.getColumnIndex() == 12) {
										applyCellStyle(
												bulkUploadData
														.isOccupationInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getOccupation());

									} else if (cell.getColumnIndex() == 13) {
										applyCellStyle(
												bulkUploadData
														.isActionInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getAction());

									} else if (cell.getColumnIndex() == 14) {

										applyCellStyle(
												bulkUploadData
														.isNextMeetinDateInValid(),
												cellDateCellStyle,
												cellStyleInvalid, cell);
										cell.setCellValue(bulkUploadData
												.getNextmeetingDateString());

									} else if (cell.getColumnIndex() == 15) {
										applyCellStyle(
												bulkUploadData
														.isNextMeetingTimeInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getNextMeetingTime());

									}

									else if (cell.getColumnIndex() == 16) {
										applyCellStyle(
												bulkUploadData
														.getIsAgentCodeInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getAgentCode());

									}

									else if (cell.getColumnIndex() == 17) {
										cell.setCellStyle(cellStyle);
										cell.setCellValue(bulkUploadData
												.getRemarks());
									}

								} catch (NullPointerException e) {
									LOGGER.error("Error in setting cell "
											+ cell.getColumnIndex() + "row :"
											+ row.getRowNum());
								}
							}
						} else if (GeneraliConstants.TYPE_BANCA.equals(type)) {
							while (columnCount <= 25) {

								Cell cell = row.createCell(columnCount++);
								try {
									if (cell.getColumnIndex() == 0) {
										applyCellStyle(
												bulkUploadData
														.isCustomerNameInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getCustomerFirstName());

									}
									// Modified as part of CR
									else if (cell.getColumnIndex() == 1) {
										applyCellStyle(
												bulkUploadData
														.isCustomerLastNameInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getCustomerLastName());

									}

									else if (cell.getColumnIndex() == 2) {
										applyCellStyle(
												bulkUploadData
														.isGenderInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getGender());

									}

									else if (cell.getColumnIndex() == 3) {
										applyCellStyle(
												bulkUploadData
														.isNationalIDInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getNationalID());

									}

									else if (cell.getColumnIndex() == 4) {
										applyCellStyle(
												bulkUploadData
														.isMobileNumberInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getMobileNumber());

									}

									else if (cell.getColumnIndex() == 5) {
										applyCellStyle(
												bulkUploadData
														.isEmailAddresInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getEmailAddress());

									}

									else if (cell.getColumnIndex() == 6) {
										applyCellStyle(
												bulkUploadData
														.isAddressInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getAddress());

									}

									else if (cell.getColumnIndex() == 7) {
										applyCellStyle(
												bulkUploadData.isDOBInValid(),
												cellDateCellStyle,
												cellStyleInvalid, cell);
										cell.setCellValue(bulkUploadData
												.getDobString());

									} else if (cell.getColumnIndex() == 8) {
										applyCellStyle(
												bulkUploadData
														.isPotentialInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getPotential());

									} else if (cell.getColumnIndex() == 9) {
										applyCellStyle(
												bulkUploadData.isStateInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getState());

									} else if (cell.getColumnIndex() == 10) {
										applyCellStyle(
												bulkUploadData
														.isNationalityInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getNationality());

									} else if (cell.getColumnIndex() == 11) {
										applyCellStyle(
												bulkUploadData
														.isAnnualIncomeInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getAnnualIncome());

									} else if (cell.getColumnIndex() == 12) {

										applyCellStyle(
												bulkUploadData.isNeedInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getNeed());

									} else if (cell.getColumnIndex() == 13) {
										applyCellStyle(
												bulkUploadData
														.isSourceInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getSource());

									}

									else if (cell.getColumnIndex() == 14) {
										applyCellStyle(
												bulkUploadData
														.isOccupationInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getOccupation());

									} else if (cell.getColumnIndex() == 15) {
										applyCellStyle(
												bulkUploadData
														.isActionInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getAction());

									} else if (cell.getColumnIndex() == 16) {
										applyCellStyle(
												bulkUploadData
														.isNextMeetinDateInValid(),
												cellDateCellStyle,
												cellStyleInvalid, cell);
										cell.setCellValue(bulkUploadData
												.getNextmeetingDateString());

									} else if (cell.getColumnIndex() == 17) {
										applyCellStyle(
												bulkUploadData
														.isNextMeetingTimeInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getNextMeetingTime());

									} else if (cell.getColumnIndex() == 18) {
										applyCellStyle(
												bulkUploadData
														.isBranchInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getBranch());

									} else if (cell.getColumnIndex() == 19) {
										applyCellStyle(
												bulkUploadData.isBankInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getBank());

									}

									else if (cell.getColumnIndex() == 20) {
										applyCellStyle(
												bulkUploadData
														.isRefNameInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getRefName());

									} else if (cell.getColumnIndex() == 21) {
										applyCellStyle(
												bulkUploadData
														.isRefMobileNumberInValid(),
												cellStyle, cellStyleInvalid,
												cell);
										cell.setCellValue(bulkUploadData
												.getRefMobileNumber());

									}

									else if (cell.getColumnIndex() == 22) {

										cell.setCellStyle(cellStyle);
										if (GeneraliConstants.VALID
												.equals(fileType)) {
											cell.setCellValue(bulkUploadData
													.getAgentCode());
										} else {
											cell.setCellValue(bulkUploadData
													.getRemarks());
										}
									}
									if (GeneraliConstants.VALID
											.equals(fileType)) {
										if (cell.getColumnIndex() == 23) {

											cell.setCellStyle(cellStyle);
											cell.setCellValue(bulkUploadData
													.getAgentName());
										} else if (cell.getColumnIndex() == 24) {
											cell.setCellStyle(cellStyle);
											cell.setCellValue(bulkUploadData
													.getAllocatedBranchCode());

										} else if (cell.getColumnIndex() == 25) {
											cell.setCellStyle(cellStyle);
											cell.setCellValue(bulkUploadData
													.getAllocatedBranchName());

										}
									}

								} catch (NullPointerException e) {
									LOGGER.error("Error in setting cell "
											+ cell.getColumnIndex() + "row :"
											+ row.getRowNum());
								}
							}

						}
					}
				}

			}

		} catch (IOException e) {
			LOGGER.error("Error in  Wring Excel Files");

		}

		return wb_Output;

	}

	/**
	 * Method to do Field level validations for Agency Excel
	 * 
	 * @param customerData
	 * @return
	 */
	public boolean validateAgencyCustomerData(BulkUploadData customerData) {
		Boolean isCustomerDataValid = true;
		Date currentDate = new Date();
		
		String firstname = customerData.getCustomerFirstName();
		if (firstname == null || GeneraliConstants.JSON_EMPTY.equals(firstname)) {
			isCustomerDataValid = false;
			customerData.setCustomerNameInValid(true);
			customerData.setRemarks(GeneraliConstants.EMPTY_FIRST_NAME);
		} else if (!isLettersandSpaceOnly(firstname)) {
			customerData.setCustomerNameInValid(true);
			isCustomerDataValid = false;
			customerData.setRemarks(GeneraliConstants.INVALID_FIRST_NAME);
		} else if ((firstname).length() > 60) {
			customerData.setCustomerNameInValid(true);
			isCustomerDataValid = false;
			customerData.setRemarks(GeneraliConstants.INVALID_FIRST_NAME_LENGTH);
		}
		String lastName = customerData.getCustomerLastName();
		if (lastName == null || GeneraliConstants.JSON_EMPTY.equals(lastName)) {
			isCustomerDataValid = false;
			customerData.setCustomerLastNameInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.EMPTY_LAST_NAME));
		} else if (!isLettersandSpaceOnly(lastName)) {
			customerData.setCustomerLastNameInValid(true);
			isCustomerDataValid = false;
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.INVALID_LAST_NAME));
		} else if ((lastName).length() > 60) {
			customerData.setCustomerLastNameInValid(true);
			isCustomerDataValid = false;
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.INVALID_LAST_NAME_LENGTH));
		}
		String gender = customerData.getGender();
		if (gender == null || GeneraliConstants.JSON_EMPTY.equals(gender)) {
			isCustomerDataValid = false;
			customerData.setGenderInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.EMPTY_GENDER));
		}

		String mobileNumber = customerData.getMobileNumber();
		if (mobileNumber == null
				|| GeneraliConstants.JSON_EMPTY.equals(mobileNumber)) {
			isCustomerDataValid = false;
			customerData.setMobileNumberInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_MOBILE_NUMBER));
		} else if (!isNumericOnly(mobileNumber)) {
			isCustomerDataValid = false;
			customerData.setMobileNumberInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_MOBILE_NUMBER));
		} else if ((mobileNumber.length() > 16)) {
			isCustomerDataValid = false;
			customerData.setMobileNumberInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_MOBILE_NUMBER));
		} 
		String nationalID = customerData.getNationalID();
		if (nationalID != null && nationalID.length() > 30) {
			isCustomerDataValid = false;
			customerData.setNationalIDInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_NATIONALID));
		}

		else if (nationalID != null
				&& (nationalID.contains(" ") || nationalID.contains("\n"))) {
			isCustomerDataValid = false;
			customerData.setNationalIDInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.NationalID_INVALID_CHARACTER));
		}

		String email = customerData.getEmailAddress();
		if (!(email == null) && !GeneraliConstants.JSON_EMPTY.equals(email)) {
			if (!isValidMail(email)) {
				isCustomerDataValid = false;
				customerData.setEmailAddresInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_EMAIL_ID));
			} else if (email.length() > 50) {
				isCustomerDataValid = false;
				customerData.setEmailAddresInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_EMAIL_ID));
			}
		}

		String address = customerData.getAddress();
		if (address != null && address.length() > 100) {
			isCustomerDataValid = false;
			customerData.setAddressInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_ADDRESS));
		}

		String state = customerData.getState();
		if (state != null && state.length() > 50) {
			isCustomerDataValid = false;
			customerData.setStateInValid(true);
			customerData
					.setRemarks(attachErrorMessages(customerData.getRemarks(),
							GeneraliConstants.INVALID_STATE));
		}

		String nationality = customerData.getNationality();
		if (nationality != null && nationality.length() > 50) {
			isCustomerDataValid = false;
			customerData.setNationalityInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_NATIONALITY));
		}

		Date dob = customerData.getDob();

		if (dob != null) {
			if (validateDateCell(dob)) {
				isCustomerDataValid = false;
				customerData.setDOBInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_DOB));
			} else if (dob.after(currentDate)) {
				isCustomerDataValid = false;
				customerData.setDOBInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_DOB));
			}
		}

		String potential = customerData.getPotential();
		if (potential == null || GeneraliConstants.JSON_EMPTY.equals(potential)) {
			isCustomerDataValid = false;
			customerData.setPotentialInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.EMPTY_POTENTIAL));
		}
		String source = customerData.getSource();
		if (source == null || GeneraliConstants.JSON_EMPTY.equals(source)) {
			isCustomerDataValid = false;
			customerData.setSourceInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.EMPTY_SOURCE));
		}

		String action = customerData.getAction();
		if (action == null || GeneraliConstants.JSON_EMPTY.equals(action)) {
			isCustomerDataValid = false;
			customerData.setActionInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.EMPTY_ACTION));
		}

		BigDecimal number = new BigDecimal(60 * 60 * 24);
		BigDecimal number2 = new BigDecimal(90 * 1000);
		number = number.multiply(number2);

		String nextMeetingTime = customerData.getNextMeetingTime();
		Date nextMeetingDate = null;
		Date nextMeetingDateTime = null;

		nextMeetingDate = customerData.getNextmeetingDate();
		nextMeetingDateTime = nextMeetingDate;
		if (nextMeetingDate != null) {

			if ((nextMeetingDate != null)
					&& (nextMeetingTime == null || GeneraliConstants.JSON_EMPTY
							.equals(nextMeetingTime))) {
				isCustomerDataValid = false;
				customerData.setNextMeetingTimeInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.NEXT_MEETING_TIME_INVALID));
			}
			if (validateDateCell(nextMeetingDate)) {
				isCustomerDataValid = false;
				customerData.setNextMeetinDateInValid(true);
				customerData
						.setRemarks(attachErrorMessages(
								customerData.getRemarks(),
								GeneraliConstants.EMPTY_DATE));
			}
			if (nextMeetingDate != null && nextMeetingTime != null
					&& !GeneraliConstants.JSON_EMPTY.equals(nextMeetingTime)) {
				nextMeetingDateTime = getModifiedDateTime(nextMeetingDate,
						nextMeetingTime);
			}

			BigDecimal nxtTime = new BigDecimal(nextMeetingDateTime.getTime());
			BigDecimal crtTime = new BigDecimal(currentDate.getTime());
			nxtTime = nxtTime.subtract(crtTime);

			if (nextMeetingDateTime.before(currentDate)) {
				isCustomerDataValid = false;
				customerData.setNextMeetinDateInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INAVALID_MEETING_DATE));

			} else if (nxtTime.compareTo(number) > 0) {
				isCustomerDataValid = false;
				customerData.setNextMeetinDateInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INAVALID_MEETING_DATE));
			}

		}

		if (!(nextMeetingTime == null)
				&& !GeneraliConstants.JSON_EMPTY.equals(nextMeetingTime)) {
			if (nextMeetingDate == null) {
				isCustomerDataValid = false;
				customerData.setNextMeetinDateInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.NEXT_MEETING_DATE_INVALID));
			}
		}

		String refName = customerData.getRefName();
		if (refName != null) {
			if (!isLettersandSpaceOnly(refName)) {
				isCustomerDataValid = false;
				customerData.setRefNameInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_REF_NAME));
			}
		}
		String refMobNumber = customerData.getRefMobileNumber();
		if (refMobNumber != null) {
			if (!isNumericOnly(refMobNumber)) {
				isCustomerDataValid = false;
				customerData.setRefMobileNumberInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_MOBILE_NUMBER));
			}
		}

		customerData.setStatus(isCustomerDataValid);
		return isCustomerDataValid;
	}

	/**
	 * Method to validate Banca Input Excel field level Validations
	 * 
	 * @param customerData
	 * @return
	 */
	public boolean validateBancaCustomerData(BulkUploadData customerData) {
		Boolean isCustomerDataValid = true;
		Date currentDate = new Date();

		String firstname = customerData.getCustomerFirstName();
		if (firstname == null || GeneraliConstants.JSON_EMPTY.equals(firstname)) {
			isCustomerDataValid = false;
			customerData.setCustomerNameInValid(true);
			customerData.setRemarks(GeneraliConstants.EMPTY_FIRST_NAME);
		} else if (!isLettersandSpaceOnly(firstname)) {
			customerData.setCustomerNameInValid(true);
			isCustomerDataValid = false;
			customerData.setRemarks(GeneraliConstants.INVALID_FIRST_NAME);
		} else if ((firstname).length() > 60) {
			customerData.setCustomerNameInValid(true);
			isCustomerDataValid = false;
			customerData
					.setRemarks(GeneraliConstants.INVALID_FIRST_NAME_LENGTH);
		}
		String lastName = customerData.getCustomerLastName();
		if (lastName == null || GeneraliConstants.JSON_EMPTY.equals(lastName)) {
			isCustomerDataValid = false;
			customerData.setCustomerLastNameInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.EMPTY_LAST_NAME));
		} else if (!isLettersandSpaceOnly(lastName)) {
			customerData.setCustomerLastNameInValid(true);
			isCustomerDataValid = false;
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_LAST_NAME));
		} else if ((lastName).length() > 60) {
			customerData.setCustomerLastNameInValid(true);
			isCustomerDataValid = false;
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_LAST_NAME_LENGTH));
		}
		
		String gender = customerData.getGender();
		if (gender == null || GeneraliConstants.JSON_EMPTY.equals(gender)) {
			isCustomerDataValid = false;
			customerData.setGenderInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.EMPTY_GENDER));
		}

		String mobileNumber = customerData.getMobileNumber();
		if (mobileNumber == null
				|| GeneraliConstants.JSON_EMPTY.equals(mobileNumber)) {
			isCustomerDataValid = false;
			customerData.setMobileNumberInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_MOBILE_NUMBER));

		} else if (!isNumericOnly(mobileNumber)) {
			isCustomerDataValid = false;
			customerData.setMobileNumberInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_MOBILE_NUMBER));
		} else if ((mobileNumber.length() > 16)) {
			isCustomerDataValid = false;
			customerData.setMobileNumberInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_MOBILE_NUMBER));
		}

		String email = customerData.getEmailAddress();
		if (email != null && !GeneraliConstants.JSON_EMPTY.equals(email)
				&& !isValidMail(email)) {
			isCustomerDataValid = false;
			customerData.setEmailAddresInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_EMAIL_ID));
		} else if (email != null && email.length() > 50) {
			isCustomerDataValid = false;
			customerData.setEmailAddresInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_EMAIL_ID));
		}

		String address = customerData.getAddress();
		if (address != null && address.length() > 100) {
			isCustomerDataValid = false;
			customerData.setAddressInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INVALID_ADDRESS));
		}

		Date dob = customerData.getDob();
		if (dob != null) {
			if (validateDateCell(dob)) {
				isCustomerDataValid = false;
				customerData.setDOBInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_DOB));
			} else if (dob.after(currentDate)) {
				isCustomerDataValid = false;
				customerData.setDOBInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_DOB));
			}
		}
		String nationalID = customerData.getNationalID();
		if (nationalID != null
				&& (nationalID.contains(" ") || nationalID.contains("\n"))) {
			isCustomerDataValid = false;
			customerData.setNationalIDInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.NationalID_INVALID_CHARACTER));
		}
		String potential = customerData.getPotential();
		if (potential == null || GeneraliConstants.JSON_EMPTY.equals(potential)) {
			isCustomerDataValid = false;
			customerData.setPotentialInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.EMPTY_POTENTIAL));
		}

		String state = customerData.getState();
		if (state == null || GeneraliConstants.JSON_EMPTY.equals(state)) {
			isCustomerDataValid = false;
			customerData.setStateInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.EMPTY_STATE));
		}

		String source = customerData.getSource();
		if (source == null || GeneraliConstants.JSON_EMPTY.equals(source)) {
			isCustomerDataValid = false;
			customerData.setSourceInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.EMPTY_SOURCE));
		}

		String action = customerData.getAction();
		if (action == null || GeneraliConstants.JSON_EMPTY.equals(action)) {
			isCustomerDataValid = false;
			customerData.setActionInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(), GeneraliConstants.EMPTY_ACTION));
		}

		BigDecimal number = new BigDecimal(60 * 60 * 24);
		BigDecimal number2 = new BigDecimal(90 * 1000);
		number = number.multiply(number2);

		String nextMeetingTime = customerData.getNextMeetingTime();
		Date nextMeetingDate = null;
		Date nextMeetingDateTime = null;

		nextMeetingDate = customerData.getNextmeetingDate();
		nextMeetingDateTime = nextMeetingDate;
		if (nextMeetingDate != null) {
			// customerData.setNextmeetingDate(nextMeetingDate);

			if ((nextMeetingDate != null)
					&& (nextMeetingTime == null || GeneraliConstants.JSON_EMPTY
							.equals(nextMeetingTime))) {
				isCustomerDataValid = false;
				customerData.setNextMeetingTimeInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.NEXT_MEETING_TIME_INVALID));
			}
			if (validateDateCell(nextMeetingDate)) {
				isCustomerDataValid = false;
				customerData.setNextMeetinDateInValid(true);
				customerData
						.setRemarks(attachErrorMessages(
								customerData.getRemarks(),
								GeneraliConstants.EMPTY_DATE));
			}
			if (nextMeetingDate != null && nextMeetingTime != null
					&& !GeneraliConstants.JSON_EMPTY.equals(nextMeetingTime)) {
				nextMeetingDateTime = getModifiedDateTime(nextMeetingDate,
						nextMeetingTime);
			}

			BigDecimal nxtTime = new BigDecimal(nextMeetingDateTime.getTime());
			BigDecimal crtTime = new BigDecimal(currentDate.getTime());
			// nextMeetingDateTime = customerData.getNextmeetingDate();
			nxtTime = nxtTime.subtract(crtTime);

			if (nextMeetingDateTime.before(currentDate)) {
				isCustomerDataValid = false;
				customerData.setNextMeetinDateInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INAVALID_MEETING_DATE));

			} else if (nxtTime.compareTo(number) > 0) {
				isCustomerDataValid = false;
				customerData.setNextMeetinDateInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INAVALID_MEETING_DATE));
			}

		}

		if (!(nextMeetingTime == null)
				&& !GeneraliConstants.JSON_EMPTY.equals(nextMeetingTime)) {
			if (nextMeetingDate == null) {
				isCustomerDataValid = false;
				customerData.setNextMeetinDateInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.NEXT_MEETING_DATE_INVALID));
			}
			if (!isValidTimeFormat(nextMeetingTime)) {
				isCustomerDataValid = false;
				customerData.setNextMeetingTimeInValid(true);
				customerData
						.setRemarks(attachErrorMessages(
								customerData.getRemarks(),
								GeneraliConstants.EMPTY_DATE));
			}
		}

		String bank = customerData.getBank();
		if (bank == null || GeneraliConstants.JSON_EMPTY.equals(bank)) {
			isCustomerDataValid = false;
			customerData.setBankInValid(true);
			customerData.setRemarks(attachErrorMessages(
					customerData.getRemarks(),
					GeneraliConstants.INAVALID_BANK_NAME));
		}

		String refName = customerData.getRefName();
		if (refName != null && !GeneraliConstants.JSON_EMPTY.equals(refName)) {
			if (!isLettersandSpaceOnly(refName)) {
				isCustomerDataValid = false;
				customerData.setRefNameInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_REF_NAME));
			} else if (refName.length() > 30) {
				customerData.setRefNameInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_REF_NAME));
			}
		}

		String refMobNumber = customerData.getRefMobileNumber();
		if (refMobNumber != null
				&& !GeneraliConstants.JSON_EMPTY.equals(refMobNumber)) {
			if (!isNumericOnly(refMobNumber)) {
				isCustomerDataValid = false;
				customerData.setRefMobileNumberInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_MOBILE_NUMBER));
			} else if ((refMobNumber.length() > 16)) {
				isCustomerDataValid = false;
				customerData.setRefMobileNumberInValid(true);
				customerData.setRemarks(attachErrorMessages(
						customerData.getRemarks(),
						GeneraliConstants.INVALID_MOBILE_NUMBER));
			}

		}

		customerData.setStatus(isCustomerDataValid);
		return isCustomerDataValid;
	}

	/**
	 * Method to check Input contains letters and spaces Validation for Name
	 * Field
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLettersandSpaceOnly(String string) {
		boolean isValid = false;
		if (string != null) {
			Pattern p = Pattern.compile("^[\\p{L} .' ]+$");

			Matcher m = p.matcher(string);
			isValid = m.matches();
		}
		return isValid;

	}
	/**
	 * isLettersOnly
	 * @param string
	 * @return
	 */
	public boolean isLettersOnly(String string) {
		boolean isValid = false;
		if (string != null) {
			Pattern p = Pattern.compile("^[\\p{L}.']+$");

			Matcher m = p.matcher(string);
			isValid = m.matches();
		}
		return isValid;

	}

	/**
	 * method to check input is Numeric Validation for Phone Number
	 * 
	 * @param string
	 * @return
	 */
	public boolean isNumericOnly(String string) {

		boolean isValid = false;
		try {
			StringTokenizer st = new StringTokenizer(string, ".");
			int tokens = st.countTokens();
			String number = st.nextToken();
			String decimalPart = "0";
			if (tokens == 2) {
				decimalPart = st.nextToken();
			}
			if (tokens <= 2) {

				Integer deciaml = Integer.valueOf(decimalPart);
				if (deciaml == 0) {
					Pattern p = Pattern.compile("^[0-9]+");
					Matcher m = p.matcher(number);
					isValid = m.matches();
				}
			}
		} catch (Exception e) {

		}

		return isValid;

	}

	/**
	 * Method to validate HH:mm time format
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidTimeFormat(String string) {
		boolean isValid = false;
		try {
			StringTokenizer st = new StringTokenizer(string, ":");
			int tokens = st.countTokens();
			String hh = st.nextToken();
			String mm = st.nextToken();
			if (tokens == 2) {
				if (Integer.parseInt(hh) <= 23) {
					if (Integer.parseInt(mm) <= 59) {
						isValid = true;
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in timeFormat", e);
		}
		return isValid;

	}

	/**
	 * Method to Fill comments section with Validation Error messages
	 * 
	 * @param errorMessage1
	 * @param errorMessage2
	 * @return
	 */
	public String attachErrorMessages(String errorMessage1, String errorMessage2) {
		String result = errorMessage2;
		if (errorMessage1 != null) {
			StringBuffer sb = new StringBuffer(errorMessage1);
			sb.append("\n");
			sb.append(errorMessage2);
			result = sb.toString();
		}

		return result;
	}

	/**
	 * Method to validate email
	 * 
	 * @param mailId
	 * @return
	 */
	public boolean isValidMail(String mailId) {
		boolean isValid = false;
		if (mailId != null) {
			Pattern p = Pattern
					.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
							+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			Matcher m = p.matcher(mailId);
			isValid = m.matches();
		}
		return isValid;
	}

	
	/**
	 * Method to Generate 3 output excels based on the file processing
	 * 
	 * @param leadDataMap
	 * @param type
	 * @return
	 * @throws IOException
	 */
	public Map<String, String> generateExcelForMail(
			Map<String, List<BulkUploadData>> leadDataMap, String type)
			throws IOException {
		Map<String, String> excelList = new HashMap<String, String>();
		String fileName = "";
		String fileType = "";
		String dateTime = "";
		String validationCheckfailed = "";
		String successfullyAllocated = "";
		String unsuccessfullyAllocated = "";
		Date date = new Date();
		SimpleDateFormat dateFormatWithoutSlash = new SimpleDateFormat(DATE_FORMAT_WITHOUT_SLASH);
		SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_FORMAT_WITHOUT_SECONDS);
		dateTime = dateTime.concat(dateFormatWithoutSlash.format(date)).concat("_").concat(timeFormat.format(date));
		
		Calendar lCDateTime = Calendar.getInstance();
		Long timeInMS = lCDateTime.getTimeInMillis();

		validationCheckfailed = timeInMS+"_VALIDATION CHECK FAILED";
		successfullyAllocated = timeInMS+"_SUCCESSFULLY ALLOCATED";
		unsuccessfullyAllocated = timeInMS+"_UNSUCCESSFULLY ALLOCATED";
		try {
			if (leadDataMap != null) {
				if (GeneraliConstants.TYPE_BANCA.equals(type)) {
					fileName = bancaExcelFileName;
				} else if (GeneraliConstants.TYPE_AGENCY.equals(type)) {
					fileName = agencyExcelFileName;
				}

				List<BulkUploadData> validDataList = getLeadDataList(
						leadDataMap, GeneraliConstants.VALID);

				List<BulkUploadData> validationFailedDataList = getLeadDataList(
						leadDataMap, GeneraliConstants.INVALID);
				List<BulkUploadData> agentValidationFaileddata = getLeadDataList(
						leadDataMap, GeneraliConstants.AGENT_FAILED);
				if (validDataList != null && validDataList.size() > 0) {
					fileType = GeneraliConstants.VALID;
					Workbook savedDataWorkBook = writeToExcelFile(folderPath
							+ fileName, validDataList, type, fileType);
					if (savedDataWorkBook != null) {
						excelList.put(GeneraliConstants.VALID,
								 folderPath+successfullyAllocated+".xlsx");
						
						  FileOutputStream outputStream = new FileOutputStream(
						  folderPath+successfullyAllocated+".xlsx");
						  savedDataWorkBook.write(outputStream);
						  outputStream.flush();
						  outputStream.close();
						 
					}
				}
				if (validationFailedDataList != null
						&& validationFailedDataList.size() > 0) {
					fileType = GeneraliConstants.INVALID;
					Workbook validationFailedWorkBook = writeToExcelFile(
							folderPath + fileName, validationFailedDataList,
							type, fileType);
					if (validationFailedWorkBook != null) {

						excelList.put(GeneraliConstants.INVALID,
								folderPath+validationCheckfailed+".xlsx");
						
						 FileOutputStream outputStream = new FileOutputStream(
						folderPath+validationCheckfailed+".xlsx");
						 validationFailedWorkBook.write(outputStream);
						 outputStream.flush();
						  outputStream.close();
						 
					}
				}
				if (agentValidationFaileddata != null
						&& agentValidationFaileddata.size() > 0) {
					fileType = GeneraliConstants.INVALID;

					Workbook agentValidationFailedWorkBook = writeToExcelFile(
							folderPath + fileName, agentValidationFaileddata,
							type, fileType);
					if (agentValidationFailedWorkBook != null) {
						excelList.put(GeneraliConstants.AGENT_FAILED,
								folderPath+unsuccessfullyAllocated+".xlsx");
						
						 FileOutputStream outputStream = new FileOutputStream(
						 folderPath+unsuccessfullyAllocated+".xlsx");
						 agentValidationFailedWorkBook.write(outputStream);
						 outputStream.flush();
						  outputStream.close();
						 
					}
				}

			}
		} catch (NullPointerException e) {
			LOGGER.error("Filename is null");
		}
		return excelList;
	}
	/**
	 * Gets the lead data
	 * 
	 * @param leadDataMap
	 * @param Key
	 * @return
	 */
	public List<BulkUploadData> getLeadDataList(
			Map<String, List<BulkUploadData>> leadDataMap, String Key) {
		List<BulkUploadData> leadDataList = null;
		if (leadDataMap.containsKey(Key)) {
			leadDataList = leadDataMap.get(Key);
		}
		return leadDataList;

	}

	/**
	 * Method to apply the defined style to output excel
	 * 
	 * @param flag
	 * @param cellStyleValid
	 * @param cellStleInvalid
	 * @param cell
	 */
	public void applyCellStyle(Boolean flag, CellStyle cellStyleValid,
			CellStyle cellStleInvalid, Cell cell) {
		CellStyle cellStyleResult = cellStyleValid;
		if (flag) {
			cellStyleResult = cellStleInvalid;
		}
		cell.setCellStyle(cellStyleResult);
	}

	/**
	 * Method to get the state, occupation etc codes from Code-Name input
	 * 
	 * @param string
	 * @return
	 */
	public String getCode(String string) {
		String code = "";
		try {
			StringTokenizer st = new StringTokenizer(string, "-");
			int tokens = st.countTokens();
			if (tokens > 1) {
				code = st.nextToken();
				if (code != null) {
					code = code.trim();
				}
			}
		} catch (Exception e) {

		}
		return code;
	}

	/**
	 * Method to get Name from Code-Name input
	 * 
	 * @param string
	 * @return
	 */
	public String getValue(String string) {
		String code = "";
		String value = "";
		try {
			StringTokenizer st = new StringTokenizer(string, "-");
			int tokens = st.countTokens();

			if (tokens > 1) {
				code = st.nextToken();
				value = st.nextToken();
				if (value != null) {
					value = value.trim();
				}
			}
		} catch (Exception e) {

		}
		return value;
	}

	/**
	 * Method to read a String input from excel
	 * 
	 * @param dataCell
	 * @return
	 */
	public String returnStringCell(Cell dataCell) {
		String value = null;
		if (dataCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			if (DateUtil.isCellDateFormatted(dataCell)) {
				value = new DataFormatter().formatCellValue(dataCell);
			} else {
				value = String.valueOf((dataCell.getNumericCellValue()));
				double d = dataCell.getNumericCellValue();
			}

		} else if (dataCell.getCellType() == Cell.CELL_TYPE_STRING) {

			value = dataCell.getStringCellValue();
		}
		return value;
	}

	/**
	 * Method to read Numeric cell
	 * 
	 * @param dataCell
	 * @return
	 */
	public Long returnNumericCell(Cell dataCell) {
		Double value = null;
		try {
			if (dataCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				value = dataCell.getNumericCellValue();

			} else if (dataCell.getCellType() == Cell.CELL_TYPE_STRING) {

				value = Double.parseDouble(dataCell.getStringCellValue());
			}

		} catch (Exception e) {

		}
		return value != null ? value.longValue() : null;
	}

	/**
	 * Method to validate Date cell.
	 * 
	 * @param date
	 * @return
	 */
	public boolean validateDateCell(Date date) {
		Date defaultErrorDate = new Date();
		boolean isDateInValid = false;
		;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		String input = "01/01/1900";
		try {
			defaultErrorDate = sdf.parse(input);
			if (date.equals(defaultErrorDate))
				isDateInValid = true;

		} catch (IllegalStateException e) {
			LOGGER.error("Date format not correct");
		} catch (IllegalArgumentException e) {
			LOGGER.error("format not correct");
		} catch (ParseException e) {
			LOGGER.error("Parsing Exception");
		} catch (NullPointerException e) {
			LOGGER.error("Date is null");
		}

		return isDateInValid;
	}

	/**
	 * Method to get Date cell
	 * 
	 * @param date
	 * @return
	 */
	public Date getDateCell(String date) {
		Date input = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			input = sdf.parse(date);

		} catch (IllegalStateException e) {
			LOGGER.error("Date format not correct", e);
		} catch (IllegalArgumentException e) {
			LOGGER.error("format not correct", e);
		} catch (ParseException e) {
			LOGGER.error("Parsing Exception", e);
		} catch (NullPointerException e) {
			LOGGER.error("Date is null", e);
		}

		return input;
	}

	/**
	 * Method to read Date cell from excel
	 * 
	 * @param dataCell
	 * @return
	 */
	public Date returnDateCell(Cell dataCell) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		String input = "01/01/1900";
		Date value = null;
		try {
			if (Cell.CELL_TYPE_BLANK != dataCell.getCellType()) {
				value = sdf.parse(input);
			}

			if (Cell.CELL_TYPE_NUMERIC == dataCell.getCellType()) {
				if (DateUtil.isCellDateFormatted(dataCell)) {
					value = dataCell.getDateCellValue();
				} else {
					value = sdf.parse(input);
				}
			} else if (Cell.CELL_TYPE_STRING == dataCell.getCellType()) {
				String dateString = dataCell.getStringCellValue();
				value = sdf.parse(dateString);
			}

		} catch (IllegalStateException e) {
			LOGGER.error("Date format not correct", e);
		} catch (IllegalArgumentException e) {
			LOGGER.error("format not correct", e);
		} catch (ParseException e) {
			LOGGER.error("Parsing Exception", e);
		} catch (NullPointerException e) {
			LOGGER.error("Date is null", e);
		}
		return value;
	}

	public String getMobileNumber(String mobileNumber) {

		String result = mobileNumber;
		String number = "";
		try {
			StringTokenizer st = new StringTokenizer(mobileNumber, ".");

			number = st.nextToken();

		} catch (Exception e) {
			LOGGER.error("Error in handling Mobile Number", e);
		}
		result = number;
		return result;
	}

	public boolean validateNextMeetingDate(Date meetingDate) {
		Calendar calCurrent = Calendar.getInstance();
		Calendar nextmeeting = Calendar.getInstance();
		nextmeeting.setTime(meetingDate);
		int d = calCurrent.compareTo(nextmeeting);
		return false;

	}

	public String getDobString(Date dob) {
		String dateString = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		dateString = sdf.format(dob);
		return dateString;

	}

	/**
	 * Method to make next meeting date from Next meeting date and time
	 * 
	 * @param nextMeetingDate
	 * @param time
	 * @return
	 */
	public Date getModifiedDateTime(Date nextMeetingDate, String time) {
		String dateString = "";
		Date modifiedDateTime = nextMeetingDate;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			dateString = sdf.format(nextMeetingDate);
			if (time != null && !GeneraliConstants.JSON_EMPTY.equals(time)) {
				dateString = dateString + " " + time;
			}
			SimpleDateFormat sdfDateTime = new SimpleDateFormat(
					"dd/MM/yyyy hh:mm");
			modifiedDateTime = sdfDateTime.parse(dateString);
		} catch (Exception e) {
			LOGGER.error("Error in parsing modified date", e);
		}
		return modifiedDateTime;

	}

    public String generateExcelReport(List<AgentReportVo> agentData, String mode) {
        String fileName = "";
        Workbook workbook = null;
        Sheet sheet = null;
       // folderPath="C:/Application/";
        try {
            fileName = getFileName(mode);
            fileName = fileName + ".xlsx";
            workbook = new XSSFWorkbook();
            
            //Create sheet
            sheet = workbook.createSheet("AgentReport");
            
            // Create a Font for styling header cells
            Font headerFont = workbook.createFont();
            headerFont.setBoldweight((short) 14);
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setColor(IndexedColors.RED.getIndex());
            
            // Create a CellStyle with the font
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            
            // Create a Row
            Row headerRow = sheet.createRow(0);

            // Create cells
            for(int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }
            // Create Other rows and cells

            int rowNum = 1;
            for (AgentReportVo agentVo : agentData) {
                Row row = sheet.createRow(rowNum++);
             //   HSSFRow o;
                row.createCell(0).setCellValue(agentVo.getAgentCode());
                row.createCell(1).setCellValue(agentVo.getAgentName());
                row.createCell(2).setCellValue(agentVo.getPosition());
                row.createCell(3).setCellValue(agentVo.getSmName());
                row.createCell(4).setCellValue(agentVo.getGmName());
                row.createCell(5).setCellValue(agentVo.getTeamName());
                Cell c = row.createCell(6);
                
                c.setCellType(Cell.CELL_TYPE_NUMERIC);
                c.setCellValue(Integer.parseInt(agentVo.getLeadCount() != null ? agentVo.getLeadCount() : "0"));
               
                c = row.createCell(7);
                c.setCellType(Cell.CELL_TYPE_NUMERIC);
                c.setCellValue(Integer.parseInt(agentVo.getCallCount() != null ? agentVo.getCallCount() : "0"));
             
                c = row.createCell(8);
                c.setCellType(Cell.CELL_TYPE_NUMERIC);
                c.setCellValue(Integer.parseInt(agentVo.getAppointmentcount() != null ? agentVo.getAppointmentcount() : "0"));
               
                c = row.createCell(9);
                c.setCellType(Cell.CELL_TYPE_NUMERIC);
                c.setCellValue(Integer.parseInt(agentVo.getVisitCount() != null ? agentVo.getVisitCount() : "0"));
               
                c = row.createCell(10);
                c.setCellType(Cell.CELL_TYPE_NUMERIC);
                c.setCellValue(Integer.parseInt(agentVo.getIllustrationCount() != null ? agentVo.getIllustrationCount() : "0"));
              
                c = row.createCell(11);
                c.setCellType(Cell.CELL_TYPE_NUMERIC);
                c.setCellValue(Integer.parseInt(agentVo.geteAppCount() != null ? agentVo.geteAppCount() : "0"));  // row.createCell(11).setCellValue(agentVo.geteAppCount() != null ? agentVo.geteAppCount() : "0");
            }

            // Resize all columns to fit the content size
            for(int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }

            FileOutputStream outputStream = new FileOutputStream(folderPath + fileName);
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            LOGGER.error("Error while generating excel report" + e.getMessage());
        }
        // TODO Auto-generated method stub
        return fileName;
    }

    private String getFileName(String mode) {
        SimpleDateFormat dateFormatWithoutSlash = new SimpleDateFormat(DATE_FORMAT_WITHOUT_SLASH);
        Date date = null;
        String dateString = "";
        if (mode.equalsIgnoreCase("Weekly")) {
            dateString = "AgentWeeklyReport_";
        } else {
            dateString = "AgentMonthlyReport_";
        }

        try {
            date = new Date();
            dateString = dateString.concat(dateFormatWithoutSlash.format(date));

        } catch (Exception e) {
            LOGGER.error("Error while creating agent report file name");

        }
        return dateString;

    }
    
   /* public static void main(String[] arg) {
        // System.out.println(getFileName());
        new GeneraliExcelSheetUtil().generateExcelReport(null);
    }*/
    
}
