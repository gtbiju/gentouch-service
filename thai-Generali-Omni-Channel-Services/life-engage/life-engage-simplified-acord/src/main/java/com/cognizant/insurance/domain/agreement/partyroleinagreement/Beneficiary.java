/**
 * 
 */
package com.cognizant.insurance.domain.agreement.partyroleinagreement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;

import com.cognizant.insurance.domain.agreement.agreementcodelists.BeneficiaryDesignationCodeList;
import com.cognizant.insurance.domain.commonelements.commoncodelists.BenefitDistributionCalculationCodeList;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;

/**
 * A party who is designated by the owner of an insurance policy to receive the
 * proceeds from one or more of the benefits defined within the terms of the
 * policy.
 * 
 * e.g: Mary Smith, as beneficiary of an endowment policy.
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("BENEFICIARY")
public class Beneficiary extends PartyRoleInAgreement {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4924638561733350399L;

    /**
     * Designation of beneficiary. Indicates how the beneficiary will be
     * identified.
     * 
     * e.g: By name
     * 
     * e.g: Children of the insured
     * 
     * e.g: Domestic partner
     * 
     * e.g: Insured
     * 
     * e.g: Spouse
     * 
     * 
     * 
     */
    @Enumerated
    private BeneficiaryDesignationCodeList designationCode;

    /**
     * Indicates whether or not the beneficiary is irrevocable. TRUE if
     * irrevocable, FALSE if revocable. 
     * 
     * 
     * 
     */
    private Boolean irrevocableIndicator;

    /**
     * The exact, legal, beneficiary description. 
     * 
     * 
     * 
     */
    private String legalWording;

    /**
     * Indicates whether this beneficiary has explicitly accepted the beneficial
     * rights of the financial services agreement in the context of which this
     * role is being played. Such acceptance may have the consequence that the
     * policyholder can no longer solely decide about changes to the insurance
     * policy.
     * 
     * 
     * 
     */
    private Boolean acceptedIndicator;

    /**
     * Indicates the Float of the benefit amount that can be excluded from
     * current income for the purpose of calculating income taxation.
     * 
     * 
     * 
     */
    private Float partExemptFromTaxesPercentage;

    /**
     * Indicates the way the share of the benefit for this beneficiary is
     * calculated.
     * 
     * 
     * 
     */
    @Enumerated
    private BenefitDistributionCalculationCodeList benefitDistributionCalculationCode;
    
    /******Newly added for EAp - Start********/
    /**
     * Indicates the percentage of share that the beneficiary receives. 
     */
    private Float sharePercentage;
    
    /**
     * Indicates the relationship of the beneficiary with insured. 
     */    
    private String relationWithInsured;
    /******Newly added for EAp - End ********/
    
    /** Added for generali */
    
    private String isSameAsPolicyHolder;
    
    /** Added for generali */
    
    private Boolean isSameAsPolicyHolderFlag;
    
    /** Added for generali */
    private Boolean hasEngagementLetter;
    
    private String beneficiaryId="0";
    
    
    public Boolean getHasEngagementLetter() {
        return hasEngagementLetter;
    }

    public void setHasEngagementLetter(Boolean hasEngagementLetter) {
        this.hasEngagementLetter = hasEngagementLetter;
    }

    public String getIsSameAsPolicyHolder() {
		return isSameAsPolicyHolder;
	}

	public void setIsSameAsPolicyHolder(String isSameAsPolicyHolder) {
		this.isSameAsPolicyHolder = isSameAsPolicyHolder;
	}

	public Boolean getIsSameAsPolicyHolderFlag() {
		return isSameAsPolicyHolderFlag;
	}

	public void setIsSameAsPolicyHolderFlag(Boolean isSameAsPolicyHolderFlag) {
		this.isSameAsPolicyHolderFlag = isSameAsPolicyHolderFlag;
	}

	/**
     * Gets the designationCode.
     * 
     * @return Returns the designationCode.
     */
    public BeneficiaryDesignationCodeList getDesignationCode() {
        return designationCode;
    }

    /**
     * Gets the irrevocableIndicator.
     * 
     * @return Returns the irrevocableIndicator.
     */
    public Boolean getIrrevocableIndicator() {
        return irrevocableIndicator;
    }

    /**
     * Gets the legalWording.
     * 
     * @return Returns the legalWording.
     */
    public String getLegalWording() {
        return legalWording;
    }

    /**
     * Gets the acceptedIndicator.
     * 
     * @return Returns the acceptedIndicator.
     */
    public Boolean getAcceptedIndicator() {
        return acceptedIndicator;
    }

    /**
     * Gets the partExemptFromTaxesPercentage.
     * 
     * @return Returns the partExemptFromTaxesPercentage.
     */
    public Float getPartExemptFromTaxesPercentage() {
        return partExemptFromTaxesPercentage;
    }

    /**
     * Gets the benefitDistributionCalculationCode.
     * 
     * @return Returns the benefitDistributionCalculationCode.
     */
    public BenefitDistributionCalculationCodeList
            getBenefitDistributionCalculationCode() {
        return benefitDistributionCalculationCode;
    }

    /**
     * Sets The designationCode.
     * 
     * @param designationCode
     *            The designationCode to set.
     */
    public void setDesignationCode(
            final BeneficiaryDesignationCodeList designationCode) {
        this.designationCode = designationCode;
    }

    /**
     * Sets The irrevocableIndicator.
     * 
     * @param irrevocableIndicator
     *            The irrevocableIndicator to set.
     */
    public void
            setIrrevocableIndicator(final Boolean irrevocableIndicator) {
        this.irrevocableIndicator = irrevocableIndicator;
    }

    /**
     * Sets The legalWording.
     * 
     * @param legalWording
     *            The legalWording to set.
     */
    public void setLegalWording(final String legalWording) {
        this.legalWording = legalWording;
    }

    /**
     * Sets The acceptedIndicator.
     * 
     * @param acceptedIndicator
     *            The acceptedIndicator to set.
     */
    public void setAcceptedIndicator(final Boolean acceptedIndicator) {
        this.acceptedIndicator = acceptedIndicator;
    }

    /**
     * Sets The partExemptFromTaxesPercentage.
     * 
     * @param partExemptFromTaxesPercentage
     *            The partExemptFromTaxesPercentage to set.
     */
    public void setPartExemptFromTaxesPercentage(
            final Float partExemptFromTaxesPercentage) {
        this.partExemptFromTaxesPercentage = partExemptFromTaxesPercentage;
    }

    /**
     * Sets The benefitDistributionCalculationCode.
     * 
     * @param benefitDistributionCalculationCode
     *            The benefitDistributionCalculationCode to set.
     */
    public
            void
            setBenefitDistributionCalculationCode(
                    final BenefitDistributionCalculationCodeList benefitDistributionCalculationCode) {
        this.benefitDistributionCalculationCode =
                benefitDistributionCalculationCode;
    }

    /**
     * @return the sharePercentage
     */
    public Float getSharePercentage() {
        return sharePercentage;
    }

    /**
     * @param sharePercentage the sharePercentage to set
     */
    public void setSharePercentage(Float sharePercentage) {
        this.sharePercentage = sharePercentage;
    }

    /**
     * Gets the relationWithInsured.
     *
     * @return Returns the relationWithInsured.
     */
    public final String getRelationWithInsured() {
        return relationWithInsured;
    }

    /**
     * Sets The relationWithInsured.
     *
     * @param relationWithInsured The relationWithInsured to set.
     */
    public final void setRelationWithInsured(String relationWithInsured) {
        this.relationWithInsured = relationWithInsured;
    }

	public String getBeneficiaryId() {
		return beneficiaryId;
	}

	public void setBeneficiaryId(String beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}
    

}
