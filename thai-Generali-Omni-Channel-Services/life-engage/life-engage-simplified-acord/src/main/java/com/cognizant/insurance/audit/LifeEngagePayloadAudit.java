/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 12, 2013
 */
package com.cognizant.insurance.audit;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Lob;

/**
 * The Class class LifeEngagePayloadAudit.
 * 
 * @author 300797
 */
@Entity
public class LifeEngagePayloadAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/** offlineIdentifier. */
	private String offlineIdentifier;

	/** Customer Id. */
	private String key1;

	/** FNA Id. */
	private String key2;

	/** Illustration Id. */
	private String key3;

	/** ProposalId. */
	private String key4;
	
	/** Product Id. */
	private String key5;

	/** . */
	private String key6;

	/** . */
	private String key7;

	/** . */
	private String key8;
	
	/** RuleStatus. */
	private String key9;
	
	/** RetrieveType. */
	private String key10;
	
	/** Agent Id. */
	private String key11;
	
	/** Last Sync Date. */
	@Temporal(TemporalType.TIMESTAMP)
	private Date key12;
	
	/** Creation Date. */
	@Temporal(TemporalType.TIMESTAMP)
	private Date key13;

	/** Modified Date. */
	@Temporal(TemporalType.TIMESTAMP)
	private Date key14;
	
	/** Status. */
	private String key15;
	
	/** Sync Status. */
	private String key16;
	
	/** Sync Error. */
	private String key17;
	
	/** . */
	private String key18;
	
	/** . */
	private String key19;
	
	/** . */
	private String key20;
	
	/** . */
	private String key21;
	
	/** . */
	private String key22;
	
	/** s. */
	private String key23;
	
	/** . */
	private String key24;
	
	/** . */
	private String key25;

	/** The trans tracking id. */
	private String transTrackingID;

	/** The Type. */
	private String type;

	/** The request hash code. */
	private Integer requestHashCode;
	
	/** The request json. */
    @Lob
    private String responseJson;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name="lifeEngageAuditId")
	private LifeEngageAudit lifeEngageAudit;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the key1
	 */
	public String getKey1() {
		return key1;
	}

	/**
	 * @return the key2
	 */
	public String getKey2() {
		return key2;
	}

	/**
	 * @return the key3
	 */
	public String getKey3() {
		return key3;
	}


	/**
	 * @return the key6
	 */
	public String getKey6() {
		return key6;
	}

	/**
	 * @return the key7
	 */
	public String getKey7() {
		return key7;
	}

	/**
	 * @return the key8
	 */
	public String getKey8() {
		return key8;
	}

	/**
	 * @return the offlineIdentifier
	 */
	public String getOfflineIdentifier() {
		return offlineIdentifier;
	}

	/**
	 * @return the requestHashCode
	 */
	public Integer getRequestHashCode() {
		return requestHashCode;
	}

	/**
	 * @return the transTrackingID
	 */
	public String getTransTrackingID() {
		return transTrackingID;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param key1
	 *            the key1 to set
	 */
	public void setKey1(String key1) {
		this.key1 = key1;
	}

	/**
	 * @param key2
	 *            the key2 to set
	 */
	public void setKey2(String key2) {
		this.key2 = key2;
	}

	/**
	 * @param key3
	 *            the key3 to set
	 */
	public void setKey3(String key3) {
		this.key3 = key3;
	}


	/**
	 * @param key6
	 *            the key6 to set
	 */
	public void setKey6(String key6) {
		this.key6 = key6;
	}

	/**
	 * @param key7
	 *            the key7 to set
	 */
	public void setKey7(String key7) {
		this.key7 = key7;
	}

	/**
	 * @param key8
	 *            the key8 to set
	 */
	public void setKey8(String key8) {
		this.key8 = key8;
	}

	/**
	 * @param offlineIdentifier
	 *            the offlineIdentifier to set
	 */
	public void setOfflineIdentifier(String offlineIdentifier) {
		this.offlineIdentifier = offlineIdentifier;
	}

	/**
	 * @param requestHashCode
	 *            the requestHashCode to set
	 */
	public void setRequestHashCode(Integer requestHashCode) {
		this.requestHashCode = requestHashCode;
	}

	/**
	 * @param transTrackingID
	 *            the transTrackingID to set
	 */
	public void setTransTrackingID(String transTrackingID) {
		this.transTrackingID = transTrackingID;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

    /**
     * @param lifeEngageAudit the lifeEngageAudit to set
     */
    public void setLifeEngageAudit(LifeEngageAudit lifeEngageAudit) {
        this.lifeEngageAudit = lifeEngageAudit;
    }

    /**
     * @return the lifeEngageAudit
     */
    public LifeEngageAudit getLifeEngageAudit() {
        return lifeEngageAudit;
    }

	public String getKey4() {
		return key4;
	}

	public void setKey4(String key4) {
		this.key4 = key4;
	}

	public String getKey5() {
		return key5;
	}

	public void setKey5(String key5) {
		this.key5 = key5;
	}

	public String getKey9() {
		return key9;
	}

	public void setKey9(String key9) {
		this.key9 = key9;
	}

	public String getKey10() {
		return key10;
	}

	public void setKey10(String key10) {
		this.key10 = key10;
	}

	public String getKey11() {
		return key11;
	}

	public void setKey11(String key11) {
		this.key11 = key11;
	}

	public Date getKey12() {
		return key12;
	}

	public void setKey12(Date key12) {
		this.key12 = key12;
	}

	public Date getKey13() {
		return key13;
	}

	public void setKey13(Date key13) {
		this.key13 = key13;
	}

	public Date getKey14() {
		return key14;
	}

	public void setKey14(Date key14) {
		this.key14 = key14;
	}

	public String getKey15() {
		return key15;
	}

	public void setKey15(String key15) {
		this.key15 = key15;
	}

	public String getKey16() {
		return key16;
	}

	public void setKey16(String key16) {
		this.key16 = key16;
	}

	public String getKey17() {
		return key17;
	}

	public void setKey17(String key17) {
		this.key17 = key17;
	}

	public String getKey18() {
		return key18;
	}

	public void setKey18(String key18) {
		this.key18 = key18;
	}

	public String getKey19() {
		return key19;
	}

	public void setKey19(String key19) {
		this.key19 = key19;
	}

	public String getKey20() {
		return key20;
	}

	public void setKey20(String key20) {
		this.key20 = key20;
	}

	public String getKey21() {
		return key21;
	}

	public void setKey21(String key21) {
		this.key21 = key21;
	}

	public String getKey22() {
		return key22;
	}

	public void setKey22(String key22) {
		this.key22 = key22;
	}

	public String getKey23() {
		return key23;
	}

	public void setKey23(String key23) {
		this.key23 = key23;
	}

	public String getKey24() {
		return key24;
	}

	public void setKey24(String key24) {
		this.key24 = key24;
	}

	public String getKey25() {
		return key25;
	}

	public void setKey25(String key25) {
		this.key25 = key25;
	}

	public String getResponseJson() {
		return responseJson;
	}

	public void setResponseJson(String responseJson) {
		this.responseJson = responseJson;
	}
}
