/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * The Interface interface EmailService.
 */
public interface EmailService {

    /**
     * Sends the email.
     *
     * @param json the json
     * @return the string
     * @throws BusinessException the business exception
     */
    String sendEmail(String json) throws BusinessException;
    
    /**
     * Trigger the initialized email .
     *
     * @param json the json
     * @throws BusinessException the business exception
     */
    void triggerEmail(final String json) throws BusinessException;



    /**
     * Update email details.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String updateEmailDetails(String json) throws BusinessException;
    
    /**
     * Sends the email with attachment.
     *
     * @param json the json
     * @return the string
     * @throws BusinessException the business exception
     */
    String sendEmailWithAttachment(String json) throws BusinessException;
}
