/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 19, 2015
 */
package com.cognizant.insurance.component.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.insurance.component.LifeEngageEncryptionKeyComponent;
import com.cognizant.insurance.component.repository.EncryptionKeyRepository;

/**
 * The Class class LifeEngageEncryptionKeyComponentImpl.
 */
@Service("lifeEngageEncryptionKeyComponent")
public class LifeEngageEncryptionKeyComponentImpl implements LifeEngageEncryptionKeyComponent {

	/** The encryption key repository. */
	@Autowired
    private EncryptionKeyRepository encryptionKeyRepository;
    
    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageEncryptionKeyComponentImpl.class);
    
    
    /**
     * Fetch key.
     *
     * @return the string
     */
    @Override
    public String fetchKey() {
	   
	 String response =  encryptionKeyRepository.fetchKey();
	   
	   return response;
   }

    
}
