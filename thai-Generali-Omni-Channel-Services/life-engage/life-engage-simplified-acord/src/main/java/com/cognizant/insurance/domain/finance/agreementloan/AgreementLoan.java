/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Feb 6, 2013
 */

package com.cognizant.insurance.domain.finance.agreementloan;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.agreement.FinancialServicesAgreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.LoanInterestMethodCodeList;
import com.cognizant.insurance.domain.agreement.agreementcodelists.LoanInterestTimingCodeList;
import com.cognizant.insurance.domain.agreement.agreementcodelists.LoanInterestTypeCodeList;
import com.cognizant.insurance.domain.agreement.agreementcodelists.LoanReasonCodeList;
import com.cognizant.insurance.domain.agreement.agreementcodelists.LoanStatusCodeList;
import com.cognizant.insurance.domain.commonelements.commoncodelists.FinancialMediumTypeCodeList;
import com.cognizant.insurance.domain.commonelements.commoncodelists.LoanTypeCodeList;
import com.cognizant.insurance.domain.commonelements.commoncodelists.PaymentFrequencyCodeList;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.domain.finance.FinancialProvision;

/**
 * An agreement loan (or policy loan) is a loan made by an insurer from its general funds to a policy owner on the
 * security of the cash value of a life insurance policy.
 * 
 * Generally, loans may reduce the policy's death benefit and cash value. <!-- end-UML-doc -->
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class AgreementLoan extends FinancialProvision {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8360375128188050159L;

    /**
     * Amount of the loan.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private CurrencyAmount loanAmount;

    /**
     * This field allows to describe the reasons of the loan.
     * 
     * 
     * 
     */
    @Enumerated
    private LoanReasonCodeList loanReasonTypeCode;

    /**
     * Values are New Re new.
     * 
     * 
     * 
     */
    private String natureOfLoan;

    /**
     * This is the % of the loan versus the contract amount (Capital, other).
     * 
     * 
     * 
     */
    private Float loanOfAgreementValuePercentage;

    /**
     * interest rate that applied to the loan .
     * 
     * 
     * 
     */
    private Float annualInterestRatePercentage;
    
    /**
     * outstanding rate that applied to the loan .
     * 
     * 
     * 
     */
    private Float annualOutstandingRatePercentage;
    

    /**
     * Different means of payment such as check, cash, direct debit, credit card, etc...
     * 
     * 
     * 
     */
    @Enumerated
    private FinancialMediumTypeCodeList paymentMeansCode;

    /**
     * Description of the mean of payment, Name, Bank name, address ...
     * 
     * 
     * 
     */
    private String accountDescription;

    /**
     * Values are weeks month.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private TimePeriod unitOfLoanPeriod;

    /**
     * This is the duration of the loan.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private TimePeriod loanPeriod;

    /**
     * Defines the frequency of the loan repayment.
     * 
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private PaymentFrequencyCodeList paymentFrequencyCode;

    /**
     * Indicates the type of loan.
     * 
     * 
     * 
     */
    @Enumerated
    private LoanTypeCodeList loanTypeCode;

    /**
     * Indicates the status of the loan.
     * 
     * 
     * 
     */
    @Enumerated
    private LoanStatusCodeList loanStatusCode;

    /**
     * Indicates the type of loan interest rate.
     * 
     * 
     * 
     */
    @Enumerated
    private LoanInterestTypeCodeList loanInterestRateTypeCode;

    /**
     * Indicates the timing of the loan interest.
     * 
     * 
     * 
     */
    @Enumerated
    private LoanInterestTimingCodeList loanInterestTimingCode;

    /**
     * Indicates the method for the loan interest.
     * 
     * 
     * 
     */
    @Enumerated
    private LoanInterestMethodCodeList loanInterestMethodCode;

    /**
     * Next due loan repayment date.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date nextDueDate;

    /**
     * This relationship links an agreement loan to its related financial services agreement.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private FinancialServicesAgreement relatedAgreement;

    /** The Loan repay num year. */
    private Integer loanRepayNumYear;

    /** The Issue age. */
    private Integer issueAge;

    /******Newly added for Illustration - Start ********/

    /** The required. */
    private Boolean required;

    

    /******Newly added for Illustration - End ********/

    /**
     * Gets the loanAmount.
     * 
     * @return Returns the loanAmount.
     */
    public CurrencyAmount getLoanAmount() {
        return loanAmount;
    }

    /**
     * Gets the loanReasonTypeCode.
     * 
     * @return Returns the loanReasonTypeCode.
     */
    public LoanReasonCodeList getLoanReasonTypeCode() {
        return loanReasonTypeCode;
    }

    /**
     * Gets the natureOfLoan.
     * 
     * @return Returns the natureOfLoan.
     */
    public String getNatureOfLoan() {
        return natureOfLoan;
    }

    /**
     * Gets the loanOfAgreementValuePercentage.
     * 
     * @return Returns the loanOfAgreementValuePercentage.
     */
    public Float getLoanOfAgreementValuePercentage() {
        return loanOfAgreementValuePercentage;
    }

    /**
     * Gets the annualInterestRatePercentage.
     * 
     * @return Returns the annualInterestRatePercentage.
     */
    public Float getAnnualInterestRatePercentage() {
        return annualInterestRatePercentage;
    }

    /**
     * Gets the paymentMeansCode.
     * 
     * @return Returns the paymentMeansCode.
     */
    public FinancialMediumTypeCodeList getPaymentMeansCode() {
        return paymentMeansCode;
    }

    /**
     * Gets the accountDescription.
     * 
     * @return Returns the accountDescription.
     */
    public String getAccountDescription() {
        return accountDescription;
    }

    /**
     * Gets the unitOfLoanPeriod.
     * 
     * @return Returns the unitOfLoanPeriod.
     */
    public TimePeriod getUnitOfLoanPeriod() {
        return unitOfLoanPeriod;
    }

    /**
     * Gets the loanPeriod.
     * 
     * @return Returns the loanPeriod.
     */
    public TimePeriod getLoanPeriod() {
        return loanPeriod;
    }

    /**
     * Gets the paymentFrequencyCode.
     * 
     * @return Returns the paymentFrequencyCode.
     */
    public PaymentFrequencyCodeList getPaymentFrequencyCode() {
        return paymentFrequencyCode;
    }

    /**
     * Gets the loanTypeCode.
     * 
     * @return Returns the loanTypeCode.
     */
    public LoanTypeCodeList getLoanTypeCode() {
        return loanTypeCode;
    }

    /**
     * Gets the loanStatusCode.
     * 
     * @return Returns the loanStatusCode.
     */
    public LoanStatusCodeList getLoanStatusCode() {
        return loanStatusCode;
    }

    /**
     * Gets the loanInterestRateTypeCode.
     * 
     * @return Returns the loanInterestRateTypeCode.
     */
    public LoanInterestTypeCodeList getLoanInterestRateTypeCode() {
        return loanInterestRateTypeCode;
    }

    /**
     * Gets the loanInterestTimingCode.
     * 
     * @return Returns the loanInterestTimingCode.
     */
    public LoanInterestTimingCodeList getLoanInterestTimingCode() {
        return loanInterestTimingCode;
    }

    /**
     * Gets the loanInterestMethodCode.
     * 
     * @return Returns the loanInterestMethodCode.
     */
    public LoanInterestMethodCodeList getLoanInterestMethodCode() {
        return loanInterestMethodCode;
    }

    /**
     * Gets the nextDueDate.
     * 
     * @return Returns the nextDueDate.
     */
    public Date getNextDueDate() {
        return nextDueDate;
    }

    /**
     * Gets the relatedAgreement.
     * 
     * @return Returns the relatedAgreement.
     */
    public FinancialServicesAgreement getRelatedAgreement() {
        return relatedAgreement;
    }

    /**
     * Sets The loanAmount.
     * 
     * @param loanAmount
     *            The loanAmount to set.
     */
    public void setLoanAmount(final CurrencyAmount loanAmount) {
        this.loanAmount = loanAmount;
    }

    /**
     * Sets The loanReasonTypeCode.
     * 
     * @param loanReasonTypeCode
     *            The loanReasonTypeCode to set.
     */
    public void setLoanReasonTypeCode(final LoanReasonCodeList loanReasonTypeCode) {
        this.loanReasonTypeCode = loanReasonTypeCode;
    }

    /**
     * Sets The natureOfLoan.
     * 
     * @param natureOfLoan
     *            The natureOfLoan to set.
     */
    public void setNatureOfLoan(final String natureOfLoan) {
        this.natureOfLoan = natureOfLoan;
    }

    /**
     * Sets The loanOfAgreementValuePercentage.
     * 
     * @param loanOfAgreementValuePercentage
     *            The loanOfAgreementValuePercentage to set.
     */
    public void setLoanOfAgreementValuePercentage(final Float loanOfAgreementValuePercentage) {
        this.loanOfAgreementValuePercentage = loanOfAgreementValuePercentage;
    }

    /**
     * Sets The annualInterestRatePercentage.
     * 
     * @param annualInterestRatePercentage
     *            The annualInterestRatePercentage to set.
     */
    public void setAnnualInterestRatePercentage(final Float annualInterestRatePercentage) {
        this.annualInterestRatePercentage = annualInterestRatePercentage;
    }

    /**
     * Sets The paymentMeansCode.
     * 
     * @param paymentMeansCode
     *            The paymentMeansCode to set.
     */
    public void setPaymentMeansCode(final FinancialMediumTypeCodeList paymentMeansCode) {
        this.paymentMeansCode = paymentMeansCode;
    }

    /**
     * Sets The accountDescription.
     * 
     * @param accountDescription
     *            The accountDescription to set.
     */
    public void setAccountDescription(final String accountDescription) {
        this.accountDescription = accountDescription;
    }

    /**
     * Sets The unitOfLoanPeriod.
     * 
     * @param unitOfLoanPeriod
     *            The unitOfLoanPeriod to set.
     */
    public void setUnitOfLoanPeriod(final TimePeriod unitOfLoanPeriod) {
        this.unitOfLoanPeriod = unitOfLoanPeriod;
    }

    /**
     * Sets The loanPeriod.
     * 
     * @param loanPeriod
     *            The loanPeriod to set.
     */
    public void setLoanPeriod(final TimePeriod loanPeriod) {
        this.loanPeriod = loanPeriod;
    }

    /**
     * Sets The paymentFrequencyCode.
     * 
     * @param paymentFrequencyCode
     *            The paymentFrequencyCode to set.
     */
    public void setPaymentFrequencyCode(final PaymentFrequencyCodeList paymentFrequencyCode) {
        this.paymentFrequencyCode = paymentFrequencyCode;
    }

    /**
     * Sets The loanTypeCode.
     * 
     * @param loanTypeCode
     *            The loanTypeCode to set.
     */
    public void setLoanTypeCode(final LoanTypeCodeList loanTypeCode) {
        this.loanTypeCode = loanTypeCode;
    }

    /**
     * Sets The loanStatusCode.
     * 
     * @param loanStatusCode
     *            The loanStatusCode to set.
     */
    public void setLoanStatusCode(final LoanStatusCodeList loanStatusCode) {
        this.loanStatusCode = loanStatusCode;
    }

    /**
     * Sets The loanInterestRateTypeCode.
     * 
     * @param loanInterestRateTypeCode
     *            The loanInterestRateTypeCode to set.
     */
    public void setLoanInterestRateTypeCode(final LoanInterestTypeCodeList loanInterestRateTypeCode) {
        this.loanInterestRateTypeCode = loanInterestRateTypeCode;
    }

    /**
     * Sets The loanInterestTimingCode.
     * 
     * @param loanInterestTimingCode
     *            The loanInterestTimingCode to set.
     */
    public void setLoanInterestTimingCode(final LoanInterestTimingCodeList loanInterestTimingCode) {
        this.loanInterestTimingCode = loanInterestTimingCode;
    }

    /**
     * Sets The loanInterestMethodCode.
     * 
     * @param loanInterestMethodCode
     *            The loanInterestMethodCode to set.
     */
    public void setLoanInterestMethodCode(final LoanInterestMethodCodeList loanInterestMethodCode) {
        this.loanInterestMethodCode = loanInterestMethodCode;
    }

    /**
     * Sets The nextDueDate.
     * 
     * @param nextDueDate
     *            The nextDueDate to set.
     */
    public void setNextDueDate(final Date nextDueDate) {
        this.nextDueDate = nextDueDate;
    }

    /**
     * Sets The relatedAgreement.
     * 
     * @param relatedAgreement
     *            The relatedAgreement to set.
     */
    public void setRelatedAgreement(final FinancialServicesAgreement relatedAgreement) {
        this.relatedAgreement = relatedAgreement;
    }

    /**
     * Gets the loanRepayNumYear.
     * 
     * @return Returns the loanRepayNumYear.
     */
    public Integer getLoanRepayNumYear() {
        return loanRepayNumYear;
    }

    /**
     * Sets The loanRepayNumYear.
     * 
     * @param loanRepayNumYear
     *            The loanRepayNumYear to set.
     */
    public void setLoanRepayNumYear(final Integer loanRepayNumYear) {
        this.loanRepayNumYear = loanRepayNumYear;
    }

    /**
     * Gets the issueAge.
     * 
     * @return Returns the issueAge.
     */
    public Integer getIssueAge() {
        return issueAge;
    }

    /**
     * Sets The issueAge.
     * 
     * @param issueAge
     *            The issueAge to set.
     */
    public void setIssueAge(final Integer issueAge) {
        this.issueAge = issueAge;
    }

	/**
	 * @return the annualOutstandingRatePercentage
	 */
	public Float getAnnualOutstandingRatePercentage() {
		return annualOutstandingRatePercentage;
	}

	/**
	 * @param annualOutstandingRatePercentage the annualOutstandingRatePercentage to set
	 */
	public void setAnnualOutstandingRatePercentage(
			Float annualOutstandingRatePercentage) {
		this.annualOutstandingRatePercentage = annualOutstandingRatePercentage;
	}

    /**
     * @return the required
     */
    public Boolean getRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(Boolean required) {
        this.required = required;
    }

}
