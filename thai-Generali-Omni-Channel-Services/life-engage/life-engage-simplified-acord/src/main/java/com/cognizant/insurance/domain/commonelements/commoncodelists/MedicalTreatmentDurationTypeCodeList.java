/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Medical Treatment Duration Type.
 * 
 * @author 301350
 * 
 * 
 */
public enum MedicalTreatmentDurationTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    LimitedPeriod,
    /**
     * 
     * 
     * 
     * 
     */
    ForLife,
    /**
     * 
     * 
     * 
     * 
     */
    OneTime
}
