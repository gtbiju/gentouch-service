/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.response.vo;

import java.util.Date;

/**
 * The Class class ResponseInfo.
 *
 * @author 300797
 * 
 * Class ResponseInfo - Created for EApp. Used to map ResponseInfo details.
 */
public class ResponseInfo {
    
    /** The user name. */
    private String userName;
    
    /** The creation date time. */
    private Date creationDateTime;   
    
    /** The source info name. */
    private String sourceInfoName;
    
    /** The transaction id. */
    private String transactionId;
    
    /** The requestor token. */
    private String requestorToken;
    
    /** The lastSyncDate. */
    private Date lastSyncDate;
    
    /** The user email. */
    private String userEmail;
    
    /** The device uuid. */
    private String deviceUUID;
    
    /**
     * Gets the user name.
     *
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }
    
    /**
     * Sets the user name.
     *
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    /**
     * Gets the creation date time.
     *
     * @return the creationDateTime
     */
    public Date getCreationDateTime() {
        return creationDateTime;
    }
    
    /**
     * Sets the creation date time.
     *
     * @param creationDateTime the creationDateTime to set
     */
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
    
    /**
     * Gets the source info name.
     *
     * @return the sourceInfoName
     */
    public String getSourceInfoName() {
        return sourceInfoName;
    }
    
    /**
     * Sets the source info name.
     *
     * @param sourceInfoName the sourceInfoName to set
     */
    public void setSourceInfoName(String sourceInfoName) {
        this.sourceInfoName = sourceInfoName;
    }
    
    /**
     * Gets the transaction id.
     *
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }
    
    /**
     * Sets the transaction id.
     *
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    
    /**
     * Gets the requestor token.
     *
     * @return the requestorToken
     */
    public String getRequestorToken() {
        return requestorToken;
    }
    
    /**
     * Sets the requestor token.
     *
     * @param requestorToken the requestorToken to set
     */
    public void setRequestorToken(String requestorToken) {
        this.requestorToken = requestorToken;
    }

    public Date getLastSyncDate() {
        return lastSyncDate;
    }

    public void setLastSyncDate(Date lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getDeviceUUID() {
        return deviceUUID;
    }

    public void setDeviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
    }
}
