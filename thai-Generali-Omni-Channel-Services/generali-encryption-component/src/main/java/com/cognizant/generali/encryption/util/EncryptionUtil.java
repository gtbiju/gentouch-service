package com.cognizant.generali.encryption.util;


import java.io.FileInputStream;
import java.io.IOException;
import java.security.Key;
import javax.crypto.spec.SecretKeySpec;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import javax.crypto.Cipher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/*
 * @Authour 390229
 * Class EncryptionUtil
 * To encrypt and decrypt service requests.
 * Using AES.
 */
@SuppressWarnings("restriction")
@Component
public class EncryptionUtil {

	  /** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory
			.getLogger(EncryptionUtil.class);

	public static final String key = PropertyFileReader.fetchProperty(Constants.KEY,
			Constants.CONFIG_PROPERTY_FILE);
	

	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES";

	/**
	 * encryptString
	 * @param inputString
	 * @return encrypted String
	 */
	public  String encryptString(String inputString) {
		return encript(Cipher.ENCRYPT_MODE, key, inputString);
	}

	/**
	 * encript
	 * @param cipherMode
	 * @param key
	 * @param inputString
	 * @return
	 */
	private String encript(int cipherMode, String key, String inputString) {
		String rs = inputString;
		try {

			Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
			Cipher cipher = Cipher.getInstance(TRANSFORMATION);
			cipher.init(cipherMode, secretKey);
			if (cipherMode == Cipher.ENCRYPT_MODE) {
				byte[] inputBytes = inputString.getBytes("utf-8");
				byte[] outputBytes = cipher.doFinal(inputBytes);
				rs = Base64.encode(outputBytes);
			} else {
				byte[] outputdECODEBytes=Base64.decode(inputString);
				byte[] outputBytes = cipher.doFinal(outputdECODEBytes);
				rs = new String(outputBytes, "utf-8");
		
			}

		} catch (Exception ex) {
			LOGGER.error("Error in encrypting or decrypting request"+inputString,ex);
		}
		return rs;
	}
	/**
	 * 
	 * @param inputString
	 * @return
	 */
	public  String decryptString(String inputString) {
		return encript(Cipher.DECRYPT_MODE, key, inputString);
	}


	/**
	 * Methods for local testing
	 * @param key
	 * @param inputFilePath
	 */
    public  void encrypt(String key, String inputFilePath) {
        doCrypto(Cipher.ENCRYPT_MODE, key, inputFilePath);
    }
 
    /**
     * Methods for local testing
     * @param key
     * @param encryptedString
     */
    public  void decrypt(String key, String encryptedString){
    	System.out.println(encript(Cipher.DECRYPT_MODE, key, encryptedString));
    }
    /**
     * Methods for local testing
     * @param cipherMode
     * @param key
     * @param inputFile
     */
    private  void doCrypto(int cipherMode, String key, String inputFile){
    	FileInputStream inputStream =null;
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);
            if (cipherMode == Cipher.ENCRYPT_MODE) {  
            	inputStream = new FileInputStream(inputFile);
            }
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);
             
            byte[] outputBytes = cipher.doFinal(inputBytes);
            System.out.println("Output String: "+outputBytes.toString());
             
        } catch (Exception ex) {
        	LOGGER.error("Error in encrypting or decrypting request",ex);
        }
        finally{
            try {
				inputStream.close();
				
			} catch (IOException e) {
				LOGGER.error("Error in closing inputstream",e);

			}
        }
    }
    
//To encrypt and decrypt input for local test. uncomment the data
    
/*public static void main(String args[]) {
		EncryptionUtil encryptionUtilLocatest = new EncryptionUtil();
		//encryptionUtilLocatest.encrypt("Generali@OmniAPI", "D:\\json.txt");
		encryptionUtilLocatest.decrypt("Generali@OmniAPI","lcETz7KChA1qv1WNqqwSqidw6nlPKAaeguEy1dGjkZFTraTLP1nyA3Mp41him60K1P2lGfVME2WyloOn7BCCBXuEFKHpNDD+IhcjLe0oU9QD9QWT3HmWNDobIpoiF54mk8Unf1M+wW7UJ+Gg6ZrJlQ9tvLQPuRkeQjafKux4YZLPYytbSp4/siLZzWPIH/zwNLrVRQkxfqDiKNz2N72O+DgfUd3+208/h2gsPKKY20kOYjoUrfL9HuScBE5fVSyd06/9felPl89bOwMEVs/94SGyzZoGYiJudrwPopc9ilcL2FDcTDtnQJ/OfHD4reS4Pu8VD86kXIyoFInaaJx/wLkbw9bQbIwPUpAhbkvRWdhqZ8h2c0edZjABfDVRCV/R097xqBfx4QvvpY2yYIjHfA1i61e45LZ/z2fBU8kGPjXqVKLerqoFaGn/YGHvrzKjqLT7ghDc/G/naJJLAWzmHzOSyt/A0C0B9YDi3NgRsCRTvizWe++MQbdhEW09PbfETQ4QJ7Nxog0EETg9igUsz6XrIVzXFO12uxBp4lnulIDJZgO6avNiyKQMeCpdkM7MU5sBixdDRCLl/xuwx2UJkykU6f3/NIkH/aNAWbFzQAF9WLr6NO+KQtyUdxZ5+vJv1B9kMyVDNX53i9FJQqTV4sAgFUd5jBNlb2PGVAaMEzk1j4Vg6hv6zc5+otA2X2VTUEgc2Su/RTmq+PnO1bTjAug9vzX75dkYQ9y5O35AgXiaRQpKc1qFXojy/4PprHoLj0+LX23gZNjEbi5HySQ3T3BNxr7ON7F96s0ry951T0KKp3aQM+b5+qfKn20fLxt3kjciwymn5M6mjg+S9okIzKhx81v2qs1UQ1HCqk5UgM/mR+lsr31miaM+xN7/f+f/cJsEGXe59BkkFsvW0Xt/7BOphPVGJ9l5SVXH+EwJ847TTRgvJYGC4FKRZtb4zrhX2gmjmaRWyCLv0EX9ZkxqPfP2DweY3RnEp3/HgNOxrVfpdW8XIKR+ZzfoxoWV5FS+oRgD48X4J/+qkHWKcB0AShKzLSZNCg8VNM/7KerUUBVoyWFJjSnICJjlDsAaAed/oTpiEBTSCBAUO0dlsE6o220faAa6jC/1EK+w0hlv7n1uLhnBp0yyHoeJ0gB72ARWZy5dkuzuwu5+S9Yn3S+3O6B/jsDvMXd/MrdDK10H3jr0JWTJ/Q43OR3M1V9ymFvAcmaQvVJdS1Kw2P5euDY5MgaHXOYcfA/U8lQjxIkOa+cEGvNH29j5VsfLgnW1A912JTQVnudiEM0dYxzfmDWLdp5QGdMpDNy4e93vSSFpSb6yCC6ciwTEHaBWG5rZ6Q9ivWWKwaTQoSmP+fCJwoCV6G9HQ183DHa8bpSJpCsKvJycYjBxjOqIA3NMwu06BkUBjed4qaoY0hKPb5viMZPlVrrXjurgVBs5H4iJt0OnJcbxaVn+6GLWglSElJGqnCyznfSGwg/qwHfh9ayt3LmGqgBBD8Z5Ed1XowfMfAKId9pkeaugg7Aeim9dsC/dHda8");
	}*/
}
