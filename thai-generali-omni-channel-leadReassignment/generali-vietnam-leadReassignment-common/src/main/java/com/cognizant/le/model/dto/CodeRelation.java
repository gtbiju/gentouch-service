package com.cognizant.le.model.dto;

/**
 * The Class Relation.
 */
public class CodeRelation extends MasterDBEntry {
	
	/** The parent code. */
	private int parentCode;
	
	/** The child code. */
	private int childCode;
	
	/** The code type. */
	private int codeType;

	/**
	 * Gets the parent code.
	 *
	 * @return the parent code
	 */
	public int getParentCode() {
		return parentCode;
	}

	/**
	 * Sets the parent code.
	 *
	 * @param parentCode the new parent code
	 */
	public void setParentCode(int parentCode) {
		this.parentCode = parentCode;
	}

	/**
	 * Gets the child code.
	 *
	 * @return the child code
	 */
	public int getChildCode() {
		return childCode;
	}

	/**
	 * Sets the child code.
	 *
	 * @param childCode the new child code
	 */
	public void setChildCode(int childCode) {
		this.childCode = childCode;
	}

	/**
	 * Gets the code type.
	 *
	 * @return the code type
	 */
	public int getCodeType() {
		return codeType;
	}

	/**
	 * Sets the code type.
	 *
	 * @param codeType the new code type
	 */
	public void setCodeType(int codeType) {
		this.codeType = codeType;
	}

}
