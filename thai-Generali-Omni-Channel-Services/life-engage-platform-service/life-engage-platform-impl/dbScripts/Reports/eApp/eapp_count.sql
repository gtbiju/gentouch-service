IF OBJECT_ID ( 'dbo.sp_RetrieveAPE', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.sp_RetrieveAPE; 
GO  
CREATE PROCEDURE sp_RetrieveAPE
@DreportSatartDate varchar(30),
@ReportEndDate varchar(30)
AS
SET NOCOUNT ON;
BEGIN

--Retrieveing Count of cases for Agency
Select ISnULL(count(temp.aid),0) TotalCreated, 
ISNULL(SUM(CASE WHEN temp.statusCode = 12 THEN 1 ELSE 0 END),0) Confirmed,
ISNULL(SUM(CASE WHEN temp.statusCode = 15 THEN 1 ELSE 0 END),0) 'Total Submitted',
isNULL(SUM(CASE WHEN temp.bpmStatus = 'Submitted' THEN 1 ELSE 0 END),0) Submitted,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'PE' THEN 1 ELSE 0 END),0) Pending,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'IF' THEN 1 ELSE 0 END),0) Inforce,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'DC' THEN 1 ELSE 0 END),0) Decline,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'WD' THEN 1 ELSE 0 END),0) Withdraw,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'NT' THEN 1 ELSE 0 END),0) NTU,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'CF' THEN 1 ELSE 0 END),0) Cancel,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'PO' THEN 1 ELSE 0 END),0) Postpone
from (select ae.id aeId,ae.bpmstatus,a.id aid, a.statusCode 
statusCode from AgreementExtension ae with (nolock) join 
agreement a with (nolock) on ae.id=a.agreementExtension_id where  
a.identifier like 'AG%' and 
a.contextid=4 and a.statusCode not in (4)
and CAST(modifiedDateTime AS DATE)>=CAST(@DreportSatartDate AS DATE) and 
 CAST(modifiedDateTime AS DATE)<=CAST(@ReportEndDate AS DATE)) temp

--Retrieveing Count of cases for Banca
Select ISnULL(count(temp.aid),0) TotalCreated, 
ISNULL(SUM(CASE WHEN temp.statusCode = 12 THEN 1 ELSE 0 END),0) Confirmed,
ISNULL(SUM(CASE WHEN temp.statusCode = 15 THEN 1 ELSE 0 END),0) 'Total Submitted',
isNULL(SUM(CASE WHEN temp.bpmStatus = 'Submitted' THEN 1 ELSE 0 END),0) Submitted,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'PE' THEN 1 ELSE 0 END),0) Pending,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'IF' THEN 1 ELSE 0 END),0) Inforce,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'DC' THEN 1 ELSE 0 END),0) Decline,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'WD' THEN 1 ELSE 0 END),0) Withdraw,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'NT' THEN 1 ELSE 0 END),0) NTU,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'CF' THEN 1 ELSE 0 END),0) Cancel,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'PO' THEN 1 ELSE 0 END),0) Postpone
from (select ae.id aeId,ae.bpmstatus,a.id aid, a.statusCode 
statusCode from AgreementExtension ae with (nolock) join 
agreement a with (nolock) on ae.id=a.agreementExtension_id where  
a.identifier like 'BI%' and 
a.contextid=4 and a.statusCode not in (4)
and CAST(modifiedDateTime AS DATE)>=CAST(@DreportSatartDate AS DATE) and 
 CAST(modifiedDateTime AS DATE)<=CAST(@ReportEndDate AS DATE)) temp

--Retrieveing APE for Agency
select ISNULL(SUM(temp.APE*temp.pf),0) 'Total_APE', 
ISNULL(SUM(CASE WHEN temp.statusCode = 12 THEN (temp.APE*temp.pf) ELSE 0 END),0) Confirmed_APE,
ISNULL(SUM(CASE WHEN temp.statusCode = 15 THEN (temp.APE*temp.pf)  ELSE 0 END),0) Total_Submitted_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'Submitted' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Submitted_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'PE' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Pending_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'IF' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Inforce_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'DC' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Decline_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'WD' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Withdrawal_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'NT' THEN (temp.APE*temp.pf)  ELSE 0 END),0) NTU_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'CF' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Cancel_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'PO' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Postpone_APE
from(
select CAST(agmt.modifiedDateTime AS date) moddate, 
agmt.statusCode statusCode,
isNull((ca.amount),0) APE,ae.bpmStatus bpmStatus, isnull(CASE c.premiumFrequency
		WHEN 10 THEN 4 -- QUARTERLY
		WHEN 14 THEN 1 --YEARLY
		WHEN 15 THEN 0 --NULL
		WHEN 20 THEN 2 -- HALF YEARLY
		END,0) pf
from  currencyAmount ca   with (nolock) inner join premium p  with (nolock) on 
ca.id= p.amount_id inner join Coverage_Premium cp   with (nolock) on cp.premium_id=p.id 
inner join coverage c with (nolock) on c.id=cp.Coverage_id inner join agreement_coverage ac  
with (nolock) on c.id =ac.includesCoverage_id inner join agreement agmt  with (nolock) on agmt.id=ac.agreement_id
INNER join agreementExtension ae with (nolock) on agmt.agreementextension_id=ae.id
where agmt.contextID = 4 and agmt.statuscode NOT IN (4) and c. coverageCode=1 and p.natureCode=16 and 
agmt.identifier like 'AG%'  and cast(agmt.modifiedDateTime as Date)>=cast(@DreportSatartDate as date)
and cast(agmt.modifiedDateTime as Date)<=cast(@ReportEndDate as date)) temp

--Retrieveing APE for Banca
select  ISNULL(SUM(temp.APE*temp.pf),0) 'Total_APE',
ISNULL(SUM(CASE WHEN temp.statusCode = 12 THEN (temp.APE*temp.pf)  ELSE 0 END),0) Confirmed_APE,
ISNULL(SUM(CASE WHEN temp.statusCode = 15 THEN (temp.APE*temp.pf)  ELSE 0 END),0) Total_Submitted_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'Submitted' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Submitted_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'PE' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Pending_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'IF' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Inforce_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'DC' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Decline_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'WD' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Withdrawal_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'NT' THEN (temp.APE*temp.pf)  ELSE 0 END),0) NTU_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'CF' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Cancel_APE,
ISNULL(SUM(CASE WHEN temp.bpmStatus = 'PO' THEN (temp.APE*temp.pf)  ELSE 0 END),0) Postpone_APE
from(
select CAST(agmt.modifiedDateTime AS date) moddate, 
agmt.statusCode statusCode,
isNull((ca.amount),0) APE,ae.bpmStatus bpmStatus,isnull(CASE c.premiumFrequency
		WHEN 10 THEN 4 -- QUARTERLY
		WHEN 14 THEN 1 --YEARLY
		WHEN 15 THEN 0 --NULL
		WHEN 20 THEN 2 -- HALF YEARLY
		END,0) pf
from  currencyAmount ca   with (nolock) inner join premium p  with (nolock) on 
ca.id= p.amount_id inner join Coverage_Premium cp   with (nolock) on cp.premium_id=p.id 
inner join coverage c with (nolock) on c.id=cp.Coverage_id inner join agreement_coverage ac  
with (nolock) on c.id =ac.includesCoverage_id inner join agreement agmt  with (nolock) on agmt.id=ac.agreement_id
INNER join agreementExtension ae with (nolock) on agmt.agreementextension_id=ae.id
where agmt.contextID = 4 and agmt.statuscode NOT IN (4) and c. coverageCode=1 and p.natureCode=16 and 
agmt.identifier like 'BI%' and cast(agmt.modifiedDateTime as Date)>=cast(@DreportSatartDate as date)
and cast(agmt.modifiedDateTime as Date)<=cast(@ReportEndDate as date)) temp
END

--Executing Procedure
exec dbo.sp_RetrieveAPE '2016-12-05','2017-01-12'

