/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.EmailService;

/**
 * The Class class EmailServiceEndPoint.
 * 
 * @author 300797
 */
@Controller
@RequestMapping("/emailService")
public class EmailServiceEndPoint {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LookUpServiceEndPoint.class);

    /** The email service. */
    @Autowired
    private EmailService emailService;

    /**
     * Send email.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public final
            String sendEmail(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return emailService.sendEmail(json);
            }
        }.execute(json);
    }
    
    /**
     * Send email.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException 
     */
    @RequestMapping(value = "/triggerEmail", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public final
            void triggerEmail(@RequestBody final String json) throws BusinessException {
                emailService.triggerEmail(json);
    }

    /**
     * Update email component.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/updateEmailDetails", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String updateEmailDetails(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return emailService.updateEmailDetails(json);
            }
        }.execute(json);
    }
    
    /**
     * Send email with attachment.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/sendEmailWithAttachment", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String sendEmailWithAttachment(@RequestBody final String json) throws BusinessException {
    	
             return emailService.sendEmailWithAttachment(json);
    }
    
    /**
     * Gets the email service.
     * 
     * @return the emailService
     */
    public final EmailService getEmailService() {
        return emailService;
    }

    /**
     * Sets the email service.
     * 
     * @param emailService
     *            the emailService to set
     */
    public final void setEmailService(final EmailService emailService) {
        this.emailService = emailService;
    }

}
