/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304000
 * @version       : 0.1, Feb 10, 2014
 */
package com.cognizant.insurance.component;

import java.text.ParseException;

import org.json.JSONObject;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * The Interface interface LifeEngageFNAComponent.
 */
public interface LifeEngageFNAComponent {
    /**
     * Retrieve all FNA.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonObj
     *            the json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveAllFNA(RequestInfo requestInfo, JSONObject jsonObj) throws BusinessException, ParseException;

    /**
     * Retrieve Single FNA.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonObj
     *            the json obj
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    String retrieveFNA(RequestInfo requestInfo, JSONObject jsonObj) throws BusinessException, ParseException;
    /**
     * Save FNA.
     * 
     * @param json
     *            the json
     * @param requestInfo
     *            the request info
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     */
    String saveFNA(String json, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit);
}
