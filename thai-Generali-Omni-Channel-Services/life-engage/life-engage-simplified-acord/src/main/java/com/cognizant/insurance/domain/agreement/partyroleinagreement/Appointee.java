/**
 * 
 */
package com.cognizant.insurance.domain.agreement.partyroleinagreement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;

/**
 * A party who is designated by the owner of an insurance policy to take care of proceeds
 * from one or more of the benefits defined within the terms of the policy if the nomination 
 * is in favour of a minor.
 * 
 * e.g: Mary Smith, as Appointee of an endowment policy for the minor beneficiary.
 * 
 * @author 301350
 * 
 * ******Class newly added for EApp - 301350,29July2013 ******
 * 
 */


@Entity
@DiscriminatorValue("APPOINTEE")
public class Appointee extends PartyRoleInAgreement {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4924638561733350399L;

    /**
     * Indicates the relationship of the Appointee with Beneficiary. 
     */    
    private String relationWithBeneficiary;

    /**
     * Gets the relationWithBeneficiary.
     *
     * @return Returns the relationWithBeneficiary.
     */
    public final String getRelationWithBeneficiary() {
        return relationWithBeneficiary;
    }

    /**
     * Sets The relationWithBeneficiary.
     *
     * @param relationWithBeneficiary The relationWithBeneficiary to set.
     */
    public final void setRelationWithBeneficiary(String relationWithBeneficiary) {
        this.relationWithBeneficiary = relationWithBeneficiary;
    }
    
    
   }
