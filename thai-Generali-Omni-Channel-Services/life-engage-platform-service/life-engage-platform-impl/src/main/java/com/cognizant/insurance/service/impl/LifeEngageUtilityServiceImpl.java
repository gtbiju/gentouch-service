/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 22, 2016
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngageAuditComponent;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.services.LifeEngageUtilityService;

// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageUtilityServiceImpl.
 * 
 * @author 300797
 */
@Service("leUtilityService")
public class LifeEngageUtilityServiceImpl implements LifeEngageUtilityService {

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageUtilityServiceImpl.class);

    /** The life engage audit component. */
    @Autowired
    private LifeEngageAuditComponent lifeEngageAuditComponent;

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";

    /** The Constant DATA_WIPE_AUDIT. */
    private static final String DATA_WIPE_AUDIT = "DataWipeAudit";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /* Added for LE_datawipe functionality */
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageUtilityService#saveDataWipeAudit(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String saveDataWipeAudit(String json) throws BusinessException {

        LOGGER.trace("Inside LifeEngageSyncServiceImpl.Save :json " + json);
        JSONObject jsonObject;
        final JSONObject jsonObjectRs = new JSONObject();
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            final JSONArray jsonRsArray = new JSONArray();
            if (jsonRqArray.length() > 0) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    final JSONObject jsonObj = jsonRqArray.getJSONObject(i);
                    String response = null;
                    if (DATA_WIPE_AUDIT.equals(jsonObj.getString(TYPE))) {
                        response =
                                lifeEngageAuditComponent.saveLifeEngageDataWipeAudit(jsonObj.toString(), requestInfo,
                                        lifeEngageAudit);
                    }
                    jsonRsArray.put(new JSONObject(response));
                }
            }
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.Save : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }
    /* Added for LE_datawipe functionality */
}
