/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Mar 14, 2013
 */
package com.cognizant.insurance.agent.domain.codes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cognizant.insurance.agent.domain.product.Carrier;



/**
 * The Class class CodeTypeLookUp.
 */

@Entity
@Table(name = "CODE_TYPE_LOOKUP")
public class CodeTypeLookUp {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4137523748731569609L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /** The name. */
    @Column(name = "NAME")
    private String name;

    /** The carrier. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JoinColumn(name = "CARRIER_ID")
    private Carrier carrier;

    /** The code look ups. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "codeType")
    private Set<CodeLookUp> codeLookUps;

    /** The code relations. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "type")
    private Set<CodeRelation> codeRelations;

    /**
     * Gets the id.
     * 
     * @return Returns the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets The id.
     * 
     * @param id
     *            The id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the carrier.
     * 
     * @return Returns the carrier.
     */
    public Carrier getCarrier() {
        return carrier;
    }

    /**
     * Sets The carrier.
     * 
     * @param carrier
     *            The carrier to set.
     */
    public void setCarrier(final Carrier carrier) {
        this.carrier = carrier;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the codeLookUps.
     * 
     * @return Returns the codeLookUps.
     */
    public Set<CodeLookUp> getCodeLookUps() {
        return codeLookUps;
    }

    /**
     * Sets The codeLookUps.
     * 
     * @param codeLookUps
     *            The codeLookUps to set.
     */
    public void setCodeLookUps(Set<CodeLookUp> codeLookUps) {
        this.codeLookUps = codeLookUps;
    }

    /**
     * Gets the codeRelations.
     * 
     * @return Returns the codeRelations.
     */
    public Set<CodeRelation> getCodeRelations() {
        return codeRelations;
    }

    /**
     * Sets The codeRelations.
     * 
     * @param codeRelations
     *            The codeRelations to set.
     */
    public void setCodeRelations(Set<CodeRelation> codeRelations) {
        this.codeRelations = codeRelations;
    }
}
