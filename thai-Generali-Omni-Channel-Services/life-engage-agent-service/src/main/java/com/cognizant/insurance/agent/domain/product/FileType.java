/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Dec 13, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class class FileType.
 */

@Entity
@DiscriminatorValue("File_Types")
public class FileType extends Type {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2182594528943276604L;

    /** The product collaterals. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "fileType")
    private Set<ProductCollaterals> productCollaterals;

    /**
     * The Constructor.
     */
    public FileType() {
        super();
    }

    /**
     * The Constructor.
     * 
     * @param id
     *            the id
     */
    public FileType(final String id) {
        super(id);
    }

    /**
     * Gets the product collaterals.
     * 
     * @return the productCollaterals
     */
    public final Set<ProductCollaterals> getProductCollaterals() {
        return productCollaterals;
    }

    /**
     * Sets the product collaterals.
     * 
     * @param productCollaterals
     *            the productCollaterals to set
     */
    public final void setProductCollaterals(Set<ProductCollaterals> productCollaterals) {
        this.productCollaterals = productCollaterals;
    }

}
