package com.cognizant.le.model.dto;

public class ExcelStatus {

    private boolean validFileType;

    private boolean emptyExcel;

    private boolean validTemplate;

    /**
     * @return the validFileType
     */
    public boolean isValidFileType() {
        return validFileType;
    }

    /**
     * @param validFileType the validFileType to set
     */
    public void setValidFileType(boolean validFileType) {
        this.validFileType = validFileType;
    }

    /**
     * @return the emptyExcel
     */
    public boolean isEmptyExcel() {
        return emptyExcel;
    }

    /**
     * @param emptyExcel the emptyExcel to set
     */
    public void setEmptyExcel(boolean emptyExcel) {
        this.emptyExcel = emptyExcel;
    }

    /**
     * @return the validTemplate
     */
    public boolean isValidTemplate() {
        return validTemplate;
    }

    /**
     * @param validTemplate the validTemplate to set
     */
    public void setValidTemplate(boolean validTemplate) {
        this.validTemplate = validTemplate;
    }


}
