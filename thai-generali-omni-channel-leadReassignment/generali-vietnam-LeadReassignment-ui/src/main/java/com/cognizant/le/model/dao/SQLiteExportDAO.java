package com.cognizant.le.model.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * The Interface SQLiteExportDAO.
 */
public interface SQLiteExportDAO {

	/**
	 * Gets the tables list.
	 *
	 * @return the tables list
	 * @throws Exception the exception
	 */
	List<String> getTablesList() throws Exception;

	/**
	 * Check for entries.
	 *
	 * @param table the table
	 * @param tablesWithEntries the tables with entries
	 * @return the list
	 */
	List<String> checkForEntries(String table, List<String> tablesWithEntries);

	/**
	 * Generate sq lite scripts for mysql.
	 *
	 * @param tablesWithEntries the tables with entries
	 * @param fileWriter the file writer
	 * @throws Exception the exception
	 */
	void generateSQLiteScriptsForMysql(List<String> tablesWithEntries,
			FileWriter fileWriter) throws Exception;

	/**
	 * Execute sq lite scripts.
	 *
	 * @param inputFile the input file
	 * @return the file
	 * @throws SQLException the sQL exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	File executeSQLiteScripts(File inputFile) throws SQLException, FileNotFoundException, IOException;

}
