/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.familymembersubtypes;

import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.FamilyMember;

/**
 * This concept represents a sibling's child (e.g. niece, nephew).
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class SiblingChild extends FamilyMember {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4656162805428044513L;
}
