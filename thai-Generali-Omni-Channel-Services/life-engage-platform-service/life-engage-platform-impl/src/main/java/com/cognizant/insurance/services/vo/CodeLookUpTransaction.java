/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jan 23, 2014
 */
package com.cognizant.insurance.services.vo;

import java.util.List;

import com.cognizant.insurance.domain.codes.CodeLookUp;

/**
 * The Class class CodeLookUpResponse.
 * 
 * @author 300797
 */
public class CodeLookUpTransaction {

	/** The country. */
	private String country;

	/** The language. */
	private String language;

	/** The type name. */
	private String typeName;

	/** The code. */
	private String code;

	/** The code look ups. */
	private List<CodeLookUp> codeLookUps;

	/**
	 * Gets the country.
	 * 
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 * 
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the language.
	 * 
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 * 
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Gets the type name.
	 * 
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * Sets the type name.
	 * 
	 * @param typeName
	 *            the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * Gets the code.
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the code look ups.
	 * 
	 * @return the codeLookUps
	 */
	public List<CodeLookUp> getCodeLookUps() {
		return codeLookUps;
	}

	/**
	 * Sets the code look ups.
	 * 
	 * @param codeLookUps
	 *            the codeLookUps to set
	 */
	public void setCodeLookUps(List<CodeLookUp> codeLookUps) {
		this.codeLookUps = codeLookUps;
	}

}
