package com.cognizant.insurance.omnichannel.utils;

import java.util.Set;

import org.jfree.util.Log;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONArray;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.Country;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliEAppRepository;
import com.cognizant.insurance.request.vo.Transactions;

@Component("additionalQuestionnaireUtil")
public class AdditionalQuestionnaireUtil {
	
	@Autowired
	private GeneraliEAppRepository eAppRepository;
	
	Insured insured;
	Set<Insured> additionalInsuredes;
	
	PremiumPayer payer;
	
	AgreementHolder proposer;
	
	Person person;
	
	
	
	/** The countries. */
	Set<Country> countries;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AdditionalQuestionnaireUtil.class);

		private static final String TRANSACTIONDATA = "TransactionData";
	
	private static final String OPEN_BRACKET = "[";
	
	private static final String CLOSING_BRACKET = "]";
	
	private boolean getQuestionAnswerList(Set<UserSelection> userSelections){
		Boolean flag=false;
		try{
		if (userSelections != null) {
		
			for (UserSelection userSelectedObject : userSelections) {
				
				if (userSelectedObject.getQuestionnaireType() != null) {

					if (("Health").equalsIgnoreCase(userSelectedObject
							.getQuestionnaireType().name())) {
						
						if (userSelectedObject.getQuestionId() == 2
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 3
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 4
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 5
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 6
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 7
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 8
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 9
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 10
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 11
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						} else if (userSelectedObject.getQuestionId() == 12
								&& userSelectedObject.getSelectedOption() == true) {
							flag = true;
							break;
						}

					}

					
					else if (userSelectedObject.getQuestionnaireType().name()
								.equalsIgnoreCase("LifeStyle")) {
							
							if (userSelectedObject.getQuestionId() == 1
									&& userSelectedObject.getSelectedOption() == true) {
								flag = true;
								break;
							} else if (userSelectedObject.getQuestionId() == 2
									&& userSelectedObject.getSelectedOption() == true) {
								flag = true;
								break;
							} else if (userSelectedObject.getQuestionId() == 3
									&& userSelectedObject.getSelectedOption() == true) {
								flag = true;
								break;
							}

						}
					
					
					else if (userSelectedObject.getQuestionnaireType().name()
								.equalsIgnoreCase("Other")) {
							

							if (userSelectedObject.getQuestionId() == 1
									&& userSelectedObject.getSelectedOption() == true) {
								flag = true;
								break;
							}
						}
					
				}
			}
				
					if (!flag) {
						String premiumPayer = payer.getPartyType();
						if (premiumPayer.equalsIgnoreCase("Other")) {
							flag = true;
							
						}
					}
				}
			
		
		}
		catch(Exception e){
			Log.error(e);
		}
		return flag;

	}
	
	
	public boolean isAdditionalQuestionnaireNeeded(String proposolNumber,
			String transactionId) {
		boolean flag = false;
		Boolean flagForeigner = false;
		Set<UserSelection> insuredUserSelections = null;
		Transactions transaction = eAppRepository.retrieveEApp(proposolNumber,
				transactionId);
		insured = transaction.getInsured();
		payer = transaction.getPayer();
		proposer = transaction.getProposer();
		additionalInsuredes = transaction.getAdditionalInsuredes();
		if (insured != null) {
			person = (Person) insured.getPlayerParty();
			insuredUserSelections = insured.getSelectedOptions();
		}

		Set<Beneficiary> beneficiaries = null;

		beneficiaries = transaction.getBeneficiaries();

		flag = getQuestionAnswerList(insuredUserSelections);
		flagForeigner = getOtherQuestionAnswerList(insuredUserSelections,
				person);
		if (!flag && !flagForeigner) {
			if(additionalInsuredes!=null){
			for (Insured additionalInsured : additionalInsuredes) {

				Set<UserSelection> additionalInsuredUserSelections = additionalInsured
						.getSelectedOptions();
				Person additionalInsuredPerson = (Person) additionalInsured
						.getPlayerParty();
				flag = getQuestionAnswerList(additionalInsuredUserSelections);
				flagForeigner = getOtherQuestionAnswerList(
						additionalInsuredUserSelections,
						additionalInsuredPerson);
				if (flag || flagForeigner) {
					flag = true;
					break;
				}

			}
			}
		} else {
			flag = true;
		}

		if (!flag) {
			for (Beneficiary beneficiary : beneficiaries) {

				Set<UserSelection> beneficiaryUserSelections = beneficiary
						.getSelectedOptions();
				Person beneficiaryPerson = (Person) beneficiary
						.getPlayerParty();
				flag = getQuestionAnswerList(beneficiaryUserSelections);
				flagForeigner = getOtherQuestionAnswerList(
						beneficiaryUserSelections, beneficiaryPerson);
				if (flag || flagForeigner) {
					flag = true;
					break;
				}

			}
		}

		if (!flag) {
			String premiumPayer = payer.getPartyType();
			if (premiumPayer.equalsIgnoreCase("Other")) {
				flag = true;
			}

		}
		return flag;

	}
	
	 public Boolean getOtherQuestionAnswerList(Set<UserSelection> userSelections, Person insuredPerson){
		   	
		 Boolean flag=false;
	   		String nationality = null;
	   		countries = insuredPerson.getNationalityCountry();
	   		try {				
					for (Country country : countries) {			
						nationality = country.getName();				
					if(nationality!=null){
						if(!(nationality.equalsIgnoreCase("Indonesia")) && nationality.length()>0)
						{
							flag=true;
						}
					}
					}
	       }
	       catch(Exception e){
	    	   
	       }
		return flag;
	 }
	 
	 public boolean isAdditionalInsuredPresent(JSONObject jsonObj) {

			boolean flag = false;
		try {
			JSONObject transactionJsonObject = jsonObj
					.getJSONObject(TRANSACTIONDATA);
			org.json.JSONArray additionlInsured = null;
			if (transactionJsonObject != null) {
				additionlInsured = transactionJsonObject
						.getJSONArray("AdditionalInsured");
				if (additionlInsured.length() > 0) {
					flag = true;
				}
			}
		} catch (Exception e) {

		}
			return flag;
	 }
	 
	 boolean isQuestionnaireSectionPresent(String input) {
			boolean flag = false;
			try {
				if (!GeneraliConstants.JSON_EMPTY.equals(input)
						&& input.length() > 0) {
					if (input.contains(OPEN_BRACKET)
							&& input.contains(CLOSING_BRACKET)) {
						input = input.substring(input.indexOf(OPEN_BRACKET) + 1,
								input.indexOf(CLOSING_BRACKET));
						if (input.length() > 0) {
							flag = true;
						}
					} else {
						if (input.equals("true")) {
							flag = true;
						}
					}
				}
			} catch (Exception e) {

			}
			return flag;

		}
	 
	 String getFromJson(JSONObject jsonObject, String key) {
			String value = "";
			try {
				if (jsonObject.has(key)) {

					if (jsonObject.get(key) != null) {
						value = jsonObject.get(key).toString();
					}
					if (value == null) {
						value = "";
					}
				}
			} catch (Exception e) {

			}
			return value;

		}
}




