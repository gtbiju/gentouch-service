package com.cognizant.insurance.omnichannel.auth;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationService {

    @Value("${le.platform.service.eapp.username}")
    private String userName;

    @Value("${le.platform.service.eapp.password}")
    private String passWord;

    private boolean authenticationStatus;

    public boolean authenticate(String authCredentials) {

        authenticationStatus = false;

        if (null == authCredentials)
            return false;
        final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
        String authorizationKey = Base64.encodeBase64String((userName + ":" + passWord).getBytes());

        try {

            if (encodedUserPassword.equals(authorizationKey)) {
                authenticationStatus = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return authenticationStatus;
    }

}
