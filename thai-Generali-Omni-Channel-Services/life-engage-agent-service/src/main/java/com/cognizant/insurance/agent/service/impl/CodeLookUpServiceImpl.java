package com.cognizant.insurance.agent.service.impl;

import static com.cognizant.insurance.agent.core.helper.ExceptionHelper.throwBusinessException;
import java.text.ParseException;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
/* Added for security_optimizations */
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.cognizant.insurance.agent.constants.GeneraliConstants;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.agent.domain.codes.CodeLookUp;
import com.cognizant.insurance.agent.request.vo.RequestInfo;
import com.cognizant.insurance.agent.response.vo.CodeLookUpResponse;
import com.cognizant.insurance.agent.response.vo.ResponseInfo;
import com.cognizant.insurance.agent.service.CodeLookUpService;
import com.cognizant.insurance.agent.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.agent.service.helper.LifeEngageValidationHelper;
import com.cognizant.insurance.agent.service.repository.CodeLookUpRepository;
import com.google.common.base.Strings;
/* Added for security_optimizations */

/**
 * The Class class LookUpserviceImpl
 */
@Service("codeLookUpService")
public class CodeLookUpServiceImpl implements CodeLookUpService {

    
    /** The code lookup holder. */
    @Autowired
    @Qualifier("codeLookupMapping")
    private LifeEngageSmooksHolder codeLookupHolder;

    /** The look up repository. */
    @Autowired
    private CodeLookUpRepository codeLookUpRepository;
    
    /* Added for security_optimizations */
    @Value("${le.platform.security.validationPattern}")
    private String validationPattern;
    /* Added for security_optimizations */
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.life.service.LookUpService#getLookUpData(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String getLookUpData(final String json) throws BusinessException {

        JSONObject jsonObject;
        String response = "{}";
    	
    	/* Added for security_optimizations */
    	JSONObject jsonObjectRs = new JSONObject();
    	/* Added for security_optimizations */
        try {
        	/* Added for security_optimizations */
        	//LifeEngageValidationHelper.validateInputString(json.toString(),validationPattern);
        	/* Added for security_optimizations */
        	jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(GeneraliConstants.REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_INFO);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(GeneraliConstants.REQUEST_PAYLOAD);
            final JSONArray jsonTransactionArray = jsonRequestPayloadObj.getJSONArray(GeneraliConstants.TRANSACTIONS);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            List<CodeLookUp> typeLookUps = null;
            if (jsonTransactionArray.length() > 0) {
                final JSONObject jsonObj = jsonTransactionArray.getJSONObject(0);
                final JSONObject jsonTxDataObj = jsonObj.getJSONObject(GeneraliConstants.TRANSACTION_DATA);
                
                final String country = getStringKeyValue("country", jsonTxDataObj);
                final String language = getStringKeyValue("language", jsonTxDataObj);
                final String type = getStringKeyValue("typeName", jsonTxDataObj);
                final String code = getStringKeyValue("code", jsonTxDataObj);

                if (Strings.isNullOrEmpty(country) || Strings.isNullOrEmpty(language) || Strings.isNullOrEmpty(type)) {
                    throwBusinessException(true, "Either Country, Language or Type Name is missing");
                }
                if (Strings.isNullOrEmpty(code)) {
                    typeLookUps = codeLookUpRepository.fetchLookUpData(type, language, country);
                } else {
                    typeLookUps = codeLookUpRepository.fetchLookUpDataForCode(type, code, language, country);
                }
            }
            final CodeLookUpResponse codeLookUpResponse = new CodeLookUpResponse();
            codeLookUpResponse.setCodeLookUps(typeLookUps);
            codeLookUpResponse.setResponseInfo(buildResponseInfo(requestInfo));
            response = codeLookupHolder.parseBO(codeLookUpResponse);
            jsonObjectRs = new JSONObject(response);

        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
        }catch (Exception e)  {
       // catch (InputValidationException e) {
        	JSONObject jsonResponse= new JSONObject();
        	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
        	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
        	jsonResponse.put("statusMessage", e.getMessage());                    	
        	jsonObjectRs.put("StatusData", jsonResponse);		       			
        }

        return response;
    }

    /**
     * Builds the response info.
     * 
     * @param requestInfo
     *            the request info
     * @return the response info
     */
    private ResponseInfo buildResponseInfo(final RequestInfo requestInfo) {
        final ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setCreationDateTime(requestInfo.getCreationDateTime());
        responseInfo.setRequestorToken(requestInfo.getRequestorToken());
        responseInfo.setSourceInfoName(requestInfo.getSourceInfoName());
        responseInfo.setTransactionId(requestInfo.getTransactionId());
        responseInfo.setUserName(requestInfo.getUserName());
        return responseInfo;
    }

    /**
     * Gets the string key value.
     * 
     * @param key
     *            the key
     * @param jsonObj
     *            the json obj
     * @return the string key value
     */
    private static String getStringKeyValue(final String key, final JSONObject jsonObj) {
        String result = null;
        if (jsonObj.has(key)) {
            result = jsonObj.getString(key);
        }

        return result;
    }

    /**
     * @return the codeLookupHolder
     */
    public final LifeEngageSmooksHolder getCodeLookupHolder() {
        return codeLookupHolder;
    }

    /**
     * @param codeLookupHolder
     *            the codeLookupHolder to set
     */
    public final void setCodeLookupHolder(final LifeEngageSmooksHolder codeLookupHolder) {
        this.codeLookupHolder = codeLookupHolder;
    }

    /**
     * @return the codeLookUpRepository
     */
    public final CodeLookUpRepository getCodeLookUpRepository() {
        return codeLookUpRepository;
    }

    /**
     * @param codeLookUpRepository
     *            the codeLookUpRepository to set
     */
    public final void setCodeLookUpRepository(final CodeLookUpRepository codeLookUpRepository) {
        this.codeLookUpRepository = codeLookUpRepository;
    }

}
