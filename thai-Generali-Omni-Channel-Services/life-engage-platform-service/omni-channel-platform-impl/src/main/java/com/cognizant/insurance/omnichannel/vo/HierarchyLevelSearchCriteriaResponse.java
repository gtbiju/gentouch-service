package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;
import java.util.List;



public class HierarchyLevelSearchCriteriaResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private List<HierarchyLevelResult> hierarchyLevelList;
	
	public List<HierarchyLevelResult> getHierarchyLevelList() {
		return hierarchyLevelList;
	}

	public void setHierarchyLevelList(List<HierarchyLevelResult> hierarchyLevelList) {
		this.hierarchyLevelList = hierarchyLevelList;
	}
}
