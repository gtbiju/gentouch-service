package com.cognizant.insurance.omnichannel.component.impl;

import com.cognizant.insurance.request.vo.Transactions;

public interface LifeAsiaProcessEngine {

	public void createAndSentBPMRequest(Transactions transactions);

	public void createAndSentLifeAsiaRequest(String proposalNo);
	public void reSubmissionJob();
	public String getLifeAsiaMsg(String json);
}
