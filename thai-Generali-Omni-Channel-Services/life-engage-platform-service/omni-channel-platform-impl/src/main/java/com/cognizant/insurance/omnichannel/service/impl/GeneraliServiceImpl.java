package com.cognizant.insurance.omnichannel.service.impl;

import java.text.ParseException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.commons.collections.map.LRUMap;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.InputValidationException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliBPMEmailPdfComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliEAppComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliEtrComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliPushNotificationsComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliSPAJNoComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliUserComponent;
import com.cognizant.insurance.omnichannel.component.LifeAsiaComponent;
import com.cognizant.insurance.omnichannel.component.repository.helper.GeneraliComponentRepositoryHelper;
import com.cognizant.insurance.omnichannel.service.GeneraliService;
import com.cognizant.insurance.omnichannel.utils.MemoApplicationData;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;

@Service
public class GeneraliServiceImpl implements GeneraliService {

	public static final String APP_STATUS = "AppStatus";

	public static final String POLICY_NO = "PolicyNo";

	public static final String APP_NO = "AppNo";

	/** The Constant REQUEST. */
	private static final String REQUEST = "Request";

	/** The Constant REQUEST_INFO. */
	private static final String REQUEST_INFO = "RequestInfo";

	/** The Constant REQUEST_PAYLOAD. */
	private static final String REQUEST_PAYLOAD = "RequestPayload";

	/** The Constant TRANSACTIONS. */
	private static final String TRANSACTIONS = "Transactions";
	
	/** The Constant TRANSACTIONS. */
	private static final String TRANSACTIONS_DATA = "TransactionData";

	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliServiceImpl.class);

	/** The Constant TYPE. */
	private static final String TYPE = "Type";

	/** The Constant Key4. */
	private static final String KEY_4 = "Key4";
	private static final String KEY_21 = "Key21";

	/** The Constant Key18. */
	private static final String KEY_18 = "Key18";
	private static final String KEY_39 = "Key39";
	private static volatile LRUMap proccesdRequestCache =new LRUMap(250);
	
	/** The Constants for Memo. */
	private static final String MEMO_DETAILS = "MemoDetails";
	private static final String EAPPDETAILS = "eAPPDetails";
	

	@Autowired
	private GeneraliSPAJNoComponent generaliComponent;
	
	@Autowired
	private GeneraliBPMEmailPdfComponent generaliBPMEmailPdfComponent;
	
	@Autowired
	private TaskExecutor lifeAsiaMailPdfThreadPool;
	
	@Autowired
	private LifeAsiaComponent lifeAsiaComponent;
	
	@Autowired
	private GeneraliUserComponent userComponent;
	
	@Autowired
	private GeneraliEtrComponent etrComponent;
	
	@Autowired
    private GeneraliEAppComponent generaliEAppComponent;

    @Autowired
	private TaskExecutor pushNotificationThreadPool;

    @Autowired
    private GeneraliPushNotificationsComponent pushNotificationsComponent;
    
	@Override
	public String generateSPAJNo(String requestJson) throws BusinessException {
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(requestJson);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			return generaliComponent.generateSPAJNo(jsonTransactionArray,
					requestInfo);
		} catch (ParseException e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public String isSpajExisting(String requestJson) throws BusinessException {
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(requestJson);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			return generaliComponent.isSpajExisting(jsonTransactionArray,
					requestInfo);
		} catch (ParseException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public String generateBPMEmailPdf(String requestJson) {
		JSONObject jsonObject;

		String transactionArray = "{\"Transactions\":[]}";
		JSONObject jsonObjectRs = new JSONObject();
		JSONArray jsonRsArray = new JSONArray();
		try {
			jsonObject = new JSONObject(requestJson);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);

			JSONObject jsonObjectResp = new JSONObject(transactionArray);
			jsonRsArray = jsonObjectResp.getJSONArray(TRANSACTIONS);
			LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo,
					jsonRsArray, jsonObjectRs, null);
			JSONObject requestJsonObject = jsonTransactionArray.getJSONObject(0);
			String agentId = requestJsonObject.getString(Constants.KEY11);
			//Application Number
			LOGGER.info("** Entry: Submission of BPM request Start with agentId**" + agentId);
			String proposalNo = requestJsonObject.getString(KEY_21);
			LOGGER.info("** Entry: Submission of BPM request with Application Number**" + proposalNo);
			LOGGER.info("** Entry: Submission of BPM request Start to check the Application Number**" + proposalNo);
			if(!isProcessedRequest(proposalNo, agentId)){
				lifeAsiaMailPdfThreadPool.execute(new CreateBPMEmailPdfTask(
						jsonTransactionArray));
			}	

		} catch (ParseException e) {
			LOGGER.error("Error in BPM PDF Generation" + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("Error in BPM PDF Generation" + e.getMessage());
		}
		return jsonObjectRs.toString();
	}
	
	private class CreateBPMEmailPdfTask implements Runnable {

		private JSONArray jsonTransactionArray;

		public CreateBPMEmailPdfTask(JSONArray jsonTransactionArray) {
			super();
			this.jsonTransactionArray = jsonTransactionArray;

		}

		@Override
		public void run() {
			JSONObject requestJson = null;
			LOGGER.info("** Entry: Application Submission **");
			requestJson = jsonTransactionArray.getJSONObject(0);
			String agentId = requestJson.getString(Constants.KEY11);
			//Application Number
			String SPAJNo = requestJson.getString(GeneraliConstants.KEY21);
			int spajLength = SPAJNo.length();
			String type = requestJson.getString(TYPE);
			//EApp identifier
			String eAppId = (String) requestJson.getString(KEY_4);
			//Email flag
			String emailFlag = (String) requestJson.getString(KEY_18);
			String checkDevice = (String) requestJson.getString(KEY_39);
			LOGGER.info("** Calling updateEappStatus ** eAppID"+eAppId+" AgentId "+ agentId+"  Application No"+SPAJNo);
			generaliBPMEmailPdfComponent.updateEappBPMStatus(eAppId, AgreementStatusCodeList.Submitted.name(),checkDevice);			
            // if (emailFlag != null&& (Constants.TRUE).equalsIgnoreCase(emailFlag)) {
            
            /*
             * Set<AgreementDocument> agreementDocuments = generaliBPMEmailPdfComponent .generateBPMPdf(requestJson);
             * generaliBPMEmailPdfComponent.saveBPMPdf(agreementDocuments, eAppId);
             */
                    
            if(SPAJNo!=null && spajLength!=GeneraliConstants.SPAJAPPLENGTH) {
            	LOGGER.info("** Calling saveGeneraliEmail ** eAppID" + eAppId + " AgentId " + agentId + "  Application No " + SPAJNo);
            	generaliBPMEmailPdfComponent.saveGeneraliEmail(requestJson);
            	LOGGER.info("** Calling triggerEmail ** eAppID" + eAppId + " AgentId " + agentId + "  Application No"+ SPAJNo);
            	generaliBPMEmailPdfComponent.triggerEmail(agentId, type);
            }else {
            	LOGGER.info("** Not Calling saveGeneraliEmail due to Manual ** eAppID" + eAppId + " AgentId " + agentId + "  Application No "+ SPAJNo);
            	LOGGER.info("** Not Calling triggerEmail due to Manual ** eAppID" + eAppId + " AgentId " + agentId + "  Application No "+ SPAJNo);
            }
            
            LOGGER.info("** Calling triggerBPMRequest ** eAppID" + eAppId + " AgentId " + agentId + "  Application No " + SPAJNo);
            generaliBPMEmailPdfComponent.triggerBPMRequest(eAppId);
            LOGGER.info("** Exit: Application Submission **eAppID" + eAppId + " AgentId " + agentId
                    + "  Application No" + SPAJNo);
            // }
		}
	}
	private  boolean isProcessedRequest(String proposalNo,String agentId){
		boolean isProcessed=false;
		if(!proccesdRequestCache.containsKey(proposalNo)){
			proccesdRequestCache.put(proposalNo, agentId);
		}else{
			LOGGER.error("Received multiple Request for Final submission with proposal No :"+proposalNo+" ,agent Id"+agentId);
			isProcessed=true;
		}
		LOGGER.info("** Entry: Submission of BPM request Check Passed for Application Number**" + proposalNo);
		return isProcessed;
	}

	@Override
	public void generateICMInfo(String eAppId) {
		lifeAsiaComponent.createICMInfo(eAppId,false);
		
	}
	
	public void resubmit(String eAppId){
		lifeAsiaComponent.createICMInfo(eAppId,true);
	}

	@Override
	public String isLASAvailable() {
	    
	    //lifeAsiaComponent.isLASAvailable_withDetails();
		
	    return "{}";
	}
	
	public String validateAgentCode(String requestJson) throws BusinessException{
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(requestJson);
			JSONArray jsonRsArray;
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
		    final JSONObject jsonObjectRs = new JSONObject();
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			String transactionArray = userComponent.validateAgentCode(jsonTransactionArray.getJSONObject(0),
					requestInfo);
            jsonRsArray = new JSONArray(transactionArray);
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
			return jsonObjectRs.toString();
		} catch (ParseException e) {
			throw new BusinessException(e);
		}
	}
	
	public String retrieveMemoDetails(String requestJson) throws BusinessException{
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(requestJson);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			return lifeAsiaComponent.retrieveMemoDetails(requestInfo,jsonTransactionArray.getJSONObject(0));
			} catch (ParseException e) {
			throw new BusinessException(e);
		}
		catch (InputValidationException ev) {
			throw new BusinessException(ev);
		}
	}
	
	public String updateMemoDetails(String requestJson) throws BusinessException{
		JSONObject jsonObject;
		String responseJson = "";
		
		try {
		    LOGGER.info("Update from BPM ->Start");
			jsonObject = new JSONObject(requestJson);
			LOGGER.info("Update from BPM ->Start - >JSON "+requestJson);
			final JSONArray applicationDetails = jsonObject.getJSONArray(EAPPDETAILS);
			
			for(int i=0; i < applicationDetails.length(); i++) {
				String toCheckForMail=null;
				JSONObject application = applicationDetails.getJSONObject(i);
				MemoApplicationData memoAppData = new MemoApplicationData();
				GeneraliComponentRepositoryHelper.parseMemoAppData(memoAppData, application);
                if(memoAppData.getMemoList().size()>0){
                    responseJson = lifeAsiaComponent.updateMemoDetails(memoAppData);
                  }else{
                    responseJson = lifeAsiaComponent.updateApplicationStatus(memoAppData);
                }
                
                JSONArray memoDetailArray1 = application.getJSONArray(GeneraliConstants.MEMO_DETAILS);
                for(int j=0; j < memoDetailArray1.length(); j++) {
        			JSONObject jsonObjCheck = memoDetailArray1.getJSONObject(j);
        			if (jsonObjCheck.getString(GeneraliConstants.MEMO_CODE) != null
        					&& !jsonObjCheck.getString(GeneraliConstants.MEMO_CODE)
        							.equals("")) {
        				if(jsonObjCheck.getString(GeneraliConstants.MEMO_STATUS) !=null && jsonObjCheck.getString(GeneraliConstants.MEMO_STATUS).equals(GeneraliConstants.OPEN)) {
        				toCheckForMail = "triggerMail";
        				break;
        				}
        			}
                }
                
                if(toCheckForMail!=null && toCheckForMail.equals("triggerMail")) {
                	LOGGER.info("Mail Triggering for the Application due to Open Status into BPM ->End");
                	lifeAsiaComponent.triggerEappMemoEmail(memoAppData.getAppNumber());
                }
 			}
			LOGGER.info("Update from BPM ->End");
		} catch (ParseException e) {
			throw new BusinessException(e);
		}catch (InputValidationException ev) {
			throw new BusinessException(ev);
		}catch(BusinessException e){
		    JSONObject jsonObj = new JSONObject();
            JSONObject rsJson = new JSONObject();
            JSONObject msgInfo = new JSONObject();
            msgInfo.put(GeneraliConstants.MSG_DESCRIPTION, e.getMessage());
            msgInfo.put(GeneraliConstants.MSG_CODE, GeneraliConstants.FAILURE_CODE);
            msgInfo.put(GeneraliConstants.MSG, GeneraliConstants.FAILURE);
            rsJson.put(GeneraliConstants.MSGINFO, msgInfo);
            jsonObj.put(GeneraliConstants.RESPONSE, responseJson);
            responseJson = jsonObj.toString();
		}
		return responseJson;
	}
	
	public String updateAppainStatus(String requestJson) throws BusinessException {
		JSONObject jsonObject;
		String responseJson = "";
		try {
			jsonObject = new JSONObject(requestJson);
			final JSONArray uwRuleFields = jsonObject.getJSONArray(GeneraliConstants.UWRULEFIELDS);
			JSONObject uwJsonObject = uwRuleFields.getJSONObject(0);
			String appNo = uwJsonObject.getString(GeneraliConstants.APP_NO);
			responseJson = lifeAsiaComponent.updateAppainStatus(appNo, requestJson);
			} catch (ParseException e) {
			throw new BusinessException(e);
			} catch (InputValidationException e) {
		    LOGGER.error("Error in updateAppainStatus "+e.getMessage());
		    throw new BusinessException(e);
		}
		return responseJson;
	}

    public String generateEtr(String requestJson) throws BusinessException {
        JSONObject jsonObject;
        String agentId = "";
        try {
            jsonObject = new JSONObject(requestJson);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonTransactionArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            final JSONObject jsonTransaction = jsonTransactionArray.getJSONObject(0);
            final JSONObject jsonTransactionData = jsonTransaction.getJSONObject(TRANSACTIONS_DATA);

            agentId = jsonTransaction.getString(Constants.KEY11);

            return etrComponent.retrieveEtr(jsonTransactionData, agentId);
        } catch (ParseException e) {
            throw new BusinessException(e);
        } catch (InvalidKeyException e) {
            throw new BusinessException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new BusinessException(e);
        } catch (InvalidKeySpecException e) {
            throw new BusinessException(e);
        } catch (SignatureException e) {
            throw new BusinessException(e);
        } catch (IOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String lockOrUnlockCase(String json) {
        LOGGER.info("Inside LifeEngageSyncImpl lockOrUnlockCase entry"+ json);
        JSONObject jsonObject;
        final JSONObject jsonObjectRs = new JSONObject();
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj
                    .getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper
                    .parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj
                    .getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj
                    .getJSONArray(TRANSACTIONS);
            final JSONArray jsonRsArray = new JSONArray();
            HashMap<String,Integer> pushNotificationAgentMap = new HashMap<String, Integer>();
            if (jsonRqArray.length() > 0) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    String responseString = StringUtils.EMPTY;
                    final JSONObject jsonObj = jsonRqArray.getJSONObject(i);
                    responseString = generaliEAppComponent.lockOrUnlock(requestInfo, jsonObj);
                    jsonRsArray.put(new JSONObject(responseString));
                    
                    addtoMap(pushNotificationAgentMap,responseString);
                    String action=jsonObj.getString("Action");
                    
                    JSONObject	responseObj= new JSONObject(responseString);
                    String agentName=responseObj.getString("agentName");
                    
                    if(action.equals(GeneraliConstants.MARK_COMPLETE)){
                    	pushNotificationThreadPool.execute(new CreatePushNotificationTask(pushNotificationAgentMap,requestInfo,GeneraliConstants.GAO_PUSH_MSG_PART1+agentName+GeneraliConstants.GAO_PUSH_MSG_PART2));
                    }
                }
            }
           
            
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo,
                    jsonRsArray, jsonObjectRs, null);
        }catch(ParseException e){
            LOGGER.error(e.getMessage());
        }
        
        return jsonObjectRs.toString();
    }

    /**
     * Method to allocate no of leads assigned to each agent.
     * @param agentMap
     * @param response
     */
    	public void addtoMap(HashMap<String, Integer> agentMap, String response) {
    		try {
    			
    			JSONObject	responseObj= new JSONObject(response);
    			JSONObject statusObj=responseObj.getJSONObject("StatusData");
    			String statusMessage=statusObj.getString("Status");
    			String agentId=responseObj.getString("agentId");
    			if (Constants.SUCCESS.equals(statusMessage)) {
    						agentMap.put(agentId, 1);    					
    			} 
    		} catch (NoSuchElementException e) {
    			throw new SystemException(e.getMessage());
    		} catch (ParseException e) {
    			throw new SystemException(e.getMessage());

    		}
    	}
    	
    private class CreatePushNotificationTask implements Runnable {
		private HashMap<String, Integer> agentPushNotificationMapMap;
		private RequestInfo requestInfo;
		private String message;
		public CreatePushNotificationTask(HashMap<String, Integer> agentMap,
				RequestInfo requestInfo, String message) {
			super();
			this.requestInfo=requestInfo;
			this.agentPushNotificationMapMap=agentMap;
			this.message=message;
		}

		@Override
		public void run() {
				pushNotificationsComponent.pushNotifications(agentPushNotificationMapMap,requestInfo,GeneraliConstants.TYPE_MARKCOMPLETE,message);			
		}
	}
    
    @Override
    public String sendUpdateToBPM(String json) {
        String response = StringUtils.EMPTY;
        try{
            JSONObject jsonObj = new JSONObject(json);
            response = lifeAsiaComponent.sendUpdateToBPM(jsonObj.getString("input"), jsonObj.getString("url"));
        }catch(BusinessException e){
            LOGGER.error(e.getMessage());
        } catch (ParseException e) {
            LOGGER.error(e.getMessage());
        }
        return response;
    }
    
    
}
