package com.cognizant.insurance.omnichannel.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.generali.security.repository.GeneraliLoginRepository;
import com.cognizant.insurance.component.LifeEngageAgentComponent;
import com.cognizant.insurance.component.VendorAgentComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.component.GeneraliLifeEngageAgentComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliSPAJNoComponent;
import com.cognizant.insurance.omnichannel.utils.GeneraliDateUtil;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.service.impl.AgentProfileServiceImpl;
import com.cognizant.le.security.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.le.security.request.Request;
import com.cognizant.le.security.request.impl.JPARequestImpl;

public class GeneraliAgentProfileServiceImpl extends AgentProfileServiceImpl {
	
	/** The life engage agent component. */
	@Autowired
	private GeneraliLifeEngageAgentComponent lifeEngageAgentComponent;
	
	/** The vendor agent component. */
	@Autowired
	private VendorAgentComponent vendorAgentComponent;
	
	@Autowired
	private GeneraliSPAJNoComponent spajNoComponent;
	
	@Autowired
	private GeneraliLoginRepository generaliLoginRepository;
	
	 /** The Constant DATEFORMAT_YYYY_MM_DD_HH_MM_SS. */
    private static final String DATEFORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";    
    
    /** The Constant VALIDATE_SIGNATURE */
    private static final String VALIDATE_SIGNATURE = "validateSignature";
    
    @Value("${generali.platform.signatureValidityDays}")
    private String signatureValidity;
	
	@Override
	public String retrieveAgentByCode(String json) throws BusinessException
			{
		JSONObject jsonObject = null;
		String response = null;		
		try {
			jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
				response = lifeEngageAgentComponent.retrieveAgentProfileByCode(
						requestInfo, jsonTransactionArray);
				
		} catch (BusinessException e) {
			throw e;

		} catch (ParseException e) {
        	LOGGER.error("PARSE ERROR : Parse exception : ## ERR ##" +e.getMessage() , e);
		}

		return response;
	}
	
		
	@Override
	public String validateReassignLogin(String json) throws BusinessException{
		
		JSONObject jsonObject = null;
		JSONObject resultJson = new JSONObject();
		String response = null;		
		try {
			jsonObject = new JSONObject(json);
			String agentName = jsonObject.has("agentCode")?jsonObject.getString("agentCode"):StringUtils.EMPTY;
			String password = jsonObject.has("password")?jsonObject.getString("password"):StringUtils.EMPTY;
			response = lifeEngageAgentComponent.validateReassignLogin(agentName, password);
			if(response.equals(Constants.SUCCESS)) {
				resultJson.put("status", Constants.SUCCESS);
				resultJson.put("userType", "rm_reassign");
				
			} else if(response.equalsIgnoreCase(Constants.INVALID_AGENTCODE_PWD_ERROR_MSG)) {
				resultJson.put("status", Constants.INVALID_AGENTCODE_PWD_ERROR_MSG);
				resultJson.put("userType", "rm_reassign");
				
			} else {
				resultJson.put("status", Constants.FAILURE);
				resultJson.put("loginInfo", "issue in login");
			}
			
		} catch (ParseException e) {
        	LOGGER.error("PARSE ERROR : Parse exception : ## ERR ##" +e.getMessage() , e);
		}
		return resultJson.toString();
		
	}
	
	@Override
	public String retrieveAgentsForReassign(String json) throws BusinessException{
		JSONObject jsonObject = null;
		String response = null;		
		try {
			jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
				response = lifeEngageAgentComponent.retrieveAgentsForReassign(requestInfo, jsonTransactionArray);
				
		} catch (BusinessException e) {
			throw e;

		} catch (ParseException e) {
        	LOGGER.error("PARSE ERROR : Parse exception : ## ERR ##" +e.getMessage() , e);
		}

		return response;
	}
	
	@Override
	public String retrieveLeads(String json) throws BusinessException{
		JSONObject jsonObject = null;
		String response = null;		
		try {
			jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
				response = lifeEngageAgentComponent.retrieveLeads(requestInfo, jsonTransactionArray);
				
		} catch (BusinessException e) {
			throw e;

		} catch (ParseException e) {
        	LOGGER.error("PARSE ERROR : Parse exception : ## ERR ##" +e.getMessage() , e);
		}

		return response;
	}

    private String checkSignatureDate(JSONArray jsonTransactionArray, String mergedResponse) throws ParseException {
        JSONObject request = jsonTransactionArray.getJSONObject(0);
        String signature = request.getString("Key25");
        long validityDays = Long.parseLong(signatureValidity);
        boolean dateFlag = false;
        JSONObject response = new JSONObject(mergedResponse);
        JSONObject objTransaction =
            (JSONObject) response.getJSONObject(RESPONSE)
                .getJSONObject(RESPONSE_PAYLOAD)
                .getJSONArray(TRANSACTIONS)
                .get(0);
        JSONObject objTransactionData = objTransaction.getJSONObject(TRANSACTIONS_DATA);
        
        
        if (VALIDATE_SIGNATURE.equals(signature)) {
            final SimpleDateFormat dateFormat =
                    new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
            Date currentDate = LifeEngageComponentHelper.getCurrentdate();
      
			if (request.has("TransactionData")
					&& !request.getString("TransactionData").equals("{}")
					&& request.getJSONObject("TransactionData").has(
							"Declaration")
					&& !request.getJSONObject("TransactionData")
							.getString("Declaration").equals("{}")) {

				final JSONObject jsonObjDeclaration = request.getJSONObject(
						"TransactionData").getJSONObject("Declaration");
				Date spajSignDate = LifeEngageComponentHelper.getDateKeyValue(
						dateFormat, "spajSignDate", jsonObjDeclaration);
				Date questionaireSignDate = LifeEngageComponentHelper
						.getDateKeyValue(dateFormat, "questionaireSignDate",
								jsonObjDeclaration);
				Date agentSignDate = LifeEngageComponentHelper
				.getDateKeyValue(dateFormat, "agentSignDate",
						jsonObjDeclaration);
				
				long days = GeneraliDateUtil.getNoOfDaysBetweenDates(
						currentDate, spajSignDate);

				if (days > validityDays) {
					dateFlag = false;

				} else {
					dateFlag = isValidDate(currentDate, questionaireSignDate, validityDays);
					if(dateFlag){
						dateFlag = isValidDate(currentDate, agentSignDate, validityDays);
						if(dateFlag){
							dateFlag = true;
						}else{
							dateFlag = false;
						}
					}else{
						dateFlag = false;
					}
				}
			} else {
				 dateFlag = true;
			}
        } else {
            dateFlag = true;
        }
        
        if(dateFlag){
            objTransactionData.put("signatureDate", "");
        }else{
            objTransactionData.put("signatureDate", "InvalidDate");
        }
        return response.toString();
    }
    
    private boolean isValidDate(Date currentDate,Date compDate , long validityDays){
    	boolean dateFlag = false;
    	long days = 0l;
    	try{
    		days = GeneraliDateUtil.getNoOfDaysBetweenDates(currentDate, compDate);
    		if (days > validityDays) {
				dateFlag = false;
			} else {
				dateFlag = true;
			}
    	}catch(Exception e){
    		LOGGER.error("Error in isValidDate :"+e.getMessage());
    	}
		return dateFlag;
    }

    
    private boolean isActiveAgent(String vendorResponse) {
    	//Uncomment the following block to enable the SAPJ Generation along with Agent profile
    	 try {
			JSONObject response = new JSONObject(vendorResponse);
			 JSONObject obj1Transaction =
		                (JSONObject) response.getJSONObject(RESPONSE)
		                    .getJSONObject(RESPONSE_PAYLOAD_)
		                    .getJSONArray(TRANSACTIONS)
		                    .get(0);
			String agentStaus= obj1Transaction.getJSONObject(
						TRANSACTIONS_DATA).getJSONObject(AGENT_DETAILS).getString(AGENT_STATUS);
			if(AGENT_ACTIVE_STATUS.equals(agentStaus.toLowerCase())){
				return true;
			}
			return true;
		} catch (ParseException e) {
			LOGGER.error("Unable to process the Vender response "+e.getMessage());
		}
		return false;
	}

	private JSONObject mergeJSON(JSONObject obj1, JSONObject obj2,JSONObject spajObj) {
        JSONObject obj1Transaction =
                (JSONObject) obj1.getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD_)
                    .getJSONArray(TRANSACTIONS)
                    .get(0);
        JSONObject obj2Transaction =
                (JSONObject) obj2.getJSONObject(RESPONSE)
                    .getJSONObject(RESPONSE_PAYLOAD)
                    .getJSONArray(TRANSACTIONS)
                    .get(0);    
            
		if (obj2Transaction.has("TransactionData")) {
			if (obj2Transaction.getJSONObject("TransactionData").has(
					ACHIEVEMENTS)) {

				obj1Transaction.getJSONObject(TRANSACTIONS_DATA).put(
						ACHIEVEMENTS,
						obj2Transaction.getJSONObject(TRANSACTIONS_DATA)
								.getJSONArray(ACHIEVEMENTS));
			} else {
				/*JSONObject  object=new JSONObject();
				object.put("achievementCode", "2015 ABM Qualifier");
				object.put("title", "BADGE3");*/
				JSONArray array=new JSONArray();
				//array.put(object);
				obj1Transaction.getJSONObject(TRANSACTIONS_DATA).put(
						ACHIEVEMENTS, array);
			}
		}
		//Check is SPAJ NO Object is there 
		if (spajObj!=null) {
			JSONObject spajTransaction = (JSONObject) spajObj
					.getJSONObject(RESPONSE).getJSONObject(RESPONSE_PAYLOAD)
					.getJSONArray(TRANSACTIONS).get(0);
			if (spajTransaction.getJSONObject(TRANSACTIONS_DATA).length() > 0
					&& !(spajTransaction.getJSONObject(TRANSACTIONS_DATA)
							.isNull(SPAJ_NOS))) {
				obj1Transaction.getJSONObject(TRANSACTIONS_DATA).put(
						SPAJ_NOS,
						spajTransaction.getJSONObject(TRANSACTIONS_DATA)
								.getJSONArray(SPAJ_NOS));
			}
		}
		// TEMP FIX for the isRDSUser flag issue in TABLET - Remove if we release  the TAB build with RDS change
		if(obj1Transaction!=null && obj1Transaction.has("TransactionData") &&
				obj1Transaction.getJSONObject("TransactionData").has("AgentDetails")&& 
				obj1Transaction.getJSONObject("TransactionData").getJSONObject("AgentDetails").has("isRDSUser") ){
			if(!"true".equals(obj1Transaction.getJSONObject("TransactionData").getJSONObject("AgentDetails").getString("isRDSUser").toLowerCase())){
				obj1Transaction.getJSONObject("TransactionData").getJSONObject("AgentDetails").remove("isRDSUser");
			}
		}
		
		JSONObject payLoad = obj1.getJSONObject(RESPONSE)
        .getJSONObject(RESPONSE_PAYLOAD_);
		
		obj1.getJSONObject(RESPONSE).remove(RESPONSE_PAYLOAD_);
		
		obj1.getJSONObject(RESPONSE).put(RESPONSE_PAYLOAD, payLoad);
        
		
		return obj1;
    }
    

    protected String mergeResponse(String responseData, String vendorResponseData,String spajResponse) throws ParseException {
        JSONObject response = new JSONObject(responseData);
        JSONObject vendorResponse = new JSONObject(vendorResponseData);
        JSONObject spajObj=null;
        if(spajResponse!=null&&!spajResponse.isEmpty()){
        	 spajObj = new JSONObject(spajResponse);
        }       
        return mergeJSON(vendorResponse, response,spajObj).toString();
    }
    
	/** The Constant REQUEST. */
	private static final String REQUEST = "Request";

	/** The Constant REQUEST_INFO. */
	private static final String REQUEST_INFO = "RequestInfo";

	/** The Constant REQUEST_PAYLOAD. */
	private static final String REQUEST_PAYLOAD = "RequestPayload";



    /** The Constant REQUEST. */
    private static final String RESPONSE = "Response";

    /** The Constant REQUEST_INFO. */
    private static final String RESPONSE_INFO = "ResponseInfo";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";
    
    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD_ = "ResponsePayLoad";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The constant TRANSACTION_DATA **/
    private static final String TRANSACTIONS_DATA = "TransactionData";

    /** The constant AGENT_DETAILS **/
    private static final String AGENT_DETAILS = "AgentDetails";

    private static final String AGENT_DESC = "agentDesc";
    
    private static final String ACHIEVEMENTS = "Achievements";   
    
    private static final String AGENT_STATUS= "status";
    private static final String AGENT_ACTIVE_STATUS= "active";
    private static final String SPAJ_NOS= "spajNos";
    
    
}
