/**
 * 
 */
package com.cognizant.insurance.agent.domain.contactandplace.contactcodelists;

/**
 * A code list indicating types of country subdivisions.
 * 
 * 
 * @author 301350
 * 
 * 
 */
public enum CountrySubdivisionTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    District,
    /**
     * 
     * 
     * 
     * 
     */
    Province,
    /**
     * 
     * 
     * 
     * 
     */
    State,
    /**
     * 
     * 
     * 
     * 
     */
    Territory,
    /**
     * 
     * 
     * 
     * 
     */
    Arkansas,
    /**
     * 
     * 
     * 
     * 
     */
    //Added for Thailand
    SubDistrict
}
