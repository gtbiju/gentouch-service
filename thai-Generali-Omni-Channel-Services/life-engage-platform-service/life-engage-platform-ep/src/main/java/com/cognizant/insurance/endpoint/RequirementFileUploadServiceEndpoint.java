/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 325754
 * @version       : 0.1, Oct 2, 2015
 */
package com.cognizant.insurance.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.RequirementFileUploadService;

/**
 * The Class class RequirementServiceEndPoint.
 */

@Controller
@RequestMapping("/requirementFileUploadService")
public class RequirementFileUploadServiceEndpoint {
    
    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(RequirementFileUploadServiceEndpoint.class);

    /** The document service. */
    @Autowired
    private RequirementFileUploadService requirementDocFileUploadService;
   
     /**
     * Upload files.
     * 
     * @param inputJsonObj
     *            the input BASE64 encoded String
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    @RequestMapping(value = "/uploadRequirementDocFile", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String uploadRequirementDocFile(@RequestBody final String inputJsonObj) throws BusinessException {
       return requirementDocFileUploadService.uploadRequirementDocFile(inputJsonObj);
    }
    
    
    @RequestMapping(value = "/getRequirementDocFile", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getRequirementDocumentFile(@RequestBody final String inputJsonObj) throws BusinessException {
        return requirementDocFileUploadService.getRequirementDocumentFile(inputJsonObj);
    }
    
    @RequestMapping(value = "/getRequirementDocFilesList", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String getRequirementDocFilesList(@RequestBody final String inputJsonObj) throws BusinessException {
        return requirementDocFileUploadService.getRequirementDocFilesList(inputJsonObj);
    }
    
    @RequestMapping(value = "/saveRequirement", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String saveRequirement(@RequestBody final String inputJsonObj) throws BusinessException {
       return requirementDocFileUploadService.saveRequirement(inputJsonObj);
    }
    
}
