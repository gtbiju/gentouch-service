/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Identifies the concept to which the deductible applies.
 * 
 * @author 301350
 * 
 * 
 */
public enum DeductibleAppliesToCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    AnnualAggregate,
    /**
     * 
     * 
     * 
     * 
     */
    EachAccident,
    /**
     * 
     * 
     * 
     * 
     */
    EachLoss,
    /**
     * 
     * 
     * 
     * 
     */
    EachOccurrence
}
