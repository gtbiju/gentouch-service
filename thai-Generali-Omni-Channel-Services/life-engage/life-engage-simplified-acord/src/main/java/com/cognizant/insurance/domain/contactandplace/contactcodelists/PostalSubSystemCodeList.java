/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.contactcodelists;

/**
 * A code list identifying the type of postal code system. This is used only
 * when the primary postal code system is being refined in a given locale.
 * 
 * e.g. CEDEX in France
 * 
 * @author 301350
 * 
 * 
 */
public enum PostalSubSystemCodeList {
    /**
     * CEDEX is a French system that stands for Courrier d'Entreprise
     * Distribution EXceptionnelle, designed for recipients of large volumes of
     * mail. A postal code is allocated to each large organization.
     * 
     * 
     * 
     */
    CEDEX
}
