/**
 * 
 */
package com.cognizant.insurance.agent.domain.party;

import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.agent.domain.party.partycodelists.ApplicabilityCodeList;

/**
 * This concept is a generalizing concept providing further details about a
 * person or an organization. Some details regarding a party are not likely to
 * be necessary to perform basic processes about parties. As such, they have
 * been separated from basic party information and identified as details.
 * Examples are financial results of a company, family details... Thus, a list
 * of party details are linked to a party, being an organization or a party:
 * asking for a detailed view of a party is tantamount to asking for this list.
 * Elements of this list are typed as: - person specific details: birth and
 * marriage details... - organization specific details: financial results for
 * instance - fiscal residence - tax file - and membership (see Membership)
 * 
 * Party details are saved within instances of specialization of PartyDetail.
 * 
 * 
 * @author 301350
 * 
 * 
 */
@MappedSuperclass
public abstract class PartyDetail extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4597809626452330677L;

    /**
     * Validity period of the piece of information. 
     * 
     * 
     * 
     */
    @ManyToOne
    private TimePeriod effectivePeriod;

    /**
     * A code indicating the applicability of something concerning a party
     * (person, organization, etc.).
     * 
     * 
     * 
     */
    @Enumerated
    private ApplicabilityCodeList applicabilityCode;

    /**
     * Gets the effectivePeriod.
     * 
     * @return Returns the effectivePeriod.
     */
    public TimePeriod getEffectivePeriod() {
        return effectivePeriod;
    }

    /**
     * Gets the applicabilityCode.
     * 
     * @return Returns the applicabilityCode.
     */
    public ApplicabilityCodeList getApplicabilityCode() {
        return applicabilityCode;
    }

    /**
     * Sets The effectivePeriod.
     * 
     * @param effectivePeriod
     *            The effectivePeriod to set.
     */
    public void setEffectivePeriod(final TimePeriod effectivePeriod) {
        this.effectivePeriod = effectivePeriod;
    }

    /**
     * Sets The applicabilityCode.
     * 
     * @param applicabilityCode
     *            The applicabilityCode to set.
     */
    public void setApplicabilityCode(
            final ApplicabilityCodeList applicabilityCode) {
        this.applicabilityCode = applicabilityCode;
    }

}
