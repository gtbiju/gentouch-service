/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.generali.encryption.util;

/**
 * The Class class Constants.
 */
public final class Constants {

	/** The Constant CONFIG_PROPERTY_FILE. */
	public static final String CONFIG_PROPERTY_FILE = "omni-channel-encryption-config.properties";
	
	public static final String KEY = "generali.platform.encryption.key";

	/**
	 * Instantiates a new constants.
	 */
	private Constants() {

	}
}
