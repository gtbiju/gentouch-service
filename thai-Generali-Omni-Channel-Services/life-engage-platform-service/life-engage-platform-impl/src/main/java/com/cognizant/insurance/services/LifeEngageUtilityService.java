/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Nov 22, 2016
 */

package com.cognizant.insurance.services;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * The Interface interface LifeEngageUtilityService.
 */
public interface LifeEngageUtilityService {
	/* Added for LE_datawipe functionality */
    /**
     * Save data wipe audit.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    String saveDataWipeAudit(String json) throws BusinessException;
    /* Added for LE_datawipe functionality */
}
