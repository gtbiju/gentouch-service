/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 356551
 * @version       : 0.1, Mar 7, 2018
 */
package com.cognizant.insurance.omnichannel.utils;

import java.util.List;

import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;

// TODO: Auto-generated Javadoc
/**
 * The Class MemoApplicationData.
 */
public class MemoApplicationData {
    
    /** The app number. */
    private String appNumber;
    
    /** The policy number. */
    private String policyNumber;
    
    /** The app status. */
    private String appStatus;
    
    /** The memo count. */
    private String memoCount;
    
    /** The memo date. */
    private String memoDate;
    
    /** The memo list. */
    private List<AppianMemoDetails> memoList;

    /**
     * Gets the appNumber.
     *
     * @return Returns the appNumber.
     */
    public String getAppNumber() {
        return appNumber;
    }

    /**
     * Sets The appNumber.
     *
     * @param appNumber The appNumber to set.
     */
    public void setAppNumber(String appNumber) {
        this.appNumber = appNumber;
    }

    /**
     * Gets the policyNumber.
     *
     * @return Returns the policyNumber.
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets The policyNumber.
     *
     * @param policyNumber The policyNumber to set.
     */
    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    /**
     * Gets the appStatus.
     *
     * @return Returns the appStatus.
     */
    public String getAppStatus() {
        return appStatus;
    }

    /**
     * Sets The appStatus.
     *
     * @param appStatus The appStatus to set.
     */
    public void setAppStatus(String appStatus) {
        this.appStatus = appStatus;
    }

    /**
     * Gets the memoDate.
     *
     * @return Returns the memoDate.
     */
    public String getMemoDate() {
        return memoDate;
    }

    /**
     * Sets The memoDate.
     *
     * @param memoDate The memoDate to set.
     */
    public void setMemoDate(String memoDate) {
        this.memoDate = memoDate;
    }

    /**
     * Gets the memoCount.
     *
     * @return Returns the memoCount.
     */
    public String getMemoCount() {
        return memoCount;
    }

    /**
     * Sets The memoCount.
     *
     * @param memoCount The memoCount to set.
     */
    public void setMemoCount(String memoCount) {
        this.memoCount = memoCount;
    }

    /**
     * Gets the memoList.
     *
     * @return Returns the memoList.
     */
    public List<AppianMemoDetails> getMemoList() {
        return memoList;
    }

    /**
     * Sets The memoList.
     *
     * @param memoList The memoList to set.
     */
    public void setMemoList(List<AppianMemoDetails> memoList) {
        this.memoList = memoList;
    }

}
