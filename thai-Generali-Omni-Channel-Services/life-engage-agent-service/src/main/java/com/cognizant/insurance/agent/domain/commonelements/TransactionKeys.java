/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Jul 27, 2015
 */
package com.cognizant.insurance.agent.domain.commonelements;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

// TODO: Auto-generated Javadoc
/**
 * The Class class TransactionKeys.
 */
@Entity
public class TransactionKeys {

    /** The id. */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    /** The key1. */
    private String key1;

    /** The key2. */
    @Column(columnDefinition="nvarchar(100)")
    private String key2;

    /** The key3. */
    private String key3;

    /** The key4. */
    private String key4;

    /** The key5. */
    private String key5;

    /** The key6. */
	@Column(columnDefinition="nvarchar(400)")
    private String key6;

    /** The key7. */
    private String key7;

    /** The key8. */
    private String key8;

    /** The key9. */
    private String key9;

    /** The key10. */
    private String key10;

    /** The key11. */
    private String key11;

    /** The key12. */
    private String key12;

    /** The key13. */
    private String key13;

    /** The key14. */
    private String key14;

    /** The key15. */
    private String key15;

    /** The key16. */
    private String key16;

    /** The key17. */
    private String key17;

    /** The key18. */
    private String key18;

    /** The key19. */
    private String key19;

    /** The key20. */
    private String key20;

    /** The key21. */
    private String key21;

    /** The key22. */
    private String key22;

    /** The key23. */
    private String key23;

    /** The key24. */
    private String key24;

    /** The key25. */
    private String key25;

    /** The key26. */
    private String key26;

    /** The key27. */
    private String key27;

    /** The key28. */
    private String key28;

    /** The key29. */
    private String key29;

    /** The key30. */
    private String key30;
    
    /** The business creation date. */
    private Date businessCreationDate; 
    
    /** The business modified date. */
    private Date businessModifiedDate;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the key1.
     * 
     * @return the key1
     */
    public String getKey1() {
        return key1;
    }

    /**
     * Sets the key1.
     * 
     * @param key1
     *            the key1 to set.
     */
    public void setKey1(String key1) {
        this.key1 = key1;
    }

    /**
     * Gets the key2.
     * 
     * @return the key2
     */
    public String getKey2() {
        return key2;
    }

    /**
     * Sets the key2.
     * 
     * @param key2
     *            the key2 to set.
     */
    public void setKey2(String key2) {
        this.key2 = key2;
    }

    /**
     * Gets the key3.
     * 
     * @return the key3
     */
    public String getKey3() {
        return key3;
    }

    /**
     * Sets the key3.
     * 
     * @param key3
     *            the key3 to set.
     */
    public void setKey3(String key3) {
        this.key3 = key3;
    }

    /**
     * Gets the key4.
     * 
     * @return the key4
     */
    public String getKey4() {
        return key4;
    }

    /**
     * Sets the key4.
     * 
     * @param key4
     *            the key4 to set.
     */
    public void setKey4(String key4) {
        this.key4 = key4;
    }

    /**
     * Gets the key5.
     * 
     * @return the key5
     */
    public String getKey5() {
        return key5;
    }

    /**
     * Sets the key5.
     * 
     * @param key5
     *            the key5 to set.
     */
    public void setKey5(String key5) {
        this.key5 = key5;
    }

    /**
     * Gets the key6.
     * 
     * @return the key6
     */
    public String getKey6() {
        return key6;
    }

    /**
     * Sets the key6.
     * 
     * @param key6
     *            the key6 to set.
     */
    public void setKey6(String key6) {
        this.key6 = key6;
    }

    /**
     * Gets the key7.
     * 
     * @return the key7
     */
    public String getKey7() {
        return key7;
    }

    /**
     * Sets the key7.
     * 
     * @param key7
     *            the key7 to set.
     */
    public void setKey7(String key7) {
        this.key7 = key7;
    }

    /**
     * Gets the key8.
     * 
     * @return the key8
     */
    public String getKey8() {
        return key8;
    }

    /**
     * Sets the key8.
     * 
     * @param key8
     *            the key8 to set.
     */
    public void setKey8(String key8) {
        this.key8 = key8;
    }

    /**
     * Gets the key9.
     * 
     * @return the key9
     */
    public String getKey9() {
        return key9;
    }

    /**
     * Sets the key9.
     * 
     * @param key9
     *            the key9 to set.
     */
    public void setKey9(String key9) {
        this.key9 = key9;
    }

    /**
     * Gets the key10.
     * 
     * @return the key10
     */
    public String getKey10() {
        return key10;
    }

    /**
     * Sets the key10.
     * 
     * @param key10
     *            the key10 to set.
     */
    public void setKey10(String key10) {
        this.key10 = key10;
    }

    /**
     * Gets the key11.
     * 
     * @return the key11
     */
    public String getKey11() {
        return key11;
    }

    /**
     * Sets the key11.
     * 
     * @param key11
     *            the key11 to set.
     */
    public void setKey11(String key11) {
        this.key11 = key11;
    }

    /**
     * Gets the key12.
     * 
     * @return the key12
     */
    public String getKey12() {
        return key12;
    }

    /**
     * Sets the key12.
     * 
     * @param key12
     *            the key12 to set.
     */
    public void setKey12(String key12) {
        this.key12 = key12;
    }

    /**
     * Gets the key13.
     * 
     * @return the key13
     */
    public String getKey13() {
        return key13;
    }

    /**
     * Sets the key13.
     * 
     * @param key13
     *            the key13 to set.
     */
    public void setKey13(String key13) {
        this.key13 = key13;
    }

    /**
     * Gets the key14.
     * 
     * @return the key14
     */
    public String getKey14() {
        return key14;
    }

    /**
     * Sets the key14.
     * 
     * @param key14
     *            the key14 to set.
     */
    public void setKey14(String key14) {
        this.key14 = key14;
    }

    /**
     * Gets the key15.
     * 
     * @return the key15
     */
    public String getKey15() {
        return key15;
    }

    /**
     * Sets the key15.
     * 
     * @param key15
     *            the key15 to set.
     */
    public void setKey15(String key15) {
        this.key15 = key15;
    }

    /**
     * Gets the key16.
     * 
     * @return the key16
     */
    public String getKey16() {
        return key16;
    }

    /**
     * Sets the key16.
     * 
     * @param key16
     *            the key16 to set.
     */
    public void setKey16(String key16) {
        this.key16 = key16;
    }

    /**
     * Gets the key17.
     * 
     * @return the key17
     */
    public String getKey17() {
        return key17;
    }

    /**
     * Sets the key17.
     * 
     * @param key17
     *            the key17 to set.
     */
    public void setKey17(String key17) {
        this.key17 = key17;
    }

    /**
     * Gets the key18.
     * 
     * @return the key18
     */
    public String getKey18() {
        return key18;
    }

    /**
     * Sets the key18.
     * 
     * @param key18
     *            the key18 to set.
     */
    public void setKey18(String key18) {
        this.key18 = key18;
    }

    /**
     * Gets the key19.
     * 
     * @return the key19
     */
    public String getKey19() {
        return key19;
    }

    /**
     * Sets the key19.
     * 
     * @param key19
     *            the key19 to set.
     */
    public void setKey19(String key19) {
        this.key19 = key19;
    }

    /**
     * Gets the key20.
     * 
     * @return the key20
     */
    public String getKey20() {
        return key20;
    }

    /**
     * Sets the key20.
     * 
     * @param key20
     *            the key20 to set.
     */
    public void setKey20(String key20) {
        this.key20 = key20;
    }

    /**
     * Gets the key21.
     * 
     * @return the key21
     */
    public String getKey21() {
        return key21;
    }

    /**
     * Sets the key21.
     * 
     * @param key21
     *            the key21 to set.
     */
    public void setKey21(String key21) {
        this.key21 = key21;
    }

    /**
     * Gets the key22.
     * 
     * @return the key22
     */
    public String getKey22() {
        return key22;
    }

    /**
     * Sets the key22.
     * 
     * @param key22
     *            the key22 to set.
     */
    public void setKey22(String key22) {
        this.key22 = key22;
    }

    /**
     * Gets the key23.
     * 
     * @return the key23
     */
    public String getKey23() {
        return key23;
    }

    /**
     * Sets the key23.
     * 
     * @param key23
     *            the key23 to set.
     */
    public void setKey23(String key23) {
        this.key23 = key23;
    }

    /**
     * Gets the key24.
     * 
     * @return the key24
     */
    public String getKey24() {
        return key24;
    }

    /**
     * Sets the key24.
     * 
     * @param key24
     *            the key24 to set.
     */
    public void setKey24(String key24) {
        this.key24 = key24;
    }

    /**
     * Gets the key25.
     * 
     * @return the key25
     */
    public String getKey25() {
        return key25;
    }

    /**
     * Sets the key25.
     * 
     * @param key25
     *            the key25 to set.
     */
    public void setKey25(String key25) {
        this.key25 = key25;
    }

    /**
     * Gets the key26.
     * 
     * @return the key26
     */
    public String getKey26() {
        return key26;
    }

    /**
     * Sets the key26.
     * 
     * @param key26
     *            the key26 to set.
     */
    public void setKey26(String key26) {
        this.key26 = key26;
    }

    /**
     * Gets the key27.
     * 
     * @return the key27
     */
    public String getKey27() {
        return key27;
    }

    /**
     * Sets the key27.
     * 
     * @param key27
     *            the key27 to set.
     */
    public void setKey27(String key27) {
        this.key27 = key27;
    }

    /**
     * Gets the key28.
     * 
     * @return the key28
     */
    public String getKey28() {
        return key28;
    }

    /**
     * Sets the key28.
     * 
     * @param key28
     *            the key28 to set.
     */
    public void setKey28(String key28) {
        this.key28 = key28;
    }

    /**
     * Gets the key29.
     * 
     * @return the key29
     */
    public String getKey29() {
        return key29;
    }

    /**
     * Sets the key29.
     * 
     * @param key29
     *            the key29 to set.
     */
    public void setKey29(String key29) {
        this.key29 = key29;
    }

    /**
     * Gets the key30.
     * 
     * @return the key30
     */
    public String getKey30() {
        return key30;
    }

    /**
     * Sets the key30.
     * 
     * @param key30
     *            the key30 to set.
     */
    public void setKey30(String key30) {
        this.key30 = key30;
    }

	public Date getBusinessCreationDate() {
		return businessCreationDate;
	}

	public void setBusinessCreationDate(Date businessCreationDate) {
		this.businessCreationDate = businessCreationDate;
	}

	public Date getBusinessModifiedDate() {
		return businessModifiedDate;
	}

	public void setBusinessModifiedDate(Date businessModifiedDate) {
		this.businessModifiedDate = businessModifiedDate;
	}

}
