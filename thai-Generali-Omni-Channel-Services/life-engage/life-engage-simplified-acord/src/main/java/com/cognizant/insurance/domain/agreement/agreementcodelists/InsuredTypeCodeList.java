/**
 * 
 */
package com.cognizant.insurance.domain.agreement.agreementcodelists;

/**
 * A code list indicating the type of insured. 
 * 
 * @author 301350
 * 
 * 
 */
/**
 * @author 444873
 *
 */
public enum InsuredTypeCodeList {
    /**
     * A party (person or organization) that is not automatically included as an
     * insured under the agreement (e.g. contract) of another (insured) party,
     * but for whom the named insured's agreement provides a certain degree of
     * protection.
     * 
     * An endorsement is typically required to effect additional insured status.
     * The named insured's impetus for providing additional insured status to
     * others may be a desire to protect the other person or organization
     * because of a close relationship with that person or organization (e.g.,
     * employees or members of an insured club), or to comply with a contractual
     * agreement requiring the named insured to do so (e.g., customers or owners
     * of property leased by the named insured).
     * 
     * 
     * 
     */
    Additional,
    /**
     * 
     * 
     * 
     * 
     */
    Joint,
    /**
     * The first name on the declaration page of the agreement (e.g. contract).
     * 
     * Explanation: Because there is wording in the contract specific to 'First
     * Name Insured'. The first Named Insured is required to keep records. Audit
     * and cancellations notices are sent to the first Named Insured.
     * Non-renewal notice is sent to the first Named Insured. The contract is
     * very specific.
     * 
     * 
     * 
     */
    FirstNamed,
    /**
     * A party (person or organization), other than the first named insured,
     * identified as an insured in the agreement (e.g. contract) declarations or
     * an addendum to the contract declarations.
     * 
     * Explanation: It can be all the Named Insured on the declarations page
     * other than the First Named Insured. It could also apply as you endorse
     * another party onto the declarations page. Or sometimes, it can apply when
     * you acquire a new operation or organization.
     * 
     * The name on the declaration page of the contract. Explanation: There is
     * wording in the CGL form that specifically refers to the Named Insured.
     * For instance, 'No person or organization is an insured with respect to
     * the conduct of any current or past partnership, joint venture or limited
     * liability company that is not shown as a Named Insured in the
     * Declarations.' See also Insured.
     * 
     * The term additional named insured has not acquired a uniformly
     * agreed-upon meaning within the insurance industry, and use of the term in
     * the two different senses defined above often produces confusion in
     * requests for additional insured status between contracting parties. <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    Named,
    /**
     * A party covered as defined in the insurance contract that is named as the
     * primary insured. Coverage is provided primarily to the primary insured
     * and the insurer will primarily deal with the primary insured, or his
     * representative, for all issues concerning the insurance policy.
     * 
     * In life insurance policies there is one designated insured, the person so
     * named; or a contract can be issued to numerous insureds on a group basis.
     * 
     * For non-life insurance policies (e.g. P&C, general insurance) the primary
     * insured is often referred to as the "first named insured" with
     * corresponding responsibilities and provisions as outlined in the
     * agreement.
     * 
     * For example, a personal auto policy may show "John Doe" as the
     * policyholder on the declarations page, thereby making him the
     * "first named insured". John's wife and children who reside with him would
     * be considered "insureds" on this policy.
     * 
     * 
     * 
     */
    Primary,
    
    /**
     * Added for Generali
     */
    NotMentioned
}
