/**
 * 
 */
package com.cognizant.insurance.omnichannel.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.component.CheckinComponent;
import com.cognizant.insurance.omnichannel.domain.CheckinData;
import com.cognizant.insurance.omnichannel.domain.CheckinData.Status;
import com.cognizant.insurance.omnichannel.service.CheckinService;
import com.cognizant.insurance.omnichannel.vo.CheckinResponse;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;

/**
 * @author 397850
 *
 */
@Service
public class CheckinServiceImpl implements CheckinService {
	
	/** The Constant REQUEST. */
	private static final String REQUEST = "Request";

	/** The Constant REQUEST_INFO. */
	private static final String REQUEST_INFO = "RequestInfo";

	/** The Constant REQUEST_PAYLOAD. */
	private static final String REQUEST_PAYLOAD = "RequestPayload";

	/** The Constant TRANSACTIONS. */
	private static final String TRANSACTIONS = "Transactions";
	
	public static final String CHECKINDETAILS="CheckInDetails";
	
	public static final String TRANSACTIONDATA="TransactionData";
	
	public static final String AGENTID="AgentID";
	
	public static final String CHECKEDINBRANCHID="CheckedinBranch";
	
	private static final String SENDNOTIFICATION = "sendPushnotification";
	
	private static final String NOTIFIERID="NotifierID";
	
	private static final String TYPE="type";
	
	private static final String AGENTTYPE="agentType";
	
	private static final String AGENTNAME="agentName";
	
	private static final String USERDETAILS="UserDetails";
	
	private static final String AGENTCODE="agentCode";
	
	private static final String BRANCHNAME="branchName";
	
	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory.getLogger(CheckinServiceImpl.class);
	
	@Autowired
	private CheckinComponent checkinComponent;

	@Override
	public String checkinAgentDetails(String requestJson) throws BusinessException {
		JSONObject jsonObject;
		String response="";
		String typeVariableObj="";
		CheckinData checkinData=new CheckinData();
		List<String> pushNotificationList=new ArrayList<String>();
		final JSONArray jsonRsArray = new JSONArray();
	    final JSONObject jsonObjectRs = new JSONObject();
		try {
			jsonObject = new JSONObject(requestJson);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			if(jsonTransactionArray.length()>0){
				for(int j=0;j<jsonTransactionArray.length();j++){
					JSONObject request = jsonTransactionArray.getJSONObject(j);
					JSONObject transactionData= request.getJSONObject(TRANSACTIONDATA);
					JSONObject checkinDetails=transactionData.getJSONObject(CHECKINDETAILS);
				
					String agentId=checkinDetails.getString(AGENTID);
					String branchId=checkinDetails.getString(CHECKEDINBRANCHID);
					Boolean sendNotification=checkinDetails.getBoolean(SENDNOTIFICATION);
					JSONArray notifierList=checkinDetails.getJSONArray(NOTIFIERID);
					typeVariableObj=checkinDetails.getString(TYPE);
					String agentName=checkinDetails.getString(AGENTNAME);
					String agentType=checkinDetails.getString(AGENTTYPE);
					Date checkindate=LifeEngageComponentHelper.getCurrentdate();
					checkinData.setAgentId(agentId);
					checkinData.setBranchId(branchId);
					checkinData.setCheckinDateTime(checkindate);
					checkinData.setAgentName(agentName);
					checkinData.setStatus(Status.checkedin);
					checkinData.setAgentType(agentType);
					checkinData.setBranchName(checkinDetails.getString(BRANCHNAME));
					if(notifierList.length()>0){
						for(int i=0; i<notifierList.length(); i++){
							pushNotificationList.add(notifierList.getString(i));
						}
					}
					response=checkinComponent.checkinDetailsSave(checkinData,requestInfo,sendNotification,pushNotificationList,typeVariableObj);
				}
			}
			LifeEngageComponentHelper.createStatusResponse(requestInfo,jsonRsArray,jsonObjectRs,response);
				
		} catch (ParseException e) {
			LOGGER.error("Parse exception",e.getMessage());
			response="Failure";
			throw new BusinessException(e);
		} catch(Exception e){	
			LOGGER.error("Exception",e.getMessage());
			response="Failure";
			
		}
		return jsonObjectRs.toString();
	}

	@Override
	public String retrieveDetails(String json) throws BusinessException {
		
		JSONObject jsonObject;
		List<String> agentList=new ArrayList<String>();
		
		List<CheckinResponse> checkinResponseList=new ArrayList<CheckinResponse>();
		String response="";

		try {
			jsonObject = new JSONObject(json);
			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			if(jsonTransactionArray.length()>0){
				for(int j=0;j<jsonTransactionArray.length();j++){
					JSONObject request = jsonTransactionArray.getJSONObject(j);
					JSONObject transactionData= request.getJSONObject(TRANSACTIONDATA);
					JSONArray userDetails=transactionData.getJSONArray(USERDETAILS);

					for(int i=0;i<userDetails.length();i++){
						JSONObject userDetail=userDetails.getJSONObject(i);
						CheckinResponse checkinResponse =new CheckinResponse();
						agentList.add(userDetail.getString(AGENTCODE));
						checkinResponse.setAgentCode(userDetail.getString(AGENTCODE));	
						checkinResponse.setAgentName(userDetail.getString(AGENTNAME));
						checkinResponseList.add(checkinResponse);
					}
				}
			}
			
			response=checkinComponent.retrieveDetails(requestInfo,agentList,checkinResponseList);
		} catch (ParseException e) {
			throw new BusinessException(e);
		}catch(Exception e){
			LOGGER.error("Exception",e.getMessage());
			
		}
		
		return response;
	}

}
