package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class SalesActivityResponseDataVo implements Serializable {

    /** The serialVersionUID **/
    private static final long serialVersionUID = 1L;

    /** The command **/
    private String command;

    /** The agentCode **/
    private String agentCode;

    /** The teamAgent **/
    private String teamAgent;

    /** The team **/
    private String team;

    /** The teamDetails **/
    private List<TeamDataVo> teamDetails;

    /** The Sales Activity Tab Details **/
    private List<SalesActivityDataVo> salesActivityDetails;

    /** The Scoring Tab Details **/
    private List<ScoringDataVo> scoringDetails;

    /** The Conversion Ratio Details **/
    private List<ConversionRatioDataVo> conversionRatioDetails;
    
    private HashMap<String,List<AgentDataVo>> teamListMap;
    
    private String position;

    public String getTeamAgent() {
        return teamAgent;
    }

    public void setTeamAgent(String teamAgent) {
        this.teamAgent = teamAgent;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public List<ConversionRatioDataVo> getConversionRatioDetails() {
        return conversionRatioDetails;
    }

    public void setConversionRatioDetails(List<ConversionRatioDataVo> conversionRatioDetails) {
        this.conversionRatioDetails = conversionRatioDetails;
    }

    public List<ScoringDataVo> getScoringDetails() {
        return scoringDetails;
    }

    public void setScoringDetails(List<ScoringDataVo> scoringDetails) {
        this.scoringDetails = scoringDetails;
    }

    public List<SalesActivityDataVo> getSalesActivityDetails() {
        return salesActivityDetails;
    }

    public void setSalesActivityDetails(List<SalesActivityDataVo> salesActivityDetails) {
        this.salesActivityDetails = salesActivityDetails;
    }

    public List<TeamDataVo> getTeamDetails() {
        return teamDetails;
    }

    public void setTeamDetails(List<TeamDataVo> teamDetails) {
        this.teamDetails = teamDetails;
    }

    public HashMap<String, List<AgentDataVo>> getTeamListMap() {
        return teamListMap;
    }

    public void setTeamListMap(HashMap<String, List<AgentDataVo>> teamListMap) {
        this.teamListMap = teamListMap;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
