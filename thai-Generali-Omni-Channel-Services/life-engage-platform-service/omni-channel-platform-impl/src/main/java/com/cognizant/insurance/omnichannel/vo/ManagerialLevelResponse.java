package com.cognizant.insurance.omnichannel.vo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ManagerialLevelResponse {
	
	private String responseJSON;
	
	private ManagerialLevelUserDetails managerialLevelUserDetailsList;

	public String getResponseJSON() {
		return responseJSON;
	}

	public void setResponseJSON(String responseJSON) {
		this.responseJSON = responseJSON;
	}

	public ManagerialLevelUserDetails getManagerialLevelUserDetailsList() {
		return managerialLevelUserDetailsList;
	}

	public void setManagerialLevelUserDetailsList(
			ManagerialLevelUserDetails managerialLevelUserDetailsList) {
		this.managerialLevelUserDetailsList = managerialLevelUserDetailsList;
	}

	


	
}
