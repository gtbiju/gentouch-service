/**
 * 
 */
package com.cognizant.insurance.domain.party.partycodelists;

/**
 * A code list representing various types of names common to all parties.
 * 
 * @author 301350
 * 
 * 
 */
public enum CommonPartyNameUsageCodeList {
    /**
     * also known as "official name".
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    LegalName,
    /**
     * 
     * 
     * 
     * 
     */
    AlsoKnownAsNameAKA
}
