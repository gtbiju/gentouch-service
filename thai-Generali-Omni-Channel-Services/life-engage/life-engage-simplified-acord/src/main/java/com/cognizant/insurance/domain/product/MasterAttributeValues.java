/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Sep 20, 2017
 */

package com.cognizant.insurance.domain.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.cognizant.insurance.domain.product.PropertyValueType;

@Entity
@Table(name = "MST_PROPERTY_VALUES")
public class MasterAttributeValues {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	/** The data type. */
	@ManyToOne
	@JoinColumn(name = "DATA_TYPE")
	private PropertyValueType dataType;
	
	/** The data value. */
	@Column(name="DATA_VALUE")
	private String dataValue;
	
	/** The default ind. */
	@Column(name="DEFAULT_INDICATOR")
	private Boolean defaultInd;
	
	@ManyToOne
	@JoinColumn(name="propertySpecification_id")
	private MasterAttribute property;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PropertyValueType getDataType() {
		return dataType;
	}

	public void setDataType(PropertyValueType dataType) {
		this.dataType = dataType;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}

	public Boolean getDefaultInd() {
		return defaultInd;
	}

	public void setDefaultInd(Boolean defaultInd) {
		this.defaultInd = defaultInd;
	}

	public MasterAttribute getProperty() {
		return property;
	}

	public void setProperty(MasterAttribute property) {
		this.property = property;
	}
}
