package com.cognizant.insurance.omnichannel.utils;

/**
 * @author 420118
 * 
 *         The Class SelectedRiderTableData
 * 
 *         This class is a POJO for Rider details in illustration PDF..
 * 
 */
public class SelectedRiderTableData {

	private String riderNameForPdf;

	private String riderSumAssured;

	private String riderPremium;

	/**
	 * @return the riderName
	 */
	public String getRiderNameForPdf() {
		return riderNameForPdf;
	}

	/**
	 * @param riderName
	 *            the riderName to set
	 */
	public void setRiderNameForPdf(String riderNameForPdf) {
		this.riderNameForPdf = riderNameForPdf;
	}

	/**
	 * @return the sumAssured
	 */
	public String getRiderSumAssured() {
		return riderSumAssured;
	}

	/**
	 * @param sumAssured
	 *            the sumAssured to set
	 */
	public void setRiderSumAssured(String riderSumAssured) {
		this.riderSumAssured = riderSumAssured;
	}

	/**
	 * @return the totalPremium
	 */
	public String getRiderPremium() {
		return riderPremium;
	}

	/**
	 * @param totalPremium
	 *            the totalPremium to set
	 */
	public void setRiderPremium(String riderPremium) {
		this.riderPremium = riderPremium;
	}

	

}
