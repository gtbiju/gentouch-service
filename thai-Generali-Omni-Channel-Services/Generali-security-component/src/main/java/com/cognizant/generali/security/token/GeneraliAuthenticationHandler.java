package com.cognizant.generali.security.token;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.cognizant.generali.security.GeneraliAuthResponse;
import com.cognizant.generali.security.GeneraliUserDetailsImpl;
import com.cognizant.generali.security.constants.GeneraliExceptionCodes;
import com.cognizant.generali.security.exception.AccountLockedException;
import com.cognizant.generali.security.exception.PasswordExpiredException;
import com.cognizant.generali.security.exception.UserLicenseException;
import com.cognizant.generali.security.exception.UserTerminatedException;
import com.cognizant.generali.security.exception.UserUnauthorizedException;
import com.cognizant.le.security.exception.PasswordIncorrectException;
import com.cognizant.le.security.exception.UserNotFoundException;
import com.cognizant.le.security.token.AuthenticationHandler;

public class GeneraliAuthenticationHandler extends AuthenticationHandler {
	
	/***
	 * On Auth Success it will return 200 and Auth is Success and User is suspected return 220
	 */
	@Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {

		int respStatus=HttpServletResponse.SC_OK;
		if(authentication.getPrincipal() instanceof GeneraliUserDetailsImpl){
			GeneraliUserDetailsImpl detailsImpl=(GeneraliUserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if(detailsImpl.isSuspented()){
				respStatus=GeneraliAuthResponse.USER_SUSPENDED;
			}
		}
		response.setStatus(respStatus);

        UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<SimpleGrantedAuthority> authorities = (List<SimpleGrantedAuthority>) principal.getAuthorities();
        JSONObject jsonObj = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        if (authorities != null) {
            for (SimpleGrantedAuthority simpleGrantedAuthority : authorities) {
                jsonArray.put(simpleGrantedAuthority.getAuthority());
            }
        }
        jsonObj.put("roles", jsonArray);
        jsonObj.put("Status", GeneraliAuthResponse.SUCCESS);
        response.setContentType("application/json");
        response.getWriter().write(jsonObj.toString());
    }


	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		JSONObject jsonObj = new JSONObject();
		try {
            throw exception;        
        } catch (UserLicenseException e) {
            response.setStatus(Integer.parseInt(e.getErrorCode()));
		} catch (UserTerminatedException e) {
			response.setStatus(Integer.parseInt(e.getErrorCode()));
		} catch (UserUnauthorizedException e) {
			response.setStatus(Integer.parseInt(e.getErrorCode()));
		} catch (UserNotFoundException e){
			response.setStatus(Integer.parseInt(e.getErrorCode()));
        } catch (PasswordIncorrectException e){
        	response.setStatus(Integer.parseInt(e.getErrorCode()));
        } catch (AuthenticationServiceException e){
        	response.setStatus(Integer.parseInt(GeneraliExceptionCodes.ERROR_502));
        } catch (AccountLockedException e){
			response.setStatus(Integer.parseInt(e.getErrorCode()));
        } catch (PasswordExpiredException e){
			response.setStatus(Integer.parseInt(e.getErrorCode()));
        }
		catch (Exception e) {
			jsonObj.put("Status", HttpServletResponse.SC_UNAUTHORIZED);
			//jsonObj.put("Status", GeneraliExceptionCodes.ERROR_502);
			jsonObj.put("Message", e.getMessage());
			response.setContentType("application/json");
			response.getWriter().write(jsonObj.toString());
        }
	}
}
