/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 15, 2012
 */
package com.cognizant.insurance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cognizant.insurance.dao.Dao;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.Request;

/**
 * The Class DaoImpl.
 * 
 */
public class DaoImpl implements Dao {

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#save(com.cognizant.icr.request.Request)
     */
    @Override
    public final <T> void save(final Request<T> request) {
        try {
            final JPARequestImpl<T> jpaRequestImpl = (JPARequestImpl<T>) request;
            jpaRequestImpl.getEntityManager().persist(jpaRequestImpl.getType());
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#save(java.lang.Object, javax.persistence.EntityManager)
     */
    @Override
    public final <T> void save(final T type, final EntityManager entityManager) {
        try {
            entityManager.merge(type);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#merge(com.cognizant.icr.request.Request)
     */
    @Override
    public final <T> void merge(final Request<T> request) {
        try {
            final JPARequestImpl<T> jpaRequestImpl = (JPARequestImpl<T>) request;
            jpaRequestImpl.getEntityManager().merge(jpaRequestImpl.getType());
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#merge(java.lang.Object, javax.persistence.EntityManager)
     */
    @Override
    public final <T> void merge(final T type, final EntityManager entityManager) {
        try {
            entityManager.merge(type);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#findClass(com.cognizant.icr.request.Request, java.lang.Integer)
     */
    @Override
    public final <T> Object findById(final Request<T> request, final Object primaryKey) {
        try {
            return ((JPARequestImpl<T>) request).getEntityManager().find(request.getType().getClass(), primaryKey);
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public final <T, R> T findClassById(final Request<R> request, final Object primaryKey, final Class<T> clazz) {
        return ((JPARequestImpl<R>) request).getEntityManager().find(clazz, primaryKey);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#remove(com.cognizant.icr.request.Request)
     */
    @Override
    public final <T> void remove(final Request<T> request) {
        try {
            final JPARequestImpl<T> jpaRequestImpl = (JPARequestImpl<T>) request;
            jpaRequestImpl.getEntityManager().remove(jpaRequestImpl.getType());
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#findByNamedQuery(com.cognizant.icr.request.Request, java.lang.String,
     * java.lang.Object[])
     */
    @Override
    public final <T> List<T> findByNamedQuery(final Request<T> request, final String queryString,
            final Object... params) {

        try {
            final JPARequestImpl<T> jpaRequestImpl = (JPARequestImpl<T>) request;
            final Query query = jpaRequestImpl.getEntityManager().createNamedQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);                    
                }
            }
            return (List<T>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#findByQuery(com.cognizant.icr.request.Request, java.lang.String,
     * java.lang.Object[])
     */
    @Override
    public final <T> List<T> findByQuery(final Request<T> request, final String queryString, final Object... params) {
        try {
            final JPARequestImpl<T> jpaRequestImpl = (JPARequestImpl<T>) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<T>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.dao.Dao#executeUpdate(Request, String, Object[])
     */
    @Override
    public final <T> int executeUpdate(final Request<T> request, final String queryString, final Object... params) {
        try {
            final JPARequestImpl<?> jpaRequestImpl = (JPARequestImpl<?>) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return query.executeUpdate();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.insurance.dao.Dao#findByQuery1(com.cognizant.insurance.request.Request, java.lang.String, java.lang.Object[])
     */
    @Override
    public <T, R> List<R> findByQueryAndReturnDiffType(Request<T> request, String queryString, Object... params) {
        try {
            final JPARequestImpl<T> jpaRequestImpl = (JPARequestImpl<T>) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<R>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }
    
    @Override
    public final List<Object> findByNativeQuery(final Request request, final String queryString, final Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createNativeQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                   query.setParameter(i + 1, params[i]);
                }
            }

            return (List<Object>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

}
