package com.cognizant.insurance.omnichannel.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.internet.MimeBodyPart;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.icr.pdf.component.PDFComponent;
import com.cognizant.icr.pdf.component.impl.PDFComponentImpl;
import com.cognizant.icr.pdf.file.FileNameGenerator;
import com.cognizant.icr.pdf.file.impl.EAPPFileNameGenerator;
import com.cognizant.icr.pdf.repository.FileRepository;
import com.cognizant.icr.pdf.repository.impl.FileRepositoryImpl;
import com.cognizant.icr.pdf.request.PDFRequest;
import com.cognizant.icr.pdf.request.impl.PDFRequestImpl;
import com.cognizant.insurance.audit.LifeEngagePayloadAudit;
import com.cognizant.insurance.component.impl.LifeEngageDocumentComponentImpl;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.EAppRepository;
import com.cognizant.insurance.component.repository.IllustrationRepository;
import com.cognizant.insurance.component.repository.RequirementDocumentFileRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.codes.CodeLookUp;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.party.Party;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliDocumentComponent;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliDocumentRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliEAppRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliRequirementDocumentFileRepository;
import com.cognizant.insurance.omnichannel.component.repository.LifeAsiaRepository;
import com.cognizant.insurance.omnichannel.component.repository.UserRepository;
import com.cognizant.insurance.omnichannel.component.repository.helper.GeneraliComponentRepositoryHelper;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.omnichannel.utils.FileOperationsUtil;
import com.cognizant.insurance.omnichannel.vo.GeneraliTransactions;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.service.repository.CodeLookUpRepository;
import com.cognizant.insurance.services.vo.SignatureLine;
import com.cognizant.insurance.utils.Base64Utils;
import com.google.gson.Gson;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class GeneraliDocumentComponenetImpl extends
		LifeEngageDocumentComponentImpl implements GeneraliDocumentComponent{
	@Autowired
	private GeneraliDocumentRepository documentRepository;

	/** The illustration repository. */
	@Autowired
	private IllustrationRepository illustrationRepository;
	
	 /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;
    
    /** The e app repository. */
    @Autowired
    private GeneraliEAppRepository eAppRepository;
    
    @Autowired
    private LifeAsiaRepository bpmRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private CodeLookUpRepository codeLookUpRepository;
    
    /**
     * The requirementDocumentFileRepository
     */
    @Autowired
    RequirementDocumentFileRepository requirementDocumentFileRepository;
    
    /**
     * The requirementDocumentFileRepository
     */
    @Autowired
    GeneraliRequirementDocumentFileRepository generaliRequirementDocumentFileRepository;
    

    /** The Constant ID. */
    private static final String ID = "id";
    
    /** The Constant ILLUSTRATION. */
    private static final String ILLUSTRATION = "illustration";    

    /** The Constant TYPE. */
    private static final String TYPE = "Type";
    
    /** The Constant TEMPLATE_IN **/
    private static final String TEMPLATE_IN = "template_in";

    /** The Constant TEMPLATE_EN **/
    private static final String TEMPLATE_EN = "template_en";
    
    /** The Constant TEMPLATE_ILLUSTRATION_COPY to attch with SPAJ **/
    private static final String TEMPLATE_ILLUSTRATION_COPY = "template_illustration_copy";
    
    /** The Constant AdditionalQuestionnaire_case. */
    private static final String IS_ADDITIONAL_QUESTIONNAIRE_CASE = "isAdditionalQuestionnaireCase";
    
    /** The Constant DATEFORMAT_YYYY_MM_DD_HH_MM_SS. */
    public static final String DATEFORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    /** The Constant ILLUSTRATION_ID. */
    private static final String ILLUSTRATION_ID = "illustrationId";
        
    /** The Constant E_APP. */
    private static final String E_APP = "eApp";
    
    /** The Constant PDF. */
    private static final String PDF = ".pdf";

    @Autowired
	private FileOperationsUtil fileOperationsUtil;
    
    @Value("${generali.platform.bpm.staticPDFPath}")
    private String staticPDFPath;
    
    @Value("${le.platform.service.fileUploadPath}")
    private String fileUploadPath;
    
    @Value("${generali.platform.bpm.commonMemoTemplateID}")
    private String commonMemoTemplateId;
    
    @Value("${generali.platform.bpm.COFMemoTemplateID}")
    private String memoCOFTemplateId;
    
    @Value("${le.platform.default.eAppTemplate}")
    private String defaultEAppPDFTemplate;
    
	/** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliDocumentComponenetImpl.class);
   

	/**
	 * Generate illustration pdf.
	 * 
	 * @param proposalnumber
	 *            the proposal number
	 * @param type
	 *            the type
	 * @param templateId
	 *            the product code
	 * @return the byte array output stream
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class }, readOnly = true, propagation = Propagation.REQUIRES_NEW)
	public ByteArrayOutputStream generateIllustrationPdf(String proposalNumber,
			String type, String templateId) {

		PDFComponent<Transactions> pdfComponentImpl;
		FileNameGenerator fileNameGenerator;
		FileRepository fileRepository;
		pdfComponentImpl = new PDFComponentImpl<Transactions>();
		fileNameGenerator = new EAPPFileNameGenerator();
		fileRepository = new FileRepositoryImpl();
		ByteArrayOutputStream byteStream = null;
		final ArrayList<Transactions> transactionsList = new ArrayList<Transactions>();
		Transactions responseTransactions = new Transactions();

		try {
			if (proposalNumber != null) {
				responseTransactions=getDocumentsForPDF(proposalNumber, type);
			}
			transactionsList.add(responseTransactions);

			PDFRequest<Transactions> pdfRequest = getPDFRequest(
					transactionsList, type, templateId);

			pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
			pdfComponentImpl.setFileRepository(fileRepository);

			byteStream = (ByteArrayOutputStream) pdfComponentImpl
					.createPdf(pdfRequest);

		} catch (IOException e) {
			throwSystemException(true, e.getMessage());

		}

		return byteStream;

	}
	
	 	@Override
	 	@Transactional(rollbackFor = { Exception.class }, readOnly = true, propagation = Propagation.REQUIRES_NEW)
	    public ByteArrayOutputStream generatePdfGet(String proposalNumber, String type, String templateId) {

	        PDFComponent<Transactions> pdfComponentImpl;
	        FileNameGenerator fileNameGenerator;
	        FileRepository fileRepository;
	        pdfComponentImpl = new PDFComponentImpl<Transactions>();
	        fileNameGenerator = new EAPPFileNameGenerator();
	        fileRepository = new FileRepositoryImpl();
	        ByteArrayOutputStream byteStream = null;
	        final ArrayList<Transactions> transactionsList = new ArrayList<Transactions>();

	        try {

	            if (proposalNumber != null) {
					 Transactions responseTransactions=getDocumentsForPDF(proposalNumber,type);
				
	               	LifeEngagePayloadAudit lifeEngagePayloadAudit =
	                        auditRepository.getLastLifeEngagePayloadAudit(proposalNumber);
	                if (lifeEngagePayloadAudit != null) {
	                    responseTransactions.setAgentId(lifeEngagePayloadAudit.getKey2());
	                    responseTransactions.setProductId(lifeEngagePayloadAudit.getKey3());
	                }

	                transactionsList.add(responseTransactions);
	                PDFRequest<Transactions> pdfRequest = getPDFRequest(transactionsList, type, templateId);
	                pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
	                pdfComponentImpl.setFileRepository(fileRepository);
	                byteStream = pdfComponentImpl.createPdf(pdfRequest);
	            }
	        }

	        catch (Exception e) {
	            throw new SystemException(e);
	        }
	        return byteStream;

	    }

	/*
	 * 
	 * Function to improve the clarity of Signature
	 */

	public BufferedImage convertJsonToImageForSign(String jsonString, String ProductCode) {
		Gson gson = new Gson();
		SignatureLine[] signatureLines = gson.fromJson(jsonString,
				SignatureLine[].class);
		int heightImage=200;
		int widthImage=410;
		int widthFill=410;
		int heightFill=225;
		if("9".equals(ProductCode)){
			heightImage=160;
			widthImage=200;
			widthFill=200;
			heightFill=160;
			
		}
		BufferedImage offscreenImage = new BufferedImage(widthImage, heightImage,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = offscreenImage.createGraphics();
		// Increased stroke
		g2.setStroke(new BasicStroke(6));
		g2.setColor(Color.white);
		g2.fillRect(0, 0, widthFill, heightFill);
		g2.setPaint(Color.black);
		for (SignatureLine line : signatureLines) {
			g2.drawLine(line.getLx(), line.getLy(), line.getMx(), line.getMy());
		}
		return offscreenImage;
	}

	/*
	 * 
	 * Function to improve the clarity of Signature
	 */

	public BufferedImage convertJsonToImageForSpajSign(String jsonString) {
		Gson gson = new Gson();
		SignatureLine[] signatureLines = gson.fromJson(jsonString,
				SignatureLine[].class);
		BufferedImage offscreenImage = new BufferedImage(480, 225,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = offscreenImage.createGraphics();
		// Increased stroke
		g2.setStroke(new BasicStroke(6));
		g2.setColor(new Color(241,239,240));
		g2.fillRect(0, 0, 480, 225);
		g2.setPaint(Color.black);
		for (SignatureLine line : signatureLines) {
			g2.drawLine(line.getLx(), line.getLy(), line.getMx(), line.getMy());
		}
		return offscreenImage;
	}

  
    /**
     * @param proposalNumber
     */
    @Override
    public Set<AgreementDocument> getBase64DocumentDetils(String proposalNumber) {

        Set<AgreementDocument> agreementDocuments = documentRepository.getBase64DocumentDetils(proposalNumber);
        return agreementDocuments;
    }
    
    /**
     * Method used to generate additional insured pdf
     * @param proposalNumber
     * @param type
     * @param templateId
     * @return
     */
    @Override
    public Set<AgreementDocument> getAdditionalInsuredPdf(String proposalNumber, String type, String templateId) {

        PDFComponent<Transactions> pdfComponentImpl;
        FileNameGenerator fileNameGenerator;
        FileRepository fileRepository;
        pdfComponentImpl = new PDFComponentImpl<Transactions>();
        fileNameGenerator = new EAPPFileNameGenerator();
        fileRepository = new FileRepositoryImpl();
        ByteArrayOutputStream byteStream = null;
        final ArrayList<Transactions> transactionsList = new ArrayList<Transactions>();
        ArrayList<ByteArrayOutputStream> pdfByteStremsList = new ArrayList<ByteArrayOutputStream> ();
        final Set<AgreementDocument> agreementDocumentsPdf = new HashSet<AgreementDocument>();
        String imageByteStream = "";
        try {

            if (proposalNumber != null) {
                Transactions responseTransactions = eAppRepository.retrieveEApp(proposalNumber, "");
                responseTransactions.setType(type);
                
                if (responseTransactions.getProposal().getRequirements() != null) {
                    
                	Set<Requirement> agreementRequirementDoc = new HashSet<Requirement>();
                        agreementRequirementDoc =
                        	generaliRequirementDocumentFileRepository.getRequirementDocFilesForPDF(responseTransactions,
                                        responseTransactions.getTransTrackingId());
                        String image = null;
                        Set<Document> documents = new HashSet<Document>();

                    if (null != agreementRequirementDoc) {
                    	for(Requirement agreementDocuments: agreementRequirementDoc){
                        if (null != agreementDocuments.getRequirementDocuments()) {
                            for (AgreementDocument doc : agreementDocuments.getRequirementDocuments()) {
                                for (Document docContent : doc.getPages()) {

                                    if (docContent.getFileNames().contains(".jpg")) {
                                      //  if (docContent.getBase64string() != null
                                               // && docContent.getBase64string().length() > 0) {
                                        	//to handle the second time hit for signature
                                        	if(docContent.getSignatureType()==null ||docContent.getSignatureType()==""){
                                        	docContent.setSignatureType(agreementDocuments.getRequirementSubType());
                                        	}
                                            documents.add(docContent);
                                            doc.setPages(documents);
                                          //  doc.setBase64string(docContent.getBase64string());
                                            imageByteStream = fileOperationsUtil.getFile(docContent.getFileNames(), proposalNumber,true);
                                            doc.setBase64string(imageByteStream);

                                       // }
                                    } else if (docContent.getFileContent() != null
                                            && docContent.getFileContent().length() > 0) {
                                        String signCordinates = docContent.getFileContent().replace("\\", "");
                                        BufferedImage buffImage = convertJsonToImageForSpajSign(signCordinates);
                                        if (buffImage != null) {
                                            java.io.ByteArrayOutputStream os = new java.io.ByteArrayOutputStream();
                                            ImageIO.write(buffImage, "jpg", os);
                                            byte[] data = os.toByteArray();
                                            image = Base64Utils.encode(data);
                                            docContent.setSignatureType(agreementDocuments.getRequirementSubType());
                                            docContent.setBase64string(image);
                                            documents.add(docContent);
                                        }
                                    }
                                }
                                responseTransactions.setDocument(doc);
                            }
                        }
                    	}
                    }
                       
                       // agreementRequirementDoc.setRequirementDocuments(requirementDocuments)
                       // agreementRequirementDoc.setPages(documents);
                       


                }
                if(responseTransactions != null && responseTransactions.getAdditionalInsuredes() != null 
                		&& responseTransactions.getAdditionalInsuredes().size() > 0){
                	byte[] data = null;
                	String base64QaddnIns = null;
            		
                	for(Insured addnInsured:responseTransactions.getAdditionalInsuredes()){
                		if(addnInsured != null){
                			responseTransactions.setAdditionalInsured(addnInsured);
                			byteStream = generateAdditionalInsuredPdf(proposalNumber,type,templateId,responseTransactions);
                			pdfByteStremsList.add(byteStream);

        					if (null != byteStream) {
        						String fileName = "AdditionalInsured_"+addnInsured.getPartyId()+proposalNumber+PDF;
        						data = byteStream.toByteArray();
        						base64QaddnIns = Base64Utils.encode(data);
        						AgreementDocument questionnaireDocument = new AgreementDocument();
        						questionnaireDocument.setBase64string(base64QaddnIns);
        						questionnaireDocument.setTypeCode(DocumentTypeCodeList.BPMPdf);
        						questionnaireDocument.setName(GeneraliConstants.ADD_INSURED_PDF+PDF);
        						questionnaireDocument.setDescription(GeneraliConstants.ADD_INSURED_PDF+addnInsured.getPartyId());
        						questionnaireDocument.setFileNames(fileName);    
        						questionnaireDocument.setDocumentTypeCode("AdditionalInsured");   
        						questionnaireDocument.setContentType("AdditionalInsured");        
        						agreementDocumentsPdf.add(questionnaireDocument);

        						try{
        							
        							fileOperationsUtil.createFileFromByteArray(proposalNumber,fileName,data);
        						}catch(Exception e){
        							LOGGER.error("-- Error while writing mail insured pdf "+e.getMessage());
        						}
        					}
                		}
                	}
                }

           /*     LifeEngagePayloadAudit lifeEngagePayloadAudit =
                        auditRepository.getLastLifeEngagePayloadAudit(proposalNumber);
                if (lifeEngagePayloadAudit != null) {
                    responseTransactions.setAgentId(lifeEngagePayloadAudit.getKey2());
                    responseTransactions.setProductId(lifeEngagePayloadAudit.getKey3());
                }

                transactionsList.add(responseTransactions);
                PDFRequest<Transactions> pdfRequest = getPDFRequest(transactionsList, type, templateId);
                pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
                pdfComponentImpl.setFileRepository(fileRepository);
                byteStream = pdfComponentImpl.createPdf(pdfRequest);*/
            }
        }

        catch (Exception e) {
            throw new SystemException(e);
        }
        return agreementDocumentsPdf;

    }

    public ByteArrayOutputStream generateAdditionalInsuredPdf(String proposalNumber, String type, String templateId,Transactions responseTransactions) {

        PDFComponent<Transactions> pdfComponentImpl;
        FileNameGenerator fileNameGenerator;
        FileRepository fileRepository;
        pdfComponentImpl = new PDFComponentImpl<Transactions>();
        fileNameGenerator = new EAPPFileNameGenerator();
        fileRepository = new FileRepositoryImpl();
        ByteArrayOutputStream byteStream = null;
        final ArrayList<Transactions> transactionsList = new ArrayList<Transactions>();

        try {

            if (proposalNumber != null) {
                LifeEngagePayloadAudit lifeEngagePayloadAudit =
                        auditRepository.getLastLifeEngagePayloadAudit(proposalNumber);
                if (lifeEngagePayloadAudit != null) {
                    responseTransactions.setAgentId(lifeEngagePayloadAudit.getKey2());
                    responseTransactions.setProductId(lifeEngagePayloadAudit.getKey3());
                }

                transactionsList.add(responseTransactions);
                PDFRequest<Transactions> pdfRequest = getPDFRequest(transactionsList, type, templateId);
                pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
                pdfComponentImpl.setFileRepository(fileRepository);
                byteStream = pdfComponentImpl.createPdf(pdfRequest);
            }
        }

        catch (Exception e) {
            throw new SystemException(e);
        }
        return byteStream;

    }

     public Transactions getDocumentsForPDF(String proposalNumber, String type) throws IOException {

        Transactions responseTransactions = null;
        if (proposalNumber != null) {
            
            if(Constants.EAPP.equals(type)){
                responseTransactions = eAppRepository.retrieveEApp(proposalNumber, "");
            }
            else if(Constants.ILLUSTRATION.equals(type)){
                responseTransactions = illustrationRepository
                .retrieveIllustrationforPdfGeneration(proposalNumber);
            }
            responseTransactions.setType(type);
            responseTransactions.setKey1(responseTransactions.getProposal().getAgentId());
            responseTransactions = userRepository.validateAgentProfile(responseTransactions);
            if (responseTransactions.getProposal().getRequirements() != null) {
                
                Set<Requirement> agreementRequirementDoc = new HashSet<Requirement>();
                    agreementRequirementDoc =
                        generaliRequirementDocumentFileRepository.getRequirementDocFilesForPDF(responseTransactions,
                                    responseTransactions.getTransTrackingId());
                    String image = null;
                    Set<Document> documents = new HashSet<Document>();

                if (null != agreementRequirementDoc) {
                    String imageByteStream = "";
                    for(Requirement agreementDocuments: agreementRequirementDoc){
                    if (null != agreementDocuments.getRequirementDocuments()) {
                        for (AgreementDocument doc : agreementDocuments.getRequirementDocuments()) {
                            for (Document docContent : doc.getPages()) {

                                if (docContent.getFileNames().contains(".jpg")||docContent.getFileNames().contains(".JPG")) {
                                   /* if (docContent.getBase64string() != null
                                            && docContent.getBase64string().length() > 0) {*/
                                        //to handle the second time hit for signature
                                        if(docContent.getSignatureType()==null ||docContent.getSignatureType()==""){
                                            docContent.setSignatureType(agreementDocuments.getRequirementSubType());
                                        }
                                        documents.add(docContent);
                                        doc.setPages(documents);
                                        imageByteStream = fileOperationsUtil.getFile(docContent.getFileNames(), proposalNumber,true);
                                        doc.setBase64string(imageByteStream);

                                    //}
                                } else if (docContent.getFileContent() != null
                                        && docContent.getFileContent().length() > 0) {
                                    String signCordinates = docContent.getFileContent().replace("\\", "");
                                    BufferedImage buffImage = convertJsonToImageForSpajSign(signCordinates);
                                    if (buffImage != null) {
                                        java.io.ByteArrayOutputStream os = new java.io.ByteArrayOutputStream();
                                        ImageIO.write(buffImage, "jpg", os);
                                        byte[] data = os.toByteArray();
                                        image = Base64Utils.encode(data);
                                        docContent.setSignatureType(agreementDocuments.getRequirementSubType());
                                        docContent.setBase64string(image);
                                        documents.add(docContent);
                                    }
                                }
                            }
                            responseTransactions.setDocument(doc);
                        }
                    }
                    }
                }
            }
    }
        return responseTransactions;
    }

    @Override
    public List<ByteArrayOutputStream> generateMemoPDF(String applicationNumber, boolean isMailPDF) throws BusinessException {
       
        String dateMonthInThai="";
        boolean hasCOF=false;

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        
        InsuranceAgreement insAgrement = new InsuranceAgreement();
        Transactions txn = new Transactions();
        TransactionKeys txnKeys = new TransactionKeys(); 
        txnKeys.setKey21(applicationNumber);
        txn.setKey21(applicationNumber);
        insAgrement.setTransactionKeys(txnKeys);
        String proposalNumber = StringUtils.EMPTY;
        OutputStream outStream = null;
        OutputStream cofOutStream = null;
        FileInputStream fis = null;
        Date memoDateInThaiCalendar = null;
        File coverPdf = new File(fileUploadPath+proposalNumber+File.separator+"coverLetter.pdf");
        File cofPdf = new File(fileUploadPath+proposalNumber+File.separator+"counterOffer.pdf");
        File mergedFile = new File(fileUploadPath+proposalNumber+File.separator+"mergedPDF.pdf");
        try {
            GeneraliTransactions generaliTxn = eAppRepository.retrieveEappTxnByAppNo(insAgrement);
            proposalNumber = generaliTxn.getKey4();
            //generaliTxn = eAppRepository.retrieveEAppWithMemo(proposalNumber, generaliTxn.getTransTrackingId());
            Response<List<AppianMemoDetails>>appianMemoDetails = bpmRepository.retrieveMemoDetails(txn,GeneraliConstants.RETRIEVE);
            generaliTxn.setAppianMemoDetails(appianMemoDetails.getType());
            txn.setKey1(generaliTxn.getProposal().getAgentId());
            txn = userRepository.validateAgentProfile(txn);
            generaliTxn.setGeneraligent(txn.getGeneraligent());
            List<InputStream>  pdfList = new ArrayList<InputStream>();
            SortedSet<Date> sortedDate = new TreeSet<Date>();
            for(AppianMemoDetails memo : appianMemoDetails.getType()){
                final SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
                if(StringUtils.isNotEmpty(memo.getMemoDate())){
                	sortedDate.add(dateFormat.parse(memo.getMemoDate()));
                }
                if(GeneraliConstants.OPEN.equals(memo.getMemoStatus()) || GeneraliConstants.PENDING_UW.equals(memo.getMemoStatus())){
                    String fileName = memo.getMemoDetails().getPdfName();
                    if(memo.getMemoDetails().getMemoCode().equals(GeneraliConstants.COUNTER_MEMO)){
                    	hasCOF=true;
                    }
                    File f = new File(staticPDFPath+fileName+PDF);
                    if(f.exists()){
                        pdfList.add(new FileInputStream(f));
                    }else{
                        if((fileName!= null && !fileName.trim().isEmpty())){
                            throw new BusinessException("static pdf not found");
                        }
                    }
                    
                }
            }
        
            if(!sortedDate.isEmpty()){
	            Date dateInThai = sortedDate.last();
	            Calendar calendar =Calendar.getInstance();
	            calendar.setTime(dateInThai);
	        	calendar.add(Calendar.YEAR, 543);
	        	memoDateInThaiCalendar = calendar.getTime();
	        	dateMonthInThai= GeneraliComponentRepositoryHelper.buildDateInThai(memoDateInThaiCalendar);
            }
            generaliTxn.setDateMonthInThai(dateMonthInThai);
            generaliTxn.setMemoDate(memoDateInThaiCalendar);
            generaliTxn.setKey21(applicationNumber);
            generaliTxn.setType(GeneraliConstants.MEMO);
            
            outStream = new FileOutputStream(coverPdf);
            cofOutStream = new FileOutputStream(cofPdf);
           
        	List<ByteArrayOutputStream> memoDynamicPDFs = new ArrayList<ByteArrayOutputStream>();
        	memoDynamicPDFs.add(generateCommonMemoPDF(generaliTxn));
            memoDynamicPDFs.get(0).writeTo(outStream);
            pdfList.add(0,new FileInputStream(coverPdf));
            
        	if(hasCOF){
        		memoDynamicPDFs.add(generateCounterOfferMemoPDF(generaliTxn));
        		memoDynamicPDFs.get(1).writeTo(cofOutStream);
        		pdfList.add(1,new FileInputStream(cofPdf));
        	}
        	
            if(isMailPDF){
               return memoDynamicPDFs;
            }           
                        
            
            
            OutputStream out = new FileOutputStream(mergedFile);

            doMerge(pdfList, out);
            
            //init array with file length
            byte[] bytesArray = new byte[(int) mergedFile.length()];
            fis = new FileInputStream(mergedFile);
            fis.read(bytesArray); //read file into bytes[]
            byteStream.write(bytesArray,0,bytesArray.length);
        } catch (FileNotFoundException e) {
            throw new BusinessException("PDF creation Failed");
        } catch (IOException e) {
            throw new BusinessException("PDF creation Failed");
        } catch (DocumentException e) {
            throw new BusinessException("PDF creation Failed");
        } catch (ParseException e) {
            throw new BusinessException("PDF creation Failed");
        } finally {
            try {
                if(outStream != null){
                    outStream.close();    
                }
                if(fis != null){
                    fis.close();
                }
            } catch (IOException e) {
                throw new BusinessException("PDF creation Failed unable to close stream");
            }
            if(coverPdf.exists()){
                coverPdf.delete();
            }
            if(mergedFile.exists()){
                mergedFile.delete(); 
            }
        }
            
        return Arrays.asList(byteStream);
    }
    
    /**
     * Merge multiple pdf into one pdf
     * 
     * @param list
     *            of pdf input stream
     * @param outputStream
     *            output file output stream
     * @throws DocumentException
     * @throws IOException
     */
    @Override
    public void doMerge(List<InputStream> list, OutputStream outputStream)
            throws DocumentException, IOException {
        com.lowagie.text.Document document = new com.lowagie.text.Document();
        PdfWriter writer = PdfWriter.getInstance(document, outputStream);
        document.open();
        PdfContentByte cb = writer.getDirectContent();
        
        for (InputStream in : list) {
            PdfReader reader = new PdfReader(in);
            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
                document.newPage();
                //import the page from source pdf
                PdfImportedPage page = writer.getImportedPage(reader, i);
                //add the page to the destination pdf
                cb.addTemplate(page, 0, 0);
            }
        }
        
        outputStream.flush();
        document.close();
        outputStream.close();
    }
    
    public PDFRequest<GeneraliTransactions> getMemoPDFRequest(List<GeneraliTransactions> dataObjcts, String type, String templateId) {
        PDFRequest<GeneraliTransactions> requestObj = new PDFRequestImpl<GeneraliTransactions>();
        Map<String, GeneraliTransactions> dataObjctsBean = new HashMap<String, GeneraliTransactions>();
        String templateType = "";
        if(E_APP.equals(type)){

            templateType = "EAPP";
        }else if(ILLUSTRATION.equals(type)){
            templateType = "ILLUSTRATION";
        } else if (GeneraliConstants.MEMO.equals(type)) {
            templateType = "MEMO";
        }
        dataObjctsBean.put("1", dataObjcts.get(0));
        if (templateId == null || templateId.isEmpty() || "0".equals(templateId)) {
            requestObj.setTemplateID(defaultEAppPDFTemplate);
        } else {
            requestObj.setTemplateID(templateId);
        }
        requestObj.setDataObjsMap(dataObjctsBean);
        requestObj.setTemplateType(templateType);
        return requestObj;
    }
    
    public Map<String,Map<String,String>> setValuesForLookUp(List<String> lookups){
    	
   	 List<CodeLookUp> typeLookUps = null; 
   	 Map<String,Map<String,String>> finalLookUpMap = new HashMap<String,Map<String,String>>();
   	 
   	 for(String type:lookups){
   		 typeLookUps = codeLookUpRepository.fetchLookUpData(type,"th","TH");
   		 Map<String,String> codeValues = new HashMap<String,String>();
   		 for (CodeLookUp test : typeLookUps){
   			 codeValues.put(test.getCode(),test.getDescription());
   		 }
   		 finalLookUpMap.put(type, codeValues);
   	 }
   	return finalLookUpMap;
   }
    /**
     * Password Protected PDF
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public ByteArrayOutputStream generateEncryptedPdf(String proposalNumber, String type, String templateId,
            String masterPassword) {

        PDFComponent<Transactions> pdfComponentImpl;
        FileNameGenerator fileNameGenerator;
        FileRepository fileRepository;
        pdfComponentImpl = new PDFComponentImpl<Transactions>();
        fileNameGenerator = new EAPPFileNameGenerator();
        fileRepository = new FileRepositoryImpl();
        ByteArrayOutputStream byteStream = null;
        final ArrayList<Transactions> transactionsList = new ArrayList<Transactions>();
        String userPassword = null;

        try {

            if (proposalNumber != null) {
                Transactions responseTransactions = getDocumentsForPDF(proposalNumber, type);
                
                Person person = (Person)responseTransactions.getInsured().getPlayerParty();
                
                userPassword = person.getNationalId();

                LifeEngagePayloadAudit lifeEngagePayloadAudit =
                        auditRepository.getLastLifeEngagePayloadAudit(proposalNumber);
                if (lifeEngagePayloadAudit != null) {
                    responseTransactions.setAgentId(lifeEngagePayloadAudit.getKey2());
                    responseTransactions.setProductId(lifeEngagePayloadAudit.getKey3());
                }

                transactionsList.add(responseTransactions);
                PDFRequest<Transactions> pdfRequest = getEncryptedPDFRequest(transactionsList, type, templateId,masterPassword,userPassword);
                pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
                pdfComponentImpl.setFileRepository(fileRepository);
                byteStream = pdfComponentImpl.createEncryptedPdf(pdfRequest);
            }
        }

        catch (Exception e) {
            throw new SystemException(e);
        }
        return byteStream;

    }

    public PDFRequest<Transactions> getEncryptedPDFRequest(List<Transactions> dataObjcts, String type,
            String templateId, String masterPassword, String userPassword) {
        PDFRequest<Transactions> requestObj = new PDFRequestImpl<Transactions>();
        Map<String, Transactions> dataObjctsBean = new HashMap<String, Transactions>();

        LOGGER.info("PDF User Id==>" + userPassword);
        String templateType = "";
        if (E_APP.equals(type)) {

            templateType = "EAPP";
        }
        dataObjctsBean.put("1", dataObjcts.get(0));
        if (templateId == null || templateId.isEmpty() || "0".equals(templateId)) {
            requestObj.setTemplateID(defaultEAppPDFTemplate);
        } else {
            requestObj.setTemplateID(templateId);
        }
        requestObj.setDataObjsMap(dataObjctsBean);
        requestObj.setTemplateType(templateType);
        requestObj.setOwnerPassword(masterPassword);
        if (null != userPassword) {
            requestObj.setUserPassword(userPassword);
        }

        return requestObj;
    }
    
    @Override
    public ByteArrayOutputStream generateCommonMemoPDF(GeneraliTransactions generaliTxn ) {
    	
    	 String templateId = commonMemoTemplateId;
         PDFComponent<GeneraliTransactions> pdfComponentImpl;
         FileNameGenerator fileNameGenerator;
         FileRepository fileRepository;
         pdfComponentImpl = new PDFComponentImpl<GeneraliTransactions>();
         fileNameGenerator = new EAPPFileNameGenerator();
         fileRepository = new FileRepositoryImpl();
         final ArrayList<GeneraliTransactions> generaliTxnList = new ArrayList<GeneraliTransactions>();
         ByteArrayOutputStream coverPDFbyteStream = null;
                  
         List<String> lookups= new ArrayList<String>();
         lookups.add(GeneraliConstants.TITLE);
         lookups.add(GeneraliConstants.NATIONALITY);
         lookups.add(GeneraliConstants.NATUREOFWORK);
         Map<String,Map<String,String>> finalLookUpMap = setValuesForLookUp(lookups);
         generaliTxn.setMapForLookUpValues(finalLookUpMap);
         generaliTxnList.add(generaliTxn);

         PDFRequest<GeneraliTransactions> pdfRequest = getMemoPDFRequest(
                 generaliTxnList, GeneraliConstants.MEMO, templateId);

         pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
         pdfComponentImpl.setFileRepository(fileRepository);

         coverPDFbyteStream = (ByteArrayOutputStream) pdfComponentImpl
                 .createPdf(pdfRequest);
     
         return coverPDFbyteStream;
    }
    
 
         
    @Override
    public ByteArrayOutputStream generateCounterOfferMemoPDF(GeneraliTransactions generaliTxn ) {
    	
   	 String templateId = memoCOFTemplateId;
        PDFComponent<GeneraliTransactions> pdfComponentImpl;
        FileNameGenerator fileNameGenerator;
        FileRepository fileRepository;
        pdfComponentImpl = new PDFComponentImpl<GeneraliTransactions>();
        fileNameGenerator = new EAPPFileNameGenerator();
        fileRepository = new FileRepositoryImpl();
        final ArrayList<GeneraliTransactions> generaliTxnList = new ArrayList<GeneraliTransactions>();
        ByteArrayOutputStream coverPDFbyteStream = null;
        
                  
        generaliTxnList.add(generaliTxn);

        PDFRequest<GeneraliTransactions> pdfRequest = getMemoPDFRequest(
                generaliTxnList, GeneraliConstants.MEMO, templateId);

        pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
        pdfComponentImpl.setFileRepository(fileRepository);

        coverPDFbyteStream = (ByteArrayOutputStream) pdfComponentImpl
                .createPdf(pdfRequest);
    
        return coverPDFbyteStream;
    	
    
    }
    
    
}
