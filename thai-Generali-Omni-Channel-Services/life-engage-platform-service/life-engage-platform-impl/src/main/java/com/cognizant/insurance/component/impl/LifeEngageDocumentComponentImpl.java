/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 20, 2014
 */
package com.cognizant.insurance.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cognizant.icr.pdf.component.PDFComponent;
import com.cognizant.icr.pdf.component.impl.PDFComponentImpl;
import com.cognizant.icr.pdf.file.FileNameGenerator;
import com.cognizant.icr.pdf.file.impl.EAPPFileNameGenerator;
import com.cognizant.icr.pdf.repository.FileRepository;
import com.cognizant.icr.pdf.repository.impl.FileRepositoryImpl;
import com.cognizant.icr.pdf.request.PDFRequest;
import com.cognizant.icr.pdf.request.impl.PDFRequestImpl;
import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.audit.LifeEngagePayloadAudit;
import com.cognizant.insurance.component.LifeEngageDocumentComponent;
import com.cognizant.insurance.component.repository.AgentProfileRepository;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.DocumentRepository;
import com.cognizant.insurance.component.repository.EAppRepository;
import com.cognizant.insurance.component.repository.FNARepository;
import com.cognizant.insurance.component.repository.IllustrationRepository;
import com.cognizant.insurance.component.repository.RequirementDocumentFileRepository;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliAgentProfileRepository;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.services.vo.SignatureLine;
import com.cognizant.insurance.utils.Base64Utils;
import com.google.gson.Gson;



@Service("lifeEngageDocumentComponent")
public class LifeEngageDocumentComponentImpl implements LifeEngageDocumentComponent {

    /** The document upload holder. */
    @Autowired
    @Qualifier("eAppSaveMapping")
    private LifeEngageSmooksHolder saveEAppHolder;
    
    /** The e app repository. */
    @Autowired
    private EAppRepository eAppRepository;
    
    /** The document repository. */
    @Autowired
    private DocumentRepository documentRepository;

    /** The illustration repository. */
    @Autowired
    private IllustrationRepository illustrationRepository;

    /** The fna repository. */
    @Autowired
    private FNARepository fnaRepository;

    /** The agent profile repository. */
    @Autowired
    private GeneraliAgentProfileRepository agentRepository;

    /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;
    
    /** The Constant E_APP. */
    private static final String E_APP = "eApp";
    
    /** The Constant ILLUSTRATION. */
    private static final String ILLUSTRATION = "illustration";

    /** The Constant FNA. */
    private static final String FNA = "FNA";

    /** eApp PDF Default template *. */
    @Value("${le.platform.default.eAppTemplate}")
    private String defaultEAppPDFTemplate;
    
    @Autowired
    RequirementDocumentFileRepository requirementDocumentFileRepository;

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageDocumentComponent#generatePDF_GET(java.lang.String,
     * java.lang.String)
     */
    @Override
    public ByteArrayOutputStream generatePdfGet(String proposalNumber, String type, String templateId) {

        PDFComponent<Transactions> pdfComponentImpl;
        FileNameGenerator fileNameGenerator;
        FileRepository fileRepository;
        pdfComponentImpl = new PDFComponentImpl<Transactions>();
        fileNameGenerator = new EAPPFileNameGenerator();
        fileRepository = new FileRepositoryImpl();
        ByteArrayOutputStream byteStream = null;
        final ArrayList<Transactions> transactionsList = new ArrayList<Transactions>();

        try {

            if (proposalNumber != null) {
                Transactions responseTransactions = eAppRepository.retrieveEApp(proposalNumber, "");
                responseTransactions.setType(type);
                // To include eSignature in eApp pdf
                /*
                 * if (responseTransactions.getProposal().getRelatedDocument() != null) { String documentName = null;
                 * for (AgreementDocument document : responseTransactions.getProposal().getRelatedDocument()) { if
                 * (document.getTypeCode().toString().equals(DocumentTypeCodeList.Signature.toString())) { documentName
                 * = document.getName(); break; } }
                 * 
                 * if (documentName != null) { AgreementDocument agreementDocument = new AgreementDocument();
                 * agreementDocument = documentRepository.getDocument(responseTransactions, documentName, ""); String
                 * image = null; Set<Document> documents = new HashSet<Document>();
                 * 
                 * if (null != agreementDocument) { for (Document doc : agreementDocument.getPages()) { if
                 * (doc.getFileNames().contains(".JPG")) { if (doc.getBase64string() != null &&
                 * doc.getBase64string().length() > 0) { documents.add(doc); } } else if (doc.getFileContent() != null
                 * && doc.getFileContent().length() > 0) { String signCordinates = doc.getFileContent().replace("\\",
                 * ""); BufferedImage buffImage = convertJsonToImage(signCordinates); if (buffImage != null) {
                 * java.io.ByteArrayOutputStream os = new java.io.ByteArrayOutputStream(); ImageIO.write(buffImage,
                 * "jpg", os); byte[] data = os.toByteArray(); image = Base64Utils.encode(data);
                 * doc.setBase64string(image); documents.add(doc); } } } agreementDocument.setPages(documents);
                 * responseTransactions.setDocument(agreementDocument); } } }
                 */
                if (responseTransactions.getProposal().getRequirements() != null) {
                    
                        Requirement agreementRequirementDoc = new Requirement();
                        agreementRequirementDoc =
                                requirementDocumentFileRepository.getRequirementDocFileForPDF(responseTransactions,
                                        responseTransactions.getTransTrackingId());
                        String image = null;
                        Set<Document> documents = new HashSet<Document>();

                    if (null != agreementRequirementDoc) {
                        if (null != agreementRequirementDoc.getRequirementDocuments()) {
                            for (AgreementDocument doc : agreementRequirementDoc.getRequirementDocuments()) {
                                for (Document docContent : doc.getPages()) {

                                    if (docContent.getFileNames().contains(".jpg")) {
                                        if (docContent.getBase64string() != null
                                                && docContent.getBase64string().length() > 0) {
                                            documents.add(docContent);
                                            doc.setPages(documents);
                                            doc.setBase64string(docContent.getBase64string());

                                        }
                                    } else if (docContent.getFileContent() != null
                                            && docContent.getFileContent().length() > 0) {
                                        String signCordinates = docContent.getFileContent().replace("\\", "");
                                        BufferedImage buffImage = convertJsonToImage(signCordinates);
                                        if (buffImage != null) {
                                            java.io.ByteArrayOutputStream os = new java.io.ByteArrayOutputStream();
                                            ImageIO.write(buffImage, "jpg", os);
                                            byte[] data = os.toByteArray();
                                            image = Base64Utils.encode(data);
                                            docContent.setBase64string(image);
                                            documents.add(docContent);
                                        }
                                    }
                                }
                                responseTransactions.setDocument(doc);
                            }
                        }
                    }
                       
                       // agreementRequirementDoc.setRequirementDocuments(requirementDocuments)
                       // agreementRequirementDoc.setPages(documents);
                       


                }

                LifeEngagePayloadAudit lifeEngagePayloadAudit =
                        auditRepository.getLastLifeEngagePayloadAudit(proposalNumber);
                if (lifeEngagePayloadAudit != null) {
                    responseTransactions.setAgentId(lifeEngagePayloadAudit.getKey2());
                    responseTransactions.setProductId(lifeEngagePayloadAudit.getKey3());
                }

                transactionsList.add(responseTransactions);
                PDFRequest<Transactions> pdfRequest = getPDFRequest(transactionsList, type, templateId);
                pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
                pdfComponentImpl.setFileRepository(fileRepository);
                byteStream = pdfComponentImpl.createPdf(pdfRequest);
            }
        }

        catch (Exception e) {
            throw new SystemException(e);
        }
        return byteStream;

    }
    /**
	 * Generate illustration pdf.
	 *
	 * @param proposalnumber
					 the proposal number
	 * @param type 
					the type
	 * @param templateId
					 the product code
	 * @return the byte array output stream
	 */
    @Override
    public ByteArrayOutputStream generateIllustrationPdf(String proposalNumber, String type, String templateId) {
    	
    	PDFComponent<Transactions> pdfComponentImpl;
        FileNameGenerator fileNameGenerator;
        FileRepository fileRepository;
        pdfComponentImpl = new PDFComponentImpl<Transactions>();
        fileNameGenerator = new EAPPFileNameGenerator();
        fileRepository = new FileRepositoryImpl();
        ByteArrayOutputStream byteStream = null;
        final ArrayList<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions responseTransactions = new Transactions();
      
        try {
	        if (proposalNumber != null) {
	            responseTransactions = illustrationRepository.retrieveIllustrationforPdfGeneration(proposalNumber);
	        }
	        responseTransactions.setType(type);
	        //To include eSignature in illustration pdf
	        if(responseTransactions.getProposal().getRelatedDocument() != null){
          	  String documentName=null;
	              for(AgreementDocument document :  responseTransactions.getProposal().getRelatedDocument()){
	            	  if( document.getTypeCode().toString().equals(DocumentTypeCodeList.Signature.toString())){
	            		  documentName =  document.getName();
	            		  break;
	            	  }
	              }
	              
	              if( documentName != null ){
	            	  AgreementDocument agreementDocument = new AgreementDocument();
	            	  agreementDocument = documentRepository.getDocument(responseTransactions, documentName, "");
	            	  String image = null;
	            	  Set <Document> documents = new HashSet<Document>();
	            	  
	            	  if(null != agreementDocument){
	    	              for(Document doc : agreementDocument.getPages()){
                            if (doc.getFileNames().contains(".JPG")) {

                                if (doc.getBase64string() != null && doc.getBase64string().length() > 0) {
                                    documents.add(doc);
                                }
                            } else if (doc.getFileContent() != null && doc.getFileContent().length() > 0) {
	    		            	  String signCordinates = doc.getFileContent().replace("\\", "");
	    		            	  BufferedImage buffImage= convertJsonToImage(signCordinates);
	    		            	    if (buffImage != null) {
	    		            	          java.io.ByteArrayOutputStream os = new 
	    		            	                      java.io.ByteArrayOutputStream();
	    		            	          ImageIO.write(buffImage, "jpg", os);
	    		            	          byte[] data = os.toByteArray();
	    		            	          image = Base64Utils.encode(data);
	    		            	          doc.setBase64string(image);
	    		            	          documents.add(doc);
	    		            	    }
	    		            	    
	    	            	  }
	    	              }
	    	              agreementDocument.setPages(documents);
	    	              responseTransactions.setDocument(agreementDocument);
	                  }
	               }
	            }
	        transactionsList.add(responseTransactions);
	    	
	        PDFRequest<Transactions> pdfRequest = getPDFRequest(transactionsList, type,templateId);
	
	        pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
	        pdfComponentImpl.setFileRepository(fileRepository);
	
	        byteStream = (ByteArrayOutputStream) pdfComponentImpl.createPdf(pdfRequest);

        } catch (IOException e) {
            throwSystemException(true, e.getMessage());
			
		}
        
        return byteStream;
        
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageDocumentComponent#generateFNAReport(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public ByteArrayOutputStream generateFNAReport(final String proposalNumber, final String type,
            final String templateId) {

        PDFComponent<Transactions> pdfComponentImpl = new PDFComponentImpl<Transactions>();
        FileNameGenerator fileNameGenerator = new EAPPFileNameGenerator();
        FileRepository fileRepository = new FileRepositoryImpl();
        ByteArrayOutputStream byteStream = null;
        final ArrayList<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions responseTransactions = new Transactions();
        RequestInfo requestInfo = new RequestInfo();
        GeneraliAgent agentDetails = new GeneraliAgent();

        try {
            if (proposalNumber != null) {
                responseTransactions = fnaRepository.retrieveFNAforPdfGeneration(proposalNumber, requestInfo);
                agentDetails =
                		agentRepository.retrieveAgentODSDetailsByCode(requestInfo,
                                responseTransactions.getAgentId());
                responseTransactions.setGeneraligent(agentDetails);

            }
            responseTransactions.setType(type);

            transactionsList.add(responseTransactions);
            PDFRequest<Transactions> pdfRequest = getPDFRequest(transactionsList, type, templateId);
            pdfComponentImpl.setFileNameGenerator(fileNameGenerator);
            pdfComponentImpl.setFileRepository(fileRepository);
            byteStream =  pdfComponentImpl.createPdf(pdfRequest);

        } catch (Exception e) {
            throw new SystemException(e);
        }
        return byteStream;

    }
    
    /**
     * Gets the pDF request.
     * 
     * @param dataObjcts
     *            the data objcts
     * @return the pDF request
     */
    public PDFRequest<Transactions> getPDFRequest(List<Transactions> dataObjcts,String type, String templateId) {
        PDFRequest<Transactions> requestObj = new PDFRequestImpl<Transactions>();
        Map<String, Transactions> dataObjctsBean = new HashMap<String, Transactions>();
        String templateType = "";
        if(E_APP.equals(type)){

        	templateType = "EAPP";
        }else if(ILLUSTRATION.equals(type)){
        	templateType = "ILLUSTRATION";
        } else if (FNA.equals(type)) {
            templateType = "FNA";
        }
        dataObjctsBean.put("1", dataObjcts.get(0));
        if (templateId == null || templateId.isEmpty() || "0".equals(templateId)) {
            requestObj.setTemplateID(defaultEAppPDFTemplate);
        } else {
            requestObj.setTemplateID(templateId);
        }
        requestObj.setDataObjsMap(dataObjctsBean);
        requestObj.setTemplateType(templateType);
        return requestObj;
    }
    
    /**
     * Gets the document.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param documentName
     *            the document name
     * @param transaction
     *            the transaction
     * @return the document
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public  AgreementDocument getDocument(final Transactions transaction, final String documentName,
    		final String transactionId) throws IOException {
    	AgreementDocument agreementDocument =
         		documentRepository.getDocument(transaction, documentName, transactionId);
        return agreementDocument;
    }

    /**
     * Upload documents.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param transaction
     *            the transaction
     * @param document
     *            the document
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws BusinessException
     *             the business exception
     */
    public  void uploadDocuments(final Transactions transaction, final String transactionId,
            final AgreementDocument document) throws IOException, BusinessException {
    	documentRepository.uploadDocuments(transaction, transactionId,
    			document);
    }
	/**
	 * @return the saveEAppHolder
	 */
	public LifeEngageSmooksHolder getSaveEAppHolder() {
		return saveEAppHolder;
	}

	/**
	 * @param saveEAppHolder the saveEAppHolder to set
	 */
	public void setSaveEAppHolder(LifeEngageSmooksHolder saveEAppHolder) {
		this.saveEAppHolder = saveEAppHolder;
	}
	
    public static BufferedImage convertJsonToImage(String jsonString){
        Gson gson = new Gson();
        SignatureLine[] signatureLines = gson.fromJson(jsonString, SignatureLine[].class);
        BufferedImage offscreenImage = new BufferedImage(478, 160, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = offscreenImage.createGraphics();
        //g2.setFont(new Font("Serif", Font.PLAIN, 10));
        g2.setStroke(new BasicStroke(6));        
        g2.setColor(Color.white);
        g2.fillRect(0,0,478,160);
        g2.setPaint(Color.black);
        for (SignatureLine line : signatureLines ) {
            g2.drawLine(line.getLx(), line.getLy(), line.getMx(), line.getMy());
        }
        return offscreenImage;
    }
}