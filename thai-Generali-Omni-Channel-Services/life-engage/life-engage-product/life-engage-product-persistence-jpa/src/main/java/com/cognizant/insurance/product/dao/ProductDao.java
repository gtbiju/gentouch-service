/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270895
 * @version       : 0.1, Nov 20, 2012
 */
package com.cognizant.insurance.product.dao;

import java.util.List;

import com.cognizant.insurance.dao.Dao;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;

/**
 * The Interface interface ProductDao.
 */
public interface ProductDao extends Dao {

    /**
     * Gets the products.
     * 
     * @param productRequest
     *            the product request
     * @return the products
     */
    Response<List<ProductSpecification>> getProducts(Request<ProductSearchCriteria> productRequest);

    /**
     * Gets the active products.
     * 
     * @param productRequest
     *            the product request
     * @return the active products
     */
    Response<List<ProductSpecification>> getActiveProducts(Request<ProductSearchCriteria> productRequest);

    /**
     * Gets the product. Using product code and carrier Id for selection
     * 
     * @param criteriaRequest
     *            the criteria request
     * @return the product
     */
    ProductSpecification getProduct(Request<ProductSearchCriteria> criteriaRequest, final String version);

    /**
     * Gets the product by id.
     * 
     * @param request
     *            the request
     * @return the product by id
     */
    ProductSpecification getProductById(Request<ProductSearchCriteria> criteriaRequest, final String version);

    /**
     * Used to check whether a product is active or not
     *
     * @param criteriaRequest
     * @return
     */
    Boolean isProductActive(Request<ProductSearchCriteria> criteriaRequest, final String version);
    
    /**
     * Gets all products by filter : carrierId & ChannelId.
     * 
     * @param productRequest
     *            the product request
     * @return the products
     */
    Response<List<ProductSpecification>> getAllProductsByFilter(Request<ProductSearchCriteria> productRequest, final String version);

    /**
     * Gets the active products by filter : carrierId & ChannelId.
     * 
     * @param productRequest
     *            the product request
     * @return the active products
     */
    Response<List<ProductSpecification>> getActiveProductsByFilter(Request<ProductSearchCriteria> productRequest, final String version);

   
    /**
     * Gets the products by filter.
     * 
     * @param criteriaRequest
     *            the criteria request
     * @return the products by filter
     */
    Response<List<ProductSpecification>> getProductsByFilter(final Request<ProductSearchCriteria> criteriaRequest, final String version);
    /**
     * Gets the product templates by filter.
     * 
     * @param criteriaRequest
     *            the criteria request
     * @return the product templates by filter
     */
    Response<List<ProductTemplateMapping>> getProductTemplatesByFilter(final Request<ProductSearchCriteria> criteriaRequest);

	/**
	 * Gets the active products by filter v2.
	 *
	 * @param productRequest the product request
	 * @return the active products by filter v2
	 */
	Response<List<ProductSpecification>> getActiveProductsByFilterV2(
			Request<ProductSearchCriteria> productRequest);

	/**
	 * Gets the all products by filter v2.
	 *
	 * @param productRequest the product request
	 * @return the all products by filter v2
	 */
	Response<List<ProductSpecification>> getAllProductsByFilterV2(
			Request<ProductSearchCriteria> productRequest);

	/**
	 * Checks if is product active v2.
	 *
	 * @param criteriaRequest the criteria request
	 * @return the boolean
	 */
	Boolean isProductActiveV2(Request<ProductSearchCriteria> criteriaRequest);


	/**
	 * Gets the product v2.
	 *
	 * @param criteriaRequest the criteria request
	 * @return the product v2
	 */
	ProductSpecification getProductV2(
			Request<ProductSearchCriteria> criteriaRequest);


	/**
	 * Gets the product by id v2.
	 *
	 * @param criteriaRequest the criteria request
	 * @return the product by id v2
	 */
	ProductSpecification getProductByIdV2(
			Request<ProductSearchCriteria> criteriaRequest);



}
