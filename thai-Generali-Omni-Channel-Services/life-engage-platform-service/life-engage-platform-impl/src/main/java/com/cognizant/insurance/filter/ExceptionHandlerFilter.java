package com.cognizant.insurance.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.service.helper.LifeEngageValidationHelper;

public class ExceptionHandlerFilter extends OncePerRequestFilter {

	@Override
	public void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			filterChain.doFilter(request, response);
		} catch (RuntimeException e) {
			StatusData statusData= new StatusData(); 
			statusData.setStatus(LifeEngageValidationHelper.FAILURE);
			statusData.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			statusData.setStatusMessage(e.getMessage());
			response.getWriter().write(convertObjectToJson(statusData));
		}
	}

	public String convertObjectToJson(Object object) throws IOException {
		if (object == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}
}