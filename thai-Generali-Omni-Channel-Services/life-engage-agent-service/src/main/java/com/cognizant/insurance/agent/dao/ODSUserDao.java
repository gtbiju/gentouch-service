package com.cognizant.insurance.agent.dao;

import java.util.List;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.generali.security.entity.PasswordHistory;

public interface ODSUserDao extends Dao {
	
	public Response<GeneraliAgent> validateAgentProfile(Request<Transactions> request);
	
	public Response<GeneraliTHUser> validateLoggedInUser(Request<Transactions> request);
	
	public Response<List<PasswordHistory>> getRecentPasswords(Request<String> userRequest, Request<Integer> historyRequest);

	public Response<PasswordHistory> getOldPasswordHistory(Request<String> userRequest);
}
