/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.agreementcodelists;

/**
 * Contract Premium Nature.
 * 
 * @author 301350
 * 
 * 
 */
public enum PremiumNatureCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    InitialPremium,
    /**
     * 
     * 
     * 
     * 
     */
    SubsequentPremium,
    /**
     * Reinstatement premium (RIP) is a prorated insurance or reinsurance
     * premium charged for the reinstatement of the amount of a primary policy
     * or reinsurance coverage limit that has been reduced or exhausted by loss
     * payments under such coverages. This additional premium is charged under
     * certain excess of loss reinsurance contracts to restore coverage amounts
     * after a loss.
     * 
     * 
     * 
     */
    ReinstatementPremium,
    /**
     * A deposit premium is the amount of premium (usually for an Excess of loss
     * contract) that the Ceding company pays to the reinsurer on a periodic
     * basis during the term of the contract. This amount is generally
     * determined as a Float of the estimated amount of premium which the
     * contract will produce based on the rate and estimated subject premium. It
     * is often the same as the minimum premium but may be higher or lower. The
     * deposit premium will be adjusted to the higher of the actual developed
     * premium or the minimum premium after the actual subject premium has been
     * determined.
     * 
     * This occurs in cases where all relevant exposure or rating information
     * are not known at the start of the period of cover, or the premium to be
     * paid is dependent on the claims experience during the policy term. An
     * initial premium is paid at the start of the period of cover, followed by
     * an adjustment at the end when the information required is known.
     * 
     * Where this latter adjustment is stipulated at the outset as being upwards
     * only, the term "Minimum and Deposit Premium" applies.
     * 
     * Where it is found in cases relating to retrospective experience rating,
     * the term "swing rated premium" is often applied.
     * 
     * 
     * 
     */
    DepostPremium,
    /**
     * An amount of premium which will be charged (usually for an excess of loss
     * reinsurance contract), not withstanding that the actual premium developed
     * by applying the rate to the subject premium could have produced a lower
     * figure. Compare deposit premium. 
     * 
     * 
     * 
     */
    MinimumPremium,
    /**
     * Float of premiums paid by the reinsurer to the insurer in quota-share or
     * Facultative treaties as a contribution to the acquisition and
     * administrative costs relating to the business ceded.
     * 
     * 
     * 
     * 
     */
    ReinsurancePremium,
    
    /***
     * Added for Generali- Gemilang Product
     * Premium paid by insured at once
     * 
     * 
     */
  
    SingleBasicPremium,
    
    /* Added for Generali- Gemilang Product
    * Insurance Cost paid by policy Holder
    * 
    * 
    */
    
    InsuranceCostPerYear,
    
    /* Added for Generali- Gemilang Product
     * 
     * 
     * 
     */
   
    TotalInsuranceCost,
    
    MinProtection,
    ProtectionUsed,
    MinimumUsedPremium,
    RegularTopUpPremium,
    SingleTopUpPremium,
    RenewalPremium,
    RegularPremium,
    TotalInitialPremium,
    TotalPremium
   
    
}
