package com.cognizant.insurance.omnichannel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.agent.AgentAchievement;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliAgentAddress;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.generic.dao.impl.AgentDaoImpl;
import com.cognizant.insurance.omnichannel.dao.GeneraliAgentDao;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;

public class GeneraliAgentDaoImpl extends AgentDaoImpl implements GeneraliAgentDao {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = LoggerFactory
			.getLogger(GeneraliAgentDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cognizant.insurance.generic.dao.AgentDao#retrieveAgentByCode(com.
	 * cognizant.insurance.request.Request)
	 */
	@Override
	public GeneraliAgent retrieveAgentProfileByCode(Request<GeneraliAgent> request) {
		GeneraliAgent retrievedAgent = null;
		GeneraliAgent inpAgent = request.getType();
		String query = "SELECT agent FROM GeneraliAgent agent WHERE agent.agentCode = ?1";
		List<GeneraliAgent> agentsList = findByQuery(request, query,inpAgent.getAgentCode());
		if (agentsList.size() > 0) {
			retrievedAgent = agentsList.get(0);
		}
		return retrievedAgent;
	}

   /* @Override
    public List<GeneraliAgentProduct> retrieveAgentProductByCode(Request<List<String>> request) {
        List<String> inpAgent = request.getType();
        String query = "SELECT agent FROM GeneraliAgentProduct agent WHERE agent.productType in (?1)";
        List<GeneraliAgentProduct> agentsList = findByQuery(request, query,inpAgent.get(0));
        return agentsList;
    }*/
    
    @Override
    public List<ProductSpecification> retrieveAgentProductByCode(Request<ProductSpecification> request) {
        //List<ProductSpecification> inpAgent = request.getType();
        String query = "SELECT product FROM ProductSpecification product where product.productGroup='51001' and product.productType='812' and product.versionStatus='Approved'" ;
        List<ProductSpecification> agentsList = findByQuery(request, query);
        return agentsList;
    }
    
	@Override
	public List<GeneraliAgent> retrieveAgentProfileForReassign(Request<GeneraliAgent> request) {
		GeneraliAgent retrievedAgent = null;
		GeneraliAgent inpAgent = request.getType();
		String query = "SELECT agent FROM GeneraliAgent agent WHERE agent.smCode = ?1 or agent.gmCode = ?1 order by agent.agentCode";
		List<GeneraliAgent> agentsList = findByQuery(request, query,inpAgent.getAgentCode());
		
		return agentsList;
	}
	
	@Override
	public List<GeneraliAgent> retrieveActiveAgentProfileToReassign(Request<GeneraliAgent> request) {		
		GeneraliAgent inpAgent = request.getType();
		String query = "SELECT agent FROM GeneraliAgent agent WHERE agent.smCode = (SELECT agent.smCode FROM GeneraliAgent agent WHERE agent.agentCode = ?1)";
		List<GeneraliAgent> agentsList = findByQuery(request, query,inpAgent.getAgentCode());		
		
		return agentsList;
	}
	
	@Override
	public List<Customer> retrieveLeads(Request<Customer> request) {
		Customer retrievedAgent = null;
		Customer inpAgent = request.getType();
		String query = "SELECT customer FROM Customer customer WHERE customer.agentId = ?1 and customer.statusCode = 20 order by customer.agentId";
		List<Customer> agentsList = findByQuery(request, query,inpAgent.getAgentId());
		
		return agentsList;
	}

	
	@Override
	public Integer retrieveIllustrationCount(Request<Customer> request) {
		Customer inpAgent = request.getType();
		String queryStr = "SELECT count(agr) from Agreement agr where agr.transactionKeys.key1 =?1 and agr.statusCode in ('12') and agr.contextId = '2' and agr.transTrackingId in" + 
				" (select insAgr.transactionKeys.key3 from InsuranceAgreement insAgr where insAgr.statusCode = '21' and insAgr.contextId = '4' and insAgr.transactionKeys.key1=?1)";
		final JPARequestImpl<Customer> jpaRequestImpl = (JPARequestImpl<Customer>) request;
		final Query query = jpaRequestImpl.getEntityManager().createQuery(queryStr);
		query.setParameter(1, inpAgent.getTransTrackingId());
		List agentsList = query.getResultList();
		Long leadCount = agentsList.size() > 0 ? (Long)agentsList.get(0): new Long(0);
		Integer agentsListCount = (int)leadCount.intValue();
		return agentsListCount;
	}
	
	@Override
	public Integer retrieveEappCount(Request<Customer> request) {
		Customer inpAgent = request.getType();
		String queryStr = "SELECT count(agr) from Agreement agr where agr.transactionKeys.key1 =?1 and agr.statusCode = '21' and agr.contextId = '4'"; 
		final JPARequestImpl<Customer> jpaRequestImpl = (JPARequestImpl<Customer>) request;
		final Query query = jpaRequestImpl.getEntityManager().createQuery(queryStr);
		query.setParameter(1, inpAgent.getTransTrackingId());
		List agentsList = query.getResultList();
		Long leadCount = agentsList.size() > 0 ? (Long)agentsList.get(0): new Long(0);
		Integer agentsListCount = (int)leadCount.intValue();
		return agentsListCount;
	}
	
	@Override
	public Integer retrieveFnaCount(Request<Customer> request) {
		Customer inpAgent = request.getType();
		String queryStr = "select count(goalAndNeed) from GoalAndNeed goalAndNeed where goalAndNeed.transactionKeys.key1 =?1 "
                + "and goalAndNeed.contextId = '0' and goalAndNeed.status = 'Completed'";
		final JPARequestImpl<Customer> jpaRequestImpl = (JPARequestImpl<Customer>) request;
		final Query query = jpaRequestImpl.getEntityManager().createQuery(queryStr);
		query.setParameter(1, inpAgent.getTransTrackingId());
		List agentsList = query.getResultList();
		Long leadCount = agentsList.size() > 0 ? (Long)agentsList.get(0): new Long(0);
		Integer agentsListCount = (int)leadCount.intValue();
		return agentsListCount;
	}
	
	@Override
	public String retrieveLeadIdForFna(Request<Customer> request) {
		Customer inpAgent = request.getType();
		String leadId = StringUtils.EMPTY;
		String queryStr = "select transTrackingId from PartyRoleInRelationship where identifier = ?1";
		List<Object> agentsList = findByNativeQuery(request, queryStr,inpAgent.getIdentifier());
		
		if (CollectionUtils.isNotEmpty(agentsList)) {
            for (Object customerObj : agentsList) {
            	leadId = customerObj.toString();
            }
		}
		return leadId;
	}
	
	@Override
	public List<GoalAndNeed> retrieveGoalAndNeed(Request<GoalAndNeed> request) {
		GoalAndNeed inpAgent = request.getType();
		String queryStr = "select fna from GoalAndNeed fna where fna.leadId = ?1 and (fna.status = ?2 or fna.status = ?3)";
		List<GoalAndNeed> agentsList = findByQuery(request, queryStr, inpAgent.getLeadId(),
				AgreementStatusCodeList.Completed.toString(), AgreementStatusCodeList.ReportSent.toString());
		return agentsList;
	}
	
	@Override
	public List<Agreement> retrieveAgreement(Request<InsuranceAgreement> request) {
		InsuranceAgreement inpAgr = request.getType();
		//String queryStr = "select insagr from InsuranceAgreement insagr where fna.leadId = ?1";
		
		String queryStr = "select insAgr from InsuranceAgreement insAgr where insAgr.transactionKeys.key1 = ?1 and insAgr.contextId = '4' and insAgr.statusCode = '21' order by id";
		
		List<InsuranceAgreement> agentsList = findByQuery(request, queryStr,inpAgr.getTransactionKeys().getKey28());
		List<Agreement> result = new ArrayList<Agreement>();
		
		for(InsuranceAgreement insAgr : agentsList){
			Agreement agr = (Agreement)insAgr;
			result.add(agr);
		}
		
		return result;
	}
	
	@Override
	public List<AgentAchievement> retrieveAgentAchievements(Request<AgentAchievement> agentAchievementRequest) {
		AgentAchievement agentAchievemen = agentAchievementRequest.getType();
		String query = "SELECT agent FROM AgentAchievement agent WHERE agent.agentCode = ?1";
		List<AgentAchievement> agentsList = findByQuery(agentAchievementRequest, query,agentAchievemen.getAgentCode());
	        return agentsList;
	}
	
	@Override
	public List<InsuranceAgreement> retrieveIllustration(Request<InsuranceAgreement> request) {
		InsuranceAgreement inpAgr = request.getType();
		//String queryStr = "select insagr from InsuranceAgreement insagr where fna.leadId = ?1";
		
		String queryStr = "select iInsAgr from InsuranceAgreement iInsAgr where iInsAgr.contextId = '2' and iInsAgr.statusCode = '12' and iInsAgr.transTrackingId in" + 
				" (select insAgr.transactionKeys.key3 from InsuranceAgreement insAgr where insAgr.statusCode = '21' and insAgr.contextId = '4' and insAgr.transactionKeys.key1=?1) order by iInsAgr.id";
		
		List<InsuranceAgreement> agreementList = findByQuery(request, queryStr,inpAgr.getTransactionKeys().getKey28());
		
		return agreementList;
	}
	@Override
	public GeneraliGAOAgent retrieveGAOAgentProfileByCode(Request<GeneraliGAOAgent> request) {
		GeneraliGAOAgent retrievedAgent = null;
		GeneraliGAOAgent inpAgent = request.getType();
		String query = "SELECT agent FROM GeneraliGAOAgent agent WHERE agent.agentCode = ?1";
		List<GeneraliGAOAgent> agentsList = findByQuery(request, query,inpAgent.getAgentCode());
		if (agentsList.size() > 0) {
			retrievedAgent = agentsList.get(0);
		}
		return retrievedAgent;
	}
	
	@Override
	public GeneraliAgentAddress retrieveGAOAddress(Request<GeneraliAgentAddress> request) {
		GeneraliAgentAddress retrievedAddress = null;
		GeneraliAgentAddress inpAgent = request.getType();
		String query = "SELECT address FROM GeneraliAgentAddress address WHERE address.code = ?1";
		List<GeneraliAgentAddress> agentsList = findByQuery(request, query,inpAgent.getCode());
		if (agentsList.size() > 0) {
			retrievedAddress = agentsList.get(0);
		}
		return retrievedAddress;
	}

}
