/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Enables to specify a reinsurer's deposit nature. 
 * 
 * @author 301350
 * 
 * 
 */
public enum DepositNatureCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Cash,
    /**
     * 
     * 
     * 
     * 
     */
    Stock,
    /**
     * 
     * 
     * 
     * 
     */
    Other
}
