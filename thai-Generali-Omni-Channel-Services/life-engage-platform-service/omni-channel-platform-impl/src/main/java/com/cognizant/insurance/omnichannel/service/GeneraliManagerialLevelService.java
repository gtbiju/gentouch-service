package com.cognizant.insurance.omnichannel.service;

import com.cognizant.insurance.core.exception.BusinessException;

public interface GeneraliManagerialLevelService {

	String retrieveByManagerialLevel(String json) throws BusinessException;

}
