/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Aug 26, 2015
 */
package com.cognizant.insurance.response;

import java.io.Serializable;

/**
 * The Class class ResponseCount.
 */
public class ResponseCount implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -127477911741190446L;

   
    /** The fnaCount. */
    private String fnaCount;
    
    /** The illustrationCount. */
    private String illustrationCount;
    
    /** The eAppCount. */
    private String eAppCount;

    public String getFnaCount() {
        return fnaCount;
    }

    public void setFnaCount(String fnaCount) {
        this.fnaCount = fnaCount;
    }

    public String getIllustrationCount() {
        return illustrationCount;
    }

    public void setIllustrationCount(String illustrationCount) {
        this.illustrationCount = illustrationCount;
    }

    public String geteAppCount() {
        return eAppCount;
    }

    public void seteAppCount(String eAppCount) {
        this.eAppCount = eAppCount;
    }


   


    
}