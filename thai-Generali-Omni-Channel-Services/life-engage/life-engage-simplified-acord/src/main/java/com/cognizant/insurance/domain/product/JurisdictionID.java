/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 420118
 * @version       : 0.1, Dec 20, 2017
 */

package com.cognizant.insurance.domain.product;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

// TODO: Auto-generated Javadoc
/**
 * The Class JurisdictionID.
 */
@Embeddable
public class JurisdictionID implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5568079370335632727L;

	
	/** The code. */
	@Column(name="JURISDICTION_CODE", nullable = false)
	private String code;
	
	/** The carrier. */
    @ManyToOne(cascade = { CascadeType.REFRESH})
    @JoinColumn(name ="JURISDICTION_CARRIER" , nullable= false)
	private Carrier carrier;

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the carrier.
	 *
	 * @return the carrier
	 */
	public Carrier getCarrier() {
		return carrier;
	}

	/**
	 * Sets the carrier.
	 *
	 * @param carrier the new carrier
	 */
	public void setCarrier(Carrier carrier) {
		this.carrier = carrier;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof JurisdictionID){
			if(((JurisdictionID)obj).getCode().equals(this.getCode()) && ((JurisdictionID)obj).getCarrier().getId().equals(this.getCarrier())){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.getCode().length() * 13;
	}

}
