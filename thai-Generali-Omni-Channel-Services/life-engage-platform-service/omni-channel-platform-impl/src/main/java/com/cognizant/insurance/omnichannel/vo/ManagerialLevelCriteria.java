package com.cognizant.insurance.omnichannel.vo;

import java.util.List;

public class ManagerialLevelCriteria {

	private String command;
	
	private List<ManagerialLevelUserDetails> managerialLevelUserDetailsList;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public List<ManagerialLevelUserDetails> getManagerialLevelUserDetailsList() {
		return managerialLevelUserDetailsList;
	}

	public void setManagerialLevelUserDetailsList(
			List<ManagerialLevelUserDetails> managerialLevelUserDetailsList) {
		this.managerialLevelUserDetailsList = managerialLevelUserDetailsList;
	}
	
	
}
