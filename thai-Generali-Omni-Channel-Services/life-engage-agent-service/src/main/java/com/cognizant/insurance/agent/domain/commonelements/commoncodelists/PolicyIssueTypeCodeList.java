/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * @author 291433
 * 
 */
public enum PolicyIssueTypeCodeList {

    /**
     * Underwriting rules used to issue the coverage are specific to those
     * required to issue aviation based life insurance.
     */
    Aviation,
    /**
     * (Policy has Underwritten and Guaranteed Issue coverages.) Only applies at
     * Policy Level.
     */
    Blended,
    /**
     * No underwriting done.
     */
    Conversion_No_UnderwritingDone,
    /**
	 * 
	 */
    Conversion_Non_Contractual,
    /**
	 * 
	 */
    FieldIssue,
    /**
	 * 
	 */
    FinancialUnderwriting,
    /**
     * Answering all of the medical questions in the policy.
     */
    FullUnderwriting,
    /**
     * A type of supplementary benefit rider that gives the policyowner the
     * right to purchase additional insurance of the same type as the original
     * policy on specified dates for specified amounts without supplying
     * additional evidence of insurability.
     */
    GuaranteedIssue,
    /**
	 * 
	 */
    MassUnderwriting,
    /**
	 * 
	 */
    ReducedUnderwriting,
    /**
     * Answering slightly different and less medical type questions as compared
     * to Full Underwriting.
     */
    SimplifiedUnderwriting,
    /**
	 * 
	 */
    Other,
    /**
	 * 
	 */
    Unknown,
}
