/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.familymembersubtypes;

import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.FamilyMember;

/**
 * This concept represents a parent.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Parent extends FamilyMember {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8045665153233232397L;
}
