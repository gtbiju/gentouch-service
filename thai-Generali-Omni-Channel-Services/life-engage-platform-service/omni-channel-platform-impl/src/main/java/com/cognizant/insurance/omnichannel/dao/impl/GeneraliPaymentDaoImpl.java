package com.cognizant.insurance.omnichannel.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.omnichannel.dao.GeneraliPaymentDao;
import com.cognizant.insurance.omnichannel.domain.TinyUrlGenerator;
import com.cognizant.insurance.request.Request;

@Repository
public class GeneraliPaymentDaoImpl extends DaoImpl implements GeneraliPaymentDao{
	
	@Override
	public Long retrieveIdForPaymentUrl(Request<TinyUrlGenerator> searchCriteriatRequest) {
		String query;
		query = "select url from TinyUrlGenerator url where url.applicationNumber = ?1 order by "
				+ "url.creationDateTime desc";
		List<TinyUrlGenerator> searchCriteriaList = null;
		TinyUrlGenerator result = null;
		if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
			searchCriteriaList = findByQuery(searchCriteriatRequest, query,
					searchCriteriatRequest.getType().getApplicationNumber());
			if(CollectionUtils.isNotEmpty(searchCriteriaList)){
				result = (TinyUrlGenerator) searchCriteriaList.get(0);
			}

		}
		return result.getId();
	}

	@Override
	public TinyUrlGenerator retrieveOriginalUrlForPayment(Request<TinyUrlGenerator> searchCriteriatRequest) {
		String query;
		query = "select url from TinyUrlGenerator url where url.UUID = ?1 and url.id = ?2";
		List<TinyUrlGenerator> searchCriteriaList = null;
		TinyUrlGenerator result = null;
		
		if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
			searchCriteriaList = findByQuery(searchCriteriatRequest, query,
					searchCriteriatRequest.getType().getUUID(),
					searchCriteriatRequest.getType().getId());
			if(CollectionUtils.isNotEmpty(searchCriteriaList)){
				result = (TinyUrlGenerator) searchCriteriaList.get(0);
			}

		}
		return result;
	}

}
