package com.cognizant.insurance.omnichannel.component.repository;

import java.sql.SQLDataException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.agreement.dao.AgreementDao;
import com.cognizant.insurance.agreement.dao.impl.AgreementDaoImpl;
import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.EAppRepository;
import com.cognizant.insurance.component.repository.helper.LifeEngageComponentRepositoryHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.RiderSequence;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Appointee;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.codes.CodeLookUp;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.goalandneed.dao.GoalAndNeedDao;
import com.cognizant.insurance.goalandneed.dao.impl.GoalAndNeedDaoImpl;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliAgreementComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliPartyComponent;
import com.cognizant.insurance.omnichannel.component.LifeAsiaComponent;
import com.cognizant.insurance.omnichannel.component.UserSelectionComponent;
import com.cognizant.insurance.omnichannel.component.repository.helper.GeneraliComponentRepositoryHelper;
import com.cognizant.insurance.omnichannel.dao.GeneraliAgreementDao;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliAgreementDaoImpl;
import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.omnichannel.utils.GeneraliDateUtil;
import com.cognizant.insurance.omnichannel.vo.GeneraliTransactions;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;
import com.cognizant.insurance.service.repository.CodeLookUpRepository;

/**
 * @author 444873
 * 
 */

public class GeneraliEAppRepository extends EAppRepository {

    /** The agreement component. */
    @Autowired
    private GeneraliAgreementComponent agreementComponent;
    
    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;
    
    /** The Constant LMS. */
    private static final String EAPP = "EAPP";
    
    @Autowired
    private UserSelectionComponent userSelectionComponent;

    /** The party component. */
    @Autowired
    private GeneraliPartyComponent partyComponent;

    @Autowired
    private GeneraliComponentRepositoryHelper generaliComponentRepositoryHelper;
    
    @Autowired
    private LifeAsiaComponent lifeAsiaComponent;
    
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;
    
    /** The look up repository. */
    @Autowired
    private CodeLookUpRepository codeLookUpRepository;
    
    /** The source of truth. */
    @Value("${le.platform.service.sourceOfTruth}")
    private String sourceOfTruth;
    
    /** The conflict resolution. */
    @Value("${le.platform.service.conflictResolution}")
    private String conflictResolution;
    
    @Value("${le.platform.service.fileUploadPath}")
    private String fileUploadPath;

    @Value("${le.platform.service.slash}")
    private String slash;
    
    @Value("${le.platform.service.eApp.fetchLimit}")
    private Integer eAppLimit;
    
    /** The source of truth. */
    @Value("${generali.platform.gao.eapp.unlock.idleTime}")
    private String unlockIdleTime;
    
    
    private String RETRIEV_EAPP_FOR_LMS="RetrieveEappForLMS";
 
    public String saveEApp(final Transactions transactions, final RequestInfo requestInfo,InsuranceAgreement savedInsuranceAgreement) throws ParseException {
        LOGGER.trace("Method In :Inside GeneraliEAppRepository saveEApp : requestInfo" + requestInfo);
        final Request<Agreement> request =
                new JPARequestImpl<Agreement>(getEntityManager(), ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        InsuranceAgreement insuranceAgreement = (InsuranceAgreement) transactions.getProposal();
        if(transactions.getKey40()!=null && transactions.getKey40().equalsIgnoreCase("rm_reassign")){
        	/*String result = Constants.FAILURE;
        	String newAgentId = insuranceAgreement.getTransactionKeys().getKey39();
        	
        	updateAgreementStatusREASSIGN(savedInsuranceAgreement,requestInfo,ContextTypeCodeList.EAPP);
        	Date creationDate = savedInsuranceAgreement.getModifiedDateTime();
        	InsuranceAgreement newInsAgr = new InsuranceAgreement();
        	newInsAgr.setAgentId(newAgentId);
        	newInsAgr.setStatusCode(AgreementStatusCodeList.Reassigned);
        	String eappReassignIdentifier = newAgentId+String.valueOf(idGenerator.generate(EAPP));
        	newInsAgr.setIdentifier(eappReassignIdentifier);
        	newInsAgr.setCreationDateTime(creationDate);
        	newInsAgr.setModifiedDateTime(creationDate);
        	newInsAgr.setTransactionId(savedInsuranceAgreement.getTransactionId());
        	newInsAgr.setRequirements(savedInsuranceAgreement.getRequirements());
        	TransactionKeys newTransactionKeys = savedInsuranceAgreement.getTransactionKeys();
        	String newTransTrackingId = GeneraliConstants.LEADREASSIGN+"-"+newAgentId+"-"+String.valueOf(idGenerator.generate())+"-"+String.valueOf(idGenerator.generate());
        	newTransactionKeys.setKey11(newAgentId);
        	newTransactionKeys.setKey1(newTransTrackingId);
        	newTransactionKeys.setKey3(eappReassignIdentifier);
        	newTransactionKeys.setKey15(AgreementStatusCodeList.Reassigned.toString());
        	newInsAgr.setTransTrackingId(newTransTrackingId);
        	newInsAgr.setTransactionKeys(newTransactionKeys);
        	request.setType(newInsAgr);
        	Response<Agreement> response = agreementComponent.saveAgreement(request);*/
        	transactions.setProductId(savedInsuranceAgreement.getTransactionKeys().getKey5());
        	String status = onAgreementUpdate(transactions, requestInfo,savedInsuranceAgreement);
        	String newAgentId = transactions.getKey39();
        	String eappReassignIdentifier = newAgentId+String.valueOf(idGenerator.generate(EAPP));
        	String newTransTrackingId = GeneraliConstants.LEADREASSIGN+"-"+newAgentId+"-"+String.valueOf(idGenerator.generate())+"-"+String.valueOf(idGenerator.generate());
        	insuranceAgreement.setAgentId(newAgentId);
        	insuranceAgreement.setTransTrackingId(newTransTrackingId);
        	insuranceAgreement.setStatusCode(AgreementStatusCodeList.PendingSubmission);
        	insuranceAgreement.setIdentifier(savedInsuranceAgreement.getIdentifier());
        	insuranceAgreement.setTransactionKeys(savedInsuranceAgreement.getTransactionKeys());
        	insuranceAgreement.getTransactionKeys().setKey11(newAgentId);
        	insuranceAgreement.getTransactionKeys().setKey1(transactions.getKey22());
        	insuranceAgreement.getTransactionKeys().setKey40("rm_reassign");
        	/*insuranceAgreement.getTransactionKeys().setKey1(newTransTrackingId);
        	insuranceAgreement.getTransactionKeys().setKey3(eappReassignIdentifier);
        	insuranceAgreement.getTransactionKeys().setKey15(AgreementStatusCodeList.Reassigned.toString());*/
        	
        	insuranceAgreement.setContextId(savedInsuranceAgreement.getContextId());
        	
        	if (null!=insuranceAgreement && null!=insuranceAgreement.getAgreementExtension() && null!=insuranceAgreement.getAgreementExtension().getSelectedOptions()
                	&& !insuranceAgreement.getAgreementExtension().getSelectedOptions().isEmpty()){
                	generaliComponentRepositoryHelper.saveUserSelections(insuranceAgreement.getAgreementExtension().getSelectedOptions(), userSelectionComponent,entityManager,ContextTypeCodeList.EAPP,requestInfo);
                }
        	request.setType(insuranceAgreement);
        	saveAgreementProductSpecification(transactions, requestInfo, insuranceAgreement);
        	insuranceAgreement.setRequirements(transactions.getRequirements());
        	agreementComponent.saveAgreement(request);
            final Request<Agreement> agreementRequest =
                    new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
            agreementRequest.setType(savedInsuranceAgreement);
            getAgreementDetails(transactions, agreementRequest, requestInfo.getTransactionId());
            generaliComponentRepositoryHelper.saveInsured(transactions.getInsured(), requestInfo, insuranceAgreement,
                    getEntityManager(), ContextTypeCodeList.EAPP, partyComponent,userSelectionComponent);
            saveProposer(transactions.getProposer(), requestInfo, insuranceAgreement);
            generaliComponentRepositoryHelper.saveAgent(
    				transactions.getAgreementProducer(), requestInfo,
    				insuranceAgreement, partyComponent, getEntityManager(),
    				ContextTypeCodeList.EAPP);
            saveBeneficiary(transactions.getBeneficiaries(), requestInfo, insuranceAgreement);
            saveInsuredPreviousPolicyList(transactions.getInsuredPreviousPolicyList(), requestInfo,
                    transactions.getInsured(),transactions.getAdditionalInsuredes());
            generaliComponentRepositoryHelper.savePayer(transactions.getPayer(), requestInfo, insuranceAgreement,
                    getEntityManager(), ContextTypeCodeList.EAPP, partyComponent, userSelectionComponent);
            
        	/*Agreement responseAgreement = response.getType();
        	if(responseAgreement != null){
        		result = Constants.SUCCESS;
        	}*/
        	return status;
        }
        else{
        insuranceAgreement.setContextId(ContextTypeCodeList.EAPP);
        onBeforeSave(transactions, requestInfo);
        if (Constants.UPDATE.equals(transactions.getMode())) {
            String status = onAgreementUpdate(transactions, requestInfo,savedInsuranceAgreement);
            if (!Constants.SUCCESS.equals(status)) {
                return status;
            }
        } else {
            insuranceAgreement.setCreationDateTime(transactions.getCreationDateTime());
        }

       
        if (null!=insuranceAgreement && null!=insuranceAgreement.getAgreementExtension() && null!=insuranceAgreement.getAgreementExtension().getSelectedOptions()
        	&& !insuranceAgreement.getAgreementExtension().getSelectedOptions().isEmpty()){
        	generaliComponentRepositoryHelper.saveUserSelections(insuranceAgreement.getAgreementExtension().getSelectedOptions(), userSelectionComponent,entityManager,ContextTypeCodeList.EAPP,requestInfo);
        }
        request.setType(insuranceAgreement);
        saveAgreementProductSpecification(transactions, requestInfo, insuranceAgreement);
        insuranceAgreement.setRequirements(transactions.getRequirements());
        agreementComponent.saveAgreement(request);
        generaliComponentRepositoryHelper.saveInsured(transactions.getInsured(), requestInfo, insuranceAgreement,
                getEntityManager(), ContextTypeCodeList.EAPP, partyComponent,userSelectionComponent);
//        generaliComponentRepositoryHelper.saveAdditionalInsuredes(transactions.getAdditionalInsuredes(), requestInfo,
//                insuranceAgreement, getEntityManager(), ContextTypeCodeList.EAPP, partyComponent,userSelectionComponent);
        saveProposer(transactions.getProposer(), requestInfo, insuranceAgreement);        
        generaliComponentRepositoryHelper.saveAgent(
				transactions.getAgreementProducer(), requestInfo,
				insuranceAgreement, partyComponent, getEntityManager(),
				ContextTypeCodeList.EAPP);
        //generaliComponentRepositoryHelper.saveUserSelections(transactions.getProposer().getSelectedOptions(),userSelectionComponent,getEntityManager(),
			//	ContextTypeCodeList.EAPP,requestInfo);
        saveBeneficiary(transactions.getBeneficiaries(), requestInfo, insuranceAgreement);
        saveAppointee(transactions.getAppointee(), requestInfo, insuranceAgreement);
        generaliComponentRepositoryHelper.savePayer(transactions.getPayer(), requestInfo, insuranceAgreement,
                getEntityManager(), ContextTypeCodeList.EAPP, partyComponent, userSelectionComponent);
        saveInsuredPreviousPolicyList(transactions.getInsuredPreviousPolicyList(), requestInfo,
                transactions.getInsured(),transactions.getAdditionalInsuredes());
        LOGGER.trace("Method out :Inside GeneraliEAppRepository saveEApp");
        return insuranceAgreement.getStatusCode().toString();
        }
    }
    
	protected void onBeforeSave(Transactions transactions, RequestInfo requestInfo) {
        LOGGER.trace("Method In :Inside GeneraliEAppRepository onBeforeSave ");
        final InsuranceAgreement insuranceAgreement = transactions.getProposal();

        // Since ModifiedDate is using for the chunk retrieve and SourceOfTruth functionality
        insuranceAgreement.setModifiedDateTime(transactions.getModifiedTimeStamp());
        insuranceAgreement.setTransactionId(requestInfo.getTransactionId());
        insuranceAgreement.setIdentifier(transactions.getProposalNumber());

        if (null != transactions.getStatus() && transactions.getStatus().equals(GeneraliConstants.PENDING_SUBMISSION)) {
            insuranceAgreement.setStatusCode(AgreementStatusCodeList.PendingSubmission);
        } else if (null != transactions.getStatus()
                && transactions.getStatus().equals(AgreementStatusCodeList.Confirmed.toString())) {
            insuranceAgreement.setStatusCode(AgreementStatusCodeList.Confirmed);
        } else if (null != transactions.getStatus()
                && transactions.getStatus().equals(AgreementStatusCodeList.Submitted.toString())) {
            insuranceAgreement.setStatusCode(AgreementStatusCodeList.Submitted);
        } else if (null != transactions.getStatus()
                && transactions.getStatus().equals(AgreementStatusCodeList.Cancelled.toString())) {
            insuranceAgreement.setStatusCode(AgreementStatusCodeList.Cancelled);
        } else if (null != transactions.getStatus()
                && transactions.getStatus().equals(AgreementStatusCodeList.PaymentDone.toString())) {
            insuranceAgreement.setStatusCode(AgreementStatusCodeList.PaymentDone);
        }

        if (transactions.getAgentId() != null) {
            insuranceAgreement.setAgentId(transactions.getAgentId());
        }
        LOGGER.trace("Method Out :Inside GeneraliEAppRepository onBeforeSave ");
    }

    protected void getAgreementDetails(final Transactions transaction, final Request<Agreement> agreementRequest,
            final String transactionId) {
        LOGGER.trace("Method In :Inside GeneraliEAppRepository getAgreementDetails ");

/*        final Response<Insured> insuredResponse = partyComponent.retrieveInsured(agreementRequest);
        transaction.setInsured(insuredResponse.getType());

        final Response<Set<Insured>> additionalInsuredResponse =
            partyComponent.retrieveAdditionalInsureds(agreementRequest);
        transaction.setAdditionalInsuredes(additionalInsuredResponse.getType());*/
        Set<Insured> insuredPreviousPolicyList = new HashSet<Insured>();
        final Response<Set<Insured>> additionalInsuredResponse = partyComponent.retrieveAdditionalInsureds(agreementRequest);
        Set<Insured> insuredSet = additionalInsuredResponse.getType();
        Set<Insured> additionalInsuredSet = new HashSet<Insured>();
        if (CollectionUtils.isNotEmpty(insuredSet)) {
            for (Insured insured : insuredSet) {
                if ((insured.getTypeCode() != null) && ("Additional").equalsIgnoreCase(insured.getTypeCode().name())) {
                    additionalInsuredSet.add(insured);
                }else if ((insured.getTypeCode() != null) && ("Primary").equalsIgnoreCase(insured.getTypeCode().name())) {
                        transaction.setInsured(insured);
                }
            }
        }
        transaction.setAdditionalInsuredes(additionalInsuredSet);
        

        if (transaction.getInsured() != null) {
            final Request<PartyRoleInAgreement> partyRoleInAgreementRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(getEntityManager(), ContextTypeCodeList.EAPP,
                            transactionId);
            partyRoleInAgreementRequest.setType(transaction.getInsured());
            final Response<Set<InsuranceAgreement>> previousAgreementResponse =
                    agreementComponent.getPreviousAgreements(partyRoleInAgreementRequest);
           // transaction.setPreviousAgreements(previousAgreementResponse.getType());
            if (previousAgreementResponse.getType() != null) {
				for (InsuranceAgreement insuredInsuranceAgreement : previousAgreementResponse
						.getType()) {
					Insured insured = new Insured();
					insured.setPartyId(transaction.getInsured().getPartyId());
					insured.setTypeCode(transaction.getInsured().getTypeCode());
					insured.setPlayerParty(transaction.getInsured().getPlayerParty());
					insured.setIsPartyRoleIn(insuredInsuranceAgreement);
					insuredPreviousPolicyList.add(insured);
				}
			}
        }
        
        /*if(transaction.getAdditionalInsuredes() != null && transaction.getAdditionalInsuredes().size() > 0){
        	for(Insured addInsured:transaction.getAdditionalInsuredes()){
        		final Request<PartyRoleInAgreement> partyRoleInAgreementRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(getEntityManager(), ContextTypeCodeList.EAPP,
                            transactionId);
            partyRoleInAgreementRequest.setType(addInsured);
            final Response<Set<InsuranceAgreement>> previousAgreementResponse =
                    agreementComponent.getPreviousAgreements(partyRoleInAgreementRequest);
            //transaction.setPreviousAgreements(previousAgreementResponse.getType());
            if (previousAgreementResponse.getType() != null) {
				for (InsuranceAgreement insuredInsuranceAgreement : previousAgreementResponse
						.getType()) {
					Insured additionalInsured = new Insured();
					additionalInsured.setPartyId(addInsured.getPartyId());
					additionalInsured.setTypeCode(addInsured.getTypeCode());
					additionalInsured.setPlayerParty(addInsured.getPlayerParty());
					additionalInsured.setIsPartyRoleIn(insuredInsuranceAgreement);
					insuredPreviousPolicyList.add(additionalInsured);
				}
			}
            
        	}
        }*/
    	transaction.setInsuredPreviousPolicyList(insuredPreviousPolicyList);
        
        final Response<AgreementHolder> proposerResponse = partyComponent.retrieveProposer(agreementRequest);
        transaction.setProposer(proposerResponse.getType());
        
        final Response<AgreementProducer> agentResponse = partyComponent.retrieveAgent(agreementRequest);
        transaction.setAgreementProducer(agentResponse.getType());

        final Response<Set<Beneficiary>> beneficiariesResponse = partyComponent.retrieveBeneficiaries(agreementRequest);
        transaction.setBeneficiaries(beneficiariesResponse.getType());

        final Response<Appointee> appointeeResponse = partyComponent.retrieveAppointee(agreementRequest);
        transaction.setAppointee(appointeeResponse.getType());

        final Response<PremiumPayer> payerResponse = partyComponent.retrievePayer(agreementRequest);
        transaction.setPayer(payerResponse.getType());
    }

    protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(String operation) {
        List<AgreementStatusCodeList> statusCodeLists = new ArrayList<AgreementStatusCodeList>();
        if (Constants.FIRST_TIME_SYNC.equals(operation) || Constants.RETRIEVE.equals(operation)
                || Constants.RETRIEVE_BY_FILTER.equals(operation) || Constants.RETRIEVE_BY_COUNT.equals(operation)) {
            statusCodeLists.add(AgreementStatusCodeList.PendingSubmission);
            statusCodeLists.add(AgreementStatusCodeList.Submitted);            
        } else if (Constants.RETRIEVE_ALL.equals(operation) || Constants.UPDATE_OP.equals(operation)) {
            statusCodeLists.add(AgreementStatusCodeList.PendingSubmission);
            statusCodeLists.add(AgreementStatusCodeList.Submitted);
            statusCodeLists.add(AgreementStatusCodeList.Confirmed);
            statusCodeLists.add(AgreementStatusCodeList.Cancelled);
            statusCodeLists.add(AgreementStatusCodeList.PaymentDone);
        }
     else if (RETRIEV_EAPP_FOR_LMS.equals(operation)) {
    	 statusCodeLists.add(AgreementStatusCodeList.Suspended);
        statusCodeLists.add(AgreementStatusCodeList.Cancelled);
    }
        return statusCodeLists;
    }

    /**
     * Retrieve by count e app.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the search count result
     */
    public SearchCountResult
            retrieveEAppCountForLMS(final RequestInfo requestInfo, final SearchCriteria searchCriteria) {

        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(getEntityManager(), ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_COUNT);
        statusRequest.setType(statusList);

        final Response<SearchCountResult> searchCountResponse =
                agreementComponent.getAgreementCountForLMS(searchCriteriatRequest, statusRequest);
        SearchCountResult searchCountResult = null;
        if (searchCountResponse.getType() != null) {
            searchCountResult = (SearchCountResult) searchCountResponse.getType();

        }

        return searchCountResult;

    }
    protected void saveInsuredPreviousPolicyList(final Set<Insured> previousHistoryInsuredList,
			final RequestInfo requestInfo, final Insured insured,
			final Set<Insured> additionalInsured) {
		final Request<PartyRoleInAgreement> previousHistoryInsuredRequest = new JPARequestImpl<PartyRoleInAgreement>(
				getEntityManager(), ContextTypeCodeList.EAPP,
				requestInfo.getTransactionId());

		if (CollectionUtils.isNotEmpty(previousHistoryInsuredList)) {
			for (Insured previousHistoryInsured : previousHistoryInsuredList) {
				
				previousHistoryInsured.setPlayerParty(insured.getPlayerParty());
				previousHistoryInsured.setTransactionId(requestInfo.getTransactionId());
				previousHistoryInsuredRequest.setType(previousHistoryInsured);
				partyComponent.savePartyRoleInAgreement(previousHistoryInsuredRequest);
				
			/*	if (insured != null && previousHistoryInsured != null && previousHistoryInsured.getTypeCode()!= null && 
						previousHistoryInsured.getTypeCode().name().equals("Primary")) {
					previousHistoryInsured.setPlayerParty(insured.getPlayerParty());
				} else if (previousHistoryInsured != null && previousHistoryInsured.getTypeCode()!= null &&
						previousHistoryInsured.getTypeCode().name().equals("Additional")) {
					for (Insured addnInsured : additionalInsured) {
						if (addnInsured != null && addnInsured.getPartyId()!= null && addnInsured.getPartyId().equals(
										previousHistoryInsured.getPartyId())) {
							// saving addnInsured previous policy
							previousHistoryInsured.setPlayerParty(addnInsured.getPlayerParty());
						}
					}
				}
				previousHistoryInsured.setTransactionId(requestInfo.getTransactionId());
				previousHistoryInsuredRequest.setType(previousHistoryInsured);
				partyComponent.savePartyRoleInAgreement(previousHistoryInsuredRequest);*/
				
				
			}
		}
    }
	
    
    /**
     * On agreement update.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the string
     */
    protected String onAgreementUpdate(Transactions transactions, RequestInfo requestInfo,InsuranceAgreement savedInsuranceAgreement) {
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.UPDATE_OP);
        InsuranceAgreement agreement = null;
        if(null != savedInsuranceAgreement) // We already have agreement with transTracking Id
            agreement = savedInsuranceAgreement;
        else {
        agreement =
                LifeEngageComponentRepositoryHelper.createReplacingAgreement(transactions, requestInfo, getEntityManager(),
                        agreementComponent, ContextTypeCodeList.EAPP, statusList);
        }
        // Checking whether the status code is cancelled, if so return directly.
        if (agreement.getStatusCode() != null && agreement.getStatusCode().equals(AgreementStatusCodeList.Cancelled)) {
            return AgreementStatusCodeList.Cancelled.toString();

        }

        // Checking whether the server data is latest when source of truth is server, if so reject the client data and
        // send rejected status
        if (Constants.TRUE.equals(conflictResolution) && Constants.SERVER.equals(sourceOfTruth)) {
        	
           	
        	// Modified the logic
        	//If the new agreement has latest or same  Status (for submitted and confirmed )than the  status of the existing illustration, will proceed with the save.
        	// Will not route to Reject
        	
        	InsuranceAgreement agreementToSave= transactions.getProposal();
        	Boolean agreementTosaveStatusLatestthanExistingStaus=false;
        	
        	AgreementStatusCodeList statusToSave = agreementToSave.getStatusCode();        	
        	AgreementStatusCodeList statusExisting = agreement.getStatusCode();
        	
			if (statusToSave != null
					&& statusToSave.equals(AgreementStatusCodeList.Submitted)) {
				if (statusExisting.equals(AgreementStatusCodeList.PendingSubmission)
						|| statusExisting
								.equals(AgreementStatusCodeList.Confirmed)
						|| statusExisting
								.equals(AgreementStatusCodeList.Submitted)) {
					agreementTosaveStatusLatestthanExistingStaus = true;
				}
			}

			else if (statusToSave != null
					&& statusToSave.equals(AgreementStatusCodeList.Confirmed)) {
				if (statusExisting.equals(AgreementStatusCodeList.PendingSubmission)
						|| statusExisting
								.equals(AgreementStatusCodeList.Confirmed)) {
					agreementTosaveStatusLatestthanExistingStaus = true;
				}
			}
            Date modifiedTimeStamp = agreement.getModifiedDateTime();

            // Changed the modified date to lastsync date

            if (!(transactions.getLastSyncDate().toString().equals(GeneraliDateUtil.getDefaultDate()))&& !agreementTosaveStatusLatestthanExistingStaus
                    && ((null != modifiedTimeStamp) && (modifiedTimeStamp.after(transactions.getLastSyncDate())))) {

                LOGGER.trace("Inside if case and rejected the case.."+agreement.getIdentifier());
                return Constants.REJECTED;

            }
        }
        
        //Copying BPM Related Values from Previous Agreement to new Agreement
        if(transactions.getProposal().getAgreementExtension()!=null &&
        		agreement.getAgreementExtension()!=null){
            transactions.getProposal().getAgreementExtension().setBpmDocId(agreement.getAgreementExtension().getBpmDocId());
            transactions.getProposal().getAgreementExtension().setBpmPendingInfo(agreement.getAgreementExtension().getBpmPendingInfo());
            transactions.getProposal().getAgreementExtension().setBpmStatus(agreement.getAgreementExtension().getBpmStatus());        	
        }

        
        Set<Agreement> replacingAgeements =
                LifeEngageComponentRepositoryHelper.updateAgreementStatus(agreement, agreementComponent, requestInfo,
                		getEntityManager(), ContextTypeCodeList.EAPP);
        transactions.getProposal().setCreationDateTime(agreement.getCreationDateTime());
        transactions.getProposal().setReplacesAgreements(replacingAgeements);

        Set<AgreementDocument> agreementDocumentsSet = new HashSet<AgreementDocument>();
        if (replacingAgeements != null) {
            agreementDocumentsSet = generaliComponentRepositoryHelper.createReplaceDocument(replacingAgeements);
            LOGGER.trace("Previous agreementDocument Size=" + agreementDocumentsSet.size());
            // While copying all the Previous documents will be set in the current agreement
            transactions.getProposal().setRelatedDocument(agreementDocumentsSet);

        }
        
        // replacingAgeements is set as the previous obj to current agreement - so its requirements needs to be compared
        // wth current

        if (replacingAgeements != null) {
            LifeEngageComponentRepositoryHelper.createReplacingRequirementSet(transactions, replacingAgeements,
                    fileUploadPath + slash);
            // While copying all the Previous requirements will be set in the current agreement
        }

        
        return Constants.SUCCESS;
    }
    
    protected void updateAgreementStatusREASSIGN(final Agreement agreement, final RequestInfo requestInfo,
            final ContextTypeCodeList contextType) {
        final Request<Agreement> request =
                new JPARequestImpl<Agreement>(entityManager, contextType, requestInfo.getTransactionId());
        agreement.getTransactionKeys().setKey15(AgreementStatusCodeList.Suspended.toString());
        try {
        	agreement.setModifiedDateTime(LifeEngageComponentHelper.getCurrentdate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        agreement.setStatusCode(AgreementStatusCodeList.Suspended);
        request.setType(agreement);
        final GeneraliAgreementDao agreementDao = new GeneraliAgreementDaoImpl();
        agreementDao.updateAgreementStatusForLeadReassign(request);
    }

    /**
     * Retrieve by filter e app.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the list
     */
    public List<Transactions> retrieveByFilterEAppRelatedTransactions(final RequestInfo requestInfo, final SearchCriteria searchCriteria) {
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_FILTER);
       
        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(getEntityManager(), ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Response<List<Agreement>> searchCriteriatResponse =
                agreementComponent.getAgreementByFilterRelatedTransactions(searchCriteriatRequest, statusRequest);
        final List<Agreement> agreements = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(agreements)) {
            for (Agreement agreement : agreements) {
                transaction = new Transactions();
                transaction.setProposal((InsuranceAgreement) agreement);
                transaction.setProposalNumber(agreement.getIdentifier().toString());
                transaction.setType(searchCriteria.getType());
                transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }
        LOGGER.trace("Inside EAppRepository retrieveByFilterEApp : transactionsList" + transactionsList.size());
        return transactionsList;
    }
    
    /**
     * Save beneficiary.
     * 
     * @param beneficiaries
     *            the beneficiaries
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    @Override
    protected void saveBeneficiary(final Set<Beneficiary> beneficiaries, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement) {

        if (CollectionUtils.isNotEmpty(beneficiaries)) {
            final Request<PartyRoleInAgreement> beneficiaryRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, ContextTypeCodeList.EAPP,
                            requestInfo.getTransactionId());
            for (Beneficiary beneficiary : beneficiaries) {
                beneficiary.setIsPartyRoleIn(insuranceAgreement);
                beneficiary.setTransactionId(requestInfo.getTransactionId());
                beneficiaryRequest.setType(beneficiary);
                if(!insuranceAgreement.getTransactionKeys().getKey40().equalsIgnoreCase("rm_reassign")){
                GeneraliComponentRepositoryHelper.saveUserSelections(beneficiary.getSelectedOptions(), userSelectionComponent, entityManager, ContextTypeCodeList.EAPP, requestInfo);
                }
                partyComponent.savePartyRoleInAgreement(beneficiaryRequest);
            }
        }
    }
    
    /**
     * Save appointee.
     * 
     * @param appointee
     *            the appointee
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    protected void saveAppointee(final Appointee appointee, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement) {

        if (appointee != null) {
            appointee.setIsPartyRoleIn(insuranceAgreement);
            appointee.setTransactionId(requestInfo.getTransactionId());
            final Request<PartyRoleInAgreement> appointeeRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, ContextTypeCodeList.EAPP,
                            requestInfo.getTransactionId());
            GeneraliComponentRepositoryHelper.saveUserSelections(appointee.getSelectedOptions(), userSelectionComponent, entityManager, ContextTypeCodeList.EAPP, requestInfo);
            appointeeRequest.setType(appointee);
            partyComponent.savePartyRoleInAgreement(appointeeRequest);
        }
    }
    
    /**
     * Save proposer.
     * 
     * @param proposer
     *            the proposer
     * @param requestInfo
     *            the request info
     * @param insuranceAgreement
     *            the insurance agreement
     */
    @Override
    protected void saveProposer(final AgreementHolder proposer, final RequestInfo requestInfo,
            final InsuranceAgreement insuranceAgreement) {

        if (proposer != null) {
            proposer.setIsPartyRoleIn(insuranceAgreement);
            proposer.setTransactionId(requestInfo.getTransactionId());
            if(!insuranceAgreement.getTransactionKeys().getKey40().equalsIgnoreCase("rm_reassign")){
            GeneraliComponentRepositoryHelper.saveUserSelections(proposer.getSelectedOptions(),userSelectionComponent, entityManager, ContextTypeCodeList.EAPP, requestInfo);
            }
            final Request<PartyRoleInAgreement> proposerRequest =
                    new JPARequestImpl<PartyRoleInAgreement>(entityManager, ContextTypeCodeList.EAPP,
                            requestInfo.getTransactionId());
            proposerRequest.setType(proposer);
            partyComponent.savePartyRoleInAgreement(proposerRequest);
        }
    }

    /**
     * Retrieve Eapps  related with a Lead
     * @param requestInfo
     *            the request Information
     * @param searchCriteria
     *            the search Criteria 
     * 
     */
    
	public List<Transactions> retrieveEappForLMS(final RequestInfo requestInfo,
			final SearchCriteria searchCriteria) {
		final List<Transactions> transactionsList = new ArrayList<Transactions>();
		Transactions transaction = null;
		final Request<SearchCriteria> searchCriteriatRequest = new JPARequestImpl<SearchCriteria>(
				getEntityManager(), ContextTypeCodeList.EAPP,
				requestInfo.getTransactionId());
		final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(RETRIEV_EAPP_FOR_LMS);

		searchCriteriatRequest.setType(searchCriteria);

		final Request<List<AgreementStatusCodeList>> statusRequest = new JPARequestImpl<List<AgreementStatusCodeList>>(
				getEntityManager(), ContextTypeCodeList.EAPP,
				requestInfo.getTransactionId());

		statusRequest.setType(statusList);

		final Response<List<Agreement>> searchCriteriatResponse = agreementComponent
				.getRelatedAgreementsForLMS(searchCriteriatRequest,
						statusRequest);
		final List<Agreement> agreements = searchCriteriatResponse.getType();
		if (CollectionUtils.isNotEmpty(agreements)) {
			for (Agreement agreement : agreements) {
				transaction = new Transactions();
				transaction.setProposal((InsuranceAgreement) agreement);
				transaction.setType(Constants.EAPP);
				transaction.setProposalNumber(agreement.getIdentifier()
						.toString());
				transaction.setModifiedTimeStamp(agreement
						.getModifiedDateTime());
				transactionsList.add(transaction);
			}
		}
		LOGGER.trace("Inside EAppRepository retrieveEappForLMS : transactionsList"
				+ transactionsList.size());

		return transactionsList;

	}
    /**
     * @param transTrackingId
     * @return
     */
    public InsuranceAgreement
            retrieveEappByTransTrackingId(String transTrackingId, String agentId, String transactionId) {
        InsuranceAgreement agreementToReturn = null;
        final List<AgreementStatusCodeList> statusList =getAgreementStatusCodesFor(Constants.RETRIEVE);

        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setTransTrackingId(transTrackingId);
        insuranceAgreement.setAgentId(agentId);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementResponse =
                agreementComponent.retrieveAgreementForTransTrackingId(agreementRequest, statusRequest);
        final InsuranceAgreement agreement = (InsuranceAgreement) agreementResponse.getType();
        if (agreement != null) {
            agreementToReturn = (InsuranceAgreement) agreementResponse.getType();
        }
        return agreementToReturn;
    }
  /**
     * Retrieve by filter e app.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @param agentId
     *            the agent id
     * @return the list
     */
    public List<Transactions> retrieveByFilterEApp(final RequestInfo requestInfo, final SearchCriteria searchCriteria) {
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_FILTER);
       
        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);
        /*
        String agentId=searchCriteria.getAgentId();
        try {
			lifeAsiaComponent.fetchAndUpdateStatus(agentId);
		} catch (Exception e) {
			LOGGER.error("Failed to update the Status from LifeAsia for agent : "+agentId);
		}
		*/
        final Response<List<Agreement>> searchCriteriatResponse =
                agreementComponent.getAgreementByFilter(searchCriteriatRequest, statusRequest);
        final List<Agreement> agreements = searchCriteriatResponse.getType();
		if (CollectionUtils.isNotEmpty(agreements)) {
			for (Agreement agreement : agreements) {
				transaction = new Transactions();
				transaction.setProposal((InsuranceAgreement) agreement);
				/*final Request<Agreement> agreementRequest = new JPARequestImpl<Agreement>(
						entityManager, ContextTypeCodeList.EAPP,
						requestInfo.getTransactionId());
				agreementRequest.setType(agreement);
				final Response<AgreementHolder> proposerResponse = partyComponent
						.retrieveProposer(agreementRequest);
				transaction.setProposer(proposerResponse.getType());*/
				transaction.setProposalNumber(agreement.getIdentifier()
						.toString());
				transaction.setType(searchCriteria.getType());
				transaction.setModifiedTimeStamp(agreement
						.getModifiedDateTime());
				transactionsList.add(transaction);
			}
        }
        LOGGER.trace("Inside EAppRepository retrieveByFilterEApp : transactionsList" + transactionsList.size());
        return transactionsList;
    }
    /**
     * 
     * @param requestInfo
     * @param searchCriteria
     * @return
     */
    public List<Object> getEappCountForSalesActivity(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        // TODO Auto-generated method stub

        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.EAPP, null);
        searchCriteriatRequest.setType(searchCriteria);

        final Response<List<Object>> searchCountResponse =
                agreementComponent.getCountForSalesActivity(searchCriteriatRequest);

        final List<Object> eAppCountList = (List<Object>) searchCountResponse.getType();

        return eAppCountList;
    }

    public Map<String, Integer> retrieveMonthwiseCount(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.EAPP, null);
        searchCriteriatRequest.setType(searchCriteria);
        final Map<String, Integer> searchCountResponse =
                agreementComponent.retrieveMonthwiseCount(searchCriteriatRequest);
        return searchCountResponse;
    }

    public Map<String, Integer> retrieveWeeklyCount(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.EAPP, null);
        searchCriteriatRequest.setType(searchCriteria);
        final Map<String, Integer> searchCountResponse =
                agreementComponent.retrieveWeeklyCount(searchCriteriatRequest);
        return searchCountResponse;
    }

    public List<Transactions> retrieveMonthlyCloseData(final RequestInfo requestInfo,
            final SearchCriteria searchCriteria) {
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        List<AgreementStatusCodeList> statusCodeLists = new ArrayList<AgreementStatusCodeList>();
        statusCodeLists.add(AgreementStatusCodeList.Submitted);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusCodeLists);

        final Response<List<Agreement>> searchCriteriatResponse =
                agreementComponent.getMonthlyData(searchCriteriatRequest, statusRequest);

        final List<Agreement> agreements = searchCriteriatResponse.getType();

        if (CollectionUtils.isNotEmpty(agreements)) {
            for (Agreement agreement : agreements) {
                transaction = new Transactions();
                transaction.setProposal((InsuranceAgreement) agreement);
                transactionsList.add(transaction);
            }
        }
        return transactionsList;

    }

    public InsuranceAgreement getEAppAgreementByAppNo(InsuranceAgreement insAgrement){
        final Request<InsuranceAgreement> aggrementRequest =
                new JPARequestImpl<InsuranceAgreement>(entityManager, ContextTypeCodeList.EAPP,"");
        aggrementRequest.setType(insAgrement);
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_FILTER);
        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        "");
        statusRequest.setType(statusList);
        
        final Response<InsuranceAgreement> agreementResponse = agreementComponent.getEAppAgreementByAppNo(aggrementRequest, statusRequest);
        return agreementResponse.getType();
        
    }
    
    public GeneraliTransactions retrieveEappTxnByAppNo(InsuranceAgreement insAgrement) throws BusinessException {
        GeneraliTransactions transaction = new GeneraliTransactions(); 
        try{
            InsuranceAgreement agreement = getEAppAgreementByAppNo(insAgrement);
            transaction.setTransTrackingId(agreement.getTransactionId());
            transaction.setKey4(agreement.getIdentifier());
            transaction.setAgentId(agreement.getTransactionKeys().getKey11());
            
            final Request<Agreement> agreementRequest =
                    new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, "");
            transaction.setProposal(agreement);

            agreementRequest.setType(agreement);

            getAgreementDetails(transaction, agreementRequest, "");

            if (transaction.getProposal() != null) {
                transaction.setProposalNumber(agreement.getIdentifier().toString());
            }
            transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
        } catch(DaoException e){
            throw new BusinessException(e.getMessage());
        }
        
        return transaction;       
    }
    
    public void updateEappMemoCountAndStatus(InsuranceAgreement insAgrement, List<AppianMemoDetails> appianMemoList) throws BusinessException {
        try{
        InsuranceAgreement insuranceAgreement = getEAppAgreementByAppNo(insAgrement);
        insuranceAgreement.getAgreementExtension().setBpmStatus(insAgrement.getAgreementExtension().getBpmStatus());
        insuranceAgreement.getTransactionKeys().setKey30(insAgrement.getTransactionKeys().getKey30());
        
        insuranceAgreement.getTransactionKeys().setKey7(insAgrement.getTransactionKeys().getKey7());
        Date date =LifeEngageComponentHelper.getCurrentdate();
        insuranceAgreement.setModifiedDateTime(date);
        insuranceAgreement.setCreationDateTime(date);
        SimpleDateFormat dateFormat = new SimpleDateFormat(LifeEngageComponentHelper.DATEFORMAT_YYYY_MM_DD_HH_MM_SS, Locale.getDefault());
        insuranceAgreement.getTransactionKeys().setKey22(dateFormat.format(date));
        Set<Requirement> reqSet = new HashSet<Requirement>();
        //Map<String,Requirement> reqMap = new HashMap<String,Requirement>();
        for(AppianMemoDetails memo : appianMemoList){
            boolean memoReqFlag = true;
            for(Requirement req : insuranceAgreement.getRequirements()){
                if(req.getRequirementName().equals(memo.getMemoId())){
                    memoReqFlag = false;
                    req.setPartyIdentifier(memo.getMemoStatus());
                    if(StringUtils.isNotEmpty(memo.getSubmissionDate())){
	                    for(AgreementDocument doc : req.getRequirementDocuments()){
	                    	if(("true").equals(doc.getDocumentUploadStatus())){
	                    		doc.setModifiableIndicator(false);
	                    	}
	                    } 
	                    if(memo.getMemoStatus().equals(GeneraliConstants.OPEN)){	                    	 
	                         List<AgreementDocument> docList = req.getRequirementDocuments();
	                         AgreementDocument submittedDoc = docList.get(docList.size()-1);
	                         if(("true").equals(submittedDoc.getDocumentUploadStatus())){
		                         AgreementDocument doc = new AgreementDocument();
		                         doc.setName(memo.getMemoDetails().getMemoCode()+Integer.toString(docList.size()+1));
		                         doc.setDocumentProofSubmitted(doc.getName());
		                         doc.setModifiableIndicator(true);
		                         docList.add(doc);
	                         }
	                    }
                    }
                }
            }
            if(memoReqFlag){
                Requirement rq = new Requirement();
                AgreementDocument doc = new AgreementDocument();
                List<AgreementDocument> docList = new ArrayList<AgreementDocument>();
                doc.setName(memo.getMemoDetails().getMemoCode()+"1");
                doc.setDocumentProofSubmitted(doc.getName());
                doc.setModifiableIndicator(true);
                docList.add(doc);
                rq.setRequirementDocuments(docList);
                rq.setRequirementType(DocumentTypeCodeList.Memo);
                rq.setRequirementSubType(memo.getMemoDetails().getMemoCode());
                rq.setPartyIdentifier(memo.getMemoStatus());
                rq.setRequirementName(memo.getMemoId());
                reqSet.add(rq);
            }
        }
        insuranceAgreement.getRequirements().addAll(reqSet);
        final Request<Agreement> aggrementReq =
               new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP,"");
        aggrementReq.setType(insuranceAgreement);
        agreementComponent.mergeAgreement(aggrementReq);
        } catch(ParseException e){
            throw new BusinessException(e.getMessage());
        } catch(DaoException e){
            throw new BusinessException(e.getMessage());
        }
        LOGGER.info("Updated application status of  Application no"+insAgrement.getTransactionKeys().getKey21()+" with  status, info  :- "
                +", "+insAgrement.getAgreementExtension().getBpmStatus());

        
        
        
    }
    
    public GeneraliTransactions retrieveEAppWithMemo(String proposalNumber, String transactionId) {
        LOGGER.trace("LifeEngageEAppRepository.retrieveEApp: proposalNumber" + proposalNumber);
        LOGGER.trace("LifeEngageEAppRepository.retrieveEApp: transactionId" + transactionId);
        final GeneraliTransactions transaction = new GeneraliTransactions();
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE);

        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(proposalNumber);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementsResponse =
                agreementComponent.getInsuranceAgreement(agreementRequest, statusRequest);
        if (agreementsResponse.getType() != null) {
            final InsuranceAgreement agreement = (InsuranceAgreement) agreementsResponse.getType();
            transaction.setProposal(agreement);

            agreementRequest.setType(agreement);

            getAgreementDetails(transaction, agreementRequest, transactionId);

            if (transaction.getProposal() != null) {
                transaction.setProposalNumber(agreement.getIdentifier().toString());
            }
            transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
        }
        LOGGER.trace("LifeEngageEAppRepository.retrieveEApp: transaction" + transaction);
        return transaction;
    }
    
    public List<GeneraliTransactions> retrieveAllWithMemo(Transactions transactions, RequestInfo requestInfo,
            boolean isFullDetailsRequired) {
        LOGGER.trace("Inside EAppRepository retrieveAll : retrieveAll" + requestInfo);
        final List<GeneraliTransactions> transactionsList = new ArrayList<GeneraliTransactions>();
        GeneraliTransactions transaction = null;
         List<AgreementStatusCodeList> statusList =null;
        if (requestInfo.getFirstTimeSync()) {
            statusList=getAgreementStatusCodesFor(Constants.FIRST_TIME_SYNC);
            
        } else {
            statusList=getAgreementStatusCodesFor(Constants.RETRIEVE_ALL);
        }
        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setAgentId(transactions.getAgentId());
        insuranceAgreement.setLastSyncDate(requestInfo.getLastSyncDate());
        agreementRequest.setType(insuranceAgreement);
        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);
        Request<Integer> limitRequest = null;
        if (isFullDetailsRequired) {
            if (eAppLimit != null) {
                limitRequest =
                        new JPARequestImpl<Integer>(entityManager, ContextTypeCodeList.EAPP,
                                requestInfo.getTransactionId());
                limitRequest.setType(eAppLimit);
            }
        }
        final Response<List<Agreement>> agreementsResponse =
                agreementComponent.getAgreementsList(agreementRequest, statusRequest, limitRequest);
        final List<Agreement> agreements = agreementsResponse.getType();
        if (CollectionUtils.isNotEmpty(agreements)) {
            for (Agreement agreement : agreements) {
                transaction = new GeneraliTransactions();
                transaction.setProposal((InsuranceAgreement) agreement);
                agreementRequest.setType(agreement);
                if (isFullDetailsRequired) {
                    getAgreementDetails(transaction, agreementRequest, requestInfo.getTransactionId());
                } else {
                    final Response<AgreementHolder> proposerResponse =
                            partyComponent.retrieveProposer(agreementRequest);
                    transaction.setProposer(proposerResponse.getType());
                    final Response<Insured> insuredResponse = partyComponent.retrieveInsured(agreementRequest);
                    transaction.setInsured(insuredResponse.getType());
                }
                if (transactions.getProposal() != null) {
                    transactions.setProposalNumber(agreement.getIdentifier().toString());
                }
                if (agreement.getModifiedDateTime() != null && !(agreement.getModifiedDateTime().toString().equals(""))) {
                    transaction.setLastSyncDate(agreement.getModifiedDateTime());
                } else if (agreement.getCreationDateTime() != null
                        && !("".equals(agreement.getCreationDateTime().toString()))) {
                    transaction.setLastSyncDate(agreement.getCreationDateTime());
                }
                // illustration id retrieve
                final Set<Agreement> referenceAgreementList = transaction.getProposal().getReferencedAgreement();
                String key3 = "";
                for (Agreement list : referenceAgreementList) {
                    if (ContextTypeCodeList.ILLUSTRATION.equals(list.getContextId())) {
                        key3 = list.getIdentifier().toString();
                    }
                }
                transaction.setIllustrationId(key3);
                transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
                transactionsList.add(transaction);
            }
        }
        // LOGGER.trace("Inside EAppRepository retrieveAll : transactionsList" + transactionsList.size());
        return transactionsList;
    }
    
    
    public SearchCountResult retrieveByCountEApp(final RequestInfo requestInfo, final SearchCriteria searchCriteria) {

        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE_BY_COUNT);
        statusRequest.setType(statusList);

        final Response<SearchCountResult> searchCountResponse =
                agreementComponent.getAgreementCountforEapp(searchCriteriatRequest, statusRequest);
        SearchCountResult searchCountResult = null;
        if (searchCountResponse.getType() != null) {
            searchCountResult = (SearchCountResult) searchCountResponse.getType();

        }

        return searchCountResult;

    }
    

    public GeneraliTransactions lockOrUnlock(RequestInfo requestInfo, GeneraliTransactions txn) {
        GeneraliTransactions transaction = new GeneraliTransactions();
        StatusData statusData = new StatusData();
        String agentId = "";
        String agentName = "";
        try{
        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(txn.getProposalNumber());
        agreementRequest.setType(insuranceAgreement);
        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        List<AgreementStatusCodeList> statusList =null;
        statusList = getAgreementStatusCodesFor(Constants.STATUS_UPDATE);
        statusRequest.setType(statusList);
        
        final Response<Agreement> response = agreementComponent.retrieveAgreementForIdentifier(agreementRequest, statusRequest);
        Agreement insAgreement = response.getType();
        agentId = ((InsuranceAgreement) insAgreement).getAgentId();
        agentName = insAgreement.getTransactionKeys().getKey6();
        agentName = agentName.split("\\s+")[0];
        if(txn.getLockOrUnlockMode().equals(GeneraliConstants.LOCK)){
            insAgreement.getTransactionKeys().setKey36(txn.getGeneraligent().getGaoCode());
            insAgreement.setModifiedDateTime(LifeEngageComponentHelper.getCurrentdate());
        }else if(txn.getLockOrUnlockMode().equals(GeneraliConstants.UNLOCK)){
            insAgreement.getTransactionKeys().setKey36(StringUtils.EMPTY);
            insAgreement.setModifiedDateTime(LifeEngageComponentHelper.getCurrentdate());
        }if(txn.getLockOrUnlockMode().equals(GeneraliConstants.ASSIGN_GAO)){
            insAgreement.getTransactionKeys().setKey38(txn.getGeneraligent().getGivenName());
            insAgreement.getTransactionKeys().setKey35(txn.getGeneraligent().getZone());
            insAgreement.setModifiedDateTime(LifeEngageComponentHelper.getCurrentdate());
        }if(txn.getLockOrUnlockMode().equals(GeneraliConstants.MARK_COMPLETE)){
            insAgreement.getTransactionKeys().setKey35(StringUtils.EMPTY);
            insAgreement.getTransactionKeys().setKey36(StringUtils.EMPTY);
            insAgreement.getTransactionKeys().setKey38(StringUtils.EMPTY);
            insAgreement.setModifiedDateTime(LifeEngageComponentHelper.getCurrentdate());
            //TO DO pushNotification to be send
        }
        
        agreementRequest.setType(insAgreement);
        agreementComponent.saveAgreement(agreementRequest);
        statusData.setStatus(Constants.SUCCESS);
        statusData.setStatusCode(Constants.SUCCESS_CODE);
        }catch(Exception e){
            statusData.setStatus(Constants.FAILURE);
            statusData.setStatusCode(Constants.FAILURE_CODE);
            statusData.setStatusMessage(e.getMessage());
        }finally{
            transaction.setProposalNumber(txn.getProposalNumber());
            transaction.setAgentId(agentId);
            transaction.setKey6(agentName);
            transaction.setStatusData(statusData);
        }
        return transaction;
    }
    
    public void updateGAOLockStatus(){
        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, "");
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        Date idleDate = new Date();
        Calendar c = Calendar.getInstance(); // starts with today's date and time
        c.add(Calendar.DAY_OF_YEAR, (0-Integer.parseInt(unlockIdleTime)));
        idleDate = c.getTime();
        insuranceAgreement.setModifiedDateTime(idleDate);
        agreementRequest.setType(insuranceAgreement);
        
        List<Agreement> expiredAgreements = agreementComponent.getAggrementsWithExpiredGAOLocks(agreementRequest);
        for(Agreement agreement : expiredAgreements){
            agreement.getTransactionKeys().setKey36(null);
            agreement.getTransactionKeys().setKey37(null);
            agreementRequest.setType(agreement);
            agreementComponent.saveAgreement(agreementRequest);
            
        }
    }
    
    public Transactions retrieveEApp(final String proposalNumber, final String transactionId) {
        LOGGER.trace("LifeEngageEAppRepository.retrieveEApp: proposalNumber" + proposalNumber);
        LOGGER.trace("LifeEngageEAppRepository.retrieveEApp: transactionId" + transactionId);
        final Transactions transaction = new Transactions();
        
       
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.RETRIEVE);

        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, ContextTypeCodeList.EAPP, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(proposalNumber);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementsResponse =
                agreementComponent.getInsuranceAgreement(agreementRequest, statusRequest);
        if (agreementsResponse.getType() != null) {
            final InsuranceAgreement agreement = (InsuranceAgreement) agreementsResponse.getType();
            transaction.setProposal(agreement);

            agreementRequest.setType(agreement);

            getAgreementDetails(transaction, agreementRequest, transactionId);

            if (transaction.getProposal() != null) {
                transaction.setProposalNumber(agreement.getIdentifier().toString());
            }
            transaction.setModifiedTimeStamp(agreement.getModifiedDateTime());
            
            List<String> lookups= new ArrayList<String>();
            lookups.add(GeneraliConstants.TITLE);
            lookups.add(GeneraliConstants.NATIONALITY);
            lookups.add(GeneraliConstants.NATUREOFWORK);
            lookups.add(GeneraliConstants.IDENTITYTYPE);
            lookups.add(GeneraliConstants.WARD);
            lookups.add(GeneraliConstants.BENEFICIARYRELATIONSIP);
            lookups.add(GeneraliConstants.COMPANYNAME);
            Map<String,Map<String,String>> finalLookUpMap = setValuesForLookUp(lookups);
            transaction.setMapForLookUpValues(finalLookUpMap);
        }
        LOGGER.trace("LifeEngageEAppRepository.retrieveEApp: transaction" + transaction);
        return transaction;
    }
    
    public Map<String,Map<String,String>> setValuesForLookUp(List<String> lookups){
    	
    	 List<CodeLookUp> typeLookUps = null; 
    	 Map<String,Map<String,String>> finalLookUpMap = new HashMap<String,Map<String,String>>();
    	 
    	 for(String type:lookups){
    		 typeLookUps = codeLookUpRepository.fetchLookUpData(type,"th","TH");
    		 Map<String,String> codeValues = new HashMap<String,String>();
    		 for (CodeLookUp test : typeLookUps){
    			 codeValues.put(test.getCode(),test.getDescription());
    		 }
    		 finalLookUpMap.put(type, codeValues);
    	 }
    	return finalLookUpMap;
    }

	public List<RiderSequence> retrieveRiderSequence() {
		// TODO Auto-generated method stub
		List<RiderSequence> riderSequence = agreementComponent.retrieveRiderSequence();
		return riderSequence;
	}
    
    public Map<String, String>
            getEAppIllustrationCountForReport(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(getEntityManager(), ContextTypeCodeList.EAPP, null);
        searchCriteriatRequest.setType(searchCriteria);
        final Map<String, String> searchCountResponse =
                agreementComponent.getEAppIllustrationCountForReport(searchCriteriatRequest);
        return searchCountResponse;
    }
    
}
