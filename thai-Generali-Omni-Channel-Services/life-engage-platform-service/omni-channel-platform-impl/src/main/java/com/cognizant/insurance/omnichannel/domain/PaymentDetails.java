package com.cognizant.insurance.omnichannel.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PaymentDetails {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;	
	private String payMode;
	private String payMethod;
	private String payStatus;
	private String payDecision;
	private String tRDate;
	private String transDate;
	private String payDT;
	private BigDecimal payAmount;
	private String tRNumber;
	private String approvalCode;
	private String bankName;
	private String branchName;
	private String payReceiveDate;
	private String bankAccNo;
	private BigDecimal premium;
	private String bankCharge;
	private String receiptNo;
	private String receiptDate;
	private String checkNo;
	private String checkDate;
	private String agentCode;
	private String agentName;
	private String eTRNumber;
	private String eTRDate;
	private String creditCardNo;
	private String remarks;
	private String merchantID;
	private String description;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPayMode() {
		return payMode;
	}
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	public String getPayMethod() {
		return payMethod;
	}
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	public String getPayDecision() {
		return payDecision;
	}
	public void setPayDecision(String payDecision) {
		this.payDecision = payDecision;
	}
	public String gettRDate() {
		return tRDate;
	}
	public void settRDate(String tRDate) {
		this.tRDate = tRDate;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getPayDT() {
		return payDT;
	}
	public void setPayDT(String payDT) {
		this.payDT = payDT;
	}
	public BigDecimal getPayAmount() {
		return payAmount;
	}
	public void setPayAmount(BigDecimal payAmount) {
		this.payAmount = payAmount;
	}
	public String gettRNumber() {
		return tRNumber;
	}
	public void settRNumber(String tRNumber) {
		this.tRNumber = tRNumber;
	}
	public String getApprovalCode() {
		return approvalCode;
	}
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getDesc() {
		return description;
	}
	public void setDesc(String desc) {
		this.description = desc;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getPayReceiveDate() {
		return payReceiveDate;
	}
	public void setPayReceiveDate(String payReceiveDate) {
		this.payReceiveDate = payReceiveDate;
	}
	public String getBankAccNo() {
		return bankAccNo;
	}
	public void setBankAccNo(String bankAccNo) {
		this.bankAccNo = bankAccNo;
	}
	public BigDecimal getPremium() {
		return premium;
	}
	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}
	public String getBankCharge() {
		return bankCharge;
	}
	public void setBankCharge(String bankCharge) {
		this.bankCharge = bankCharge;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getCheckNo() {
		return checkNo;
	}
	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}
	public String getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String geteTRNumber() {
		return eTRNumber;
	}
	public void seteTRNumber(String eTRNumber) {
		this.eTRNumber = eTRNumber;
	}
	public String geteTRDate() {
		return eTRDate;
	}
	public void seteTRDate(String eTRDate) {
		this.eTRDate = eTRDate;
	}
	public String getCreditCardNo() {
		return creditCardNo;
	}
	public void setCreditCardNo(String creditCardNo) {
		this.creditCardNo = creditCardNo;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}


}
