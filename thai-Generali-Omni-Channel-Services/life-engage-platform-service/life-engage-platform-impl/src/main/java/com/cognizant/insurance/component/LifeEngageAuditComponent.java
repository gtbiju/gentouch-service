/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Nov 29, 2013
 */
package com.cognizant.insurance.component;

import java.text.ParseException;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;

// TODO: Auto-generated Javadoc
/**
 * The Interface interface LifeEngageAuditComponent.
 * 
 * @author 300797
 */
public interface LifeEngageAuditComponent {

    /**
     * Savelife engage audit.
     *
     * @param requestInfo the request info
     * @param json the json
     * @return the life engage audit
     * @throws ParseException the parse exception
     */
    LifeEngageAudit savelifeEngageAudit(RequestInfo requestInfo, String json) throws ParseException;
    
    /**
     * Savelife engage payload audit.
     *
     * @param transactions the transactions
     * @param transactionId the transaction id
     * @param json the json
     * @param lifeEngageAudit the life engage audit
     */
    void savelifeEngagePayloadAudit(Transactions transactions,
            String transactionId, String json, LifeEngageAudit lifeEngageAudit);
    
    /**
     * Save life engage data wipe audit.
     * 
     * @param json
     *            the json
     * @param requestInfo
     *            the request info
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     */
    String saveLifeEngageDataWipeAudit(String json, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit);

    /**
     * Save audit.
     *
     * @param transactions the transactions
     * @param transactionId the transaction id
     * @param json the json
     * @param lifeEngageAudit the life engage audit
     * @param eAppResponse the e app response
     */
    void saveAudit(Transactions transactions, String transactionId, String json, LifeEngageAudit lifeEngageAudit,
            String eAppResponse);
}
