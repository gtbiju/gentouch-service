/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 19, 2015
 */
package com.cognizant.insurance.generic.component;

import java.util.List;

import com.cognizant.insurance.pushnotification.ApplicationDetails;
import com.cognizant.insurance.pushnotification.DeviceRegister;
import com.cognizant.insurance.pushnotification.SNSDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;

/**
 * The Interface interface PushNotificationComponent.
 */
public interface PushNotificationComponent {

    /**
     * Register device.
     * 
     * @param request
     *            the request
     * @return the string
     */
    void registerDevice(Request<DeviceRegister> request);

    /**
     * Checks if is device id exists.
     * 
     * @param request
     *            the request
     * @return true, if is device id exists
     */
    boolean isDeviceIdExists(Request<DeviceRegister> request);

    /**
     * Un register device.
     * 
     * @param request
     *            the request
     * @return the string
     */
    void unRegisterDevice(Request<DeviceRegister> request);

    /**
     * Gets the device details.
     * 
     * @param request
     *            the request
     * @return the device details
     */
    Response<List<DeviceRegister>> getDeviceDetails(Request<DeviceRegister> request);

    /**
     * Gets the application credentials.
     * 
     * @param request
     *            the request
     * @return the application credentials
     */
    Response<List<ApplicationDetails>> getApplicationDetails(Request<ApplicationDetails> request);

    /**
     * Save push notification details.
     * 
     * @param snsServiceRq
     *            the sns service rq
     */
    void savePushNotificationDetails(Request<SNSDetails> snsServiceRq);

}
