/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.observation;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;

/**
 * The Class class Observation.
 *
 * @author 345166
 * 
 */
@Entity
public class Observation {
	
    /** The id. */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Temporal(TemporalType.DATE)
	private Date creationDate;
    
    /** The context id. */
    @Enumerated
    private ContextTypeCodeList contextId;
	
	private String page;
	
	private String path;
	
	private String customerName;
	
	private String email;
	
	private String agent;
	
	private String customerDbId;
	
	private String customerDeviceId;
	
	private String customerId;
	
	@Column(length=15)
	private String subOrdinateId;
	
	private String subModule;

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
     * Gets the id.
     *
     * @return Returns the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets The id.
     *
     * @param id The id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the context.
     *
     * @return Returns the context.
     */
    public final ContextTypeCodeList getContextId() {
        return contextId;
    }

    /**
     * Sets The context.
     *
     * @param context The context to set.
     */
    public void setContextId(final ContextTypeCodeList contextId) {
        this.contextId = contextId;
    }

    /**
	 * @return the page
	 */
	public String getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(final String page) {
		this.page = page;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(final String path) {
		this.path = path;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(final String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * @param agent the agent to set
	 */
	public void setAgent(final String agent) {
		this.agent = agent;
	}

	/**
	 * @return the customerDbId
	 */
	public String getCustomerDbId() {
		return customerDbId;
	}

	/**
	 * @param customerDbId the customerDbId to set
	 */
	public void setCustomerDbId(final String customerDbId) {
		this.customerDbId = customerDbId;
	}

	/**
	 * @return the customerDeviceId
	 */
	public String getCustomerDeviceId() {
		return customerDeviceId;
	}

	/**
	 * @param customerDeviceId the customerDeviceId to set
	 */
	public void setCustomerDeviceId(final String customerDeviceId) {
		this.customerDeviceId = customerDeviceId;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(final String customerId) {
		this.customerId = customerId;
	}

	public String getSubOrdinateId() {
		return subOrdinateId;
	}

	public void setSubOrdinateId(String subOrdinateId) {
		this.subOrdinateId = subOrdinateId;
	}

	
	public String getSubModule() {
		return subModule;
	}

	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}

		
}
