/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.dao;

import com.cognizant.insurance.dao.Dao;
import com.cognizant.insurance.lookup.CountryDetails;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.vo.CountryLookUpResponse;

/**
 * The Interface interface CountryDetailsDao.
 *
 * @author 300797
 */
public interface CountryDetailsDao extends Dao {
    
   Response<CountryLookUpResponse> retrieve(Request<CountryDetails> request);

   Response<CountryLookUpResponse>retrieveCountryOrCountrySubDivisionList(Request<CountryDetails> request);

}
