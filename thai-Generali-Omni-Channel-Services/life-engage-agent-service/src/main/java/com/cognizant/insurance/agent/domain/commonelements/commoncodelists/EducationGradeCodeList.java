/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of EducationCertificates according to their
 * grade.
 * 
 * @author 301350
 * 
 * 
 */
public enum EducationGradeCodeList {
    /**
     * Identifies an education certificate with grade 'magna cum laude'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    MagnaCumLaude,
    /**
     * Identifies an education certificate with grade 'honors'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Honours,
    /**
     * Identifies an education certificate with grade 'black belt'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    BlackBelt,
    /**
     * Identifies an education certificate with grade 'pass'.
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Pass,
    /**
     * Identifies an education certificate with grade 'brown belt'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    BrownBelt
}
