/**
 * 
 */
package com.cognizant.insurance.omnichannel.jpa.component;

import java.util.Date;
import java.util.List;

import com.cognizant.insurance.omnichannel.domain.CheckinData;
import com.cognizant.insurance.omnichannel.domain.CheckinData.Status;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;

/**
 * @author 397850
 *
 */
public interface CheckedinDetailsComponent {
	
	public Response<CheckinData> save(Request<CheckinData> request);

	public CheckinData retriveCheckedinDetails(Request<CheckinData> checkedinRequest, String agentId,
			Status status);

	public void update(Request<CheckinData> checkedinRequest, CheckinData retirevedData);

	public List<CheckinData> retireveCheckinList(Request<CheckinData> checkedinRequest,
			Status status, Request<List<String>> statusRequest);

	public void updateCheckinDetails(Request<CheckinData> checkedinRequest, Date date);

}
