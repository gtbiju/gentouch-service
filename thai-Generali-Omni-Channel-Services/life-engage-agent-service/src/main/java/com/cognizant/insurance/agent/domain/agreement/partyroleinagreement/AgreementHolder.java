/**
 * 
 */
package com.cognizant.insurance.agent.domain.agreement.partyroleinagreement;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;

import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.AgreementHolderRejectionReasonCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;

/**
 * The party (e.g. person or organization) with rightful possession of an agreement (e.g. insurance contract), usually
 * the "contract holder". SOURCE: Barron's
 * 
 * A party who holds/owns an agreement (e.g. insurance policy).
 * 
 * e.g: John Doe, as policyholder of an automobile comprehensive cover policy.
 * 
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("AGREEMENTHOLDER")
public class AgreementHolder extends PartyRoleInAgreement {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5055328017225578821L;

    /**
     * The reason given by the policyholder for his rejection of the conditions stated in the financial services
     * agreement.
     * 
     * e.g: Decided for a different agreement
     * 
     * e.g: Decided for a different insurer
     * 
     * 
     * 
     */
    @Enumerated
    private AgreementHolderRejectionReasonCodeList agreementHolderRejectionReasonCode;
    
	/**
	 * Indicates if the proposer is different from the Insured.
	 */
	private Boolean proposerDifferentFromInsuredIndicator;

	/* Added for vietnam */
   
	private String type;

	public Boolean getProposerDifferentFromInsuredIndicator() {
        return proposerDifferentFromInsuredIndicator;
    }

    public void setProposerDifferentFromInsuredIndicator(Boolean proposerDifferentFromInsuredIndicator) {
        this.proposerDifferentFromInsuredIndicator = proposerDifferentFromInsuredIndicator;
    }

    /**
     * Gets the agreementHolderRejectionReasonCode.
     * 
     * @return Returns the agreementHolderRejectionReasonCode.
     */
    public AgreementHolderRejectionReasonCodeList getAgreementHolderRejectionReasonCode() {
        return agreementHolderRejectionReasonCode;
    }

    /**
     * Sets The agreementHolderRejectionReasonCode.
     * 
     * @param agreementHolderRejectionReasonCode
     *            The agreementHolderRejectionReasonCode to set.
     */
    public void setAgreementHolderRejectionReasonCode(
            final AgreementHolderRejectionReasonCodeList agreementHolderRejectionReasonCode) {
        this.agreementHolderRejectionReasonCode = agreementHolderRejectionReasonCode;
    }

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
