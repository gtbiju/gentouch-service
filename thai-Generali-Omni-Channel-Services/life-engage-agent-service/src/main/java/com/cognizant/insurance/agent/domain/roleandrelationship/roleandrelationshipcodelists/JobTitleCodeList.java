/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists;

/**
 * Identifies a classification of Employees according to their job title.
 * 
 * @author 301350
 * 
 * 
 */
public enum JobTitleCodeList {
    /**
     * Identifies an Employee with job title 'Chief Executive Officer'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    ChiefExecutiveOfficer,
    /**
     * Identifies an employee with job title 'Clerk'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Clerk,
    /**
     * Identifies an employee with job title 'Managing director'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    ManagingDirector,
    /**
     * Identifies an employee with job title 'Marketing manager'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    MarketingManager,
    /**
     * 
     * 
     * 
     * 
     */
    FieldOperationsRiskManager,
    /**
     * 
     * 
     * 
     * 
     */
    GeneralOperationsRiskManager
}
