/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.familymembersubtypes;

import javax.persistence.Entity;

import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.FamilyMember;

/**
 * This concept represents a sibling. 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Sibling extends FamilyMember {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3133287540540923512L;
}
