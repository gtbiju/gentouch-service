/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of EducationCertificates according to their
 * title.
 * 
 * @author 301350
 * 
 * 
 */
public enum EducationCertificateTitleCodeList {
    /**
     * Bachelor of Science.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    BSc,
    /**
     * Chartered Life Underwriter.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    CLU,
    /**
     * Fellow of the Chartered Insurance Institute.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    FCII,
    /**
     * Doctor of Philosophy.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    PhD
}
