package com.cognizant.le.manager;

public interface LeadManager {

	public String getLeads(String agentId);
	
	public String getLeadsAndUpdate(String[] leadIds, String reAssignAgentId, String agentId);

}
