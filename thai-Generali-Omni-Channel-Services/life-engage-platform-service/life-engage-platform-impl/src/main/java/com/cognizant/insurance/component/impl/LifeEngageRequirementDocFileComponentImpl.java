/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 325754
 * @version       : 0.1, Oct 6, 2015
 */
package com.cognizant.insurance.component.impl;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.LifeEngageRequirementDocFileComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.RequirementDocumentFileRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;

/**
 * @author 325754
 * 
 */
@Service("lifeEngageRequirementComponent")
public class LifeEngageRequirementDocFileComponentImpl implements LifeEngageRequirementDocFileComponent {

    /** The document repository. */
    @Autowired
    private RequirementDocumentFileRepository requirementDocumentFileRepository;

    /** The document upload holder. */
    @Autowired
    @Qualifier("uploadRequirementDocumentFileMapping")
    private LifeEngageSmooksHolder requirementDocumentFileUploadHolder;

    @Autowired
    @Qualifier("getRequirementDocumentFileMapping")
    private LifeEngageSmooksHolder requirementDocumentFileRetrieveHolder;

    @Autowired
    @Qualifier("getRequirementDocumentFileListMapping")
    private LifeEngageSmooksHolder requirementDocumentFileListRetrieveHolder;
    
    @Autowired
    @Qualifier("saveRequirementFileMapping")
    private LifeEngageSmooksHolder saveRequirementHolder;

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageRequirementDocFileComponentImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageRequirementDocFileComponent#uploadRequirementDocFile(org.json.JSONObject
     * )
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String uploadRequirementDocFile(String jsonObj, String transactionId) throws BusinessException, IOException {
        String uploadResponse = null;
        Transactions transactions = new Transactions();
        try {
            transactions = (Transactions) requirementDocumentFileUploadHolder.parseJson(jsonObj);

            requirementDocumentFileRepository.saveRequirementDocFile(transactions, transactionId);

            transactions.setStatusData(getStatusDatObject(LifeEngageComponentHelper.SUCCESS,
                    LifeEngageComponentHelper.SUCCESS_CODE, "Successfully uploaded document file"));
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_104);

        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_103);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        }
        uploadResponse = requirementDocumentFileUploadHolder.parseBO(transactions);
        return uploadResponse;
    }

    /**
     * Gets the status dat object.
     * 
     * @param status
     *            the status
     * @param statusCode
     *            the status code
     * @param statusMessage
     *            the status message
     * @return the status dat object
     */

    protected StatusData getStatusDatObject(final String status, final String statusCode, final String statusMessage) {
        final StatusData statusData = new StatusData();
        statusData.setStatus(status);
        statusData.setStatusCode(statusCode);
        statusData.setStatusMessage(statusMessage);
        return statusData;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageRequirementDocFileComponent#getRequirementDocFile(java.lang.String,
     * java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String getRequirementDocFile(JSONObject jsonObj, String transactionId) {
        String getResponse = null;
        Transactions responseTransactions = new Transactions();
        try {
            boolean canProceed = false;
            if (E_APP.equals(jsonObj.getString(TYPE))) {
                if (jsonObj.has(LifeEngageComponentHelper.KEY_4)
                        && jsonObj.getString(LifeEngageComponentHelper.KEY_4) != null
                        && !"".equals(jsonObj.getString(LifeEngageComponentHelper.KEY_4))
                        && !jsonObj.getString(LifeEngageComponentHelper.KEY_4).equals(Constants.NULL))
                    canProceed = true;
            } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
                if (jsonObj.has(LifeEngageComponentHelper.KEY_3)
                        && jsonObj.getString(LifeEngageComponentHelper.KEY_3) != null
                        && !"".equals(jsonObj.getString(LifeEngageComponentHelper.KEY_3))
                        && !jsonObj.getString(LifeEngageComponentHelper.KEY_3).equals(Constants.NULL))
                    canProceed = true;

            }
            if (canProceed) {
                responseTransactions =
                        (Transactions) requirementDocumentFileRetrieveHolder.parseJson(jsonObj.toString());
                Set<Requirement> responseReq = new HashSet<Requirement>();
                // List<AgreementDocument> requirementDocuments = new ArrayList<AgreementDocument>();
                Requirement req =
                        requirementDocumentFileRepository.getRequirementDocFile(responseTransactions, transactionId);
                // req.setRequirementDocuments(requirementDocuments);
                responseReq.add(req);
                responseTransactions.setRequirements(responseReq);
                responseTransactions.setStatusData(getStatusDatObject(LifeEngageComponentHelper.SUCCESS,
                        LifeEngageComponentHelper.SUCCESS_CODE, "Successfully retrieved document file"));
                getResponse = requirementDocumentFileRetrieveHolder.parseBO(responseTransactions);
            }
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_105);
            getResponse = requirementDocumentFileRetrieveHolder.parseBO(responseTransactions);

        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_103);
                getResponse = requirementDocumentFileRetrieveHolder.parseBO(responseTransactions);
            } else {
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
                getResponse = requirementDocumentFileRetrieveHolder.parseBO(responseTransactions);
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
                getResponse = requirementDocumentFileRetrieveHolder.parseBO(responseTransactions);
            } else {
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
                getResponse = requirementDocumentFileRetrieveHolder.parseBO(responseTransactions);
            }

        } catch (IOException e) {
            LOGGER.error("IO Exception : ", e);
            LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_105);
        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_100);
            getResponse = requirementDocumentFileRetrieveHolder.parseBO(responseTransactions);
        }
        return getResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageRequirementDocFileComponent#getRequirementDocFileList(org.json.JSONObject
     * , java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String getRequirementDocFileList(JSONObject jsonObj, String transactionId) {
        String getResponse = null;
        Transactions responseTransactions = new Transactions();
        try {

            if (jsonObj.has(LifeEngageComponentHelper.KEY_4)
                    && jsonObj.getString(LifeEngageComponentHelper.KEY_4) != null
                    && !"".equals(jsonObj.getString(LifeEngageComponentHelper.KEY_4))
                    && !jsonObj.getString(LifeEngageComponentHelper.KEY_4).equals(Constants.NULL)) {

                responseTransactions =
                        (Transactions) requirementDocumentFileRetrieveHolder.parseJson(jsonObj.toString());
                Set<Requirement> responseReq = new HashSet<Requirement>();
                // List<AgreementDocument> requirementDocuments = new ArrayList<AgreementDocument>();
                Requirement req =
                        requirementDocumentFileRepository.getRequirementDocFileList(responseTransactions, transactionId);
                // req.setRequirementDocuments(requirementDocuments);
                responseReq.add(req);
                responseTransactions.setRequirements(responseReq);
                responseTransactions.setStatusData(getStatusDatObject(LifeEngageComponentHelper.SUCCESS,
                        LifeEngageComponentHelper.SUCCESS_CODE, "Successfully retrieved document file"));
            }
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_104);

        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_103);
            } else {
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }

        } catch (IOException e) {
            LOGGER.error("IO Exception : ", e);
            LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_100);
        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_100);
        }
        getResponse = requirementDocumentFileListRetrieveHolder.parseBO(responseTransactions);
        return getResponse;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageRequirementDocFileComponent#uploadRequirementDocFile(org.json.JSONObject
     * )
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String saveRequirement(String jsonObj, String transactionId) throws BusinessException, IOException {
        String uploadResponse = null;
        Transactions transactions = new Transactions();
        try {
            transactions = (Transactions) saveRequirementHolder.parseJson(jsonObj);

            requirementDocumentFileRepository.saveRequirement(transactions, transactionId);

            transactions.setStatusData(getStatusDatObject(LifeEngageComponentHelper.SUCCESS,
                    LifeEngageComponentHelper.SUCCESS_CODE, "Successfully uploaded requirement file"));
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_104);

        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_103);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        }
        uploadResponse = saveRequirementHolder.parseBO(transactions);
        return uploadResponse;
    }

    
}
