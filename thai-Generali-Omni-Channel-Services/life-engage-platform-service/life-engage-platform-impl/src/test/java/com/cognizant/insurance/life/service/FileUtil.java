package com.cognizant.insurance.life.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class FileUtil {
	
	public static void write(String fileName,String content){
		File file=new File(fileName);
		PrintWriter printWriter=null;
		try {
			printWriter=new PrintWriter(file);
			printWriter.println(content);
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		finally{
			printWriter.close();
		}
		
	}

}
