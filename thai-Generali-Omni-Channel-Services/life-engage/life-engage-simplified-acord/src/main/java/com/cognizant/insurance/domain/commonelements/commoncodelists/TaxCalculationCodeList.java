/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * A code list of how taxation has been taken into consideration when
 * calculating the values represented by analytical measures.
 * 
 * 
 * @author 301350
 * 
 * 
 */
public enum TaxCalculationCodeList {
    /**
     * Identifies a scenario with tax status 'Gross', specifying that the
     * analytical measures to which the applies represent amounts before the
     * effect of taxation.
     * 
     * 
     * 
     */
    Gross,
    /**
     * Identifies a with tax status 'Net', specifying that the analytical
     * measures to which the applies represent amounts after the effect of
     * taxation.
     * 
     * 
     * 
     */
    Net
}
