/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 420118
 * @version       : 0.1, Dec 20, 2017
 */

package com.cognizant.insurance.domain.product;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class JurisdictionCode.
 */
@Entity
@Table(name = "CORE_JURISDICTION_LOOKUP")
@NamedQueries({
    @NamedQuery(name = "JurisdictionCode.retrieveAllJurisdictionsByFilter", query = "select jc from JurisdictionCode jc where (:code is null OR jc.jurisdictionId.code = :code) and jc.jurisdictionId.carrier.id = :carrierId")
}
)
public class JurisdictionCode implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4448759300742319722L;

	/** The description. */
	@Column(name="DESCRIPTION")
	private String description;
	
	/** The jurisdiction id. */
	@EmbeddedId
	private JurisdictionID jurisdictionId;

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof JurisdictionCode){
			if(null!=obj && ((JurisdictionCode)obj).getJurisdictionId().getCode().equals(this.getJurisdictionId().getCode())){
				return true;
			}
		}
		return false;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return getJurisdictionId().getCode().length() * 13;
	}


	/**
	 * Gets the jurisdiction id.
	 *
	 * @return the jurisdiction id
	 */
	public JurisdictionID getJurisdictionId() {
		return jurisdictionId;
	}

	/**
	 * Sets the jurisdiction id.
	 *
	 * @param jurisdictionId the new jurisdiction id
	 */
	public void setJurisdictionId(JurisdictionID jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}
}
