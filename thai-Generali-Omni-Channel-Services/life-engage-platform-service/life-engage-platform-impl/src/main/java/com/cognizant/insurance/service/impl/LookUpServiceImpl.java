/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.component.repository.LookUpRepository;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.vo.CountryLookUpResponse;
import com.cognizant.insurance.response.vo.ResponseInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.services.LookUpService;

/**
 * The Class class LookUpServiceImpl.
 * 
 * @author 300797
 */
@Service("lookUpService")
public class LookUpServiceImpl implements LookUpService {

    /** The LookUp repository. */
    @Autowired
    private LookUpRepository lookUpRepository;

    /** The country lookup holder. */
    @Autowired
    @Qualifier("countryLookupMapping")
    private LifeEngageSmooksHolder countryLookupHolder;

    /** The country listing lookup holder. */
    @Autowired
    @Qualifier("countryListingMapping")
    private LifeEngageSmooksHolder countryListingHolder;

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#save(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String getCountrySpecificDetails(final String json) throws BusinessException {

        JSONObject jsonObject;
        String countryId = null;
        String countryName = null;
        String countryLookUpReturn = null;
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonTransactionArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);

            if (jsonTransactionArray.length() > 0) {
                for (int i = 0; i < jsonTransactionArray.length(); i++) {
                    final JSONObject jsonObj = jsonTransactionArray.getJSONObject(i);
                    final JSONObject jsonTxDataObj = jsonObj.getJSONObject(TRANSACTION_DATA);

                    countryId = jsonTxDataObj.getString("Id");
                    countryName = jsonTxDataObj.getString("Name");
                }
            }
            Response<CountryLookUpResponse> response = null;
            response =
                    lookUpRepository.getCountrySpecificDetails(Integer.parseInt(countryId), countryName, requestInfo);
            final CountryLookUpResponse countryLookUpResponse = response.getType();
            countryLookUpResponse.setResponseInfo(buildResponseInfo(requestInfo));
            countryLookUpReturn = countryLookupHolder.parseBO(countryLookUpResponse);

        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
        }

        return countryLookUpReturn;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LookUpService#getCountryOrCountrySubDivisionList(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public final String getCountryOrCountrySubDivisionList(final String json) throws BusinessException {

        String countryLookUpReturn = null;
        String subDivisionType = null;
        Integer parentId = -1;

        Response<CountryLookUpResponse> response = null;

        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(json);

            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonTransactionArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);

            if (jsonTransactionArray.length() > 0) {
                for (int i = 0; i < jsonTransactionArray.length(); i++) {
                    final JSONObject jsonObj = jsonTransactionArray.getJSONObject(i);
                    final JSONObject jsonTxDataObj = jsonObj.getJSONObject(TRANSACTION_DATA);

                    subDivisionType = jsonTxDataObj.getString("subDivisionType");
                    // Setting the Id to ParentId as this will be used in the search
                    parentId = jsonTxDataObj.getInt("Id");
                }
            }
            response = lookUpRepository.getCountryOrCountrySubDivisionList(subDivisionType, parentId, requestInfo);
            final CountryLookUpResponse countryLookUpResponse = response.getType();
            countryLookUpResponse.setResponseInfo(buildResponseInfo(requestInfo));
            countryLookUpReturn = countryListingHolder.parseBO(countryLookUpResponse);

        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
        }
        return countryLookUpReturn;
    }

    
    /**
     * Builds the response info.
     * 
     * @param requestInfo
     *            the request info
     * @return the response info
     */
    private ResponseInfo buildResponseInfo(final RequestInfo requestInfo) {
        final ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setCreationDateTime(requestInfo.getCreationDateTime());
        responseInfo.setRequestorToken(requestInfo.getRequestorToken());
        responseInfo.setSourceInfoName(requestInfo.getSourceInfoName());
        responseInfo.setTransactionId(requestInfo.getTransactionId());
        responseInfo.setUserName(requestInfo.getUserName());
        return responseInfo;
    }

    /**
     * @return the lookUpRepository
     */
    public final LookUpRepository getLookUpRepository() {
        return lookUpRepository;
    }

    /**
     * @param lookUpRepository
     *            the lookUpRepository to set
     */
    public final void setLookUpRepository(final LookUpRepository lookUpRepository) {
        this.lookUpRepository = lookUpRepository;
    }

    /**
     * @return the countryLookupHolder
     */
    public final LifeEngageSmooksHolder getCountryLookupHolder() {
        return countryLookupHolder;
    }

    /**
     * @param countryLookupHolder the countryLookupHolder to set
     */
    public final void setCountryLookupHolder(final LifeEngageSmooksHolder countryLookupHolder) {
        this.countryLookupHolder = countryLookupHolder;
    }

    /**
     * @return the countryListingHolder
     */
    public final LifeEngageSmooksHolder getCountryListingHolder() {
        return countryListingHolder;
    }

    /**
     * @param countryListingHolder the countryListingHolder to set
     */
    public final void setCountryListingHolder(final LifeEngageSmooksHolder countryListingHolder) {
        this.countryListingHolder = countryListingHolder;
    }

}
