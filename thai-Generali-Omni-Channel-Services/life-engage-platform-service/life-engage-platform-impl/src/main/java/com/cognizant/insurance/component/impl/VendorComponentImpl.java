package com.cognizant.insurance.component.impl;

import org.json.JSONArray;
import org.springframework.stereotype.Service;

import com.cognizant.insurance.component.VendorAgentComponent;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * The Class VendorComponentImpl.
 */
@Service("lifeEngaAgentComponent")
public class VendorComponentImpl implements VendorAgentComponent {

	/* (non-Javadoc)
	 * @see com.cognizant.insurance.component.VendorAgentComponent#retrieveAgentProfileByCode(com.cognizant.insurance.request.vo.RequestInfo, java.lang.String)
	 */
	@Override
	public String retrieveAgentProfileByCode(RequestInfo requestInfo,
			JSONArray jsonArray) throws BusinessException {
		return "{}";
	}

	@Override
	public String retrieveAgentsbyBranchCode(RequestInfo requestInfo,
			JSONArray jsonArray) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

}
