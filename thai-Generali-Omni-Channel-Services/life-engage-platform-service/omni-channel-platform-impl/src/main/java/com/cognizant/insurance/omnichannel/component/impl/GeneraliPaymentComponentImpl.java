package com.cognizant.insurance.omnichannel.component.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.cognizant.generali.encryption.util.EncryptionUtil;
import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.component.LifeEngageEmailComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.EmailRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.GeneraliPaymentComponent;
import com.cognizant.insurance.omnichannel.component.repository.UserRepository;
import com.cognizant.insurance.omnichannel.dao.GeneraliAgreementDao;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliAgreementDaoImpl;
import com.cognizant.insurance.omnichannel.dao.impl.GeneraliPaymentDaoImpl;
import com.cognizant.insurance.omnichannel.domain.TinyUrlGenerator;
import com.cognizant.insurance.omnichannel.vo.PaymentBO;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.google.gson.Gson;

@Component
public class GeneraliPaymentComponentImpl implements GeneraliPaymentComponent {

    @Value("${generali.platform.payment.paymentValidationURL}")
    private String paymentValidationURL;
    
    @Value("${generali.platform.payment.paymentTransactionURL}")
    private String paymentTransactionURL;
    
    @Value("${generali.platform.payment.paymentStatusURL}")
    private String paymentStatusURL;
    
    @Value("${generali.platform.payment.authenticationURL}")
    private String authenticationURL;
    
    @Value("${generali.platform.payment.userPaymentUrl}")
    private String urlForUser;
    
    @Value("${generali.platform.payment.mailSubject}")
    private String mailSubject;
    
    @Value("${generali.platform.payment.grantType}")
    private String grantType;
    
    @Value("${generali.platform.payment.clientId}")
    private String clientId;
    
    @Value("${generali.platform.payment.clientSecret}")
    private String clientSecret;
    
    @Value("${generali.platform.payment.portalInfo}")
    private String portalInfo;
    
    @Value("${generali.platform.payment.url.expiryDays}")
    private String expiryDate;
    
    @Value("${generali.platform.payment.accessToken}")
    private String accessToken;
    
    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;
    
    @Autowired
    EncryptionUtil encryptionUtil;
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private GeneraliPaymentDaoImpl paymentDao;
    
    @Autowired
    @Qualifier("eAppSaveMapping")
    private LifeEngageSmooksHolder saveEAppHolder;
    
    /** The Constant KEY1. */
    public static final String KEY1 = "Key1";
    
    /** The Constant payment type. */
    public static final String PAYMENTTYPE = "PaymentOption";
    
    public static final String PAYMENTSTATUS = "payment_status";
    
    /** The Constant SUCCESS_CODE. */
    public static final String SUCCESS_CODE = "200";
    
    /** The Constant FAILURE_CODE. */
    public static final String FAILURE_CODE = "100";
    
    /** The Constant TYPE. */
	private static final String PAYMENT_BY_URL = "paymentByUrl";
	
	/** The Constant MAIL_SUBJECT. */
    private static final String MAIL_SUBJECT = "mailSubject";
    
    /** The Constant AGENT_NAME **/
    private static final String AGENT_NAME = "agentName";
	
	private static final String EMAIL_ID = "toMailIds";
	
	/** The Constant CC_MAIL_ID. */
    private static final String CC_MAIL_ID = "ccMailIds";
	
	/** The Constant TYPE. */
	private static final String TYPE = "Type";
	
	/** The Constant TYPE. */
	private static final String PAYMENT_URL = "paymentUrl";
	
	/** The Constant INITIALISED. */
	private static final String INITIALISED = "initialised";
	
	/** The Constant AGENT ID. */
	private static final String AGENT_ID = "agentId";
	
	/** The Constant EMAIL_FROM_ADDRESS. */
	private static final String EMAIL_FROM_ADDRESS = "fromMailId";
	
	/** The Constant ID. */
	private static final String ID = "id";
	
	/** The Constant EMAIL_TEMPLATE. */
	private static final String EMAIL_TEMPLATE = "emailTemplate";
    
    @Autowired
	private EmailRepository emailRepository;
    
    @Autowired
	private LifeEngageEmailComponent lifeEngageEmailComponent;
    
    @Autowired
    private UserRepository userRepository;
    
    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliPaymentComponentImpl.class);

    @Override
    public String retrievePaymentService(String json) throws BusinessException {
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String encryptedJson="";
        encryptedJson=encryptionUtil.encryptString(json);
        HttpEntity<String> httpRequest = new HttpEntity<String>(encryptedJson, headers);
        ResponseEntity<String> response;
        String responseBodyString="";
        try {
            response = restTemplate.postForEntity(paymentValidationURL, httpRequest, String.class);
            String responseBodyJson=response.getBody();
            responseBodyString= new Gson().fromJson(responseBodyJson, String.class);
            
        } catch (RestClientException e) {
            throw new BusinessException("Unable to complete Payment Validation Service request - RestClient Exception", e);
        } catch (Exception e) {
            throw new BusinessException("Unable to complete Payment Validation Service request", e);
        }
        if (response.getStatusCode().value() != 200) {
            LOGGER.error("Vendor Service error : Status " + response.getStatusCode() + " - " + response.getBody());
            throw new BusinessException(
                    "Unable to complete Payment Validation Service request : Failed to process the response");

        }
        return encryptionUtil.decryptString(responseBodyString);
    }
    
        
	@Override
	public String makePaymentViaOnline(RequestInfo requestInfo, String json)
			throws BusinessException {
		System.out.println("Entering make payment Online");
		ResponseEntity<String> response = null;
		String responseBodyString = "";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set(GeneraliConstants.AUTHORIZATION, accessToken);
		HttpEntity<String> httpRequest = new HttpEntity<String>(json,headers);
		try {
			System.out.println("Before External service call");
			response = restTemplate.postForEntity(paymentTransactionURL, httpRequest, String.class);
			System.out.println("After External Service Call Over ");
			responseBodyString = response.getBody().toString();
			System.out.println("The response body String" + responseBodyString);
		} catch (RestClientException e) {
			throw new BusinessException("Unable to complete Payment Validation Service request - RestClient Exception",
					e);
		} catch (Exception e) {
			throw new BusinessException("Unable to complete Payment Validation Service request", e);
		}

		if (response.getStatusCode().value() != 200) {
			LOGGER.error("Vendor Service error : Status " + response.getStatusCode() + " - " + response.getBody());
			throw new BusinessException(
					"Unable to complete Payment Validation Service request : Failed to process the response");

		}
		return responseBodyString;
	}

	@Transactional
	@Override
	public String makePaymentViaURL(JSONObject jsonObject, String transactionId) throws BusinessException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		String responseUrl = StringUtils.EMPTY;
		String result = StringUtils.EMPTY;
		String finalResult = StringUtils.EMPTY;
		JSONObject transactionData = jsonObject.getJSONObject(GeneraliConstants.TRANSACTION_DATA);
		ResponseEntity<String> response;
		String randomUUID = StringUtils.EMPTY;
		if (jsonObject != null) {
			JSONObject paymentInfo = transactionData.getJSONObject(GeneraliConstants.PAYMENT_INFO);
			
			UriComponentsBuilder builderInstance = UriComponentsBuilder.fromHttpUrl(paymentTransactionURL);
			builderInstance.queryParam(GeneraliConstants.PAYMENT_NO, paymentInfo.getString(GeneraliConstants.PAYMENT_NO))
			.queryParam(GeneraliConstants.PAYMENT_PURPOSE,paymentInfo.getString(GeneraliConstants.PAYMENT_PURPOSE))
			.queryParam(GeneraliConstants.FIRSTNAME, paymentInfo.getString(GeneraliConstants.FIRSTNAME))
			.queryParam(GeneraliConstants.LASTNAME, paymentInfo.getString(GeneraliConstants.LASTNAME))
			.queryParam(Constants.EMAIL, paymentInfo.getString(Constants.EMAIL))
			.queryParam(GeneraliConstants.PREMIUM, paymentInfo.getString(GeneraliConstants.PREMIUM))
			.queryParam(GeneraliConstants.COMPANY, paymentInfo.getString(GeneraliConstants.COMPANY))
			.queryParam(GeneraliConstants.GRACE_DUE_DATE,paymentInfo.getString(GeneraliConstants.GRACE_DUE_DATE))
			.queryParam(GeneraliConstants.ALLOW_TO_PAY_SHORTAGE,paymentInfo.getString(GeneraliConstants.ALLOW_TO_PAY_SHORTAGE))
			.queryParam(GeneraliConstants.SOURCE_ID, paymentInfo.getString(GeneraliConstants.SOURCE_ID));
			
			try {
				HttpEntity<String> httpRequest = new HttpEntity<String>(headers);
				System.out.println("Running before the call" + builderInstance.build().toUriString());
				response = restTemplate.exchange(builderInstance.build().toUriString(), HttpMethod.POST, httpRequest, String.class);
				result = response.getBody();
				System.out.println("Running after the call" + result);
				JSONObject responseObject = new JSONObject(result);
				
				/*Dummy Code for Testing
				 JSONObject testJson = new JSONObject();
				testJson.put("results", "https://www.google.com/");
				result = testJson.toString();
				JSONObject responseObject = new JSONObject(testJson.toString());*/
				
				if (responseObject.has(GeneraliConstants.RESULTS)) {
					responseUrl = responseObject.getString(GeneraliConstants.RESULTS);
				}
				randomUUID = getRandomUUID();
				TinyUrlGenerator saveUrl = new TinyUrlGenerator();
				saveUrl.setCreationDateTime(new Date());
				saveUrl.setOriginalUrl(responseUrl);
				saveUrl.setUUID(randomUUID);
				String applicationNumber = jsonObject.getString(GeneraliConstants.KEY21);
				saveUrl.setApplicationNumber(applicationNumber);
				
				final Request<TinyUrlGenerator> saveRequest =
								                new JPARequestImpl<TinyUrlGenerator>(entityManager, ContextTypeCodeList.EAPP,
								                		transactionId);
				saveRequest.setType(saveUrl);
				paymentDao.save(saveRequest);

			} catch (ParseException e) {
				throw new BusinessException("Error in parsing request/response", e);
			} catch(RestClientException e){
				throw new BusinessException("Encountered HTTP errors", e);
			}

			if (response.getStatusCode().value() != 200) {
				LOGGER.error("Vendor Service error : Status " + response.getStatusCode() + " - " + response.getBody());
				throw new BusinessException(
						"Unable to complete Payment Validation Service request : Failed to process the response");

			}
			
			if(jsonObject.getString(Constants.KEY1).equalsIgnoreCase(Constants.SEND_BY_URL)){
				finalResult = randomUUID;
			}
			else {
				finalResult = result;
			}
		}
		return finalResult;
	}

	//@Transactional
	@Override
	public String retrieveAndSendMail(JSONObject jsonObject, String randomUUID, RequestInfo requestInfo) {
		
		JSONObject transactionData = jsonObject.getJSONObject(GeneraliConstants.TRANSACTION_DATA);
		JSONObject resultObject = new JSONObject();
		String agentName = StringUtils.EMPTY;
		if(transactionData != null){
		
		final Request<TinyUrlGenerator> saveRequest =
                new JPARequestImpl<TinyUrlGenerator>(entityManager, ContextTypeCodeList.EAPP,
                		requestInfo.getTransactionId());
		
		TinyUrlGenerator saveUrl = new TinyUrlGenerator();
		String applicationNumber = jsonObject.getString(GeneraliConstants.KEY21);
		saveUrl.setApplicationNumber(applicationNumber);
		saveRequest.setType(saveUrl);
		
		JSONObject paymentInfo = transactionData.getJSONObject(GeneraliConstants.PAYMENT_INFO);
		String dummyUserUrl = StringUtils.EMPTY;
		Long id = paymentDao.retrieveIdForPaymentUrl(saveRequest);

		dummyUserUrl = urlForUser + randomUUID + "/" + id;
		String agentId = jsonObject.getString(Constants.KEY11);
		
		Transactions transactions = new Transactions();
        transactions.setKey1(agentId);
        Response<GeneraliTHUser> generaliTHUser = userRepository.validateLoggedInUser(transactions);
        
		String agentEmail = generaliTHUser.getType().geteMail();
		
		agentName = paymentInfo.getString(GeneraliConstants.FIRSTNAME) + " " + paymentInfo.getString(GeneraliConstants.LASTNAME);
		
		JSONObject transactionObject = new JSONObject();
		transactionObject.put(PAYMENT_URL, dummyUserUrl);
		transactionObject.put(EMAIL_ID,paymentInfo.getString(Constants.EMAIL));
		resultObject.put(GeneraliConstants.TRANSACTION_DATA, transactionObject);
		resultObject.put(Constants.TYPE, PAYMENT_BY_URL);
		resultObject.put(CC_MAIL_ID, agentEmail);
		resultObject.put(AGENT_NAME, agentName);
		resultObject.put(Constants.KEY11, jsonObject.getString(Constants.KEY11));
		
		}
		return resultObject.toString();
	}	
	
	@Transactional(rollbackFor = { Exception.class })
	@Override
	public LifeEngageEmail saveGeneraliEmail(final JSONObject jsonObj) {
		LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
		try {
			
			final JSONObject emailObject = new JSONObject();
			final JSONObject emailTemplateObj = new JSONObject();

			String agentId = jsonObj.getString(Constants.KEY11);

			final JSONObject emailObj = jsonObj.getJSONObject(GeneraliConstants.TRANSACTION_DATA);
			String toMailIds = emailObj.getString(EMAIL_ID);
			String paymentUrl = StringUtils.EMPTY;
			
			String ccMailId = jsonObj.getString(CC_MAIL_ID);
			String type = "";
			type = jsonObj.getString(TYPE);
			
			String agentName = jsonObj.has(AGENT_NAME)?jsonObj.getString(AGENT_NAME):StringUtils.EMPTY;
			if (PAYMENT_BY_URL.equals(jsonObj.getString(TYPE))) {
				if (emailObj.has(PAYMENT_URL)) {
					paymentUrl = (String) emailObj.getString(PAYMENT_URL);
				}
				if("undefined".equals(PAYMENT_URL)){
					paymentUrl="";
				}
			}


			emailObject.put(MAIL_SUBJECT, mailSubject);
			emailObject.put(TYPE, type);
			emailObject.put(AGENT_NAME, agentName);
			emailObject.put(EMAIL_ID, toMailIds);
			emailObject.put(CC_MAIL_ID, ccMailId);
			//emailObject.put(EMAIL_FROM_ADDRESS, fromMailId);
			emailObject.put(AGENT_ID, agentId);
			emailTemplateObj.put(PAYMENT_URL, paymentUrl);
			emailTemplateObj.put(AGENT_NAME, agentName);
			emailObject.put(EMAIL_TEMPLATE, emailTemplateObj);
			lifeEngageEmail.setSendTime(LifeEngageComponentHelper
						.getCurrentdate());
			lifeEngageEmail.setEmailValues(emailObject.toString());
			lifeEngageEmail.setAttemptNumber("1");
			lifeEngageEmail.setStatus(INITIALISED);
			lifeEngageEmail.setAgentId(agentId);
			lifeEngageEmail.setType(type);
			lifeEngageEmail = emailRepository
						.savelifeEngageEmail(lifeEngageEmail);

		} catch (ParseException e) {
			LOGGER.error("**Error while Saving Email Data **" + e.getMessage());
		} catch (Exception e) {
			LOGGER.error("**Error while Saving Email Data**" + e.getMessage());
		}
		
		return lifeEngageEmail;
	}
	
	@Override
	@Async
	@Transactional(rollbackFor = { Exception.class })
	public void triggerEmail(String agentId, String type) {
		try {
			// Triggering Generali Email
			
			List<LifeEngageEmail> lifeEngageEmails = emailRepository
					.getInitialisedLifeEngageEmails(agentId, type);
			for (LifeEngageEmail email : lifeEngageEmails) {
				String emailJson = null;
				String statusFlag = null;
				emailJson = email.getEmailValues();
				final JSONObject emailJSONObj = new JSONObject(emailJson);
				final JSONObject templateObj = emailJSONObj
						.getJSONObject(EMAIL_TEMPLATE);
				StatusData statusData = lifeEngageEmailComponent.sendEmail(
						emailJson, "", templateObj.toString(), "");

				statusFlag = statusData.getStatus();
				final LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
				lifeEngageEmail.setSendTime(LifeEngageComponentHelper
						.getCurrentdate());
				lifeEngageEmail.setId(email.getId());
				lifeEngageEmail.setEmailValues(emailJson);
				lifeEngageEmail.setStatus(statusFlag);
				lifeEngageEmail.setAttemptNumber("1");
				lifeEngageEmail.setStatusMessage(statusData.getStatusMessage());
				lifeEngageEmail.setBatchJobId(null);
				lifeEngageEmail.setType(type);
				lifeEngageEmail.setAgentId(agentId);
				if (Constants.SUCCESS.equals(statusFlag)) {
					statusFlag = Constants.SUCCESS;
					emailRepository.updatelifeEngageEmail(lifeEngageEmail);
				} else {
					statusFlag = Constants.FAILURE;
					emailRepository.updatelifeEngageEmail(lifeEngageEmail);
				}
			}

		} catch (Exception e) {
			LOGGER.error("Error while triggering Email " + e.getMessage());
		}
	}
	
	private String getRandomUUID() {
		UUID idOne = UUID.randomUUID();
		String uuid_lsd = idOne.getLeastSignificantBits() + "";
		uuid_lsd = uuid_lsd.replace("-", "");
		return uuid_lsd;
	}
	
	/*@Override
	public String getCardPaymentStatus(String transactionRefNumber) throws BusinessException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> mediaTypes = new ArrayList<MediaType>();
		mediaTypes.add(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypes);
		String paymentStatus = "";
		String responseBody = StringUtils.EMPTY;
		String resultResponse = StringUtils.EMPTY;
		HttpEntity<String> httpRequest = new HttpEntity<String>(headers);
		ResponseEntity<String> response = null;
		
		paymentStatusURL = paymentStatusURL+"/"+transactionRefNumber;
		
		try {
			response = restTemplate.exchange(paymentStatusURL,
					HttpMethod.GET, httpRequest, String.class);
			responseBody = response.getBody();
			JSONObject respJson = new JSONObject(responseBody);
			if(respJson.has(PAYMENTSTATUS) && !StringUtils.isEmpty(respJson.get(PAYMENTSTATUS).toString())){
				paymentStatus = respJson.get(PAYMENTSTATUS).toString();
			}
			
			//resultResponse =  saveEAppHolder.parseBO(respJson);

		} catch (RestClientException e) {
			throw new BusinessException(e.getMessage());
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		return responseBody;
	}*/
	
	@Override
	public String getUrlPaymentStatus(String json) throws BusinessException {
		LOGGER.info("Payment Status start->");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<MediaType> mediaTypes = new ArrayList<MediaType>();
		mediaTypes.add(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypes);
		String transactionRefNumber= StringUtils.EMPTY;
		String purpose= StringUtils.EMPTY;
		String responseBody = StringUtils.EMPTY;
		
		HttpEntity<String> httpRequest = new HttpEntity<String>(headers);
		ResponseEntity<String> response = null;
		JSONObject jsonObj = null;
		
		
		try {
			jsonObj = new JSONObject(json);
			LOGGER.info("Payment Status request->"+jsonObj);
			transactionRefNumber = jsonObj.has(GeneraliConstants.KEY21)?jsonObj.getString(GeneraliConstants.KEY21):StringUtils.EMPTY;
			purpose = jsonObj.has(GeneraliConstants.KEY20)?jsonObj.getString(GeneraliConstants.KEY20):StringUtils.EMPTY;
			
			UriComponentsBuilder builderInstance = UriComponentsBuilder.fromHttpUrl(paymentStatusURL);
			builderInstance.queryParam(GeneraliConstants.PAYMENT_NO, transactionRefNumber)
			.queryParam(GeneraliConstants.PAYMENT_PURPOSE,purpose);
			
			response = restTemplate.exchange(builderInstance.build().toUriString(),
					HttpMethod.POST, httpRequest, String.class);
		    responseBody = response.getBody();
			
			
			/*Dummy Code for Testing
			 JSONObject testJson = new JSONObject();
			testJson.put("results", "https://www.google.com/");
			responseBody = testJson.toString();*/
		    LOGGER.info("Payment Status Response->"+responseBody);
		    LOGGER.info("Payment Status End->");
			
			
		} catch (RestClientException e) {
			throw new BusinessException(e.getMessage());
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		return responseBody;
	}

	@Override
	public String processNotifaction(String notification)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String processResponse(String status, String transactionId,
			String data) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}
	
	private String buildResponseData(JSONArray jsonTransactionArray,String paymentStatus, String message,String statusCode) {
		String response = "";
		JSONObject transactionValues = jsonTransactionArray.getJSONObject(0);
		JSONObject responseData = new JSONObject();
		JSONObject responseInfo = new JSONObject();
		JSONObject mainResponse = new JSONObject();
		JSONArray transactions = new JSONArray();
		JSONObject transactionData = new JSONObject();
		JSONObject paymentData = new JSONObject();
		JSONObject statusDataObj = new JSONObject();
		JSONObject mainTransactions = new JSONObject();
		                
        paymentData.put("GapAmount", "0");
        paymentData.put("MessageCode", statusCode);
        paymentData.put("Message", message);
        
        statusDataObj.put("StatusCode",statusCode);
        statusDataObj.put("Status", paymentStatus);
        statusDataObj.put("StatusMessage", message);
        
        transactionData.put("PaymentDetails", paymentData);
        transactionData.put("StatusData", statusDataObj);
        transactionValues.put("TransactionData", transactionData);
        transactions.put(transactionValues);
        mainTransactions.put("Transactions", transactions);
        responseData.put("ResponsePayLoad", mainTransactions);
        responseData.put("ResponseInfo", responseInfo);
        mainResponse.put("Response", responseData);
        response = mainResponse.toString();
		return response;
	}
	
	@Override
	public String isRecieptNoExisting(JSONArray jsonTransactionArray,RequestInfo requestInfo) throws BusinessException {
		JSONObject request = new JSONObject();
		String response ="";
		String paymentStatus ="";
		String statusCode="";
		String message ="";
		final Request<PaymentBO> paymentReciptCheckRequest =
             new JPARequestImpl<PaymentBO>(entityManager, ContextTypeCodeList.EAPP,
                     requestInfo.getTransactionId());
		PaymentBO paymentReqObj = new PaymentBO();
    	GeneraliAgreementDao agreementDao = new GeneraliAgreementDaoImpl();
        try {
        	if (jsonTransactionArray.length() > 0) {
        		request = jsonTransactionArray.getJSONObject(0);        		
        	}
        	String recieptNo = request.getString(KEY1);
        	String paymentType = request.getString(PAYMENTTYPE);
        	ArrayList<String> paymentTypeList = new ArrayList<String>();
        	paymentTypeList.add(paymentType);
        	paymentReqObj.setPaymentType(paymentTypeList);
        	paymentReqObj.setRecieptNo(recieptNo);
        	paymentReciptCheckRequest.setType(paymentReqObj);
        	
        	boolean isRecieptExisting = agreementDao.isRecieptExisting(paymentReciptCheckRequest);
        	
        	if(isRecieptExisting){        		
        		paymentStatus = Constants.FAILURE;
        		message = "Payment validation is failure";
        		statusCode = FAILURE_CODE;
        	}else{
        		paymentStatus = Constants.SUCCESS;
        		message = "Payment validation is success";
        		statusCode = SUCCESS_CODE;
        	}
        	
        	
        	response = buildResponseData(jsonTransactionArray,paymentStatus,message,statusCode);        	
        	
        } catch (Exception e) {
			LOGGER.error("Key1 is empty " + e);
		}
		return response;
	}
	
	public String getAuthenticationToken()throws BusinessException{
		
		HttpHeaders headers = new HttpHeaders();
		headers.set(GeneraliConstants.GRANT_TYPE_KEY,grantType);
		headers.set(GeneraliConstants.CLIENT_ID_KEY,clientId);
		headers.set(GeneraliConstants.CLIENT_SECRET_KEY,clientSecret);
		headers.set(GeneraliConstants.SCOPE,portalInfo);
		HttpEntity<String> httpRequest = new HttpEntity<String>(headers);
		ResponseEntity<String> response;
		String responseString = StringUtils.EMPTY;
		JSONObject responseObject;
		String accessToken = StringUtils.EMPTY;
		try {
	        response = restTemplate.postForEntity(authenticationURL, httpRequest, String.class);
	        responseString = response.getBody();
	        responseObject = new JSONObject(responseString);
	        accessToken = responseObject.getString(GeneraliConstants.ACCESS_TOKEN);
	    } catch (RestClientException e) {
	        throw new BusinessException("Unable to retrieve authentication token - RestClient Exception", e);
	    } catch (Exception e) {
	        throw new BusinessException("Unable to complete retrieve authentication Service request", e);
	    }
	    if (response.getStatusCode().value() != 200) {
	        LOGGER.error("Vendor Service error : Status " + response.getStatusCode() + " - " + response.getBody());
	        throw new BusinessException(
	                "Unable to retrieve authentication token Service request : Failed to process the response");

	    }
	    return accessToken;
	}

	@Transactional
	@Override
	public TinyUrlGenerator makePaymentViaUrl(String uuid, String id) {
		TinyUrlGenerator saveUrl = new TinyUrlGenerator();
		saveUrl.setUUID(uuid);
		saveUrl.setId(Long.valueOf(id));
		String transactionId = "1050urlPaymentTransaction";
		final Request<TinyUrlGenerator> saveRequest =
                new JPARequestImpl<TinyUrlGenerator>(entityManager, ContextTypeCodeList.EAPP,transactionId);
		saveRequest.setType(saveUrl);
		
		TinyUrlGenerator result = paymentDao.retrieveOriginalUrlForPayment(saveRequest);
		
		return result;
	}

}
