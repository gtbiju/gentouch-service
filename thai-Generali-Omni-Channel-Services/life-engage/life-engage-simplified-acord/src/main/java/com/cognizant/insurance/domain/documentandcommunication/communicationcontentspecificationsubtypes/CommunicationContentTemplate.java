/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.domain.documentandcommunication.communicationcontentspecificationsubtypes;

import javax.persistence.Entity;

import com.cognizant.insurance.domain.documentandcommunication.CommunicationContentSpecification;

/**
 * A template comprised of one or more text or graphical components. Text components include static text blocks and
 * dynamic text blocks.
 * 
 * e.g: The form used to fill in auto claims.
 * 
 * @author 300797
 * 
 * 
 */

@Entity
public class CommunicationContentTemplate extends CommunicationContentSpecification {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2601537824856028113L;
}
