package com.cognizant.insurance.omnichannel.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class LifeAsiaRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String spajNo;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	private String status;

	private String documentId;

	@Temporal(TemporalType.TIMESTAMP)
	private Date responseTime;

	@Column(columnDefinition="NVARCHAR(MAX)")
	private String requestData;

	@Column(columnDefinition="NVARCHAR(MAX)")
	private String responseData;

	private int retryAttempt;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastAttempt;
	
	private String eAppId;
	
	private String agentId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSpajNo() {
		return spajNo;
	}
	public void setSpajNo(String spajNo) {
		this.spajNo = spajNo;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	public Date getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}
	public String getRequestData() {
		return requestData;
	}
	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}
	public String getResponseData() {
		return responseData;
	}
	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}
	public int getRetryAttempt() {
		return retryAttempt;
	}
	public void setRetryAttempt(int retryAttempt) {
		this.retryAttempt = retryAttempt;
	}
	public Date getLastAttempt() {
		return lastAttempt;
	}
	public void setLastAttempt(Date lastAttempt) {
		this.lastAttempt = lastAttempt;
	}
	
	
	public String geteAppId() {
		return eAppId;
	}
	public void seteAppId(String eAppId) {
		this.eAppId = eAppId;
	}
	


	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
}



