/**
 * 
 */
package com.cognizant.insurance.omnichannel.vo;

import java.util.Date;

import com.cognizant.insurance.omnichannel.domain.CheckinData;

// TODO: Auto-generated Javadoc
/**
 * The Class CheckinResponse.
 *
 * @author 397850
 */
public class CheckinResponse {
	
	/** The agent code. */
	private String agentCode;
	
	/** The agent name. */
	private String agentName;
	
	/** The role. */
	private String role;
	
	/** The status. */
	private String status;
	
	/** The termination date. */
	private Date terminationDate;
	
	/** The joining date. */
	private Date joiningDate;
	
	private CheckinData checkinData;

	/**
	 * Gets the agent code.
	 *
	 * @return the agent code
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * Sets the agent code.
	 *
	 * @param agentCode the new agent code
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * Gets the agent name.
	 *
	 * @return the agent name
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * Sets the agent name.
	 *
	 * @param agentName the new agent name
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the termination date.
	 *
	 * @return the termination date
	 */
	public Date getTerminationDate() {
		return terminationDate;
	}

	/**
	 * Sets the termination date.
	 *
	 * @param terminationDate the new termination date
	 */
	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	/**
	 * Gets the joining date.
	 *
	 * @return the joining date
	 */
	public Date getJoiningDate() {
		return joiningDate;
	}

	/**
	 * Sets the joining date.
	 *
	 * @param joiningDate the new joining date
	 */
	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}

	/**
	 * @param checkinData the checkinData to set
	 */
	public void setCheckinData(CheckinData checkinData) {
		this.checkinData = checkinData;
	}

	/**
	 * @return the checkinData
	 */
	public CheckinData getCheckinData() {
		return checkinData;
	}

	
}
