/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 22, 2013
 */
package com.cognizant.insurance.life.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.sql.DataSource;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.dbutils.QueryRunner;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.LifeEngageSyncService;

/**
 * The Class class TestLifeEngageIllustrationService.
 * 
 * @author 300797
 */
@ContextConfiguration("/service-context-test.xml")
public class TestLifeEngageIllustrationService extends AbstractJUnit4SpringContextTests {

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String RESPONSE_PAYLOAD = "ResponsePayload";

    /** The Constant KEY2. */
    private static final String KEY11 = "Key11";

    /** The Constant KEY7. */
    private static final String KEY15 = "Key15";
    
    /** The Constant KEY12. */
    private static final String KEY12 = "Key12";
    
    /** The Constant KEY16. */
    private static final String KEY16 = "Key16";

    /** The Constant KEY17. */
    private static final String KEY17 = "Key17";

    /** The Constant KEY3. */
    private static final String KEY3 = "Key3";

    /** The Constant STATUS_DATA. */
    private static final String STATUS_DATA = "StatusData";

    /** The Constant STATUS_CODE. */
    private static final String STATUS_CODE = "StatusCode";

    /** The Constant STATUS. */
    private static final String STATUS = "Status";

    /** The Constant STATUS_MESSAGE. */
    private static final String STATUS_MESSAGE = "StatusMessage";

    /** The Constant RESPONSE. */
    private static final String RESPONSE = "Response";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";

    /** The Constant PRODUCT. */
    private static final String PRODUCT = "Product";

    /** The Constant WITHDRAWAL_DETAILS. */
    private static final String WITHDRAWAL_DETAILS = "withdrawalDetails";

    /** The Constant RIDER_DETAILS. */
    private static final String RIDER_DETAILS = "RiderDetails";

    /** The Constant LOAN_DETAILS. */
    private static final String LOAN_DETAILS = "loanDetails";

    /** The Constant PRODUCT_DETAILS. */
    private static final String PRODUCT_DETAILS = "ProductDetails";

    /** The Constant POLICY_DETAILS. */
    private static final String POLICY_DETAILS = "policyDetails";

    /** The Constant PRODUCT_TYPE. */
    private static final String PRODUCT_TYPE = "productType";

    /** The Constant INSURED. */
    private static final String INSURED = "Insured";

    /** The Constant BASIC_DETAILS. */
    private static final String BASIC_DETAILS = "BasicDetails";

    /** The Constant FIRST_NAME. */
    private static final String FIRST_NAME = "firstName";

    /** The Constant LAST_NAME. */
    private static final String LAST_NAME = "lastName";

    /** The Constant PAYER. */
    private static final String PAYER = "Payer";

	private static final String FUND_INFORMATION = "FundInformation";

	private static final String EMR = "EMR";

	private static final String FLAT_EXTRA = "FlatExtraBase";

	private static final String TOPUP = "TopUp";

	private static final String TOPUP_DETAILS = "TopUpDetails";

	private static final String INV_PENSION = "InvPension";

	private static final String EXTENSION = "Extension";

	private static final String FEE_TYPE = "FeeType";

	private static final String PRODUCT_NAME = "productName";

	private static final String VESTING_AGE = "vestingAge";

	private static final String DIVIDEND_ADJUSTMENT = "dividendAdjustment";

    /** The life engage sync service. */
    @Autowired
    LifeEngageSyncService lifeEngageSyncService;

    /** The data source. */
    @Autowired
    private DataSource dataSource;

    /**
     * This Method Tests the illustration Save, retrieve, retreiveAll(basic details and full details) methods.
     * 
     * @throws BusinessException
     *             the business exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public void illustrationTest() throws BusinessException, IOException, ParseException {

        SimpleDateFormat newDt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar();
        String currentDate = newDt.format(calendar.getTime());
        String agentId = "Agent1";
        String productTypeFirst = "Term";
        String productNameFirst = "Max Life Whole Life Super";
        String dividendAdjustmentFirst = "No Adjustment";
        String insuredFirstNameFirst = "John";
        String insuredLastNameFirst = "Neman";
        String productTypeSecond = "WholeLife";
        String insuredFirstNameSecond = "Philip";
        String insuredLastNameSecond = "Cartor";
        String illustrationStatusFinal = "Final";
        String illustrationStatusSaved = "Saved";
        String statusCode100 = "100";
        String statusSuccess = "SUCCESS";
        String rejectedCode102 = "102";
        String statusRejected = "REJECTED";
        String statusCode400 = "400";
        String statusFailure = "FAILURE";
        String statusMessage = "Illustration Saved";
        String illustrationStatusRejected = "Rejected";
        String statusMessageUpdate = "Illustration Updated";
        String rejectedMessage = "Illustration Rejected";
        String type = "illustration";

        // creating the first illustration save request
        String saveRequest = getRequest("illustrationRequest/illustrationServiceRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", productTypeFirst)
                    .replace("{4}", insuredFirstNameFirst)
                    .replace("{5}", insuredLastNameFirst)
                    .replace("{6}", "")
                    .replace("{7}", productNameFirst)
        			.replace("{8}", dividendAdjustmentFirst)
        			.replace("{9}", "");

        // Saving first illustration Request
        String responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        JSONObject jsonObject = new JSONObject(responseJson);
        JSONObject jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        JSONObject jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        JSONArray jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        JSONObject jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(illustrationStatusFinal, jsonObj.getString(KEY15));
        String illustrationIdFirst = jsonObj.getString(KEY3);
        assertNotNull(illustrationIdFirst);
        JSONObject statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));
        assertEquals(statusSuccess, jsonObj.getString(KEY16));
        assertNotNull(KEY12);
        // Trying to save same illustration Request
        responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(illustrationStatusSaved, jsonObj.getString(KEY15));
        assertEquals("", jsonObj.getString(KEY3));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode400, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusFailure, statusDataObj.getString(STATUS));
        assertEquals(statusFailure, jsonObj.getString(KEY16));
        assertNotNull(KEY12);
        assertTrue(statusDataObj.getString(STATUS_MESSAGE).contains("This Trasaction Data is already processed"));
        assertTrue(jsonObj.getString(KEY17).contains("This Trasaction Data is already processed"));
        // Retrieving the first saved illustration
        String retrieveRequest = getRequest("illustrationRequest/IllustrationRetrieveRq.json");
        retrieveRequest = retrieveRequest.replace("{1}", currentDate)
        								 .replace("{2}", illustrationIdFirst)
        								 .replace("{3}", agentId);
        // System.out.println(retrieveRequest);
        String retrieveResponseJson = lifeEngageSyncService.retrieve(retrieveRequest);
        // System.out.println(retrieveResponseJson);
        assertNotNull(retrieveResponseJson);
        jsonObject = new JSONObject(retrieveResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(0).getString(KEY11));
      //  assertEquals(currentDate, jsonRsTransactionsArray.getJSONObject(0).getString(KEY4));
        assertEquals(illustrationIdFirst, jsonRsTransactionsArray.getJSONObject(0).getString(KEY3));
        assertEquals(type, jsonRsTransactionsArray.getJSONObject(0).getString(TYPE));
        assertEquals(illustrationStatusFinal, jsonRsTransactionsArray.getJSONObject(0).getString(KEY15));
        assertEquals(1, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONArray(WITHDRAWAL_DETAILS)
            .length());
        assertEquals(0, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONArray(RIDER_DETAILS)
            .length());
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONArray(FUND_INFORMATION));
        assertEquals(1, jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONArray(FUND_INFORMATION)
                .length());
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(EMR));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(FLAT_EXTRA));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(TOPUP));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(TOPUP)
                .getJSONArray(TOPUP_DETAILS));
        assertEquals(1, jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(TOPUP)
                .getJSONArray(TOPUP_DETAILS)
                .length());
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(INV_PENSION));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(EXTENSION));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(FEE_TYPE));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONArray(LOAN_DETAILS));
        assertEquals(1, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONArray(LOAN_DETAILS)
            .length());
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONObject(PRODUCT_DETAILS));
        assertEquals(productTypeFirst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONObject(PRODUCT_DETAILS)
            .getString(PRODUCT_TYPE));
        assertEquals(productNameFirst, jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(PRODUCT_DETAILS)
                .getString(PRODUCT_NAME));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PRODUCT)
            .getJSONObject(POLICY_DETAILS));
        //TODO VESTING_AGE  is not using in this  product
//        assertEquals(55, jsonRsTransactionsArray.getJSONObject(0)
//                .getJSONObject(TRANSACTION_DATA)
//                .getJSONObject(PRODUCT)
//                .getJSONObject(POLICY_DETAILS)
//                .getInt(VESTING_AGE));
        assertEquals(dividendAdjustmentFirst, jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(POLICY_DETAILS)
                .getString(DIVIDEND_ADJUSTMENT));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(INSURED));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(BASIC_DETAILS));
        assertEquals(insuredFirstNameFirst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(BASIC_DETAILS)
            .getString(FIRST_NAME));
        assertEquals(insuredLastNameFirst, jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(INSURED)
            .getJSONObject(BASIC_DETAILS)
            .getString(LAST_NAME));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).getJSONObject(PAYER));
        assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
            .getJSONObject(TRANSACTION_DATA)
            .getJSONObject(PAYER)
            .getJSONObject(BASIC_DETAILS));

        // creating the Second illustration save request
        saveRequest = getRequest("illustrationRequest/illustrationServiceRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", productTypeSecond)
                    .replace("{4}", insuredFirstNameSecond)
                    .replace("{5}", insuredLastNameSecond)
                    .replace("{6}", "")
                    .replace("{9}", "");

        // Saving Second illustrationRequest
        responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(illustrationStatusFinal, jsonObj.getString(KEY15));
        String illustrationIdSecond = jsonObj.getString(KEY3);
        assertNotNull(illustrationIdSecond);
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusMessage, statusDataObj.getString(STATUS_MESSAGE));
        assertEquals(statusSuccess, jsonObj.getString(KEY16));
        assertNotNull(KEY12);
        // Retrieving the illustration list - basic details
        String retrieveListRequest = getRequest("illustrationRequest/IllustrationRetrieveListRq.json");
        retrieveListRequest =
                retrieveListRequest.replace("{1}", currentDate).replace("{2}", agentId).replace("{3}", BASIC_DETAILS).replace("{4}", currentDate).replace("{5}", currentDate);
        String retrieveListResponseJson = lifeEngageSyncService.retrieveAll(retrieveListRequest);
        // System.out.println(retrieveListResponseJson);
        assertNotNull(retrieveListResponseJson);
        jsonObject = new JSONObject(retrieveListResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(2, jsonRsTransactionsArray.length());
        for (int i = 0; i < jsonRsTransactionsArray.length(); i++) {
            assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(i).getString(KEY11));
           // assertEquals(currentDate, jsonRsTransactionsArray.getJSONObject(i).getString(KEY4));
            assertEquals(type, jsonRsTransactionsArray.getJSONObject(i).getString(TYPE));
            assertEquals(illustrationStatusFinal, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
            assertEquals(false,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .has(WITHDRAWAL_DETAILS));
            assertEquals(false,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .has(RIDER_DETAILS));
            assertEquals(false,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .has(LOAN_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(PRODUCT_DETAILS));
            assertEquals(false,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .has(POLICY_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(INSURED));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(INSURED)
                .getJSONObject(BASIC_DETAILS));
            assertEquals(true, jsonRsTransactionsArray.getJSONObject(0).getJSONObject(TRANSACTION_DATA).has(PAYER));
            if (illustrationIdFirst.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY3))) {
                assertEquals(productTypeFirst, jsonRsTransactionsArray.getJSONObject(i)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject(PRODUCT)
                    .getJSONObject(PRODUCT_DETAILS)
                    .getString(PRODUCT_TYPE));
                assertEquals(insuredFirstNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(INSURED)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(insuredLastNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(INSURED)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(LAST_NAME));
            } else if (illustrationIdSecond.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY3))) {
                assertEquals(productTypeSecond, jsonRsTransactionsArray.getJSONObject(i)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject(PRODUCT)
                    .getJSONObject(PRODUCT_DETAILS)
                    .getString(PRODUCT_TYPE));
                assertEquals(insuredFirstNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(INSURED)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(insuredLastNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(INSURED)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(LAST_NAME));
            }

        }

        // Retrieving the illustration list - Full details
        retrieveListRequest = getRequest("illustrationRequest/IllustrationRetrieveListRq.json");
        retrieveListRequest =
                retrieveListRequest.replace("{1}", currentDate).replace("{2}", agentId).replace("{3}", "FullDetails").replace("{4}", currentDate).replace("{5}", currentDate);
        retrieveListResponseJson = lifeEngageSyncService.retrieveAll(retrieveListRequest);
        // System.out.println(retrieveListResponseJson);
        assertNotNull(retrieveListResponseJson);
        jsonObject = new JSONObject(retrieveListResponseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(2, jsonRsTransactionsArray.length());
        for (int i = 0; i < jsonRsTransactionsArray.length(); i++) {
            assertEquals(agentId, jsonRsTransactionsArray.getJSONObject(i).getString(KEY11));
            //assertEquals(currentDate, jsonRsTransactionsArray.getJSONObject(i).getString(KEY13));
            assertEquals(type, jsonRsTransactionsArray.getJSONObject(i).getString(TYPE));
            assertEquals(illustrationStatusFinal, jsonRsTransactionsArray.getJSONObject(i).getString(KEY15));
            assertEquals(1,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .getJSONArray(WITHDRAWAL_DETAILS)
                        .length());
            assertEquals(0,
                    jsonRsTransactionsArray.getJSONObject(i)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .getJSONArray(RIDER_DETAILS)
                        .length());
            assertNotNull(jsonRsTransactionsArray.getJSONObject(0)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONArray(LOAN_DETAILS));
            assertEquals(1,
                    jsonRsTransactionsArray.getJSONObject(0)
                        .getJSONObject(TRANSACTION_DATA)
                        .getJSONObject(PRODUCT)
                        .getJSONArray(LOAN_DETAILS)
                        .length());
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(PRODUCT_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PRODUCT)
                .getJSONObject(POLICY_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(INSURED));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(INSURED)
                .getJSONObject(BASIC_DETAILS));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i).getJSONObject(TRANSACTION_DATA).getJSONObject(PAYER));
            assertNotNull(jsonRsTransactionsArray.getJSONObject(i)
                .getJSONObject(TRANSACTION_DATA)
                .getJSONObject(PAYER)
                .getJSONObject(BASIC_DETAILS));
            if (illustrationIdFirst.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY3))) {
                assertEquals(productTypeFirst, jsonRsTransactionsArray.getJSONObject(i)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject(PRODUCT)
                    .getJSONObject(PRODUCT_DETAILS)
                    .getString(PRODUCT_TYPE));
                assertEquals(insuredFirstNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(INSURED)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(insuredLastNameFirst,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(INSURED)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(LAST_NAME));
            } else if (illustrationIdSecond.equals(jsonRsTransactionsArray.getJSONObject(i).getString(KEY3))) {
                assertEquals(productTypeSecond, jsonRsTransactionsArray.getJSONObject(i)
                    .getJSONObject(TRANSACTION_DATA)
                    .getJSONObject(PRODUCT)
                    .getJSONObject(PRODUCT_DETAILS)
                    .getString(PRODUCT_TYPE));
                assertEquals(insuredFirstNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(INSURED)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(FIRST_NAME));
                assertEquals(insuredLastNameSecond,
                        jsonRsTransactionsArray.getJSONObject(i)
                            .getJSONObject(TRANSACTION_DATA)
                            .getJSONObject(INSURED)
                            .getJSONObject(BASIC_DETAILS)
                            .getString(LAST_NAME));
            }
        }

        // Creating the first illustration update request
        saveRequest = getRequest("illustrationRequest/illustrationServiceRq.json");
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", currentDate)
                    .replace("{3}", productTypeFirst)
                    .replace("{4}", insuredFirstNameFirst)
                    .replace("{5}", insuredLastNameFirst)
                    .replace("{6}", illustrationIdFirst)
                    .replace("{9}", "");

        // Updating first illustration Request
        responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(illustrationStatusFinal, jsonObj.getString(KEY15));
        assertEquals(illustrationIdFirst, jsonObj.getString(KEY3));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(statusCode100, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusSuccess, statusDataObj.getString(STATUS));
        assertEquals(statusSuccess, jsonObj.getString(KEY16));
        assertNotNull(KEY12);
        assertEquals(statusMessageUpdate, statusDataObj.getString(STATUS_MESSAGE));
        
        
        // Creating the first illustration update request again with ConflictResolution true and sourceOfTruth as server
        saveRequest = getRequest("illustrationRequest/illustrationServiceRq.json");
        calendar.add(Calendar.DATE, -1); 
        final String previousDate = newDt.format(calendar.getTime());
        saveRequest =
                saveRequest.replace("{1}", agentId)
                    .replace("{2}", previousDate)
                    .replace("{3}", productTypeFirst)
                    .replace("{4}", insuredFirstNameFirst)
                    .replace("{5}", insuredLastNameFirst)
                    .replace("{6}", illustrationIdFirst)                    
                    .replace("{9}", previousDate);
        // Updating first illustration Request with ConflictResolution true and sourceOfTruth as server
        responseJson = lifeEngageSyncService.save(saveRequest);
        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        jsonResponseObj = jsonObject.getJSONObject(RESPONSE);
        jsonResponsePayloadObj = jsonResponseObj.getJSONObject(RESPONSE_PAYLOAD);
        jsonRsTransactionsArray = jsonResponsePayloadObj.getJSONArray(TRANSACTIONS);
        assertEquals(1, jsonRsTransactionsArray.length());
        jsonObj = jsonRsTransactionsArray.getJSONObject(0);
        assertEquals(illustrationStatusRejected, jsonObj.getString(KEY15));
        assertEquals(illustrationIdFirst, jsonObj.getString(KEY3));
        statusDataObj = jsonObj.getJSONObject(STATUS_DATA);
        assertNotNull(statusDataObj);
        assertEquals(rejectedCode102, statusDataObj.getString(STATUS_CODE));
        assertEquals(statusRejected, statusDataObj.getString(STATUS));
        assertEquals(statusRejected, jsonObj.getString(KEY16));
        assertNotNull(KEY12);
        assertEquals(rejectedMessage, statusDataObj.getString(STATUS_MESSAGE));
   }

    /**
     * Run illustration test.
     * 
     * @throws BusinessException
     *             the business exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public void runIllustrationTest() throws BusinessException, IOException, ParseException {
        insertExtnTableAndRuleDetails();
        String runIllustrationRequest = getRequest("illustrationRequest/runIllustrationRq.json");
        runIllustrationRequest = runIllustrationRequest.replace("{1}", "SinglePayment");

        String responseJson = lifeEngageSyncService.runIllustration(runIllustrationRequest);

        // System.out.println(responseJson);
        assertNotNull(responseJson);
        JSONObject jsonObject = new JSONObject(responseJson);
        assertEquals(Boolean.FALSE, jsonObject.getBoolean("isSuccess"));
        assertEquals(true, jsonObject.has("ValidationWarnings"));
        assertNotNull(jsonObject.getJSONObject("ValidationWarnings"));

        runIllustrationRequest = getRequest("illustrationRequest/runIllustrationRq.json");
        runIllustrationRequest = runIllustrationRequest.replace("{1}", "Annual");
        responseJson = lifeEngageSyncService.runIllustration(runIllustrationRequest);

        // System.out.println(responseJson);
        assertNotNull(responseJson);
        jsonObject = new JSONObject(responseJson);
        assertEquals(false, jsonObject.has("isSuccess"));
        assertEquals(true, jsonObject.has("illustrationTableData"));
        assertNotNull(jsonObject.getJSONArray("illustrationTableData").length() > 0);
    }

    /**
     * Execute Rule test.
     * 
     * @throws BusinessException
     *             the business exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public void executeRuleTest() throws BusinessException, IOException, ParseException {
        insertExtnTableAndRuleDetails();


        String runIllustrationRequest = getRequest("illustrationRequest/executeRuleRq.json");
        runIllustrationRequest = runIllustrationRequest.replace("{1}", "Annual");
        String responseJson = lifeEngageSyncService.executeRule(runIllustrationRequest);

        System.out.println(responseJson);
        assertNotNull(responseJson);
        JSONObject jsonObject = new JSONObject(responseJson);
        assertEquals(false, jsonObject.has("isSuccess"));
        assertEquals(true, jsonObject.has("illustrationTableData"));
        assertNotNull(jsonObject.getJSONArray("illustrationTableData").length() > 0);
    }

    /**
     * Insert extn table and rule details.
     */
    private void insertExtnTableAndRuleDetails() {
        try {

            QueryRunner quryrnr = new QueryRunner(dataSource);
            quryrnr.update("Drop table IF EXISTS `RULE_TABLE`;");
            quryrnr.update("Drop table IF EXISTS `RULE_TYPE`;");
            quryrnr.update("CREATE TABLE `RULE_TYPE` (  `id` int(11) NOT NULL,  `type_name` varchar(20) NOT NULL,  PRIMARY KEY (`id`))");
            quryrnr.update("INSERT INTO `RULE_TYPE` (`id`, `type_name`) VALUES (0,'BusinessRuleSet'),(1,'BusinessRule'),(2,'CalculationRule'),(3,'BusinessRuleSet'),(4,'CalculationRuleSet');");
            quryrnr.update("CREATE TABLE `RULE_TABLE` (  `ID` int(11) NOT NULL AUTO_INCREMENT,  `TYPE_ID` int(11) NOT NULL,  `RuleName` varchar(250) NOT NULL,  `CREATED_DATE` datetime NOT NULL,  `VERSION` float(2) DEFAULT NULL,  `RULEJSON` longtext,  `RuleDesc` varchar NOT NULL,  `UPDATED_DATE` datetime DEFAULT NULL,  `Category` varchar(20) NOT NULL,  `ClientId` varchar(20) DEFAULT NULL,  PRIMARY KEY (`ID`),  CONSTRAINT `FK_RULE_RULETYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `RULE_TYPE` (`id`))");
            quryrnr.update("INSERT INTO `RULE_TABLE` (`ID`, `TYPE_ID`, `RuleName`, `CREATED_DATE`, `VERSION`, `RULEJSON`, `RuleDesc`, `UPDATED_DATE`, `Category`, `ClientId`) VALUES (1953,1,'WLPS_ChangeEnumValues','2013-09-19 10:28:59',NULL,NULL,'function WLPS_ChangeEnumValues(inoutMap) {if (inoutMap.state == \"23\") {inoutMap.state = \"JK\";} else {inoutMap.state = \"DL\";}if (inoutMap.sex == \"Female\") {inoutMap.sex = \"F\";    } else if (inoutMap.sex == \"Male\") {        inoutMap.sex = \"M\";    }      if (inoutMap.payorSex == \"Female\") {        inoutMap.payorSex = \"F\";    } else if (inoutMap.payorSex == \"Male\") {        inoutMap.payorSex = \"M\";    }      if (inoutMap.premiumMode == \"Annual\") {        inoutMap.premiumMode = \"6001\";    } else if (inoutMap.premiumMode == \"SemiAnnual\") {        inoutMap.premiumMode = \"6002\";    } else if (inoutMap.premiumMode == \"Quarterly\") {        inoutMap.premiumMode = \"6003\";    } else if (inoutMap.premiumMode == \"Monthly\") {        inoutMap.premiumMode = \"6004\";    } else if (inoutMap.premiumMode == \"SinglePayment\") {        inoutMap.premiumMode = \"6005\";    }       if (inoutMap.bonusOption == \"PUA\") {        inoutMap.bonusOption = \"1101\";    } else if (inoutMap.bonusOption == \"Cash\") {        inoutMap.bonusOption = \"1102\";    } else if (inoutMap.bonusOption == \"Premium Offset\") {        inoutMap.bonusOption = \"1103\";    }       return JSON.stringify(inoutMap);}',NULL,'MLI','LE');");
            quryrnr.update("INSERT INTO `RULE_TABLE` (`ID`, `TYPE_ID`, `RuleName`, `CREATED_DATE`, `VERSION`, `RULEJSON`, `RuleDesc`, `UPDATED_DATE`, `Category`, `ClientId`) VALUES (1375,1,'WLPS_Validations_All','2013-09-19 10:28:59',1.00,NULL,\'function WLPS_Validations_All(inoutMap) {    var ValidationResults = {};    updateJSON(inoutMap, \"ValidationResults\", ValidationResults);   var ValidationWarnings = {};    updateJSON(inoutMap, \"ValidationWarnings\", ValidationWarnings);    isSuccess = Boolean(true);    isWarning =Boolean(false);    if (inoutMap.bonusOption != 1101 && inoutMap.bonusOption != 1102 && inoutMap.bonusOption != 1103) {       inoutMap.ValidationResults.WLPS_RULEFWLPS05 = \"Please choose a valid bonus option\";        isSuccess = Boolean(false);    }    if(inoutMap.premiumMode != 6001 && inoutMap.premiumMode != 6002 && inoutMap.premiumMode != 6003 && inoutMap.premiumMode != 6004) {       inoutMap.ValidationResults.WLPS_RULEFWLPS06 = \"Please choose a valid premium payment mode\";        isSuccess = Boolean(false);    }    if(inoutMap.payorRiderRequired == \"Yes\") {        if (inoutMap.payorAge.length == Number(0) || inoutMap.payorSex.length == Number(0) ||inoutMap.payorName.length == Number(0)) {            inoutMap.ValidationResults.WLPS_RULEFWLPS07 = \"Please enter payor details\";            isSuccess = Boolean(false);        }    }    updateJSON(inoutMap, \"isSuccess\", isSuccess);    updateJSON(inoutMap,\"isWarning\", isWarning);    return JSON.stringify(inoutMap);}\','2013-10-08 16:48:49','MLI','LE'),(1396,1,'WLPS_Validations','2013-10-01 17:05:35',1.00,NULL,'function WLPS_Validations(inoutMap) {   inoutMap=JSON.parse(CovertingCannonicalToFlat(inoutMap)); inoutMap=JSON.parse(WLPS_ChangeEnumValues(inoutMap));WLPS_Validations_All(inoutMap);    return JSON.stringify(inoutMap);}',NULL,'MLI','LE'),(1397,1,'WLPS_Illustration','2013-10-01 17:05:35',1.00,NULL,\'function WLPS_Illustration(inoutMap) {	var illustrationTableData = [];	updateJSON(inoutMap, \"illustrationTableData\", illustrationTableData);		inoutMap.illustrationTableData.push({		\"PREMIUM_VALUE\": \"100\",	\"SURRENDER_VALUE\": \"400\"	});	return JSON.stringify(inoutMap);}\',NULL,'MLI','LE');");
            quryrnr.update("INSERT INTO `RULE_TABLE` (`ID`, `TYPE_ID`, `RuleName`, `CREATED_DATE`, `VERSION`, `RULEJSON`, `RuleDesc`, `UPDATED_DATE`, `Category`, `ClientId`) VALUES (1952,1,'CovertingCannonicalToFlat','2013-09-03 09:59:01',NULL,NULL,'function CovertingCannonicalToFlat(inoutMap) {rowData = {\"productCode\": inoutMap.PolicyDetails.productCode,\"committedPremium\": inoutMap.PolicyDetails.committedPremium,\"age\": inoutMap.InsuredDetails.age,\"sex\": inoutMap.InsuredDetails.sex,\"state\": inoutMap.InsuredDetails.state,\"premiumMode\": inoutMap.PolicyDetails.premiumMode,\"PPT\": inoutMap.PolicyDetails.premiumPayingTerm,\"bonusOption\": inoutMap.PolicyDetails.bonusOption,\"surrenderOption\": inoutMap.SurrenderOption.isRequired,\"payorAge\": inoutMap.PayorDetails.age,\"payorName\": inoutMap.PayorDetails.name,\"payorRiderID\": inoutMap.PayorRider.id,\"payorRiderRequired\" : inoutMap.PayorRider.isRequired ,\"payorSex\": inoutMap.PayorDetails.sex,\"flatExtraBaseRequired\" : inoutMap.FlatExtraBase.isRequired,\"extraMortalityRateBase\" : inoutMap.EMR.isRequired,\"surrenderAmountInput\": inoutMap.SurrenderOption.amount};inoutMap = rowData;return JSON.stringify(inoutMap)}',NULL,'MLI','LE');");

        } catch (SQLException e) {
            // System.out.println("=======================================> Error :" + e.getMessage());
        }
    }

    @After
    public void cleanup() {
        try {
            QueryRunner queryRunner = new QueryRunner(dataSource);
            queryRunner.update("Drop table IF EXISTS `RULE_TABLE`;");
            queryRunner.update("Drop table IF EXISTS `RULE_TYPE`;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the life engage sync service.
     * 
     * @return the life engage sync service
     */
    public final LifeEngageSyncService getLifeEngageSyncService() {
        return lifeEngageSyncService;
    }

    /**
     * Sets the life engage sync service.
     * 
     * @param lifeEngageSyncService
     *            the life engage sync service to set.
     */
    public final void setLifeEngageSyncService(LifeEngageSyncService lifeEngageSyncService) {
        this.lifeEngageSyncService = lifeEngageSyncService;
    }

    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(new String(line.getBytes(), "UTF-8"));
        }

        return sb.toString();
    }

}
