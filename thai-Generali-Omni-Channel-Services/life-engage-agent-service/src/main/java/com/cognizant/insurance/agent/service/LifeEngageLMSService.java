package com.cognizant.insurance.agent.service;

import com.cognizant.insurance.agent.core.exception.BusinessException;

public interface LifeEngageLMSService {
	
	String save(String json) throws BusinessException;
	
	String createLead(String json) throws BusinessException;
	
	String retrieve(String json) throws BusinessException;
	
	String retrieveCustomerList(String json) throws BusinessException;
	
	String saveObservations(String json) throws BusinessException;
	
	String getStatus(String json) throws BusinessException;
	
	String retrieveAll(String json) throws BusinessException;
	 
	String delete(String json) throws BusinessException;
	
	String retrieveByFilter(String json) throws BusinessException;
	 
 }
