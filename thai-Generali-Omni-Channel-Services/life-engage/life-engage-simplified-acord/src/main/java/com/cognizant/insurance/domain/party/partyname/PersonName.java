/**
 * 
 */
package com.cognizant.insurance.domain.party.partyname;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;

import com.cognizant.insurance.domain.party.partycodelists.PersonNameUsageCodeList;
import com.cognizant.insurance.domain.party.partycodelists.PrefixTitleCodeList;

/**
 * This concept represents a person's name. 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("Person")
public class PersonName extends PartyName {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6500873826132622734L;

    /**
     * The honorifics or titles that precede a person's name when addressing a
     * person in polite, somewhat formal circumstances.
     * 
     * e.g. Dr., Ms., Miss, Mr., Mrs., etc.
     * 
     * 
     * 
     */
    @Enumerated
    private PrefixTitleCodeList prefixTitleCode;

    /**
     * The name given to an individual which does not denote his/her family,
     * tribal or geographic antecedents. It may be represented via one or more
     * words, which collectively represent the given name.
     * 
     * Examples:
     * 
     * The name: William Shakespeare "William" is the given name, also known as
     * "first name" in some locales. "Shakespeare" is the surname, also known as
     * "last name" in some locales.
     * 
     * The name: Mary Beth Doe "Mary Beth" is the given name. "Doe" is the
     * surname.
     * 
     * 
     * 
     * 
     * 
     */
    @Column(columnDefinition="nvarchar(100)")
    private String givenName;

    /**
     * The name given to an individual which does not denote his/her family,
     * tribal or geographic antecedents, and is not the person's given name
     * (e.g. first name). This is known as middle name in various locales. It
     * may be represented via one or more words, which collectively represent
     * the middle name.
     * 
     * Examples:
     * 
     * The name: Juan Manuel Fangio "Juan" is the given name, also known as
     * "first name" in some locales. "Manuel" is the middle name, also known as
     * other given name in some locales. "Fangio" is the surname, also known as
     * "last name" in some locales.
     * 
     * The name: George Herbert Walker Bush "George" is the given name.
     * "Herbert Walker" is the middle name. "Bush" is the surname. <!--
     * end-UML-doc -->
     * 
     * 
     * 
     */
    @Column(columnDefinition="nvarchar(100)")
    private String middleName;

    /**
     * The name or compound name used by an individual to denote his/her
     * familial, tribal or geographic antecedents. It may be represented via one
     * or more words, which collectively represent the surname.
     * 
     * Examples
     * 
     * The name: William Shakespeare "William" is the given name, also known as
     * first name in some locales. "Shakespeare" is the surname, also known as a
     * family name or individual's "last name" in some locales.
     * 
     * The name: Elio de Angelis "Elio" is the given name. "de Angelis" is the
     * surname.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    @Column(columnDefinition="nvarchar(100)")
    private String surname;

    /**
     * A suffix following the person's name, if applicable. This may be whatever
     * it needs to be, including text such as "Jr.", or "III", as well as text
     * depicting professional designations, such as "MD", "CPCU", "CLU".
     * 
     * Example:
     * 
     * The name: William Shakespeare III, CPCU "William" is the given name.
     * "Shakespeare" is the surname. "III, CPCU" is the suffix.
     * 
     * 
     * 
     * 
     * 
     */
    private String suffix;

    /**
     * A code indicating the use of the person's name.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private PersonNameUsageCodeList usageCode;
    
    /**
     * The initials of the person's name.
     */
    private String initials;
    
    /**
     * The title of the person.
     */
    @Basic(fetch=FetchType.EAGER)
    @Column(columnDefinition="nvarchar(100)")
    private String title;
    
    /**
     * Gets the givenName.
     * 
     * @return Returns the givenName.
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * Gets the initials.
     *
     * @return Returns the initials.
     */
    public String getInitials() {
        return initials;
    }

    /**
     * Gets the middleName.
     * 
     * @return Returns the middleName.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Gets the prefixTitleCode.
     * 
     * @return Returns the prefixTitleCode.
     */
    public PrefixTitleCodeList getPrefixTitleCode() {
        return prefixTitleCode;
    }

    /**
     * Gets the suffix.
     * 
     * @return Returns the suffix.
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Gets the surname.
     * 
     * @return Returns the surname.
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Gets the title.
     *
     * @return Returns the title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the usageCode.
     * 
     * @return Returns the usageCode.
     */
    public PersonNameUsageCodeList getUsageCode() {
        return usageCode;
    }

    /**
     * Sets The givenName.
     * 
     * @param givenName
     *            The givenName to set.
     */
    public void setGivenName(final String givenName) {
        this.givenName = givenName;
    }

    /**
     * Sets The initials.
     *
     * @param initials The initials to set.
     */
    public void setInitials(final String initials) {
        this.initials = initials;
    }

    /**
     * Sets The middleName.
     * 
     * @param middleName
     *            The middleName to set.
     */
    public void setMiddleName(final String middleName) {
        this.middleName = middleName;
    }

    /**
     * Sets The prefixTitleCode.
     * 
     * @param prefixTitleCode
     *            The prefixTitleCode to set.
     */
    public void setPrefixTitleCode(
            final PrefixTitleCodeList prefixTitleCode) {
        this.prefixTitleCode = prefixTitleCode;
    }

    /**
     * Sets The suffix.
     * 
     * @param suffix
     *            The suffix to set.
     */
    public void setSuffix(final String suffix) {
        this.suffix = suffix;
    }

    /**
     * Sets The surname.
     * 
     * @param surname
     *            The surname to set.
     */
    public void setSurname(final String surname) {
        this.surname = surname;
    }

    /**
     * Sets The title.
     *
     * @param title The title to set.
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Sets The usageCode.
     * 
     * @param usageCode
     *            The usageCode to set.
     */
    public void setUsageCode(final PersonNameUsageCodeList usageCode) {
        this.usageCode = usageCode;
    }

}
