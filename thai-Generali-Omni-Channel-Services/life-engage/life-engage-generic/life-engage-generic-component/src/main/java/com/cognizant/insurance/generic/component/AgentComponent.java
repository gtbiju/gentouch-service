package com.cognizant.insurance.generic.component;

import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;

/**
 * The Interface AgentComponent.
 */
public interface AgentComponent {
	
	/**
	 * Retrieve agent by code.
	 *
	 * @param request the agent request
	 * @return the response
	 */
	Response<Agent> retrieveAgentByCode(Request<Agent> request);

}
