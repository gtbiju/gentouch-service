/**
 * 
 */
package com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.agent.domain.agreement.Agreement;
import com.cognizant.insurance.agent.domain.roleandrelationship.PartyRole;

/**
 * A role played by a party in the context of an agreement.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PARTY_ROLE_IN_AGREEMENT_TYPE", discriminatorType = DiscriminatorType.STRING)
public class PartyRoleInAgreement extends PartyRole {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5923382303990769900L;
    
    /**
     * This relationship links a party role with an agreement.
     * 
     * 
     * 
     * 
     * 
     */
    
    /**
     * UUID for a party added for Requirement upload
     * 
     */
    private String partyIdentifier;
    
    @ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE }, fetch=FetchType.LAZY)
    private Agreement isPartyRoleIn;

    /**
     * added for generali
     * 
     */
    private String partyId;
    /**
     * added for generali vietnam
     * 
     * to map objects passed from Choose party Screen
     * 
     */
    @Column(columnDefinition="NVARCHAR(MAX)")
    private String photo;
    
    private String typeParty;
    
    private String payerPartyId;
    
    private String insuredPartyId;
    
    private String benfPartyId;
    
    private String addInsuredPartyId;
    
    private Boolean isPayer;
    
    private Boolean isInsured;
    
    private Boolean isBeneficiary;
    
    private Boolean isAdditionalInsured;
    
    
    public PartyRoleInAgreement() {
        this.partyId = "0";
    }
    
    public String getPartyIdentifier() {
        return partyIdentifier;
    }

    public void setPartyIdentifier(String partyIdentifier) {
        this.partyIdentifier = partyIdentifier;
    }

    /**
     * Gets the isPartyRoleIn.
     * 
     * @return Returns the isPartyRoleIn.
     */
    public Agreement getIsPartyRoleIn() {
        return isPartyRoleIn;
    }

    /**
     * Sets The isPartyRoleIn.
     * 
     * @param isPartyRoleIn
     *            The isPartyRoleIn to set.
     */
    public void setIsPartyRoleIn(final Agreement isPartyRoleIn) {
        this.isPartyRoleIn = isPartyRoleIn;
    }

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	/**
	 * @param photo the photo to set
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	/**
	 * @return the photo
	 */
	public String getPhoto() {
		return photo;
	}


	/**
	 * @return the payerPartyId
	 */
	public String getPayerPartyId() {
		return payerPartyId;
	}

	/**
	 * @param payerPartyId the payerPartyId to set
	 */
	public void setPayerPartyId(String payerPartyId) {
		this.payerPartyId = payerPartyId;
	}

	/**
	 * @return the insuredPartyId
	 */
	public String getInsuredPartyId() {
		return insuredPartyId;
	}

	/**
	 * @param insuredPartyId the insuredPartyId to set
	 */
	public void setInsuredPartyId(String insuredPartyId) {
		this.insuredPartyId = insuredPartyId;
	}

	/**
	 * @return the benfPartyId
	 */
	public String getBenfPartyId() {
		return benfPartyId;
	}

	/**
	 * @param benfPartyId the benfPartyId to set
	 */
	public void setBenfPartyId(String benfPartyId) {
		this.benfPartyId = benfPartyId;
	}

	/**
	 * @return the addInsuredPartyId
	 */
	public String getAddInsuredPartyId() {
		return addInsuredPartyId;
	}

	/**
	 * @param addInsuredPartyId the addInsuredPartyId to set
	 */
	public void setAddInsuredPartyId(String addInsuredPartyId) {
		this.addInsuredPartyId = addInsuredPartyId;
	}

	/**
	 * @return the isPayer
	 */
	public Boolean getIsPayer() {
		return isPayer;
	}

	/**
	 * @param isPayer the isPayer to set
	 */
	public void setIsPayer(Boolean isPayer) {
		this.isPayer = isPayer;
	}

	/**
	 * @return the isInsured
	 */
	public Boolean getIsInsured() {
		return isInsured;
	}

	/**
	 * @param isInsured the isInsured to set
	 */
	public void setIsInsured(Boolean isInsured) {
		this.isInsured = isInsured;
	}

	/**
	 * @return the isBeneficiary
	 */
	public Boolean getIsBeneficiary() {
		return isBeneficiary;
	}

	/**
	 * @param isBeneficiary the isBeneficiary to set
	 */
	public void setIsBeneficiary(Boolean isBeneficiary) {
		this.isBeneficiary = isBeneficiary;
	}

	/**
	 * @return the isAdditionalInsured
	 */
	public Boolean getIsAdditionalInsured() {
		return isAdditionalInsured;
	}

	/**
	 * @param isAdditionalInsured the isAdditionalInsured to set
	 */
	public void setIsAdditionalInsured(Boolean isAdditionalInsured) {
		this.isAdditionalInsured = isAdditionalInsured;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @param typeParty the typeParty to set
	 */
	public void setTypeParty(String typeParty) {
		this.typeParty = typeParty;
	}

	/**
	 * @return the typeParty
	 */
	public String getTypeParty() {
		return typeParty;
	}
	
	
    
}
