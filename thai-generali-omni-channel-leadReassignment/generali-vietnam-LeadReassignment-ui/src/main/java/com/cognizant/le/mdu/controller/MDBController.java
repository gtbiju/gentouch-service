package com.cognizant.le.mdu.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.cognizant.le.mdu.exception.ValidationException;
import com.cognizant.le.model.dao.SQLiteExportDAO;
import com.cognizant.le.model.dto.ResponseBean;
import com.cognizant.le.util.MasterDataExcelParser;

@Controller
public class MDBController {

	@Autowired
	MasterDataExcelParser masterDataExcelParser;

	/** The sq lite export dao. */
	@Autowired
	private SQLiteExportDAO sqLiteExportDAO;

	@RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "text/plain")
	public @ResponseBody
	List<ResponseBean> upload(MultipartHttpServletRequest request,
			HttpServletResponse response) throws Exception {

		List<ResponseBean> responses = new ArrayList<ResponseBean>();
		ResponseBean responseBean = null;
		Iterator<String> itr = request.getFileNames();
		MultipartFile mpf = null;

		while (itr.hasNext()) {
			mpf = request.getFile(itr.next());
			System.out.println(mpf.getOriginalFilename() + " uploaded! ");
			try {
				File tempFile = File.createTempFile("temp_xl_file", ".xlsx");
				FileOutputStream fileOutputStream = new FileOutputStream(
						tempFile);
				fileOutputStream.write(mpf.getBytes());
				fileOutputStream.flush();
				fileOutputStream.close();
				try {
					responses = masterDataExcelParser.parseExcelFile(tempFile);
					responses.add(responseBean);
				} catch (ValidationException ve) {
					responses = new ArrayList<ResponseBean>();
					ResponseBean rBean = new ResponseBean();
					rBean.setMessage(ve.getMessage());
					rBean.setResponseStatus("ERROR");
					responses.add(rBean);
				}
			} catch (IOException e) {
				responseBean = new ResponseBean();
				responseBean.setMessage("Failure");
				e.printStackTrace();
			}
		}
		return responses;
	}

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public void getData(HttpServletResponse response) throws Exception {
		File dbFile = null;
		List<String> tablesList = sqLiteExportDAO.getTablesList();
		// List<String> tablesWithEntries = new ArrayList<String>();
		File inputFile = File.createTempFile("extndb", ".sql");
		FileWriter fileWriter = new FileWriter(inputFile);
		/*
		 * for (String table : tablesList) { tablesWithEntries =
		 * sqLiteExportDAO.checkForEntries(table, tablesWithEntries); }
		 */
		// System.out.println("Tables "+tablesWithEntries);
		sqLiteExportDAO.generateSQLiteScriptsForMysql(tablesList, fileWriter);
		fileWriter.flush();
		fileWriter.close();
		dbFile = sqLiteExportDAO.executeSQLiteScripts(inputFile);
		System.out.println(dbFile.getName());
		InputStream inputStream = new FileInputStream(dbFile);
		response.setHeader("Content-Disposition",
				"attachment; filename=masterdb.db");
		IOUtils.copy(inputStream, response.getOutputStream());
		response.flushBuffer();
	}
}
