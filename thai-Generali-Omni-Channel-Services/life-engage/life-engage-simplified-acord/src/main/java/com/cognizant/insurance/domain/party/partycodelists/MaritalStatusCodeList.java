/**
 * 
 */
package com.cognizant.insurance.domain.party.partycodelists;

/**
 * A code list representing various marital status'. 
 * 
 * @author 301350
 * 
 * 
 */
public enum MaritalStatusCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Single,
    
    /**
     * 
     * 
     * 
     * 
     */
    Married,
    /**
     * 
     * 
     * 
     * 
     */
    Divorce,
    /*
     * 
     * 
     * 
     */
    Widow,
    /*
     * 
     * 
     * 
     */
    Widower,    
    /*
     * 
     * 
     * 
     */
    NotMentioned,
    /*
     * 
     * 
     * 
     */
    Others
}
