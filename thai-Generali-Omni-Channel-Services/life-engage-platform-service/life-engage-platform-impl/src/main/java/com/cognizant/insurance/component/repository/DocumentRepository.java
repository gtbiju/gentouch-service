/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 418964
 * @version       : 0.1, Jan 12, 2015
 */
package com.cognizant.insurance.component.repository;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;
import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.agreement.component.AgreementComponent;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.utils.Base64Utils;
import com.cognizant.insurance.utils.FileUtils;

/**
 * The Class Document Repository.
 * 
 * @author 
 */
// @Repository
public class DocumentRepository {

    /** The entity manager. */
    @PersistenceContext(unitName="LE_Platform")
    private EntityManager entityManager;

    /** The agreement component. */
    @Autowired
    private AgreementComponent agreementComponent;

    @Value( "${le.platform.service.fileUploadPath}" )
    private String fileUploadPath;

    @Value( "${le.platform.service.slash}" )
    private String slash;

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";
    
    /** The Constant ILLUSTRATION. */
    private static final String ILLUSTRATION = "illustration";
    
    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(DocumentRepository.class);

   
    /**
     * Gets the document.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param documentName
     *            the document name
     * @param transactionId
     *            the transaction id
     * @return the document
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public  AgreementDocument getDocument(final Transactions transaction, final String documentName,
    		final String transactionId) throws IOException {

        // get the Agreement object with proposal number
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.UPDATE);
        
        String type = transaction.getType();
        ContextTypeCodeList contextTypeCodeList = null;
        String id = "";
        if(E_APP.equals(type)){
        	contextTypeCodeList = ContextTypeCodeList.EAPP;
        	id = transaction.getProposalNumber();
        }else if(ILLUSTRATION.equals(type)){
        	contextTypeCodeList = ContextTypeCodeList.ILLUSTRATION;
        	id = transaction.getIllustrationId();
        }
        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, contextTypeCodeList, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(id);
        final Set<AgreementDocument> agreementDocuments = new HashSet<AgreementDocument>();
        final AgreementDocument agreementDocument = new AgreementDocument();
        agreementDocument.setName(documentName);
        agreementDocuments.add(agreementDocument);
        insuranceAgreement.setRelatedDocument(agreementDocuments);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, contextTypeCodeList,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<AgreementDocument> agreementDocumentResponse =
                agreementComponent.getAgreementDocumentDetails(agreementRequest, statusRequest);

        setFileContent(id, agreementDocumentResponse.getType());

        return agreementDocumentResponse.getType();
    }

    /**
     * Sets the file content.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param agreementDocument
     *            the agreement document
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void setFileContent(final String proposalNumber, final AgreementDocument agreementDocument)
            throws IOException {
        if (agreementDocument != null && CollectionUtils.isNotEmpty(agreementDocument.getPages())) {
            final String documentPath =fileUploadPath
                            + proposalNumber
                            + slash;
            if (DocumentTypeCodeList.Signature.equals(agreementDocument.getTypeCode())) {
                for (Document document : agreementDocument.getPages()) {
                	/**
                	 * if the file/page  exist physically . Read the file 
                	 */
                	if(new File(documentPath + document.getFileNames()).exists()){
                    document.setFileContent(FileUtils.getFileContent(documentPath + document.getFileNames()));
                	}
                }
            } else {
                for (Document document : agreementDocument.getPages()) {
                	/**
                	 * if the file/page  exist physically . Read the file 
                	 */
                	if(new File(documentPath + document.getFileNames()).exists()){
                		 document.setBase64string(Base64Utils.encodeFileToBase64Binary(documentPath
                                 + document.getFileNames()));
                	}                   
                }
            }
        }
    }

    /**
     * Upload documents.
     * 
     * @param proposalNumber
     *            the proposal number
     * @param transactionId
     *            the transaction id
     * @param document
     *            the document
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws BusinessException
     *             the business exception
     */
    public  void uploadDocuments(final Transactions transaction, final String transactionId,
            final AgreementDocument document) throws IOException, BusinessException {

        if (document == null || StringUtils.isEmpty(document.getName())) {
            throwBusinessException(true, Constants.DOCUMENT_DETAILS_MISSING);
        }
        // get the Agreement object with proposal number
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.UPDATE);
     

        String type = transaction.getType();
        String id = "";
        ContextTypeCodeList contextTypeCodeList = null;
        if(E_APP.equals(type)){
        	contextTypeCodeList = ContextTypeCodeList.EAPP;
        	id = transaction.getProposalNumber();
        }else if(ILLUSTRATION.equals(type)){
        	contextTypeCodeList = ContextTypeCodeList.ILLUSTRATION;
        	id = transaction.getIllustrationId();
        }
        final Request<Agreement> agreementRequest =
                new JPARequestImpl<Agreement>(entityManager, contextTypeCodeList, transactionId);
        final InsuranceAgreement insuranceAgreement = new InsuranceAgreement();
        insuranceAgreement.setIdentifier(id);
        agreementRequest.setType(insuranceAgreement);

        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, contextTypeCodeList,
                        transactionId);
        statusRequest.setType(statusList);

        final Response<Agreement> agreementsResponse =
                agreementComponent.getInsuranceAgreement(agreementRequest, statusRequest);
        if (agreementsResponse.getType() == null) {
            throwSystemException(true,Constants.NO_PROPOSAL_EXIST);
        } else {
            final InsuranceAgreement agreement = (InsuranceAgreement) agreementsResponse.getType();

            // create the folder with proposal number if it not exitsts
            final File tempDirectory =
                    new File(fileUploadPath + id);
            createDirectory(tempDirectory);

            // defining a set which is used to store all current file name(used for deletion operation)
            final Set<String> documentsNeedToDelete = new HashSet<String>();
            // if there is no document for the particular proposal, we need to add the full document that is coming to
            // the service
            if (agreement.getRelatedDocument() != null && agreement.getRelatedDocument().size() == 0) {
                final Set<AgreementDocument> agreementDocuments = new HashSet<AgreementDocument>();
                agreementDocuments.add(document);
                agreement.setRelatedDocument(agreementDocuments);
                createAllFilesInDocuments(document, tempDirectory);
            } else {
                verifyPagesIfDocumentAvailable(agreement, document, documentsNeedToDelete, tempDirectory);
            }
            if (!DocumentTypeCodeList.Signature.equals(document.getTypeCode())
                    && !DocumentTypeCodeList.Photograph.equals(document.getTypeCode())) {
                agreement.setStatusCode(AgreementStatusCodeList.Final);
            }
            // updation the agreement object
            agreementRequest.setType(agreement);
            agreementComponent.mergeAgreement(agreementRequest);

            // deleting the unwanted files
            if (CollectionUtils.isNotEmpty(documentsNeedToDelete)) {
                deleteUnwantedFiles(documentsNeedToDelete, tempDirectory);
            }
        }
    }
    
    /**
     * Creates the directory.
     * 
     * @param tempDirectory
     *            the temp directory
     */
    protected void createDirectory(final File tempDirectory) {
        if (!tempDirectory.exists() && !tempDirectory.mkdirs()) {
            LOGGER.error("Failed to create directory : " + tempDirectory.getPath());
            throwSystemException(true, Constants.FAILED_DIRECTORY_CREATION);
        }
    }

    /**
     * Verify pages if document available.
     * 
     * @param agreement
     *            the agreement
     * @param document
     *            the document
     * @param documentsNeedToDelete
     *            the documents need to delete
     * @param tempDirectory
     *            the temp directory
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void verifyPagesIfDocumentAvailable(final InsuranceAgreement agreement, final AgreementDocument document,
            final Set<String> documentsNeedToDelete, final File tempDirectory) throws IOException {

        boolean isUpdate = false;
        for (final Iterator<AgreementDocument> j = agreement.getRelatedDocument().iterator(); j.hasNext();) {
            final AgreementDocument agreementDocument = j.next();
            // If there is a document with name same as the in the input request,
            // we need compare each pages in order to check whether any page is deleted or new page is added
            
            if (agreementDocument.getName()!=null && agreementDocument.getName().equals(document.getName()) && !("Deleted".equalsIgnoreCase(document.getDocumentStatus()))) {
                // This variable is used to identify whether the current document is available in our db or not
                isUpdate = true;
                // New page set for replacing with old page set
                final Set<Document> newPageSet = new LinkedHashSet<Document>();

                // loading all existing file names in to set for deletion purpose
                for (Document currentPage : agreementDocument.getPages()) {
                    documentsNeedToDelete.add(currentPage.getFileNames());
                }

                // iteration through new document set
                for (Document pageDocument : document.getPages()) {
                    boolean identifiedPage = false;
                    // checking each new page name with the current pages
                    for (final Iterator<Document> i = agreementDocument.getPages().iterator(); i.hasNext();) {
                        final Document element = i.next();
                        if (pageDocument.getFileNames().equals(element.getFileNames())) {
                            identifiedPage = true;
                            // if the page exists in our db, just copying the document object to the temporary
                            // new page set
                            newPageSet.add(element);
                            // Since this page should retain as it is, removing it from the file name list for
                            // the deletion
                            documentsNeedToDelete.remove(element.getFileNames());
                            break;
                        }

                    }
                    // if the new page is not present in our db, copying the page object to the temporary new
                    // page set
                    // and creating the file in physical location
                    if (!identifiedPage) {
                        newPageSet.add(pageDocument);
                        createOnePageInDocuments(pageDocument, tempDirectory, document.getTypeCode());
                    }
                }
                // setting the new page set in current document object
                agreementDocument.setPages(newPageSet);
                if(document.getDescription() != null && !document.getDescription().equalsIgnoreCase(agreementDocument.getDescription())){
                	agreementDocument.setDescription(document.getDescription());	
                }
                if(document.getSignedIndicator() != null ){
                	agreementDocument.setSignedIndicator(document.getSignedIndicator());
                }
                break;
                
            }else if(agreementDocument.getName()!=null && agreementDocument.getName().equals(document.getName()) && "Deleted".equalsIgnoreCase(document.getDocumentStatus())){
            	// This variable is used to identify whether the current document is available in our db or not
                isUpdate = true;
                
               // remove the document from  old page set
                agreement.getRelatedDocument().remove(agreementDocument);
                
                // loading all existing file names in to set for deletion purpose
                for (Document currentPage : agreementDocument.getPages()) {
                    documentsNeedToDelete.add(currentPage.getFileNames());
                }
                break;
                
            }
        }
        // if the current traversing document is not available in our db, we are adding it to the document list
        // and creating each page in physical location
        if (!isUpdate && !("Deleted".equalsIgnoreCase(document.getDocumentStatus()))) {
            agreement.getRelatedDocument().add(document);
            createAllFilesInDocuments(document, tempDirectory);
        }

    }

    /**
     * Delete unwanted files.
     * 
     * @param documents
     *            the documents
     * @param tempDirectory
     *            the temp directory
     */
    protected void deleteUnwantedFiles(final Set<String> documents, final File tempDirectory) {
        for (String documentName : documents) {
            final File file =
                    new File(tempDirectory.getPath()+ slash + documentName);
            if (!file.delete()) {
                LOGGER.error("File Deletion Error! : " + file.getPath());
            }
        }
    }

    /**
     * Creates the one page in documents.
     * 
     * @param document
     *            the document
     * @param tempDirectory
     *            the temp directory
     * @param documentTypeCode
     *            the document type code
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void createOnePageInDocuments(final Document document, final File tempDirectory,
            final DocumentTypeCodeList documentTypeCode) throws IOException {
        if (document != null) {
            final String fileName = document.getFileNames();
            byte[] decodedBytes;
            if (DocumentTypeCodeList.Signature.equals(documentTypeCode)) {
                final String fileContent = document.getFileContent();
                decodedBytes = fileContent.getBytes();
            } else {
                String fileContent = document.getBase64string();
                final String pattern = "data(.*?)\\;base64,";
                fileContent = fileContent.replaceAll(pattern, "");
                // Converting a Base64 String into byte array
                decodedBytes = Base64.decodeBase64(fileContent);
            }
            createFile(fileName, decodedBytes, tempDirectory);
        }
    }

    /**
     * Creates the all files in documents.
     * 
     * @param agreementDocument
     *            the agreement document
     * @param tempDirectory
     *            the temp directory
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void createAllFilesInDocuments(final AgreementDocument agreementDocument, final File tempDirectory)
            throws IOException {
        if (agreementDocument.getPages() != null) {
            for (Document document : agreementDocument.getPages()) {
                createOnePageInDocuments(document, tempDirectory, agreementDocument.getTypeCode());
            }
        }
    }

    /**
     * Creates the file.
     * 
     * @param fileName
     *            the file name
     * @param fileByteArray
     *            the file byte array
     * @param tempDirectory
     *            the temp directory
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void createFile(final String fileName, final byte[] fileByteArray, final File tempDirectory)
            throws IOException {
        final File someFile = new File(tempDirectory, fileName);
        final FileOutputStream fos = new FileOutputStream(someFile);
        fos.write(fileByteArray);
        fos.flush();
        fos.close();
    }
    
    protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(String operation){
    	List<AgreementStatusCodeList> statusCodeLists=new ArrayList<AgreementStatusCodeList>();
    	if(Constants.UPDATE.equals(operation)){
    		statusCodeLists.add(AgreementStatusCodeList.Initial);
    		statusCodeLists.add(AgreementStatusCodeList.Final);
    		statusCodeLists.add(AgreementStatusCodeList.Proposed);
    	}
    	return statusCodeLists;
    }
    
    /**
     * Gets the entity manager.
     * 
     * @return the entityManager
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Sets the entity manager.
     * 
     * @param entityManager
     *            the entityManager to set
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
