package com.cognizant.insurance.omnichannel.utils;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.domain.agreement.Coverage;
import com.cognizant.insurance.domain.agreement.agreementpremium.Premium;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.commonelements.TransactionKeys;
import com.cognizant.insurance.domain.commonelements.commonclasses.GLISelectedOption;
import com.cognizant.insurance.domain.commonelements.commonclasses.UserSelection;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.contactandplace.ContactPoint;
import com.cognizant.insurance.domain.contactandplace.ContactPreference;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.AddressNatureCodeList;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.ElectronicContact;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.PostalAddressContact;
import com.cognizant.insurance.domain.contactandplace.contactpointsubtypes.TelephoneCallContact;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.City;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.Country;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.CountryElement;
import com.cognizant.insurance.domain.contactandplace.placesubtypes.CountrySubdivision;
import com.cognizant.insurance.domain.extension.Extension;
import com.cognizant.insurance.domain.finance.FinancialScheduler;
import com.cognizant.insurance.domain.finance.paymentmethodsubtypes.CreditCardPayment;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.party.partycodelists.EducationLevelCodeList;
import com.cognizant.insurance.domain.party.partydetailsubtypes.PersonDetail;
import com.cognizant.insurance.domain.party.partyname.PersonName;
import com.cognizant.insurance.domain.party.persondetailsubtypes.EducationDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.IncomeDetail;
import com.cognizant.insurance.domain.party.persondetailsubtypes.OccupationDetail;
import com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes.EmploymentRelationship;

/**
 * The Class generaliEappScriptlet.
 */
public class GeneraliEappScriptlet extends JRDefaultScriptlet {

	private Set<PersonName> personNames;
	private Person person;
	private Set<PersonDetail> personDetails;
	private IncomeDetail incomeDetail;
	private Set<Country> countries;
	private Set<ContactPreference> contactPreferences;
	private PostalAddressContact postalAddressContact;
	private EducationDetail educationDetail;
	private PostalAddressContact permanentAddress;
	private OccupationDetail occupationDetail;
	private TelephoneCallContact home1Contact;
	private ElectronicContact electronicContact;
	
	private String insuredMobileNumber1;
	private String insuredHomeNumber1;
	private String insuredOfficeNumber;

	/** Declaration Questions set**/
	private Set<UserSelection> declationQuestions;
	private String agentSignDate;
	private String agentSignPlace;
	private Date spajSignDate;
	private String SpajSignPlace;
	private String spajAccountHolderName;
	private String spajBankName;
	private String spajBranchName;
	private String spajAccountNumber;

	private String identityNo;
	private Date insuredIdentityDate;
	private String insuredIdentityPlace;

	private BigDecimal insuredHeight;
	private BigDecimal insuredWeight;

	/** The proposer. */
	private AgreementHolder proposer;
	private Person proposerPerson;
	private Set<PersonName> proposerPersonNames;

	private Date proposerdob;
	private Date insuredDob;

	String proposerFullName;
	private Set<PersonDetail> proposerPersonDetails;
	private Set<ContactPreference> proposerContactPreferences;
	private Set<Country> proposerCountries;
	private Country proposerResidenceCountry;
	private Country insuredResidenceCountry;
	private ElectronicContact proposerElectronicContact;
	private PostalAddressContact proposerPostalAddressContact;
	private PostalAddressContact proposerPermanentAddress;
	private TelephoneCallContact telephoneCallContact;
	private String proposerMobileNumber1;
	private String proposerHomeNumber1;
	private String proposerOfficeNumber;

	private String relationshipWithProposer = "";
	private String insuredReasonForInsurance;
	private OccupationDetail proposerOccupationDetail;
	private String proposerIdentityProof;
	private String proposerIdentityDate;
	private String proposerIdentityPlace;

	IncomeDetail proposerIncomeDetail;
	EducationDetail proposerEducationDetail;
	String proposerPaymentMethod;


	/** The additional insured list. */
	private Set<Insured> additionalInsuredes;

	Insured firstAddInsured;
	Insured secondAddInsured;
	Insured thirdAddInsured;
	Insured fourthAddInsured;
	private Person firstAddInsuredPerson;
	private Person secondAddInsuredPerson;
	private Person thirdAddInsuredPerson;
	private Person fourthAddInsuredPerson;
	


	private Date firstAddInsuredDob;
	private Set<PersonName> firstAddInsuredPersonNames;
	private Set<PersonDetail> firstAddInsuredPersonDetails;
	private Set<Country> firstAddInsuredCountries;
	private Country firstAddInsuredResidenceCountry;
	private OccupationDetail firstAddInsuredOccupationDetail;
	private String firstAddInsuredRelationWithInsured;
	private BigDecimal firstAddInsuredHeight;
	private BigDecimal firstAddInsuredWeight;
	private String firstAddInsuredIdCardType;
	private String firstAddInsuredIdCardNumber;

	Date secondAddInsuredDob;
	private Set<PersonName> secondAddInsuredPersonNames;
	private Set<PersonDetail> secondAddInsuredPersonDetails;
	private Set<Country> secondAddInsuredCountries;
	Country secondAddInsuredResidenceCountry;
	OccupationDetail secondAddInsuredOccupationDetail;
	String secondAddInsuredRelationWithInsured;
	private String secondAddInsuredIdCardType;
	private String secondAddInsuredIdCardNumber;
	private BigDecimal secondAddInsuredHeight;
	private BigDecimal secondAddInsuredWeight;


	Date thirdAddInsuredDob;
	private Set<PersonName> thirdAddInsuredPersonNames;
	private Set<PersonDetail> thirdAddInsuredPersonDetails;
	private Set<Country> thirdAddInsuredCountries;
	Country thirdAddInsuredResidenceCountry;
	OccupationDetail thirdAddInsuredOccupationDetail;
	String thirdAddInsuredRelationWithInsured;
	private String thirdAddInsuredIdCardType;
	private String thirdAddInsuredIdCardNumber;
	private BigDecimal thirdAddInsuredHeight;
	private BigDecimal thirdAddInsuredWeight;

	Date fourthAddInsuredDob;
	private Set<PersonName> fourthAddInsuredPersonNames;
	private Set<PersonDetail> fourthAddInsuredPersonDetails;
	private Set<Country> fourthAddInsuredCountries;
	Country fourthAddInsuredResidenceCountry;
	OccupationDetail fourthAddInsuredOccupationDetail;
	String fourthAddInsuredRelationWithInsured;
	private String fourthAddInsuredIdCardType;
	private String fourthAddInsuredIdCardNumber;
	private BigDecimal fourthAddInsuredHeight;
	private BigDecimal fourthAddInsuredWeight;


	
	/** The beneficiary list. */

	private Person firstBeneficiaryPerson;
	private Set<PersonName> firstBeneficiaryPersonNames;
	private Date firstBeneficiaryDOB;
	private String firstBeneficiaryGender;
	private Float firstBeneficiarySharePercentage;
	private String firstBeneficiaryRelationWithInsured;

	private Person secondBeneficiaryPerson;
	private Set<PersonName> secondBeneficiaryPersonNames;
	private Date secondBeneficiaryDOB;
	private String secondBeneficiaryGender;
	private Float secondBeneficiarySharePercentage;
	private String secondBeneficiaryRelationWithInsured;

	private Person thirdBeneficiaryPerson;
	private Set<PersonName> thirdBeneficiaryPersonNames;
	private Date thirdBeneficiaryDOB;
	private String thirdBeneficiaryGender;
	private Float thirdBeneficiarySharePercentage;
	private String thirdBeneficiaryRelationWithInsured;

	private Person fourthBeneficiaryPerson;
	private Set<PersonName> fourthBeneficiaryPersonNames;
	private Date fourthBeneficiaryDOB;
	private String fourthBeneficiaryGender;
	private Float fourthBeneficiarySharePercentage;
	private String fourthBeneficiaryRelationWithInsured;

	private Person fifthBeneficiaryPerson;
	private Set<PersonName> fifthBeneficiaryPersonNames;
	private Date fifthBeneficiaryDOB;
	private String fifthBeneficiaryGender;
	private Float fifthBeneficiarySharePercentage;
	private String fifthBeneficiaryRelationWithInsured;




	private Set<UserSelection> proposerUserSelections;
	String proposerFatcaQues1;
	String proposerFatcaQues2;

	private Set<UserSelection> firstBeneficiarySelectedOptions;
	private Set<UserSelection> secondBeneficiarySelectedOptions;
	private Set<UserSelection> thirdBeneficiarySelectedOptions;
	private Set<UserSelection> fourthBeneficiarySelectedOptions;
	private Set<UserSelection> fifthBeneficiarySelectedOptions;
	Boolean beneficiaryFatcaQues1;
	Boolean beneficiaryFatcaQues2;

	String spajNumber;
	String illustrationNumber;
	String agentCode;

	

	

	/** Previous Policy */
	private Set<Insured> insuredPreviousPolicyList;
	private String firstCompanyName;
	private String firstPrevInsuredName;
	private String firstFinalDecision;
	private String firstTypeOfInsurance;
	private BigDecimal firstPrevSumAssured;
	private String firstPolicyStatus;

	private String secondCompanyName;
	private String secondPrevInsuredName;
	private String secondFinalDecision;
	private String secondTypeOfInsurance;
	private BigDecimal secondPrevSumAssured;
	private String secondPolicyStatus;

	private String thirdCompanyName;
	private String thirdPrevInsuredName;
	private String thirdFinalDecision;
	private String thirdTypeOfInsurance;
	private BigDecimal thirdPrevSumAssured;
	private String thirdPolicyStatus;

	private String fourthCompanyName;
	private String fourthPrevInsuredName;
	private String fourthFinalDecision;
	private String fourthTypeOfInsurance;
	private BigDecimal fourthPrevSumAssured;
	private String fourthPolicyStatus;

	private Boolean isPropDiffFromInsured;

	
	private static final Logger LOGGER = LoggerFactory.getLogger(GeneraliEappScriptlet.class);

	
	public void beforeDetailEval() {
		try {
			com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured insured = (com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured) this
					.getFieldValue("insured");
			
			try{
				if (null != insured) {
					person = (Person) insured.getPlayerParty();
					relationshipWithProposer = insured.getRelationshipWithProposer();
					insuredReasonForInsurance = insured.getReasonForInsurance();
				}
			}catch(Exception e){
				LOGGER.error("Error while getting insured Person "+e.getMessage());
			}

			try{
				if (null != person) {
					personNames = person.getName();
					personDetails = person.getDetail();
					contactPreferences = person.getPreferredContact();
					countries = person.getNationalityCountry();
					insuredResidenceCountry = person.getResidenceCountry();
					insuredDob = person.getBirthDate();
					if(person.getHeight()!= null && !"".equals(person.getHeight())){
						insuredHeight = person.getHeight().getValue();
					}
					if(person.getWeight()!= null && !"".equals(person.getWeight())){
						insuredWeight = person.getWeight().getValue();
					}
					identityNo = person.getIdentityProof();
					insuredIdentityDate = person.getIdentityDate();
					insuredIdentityPlace =person.getIdentityPlace();
				}
				}catch(Exception e){
					LOGGER.error("Error  appointeePerson"+e.getMessage());
				}
			
			
			
		

			try{
				insuredPreviousPolicyList =  (Set<Insured>) this.getFieldValue("insuredPreviousPolicyList");
			}catch(Exception e){
				LOGGER.error("Error while getting fieldValue insuredPreviousPolicyList "+e.getMessage());
			}
			
			
			
			
			
			
			
			
			if (null != personDetails) {
				for (PersonDetail personDetail : personDetails) {
					if (personDetail instanceof IncomeDetail) {
						incomeDetail = (IncomeDetail) personDetail;
					} else if (personDetail instanceof EducationDetail) {
						educationDetail = (EducationDetail) personDetail;
					} else if (personDetail instanceof OccupationDetail) {
						occupationDetail = (OccupationDetail) personDetail;
					}
				}
			}


			if (null != contactPreferences) {
				for (ContactPreference contactPreference : contactPreferences) {
					ContactPoint contactPoint = contactPreference
							.getPreferredContactPoint();
					if (contactPoint instanceof PostalAddressContact) {
						if (((PostalAddressContact) contactPoint)
								.getAddressNatureCode().equals(
										AddressNatureCodeList.Current)) {
							postalAddressContact = (PostalAddressContact) contactPoint;
						} else if (((PostalAddressContact) contactPoint)
								.getAddressNatureCode().equals(
										AddressNatureCodeList.Permanent)) {
							permanentAddress = (PostalAddressContact) contactPoint;
						}

					} else if (contactPoint instanceof TelephoneCallContact) {
						if (null != contactPreference.getPriorityLevel()) {
							if ((contactPreference.getPriorityLevel() == 1)) {
								home1Contact = (TelephoneCallContact) contactPoint;
							}
						}
					} else if (contactPoint instanceof ElectronicContact) {
						electronicContact = (ElectronicContact) contactPoint;
					}
				}
			}


			com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument document =
					(com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument) this.getFieldValue("document");

			
			//--------------------------------//
			try {
				proposer = (com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder) this
						.getFieldValue("proposer");
				proposerFatcaQues1="No";
				proposerFatcaQues2="No";
				if (null != proposer) {
					proposerPerson = (Person) proposer.getPlayerParty();
					proposerUserSelections = proposer.getSelectedOptions();
					isPropDiffFromInsured = proposer.getProposerDifferentFromInsuredIndicator();
				}
				if(proposerUserSelections != null && proposerUserSelections.size() >0){
					for(UserSelection selection:proposerUserSelections){
						if(selection != null && selection.getQuestionnaireType() != null){
							if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
								if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
										selection.getSelectedOption() == true){
									proposerFatcaQues1 = "Yes";
								}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
										selection.getSelectedOption() == true){
									proposerFatcaQues2 = "Yes";
								}
							}
						}
					}
				}

				if (null != proposerPerson) {
					proposerdob = proposerPerson.getBirthDate();
					proposerPersonNames = proposerPerson.getName();
					proposerPersonDetails = proposerPerson.getDetail();
					proposerContactPreferences = proposerPerson.getPreferredContact();
					proposerCountries = proposerPerson.getNationalityCountry();
					proposerResidenceCountry = proposerPerson.getResidenceCountry();
					proposerIdentityProof = proposerPerson.getIdentityProof();
					proposerIdentityPlace = proposerPerson.getIdentityPlace();
					
				}

				if (null != proposerPersonDetails) {
					for (PersonDetail personDetail : proposerPersonDetails) {
						if (personDetail instanceof IncomeDetail) {
							proposerIncomeDetail = (IncomeDetail) personDetail;
						} else if (personDetail instanceof EducationDetail) {
							proposerEducationDetail = (EducationDetail) personDetail;
						} else
							if (personDetail instanceof OccupationDetail) {
								proposerOccupationDetail = (OccupationDetail) personDetail;
							}
					}
				}
			}
			catch(Exception e){
				LOGGER.error("ScriptletSample Error :while getting field value proposer"+ e);
			}
			

			beneficiaryFatcaQues1=false;
			beneficiaryFatcaQues2=false;
			Set<Beneficiary> beneficiaries=null;
			try{
				beneficiaries = (Set<Beneficiary>) this.getFieldValue("beneficiaries");

				Beneficiary firstBeneficiary = null;
				Beneficiary secondBeneficiary = null;
				Beneficiary thirdBeneficiary = null;
				Beneficiary fourthBeneficiary = null;
				Beneficiary fifthBeneficiary = null;

				if(beneficiaries != null){
					for(Beneficiary beneficiary:beneficiaries){
						if(beneficiary != null ){
							for(Extension benExtension : beneficiary.getPlayerParty().getIncludesExtension()){
								if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("1")){
									firstBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("2")){
									secondBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("3")){
									thirdBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("4")){
									fourthBeneficiary = beneficiary;
								}else if(benExtension.getName().equalsIgnoreCase("BeneficiaryId") &&
										benExtension.getValue().equalsIgnoreCase("5")){
									fifthBeneficiary = beneficiary;
								}
							}
						}
					}

					if(firstBeneficiary != null){
						firstBeneficiarySelectedOptions=firstBeneficiary.getSelectedOptions();
						firstBeneficiaryPerson = (Person)firstBeneficiary.getPlayerParty();
						if(firstBeneficiaryPerson != null){
							firstBeneficiaryPersonNames = firstBeneficiaryPerson.getName();
						}
						if(firstBeneficiarySelectedOptions != null && firstBeneficiarySelectedOptions.size() >0){
							for(UserSelection selection:firstBeneficiarySelectedOptions){
								if(selection != null && selection.getQuestionnaireType() != null){
									if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
										if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
												selection.getSelectedOption() == true){
											beneficiaryFatcaQues1 = selection.getSelectedOption();
										}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
												selection.getSelectedOption() == true){
											beneficiaryFatcaQues2 = selection.getSelectedOption();
										}
									}
								}
							}
						}
						firstBeneficiaryDOB = firstBeneficiaryPerson.getBirthDate();
						firstBeneficiaryGender = firstBeneficiaryPerson.getGenderCode().name();
						firstBeneficiarySharePercentage = firstBeneficiary.getSharePercentage();
						firstBeneficiaryRelationWithInsured = firstBeneficiary.getRelationWithInsured();
					}

					if(secondBeneficiary != null){
						secondBeneficiarySelectedOptions=secondBeneficiary.getSelectedOptions();
						secondBeneficiaryPerson = (Person)secondBeneficiary.getPlayerParty();
						if(secondBeneficiaryPerson != null){
							secondBeneficiaryPersonNames = secondBeneficiaryPerson.getName();
						}
						secondBeneficiaryDOB = secondBeneficiaryPerson.getBirthDate();
						secondBeneficiaryGender = secondBeneficiaryPerson.getGenderCode().name();
						secondBeneficiarySharePercentage = secondBeneficiary.getSharePercentage();
						secondBeneficiaryRelationWithInsured = secondBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(secondBeneficiarySelectedOptions != null && secondBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:secondBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null && 
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();
											}
										}
									}
								}
							}
						}
					}

					if(thirdBeneficiary != null){
						thirdBeneficiarySelectedOptions=thirdBeneficiary.getSelectedOptions();
						thirdBeneficiaryPerson = (Person)thirdBeneficiary.getPlayerParty();
						if(thirdBeneficiaryPerson != null){
							thirdBeneficiaryPersonNames = thirdBeneficiaryPerson.getName();
						}
						thirdBeneficiaryDOB = thirdBeneficiaryPerson.getBirthDate();
						thirdBeneficiaryGender = thirdBeneficiaryPerson.getGenderCode().name();
						thirdBeneficiarySharePercentage = thirdBeneficiary.getSharePercentage();
						thirdBeneficiaryRelationWithInsured = thirdBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(thirdBeneficiarySelectedOptions != null && thirdBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:thirdBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null && 
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();
											}
										}
									}
								}
							}
						}
					}

					if(fourthBeneficiary != null){
						fourthBeneficiarySelectedOptions=fourthBeneficiary.getSelectedOptions();
						fourthBeneficiaryPerson = (Person)fourthBeneficiary.getPlayerParty();
						if(fourthBeneficiaryPerson != null){
							fourthBeneficiaryPersonNames = fourthBeneficiaryPerson.getName();
						}

						fourthBeneficiaryDOB = fourthBeneficiaryPerson.getBirthDate();
						fourthBeneficiaryGender = fourthBeneficiaryPerson.getGenderCode().name();
						fourthBeneficiarySharePercentage = fourthBeneficiary.getSharePercentage();
						fourthBeneficiaryRelationWithInsured = fourthBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(fourthBeneficiarySelectedOptions != null && fourthBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:fourthBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null && 
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();

											}

										}
									}
								}
							}
						}
					}

					if(fifthBeneficiary != null){
						fifthBeneficiarySelectedOptions=fifthBeneficiary.getSelectedOptions();
						fifthBeneficiaryPerson = (Person)fifthBeneficiary.getPlayerParty();
						if(fifthBeneficiaryPerson != null){
							fifthBeneficiaryPersonNames = fifthBeneficiaryPerson.getName();
						}
						fifthBeneficiaryDOB = fifthBeneficiaryPerson.getBirthDate();
						fifthBeneficiaryGender = fifthBeneficiaryPerson.getGenderCode().name();
						fifthBeneficiarySharePercentage = fifthBeneficiary.getSharePercentage();
						fifthBeneficiaryRelationWithInsured = fifthBeneficiary.getRelationWithInsured();
						if(!beneficiaryFatcaQues1||!beneficiaryFatcaQues2){
							if(fifthBeneficiarySelectedOptions != null && fifthBeneficiarySelectedOptions.size() >0){
								for(UserSelection selection:fifthBeneficiarySelectedOptions){
									if(selection != null && selection.getQuestionnaireType() != null){
										if(selection.getQuestionnaireType().name().equalsIgnoreCase("Basic")){
											if(selection.getQuestionId() == 1 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues1 = selection.getSelectedOption();
											}else if(selection.getQuestionId() == 2 && selection.getSelectedOption() != null &&
													selection.getSelectedOption() == true){
												beneficiaryFatcaQues2 = selection.getSelectedOption();

											}
										}
									}
								}
							}
						}
					}

				}
			}catch (Exception e) {
				LOGGER.error("ScriptletSample Error : populating beneficiaries:",e);
			}


			try {
				InsuranceAgreement insuranceAgreement = (InsuranceAgreement) this.getFieldValue("proposal");
				try{
					if(insuranceAgreement != null){
						TransactionKeys transactionKey = insuranceAgreement.getTransactionKeys();
						if(transactionKey != null){
							spajNumber = transactionKey.getKey21();
							illustrationNumber = transactionKey.getKey24();
							//agentCode = transactionKey.getKey11();
						}
					}
				}catch(Exception e){
					LOGGER.error("Error while getting keys "+e.getMessage());
				}


				if(insuranceAgreement != null && insuranceAgreement.getAgreementExtension() != null){
					declationQuestions=insuranceAgreement.getAgreementExtension().getSelectedOptions();
					agentSignDate = insuranceAgreement.getAgreementExtension().getAgentSignDate();
					agentSignPlace = insuranceAgreement.getAgreementExtension().getAgentSignPlace();
					spajSignDate = insuranceAgreement.getAgreementExtension().getSpajDate();
					SpajSignPlace = insuranceAgreement.getAgreementExtension().getSpajSignPlace();
					spajAccountHolderName = insuranceAgreement.getAgreementExtension().getSpajDeclarationName1();
					spajBankName = insuranceAgreement.getAgreementExtension().getSpajDeclarationName2();
					spajAccountNumber = insuranceAgreement.getAgreementExtension().getSpajDeclarationName3();
					spajBranchName = insuranceAgreement.getAgreementExtension().getSpajDeclarationName4();
				}

				if(insuranceAgreement != null){
					for(Premium premium	:insuranceAgreement.getAgreementPremium()){
						if(("RenewalPremium").equals(premium.getNatureCode().name())){
							for(FinancialScheduler financialScheduler :premium.getAttachedFinancialScheduler()){
								proposerPaymentMethod =  financialScheduler.getPaymentMethodCode().name();
								CreditCardPayment creditCardPayment= (CreditCardPayment) financialScheduler.getPaymentMeans();
							}

						}
					}
				}

			}catch(Exception e){
				LOGGER.error("ScriptletSample Error :while getting field value proposer"+ e);
			}

			
			try{

				for(Insured additionalInsured :additionalInsuredes){
					if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("1")){
						firstAddInsured = additionalInsured;
					}else if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("2")){
						secondAddInsured = additionalInsured;
					}else if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("3")){
						thirdAddInsured = additionalInsured;
					}else if(additionalInsured.getPartyId()!= null &&
							additionalInsured.getPartyId().equalsIgnoreCase("4")){
						fourthAddInsured = additionalInsured;
					}
				}

				if (null != firstAddInsured) {
					firstAddInsuredPerson = (Person) firstAddInsured.getPlayerParty();
					firstAddInsuredRelationWithInsured = firstAddInsured.getRelationshipWithProposer();
				}
				if (null != secondAddInsured) {
					secondAddInsuredPerson = (Person) secondAddInsured.getPlayerParty();
					secondAddInsuredRelationWithInsured = secondAddInsured.getRelationshipWithProposer();
				}
				if (null != thirdAddInsured) {
					thirdAddInsuredPerson = (Person) thirdAddInsured.getPlayerParty();
					thirdAddInsuredRelationWithInsured = thirdAddInsured.getRelationshipWithProposer();
				}
				if (null != fourthAddInsured) {
					fourthAddInsuredPerson = (Person) fourthAddInsured.getPlayerParty();
					fourthAddInsuredRelationWithInsured = fourthAddInsured.getRelationshipWithProposer();
				}

				if (null != firstAddInsuredPerson) {
					firstAddInsuredDob = firstAddInsuredPerson.getBirthDate();
					firstAddInsuredPersonNames = firstAddInsuredPerson.getName();
					firstAddInsuredPersonDetails = firstAddInsuredPerson.getDetail();
					firstAddInsuredCountries = firstAddInsuredPerson.getNationalityCountry();
					firstAddInsuredResidenceCountry = firstAddInsuredPerson.getResidenceCountry();
					firstAddInsuredIdCardType = firstAddInsuredPerson.getNationalIdType();
					firstAddInsuredIdCardNumber = firstAddInsuredPerson.getNationalId();
					firstAddInsuredHeight = firstAddInsuredPerson.getHeight().getValue();
					firstAddInsuredWeight = firstAddInsuredPerson.getWeight().getValue();
				}
				if (null != firstAddInsuredPersonDetails) {
					for (PersonDetail personDetail : firstAddInsuredPersonDetails) {
						if (personDetail instanceof OccupationDetail) {
							firstAddInsuredOccupationDetail = (OccupationDetail) personDetail;
						}
					}
				}

				if (null != secondAddInsuredPerson) {
					secondAddInsuredDob = secondAddInsuredPerson.getBirthDate();
					secondAddInsuredPersonNames = secondAddInsuredPerson.getName();
					secondAddInsuredPersonDetails = secondAddInsuredPerson.getDetail();
					secondAddInsuredCountries = secondAddInsuredPerson.getNationalityCountry();
					secondAddInsuredResidenceCountry = secondAddInsuredPerson.getResidenceCountry();
					secondAddInsuredIdCardType = secondAddInsuredPerson.getNationalIdType();
					secondAddInsuredIdCardNumber = secondAddInsuredPerson.getNationalId();
					secondAddInsuredHeight = secondAddInsuredPerson.getHeight().getValue();
					secondAddInsuredWeight = secondAddInsuredPerson.getWeight().getValue();
				}

				if (null != secondAddInsuredPersonDetails) {
					for (PersonDetail personDetail : secondAddInsuredPersonDetails) {
						if (personDetail instanceof OccupationDetail) {
							secondAddInsuredOccupationDetail = (OccupationDetail) personDetail;
						}
					}
				}

				if (null != thirdAddInsuredPerson) {
					thirdAddInsuredDob = thirdAddInsuredPerson.getBirthDate();
					thirdAddInsuredPersonNames = thirdAddInsuredPerson.getName();
					thirdAddInsuredPersonDetails = thirdAddInsuredPerson.getDetail();
					thirdAddInsuredCountries = thirdAddInsuredPerson.getNationalityCountry();
					thirdAddInsuredResidenceCountry = thirdAddInsuredPerson.getResidenceCountry();
					thirdAddInsuredIdCardType = thirdAddInsuredPerson.getNationalIdType();
					thirdAddInsuredIdCardNumber = thirdAddInsuredPerson.getNationalId();
					thirdAddInsuredHeight = thirdAddInsuredPerson.getHeight().getValue();
					thirdAddInsuredWeight = thirdAddInsuredPerson.getWeight().getValue();
				}

				if (null != thirdAddInsuredPersonDetails) {
					for (PersonDetail personDetail : thirdAddInsuredPersonDetails) {
						if (personDetail instanceof OccupationDetail) {
							thirdAddInsuredOccupationDetail = (OccupationDetail) personDetail;
						}
					}
				}

				if (null != fourthAddInsuredPerson) {
					fourthAddInsuredDob = fourthAddInsuredPerson.getBirthDate();
					fourthAddInsuredPersonNames = fourthAddInsuredPerson.getName();
					fourthAddInsuredPersonDetails = fourthAddInsuredPerson.getDetail();
					fourthAddInsuredCountries = fourthAddInsuredPerson.getNationalityCountry();
					fourthAddInsuredResidenceCountry = fourthAddInsuredPerson.getResidenceCountry();
					fourthAddInsuredIdCardType = fourthAddInsuredPerson.getNationalIdType();
					fourthAddInsuredIdCardNumber = fourthAddInsuredPerson.getNationalId();
					fourthAddInsuredHeight = fourthAddInsuredPerson.getHeight().getValue();
					fourthAddInsuredWeight = fourthAddInsuredPerson.getWeight().getValue();
				}

				if (null != fourthAddInsuredPersonDetails) {
					for (PersonDetail personDetail : fourthAddInsuredPersonDetails) {
						if (personDetail instanceof OccupationDetail) {
							fourthAddInsuredOccupationDetail = (OccupationDetail) personDetail;
						}
					}
				}
			}catch(Exception ex){
				LOGGER.error("Error in additional insured "+ex.getMessage());
			}
		
			/** Previous Policy */

			try{
				if(insuredPreviousPolicyList != null && insuredPreviousPolicyList.size() >0){
					LOGGER.error("insuredPreviousPolicyList  "+insuredPreviousPolicyList.size());
					int count = 0;
					for(Insured ins:insuredPreviousPolicyList){
						if(count == 0){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									firstCompanyName = agmnt.getInsurerName();
									firstPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										firstPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										firstFinalDecision = agmnt.getAgreementExtension().getRemark();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											firstTypeOfInsurance = coverage.getMarketingName();
											firstPrevSumAssured = coverage.getSumAssured().getAmount();
										}

									}
								}
							}
						}else if(count == 1){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									secondCompanyName = agmnt.getInsurerName();
									secondPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										secondPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										secondFinalDecision = agmnt.getAgreementExtension().getRemark();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											secondTypeOfInsurance = coverage.getMarketingName();
											secondPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}else if(count == 2){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									thirdCompanyName = agmnt.getInsurerName();
									thirdPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										thirdPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										thirdFinalDecision = agmnt.getAgreementExtension().getRemark();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											thirdTypeOfInsurance = coverage.getMarketingName();
											thirdPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}else if(count == 3){
							if(ins.getIsPartyRoleIn() != null){
								InsuranceAgreement agmnt = (InsuranceAgreement)ins.getIsPartyRoleIn();
								if(agmnt != null){
									fourthCompanyName = agmnt.getInsurerName();
									fourthPolicyStatus = agmnt.getStatusCode().name();
									if(agmnt.getAgreementExtension()!= null){
										fourthPrevInsuredName = agmnt.getAgreementExtension().getPrevPolicyInsuredName();
										fourthFinalDecision = agmnt.getAgreementExtension().getRemark();
										for(Coverage coverage : agmnt.getIncludesCoverage()){
											fourthTypeOfInsurance = coverage.getMarketingName();
											fourthPrevSumAssured = coverage.getSumAssured().getAmount();
										}
									}
								}
							}
						}else if(count == 4){}
						count++;
					}
				}
			}catch(Exception e){
				LOGGER.warn("Error in getting previous policy details"+e);
			}
		} catch (Exception e) {
			LOGGER.warn("Error in beforeDetailEval"+e);
		}
	}
	//****************************************************************************//

	/* Main Insured Details*/
	
	public String getInsuredFullName() throws JRScriptletException {
		String name = null;
		for (PersonName personName : personNames) {
			name = personName.getFullName();
		}
		return name;
	}
	public Date getInsuredDob() {
		return insuredDob;
	}
	public String getInsuredYearOfBirth(){
		String year = "";
		if(isPropDiffFromInsured){
			try{
				Date insDob = getInsuredDob();
				String insDobValue = insDob.toString();
				if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
					year = insDobValue.substring(0, 4);
				}
			}catch(Exception e){
				LOGGER.error("Error while getting proposer year of birth ");
			}
		}
		return year;
	}

	public String getInsuredMonthOfBirth(){
		String month = "";
		if(isPropDiffFromInsured){
			try{
				Date insDob = getInsuredDob();
				String insDobValue = insDob.toString();
				if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
					month = insDobValue.substring(5, 7);
				}
			}catch(Exception e){
				LOGGER.error("Error while getting insured month of birth ");
			}
		}
		return month;
	}

	public String getInsuredDayOfBirth(){
		String date = "";
		if(isPropDiffFromInsured){
			try{
				Date insDob = getInsuredDob();
				String insDobValue = insDob.toString();
				if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
					date = insDobValue.substring(8, 10);
				}
			}catch(Exception e){
				LOGGER.error("Error while getting insured day of birth ");
			}
		}
		return date;
	}
	
	public String getInsuredGender() {
		return person.getGenderCode().name();
	}
	
	public String getInsuredNationality() {
		String nationality = "";
			for (Country country : countries) {
				nationality = country.getName();
			}
		return nationality;
	}
	
	
	public String getInsuredMaritalStatus() {
		return person.getMaritalStatusCode().name();
	}
	
	public BigDecimal getInsuredHeight() {
		return insuredHeight;
	}
	public BigDecimal getInsuredWeight() {
		return insuredWeight;
	}
	
	public String getInsuredEmail() {
		return electronicContact.getEmailAddress();
	}
	
	public String getInsuredMobileNumber() {
			populateCP();
			return insuredMobileNumber1;
	}
	
	public String getInsuredOfficeNumber() {
		return insuredOfficeNumber;
	}

	public String getInsuredHomeNumber() {
			populateCP();
			return insuredHomeNumber1;
	}
	public String getIdentityNo() {
		return identityNo;
	}

	public Date getInsuredIdentityDate() {
		return insuredIdentityDate;
	}

	public String getInsuredIdentityPlace() {
		return insuredIdentityPlace;
	}
	
	public String getInsuredPerStreetName() {
		return permanentAddress.getStreetName();
	}
	public String getInsuredPerHouseNo() {
		return permanentAddress.getBoxNumber();
	}
	
	public String getInsuredPermWard() {
		return permanentAddress.getAddressLine2();
	}
	public String getInsuredPermDistrict() {
		return permanentAddress.getAddressLine1();
	}
	
	public String getInsuredPermCity() {
		Set<CountryElement> countryElements = permanentAddress
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}
	
	public String getInsuredCurrentStreetName() {
		return postalAddressContact.getStreetName();
	}
	public String getInsuredCurrentHouseNo() {
		return postalAddressContact.getBoxNumber();
	}
	
	public String getInsuredCurrentWard() {
		return postalAddressContact.getAddressLine2();
	}
	public String getInsuredCurrentDistrict() {
		return postalAddressContact.getAddressLine1();
	}
	
	public String getInsuredCurrentCity() {
		Set<CountryElement> countryElements = postalAddressContact
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}
	
	public String getInsuredNameOfComapny() {
		return occupationDetail.getNameofInstitution();
	}

	public String getInsuredAddressOfComapny() {
		return postalAddressContact.getAddressLine3();
	}

	public String getInsuredOccupation() {
		return occupationDetail.getOccupationClass();
	}

	
	public String getInsuredNatureOfWork() {
		return occupationDetail.getNatureofWork();
	}
	
	public String getInsuredAnnualIncome() {
		return incomeDetail.getGrossIncome();
	}
	public String getInsuredBirthPlace(){
		String resCountry = "";
			if(insuredResidenceCountry != null){
				resCountry = insuredResidenceCountry.getName();
			}
		return resCountry;
	}
	/* Proposer Details*/

	public String getProposerFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName proposerPersonName : proposerPersonNames) {
			name = proposerPersonName.getGivenName();
		}
		return name;
	}

	public String getProposerLastName() throws JRScriptletException {
		String name = null;
		for (PersonName proposerPersonName : proposerPersonNames) {
			name = proposerPersonName.getSurname();
		}
		return name;
	}


	public String getProposerFirstAndLastName(){
		try {
			proposerFullName = getProposerFirstName()+" "+getProposerLastName();
		} catch (Exception e) {
			LOGGER.error("Error while retrieving ProposerFirstAndLastName "+e.getMessage());
		}
		return proposerFullName;

	}

	public String getProposerGender() {
		String gender = null;
		try {
			if (proposerPerson.getGenderCode() != null) {
				gender = proposerPerson.getGenderCode().name();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving getProposerGender "+e.getMessage());
		}

		return gender;
	}

	public Date getProposerdob() {
		if(proposerdob!= null && !"".equals(proposerdob)){
			return proposerdob;
		}
		return proposerdob;
	}

	public String getProposerNationality() {
		String nationality = null;
		for (Country country : proposerCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	public String getProposerEmail() {
		return proposerElectronicContact.getEmailAddress();
	}
	public String getProposerMobileNumber() {
		return proposerMobileNumber1;
	}
	public String getProposerHomeNumber() {
		return proposerHomeNumber1;
	}
	
	public String getProposerOfficeNumber() {
		return proposerOfficeNumber;
	}

	public String getProposerIdentityProof() {
		return proposerIdentityProof;
	}

	public String getProposerIdentityDate() {
		return proposerIdentityDate;
	}

	public String getProposerIdentityPlace() {
		return proposerIdentityPlace;
	}
	
	
	
	
	
	

	public void populateCP() {
		for (ContactPreference contactPreference : contactPreferences) {
			ContactPoint contactPoint = contactPreference
					.getPreferredContactPoint();
			if (contactPoint instanceof PostalAddressContact) {
				if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Current)) {
					postalAddressContact = (PostalAddressContact) contactPoint;
				} else if (((PostalAddressContact) contactPoint)
						.getAddressNatureCode().equals(
								AddressNatureCodeList.Permanent)) {
					permanentAddress = (PostalAddressContact) contactPoint;
				}

			}else if (contactPoint instanceof TelephoneCallContact) {
						telephoneCallContact = (TelephoneCallContact) contactPoint;
						if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
							insuredMobileNumber1 = telephoneCallContact.getFullNumber();
						} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
							insuredHomeNumber1 = telephoneCallContact.getFullNumber();
						}else if(telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Business)){
							insuredOfficeNumber  = telephoneCallContact.getFullNumber();
						}
			} else if (contactPoint instanceof ElectronicContact) {
				electronicContact = (ElectronicContact) contactPoint;
			}
		}
	}


	/** proposer contact details */
	public void populateProposerCP() {
		try {
			for (ContactPreference contactPreference : proposerContactPreferences) {
				ContactPoint contactPoint = contactPreference.getPreferredContactPoint();
				if (contactPoint instanceof PostalAddressContact) {
					if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Current)) {
						proposerPostalAddressContact = (PostalAddressContact) contactPoint;
					} else if (((PostalAddressContact) contactPoint).getAddressNatureCode().equals(AddressNatureCodeList.Permanent)) {
						proposerPermanentAddress = (PostalAddressContact) contactPoint;
					}
				} else if (contactPoint instanceof TelephoneCallContact) {
							telephoneCallContact = (TelephoneCallContact) contactPoint;
							if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Mobile)) {
								proposerMobileNumber1 = telephoneCallContact.getFullNumber();
							} else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Home)) {
								proposerHomeNumber1 = telephoneCallContact.getFullNumber();
							}else if (telephoneCallContact.getTypeCode().equals(TelephoneTypeCodeList.Business)) {
								proposerOfficeNumber = telephoneCallContact.getFullNumber();
							}
				} else if (contactPoint instanceof ElectronicContact) {
					proposerElectronicContact = (ElectronicContact) contactPoint;
				}
			}
		} catch (Exception e) {
			LOGGER.error("ScriptletSample Error :proposerContactPreferences"+ e.getStackTrace());
		}
	}

	



	

	
	

	
	

	
	



	

	

	





	

	public String getInsuredCompanyName() {
		String insuredCompany = "";
		if(isPropDiffFromInsured){
			if (occupationDetail != null) {
				insuredCompany=occupationDetail.getNameofInstitution();
			}
		}
		return insuredCompany;
	}
	public String getRelationshipWithProposer() {
		return relationshipWithProposer;
	}

	public String getOccupation() {
		String occupation = "";
		if(isPropDiffFromInsured){
			occupation = occupationDetail.getDescriptionOthers();
		}
		return occupation;
	}
	public String getOccupationIndustry() {
		String occupation = "";
		if(isPropDiffFromInsured){
			occupation = occupationDetail.getNatureofWorkValue();
			try{
				if(occupation != null && !"".equals(occupation)){
					if(occupation.equalsIgnoreCase("Others")){
						for(EmploymentRelationship relationship :occupationDetail.getProvidingEmployment()){
							occupation = relationship.getBusinessUnit();
						}
					}
					if(occupation != null && !"".equals(occupation) && occupation.contains("&amp;")){
						occupation = occupation.replace("&amp;","&");
					}
				}
			}catch(Exception e){
				LOGGER.error("getProposerOccupation .. "+e.getMessage());
			}
		}
		return occupation;
	}

	public String getJobDescription() {
		String jobDescription = "";
		if(isPropDiffFromInsured){
			for(EmploymentRelationship empRelationship:occupationDetail.getProvidingEmployment()){
				jobDescription = empRelationship.getJobDescription();
			}
		}
		return jobDescription;
	}



	
	
	/**
	 * Gets the spouse name.
	 *
	 * @return the spouse name
	 */
	public String getSpouseName() {
		return person.getSpouseOrParentName().getFullName();
	}

	


	
	/** Proposer detail */

	
	public String getProposerAddressLine1() {
		populateProposerCP();
		String addrLine1 = null;
		/*if(proposerPostalAddressContact!=null&&proposerPostalAddressContact.getPostalMunicipality()!=null){
			addrLine1 = proposerPostalAddressContact.getPostalMunicipality().getName();
		}*/
		addrLine1 = proposerPostalAddressContact.getAddressLine1();
		return addrLine1;
	}


	public String getProposerAddressLine2() {
		populateProposerCP();
		String addrLine2 = null;
		addrLine2 = proposerPostalAddressContact.getAddressLine2();
		return addrLine2;
	}

	public String getProposerCity() {
		populateProposerCP();
		Set<CountryElement> countryElements = proposerPostalAddressContact
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}

	public String getProposerState() {
		populateProposerCP();
		CountrySubdivision subDiv = proposerPostalAddressContact
				.getPostalCountrySubdivision();
		return subDiv.getName();
	}

	public String getProposerCountry() {
		populateProposerCP();
		Country country = proposerPostalAddressContact.getPostalCountry();
		if (country != null) {
			return country.getName();
		}
		return null;
	}


	public String getProposerZipCode() {
		populateProposerCP();
		if (null != proposerPostalAddressContact
				&& null != proposerPostalAddressContact.getPostalPostCode()
				&& null != proposerPostalAddressContact.getPostalPostCode()
				.getAssignedCode()) {
			return (proposerPostalAddressContact.getPostalPostCode()
					.getAssignedCode().getCode());
		}
		return null;
	}

	/** proposer permanent address */
	public String getProposerPermAddr1() {
		/*if(proposerPermanentAddress!=null&&proposerPermanentAddress.getPostalMunicipality()!=null){
			return proposerPermanentAddress.getPostalMunicipality().getName();
		}
		else{
			return null;
		}*/
		return proposerPermanentAddress.getAddressLine1();
	}

	public String getProposerPermAddr2() {
		return proposerPermanentAddress.getAddressLine2();
	}

	public String getProposerPermCity() {
		populateProposerCP();
		Set<CountryElement> countryElements = proposerPermanentAddress
				.getIncludedCountryElement();
		for (CountryElement countryElement : countryElements) {
			if (countryElement instanceof City) {
				return ((City) countryElement).getName();
			}
		}
		return null;
	}

	public String getProposerPermState() {
		populateProposerCP();
		CountrySubdivision subDiv = proposerPermanentAddress
				.getPostalCountrySubdivision();
		return subDiv.getName();
	}

	public String getProposerPermCountry() {
		populateProposerCP();
		Country country = proposerPermanentAddress.getPostalCountry();
		if (country != null) {
			return country.getName();
		}
		return null;
	}
	public String getProposerPermZipCode() {
		populateProposerCP();
		if (null != proposerPermanentAddress
				&& null != proposerPermanentAddress.getPostalPostCode()
				&& null != proposerPermanentAddress.getPostalPostCode()
				.getAssignedCode()) {
			return (proposerPermanentAddress.getPostalPostCode()
					.getAssignedCode().getCode());
		}
		return null;
	}

	/** Residence country taken as birth country */

	public String getProposerResidenceCountry(){
		String resCountry = null;
		if(proposerResidenceCountry != null){
			resCountry = proposerResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getProposerMaritalStatus() {
		if (proposerPerson != null) {
			if (proposerPerson.getMaritalStatusCode() != null) {
				return proposerPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getProposerCompanyName() {
		String proposerCompanyName = null;
		proposerCompanyName = proposerOccupationDetail.getNameofInstitution();
		return proposerCompanyName;
	}

	public String getProposerIndustry() {
		String proposerOccupation = null;
		proposerOccupation = proposerOccupationDetail.getNatureofWorkValue();
		try{
			if(proposerOccupation != null && !"".equals(proposerOccupation)){
				if(proposerOccupation.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :proposerOccupationDetail.getProvidingEmployment()){
						proposerOccupation = relationship.getBusinessUnit();
					}
				}
				if(proposerOccupation.contains("&amp;")){
					proposerOccupation = proposerOccupation.replace("&amp;","&");
				}
			}
		}catch(Exception e){
			LOGGER.error("getProposerOccupation .. "+e.getMessage());
		}
		return proposerOccupation;
	}
	public String getProposerOccupation() {
		String proposerOccupation = null;
		try{
			proposerOccupation = proposerOccupationDetail.getDescriptionOthers();
		}catch(Exception e){
			LOGGER.error("getProposerOccupation .. "+e.getMessage());
		}
		return proposerOccupation;
	}


	public String getProposerPosition() {
		String proposerPosition = null;
		for(EmploymentRelationship empRelationship: proposerOccupationDetail.getProvidingEmployment()){
			proposerPosition = empRelationship.getOccupationTypeCode();
		}
		return proposerPosition;
	}
	public String getInsuredPosition() {
		String insuredPosition = "";
		if(isPropDiffFromInsured){
			for(EmploymentRelationship empRelationship: occupationDetail.getProvidingEmployment()){
				insuredPosition = empRelationship.getOccupationTypeCode();
			}
		}
		return insuredPosition;
	}

	public String getProposerJobDescription() {
		String proposerJobDescription = null;
		for(EmploymentRelationship empRelationship:proposerOccupationDetail.getProvidingEmployment()){
			proposerJobDescription = empRelationship.getJobDescription();
		}
		return proposerJobDescription;
	}

	

	/** Additional insured */

	/** First additional insured */

	public Date getFirstAddInsuredDob() {
		return firstAddInsuredDob;
	}

	public String getFirstAddInsuredFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : firstAddInsuredPersonNames) {
			name = addInsuredPersonName.getGivenName();
		}
		return name;
	}

	public String getFirstAddInsuredLastName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : firstAddInsuredPersonNames) {
			name = addInsuredPersonName.getSurname();
		}
		return name;
	}


	public String getFirstAddInsuredFirstAndLastName(){
		String firstAddInsuredFullName = null;
		try {
			firstAddInsuredFullName = getFirstAddInsuredFirstName()+" "+getFirstAddInsuredLastName();
		} catch (Exception e) {
			LOGGER.error("Error getFirstAddInsuredFirstAndLastName "+e.getMessage());
		}
		return firstAddInsuredFullName;

	}
	public String getFirstAddInsuredGender() {
		return firstAddInsuredPerson.getGenderCode().name();
	}

	public String getFirstAddInsuredNationality() {
		String nationality = null;
		for (Country country : firstAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	/** Residence country taken as birth country */
	public String getFirstAddInsuredResidenceCountry(){
		String resCountry = null;
		if(firstAddInsuredResidenceCountry != null){
			resCountry = firstAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getFirstAddInsuredMaritalStatus() {
		if (firstAddInsuredPerson != null) {
			if (firstAddInsuredPerson.getMaritalStatusCode() != null) {
				return firstAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getFirstAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = firstAddInsuredOccupationDetail
				.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getFirstAddInsuredOccupation() {
		String firstAddInsuredOccupation = null;
		firstAddInsuredOccupation = firstAddInsuredOccupationDetail
				.getDescriptionOthers();
		return firstAddInsuredOccupation;
	}

	public String getFirstAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for (EmploymentRelationship employmentRelationship : firstAddInsuredOccupationDetail
				.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship
			.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getFirstAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for (EmploymentRelationship employmentRelationship : firstAddInsuredOccupationDetail
				.getProvidingEmployment()) {
			firstAddInsuredJobDescription = employmentRelationship
					.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}

	public String getFirstAddInsuredLineOfBusiness() {
		String firstAddInsuredLineOfBusiness = null;
		firstAddInsuredLineOfBusiness = firstAddInsuredOccupationDetail
				.getNatureofWorkValue();
		try{
			if(firstAddInsuredLineOfBusiness != null && !"".equals(firstAddInsuredLineOfBusiness)){
				if(firstAddInsuredLineOfBusiness.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :firstAddInsuredOccupationDetail.getProvidingEmployment()){
						firstAddInsuredLineOfBusiness = relationship.getBusinessUnit();
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("firstAddInsuredLineOfBusiness "+e.getMessage());
		}
		return firstAddInsuredLineOfBusiness;
	}
	public String getFirstAddInsuredRelationWithInsured() {
		return firstAddInsuredRelationWithInsured;
	}

	public String getFirstAddInsuredIdCardType() {
		return firstAddInsuredIdCardType;
	}

	public String getFirstAddInsuredIdCardNumber() {
		return firstAddInsuredIdCardNumber;
	}

	public BigDecimal getFirstAddInsuredHeight() {
		return firstAddInsuredHeight;
	}

	public BigDecimal getFirstAddInsuredWeight() {
		return firstAddInsuredWeight;
	}

	
	/** Second additional insured */

	public Date getSecondAddInsuredDob() {
		return secondAddInsuredDob;
	}
	public String getSecondAddInsuredRelationWithInsured() {
		return secondAddInsuredRelationWithInsured;
	}

	public String getSecondAddInsuredFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : secondAddInsuredPersonNames) {
			name = addInsuredPersonName.getGivenName();
		}
		return name;
	}

	public String getSecondAddInsuredLastName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : secondAddInsuredPersonNames) {
			name = addInsuredPersonName.getSurname();
		}
		return name;
	}
	public String getSecondAddInsuredFirstAndLastName(){
		String secondAddInsuredFullName = null;
		try {
			secondAddInsuredFullName = getSecondAddInsuredFirstName()+" "+getSecondAddInsuredLastName();
		} catch (Exception e) {
			LOGGER.error("Error getSecondAddInsuredFirstAndLastName "+e.getMessage());
		}
		return secondAddInsuredFullName;

	}

	public String getSecondAddInsuredGender() {
		return secondAddInsuredPerson.getGenderCode().name();
	}

	public String getSecondAddInsuredMaritalStatus() {
		if (secondAddInsuredPerson != null) {
			if (secondAddInsuredPerson.getMaritalStatusCode() != null) {
				return secondAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getSecondAddInsuredNationality() {
		String nationality = null;
		for (Country country : secondAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	public String getSecondAddInsuredResidenceCountry(){
		String resCountry = null;
		if(secondAddInsuredResidenceCountry != null){
			resCountry = secondAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getSecondAddInsuredIdCardType() {
		return secondAddInsuredIdCardType;
	}

	public String getSecondAddInsuredIdCardNumber() {
		return secondAddInsuredIdCardNumber;
	}

	public BigDecimal getSecondAddInsuredHeight() {
		return secondAddInsuredHeight;
	}

	public BigDecimal getSecondAddInsuredWeight() {
		return secondAddInsuredWeight;
	}

	public String getSecondAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = secondAddInsuredOccupationDetail.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getSecondAddInsuredOccupation() {
		String firstAddInsuredOccupation = null;
		firstAddInsuredOccupation = secondAddInsuredOccupationDetail.getDescriptionOthers();
		return firstAddInsuredOccupation;
	}

	public String getSecondAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for(EmploymentRelationship employmentRelationship:  secondAddInsuredOccupationDetail.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getSecondAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for(EmploymentRelationship employmentRelationship : secondAddInsuredOccupationDetail.getProvidingEmployment()){
			firstAddInsuredJobDescription=employmentRelationship.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}
	public String getSecondAddInsuredLineOfBusiness() {
		String secondAddInsuredLineOfBusiness = null;
		secondAddInsuredLineOfBusiness = secondAddInsuredOccupationDetail.getNatureofWorkValue();
		try{
			if(secondAddInsuredLineOfBusiness != null && !"".equals(secondAddInsuredLineOfBusiness)){
				if(secondAddInsuredLineOfBusiness.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :secondAddInsuredOccupationDetail.getProvidingEmployment()){
						secondAddInsuredLineOfBusiness = relationship.getBusinessUnit();
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("getSecondAddInsuredLineOfBusiness "+e.getMessage());
		}
		return secondAddInsuredLineOfBusiness;
	}

		/** Third additional insured */

	public Date getThirdAddInsuredDob() {
		return thirdAddInsuredDob;
	}
	public String getThirdAddInsuredFirstName() throws JRScriptletException {
		String name = "";
		for (PersonName addInsuredPersonName : thirdAddInsuredPersonNames) {
			name = addInsuredPersonName.getGivenName();
		}
		return name;
	}

	public String getThirdAddInsuredLastName() throws JRScriptletException {
		String name = "";
		for (PersonName addInsuredPersonName : thirdAddInsuredPersonNames) {
			name = addInsuredPersonName.getSurname();
		}
		return name;
	}
	public String getThirdAddInsuredFirstAndLastName(){
		String thirdAddInsuredFullName = null;
		try {
			thirdAddInsuredFullName = getThirdAddInsuredFirstName()+" "+getThirdAddInsuredLastName();
		} catch (Exception e) {
			LOGGER.error("Error getThirdAddInsuredFirstAndLastName "+e.getMessage());
		}
		return thirdAddInsuredFullName;

	}

	public String getThirdAddInsuredGender() {
		return thirdAddInsuredPerson.getGenderCode().name();
	}

	public String getThirdAddInsuredMaritalStatus() {
		if (thirdAddInsuredPerson != null) {
			if (thirdAddInsuredPerson.getMaritalStatusCode() != null) {
				return thirdAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getThirdAddInsuredNationality() {
		String nationality = null;
		for (Country country : thirdAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	public String getThirdAddInsuredResidenceCountry(){
		String resCountry = null;
		if(thirdAddInsuredResidenceCountry != null){
			resCountry = thirdAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getThirdAddInsuredIdCardType() {
		return thirdAddInsuredIdCardType;
	}

	public String getThirdAddInsuredIdCardNumber() {
		return thirdAddInsuredIdCardNumber;
	}

	public BigDecimal getThirdAddInsuredHeight() {
		return thirdAddInsuredHeight;
	}

	public BigDecimal getThirdAddInsuredWeight() {
		return thirdAddInsuredWeight;
	}

	public String getThirdAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = thirdAddInsuredOccupationDetail.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getThirdAddInsuredOccupation() {
		String firstAddInsuredOccupation = null;
		firstAddInsuredOccupation = thirdAddInsuredOccupationDetail.getDescriptionOthers();
		return firstAddInsuredOccupation;
	}

	public String getThirdAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for(EmploymentRelationship employmentRelationship:  thirdAddInsuredOccupationDetail.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getThirdAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for(EmploymentRelationship employmentRelationship : thirdAddInsuredOccupationDetail.getProvidingEmployment()){
			firstAddInsuredJobDescription=employmentRelationship.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}
	public String getThirdAddInsuredLineOfBusiness() {
		String thirdAddInsuredLineOfBusiness = null;
		thirdAddInsuredLineOfBusiness = thirdAddInsuredOccupationDetail.getNatureofWorkValue();
		try{
			if(thirdAddInsuredLineOfBusiness != null && !"".equals(thirdAddInsuredLineOfBusiness)){
				if(thirdAddInsuredLineOfBusiness.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :thirdAddInsuredOccupationDetail.getProvidingEmployment()){
						thirdAddInsuredLineOfBusiness = relationship.getBusinessUnit();
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("getThirdAddInsuredLineOfBusiness .. "+e.getMessage());
		}
		return thirdAddInsuredLineOfBusiness;
	}
	public String getThirdAddInsuredRelationWithInsured() {
		return thirdAddInsuredRelationWithInsured;
	}

	
	/** Fourth additional insured */

	public Date getFourthAddInsuredDob() {
		return fourthAddInsuredDob;
	}
	public String getFourthAddInsuredFirstName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : fourthAddInsuredPersonNames) {
			name = addInsuredPersonName.getGivenName();
		}
		return name;
	}

	public String getFourthAddInsuredLastName() throws JRScriptletException {
		String name = null;
		for (PersonName addInsuredPersonName : fourthAddInsuredPersonNames) {
			name = addInsuredPersonName.getSurname();
		}
		return name;
	}
	public String getFourthAddInsuredFirstAndLastName(){
		String fourthAddInsuredFullName = null;
		try {
			fourthAddInsuredFullName = getFourthAddInsuredFirstName()+" "+getFourthAddInsuredLastName();
		} catch (Exception e) {
			LOGGER.error("Error getFourthAddInsuredFirstAndLastName "+e.getMessage());
		}
		return fourthAddInsuredFullName;

	}

	public String getFourthAddInsuredGender() {
		return fourthAddInsuredPerson.getGenderCode().name();
	}

	public String getFourthAddInsuredMaritalStatus() {
		if (fourthAddInsuredPerson != null) {
			if (fourthAddInsuredPerson.getMaritalStatusCode() != null) {
				return fourthAddInsuredPerson.getMaritalStatusCode().name();
			}
		}
		return null;
	}

	public String getFourthAddInsuredNationality() {
		String nationality = null;
		for (Country country : fourthAddInsuredCountries) {
			nationality = country.getName();
		}
		return nationality;
	}

	public String getFourthAddInsuredResidenceCountry(){
		String resCountry = null;
		if(fourthAddInsuredResidenceCountry != null){
			resCountry = fourthAddInsuredResidenceCountry.getName();
		}
		return resCountry;
	}

	public String getFourthAddInsuredIdCardType() {
		return fourthAddInsuredIdCardType;
	}

	public String getFourthAddInsuredIdCardNumber() {
		return fourthAddInsuredIdCardNumber;
	}

	public BigDecimal getFourthAddInsuredHeight() {
		return fourthAddInsuredHeight;
	}

	public BigDecimal getFourthAddInsuredWeight() {
		return fourthAddInsuredWeight;
	}

	public String getFourthAddInsuredCompanyName() {
		String firstAddInsuredComapnyName = null;
		firstAddInsuredComapnyName = fourthAddInsuredOccupationDetail.getNameofInstitution();
		return firstAddInsuredComapnyName;
	}

	public String getFourthAddInsuredOccupation() {
		String firstAddInsuredOccupation = null;
		firstAddInsuredOccupation = fourthAddInsuredOccupationDetail.getDescriptionOthers();
		return firstAddInsuredOccupation;
	}

	public String getFourthAddInsuredPosition() {
		String firstAddInsuredPosition = null;
		for(EmploymentRelationship employmentRelationship:  fourthAddInsuredOccupationDetail.getProvidingEmployment())
			firstAddInsuredPosition = employmentRelationship.getOccupationTypeCode();
		return firstAddInsuredPosition;
	}

	public String getFourthAddInsuredJobDescription() {
		String firstAddInsuredJobDescription = null;
		for(EmploymentRelationship employmentRelationship : fourthAddInsuredOccupationDetail.getProvidingEmployment()){
			firstAddInsuredJobDescription=employmentRelationship.getJobDescription();
		}
		return firstAddInsuredJobDescription;
	}
	public String getFourthAddInsuredLineOfBusiness() {
		String fourthAddInsuredLineOfBusiness = null;
		fourthAddInsuredLineOfBusiness = fourthAddInsuredOccupationDetail.getNatureofWorkValue();
		try{
			if(fourthAddInsuredLineOfBusiness != null && !"".equals(fourthAddInsuredLineOfBusiness)){
				if(fourthAddInsuredLineOfBusiness.equalsIgnoreCase("Others")){
					for(EmploymentRelationship relationship :fourthAddInsuredOccupationDetail.getProvidingEmployment()){
						fourthAddInsuredLineOfBusiness = relationship.getBusinessUnit();
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("getFourthAddInsuredLineOfBusiness .. "+e.getMessage());
		}
		return fourthAddInsuredLineOfBusiness;
	}
	public String getFourthAddInsuredRelationWithInsured() {
		return fourthAddInsuredRelationWithInsured;
	}

		/** Beneficiary */
	/** First beneficiary */

	public String getFirstBeneficiaryFirstName() throws JRScriptletException {
		String firstBeneficiaryFirstName = "";
		for (PersonName firstBeneficiaryPersonName : firstBeneficiaryPersonNames) {
			firstBeneficiaryFirstName = firstBeneficiaryPersonName.getGivenName();
		}
		return firstBeneficiaryFirstName;
	}

	public String getFirstBeneficiaryLastName() throws JRScriptletException {
		String firstBeneficiaryLastName = "";
		for (PersonName firstBeneficiaryPersonName : firstBeneficiaryPersonNames) {
			firstBeneficiaryLastName = firstBeneficiaryPersonName.getSurname();
		}
		return firstBeneficiaryLastName;
	}

	public String getFirstBeneficiaryFirstAndLastName(){
		String firstBeneficiaryFullName = "";
		try {
			if (getFirstBeneficiaryLastName()!=null){
				firstBeneficiaryFullName = getFirstBeneficiaryFirstName()+" "+getFirstBeneficiaryLastName();
			}else{
				firstBeneficiaryFullName = getFirstBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving BeneficiaryFirstAndLastName "+e.getMessage());
		}
		return firstBeneficiaryFullName;
	}

	public Date getFirstBeneficiaryDob() {
		return firstBeneficiaryDOB;
	}

	public String getFirstBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(firstBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFirstBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getFirstBeneficiarySharePercentage() {
		return firstBeneficiarySharePercentage;
	}

	public String getFirstBeneficiaryRelationWithInsured() {
		String rel = "";
		if(firstBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(firstBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getFirstBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if(firstBeneficiaryDobValue != null && !"".equalsIgnoreCase(firstBeneficiaryDobValue)){
				year = firstBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first beneficiary year of birth ");
		}
		return year;
	}

	public String getFirstBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if(firstBeneficiaryDobValue != null && !"".equalsIgnoreCase(firstBeneficiaryDobValue)){
				month = firstBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first beneficiary month of birth ");
		}
		return month;
	}

	public String getFirstBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date firstBeneficiaryDob = getFirstBeneficiaryDob();
			String firstBeneficiaryDobValue = firstBeneficiaryDob.toString();
			if(firstBeneficiaryDobValue != null && !"".equalsIgnoreCase(firstBeneficiaryDobValue)){
				date = firstBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first beneficiary day of birth ");
		}
		return date;
	}

	/** Second beneficiary */

	public String getSecondBeneficiaryFirstName() throws JRScriptletException {
		String secondBeneficiaryFirstName = "";
		for (PersonName secondBeneficiaryPersonName : secondBeneficiaryPersonNames) {
			secondBeneficiaryFirstName = secondBeneficiaryPersonName.getGivenName();
		}
		return secondBeneficiaryFirstName;
	}

	public String getSecondBeneficiaryLastName() throws JRScriptletException {
		String secondBeneficiaryLastName = "";
		for (PersonName secondBeneficiaryPersonName : secondBeneficiaryPersonNames) {
			secondBeneficiaryLastName = secondBeneficiaryPersonName.getSurname();
		}
		return secondBeneficiaryLastName;
	}

	public String getSecondBeneficiaryFirstAndLastName(){
		String secondBeneficiaryFullName = "";
		try {
			if (getSecondBeneficiaryLastName()!=null){
				secondBeneficiaryFullName = getSecondBeneficiaryFirstName()+" "+getSecondBeneficiaryLastName();
			}else{
				secondBeneficiaryFullName = getSecondBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving SecondBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return secondBeneficiaryFullName;
	}

	public Date getSecondBeneficiaryDob() {
		return secondBeneficiaryDOB;
	}

	public String getSecondBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(secondBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFirstBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getSecondBeneficiarySharePercentage() {
		return secondBeneficiarySharePercentage;
	}

	public String getSecondBeneficiaryRelationWithInsured() {
		String rel = "";
		if(secondBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(secondBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getSecondBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if(secondBeneficiaryDobValue != null && !"".equalsIgnoreCase(secondBeneficiaryDobValue)){
				year = secondBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second beneficiary year of birth ");
		}
		return year;
	}

	public String getSecondBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if(secondBeneficiaryDobValue != null && !"".equalsIgnoreCase(secondBeneficiaryDobValue)){
				month = secondBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second beneficiary month of birth ");
		}
		return month;
	}

	public String getSecondBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date secondBeneficiaryDob = getSecondBeneficiaryDob();
			String secondBeneficiaryDobValue = secondBeneficiaryDob.toString();
			if(secondBeneficiaryDobValue != null && !"".equalsIgnoreCase(secondBeneficiaryDobValue)){
				date = secondBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second beneficiary day of birth ");
		}
		return date;
	}

	/** Third beneficiary */

	public String getThirdBeneficiaryFirstName() throws JRScriptletException {
		String thirdBeneficiaryFirstName = null;
		for (PersonName thirdBeneficiaryPersonName : thirdBeneficiaryPersonNames) {
			thirdBeneficiaryFirstName = thirdBeneficiaryPersonName.getGivenName();
		}
		return thirdBeneficiaryFirstName;
	}

	public String getThirdBeneficiaryLastName() throws JRScriptletException {
		String thirdBeneficiaryLastName = null;
		for (PersonName thirdBeneficiaryPersonName : thirdBeneficiaryPersonNames) {
			thirdBeneficiaryLastName = thirdBeneficiaryPersonName.getSurname();
		}
		return thirdBeneficiaryLastName;
	}

	public String getThirdBeneficiaryFirstAndLastName(){
		String thirdBeneficiaryFullName = null;
		try {
			if (getThirdBeneficiaryLastName()!=null){
				thirdBeneficiaryFullName = getThirdBeneficiaryFirstName()+" "+getThirdBeneficiaryLastName();
			}else{
				thirdBeneficiaryFullName = getThirdBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving ThirdBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return thirdBeneficiaryFullName;
	}

	public Date getThirdBeneficiaryDob() {
		return thirdBeneficiaryDOB;
	}

	public String getThirdBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(thirdBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFirstBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getThirdBeneficiarySharePercentage() {
		return thirdBeneficiarySharePercentage;
	}

	public String getThirdBeneficiaryRelationWithInsured() {
		String rel = "";
		if(thirdBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(thirdBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getThirdBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if(thirdBeneficiaryDobValue != null && !"".equalsIgnoreCase(thirdBeneficiaryDobValue)){
				year = thirdBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third beneficiary year of birth ");
		}
		return year;
	}

	public String getThirdBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if(thirdBeneficiaryDobValue != null && !"".equalsIgnoreCase(thirdBeneficiaryDobValue)){
				month = thirdBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third beneficiary month of birth ");
		}
		return month;
	}

	public String getThirdBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date thirdBeneficiaryDob = getThirdBeneficiaryDob();
			String thirdBeneficiaryDobValue = thirdBeneficiaryDob.toString();
			if(thirdBeneficiaryDobValue != null && !"".equalsIgnoreCase(thirdBeneficiaryDobValue)){
				date = thirdBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third beneficiary day of birth ");
		}
		return date;
	}

	/** Fourth beneficiary */

	public String getFourthBeneficiaryFirstName() throws JRScriptletException {
		String fourthBeneficiaryFirstName = "";
		for (PersonName fourthBeneficiaryPersonName : fourthBeneficiaryPersonNames) {
			fourthBeneficiaryFirstName = fourthBeneficiaryPersonName.getGivenName();
		}
		return fourthBeneficiaryFirstName;
	}

	public String getFourthBeneficiaryLastName() throws JRScriptletException {
		String fourthBeneficiaryLastName = "";
		for (PersonName fourthBeneficiaryPersonName : fourthBeneficiaryPersonNames) {
			fourthBeneficiaryLastName = fourthBeneficiaryPersonName.getSurname();
		}
		return fourthBeneficiaryLastName;
	}

	public String getFourthBeneficiaryFirstAndLastName(){
		String fourthBeneficiaryFullName = "";
		try {
			if (getFourthBeneficiaryLastName()!=null){
				fourthBeneficiaryFullName = getFourthBeneficiaryFirstName()+" "+getFourthBeneficiaryLastName();
			}else{
				fourthBeneficiaryFullName = getFourthBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving FourthBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return fourthBeneficiaryFullName;
	}

	public Date getFourthBeneficiaryDob() {
		return fourthBeneficiaryDOB;
	}

	public String getFourthBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(fourthBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFourthBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getFourthBeneficiarySharePercentage() {
		return fourthBeneficiarySharePercentage;
	}

	public String getFourthBeneficiaryRelationWithInsured() {
		String rel = "";
		if(fourthBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(fourthBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getFourthBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if(fourthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fourthBeneficiaryDobValue)){
				year = fourthBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth beneficiary year of birth ");
		}
		return year;
	}

	public String getFourthBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if(fourthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fourthBeneficiaryDobValue)){
				month = fourthBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth beneficiary month of birth ");
		}
		return month;
	}

	public String getFourthBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date fourthBeneficiaryDob = getFourthBeneficiaryDob();
			String fourthBeneficiaryDobValue = fourthBeneficiaryDob.toString();
			if(fourthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fourthBeneficiaryDobValue)){
				date = fourthBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth beneficiary day of birth ");
		}
		return date;
	}

	/** Fifth beneficiary */

	public String getFifthBeneficiaryFirstName() throws JRScriptletException {
		String fifthBeneficiaryFirstName = "";
		for (PersonName fifthBeneficiaryPersonName : fifthBeneficiaryPersonNames) {
			fifthBeneficiaryFirstName = fifthBeneficiaryPersonName.getGivenName();
		}
		return fifthBeneficiaryFirstName;
	}

	public String getFifthBeneficiaryLastName() throws JRScriptletException {
		String fifthBeneficiaryLastName = "";
		for (PersonName fifthBeneficiaryPersonName : fifthBeneficiaryPersonNames) {
			fifthBeneficiaryLastName = fifthBeneficiaryPersonName.getSurname();
		}
		return fifthBeneficiaryLastName;
	}

	public String getFifthBeneficiaryFirstAndLastName(){
		String fifthBeneficiaryFullName = "";
		try {
			if (getFifthBeneficiaryLastName()!=null){
				fifthBeneficiaryFullName = getFifthBeneficiaryFirstName()+" "+getFifthBeneficiaryLastName();
			}else{
				fifthBeneficiaryFullName = getFifthBeneficiaryFirstName();
			}
		} catch (Exception e) {
			LOGGER.error("Error while retrieving FifthBeneficiaryFirstAndLastName "+e.getMessage());
		}
		return fifthBeneficiaryFullName;
	}

	public Date getFifthBeneficiaryDob() {
		return fifthBeneficiaryDOB;
	}

	public String getFifthBeneficiaryGender() {
		String gender = "";
		try{
			gender = getBenGender(fifthBeneficiaryGender);
		}catch(Exception e){
			LOGGER.error("getFifthBeneficiaryGender :"+e.getMessage());
		}
		return gender;
	}

	public Float getFifthBeneficiarySharePercentage() {
		return fifthBeneficiarySharePercentage;
	}

	public String getFifthBeneficiaryRelationWithInsured() {
		String rel = "";
		if(fifthBeneficiaryRelationWithInsured != null){
			rel = getBenRelationInBahsa(fifthBeneficiaryRelationWithInsured);
		}
		return rel;
	}

	public String getFifthBeneficiaryYearOfBirth(){
		String year = null;
		try{
			Date fifthBeneficiaryDob = getFifthBeneficiaryDob();
			String fifthBeneficiaryDobValue = fifthBeneficiaryDob.toString();
			if(fifthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fifthBeneficiaryDobValue)){
				year = fifthBeneficiaryDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fifth beneficiary year of birth ");
		}
		return year;
	}

	public String getFifthBeneficiaryMonthOfBirth(){
		String month = null;
		try{
			Date fifthBeneficiaryDob = getFifthBeneficiaryDob();
			String fifthBeneficiaryDobValue = fifthBeneficiaryDob.toString();
			if(fifthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fifthBeneficiaryDobValue)){
				month = fifthBeneficiaryDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fifth beneficiary month of birth ");
		}
		return month;
	}

	public String getFifthBeneficiaryDayOfBirth(){
		String date = null;
		try{
			Date fifthBeneficiaryDob = getFifthBeneficiaryDob();
			String fifthBeneficiaryDobValue = fifthBeneficiaryDob.toString();
			if(fifthBeneficiaryDobValue != null && !"".equalsIgnoreCase(fifthBeneficiaryDobValue)){
				date = fifthBeneficiaryDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fifth beneficiary day of birth ");
		}
		return date;
	}

	/** Financial Data */

	public String getProposerSourceOfIncome(){
		String proposerSourceOfIncome = null;
		proposerSourceOfIncome = proposerIncomeDetail.getTypeCode().name();
		return proposerSourceOfIncome;
	}

	public String getProposerSourceOfIncomeDescription(){
		String proposerSourceOfIncomeDesc = null;
		proposerSourceOfIncomeDesc = proposerIncomeDetail.getDescription();
		return proposerSourceOfIncomeDesc;
	}


	public String getProposerGrossIncome(){
		String proposerGrossIncome = null;
		proposerGrossIncome = proposerIncomeDetail.getGrossIncome();
		return proposerGrossIncome;
	}

	public String getProposerGrossIncomeDescription(){
		String proposerGrossIncomeDesc = null;
		proposerGrossIncomeDesc = proposerIncomeDetail.getGrossIncomeDesc();
		return proposerGrossIncomeDesc;
	}
	
	public String getProposerPaymentMethod(){
		return proposerPaymentMethod;
	}
	
	
	
	public String getSpajNumber() {
		return spajNumber;
	}
	public String getIllustrationNumber() {
		return illustrationNumber;
	}


	

	public String getProposerYearOfBirth(){
		String year = null;
		try{
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if(propDobValue != null && !"".equalsIgnoreCase(propDobValue)){
				year = propDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting proposer year of birth ");
		}
		return year;
	}

	public String getProposerMonthOfBirth(){
		String month = null;
		try{
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if(propDobValue != null && !"".equalsIgnoreCase(propDobValue)){
				month = propDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting proposer month of birth ");
		}
		return month;
	}

	public String getProposerDayOfBirth(){
		String date = null;
		try{
			Date propDob = getProposerdob();
			String propDobValue = propDob.toString();
			if(propDobValue != null && !"".equalsIgnoreCase(propDobValue)){
				date = propDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting proposer month of birth ");
		}
		return date;
	}

	
	public String getFirstAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getFirstAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first addinsured year of birth ");
		}
		return year;
	}

	public String getFirstAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getFirstAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first addinsured month of birth ");
		}
		return month;
	}

	public String getFirstAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getFirstAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting first addinsured day of birth ");
		}
		return date;
	}
	public String getSecondAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getSecondAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second addinsured year of birth ");
		}
		return year;
	}

	public String getSecondAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getSecondAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second addinsured month of birth ");
		}
		return month;
	}

	public String getSecondAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getSecondAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting second addinsured day of birth ");
		}
		return date;
	}
	public String getThirdAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getThirdAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third addinsured year of birth ");
		}
		return year;
	}

	public String getThirdAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getThirdAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third addinsured month of birth ");
		}
		return month;
	}

	public String getThirdAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getThirdAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting third addinsured day of birth ");
		}
		return date;
	}
	public String getFourthAddInsuredYearOfBirth(){
		String year = null;
		try{
			Date insDob = getFourthAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				year = insDobValue.substring(0, 4);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth addinsured year of birth ");
		}
		return year;
	}

	public String getFourthAddInsuredMonthOfBirth(){
		String month = null;
		try{
			Date insDob = getFourthAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				month = insDobValue.substring(5, 7);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth addinsured month of birth ");
		}
		return month;
	}

	public String getFourthAddInsuredDayOfBirth(){
		String date = null;
		try{
			Date insDob = getFourthAddInsuredDob();
			String insDobValue = insDob.toString();
			if(insDobValue != null && !"".equalsIgnoreCase(insDobValue)){
				date = insDobValue.substring(8, 10);
			}
		}catch(Exception e){
			LOGGER.error("Error while getting fourth addinsured day of birth ");
		}
		return date;
	}

	public String getInsuredRelationWithProposer(){
		String relationshipWithProp = "";
		if(getIsPropDiffFromInsured()){
			return relationshipWithProposer;
		}
		return relationshipWithProp;
	}

	
	

	
	
	public String splitDate(Date date, int firstIndex , int lastIndex) throws Exception{
		String dobValue = date.toString();
		String dateSplit="";

		if(dobValue != null && !"".equalsIgnoreCase(dobValue)){
			dateSplit = dobValue.substring(firstIndex, lastIndex);
		}

		return dateSplit;
	}

	public String getDecalrationQuestionAnswer(String Number) {
		String answer="";
		try{

			for(UserSelection question : declationQuestions)
			{
				if(Number.equals(question.getQuestionId().toString()) && ("Basic").equals(question.getQuestionnaireType().name()) ){
					if(true==question.getSelectedOption())
					{
						answer="true";

					}
					else if(false==question.getSelectedOption()){
						answer="false";
					}
					break;
				}
			}


		}catch(Exception e){
			LOGGER.error("Error while getting decalartion answer option of question "+Number);
		}
		return answer;

	}

	public String getDecalrationQuestionAnswerDetail(String Number) {
		String answer="";
		try{
			for(UserSelection question : declationQuestions)
			{
				if(Number.equals("7")){
					if(question.getQuestionId() != null && question.getDetailedInfo() != null && question.getQuestionId().toString().equals("7")){
						answer=question.getDetailedInfo();
					}
				}else if(Number.equals(question.getQuestionId().toString()) && ("Basic").equals(question.getQuestionnaireType().name())){
					if(true==question.getSelectedOption()){
						/*if(!empty.equals(question.getDetailedInfo())){
							answer=question.getDetailedInfo()+","+" ";
						}*/
						if(question.getDetailedInfo() != null){
							answer=question.getDetailedInfo()+",";
						}
						for(GLISelectedOption answerOption : question.getSelectedOptions()){
							/*if(!empty.equals(answerOption.getSelectedOptionValue())){
								answer+=answerOption.getSelectedOptionValue()+ ","+" ";
							}*/
							if(answerOption.getSelectedOptionValue() != null){
								answer+=answerOption.getSelectedOptionValue()+ ",";
							}
						}
						String words[] = answer.split(",");
						answer = "";
						for(int i=0;i<words.length;i++){
							if(!words[i].equals("")){
								answer += words[i]+",";
							}
						}
						answer=answer.substring(0, answer.length()-1);
						break;
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("Error while getting decalartion answer detail of question "+Number);
		}
		return answer;

	}
	
	public String getProposerFacta1(){
		return proposerFatcaQues1;
	}

	public String getProposerFacta2(){
		return proposerFatcaQues2;
	}

	public Boolean getBeneficiaryFacta1(){
		return beneficiaryFatcaQues1;
	}

	public Boolean getBeneficiaryFacta2(){
		return beneficiaryFatcaQues2;
	}

	
	
	public String getAgentCode() {
		return agentCode;
	}

	/** Previous Policy */

	public String getFirstCompanyName() {
		return firstCompanyName;
	}

	public String getFirstPrevInsuredName() {
		return firstPrevInsuredName;
	}

	public String getFirstFinalDecision() {
		return firstFinalDecision;
	}

	public String getFirstTypeOfInsurance() {
		return firstTypeOfInsurance;
	}

	public String getFirstPolicyStatus() {
		return firstPolicyStatus;
	}

	public BigDecimal getFirstPrevSumAssured() {
		return firstPrevSumAssured;
	}

	public String getSecondCompanyName() {
		return secondCompanyName;
	}

	public String getSecondPrevInsuredName() {
		return secondPrevInsuredName;
	}

	public String getSecondFinalDecision() {
		return secondFinalDecision;
	}

	public String getSecondTypeOfInsurance() {
		return secondTypeOfInsurance;
	}

	public String getSecondPolicyStatus() {
		return secondPolicyStatus;
	}

	public BigDecimal getSecondPrevSumAssured() {
		return secondPrevSumAssured;
	}

	public String getThirdCompanyName() {
		return thirdCompanyName;
	}

	public String getThirdPrevInsuredName() {
		return thirdPrevInsuredName;
	}

	public String getThirdFinalDecision() {
		return thirdFinalDecision;
	}

	public String getThirdTypeOfInsurance() {
		return thirdTypeOfInsurance;
	}

	public String getThirdPolicyStatus() {
		return thirdPolicyStatus;
	}

	public BigDecimal getThirdPrevSumAssured() {
		return thirdPrevSumAssured;
	}

	public String getFourthCompanyName() {
		return fourthCompanyName;
	}

	public String getFourthPrevInsuredName() {
		return fourthPrevInsuredName;
	}

	public String getFourthFinalDecision() {
		return fourthFinalDecision;
	}

	public String getFourthTypeOfInsurance() {
		return fourthTypeOfInsurance;
	}

	public String getFourthPolicyStatus() {
		return fourthPolicyStatus;
	}

	public BigDecimal getFourthPrevSumAssured() {
		return fourthPrevSumAssured;
	}

		public String getAgentSignDate() {
		return agentSignDate;
	}

	
	
	

	public String getAgentSignPlace() {
		return agentSignPlace;
	}

	public Date getSpajSignDate() {
		return spajSignDate;
	}

	
	

	public String getSpajSignPlace() {
		return SpajSignPlace;
	}

	

	

	

	
	public Boolean getIsPropDiffFromInsured() {
		return isPropDiffFromInsured;
	}

	
	
	

		public String getBenRelationInBahsa(String relation){
		String rel = "";
		try{
			if(relation != null && !"".equals(relation)){
				if(relation.equalsIgnoreCase("Husband")){
					rel = "Suami";
				}else if(relation.equalsIgnoreCase("Son")){
					rel = "Anak Laki Laki";
				}else if(relation.equalsIgnoreCase("Father")){
					rel = "Ayah";
				}else if(relation.equalsIgnoreCase("Wife")){
					rel = "Istri";
				}else if(relation.equalsIgnoreCase("Daughter")){
					rel = "Anak Perempuan";
				}else if(relation.equalsIgnoreCase("Mother")){
					rel = "Ibu";
				}else if(relation.equalsIgnoreCase("Siblings")){
					rel = "Saudara Kandung";
				}else if(relation.equalsIgnoreCase("Others")){
					rel = "Lainnya";
				}
			}
		}catch(Exception e){
			LOGGER.error("Error in getBenRelationInBahsa :"+e.getMessage());
		}
		return rel;
	}

	public String getBenGender(String gender){
		String gen = "";
		if(gender != null){
			if(gender.equalsIgnoreCase("Male")){
				gen = "Pria";
			}else if(gender.equalsIgnoreCase("Female")){
				gen = "Wanita";
			}
		}
		return gen;
	}

}