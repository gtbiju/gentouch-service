/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.NoSuchElementException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngageAuditComponent;
import com.cognizant.insurance.component.LifeEngageEAppComponent;
import com.cognizant.insurance.component.LifeEngageFNAComponent;
import com.cognizant.insurance.component.LifeEngageIllustrationComponent;
import com.cognizant.insurance.component.LifeEngageLMSComponent;
import com.cognizant.insurance.component.LifeEngageSearchCriteriaComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.EmailRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
/* Added for security_optimizations */
import com.cognizant.insurance.core.exception.InputValidationException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;
import com.cognizant.insurance.service.helper.LifeEngageValidationHelper;
/* Added for security_optimizations */
import com.cognizant.insurance.services.LifeEngageSyncService;

// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageSyncServiceImpl.
 * 
 * @author 300797
 */
@Service("leSyncService")
public class LifeEngageSyncServiceImpl implements LifeEngageSyncService {

    /** The life engage eapp component. */
    @Autowired
    private LifeEngageEAppComponent lifeEngageEAppComponent;

    /** The life engage illustration component. */
    @Autowired
    private LifeEngageIllustrationComponent lifeEngageIllustrationComponent;

    /** The life engage FNA component. */
    @Autowired
    private LifeEngageFNAComponent lifeEngageFNAComponent;

    /** The life engage e LMS component. */
    @Autowired
    private LifeEngageLMSComponent lifeEngageLMSComponent;

    /** The life engage search criteria component. */
    @Autowired
    private LifeEngageSearchCriteriaComponent lifeEngageSearchCriteriaComponent;

    /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;

    /** The life engage audit component. */
    @Autowired
    private LifeEngageAuditComponent lifeEngageAuditComponent;

    /** The Constant REQUEST. */
    private static final String REQUEST = "Request";

    /** The Constant REQUEST_INFO. */
    private static final String REQUEST_INFO = "RequestInfo";

    /** The Constant TRANSACTIONS. */
    private static final String TRANSACTIONS = "Transactions";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";

    /** The Constant TYPE. */
    private static final String TYPE = "Type";

    /** The Constant REQUEST_PAYLOAD. */
    private static final String REQUEST_PAYLOAD = "RequestPayload";

    /** The Constant OBSERVATIONS. */
    private static final String OBSERVATIONS = "Observations";

    /** The Constant OBSERVATIONS. */
    private static final String OBSERVATION = "Observation";

    /** The Constant Key4. */
    private static final String KEY4 = "Key4";
    
    /** The Constant KEY3. */
   	private static final String KEY3 = "Key3";	

    /** The Constant FNA. */
    private static final String FNA = "FNA";

    /** The Constant LMS. */
    private static final String LMS = "LMS";

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageSyncServiceImpl.class);

    /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = "TransactionData";

    /** The Constant IllustrationOutput. */
    private static final String ILLUSTRATION_OUTPUT = "IllustrationOutput";
    
    /** The Constant EMAIL. */
    private static final String EMAIL = "Email";
    
    /** The Constant PRODUCT. */
    private static final String PRODUCT = "Product";
    
    /** The Constant TEMPLATES. */
    private static final String TEMPLATES = "templates";
    
    /** The Constant EAPP_PDF_TEMPLATE. */
    private static final String EAPP_PDF_TEMPLATE = "eAppPdfTemplate";
    
    /** The Constant EAPP_PDF_TEMPLATE. */
    private static final String ILLUSTRATION_PDF_TEMPLATE = "illustrationPdfTemplate";
    
	/** The Constant POLICY_DETAILS. */
   	private static final String POLICY_DETAILS ="policyDetails";
   	
	/** The Constant COMMITTED_PREMIUM. */
   	private static final String COMMITTED_PREMIUM ="committedPremium";
   	
   	/** The Constant PRODUCT_NAME. */
   	private static final String PRODUCT_NAME = "productName";
   	
   	/** The Constant PRODUCT_DETAILS. */
   	private static final String PRODUCT_DETAILS ="ProductDetails";
        
 	/** The Constant EMAIL_ID. */
    private static final String EMAIL_ID = "toMailIds";
    
   	/** The Constant CC_EMAIL_ID. */
    private static final String CC_EMAIL_ID = "ccMailIds";
    
    /** The Constant EMAIL_FROM_ADDRESS. */
    private static final String EMAIL_FROM_ADDRESS = "fromMailId";
    
    /** The Constant MAIL_SUBJECT. */
   	private static final String MAIL_SUBJECT = "mailSubject";
   	
   	/** The Constant KEY18. */
    private static final String KEY18 = "Key18";
    
    /** The Constant KEY11. */
    private static final String KEY11 = "Key11";
    
    /** The Constant KEY2. */
    private static final String KEY2 = "Key2";
    
    /** The Constant TEMPLATE_ID. */
    private static final String TEMPLATE_ID = "templateId";
    
    /** The Constant AGENT ID. */
    private static final String AGENT_ID = "agentId";
    
    /** The Constant INITIALISED. */
    private static final String INITIALISED = "initialised";
    
    /** The Constant ID. */
    private static final String ID = "id";
    
    /** The Constant EMAIL_TEMPLATE. */
    private static final String EMAIL_TEMPLATE = "emailTemplate";

    /** The Constant FNA_PDF_TEMPLATE. */
    private static final String FNA_PDF_TEMPLATE = "fnaPdfTemplate";
    
    /** The e app limit. */
    @Value("${le.platform.service.eApp.fetchLimit}")
    protected Integer eAppLimit;

    /** The illustration limit. */
    @Value("${le.platform.service.illustration.fetchLimit}")
    protected Integer illustrationLimit;

    /** The fna limit. */
    @Value("${le.platform.service.fna.fetchLimit}")
    protected Integer fnaLimit;
    
    /** The lms limit. */
    @Value("${le.platform.service.lms.fetchLimit}")
    protected Integer lmsLimit;
    
    /* Added for security_optimizations */
    @Value("${le.platform.security.validationPattern}")
    private String validationPattern;
    
    /** The Constant Transaction Array. */
    private static final String TRANSACTION_ARRAY = "{\"Transactions\":[]}";

    /* Added for security_optimizations */
    
    @Autowired
	Job job;

	@Autowired
	JobLauncher jobLauncher;
	
	@Autowired
	private EmailRepository emailRepository;

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#save(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String save(final String json) throws BusinessException, CookieTheftException {
        LOGGER.trace("Inside LifeEngageSyncServiceImpl.Save :json " + json);
        JSONObject jsonObject;
        String id =  "";
        final JSONObject jsonObjectRs = new JSONObject();     
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, json);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            final JSONArray jsonRsArray = new JSONArray();
            LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
            if (jsonRqArray.length() > 0) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    
                    final JSONObject jsonObj = jsonRqArray.getJSONObject(i);                   
                    
                    String emailFlag = (String)jsonObj.getString(KEY18);    
                    
                    String response = null;
                    /* Added for security_optimizations */
                    LifeEngageValidationHelper.validateUser(jsonObj);
                    /* Added for security_optimizations */
                    try {
                    	/* Added for security_optimizations */
                    	LifeEngageValidationHelper.validateInputString(jsonObj.toString(),validationPattern);
                    	/* Added for security_optimizations */
                    	if (E_APP.equals(jsonObj.getString(TYPE))) {
                            response =
                                    lifeEngageEAppComponent.saveEApp(jsonObj.toString(), requestInfo, lifeEngageAudit);
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                savelifeEngageEmail(jsonObj, response);
                            }
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.SaveEApp response :response " + response);
                            
                        } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
                            /* Getting IllustartionOutput object from json */
                            JSONObject jsonTransactionData = null;
                            String jsonIllustrationOutput = null;
                            if (!(jsonObj.isNull(TRANSACTIONDATA))) {
                                jsonTransactionData = jsonObj.getJSONObject(TRANSACTIONDATA);
                                if (!(jsonTransactionData.isNull(ILLUSTRATION_OUTPUT))) {
                                    jsonIllustrationOutput =
                                            jsonTransactionData.getJSONObject(ILLUSTRATION_OUTPUT).toString();
                                }
                            }
                            /* EndOf Getting IllustartionOutput object from json */
                            response =
                                    lifeEngageIllustrationComponent.saveIllustration(jsonObj.toString(),
                                            jsonIllustrationOutput, requestInfo, lifeEngageAudit);
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                savelifeEngageEmail(jsonObj, response);
                            }  
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveIllustration response :response "
                                    + response);
                        } else if (FNA.equals(jsonObj.getString(TYPE))) {
                            response = lifeEngageFNAComponent.saveFNA(jsonObj.toString(), requestInfo, lifeEngageAudit);
                            if (emailFlag.equalsIgnoreCase(Constants.TRUE)) {
                                savelifeEngageEmail(jsonObj, response);
                            } 
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveFNA response :response " + response);
                        } else if (OBSERVATION.equals(jsonObj.getString(TYPE))) {
                            response =
                                    lifeEngageEAppComponent.saveToSyncObservation(jsonObj.toString(), requestInfo,
                                            lifeEngageAudit);
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveToSyncObservation response :response "
                                    + response);
                        } else if (LMS.equals(jsonObj.getString(TYPE))) {
                            response = lifeEngageLMSComponent.saveLMS(jsonObj.toString(), requestInfo, lifeEngageAudit);
                            LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveToSyncObservation response :response "
                                    + response);
                        }
                        jsonRsArray.put(new JSONObject(response));
                    } catch (SystemException e) {
                        jsonRsArray.put(new JSONObject(e.getMessage()));
                        /* Added for security_optimizations */
                    } catch(InputValidationException e){
                    	JSONObject jsonResponse= new JSONObject();
                    	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
                    	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
                    	jsonResponse.put("statusMessage", e.getMessage());                    	
                    	jsonRsArray.put(new JSONObject().put("StatusData", jsonResponse));
                    } catch (CookieTheftException e) {
                        LOGGER.error("CookieTheftException", e);
                        throw new CookieTheftException(Constants.PROCESSED_TRANSACTION);
                    } catch (Exception e) {
                    	LOGGER.error("Exception", e);
                    }
                    /* Added for security_optimizations */     
                    
                }
            }

            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.Save : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }

    /*
     * Deletes eapp (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#delete(java.lang .String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String delete(final String json) throws BusinessException {
        LOGGER.info("Inside save ==> input json : " + json);
        LOGGER.trace("Inside LifeEngageSyncServiceImpl : Delete");
        JSONObject requestJsonObject;
        String retrieveResponse;
        String deleteResponse = null;
        final JSONArray jsonRsArray = new JSONArray();
        final JSONObject deleteJsonObjectRs = new JSONObject();
        try {
            requestJsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = requestJsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            JSONObject jsonObj;
            retrieveResponse = retrieve(requestInfo, jsonRqArray);
            if (retrieveResponse != null) {
                final JSONObject saveReqJSON =
                        LifeEngageSyncServiceHelper.setDetailsInJsonRequest(retrieveResponse, jsonRequestObj);
                final LifeEngageAudit lifeEngageAudit =
                        lifeEngageAuditComponent.savelifeEngageAudit(requestInfo, saveReqJSON.toString());
                if (saveReqJSON.getJSONObject(REQUEST) != null
                        && saveReqJSON.getJSONObject(REQUEST) != null
                        && saveReqJSON.getJSONObject(REQUEST).getJSONObject(REQUEST_PAYLOAD).getJSONArray(TRANSACTIONS) != null) {
                    final JSONArray jsonTxnArray =
                            saveReqJSON.getJSONObject(REQUEST)
                                .getJSONObject(REQUEST_PAYLOAD)
                                .getJSONArray(TRANSACTIONS);
                    if (jsonTxnArray.length() > 0) {
                        for (int i = 0; i < jsonRqArray.length(); i++) {
                            jsonObj = jsonTxnArray.getJSONObject(i);
                            /* Added for security_optimizations */
                            LifeEngageValidationHelper.validateUser(jsonObj);
                            /* Added for security_optimizations */
                            try {
                            	/* Added for security_optimizations */
                            	LifeEngageValidationHelper.validateInputString(jsonObj.toString(),validationPattern);
                            	/* Added for security_optimizations */
                            	if (E_APP.equals(jsonObj.getString(TYPE))) {
                                    deleteResponse =
                                            lifeEngageEAppComponent.saveEApp(jsonObj.toString(), requestInfo,
                                                    lifeEngageAudit);
                                    LOGGER.trace("Inside LifeEngageSyncServiceImpl.SaveEApp deleteResponse :deleteResponse "
                                            + deleteResponse);
                                } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
                                    /* Getting IllustartionOutput object from json */
                                    JSONObject jsonTransactionData = null;
                                    String jsonIllustrationOutput = null;
                                    if (!(jsonObj.isNull(TRANSACTIONDATA))) {
                                        jsonTransactionData = jsonObj.getJSONObject(TRANSACTIONDATA);
                                        if (!(jsonTransactionData.isNull(ILLUSTRATION_OUTPUT))) {
                                            jsonIllustrationOutput =
                                                    jsonTransactionData.getJSONObject(ILLUSTRATION_OUTPUT).toString();
                                        }
                                    }
                                    /* EndOf Getting IllustartionOutput object from json */
                                    deleteResponse =
                                            lifeEngageIllustrationComponent.saveIllustration(jsonObj.toString(),
                                                    jsonIllustrationOutput, requestInfo, lifeEngageAudit);
                                    LOGGER.trace("Inside LifeEngageSyncServiceImpl.saveIllustration deleteResponse :deleteResponse "
                                            + deleteResponse);
                                }
                                jsonRsArray.put(new JSONObject(deleteResponse));
                                /* Added for security_optimizations */
                            } catch (InputValidationException e) {
                            	JSONObject jsonResponse= new JSONObject();
                            	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
                            	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
                            	jsonResponse.put("statusMessage", e.getMessage());                    	
                            	jsonRsArray.put(new JSONObject().put("StatusData", jsonResponse));							
                            }
                            catch (Exception e) {
                            	jsonRsArray.put(new JSONObject(e.getMessage()));
							}
                            	/* Added for security_optimizations */
                            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray,
                                    deleteJsonObjectRs, null);

                        }
                    }
                }

            }

        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.Delete : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        return deleteJsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#retrieve(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly = true)
    public String retrieve(final String json) throws BusinessException {
        LOGGER.trace("LifeEngageSyncServiceImpl.retrieve:  json" + json);
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();

        
        
        String eAppResponse = "";
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            eAppResponse = retrieve(requestInfo, jsonRqArray);
            LOGGER.trace("LifeEngageSyncServiceImpl.retrieve:  eAppResponse" + eAppResponse);
            jsonObjectRs = new JSONObject(eAppResponse);
        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.retrieve: Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#retrieveCustomerList(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String retrieveCustomerList(final String json) throws BusinessException {

        // ToDo - The actual implementation once the use case is updated.

        // /////// Temporary fix Begin- Remove this block once the actual code is implemented //////////////
        FileReader reader = null;
        try {
            reader =
                    new FileReader(Thread.currentThread()
                        .getContextClassLoader()
                        .getResource("eAppRequest/eAppServiceRs_RetrieveCustList.json")
                        .getFile());
        } catch (FileNotFoundException e) {
            throwBusinessException(true, e.getMessage());
        }
        final BufferedReader buffer = new BufferedReader(reader);
        final StringBuffer stringbuffer = new StringBuffer();
        String line = "";
        try {
            while ((line = buffer.readLine()) != null) {
                stringbuffer.append(line);
            }
        } catch (IOException e) {
            throwBusinessException(true, e.getMessage());
        }
        return stringbuffer.toString();
        // /////// Temporary fix End- Remove this block once the actual code is implemented //////////////
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#saveObservations(java.lang.String) *
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String saveObservations(final String json) throws BusinessException {

        JSONObject jsonObject;
        String eAppResponse = Constants.SUCCESS;

        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonObjectPayload = jsonObject.getJSONObject("ObservationPayload");
            final JSONArray jsonObsArray = jsonObjectPayload.getJSONArray(OBSERVATIONS);

            if (jsonObsArray.length() > 0) {
                for (int i = 0; i < jsonObsArray.length(); i++) {
                    final JSONObject jsonObj = jsonObsArray.getJSONObject(i);
                    lifeEngageEAppComponent.saveObservations(jsonObj.toString());
                }
            }

        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
            eAppResponse = Constants.FAILURE;

        }
        return eAppResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#getStatus(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String getStatus(final String json) throws BusinessException {
        LOGGER.trace("LifeEngageSyncServiceImpl.getStatus:  json" + json);
        JSONObject jsonObject;
        final JSONObject jsonObjectRs = new JSONObject();
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            final JSONArray jsonRsArray = new JSONArray();
            try {
                if (jsonRqArray.length() > 0) {
                    for (int i = 0; i < jsonRqArray.length(); i++) {
                        final JSONObject jsonObj = jsonRqArray.getJSONObject(i);
                        String responseArray = "";
                        String proposalNo = jsonObj.getString(KEY4);
                        String type = jsonObj.getString(TYPE);
                        responseArray = lifeEngageEAppComponent.getStatus(requestInfo, proposalNo, type);
                        jsonRsArray.put(new JSONObject(responseArray));
                    }
                }
            } catch (SystemException e) {
                jsonRsArray.put(new JSONObject(e.getMessage()));
            }
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.getStatus : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }

        return jsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#retrieveAll(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly = true)
    public String retrieveAll(final String json) throws BusinessException {
        LOGGER.trace("Inside LifeEngageSyncServiceImpl RetrieveAll : json" + json);
        JSONObject jsonObjectRs = new JSONObject();
        String eAppResponse = null;
        try {
            final JSONObject jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            eAppResponse = retrieveAll(requestInfo, jsonRqArray);
            LOGGER.trace("Inside LifeEngageSyncServiceImpl RetrieveAll : " + eAppResponse);
            jsonObjectRs = new JSONObject(eAppResponse);
        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
        }
        return jsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#runIllustration(java.lang.String)
     */
    @Override
    public String runIllustration(final String json) throws BusinessException {
        String output = "{}";
        try {
            final JSONObject jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonRqArray.length() > 0) {
                JSONObject jsonObjectTransactionData =
                        jsonRqArray.getJSONObject(0).getJSONObject(Constants.TRANSACTION_DATA);
                output = lifeEngageIllustrationComponent.runIllustration(jsonObjectTransactionData);
            }
        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
        }
        return output;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#runIllustration(java.lang.String)
     */
    @Override
    public String executeRule(final String json) throws BusinessException {
        String output = "{}";
        try {
            final JSONObject jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonRqArray.length() > 0) {
                JSONObject jsonObjectTransactionData =
                        jsonRqArray.getJSONObject(0).getJSONObject(Constants.TRANSACTION_DATA);
                output = lifeEngageIllustrationComponent.executeRule(jsonObjectTransactionData);
            }
        } catch (ParseException e) {
            LOGGER.error("LifeEngageSyncServiceImpl.executeRule : Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage(), e);
        }
        return output;
    }

    /**
     * Retrieve.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonRqArray
     *            the json rq array
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    protected String retrieve(final RequestInfo requestInfo, final JSONArray jsonRqArray) throws BusinessException,
            ParseException {

        JSONObject jsonObject;
        JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        if (jsonRqArray.length() > 0) {
            final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
            /* Added for security_optimizations */
            LifeEngageValidationHelper.validateUser(jsonObj);
            try {
				LifeEngageValidationHelper.validateInputString(jsonObj.toString(),validationPattern);
            /* Added for security_optimizations */
            if (jsonObj.has(TYPE) && jsonObj.getString(TYPE) != null && !"".equals(jsonObj.getString(TYPE))
                    && !jsonObj.getString(TYPE).equals("null")) {
                String transactionArray = "{\"Transactions\":[]}";
                if (E_APP.equals(jsonObj.getString(TYPE))) {
                    transactionArray = lifeEngageEAppComponent.retrieveEApp(requestInfo, jsonObj);
                    LOGGER.trace("LifeEngageSyncServiceImpl.retrieve: transactionArray" + transactionArray.length());
                } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
                    transactionArray = lifeEngageIllustrationComponent.retrieveIllustration(requestInfo, jsonObj);
                } else if (FNA.equals(jsonObj.getString(TYPE))) {
                    transactionArray = lifeEngageFNAComponent.retrieveFNA(requestInfo, jsonObj);
                } else if (LMS.equals(jsonObj.getString(TYPE))) {
                    transactionArray = lifeEngageLMSComponent.retrieveLMS(requestInfo, jsonObj);
                }
                jsonObject = new JSONObject(transactionArray);
                jsonRsArray = jsonObject.getJSONArray(TRANSACTIONS);
            }
            /* Added for security_optimizations */
           } catch (InputValidationException e) {	          	        	
	        	JSONObject jsonResponse= new JSONObject();
	        	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
	        	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
	        	jsonResponse.put("statusMessage", e.getMessage());                    	
	        	jsonRsArray.put(new JSONObject().put("StatusData", jsonResponse));	
	        	
			}
            /* Added for security_optimizations */
        }
        LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
        return jsonObjectRs.toString();
    }

    /**
     * Retrieve all.
     * 
     * @param requestInfo
     *            the request info
     * @param jsonRqArray
     *            the json rq array
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    protected String retrieveAll(final RequestInfo requestInfo, final JSONArray jsonRqArray) throws BusinessException,
            ParseException {
        LOGGER.trace("Inside LifeEngageSyncImpl RetrieveAll : requestInfo" + requestInfo);
        JSONObject jsonObject;
        JSONArray jsonRsArray = new JSONArray();
        final JSONObject jsonObjectRs = new JSONObject();
        Integer chunkSize = null;
        if (jsonRqArray.length() > 0) {
            final JSONObject jsonObj = jsonRqArray.getJSONObject(0);
            /* Added for security_optimizations */
            LifeEngageValidationHelper.validateUser(jsonObj);
            try {
				LifeEngageValidationHelper.validateInputString(jsonObj.toString(),validationPattern);		
	            if (jsonObj.has(TYPE) && jsonObj.getString(TYPE) != null && !"".equals(jsonObj.getString(TYPE))
	                    && !jsonObj.getString(TYPE).equals("null")) {
	                String transactionArray = TRANSACTION_ARRAY;
	                if (E_APP.equals(jsonObj.getString(TYPE))) {
	                    transactionArray = lifeEngageEAppComponent.retrieveAllEApp(requestInfo, jsonObj);
	                    chunkSize = eAppLimit;
	                } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
	                    transactionArray = lifeEngageIllustrationComponent.retrieveAllIllustration(requestInfo, jsonObj);
	                    chunkSize = illustrationLimit;
	                } else if (FNA.equals(jsonObj.getString(TYPE))) {
	                    transactionArray = lifeEngageFNAComponent.retrieveAllFNA(requestInfo, jsonObj);
	                    chunkSize = fnaLimit;
	                } else if (LMS.equals(jsonObj.getString(TYPE))) {
	                    transactionArray = lifeEngageLMSComponent.retrieveAllLMS(requestInfo, jsonObj);
	                    chunkSize = lmsLimit;
	                } 
	                LOGGER.trace("Inside LifeEngageSyncImpl RetrieveAll : transactionArray" + transactionArray);
	                transactionArray = transactionArray.replaceAll("<br>", "\\\\n");
	                jsonObject = new JSONObject(transactionArray);
	                jsonRsArray = jsonObject.getJSONArray(TRANSACTIONS);
	            }
            } catch (InputValidationException e) {	          	        	
	        	JSONObject jsonResponse= new JSONObject();
	        	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
	        	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
	        	jsonResponse.put("statusMessage", e.getMessage());                    	
	        	jsonRsArray.put(new JSONObject().put("StatusData", jsonResponse));	
	        	
			}
            /* Added for security_optimizations */
        }
        LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, chunkSize);
        LOGGER.trace("Inside LifeEngageSyncImpl RetrieveAll : jsonObjectRs" + jsonObjectRs.toString());
        return jsonObjectRs.toString();
    }

    /**
     * Gets the life engage e app component.
     * 
     * @return the lifeEngageEAppComponent
     */
    public final LifeEngageEAppComponent getLifeEngageEAppComponent() {
        return lifeEngageEAppComponent;
    }

    /**
     * Sets the life engage e app component.
     * 
     * @param lifeEngageEAppComponent
     *            the lifeEngageEAppComponent to set
     */
    public final void setLifeEngageEAppComponent(final LifeEngageEAppComponent lifeEngageEAppComponent) {
        this.lifeEngageEAppComponent = lifeEngageEAppComponent;
    }

    /**
     * Gets the life engage illustration component.
     * 
     * @return the lifeEngageIllustrationComponent
     */
    public final LifeEngageIllustrationComponent getLifeEngageIllustrationComponent() {
        return lifeEngageIllustrationComponent;
    }

    /**
     * Sets the life engage illustration component.
     * 
     * @param lifeEngageIllustrationComponent
     *            the lifeEngageIllustrationComponent to set
     */
    public final void setLifeEngageIllustrationComponent(
            final LifeEngageIllustrationComponent lifeEngageIllustrationComponent) {
        this.lifeEngageIllustrationComponent = lifeEngageIllustrationComponent;
    }

    /**
     * Gets the audit repository.
     * 
     * @return the auditRepository
     */
    public final AuditRepository getAuditRepository() {
        return auditRepository;
    }

    /**
     * Sets the audit repository.
     * 
     * @param auditRepository
     *            the auditRepository to set
     */
    public final void setAuditRepository(final AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    /**
     * Gets the life engage audit component.
     * 
     * @return the lifeEngageAuditComponent
     */
    public final LifeEngageAuditComponent getLifeEngageAuditComponent() {
        return lifeEngageAuditComponent;
    }

    /**
     * Sets the life engage audit component.
     * 
     * @param lifeEngageAuditComponent
     *            the lifeEngageAuditComponent to set
     */
    public final void setLifeEngageAuditComponent(final LifeEngageAuditComponent lifeEngageAuditComponent) {
        this.lifeEngageAuditComponent = lifeEngageAuditComponent;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#generateIllustration(java.lang.String)
     */
    @Override
    public String generateIllustration(String json) throws BusinessException {
        final JSONObject jsonObjectRs = new JSONObject();
        try {
        	/* Added for security_optimizations */
        	LifeEngageValidationHelper.validateInputString(json.toString(),validationPattern);
        	/* Added for security_optimizations */
            String transactionArray = "{\"Transactions\":[]}";
            JSONArray jsonRsArray = new JSONArray();
            JSONObject jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            if (jsonRqArray.length() > 0) {
                JSONObject jsonObjectTransactionData = jsonRqArray.getJSONObject(0);
                transactionArray = lifeEngageIllustrationComponent.generateIllustration(jsonObjectTransactionData);
            }
            jsonObject = new JSONObject(transactionArray);
            jsonRsArray = jsonObject.getJSONArray(TRANSACTIONS);
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);
        } catch (ParseException e) {
            throwBusinessException(true, e.getMessage());
            /* Added for security_optimizations */
        } catch (InputValidationException e) {
        	JSONObject jsonResponse= new JSONObject();
         	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
         	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
         	jsonResponse.put("statusMessage", e.getMessage());                    	
         	jsonObjectRs.put("StatusData", jsonResponse);
        }
        /* Added for security_optimizations */
        return jsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#retrieveByFilter(java.lang.String)
     */
    /**
     * Retrieve by filter.
     * 
     * @param json
     *            the json
     * @return the string
     * @throws BusinessException
     *             the business exception
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly = true)
    public String retrieveByFilter(String json) throws BusinessException {
        LOGGER.trace("Inside LifeEngageSyncServiceImpl.retrieveByFilter :json " + json);
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        JSONArray jsonRsArray = new JSONArray();
        String transactionArray = "{\"Transactions\":[]}";
        try {
        	/* Added for security_optimizations */
        	LifeEngageValidationHelper.validateInputString(json.toString(),validationPattern);
        	/* Added for security_optimizations */
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            transactionArray = lifeEngageSearchCriteriaComponent.retrieveByFilter(requestInfo, jsonRqArray);
            jsonObject = new JSONObject(transactionArray);
            jsonRsArray = jsonObject.getJSONArray(TRANSACTIONS);

            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.retrieveByFilterCount: Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
            /* Added for security_optimizations */
        } catch (InputValidationException e) {
        	JSONObject jsonResponse= new JSONObject();
         	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
         	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
         	jsonResponse.put("statusMessage", e.getMessage());                    	
         	jsonObjectRs.put("StatusData", jsonResponse);
        }
        	/* Added for security_optimizations */
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveByFilterCount : jsonObjectRs" + jsonObjectRs.toString());
        return jsonObjectRs.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#retrieveByFilterCount(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, readOnly = true)
    public String retrieveByFilterCount(String json) throws BusinessException {
        LOGGER.trace("Inside LifeEngageSyncServiceImpl.retrieveByFilterCount :json " + json);
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        String transactionArray = "{\"Transactions\":[]}";
        try {
        	/* Added for security_optimizations */
        	LifeEngageValidationHelper.validateInputString(json.toString(),validationPattern);
        	/* Added for security_optimizations */
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            
           /*  Added for security_optimizations 
            LifeEngageValidationHelper.validateUser(jsonRqArray.getJSONObject(0));
             Added for security_optimizations */

            transactionArray = lifeEngageSearchCriteriaComponent.retrieveByFilterCount(requestInfo, jsonRqArray);

            JSONArray jsonRsArray = new JSONArray();
            jsonRsArray.put(new JSONObject(transactionArray));
            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.retrieveByFilterCount: Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
            /* Added for security_optimizations */
        } catch (InputValidationException e) {
        	JSONObject jsonResponse= new JSONObject();
         	jsonResponse.put("statusCode", LifeEngageValidationHelper.FAILURE_CODE);
         	jsonResponse.put("status", LifeEngageValidationHelper.FAILURE);                    	
         	jsonResponse.put("statusMessage", e.getMessage());                    	
         	jsonObjectRs.put("StatusData", jsonResponse);			
		}
        /* Added for security_optimizations */
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveByFilterCount : jsonObjectRs" + jsonObjectRs.toString());
        return jsonObjectRs.toString();
    }
     
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.services.LifeEngageSyncService#retrieveByIds(java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String retrieveByIds(String json) throws BusinessException {
        LOGGER.trace("Inside LifeEngageSyncServiceImpl.retrieveByIds :json " + json);
        JSONObject jsonObject;
        JSONObject jsonObjectRs = new JSONObject();
        JSONArray jsonRsArray = new JSONArray();
        String transactionArray = "{\"Transactions\":[]}";
        try {
            jsonObject = new JSONObject(json);
            final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
            final JSONObject jsonRequestInfoObj = jsonRequestObj.getJSONObject(REQUEST_INFO);
            final RequestInfo requestInfo = LifeEngageSyncServiceHelper.parseRequestInfo(jsonRequestInfoObj);
            final JSONObject jsonRequestPayloadObj = jsonRequestObj.getJSONObject(REQUEST_PAYLOAD);
            final JSONArray jsonRqArray = jsonRequestPayloadObj.getJSONArray(TRANSACTIONS);
            transactionArray = lifeEngageSearchCriteriaComponent.retrieveByIds(requestInfo, jsonRqArray);
            jsonObject = new JSONObject(transactionArray);
            jsonRsArray = jsonObject.getJSONArray(TRANSACTIONS);

            LifeEngageSyncServiceHelper.setDetailsInJsonResponse(requestInfo, jsonRsArray, jsonObjectRs, null);

        } catch (ParseException e) {
            LOGGER.trace("LifeEngageSyncServiceImpl.retrieveByIds: Parse Exception" + e.toString());
            throwBusinessException(true, e.getMessage());
        }
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveByIds : jsonObjectRs" + jsonObjectRs.toString());
        return jsonObjectRs.toString();
    }

    
    protected LifeEngageEmail savelifeEngageEmail(final JSONObject jsonObj, String response) throws ParseException {
        LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
        final JSONObject emailObject = new JSONObject();
        final JSONObject emailTemplateObj = new JSONObject();

        String agentId = (String) jsonObj.getString(KEY11);

        final JSONObject emailObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(EMAIL);
        String toMailIds = (String) emailObj.getString(EMAIL_ID);
        String ccMailIds = (String) emailObj.getString(CC_EMAIL_ID);
        String fromMailId = (String) emailObj.getString(EMAIL_FROM_ADDRESS);
        final JSONObject productObj = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(PRODUCT);

        String templateId = "";
        String type = "";
        String id = null;
        String mailSubject = "";
        String pdfName = "";
        if (E_APP.equals(jsonObj.getString(TYPE))) {
            templateId = productObj.getJSONObject(TEMPLATES).getString(EAPP_PDF_TEMPLATE);
            type = E_APP;
            id = (String) new JSONObject(response).get(KEY4);
            mailSubject = getMailSubject(productObj);
        } else if (ILLUSTRATION.equals(jsonObj.getString(TYPE))) {
            templateId = productObj.getJSONObject(TEMPLATES).getString(ILLUSTRATION_PDF_TEMPLATE);
            type = ILLUSTRATION;
            id = (String) new JSONObject(response).get(KEY3);
            mailSubject = getMailSubject(productObj);
        } else if (FNA.equals(jsonObj.getString(TYPE))) {
            templateId = jsonObj.getJSONObject(TRANSACTIONDATA).getJSONObject(TEMPLATES).getString(FNA_PDF_TEMPLATE);
            type = FNA;
            id = (String) new JSONObject(response).get(KEY2);
            mailSubject = (String) emailObj.getString(MAIL_SUBJECT);
            pdfName = (String) emailObj.getString(Constants.PDF_NAME);
            emailObject.put(Constants.PDF_NAME, pdfName);
        }

 
        emailObject.put(MAIL_SUBJECT, mailSubject);
        emailObject.put(TEMPLATE_ID, templateId);
        emailObject.put(TYPE, type);
        emailObject.put(EMAIL_ID, toMailIds);
        emailObject.put(CC_EMAIL_ID, ccMailIds);
        emailObject.put(EMAIL_FROM_ADDRESS, fromMailId);
        emailObject.put(AGENT_ID, agentId);
       
        if (!id.isEmpty()) {
            emailTemplateObj.put(ID, id);
            emailObject.put(ID, id);
            emailObject.put(EMAIL_TEMPLATE, emailTemplateObj);
            lifeEngageEmail.setSendTime(LifeEngageComponentHelper.getCurrentdate());
            lifeEngageEmail.setEmailValues(emailObject.toString());
            lifeEngageEmail.setAttemptNumber("1");
            lifeEngageEmail.setStatus(INITIALISED);
            lifeEngageEmail.setAgentId(agentId);
            lifeEngageEmail.setType(type);
            lifeEngageEmail = emailRepository.savelifeEngageEmail(lifeEngageEmail);
        }

        return lifeEngageEmail;
    }
    /**
     * Gets the mail subject.
     * 
     * @param productObj
     *            the product obj
     * @return the mail subject
     */
    public String getMailSubject(final JSONObject productObj) {
        final String premium = productObj.getJSONObject(POLICY_DETAILS).getString(COMMITTED_PREMIUM);
        final String productName = productObj.getJSONObject(PRODUCT_DETAILS).getString(PRODUCT_NAME);
        final String mailSubject = productName + "_" + premium;
        return mailSubject;
    }
}
