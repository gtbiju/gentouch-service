/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * It indicates gender.
 * 
 * @author 301350
 * 
 * 
 */
public enum GenderCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Female,
    /**
     * 
     * 
     * 
     * 
     */
    Male,
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    NotMentioned
}
