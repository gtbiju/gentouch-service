package com.cognizant.insurance.domain.agreement;

public class AutoETRPaymentDetails {
	private int id;	
	 String paymentDate;
    String product_id;
    String premium;
    String payor_role;
    String application_date;
    String cash_payment;
    String source_of_data;
    String payor_email;
    String policy_holder;
    String dob_policy_holder;
    String payor_name;
    String application_no;
    String pay_for;
    String sum_assured;
    String agent_code;
    String bankName;
    Boolean hasBeenPaid;
		String bankBranchName;
		String bankAccountNumber;
		String branchCode;
		String bankBranchCode;
		String splitPayment;
		
		String mobile;
		String etrNumber;
		String etrDate;
		Boolean isValuePresent;
		
		public Boolean getHasBeenPaid() {
			return hasBeenPaid;
		}
		public void setHasBeenPaid(Boolean hasBeenPaid) {
			this.hasBeenPaid = hasBeenPaid;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public String getEtrNumber() {
			return etrNumber;
		}
		public void setEtrNumber(String etrNumber) {
			this.etrNumber = etrNumber;
		}
		public String getEtrDate() {
			return etrDate;
		}
		public void setEtrDate(String etrDate) {
			this.etrDate = etrDate;
		}
		public String getPaymentDate() {
			return paymentDate;
		}
		public void setPaymentDate(String paymentDate) {
			this.paymentDate = paymentDate;
		}
		public String getProduct_id() {
			return product_id;
		}
		public void setProduct_id(String product_id) {
			this.product_id = product_id;
		}
		public String getPremium() {
			return premium;
		}
		public void setPremium(String premium) {
			this.premium = premium;
		}
		public String getPayor_role() {
			return payor_role;
		}
		public void setPayor_role(String payor_role) {
			this.payor_role = payor_role;
		}
		public String getApplication_date() {
			return application_date;
		}
		public void setApplication_date(String application_date) {
			this.application_date = application_date;
		}
		public String getCash_payment() {
			return cash_payment;
		}
		public void setCash_payment(String cash_payment) {
			this.cash_payment = cash_payment;
		}
		public String getSource_of_data() {
			return source_of_data;
		}
		public void setSource_of_data(String source_of_data) {
			this.source_of_data = source_of_data;
		}
		public String getPayor_email() {
			return payor_email;
		}
		public void setPayor_email(String payor_email) {
			this.payor_email = payor_email;
		}
		public String getPolicy_holder() {
			return policy_holder;
		}
		public void setPolicy_holder(String policy_holder) {
			this.policy_holder = policy_holder;
		}
		public String getDob_policy_holder() {
			return dob_policy_holder;
		}
		public void setDob_policy_holder(String dob_policy_holder) {
			this.dob_policy_holder = dob_policy_holder;
		}
		public String getPayor_name() {
			return payor_name;
		}
		public void setPayor_name(String payor_name) {
			this.payor_name = payor_name;
		}
		public String getApplication_no() {
			return application_no;
		}
		public void setApplication_no(String application_no) {
			this.application_no = application_no;
		}
		public String getPay_for() {
			return pay_for;
		}
		public void setPay_for(String pay_for) {
			this.pay_for = pay_for;
		}
		public String getSum_assured() {
			return sum_assured;
		}
		public void setSum_assured(String sum_assured) {
			this.sum_assured = sum_assured;
		}
		public String getAgent_code() {
			return agent_code;
		}
		public void setAgent_code(String agent_code) {
			this.agent_code = agent_code;
		}
		public String getBankName() {
			return bankName;
		}
		public void setBankName(String bankName) {
			this.bankName = bankName;
		}
		public String getBankBranchName() {
			return bankBranchName;
		}
		public void setBankBranchName(String bankBranchName) {
			this.bankBranchName = bankBranchName;
		}
		public String getBankAccountNumber() {
			return bankAccountNumber;
		}
		public void setBankAccountNumber(String bankAccountNumber) {
			this.bankAccountNumber = bankAccountNumber;
		}
		public String getBranchCode() {
			return branchCode;
		}
		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}
		public String getBankBranchCode() {
			return bankBranchCode;
		}
		public void setBankBranchCode(String bankBranchCode) {
			this.bankBranchCode = bankBranchCode;
		}
		public String getSplitPayment() {
			return splitPayment;
		}
		public void setSplitPayment(String splitPayment) {
			this.splitPayment = splitPayment;
		}
		public Boolean getIsValuePresent() {
			return isValuePresent;
		}
		public void setIsValuePresent(Boolean isValuePresent) {
			this.isValuePresent = isValuePresent;
		}
			
}
