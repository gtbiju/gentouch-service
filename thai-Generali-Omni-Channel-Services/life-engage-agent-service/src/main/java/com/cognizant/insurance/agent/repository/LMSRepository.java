/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.agent.repository;

import static com.cognizant.insurance.agent.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.agent.agreement.component.AgreementComponent;
import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.constants.ErrorConstants;
import com.cognizant.insurance.agent.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
//import com.cognizant.insurance.agent.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
//import com.cognizant.insurance.agent.goalandneed.component.GoalAndNeedComponent;
import com.cognizant.insurance.agent.party.component.PartyComponent;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.request.impl.JPARequestImpl;
import com.cognizant.insurance.agent.request.vo.RequestInfo;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.searchcriteria.SearchCountResult;
/*import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;*/
import com.cognizant.insurance.agent.searchcriteria.SearchCriteria;

/**
 * The Class EAppRepository.
 * 
 * @author 300797
 */
@Repository
public class LMSRepository {

    /** The entity manager. */
    @PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;

    /** The party component. */
    @Autowired
    private PartyComponent partyComponent;

    /** The goal and need component. */
 /*   @Autowired
    private GoalAndNeedComponent goalAndNeedComponent;*/

    /** The agreement component. */
    @Autowired
    private AgreementComponent agreementComponent;

    /** The lms limit. */
    @Value("${le.platform.service.lms.fetchLimit}")
    private Integer lmsLimit;

    /** The conflict resolution. */
    @Value("${le.platform.service.conflictResolution}")
    private String conflictResolution;

    /** The source of truth. */
    @Value("${le.platform.service.sourceOfTruth}")
    private String sourceOfTruth;

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LMSRepository.class);

    /**
     * Save EAppR.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param customer 
     * @return the string
     * @throws ParseException
     *             the parse exception
     */
    //@Transactional("le_txn_manager")
    public String saveLMS(final Transactions transactions, final RequestInfo requestInfo, Customer responseCustomer) throws ParseException {
        LOGGER.trace("Inside LMSRepository saveLMS : requestInfo" + requestInfo);

        
        long start_time = System.currentTimeMillis();      
       
        
        
        Boolean isDuplicateLeadExists = isExistingCustomer(transactions, requestInfo);
        
        long end_time = System.currentTimeMillis();
        long difference = end_time-start_time;
        LOGGER.info("*****Lead Duplicate Check Time in Millis ******"+difference);

        if (isDuplicateLeadExists) {
            throwSystemException(true, ErrorConstants.LE_SYNC_ERR_107, Constants.DUPLICATE_LEAD);
        }
        onBeforeSave(transactions, requestInfo);

        if (Constants.UPDATE.equals(transactions.getMode())) {
            String status = onLMSUpdate(transactions, requestInfo,responseCustomer);
            if (!Constants.SUCCESS.equals(status)) {
                return status;
            }
        } else {
            transactions.getCustomer().setCreationDateTime(transactions.getCreationDateTime());
        }

        final Request<PartyRoleInRelationship> request =
                new JPARequestImpl<PartyRoleInRelationship>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        request.setType(transactions.getCustomer());

        LOGGER.trace("Inside LMSRepository saveLMS : request" + request.toString());
        
        start_time = System.currentTimeMillis();  
        partyComponent.savePartyRoleInRelationship(request);
        end_time = System.currentTimeMillis();
        difference = end_time-start_time;
        LOGGER.info("*****Lead Save Time in Millis ******"+difference);

        // soft delete of eApp, illustration fna related to LMS for which the status is cancelled
        /*if (Constants.CANCELLED.equals(transactions.getStatus())) {
            deleteLead(transactions, requestInfo);
        }*/

        return transactions.getCustomer().getStatusCode().toString();
    }

    /**
     * Delete lead.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     *//*
    private void deleteLead(final Transactions transactions, final RequestInfo requestInfo) {
        final List<AgreementStatusCodeList> statusList = getAgreementStatusCodesFor(Constants.DELETE);
        final Request<List<AgreementStatusCodeList>> statusRequest =
                new JPARequestImpl<List<AgreementStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        // eApp soft delete
        final Request<String> eAppLeadTransTrackingIdRq =
                new JPARequestImpl<String>(entityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
        eAppLeadTransTrackingIdRq.setType(transactions.getTransTrackingId());
        eAppLeadTransTrackingIdRq.setContextId(ContextTypeCodeList.EAPP);

        agreementComponent.updateAgreementStatusForLead(eAppLeadTransTrackingIdRq, statusRequest);

        // illustration soft delete
        final Request<String> illustrationLeadTransTrackingIdEappRq =
                new JPARequestImpl<String>(entityManager, ContextTypeCodeList.ILLUSTRATION,
                        requestInfo.getTransactionId());
        illustrationLeadTransTrackingIdEappRq.setType(transactions.getTransTrackingId());
        illustrationLeadTransTrackingIdEappRq.setContextId(ContextTypeCodeList.ILLUSTRATION);

        agreementComponent.updateAgreementStatusForLead(illustrationLeadTransTrackingIdEappRq, statusRequest);

        // fna soft delete
        final Request<GoalAndNeed> goalAndNeedRequest =
                new JPARequestImpl<GoalAndNeed>(entityManager, ContextTypeCodeList.FNA, requestInfo.getTransactionId());
        final GoalAndNeed goalAndNeed = new GoalAndNeed();
        goalAndNeed.setLeadId(transactions.getTransTrackingId());
        goalAndNeed.setStatus(Constants.CANCELLED);
        goalAndNeedRequest.setType(goalAndNeed);

        goalAndNeedComponent.updateGoalAndNeedStatusForLead(goalAndNeedRequest);
    }

    *//**
     * Checks if is existing customer.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return true, if is existing customer
     */
    protected boolean isExistingCustomer(Transactions transactions, RequestInfo requestInfo) {
        boolean isDuplicateLeadExists = false;
        final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.DUPLICATE_CHECK);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final List<TelephoneTypeCodeList> telephoneTypeList = new ArrayList<TelephoneTypeCodeList>();
        telephoneTypeList.add(TelephoneTypeCodeList.Mobile);
        telephoneTypeList.add(TelephoneTypeCodeList.Home);

        final Request<List<TelephoneTypeCodeList>> telephoneTypeRequest =
                new JPARequestImpl<List<TelephoneTypeCodeList>>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        telephoneTypeRequest.setType(telephoneTypeList);

        final Request<PartyRoleInRelationship> customerRequest =
                new JPARequestImpl<PartyRoleInRelationship>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        customerRequest.setType(transactions.getCustomer());

        final Response<List<Customer>> duplicateCustomerResponse =
                partyComponent.retrieveDuplicateLeadList(customerRequest, transactions, statusRequest,
                        telephoneTypeRequest);

        final List<Customer> customers = duplicateCustomerResponse.getType();

        if (CollectionUtils.isEmpty(customers)) {
            isDuplicateLeadExists = false;

        } else {
            for (Customer customerObj : customers) {
                if (!(transactions.getKey1().equals(customerObj.getIdentifier()))) {
                    isDuplicateLeadExists = true;
                }
            }
        }
        return isDuplicateLeadExists;
    }

    /**
     * On fna update.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @return the string
     */
    @Transactional("le_txn_manager")
    protected String onLMSUpdate(Transactions transactions, RequestInfo requestInfo, Customer responseCustomer) {

        final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.UPDATE_OP);
        Customer customer = new Customer();
        if( responseCustomer != null ){
        	customer = responseCustomer;
        }else{
        	customer = createReplacingCustomer(transactions, requestInfo, ContextTypeCodeList.LMS, statusList);
        }
        // Checking whether the server data is latest when source of
        // truth is server, if so reject the client data
        // and send rejected status
        /*if (Constants.TRUE.equals(conflictResolution) && Constants.SERVER.equals(sourceOfTruth)) {
            Date modifiedTimeStamp = customer.getModifiedDateTime();
            if (!(transactions.getLastSyncDate().toString().equals(Constants.DEFAULT_DATE))
                    && ((null != modifiedTimeStamp) && (modifiedTimeStamp.after(transactions.getLastSyncDate())))) {

                LOGGER.trace("Inside if case and rejected the case..");
                return Constants.REJECTED;

            }
        }*/
        updateCustomerStatus(customer, requestInfo, ContextTypeCodeList.EAPP);
        transactions.getCustomer().setCreationDateTime(customer.getCreationDateTime());
        return Constants.SUCCESS;
    }

    /**
     * On before save.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     */
    protected void onBeforeSave(Transactions transactions, RequestInfo requestInfo) {
        transactions.getCustomer().setTransactionId(requestInfo.getTransactionId());
        transactions.getCustomer().setIdentifier(transactions.getKey1());
        transactions.getCustomer().setModifiedDateTime(transactions.getModifiedTimeStamp());
    }

    /**
     * Retrieve lms.
     * 
     * @param transactions
     *            the lead identifier
     * @param transactionId
     *            the transaction id
     * @param requestInfo
     *            the request info
     * @return the transactions
     */
    //@Transactional("le_txn_manager")
    public Transactions retrieveLMS(final Transactions leadIdentifier, final String transactionId,
            final RequestInfo requestInfo) {
        LOGGER.trace("LMSRepository.retrieveLMS: leadIdentifier" + leadIdentifier);
        LOGGER.trace("LMSRepository.retrieveLMS: transactionId" + transactionId);
        final Transactions transaction = new Transactions();
        final Request<Customer> leadRequest =
                new JPARequestImpl<Customer>(entityManager, ContextTypeCodeList.LMS, transactionId);
        final Customer customer = new Customer();
        customer.setIdentifier(leadIdentifier.getKey1());
        customer.setTransTrackingId(leadIdentifier.getTransTrackingId());
        customer.setAgentId(leadIdentifier.getAgentId());
        leadRequest.setType(customer);

        final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.RETRIEVE);
        

        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Response<Customer> customerResponse = partyComponent.retrieveCustomer(leadRequest, statusRequest);
        if (customerResponse.getType() != null) {
            transaction.setCustomer(customerResponse.getType());
            transaction.setModifiedTimeStamp(customerResponse.getType().getModifiedDateTime());
        }

        return transaction;

    }

    /**
     * Retrieve all.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param isFullDetailsRequired
     *            the is full details required
     * @return the list
     */
    public List<Transactions> retrieveAll(final Transactions transactions, final RequestInfo requestInfo,
            final boolean isFullDetailsRequired) {
        LOGGER.trace("Inside LMSRepository retrieveAll : retrieveAll" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;

        final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.RETRIEVE_ALL);

        final Request<Customer> customerRequest =
                new JPARequestImpl<Customer>(entityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
        final Customer customer = new Customer();
        customer.setAgentId(transactions.getAgentId());
        customer.setLastSyncDate(requestInfo.getLastSyncDate());
        customerRequest.setType(customer);

        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        Request<Integer> limitRequest = null;

        if ((isFullDetailsRequired) && (lmsLimit != null)) {

            limitRequest =
                    new JPARequestImpl<Integer>(entityManager, ContextTypeCodeList.EAPP, requestInfo.getTransactionId());
            limitRequest.setType(lmsLimit);

        }

        final Response<List<Customer>> customersResponse =
                partyComponent.retrieveCustomers(customerRequest, statusRequest, limitRequest);
        final List<Customer> customers = customersResponse.getType();
        if (CollectionUtils.isNotEmpty(customers)) {
            for (Customer customerObj : customers) {
                transaction = new Transactions();
                transaction.setCustomer(customerObj);
                if (customerObj.getModifiedDateTime() != null
                        && !(customerObj.getModifiedDateTime().toString().equals(""))) {
                    LOGGER.trace("Inside LMSRepository customerObj.getModifiedDateTime() != null"
                            + customerObj.getModifiedDateTime());
                    transaction.setLastSyncDate(customerObj.getModifiedDateTime());
                    transaction.setModifiedTimeStamp(customerObj.getModifiedDateTime());
                } else if (customerObj.getCreationDateTime() != null
                        && !("".equals(customerObj.getCreationDateTime().toString()))) {
                    LOGGER.trace("Inside LMSRepository getCreationDateTime() != null"
                            + customerObj.getCreationDateTime());
                    transaction.setLastSyncDate(customerObj.getCreationDateTime());
                }
                transactionsList.add(transaction);
            }
        }

        return transactionsList;
    }

   /**
     * retrieving existing customer.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param contextType
     *            the context type
     * @param statusList
     *            the status list
     * @return the customer
     */
    protected Customer createReplacingCustomer(final Transactions transactions, final RequestInfo requestInfo,
            final ContextTypeCodeList contextType, final List<CustomerStatusCodeList> statusList) {

        final Request<Customer> request =
                new JPARequestImpl<Customer>(entityManager, contextType, requestInfo.getTransactionId());
        final Customer customer = new Customer();
        String identifier = transactions.getKey1();

        customer.setIdentifier(transactions.getKey1());
        request.setType(customer);

        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, contextType,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);
        final Response<Customer> response = partyComponent.retrieveCustomerForIdentifier(request, statusRequest);
        if (response == null || response.getType() == null) {
            throwSystemException(true, "No details with identifier : " + identifier);
        }

        return (Customer) response.getType();

    }

    /**
     * update the customer status.
     * 
     * @param customer
     *            the customer
     * @param requestInfo
     *            the request info
     * @param contextType
     *            the context type
     */
    protected void updateCustomerStatus(final Customer customer, final RequestInfo requestInfo,
            final ContextTypeCodeList contextType) {
        final Request<Customer> request =
                new JPARequestImpl<Customer>(entityManager, contextType, requestInfo.getTransactionId());

        customer.setStatusCode(CustomerStatusCodeList.Suspended);
        request.setType(customer);
        partyComponent.updateCustomer(request);
    }

    /**
     * Retrieve by count lms.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the search count result
     */
    public SearchCountResult retrieveByCountLMS(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        searchCriteriatRequest.setType(searchCriteria);

        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.LMS,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList = getCustomerStatusCodesFor(Constants.RETRIEVE_BY_COUNT);
        statusRequest.setType(statusList);

        final Response<SearchCountResult> searchCountResponse =
                partyComponent.getCustomerCount(searchCriteriatRequest, statusRequest);

        SearchCountResult searchCountResult = null;
        if (searchCountResponse.getType() != null) {
            searchCountResult = (SearchCountResult) searchCountResponse.getType();

        }
        return searchCountResult;

    }

    /**
     * Retrieve by filter lms.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     *//*
    public List<Transactions> retrieveByFilterLMS(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList =getCustomerStatusCodesFor(Constants.RETRIEVE_BY_FILTER);

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Response<List<Customer>> searchCriteriatResponse =
                partyComponent.retrieveFilterCustomers(searchCriteriatRequest, statusRequest);
        final List<Customer> customers = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(customers)) {
            for (Customer customerObj : customers) {
                transaction = new Transactions();
                transaction.setCustomer(customerObj);
                transaction.setCustomerId(customerObj.getIdentifier());
                transaction.setType(searchCriteria.getType());
                if (customerObj.getModifiedDateTime() != null
                        && !(customerObj.getModifiedDateTime().toString().equals(""))) {
                    LOGGER.trace("Inside LMSRepository customerObj.getModifiedDateTime() != null"
                            + customerObj.getModifiedDateTime());
                    transaction.setLastSyncDate(customerObj.getModifiedDateTime());
                    transaction.setModifiedTimeStamp(customerObj.getModifiedDateTime());
                } else if (customerObj.getCreationDateTime() != null
                        && !("".equals(customerObj.getCreationDateTime().toString()))) {
                    LOGGER.trace("Inside LMSRepository getCreationDateTime() != null"
                            + customerObj.getCreationDateTime());
                    transaction.setLastSyncDate(customerObj.getCreationDateTime());
                }
                transactionsList.add(transaction);
            }
        }

        return transactionsList;
    }*/

    /**
     * Gets the entity manager.
     * 
     * @return the entityManager
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Sets the entity manager.
     * 
     * @param entityManager
     *            the entityManager to set
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Gets the party component.
     * 
     * @return the partyComponent
     */
    public final PartyComponent getPartyComponent() {
        return partyComponent;
    }

    /**
     * Sets the party component.
     * 
     * @param partyComponent
     *            the partyComponent to set
     */
    public final void setPartyComponent(final PartyComponent partyComponent) {
        this.partyComponent = partyComponent;
    }
    
   protected List<CustomerStatusCodeList> getCustomerStatusCodesFor(String operation){
    	List<CustomerStatusCodeList> statusCodeLists=new ArrayList<CustomerStatusCodeList>();
    	if(Constants.RETRIEVE.equals(operation)||Constants.RETRIEVE_ALL.equals(operation)||
    			Constants.RETRIEVE_BY_FILTER.equals(operation)||Constants.RETRIEVE_BY_COUNT.equals(operation)||
    			Constants.UPDATE_OP.equals(operation)||Constants.DELETE.equals(operation)||Constants.DUPLICATE_CHECK.equals(operation)){
    		statusCodeLists.add(CustomerStatusCodeList.Open);
    		statusCodeLists.add(CustomerStatusCodeList.Dropped);
    		statusCodeLists.add(CustomerStatusCodeList.Login);
    		statusCodeLists.add(CustomerStatusCodeList.New);
    	}
    	return statusCodeLists;
    }
    
    protected List<AgreementStatusCodeList> getAgreementStatusCodesFor(String operation){
    	List<AgreementStatusCodeList> statusCodeLists=new ArrayList<AgreementStatusCodeList>();
    	if(Constants.DELETE.equals(operation)){
    		statusCodeLists.add(AgreementStatusCodeList.Initial);
    		statusCodeLists.add(AgreementStatusCodeList.Final);
    		statusCodeLists.add(AgreementStatusCodeList.Proposed);
    	}
    	return statusCodeLists;
    }
    
    /**
     * Retrieve by filter lms.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     */
    public List<Transactions> retrieveByFilterLMS(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        LOGGER.trace("Inside LMSRepository retrieveByFilterLMS : retrieveAll" + requestInfo);
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        Transactions transaction = null;
        final Request<SearchCriteria> searchCriteriatRequest =
                new JPARequestImpl<SearchCriteria>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        final List<CustomerStatusCodeList> statusList =getCustomerStatusCodesFor(Constants.RETRIEVE_BY_FILTER);

        searchCriteriatRequest.setType(searchCriteria);
        final Request<List<CustomerStatusCodeList>> statusRequest =
                new JPARequestImpl<List<CustomerStatusCodeList>>(entityManager, ContextTypeCodeList.EAPP,
                        requestInfo.getTransactionId());
        statusRequest.setType(statusList);

        final Response<List<Customer>> searchCriteriatResponse =
                partyComponent.retrieveFilterCustomers(searchCriteriatRequest, statusRequest);
        final List<Customer> customers = searchCriteriatResponse.getType();
        if (CollectionUtils.isNotEmpty(customers)) {
            for (Customer customerObj : customers) {
                transaction = new Transactions();
                transaction.setCustomer(customerObj);
                transaction.setCustomerId(customerObj.getIdentifier());
                transaction.setType(searchCriteria.getType());
                if (customerObj.getModifiedDateTime() != null
                        && !(customerObj.getModifiedDateTime().toString().equals(""))) {
                    LOGGER.trace("Inside LMSRepository customerObj.getModifiedDateTime() != null"
                            + customerObj.getModifiedDateTime());
                    transaction.setLastSyncDate(customerObj.getModifiedDateTime());
                    transaction.setModifiedTimeStamp(customerObj.getModifiedDateTime());
                } else if (customerObj.getCreationDateTime() != null
                        && !("".equals(customerObj.getCreationDateTime().toString()))) {
                    LOGGER.trace("Inside LMSRepository getCreationDateTime() != null"
                            + customerObj.getCreationDateTime());
                    transaction.setLastSyncDate(customerObj.getCreationDateTime());
                }
                transactionsList.add(transaction);
            }
        }

        return transactionsList;
    }
    
    /*public List<Transactions> retrieveIdsByEmailLMS(RequestInfo requestInfo, SearchCriteria searchCriteria) {
        return null;
    }*/

}
