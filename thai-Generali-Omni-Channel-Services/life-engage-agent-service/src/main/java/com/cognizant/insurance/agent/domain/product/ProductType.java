/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class ProductType.
 *
 * @author 304007
 */

@Entity
@DiscriminatorValue("Product")
public class ProductType extends Type {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4076368075513526994L;

    /**
     * The Constructor.
     */
    public ProductType() {
        super();
    }

    /**
     * The Constructor.
     *
     * @param id the id
     */
    public ProductType(final String id) {
        super(id);
    }

    /** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "productType")
    private Set<ProductSpecification> productSpecifications;

    /**
     * @param productSpecifications
     *            the productSpecifications to set
     */
    public void setProductSpecifications(Set<ProductSpecification> productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    /**
     * @return the productSpecifications
     */
    public Set<ProductSpecification> getProductSpecifications() {
        return productSpecifications;
    }
}
