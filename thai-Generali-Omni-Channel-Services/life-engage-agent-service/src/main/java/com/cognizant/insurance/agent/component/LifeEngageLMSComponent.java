package com.cognizant.insurance.agent.component;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cognizant.insurance.agent.audit.LifeEngageAudit;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.request.vo.RequestInfo;


public interface LifeEngageLMSComponent {
	
	String retrieveLMS(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
    ParseException;
	
	String retrieveAllLMS(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
    ParseException;
	
	String saveLMS(String json, RequestInfo requestInfo, LifeEngageAudit lifeEngageAudit);
	
	String retrieveByFilter(RequestInfo requestInfo, JSONArray jsonRqArray) throws BusinessException, ParseException;

}
