/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Annuity Premium Type.
 * 
 * @author 301350
 * 
 * 
 */
public enum AnnuityPremiumTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * Only one payment is allowed.
     * 
     * 
     * 
     */
    Single,
    /**
     * Payments of differing amount are allowed. If an annuity is a single
     * premium that allows subsequent payments, it should e identified as
     * flexible.
     * 
     * 
     * 
     */
    Flexible,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * Regular scheduled payments are required.
     * 
     * 
     * 
     */
    Fixed
}
