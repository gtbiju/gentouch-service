/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.agent.dao;

import java.util.List;

import com.cognizant.insurance.agent.audit.LifeEngagePayloadAudit;
import com.cognizant.insurance.agent.dao.Dao;
import com.cognizant.insurance.agent.request.Request;

/**
 * The Interface interface AuditDao.
 *
 * @author 300797
 */
public interface AuditDao extends Dao {

    /**
     * Retrieve life engage payload audit.
     *
     * @param request the request
     * @return the response
     */
    List<LifeEngagePayloadAudit> retrieveLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request);
    
    /**
     * Retrieve LMS payload audit.
     *
     * @param request the request
     * @return the response
     */
    List<LifeEngagePayloadAudit> retrieveLMSPayloadAudit(Request<LifeEngagePayloadAudit> request);


    /**
     * Retrieve last life engage payload audit.
     * 
     * @param request
     *            the request
     * @return the life engage payload audit
     */
    LifeEngagePayloadAudit retrieveLastLifeEngagePayloadAudit(Request<LifeEngagePayloadAudit> request);
}
