package com.cognizant.insurance.agent.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.agent.dao.ODSUserDao;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.request.impl.JPARequestImpl;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.response.impl.ResponseImpl;
import com.cognizant.insurance.generali.security.entity.PasswordHistory;

public class ODSUserDaoImpl extends DaoImpl implements ODSUserDao {

	@Override
	public Response<GeneraliAgent> validateAgentProfile(Request<Transactions> request) {
		Response<GeneraliAgent> response=new ResponseImpl<GeneraliAgent>();
		JPARequestImpl<Transactions> requestImpl=(JPARequestImpl<Transactions>) request;
		String queryString ="select agent from GeneraliAgent agent where agent.agentCode =:agentCode";
		
		Query query=requestImpl.getEntityManager().createQuery(queryString);
		query.setParameter("agentCode", request.getType().getKey1());
		List<Object> resultList=query.getResultList();
		if(resultList!=null && resultList.size() >0){
			response.setType((GeneraliAgent) resultList.get(0));
		}
		return response;
	}	
	
	@Override
	public Response<GeneraliTHUser> validateLoggedInUser(Request<Transactions> request) {
		Response<GeneraliTHUser> response=new ResponseImpl<GeneraliTHUser>();
		JPARequestImpl<Transactions> requestImpl=(JPARequestImpl<Transactions>) request;
		String queryString ="select user from GeneraliTHUser user where user.userId =:userId";
		
		Query query=requestImpl.getEntityManager().createQuery(queryString);
		query.setParameter("userId", request.getType().getKey1());
		List<Object> resultList=query.getResultList();
		if(resultList!=null && resultList.size() >0){
			response.setType((GeneraliTHUser) resultList.get(0));
		}
		return response;
	}
	
	@Override
	public Response<List<PasswordHistory>> getRecentPasswords(Request<String> userRequest, Request<Integer> historyRequest) {
		Response<List<PasswordHistory>> response = new ResponseImpl<List<PasswordHistory>>();
		List<PasswordHistory> histories=new ArrayList<PasswordHistory>();
		response.setType(histories);
        JPARequestImpl<String> requestImpl = (JPARequestImpl<String>) userRequest;
        Query query = requestImpl.getEntityManager().createQuery(
                            "select password from PasswordHistory password  where password.userId=:userId order by password.createdOn DESC");
        query.setParameter("userId", userRequest.getType());
        query.setMaxResults(historyRequest.getType());
        List<PasswordHistory> list = query.getResultList();
        if (CollectionUtils.isNotEmpty(list)) {
            histories.addAll(list);
        } 
        return response;
	}
	
	@Override
	public Response<PasswordHistory> getOldPasswordHistory(Request<String> userRequest) {
		Response<PasswordHistory> response = new ResponseImpl<PasswordHistory>();		
        JPARequestImpl<String> requestImpl = (JPARequestImpl<String>) userRequest;
        Query query = requestImpl.getEntityManager().createQuery(
                            "select password from PasswordHistory password  where password.userId=:userId order by password.createdOn ASC");
        query.setParameter("userId", userRequest.getType());
        List<PasswordHistory> resultList = query.getResultList();
        if (resultList!=null && resultList.size() > 5) {
            response.setType(resultList.get(0));
        } 
        return response;
	}
}
