/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Mar 15, 2013
 */
package com.cognizant.insurance.service.repository;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.cognizant.insurance.domain.codes.CodeLookUp;

/**
 * The Interface interface TypeLookUpRepositoryCustom.
 */
public interface CodeLookUpRepositoryCusotm {

    /**
     * Fetch look up data for code.
     * 
     * @param typeName
     *            the type name
     * @param code
     *            the code
     * @param language
     *            the language
     * @param country
     *            the country
     * @return the list
     */
    List<CodeLookUp> fetchLookUpDataForCode(@Param("typeName") String typeName, @Param("code") String code,
            @Param("language") String language, @Param("country") String country);

    /**
     * Fetch look up data.
     * 
     * @param typeName
     *            the type name
     * @param language
     *            the language
     * @param country
     *            the country
     * @return the list
     */
    List<CodeLookUp> fetchLookUpData(@Param("typeName") String typeName, @Param("language") String language,
            @Param("country") String country);
}
