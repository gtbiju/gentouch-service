/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Sep 20, 2017
 */

package com.cognizant.insurance.domain.product;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Version_Status")
public class VersionStatusType extends Type {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1138620690274365205L;

	public VersionStatusType() {
		super();
	}

}
