package com.cognizant.insurance.omnichannel.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.insurance.audit.IllustrationResponsePayLoad;
import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.impl.LifeEngageIllustrationComponentImpl;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.IllustrationRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliAgentProfileRepository;

public class GeneraliIllustrationComponentImpl extends LifeEngageIllustrationComponentImpl{
	
	private static final String KEY22 = "Key22";

	private static final String KEY39 = "Key39";

	private static final String RM_REASSIGN = "rm_reassign";

	private static final String ALLOCATION_TYPE = "AllocationType";

	private static final String TRANSACTION_DATA = "TransactionData";

	/** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;

    /** The save illustration holder. */
    @Autowired
    @Qualifier("illustrationSaveMapping")
    private LifeEngageSmooksHolder saveIllustrationHolder;
    
    /** The Constant SUCCESS_MESSAGE_SAVE. */
    private static final String SUCCESS_MESSAGE_SAVE = "Illustration Saved";

    /** The Constant SUCCESS_MESSAGE_UPDATE. */
    private static final String SUCCESS_MESSAGE_UPDATE = "Illustration Updated";

    /** The Constant REJECTED_MESSAGE. */
    private static final String REJECTED_MESSAGE = "Illustration Rejected";

    /** The Constant ILLUSTRATION. */
    private static final String ILLUSTRATION = "Illustration";
    
    /** The illustration repository. */
    @Autowired
    private IllustrationRepository illustrationRepository;
    
    /** The id generator. */
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;
    
    @Autowired
	GeneraliAgentProfileRepository agentRepository;
    
    
    /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = TRANSACTION_DATA;
    
    /** The Constant IllustrationOutput. */
    private static final String PRODUCT = "Product";
    /** The Constant IllustrationOutput. */
    private static final String PREMIUM_SUMMARY = "premiumSummary";
    
    /** The Constant IllustrationOutput. */
    private static final String ILLUSTRATION_OUTPUT = "IllustrationOutput";
    
    /** The Constant KEY18. */
    private static final String KEY18 = "Key18";
    
    /** The Constant FALSE. */
    public static final String FALSE = "false";
    

	/*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageIllustrationComponent#saveIllustration(java.lang.String,
     * com.cognizant.insurance.request.vo.RequestInfo, com.cognizant.insurance.audit.LifeEngageAudit)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String saveIllustration(final String json, String jsonIllustrationOutput, final RequestInfo requestInfo,
            final LifeEngageAudit lifeEngageAudit) {
        String eAppResponse = null;
        Transactions transactions = new Transactions();
        String successMessage = "";
        String offlineIdentifier = null;
        Boolean isDeleteRequest = Boolean.FALSE;

        try {
            LifeEngageComponentHelper.parseTrasactionDetails(json, transactions, false);
            // Store to a temp variable to set it back to response.
            offlineIdentifier = transactions.getOfflineIdentifier();
            // Do not process eApp data if already done
            if (auditRepository.isTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                throwSystemException(true, ErrorConstants.LE_SYNC_ERR_102, Constants.PROCESSED_TRANSACTION);
            }
			// Do not process eApp data if already done
            if (null != transactions.getTransTrackingId()) {
             Agreement latestagreement = illustrationRepository.getLatestIllustrationAgreement(transactions,
                                 requestInfo.getTransactionId());
               if(latestagreement != null){
            	   throwSystemException(true, ErrorConstants.LE_SYNC_ERR_102, Constants.PROCESSED_TRANSACTION);
               }
            }
            if (transactions.getStatus() != null
                    && transactions.getStatus().equals(AgreementStatusCodeList.Cancelled.toString())) {
                isDeleteRequest = Boolean.TRUE;
            }
            
            JSONObject jsonObjToRemove;
            jsonObjToRemove = new JSONObject(json);
            JSONObject jsonProductToRemove = null;

            JSONObject jsonTransactionDataToRemove = jsonObjToRemove.getJSONObject(TRANSACTIONDATA);
            
			if (!(jsonTransactionDataToRemove.isNull(ILLUSTRATION_OUTPUT))) {
				jsonTransactionDataToRemove.remove(ILLUSTRATION_OUTPUT);
			}
            if (!(jsonTransactionDataToRemove.isNull(PRODUCT))) {

                jsonProductToRemove = jsonTransactionDataToRemove.getJSONObject(PRODUCT);
                if (!(jsonProductToRemove.isNull(PREMIUM_SUMMARY))) {
                    jsonProductToRemove.remove(PREMIUM_SUMMARY);
                }

            }
            
            transactions = (Transactions) saveIllustrationHolder.parseJson(jsonObjToRemove.toString());
            if (isDeleteRequest) {
                transactions.setStatus(AgreementStatusCodeList.Cancelled.toString());
            }
            JSONObject transObject = jsonObjToRemove.has(TRANSACTION_DATA)?jsonObjToRemove.getJSONObject(TRANSACTION_DATA):new JSONObject();
            String typeVariableObj = transObject.has(ALLOCATION_TYPE)?transObject.getString(ALLOCATION_TYPE):StringUtils.EMPTY;
            String newAgentId ="";
            String lmsTrackingId ="";
            // Fetching agreement By transTracking Id - Fix for duplicate Illustration when key3 not returned properly
            InsuranceAgreement agreement = null;
            List<InsuranceAgreement> agreementList = new ArrayList<InsuranceAgreement>();
            if(typeVariableObj.equalsIgnoreCase(RM_REASSIGN)) {
            	if (jsonObjToRemove.has(KEY39)) {
                	newAgentId = jsonObjToRemove.getString(KEY39);
                }
            	if (jsonObjToRemove.has(KEY22)) {
                	lmsTrackingId = jsonObjToRemove.getString(KEY22);
                }
                transactions.setKey39(newAgentId);
                transactions.setKey22(lmsTrackingId);
            	String leadID = transactions.getCustomerId();
            	transactions.setAllocationType(typeVariableObj);
            	if(leadID != null){
            		String agreementID = "";
            	agreementID = agentRepository.retrieveLeadIdForFna(requestInfo, leadID);
            	agreementList =  agentRepository.retrieveIllustration(requestInfo, agreementID);
            	if(!agreementList.isEmpty()){
        		for(InsuranceAgreement insAgr:agreementList) {
        	        	transactions.setIllustrationId(insAgr.getIdentifier());
                        transactions.setProposal(insAgr);
        	        	buildTransactionWithAgreementData(transactions);
        	        	transactions.setKey39(newAgentId);
                        transactions.setKey22(lmsTrackingId);
	               		String proposalstatus = illustrationRepository.saveIllustration(transactions, requestInfo, insAgr);
	               		
	               		LifeEngageComponentHelper.createResponseStatus(transactions, proposalstatus, successMessage, null);
	
	                    transactions.setOfflineIdentifier(offlineIdentifier);
	
	                    auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
	                           lifeEngageAudit,"");
	               	}
            	}
            	else{
            		LifeEngageComponentHelper.createResponseStatus(transactions, Constants.SUCCESS, successMessage, null);
                    //eAppResponse = saveEAppHolder.parseBO(transactions);
            	}
                }
            }
            else {
            	
	            if (null != transactions.getTransTrackingId()) {
	                agreement =
	                    illustrationRepository.getIllustrationAgreementByTransTrackingId(transactions,
	                                 requestInfo.getTransactionId());
	            }
	            if (agreement != null  || StringUtils.isNotEmpty(transactions.getIllustrationId())) {
	                if(agreement != null){
	                    transactions.setIllustrationId(String.valueOf(agreement.getIdentifier()));
	                }
	                transactions.setMode(Constants.STATUS_UPDATE);
	                successMessage = SUCCESS_MESSAGE_UPDATE;
	            } else {
	                transactions.setIllustrationId(transactions.getAgentId()+String.valueOf(idGenerator.generate(ILLUSTRATION)));
	                transactions.setMode(Constants.STATUS_SAVE);
	                successMessage = SUCCESS_MESSAGE_SAVE;
	            }
				JSONObject jsonObj;
				jsonObj = new JSONObject(json);
	
				JSONObject jsonTransactionData = null;
				JSONObject jsonProduct = null;
				String premiumSummary = null;
				if (!(jsonObj.isNull(TRANSACTIONDATA))) {
					jsonTransactionData = jsonObj.getJSONObject(TRANSACTIONDATA);
	
					if (!(jsonTransactionData.isNull(PRODUCT))) {
						jsonProduct = jsonTransactionData.getJSONObject(PRODUCT);
						if (!(jsonProduct.isNull(PREMIUM_SUMMARY))) {
							premiumSummary = jsonProduct.getJSONObject(
									PREMIUM_SUMMARY).toString();
						}
					}
				}
	           
	            onBeforeSave(transactions, requestInfo, jsonIllustrationOutput,premiumSummary);
	
	            final String proposalstatus = illustrationRepository.saveIllustration(transactions, requestInfo, agreement);
	
	            if (Constants.REJECTED.equals(proposalstatus)) {
	                successMessage = REJECTED_MESSAGE;
	            }
	            LifeEngageComponentHelper.createResponseStatus(transactions, proposalstatus, successMessage, null);
	
	            transactions.setOfflineIdentifier(offlineIdentifier);
	
	            auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
	                   lifeEngageAudit,"");
            }
            eAppResponse = saveIllustrationHolder.parseBO(transactions);
            final JSONObject responseObject = new JSONObject(eAppResponse);
            responseObject.put(KEY18, FALSE);
            eAppResponse = responseObject.toString();
        } catch (DaoException e) {
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_104);
            transactions.setOfflineIdentifier(offlineIdentifier);
            eAppResponse = saveIllustrationHolder.parseBO(transactions);

        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_103);
                transactions.setOfflineIdentifier(offlineIdentifier);
            }

            else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
                transactions.setOfflineIdentifier(offlineIdentifier);
            }
            eAppResponse = saveIllustrationHolder.parseBO(transactions);
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
                transactions.setOfflineIdentifier(offlineIdentifier);
			} else if (ErrorConstants.LE_SYNC_ERR_102.equals(e.getErrorCode())) {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						LifeEngageComponentHelper.SUCCESS,
						SUCCESS_MESSAGE_SAVE, null);

			} else {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						Constants.FAILURE, e.getMessage(), e.getErrorCode());
				transactions.setOfflineIdentifier(offlineIdentifier);
			}
            eAppResponse = saveIllustrationHolder.parseBO(transactions);

        } catch (Exception e) {
            LOGGER.error("Exception", e);         
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_100);
            transactions.setOfflineIdentifier(offlineIdentifier);
            eAppResponse = saveIllustrationHolder.parseBO(transactions);

        }

        return eAppResponse;
    }

    
    private Transactions buildTransactionWithAgreementData(Transactions transactions) throws BusinessException{
    	List<Transactions> transList = new ArrayList<Transactions>();
    	transList.add(transactions);
    	String Respjson = getRetrieveIllustrationHolder().parseBO(transList);
    	transactions = (Transactions) saveIllustrationHolder.parseJson(Respjson);
    	return transactions;
    }
    /**
     * On before save.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @param jsonIllustrationOutput
     *            the json illustration output
     * @throws ParseException
     *             the parse exception
     */
    protected void onBeforeSave(Transactions transactions, RequestInfo requestInfo, String jsonIllustrationOutput, String premiumSummary)
            throws ParseException {
        /* May illustrationOutput can be null while saving before showing the output */
        IllustrationResponsePayLoad illustrationResponsePayLoad = null;
        if (jsonIllustrationOutput != null|| premiumSummary!=null) {
            illustrationResponsePayLoad = new IllustrationResponsePayLoad();
            if (jsonIllustrationOutput != null){
            illustrationResponsePayLoad.setResponseJson(jsonIllustrationOutput);
            }
            if (premiumSummary!=null){
            illustrationResponsePayLoad.setSummaryJson(premiumSummary);	
            }
           // illustrationResponsePayLoad.setIdentifier(Integer.parseInt(transactions.getIllustrationId()));
            illustrationResponsePayLoad.setAgreement(transactions.getProposal());
            Set<IllustrationResponsePayLoad> illustrationResponsePayLoads = new HashSet<IllustrationResponsePayLoad>();
            illustrationResponsePayLoads.add(illustrationResponsePayLoad);
            transactions.getProposal().setIllustrationResponse(illustrationResponsePayLoads);
        }

        transactions.setCreationDateTime(LifeEngageComponentHelper.getCurrentdate());
        transactions.setModifiedTimeStamp(LifeEngageComponentHelper.getCurrentdate());
    }
}
