package com.cognizant.insurance.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class IllusDataGenli.
 */
public class IllusDataGenli {
	
	/** The policy year. */
	private Integer policyYear;
	
	/** The age of life assured. */
	private Integer ageOfLifeAssured;
	
	/** The annual premium. */
	private Long annualPremium;
	
	/** The top up. */
	private Long topUp;
	
	/** The withdrawal amount. */
	private Long withdrawalAmount;
	
	/** The policy value low. */
	private Long policyValueLow;
	
	/** The policy value med. */
	private Long policyValueMed;
	
	/** The policy value high. */
	private Long policyValueHigh;
	
	/** The stringdeath benefit. */
	private Long stringdeathBenefit;

	/**
	 * Gets the policy year.
	 *
	 * @return the policy year
	 */
	public Integer getPolicyYear() {
		return policyYear;
	}

	/**
	 * Sets the policy year.
	 *
	 * @param policyYear the new policy year
	 */
	public void setPolicyYear(Integer policyYear) {
		this.policyYear = policyYear;
	}

	/**
	 * Gets the age of life assured.
	 *
	 * @return the age of life assured
	 */
	public Integer getAgeOfLifeAssured() {
		return ageOfLifeAssured;
	}

	/**
	 * Sets the age of life assured.
	 *
	 * @param ageOfLifeAssured the new age of life assured
	 */
	public void setAgeOfLifeAssured(Integer ageOfLifeAssured) {
		this.ageOfLifeAssured = ageOfLifeAssured;
	}

	/**
	 * Gets the annual premium.
	 *
	 * @return the annual premium
	 */
	public Long getAnnualPremium() {
		return annualPremium;
	}

	/**
	 * Sets the annual premium.
	 *
	 * @param annualPremium the new annual premium
	 */
	public void setAnnualPremium(Long annualPremium) {
		this.annualPremium = annualPremium;
	}

	/**
	 * Gets the top up.
	 *
	 * @return the top up
	 */
	public Long getTopUp() {
		return topUp;
	}

	/**
	 * Sets the top up.
	 *
	 * @param topUp the new top up
	 */
	public void setTopUp(Long topUp) {
		this.topUp = topUp;
	}

	/**
	 * Gets the withdrawal amount.
	 *
	 * @return the withdrawal amount
	 */
	public Long getWithdrawalAmount() {
		return withdrawalAmount;
	}

	/**
	 * Sets the withdrawal amount.
	 *
	 * @param withdrawalAmount the new withdrawal amount
	 */
	public void setWithdrawalAmount(Long withdrawalAmount) {
		this.withdrawalAmount = withdrawalAmount;
	}

	/**
	 * Gets the policy value low.
	 *
	 * @return the policy value low
	 */
	public Long getPolicyValueLow() {
		return policyValueLow;
	}

	/**
	 * Sets the policy value low.
	 *
	 * @param policyValueLow the new policy value low
	 */
	public void setPolicyValueLow(Long policyValueLow) {
		this.policyValueLow = policyValueLow;
	}

	/**
	 * Gets the policy value med.
	 *
	 * @return the policy value med
	 */
	public Long getPolicyValueMed() {
		return policyValueMed;
	}

	/**
	 * Sets the policy value med.
	 *
	 * @param policyValueMed the new policy value med
	 */
	public void setPolicyValueMed(Long policyValueMed) {
		this.policyValueMed = policyValueMed;
	}

	/**
	 * Gets the policy value high.
	 *
	 * @return the policy value high
	 */
	public Long getPolicyValueHigh() {
		return policyValueHigh;
	}

	/**
	 * Sets the policy value high.
	 *
	 * @param policyValueHigh the new policy value high
	 */
	public void setPolicyValueHigh(Long policyValueHigh) {
		this.policyValueHigh = policyValueHigh;
	}

	/**
	 * Gets the stringdeath benefit.
	 *
	 * @return the stringdeath benefit
	 */
	public Long getStringdeathBenefit() {
		return stringdeathBenefit;
	}

	/**
	 * Sets the stringdeath benefit.
	 *
	 * @param stringdeathBenefit the new stringdeath benefit
	 */
	public void setStringdeathBenefit(Long stringdeathBenefit) {
		this.stringdeathBenefit = stringdeathBenefit;
	}

}
