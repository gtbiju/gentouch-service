/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * Loan Type.
 * 
 * @author 301350
 * 
 * 
 */
public enum LoanTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    PreferredLoan,
    /**
     * Would Include annuities, RRSP. Note that the current life fields become
     * regular loan fields. Thus there is no 'regular loan' value in this lookup
     * so that we do not have 2 different ways of representing the same thing.
     * This will be fixed in 3.0. 
     * 
     * 
     * 
     */
    NonLifeRegularLoan,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * 
     * 
     * 
     * 
     */
    MarginAccount,
    /**
     * 
     * 
     * 
     * 
     */
    AutomaticPremiumLoan,
    /**
     * Original mortgage loan on home.
     * 
     * 
     * 
     * 
     * 
     */
    Original,
    /**
     * Refinanced mortgage loan on home.
     * 
     * Notes: Used for annuities
     * 
     * 
     * 
     */
    Refinanced,
    /**
     * Home Equity loan on home.
     * 
     * 
     * 
     */
    HomeEquity
}
