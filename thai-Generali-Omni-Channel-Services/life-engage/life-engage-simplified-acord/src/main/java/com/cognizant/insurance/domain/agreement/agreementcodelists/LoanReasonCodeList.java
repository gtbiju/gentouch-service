/**
 * 
 */
package com.cognizant.insurance.domain.agreement.agreementcodelists;

/**
 * Loan Reason.
 * 
 * @author 301350
 * 
 * 
 */
public enum LoanReasonCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    Topayoffanotherloan,
    /**
     * 
     * 
     * 
     * 
     */
    Topaypremiums,
    /**
     * 
     * 
     * 
     * 
     */
    Other
}
