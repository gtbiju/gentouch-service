package com.cognizant.insurance.domain.documentandcommunication;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;

@Entity
public class Requirement extends InformationModelObject {
    /**
     * 
     */
    private static final long serialVersionUID = 7571675872570970712L;

    /**
     * 
     * This relationship links document with a requirement document.
     * 
     * 
     * 
     */

    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<AgreementDocument> requirementDocuments;

    /**
     * Links the comments for a requirement
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Comment> requirementComments;

    /**
     * @return the requirementComments
     */
    public List<Comment> getRequirementComments() {
        return requirementComments;
    }

    /**
     * @param requirementComments
     *            the requirementComments to set
     */
    public void setRequirementComments(List<Comment> requirementComments) {
        this.requirementComments = requirementComments;
    }

    /**
     * Indicates the requirement name. for example Age Proof
     */

    private String requirementName;

    /**
     * Defines the requirement Status
     **/
    private String syncStatus;

    /**
     * Defines the document Status
     **/
    private String uploadstatus;

    /**
     * Defines the requirement date
     **/
    private Date requirementDate;

    /**
     * 
     */
    private Boolean isMandatory;

    /**
     * Links the party associated. for example insured/proposer
     **/
    @Column(name = "partyIdentifier")
    private String partyIdentifier;

    /**
     * Defines the requirement type. for example age/address
     **/
    private DocumentTypeCodeList requirementType;
    
    /**
     * Added for generali
     */
    private String requirementSubType;
    
    private String serialNumber;
    
    public String getRequirementSubType() {
        return requirementSubType;
    }

    public void setRequirementSubType(String requirementSubType) {
        this.requirementSubType = requirementSubType;
    }

    /**
     * @return the syncStatus
     */
    public String getSyncStatus() {
        return syncStatus;
    }

    /**
     * @param syncStatus
     *            the syncStatus to set
     */
    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    /**
     * @return the uploadstatus
     */
    public String getUploadstatus() {
        return uploadstatus;
    }

    /**
     * @param uploadstatus
     *            the uploadstatus to set
     */
    public void setUploadstatus(String uploadstatus) {
        this.uploadstatus = uploadstatus;
    }

    /**
     * @return the requirementType
     */
    public DocumentTypeCodeList getRequirementType() {
        return requirementType;
    }

    /**
     * @param requirementType
     *            the requirementType to set
     */
    public void setRequirementType(DocumentTypeCodeList requirementType) {
        this.requirementType = requirementType;
    }

    /**
     * @return the partyIdentifier
     */
    public String getPartyIdentifier() {
        return partyIdentifier;
    }

    /**
     * @param partyIdentifier
     *            the partyIdentifier to set
     */
    public void setPartyIdentifier(String partyIdentifier) {
        this.partyIdentifier = partyIdentifier;
    }

    /**
     * @param requirementDate
     *            the requirementDate to set
     */
    public void setRequirementDate(Date requirementDate) {
        this.requirementDate = requirementDate;
    }

    /**
     * @return the requirementDate
     */
    public Date getRequirementDate() {
        return requirementDate;
    }

    /**
     * @param requirementName
     *            the requirementName to set
     */
    public void setRequirementName(String requirementName) {
        this.requirementName = requirementName;
    }

    /**
     * @return the requirementName
     */
    public String getRequirementName() {
        return requirementName;
    }

    /**
     * @return the isMandatory
     */
    public Boolean getIsMandatory() {
        return isMandatory;
    }

    /**
     * @param isMandatory
     *            the isMandatory to set
     */
    public void setIsMandatory(Boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

    /**
     * @return the requirementDocuments
     */
    public List<AgreementDocument> getRequirementDocuments() {
        return requirementDocuments;
    }

    /**
     * @param requirementDocuments
     *            the requirementDocuments to set
     */
    public void setRequirementDocuments(List<AgreementDocument> requirementDocuments) {
        this.requirementDocuments = requirementDocuments;
    }

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
}
