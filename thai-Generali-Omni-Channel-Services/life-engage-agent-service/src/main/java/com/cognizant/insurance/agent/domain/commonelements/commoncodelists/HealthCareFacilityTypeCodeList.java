/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of health care facilities according to their
 * type.
 * 
 * @author 301350
 * 
 * 
 */
public enum HealthCareFacilityTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    CentreOfExcellence,
    /**
     * 
     * 
     * 
     * 
     */
    NursingHome,
    /**
     * 
     * 
     * 
     * 
     */
    TraumaCentre,
    /**
     * 
     * 
     * 
     * 
     */
    BirthingCentre,
    /**
     * 
     * 
     * 
     * 
     */
    AmbulatoryFacility,
    /**
     * 
     * 
     * 
     * 
     */
    EmergencyCentre
}
