package com.cognizant.insurance.omnichannel.service;

public interface LifeAsiaService {
	
	public void submit(String spajNo);
	public String getStatus(String spajNo);
	public String getRequestData(String spajNo);
	public String generateBPMRequest(String data);
	public void resubmit(String spajNo);
	public String invokeSchedule();
	public String generateBPMEmailPdf(String json);
	

}
