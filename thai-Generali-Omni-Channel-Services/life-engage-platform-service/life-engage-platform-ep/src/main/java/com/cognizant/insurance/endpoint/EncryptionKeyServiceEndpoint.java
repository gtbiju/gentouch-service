/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 356551
 * @version       : 0.1, Mar 10, 2016
 */
package com.cognizant.insurance.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.EncryptionKeyService;

/**
 * The Class class EncryptionKeyServiceEndpoint.
 */
@Controller
@RequestMapping("/encryptionKeyService")
public class EncryptionKeyServiceEndpoint {

    /** The encryption key service. */
    @Autowired
    private EncryptionKeyService encryptionKeyService;

    @RequestMapping(value = "/fetchKey", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String fetchKey(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return encryptionKeyService.fetchKey(json);
            }
        }.execute(json);
    }
    
}
