/**
 * 
 */
package com.cognizant.insurance.domain.agreement.partyroleinagreement;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;

/**
 * A party applying for financial services. The party has not yet been accepted
 * by the financial services provider; no financial services agreement has been
 * issued yet.
 * 
 * e.g: Eds Construction Company that wants to be insured in a new construction
 * liability insurance.
 * 
 * e.g: John Doe who wants to be insured in a new Life policy. 
 *
 * 
 * @author 301350
 * 
 * 
 */


@Entity
@DiscriminatorValue("APPLICANT")
public class Applicant extends PartyRoleInAgreement {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2903430577321070802L;

    /**
     * The duration prior to the agreement start date during which the applicant
     * has not made a claim on any insurer for this type of agreement or
     * coverage. A premium discount is often given if the period is sufficiently
     * long.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private TimePeriod noClaimDiscountDuration;

    /**
     * The monetary amount of claims declared by an applicant, not including
     * claims filed to the modeled organization. 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private CurrencyAmount declaredClaimAmount;

    /**
     * The number of claims declared by an applicant, not including claims filed
     * to the modeled organization.
     * 
     * 
     * 
     */
    private BigDecimal declaredClaimCount;

    /**
     * An indicator the applicant had an agreement that was terminated by an
     * insurer.
     * 
     * 
     * 
     */
    private Boolean agreementTerminatedByInsurerIndicator;

    /**
     * The reason the agreement was terminated by the insurer.
     * 
     * e.g. claim activity, driving record, false declaration, non payment of
     * premium
     * 
     * 
     * 
     */
    private String agreementTerminationReason;

    /**
     * The amount of time that has passed since the party's prior agreement was
     * terminated.
     * 
     * e.g. Less than 3 months From 6 to 12 months From 12 to 24 months From 24
     * to 36 months More than 36 months'
     * 
     * 
     * 
     */
    private String timeSinceAgreementTermination;

    /**
     * The time period, as determined by the underwriting party, associated with
     * the applicant's loss history (e.g. 3 years, 5 years).
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private TimePeriod lossTimePeriodDuration;

    /**
     * Gets the noClaimDiscountDuration.
     * 
     * @return Returns the noClaimDiscountDuration.
     */
    public TimePeriod getNoClaimDiscountDuration() {
        return noClaimDiscountDuration;
    }

    /**
     * Gets the declaredClaimAmount.
     * 
     * @return Returns the declaredClaimAmount.
     */
    public CurrencyAmount getDeclaredClaimAmount() {
        return declaredClaimAmount;
    }

    /**
     * Gets the declaredClaimCount.
     * 
     * @return Returns the declaredClaimCount.
     */
    public BigDecimal getDeclaredClaimCount() {
        return declaredClaimCount;
    }

    /**
     * Gets the agreementTerminatedByInsurerIndicator.
     * 
     * @return Returns the agreementTerminatedByInsurerIndicator.
     */
    public Boolean getAgreementTerminatedByInsurerIndicator() {
        return agreementTerminatedByInsurerIndicator;
    }

    /**
     * Gets the agreementTerminationReason.
     * 
     * @return Returns the agreementTerminationReason.
     */
    public String getAgreementTerminationReason() {
        return agreementTerminationReason;
    }

    /**
     * Gets the timeSinceAgreementTermination.
     * 
     * @return Returns the timeSinceAgreementTermination.
     */
    public String getTimeSinceAgreementTermination() {
        return timeSinceAgreementTermination;
    }

    /**
     * Gets the lossTimePeriodDuration.
     * 
     * @return Returns the lossTimePeriodDuration.
     */
    public TimePeriod getLossTimePeriodDuration() {
        return lossTimePeriodDuration;
    }

    /**
     * Sets The noClaimDiscountDuration.
     * 
     * @param noClaimDiscountDuration
     *            The noClaimDiscountDuration to set.
     */
    public void setNoClaimDiscountDuration(
            final TimePeriod noClaimDiscountDuration) {
        this.noClaimDiscountDuration = noClaimDiscountDuration;
    }

    /**
     * Sets The declaredClaimAmount.
     * 
     * @param declaredClaimAmount
     *            The declaredClaimAmount to set.
     */
    public void setDeclaredClaimAmount(final CurrencyAmount declaredClaimAmount) {
        this.declaredClaimAmount = declaredClaimAmount;
    }

    /**
     * Sets The declaredClaimCount.
     * 
     * @param declaredClaimCount
     *            The declaredClaimCount to set.
     */
    public void
            setDeclaredClaimCount(final BigDecimal declaredClaimCount) {
        this.declaredClaimCount = declaredClaimCount;
    }

    /**
     * Sets The agreementTerminatedByInsurerIndicator.
     * 
     * @param agreementTerminatedByInsurerIndicator
     *            The agreementTerminatedByInsurerIndicator to set.
     */
    public void setAgreementTerminatedByInsurerIndicator(
            final Boolean agreementTerminatedByInsurerIndicator) {
        this.agreementTerminatedByInsurerIndicator =
                agreementTerminatedByInsurerIndicator;
    }

    /**
     * Sets The agreementTerminationReason.
     * 
     * @param agreementTerminationReason
     *            The agreementTerminationReason to set.
     */
    public void setAgreementTerminationReason(
            final String agreementTerminationReason) {
        this.agreementTerminationReason = agreementTerminationReason;
    }

    /**
     * Sets The timeSinceAgreementTermination.
     * 
     * @param timeSinceAgreementTermination
     *            The timeSinceAgreementTermination to set.
     */
    public void setTimeSinceAgreementTermination(
            final String timeSinceAgreementTermination) {
        this.timeSinceAgreementTermination = timeSinceAgreementTermination;
    }

    /**
     * Sets The lossTimePeriodDuration.
     * 
     * @param lossTimePeriodDuration
     *            The lossTimePeriodDuration to set.
     */
    public void setLossTimePeriodDuration(
            final TimePeriod lossTimePeriodDuration) {
        this.lossTimePeriodDuration = lossTimePeriodDuration;
    }

}
