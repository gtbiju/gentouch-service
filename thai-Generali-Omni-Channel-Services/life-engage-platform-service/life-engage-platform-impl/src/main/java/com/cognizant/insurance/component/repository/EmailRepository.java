package com.cognizant.insurance.component.repository;

import java.text.ParseException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.generic.component.EmailComponent;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.response.Response;
import org.springframework.beans.factory.annotation.Value;
/**
 * The Class class EmailRepository.
 * 
 */
//@Repository
public class EmailRepository {
    /** The entity manager. */
    @PersistenceContext(unitName="LE_Platform")
    private EntityManager entityManager;

    /** The email component. */
    @Autowired
    private EmailComponent emailComponent;
    
    @Value("${le.platform.service.email.maxAttemptNo}")
    private String maxAttemptNo;

    /**
     * Save LifeEngage email details.
     * 
     * @param json
     *            the json
     * @param attemptNo
     *            the attemptNo
     *  @param status
     *            the status          
     * @return the LifeEngageEmail
     */
    public final LifeEngageEmail savelifeEngageEmail(final LifeEngageEmail lifeEngageEmail) {
        final Request<LifeEngageEmail> request =
                new JPARequestImpl<LifeEngageEmail>(entityManager, ContextTypeCodeList.EMAIL, null);
        request.setType(lifeEngageEmail);
        emailComponent.save(request);
        return lifeEngageEmail;
    }
    /**
     * Update LifeEngage email details.
     * 
     * @param id
     *            the id
     * @param json
     *            the json
     * @param attemptNo
     *            the attemptNo
     *  @param status
     *            the status          
     * @return the LifeEngageEmail
     */
    public final LifeEngageEmail updatelifeEngageEmail(final LifeEngageEmail lifeEngageEmail) {
        final Request<LifeEngageEmail> request =
                new JPARequestImpl<LifeEngageEmail>(entityManager, ContextTypeCodeList.EMAIL, null);
        request.setType(lifeEngageEmail);
        emailComponent.update(request);
        return lifeEngageEmail;
    }
    
    /**
     * Get failed email details list.
     * 
     * @return the LifeEngageEmail List
     */
    public final List<LifeEngageEmail> getFailedLifeEngageEmails() {
    	final Request<LifeEngageEmail> request =
                new JPARequestImpl<LifeEngageEmail>(entityManager, ContextTypeCodeList.EMAIL,
                        null);
    	final Response<List<LifeEngageEmail>> response= emailComponent.retrieveFailedLifeEngageEmailDetails(request,maxAttemptNo);
    	List<LifeEngageEmail> failedEmailList = response.getType();
    	return failedEmailList;
    }
    
    /**
     * Get initialised email details list.
     * 
     * @return the LifeEngageEmail List
     */
    public final List<LifeEngageEmail> getInitialisedLifeEngageEmails(final String agentId, final String type) {
    	final Request<LifeEngageEmail> request =
                new JPARequestImpl<LifeEngageEmail>(entityManager, ContextTypeCodeList.EMAIL,
                        null);
    	 LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
         lifeEngageEmail.setAgentId(agentId);
         lifeEngageEmail.setType(type);
         request.setType(lifeEngageEmail);
    	final Response<List<LifeEngageEmail>> response= emailComponent.retrieveInitialisedLifeEngageEmailDetails(request);
    	List<LifeEngageEmail> initialisedEmailList = response.getType();
    	return initialisedEmailList;
    }
    /**
     * @return the entityManager
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager
     *            the entityManager to set
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    /**
     * @return the emailComponent
     */
	public EmailComponent getEmailComponent() {
		return emailComponent;
	}
	 /**
     * @param emailComponent
     *            the emailComponent to set
     */
	public void setEmailComponent(EmailComponent emailComponent) {
		this.emailComponent = emailComponent;
	}

}
