package com.cognizant.insurance.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class IllusFundDataGli.
 */
public class IllusFundDataGli {

	/** The policy year. */
	private Integer policyYear;
	
	/** The age of life assured. */
	private Integer ageOfLifeAssured;
	
	/** The money market low. */
	private Long moneyMarketLow;
	
	/** The money market med. */
	private Long moneyMarketMed;
	
	/** The money market high. */
	private Long moneyMarketHigh;
	
	/** The equity market low. */
	private Long equityMarketLow;
	
	/** The equity market med. */
	private Long equityMarketMed;
	
	/** The equity market high. */
	private Long equityMarketHigh;
	
	/** The fixed market low. */
	private Long fixedMarketLow;
	
	/** The fixed market med. */
	private Long fixedMarketMed;
	
	/** The fixed market high. */
	private Long fixedMarketHigh;
	
	/** The death benefit. */
	private Long deathBenefit;

	/**
	 * Gets the policy year.
	 *
	 * @return the policy year
	 */
	public Integer getPolicyYear() {
		return policyYear;
	}

	/**
	 * Sets the policy year.
	 *
	 * @param policyYear the new policy year
	 */
	public void setPolicyYear(Integer policyYear) {
		this.policyYear = policyYear;
	}

	/**
	 * Gets the age of life assured.
	 *
	 * @return the age of life assured
	 */
	public Integer getAgeOfLifeAssured() {
		return ageOfLifeAssured;
	}

	/**
	 * Sets the age of life assured.
	 *
	 * @param ageOfLifeAssured the new age of life assured
	 */
	public void setAgeOfLifeAssured(Integer ageOfLifeAssured) {
		this.ageOfLifeAssured = ageOfLifeAssured;
	}

	/**
	 * Gets the money market low.
	 *
	 * @return the money market low
	 */
	public Long getMoneyMarketLow() {
		return moneyMarketLow;
	}

	/**
	 * Sets the money market low.
	 *
	 * @param moneyMarketLow the new money market low
	 */
	public void setMoneyMarketLow(Long moneyMarketLow) {
		this.moneyMarketLow = moneyMarketLow;
	}

	/**
	 * Gets the money market med.
	 *
	 * @return the money market med
	 */
	public Long getMoneyMarketMed() {
		return moneyMarketMed;
	}

	/**
	 * Sets the money market med.
	 *
	 * @param moneyMarketMed the new money market med
	 */
	public void setMoneyMarketMed(Long moneyMarketMed) {
		this.moneyMarketMed = moneyMarketMed;
	}

	/**
	 * Gets the money market high.
	 *
	 * @return the money market high
	 */
	public Long getMoneyMarketHigh() {
		return moneyMarketHigh;
	}

	/**
	 * Sets the money market high.
	 *
	 * @param moneyMarketHigh the new money market high
	 */
	public void setMoneyMarketHigh(Long moneyMarketHigh) {
		this.moneyMarketHigh = moneyMarketHigh;
	}

	/**
	 * Gets the equity market low.
	 *
	 * @return the equity market low
	 */
	public Long getEquityMarketLow() {
		return equityMarketLow;
	}

	/**
	 * Sets the equity market low.
	 *
	 * @param equityMarketLow the new equity market low
	 */
	public void setEquityMarketLow(Long equityMarketLow) {
		this.equityMarketLow = equityMarketLow;
	}

	/**
	 * Gets the equity market med.
	 *
	 * @return the equity market med
	 */
	public Long getEquityMarketMed() {
		return equityMarketMed;
	}

	/**
	 * Sets the equity market med.
	 *
	 * @param equityMarketMed the new equity market med
	 */
	public void setEquityMarketMed(Long equityMarketMed) {
		this.equityMarketMed = equityMarketMed;
	}

	/**
	 * Gets the equity market high.
	 *
	 * @return the equity market high
	 */
	public Long getEquityMarketHigh() {
		return equityMarketHigh;
	}

	/**
	 * Sets the equity market high.
	 *
	 * @param equityMarketHigh the new equity market high
	 */
	public void setEquityMarketHigh(Long equityMarketHigh) {
		this.equityMarketHigh = equityMarketHigh;
	}

	/**
	 * Gets the fixed market low.
	 *
	 * @return the fixed market low
	 */
	public Long getFixedMarketLow() {
		return fixedMarketLow;
	}

	/**
	 * Sets the fixed market low.
	 *
	 * @param fixedMarketLow the new fixed market low
	 */
	public void setFixedMarketLow(Long fixedMarketLow) {
		this.fixedMarketLow = fixedMarketLow;
	}

	/**
	 * Gets the fixed market med.
	 *
	 * @return the fixed market med
	 */
	public Long getFixedMarketMed() {
		return fixedMarketMed;
	}

	/**
	 * Sets the fixed market med.
	 *
	 * @param fixedMarketMed the new fixed market med
	 */
	public void setFixedMarketMed(Long fixedMarketMed) {
		this.fixedMarketMed = fixedMarketMed;
	}

	/**
	 * Gets the fixed market high.
	 *
	 * @return the fixed market high
	 */
	public Long getFixedMarketHigh() {
		return fixedMarketHigh;
	}

	/**
	 * Sets the fixed market high.
	 *
	 * @param fixedMarketHigh the new fixed market high
	 */
	public void setFixedMarketHigh(Long fixedMarketHigh) {
		this.fixedMarketHigh = fixedMarketHigh;
	}

	/**
	 * Gets the death benefit.
	 *
	 * @return the death benefit
	 */
	public Long getDeathBenefit() {
		return deathBenefit;
	}

	/**
	 * Sets the death benefit.
	 *
	 * @param deathBenefit the new death benefit
	 */
	public void setDeathBenefit(Long deathBenefit) {
		this.deathBenefit = deathBenefit;
	}


}
