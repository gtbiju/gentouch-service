/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.contactcodelists;

/**
 * A code list indicating the nature of the city with regards to government
 * administrative functions, if applicable. 
 * 
 * @author 301350
 * 
 * 
 */
public enum AdministrativeSubDivisionCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    StateCapital,
    /**
     * 
     * 
     * 
     * 
     */
    CountryCapital,
    /**
     * 
     * 
     * 
     * 
     */
    CountySeat
}
