/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Feb 27, 2013
 */
package com.cognizant.insurance.searchcriteria;

import java.io.Serializable;
import java.util.Map;

/**
 * The Class class ProductSelectionCriteria.
 */
public class ProductSelectionCriteria implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 5259363995900604707L;
    private Map<String, Map<String, String>> criteriaMap;

    /**
     * Sets The criteriaMap.
     *
     * @param criteriaMap The criteriaMap to set.
     */
    public void setCriteriaMap(Map<String, Map<String, String>> criteriaMap) {
        this.criteriaMap = criteriaMap;
    }

    /**
     * Gets the criteriaMap.
     *
     * @return Returns the criteriaMap.
     */
    public Map<String, Map<String, String>> getCriteriaMap() {
        return criteriaMap;
    }
    
}
