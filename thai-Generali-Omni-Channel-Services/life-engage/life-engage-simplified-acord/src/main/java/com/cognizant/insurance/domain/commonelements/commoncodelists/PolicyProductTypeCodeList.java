/**
 * 
 */
package com.cognizant.insurance.domain.commonelements.commoncodelists;

/**
 * @author 291433
 * 
 */
public enum PolicyProductTypeCodeList {

    /**
     * 
     * 
     * 
     * 
     */
    AccidenAndHealth,

    /**
     * Accidental Death & Dismemberment (ADD) insurance provides a benefit if
     * the death or dismemberment may be resulting from an accident.
     */

    AccidentalDeathAndDismemberment,
    /**
     * 
     * 
     * 
     * 
     */
    AccidentalDeathOrDisabilityPolicy,
    /**
     * This would be modeled using the life policy, similar to a decreasing
     * term.
     * 
     * 
     */
    CreditMortgage,

    /**
     * Critical Illness is designed to assist individuals by cover excess
     * expenses (usually beyond what health care or disability insurance would
     * cover) in the event that an individual contracts one of the diseases
     * (usually 5-7 diseases) named in the policy. It usually pay a lump sum
     * upon proof of claim. It would be modeled using the life section of the
     * model.
     */
    CriticalIllness,

    /**
     * Dental insurance provides coverage for the cost associated with dental
     * care based on the conditions, benefits and choices of treatment specified
     * within the policy.
     */
    Dental,
    /**
     * 
     * 
     * 
     * 
     */
    DisabilityIncomeReplacementLongTerm,
    /**
     * 
     * 
     * 
     * 
     */
    DisabilityIncomeReplacementShortTerm,
    /**
     * 
     * 
     * 
     * 
     */
    DisabilityPolicyThatPaysOnlyLumpSum,
    /**
     * This is a base policy that covers the life assured in the event of either
     * contracting a dreaded disease or becoming impaired.
     * 
     */
    DreadDiseaseAndImpairment,

    /**
     * Endowment insurance has elements of both term and permanent insurance.
     * Like term insurance, it provides a death benefit if the insured dies
     * within a specified term. Like whole life insurance, it provides a savings
     * element (cash value). An additional feature is that it provides a policy
     * benefit if the insured lives to the end of the endowment period.
     */
    Endowment,
    /**
     * 
     * 
     * 
     * 
     */
    ErrorsAndOmissionCoverage,
    /**
     * This is a designation for a group plan covering a specific group of
     * people that Gets rates and benefits based on the experience of the group.
     */
    ExperienceGroupPlan,
    /**
     * 
     * 
     * 
     * 
     */
    FuneralPolicy,
    /**
     * 
     * 
     * 
     * 
     */
    GroupMasterContract,
    /**
     * 
     * 
     * 
     * 
     */
    GuaranteedInsurabilityPolicy,
    /**
     * Long Term Disability (LTD) insurance provides a replacement of monthly
     * earnings to insureds that become disabled for extended periods of time
     * due to an accident or sickness.
     */
    LongTermDisability,
    /**
     * 
     * 
     * 
     * 
     */
    OpenEndedInvestment,
    /**
     * 
     * 
     * 
     * 
     */
    OpenEndedInvestmentPolicyForRetirement,

    /**
     * 
     * 
     * 
     * 
     */
    PolicyPaysOutAnIncomeStreamInsteadOfLumpSum,
    /**
     * A plan or account designed for providing the accumulation and then later
     * payout of assets for the purpose of providing continued income in
     * retirement. It may or may not include insured products, like life
     * insurance, annuities, long term care and most likely will include other
     * non-insurance products such as funds, bonds, etc. They may also be
     * qualified or non-qualified.
     */
    RetirementPlan,
    /**
     * An income replacement type benefit should the client become retrenched
     * from his job (unemployed).
     */
    RetrenchmentBenefit,
    /**
     * Short Term Disability (STD) insurance provides a replacment of employment
     * wages during a leave of absence due to an injury or sickness that
     * prevents an insured from attending work for a brief period of time.
     */
    ShortTermDisability,
    /**
     * 
     * 
     * 
     * 
     */
    TermWithCashValue,
    /**
     * Vision insurance provides coverage for the services rendered for eye care
     * based on the conditions, benefits and choices of treatment specified
     * within the policy.
     */
    Vision,
    /**
     * a) A series of payments made or received at regular intervals. b) A
     * contract that provides for a series of payments to be made or received at
     * regular intervals. Annuity value is tied to fixed interest rate.
     */
    FixedAnnuity,
    /**
     * Annuity value tied to pricing index e.g., S and P index. Regular
     * annuity fixed interest rate. Variable annuity is tied to stocks and
     * investment returns.
     */
    IndexedAnnuity,
    /**
     * A form of annuity policy under which the amount of each benefit is not
     * guaranteed and specified in the policy, but which instead fluctuates
     * according to the earnings of a separate account fund. A variable annuity
     * is designed for people willing to take more risk with their money in
     * exchange for a greater growth potential. Annuity value is tied to stock
     * and investment returns.
     */
    VariableAnnuity,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * Pays for the treatment of myocardial infarction, stroke, cancer, kidney
     * failure, angioplasty, bypass and organ transplant based on selected
     * amount and is reduced by a set percentage at carrier specified age.
     */
    HeartOrStroke,
    /**
     * 
     * 
     * 
     * 
     */
    IntensiveCare,
    /**
     * Limited Benefit convalescent care with a maximum lifetime benefit set in
     * the policy. Could include: payments for Facility Care including a bed
     * reservation; payments for home care visits by a nurse, aide or therapist;
     * payments for supplies such as a wheelchair or hospital bed and could pay
     * for hospice care and adult day care.
     * 
     */
    ShortTermCare,
    /**
     * The purchase of controlling interest in one corporation by another
     * corporation, in order to take over assets and/or operations.
     */
    Buyout,
    /**
     * A very narrow form of health insurance that covers the policyholder in
     * the event he or she contracts cancer. Policies often exclude skin cancer.
     * Some policies will not pay for cancer treatments until several years
     * after the policy was purchased. Consumer groups and insurance regulators
     * have said cancer insurance policies are more expensive than they are
     * worth, since the insurance companies pay out a rather small percentage of
     * the premiums they collect.
     */
    Cancer,
    /**
     * A type of medical expense policy that is designed to cover only those
     * medical expenses incurred by an insured who has contracted a specified
     * disease, such as cancer, which is named in the policy. Also known as
     * limited coverage policy.
     */
    DreadDisease,
    /**
     * Health insurance that provides a stipulated daily, weekly, or monthly
     * payment to an insured person during hospital confinement, without regard
     * to the actual confinement expense.
     */
    HospitalIndemnity,
    /**
     * A health insurance policy that provides a predetermined flat benefit
     * amount for each day an insured is hospitalized; the amount does not vary
     * according to the amount of medical expenses the insured incurs.
     */
    HospitalOrSurgical,
    /**
     * Benefit in disability income insurance whereby an injured or ill wage
     * earner receives a monthly income payment to replace a percentage of his
     * or her lost earnings.
     */
    IncomeReplacement,
    /**
     * 
     * 
     * 
     * 
     */
    MajorMedical,
    /**
     * Medical expense insurance provides coverage against loss from illness or
     * bodily injury based on the conditions, benefits and choices of treatment
     * specified within the policy. This coverage can be used to pay for
     * medicine, visits to the doctor, hospital stays, and other medical
     * expenses.
     */
    MedicalExpense,
    /**
     * A policy designed to act as a supplement to Medicare. The supplement is
     * in the form of additional benefits to that provided by Medicare. The
     * additional benefits are in the form of payment for medical expenses
     * incurred but excluded by Medicare's deductibles by limitations on
     * approval medical charges, by limitations on length and type of care in
     * nursing facilities, and by limitations imposed by various cost-sharing
     * requirements. Most of these policies pay substantially less than 100% of
     * the expenses not covered under Medicare. Also known as Medigap insurance.
     */
    MedicareSupplement,
    /**
     * Insurance for business owners to help offset continuing business expenses
     * if the owner is disabled. OR Type of disability income policy used to
     * provide funds for the ongoing monthly business expenses (such as employee
     * salaries, utility charges, rent, and equipment payment due) necessary to
     * maintain continuing operations in the event an owner/ key person becomes
     * disabled. Generally, there is a 60-day elimination period after which
     * monthly income payments commence until a stipulated aggregate limit has
     * been reached.
     */
    OverheadExpense,
    /**
     * An accident death benefit often included in group insurance policies
     * issued to employer-employee groups. This benefit is payable only if an
     * accident occurs while an employee is travelling for the employer.
     */
    TravelAccident,
    /**
     * Benefit pays for expenses incurred for home and community care services
     * such as a nurse or licensed therapist, adult day care, hospice care,
     * respite care, equipment/home modification, caregiver training, home
     * health aide/personal care attendant, homemaker services, and chore
     * services. There are other services that could fall into this category
     */
    HomeHealthCare,
    /**
     * This is day-to-day care that patient(generally older than 65) receives in
     * a nursing facility or in his or her residence following an illness or
     * injury, or in old age, such that the patient can no longer perform at
     * least two of the five basic activities of daily living: walking, eating,
     * dressing, using the bathroom, and mobility from one place to another.
     */
    LongTermCare,
    /**
     * This is also known as "Facility Care or Facilities". Nursing Home Care
     * can be sold as a stand-alone product; however it is common to have this
     * with a combination of "Home and Community Care".
     */
    NursingHome,
    /**
     * Amount credited to the cash value of an insured's life insurance policy
     * above the minimum interest rate it guarantees. This payment is of extreme
     * importance to a policy owner since it will directly affect the size of
     * the cash value.
     */
    ExcessInterestLife,
    /**
	 * 
	 */
    IndexedUniversalLife,
    /**
     * Whole life policies, which vary the premium rate to reflect changing
     * assumptions regarding mortality, investment, and expense factors: these
     * policies also specify that the cash value can be greater than that
     * guaranteed if changing assumptions warrant such an increase. Also known
     * as "currentassumption whole life."
     */
    InterestSensitiveWholeLife,
    /**
     * A plan of insurance that covers the insured for only a certain period of
     * time (term), not for his or her entire life. The policy pays death
     * benefits only if the insured dies during the term.
     */
    Term,
    /**
     * Adjustable life insurance under which (1) premiums are flexible, not
     * fixed; (2) protection is adjustable, not fixed; (3) insurance company
     * expenses and other charges are specifically disclosed to a purchaser.
     */
    UniversalLife,
    /**
     * A form of whole life that combines the premium and face amount
     * flexibility of Universal life with the investment flexibility and risk of
     * variable life insurance. Also known as "universal life II" or
     * "flexiblepremium variable life policies."
     */
    VariableUniversalLife,
    /**
     * The face value and the cash value of the policy fluctuate according to
     * the investment performance of a separate account fund.
     */
    VariableWholeLife,
    /**
     * A plan of insurance for life, with premiums payable for a person's entire
     * life. Life insurance payable to a beneficiary upon the death of an
     * insured whenever that event occurs.
     */
    WholeLife,
    /**
     * This type code should only be used for the policy object that represents
     * the package details and not the individual policies that make up the
     * package.
     */
    Package,
    /**
     * This product type is Automobile insurance. Which covers insuring a
     * vehicle for loss or repairs after an accident.
     */
    Auto,
    /**
     * Boat coverage will provide shelter to your boat and protection for you
     * and your passengers.
     */
    Boat,
    /**
     * A combination of various Property and Casualty insurance policies, for
     * example a Home and Auto policy combined into one, or an Auto and Boat,
     * where a partied would share roles on the same policy, for example the
     * driver of the boat and driver of the auto.
     */
    Combo,
    /**
     * A basic home policy covers perils, which are specifically named risks
     * such as lightning, theft, fire, smoke, wind and explosion. Home insurance
     * can protect a person or company from damages to their home, its contents
     * or even from financial liability if someone outside of your household is
     * injured and you are found to be responsible. From some insurance
     * companies you are able to purchase special policies that cover the house
     * for all perils except those explicitly excluded by the policy though,
     * such as pet damage.
     */
    Home,
    /**
     * Personal Excess Liability Policy, which is an Umbrella coverage policy
     * protecting your assets against lawsuits by providing additional liability
     * coverage.
     */
    PELP,
    /**
     * It is a grouped retirement fund opposed to a Retirement Annuity which is
     * an individual fund.
     */
    GroupPension,
    /**
     * This is a plan of which the savings element is invested under a LISP
     * (Linked Investment Service Provider) licence in Unit Trusts and Shares
     * that is also subject to Capital Gains Tax in the hands of the investor.
     * However there could also be risk benefits included in this plan that is
     * then sold under the LIFE licence. It is therefore a combination of LISP
     * savings with LIFE risk benefits that is sold as one product.
     */
    LifeLinkedInvestment,
    /**
     * At retirement, a member may take up to one third of the capital as a lump
     * sum. The remaining two thirds must be used to provide a pension for life.
     * Tax is deducted at retirement. A member can make one taxablead hoc
     * withdrawal from the fund before retirement
     */
    PreservationPensionFund,
    /**
     * At retirement, a member may take the full proceeds as a lump sum. Member
     * may purchase a Life Annuity with the proceeds or after tax has been
     * deducted, one may also invest the lump sum. A member can make one taxable
     * ad hoc withdrawal from the fund before retirement.
     */
    PreservationProvidentFund,
    /**
     * It is a grouped retirement fund opposed to an individual endowment
     * policy. The tax implication for the proceeds of provident fund and a
     * provident owned endowment policy differs from that of an individual
     * endowment policy
     */
    ProvidentFund,
    /**
     * Sickness: shall mean any disability resulting from disease, injury,
     * accident or other cause or condition, that necessitates medical or dental
     * treatment."Permanent Incapacity" shall mean that the Policyholder is
     * significantly prevented, due to Sickness, from carrying out his
     * professional duties or other such professional duties as his professional
     * qualifications and experience enable him to carry out and there is little
     * likelihood of him again being fully able to carry out such professional
     * duties and he shall be deemed either totally or partially Permanently
     * Incapacitated accordingly.
     */
    TheSicknessAndPermanentIncapacity,
    /**
	 * 
	 */
    RetirementAnnuity,
    /**
	 * 
	 */
    GroupSuperannuation,
    /**
	 * 
	 */
    IndividualSuperannuation,
    /**
	 * 
	 */
    NotMentioned,    
    /**
     * Added as per product family mapping in db
     */
    Pension,
    /**
     * Added as per product family mapping in db
     */
    Investment,
    /**
     * Added for ULIP Product
     * 
     */
    UnitLinked

}
