package com.cognizant.insurance.omnichannel.service.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONObject;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.BulkUploadComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliEmailComponent;
import com.cognizant.insurance.omnichannel.component.GeneraliPushNotificationsComponent;
import com.cognizant.insurance.omnichannel.service.BulkUploadService;
import com.cognizant.insurance.omnichannel.vo.BulkUploadData;
import com.cognizant.insurance.request.vo.RequestInfo;

/**
 * Class BulkUploadServiceImpl
 * 
 * @author 390229
 * 
 */
@Service("bulkUploadService")
public class BulkUploadServiceImpl implements BulkUploadService {

	@Autowired
	BulkUploadComponent bulkUploadComponenet;

	@Autowired
	GeneraliEmailComponent generaliEmailComponent;

	@Autowired
	GeneraliPushNotificationsComponent pushNotificationsComponent;

	@Autowired
	private TaskExecutor bulkUploadThreadpool;

	@Autowired
	private TaskExecutor pushNotificationsthreadPool;

	/** pushNotififcation message */
	@Value("${le.platform.service.requestInfoJson}")
	private String requestInfoJson;

	/** Bulk upload Folder path */
	@Value("${le.platform.service.folderPath}")
	private String folderPath;
	
	public static final Logger LOGGER = LoggerFactory
			.getLogger(BulkUploadServiceImpl.class);
	/**
	 * validateAndAllocateLeads
	 * 
	 */
	@Override
	public String validateAndAllocateLeads(String json)
			throws BusinessException {

		String type = "";
		String fileName = "";
		JSONObject jsonObject;
		String response = "";
		String user = "";
		String emailId="";
		try {
			jsonObject = new JSONObject(json);
			LOGGER.info("Inputjson-----" + json);
			type = jsonObject.getString("type");
			fileName = jsonObject.getString("fileName");
			user = jsonObject.getString("user");
			emailId=jsonObject.getString("emailId");
			response=allocateLeadsandValidate(fileName,user,type,emailId);
			/*if(Constants.SUCCESS.equals(response)){
		
			}*/
		} catch (Exception e) {
			LOGGER.error("Error in received request with FileName " + fileName
					+ "Type: " + type + e.getMessage());
			throwBusinessException(true, e.getMessage());
		}
		return response;
	}
	
	/**
	 * allocateLeadsandValidate
	 * @param fileName
	 * @param userId
	 * @param type
	 * @return
	 */
	@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
	public String allocateLeadsandValidate(String fileName, String userId, String type, String emailId){
		String response="";
		if (fileName != null
				&& !"".equals("fileName")
				&& (GeneraliConstants.TYPE_AGENCY.equals(type) || GeneraliConstants.TYPE_BANCA
						.equals(type)) && fileName.endsWith(".xlsx")
				|| fileName.endsWith("xls")) {

			response = "Success";

			bulkUploadThreadpool.execute(new bulkUploadTask(type, fileName,
					userId,emailId));
		}
		return response;
	}
	/**
	 * Distinguishes between Agency and Banca Excel, save valid data, calls
	 * pushnotification
	 * 
	 * @param fileName
	 * @param type
	 * @param user
	 * @return
	 * @throws BusinessException
	 */
	public Map<String, String> validateAndAllocateLeadsExcel(String fileName,
			String type, String user) throws BusinessException {
		Map<String, String> excelSheetMap = null;
		try {
			Map<String, List<BulkUploadData>> agencyLeadMap = null;
			Map<String, Map<String, String>> codeLookUpData = new HashMap<String, Map<String, String>>();
			HashMap<String, Integer> pushNotificationMap = new HashMap<String, Integer>();
			codeLookUpData = bulkUploadComponenet.getLookUpData();
			if (GeneraliConstants.TYPE_AGENCY.equals(type)) {
				agencyLeadMap = bulkUploadComponenet.readAgency(fileName, user);
			} else if (GeneraliConstants.TYPE_BANCA.equals(type)) {
				agencyLeadMap = bulkUploadComponenet.readBanca(fileName, user);
			} else {
				LOGGER.error("Invalid File Type");
			}
			if (agencyLeadMap != null) {

				excelSheetMap = bulkUploadComponenet.saveBulkUploadAgencyLead(
						agencyLeadMap, type, pushNotificationMap,
						codeLookUpData);
			}
			final RequestInfo requestInfo = bulkUploadComponenet
					.createRequestInfo(requestInfoJson);
			String pushNoitificationMessage = "";
			if (GeneraliConstants.TYPE_AGENCY.equals(type)) {
				pushNoitificationMessage = GeneraliConstants.bulkUploadPushNotificationMessageAgency;
			} else {
				pushNoitificationMessage = GeneraliConstants.pushNotificationMessage;
			}
			pushNotificationsthreadPool.execute(new pushNotificationtask(
					pushNotificationMap, requestInfo, type));

		} catch (Exception e) {
			LOGGER.error("Error in received request with FileName " + fileName
					+ "Type: " + type, e);
			throwBusinessException(true, e.getMessage());
		}

		return excelSheetMap;
	}

	/**
	 * class bulkUploadTask
	 * 
	 * @author 390229
	 * 
	 */
	private class bulkUploadTask implements Runnable {

		String type;
		String fileName;
		String userName;
		String emailId;

		public bulkUploadTask(String type, String fileName, String user, String emailId) {
			super();

			this.type = type;
			this.fileName = fileName;
			this.userName = user;
			this.emailId=emailId;
		}

		@Override
		public void run() {
			Map<String, String> excelDataList = null;
			try {
				String status="";
				
				excelDataList = validateAndAllocateLeadsExcel(fileName, type,
						userName);
				//Retrieves emailId of the user from Glink service 
				//String emailId=bulkUploadComponenet.getUserEmailId(userName);
				LOGGER.debug("Created Excel map");
				String agentId = userName;

				//generaliEmailComponent.saveBulkUploadEmail(userName,type, type,excelDataList);
				LOGGER.info("Start to Send Email for User: "+ userName+ "Type: "+type+" Email : "+emailId);
				status=generaliEmailComponent.triggerBulkUploadEmail(userName,emailId,
						type, excelDataList);
				LOGGER.info("Send Email Completed for User: "+ userName+ "Type: "+type+" Email : "+emailId+ "With status : "+status);
				if(Constants.SUCCESS.equals(status)){
				bulkUploadComponenet.moveFiles(folderPath+fileName);
				}

				LOGGER.debug("sent mail");

			} catch (BusinessException e) {
				LOGGER.error("Error in sending mail for bulkupload " + type
						+ e.getMessage());
			} catch (ParseException e) {
				LOGGER.error("Error in saving mail for bulkupload " + type
						+ e.getMessage());
			}
			
		}

	}
	
	/**
	 * pushNotificationtask
	 * 
	 * @author 390229
	 * 
	 */
	private class pushNotificationtask implements Runnable {
		HashMap<String, Integer> agentMap;
		RequestInfo requestInfo;
		String type;
		String message="";

		public pushNotificationtask(HashMap<String, Integer> agentMap,
				RequestInfo requestInfo, String type) {
			super();
			this.requestInfo = requestInfo;
			this.agentMap = agentMap;
			this.type = type;
		}

		@Override
		public void run() {
			try {
				pushNotificationsComponent.pushNotifications(agentMap,
						requestInfo, type,message);
			} catch (Exception e) {
				LOGGER.error("Error in sending mail for PushNotification", e);

			}

		}
	}
}
