/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 301350
 * @version       : 0.1, Aug 08, 2013
 */
package com.cognizant.insurance.domain.agreement;

import java.math.BigDecimal;

import javax.persistence.Entity;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;

/**
 * The Class TopUp. Newly added for Illustration - 301350, 081Aug2013. Not part
 * of ACORD.
 */

@Entity
public class TopUp extends InformationModelObject {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8254159605449610889L;

     /** The top up from year. */
     private Integer topUpFromYear;

     /** The top up to year. */
     private Integer topUpToYear;
     
     /** The Top up premium. */
 	private BigDecimal topUpPremium;


     /**
      * Gets the top up from year.
      *
      * @return the top up from year
      */
     public Integer getTopUpFromYear() {
		return topUpFromYear;
	}

	/**
	 * Sets the top up from year.
	 *
	 * @param topUpFromYear the new top up from year
	 */
	public void setTopUpFromYear(Integer topUpFromYear) {
		this.topUpFromYear = topUpFromYear;
	}

	/**
	 * Gets the top up to year.
	 *
	 * @return the top up to year
	 */
	public Integer getTopUpToYear() {
		return topUpToYear;
	}

	/**
	 * Sets the top up to year.
	 *
	 * @param topUpToYear the new top up to year
	 */
	public void setTopUpToYear(Integer topUpToYear) {
		this.topUpToYear = topUpToYear;
	}

	public void setTopUpPremium(BigDecimal topUpPremium) {
		this.topUpPremium = topUpPremium;
	}

	public BigDecimal getTopUpPremium() {
		return topUpPremium;
	}

}
