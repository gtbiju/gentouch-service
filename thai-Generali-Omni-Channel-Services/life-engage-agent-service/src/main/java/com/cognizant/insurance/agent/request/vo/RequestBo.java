/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.agent.request.vo;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class class RequestBo.
 *
 * @author 345166
 * 
 * Class RequestBo - Created for EApp. Used to map RequestBo details.
 */
public class RequestBo {
	
	/** The request info. */
	private RequestInfo requestInfo;
    
	/** The Request payload. */
	private List<RequestPayload> requestPayload;

	/**
	 * Gets the request info.
	 *
	 * @return the request info
	 */
	public RequestInfo getRequestInfo() {
		return requestInfo;
	}

	/**
	 * Sets the request info.
	 *
	 * @param requestInfo the new request info
	 */
	public void setRequestInfo(RequestInfo requestInfo) {
		this.requestInfo = requestInfo;
	}

	/**
	 * Gets the request payload.
	 *
	 * @return the request payload
	 */
	public List<RequestPayload> getRequestPayload() {
		return requestPayload;
	}

	/**
	 * Sets the request payload.
	 *
	 * @param requestPayload the new request payload
	 */
	public void setRequestPayload(List<RequestPayload> requestPayload) {
		this.requestPayload = requestPayload;
	}
	


}
