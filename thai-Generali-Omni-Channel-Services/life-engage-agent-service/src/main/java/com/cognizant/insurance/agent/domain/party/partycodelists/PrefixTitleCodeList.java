/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partycodelists;

/**
 * Identifies a classification of PersonNames according to their prefix titles.
 * 
 * @author 301350
 * 
 * 
 */
public enum PrefixTitleCodeList {
    /**
     * Identifies a PersonName with prefix titles 'Mr'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Mr,
    /**
     * Identifies a PersonName with prefix titles 'Mrs'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Mrs,
    /**
     * Identifies a person name with prefix titles 'Dr'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Dr,
    /**
     * Identifies a person name with prefix titles 'Herr'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Herr,
    /**
     * Identifies a person name with prefix titles 'Ms'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Ms,
    /**
     * Identifies a person name with prefix titles 'Prof'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Prof,
    /**
     * Mademoiselle.
     * 
     * 
     * 
     */
    Miss
}
