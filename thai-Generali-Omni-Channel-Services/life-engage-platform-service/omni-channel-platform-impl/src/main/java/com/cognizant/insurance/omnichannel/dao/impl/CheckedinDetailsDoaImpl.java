/**
 * 
 */
package com.cognizant.insurance.omnichannel.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;

import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.dao.impl.DaoImpl;
import com.cognizant.insurance.omnichannel.dao.CheckedinDetailsDoa;
import com.cognizant.insurance.omnichannel.domain.CheckinData;
import com.cognizant.insurance.omnichannel.domain.CheckinData.Status;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;


/**
 * @author 397850
 *
 */
public class CheckedinDetailsDoaImpl extends DaoImpl implements CheckedinDetailsDoa {
	
    protected List<Object> findUsingQuery(Request request, String queryString, Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<Object>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

	@Override
	public CheckinData retriveCheckedinDetails(Request<CheckinData> checkedinRequest,String agentId,
			Status status) {
		String query;
		CheckinData data=new CheckinData();
	
		if(!agentId.isEmpty()){
			query="SELECT data from CheckinData data where data.agentId=?1 and data.status=?2";
			
			final List<Object> checkedinList = findUsingQuery(checkedinRequest, query, agentId,status);
			
			if (CollectionUtils.isNotEmpty(checkedinList)) {
				data=(CheckinData) checkedinList.get(0);
			}
		}
		return data;
	}

	@Override
	public void update(Request<CheckinData> checkedinRequest,CheckinData retirevedData) {
		String query;
		
		if(!retirevedData.getAgentId().isEmpty()){
			
			query="UPDATE CheckinData data SET data.status=?1,data.checkoutDateTime=?2 where data.agentId=?3 and data.id=?4";
			
			final int result = executeUpdate(checkedinRequest, query,retirevedData.getStatus(),retirevedData.getCheckoutDateTime(), retirevedData.getAgentId(),retirevedData.getId());
			
		}
		
	}

	@Override
	public List<CheckinData> retireveCheckinList(Request<CheckinData> checkedinRequest,
			Status status,Request<List<String>> statusRequest) {
		String query;
		final List<CheckinData> list = new ArrayList<CheckinData>();
		
		if(status != null){
			
			query="SELECT data from CheckinData data where data.status=?1 and data.agentId in ?2";
			
			final List<CheckinData> checkedinList = findByQuery(checkedinRequest,query,status,statusRequest.getType());
			
              if (CollectionUtils.isNotEmpty(checkedinList)) {
                  for (Object obj : checkedinList) {
                	  list.add((CheckinData) obj);
                  }
              }
			
		}
		return list;
	}

	@Override
	public void updateCheckinDetails(Request<CheckinData> checkedinRequest, Date date) {
		
		String query;	
			
			query="UPDATE CheckinData data SET data.status=?1,data.checkoutDateTime=?2 where data.status=?3";
			
			final int result = executeUpdate(checkedinRequest, query,Status.checkedout,date,Status.checkedin);
		
	}

}
