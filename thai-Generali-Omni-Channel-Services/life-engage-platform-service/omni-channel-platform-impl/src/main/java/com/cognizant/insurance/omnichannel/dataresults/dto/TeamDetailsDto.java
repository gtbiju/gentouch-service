package com.cognizant.insurance.omnichannel.dataresults.dto;

public class TeamDetailsDto {
    
    private String agentCode;
    
    private String agentName;
    
    private String teamNameInThai;
    
    private String teamNameInEng;
    
    public TeamDetailsDto(String agentCode, String agentName, String teamNameInThai, String teamNameInEng){
        this.agentCode = agentCode;
        this.agentName = agentName;
        this.teamNameInThai = teamNameInThai;
        this.teamNameInEng = teamNameInEng;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getTeamNameInThai() {
        return teamNameInThai;
    }

    public void setTeamNameInThai(String teamNameInThai) {
        this.teamNameInThai = teamNameInThai;
    }

    public String getTeamNameInEng() {
        return teamNameInEng;
    }

    public void setTeamNameInEng(String teamNameInEng) {
        this.teamNameInEng = teamNameInEng;
    }


}
