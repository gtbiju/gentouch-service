/**
 * 
 */
package com.cognizant.insurance.agent.domain.finance.paymentmethodsubtypes;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.insurance.agent.domain.finance.bankaccount.BankAccount;
import com.cognizant.insurance.agent.domain.finance.financialactivitysubtypes.PaymentMethod;

/**
 * This concept states a payment by check.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@DiscriminatorValue("CHECK")
public class CheckPayment extends PaymentMethod {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4463774284535318817L;

    /**
     * The check identification number.
     * 
     * 
     * 
     */
    private Integer checkIdentifier;

    /**
     * Name of the bank of the payer.
     * 
     * 
     * 
     */
    private String bankName;

    /**
     * Issue date of the check.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date issueDate;

    /**
     * This relation identifies the bank account in which to draw the check.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private BankAccount fromAccount;

    /**
     * Gets the checkIdentifier.
     * 
     * @return Returns the checkIdentifier.
     */
    public Integer getCheckIdentifier() {
        return checkIdentifier;
    }

    /**
     * Gets the bankName.
     * 
     * @return Returns the bankName.
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Gets the issueDate.
     * 
     * @return Returns the issueDate.
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * Gets the fromAccount.
     * 
     * @return Returns the fromAccount.
     */
    public BankAccount getFromAccount() {
        return fromAccount;
    }

    /**
     * Sets The checkIdentifier.
     * 
     * @param checkIdentifier
     *            The checkIdentifier to set.
     */
    public void setCheckIdentifier(final Integer checkIdentifier) {
        this.checkIdentifier = checkIdentifier;
    }

    /**
     * Sets The bankName.
     * 
     * @param bankName
     *            The bankName to set.
     */
    public void setBankName(final String bankName) {
        this.bankName = bankName;
    }

    /**
     * Sets The issueDate.
     * 
     * @param issueDate
     *            The issueDate to set.
     */
    public void setIssueDate(final Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * Sets The fromAccount.
     * 
     * @param fromAccount
     *            The fromAccount to set.
     */
    public void setFromAccount(final BankAccount fromAccount) {
        this.fromAccount = fromAccount;
    }

}
