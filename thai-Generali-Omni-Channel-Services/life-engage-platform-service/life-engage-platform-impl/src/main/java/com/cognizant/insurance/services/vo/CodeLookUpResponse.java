/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jan 23, 2014
 */
package com.cognizant.insurance.services.vo;

import java.util.List;

import com.cognizant.insurance.domain.codes.CodeLookUp;
import com.cognizant.insurance.response.vo.ResponseInfo;

/**
 * The Class class CodeLookUpResponse.
 * 
 * @author 300797
 */
public class CodeLookUpResponse {

    /** The response info. */
    private ResponseInfo responseInfo;

    /** The code look ups. */
    private List<CodeLookUp> codeLookUps;

    /**
     * Gets the response info.
     * 
     * @return the responseInfo
     */
    public final ResponseInfo getResponseInfo() {
        return responseInfo;
    }

    /**
     * Sets the response info.
     * 
     * @param responseInfo
     *            the responseInfo to set
     */
    public final void setResponseInfo(final ResponseInfo responseInfo) {
        this.responseInfo = responseInfo;
    }

    /**
     * Gets the code look ups.
     * 
     * @return the codeLookUps
     */
    public final List<CodeLookUp> getCodeLookUps() {
        return codeLookUps;
    }

    /**
     * Sets the code look ups.
     * 
     * @param codeLookUps
     *            the codeLookUps to set
     */
    public final void setCodeLookUps(final List<CodeLookUp> codeLookUps) {
        this.codeLookUps = codeLookUps;
    }
}
