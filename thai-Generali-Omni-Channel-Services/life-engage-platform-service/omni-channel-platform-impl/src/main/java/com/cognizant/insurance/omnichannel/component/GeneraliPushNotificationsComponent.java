package com.cognizant.insurance.omnichannel.component;


import java.util.HashMap;
import com.cognizant.insurance.request.vo.RequestInfo;

public interface GeneraliPushNotificationsComponent  {

    public void pushNotifications(HashMap<String, Integer> agentMap,
			RequestInfo requestInfo,String type, String message) ;

}
