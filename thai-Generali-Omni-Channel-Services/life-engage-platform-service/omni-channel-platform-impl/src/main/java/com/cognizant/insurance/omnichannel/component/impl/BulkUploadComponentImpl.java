package com.cognizant.insurance.omnichannel.component.impl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.Collator;
import java.text.ParseException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngageAuditComponent;
import com.cognizant.insurance.component.LifeEngageLMSComponent;
import com.cognizant.insurance.component.VendorAgentComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.component.BulkUploadComponent;
import com.cognizant.insurance.omnichannel.component.repository.BulkUploadRepository;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliLMSRepository;
import com.cognizant.insurance.omnichannel.utils.GeneraliExcelSheetUtil;
import com.cognizant.insurance.omnichannel.vo.AgentData;
import com.cognizant.insurance.omnichannel.vo.BankBranch;
import com.cognizant.insurance.omnichannel.vo.BulkUploadData;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;

/**
 * The Class class BulkUploadComponentImpl.
 * 
 * @author 390229
 */
public class BulkUploadComponentImpl implements BulkUploadComponent {

	@Autowired
	@Qualifier("bulkUploadMapping")
	private LifeEngageSmooksHolder saveLMSBulkUploadHolder;

	@Autowired
	@Qualifier("agencyProfileMapping")
	private LifeEngageSmooksHolder retriveAgentBulkUploadHolder;

	@Autowired
	@Qualifier("stateBankAgentMapping")
	private LifeEngageSmooksHolder retriveStateBankAgentBulkUploadHolder;

	@Autowired
	private GeneraliExcelSheetUtil generaliExcelSheetUtil;

	/** pushNotififcation message */
	@Value("${le.platform.service.requestInfoJson}")
	private String requestInfoJson;

	/** Bulk upload Folder path */
	@Value("${le.platform.service.folderPath}")
	private String folderPath;

	/** The Constant REQUEST_INFO. */
	private static final String REQUEST_INFO = "RequestInfo";

	/** The Constant TRANSACTIONS. */
	private static final String TRANSACTIONS = "Transactions";

	@Autowired
	LifeEngageLMSComponent lifeEngageLMSComponent;

	@Autowired
	GeneraliLMSRepository lmsRepository;

	/** The vendor agent component. */
	@Autowired
	private VendorAgentComponent vendorAgentComponent;

	/** The vendor agent component. */
	@Autowired
	private BulkUploadRepository bulkUploadRepository;

	/** The life engage audit component. */
	@Autowired
	private LifeEngageAuditComponent lifeEngageAuditComponent;

	public static final Logger LOGGER = LoggerFactory
			.getLogger(BulkUploadComponentImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cognizant.insurance.omnichannel.component.BulkUploadComponenet#readAgency
	 * (java.lang.String)
	 * 
	 * 
	 * Method to read agency Excel Sheet
	 */
	public Map<String, List<BulkUploadData>> readAgency(String fileName,
			String user) {
		BulkUploadData customerData = new BulkUploadData();

		Map<String, List<BulkUploadData>> customerMap = new HashMap<String, List<BulkUploadData>>();
		List<BulkUploadData> customerList = new ArrayList<BulkUploadData>();
		List<BulkUploadData> vFailCustomerList = new ArrayList<BulkUploadData>();
		Set<String> agentSet = new TreeSet<String>();
		int count = 0;

		try {
			Sheet sheet = generaliExcelSheetUtil.retrieveFile(folderPath
					+ fileName);
			LOGGER.debug("fOLDERPATH" + folderPath);
			LOGGER.debug("FILEnAME" + fileName);
			Iterator<Row> rows = sheet.rowIterator();
			Boolean isNextRawExist = true;

			while (rows.hasNext()) {
				count++;
				customerData = new BulkUploadData();
				Row myRow = (Row) rows.next();
				if (isNextRawExist == false) {
					break;
				}
				isNextRawExist = false;
				if (myRow.getRowNum() == 0) {
					isNextRawExist = true;
					continue;
				}

				if (myRow.getRowNum() > 0) {
					Iterator<Cell> cellIter = myRow.cellIterator();

					while (cellIter.hasNext()) {

						// To Find blank cell
						Cell myCell = (Cell) cellIter.next();

						if (myCell.getCellType() != Cell.CELL_TYPE_BLANK) {
							if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {
								if (generaliExcelSheetUtil
										.returnStringCell(myCell) != null) {
									isNextRawExist = true;
								}
							} else if (generaliExcelSheetUtil
									.returnDateCell(myCell) != null) {
								isNextRawExist = true;
							}

						}

						if (myCell.getColumnIndex() == 0) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String name = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setCustomerFirstName(name);
							
						} else if (myCell.getColumnIndex() == 1) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String customerLastName = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setCustomerLastName(customerLastName);
						
						} else if (myCell.getColumnIndex() == 2) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String gender = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setGender(gender);
						}

						else if (myCell.getColumnIndex() == 3) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String phoneNumber = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setMobileNumber(phoneNumber);
						}

						else if (myCell.getColumnIndex() == 4) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String nationalId = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setNationalID(nationalId);
						}

						else if (myCell.getColumnIndex() == 5) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String emailAddress = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setEmailAddress(emailAddress);

						} else if (myCell.getColumnIndex() == 6) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String address = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setAddress(address);

						} else if (myCell.getColumnIndex() == 7) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String state = generaliExcelSheetUtil
									.returnStringCell(myCell);
							if (state != null
									&& !GeneraliConstants.JSON_EMPTY
											.equals(state)) {
								customerData
										.setStateCode(generaliExcelSheetUtil
												.getCode(state));
								customerData
										.setStateName(generaliExcelSheetUtil
												.getValue(state));
							}
							customerData.setState(state);

						} else if (myCell.getColumnIndex() == 8) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String nationality = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setNationality(nationality);

						} else if (myCell.getColumnIndex() == 9) {
							Date dob = generaliExcelSheetUtil
									.returnDateCell(myCell);
							String dobString = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setDob(dob);
							customerData.setDobString(dobString);

						} else if (myCell.getColumnIndex() == 10) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String potential = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setPotential(potential);

						} else if (myCell.getColumnIndex() == 11) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String source = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setSource(source);

						} else if (myCell.getColumnIndex() == 12) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String occupation = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setOccupation(occupation);

						} else if (myCell.getColumnIndex() == 13) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String action = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setAction(action);

						} else if (myCell.getColumnIndex() == 14) {
							Date nextmeetingDate = generaliExcelSheetUtil
									.returnDateCell(myCell);
							customerData.setNextmeetingDate(nextmeetingDate);
							String nextMeetingDateString = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData
									.setNextmeetingDateString(nextMeetingDateString);

						} else if (myCell.getColumnIndex() == 15) {
							String time = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setNextMeetingTime(time);

						} else if (myCell.getColumnIndex() == 16) {
							String agentCode = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setAgentCode(agentCode);
							if (agentCode != null) {
								if (agentCode.contains("\n")) {
									agentCode = agentCode.replaceAll("\n",
											GeneraliConstants.JSON_EMPTY);
								}
								agentSet.add(agentCode);

							}
						}

						else if (myCell.getColumnIndex() > 16) {
							break;
						}

					}// while

				}// IF

				Boolean isCustomerValid = generaliExcelSheetUtil
						.validateAgencyCustomerData(customerData);
				if (isCustomerValid) {
					customerData.setUser(user);
					customerData.setChannelType(GeneraliConstants.BULK_AGENCY);
					customerList.add(customerData);
				} else {

					vFailCustomerList.add(customerData);
				}
			}
			if (vFailCustomerList != null && vFailCustomerList.size() > 0
					&& !isNextRawExist) {
				int faliedListSize = vFailCustomerList.size();
				vFailCustomerList.remove(faliedListSize - 1);
			}
			customerMap.put(GeneraliConstants.VALID_SUCCESS, customerList);
			customerMap.put(GeneraliConstants.INVALID, vFailCustomerList);

			validateIsAgentCodeValid(customerMap, agentSet);

		} catch (Exception e) {
			LOGGER.error("Error in reading agency sheet" + e.getMessage(), e);
		}

		return customerMap;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cognizant.insurance.omnichannel.component.BulkUploadComponenet#
	 * saveBulkUploadAgencyLead(java.util.Map, java.lang.String, java.util.Map)
	 * 
	 * 
	 * Method to save the lead data.
	 */

	public Map<String, String> saveBulkUploadAgencyLead(
			Map<String, List<BulkUploadData>> leadDataMap, String type,
			Map<String, Integer> pushNotificationMap,
			Map<String, Map<String, String>> lookUpData) {
		List<BulkUploadData> agencyLeadDataList;

		if (leadDataMap.containsKey(GeneraliConstants.VALID_SUCCESS)) {
			agencyLeadDataList = leadDataMap
					.get(GeneraliConstants.VALID_SUCCESS);
			if (GeneraliConstants.TYPE_BANCA.equals(type)) {

			}
			if (agencyLeadDataList != null) {
				for (BulkUploadData agencyCustomerData : agencyLeadDataList) {
					String response = null;

					try {
						bulkUploadRepository.processData(agencyCustomerData,
								lookUpData);
						response = saveLeadData(agencyCustomerData);
						addtoCustomerMap(agencyCustomerData, leadDataMap,
								response, pushNotificationMap);

					} catch (ParseException e) {
						LOGGER.error(
								"Error in parsing Excel data" + " Agent: "
										+ agencyCustomerData.getAgentCode()
										+ "Mobile number: "
										+ agencyCustomerData.getMobileNumber(),
								e);
						e.printStackTrace();
					} catch (Exception e) {

						LOGGER.error("Error in Saving data", e);
					}

				}
			}
		}
		Map<String, String> leadExcelsList = null;
		try {
			leadExcelsList = generaliExcelSheetUtil.generateExcelForMail(
					leadDataMap, type);
		} catch (IOException e) {
			LOGGER.error("Error in generating excels for Mail", e);
		}
		return leadExcelsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cognizant.insurance.omnichannel.component.BulkUploadComponenet#readBanca
	 * (java.lang.String, java.lang.String)
	 * 
	 * Method to read Banca Excel sheet
	 * 
	 * user-- Defines Channel
	 */
	@Override
	public Map<String, List<BulkUploadData>> readBanca(String fileName,
			String user) {

		BulkUploadData customerData = new BulkUploadData();
		Map<String, List<BulkUploadData>> customerMap = new HashMap<String, List<BulkUploadData>>();
		List<BulkUploadData> customerList = new ArrayList<BulkUploadData>();
		List<BulkUploadData> vFailCustomerList = new ArrayList<BulkUploadData>();
		Set<String> branchCodeSet = new TreeSet<String>();
		Set<BankBranch> bankBranchSet = new TreeSet<BankBranch>();
		int count = 0;

		try {
			Sheet sheet = generaliExcelSheetUtil.retrieveFile(folderPath
					+ fileName);
			Iterator<Row> rows = sheet.rowIterator();

			Boolean isNextRowExist = true;

			while (rows.hasNext()) {
				count++;
				customerData = new BulkUploadData();
				BankBranch bankBranch = new BankBranch();
				Row myRow = (Row) rows.next();
				if (isNextRowExist == false) {
					break;
				}
				isNextRowExist = false;
				if (myRow.getRowNum() == 0) {
					isNextRowExist = true;
					continue;
				}

				if (myRow.getRowNum() > 0) {
					Iterator<Cell> cellIter = myRow.cellIterator();

					while (cellIter.hasNext()) {

						Cell myCell = (Cell) cellIter.next();

						if (myCell.getCellType() != Cell.CELL_TYPE_BLANK) {
							if (myCell.getCellType() == Cell.CELL_TYPE_STRING) {
								if (generaliExcelSheetUtil
										.returnStringCell(myCell) != null) {
									isNextRowExist = true;
								}
							} else if (generaliExcelSheetUtil
									.returnDateCell(myCell) != null) {
								isNextRowExist = true;
							}
						}

						if (myCell.getColumnIndex() == 0) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String name = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setCustomerFirstName(name);

						} 
						else if (myCell.getColumnIndex() == 1) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String customerLastName = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setCustomerLastName(customerLastName);

						} else if (myCell.getColumnIndex() == 2) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String gender = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setGender(gender);

						} else if (myCell.getColumnIndex() == 3) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String nationalId = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setNationalID(nationalId);

						} else if (myCell.getColumnIndex() == 4) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String phoneNumber = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setMobileNumber(phoneNumber);

						} else if (myCell.getColumnIndex() == 5) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String emailAddress = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setEmailAddress(emailAddress);

						} else if (myCell.getColumnIndex() == 6) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String address = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setAddress(address);

						} else if (myCell.getColumnIndex() == 7) {
							String dobString = generaliExcelSheetUtil
									.returnStringCell(myCell);
							Date dob = generaliExcelSheetUtil
									.returnDateCell(myCell);
							customerData.setDob(dob);
							customerData.setDobString(dobString);

						} else if (myCell.getColumnIndex() == 8) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String potential = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setPotential(potential);

						} else if (myCell.getColumnIndex() == 9) {
							String state = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setState(state);
							if (state != null) {
								customerData
										.setStateCode(generaliExcelSheetUtil
												.getCode(state));
								customerData
										.setStateName(generaliExcelSheetUtil
												.getValue(state));
							}

						} else if (myCell.getColumnIndex() == 10) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String nationality = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setNationality(nationality);

						} else if (myCell.getColumnIndex() == 11) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String annualIncome = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setAnnualIncome(annualIncome);

						} else if (myCell.getColumnIndex() == 12) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String need = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setNeed(need);

						} else if (myCell.getColumnIndex() == 13) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String source = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setSource(source);

						} else if (myCell.getColumnIndex() == 14) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String occupation = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setOccupation(occupation);

						} else if (myCell.getColumnIndex() == 15) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String action = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setAction(action);

						} else if (myCell.getColumnIndex() == 16) {
							String nextMeetingDateString = generaliExcelSheetUtil
									.returnStringCell(myCell);
							Date nextMeetingDate = generaliExcelSheetUtil
									.returnDateCell(myCell);
							customerData
									.setNextmeetingDateString(nextMeetingDateString);
							customerData.setNextmeetingDate(nextMeetingDate);

						} else if (myCell.getColumnIndex() == 17) {
							String time = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setNextMeetingTime(time);

						} else if (myCell.getColumnIndex() == 18) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String branch = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setBranch(branch);
							if (branch != null) {
								String branchCode = generaliExcelSheetUtil
										.getCode(branch);
								branchCodeSet.add(branchCode);
								customerData.setBranchCode(branchCode);
							}

						} else if (myCell.getColumnIndex() == 19) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String bank = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setBank(bank);
							if (bank != null) {
								customerData.setBankCode(generaliExcelSheetUtil
										.getCode(bank));
								customerData.setBankName(generaliExcelSheetUtil
										.getValue(bank));
							}

						} else if (myCell.getColumnIndex() == 20) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String refName = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setRefName(refName);

						} else if (myCell.getColumnIndex() == 21) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							String refMobileNumber = generaliExcelSheetUtil
									.returnStringCell(myCell);
							customerData.setRefMobileNumber(refMobileNumber);

						} else if (myCell.getColumnIndex() > 21) {
							break;
						}
					}
				}
				// Field validations
				Boolean isCustomerValid = generaliExcelSheetUtil
						.validateBancaCustomerData(customerData);
				if (isCustomerValid) {
					if (customerData.getBranch() == null
							|| ("").equals(customerData.getBranch())) {

						// Setting the bank and state code if branch id is not
						// available
						bankBranch.setBank(customerData.getBankCode());
						bankBranch.setState(customerData.getStateCode());
						bankBranch.setBankOrStateOrBranchCode(customerData
								.getStateCode()
								+ "-"
								+ customerData.getBankCode());
						bankBranchSet.add(bankBranch);
						// Setting user type this is need to avoid duplicate
						// check.

					}
					customerData.setUser(user);
					customerData.setChannelType(GeneraliConstants.BULK_BANCA);
					customerList.add(customerData);
				} else {
					vFailCustomerList.add(customerData);
				}
			}
			if (vFailCustomerList != null && vFailCustomerList.size() > 0
					&& !isNextRowExist) {
				int faliedListSize = vFailCustomerList.size();
				vFailCustomerList.remove(faliedListSize - 1);
			}

			// Validations Successful Rows
			customerMap.put(GeneraliConstants.VALID_SUCCESS, customerList);
			// Validation Failed Rows
			customerMap.put(GeneraliConstants.INVALID, vFailCustomerList);

			Map<String, List<AgentData>> reportingAgetnsMap = new HashMap<String, List<AgentData>>();

			Map<String, List<AgentData>> bankBranchMap = new HashMap<String, List<AgentData>>();

			if (branchCodeSet != null && branchCodeSet.size() > 0) {
				// Calls GLink service to retrieve all reporting agents of the
				// Unique branch codes entered in the excel.
				reportingAgetnsMap = getReportingAgetnsMap(branchCodeSet);
			}
			if (bankBranchSet != null && bankBranchSet.size() > 0) {
				// Calls Glink service to retrieve the reporting agents of
				// Unique Bank State Combinations
				bankBranchMap = getBankBranchReportingAgetnsMap(bankBranchSet);
			}
			RequestInfo requestInfo = createRequestInfo(requestInfoJson);

			// Rule to allocate leads to appropriate agent.
			leadAllocationRule(customerMap, reportingAgetnsMap, bankBranchMap,
					requestInfo);
		} catch (BusinessException e) {

			// If error in connecting to Glink. All remaining valid rows will be
			// saved as allocation failed.
			LOGGER.error("Error in Conecting to VendoService" + e.getMessage(),
					e);
			customerMap.put(GeneraliConstants.AGENT_FAILED, customerList);
			customerMap.put(GeneraliConstants.VALID_SUCCESS, null);

		} catch (ParseException e) {
			// If error in the response structure of Glink All remaining valid
			// rows will be saved as allocation failed. And nothing will be
			// saved to data base.
			LOGGER.error(
					"Error in Parsing to Vendor Response" + e.getMessage(), e);
			customerMap.put(GeneraliConstants.AGENT_FAILED, customerList);
			customerMap.put(GeneraliConstants.VALID_SUCCESS, null);

		} catch (Exception e) {
			LOGGER.error("Error in Reading Banca Excel" + e.getMessage(), e);
			customerMap.put(GeneraliConstants.AGENT_FAILED, customerList);
			customerMap.put(GeneraliConstants.VALID_SUCCESS, null);
			LOGGER.error("Error in Reading Banca Excel" + e.getMessage(), e);
		}
		return customerMap;
	}

	/**
	 * Method to allocate no of leads assigned to each agent.
	 * 
	 * @param pushNotificationMap
	 * @param jsonObject
	 * @param response
	 */
	public void addtoPushNotificationMap(
			Map<String, Integer> pushNotificationMap, String agentId) {
		try {
			Integer count = 0;
			if (pushNotificationMap.containsKey(agentId)) {
				count = (Integer) pushNotificationMap.get(agentId);
				if (count != null) {
					count++;
				} else {
					count = 1;
				}
				pushNotificationMap.put(agentId, count);
			} else {
				pushNotificationMap.put(agentId, 1);
			}

		} catch (NoSuchElementException e) {

			throw new SystemException(e.getMessage());
		}
	}

	/**
	 * Method to allocate no of leads assigned to each agent.
	 * 
	 * @param agentMap
	 * @param jsonObject
	 * @param response
	 */
	public void addtoCustomerMap(BulkUploadData leadData,
			Map<String, List<BulkUploadData>> leadDataMap, String response,
			Map<String, Integer> pushNotificationMap) {

		List<BulkUploadData> agencyInvalidLeadDataList = leadDataMap
				.get(GeneraliConstants.INVALID);
		List<BulkUploadData> savedAgencyLeadDataList = new ArrayList<BulkUploadData>();

		if (leadDataMap.containsKey(GeneraliConstants.VALID)) {
			savedAgencyLeadDataList.addAll(leadDataMap
					.get(GeneraliConstants.VALID));
		}
		try {
			JSONObject responseObj = new JSONObject(response);
			JSONObject statusObj = responseObj.getJSONObject("StatusData");
			String status = statusObj.getString("Status");
			String statusMessage = statusObj.getString("StatusMessage");

			if (LifeEngageComponentHelper.SUCCESS.equals(status)) {
				savedAgencyLeadDataList.add(leadData);
				leadDataMap.put(GeneraliConstants.VALID,
						savedAgencyLeadDataList);
				addtoPushNotificationMap(pushNotificationMap,
						leadData.getAgentCode());
			} else {
				if ("Duplicate Lead : ".equals(statusMessage)) {
					statusMessage = GeneraliConstants.DUPLICATE_LEAD;
					leadData.setCustomerNameInValid(true);

				} else {
					statusMessage = GeneraliConstants.INVALID_TEMPLATE;
				}
				leadData.setRemarks(statusMessage);
				leadData.setStatus(false);
				agencyInvalidLeadDataList.add(leadData);
				leadDataMap.put(GeneraliConstants.INVALID,
						agencyInvalidLeadDataList);
			}

		} catch (NoSuchElementException e) {
			LOGGER.error("Error in parsing RequestInfo details", e);
			leadData.setRemarks("System Error");
			leadData.setStatus(false);
			agencyInvalidLeadDataList.add(leadData);
			leadDataMap.put(GeneraliConstants.INVALID,
					agencyInvalidLeadDataList);

		} catch (ParseException e) {
			leadData.setRemarks("System Error");
			agencyInvalidLeadDataList.add(leadData);
			leadDataMap.put(GeneraliConstants.INVALID,
					agencyInvalidLeadDataList);
			LOGGER.error("Error in parsing RequestInfo details", e);

		} catch (Exception e) {
			leadData.setRemarks("System Error");
			agencyInvalidLeadDataList.add(leadData);
			leadDataMap.put(GeneraliConstants.INVALID,
					agencyInvalidLeadDataList);
			LOGGER.error("Error in parsing RequestInfo details", e);

		}
	}

	/**
	 * 
	 * Method to get Agent Names
	 * 
	 */
	public List<AgentData> getFirstNameAgent(List<String> agentList)
			throws ParseException, BusinessException {
		String vendorResponse = null;
		String transactionJson = null;
		List<AgentData> agentDataList = new ArrayList<AgentData>();

		transactionJson = retriveAgentBulkUploadHolder.parseBO(agentList);
		if (transactionJson != null) {
			JSONObject jsonRequestPayloadObj = new JSONObject(transactionJson);
			JSONObject jsonRequestObj = new JSONObject(requestInfoJson);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestObj);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);

			vendorResponse = vendorAgentComponent.retrieveAgentProfileByCode(
					requestInfo, jsonTransactionArray);

			JSONObject vendorObj = new JSONObject(vendorResponse);
			JSONObject vendorResponseObj = vendorObj.getJSONObject("Response");
			JSONObject vendorResponsePayloadObj = vendorResponseObj
					.getJSONObject("ResponsePayLoad");
			final JSONArray vendorResponseTransactionArray = vendorResponsePayloadObj
					.getJSONArray(TRANSACTIONS);
			for (int i = 0; i < vendorResponseTransactionArray.length(); i++) {
				final JSONObject jsonObj = vendorResponseTransactionArray
						.getJSONObject(i);
				AgentData agentData = new AgentData();
				JSONObject transactionDataObj = jsonObj
						.getJSONObject("TransactionData");
				JSONObject statusObj = jsonObj.getJSONObject("StatusData");
				String status = statusObj.getString("Status");
				if (LifeEngageComponentHelper.SUCCESS.equals(status)) {
					if (transactionDataObj.has("AgentDetails")) {
						JSONObject agentDetailsDataObj = transactionDataObj
								.getJSONObject("AgentDetails");
						if (agentDetailsDataObj.has("agentName")) {
							agentData.setAgentName(agentDetailsDataObj
									.getString("agentName"));
							agentData.setAgentCode(agentDetailsDataObj
									.getString("agentCode"));
						}
					}
					agentDataList.add(agentData);
				}
			}
			Collections.sort(agentDataList, new Comparator<AgentData>() {
				public int compare(AgentData o1, AgentData o2) {

					String s_1 = o1.getAgentName();
					String s_2 = o2.getAgentName();

					return s_1.compareTo(s_2);

				}
			});
		}

		AgentData agentDataOfFirstName = null;
		if (agentDataList != null && agentDataList.size() > 0) {
			agentDataOfFirstName = agentDataList.get(0);
		}
		return agentDataList;

	}

	/**
	 * Validate if agent code is valid or not
	 * 
	 * @param leadDataMap
	 * @param agentSet
	 * @throws BusinessException
	 */
	public void validateIsAgentCodeValid(
			Map<String, List<BulkUploadData>> leadDataMap, Set<String> agentSet)
			throws BusinessException {
		List<BulkUploadData> fieldValidationSuccessList = leadDataMap
				.get(GeneraliConstants.VALID_SUCCESS);

		List<BulkUploadData> filedValidationFailedData = leadDataMap
				.get(GeneraliConstants.INVALID);
		String vendorResponse = null;
		String transactionJson = null;
		List<String> list = new ArrayList<String>();
		list.addAll(agentSet);
		List<BulkUploadData> validAgentBulkUploadData = new ArrayList<BulkUploadData>();
		List<BulkUploadData> invalidAgentBulkUploadData = new ArrayList<BulkUploadData>();
		try {
			transactionJson = retriveAgentBulkUploadHolder.parseBO(list);
			if (transactionJson != null) {
				JSONObject jsonRequestPayloadObj = new JSONObject(
						transactionJson);
				JSONObject jsonRequestObj = new JSONObject(requestInfoJson);
				final RequestInfo requestInfo = LifeEngageSyncServiceHelper
						.parseRequestInfo(jsonRequestObj);
				final JSONArray jsonTransactionArray = jsonRequestPayloadObj
						.getJSONArray(TRANSACTIONS);

				vendorResponse = vendorAgentComponent
						.retrieveAgentProfileByCode(requestInfo,
								jsonTransactionArray);

				JSONObject vendorObj = new JSONObject(vendorResponse);
				JSONObject vendorResponseObj = vendorObj
						.getJSONObject("Response");
				JSONObject vendorResponsePayloadObj = vendorResponseObj
						.getJSONObject("ResponsePayLoad");
				final JSONArray vendorResponseTransactionArray = vendorResponsePayloadObj
						.getJSONArray(TRANSACTIONS);
				List<String> inActiveAgents = new ArrayList<String>();
				List<String> activeAgents = new ArrayList<String>();
				List<String> otherChanneAgents = new ArrayList<String>();
				for (int i = 0; i < vendorResponseTransactionArray.length(); i++) {
					final JSONObject jsonObj = vendorResponseTransactionArray
							.getJSONObject(i);

					JSONObject statusObj = jsonObj.getJSONObject("StatusData");
					String status = statusObj.getString("Status");
					JSONObject transactionDataObj = jsonObj
							.getJSONObject("TransactionData");
					String agentId = "";
					String agentStatus = "";
					String agentType = "";
					if (transactionDataObj.has("AgentDetails")) {
						JSONObject agentDetailsDataObj = transactionDataObj
								.getJSONObject("AgentDetails");
						if (agentDetailsDataObj.has("status")) {
							agentStatus = agentDetailsDataObj
									.getString("status");
						}
						if (agentDetailsDataObj.has("agentType")) {
							agentType = agentDetailsDataObj
									.getString("agentType");
						}
					}
					if (jsonObj.has(Constants.KEY11)) {
						agentId = jsonObj.getString(Constants.KEY11);
					}

					if (LifeEngageComponentHelper.SUCCESS.equals(status)) {
						if (GeneraliConstants.STATUS_ACTIVE.equals(agentStatus)) {
							if (GeneraliConstants.TYPE_AGENCY.equals(agentType)) {
								activeAgents.add(agentId);
							} else {
								otherChanneAgents.add(agentId);
							}
						} else {
							inActiveAgents.add(agentId);

						}
					}

				}
				for (BulkUploadData bulkUploadData : fieldValidationSuccessList) {

					String agentcode = bulkUploadData.getAgentCode();
					if (activeAgents.contains(agentcode)) {
						validAgentBulkUploadData.add(bulkUploadData);

					} else if (inActiveAgents.contains(agentcode)) {
						bulkUploadData
								.setRemarks(GeneraliConstants.SUSPENDED_TERMINATED_AGENT);
						invalidAgentBulkUploadData.add(bulkUploadData);

					} else if (otherChanneAgents.contains(bulkUploadData
							.getAgentCode())) {
						bulkUploadData
								.setRemarks(GeneraliConstants.OTHER_CHANNEL_AGENTS);
						invalidAgentBulkUploadData.add(bulkUploadData);
					} else {
						bulkUploadData
								.setRemarks(GeneraliConstants.INVALID_AGENTCODE);
						invalidAgentBulkUploadData.add(bulkUploadData);
					}
				}

				for (BulkUploadData bulkUploadData : filedValidationFailedData) {

					String agentcode = bulkUploadData.getAgentCode();

					if (inActiveAgents.contains(agentcode)) {
						bulkUploadData
								.setRemarks(generaliExcelSheetUtil.attachErrorMessages(
										bulkUploadData.getRemarks(),
										GeneraliConstants.SUSPENDED_TERMINATED_AGENT));
						bulkUploadData.setIsAgentCodeInValid(true);

					} else if (otherChanneAgents.contains(bulkUploadData
							.getAgentCode())) {
						bulkUploadData
								.setRemarks(generaliExcelSheetUtil.attachErrorMessages(
										bulkUploadData.getRemarks(),
										GeneraliConstants.OTHER_CHANNEL_AGENTS));
						bulkUploadData.setIsAgentCodeInValid(true);

					} else if (!activeAgents.contains(agentcode)) {
						bulkUploadData.setRemarks(generaliExcelSheetUtil
								.attachErrorMessages(
										bulkUploadData.getRemarks(),
										GeneraliConstants.INVALID_AGENTCODE));
						bulkUploadData.setIsAgentCodeInValid(true);
					}

				}

			}

		} catch (BusinessException e) {
			LOGGER.error(
					"PARSE ERROR : Parse exception : ## ERR ##"
							+ e.getMessage(), e);
			invalidAgentBulkUploadData.addAll(fieldValidationSuccessList);
		} catch (ParseException e) {
			LOGGER.error(
					"PARSE ERROR : Parse exception : ## ERR ##"
							+ e.getMessage(), e);
		} catch (Exception e) {
			LOGGER.error("ERROR :  exception : ## ERR ##" + e.getMessage(), e);
		}

		leadDataMap.put(GeneraliConstants.VALID_SUCCESS,
				validAgentBulkUploadData);
		leadDataMap.put(GeneraliConstants.AGENT_FAILED,
				invalidAgentBulkUploadData);

	}

	/**
	 * Getting reporting agents
	 * 
	 * @param branchCodeSet
	 * @return
	 * @throws ParseException
	 * @throws BusinessException
	 */
	public Map<String, List<AgentData>> getReportingAgetnsMap(
			Set<String> branchCodeSet) throws ParseException, BusinessException {
		String transactionJson = null;
		Map<String, List<AgentData>> branchIdRepotingagentsMap = new HashMap<String, List<AgentData>>();
		String vendorResponse = null;
		String agentId = "";
		List<String> list = new ArrayList<String>();
		list.addAll(branchCodeSet);
		try {
			transactionJson = retriveAgentBulkUploadHolder.parseBO(list);
			JSONObject jsonRequestPayloadObj = new JSONObject(transactionJson);
			JSONObject jsonRequestObj = new JSONObject(requestInfoJson);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestObj);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			vendorResponse = vendorAgentComponent.retrieveAgentProfileByCode(
					requestInfo, jsonTransactionArray);
			JSONObject vendorObj = new JSONObject(vendorResponse);
			JSONObject vendorResponseObj = vendorObj.getJSONObject("Response");
			JSONObject vendorResponsePayloadObj = vendorResponseObj
					.getJSONObject("ResponsePayLoad");
			final JSONArray vendorResponseTransactionArray = vendorResponsePayloadObj
					.getJSONArray(TRANSACTIONS);
			String agentStatus = "";
			JSONArray reportingAgents = null;
			for (int i = 0; i < vendorResponseTransactionArray.length(); i++) {
				final JSONObject jsonObj = vendorResponseTransactionArray
						.getJSONObject(i);

				JSONObject statusObj = jsonObj.getJSONObject("StatusData");
				String status = statusObj.getString("Status");
				if (LifeEngageComponentHelper.SUCCESS.equals(status)) {
					JSONObject transactionDataObj = jsonObj
							.getJSONObject("TransactionData");
					if (transactionDataObj.has("AgentDetails")) {
						JSONObject agentDetailsDataObj = transactionDataObj
								.getJSONObject("AgentDetails");
						if (agentDetailsDataObj.has("status")) {
							agentStatus = agentDetailsDataObj
									.getString("status");
						}
						if (LifeEngageComponentHelper.SUCCESS.equals(status)
								&& GeneraliConstants.STATUS_ACTIVE
										.equals(agentStatus)) {
							reportingAgents = agentDetailsDataObj
									.getJSONArray("reportingAgents");
						}
					}
					if (jsonObj.has(Constants.KEY11)) {
						agentId = jsonObj.getString(Constants.KEY11);
					}

					List<AgentData> repotingagents = new ArrayList<AgentData>();
					List<String> agentCodeList = new ArrayList<String>();
					if (reportingAgents != null) {

						for (int i1 = 0; i1 < reportingAgents.length(); i1++) {
							if (reportingAgents.get(i1) != null) {
								JSONObject agentJsonObj = reportingAgents
										.getJSONObject(i1);
								AgentData agentData = new AgentData();
								agentData.setAgentCode(agentJsonObj
										.getString("agentCode"));
								agentData.setAgentName(agentJsonObj
										.getString("agentName"));
								agentCodeList.add(agentJsonObj
										.getString("agentCode"));
								repotingagents.add(agentData);
							}
						}
					}

					// Trying to get the active reporting agents;
					if (agentCodeList != null && agentCodeList.size() > 0) {
						String vendorResponseString = getVendorResponse(
								agentCodeList, retriveAgentBulkUploadHolder);

						List<String> activeAgentCodes = getActiveReportingAgents(
								vendorResponseString,
								GeneraliConstants.TYPE_BANCA_AGENT);
						List<AgentData> activeReportingAgents = new ArrayList<AgentData>();

						for (AgentData agentData : repotingagents) {
							String agentCode = agentData.getAgentCode();
							if (activeAgentCodes.contains(agentCode)) {
								activeReportingAgents.add(agentData);
							}
						}
						if (activeReportingAgents.size() > 0) {
							branchIdRepotingagentsMap.put(agentId,
									activeReportingAgents);
						}
					}

				}
			}
		} catch (BusinessException e) {
			LOGGER.error(
					"PARSE ERROR : Parse exception : ## ERR ##"
							+ e.getMessage(), e);

		} catch (ParseException e) {
			LOGGER.error(
					"PARSE ERROR : Parse exception : ## ERR ##"
							+ e.getMessage(), e);
		} catch (Exception e) {
			LOGGER.error("ERROR :  exception : ## ERR ##" + e.getMessage(), e);
		}
		return branchIdRepotingagentsMap;
	}

	/**
	 * Getting reporting agents for Bank and Branch input
	 * 
	 * @param bankBranchSet
	 * @return
	 * @throws ParseException
	 * @throws BusinessException
	 */
	public Map<String, List<AgentData>> getBankBranchReportingAgetnsMap(
			Set<BankBranch> bankBranchSet) throws ParseException,
			BusinessException {
		String transactionJson = null;
		Map<String, List<AgentData>> branchIdRepotingagentsMap = new HashMap<String, List<AgentData>>();
		String vendorResponse = null;
		String agentId = "";
		List<BankBranch> list = new ArrayList<BankBranch>();
		list.addAll(bankBranchSet);

		transactionJson = retriveStateBankAgentBulkUploadHolder.parseBO(list);
		JSONObject jsonRequestPayloadObj = new JSONObject(transactionJson);
		JSONObject jsonRequestObj = new JSONObject(requestInfoJson);
		final RequestInfo requestInfo = LifeEngageSyncServiceHelper
				.parseRequestInfo(jsonRequestObj);
		final JSONArray jsonTransactionArray = jsonRequestPayloadObj
				.getJSONArray(TRANSACTIONS);
		vendorResponse = vendorAgentComponent.retrieveAgentsbyBranchCode(
				requestInfo, jsonTransactionArray);
		JSONObject vendorObj = new JSONObject(vendorResponse);
		JSONObject vendorResponseObj = vendorObj.getJSONObject("Response");
		JSONObject vendorResponsePayloadObj = vendorResponseObj
				.getJSONObject("ResponsePayLoad");
		final JSONArray vendorResponseTransactionArray = vendorResponsePayloadObj
				.getJSONArray(TRANSACTIONS);
		String agentStatus = "";
		JSONArray reportingAgents = null;
		int size = 0;
		if (vendorResponseTransactionArray != null) {
			size = vendorResponseTransactionArray.length();
		}
		for (int i = 0; i < size; i++) {
			try {

				final JSONObject jsonObj = vendorResponseTransactionArray
						.getJSONObject(i);
				JSONObject transactionDataObj = jsonObj
						.getJSONObject("TransactionData");
				JSONObject statusObj = transactionDataObj
						.getJSONObject("StatusData");
				String status = statusObj.getString("Status");
				if (LifeEngageComponentHelper.SUCCESS.equals(status)) {
					transactionDataObj = jsonObj
							.getJSONObject("TransactionData");
					if (transactionDataObj.has("AgentDetails")) {
						JSONObject agentDetailsDataObj = transactionDataObj
								.getJSONObject("AgentDetails");
						if (agentDetailsDataObj.has("status")) {
							agentStatus = agentDetailsDataObj
									.getString("status");
						}
						if (LifeEngageComponentHelper.SUCCESS.equals(status)) {
							reportingAgents = agentDetailsDataObj
									.getJSONArray("reportingAgents");
						}
					}
					if (jsonObj.has(Constants.KEY11)) {
						agentId = jsonObj.getString(Constants.KEY11);

						if ((jsonObj.has(GeneraliConstants.KEY12) && !("")
								.equals(jsonObj
										.getString(GeneraliConstants.KEY12)))) {
							agentId = agentId
									+ "-"
									+ jsonObj
											.getString(GeneraliConstants.KEY12);

						}

					}

					List<AgentData> repotingagents = new ArrayList<AgentData>();
					if (reportingAgents != null) {
						int reportingAgentSize = reportingAgents.length();
						for (int i1 = 0; i1 < reportingAgentSize; i1++) {
							if (reportingAgents.get(i1) != null) {
								JSONObject agentJsonObj = reportingAgents
										.getJSONObject(i1);
								AgentData agentData = new AgentData();
								if (agentJsonObj.has("status")
										&& GeneraliConstants.STATUS_ACTIVE
												.equals(agentJsonObj
														.getString("status"))
										&& GeneraliConstants.TYPE_BANCA
												.equals(agentJsonObj
														.getString("agentType"))) {
									agentData.setAgentCode(agentJsonObj
											.getString("agentCode"));
									agentData.setAgentName(agentJsonObj
											.getString("agentName"));
									repotingagents.add(agentData);
								}
							}
						}
					}
					if (repotingagents.size() > 0) {
						branchIdRepotingagentsMap.put(agentId, repotingagents);
					}
				}
			} catch (Exception e) {
				LOGGER.error("Parsing in vendor response", e);
				branchIdRepotingagentsMap.put(agentId, null);
			}
		}

		return branchIdRepotingagentsMap;
	}

	/**
	 * 
	 * Method to find and allocate agents for banca channel bulk upload
	 */
	public void leadAllocationRule(
			Map<String, List<BulkUploadData>> leadDataMap,
			Map<String, List<AgentData>> branchIdRepotingagentsMap,
			Map<String, List<AgentData>> bankStateReportingAgentsMap,
			RequestInfo requestInfo) {
		Map<String, Integer> agentCountMap = new TreeMap<String, Integer>();
		List<BulkUploadData> validationSuccessCustomerList = getLeadDataList(
				leadDataMap, GeneraliConstants.VALID_SUCCESS);

		List<BulkUploadData> allocationFailed = getLeadDataList(leadDataMap,
				GeneraliConstants.AGENT_FAILED);
		if (allocationFailed == null) {
			allocationFailed = new ArrayList<BulkUploadData>();
		}

		List<BulkUploadData> allocatedDataList = new ArrayList<BulkUploadData>();
		List<String> reportingAgentsList = new ArrayList<String>();
		List<String> agentswithLessthan50Followups = new ArrayList<String>();
		List<AgentData> agentsMaxYTD = new ArrayList<AgentData>();
		for (BulkUploadData customerData : validationSuccessCustomerList) {

			try {

				reportingAgentsList = getReportingAgentsListFromVendorService(
						customerData, branchIdRepotingagentsMap,
						bankStateReportingAgentsMap);
				if (reportingAgentsList == null
						|| reportingAgentsList.size() == 0) {
					customerData.setStatus(false);
					customerData.setRemarks(GeneraliConstants.NO_ALLOCATION);

				} else if (reportingAgentsList.size() == 1) {
					customerData.setAgentCode(reportingAgentsList.get(0));
					getMappedBranchAndAgentDetails(customerData);
					allocatedDataList.add(customerData);
				} else {
					agentswithLessthan50Followups = getAgentsWithlessthan50FollowUps(
							reportingAgentsList, requestInfo);
					if (agentswithLessthan50Followups.size() == 1) {
						customerData.setAgentCode(agentswithLessthan50Followups
								.get(0));
						getMappedBranchAndAgentDetails(customerData);
						allocatedDataList.add(customerData);
					} else if (agentswithLessthan50Followups.size() > 1) {
						agentsMaxYTD = lmsRepository
								.retrieveAgentwithMaximumYTD(
										agentswithLessthan50Followups,
										requestInfo);
						if (agentsMaxYTD.size() == 1) {
							customerData.setAgentCode(agentsMaxYTD.get(0)
									.getAgentCode());
							getMappedBranchAndAgentDetails(customerData);
							allocatedDataList.add(customerData);
						} else if (agentsMaxYTD.size() > 1) {
							List<AgentData> sortYTDalphapetic = new ArrayList<AgentData>();

							sortYTDalphapetic
									.addAll(sortYTDalphapetic(agentsMaxYTD));
							String agentCode = "";
							agentCode = findAgentCode(sortYTDalphapetic,
									agentCountMap);
							customerData.setAgentCode(agentCode);
							getMappedBranchAndAgentDetails(customerData);
							allocatedDataList.add(customerData);

						}
					}

				}

			} catch (BusinessException e) {
				LOGGER.error("Error in getting Vendor Service", e);
			} catch (ParseException e) {
				LOGGER.error("Error in parsing VendorResponse", e);

			} catch (Exception e) {
				LOGGER.error("Error in Saving data", e);
			}
			if ((customerData.getAgentCode() == null || ((GeneraliConstants.JSON_EMPTY)
					.equals(customerData.getAgentCode())))) {
				customerData.setRemarks(GeneraliConstants.NO_ALLOCATION);
				allocationFailed.add(customerData);
			} else {
				int count = 0;
				if (agentCountMap != null) {
					if (agentCountMap.containsKey(customerData.getAgentCode())) {
						count = agentCountMap.get(customerData.getAgentCode());
						count++;
					} else {
						count = 1;
					}
					agentCountMap.put(customerData.getAgentCode(), count);
				}
			}
		}

		leadDataMap.put(GeneraliConstants.AGENT_FAILED, allocationFailed);
		leadDataMap.put(GeneraliConstants.VALID_SUCCESS, allocatedDataList);

	}

	/**
	 * 
	 * @param leadDataMap
	 * @param Key
	 * @return
	 */
	public List<BulkUploadData> getLeadDataList(
			Map<String, List<BulkUploadData>> leadDataMap, String Key) {
		List<BulkUploadData> leadDataList = null;
		if (leadDataMap.containsKey(Key)) {
			leadDataList = leadDataMap.get(Key);
		}
		return leadDataList;

	}

	/**
	 * 
	 * @param agentCodeMap
	 * @param Key
	 * @return
	 */
	public List<String> getAgentCodeList(
			Map<String, List<AgentData>> agentCodeMap, String Key) {
		List<AgentData> agentDataList = null;
		List<String> agentCodeList = new ArrayList<String>();
		if (agentCodeMap != null) {
			if (agentCodeMap.containsKey(Key)) {
				agentDataList = agentCodeMap.get(Key);

				for (AgentData agentData : agentDataList) {
					if (agentData.getAgentCode() != null) {
						agentCodeList.add(agentData.getAgentCode());
					}
				}
			}
		}
		return agentCodeList;

	}

	/**
	 * method to save lead data
	 * @param agencyCustomerData
	 * @return
	 * @throws ParseException
	 */
	public String saveLeadData(BulkUploadData agencyCustomerData)
			throws ParseException {
		String validLeadJsonString = "";
		String response = "";
		validLeadJsonString = saveLMSBulkUploadHolder
				.parseBO(agencyCustomerData);
		//LOGGER.error("JSON"+validLeadJsonString);
		if (validLeadJsonString != null) {
			JSONObject jsonRequestObj = new JSONObject(requestInfoJson);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);
			final LifeEngageAudit lifeEngageAudit = lifeEngageAuditComponent
					.savelifeEngageAudit(requestInfo, validLeadJsonString);
			response = lifeEngageLMSComponent.saveLMS(validLeadJsonString,
					requestInfo, lifeEngageAudit);

		}
		return response;
	}

	/**
	 * Creates Request Info
	 */
	@Override
	public RequestInfo createRequestInfo(String json) throws ParseException {
		RequestInfo requestInfo = new RequestInfo();
		JSONObject jsonRequestObj = new JSONObject(requestInfoJson);
		final JSONObject jsonRequestInfoObj = jsonRequestObj
				.getJSONObject(REQUEST_INFO);
		requestInfo = LifeEngageSyncServiceHelper
				.parseRequestInfo(jsonRequestInfoObj);
		return requestInfo;

	}

	/**
	 * Get agents from Bank and state
	 * @param type
	 * @param customerData
	 * @return
	 * @throws ParseException
	 * @throws BusinessException
	 */
	public List<String> getStateBankAgent(String type,
			BulkUploadData customerData) throws ParseException,
			BusinessException {
		List<String> repotingAgentsList = new ArrayList<String>();
		BankBranch bankBranch = new BankBranch();
		Set<BankBranch> bankBranchSet = new TreeSet<BankBranch>();
		Map<String, List<AgentData>> bankBranchMap = new HashMap<String, List<AgentData>>();
		if ("bankState".equals(type)) {
			bankBranch.setBank(customerData.getBankCode());
			bankBranch.setState(customerData.getStateCode());
			bankBranch.setBankOrStateOrBranchCode(customerData.getStateCode()
					+ "-" + customerData.getBankCode());
			bankBranchSet.add(bankBranch);
			bankBranchMap = getBankBranchReportingAgetnsMap(bankBranchSet);
			repotingAgentsList = getAgentCodeList(bankBranchMap,
					bankBranch.getBankOrStateOrBranchCode());
		} else if ("state".equals(type)) {
			bankBranch.setBank(null);
			bankBranch.setState(customerData.getStateCode());
			bankBranch.setBankOrStateOrBranchCode(customerData.getStateCode());
			bankBranchSet.add(bankBranch);
			bankBranchMap = getBankBranchReportingAgetnsMap(bankBranchSet);
			repotingAgentsList = getAgentCodeList(bankBranchMap,
					customerData.getStateCode());
		}
		return repotingAgentsList;

	}

	/**
	 * Gets reporting agents of the branch or Bank and state 
	 * @param customerData
	 * @param branchIdRepotingagentsMap
	 * @param bankStateReportingAgentsMap
	 * @return
	 * @throws ParseException
	 * @throws BusinessException
	 */
	public List<String> getReportingAgentsListFromVendorService(
			BulkUploadData customerData,
			Map<String, List<AgentData>> branchIdRepotingagentsMap,
			Map<String, List<AgentData>> bankStateReportingAgentsMap)
			throws ParseException, BusinessException {

		List<String> reportingAgentslist = new ArrayList<String>();
		try {
			if (customerData.getBranch() != null
					&& !("").equals(customerData.getBranch())) {

				reportingAgentslist = getAgentCodeList(
						branchIdRepotingagentsMap, customerData.getBranchCode());
			}
			if (reportingAgentslist == null || reportingAgentslist.size() == 0) {
				reportingAgentslist = getAgentCodeList(
						bankStateReportingAgentsMap,
						customerData.getStateCode() + "-"
								+ customerData.getBankCode());
			}

			if (reportingAgentslist == null || reportingAgentslist.size() == 0) {
				reportingAgentslist = getStateBankAgent("bankState",
						customerData);
			}
		} catch (Exception e) {
			LOGGER.error("Error in getting agentCodes", e);
		}
		return reportingAgentslist;

	}

	/**
	 * Returns agents with follow up count less than 50
	 * @param reportingAgentsList
	 * @param requestInfo
	 * @return
	 */
	public List<String> getAgentsWithlessthan50FollowUps(
			List<String> reportingAgentsList, RequestInfo requestInfo) {
		List<String> agentswithLessthan50Followups = new ArrayList<String>();
		List<AgentData> agentYTD = new ArrayList<AgentData>();
		agentswithLessthan50Followups = lmsRepository
				.retrieveAgentwithLessthan50Followups(reportingAgentsList,
						requestInfo);
		if (agentswithLessthan50Followups == null
				|| agentswithLessthan50Followups.size() == 0) {
			agentYTD = lmsRepository.retrieveAgentwithMaximumYTD(
					reportingAgentsList, requestInfo);
			agentswithLessthan50Followups = new ArrayList<String>();
			for (AgentData agentDetails : agentYTD) {

				agentswithLessthan50Followups.add(agentDetails.getAgentCode());
			}
		}
		return agentswithLessthan50Followups;

	}

	/**
	 * Method to delete files.
	 */
	public void moveFiles(String fileName) {
		try {
			
			File sourcefile = new File(fileName);
			sourcefile.delete();
		} catch (Exception e) {
			LOGGER.error("Error in deleting file", e);
		}
	}

	/**
	 * Method getVendorResponse
	 * @param list
	 * @param smooksHolder
	 * @return
	 * @throws ParseException
	 * @throws BusinessException
	 */
	public String getVendorResponse(List list,
			LifeEngageSmooksHolder smooksHolder) throws ParseException,
			BusinessException {
		String transactionJson = "";
		String vendorResponse = "";

		transactionJson = smooksHolder.parseBO(list);
		try {

			if (transactionJson != null) {
				JSONObject jsonRequestPayloadObj = new JSONObject(
						transactionJson);
				JSONObject jsonRequestObj = new JSONObject(requestInfoJson);
				final RequestInfo requestInfo = LifeEngageSyncServiceHelper
						.parseRequestInfo(jsonRequestObj);
				final JSONArray jsonTransactionArray = jsonRequestPayloadObj
						.getJSONArray(TRANSACTIONS);

				vendorResponse = vendorAgentComponent
						.retrieveAgentProfileByCode(requestInfo,
								jsonTransactionArray);
			}
		} catch (BusinessException e) {
			LOGGER.error("Error in getting vendor response", e);
		}
		return vendorResponse;
	}

	/**
	 * 
	 * @param vendorResponse
	 * @param channel
	 * @return
	 */
	public List<String> getActiveReportingAgents(String vendorResponse,
			String channel) {
		List<String> activeAgents = new ArrayList<String>();
		try {
			JSONObject vendorObj = new JSONObject(vendorResponse);
			JSONObject vendorResponseObj = vendorObj.getJSONObject("Response");
			JSONObject vendorResponsePayloadObj = vendorResponseObj
					.getJSONObject("ResponsePayLoad");
			final JSONArray vendorResponseTransactionArray = vendorResponsePayloadObj
					.getJSONArray(TRANSACTIONS);
			int size = 0;
			if (vendorResponseTransactionArray != null) {
				size = vendorResponseTransactionArray.length();
			}
			for (int i = 0; i < size; i++) {
				final JSONObject jsonObj = vendorResponseTransactionArray
						.getJSONObject(i);

				JSONObject statusObj = jsonObj.getJSONObject("StatusData");
				String status = statusObj.getString("Status");
				JSONObject transactionDataObj = jsonObj
						.getJSONObject("TransactionData");
				String agentId = "";
				String agentStatus = "";
				String agentType = "";
				if (transactionDataObj.has("AgentDetails")) {
					JSONObject agentDetailsDataObj = transactionDataObj
							.getJSONObject("AgentDetails");
					if (agentDetailsDataObj.has("status")) {
						agentStatus = agentDetailsDataObj.getString("status");
					}
					if (agentDetailsDataObj.has("agentType")) {
						agentType = agentDetailsDataObj.getString("agentType");
					}
				}
				if (jsonObj.has(Constants.KEY11)) {
					agentId = jsonObj.getString(Constants.KEY11);
				}

				if (LifeEngageComponentHelper.SUCCESS.equals(status)) {
					if (GeneraliConstants.STATUS_ACTIVE.equals(agentStatus)) {
						if (channel.equals(agentType)) {
							activeAgents.add(agentId);
						}

					}
				}

			}
		} catch (Exception e) {
			LOGGER.error("error in parsing vendor response", e);
		}
		return activeAgents;

	}

	/***
	 * 
	 * @param customerData
	 */
	// To get the Mapped branch and agent Name of allocated agent.
	public void getMappedBranchAndAgentDetails(BulkUploadData customerData) {
		// String mappedBranch = "";
		BankBranch bankBranch = new BankBranch();
		Map<String, BankBranch> maappedBranchMap = new HashMap<String, BankBranch>();
		Map<String, AgentData> agentDataMap = new HashMap<String, AgentData>();
		List<String> agentCodeList = new ArrayList<String>();
		try {
			agentCodeList.add(customerData.getAgentCode());
			String vendorResponse = getVendorResponse(agentCodeList,
					retriveAgentBulkUploadHolder);
			agentDataMap = getMappedBranchesFromRequest(vendorResponse);
			AgentData agentData = new AgentData();
			if (agentDataMap != null
					&& agentDataMap.containsKey(customerData.getAgentCode())) {
				agentData = agentDataMap.get(customerData.getAgentCode());
				if (agentData != null) {
					maappedBranchMap = agentData.getMappedBranches();
				}
			}
			String branchCode = null;
			if (maappedBranchMap != null
					&& maappedBranchMap.containsKey(customerData
							.getBranchCode())) {
				bankBranch = maappedBranchMap.get(customerData.getBranchCode());
				// mappedBranch = customerData.getBranch();

			} else {
				Set<String> branchCodeSet = maappedBranchMap.keySet();
				List<String> branchCodeList = new ArrayList<String>();
				branchCodeList.addAll(branchCodeSet);

				if (branchCodeList != null) {
					branchCode = branchCodeList.get(0);
				}
			
				bankBranch = maappedBranchMap.get(branchCode);
			}
				// branchCode=maappedBranchMap.get
				if (bankBranch != null
						&& bankBranch.getBranchCode() != null
						&& bankBranch.getBranchName() != null
						&& !GeneraliConstants.JSON_EMPTY.equals(bankBranch
								.getBranchCode())
						&& !GeneraliConstants.JSON_EMPTY.equals(bankBranch
								.getBranchName())) {
					// mappedBranch = bankBranch.getBranchCode() + " " + "-" +
					// " "
					// + bankBranch.getBranchName();
					customerData.setAllocatedBranchName(bankBranch
							.getBranchName());
					customerData.setAllocatedBranchCode(bankBranch
							.getBranchCode());

				}
			
			customerData.setAgentName(agentData.getAgentName());
			// customerData.setAllocatedBranchCode(branchCode);
		} catch (Exception e) {
			LOGGER.error("error in parsing data vendorResponse", e);
		}

	}

	/**
	 * 
	 * @param vendorResponse
	 * @return
	 */
	public Map<String, AgentData> getMappedBranchesFromRequest(
			String vendorResponse) {
		Map<String, AgentData> agentResultMap = new HashMap<String, AgentData>();
		try {
			JSONObject vendorObj = new JSONObject(vendorResponse);
			JSONObject vendorResponseObj = vendorObj.getJSONObject("Response");
			JSONObject vendorResponsePayloadObj = vendorResponseObj
					.getJSONObject("ResponsePayLoad");
			final JSONArray vendorResponseTransactionArray = vendorResponsePayloadObj
					.getJSONArray(TRANSACTIONS);

			for (int i = 0; i < vendorResponseTransactionArray.length(); i++) {
				final JSONObject jsonObj = vendorResponseTransactionArray
						.getJSONObject(i);
				AgentData agentData = new AgentData();
				Map<String, BankBranch> mappedBranchCode = new HashMap<String, BankBranch>();
				JSONObject statusObj = jsonObj.getJSONObject("StatusData");
				String status = statusObj.getString("Status");
				JSONObject transactionDataObj = jsonObj
						.getJSONObject("TransactionData");
				if (transactionDataObj.has("AgentDetails")) {
					JSONObject agentDetailsDataObj = transactionDataObj
							.getJSONObject("AgentDetails");
					if (agentDetailsDataObj.has("agentName")) {
						agentData.setAgentName(agentDetailsDataObj
								.getString("agentName"));
						agentData.setAgentCode(agentDetailsDataObj
								.getString("agentCode"));
					}
					if (agentDetailsDataObj.has("mappedBranches")) {
						JSONArray mappedBranches = agentDetailsDataObj
								.getJSONArray("mappedBranches");
						int mappedSize = mappedBranches.length();
						for (int i1 = 0; i1 < mappedSize; i1++) {
							BankBranch bankBranch = new BankBranch();
							//UAT Bug 89 Fix
							JSONObject agentJsonObj = mappedBranches
									.getJSONObject(i1);
							if (agentJsonObj.has("branchCode")) {
								bankBranch.setBranchCode(agentJsonObj
										.getString("branchCode"));
								bankBranch.setBranchName(agentJsonObj
										.getString("branchName"));
								mappedBranchCode.put(
										agentJsonObj.getString("branchCode"),
										bankBranch);
							}

						}
					}
					agentData.setMappedBranches(mappedBranchCode);
				}
				agentResultMap.put(agentData.getAgentCode(), agentData);
			}
		} catch (Exception e) {
			LOGGER.error("Error in parsing Vendor response", e);
		}
		return agentResultMap;

	}

	/**
	 * 
	 * @param agentYTDList
	 * @param countMap
	 * @return
	 */
	public String findAgentCode(List<AgentData> agentYTDList,
			Map<String, Integer> countMap) {
		String agentCode = "";
		boolean flagAgentExist = false;
		boolean flagAgentFound = false;
		String agentCodeYTD = "";
		for (AgentData agentData : agentYTDList) {

			agentCodeYTD = agentData.getAgentCode();
			if (!countMap.containsKey(agentCodeYTD)) {
				agentCode = agentCodeYTD;
				flagAgentFound = true;
				break;
			} else {
				flagAgentExist = true;
			}
		}

		if (!flagAgentFound) {
			Collection<Integer> counts = (Collection<Integer>) countMap
					.values();
			List<Integer> countList = new ArrayList<Integer>();
			countList.addAll(counts);
			Collections.sort(countList);
			boolean agentfoundToallocate = false;
			int maxCount = countList.get(countList.size() - 1);
			for (AgentData agentData : agentYTDList) {
				int count = countMap.get(agentData.getAgentCode());

				if (maxCount != count) {
					agentCode = agentData.getAgentCode();
					agentfoundToallocate = true;
					break;
				}
			}
			if (!agentfoundToallocate) {
				agentCode = agentYTDList.get(0).getAgentCode();
			}

		}
		return agentCode;

	}

	/**
	 * 
	 * @param agentYTDList
	 * @return
	 * @throws ParseException
	 * @throws BusinessException
	 */
	public List<AgentData> sortYTDalphapetic(List<AgentData> agentYTDList)
			throws ParseException, BusinessException {
		List<AgentData> nameSortedList = new ArrayList<AgentData>();
		try {
			BigDecimal maxYTD = new BigDecimal(0);
			List<String> agentCodeList = new ArrayList<String>();
			Map<String, AgentData> agentDataMap = new HashMap<String, AgentData>();
			if (agentYTDList != null && agentYTDList.size() > 0) {
				maxYTD = agentYTDList.get(0).getYTD();

			}
			for (AgentData agentDetails : agentYTDList) {
				agentDataMap.put(agentDetails.getAgentCode(), agentDetails);
				agentCodeList.add(agentDetails.getAgentCode());

			}
			if (agentDataMap != null && agentYTDList != null) {
				nameSortedList
						.addAll(getAgentNames(agentCodeList, agentDataMap));
			}
		} catch (Exception e) {
			LOGGER.error("error in sorting alphabetic oreder", e);
			nameSortedList.addAll(agentYTDList);
		}

		return nameSortedList;
	}

	/**
	 * 
	 * @param agentList
	 * @param agentDataMap
	 * @return
	 * @throws ParseException
	 * @throws BusinessException
	 */
	// To sort Agent Names if YTD are equal
	public List<AgentData> getAgentNames(List<String> agentList,
			Map<String, AgentData> agentDataMap) throws ParseException,
			BusinessException {
		String vendorResponse = null;
		String transactionJson = null;
		List<AgentData> agentDataList = new ArrayList<AgentData>();

		transactionJson = retriveAgentBulkUploadHolder.parseBO(agentList);
		if (transactionJson != null) {
			JSONObject jsonRequestPayloadObj = new JSONObject(transactionJson);
			JSONObject jsonRequestObj = new JSONObject(requestInfoJson);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestObj);
			final JSONArray jsonTransactionArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);

			vendorResponse = vendorAgentComponent.retrieveAgentProfileByCode(
					requestInfo, jsonTransactionArray);
			// LOGGER.error(vendorResponse);
			JSONObject vendorObj = new JSONObject(vendorResponse);
			JSONObject vendorResponseObj = vendorObj.getJSONObject("Response");
			JSONObject vendorResponsePayloadObj = vendorResponseObj
					.getJSONObject("ResponsePayLoad");
			final JSONArray vendorResponseTransactionArray = vendorResponsePayloadObj
					.getJSONArray(TRANSACTIONS);
			int size = vendorResponseTransactionArray.length();
			for (int i = 0; i < size; i++) {
				final JSONObject jsonObj = vendorResponseTransactionArray
						.getJSONObject(i);
				AgentData agentData = new AgentData();
				JSONObject transactionDataObj = jsonObj
						.getJSONObject("TransactionData");
				JSONObject statusObj = jsonObj.getJSONObject("StatusData");
				String status = statusObj.getString("Status");
				if (LifeEngageComponentHelper.SUCCESS.equals(status)) {
					if (transactionDataObj.has("AgentDetails")) {
						JSONObject agentDetailsDataObj = transactionDataObj
								.getJSONObject("AgentDetails");
						if (agentDetailsDataObj.has("agentName")) {
							String agentCode = agentDetailsDataObj
									.getString("agentCode");
							if (agentDataMap.containsKey(agentCode)) {
								agentData = agentDataMap.get(agentCode);
								agentData.setAgentName(agentDetailsDataObj
										.getString("agentName"));
								agentDataMap.put(agentCode, agentData);
							}

						}
					}

				}
			}

			agentDataList.addAll(agentDataMap.values());

			Collections.sort(agentDataList, new Comparator<AgentData>() {
				public int compare(AgentData o1, AgentData o2) {
					return o2.getYTD().compareTo(o1.getYTD());

				}
			});

			Collections.sort(agentDataList, new Comparator<AgentData>() {
				public int compare(AgentData o1, AgentData o2) {
					Locale locale = new Locale("vie", "VNM");
					Collator usCollator = Collator.getInstance(locale);
					usCollator.setStrength(Collator.TERTIARY);
					usCollator.setDecomposition(Collator.FULL_DECOMPOSITION);
					String s_2 = o2.getAgentName();
					String s_1 = o1.getAgentName();

					if (o1.getYTD().compareTo(o2.getYTD()) == 0) {
						return usCollator.compare(s_1, s_2);
						// return s_1.compareTo(s_2);
					} else {
						return 0;
					}

				}
			});
		}
		return agentDataList;
	}

	
	@Override
	public Map<String, Map<String, String>> getLookUpData() {
		Map<String, Map<String, String>> allLookUpsMap = new HashMap<String, Map<String, String>>();

		String type = GeneraliConstants.TYPE_STATE;
		String language = GeneraliConstants.LANG_BULK;
		String country = GeneraliConstants.BULK;

		Map<String, String> stateMap = bulkUploadRepository.getLookUpdata(type,
				language, country);
		if (stateMap != null && !stateMap.isEmpty()) {
			allLookUpsMap.put(type, stateMap);
		}
		String type_NATIONALITY = GeneraliConstants.TYPE_NATIONALITY;
		language = GeneraliConstants.LANG_VT;
		country = GeneraliConstants.COUNTRY_VT;

		Map<String, String> nationalityMap = bulkUploadRepository
				.getLookUpdata(type_NATIONALITY, language, country);

		if (nationalityMap != null && !nationalityMap.isEmpty()) {
			allLookUpsMap.put(type_NATIONALITY, nationalityMap);
		}
		language = GeneraliConstants.LANG_EN;
		country = GeneraliConstants.US;
		String type_OCCUPATION = GeneraliConstants.TYPE_OCCUPATION;
		Map<String, String> occupationMap = bulkUploadRepository.getLookUpdata(
				type_OCCUPATION, language, country);

		if (occupationMap != null && !occupationMap.isEmpty()) {
			allLookUpsMap.put(type_OCCUPATION, occupationMap);
		}
		String type_INCOME = GeneraliConstants.TYPE_INCOME;
		Map<String, String> incomeMap = bulkUploadRepository.getLookUpdata(
				type_INCOME, language, country);

		
		if (incomeMap != null && !incomeMap.isEmpty()) {
			allLookUpsMap.put(type_INCOME, incomeMap);
		}

		String type_Need = GeneraliConstants.TYPE_NEED;
		Map<String, String> needMap = bulkUploadRepository.getLookUpdata(
				type_Need, language, country);

		if (occupationMap != null && !occupationMap.isEmpty()) {
			allLookUpsMap.put(type_Need, needMap);
		}
		return allLookUpsMap;
	}

}