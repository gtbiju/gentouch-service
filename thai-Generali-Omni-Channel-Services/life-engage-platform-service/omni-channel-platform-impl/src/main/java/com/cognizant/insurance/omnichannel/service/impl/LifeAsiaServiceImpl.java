package com.cognizant.insurance.omnichannel.service.impl;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cognizant.insurance.component.LifeEngageAuditComponent;
import com.cognizant.insurance.component.LifeEngageEAppComponent;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.omnichannel.component.LifeAsiaComponent;
import com.cognizant.insurance.omnichannel.component.impl.LifeAsiaProcessEngine;
import com.cognizant.insurance.omnichannel.service.LifeAsiaService;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.service.helper.LifeEngageSyncServiceHelper;

@Service
public class LifeAsiaServiceImpl implements LifeAsiaService {

	@Autowired
	LifeAsiaProcessEngine engine;
	@Autowired
	private LifeAsiaComponent lifeAsiaComponent;
    @Autowired
    private LifeEngageEAppComponent lifeEngageEAppComponent;
	@Autowired
	@Qualifier("eAppSaveMapping")
	private LifeEngageSmooksHolder saveEAppHolder;
	@Autowired
	private LifeEngageAuditComponent lifeEngageAuditComponent;
	
	

	@Override
	public void submit(String spajNo) {
		engine.createAndSentLifeAsiaRequest(spajNo);

	}
	@Override
	public void resubmit(String spajNo) {
		
	}

	@Override
	public String getStatus(String spajNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getRequestData(String data) {
		return null;

	}
	@Override
	public String generateBPMRequest(String data){
		String response="";
		try {
			JSONObject jsonObject;
			String id = "";
			final JSONObject jsonObjectRs = new JSONObject();

			jsonObject = new JSONObject(data);

			final JSONObject jsonRequestObj = jsonObject.getJSONObject(REQUEST);
			final JSONObject jsonRequestInfoObj = jsonRequestObj
					.getJSONObject(REQUEST_INFO);
			final RequestInfo requestInfo = LifeEngageSyncServiceHelper
					.parseRequestInfo(jsonRequestInfoObj);			
			final JSONObject jsonRequestPayloadObj = jsonRequestObj
					.getJSONObject(REQUEST_PAYLOAD);
			final JSONArray jsonRqArray = jsonRequestPayloadObj
					.getJSONArray(TRANSACTIONS);
			response=lifeAsiaComponent.createLifeAsiaRequestString(requestInfo, jsonRqArray.getJSONObject(0));			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
		
	}
	

	
	public String invokeSchedule(){
		return "";
	}

	/** The Constant REQUEST. */
	private static final String REQUEST = "Request";

	/** The Constant REQUEST_INFO. */
	private static final String REQUEST_INFO = "RequestInfo";

	/** The Constant TRANSACTIONS. */
	private static final String TRANSACTIONS = "Transactions";

	/** The Constant E_APP. */
	private static final String E_APP = "eApp";

	/** The Constant illustration. */
	private static final String ILLUSTRATION = "illustration";

	/** The Constant TYPE. */
	private static final String TYPE = "Type";

	/** The Constant REQUEST_PAYLOAD. */
	private static final String REQUEST_PAYLOAD = "RequestPayload";

	/** The Constant OBSERVATIONS. */
	private static final String OBSERVATIONS = "Observations";

	/** The Constant OBSERVATIONS. */
	private static final String OBSERVATION = "Observation";

	/** The Constant Key4. */
	private static final String KEY4 = "Key4";

	/** The Constant KEY3. */
	private static final String KEY3 = "Key3";

	/** The Constant FNA. */
	private static final String FNA = "FNA";

	/** The Constant LMS. */
	private static final String LMS = "LMS";



	@Override
	public String generateBPMEmailPdf(String json) {
		return null;
	}




}
