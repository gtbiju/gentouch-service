-- Should run these queries before giving CoreProductsDB to the DEV team

update CORE_PRODUCT_COMPONENT set LAUNCH_DATE='2015/01/06 00:00:00' where lineOfBusiness_ID=80900;
update CORE_PRODUCT_COMPONENT set LAUNCH_DATE='1899/12/31 00:00:00' where lineOfBusiness_ID=80905;
update CORE_PRODUCT_COMPONENT set SUSPENSION_DATE='2100/12/12 00:00:00';
update CORE_PRODUCT_COMPONENT set WITHDRAWAL_DATE='2099/01/01 00:00:00' where lineOfBusiness_ID=80900;
update CORE_PRODUCT_COMPONENT set WITHDRAWAL_DATE='2100/12/12 00:00:00' where lineOfBusiness_ID=80905;