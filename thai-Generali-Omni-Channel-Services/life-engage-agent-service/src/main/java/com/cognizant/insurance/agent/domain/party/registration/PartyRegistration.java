package com.cognizant.insurance.agent.domain.party.registration;

import javax.persistence.MappedSuperclass;

/**
 * This super-type is used for the identification of a party through specific country attributes.
 * Some data about a party can be used as a way to identify it. For example, the tax file id can be used to quickly identify a person or an organization. 
 * This relationship allows to attach such identification data to a party.  Some identification data can be common to several parties: for example a social security id can be shared with the children of a person.
 * 
 * An official recognition related to a role player. 
 *  
 * e.g: A driving license.
 */
@MappedSuperclass
public abstract class PartyRegistration extends Registration {
}