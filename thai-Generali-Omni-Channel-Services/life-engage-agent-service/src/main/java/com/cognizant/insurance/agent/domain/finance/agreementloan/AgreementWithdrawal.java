/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Feb 6, 2013
 */

package com.cognizant.insurance.agent.domain.finance.agreementloan;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.cognizant.insurance.agent.domain.agreement.FinancialServicesAgreement;
import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.PaymentFrequencyCodeList;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.agent.domain.finance.FinancialProvision;

/**
 * An agreement loan (or policy loan) is a loan made by an insurer from its general funds to a policy owner on the
 * security of the cash value of a life insurance policy.
 * 
 * Generally, loans may reduce the policy's death benefit and cash value.
 * 
 * @author
 * 
 * 
 */

@Entity
public class AgreementWithdrawal extends FinancialProvision {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8360375128188050159L;

    /**
     * Amount of the withdrawal.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private CurrencyAmount withdrawalAmount;
    
    /** The withdrawal term. */
    private Integer withdrawalTerm;

    /**
     * This is the duration of the loan.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private TimePeriod withdrawalPeriod;

    /** The withdrawal type. */
    private String withdrawalType;

    /**
     * Defines the frequency of the loan repayment.
     * 
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private PaymentFrequencyCodeList paymentFrequencyCode;

    /**
     * This relationship links an agreement loan to its related financial services agreement.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private FinancialServicesAgreement relatedAgreement;

    /** The withdrawal frequency. */
    private Integer withdrawalFrequency;

    /**
     * Gets the withdrawalAmount.
     * 
     * @return Returns the withdrawalAmount.
     */
    public CurrencyAmount getWithdrawalAmount() {
        return withdrawalAmount;
    }

    /**
     * Sets The withdrawalAmount.
     * 
     * @param withdrawalAmount
     *            The withdrawalAmount to set.
     */
    public void setWithdrawalAmount(final CurrencyAmount withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    /**
     * Gets the withdrawalPeriod.
     * 
     * @return Returns the withdrawalPeriod.
     */
    public TimePeriod getWithdrawalPeriod() {
        return withdrawalPeriod;
    }

    /**
     * Sets The withdrawalPeriod.
     * 
     * @param withdrawalPeriod
     *            The withdrawalPeriod to set.
     */
    public void setWithdrawalPeriod(final TimePeriod withdrawalPeriod) {
        this.withdrawalPeriod = withdrawalPeriod;
    }

    /**
     * Gets the paymentFrequencyCode.
     * 
     * @return Returns the paymentFrequencyCode.
     */
    public PaymentFrequencyCodeList getPaymentFrequencyCode() {
        return paymentFrequencyCode;
    }

    /**
     * Sets The paymentFrequencyCode.
     * 
     * @param paymentFrequencyCode
     *            The paymentFrequencyCode to set.
     */
    public void setPaymentFrequencyCode(final PaymentFrequencyCodeList paymentFrequencyCode) {
        this.paymentFrequencyCode = paymentFrequencyCode;
    }

    /**
     * Gets the relatedAgreement.
     * 
     * @return Returns the relatedAgreement.
     */
    public FinancialServicesAgreement getRelatedAgreement() {
        return relatedAgreement;
    }

    /**
     * Sets The relatedAgreement.
     * 
     * @param relatedAgreement
     *            The relatedAgreement to set.
     */
    public void setRelatedAgreement(final FinancialServicesAgreement relatedAgreement) {
        this.relatedAgreement = relatedAgreement;
    }

    /**
     * Gets the withdrawalType.
     * 
     * @return Returns the withdrawalType.
     */
    public String getWithdrawalType() {
        return withdrawalType;
    }

    /**
     * Sets The withdrawalType.
     * 
     * @param withdrawalType
     *            The withdrawalType to set.
     */
    public void setWithdrawalType(final String withdrawalType) {
        this.withdrawalType = withdrawalType;
    }

    /**
     * Gets the withdrawal term.
     *
     * @return the withdrawal term
     */
    public Integer getWithdrawalTerm() {
        return withdrawalTerm;
    }

    /**
     * Sets the withdrawal term.
     *
     * @param withdrawalTerm the withdrawal term to set.
     */
    public void setWithdrawalTerm(Integer withdrawalTerm) {
        this.withdrawalTerm = withdrawalTerm;
    }

    /**
     * @param withdrawalFrequency the withdrawalFrequency to set
     */
    public void setWithdrawalFrequency(Integer withdrawalFrequency) {
        this.withdrawalFrequency = withdrawalFrequency;
    }

    /**
     * @return the withdrawalFrequency
     */
    public Integer getWithdrawalFrequency() {
        return withdrawalFrequency;
    }

}
