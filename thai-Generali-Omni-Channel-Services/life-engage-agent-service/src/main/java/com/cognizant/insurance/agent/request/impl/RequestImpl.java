/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Dec 11, 2012
 */
package com.cognizant.insurance.agent.request.impl;

import com.cognizant.insurance.agent.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.agent.request.Request;

/**
 * The Class RequestImpl.
 * 
 * @param <T>
 *            the generic type
 */
public class RequestImpl<T> implements Request<T> {
    /** The type. */
    private T type;

    /** The context id. */
    private ContextTypeCodeList contextId;    

    /** The transaction id. */
    private String transactionId;

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.request.Request#setType(java.lang.Object)
     */
    @Override
    public final void setType(final T type) {
        this.type = type;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.icr.request.Request#getType()
     */
    @Override
    public final T getType() {
        return type;
    }

    /* (non-Javadoc)
     * @see com.cognizant.icr.request.Request#getContextId()
     */
    @Override
    public final ContextTypeCodeList getContextId() {
        return contextId;
    }

    /* (non-Javadoc)
     * @see com.cognizant.icr.request.Request#setContextId(java.lang.Long)
     */
    @Override
    public final void setContextId(final ContextTypeCodeList contextId) {
        this.contextId = contextId;
    }

    /* (non-Javadoc)
     * @see com.cognizant.icr.request.Request#getTransactionId()
     */
    @Override
    public final String getTransactionId() {
        return transactionId;
    }

    /* (non-Javadoc)
     * @see com.cognizant.icr.request.Request#setTransactionId(java.lang.String)
     */
    @Override
    public final void setTransactionId(final String transactionId) {
        this.transactionId = transactionId;
    }
}
