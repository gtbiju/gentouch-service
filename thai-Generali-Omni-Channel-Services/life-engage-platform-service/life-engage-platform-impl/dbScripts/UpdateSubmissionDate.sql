--- Query to update Submission Date for the closed leads

update PurchasedProduct set submissionDate=tmp.LastEditedDate

from (select pp.id ,CONVERT(VARCHAR(10),pr.modifiedDateTime,120) as LastEditedDate from PartyRoleInRelationship as pr with (nolock) 
INNER JOIN PartyRoleInRelationship_PurchasedProduct as prpd with (nolock) ON (pr.id=prpd.PartyRoleInRelationship_id)
INNER JOIN PurchasedProduct as PP with (nolock) ON (prpd.purchasedProducts_id=PP.id)
INNER JOIN CurrencyAmount as ca with (nolock) ON (pp.premium_id=ca.id)
where pr.statusCode ='24') tmp 

where PurchasedProduct.id =tmp.id