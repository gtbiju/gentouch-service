package com.cognizant.insurance.domain.commonelements.complexdatatypes;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * A numeric value including units of measure, such as liters, inches, or
 * kilometers.
 * 
 * An example is 150 km.
 * 
 * SOURCE: http://www.unece.org/cefact/codesfortrade/codes_index.htm
 */

@Entity
public class Measurement {
	
	
	
	/** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
    /**
     * The measured value expressed as a Decimal.
     */
    private BigDecimal value;

    /**
     * The code identifying the unit the measurement.
     * 
     * REFERENCE: http://www.unece.org/cefact/codesfortrade/codes_index.htm
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    private ExternalCode unitCode;

    /**
     * Gets the value.
     * 
     * @return Returns the value.
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Gets the unitCode.
     * 
     * @return Returns the unitCode.
     */
    public ExternalCode getUnitCode() {
        return unitCode;
    }

    /**
     * Sets The value.
     * 
     * @param value
     *            The value to set.
     */
    public void setValue(final BigDecimal value) {
        this.value = value;
    }

    /**
     * Sets The unitCode.
     * 
     * @param unitCode
     *            The unitCode to set.
     */
    public void setUnitCode(final ExternalCode unitCode) {
        this.unitCode = unitCode;
    }

	/**
	 * @param id the id to set
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
}
