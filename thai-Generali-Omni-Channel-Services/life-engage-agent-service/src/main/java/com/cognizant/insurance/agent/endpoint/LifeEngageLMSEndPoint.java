package com.cognizant.insurance.agent.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cognizant.insurance.agent.constants.Constants;
import com.cognizant.insurance.agent.core.exception.BusinessException;
import com.cognizant.insurance.agent.service.LifeEngageLMSService;



@Controller
@RequestMapping("/lifeEngageService")
public class LifeEngageLMSEndPoint {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageLMSEndPoint.class);
	
	@Autowired
    private LifeEngageLMSService lifeEngageLMSService;
	
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final String save(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageLMSService.createLead(json);
            }
        }.execute(json);
    }
	
	/**
     * Retrieve all data.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieve", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final String retrieve(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageLMSService.retrieve(json);
            }
        }.execute(json);
    }

    /**
     * Retrieve customer list.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveCustomers", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String retrieveCustomerList(@RequestBody final String json) { 
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageLMSService.retrieveCustomerList(json);
            }
        }.execute(json);
    }

    /**
     * Retrieve All customer Data for an Agent.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveAll", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON_UTF8, produces = Constants.TYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public final String retrieveAll(@RequestBody final String json) {
          return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageLMSService.retrieveAll(json);
            }
        }.execute(json);
    }
    
    /**
     * Retrieve By Filter.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/retrieveByFilter", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final
            String retrieveByFilter(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageLMSService.retrieveByFilter(json);
            }
        }.execute(json);
    }

    /**
     * Save observations.
     * 
     * @param json
     *            the json
     * @return the string
     */
    @RequestMapping(value = "/saveObservations", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final  String saveObservations(@RequestBody final String json) {
          return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return lifeEngageLMSService.saveObservations(json);
            }
        }.execute(json);
    }

}
