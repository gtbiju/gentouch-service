/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwBusinessException;
import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.icr.rule.bom.RuleCriteria;
import com.cognizant.insurance.audit.LifeEngageAudit;
import com.cognizant.insurance.component.LifeEngageEAppComponent;
import com.cognizant.insurance.component.TrackingIdGenerator;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.AuditRepository;
import com.cognizant.insurance.component.repository.EAppRepository;
import com.cognizant.insurance.component.repository.IllustrationRepository;
import com.cognizant.insurance.component.repository.ObservationRepository;
import com.cognizant.insurance.component.repository.RulesRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.observation.Observation;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.StatusData;
import com.cognizant.insurance.request.vo.Transactions;

// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageEAppComponentImpl.
 * 
 * @author 300797
 */
@Service("lifeEngageEAppComponent")
public class LifeEngageEAppComponentImpl implements LifeEngageEAppComponent {

    /** The get status holder. */
    @Autowired
    @Qualifier("getStatusMapping")
    private LifeEngageSmooksHolder getStatusHolder;

    /** The save eApp holder. */
    @Autowired
    @Qualifier("eAppSaveMapping")
    private LifeEngageSmooksHolder saveEAppHolder;

    /** The save eApp holder. */
    @Autowired
    @Qualifier("eAppRetrieveMapping")
    private LifeEngageSmooksHolder retrieveEAppHolder;

    /** The retrieve e app list holder. */
    @Autowired
    @Qualifier("eAppRetrieveListMapping")
    private LifeEngageSmooksHolder retrieveEAppListHolder;
    
    /** The save observation holder. */
    @Autowired
    @Qualifier("observationInfoSaveMapping")
    private LifeEngageSmooksHolder saveObservationHolder;

    // @Autowired
    // @Qualifier("productServiceMapping")

    /** The audit repository. */
    @Autowired
    private AuditRepository auditRepository;

    /** The rule repository. */
    @Autowired
    private RulesRepository rulesRepository;

    /** The Observation Repository. */
    @Autowired
    private ObservationRepository observationRepository;

    /** The rule. */
    @Value("${le.platform.service.runSTPRules.rule}")
    private String rule;

    /** The client id. */
    @Value("${le.platform.service.runSTPRules.clientId}")
    private String clientId;

    /** The category. */
    @Value("${le.platform.service.runSTPRules.category}")
    private String category;

    /** The Constant SUCCESS_MESSAGE_SAVE. */
    private static final String SUCCESS_MESSAGE_SAVE = "Proposal Saved";

    /** The Constant SUCCESS_MESSAGE_UPDATE. */
    private static final String SUCCESS_MESSAGE_UPDATE = "Proposal Updated";

    /** The Constant SUCCESS_MESSAGE_SAVE_OBS. */
    private static final String SUCCESS_MESSAGE_SAVE_OBS = "Observation Saved";

    /** The Constant REJECTED_MESSAGE. */
    private static final String REJECTED_MESSAGE = "Proposal Rejected";

    /** The Constant TRANSACTION_DATA. */
    private static final String TRANSACTION_DATA = "TransactionData";

    /** The Constant DATEFORMAT_YYYY_MM_DD_HH_MM_SS. */
    public static final String DATEFORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd hh:mm:ss";

    /** The Constant STP_Pass. */
    private static final String STP_PASS = "Pass";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The e app repository. */
    @Autowired
    private EAppRepository eAppRepository;

    /** The illustration repository. */
    @Autowired
    private IllustrationRepository illustrationRepository;

    /** The id generator. */
    @Autowired
    @Qualifier("idGenerator")
    private TrackingIdGenerator idGenerator;

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageEAppComponentImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageEAppComponent#saveEApp(java .lang.String,
     * com.cognizant.insurance.request.vo.RequestInfo, com.cognizant.insurance.audit.LifeEngageAudit)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String saveEApp(final String json, final RequestInfo requestInfo, final LifeEngageAudit lifeEngageAudit) {
        LOGGER.trace("Inside LifeEngageEApp component saveEApp json: json" + json);
        String eAppResponse;
        Transactions transactions = new Transactions();
        String successMessage;
        String offlineIdentifier = "";
        Boolean isDeleteRequest = Boolean.FALSE;
        try {
            long initial_time = System.currentTimeMillis();
            LifeEngageComponentHelper.parseTrasactionDetails(json, transactions, false);

            LOGGER.trace("IInside LifeEngageEApp component saveEApp...transactions.getgetLastSyncDate()"
                    + transactions.getLastSyncDate());
            // Store to a temp variable to set it back to response.
            offlineIdentifier = transactions.getOfflineIdentifier();
            // Do not process eApp data if already done
            if (auditRepository.isTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                throwSystemException(true, ErrorConstants.LE_SYNC_ERR_102, Constants.PROCESSED_TRANSACTION);
            }
            if (transactions.getStatus() != null
                    && transactions.getStatus().equals(AgreementStatusCodeList.Cancelled.toString())) {
                isDeleteRequest = Boolean.TRUE;
            }


            long start_time = System.currentTimeMillis();
            transactions = (Transactions) saveEAppHolder.parseJson(json);
            long end_time = System.currentTimeMillis();
            long difference = end_time-start_time;
            LOGGER.info("*****Eapp Smooks Time in Millis ******"+difference);

          
            List<Transactions> transactions2=new ArrayList<Transactions>();
            transactions2.add(transactions);
            LOGGER.trace("after saveEAppHolder.parseJson ...transactions.getLastSyncDate()"
                    + transactions.getLastSyncDate());
            if (isDeleteRequest) {
                transactions.setStatus(AgreementStatusCodeList.Cancelled.toString());
            }
            // Fetching agreement By transTracking Id - Fix for duplicate Eapp when key4 not returned properly
            InsuranceAgreement agreement = null;
            if (null != transactions.getTransTrackingId()) {
                agreement =
                        eAppRepository.retrieveEappByTransTrackingId(transactions.getTransTrackingId(),
                                transactions.getAgentId(), requestInfo.getTransactionId());
            }
            if (agreement != null  || StringUtils.isNotEmpty(transactions.getProposalNumber())) {
            	if(agreement != null){
                transactions.setProposalNumber(String.valueOf(agreement.getIdentifier()));
            	}
                transactions.setMode(Constants.STATUS_UPDATE);
                successMessage = SUCCESS_MESSAGE_UPDATE;
            } else {
                transactions.setProposalNumber(transactions.getAgentId()+String.valueOf(idGenerator.generate(E_APP)));
                transactions.setMode(Constants.STATUS_SAVE);
                successMessage = SUCCESS_MESSAGE_SAVE;
            }

            // Run STP rules on final submit
            executeStpRules(transactions, json);

            onBeforeSave(transactions, requestInfo);

            final String proposalstatus = eAppRepository.saveEApp(transactions, requestInfo, agreement);

            onAfterSave(transactions);
            if (Constants.REJECTED.equals(proposalstatus)) {
                successMessage = REJECTED_MESSAGE;
            }

            LifeEngageComponentHelper.createResponseStatus(transactions, proposalstatus, successMessage, null);

            transactions.setOfflineIdentifier(offlineIdentifier);

            auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit,"");
            eAppResponse = saveEAppHolder.parseBO(transactions);
            end_time = System.currentTimeMillis();
            difference = end_time-initial_time;
            LOGGER.info("*****Eapp Complete Save Time in Millis ******"+difference);

            
        } catch (DaoException e) {
        	
            LOGGER.error("DaoException", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_104);
            eAppResponse = saveEAppHolder.parseBO(transactions);

        } catch (BusinessException e) {
            LOGGER.error("BusinessException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_103);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
            eAppResponse = saveEAppHolder.parseBO(transactions);
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || "".equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
						ErrorConstants.LE_SYNC_ERR_105);
			} else if (ErrorConstants.LE_SYNC_ERR_102.equals(e.getErrorCode())) {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						LifeEngageComponentHelper.SUCCESS,
						SUCCESS_MESSAGE_SAVE, null);

			} else {
				LifeEngageComponentHelper.createResponseStatus(transactions,
						Constants.FAILURE, e.getMessage(), e.getErrorCode());
			}
            eAppResponse = saveEAppHolder.parseBO(transactions);
        } catch (Exception e) {
            LOGGER.error("Exception", e);
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                    ErrorConstants.LE_SYNC_ERR_100);
            eAppResponse = saveEAppHolder.parseBO(transactions);
        }
        LOGGER.trace("Inside LifeEngageEApp component eAppResponse : eAppResponse" + eAppResponse);
        return eAppResponse;
    }

    /**
     * Execute stp rules.
     * 
     * @param transactions
     *            the transactions
     * @param transactionData
     *            the transaction data
     * @throws BusinessException
     *             the business exception
     */
    protected void executeStpRules(Transactions transactions, String transactionData) throws BusinessException {
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) transactions.getProposal();
        try {
            if (insuranceAgreement.getIsFinalSubmit() != null && insuranceAgreement.getIsFinalSubmit()) {
                final String jsonOutput = runSTPRules(transactionData);
                final JSONObject jsonObject = new JSONObject(jsonOutput);
                transactions.setSTPStatus(jsonObject.getString(Constants.STPSTATUS));
                if (!(STP_PASS.equals(transactions.getSTPStatus()))) {
                    insuranceAgreement.setIsFinalSubmit(false);
                }
                final JSONObject temp = jsonObject.getJSONObject(Constants.VALIDATION_RESULTS);
                final Iterator<String> iter = temp.keys();
                final List<String> myList = new ArrayList<String>();
                while (iter.hasNext()) {
                    myList.add(temp.getString(iter.next().toString()));
                }

                transactions.setValidationResults(org.springframework.util.StringUtils.collectionToDelimitedString(
                        myList, ",\n", "\"", "\""));
            }
        } catch (ParseException e) {
            throwSystemException(true, ErrorConstants.LE_SYNC_ERR_101, e.getMessage(), e);
        } catch (Exception e) {
            throwBusinessException(true, ErrorConstants.LE_SYNC_ERR_103, "Unable to run STP rules : " + e.getMessage(),
                    e);
        }
    }

    /**
     * Manage transaction data before saving.
     * 
     * @param transactions
     *            the transactions
     * @param requestInfo
     *            the request info
     * @throws ParseException
     *             the parse exception
     */
    protected void onBeforeSave(final Transactions transactions, final RequestInfo requestInfo) throws ParseException {

        // illustration retreive

        if (StringUtils.isNotEmpty(transactions.getIllustrationId())) {
            String transactionId = requestInfo.getTransactionId();
            final Agreement illustrationResult =
                    illustrationRepository.getIllustrationAgreement(transactions, transactionId);

            Set<Agreement> refAgreements = transactions.getProposal().getReferencedAgreement();
            if (refAgreements == null) {
                refAgreements = new HashSet<Agreement>();
                transactions.getProposal().setReferencedAgreement(refAgreements);
            }

            refAgreements.add(illustrationResult);
            transactions.getProposal().setReferencedAgreement(refAgreements);
        }

        transactions.setCreationDateTime(LifeEngageComponentHelper.getCurrentdate());
        transactions.setModifiedTimeStamp(LifeEngageComponentHelper.getCurrentdate());
    }

    /**
     * Will execute in between Save operation and freemarker parsing.
     * 
     * @param transactions
     *            the transactions
     */
    protected void onAfterSave(Transactions transactions) {

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageEAppComponent#retrieveEApp
     * (com.cognizant.insurance.request.vo.RequestInfo , org.json.JSONObject)
     */
    @Override
    public String retrieveEApp(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
            ParseException {
        String response = null;
        final List<Transactions> transactionsList = new ArrayList<Transactions>();
        if (jsonObj.has(LifeEngageComponentHelper.KEY_4) && jsonObj.getString(LifeEngageComponentHelper.KEY_4) != null
                && !"".equals(jsonObj.getString(LifeEngageComponentHelper.KEY_4))
                && !jsonObj.getString(LifeEngageComponentHelper.KEY_4).equals(Constants.NULL)) {
            Transactions responseTransactions = null;
            final Transactions requestPayload = (Transactions) retrieveEAppHolder.parseJson(jsonObj.toString());
            try {

                responseTransactions =
                        eAppRepository.retrieveEApp(requestPayload.getProposalNumber(), requestInfo.getTransactionId());
                LOGGER.trace("Inside LifeEngageEApp component retrieveEApp : responseTransactions"
                        + responseTransactions);

                // illustration id retrieve

                final Set<Agreement> referenceAgreementList =
                        responseTransactions.getProposal().getReferencedAgreement();
                String key3 = "";
                for (Agreement list : referenceAgreementList) {
                    if (ContextTypeCodeList.ILLUSTRATION.equals(list.getContextId())) {
                        key3 = list.getIdentifier().toString();
                    }

                }
                responseTransactions.setIllustrationId(key3);
            } catch (Exception e) {
                LOGGER.error("Unable to retrieve EApp :" + e.getMessage());
                responseTransactions = new Transactions();
                LifeEngageComponentHelper.createResponseStatus(responseTransactions, Constants.FAILURE, e.getMessage()
                        + " : " + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);
            }

            LifeEngageComponentHelper.parseTrasactionDetails(jsonObj.toString(), responseTransactions, true);
            transactionsList.add(responseTransactions);
            
            response = retrieveEAppHolder.parseBO(transactionsList);
            response = response.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
            
            LOGGER.trace("LifeEngageEAppComponent.retrieveEApp: responseTransactions" + responseTransactions);
            
            
        }
        return response;
    }

    /**
     * This function is to save the observation on calling SyncToAll(save) service with type as 'Observation'.
     * 
     * @param json
     *            the json
     * @param requestInfo
     *            the request info
     * @param lifeEngageAudit
     *            the life engage audit
     * @return the string
     */
    @Override
    @Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRES_NEW)
    public String saveToSyncObservation(final String json, final RequestInfo requestInfo,
            final LifeEngageAudit lifeEngageAudit) {

        LOGGER.info(":::: Entering saveToSyncObservation in LifeEngageEAppComponentImpl 1 : input json >>" + json);

        String fnaResponse;
        final Transactions transactions = new Transactions();
        String successMessage;
        String offlineIdentifier;
        try {
            LifeEngageComponentHelper.parseTrasactionDetails(json, transactions, false);
            // Store to a temp variable to set it back to response.
            offlineIdentifier = transactions.getOfflineIdentifier();

            // Do not process Observation data if already done
            if (auditRepository.isTransactionAlreadyProcessed(transactions, requestInfo.getTransactionId(), json)) {
                throwSystemException(true, Constants.PROCESSED_TRANSACTION);
            }

            transactions.setProposalNumber(String.valueOf(idGenerator.generate()));
            transactions.setMode(Constants.STATUS_SAVE);
            successMessage = SUCCESS_MESSAGE_SAVE_OBS;

            final JSONObject transactionJsonObject = new JSONObject(json);
            final JSONObject jsonObject = transactionJsonObject.getJSONObject(TRANSACTION_DATA);
            final Observation observation = (Observation) saveObservationHolder.parseJson(jsonObject.toString());
            observationRepository.saveObservations(observation);

            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.SUCCESS);
            statusData.setStatusCode(LifeEngageComponentHelper.SUCCESS_CODE);
            statusData.setStatusMessage(successMessage);
            transactions.setStatusData(statusData);
            transactions.setStatus(LifeEngageComponentHelper.SUCCESS);
            transactions.setOfflineIdentifier(offlineIdentifier);
            auditRepository.savelifeEngagePayloadAudit(transactions, requestInfo.getTransactionId(), json,
                    lifeEngageAudit,"");
            fnaResponse = saveObservationHolder.parseBO(transactions);

        } catch (DaoException e) {
            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.FAILURE);
            statusData.setStatusCode(ErrorConstants.LE_SYNC_ERR_104);
            statusData.setStatusMessage(e.getMessage() + " : " + getExceptionMessage(e));
            transactions.setStatusData(statusData);
            transactions.setSyncError(ErrorConstants.LE_SYNC_ERR_104);
            fnaResponse = saveObservationHolder.parseBO(transactions);

        } catch (BusinessException e) {
            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.FAILURE);
            statusData.setStatusCode(ErrorConstants.LE_SYNC_ERR_103);
            statusData.setStatusMessage(e.getMessage() + " : " + getExceptionMessage(e));
            transactions.setStatusData(statusData);
            transactions.setSyncError(ErrorConstants.LE_SYNC_ERR_103);
            fnaResponse = saveObservationHolder.parseBO(transactions);

        } catch (SystemException e) {

            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.FAILURE);
            statusData.setStatusCode(ErrorConstants.LE_SYNC_ERR_105);
            statusData.setStatusMessage(e.getMessage() + " : " + getExceptionMessage(e));
            transactions.setStatusData(statusData);
            transactions.setSyncError(ErrorConstants.LE_SYNC_ERR_105);
            fnaResponse = saveObservationHolder.parseBO(transactions);

        } catch (ParseException e) {
            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.FAILURE);
            statusData.setStatusCode(ErrorConstants.LE_SYNC_ERR_101);
            statusData.setStatusMessage(e.getMessage() + " : " + getExceptionMessage(e));
            transactions.setStatusData(statusData);
            transactions.setSyncError(ErrorConstants.LE_SYNC_ERR_101);
            fnaResponse = saveObservationHolder.parseBO(transactions);

        } catch (Exception e) {
            final StatusData statusData = new StatusData();
            statusData.setStatus(LifeEngageComponentHelper.FAILURE);
            statusData.setStatusCode(ErrorConstants.LE_SYNC_ERR_100);
            statusData.setStatusMessage(e.getMessage() + " : " + getExceptionMessage(e));
            transactions.setStatusData(statusData);
            transactions.setSyncError(ErrorConstants.LE_SYNC_ERR_100);
            fnaResponse = saveObservationHolder.parseBO(transactions);
        }
        LOGGER.info(":::: Exiting saveToSyncObservation in LifeEngageEAppComponentImpl 1 : fnaResponse>>" + fnaResponse);
        return fnaResponse;
    }

    /**
     * This function is to saveObseravtion dtls by calling the saveObservation service direct.
     * 
     * @param json
     *            the json
     */
    @Override
    public void saveObservations(final String json) {

        try {
            final Observation observation = (Observation) saveObservationHolder.parseJson(json);
            observationRepository.saveObservations(observation);

        } catch (BusinessException e) {
            throwSystemException(true, Constants.OBSERVATION_FAILURE + getExceptionMessage(e));
        }
    }

    /**
     * Run STP rules.
     * 
     * @param inputJson
     *            the input json
     * @return the string
     */
    protected String runSTPRules(final String inputJson) {
        // Run the STP rules only on final Submit

        final RuleCriteria ruleCriteria = new RuleCriteria();
        ruleCriteria.setRule(rule);
        ruleCriteria.setClientId(clientId);
        ruleCriteria.setCategory(category);
        final String replacedInputJson = inputJson.replaceAll("\\r\\n|\\r|\\n", " ");
        String ruleResult = null;
        try {
            ruleResult = rulesRepository.executeRule(ruleCriteria, replacedInputJson);
        } catch (BusinessException e) {
            throwSystemException(true, ErrorConstants.LE_SYNC_ERR_103, e.getMessage(), e);
        } catch (Exception e) {
            throwSystemException(true, ErrorConstants.LE_SYNC_ERR_100, getExceptionMessage(e), e);
        }

        return ruleResult;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageEAppComponent#retrieveAllEApp
     * (com.cognizant.insurance.request.vo. RequestInfo, org.json.JSONObject)
     */
    @Override
    public String retrieveAllEApp(final RequestInfo requestInfo, final JSONObject jsonObj) throws BusinessException,
            ParseException {

        final Transactions transactions = (Transactions) retrieveEAppHolder.parseJson(jsonObj.toString());
        boolean fullFetch = true;
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        if (jsonObj.has(LifeEngageComponentHelper.KEY_10)
                && jsonObj.getString(LifeEngageComponentHelper.KEY_10) != null) {
            if (LifeEngageComponentHelper.FULL_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                // Retrieve All details for all proposals for an agent
                transactionsList = eAppRepository.retrieveAll(transactions, requestInfo, true);
            } else if (LifeEngageComponentHelper.BASIC_DETAILS.equals(jsonObj.getString(LifeEngageComponentHelper.KEY_10))) {
                // Retrieve Only the basic details required for listing
                transactions.setProposalNumber(null);
                transactionsList = eAppRepository.retrieveAll(transactions, requestInfo, false);

                fullFetch = false;
            }
        }
        LifeEngageComponentHelper.buildTransactionsList(transactionsList, jsonObj);

        String eAppMapToReturn = "{\"Transactions\":[]}";
        if (fullFetch) {
            eAppMapToReturn = retrieveEAppHolder.parseBO(transactionsList);
        } else {
            eAppMapToReturn = retrieveEAppListHolder.parseBO(transactionsList);
        }
        eAppMapToReturn = eAppMapToReturn.replaceAll(Constants.REPLACEMENT, Constants.REG_EXPRESSION);
        return eAppMapToReturn;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insurance.component.LifeEngageEAppComponent#getStatus
     * (com.cognizant.insurance.request.vo.RequestInfo , org.json.JSONObject)
     */
    @Override
    public String getStatus(final RequestInfo requestInfo, final String proposalNo, final String type)
            throws BusinessException, ParseException {
        String saveStatusResponse = null;
        Transactions transactions = new Transactions();
        final StatusData statusData = new StatusData();
        try {

            if (proposalNo != null && proposalNo.length() > 0) {
                final Transactions responseTransactions =
                        eAppRepository.getStatus(proposalNo, requestInfo.getTransactionId());
                AgreementStatusCodeList statuscode = responseTransactions.getProposal().getStatusCode();
                responseTransactions.setStatus(statuscode.toString());
                if (type != null && type.length() > 0) {
                    responseTransactions.setType(type);
                }

                statusData.setStatus(LifeEngageComponentHelper.VALID);
                statusData.setStatusCode(LifeEngageComponentHelper.VALID_CODE);
                statusData.setStatusMessage(LifeEngageComponentHelper.VALID_PROPOSAL_MESSAGE);

                responseTransactions.setStatusData(statusData);
                saveStatusResponse = getStatusHolder.parseBO(responseTransactions);
                LOGGER.trace("LifeEngageEAppComponent.getStatus: saveStatusResponse" + saveStatusResponse);

            } else {
                statusData.setStatus(LifeEngageComponentHelper.INVALID);
                statusData.setStatusCode(LifeEngageComponentHelper.INVALID_CODE);
                statusData.setStatusMessage(LifeEngageComponentHelper.INVALID_PROPOSAL_MESSAGE);
                transactions.setStatusData(statusData);
                transactions.setProposalNumber(proposalNo);
                transactions.setType(type);
                saveStatusResponse = getStatusHolder.parseBO(transactions);

            }
        } catch (Exception e) {
            statusData.setStatus(LifeEngageComponentHelper.INVALID);
            statusData.setStatusCode(LifeEngageComponentHelper.INVALID_CODE);
            statusData.setStatusMessage(LifeEngageComponentHelper.INVALID_PROPOSAL_MESSAGE);
            transactions.setStatusData(statusData);
            transactions.setProposalNumber(proposalNo);
            transactions.setType(type);
            saveStatusResponse = getStatusHolder.parseBO(transactions);

        }

        LOGGER.trace("LifeEngageEAppComponent.getStatus: saveStatusResponse" + saveStatusResponse);
        return saveStatusResponse;
    }

    /**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }

    /**
     * Gets the save e app holder.
     * 
     * @return the saveEAppHolder
     */
    public final LifeEngageSmooksHolder getSaveEAppHolder() {
        return saveEAppHolder;
    }

    /**
     * Sets the save e app holder.
     * 
     * @param saveEAppHolder
     *            the saveEAppHolder to set
     */
    public final void setSaveEAppHolder(final LifeEngageSmooksHolder saveEAppHolder) {
        this.saveEAppHolder = saveEAppHolder;
    }

    /**
     * Gets the retrieve e app holder.
     * 
     * @return the retrieveEAppHolder
     */
    public final LifeEngageSmooksHolder getRetrieveEAppHolder() {
        return retrieveEAppHolder;
    }

    /**
     * Sets the retrieve e app holder.
     * 
     * @param retrieveEAppHolder
     *            the retrieveEAppHolder to set
     */
    public final void setRetrieveEAppHolder(final LifeEngageSmooksHolder retrieveEAppHolder) {
        this.retrieveEAppHolder = retrieveEAppHolder;
    }

    /**
     * Gets the retrieve e app list holder.
     * 
     * @return the retrieveEAppListHolder
     */
    public final LifeEngageSmooksHolder getRetrieveEAppListHolder() {
        return retrieveEAppListHolder;
    }

    /**
     * Sets the retrieve e app list holder.
     * 
     * @param retrieveEAppListHolder
     *            the retrieveEAppListHolder to set
     */
    public final void setRetrieveEAppListHolder(final LifeEngageSmooksHolder retrieveEAppListHolder) {
        this.retrieveEAppListHolder = retrieveEAppListHolder;
    }

    /**
     * Gets the save observation holder.
     * 
     * @return the saveObservationHolder
     */
    public final LifeEngageSmooksHolder getSaveObservationHolder() {
        return saveObservationHolder;
    }

    /**
     * Sets the save observation holder.
     * 
     * @param saveObservationHolder
     *            the saveObservationHolder to set
     */
    public final void setSaveObservationHolder(final LifeEngageSmooksHolder saveObservationHolder) {
        this.saveObservationHolder = saveObservationHolder;
    }

    /**
     * Gets the audit repository.
     * 
     * @return the auditRepository
     */
    public final AuditRepository getAuditRepository() {
        return auditRepository;
    }

    /**
     * Sets the audit repository.
     * 
     * @param auditRepository
     *            the auditRepository to set
     */
    public final void setAuditRepository(final AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    /**
     * Gets the rules repository.
     * 
     * @return the rulesRepository
     */
    public final RulesRepository getRulesRepository() {
        return rulesRepository;
    }

    /**
     * Sets the rules repository.
     * 
     * @param rulesRepository
     *            the rulesRepository to set
     */
    public final void setRulesRepository(final RulesRepository rulesRepository) {
        this.rulesRepository = rulesRepository;
    }

    /**
     * Gets the observation repository.
     * 
     * @return the observationRepository
     */
    public final ObservationRepository getObservationRepository() {
        return observationRepository;
    }

    /**
     * Sets the observation repository.
     * 
     * @param observationRepository
     *            the observationRepository to set
     */
    public final void setObservationRepository(final ObservationRepository observationRepository) {
        this.observationRepository = observationRepository;
    }

    /**
     * Gets the e app repository.
     * 
     * @return the eAppRepository
     */
    public final EAppRepository getEAppRepository() {
        return eAppRepository;
    }

    /**
     * Sets the e app repository.
     * 
     * @param eAppRepository
     *            the eAppRepository to set
     */
    public final void setEAppRepository(final EAppRepository eAppRepository) {
        this.eAppRepository = eAppRepository;
    }

}
