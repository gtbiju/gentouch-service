/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 301350
 * @version       : 0.1, Nov 29, 2012
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * List of Illustration Type Codes.
 * 
 * 
 * 
 * 
 */
public enum IllustrationTypeCodesList {
    /**
     * 
     * 
     The purpose of this illustration is for both New Business and In Force
     * purposes.
     * 
     * 
     */
    Both,
    /**
     * 
     * 
     The purpose for this illustration is for In Force purposes.
     * 
     * 
     */
    InForce,
    /**
     * 
     * 
     The purpose of this illustration is for new business.
     * 
     * 
     **/
    NewBusiness,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * 
     *
     * 
     * 
     */
    Unknown,

}
