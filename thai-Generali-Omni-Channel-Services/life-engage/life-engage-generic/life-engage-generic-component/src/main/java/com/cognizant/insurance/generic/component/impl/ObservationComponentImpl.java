/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Aug 8, 2013
 */
package com.cognizant.insurance.generic.component.impl;

import com.cognizant.insurance.generic.component.ObservationComponent;
import com.cognizant.insurance.generic.dao.ObservationDao;
import com.cognizant.insurance.generic.dao.impl.ObservationDaoImpl;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;

/**
 * The Class class ObservationComponentImpl.
 *
 * @author 300797
 */
public class ObservationComponentImpl implements ObservationComponent {

    /* (non-Javadoc)
     * @see com.cognizant.insurance.audit.component.AuditComponent#savelifeEngageAudit(com.cognizant.insurance.request.Request)
     */
    @Override
    public <T> Response<Object> saveObservations(Request<T> request) {
        final ObservationDao observationDao = new ObservationDaoImpl();
        observationDao.save(request);
        
        final Response<Object> response = new ResponseImpl<Object>();
        response.setType(request.getType());
        
        return response;
    }
   
}
