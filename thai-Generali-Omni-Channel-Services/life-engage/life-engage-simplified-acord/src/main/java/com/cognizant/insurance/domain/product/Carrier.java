/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 304007
 * @version       : 0.1, Mar 13, 2013
 */
package com.cognizant.insurance.domain.product;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class class Carriers.
 */

@Entity
@Table(name = "CORE_CARRIER")
@NamedQueries({
	 @NamedQuery(name = "Carrier.retrieveCarriers", query = "select carrier from Carrier carrier where (:id is null OR id = :id)")
})
public class Carrier implements Serializable {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = -2302492032789748107L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    /** The description. */
    @Column(name="DESCRIPTION")
    private String description;
    
    /** The product specifications. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.ALL },mappedBy = "carrier")
    private Set<ProductSpecification> productSpecifications;
    

    /**
     * Gets the productSpecifications.
     *
     * @return Returns the productSpecifications.
     */
    public Set<ProductSpecification> getProductSpecifications() {
        return productSpecifications;
    }

    /**
     * Sets The productSpecifications.
     *
     * @param productSpecifications The productSpecifications to set.
     */
    public void setProductSpecifications(
            final Set<ProductSpecification> productSpecifications) {
        this.productSpecifications = productSpecifications;
    }

    /**
     * Sets The description.
     *
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the description.
     *
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets The id.
     *
     * @param id The id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the id.
     *
     * @return Returns the id.
     */
    public Long getId() {
        return id;
    }
}
