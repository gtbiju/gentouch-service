package com.cognizant.insurance.omnichannel.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity

public class GeneraliSPAJNo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GenericGenerator(name = "UniqueSPAJGenerator", strategy = "com.cognizant.insurance.omnichannel.domain.SPAJGenerator", 
		    parameters = { @Parameter(name = "prefix", value = "02"),
			@Parameter(name="jumpOn",value="9999999"),@Parameter(name="seqId",value="eAppSpajNo"),
			@Parameter(name="tableName",value="SPAJIDGen"),@Parameter(name="maskLength",value="7") })
	@GeneratedValue(generator = "UniqueSPAJGenerator")
	private String spajNo;

	private String agentId;
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date allocationDate;

	public String getSpajNo() {
		return spajNo;
	}

	public void setSpajNo(String spajNo) {
		this.spajNo = spajNo;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Date getAllocationDate() {
		return allocationDate;
	}

	public void setAllocationDate(Date allocationDate) {
		this.allocationDate = allocationDate;
	}

}
