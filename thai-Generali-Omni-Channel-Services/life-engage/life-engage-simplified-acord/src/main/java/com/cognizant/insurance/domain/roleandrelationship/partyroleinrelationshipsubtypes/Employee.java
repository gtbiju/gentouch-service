/**
 * 
 */
package com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.cognizant.insurance.domain.commonelements.complexdatatypes.CurrencyAmount;
import com.cognizant.insurance.domain.roleandrelationship.partyrolerelationshipsubtypes.EmploymentRelationship;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists.JobTitleCodeList;
import com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists.ProficiencyLevelCodeList;
import com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists.SalaryGradeCodeList;

/**
 * The role played by the person who is employed in an employment agreement by
 * an employer (even if the employment agreement itself is not instantiated). An
 * employee is not considered a contractor.
 * 
 * e.g: John Doe, as the employee of a company under an employment agreement
 * starting 06/11/95
 * 
 * @author 301350
 * 
 * 
 */

@Entity
public class Employee extends PartyRoleInRelationship {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1165908345945699569L;

    /**
     * The total taxable monetary amount that the employee receives annually in
     * the context of his employment agreement.
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private CurrencyAmount annualTaxableBenefitAmount;

    /**
     * The fixed salary an employee receives in the context of his employment
     * agreement.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private CurrencyAmount baseSalaryAmount;

    /**
     * The bonus salary an employee receives over and above the base salary in
     * the context of his employment agreement. 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private CurrencyAmount bonusSalaryAmount;

    /**
     * The textual description of the job the employee executes under the terms
     * of his employment agreement. 
     * 
     * 
     * 
     */
    private String jobDescription;

    /**
     * The overall skill level of the employee.
     * 
     * e.g: Professional
     * 
     * e.g: Semi-skilled
     * 
     * e.g: Skilled
     * 
     * e.g: Unskilled
     * 
     * 
     * 
     */
    @Enumerated
    private ProficiencyLevelCodeList skillLevelCode;

    /**
     * The job title of the position held by the employee.
     * 
     * e.g: Chief Executive Officer
     * 
     * e.g: Clerk
     * 
     * e.g: Managing Director
     * 
     * e.g: Marketing Manager
     * 
     * 
     * 
     */
    @Enumerated
    private JobTitleCodeList jobTitleCode;

    /**
     * Indicates whether the employee has a job that gives him rights to a
     * pension.
     * 
     * 
     * 
     */
    private Boolean pensionableIndicator;

    /**
     * Indicates whether the employee is exempt or non-exempt.
     * 
     * In the USA, this is associated with being paid on a basis that is not
     * subject to the 40 hour work week, minimum wage, and overtime rules. Such
     * jobs are therefore considered "exempt". Conversely, jobs that are
     * non-exempt pay salaries subject to concepts such as time worked, overtime
     * rules, etc.
     * 
     * SOURCES:
     * http://blogs.payscale.com/ask_dr_salary/2007/01/hourly_wage_vs_.html
     * 
     * http://en.wikipedia.org/wiki/Salary
     * 
     * 
     * 
     * 
     * 
     * 
     */
    private Boolean exemptIndicator;

    /**
     * Indicates the salary or wage level of an employee.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private SalaryGradeCodeList salaryGradeCode;

    /**
     * This relationship links an employment to the related employee.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private Set<EmploymentRelationship> employment;

    /**
     * Gets the annualTaxableBenefitAmount.
     * 
     * @return Returns the annualTaxableBenefitAmount.
     */
    public CurrencyAmount getAnnualTaxableBenefitAmount() {
        return annualTaxableBenefitAmount;
    }

    /**
     * Gets the baseSalaryAmount.
     * 
     * @return Returns the baseSalaryAmount.
     */
    public CurrencyAmount getBaseSalaryAmount() {
        return baseSalaryAmount;
    }

    /**
     * Gets the bonusSalaryAmount.
     * 
     * @return Returns the bonusSalaryAmount.
     */
    public CurrencyAmount getBonusSalaryAmount() {
        return bonusSalaryAmount;
    }

    /**
     * Gets the jobDescription.
     * 
     * @return Returns the jobDescription.
     */
    public String getJobDescription() {
        return jobDescription;
    }

    /**
     * Gets the skillLevelCode.
     * 
     * @return Returns the skillLevelCode.
     */
    public ProficiencyLevelCodeList getSkillLevelCode() {
        return skillLevelCode;
    }

    /**
     * Gets the jobTitleCode.
     * 
     * @return Returns the jobTitleCode.
     */
    public JobTitleCodeList getJobTitleCode() {
        return jobTitleCode;
    }

    /**
     * Gets the pensionableIndicator.
     * 
     * @return Returns the pensionableIndicator.
     */
    public Boolean getPensionableIndicator() {
        return pensionableIndicator;
    }

    /**
     * Gets the exemptIndicator.
     * 
     * @return Returns the exemptIndicator.
     */
    public Boolean getExemptIndicator() {
        return exemptIndicator;
    }

    /**
     * Gets the salaryGradeCode.
     * 
     * @return Returns the salaryGradeCode.
     */
    public SalaryGradeCodeList getSalaryGradeCode() {
        return salaryGradeCode;
    }

    /**
     * Gets the employment.
     * 
     * @return Returns the employment.
     */
    public Set<EmploymentRelationship> getEmployment() {
        return employment;
    }

    /**
     * Sets The annualTaxableBenefitAmount.
     * 
     * @param annualTaxableBenefitAmount
     *            The annualTaxableBenefitAmount to set.
     */
    public void setAnnualTaxableBenefitAmount(
            final CurrencyAmount annualTaxableBenefitAmount) {
        this.annualTaxableBenefitAmount = annualTaxableBenefitAmount;
    }

    /**
     * Sets The baseSalaryAmount.
     * 
     * @param baseSalaryAmount
     *            The baseSalaryAmount to set.
     */
    public void setBaseSalaryAmount(final CurrencyAmount baseSalaryAmount) {
        this.baseSalaryAmount = baseSalaryAmount;
    }

    /**
     * Sets The bonusSalaryAmount.
     * 
     * @param bonusSalaryAmount
     *            The bonusSalaryAmount to set.
     */
    public void setBonusSalaryAmount(final CurrencyAmount bonusSalaryAmount) {
        this.bonusSalaryAmount = bonusSalaryAmount;
    }

    /**
     * Sets The jobDescription.
     * 
     * @param jobDescription
     *            The jobDescription to set.
     */
    public void setJobDescription(final String jobDescription) {
        this.jobDescription = jobDescription;
    }

    /**
     * Sets The skillLevelCode.
     * 
     * @param skillLevelCode
     *            The skillLevelCode to set.
     */
    public void setSkillLevelCode(
            final ProficiencyLevelCodeList skillLevelCode) {
        this.skillLevelCode = skillLevelCode;
    }

    /**
     * Sets The jobTitleCode.
     * 
     * @param jobTitleCode
     *            The jobTitleCode to set.
     */
    public void setJobTitleCode(final JobTitleCodeList jobTitleCode) {
        this.jobTitleCode = jobTitleCode;
    }

    /**
     * Sets The pensionableIndicator.
     * 
     * @param pensionableIndicator
     *            The pensionableIndicator to set.
     */
    public void
            setPensionableIndicator(final Boolean pensionableIndicator) {
        this.pensionableIndicator = pensionableIndicator;
    }

    /**
     * Sets The exemptIndicator.
     * 
     * @param exemptIndicator
     *            The exemptIndicator to set.
     */
    public void setExemptIndicator(final Boolean exemptIndicator) {
        this.exemptIndicator = exemptIndicator;
    }

    /**
     * Sets The salaryGradeCode.
     * 
     * @param salaryGradeCode
     *            The salaryGradeCode to set.
     */
    public void setSalaryGradeCode(
            final SalaryGradeCodeList salaryGradeCode) {
        this.salaryGradeCode = salaryGradeCode;
    }

    /**
     * Sets The employment.
     * 
     * @param employment
     *            The employment to set.
     */
    public void
            setEmployment(final Set<EmploymentRelationship> employment) {
        this.employment = employment;
    }

}
