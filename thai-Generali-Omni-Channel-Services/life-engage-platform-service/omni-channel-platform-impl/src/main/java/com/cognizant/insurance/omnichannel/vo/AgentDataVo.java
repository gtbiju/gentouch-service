package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;

public class AgentDataVo implements Serializable {

    /**  The serialVersionUID **/
    private static final long serialVersionUID = 1L;

    /** The agentCode **/
    private String agentCode;

    /** The agentName **/
    private String agentName;

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

}
