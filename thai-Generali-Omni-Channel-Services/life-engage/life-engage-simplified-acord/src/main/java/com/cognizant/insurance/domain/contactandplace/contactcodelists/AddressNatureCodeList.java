/**
 * 
 */
package com.cognizant.insurance.domain.contactandplace.contactcodelists;

/**
 * A classification of an address.
 * 
 * @author 301350
 * 
 * Updates
 * Added Current and Permanent - 301350 24July2013
 * 
 */
public enum AddressNatureCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Mailing,
    /**
     * 
     * 
     * 
     * 
     */
    Shipping,
    /**
     * 
     * 
     * 
     * 
     */
    Residence,
    /**
     * Added for EApp
     * 
     * 
     * 
     */
    Current,
    /**
     * Added for EApp
     * 
     * 
     * 
     */
    Permanent,
    /**
     * Added for Illustration
     * 
     * 
     * 
     */
    Birth,
    /**
     * Added for EApp
     * 
     * 
     * 
     */
    Office
}
