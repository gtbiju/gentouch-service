package com.cognizant.insurance.omnichannel.utils;

/**
 * @author 471501
 * 
 *         The Class BenefitIllustrationTable.
 * 
 *         This class is a POJO for Benefit illustration table in illustration PDF.
 */

public class PolicyValueTableData {

    /** The policy year. */
    private String policyYear;
    
    /*** The Age*/
    private String age;
    
    /*** The Re-insurance Premiums*/
    private String cashValue;
    
    /*** The Policy Applies - Instant payout*/
    private String rpuCash;
    
    /*** The Policy Applies - Maturity*/
    private String rpuSA;

    /*** The Extended Period in Year*/
    private String extPeriodYear;
    
    /*** The Extended Period in Day*/
    private String extPeriodDay;
    
	/*** The Policy Applies - instant Payout*/
    private String etiCash;
    
    /*** The Policy Applies - Maturity*/
    private String etiSA;
    
    private Boolean isGenProLife20;
    
    private Boolean isGenProLife25;
    
   public String getPolicyYear() {
        return policyYear;
    }

   public void setPolicyYear(String policyYear) {
        this.policyYear = policyYear;
    }

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getCashValue() {
		return cashValue;
	}

	public void setCashValue(String cashValue) {
		this.cashValue = cashValue;
	}

	public String getRpuCash() {
		return rpuCash;
	}

	public void setRpuCash(String rpuCash) {
		this.rpuCash = rpuCash;
	}

	public String getRpuSA() {
		return rpuSA;
	}

	public void setRpuSA(String rpuSA) {
		this.rpuSA = rpuSA;
	}

	public String getExtPeriodYear() {
		return extPeriodYear;
	}

	public void setExtPeriodYear(String extPeriodYear) {
		this.extPeriodYear = extPeriodYear;
	}

	public String getExtPeriodDay() {
		return extPeriodDay;
	}

	public void setExtPeriodDay(String extPeriodDay) {
		this.extPeriodDay = extPeriodDay;
	}

	public String getEtiCash() {
		return etiCash;
	}

	public void setEtiCash(String etiCash) {
		this.etiCash = etiCash;
	}

	public String getEtiSA() {
		return etiSA;
	}

	public void setEtiSA(String etiSA) {
		this.etiSA = etiSA;
	}

	public Boolean getIsGenProLife20() {
		return isGenProLife20;
	}

	public void setIsGenProLife20(Boolean isGenProLife20) {
		this.isGenProLife20 = isGenProLife20;
	}

	public Boolean getIsGenProLife25() {
		return isGenProLife25;
	}

	public void setIsGenProLife25(Boolean isGenProLife25) {
		this.isGenProLife25 = isGenProLife25;
	}

    }
