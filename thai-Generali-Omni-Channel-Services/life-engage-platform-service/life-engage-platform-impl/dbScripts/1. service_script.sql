CREATE TABLE [dbo].[Account](
	[ACCOUNT_TYPE] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[accountIdentifier] [bigint] NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[statusCode] [int] NULL,
	[statusDate] [date] NULL,
	[statusReasonCode] [int] NULL,
	[thirdPartyIndicator] [bit] NULL,
	[validityStatusCode] [int] NULL,
	[bankAccountTypeCode] [int] NULL,
	[bankCheckIdentifier] [int] NULL,
	[bankCode] [varchar](255) NULL,
	[bankName] [varchar](255) NULL,
	[branchCode] [varchar](255) NULL,
	[branchName] [varchar](255) NULL,
	[countryCode] [varchar](255) NULL,
	[ibanCheckIdentifier] [int] NULL,
	[balanceAmount_id] [bigint] NULL,
	[currencyCode_id] [bigint] NULL,
	[effecitvePeriod_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Account_Party]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account_Party](
	[Account_id] [bigint] NOT NULL,
	[owner_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Account_id] ASC,
	[owner_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[owner_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ACHIEVEMENT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACHIEVEMENT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[achievement_code] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[title] [varchar](255) NULL,
	[agent_id] [int] NOT NULL,
	[text] [varchar](255) NULL,
	[agentId] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AGENT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AGENT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[agentCode] [varchar](255) NULL,
	[briefWriteUp] [varchar](255) NULL,
	[business_sourced] [bigint] NULL,
	[email_id] [varchar](255) NULL,
	[emp_type] [varchar](255) NULL,
	[fullname] [varchar](255) NULL,
	[agent_group] [varchar](255) NULL,
	[license_exp_date] [datetime2](7) NULL,
	[license_issue_date] [datetime2](7) NULL,
	[license_num] [varchar](255) NULL,
	[mob_num] [varchar](255) NULL,
	[name_of_excelsheet] [varchar](255) NULL,
	[num_of_service] [bigint] NULL,
	[office] [varchar](255) NULL,
	[role] [varchar](255) NULL,
	[supervisor_code] [varchar](255) NULL,
	[unit] [varchar](255) NULL,
	[uploaded_timestamp] [datetime2](7) NULL,
	[userid] [varchar](255) NULL,
	[yoe] [int] NULL,
	[trustFactor] [varchar](255) NULL,
	[go] [varchar](255) NULL,
	[employeeId] [varchar](255) NULL,
	[advisorCategory] [varchar](255) NULL,
	[admCode] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[agentCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AGENT_APP_LOGIN]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AGENT_APP_LOGIN](
	[userid] [varchar](20) NOT NULL DEFAULT (''),
	[password] [varchar](256) NULL DEFAULT (NULL),
	[active] [varchar](20) NULL DEFAULT (NULL),
	[agentcode] [varchar](255) NULL DEFAULT (NULL),
PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Agreement]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Agreement](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[identifier] [numeric](22, 0) NULL,
	[issueDate] [date] NULL,
	[name] [varchar](255) NULL,
	[reviewDate] [datetime2](7) NULL,
	[statusCode] [int] NULL,
	[statusDate] [date] NULL,
	[statusReasonCode] [int] NULL,
	[version] [varchar](255) NULL,
	[agreementExtension_id] [bigint] NULL,
	[currencyCode_id] [bigint] NULL,
	[effectivePeriod_id] [bigint] NULL,
	[isBasedOn_id] [bigint] NULL,
	[issueNation_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Agreement_Coverage]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Agreement_Coverage](
	[Agreement_id] [bigint] NOT NULL,
	[includesCoverage_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Agreement_id] ASC,
	[includesCoverage_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[includesCoverage_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Agreement_Document]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Agreement_Document](
	[Agreement_id] [bigint] NOT NULL,
	[relatedDocument_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Agreement_id] ASC,
	[relatedDocument_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[relatedDocument_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Agreement_Extension]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Agreement_Extension](
	[Agreement_id] [bigint] NOT NULL,
	[includesExtension_id] [bigint] NOT NULL,
UNIQUE NONCLUSTERED 
(
	[includesExtension_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Agreement_GoalAndNeed]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Agreement_GoalAndNeed](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[fnaID] [varchar](255) NULL,
	[illustrationID] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AGREEMENT_REFERENCEDAGREEMENT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AGREEMENT_REFERENCEDAGREEMENT](
	[AGREEMENT_ID] [bigint] NOT NULL,
	[REFERENCEDAGREEMENT_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AGREEMENT_ID] ASC,
	[REFERENCEDAGREEMENT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AGREEMENT_REPLACEDAGREEMENT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AGREEMENT_REPLACEDAGREEMENT](
	[AGREEMENT_ID] [bigint] NOT NULL,
	[REPLACEDAGREEMENT_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AGREEMENT_ID] ASC,
	[REPLACEDAGREEMENT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AgreementExtension]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgreementExtension](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[policyNumber] [bigint] NULL,
	[illustrationPolicyNumber] [varchar](255) NULL,
	[language] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AgreementLoan]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgreementLoan](
	[accountDescription] [varchar](255) NULL,
	[annualInterestRatePercentage] [float] NULL,
	[annualOutstandingRatePercentage] [float] NULL,
	[issueAge] [int] NULL,
	[loanInterestMethodCode] [int] NULL,
	[loanInterestRateTypeCode] [int] NULL,
	[loanInterestTimingCode] [int] NULL,
	[loanOfAgreementValuePercentage] [float] NULL,
	[loanReasonTypeCode] [int] NULL,
	[loanRepayNumYear] [int] NULL,
	[loanStatusCode] [int] NULL,
	[loanTypeCode] [int] NULL,
	[natureOfLoan] [varchar](255) NULL,
	[nextDueDate] [date] NULL,
	[paymentFrequencyCode] [int] NULL,
	[paymentMeansCode] [int] NULL,
	[required] [bit] NULL,
	[id] [bigint] NOT NULL,
	[loanAmount_id] [bigint] NULL,
	[loanPeriod_id] [bigint] NULL,
	[relatedAgreement_id] [bigint] NULL,
	[unitOfLoanPeriod_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AgreementWithdrawal]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgreementWithdrawal](
	[paymentFrequencyCode] [int] NULL,
	[withdrawalFrequency] [int] NULL,
	[withdrawalTerm] [int] NULL,
	[withdrawalType] [varchar](255) NULL,
	[id] [bigint] NOT NULL,
	[relatedAgreement_id] [bigint] NULL,
	[withdrawalAmount_id] [bigint] NULL,
	[withdrawalPeriod_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[APPLICATION_CONTEXTS]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[APPLICATION_CONTEXTS](
	[Id] [bigint] NOT NULL,
	[Context] [varchar](255) NULL DEFAULT (NULL),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BATCH_JOB_EXECUTION]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BATCH_JOB_EXECUTION](
	[JOB_EXECUTION_ID] [bigint] NOT NULL,
	[VERSION] [bigint] NULL,
	[JOB_INSTANCE_ID] [bigint] NOT NULL,
	[CREATE_TIME] [datetime] NOT NULL,
	[START_TIME] [datetime] NULL DEFAULT (NULL),
	[END_TIME] [datetime] NULL DEFAULT (NULL),
	[STATUS] [varchar](10) NULL,
	[EXIT_CODE] [varchar](2500) NULL,
	[EXIT_MESSAGE] [varchar](2500) NULL,
	[LAST_UPDATED] [datetime] NULL,
	[JOB_CONFIGURATION_LOCATION] [varchar](2500) NULL,
PRIMARY KEY CLUSTERED 
(
	[JOB_EXECUTION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BATCH_JOB_EXECUTION_CONTEXT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BATCH_JOB_EXECUTION_CONTEXT](
	[JOB_EXECUTION_ID] [bigint] NOT NULL,
	[SHORT_CONTEXT] [varchar](2500) NOT NULL,
	[SERIALIZED_CONTEXT] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[JOB_EXECUTION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BATCH_JOB_EXECUTION_PARAMS]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BATCH_JOB_EXECUTION_PARAMS](
	[JOB_EXECUTION_ID] [bigint] NOT NULL,
	[TYPE_CD] [varchar](6) NOT NULL,
	[KEY_NAME] [varchar](100) NOT NULL,
	[STRING_VAL] [varchar](250) NULL,
	[DATE_VAL] [datetime] NULL,
	[LONG_VAL] [bigint] NULL,
	[DOUBLE_VAL] [float] NULL,
	[IDENTIFYING] [char](1) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BATCH_JOB_EXECUTION_SEQ]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BATCH_JOB_EXECUTION_SEQ](
	[ID] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BATCH_JOB_INSTANCE]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BATCH_JOB_INSTANCE](
	[JOB_INSTANCE_ID] [bigint] NOT NULL,
	[VERSION] [bigint] NULL,
	[JOB_NAME] [varchar](100) NOT NULL,
	[JOB_KEY] [varchar](32) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[JOB_INSTANCE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [JOB_INST_UN] UNIQUE NONCLUSTERED 
(
	[JOB_NAME] ASC,
	[JOB_KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BATCH_JOB_PARAMS]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BATCH_JOB_PARAMS](
	[JOB_INSTANCE_ID] [bigint] NOT NULL,
	[TYPE_CD] [varchar](6) NOT NULL,
	[KEY_NAME] [varchar](100) NOT NULL,
	[STRING_VAL] [nvarchar](max) NULL,
	[DATE_VAL] [datetime] NULL DEFAULT (NULL),
	[LONG_VAL] [bigint] NULL,
	[DOUBLE_VAL] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BATCH_JOB_SEQ]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BATCH_JOB_SEQ](
	[ID] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BATCH_STEP_EXECUTION]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BATCH_STEP_EXECUTION](
	[STEP_EXECUTION_ID] [bigint] NOT NULL,
	[VERSION] [bigint] NOT NULL,
	[STEP_NAME] [varchar](100) NOT NULL,
	[JOB_EXECUTION_ID] [bigint] NOT NULL,
	[START_TIME] [datetime] NOT NULL,
	[END_TIME] [datetime] NULL DEFAULT (NULL),
	[STATUS] [varchar](10) NULL,
	[COMMIT_COUNT] [bigint] NULL,
	[READ_COUNT] [bigint] NULL,
	[FILTER_COUNT] [bigint] NULL,
	[WRITE_COUNT] [bigint] NULL,
	[READ_SKIP_COUNT] [bigint] NULL,
	[WRITE_SKIP_COUNT] [bigint] NULL,
	[PROCESS_SKIP_COUNT] [bigint] NULL,
	[ROLLBACK_COUNT] [bigint] NULL,
	[EXIT_CODE] [varchar](2500) NULL,
	[EXIT_MESSAGE] [varchar](2500) NULL,
	[LAST_UPDATED] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[STEP_EXECUTION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BATCH_STEP_EXECUTION_CONTEXT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BATCH_STEP_EXECUTION_CONTEXT](
	[STEP_EXECUTION_ID] [bigint] NOT NULL,
	[SHORT_CONTEXT] [varchar](2500) NOT NULL,
	[SERIALIZED_CONTEXT] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[STEP_EXECUTION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BATCH_STEP_EXECUTION_SEQ]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BATCH_STEP_EXECUTION_SEQ](
	[ID] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CODE_LOCALE_LOOKUP]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CODE_LOCALE_LOOKUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[COUNTRY] [varchar](255) NULL,
	[LANGUAGE] [varchar](255) NULL,
	[VALUE] [varchar](255) NULL,
	[LOOKUPCODE_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CODE_LOOKUP]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CODE_LOOKUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[CODE] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[CODETYPE_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CODE_RELATION]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CODE_RELATION](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[CHILD_ID] [bigint] NULL,
	[PARENT_ID] [bigint] NULL,
	[TYPE_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CODE_TYPE_LOOKUP]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CODE_TYPE_LOOKUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](255) NULL,
	[CARRIER_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Communication]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Communication](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[actualTimePeriod] [datetime2](7) NULL,
	[communicationDescription] [varchar](255) NULL,
	[communicationDirectionTypeCode] [int] NULL,
	[communicationPurposeCode] [int] NULL,
	[communicationStatusCode] [int] NULL,
	[communicationStatusDate] [date] NULL,
	[interactionType] [int] NULL,
	[isJoinCall] [bit] NULL,
	[joinCallWith] [varchar](255) NULL,
	[plannedTimePeriod] [datetime2](7) NULL,
	[priorityCode] [int] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Communication_Communication]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Communication_Communication](
	[Communication_id] [bigint] NOT NULL,
	[followsUp_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Communication_id] ASC,
	[followsUp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[followsUp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactPoint]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactPoint](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[statusCode] [int] NULL,
	[availablePeriod_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactPreference]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactPreference](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[contactInstructions] [varchar](255) NULL,
	[defaultIndicator] [bit] NULL,
	[lastValidatedDate] [date] NULL,
	[priorityLevel] [int] NULL,
	[purpose] [varchar](255) NULL,
	[purposeCode] [int] NULL,
	[redListIndicator] [bit] NULL,
	[solicitableIndicator] [bit] NULL,
	[statusCode] [int] NULL,
	[usageCode] [int] NULL,
	[validationResultCode] [int] NULL,
	[availablePeriod_id] [bigint] NULL,
	[preferredContactPoint_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CONTENT_LANGUAGES]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CONTENT_LANGUAGES](
	[id] [bigint] NOT NULL,
	[name] [varchar](60) NULL DEFAULT (NULL),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CONTENT_TYPES]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CONTENT_TYPES](
	[Id] [bigint] NOT NULL,
	[Type] [varchar](255) NULL DEFAULT (NULL),
	[priority] [int] NULL DEFAULT (NULL),
	[extensions] [varchar](256) NULL DEFAULT (NULL),
	[optionality] [tinyint] NULL,
	[previewable] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CONTENTFILES]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CONTENTFILES](
	[fileName] [varchar](255) NOT NULL,
	[lastUpdated] [datetime2](0) NULL DEFAULT (NULL),
	[version] [int] NULL DEFAULT (NULL),
	[publicURL] [varchar](255) NULL DEFAULT (NULL),
	[tabletFullPath] [varchar](255) NULL DEFAULT (NULL),
	[Type_Id] [bigint] NULL DEFAULT (NULL),
	[comments] [varchar](255) NULL DEFAULT (NULL),
	[lang_id] [bigint] NULL,
	[is_mandatory] [tinyint] NULL DEFAULT ('1'),
	[description] [varchar](255) NULL,
	[size] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[fileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CONTENTFILES_APPCONTEXT_X]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CONTENTFILES_APPCONTEXT_X](
	[contentFile] [varchar](255) NOT NULL,
	[appContextId] [bigint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_CARRIER]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_CARRIER](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_CALC_SPEC]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_CALC_SPEC](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[ACTUAL_RULE_REF] [varchar](255) NULL,
	[GROUP_NAME] [varchar](255) NULL,
	[INPUT_PARAMETERS] [varchar](max) NULL,
	[NAME] [varchar](255) NULL,
	[OUTPUT_PARAMETERS] [varchar](max) NULL,
	[VERSION] [varchar](255) NULL,
	[calculationType_ID] [varchar](255) NULL,
	[parentProduct_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_CHANNEL_MAPPING]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_CHANNEL_MAPPING](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CHANNEL_ID] [varchar](255) NULL,
	[PROD_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_CODE_TYPE_CARRIER]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_CODE_TYPE_CARRIER](
	[TYPE_LOOK_UP_ID] [varchar](255) NOT NULL,
	[CARRIER_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TYPE_LOOK_UP_ID] ASC,
	[CARRIER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP](
	[TYPE] [varchar](31) NOT NULL,
	[ID] [varchar](255) NOT NULL,
	[CODE_MAPPING_KEY] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_COLLATERALS]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_COLLATERALS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CREATED_DATE] [date] NULL,
	[CREATED_USER] [varchar](255) NULL,
	[FILE_NAME] [varchar](255) NULL,
	[LANGUAGE] [varchar](255) NULL,
	[UPDATED_DATE] [date] NULL,
	[UPDATED_USER] [varchar](255) NULL,
	[DOCUMENT_TYPE_ID] [varchar](255) NULL,
	[FILE_TYPE_ID] [varchar](255) NULL,
	[PROD_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_FUND_MAPPING]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CORE_PROD_FUND_MAPPING](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FUND_ID] [int] NULL,
	[PROD_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CORE_PROD_FUNDS]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_FUNDS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FUND_CODE] [varchar](255) NULL,
	[FUND_NAME] [varchar](255) NULL,
	[FUND_CATEGORY1_ID] [varchar](255) NULL,
	[FUND_CATEGORY2_ID] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_PROPERTIES]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_PROPERTIES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[CALCULATED_INDICATOR] [varchar](255) NULL,
	[CONSTANT_INDICATOR] [varchar](255) NULL,
	[DEFAULT_VALUE] [varchar](255) NULL,
	[MAX_VALUE] [varchar](255) NULL,
	[MIN_VALUE] [varchar](255) NULL,
	[NAME] [varchar](255) NULL,
	[OPTIONAL_INDICATOR] [varchar](255) NULL,
	[VERSION] [varchar](255) NULL,
	[parentProduct_id] [bigint] NULL,
	[propertyType_ID] [varchar](255) NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_RULE_SPEC]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_RULE_SPEC](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[ACTUAL_RULE_REF] [varchar](255) NULL,
	[CANBE_OVERWRITTEN_INDICATOR] [bit] NULL,
	[COMMENT_ON_FAILURE] [varchar](255) NULL,
	[GROUP_NAME] [varchar](255) NULL,
	[INPUT_PARAMETERS] [varchar](max) NULL,
	[NAME] [varchar](255) NULL,
	[OUTPUT_PARAMETERS] [varchar](max) NULL,
	[VERSION] [varchar](255) NULL,
	[parentProduct_id] [bigint] NULL,
	[ruleType_ID] [varchar](255) NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_TEMPLATE_MAPPING]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_TEMPLATE_MAPPING](
	[ID] [int] NOT NULL DEFAULT ((0)),
	[TEMPLATE_CONTEXT] [varchar](255) NULL,
	[PROD_ID] [bigint] NULL,
	[TEMPLATE_ID] [varchar](255) NULL,
	[TEMPLATE_TYPE] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROD_TEMPLATES]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROD_TEMPLATES](
	[TEMPLATE_ID] [varchar](255) NOT NULL,
	[COUNTRY_CODE] [varchar](255) NULL,
	[LANGUAGE] [varchar](255) NULL,
	[TEMPLATE_NAME] [varchar](255) NULL,
	[VERSION] [decimal](5, 3) NULL DEFAULT (NULL),
PRIMARY KEY CLUSTERED 
(
	[TEMPLATE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[TEMPLATE_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PRODUCT_COMPONENT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PRODUCT_COMPONENT](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[AUTO_INCLUDE_AT_ISSUE] [bit] NULL,
	[BUSINESS_DESCRIPTION] [varchar](max) NULL,
	[FORMAL_DEFINITION] [varchar](max) NULL,
	[LAUNCH_DATE] [datetime] NULL,
	[LOCAL_NAME] [varchar](255) NULL,
	[MARKETABLE_INDICATOR] [bit] NULL,
	[MARKETABLE_NAME] [varchar](255) NULL,
	[NAME] [varchar](255) NULL,
	[PLAN_CODE] [varchar](255) NULL,
	[PLAN_NAME] [varchar](255) NULL,
	[PRODUCT_CODE] [varchar](255) NULL,
	[WEIGHTAGE] [numeric](19, 2) NULL,
	[PUSH_SELL_INDICATOR] [bit] NULL,
	[SHORT_NAME] [varchar](255) NULL,
	[SUSPENSION_DATE] [datetime] NULL,
	[TERMINATION_DATE] [datetime] NULL,
	[VERSION] [varchar](255) NULL,
	[VERSION_DATE] [datetime] NULL,
	[WITHDRAWAL_DATE] [datetime] NULL,
	[carrier_id] [bigint] NULL,
	[category_ID] [varchar](255) NULL,
	[channelType_ID] [varchar](255) NULL,
	[coverType_ID] [varchar](255) NULL,
	[investmentType_ID] [varchar](255) NULL,
	[lineOfBusiness_ID] [varchar](255) NULL,
	[parentProduct_id] [bigint] NULL,
	[PRODUCT_GROUP] [varchar](255) NULL,
	[productType_ID] [varchar](255) NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORE_PROPERTY_VALUES]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORE_PROPERTY_VALUES](
	[ID] [bigint] NOT NULL,
	[DATA_TYPE] [varchar](255) NULL,
	[DATA_VALUE] [varchar](255) NULL,
	[DEFAULT_INDICATOR] [varchar](255) NULL,
	[propertySpecification_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COUNTRY_LOOKUP]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COUNTRY_LOOKUP](
	[IDENTIFIER] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[ParentID] [int] NULL,
	[Type] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDENTIFIER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Coverage]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coverage](
	[bonusOption] [varchar](255) NULL,
	[committedPremium] [int] NULL,
	[coverMultiple] [int] NULL,
	[coverageCode] [int] NULL,
	[coverageLimitAppliesToCode] [int] NULL,
	[coverageLimitCalculationBase] [varchar](255) NULL,
	[coverageLimitPercentage] [float] NULL,
	[coveragePriorityCode] [int] NULL,
	[deductibleAppliesToCode] [int] NULL,
	[deductibleCalculationBase] [varchar](255) NULL,
	[deductiblePercentage] [float] NULL,
	[deductibleTypeCode] [int] NULL,
	[divType] [varchar](255) NULL,
	[dividendAdjustment] [varchar](255) NULL,
	[emr] [float] NULL,
	[emrDuration] [int] NULL,
	[emrRequired] [bit] NULL,
	[flatExtraDuration] [int] NULL,
	[flatExtraRate] [float] NULL,
	[flatExtraRequired] [bit] NULL,
	[marketingName] [varchar](255) NULL,
	[paymentFrequencyCode] [int] NULL,
	[perClaimApplicationIndicator] [bit] NULL,
	[policyTerm] [int] NULL,
	[premiumFrequency] [int] NULL,
	[premiumPayingTerm] [int] NULL,
	[premiumTerm] [int] NULL,
	[premiumType] [varchar](255) NULL,
	[productFamily] [varchar](255) NULL,
	[productGroup] [varchar](255) NULL,
	[productType] [int] NULL,
	[required] [bit] NULL,
	[retroactiveStartDate] [datetime2](7) NULL,
	[riderTerm] [int] NULL,
	[shortName] [varchar](255) NULL,
	[surrenderOption] [bit] NULL,
	[vestingAge] [int] NULL,
	[yearOfIssue] [int] NULL,
	[id] [bigint] NOT NULL,
	[annualLimitAmount_id] [bigint] NULL,
	[coverageLimitAmount_id] [bigint] NULL,
	[deductibleAmount_id] [bigint] NULL,
	[eligibilityDuration_id] [bigint] NULL,
	[exposureAmount_id] [bigint] NULL,
	[extendedReportingDuration_id] [bigint] NULL,
	[maxsumAssured_id] [bigint] NULL,
	[minsumAssured_id] [bigint] NULL,
	[productCode_id] [bigint] NULL,
	[sumAssured_id] [bigint] NULL,
	[waitingPeriodDuration_id] [bigint] NULL,
	[autoBalancing] [bit] NULL,
	[autoBalancingValue] [int] NULL,
	[autoReentry] [bit] NULL,
	[autoReentryValue] [int] NULL,
	[autoTrading] [bit] NULL,
	[bounceBack] [bit] NULL,
	[bounceBackValue] [int] NULL,
	[cutLoss_TargetLoss] [bit] NULL,
	[cutLoss_TargetLossValue] [int] NULL,
	[emrUWCode] [varchar](255) NULL,
	[flatExtraUWCode] [varchar](255) NULL,
	[packageOption] [varchar](255) NULL,
	[profitClimbing] [bit] NULL,
	[profitClimbingValue] [int] NULL,
	[targetReturn] [bit] NULL,
	[targetReturnValue] [int] NULL,
	[healthUnits] [int] NULL,
	[minProtection] [varchar](255) NULL,
	[premiumHolidayStart] [int] NULL,
	[protectionUsed] [numeric](19, 2) NULL,
	[maxRegularTopUp_id] [bigint] NULL,
	[minRegularTopUp_id] [bigint] NULL,
	[healthUnitsPlanType] [varchar](255) NULL,
	[maxCoverageAge] [int] NULL,
	[maxProtection] [varchar](255) NULL,
	[sumInsured_id] [bigint] NULL,
	[bounceBackValues] [varchar](255) NULL,
	[insured] [varchar](255) NULL,
	[insuredType] [varchar](255) NULL,
	[riderCode] [varchar](255) NULL,
	[uniqueRiderName] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Coverage_FundInformation]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Coverage_FundInformation](
	[Coverage_id] [bigint] NOT NULL,
	[fundInformation_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Coverage_id] ASC,
	[fundInformation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[fundInformation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Coverage_Premium]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Coverage_Premium](
	[Coverage_id] [bigint] NOT NULL,
	[premium_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Coverage_id] ASC,
	[premium_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[premium_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Coverage_TopUp]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Coverage_TopUp](
	[Coverage_id] [bigint] NOT NULL,
	[includesTopUp_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Coverage_id] ASC,
	[includesTopUp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[includesTopUp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CurrencyAmount]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrencyAmount](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[amount] [numeric](19, 2) NULL,
	[currencyCode_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Document]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Document](
	[DOCUMENT_TYPE] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[documentEmissionDate] [date] NULL,
	[documentProofSubmitted] [varchar](255) NULL,
	[documentStatus] [varchar](255) NULL,
	[fileNames] [varchar](255) NULL,
	[freeContentIndicator] [bit] NULL,
	[identifier] [int] NULL,
	[modifiableIndicator] [bit] NULL,
	[name] [varchar](255) NULL,
	[receptionDate] [date] NULL,
	[signatureRequiredIndicator] [bit] NULL,
	[signedIndicator] [bit] NULL,
	[storageLocation] [varchar](255) NULL,
	[typeCode] [int] NULL,
	[versionIdentifier] [int] NULL,
	[effectivePeriod_id] [bigint] NULL,
	[languageCode_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
	[documentTypeCode] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Document_Document]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Document_Document](
	[Document_id] [bigint] NOT NULL,
	[pages_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Document_id] ASC,
	[pages_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[pages_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ElectronicContact]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ElectronicContact](
	[attachmentInd] [bit] NULL,
	[emailAddress] [varchar](255) NULL,
	[undeliverableIndicator] [bit] NULL,
	[uniformResourceLocationIdentifier] [int] NULL,
	[id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Extension]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Extension](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[value] [varchar](255) NULL,
	[language] [varchar](255) NULL,
	[templateId] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExternalCode]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExternalCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[code] [varchar](255) NULL,
	[sourceURL] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FamilyHealthHistory]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FamilyHealthHistory](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[ageAtDeath] [int] NULL,
	[causeOfDeathOrIllness] [varchar](255) NULL,
	[familyMember] [varchar](255) NULL,
	[presentAgeIfAlive] [int] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FAMILYMEMBER_FAMILYRELATIONSHIP]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAMILYMEMBER_FAMILYRELATIONSHIP](
	[FAMILYRELATIONSHIP_ID] [bigint] NOT NULL,
	[FAMILYMEMBER_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FAMILYRELATIONSHIP_ID] ASC,
	[FAMILYMEMBER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[FAMILYMEMBER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FinancialProvision]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FinancialProvision](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[estimateIndicator] [bit] NULL,
	[effectivePeriod_id] [bigint] NULL,
	[isDefinedFrom_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FinancialProvision_FinancialScheduler]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialProvision_FinancialScheduler](
	[FinancialProvision_id] [bigint] NOT NULL,
	[attachedFinancialScheduler_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FinancialProvision_id] ASC,
	[attachedFinancialScheduler_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[attachedFinancialScheduler_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FinancialProvision_Installment]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialProvision_Installment](
	[FinancialProvision_id] [bigint] NOT NULL,
	[basedOn_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FinancialProvision_id] ASC,
	[basedOn_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[basedOn_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FINANCIALPROVISION_ISBASEDFORFINANCIALPROVISION]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FINANCIALPROVISION_ISBASEDFORFINANCIALPROVISION](
	[FINANCIALPROVISION_ID] [bigint] NOT NULL,
	[ISBASEDFORFINANCIALPROVISION_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FINANCIALPROVISION_ID] ASC,
	[ISBASEDFORFINANCIALPROVISION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FINANCIALPROVISION_ISBASEDONFINANCIALPROVISION]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FINANCIALPROVISION_ISBASEDONFINANCIALPROVISION](
	[FINANCIALPROVISION_ID] [bigint] NOT NULL,
	[ISBASEDONFINANCIALPROVISION_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FINANCIALPROVISION_ID] ASC,
	[ISBASEDONFINANCIALPROVISION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FinancialScheduler]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FinancialScheduler](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[anniversaryDate] [date] NULL,
	[dayOfMonth] [int] NULL,
	[description] [varchar](255) NULL,
	[dueCollectionDateCount] [numeric](19, 2) NULL,
	[firstDueDate] [date] NULL,
	[paymentFrequencyCode] [int] NULL,
	[paymentMethodCode] [int] NULL,
	[statusEffectDate] [date] NULL,
	[effectivePeriod_id] [bigint] NULL,
	[paymentMeans_id] [bigint] NULL,
	[referencePeriod_id] [bigint] NULL,
	[relatedBankAccount_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FinancialServicesAgreement]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialServicesAgreement](
	[coSubscriptionIndicator] [bit] NULL,
	[id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FinancialServicesAgreement_Premium]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialServicesAgreement_Premium](
	[FinancialServicesAgreement_id] [bigint] NOT NULL,
	[agreementPremium_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FinancialServicesAgreement_id] ASC,
	[agreementPremium_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[agreementPremium_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FINSERVAGREEMENT_INTEGRATEDINFINSERVAGREEMENT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FINSERVAGREEMENT_INTEGRATEDINFINSERVAGREEMENT](
	[FINANCIALSERVICESAGREEMENT_ID] [bigint] NOT NULL,
	[INTEGRATEDINFINSERVAGREEMENT_ID] [bigint] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FINSERVAGREEMENT_INTEGRATESFINSERVAGREEMENT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FINSERVAGREEMENT_INTEGRATESFINSERVAGREEMENT](
	[FINANCIALSERVICESAGREEMENT_ID] [bigint] NOT NULL,
	[INTEGRATESFINSERVAGREEMENT_ID] [bigint] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FINSERVAGREEMENT_ISPAIDBYFINSERVAGREEMENT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FINSERVAGREEMENT_ISPAIDBYFINSERVAGREEMENT](
	[FINANCIALSERVICESAGREEMENT_ID] [bigint] NOT NULL,
	[ISPAIDBYFINSERVAGREEMENT_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FINANCIALSERVICESAGREEMENT_ID] ASC,
	[ISPAIDBYFINSERVAGREEMENT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FINSERVAGREEMENT_PAYSFINSERVAGREEMENT]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FINSERVAGREEMENT_PAYSFINSERVAGREEMENT](
	[FINANCIALSERVICESAGREEMENT_ID] [bigint] NOT NULL,
	[PAYSFINSERVAGREEMENT_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FINANCIALSERVICESAGREEMENT_ID] ASC,
	[PAYSFINSERVAGREEMENT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FundInformation]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FundInformation](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[allocatonPercentage] [bigint] NULL,
	[bonusOrDividend] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[payoutOrSettlement] [varchar](255) NULL,
	[transactionKeys_id] [bigint] NULL,
	[fundCode] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GoalAndNeed]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GoalAndNeed](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[agentId] [varchar](255) NULL,
	[birthDate] [date] NULL,
	[contactNumber] [varchar](255) NULL,
	[emailAddress] [varchar](255) NULL,
	[givenName] [varchar](255) NULL,
	[identifier] [varchar](255) NULL,
	[leadId] [varchar](255) NULL,
	[productId] [varchar](255) NULL,
	[requestJSON] [varchar](max) NULL,
	[status] [varchar](255) NULL,
	[surName] [varchar](255) NULL,
	[userEmail] [varchar](255) NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IllustrationResponsePayLoad]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IllustrationResponsePayLoad](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[identifier] [int] NULL,
	[responseHashCode] [int] NULL,
	[responseJson] [varchar](max) NULL,
	[agreement_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
	[summaryJson] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Installment]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Installment](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[billingOptionCode] [int] NULL,
	[dueDate] [date] NULL,
	[premiumInstallmentType] [varchar](255) NULL,
	[statusCode] [int] NULL,
	[netInitialPremiumAmount_id] [bigint] NULL,
	[netInstallmentPremiumAmount_id] [bigint] NULL,
	[netWithSurchargeAmount_id] [bigint] NULL,
	[paymentPeriod_id] [bigint] NULL,
	[withoutSurchargeAmount_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InsuranceAgreement]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InsuranceAgreement](
	[agentId] [varchar](255) NULL,
	[broadLineOfBusinessCode] [int] NULL,
	[declarationSigned] [bit] NULL,
	[downloadDate] [date] NULL,
	[eappInputTemplateId] [varchar](255) NULL,
	[eappTemplateId] [varchar](255) NULL,
	[insuranceAgreementStatusCode] [int] NULL,
	[insurerName] [varchar](255) NULL,
	[isFinalSubmit] [bit] NULL,
	[issueSubType] [int] NULL,
	[issueType] [int] NULL,
	[lastSyncDate] [datetime2](7) NULL,
	[lineOfBusinessCode] [int] NULL,
	[paidToDate] [date] NULL,
	[previousPolicyIndicator] [bit] NULL,
	[reinstatementDate] [date] NULL,
	[renewalIndicator] [bit] NULL,
	[stpstatus] [varchar](255) NULL,
	[id] [bigint] NOT NULL,
	[maturityBenefit_id] [bigint] NULL,
	[policyValue_id] [bigint] NULL,
	[policyValueLastYear_id] [bigint] NULL,
	[sumAssured_id] [bigint] NULL,
	[illustrationTemplateId] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LE_LOGIN_CHANNEL]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LE_LOGIN_CHANNEL](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CHANEEL_DESCRIPTION] [varchar](255) NULL,
	[CHANNEL_NAME] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LE_LOGIN_CHANNEL_AUD]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LE_LOGIN_CHANNEL_AUD](
	[ID] [bigint] NOT NULL,
	[REV] [int] NOT NULL,
	[REVTYPE] [smallint] NULL,
	[CHANEEL_DESCRIPTION] [varchar](255) NULL,
	[CHANNEL_NAME] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[REV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LE_LOGIN_ROLE]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LE_LOGIN_ROLE](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ROLE_DESCRIPTION] [varchar](255) NULL,
	[ROLE_NAME] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LE_LOGIN_ROLE_AUD]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LE_LOGIN_ROLE_AUD](
	[ID] [bigint] NOT NULL,
	[REV] [int] NOT NULL,
	[REVTYPE] [smallint] NULL,
	[ROLE_DESCRIPTION] [varchar](255) NULL,
	[ROLE_NAME] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[REV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LE_LOGIN_USER]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LE_LOGIN_USER](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PASSWORD] [varchar](255) NULL,
	[SERIES_ID] [varchar](255) NULL,
	[TIMESTAMP] [datetime2](7) NULL,
	[TOKEN] [varchar](255) NULL,
	[USER_ID] [varchar](255) NULL,
	[CREATEDBY] [varchar](255) NULL,
	[CREATEDDATE] [datetime] NULL,
	[EMAIL] [varchar](255) NULL,
	[FIRSTNAME] [varchar](255) NULL,
	[IS_ACTIVE] [bit] NULL,
	[LASTNAME] [varchar](255) NULL,
	[UPDATEDBY] [varchar](255) NULL,
	[UPDATEDDATE] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LE_LOGIN_USER_AUD]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LE_LOGIN_USER_AUD](
	[ID] [bigint] NOT NULL,
	[REV] [int] NOT NULL,
	[REVTYPE] [smallint] NULL,
	[PASSWORD] [varchar](255) NULL,
	[SERIES_ID] [varchar](255) NULL,
	[TIMESTAMP] [datetime2](7) NULL,
	[TOKEN] [varchar](255) NULL,
	[USER_ID] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[REV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LE_LOGIN_USER_CHANNEL]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LE_LOGIN_USER_CHANNEL](
	[USER_ID] [bigint] NOT NULL,
	[CHANNEL_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[USER_ID] ASC,
	[CHANNEL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LE_LOGIN_USER_CHANNEL_AUD]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LE_LOGIN_USER_CHANNEL_AUD](
	[REV] [int] NOT NULL,
	[USER_ID] [bigint] NOT NULL,
	[CHANNEL_ID] [bigint] NOT NULL,
	[REVTYPE] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[REV] ASC,
	[USER_ID] ASC,
	[CHANNEL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LE_LOGIN_USER_ROLE]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LE_LOGIN_USER_ROLE](
	[USER_ID] [bigint] NOT NULL,
	[ROLE_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[USER_ID] ASC,
	[ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LE_LOGIN_USER_ROLE_AUD]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LE_LOGIN_USER_ROLE_AUD](
	[REV] [int] NOT NULL,
	[USER_ID] [bigint] NOT NULL,
	[ROLE_ID] [bigint] NOT NULL,
	[REVTYPE] [smallint] NULL,
PRIMARY KEY CLUSTERED 
(
	[REV] ASC,
	[USER_ID] ASC,
	[ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LE_LOGIN_USER_TOKEN]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LE_LOGIN_USER_TOKEN](
	[DTYPE] [varchar](31) NOT NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SERIES_ID] [varchar](255) NULL,
	[TIMESTAMP] [datetime2](7) NULL,
	[TOKEN] [varchar](255) NULL,
	[userId] [varchar](255) NULL,
	[USER_ID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LifeEngageAudit]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LifeEngageAudit](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[creationDateandTime] [datetime2](7) NULL,
	[requestJson] [varchar](max) NULL,
	[sourceInfoName] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[userName] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LifeEngageEmail]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LifeEngageEmail](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[agentId] [varchar](255) NULL,
	[attemptNumber] [varchar](255) NULL,
	[batchJobId] [bigint] NULL,
	[emailValues] [varchar](max) NULL,
	[sendTime] [datetime2](7) NULL,
	[status] [varchar](255) NULL,
	[statusMessage] [varchar](255) NULL,
	[type] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LifeEngagePayloadAudit]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LifeEngagePayloadAudit](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[key1] [varchar](255) NULL,
	[key10] [varchar](255) NULL,
	[key11] [varchar](255) NULL,
	[key12] [datetime2](7) NULL,
	[key13] [datetime2](7) NULL,
	[key14] [datetime2](7) NULL,
	[key15] [varchar](255) NULL,
	[key16] [varchar](255) NULL,
	[key17] [varchar](255) NULL,
	[key18] [varchar](255) NULL,
	[key19] [varchar](255) NULL,
	[key2] [varchar](255) NULL,
	[key20] [varchar](255) NULL,
	[key21] [varchar](255) NULL,
	[key22] [varchar](255) NULL,
	[key23] [varchar](255) NULL,
	[key24] [varchar](255) NULL,
	[key25] [varchar](255) NULL,
	[key3] [varchar](255) NULL,
	[key4] [varchar](255) NULL,
	[key5] [varchar](255) NULL,
	[key6] [varchar](255) NULL,
	[key7] [varchar](255) NULL,
	[key8] [varchar](255) NULL,
	[key9] [varchar](255) NULL,
	[offlineIdentifier] [varchar](255) NULL,
	[requestHashCode] [int] NULL,
	[transTrackingID] [varchar](255) NULL,
	[type] [varchar](255) NULL,
	[lifeEngageAuditId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Measurement]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Measurement](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[value] [numeric](19, 2) NULL,
	[unitCode_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NWS_USER]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NWS_USER](
	[UID] [varchar](255) NOT NULL,
	[givenName] [varchar](255) NULL,
	[mnylAgentCode] [varchar](255) NULL,
	[mnylAgentEmail] [varchar](255) NULL,
	[mnylBossID] [varchar](255) NULL,
	[mnylMyAgentRoleCode] [varchar](255) NULL,
	[mnylStatus] [varchar](255) NULL,
	[title] [varchar](255) NULL,
	[UserMarkedAsDeleted] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Observation]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Observation](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[agent] [varchar](255) NULL,
	[contextId] [int] NULL,
	[creationDate] [date] NULL,
	[customerDbId] [varchar](255) NULL,
	[customerDeviceId] [varchar](255) NULL,
	[customerId] [varchar](255) NULL,
	[customerName] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[page] [varchar](255) NULL,
	[path] [varchar](255) NULL,
	[subModule] [varchar](255) NULL,
	[subOrdinateId] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Organization]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Organization](
	[dissolutionDate] [date] NULL,
	[foundationDate] [date] NULL,
	[memberCount] [numeric](19, 2) NULL,
	[statusCode] [int] NULL,
	[statusDate] [date] NULL,
	[statusReason] [varchar](255) NULL,
	[id] [bigint] NOT NULL,
	[accountingPeriod_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Organization_OrganizationDetail]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organization_OrganizationDetail](
	[Organization_id] [bigint] NOT NULL,
	[detail_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Organization_id] ASC,
	[detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Organization_PartyName]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organization_PartyName](
	[Organization_id] [bigint] NOT NULL,
	[name_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Organization_id] ASC,
	[name_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrganizationDetail]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrganizationDetail](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[applicabilityCode] [int] NULL,
	[effectivePeriod_id] [bigint] NULL,
	[referencePeriod_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Party]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Party](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[communicateViaEmail] [bit] NULL,
	[communicateViaPost] [bit] NULL,
	[communicateViaSMS] [bit] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Party_ContactPreference]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Party_ContactPreference](
	[Party_id] [bigint] NOT NULL,
	[preferredContact_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Party_id] ASC,
	[preferredContact_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[preferredContact_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Party_Extension]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Party_Extension](
	[Party_id] [bigint] NOT NULL,
	[includesExtension_id] [bigint] NOT NULL,
UNIQUE NONCLUSTERED 
(
	[includesExtension_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Party_TaxRegistration]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Party_TaxRegistration](
	[Party_id] [bigint] NOT NULL,
	[relatedTaxRegistrations_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Party_id] ASC,
	[relatedTaxRegistrations_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[relatedTaxRegistrations_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyName]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartyName](
	[PARTY_NAME_TYPE] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[defaultIndicator] [bit] NOT NULL,
	[description] [varchar](255) NULL,
	[fullName] [varchar](255) NULL,
	[usageCode] [int] NULL,
	[givenName] [varchar](255) NULL,
	[initials] [varchar](255) NULL,
	[middleName] [varchar](255) NULL,
	[prefixTitleCode] [int] NULL,
	[suffix] [varchar](255) NULL,
	[surname] [varchar](255) NULL,
	[title] [varchar](255) NULL,
	[effectivePeriod_id] [bigint] NULL,
	[languageCode_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartyRoleInAgreement]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartyRoleInAgreement](
	[PARTY_ROLE_IN_AGREEMENT_TYPE] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[insurableInterestCode] [int] NULL,
	[isNonMedicalCase] [bit] NULL,
	[legalWording] [varchar](255) NULL,
	[relationshipWithProposer] [varchar](255) NULL,
	[typeCode] [int] NULL,
	[acceptedIndicator] [bit] NULL,
	[benefitDistributionCalculationCode] [int] NULL,
	[designationCode] [int] NULL,
	[irrevocableIndicator] [bit] NULL,
	[partExemptFromTaxesPercentage] [float] NULL,
	[relationWithInsured] [varchar](255) NULL,
	[sharePercentage] [float] NULL,
	[payorDifferentFromInsuredIndicator] [bit] NULL,
	[leaderIndicator] [bit] NULL,
	[managementByAperitorIndicator] [bit] NULL,
	[rejectionReasonCode] [int] NULL,
	[signedLinePercentage] [float] NULL,
	[writtenLinePercentage] [float] NULL,
	[agreementTerminatedByInsurerIndicator] [bit] NULL,
	[agreementTerminationReason] [varchar](255) NULL,
	[declaredClaimCount] [numeric](19, 2) NULL,
	[timeSinceAgreementTermination] [varchar](255) NULL,
	[agreementHolderRejectionReasonCode] [int] NULL,
	[relationWithBeneficiary] [varchar](255) NULL,
	[effectivePeriod_id] [bigint] NULL,
	[rolePeriod_id] [bigint] NULL,
	[playerParty_id] [bigint] NULL,
	[isPartyRoleIn_id] [bigint] NULL,
	[declaredClaimAmount_id] [bigint] NULL,
	[lossTimePeriodDuration_id] [bigint] NULL,
	[noClaimDiscountDuration_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartyRoleInAgreement_Communication]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartyRoleInAgreement_Communication](
	[PartyRoleInAgreement_id] [bigint] NOT NULL,
	[receivesCommunication_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PartyRoleInAgreement_id] ASC,
	[receivesCommunication_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[receivesCommunication_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyRoleInAgreement_FamilyHealthHistory]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartyRoleInAgreement_FamilyHealthHistory](
	[PartyRoleInAgreement_id] [bigint] NOT NULL,
	[familyHealthHistory_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PartyRoleInAgreement_id] ASC,
	[familyHealthHistory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[familyHealthHistory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PARTYROLEINAGREEMENT_PARTYROLERELATIONSHIP]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PARTYROLEINAGREEMENT_PARTYROLERELATIONSHIP](
	[PARTYROLERELATIONSHIP_ID] [bigint] NOT NULL,
	[PARTYROLEINAGREEMENT_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PARTYROLERELATIONSHIP_ID] ASC,
	[PARTYROLEINAGREEMENT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[PARTYROLEINAGREEMENT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyRoleInAgreement_UserSelection]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartyRoleInAgreement_UserSelection](
	[PartyRoleInAgreement_id] [bigint] NOT NULL,
	[userSelectedOptions_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PartyRoleInAgreement_id] ASC,
	[userSelectedOptions_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[userSelectedOptions_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyRoleInRelationship]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartyRoleInRelationship](
	[PARTY_ROLE_IN_RELATIONSHIP] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[civilRelationNatureCode] [int] NULL,
	[exemptIndicator] [bit] NULL,
	[jobDescription] [varchar](255) NULL,
	[jobTitleCode] [int] NULL,
	[pensionableIndicator] [bit] NULL,
	[salaryGradeCode] [int] NULL,
	[skillLevelCode] [int] NULL,
	[relationNatureCode] [int] NULL,
	[agentId] [varchar](255) NULL,
	[claimCount] [varchar](255) NULL,
	[hasDependent] [bit] NULL,
	[identifier] [varchar](255) NULL,
	[importanceLevelCode] [int] NULL,
	[isProposerSameAsCustomer] [bit] NULL,
	[lastSyncDate] [datetime2](7) NULL,
	[parentLeadId] [varchar](255) NULL,
	[reason] [varchar](255) NULL,
	[source] [int] NULL,
	[statusCode] [int] NULL,
	[subReason] [varchar](255) NULL,
	[subSource] [int] NULL,
	[subStatusCode] [int] NULL,
	[suggestions] [varchar](255) NULL,
	[effectivePeriod_id] [bigint] NULL,
	[rolePeriod_id] [bigint] NULL,
	[playerParty_id] [bigint] NULL,
	[annualTaxableBenefitAmount_id] [bigint] NULL,
	[baseSalaryAmount_id] [bigint] NULL,
	[bonusSalaryAmount_id] [bigint] NULL,
	[customerRelationship_id] [bigint] NULL,
	[proposer_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartyRoleInRelationship_Communication]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartyRoleInRelationship_Communication](
	[PartyRoleInRelationship_id] [bigint] NOT NULL,
	[receivesCommunication_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PartyRoleInRelationship_id] ASC,
	[receivesCommunication_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[receivesCommunication_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyRoleInRelationship_Extension]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartyRoleInRelationship_Extension](
	[PartyRoleInRelationship_id] [bigint] NOT NULL,
	[includesExtension_id] [bigint] NOT NULL,
UNIQUE NONCLUSTERED 
(
	[includesExtension_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyRoleInRelationship_PartyRoleInRelationship]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartyRoleInRelationship_PartyRoleInRelationship](
	[PartyRoleInRelationship_id] [bigint] NOT NULL,
	[relatedCustomers_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PartyRoleInRelationship_id] ASC,
	[relatedCustomers_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[relatedCustomers_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyRoleInRelationship_PartyRoleRelationship]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartyRoleInRelationship_PartyRoleRelationship](
	[PartyRoleInRelationship_id] [bigint] NOT NULL,
	[employment_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PartyRoleInRelationship_id] ASC,
	[employment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[employment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyRoleInRelationship_PurchasedProduct]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartyRoleInRelationship_PurchasedProduct](
	[PartyRoleInRelationship_id] [bigint] NOT NULL,
	[purchasedProducts_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PartyRoleInRelationship_id] ASC,
	[purchasedProducts_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[purchasedProducts_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyRoleInRelationship_Specification]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartyRoleInRelationship_Specification](
	[PartyRoleInRelationship_id] [bigint] NOT NULL,
	[questionnaire_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PartyRoleInRelationship_id] ASC,
	[questionnaire_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[questionnaire_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartyRoleRelationship]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartyRoleRelationship](
	[PARTY_ROLE_RELATIONSHIP_TYPE] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[status] [varchar](255) NULL,
	[statusDate] [date] NULL,
	[businessUnit] [varchar](255) NULL,
	[currentIndicator] [bit] NULL,
	[exemptIndicator] [bit] NULL,
	[jobDescription] [varchar](255) NULL,
	[jobTitleCode] [int] NULL,
	[location] [varchar](255) NULL,
	[occupationClassCode] [int] NULL,
	[occupationTypeCode] [varchar](255) NULL,
	[pensionableIndicator] [bit] NULL,
	[salaryGradeCode] [int] NULL,
	[skillLevelCode] [int] NULL,
	[dependentAdultCount] [numeric](19, 2) NULL,
	[dependentChildrenCount] [numeric](19, 2) NULL,
	[familyIndicator] [bit] NULL,
	[homeOwnership] [int] NULL,
	[name] [varchar](255) NULL,
	[oldestDependentAdultBirthDate] [date] NULL,
	[oldestDependentChildBirthDate] [date] NULL,
	[youngestDependentAdultBirthDate] [date] NULL,
	[youngestDependentChildBirthDate] [date] NULL,
	[civilRelationshipSstatusDate] [date] NULL,
	[statusCode] [int] NULL,
	[effectivePeriod_id] [bigint] NULL,
	[annualTaxableBenefitAmount_id] [bigint] NULL,
	[baseSalaryAmount_id] [bigint] NULL,
	[bonusSalaryAmount_id] [bigint] NULL,
	[relatedCustomer_id] [bigint] NULL,
	[disposableIncomeAmount_id] [bigint] NULL,
	[grossIncomeAmount_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentMethod]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentMethod](
	[PAYMENT_METHOD_TYPE] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[paymentFormCode] [int] NULL,
	[bankName] [varchar](255) NULL,
	[checkIdentifier] [int] NULL,
	[issueDate] [date] NULL,
	[brandCode] [int] NULL,
	[cardIdentifier] [int] NULL,
	[creditCardTypeCode] [int] NULL,
	[cvv2] [int] NULL,
	[expiryMonth] [int] NULL,
	[expiryYear] [int] NULL,
	[pin] [varchar](255) NULL,
	[fromAccount_id] [bigint] NULL,
	[cardholderName_id] [bigint] NULL,
	[relatedBankAccount_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Person]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Person](
	[age] [int] NULL,
	[ageProof] [varchar](255) NULL,
	[authenticationID] [varchar](255) NULL,
	[birthDate] [date] NULL,
	[bloodTypeCode] [int] NULL,
	[deathDate] [date] NULL,
	[deathIndicator] [bit] NULL,
	[drinkingPremiumBasis] [varchar](255) NULL,
	[ethnicityCode] [int] NULL,
	[genderCode] [int] NULL,
	[identityExpDate] [date] NULL,
	[identityProof] [varchar](255) NULL,
	[maritalStatusCode] [varchar](255) NULL,
	[missingDate] [date] NULL,
	[missingIndicator] [bit] NULL,
	[nationalId] [varchar](255) NULL,
	[nationalIdType] [varchar](255) NULL,
	[permanentAddressSameAsCurrentIndicator] [bit] NULL,
	[tobaccoPremiumBasis] [varchar](255) NULL,
	[id] [bigint] NOT NULL,
	[birthAddress_id] [bigint] NULL,
	[height_id] [int] NULL,
	[primaryLanguageCode_id] [bigint] NULL,
	[residenceCountry_id] [bigint] NULL,
	[spouseOrParentName_id] [bigint] NULL,
	[weight_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Person_CitizenshipCountry]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person_CitizenshipCountry](
	[person_Id] [bigint] NOT NULL,
	[Country_Id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[person_Id] ASC,
	[Country_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Country_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Person_NationalityCountry]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person_NationalityCountry](
	[person_Id] [bigint] NOT NULL,
	[Country_Id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[person_Id] ASC,
	[Country_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Country_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Person_PartyName]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person_PartyName](
	[Person_id] [bigint] NOT NULL,
	[name_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Person_id] ASC,
	[name_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Person_PersonDetail]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person_PersonDetail](
	[Person_id] [bigint] NOT NULL,
	[detail_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Person_id] ASC,
	[detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersonDetail]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonDetail](
	[PERSON_DETAIL_TYPE] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[applicabilityCode] [int] NULL,
	[designationCode] [int] NULL,
	[educationLevelCode] [int] NULL,
	[fullTimeStudentIndicator] [bit] NULL,
	[childrenCount] [numeric](19, 2) NULL,
	[parentIndicator] [bit] NULL,
	[spouseIndicator] [bit] NULL,
	[frequencyCode] [int] NULL,
	[incomeProof] [varchar](255) NULL,
	[incomeProofSubmittedIndicator] [bit] NULL,
	[natureOfWorkOfParentOrSpouse] [varchar](255) NULL,
	[permanentAccountNumber] [varchar](255) NULL,
	[typeCode] [int] NULL,
	[anticipatedRetirementDate] [date] NULL,
	[anticipatedRetirementIndicator] [bit] NULL,
	[currentEmploymentIndicator] [bit] NULL,
	[description] [varchar](255) NULL,
	[nameofInstitution] [varchar](255) NULL,
	[natureofWork] [varchar](255) NULL,
	[occupationClass] [varchar](255) NULL,
	[primaryOccupationIndicator] [bit] NULL,
	[professionalTitle] [varchar](255) NULL,
	[retirementDate] [date] NULL,
	[effectivePeriod_id] [bigint] NULL,
	[providingHousehold_id] [bigint] NULL,
	[annualIncomeOfSpouse_id] [bigint] NULL,
	[disposableAmount_id] [bigint] NULL,
	[estGrossAnnualOtherIncome_id] [bigint] NULL,
	[grossAmount_id] [bigint] NULL,
	[netAmount_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonDetail_PartyRoleRelationship]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonDetail_PartyRoleRelationship](
	[PersonDetail_id] [bigint] NOT NULL,
	[providingEmployment_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PersonDetail_id] ASC,
	[providingEmployment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[providingEmployment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Place]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Place](
	[PLACE_TYPE] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[abbreviation] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[administrativeSubDivisionCode] [int] NULL,
	[typeCode] [int] NULL,
	[assignedCodeExtension] [varchar](255) NULL,
	[subSystemTypeCode] [int] NULL,
	[availablePeriod_id] [bigint] NULL,
	[alphaISOCode_id] [bigint] NULL,
	[extendedISOCode_id] [bigint] NULL,
	[telephonePrefixCode_id] [bigint] NULL,
	[relatedCountry_id] [bigint] NULL,
	[assignedCode_id] [bigint] NULL,
	[relatedid] [bigint] NULL,
	[postaladdresscontactid] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PostalAddressContact]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PostalAddressContact](
	[addressBarCodeInd] [bit] NULL,
	[addressLine1] [varchar](255) NULL,
	[addressLine2] [varchar](255) NULL,
	[addressLine3] [varchar](255) NULL,
	[addressLines] [varchar](255) NULL,
	[addressNatureCode] [int] NULL,
	[addressProof] [varchar](255) NULL,
	[addressProofIssuedDate] [date] NULL,
	[addressProofIssuingAuthority] [varchar](255) NULL,
	[addressValidInd] [bit] NULL,
	[boxNumber] [varchar](255) NULL,
	[buildingName] [varchar](255) NULL,
	[carrierRoute] [varchar](255) NULL,
	[floorNumber] [varchar](255) NULL,
	[identifiedPlace] [varbinary](255) NULL,
	[legalAddressInd] [bit] NULL,
	[postDirectionCode] [int] NULL,
	[postalBarcode] [varchar](255) NULL,
	[preDirectionCode] [int] NULL,
	[preventOverrideInd] [bit] NULL,
	[returnedMailInd] [bit] NULL,
	[streetName] [varchar](255) NULL,
	[streetNumber] [varchar](255) NULL,
	[unitNumber] [varchar](255) NULL,
	[unstructuredAddress] [varchar](255) NULL,
	[yearsAtAddress] [float] NULL,
	[id] [bigint] NOT NULL,
	[postalCountry_id] [bigint] NULL,
	[postalCountrySubdivision_id] [bigint] NULL,
	[postalMunicipality_id] [bigint] NULL,
	[postalPostCode_id] [bigint] NULL,
	[streetTypeCode_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Premium]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Premium](
	[calculationMethodCode] [int] NULL,
	[natureCode] [int] NULL,
	[id] [bigint] NOT NULL,
	[amount_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Premium_Installment]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Premium_Installment](
	[Premium_id] [bigint] NOT NULL,
	[resultingInstallment_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Premium_id] ASC,
	[resultingInstallment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[resultingInstallment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurchasedProduct]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PurchasedProduct](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[productCode] [varchar](255) NULL,
	[proposalNumber] [varchar](255) NULL,
	[submissionDate] [date] NULL,
	[premium_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[REVINFO]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REVINFO](
	[REV] [int] IDENTITY(1,1) NOT NULL,
	[REVTSTMP] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[REV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SelectedOption]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SelectedOption](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[selectedOption] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Specification]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Specification](
	[SPECIFICATION_TYPE] [varchar](31) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[firstManufacturedDate] [date] NULL,
	[kindOfElementName] [varchar](255) NULL,
	[lastManufacturedDate] [date] NULL,
	[make] [varchar](255) NULL,
	[model] [varchar](255) NULL,
	[modelYear] [int] NULL,
	[name] [varchar](255) NULL,
	[series] [varchar](255) NULL,
	[shortName] [varchar](255) NULL,
	[version] [varchar](255) NULL,
	[versionDate] [date] NULL,
	[title] [varchar](255) NULL,
	[versionNumber] [varchar](255) NULL,
	[detailedInfo] [varchar](255) NULL,
	[questionId] [bigint] NULL,
	[questionnaireType] [int] NULL,
	[selectedOption] [int] NULL,
	[unit] [varchar](255) NULL,
	[estimatedMarketAmount_id] [bigint] NULL,
	[listPriceNewAmount_id] [bigint] NULL,
	[languageCode_id] [bigint] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SPOUSE_CIVILRELATIONSHIP]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SPOUSE_CIVILRELATIONSHIP](
	[CIVILRELATIONSHIP_ID] [bigint] NOT NULL,
	[SPOUSE_ID] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SPOUSE_ID] ASC,
	[CIVILRELATIONSHIP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[CIVILRELATIONSHIP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[SPOUSE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaxRegistration]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaxRegistration](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[isRegisteredBy] [varbinary](255) NULL,
	[registrationIdentifier] [varchar](255) NULL,
	[requestDate] [datetime2](7) NULL,
	[taxFileName] [varchar](255) NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TelephoneCallContact]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TelephoneCallContact](
	[extension] [varchar](255) NULL,
	[fullNumber] [varchar](255) NULL,
	[identifier] [int] NULL,
	[invalidIndicator] [bit] NULL,
	[localNumber] [varchar](255) NULL,
	[networkTypeCode] [int] NULL,
	[trunkPrefix] [varchar](255) NULL,
	[ttyIndicator] [bit] NULL,
	[typeCode] [int] NULL,
	[id] [bigint] NOT NULL,
	[areaCode_id] [bigint] NULL,
	[countryCode_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TimePeriod]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimePeriod](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[endDateTime] [datetime2](7) NULL,
	[startDateTime] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TopUp]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TopUp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[topUpFromYear] [int] NULL,
	[topUpPremium] [numeric](19, 2) NULL,
	[topUpToYear] [int] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionKeys]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionKeys](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[key1] [varchar](255) NULL,
	[key10] [varchar](255) NULL,
	[key11] [varchar](255) NULL,
	[key12] [varchar](255) NULL,
	[key13] [varchar](255) NULL,
	[key14] [varchar](255) NULL,
	[key15] [varchar](255) NULL,
	[key16] [varchar](255) NULL,
	[key17] [varchar](255) NULL,
	[key18] [varchar](255) NULL,
	[key19] [varchar](255) NULL,
	[key2] [varchar](255) NULL,
	[key20] [varchar](255) NULL,
	[key21] [varchar](255) NULL,
	[key22] [varchar](255) NULL,
	[key23] [varchar](255) NULL,
	[key24] [varchar](255) NULL,
	[key25] [varchar](255) NULL,
	[key26] [varchar](255) NULL,
	[key27] [varchar](255) NULL,
	[key28] [varchar](255) NULL,
	[key29] [varchar](255) NULL,
	[key3] [varchar](255) NULL,
	[key30] [varchar](255) NULL,
	[key4] [varchar](255) NULL,
	[key5] [varchar](255) NULL,
	[key6] [varchar](255) NULL,
	[key7] [varchar](255) NULL,
	[key8] [varchar](255) NULL,
	[key9] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[USER]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USER](
	[USERID] [bigint] IDENTITY(1,1) NOT NULL,
	[USERNAME] [varchar](50) NOT NULL,
	[FIRSTNAME] [varchar](50) NOT NULL,
	[LASTNAME] [varchar](50) NOT NULL,
	[PASSWORD] [varchar](50) NOT NULL,
	[EMAIL] [varchar](50) NOT NULL,
	[ACTIVE] [bit] NULL,
	[CREATEDBY] [bigint] NULL,
	[CREATEDDATE] [date] NULL,
	[UPDATEDBY] [bigint] NULL,
	[UPDATEDDATE] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[USERID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserSelection]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserSelection](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[basicDataCompleteCode] [int] NULL,
	[contextId] [int] NULL,
	[creationDateTime] [datetime2](7) NULL,
	[modifiedDateTime] [datetime2](7) NULL,
	[transTrackingId] [varchar](255) NULL,
	[transactionId] [varchar](255) NULL,
	[typeName] [varchar](255) NULL,
	[answerType] [varchar](255) NULL,
	[detailedInfo] [varchar](255) NULL,
	[level] [varchar](255) NULL,
	[numberOfYears] [int] NULL,
	[parentQuestionId] [varchar](255) NULL,
	[quantityPerDay] [int] NULL,
	[questionId] [bigint] NULL,
	[questionnaireType] [int] NULL,
	[selectedOption] [bit] NOT NULL,
	[unit] [int] NULL,
	[transactionKeys_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserSelection_SelectedOption]    Script Date: 10/20/2015 9:41:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSelection_SelectedOption](
	[UserSelection_id] [bigint] NOT NULL,
	[selectedOptions_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserSelection_id] ASC,
	[selectedOptions_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[selectedOptions_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [FK_Account_exid_cid_pid_onid_nid_kid]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_Account_exid_cid_pid_onid_nid_kid] ON [dbo].[Agreement]
(
	[agreementExtension_id] ASC,
	[currencyCode_id] ASC,
	[effectivePeriod_id] ASC,
	[isBasedOn_id] ASC,
	[issueNation_id] ASC,
	[transactionKeys_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_Agreement_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_Agreement_id] ON [dbo].[Agreement_Extension]
(
	[Agreement_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_includesExtension_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_includesExtension_id] ON [dbo].[Agreement_Extension]
(
	[includesExtension_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_AgreementLoan_aid_pid_mid_uid]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_AgreementLoan_aid_pid_mid_uid] ON [dbo].[AgreementLoan]
(
	[loanAmount_id] ASC,
	[loanPeriod_id] ASC,
	[relatedAgreement_id] ASC,
	[unitOfLoanPeriod_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_loanAmount_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_loanAmount_id] ON [dbo].[AgreementLoan]
(
	[loanAmount_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_loanPeriod_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_loanPeriod_id] ON [dbo].[AgreementLoan]
(
	[loanPeriod_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_relatedAgreement_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_relatedAgreement_id] ON [dbo].[AgreementLoan]
(
	[relatedAgreement_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_AgreementWithdrawal_agrid_amtid_pid]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_AgreementWithdrawal_agrid_amtid_pid] ON [dbo].[AgreementWithdrawal]
(
	[relatedAgreement_id] ASC,
	[withdrawalAmount_id] ASC,
	[withdrawalPeriod_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_relatedAgreement_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_relatedAgreement_id] ON [dbo].[AgreementWithdrawal]
(
	[relatedAgreement_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_withdrawalAmount_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_withdrawalAmount_id] ON [dbo].[AgreementWithdrawal]
(
	[withdrawalAmount_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_availablePeriod_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_availablePeriod_id] ON [dbo].[ContactPoint]
(
	[availablePeriod_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_currencyCode_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_currencyCode_id] ON [dbo].[CurrencyAmount]
(
	[currencyCode_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_effectivePeriod_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_effectivePeriod_id] ON [dbo].[Document]
(
	[effectivePeriod_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_transactionKeys_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_transactionKeys_id] ON [dbo].[Document]
(
	[transactionKeys_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_isDefinedFrom_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_isDefinedFrom_id] ON [dbo].[FinancialProvision]
(
	[isDefinedFrom_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_transactionKeys_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_transactionKeys_id] ON [dbo].[FinancialProvision]
(
	[transactionKeys_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_transactionKeys_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_transactionKeys_id] ON [dbo].[FundInformation]
(
	[transactionKeys_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FK_InsuranceAgreement_itid_vid_yid_sid_tid]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_InsuranceAgreement_itid_vid_yid_sid_tid] ON [dbo].[InsuranceAgreement]
(
	[maturityBenefit_id] ASC,
	[policyValue_id] ASC,
	[policyValueLastYear_id] ASC,
	[sumAssured_id] ASC,
	[illustrationTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_transactionKeys_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_transactionKeys_id] ON [dbo].[Party]
(
	[transactionKeys_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_isPartyRoleIn_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_isPartyRoleIn_id] ON [dbo].[PartyRoleInAgreement]
(
	[isPartyRoleIn_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FK_PARTY_ROLE_IN_AGREEMENT_TYPE]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_PARTY_ROLE_IN_AGREEMENT_TYPE] ON [dbo].[PartyRoleInAgreement]
(
	[PARTY_ROLE_IN_AGREEMENT_TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_PartyRoleInAgreement_ids]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_PartyRoleInAgreement_ids] ON [dbo].[PartyRoleInAgreement]
(
	[effectivePeriod_id] ASC,
	[rolePeriod_id] ASC,
	[playerParty_id] ASC,
	[isPartyRoleIn_id] ASC,
	[declaredClaimAmount_id] ASC,
	[lossTimePeriodDuration_id] ASC,
	[noClaimDiscountDuration_id] ASC,
	[transactionKeys_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_birthAddress_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_birthAddress_id] ON [dbo].[Person]
(
	[birthAddress_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_alphaISOCode_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_alphaISOCode_id] ON [dbo].[Place]
(
	[alphaISOCode_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_assignedCode_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_assignedCode_id] ON [dbo].[Place]
(
	[assignedCode_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_availablePeriod_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_availablePeriod_id] ON [dbo].[Place]
(
	[availablePeriod_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_extendedISOCode_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_extendedISOCode_id] ON [dbo].[Place]
(
	[extendedISOCode_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_postaladdresscontactid]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_postaladdresscontactid] ON [dbo].[Place]
(
	[postaladdresscontactid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_telephonePrefixCode_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_telephonePrefixCode_id] ON [dbo].[Place]
(
	[telephonePrefixCode_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_transactionKeys_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_transactionKeys_id] ON [dbo].[Place]
(
	[transactionKeys_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_postalCountry_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_postalCountry_id] ON [dbo].[PostalAddressContact]
(
	[postalCountry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_postalCountrySubdivision_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_postalCountrySubdivision_id] ON [dbo].[PostalAddressContact]
(
	[postalCountrySubdivision_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_postalMunicipality_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_postalMunicipality_id] ON [dbo].[PostalAddressContact]
(
	[postalMunicipality_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_postalPostCode_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_postalPostCode_id] ON [dbo].[PostalAddressContact]
(
	[postalPostCode_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_streetTypeCode_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_streetTypeCode_id] ON [dbo].[PostalAddressContact]
(
	[streetTypeCode_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_amount_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_amount_id] ON [dbo].[Premium]
(
	[amount_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_transactionKeys_id]    Script Date: 10/20/2015 9:41:41 PM ******/
CREATE NONCLUSTERED INDEX [FK_transactionKeys_id] ON [dbo].[TopUp]
(
	[transactionKeys_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BATCH_JOB_EXECUTION_PARAMS] ADD  DEFAULT (NULL) FOR [DATE_VAL]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] ADD  DEFAULT (NULL) FOR [LAUNCH_DATE]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] ADD  DEFAULT (NULL) FOR [SUSPENSION_DATE]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] ADD  DEFAULT (NULL) FOR [TERMINATION_DATE]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] ADD  DEFAULT (NULL) FOR [VERSION_DATE]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] ADD  DEFAULT (NULL) FOR [WITHDRAWAL_DATE]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER] ADD  DEFAULT (NULL) FOR [CREATEDDATE]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER] ADD  DEFAULT (NULL) FOR [UPDATEDDATE]
GO
ALTER TABLE [dbo].[NWS_USER] ADD  DEFAULT (NULL) FOR [givenName]
GO
ALTER TABLE [dbo].[NWS_USER] ADD  DEFAULT (NULL) FOR [mnylAgentCode]
GO
ALTER TABLE [dbo].[NWS_USER] ADD  DEFAULT (NULL) FOR [mnylAgentEmail]
GO
ALTER TABLE [dbo].[NWS_USER] ADD  DEFAULT (NULL) FOR [mnylBossID]
GO
ALTER TABLE [dbo].[NWS_USER] ADD  DEFAULT (NULL) FOR [mnylMyAgentRoleCode]
GO
ALTER TABLE [dbo].[NWS_USER] ADD  DEFAULT (NULL) FOR [mnylStatus]
GO
ALTER TABLE [dbo].[NWS_USER] ADD  DEFAULT (NULL) FOR [title]
GO
ALTER TABLE [dbo].[NWS_USER] ADD  DEFAULT (NULL) FOR [UserMarkedAsDeleted]
GO
ALTER TABLE [dbo].[USER] ADD  DEFAULT (NULL) FOR [ACTIVE]
GO
ALTER TABLE [dbo].[USER] ADD  DEFAULT (NULL) FOR [CREATEDBY]
GO
ALTER TABLE [dbo].[USER] ADD  DEFAULT (NULL) FOR [CREATEDDATE]
GO
ALTER TABLE [dbo].[USER] ADD  DEFAULT (NULL) FOR [UPDATEDBY]
GO
ALTER TABLE [dbo].[USER] ADD  DEFAULT (NULL) FOR [UPDATEDDATE]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK1D0C220D4A69B475] FOREIGN KEY([currencyCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK1D0C220D4A69B475]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK1D0C220D664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK1D0C220D664F1F4C]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK1D0C220D7BFF29EB] FOREIGN KEY([effecitvePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK1D0C220D7BFF29EB]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK1D0C220DC3A8C330] FOREIGN KEY([balanceAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK1D0C220DC3A8C330]
GO
ALTER TABLE [dbo].[Account_Party]  WITH CHECK ADD  CONSTRAINT [FKE4D321464DD6BC7] FOREIGN KEY([owner_id])
REFERENCES [dbo].[Party] ([id])
GO
ALTER TABLE [dbo].[Account_Party] CHECK CONSTRAINT [FKE4D321464DD6BC7]
GO
ALTER TABLE [dbo].[Account_Party]  WITH CHECK ADD  CONSTRAINT [FKE4D321475274348] FOREIGN KEY([Account_id])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[Account_Party] CHECK CONSTRAINT [FKE4D321475274348]
GO
ALTER TABLE [dbo].[ACHIEVEMENT]  WITH CHECK ADD  CONSTRAINT [FKE87E230F94719AC5] FOREIGN KEY([agent_id])
REFERENCES [dbo].[AGENT] ([id])
GO
ALTER TABLE [dbo].[ACHIEVEMENT] CHECK CONSTRAINT [FKE87E230F94719AC5]
GO
ALTER TABLE [dbo].[Agreement]  WITH CHECK ADD  CONSTRAINT [FKB19B71EA489267C6] FOREIGN KEY([issueNation_id])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[Agreement] CHECK CONSTRAINT [FKB19B71EA489267C6]
GO
ALTER TABLE [dbo].[Agreement]  WITH CHECK ADD  CONSTRAINT [FKB19B71EA4A69B475] FOREIGN KEY([currencyCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Agreement] CHECK CONSTRAINT [FKB19B71EA4A69B475]
GO
ALTER TABLE [dbo].[Agreement]  WITH CHECK ADD  CONSTRAINT [FKB19B71EA61F5C5C] FOREIGN KEY([agreementExtension_id])
REFERENCES [dbo].[AgreementExtension] ([id])
GO
ALTER TABLE [dbo].[Agreement] CHECK CONSTRAINT [FKB19B71EA61F5C5C]
GO
ALTER TABLE [dbo].[Agreement]  WITH CHECK ADD  CONSTRAINT [FKB19B71EA664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[Agreement] CHECK CONSTRAINT [FKB19B71EA664F1F4C]
GO
ALTER TABLE [dbo].[Agreement]  WITH CHECK ADD  CONSTRAINT [FKB19B71EA66FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[Agreement] CHECK CONSTRAINT [FKB19B71EA66FE5661]
GO
ALTER TABLE [dbo].[Agreement]  WITH CHECK ADD  CONSTRAINT [FKB19B71EA6D4188C3] FOREIGN KEY([isBasedOn_id])
REFERENCES [dbo].[CORE_PRODUCT_COMPONENT] ([id])
GO
ALTER TABLE [dbo].[Agreement] CHECK CONSTRAINT [FKB19B71EA6D4188C3]
GO
ALTER TABLE [dbo].[Agreement_Coverage]  WITH CHECK ADD  CONSTRAINT [FK971D20BD85563871] FOREIGN KEY([includesCoverage_id])
REFERENCES [dbo].[Coverage] ([id])
GO
ALTER TABLE [dbo].[Agreement_Coverage] CHECK CONSTRAINT [FK971D20BD85563871]
GO
ALTER TABLE [dbo].[Agreement_Coverage]  WITH CHECK ADD  CONSTRAINT [FK971D20BDB3F7DC58] FOREIGN KEY([Agreement_id])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[Agreement_Coverage] CHECK CONSTRAINT [FK971D20BDB3F7DC58]
GO
ALTER TABLE [dbo].[Agreement_Document]  WITH CHECK ADD  CONSTRAINT [FKDF717BF0B3F7DC58] FOREIGN KEY([Agreement_id])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[Agreement_Document] CHECK CONSTRAINT [FKDF717BF0B3F7DC58]
GO
ALTER TABLE [dbo].[Agreement_Document]  WITH CHECK ADD  CONSTRAINT [FKDF717BF0E92EAB95] FOREIGN KEY([relatedDocument_id])
REFERENCES [dbo].[Document] ([id])
GO
ALTER TABLE [dbo].[Agreement_Document] CHECK CONSTRAINT [FKDF717BF0E92EAB95]
GO
ALTER TABLE [dbo].[Agreement_Extension]  WITH CHECK ADD  CONSTRAINT [FKB1FDCC0AA8636878] FOREIGN KEY([includesExtension_id])
REFERENCES [dbo].[Extension] ([id])
GO
ALTER TABLE [dbo].[Agreement_Extension] CHECK CONSTRAINT [FKB1FDCC0AA8636878]
GO
ALTER TABLE [dbo].[Agreement_Extension]  WITH CHECK ADD  CONSTRAINT [FKB1FDCC0AB3F7DC58] FOREIGN KEY([Agreement_id])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[Agreement_Extension] CHECK CONSTRAINT [FKB1FDCC0AB3F7DC58]
GO
ALTER TABLE [dbo].[AGREEMENT_REFERENCEDAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FK982FD9BC52C39951] FOREIGN KEY([REFERENCEDAGREEMENT_ID])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[AGREEMENT_REFERENCEDAGREEMENT] CHECK CONSTRAINT [FK982FD9BC52C39951]
GO
ALTER TABLE [dbo].[AGREEMENT_REFERENCEDAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FK982FD9BCB3F7DC58] FOREIGN KEY([AGREEMENT_ID])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[AGREEMENT_REFERENCEDAGREEMENT] CHECK CONSTRAINT [FK982FD9BCB3F7DC58]
GO
ALTER TABLE [dbo].[AGREEMENT_REPLACEDAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FK6E38E8A5858CB888] FOREIGN KEY([REPLACEDAGREEMENT_ID])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[AGREEMENT_REPLACEDAGREEMENT] CHECK CONSTRAINT [FK6E38E8A5858CB888]
GO
ALTER TABLE [dbo].[AGREEMENT_REPLACEDAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FK6E38E8A5B3F7DC58] FOREIGN KEY([AGREEMENT_ID])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[AGREEMENT_REPLACEDAGREEMENT] CHECK CONSTRAINT [FK6E38E8A5B3F7DC58]
GO
ALTER TABLE [dbo].[AgreementLoan]  WITH CHECK ADD  CONSTRAINT [FKB180A5FA16DBA9C0] FOREIGN KEY([relatedAgreement_id])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[AgreementLoan] CHECK CONSTRAINT [FKB180A5FA16DBA9C0]
GO
ALTER TABLE [dbo].[AgreementLoan]  WITH CHECK ADD  CONSTRAINT [FKB180A5FA23315D38] FOREIGN KEY([loanPeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[AgreementLoan] CHECK CONSTRAINT [FKB180A5FA23315D38]
GO
ALTER TABLE [dbo].[AgreementLoan]  WITH CHECK ADD  CONSTRAINT [FKB180A5FA901A3DF9] FOREIGN KEY([id])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[AgreementLoan] CHECK CONSTRAINT [FKB180A5FA901A3DF9]
GO
ALTER TABLE [dbo].[AgreementLoan]  WITH CHECK ADD  CONSTRAINT [FKB180A5FAC83C039C] FOREIGN KEY([loanAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[AgreementLoan] CHECK CONSTRAINT [FKB180A5FAC83C039C]
GO
ALTER TABLE [dbo].[AgreementLoan]  WITH CHECK ADD  CONSTRAINT [FKB180A5FAD90532BD] FOREIGN KEY([unitOfLoanPeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[AgreementLoan] CHECK CONSTRAINT [FKB180A5FAD90532BD]
GO
ALTER TABLE [dbo].[AgreementWithdrawal]  WITH CHECK ADD  CONSTRAINT [FKCD7DEE3F16DBA9C0] FOREIGN KEY([relatedAgreement_id])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[AgreementWithdrawal] CHECK CONSTRAINT [FKCD7DEE3F16DBA9C0]
GO
ALTER TABLE [dbo].[AgreementWithdrawal]  WITH CHECK ADD  CONSTRAINT [FKCD7DEE3F51479313] FOREIGN KEY([withdrawalPeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[AgreementWithdrawal] CHECK CONSTRAINT [FKCD7DEE3F51479313]
GO
ALTER TABLE [dbo].[AgreementWithdrawal]  WITH CHECK ADD  CONSTRAINT [FKCD7DEE3F901A3DF9] FOREIGN KEY([id])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[AgreementWithdrawal] CHECK CONSTRAINT [FKCD7DEE3F901A3DF9]
GO
ALTER TABLE [dbo].[AgreementWithdrawal]  WITH CHECK ADD  CONSTRAINT [FKCD7DEE3FF6523977] FOREIGN KEY([withdrawalAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[AgreementWithdrawal] CHECK CONSTRAINT [FKCD7DEE3FF6523977]
GO
ALTER TABLE [dbo].[BATCH_JOB_EXECUTION]  WITH CHECK ADD  CONSTRAINT [JOB_INST_EXEC_FK] FOREIGN KEY([JOB_INSTANCE_ID])
REFERENCES [dbo].[BATCH_JOB_INSTANCE] ([JOB_INSTANCE_ID])
GO
ALTER TABLE [dbo].[BATCH_JOB_EXECUTION] CHECK CONSTRAINT [JOB_INST_EXEC_FK]
GO
ALTER TABLE [dbo].[BATCH_JOB_EXECUTION_CONTEXT]  WITH CHECK ADD  CONSTRAINT [JOB_EXEC_CTX_FK] FOREIGN KEY([JOB_EXECUTION_ID])
REFERENCES [dbo].[BATCH_JOB_EXECUTION] ([JOB_EXECUTION_ID])
GO
ALTER TABLE [dbo].[BATCH_JOB_EXECUTION_CONTEXT] CHECK CONSTRAINT [JOB_EXEC_CTX_FK]
GO
ALTER TABLE [dbo].[BATCH_JOB_EXECUTION_PARAMS]  WITH CHECK ADD  CONSTRAINT [JOB_EXEC_PARAMS_FK] FOREIGN KEY([JOB_EXECUTION_ID])
REFERENCES [dbo].[BATCH_JOB_EXECUTION] ([JOB_EXECUTION_ID])
GO
ALTER TABLE [dbo].[BATCH_JOB_EXECUTION_PARAMS] CHECK CONSTRAINT [JOB_EXEC_PARAMS_FK]
GO
ALTER TABLE [dbo].[BATCH_JOB_PARAMS]  WITH CHECK ADD  CONSTRAINT [JOB_INST_PARAMS_FK] FOREIGN KEY([JOB_INSTANCE_ID])
REFERENCES [dbo].[BATCH_JOB_INSTANCE] ([JOB_INSTANCE_ID])
GO
ALTER TABLE [dbo].[BATCH_JOB_PARAMS] CHECK CONSTRAINT [JOB_INST_PARAMS_FK]
GO
ALTER TABLE [dbo].[BATCH_STEP_EXECUTION]  WITH CHECK ADD  CONSTRAINT [JOB_EXEC_STEP_FK] FOREIGN KEY([JOB_EXECUTION_ID])
REFERENCES [dbo].[BATCH_JOB_EXECUTION] ([JOB_EXECUTION_ID])
GO
ALTER TABLE [dbo].[BATCH_STEP_EXECUTION] CHECK CONSTRAINT [JOB_EXEC_STEP_FK]
GO
ALTER TABLE [dbo].[BATCH_STEP_EXECUTION_CONTEXT]  WITH CHECK ADD  CONSTRAINT [STEP_EXEC_CTX_FK] FOREIGN KEY([STEP_EXECUTION_ID])
REFERENCES [dbo].[BATCH_STEP_EXECUTION] ([STEP_EXECUTION_ID])
GO
ALTER TABLE [dbo].[BATCH_STEP_EXECUTION_CONTEXT] CHECK CONSTRAINT [STEP_EXEC_CTX_FK]
GO
ALTER TABLE [dbo].[CODE_LOCALE_LOOKUP]  WITH CHECK ADD  CONSTRAINT [FKA3A3342D64158820] FOREIGN KEY([LOOKUPCODE_ID])
REFERENCES [dbo].[CODE_LOOKUP] ([id])
GO
ALTER TABLE [dbo].[CODE_LOCALE_LOOKUP] CHECK CONSTRAINT [FKA3A3342D64158820]
GO
ALTER TABLE [dbo].[CODE_LOOKUP]  WITH CHECK ADD  CONSTRAINT [FKE401E00C37C1D23A] FOREIGN KEY([CODETYPE_ID])
REFERENCES [dbo].[CODE_TYPE_LOOKUP] ([id])
GO
ALTER TABLE [dbo].[CODE_LOOKUP] CHECK CONSTRAINT [FKE401E00C37C1D23A]
GO
ALTER TABLE [dbo].[CODE_RELATION]  WITH CHECK ADD  CONSTRAINT [FK43A6D00EB9E8A54B] FOREIGN KEY([CHILD_ID])
REFERENCES [dbo].[CODE_LOOKUP] ([id])
GO
ALTER TABLE [dbo].[CODE_RELATION] CHECK CONSTRAINT [FK43A6D00EB9E8A54B]
GO
ALTER TABLE [dbo].[CODE_RELATION]  WITH CHECK ADD  CONSTRAINT [FK43A6D00ED0E694C7] FOREIGN KEY([TYPE_ID])
REFERENCES [dbo].[CODE_TYPE_LOOKUP] ([id])
GO
ALTER TABLE [dbo].[CODE_RELATION] CHECK CONSTRAINT [FK43A6D00ED0E694C7]
GO
ALTER TABLE [dbo].[CODE_RELATION]  WITH CHECK ADD  CONSTRAINT [FK43A6D00ED2653EFD] FOREIGN KEY([PARENT_ID])
REFERENCES [dbo].[CODE_LOOKUP] ([id])
GO
ALTER TABLE [dbo].[CODE_RELATION] CHECK CONSTRAINT [FK43A6D00ED2653EFD]
GO
ALTER TABLE [dbo].[CODE_TYPE_LOOKUP]  WITH CHECK ADD  CONSTRAINT [FK2C4EE6CD778CA37D] FOREIGN KEY([CARRIER_ID])
REFERENCES [dbo].[CORE_CARRIER] ([id])
GO
ALTER TABLE [dbo].[CODE_TYPE_LOOKUP] CHECK CONSTRAINT [FK2C4EE6CD778CA37D]
GO
ALTER TABLE [dbo].[Communication]  WITH CHECK ADD  CONSTRAINT [FKF1E9FFB6664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[Communication] CHECK CONSTRAINT [FKF1E9FFB6664F1F4C]
GO
ALTER TABLE [dbo].[Communication_Communication]  WITH CHECK ADD  CONSTRAINT [FK1A351AED5CEB81BD] FOREIGN KEY([followsUp_id])
REFERENCES [dbo].[Communication] ([id])
GO
ALTER TABLE [dbo].[Communication_Communication] CHECK CONSTRAINT [FK1A351AED5CEB81BD]
GO
ALTER TABLE [dbo].[Communication_Communication]  WITH CHECK ADD  CONSTRAINT [FK1A351AEDB486B044] FOREIGN KEY([Communication_id])
REFERENCES [dbo].[Communication] ([id])
GO
ALTER TABLE [dbo].[Communication_Communication] CHECK CONSTRAINT [FK1A351AEDB486B044]
GO
ALTER TABLE [dbo].[ContactPoint]  WITH CHECK ADD  CONSTRAINT [FK55212970664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[ContactPoint] CHECK CONSTRAINT [FK55212970664F1F4C]
GO
ALTER TABLE [dbo].[ContactPoint]  WITH CHECK ADD  CONSTRAINT [FK5521297095311B7F] FOREIGN KEY([availablePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[ContactPoint] CHECK CONSTRAINT [FK5521297095311B7F]
GO
ALTER TABLE [dbo].[ContactPreference]  WITH CHECK ADD  CONSTRAINT [FKD04354FB54B7F8F5] FOREIGN KEY([preferredContactPoint_id])
REFERENCES [dbo].[ContactPoint] ([id])
GO
ALTER TABLE [dbo].[ContactPreference] CHECK CONSTRAINT [FKD04354FB54B7F8F5]
GO
ALTER TABLE [dbo].[ContactPreference]  WITH CHECK ADD  CONSTRAINT [FKD04354FB664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[ContactPreference] CHECK CONSTRAINT [FKD04354FB664F1F4C]
GO
ALTER TABLE [dbo].[ContactPreference]  WITH CHECK ADD  CONSTRAINT [FKD04354FB95311B7F] FOREIGN KEY([availablePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[ContactPreference] CHECK CONSTRAINT [FKD04354FB95311B7F]
GO
ALTER TABLE [dbo].[CONTENTFILES]  WITH CHECK ADD  CONSTRAINT [FK_CONFILE_CONLANG] FOREIGN KEY([lang_id])
REFERENCES [dbo].[CONTENT_LANGUAGES] ([id])
GO
ALTER TABLE [dbo].[CONTENTFILES] CHECK CONSTRAINT [FK_CONFILE_CONLANG]
GO
ALTER TABLE [dbo].[CONTENTFILES]  WITH CHECK ADD  CONSTRAINT [FKBC11519E360674B9] FOREIGN KEY([Type_Id])
REFERENCES [dbo].[CONTENT_TYPES] ([Id])
GO
ALTER TABLE [dbo].[CONTENTFILES] CHECK CONSTRAINT [FKBC11519E360674B9]
GO
ALTER TABLE [dbo].[CORE_PROD_CALC_SPEC]  WITH CHECK ADD  CONSTRAINT [FK6101AD1D664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_CALC_SPEC] CHECK CONSTRAINT [FK6101AD1D664F1F4C]
GO
ALTER TABLE [dbo].[CORE_PROD_CALC_SPEC]  WITH CHECK ADD  CONSTRAINT [FK6101AD1D82F294BD] FOREIGN KEY([calculationType_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_CALC_SPEC] CHECK CONSTRAINT [FK6101AD1D82F294BD]
GO
ALTER TABLE [dbo].[CORE_PROD_CALC_SPEC]  WITH CHECK ADD  CONSTRAINT [FK6101AD1DF88C706] FOREIGN KEY([parentProduct_id])
REFERENCES [dbo].[CORE_PRODUCT_COMPONENT] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_CALC_SPEC] CHECK CONSTRAINT [FK6101AD1DF88C706]
GO
ALTER TABLE [dbo].[CORE_PROD_CHANNEL_MAPPING]  WITH CHECK ADD  CONSTRAINT [FK3380E10A6439BB74] FOREIGN KEY([PROD_ID])
REFERENCES [dbo].[CORE_PRODUCT_COMPONENT] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_CHANNEL_MAPPING] CHECK CONSTRAINT [FK3380E10A6439BB74]
GO
ALTER TABLE [dbo].[CORE_PROD_CHANNEL_MAPPING]  WITH CHECK ADD  CONSTRAINT [FK3380E10ADBB293F7] FOREIGN KEY([CHANNEL_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_CHANNEL_MAPPING] CHECK CONSTRAINT [FK3380E10ADBB293F7]
GO
ALTER TABLE [dbo].[CORE_PROD_CODE_TYPE_CARRIER]  WITH CHECK ADD  CONSTRAINT [FKE9725D9D778CA37D] FOREIGN KEY([CARRIER_ID])
REFERENCES [dbo].[CORE_CARRIER] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_CODE_TYPE_CARRIER] CHECK CONSTRAINT [FKE9725D9D778CA37D]
GO
ALTER TABLE [dbo].[CORE_PROD_CODE_TYPE_CARRIER]  WITH CHECK ADD  CONSTRAINT [FKE9725D9DCB50B71B] FOREIGN KEY([TYPE_LOOK_UP_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_CODE_TYPE_CARRIER] CHECK CONSTRAINT [FKE9725D9DCB50B71B]
GO
ALTER TABLE [dbo].[CORE_PROD_COLLATERALS]  WITH CHECK ADD  CONSTRAINT [FKF85E6CB42ACD292E] FOREIGN KEY([DOCUMENT_TYPE_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_COLLATERALS] CHECK CONSTRAINT [FKF85E6CB42ACD292E]
GO
ALTER TABLE [dbo].[CORE_PROD_COLLATERALS]  WITH CHECK ADD  CONSTRAINT [FKF85E6CB46439BB74] FOREIGN KEY([PROD_ID])
REFERENCES [dbo].[CORE_PRODUCT_COMPONENT] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_COLLATERALS] CHECK CONSTRAINT [FKF85E6CB46439BB74]
GO
ALTER TABLE [dbo].[CORE_PROD_COLLATERALS]  WITH CHECK ADD  CONSTRAINT [FKF85E6CB485E77EF0] FOREIGN KEY([FILE_TYPE_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_COLLATERALS] CHECK CONSTRAINT [FKF85E6CB485E77EF0]
GO
ALTER TABLE [dbo].[CORE_PROD_FUND_MAPPING]  WITH CHECK ADD  CONSTRAINT [FKA17E8C7C6439BB74] FOREIGN KEY([PROD_ID])
REFERENCES [dbo].[CORE_PRODUCT_COMPONENT] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_FUND_MAPPING] CHECK CONSTRAINT [FKA17E8C7C6439BB74]
GO
ALTER TABLE [dbo].[CORE_PROD_FUND_MAPPING]  WITH CHECK ADD  CONSTRAINT [FKA17E8C7CAC6B2091] FOREIGN KEY([FUND_ID])
REFERENCES [dbo].[CORE_PROD_FUNDS] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_FUND_MAPPING] CHECK CONSTRAINT [FKA17E8C7CAC6B2091]
GO
ALTER TABLE [dbo].[CORE_PROD_FUNDS]  WITH CHECK ADD  CONSTRAINT [FK883823E6A0AB747B] FOREIGN KEY([FUND_CATEGORY1_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_FUNDS] CHECK CONSTRAINT [FK883823E6A0AB747B]
GO
ALTER TABLE [dbo].[CORE_PROD_FUNDS]  WITH CHECK ADD  CONSTRAINT [FK883823E6A0ABE8DA] FOREIGN KEY([FUND_CATEGORY2_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_FUNDS] CHECK CONSTRAINT [FK883823E6A0ABE8DA]
GO
ALTER TABLE [dbo].[CORE_PROD_PROPERTIES]  WITH CHECK ADD  CONSTRAINT [FKBA64667B664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_PROPERTIES] CHECK CONSTRAINT [FKBA64667B664F1F4C]
GO
ALTER TABLE [dbo].[CORE_PROD_PROPERTIES]  WITH CHECK ADD  CONSTRAINT [FKBA64667BAB849456] FOREIGN KEY([propertyType_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_PROPERTIES] CHECK CONSTRAINT [FKBA64667BAB849456]
GO
ALTER TABLE [dbo].[CORE_PROD_PROPERTIES]  WITH CHECK ADD  CONSTRAINT [FKBA64667BF88C706] FOREIGN KEY([parentProduct_id])
REFERENCES [dbo].[CORE_PRODUCT_COMPONENT] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_PROPERTIES] CHECK CONSTRAINT [FKBA64667BF88C706]
GO
ALTER TABLE [dbo].[CORE_PROD_RULE_SPEC]  WITH CHECK ADD  CONSTRAINT [FK320560D61E3DD5B7] FOREIGN KEY([ruleType_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_RULE_SPEC] CHECK CONSTRAINT [FK320560D61E3DD5B7]
GO
ALTER TABLE [dbo].[CORE_PROD_RULE_SPEC]  WITH CHECK ADD  CONSTRAINT [FK320560D6664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_RULE_SPEC] CHECK CONSTRAINT [FK320560D6664F1F4C]
GO
ALTER TABLE [dbo].[CORE_PROD_RULE_SPEC]  WITH CHECK ADD  CONSTRAINT [FK320560D6F88C706] FOREIGN KEY([parentProduct_id])
REFERENCES [dbo].[CORE_PRODUCT_COMPONENT] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_RULE_SPEC] CHECK CONSTRAINT [FK320560D6F88C706]
GO
ALTER TABLE [dbo].[CORE_PROD_TEMPLATE_MAPPING]  WITH CHECK ADD  CONSTRAINT [FK7C90C5314ACB87D0] FOREIGN KEY([TEMPLATE_TYPE])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PROD_TEMPLATE_MAPPING] CHECK CONSTRAINT [FK7C90C5314ACB87D0]
GO
ALTER TABLE [dbo].[CORE_PROD_TEMPLATE_MAPPING]  WITH CHECK ADD  CONSTRAINT [FK7C90C5316439BB74] FOREIGN KEY([PROD_ID])
REFERENCES [dbo].[CORE_PRODUCT_COMPONENT] ([id])
GO
ALTER TABLE [dbo].[CORE_PROD_TEMPLATE_MAPPING] CHECK CONSTRAINT [FK7C90C5316439BB74]
GO
ALTER TABLE [dbo].[CORE_PROD_TEMPLATE_MAPPING]  WITH CHECK ADD  CONSTRAINT [FK7C90C531943044A7] FOREIGN KEY([TEMPLATE_ID])
REFERENCES [dbo].[CORE_PROD_TEMPLATES] ([TEMPLATE_ID])
GO
ALTER TABLE [dbo].[CORE_PROD_TEMPLATE_MAPPING] CHECK CONSTRAINT [FK7C90C531943044A7]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CD24F27897] FOREIGN KEY([investmentType_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CD24F27897]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CD446C502B] FOREIGN KEY([lineOfBusiness_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CD446C502B]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CD477D547D] FOREIGN KEY([productType_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CD477D547D]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CD4C30A71] FOREIGN KEY([category_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CD4C30A71]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CD664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CD664F1F4C]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CD6E98F43D] FOREIGN KEY([coverType_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CD6E98F43D]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CD778CA37D] FOREIGN KEY([carrier_id])
REFERENCES [dbo].[CORE_CARRIER] ([id])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CD778CA37D]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CD8810ABFD] FOREIGN KEY([channelType_ID])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CD8810ABFD]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CDAE25FFF6] FOREIGN KEY([PRODUCT_GROUP])
REFERENCES [dbo].[CORE_PROD_CODE_TYPE_LOOK_UP] ([ID])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CDAE25FFF6]
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT]  WITH CHECK ADD  CONSTRAINT [FKE47990CDF88C706] FOREIGN KEY([parentProduct_id])
REFERENCES [dbo].[CORE_PRODUCT_COMPONENT] ([id])
GO
ALTER TABLE [dbo].[CORE_PRODUCT_COMPONENT] CHECK CONSTRAINT [FKE47990CDF88C706]
GO
ALTER TABLE [dbo].[CORE_PROPERTY_VALUES]  WITH CHECK ADD  CONSTRAINT [FK4CF2562C2A881A5D] FOREIGN KEY([propertySpecification_id])
REFERENCES [dbo].[CORE_PROD_PROPERTIES] ([id])
GO
ALTER TABLE [dbo].[CORE_PROPERTY_VALUES] CHECK CONSTRAINT [FK4CF2562C2A881A5D]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA08128C26EC] FOREIGN KEY([maxRegularTopUp_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA08128C26EC]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA081636FE40] FOREIGN KEY([minsumAssured_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA081636FE40]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA081A058C6E] FOREIGN KEY([maxsumAssured_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA081A058C6E]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA081BDD41D7] FOREIGN KEY([productCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA081BDD41D7]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA0822D04F52] FOREIGN KEY([sumAssured_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA0822D04F52]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA084B2EF119] FOREIGN KEY([coverageLimitAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA084B2EF119]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA0854DA0F40] FOREIGN KEY([extendedReportingDuration_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA0854DA0F40]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA0865747E03] FOREIGN KEY([id])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA0865747E03]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA08706EB0D0] FOREIGN KEY([annualLimitAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA08706EB0D0]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA08851E8C67] FOREIGN KEY([waitingPeriodDuration_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA08851E8C67]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA0897237565] FOREIGN KEY([exposureAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA0897237565]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA08C8286C3E] FOREIGN KEY([minRegularTopUp_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA08C8286C3E]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA08DFC5AD45] FOREIGN KEY([sumInsured_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA08DFC5AD45]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA08FA3119C8] FOREIGN KEY([eligibilityDuration_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA08FA3119C8]
GO
ALTER TABLE [dbo].[Coverage]  WITH CHECK ADD  CONSTRAINT [FKEEE2DA08FE673697] FOREIGN KEY([deductibleAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Coverage] CHECK CONSTRAINT [FKEEE2DA08FE673697]
GO
ALTER TABLE [dbo].[Coverage_FundInformation]  WITH CHECK ADD  CONSTRAINT [FKEF2CD4D0628E3359] FOREIGN KEY([fundInformation_id])
REFERENCES [dbo].[FundInformation] ([id])
GO
ALTER TABLE [dbo].[Coverage_FundInformation] CHECK CONSTRAINT [FKEF2CD4D0628E3359]
GO
ALTER TABLE [dbo].[Coverage_FundInformation]  WITH CHECK ADD  CONSTRAINT [FKEF2CD4D07E21561C] FOREIGN KEY([Coverage_id])
REFERENCES [dbo].[Coverage] ([id])
GO
ALTER TABLE [dbo].[Coverage_FundInformation] CHECK CONSTRAINT [FKEF2CD4D07E21561C]
GO
ALTER TABLE [dbo].[Coverage_Premium]  WITH CHECK ADD  CONSTRAINT [FKB2595F007C15EB9B] FOREIGN KEY([premium_id])
REFERENCES [dbo].[Premium] ([id])
GO
ALTER TABLE [dbo].[Coverage_Premium] CHECK CONSTRAINT [FKB2595F007C15EB9B]
GO
ALTER TABLE [dbo].[Coverage_Premium]  WITH CHECK ADD  CONSTRAINT [FKB2595F007E21561C] FOREIGN KEY([Coverage_id])
REFERENCES [dbo].[Coverage] ([id])
GO
ALTER TABLE [dbo].[Coverage_Premium] CHECK CONSTRAINT [FKB2595F007E21561C]
GO
ALTER TABLE [dbo].[Coverage_TopUp]  WITH CHECK ADD  CONSTRAINT [FK19F971B97E21561C] FOREIGN KEY([Coverage_id])
REFERENCES [dbo].[Coverage] ([id])
GO
ALTER TABLE [dbo].[Coverage_TopUp] CHECK CONSTRAINT [FK19F971B97E21561C]
GO
ALTER TABLE [dbo].[Coverage_TopUp]  WITH CHECK ADD  CONSTRAINT [FK19F971B9F8953383] FOREIGN KEY([includesTopUp_id])
REFERENCES [dbo].[TopUp] ([id])
GO
ALTER TABLE [dbo].[Coverage_TopUp] CHECK CONSTRAINT [FK19F971B9F8953383]
GO
ALTER TABLE [dbo].[CurrencyAmount]  WITH CHECK ADD  CONSTRAINT [FK9247E7E94A69B475] FOREIGN KEY([currencyCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[CurrencyAmount] CHECK CONSTRAINT [FK9247E7E94A69B475]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK3737353B664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK3737353B664F1F4C]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK3737353B66FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK3737353B66FE5661]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK3737353BD929234E] FOREIGN KEY([languageCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK3737353BD929234E]
GO
ALTER TABLE [dbo].[Document_Document]  WITH CHECK ADD  CONSTRAINT [FKB49DE3BF42C77B50] FOREIGN KEY([Document_id])
REFERENCES [dbo].[Document] ([id])
GO
ALTER TABLE [dbo].[Document_Document] CHECK CONSTRAINT [FKB49DE3BF42C77B50]
GO
ALTER TABLE [dbo].[Document_Document]  WITH CHECK ADD  CONSTRAINT [FKB49DE3BF57DC7007] FOREIGN KEY([pages_id])
REFERENCES [dbo].[Document] ([id])
GO
ALTER TABLE [dbo].[Document_Document] CHECK CONSTRAINT [FKB49DE3BF57DC7007]
GO
ALTER TABLE [dbo].[ElectronicContact]  WITH CHECK ADD  CONSTRAINT [FK52F0A5E4AAE64E67] FOREIGN KEY([id])
REFERENCES [dbo].[ContactPoint] ([id])
GO
ALTER TABLE [dbo].[ElectronicContact] CHECK CONSTRAINT [FK52F0A5E4AAE64E67]
GO
ALTER TABLE [dbo].[FamilyHealthHistory]  WITH CHECK ADD  CONSTRAINT [FKD3F29974664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[FamilyHealthHistory] CHECK CONSTRAINT [FKD3F29974664F1F4C]
GO
ALTER TABLE [dbo].[FAMILYMEMBER_FAMILYRELATIONSHIP]  WITH CHECK ADD  CONSTRAINT [FK63C1BE7D9704C28E] FOREIGN KEY([FAMILYRELATIONSHIP_ID])
REFERENCES [dbo].[PartyRoleRelationship] ([id])
GO
ALTER TABLE [dbo].[FAMILYMEMBER_FAMILYRELATIONSHIP] CHECK CONSTRAINT [FK63C1BE7D9704C28E]
GO
ALTER TABLE [dbo].[FAMILYMEMBER_FAMILYRELATIONSHIP]  WITH CHECK ADD  CONSTRAINT [FK63C1BE7DF52C5809] FOREIGN KEY([FAMILYMEMBER_ID])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[FAMILYMEMBER_FAMILYRELATIONSHIP] CHECK CONSTRAINT [FK63C1BE7DF52C5809]
GO
ALTER TABLE [dbo].[FinancialProvision]  WITH CHECK ADD  CONSTRAINT [FK10AEA26C664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[FinancialProvision] CHECK CONSTRAINT [FK10AEA26C664F1F4C]
GO
ALTER TABLE [dbo].[FinancialProvision]  WITH CHECK ADD  CONSTRAINT [FK10AEA26C66FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[FinancialProvision] CHECK CONSTRAINT [FK10AEA26C66FE5661]
GO
ALTER TABLE [dbo].[FinancialProvision]  WITH CHECK ADD  CONSTRAINT [FK10AEA26CD07FA42F] FOREIGN KEY([isDefinedFrom_id])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[FinancialProvision] CHECK CONSTRAINT [FK10AEA26CD07FA42F]
GO
ALTER TABLE [dbo].[FinancialProvision_FinancialScheduler]  WITH CHECK ADD  CONSTRAINT [FK40719F056BD634AC] FOREIGN KEY([FinancialProvision_id])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[FinancialProvision_FinancialScheduler] CHECK CONSTRAINT [FK40719F056BD634AC]
GO
ALTER TABLE [dbo].[FinancialProvision_FinancialScheduler]  WITH CHECK ADD  CONSTRAINT [FK40719F05842893C8] FOREIGN KEY([attachedFinancialScheduler_id])
REFERENCES [dbo].[FinancialScheduler] ([id])
GO
ALTER TABLE [dbo].[FinancialProvision_FinancialScheduler] CHECK CONSTRAINT [FK40719F05842893C8]
GO
ALTER TABLE [dbo].[FinancialProvision_Installment]  WITH CHECK ADD  CONSTRAINT [FKC3375C065466B2CF] FOREIGN KEY([basedOn_id])
REFERENCES [dbo].[Installment] ([id])
GO
ALTER TABLE [dbo].[FinancialProvision_Installment] CHECK CONSTRAINT [FKC3375C065466B2CF]
GO
ALTER TABLE [dbo].[FinancialProvision_Installment]  WITH CHECK ADD  CONSTRAINT [FKC3375C066BD634AC] FOREIGN KEY([FinancialProvision_id])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[FinancialProvision_Installment] CHECK CONSTRAINT [FKC3375C066BD634AC]
GO
ALTER TABLE [dbo].[FINANCIALPROVISION_ISBASEDFORFINANCIALPROVISION]  WITH CHECK ADD  CONSTRAINT [FK9784C2BF6BD634AC] FOREIGN KEY([FINANCIALPROVISION_ID])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[FINANCIALPROVISION_ISBASEDFORFINANCIALPROVISION] CHECK CONSTRAINT [FK9784C2BF6BD634AC]
GO
ALTER TABLE [dbo].[FINANCIALPROVISION_ISBASEDFORFINANCIALPROVISION]  WITH CHECK ADD  CONSTRAINT [FK9784C2BFF55E954C] FOREIGN KEY([ISBASEDFORFINANCIALPROVISION_ID])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[FINANCIALPROVISION_ISBASEDFORFINANCIALPROVISION] CHECK CONSTRAINT [FK9784C2BFF55E954C]
GO
ALTER TABLE [dbo].[FINANCIALPROVISION_ISBASEDONFINANCIALPROVISION]  WITH CHECK ADD  CONSTRAINT [FK70A165616BD634AC] FOREIGN KEY([FINANCIALPROVISION_ID])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[FINANCIALPROVISION_ISBASEDONFINANCIALPROVISION] CHECK CONSTRAINT [FK70A165616BD634AC]
GO
ALTER TABLE [dbo].[FINANCIALPROVISION_ISBASEDONFINANCIALPROVISION]  WITH CHECK ADD  CONSTRAINT [FK70A165618E8A9544] FOREIGN KEY([ISBASEDONFINANCIALPROVISION_ID])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[FINANCIALPROVISION_ISBASEDONFINANCIALPROVISION] CHECK CONSTRAINT [FK70A165618E8A9544]
GO
ALTER TABLE [dbo].[FinancialScheduler]  WITH CHECK ADD  CONSTRAINT [FK27B6CA52664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[FinancialScheduler] CHECK CONSTRAINT [FK27B6CA52664F1F4C]
GO
ALTER TABLE [dbo].[FinancialScheduler]  WITH CHECK ADD  CONSTRAINT [FK27B6CA5266FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[FinancialScheduler] CHECK CONSTRAINT [FK27B6CA5266FE5661]
GO
ALTER TABLE [dbo].[FinancialScheduler]  WITH CHECK ADD  CONSTRAINT [FK27B6CA526D8D593D] FOREIGN KEY([referencePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[FinancialScheduler] CHECK CONSTRAINT [FK27B6CA526D8D593D]
GO
ALTER TABLE [dbo].[FinancialScheduler]  WITH CHECK ADD  CONSTRAINT [FK27B6CA52C2CE168A] FOREIGN KEY([paymentMeans_id])
REFERENCES [dbo].[PaymentMethod] ([id])
GO
ALTER TABLE [dbo].[FinancialScheduler] CHECK CONSTRAINT [FK27B6CA52C2CE168A]
GO
ALTER TABLE [dbo].[FinancialScheduler]  WITH CHECK ADD  CONSTRAINT [FK27B6CA52E2400B96] FOREIGN KEY([relatedBankAccount_id])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[FinancialScheduler] CHECK CONSTRAINT [FK27B6CA52E2400B96]
GO
ALTER TABLE [dbo].[FinancialServicesAgreement]  WITH CHECK ADD  CONSTRAINT [FKE79F6DE365747E03] FOREIGN KEY([id])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[FinancialServicesAgreement] CHECK CONSTRAINT [FKE79F6DE365747E03]
GO
ALTER TABLE [dbo].[FinancialServicesAgreement_Premium]  WITH CHECK ADD  CONSTRAINT [FK515DE7DB6C42A77C] FOREIGN KEY([FinancialServicesAgreement_id])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[FinancialServicesAgreement_Premium] CHECK CONSTRAINT [FK515DE7DB6C42A77C]
GO
ALTER TABLE [dbo].[FinancialServicesAgreement_Premium]  WITH CHECK ADD  CONSTRAINT [FK515DE7DBE92FB925] FOREIGN KEY([agreementPremium_id])
REFERENCES [dbo].[Premium] ([id])
GO
ALTER TABLE [dbo].[FinancialServicesAgreement_Premium] CHECK CONSTRAINT [FK515DE7DBE92FB925]
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_INTEGRATEDINFINSERVAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FKAFFD6637422D967E] FOREIGN KEY([INTEGRATEDINFINSERVAGREEMENT_ID])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_INTEGRATEDINFINSERVAGREEMENT] CHECK CONSTRAINT [FKAFFD6637422D967E]
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_INTEGRATEDINFINSERVAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FKAFFD66376C42A77C] FOREIGN KEY([FINANCIALSERVICESAGREEMENT_ID])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_INTEGRATEDINFINSERVAGREEMENT] CHECK CONSTRAINT [FKAFFD66376C42A77C]
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_INTEGRATESFINSERVAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FKDB0A57616C42A77C] FOREIGN KEY([FINANCIALSERVICESAGREEMENT_ID])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_INTEGRATESFINSERVAGREEMENT] CHECK CONSTRAINT [FKDB0A57616C42A77C]
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_INTEGRATESFINSERVAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FKDB0A57619AFA1694] FOREIGN KEY([INTEGRATESFINSERVAGREEMENT_ID])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_INTEGRATESFINSERVAGREEMENT] CHECK CONSTRAINT [FKDB0A57619AFA1694]
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_ISPAIDBYFINSERVAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FK3572B74C6C42A77C] FOREIGN KEY([FINANCIALSERVICESAGREEMENT_ID])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_ISPAIDBYFINSERVAGREEMENT] CHECK CONSTRAINT [FK3572B74C6C42A77C]
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_ISPAIDBYFINSERVAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FK3572B74CED948449] FOREIGN KEY([ISPAIDBYFINSERVAGREEMENT_ID])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_ISPAIDBYFINSERVAGREEMENT] CHECK CONSTRAINT [FK3572B74CED948449]
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_PAYSFINSERVAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FK5057614A6C42A77C] FOREIGN KEY([FINANCIALSERVICESAGREEMENT_ID])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_PAYSFINSERVAGREEMENT] CHECK CONSTRAINT [FK5057614A6C42A77C]
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_PAYSFINSERVAGREEMENT]  WITH CHECK ADD  CONSTRAINT [FK5057614A6CB8848B] FOREIGN KEY([PAYSFINSERVAGREEMENT_ID])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[FINSERVAGREEMENT_PAYSFINSERVAGREEMENT] CHECK CONSTRAINT [FK5057614A6CB8848B]
GO
ALTER TABLE [dbo].[FundInformation]  WITH CHECK ADD  CONSTRAINT [FK22CDB507664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[FundInformation] CHECK CONSTRAINT [FK22CDB507664F1F4C]
GO
ALTER TABLE [dbo].[GoalAndNeed]  WITH CHECK ADD  CONSTRAINT [FK6802E85A664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[GoalAndNeed] CHECK CONSTRAINT [FK6802E85A664F1F4C]
GO
ALTER TABLE [dbo].[IllustrationResponsePayLoad]  WITH CHECK ADD  CONSTRAINT [FK2A81AE9D664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[IllustrationResponsePayLoad] CHECK CONSTRAINT [FK2A81AE9D664F1F4C]
GO
ALTER TABLE [dbo].[IllustrationResponsePayLoad]  WITH CHECK ADD  CONSTRAINT [FK2A81AE9DB3F7DC58] FOREIGN KEY([agreement_id])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[IllustrationResponsePayLoad] CHECK CONSTRAINT [FK2A81AE9DB3F7DC58]
GO
ALTER TABLE [dbo].[Installment]  WITH CHECK ADD  CONSTRAINT [FKE3389D593EED18D0] FOREIGN KEY([withoutSurchargeAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Installment] CHECK CONSTRAINT [FKE3389D593EED18D0]
GO
ALTER TABLE [dbo].[Installment]  WITH CHECK ADD  CONSTRAINT [FKE3389D596307C31C] FOREIGN KEY([netInitialPremiumAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Installment] CHECK CONSTRAINT [FKE3389D596307C31C]
GO
ALTER TABLE [dbo].[Installment]  WITH CHECK ADD  CONSTRAINT [FKE3389D59664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[Installment] CHECK CONSTRAINT [FKE3389D59664F1F4C]
GO
ALTER TABLE [dbo].[Installment]  WITH CHECK ADD  CONSTRAINT [FKE3389D598BE248E2] FOREIGN KEY([paymentPeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[Installment] CHECK CONSTRAINT [FKE3389D598BE248E2]
GO
ALTER TABLE [dbo].[Installment]  WITH CHECK ADD  CONSTRAINT [FKE3389D59912DBDD1] FOREIGN KEY([netInstallmentPremiumAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Installment] CHECK CONSTRAINT [FKE3389D59912DBDD1]
GO
ALTER TABLE [dbo].[Installment]  WITH CHECK ADD  CONSTRAINT [FKE3389D59B3D5EB2B] FOREIGN KEY([netWithSurchargeAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Installment] CHECK CONSTRAINT [FKE3389D59B3D5EB2B]
GO
ALTER TABLE [dbo].[InsuranceAgreement]  WITH CHECK ADD  CONSTRAINT [FK6299C7022D04F52] FOREIGN KEY([sumAssured_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[InsuranceAgreement] CHECK CONSTRAINT [FK6299C7022D04F52]
GO
ALTER TABLE [dbo].[InsuranceAgreement]  WITH CHECK ADD  CONSTRAINT [FK6299C7025E097C0] FOREIGN KEY([id])
REFERENCES [dbo].[FinancialServicesAgreement] ([id])
GO
ALTER TABLE [dbo].[InsuranceAgreement] CHECK CONSTRAINT [FK6299C7025E097C0]
GO
ALTER TABLE [dbo].[InsuranceAgreement]  WITH CHECK ADD  CONSTRAINT [FK6299C7051DE583E] FOREIGN KEY([maturityBenefit_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[InsuranceAgreement] CHECK CONSTRAINT [FK6299C7051DE583E]
GO
ALTER TABLE [dbo].[InsuranceAgreement]  WITH CHECK ADD  CONSTRAINT [FK6299C70EC920452] FOREIGN KEY([policyValueLastYear_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[InsuranceAgreement] CHECK CONSTRAINT [FK6299C70EC920452]
GO
ALTER TABLE [dbo].[InsuranceAgreement]  WITH CHECK ADD  CONSTRAINT [FK6299C70F740A05] FOREIGN KEY([policyValue_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[InsuranceAgreement] CHECK CONSTRAINT [FK6299C70F740A05]
GO
ALTER TABLE [dbo].[LE_LOGIN_CHANNEL_AUD]  WITH CHECK ADD  CONSTRAINT [FK1F3BA3F8DF74E053] FOREIGN KEY([REV])
REFERENCES [dbo].[REVINFO] ([REV])
GO
ALTER TABLE [dbo].[LE_LOGIN_CHANNEL_AUD] CHECK CONSTRAINT [FK1F3BA3F8DF74E053]
GO
ALTER TABLE [dbo].[LE_LOGIN_ROLE_AUD]  WITH CHECK ADD  CONSTRAINT [FKE988FB03DF74E053] FOREIGN KEY([REV])
REFERENCES [dbo].[REVINFO] ([REV])
GO
ALTER TABLE [dbo].[LE_LOGIN_ROLE_AUD] CHECK CONSTRAINT [FKE988FB03DF74E053]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_AUD]  WITH CHECK ADD  CONSTRAINT [FKE98AB3D8DF74E053] FOREIGN KEY([REV])
REFERENCES [dbo].[REVINFO] ([REV])
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_AUD] CHECK CONSTRAINT [FKE98AB3D8DF74E053]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_CHANNEL]  WITH CHECK ADD  CONSTRAINT [FK4E74DA2B5578FF54] FOREIGN KEY([USER_ID])
REFERENCES [dbo].[LE_LOGIN_USER] ([ID])
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_CHANNEL] CHECK CONSTRAINT [FK4E74DA2B5578FF54]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_CHANNEL]  WITH CHECK ADD  CONSTRAINT [FK4E74DA2BDA97DF20] FOREIGN KEY([CHANNEL_ID])
REFERENCES [dbo].[LE_LOGIN_CHANNEL] ([ID])
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_CHANNEL] CHECK CONSTRAINT [FK4E74DA2BDA97DF20]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_CHANNEL_AUD]  WITH CHECK ADD  CONSTRAINT [FKF701FA7CDF74E053] FOREIGN KEY([REV])
REFERENCES [dbo].[REVINFO] ([REV])
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_CHANNEL_AUD] CHECK CONSTRAINT [FKF701FA7CDF74E053]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_ROLE]  WITH CHECK ADD  CONSTRAINT [FK47D36C2E5578FF54] FOREIGN KEY([USER_ID])
REFERENCES [dbo].[LE_LOGIN_USER] ([ID])
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_ROLE] CHECK CONSTRAINT [FK47D36C2E5578FF54]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_ROLE]  WITH CHECK ADD  CONSTRAINT [FK47D36C2EB04E3B74] FOREIGN KEY([ROLE_ID])
REFERENCES [dbo].[LE_LOGIN_ROLE] ([ID])
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_ROLE] CHECK CONSTRAINT [FK47D36C2EB04E3B74]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_ROLE_AUD]  WITH CHECK ADD  CONSTRAINT [FK1AF1D2FFDF74E053] FOREIGN KEY([REV])
REFERENCES [dbo].[REVINFO] ([REV])
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_ROLE_AUD] CHECK CONSTRAINT [FK1AF1D2FFDF74E053]
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_TOKEN]  WITH CHECK ADD  CONSTRAINT [FKB2B645215578FF54] FOREIGN KEY([USER_ID])
REFERENCES [dbo].[LE_LOGIN_USER] ([ID])
GO
ALTER TABLE [dbo].[LE_LOGIN_USER_TOKEN] CHECK CONSTRAINT [FKB2B645215578FF54]
GO
ALTER TABLE [dbo].[LifeEngagePayloadAudit]  WITH CHECK ADD  CONSTRAINT [FK8F43892AAC262CD8] FOREIGN KEY([lifeEngageAuditId])
REFERENCES [dbo].[LifeEngageAudit] ([id])
GO
ALTER TABLE [dbo].[LifeEngagePayloadAudit] CHECK CONSTRAINT [FK8F43892AAC262CD8]
GO
ALTER TABLE [dbo].[Measurement]  WITH CHECK ADD  CONSTRAINT [FKF75C839C6D3DD3A2] FOREIGN KEY([unitCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Measurement] CHECK CONSTRAINT [FKF75C839C6D3DD3A2]
GO
ALTER TABLE [dbo].[Organization]  WITH CHECK ADD  CONSTRAINT [FK501041531BBDABB] FOREIGN KEY([id])
REFERENCES [dbo].[Party] ([id])
GO
ALTER TABLE [dbo].[Organization] CHECK CONSTRAINT [FK501041531BBDABB]
GO
ALTER TABLE [dbo].[Organization]  WITH CHECK ADD  CONSTRAINT [FK501041535B386933] FOREIGN KEY([accountingPeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[Organization] CHECK CONSTRAINT [FK501041535B386933]
GO
ALTER TABLE [dbo].[Organization_OrganizationDetail]  WITH CHECK ADD  CONSTRAINT [FKB2A0A1F0458BA871] FOREIGN KEY([detail_id])
REFERENCES [dbo].[OrganizationDetail] ([id])
GO
ALTER TABLE [dbo].[Organization_OrganizationDetail] CHECK CONSTRAINT [FKB2A0A1F0458BA871]
GO
ALTER TABLE [dbo].[Organization_OrganizationDetail]  WITH CHECK ADD  CONSTRAINT [FKB2A0A1F09F6028E0] FOREIGN KEY([Organization_id])
REFERENCES [dbo].[Organization] ([id])
GO
ALTER TABLE [dbo].[Organization_OrganizationDetail] CHECK CONSTRAINT [FKB2A0A1F09F6028E0]
GO
ALTER TABLE [dbo].[Organization_PartyName]  WITH CHECK ADD  CONSTRAINT [FK87D536859F6028E0] FOREIGN KEY([Organization_id])
REFERENCES [dbo].[Organization] ([id])
GO
ALTER TABLE [dbo].[Organization_PartyName] CHECK CONSTRAINT [FK87D536859F6028E0]
GO
ALTER TABLE [dbo].[Organization_PartyName]  WITH CHECK ADD  CONSTRAINT [FK87D53685E9620110] FOREIGN KEY([name_id])
REFERENCES [dbo].[PartyName] ([id])
GO
ALTER TABLE [dbo].[Organization_PartyName] CHECK CONSTRAINT [FK87D53685E9620110]
GO
ALTER TABLE [dbo].[OrganizationDetail]  WITH CHECK ADD  CONSTRAINT [FK198FBB84664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[OrganizationDetail] CHECK CONSTRAINT [FK198FBB84664F1F4C]
GO
ALTER TABLE [dbo].[OrganizationDetail]  WITH CHECK ADD  CONSTRAINT [FK198FBB8466FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[OrganizationDetail] CHECK CONSTRAINT [FK198FBB8466FE5661]
GO
ALTER TABLE [dbo].[OrganizationDetail]  WITH CHECK ADD  CONSTRAINT [FK198FBB846D8D593D] FOREIGN KEY([referencePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[OrganizationDetail] CHECK CONSTRAINT [FK198FBB846D8D593D]
GO
ALTER TABLE [dbo].[Party]  WITH CHECK ADD  CONSTRAINT [FK4952AC6664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[Party] CHECK CONSTRAINT [FK4952AC6664F1F4C]
GO
ALTER TABLE [dbo].[Party_ContactPreference]  WITH CHECK ADD  CONSTRAINT [FKB18C83C2489F72B4] FOREIGN KEY([Party_id])
REFERENCES [dbo].[Party] ([id])
GO
ALTER TABLE [dbo].[Party_ContactPreference] CHECK CONSTRAINT [FKB18C83C2489F72B4]
GO
ALTER TABLE [dbo].[Party_ContactPreference]  WITH CHECK ADD  CONSTRAINT [FKB18C83C2B4933F7A] FOREIGN KEY([preferredContact_id])
REFERENCES [dbo].[ContactPreference] ([id])
GO
ALTER TABLE [dbo].[Party_ContactPreference] CHECK CONSTRAINT [FKB18C83C2B4933F7A]
GO
ALTER TABLE [dbo].[Party_Extension]  WITH CHECK ADD  CONSTRAINT [FKB6BB21E6489F72B4] FOREIGN KEY([Party_id])
REFERENCES [dbo].[Party] ([id])
GO
ALTER TABLE [dbo].[Party_Extension] CHECK CONSTRAINT [FKB6BB21E6489F72B4]
GO
ALTER TABLE [dbo].[Party_Extension]  WITH CHECK ADD  CONSTRAINT [FKB6BB21E6A8636878] FOREIGN KEY([includesExtension_id])
REFERENCES [dbo].[Extension] ([id])
GO
ALTER TABLE [dbo].[Party_Extension] CHECK CONSTRAINT [FKB6BB21E6A8636878]
GO
ALTER TABLE [dbo].[Party_TaxRegistration]  WITH CHECK ADD  CONSTRAINT [FKC946C06B489F72B4] FOREIGN KEY([Party_id])
REFERENCES [dbo].[Party] ([id])
GO
ALTER TABLE [dbo].[Party_TaxRegistration] CHECK CONSTRAINT [FKC946C06B489F72B4]
GO
ALTER TABLE [dbo].[Party_TaxRegistration]  WITH CHECK ADD  CONSTRAINT [FKC946C06B5ABFA0DE] FOREIGN KEY([relatedTaxRegistrations_id])
REFERENCES [dbo].[TaxRegistration] ([id])
GO
ALTER TABLE [dbo].[Party_TaxRegistration] CHECK CONSTRAINT [FKC946C06B5ABFA0DE]
GO
ALTER TABLE [dbo].[PartyName]  WITH CHECK ADD  CONSTRAINT [FKCFB4671664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[PartyName] CHECK CONSTRAINT [FKCFB4671664F1F4C]
GO
ALTER TABLE [dbo].[PartyName]  WITH CHECK ADD  CONSTRAINT [FKCFB467166FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[PartyName] CHECK CONSTRAINT [FKCFB467166FE5661]
GO
ALTER TABLE [dbo].[PartyName]  WITH CHECK ADD  CONSTRAINT [FKCFB4671D929234E] FOREIGN KEY([languageCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[PartyName] CHECK CONSTRAINT [FKCFB4671D929234E]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement]  WITH CHECK ADD  CONSTRAINT [FK8233A869664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement] CHECK CONSTRAINT [FK8233A869664F1F4C]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement]  WITH CHECK ADD  CONSTRAINT [FK8233A86966FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement] CHECK CONSTRAINT [FK8233A86966FE5661]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement]  WITH CHECK ADD  CONSTRAINT [FK8233A8696AC99C95] FOREIGN KEY([playerParty_id])
REFERENCES [dbo].[Party] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement] CHECK CONSTRAINT [FK8233A8696AC99C95]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement]  WITH CHECK ADD  CONSTRAINT [FK8233A86989823872] FOREIGN KEY([rolePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement] CHECK CONSTRAINT [FK8233A86989823872]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement]  WITH CHECK ADD  CONSTRAINT [FK8233A8698B5A7CA4] FOREIGN KEY([lossTimePeriodDuration_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement] CHECK CONSTRAINT [FK8233A8698B5A7CA4]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement]  WITH CHECK ADD  CONSTRAINT [FK8233A869C360908A] FOREIGN KEY([declaredClaimAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement] CHECK CONSTRAINT [FK8233A869C360908A]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement]  WITH CHECK ADD  CONSTRAINT [FK8233A869C5FB6099] FOREIGN KEY([noClaimDiscountDuration_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement] CHECK CONSTRAINT [FK8233A869C5FB6099]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement]  WITH CHECK ADD  CONSTRAINT [FK8233A869E88B642B] FOREIGN KEY([isPartyRoleIn_id])
REFERENCES [dbo].[Agreement] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement] CHECK CONSTRAINT [FK8233A869E88B642B]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_Communication]  WITH CHECK ADD  CONSTRAINT [FKC8E40E604A713FA6] FOREIGN KEY([PartyRoleInAgreement_id])
REFERENCES [dbo].[PartyRoleInAgreement] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_Communication] CHECK CONSTRAINT [FKC8E40E604A713FA6]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_Communication]  WITH CHECK ADD  CONSTRAINT [FKC8E40E60C8A0BA34] FOREIGN KEY([receivesCommunication_id])
REFERENCES [dbo].[Communication] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_Communication] CHECK CONSTRAINT [FKC8E40E60C8A0BA34]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_FamilyHealthHistory]  WITH CHECK ADD  CONSTRAINT [FKBF8D809E7B9D8138] FOREIGN KEY([PartyRoleInAgreement_id])
REFERENCES [dbo].[PartyRoleInAgreement] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_FamilyHealthHistory] CHECK CONSTRAINT [FKBF8D809E7B9D8138]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_FamilyHealthHistory]  WITH CHECK ADD  CONSTRAINT [FKBF8D809EF2F34499] FOREIGN KEY([familyHealthHistory_id])
REFERENCES [dbo].[FamilyHealthHistory] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_FamilyHealthHistory] CHECK CONSTRAINT [FKBF8D809EF2F34499]
GO
ALTER TABLE [dbo].[PARTYROLEINAGREEMENT_PARTYROLERELATIONSHIP]  WITH CHECK ADD  CONSTRAINT [FK8E58E55E14FD83E7] FOREIGN KEY([PARTYROLERELATIONSHIP_ID])
REFERENCES [dbo].[PartyRoleRelationship] ([id])
GO
ALTER TABLE [dbo].[PARTYROLEINAGREEMENT_PARTYROLERELATIONSHIP] CHECK CONSTRAINT [FK8E58E55E14FD83E7]
GO
ALTER TABLE [dbo].[PARTYROLEINAGREEMENT_PARTYROLERELATIONSHIP]  WITH CHECK ADD  CONSTRAINT [FK8E58E55E4A713FA6] FOREIGN KEY([PARTYROLEINAGREEMENT_ID])
REFERENCES [dbo].[PartyRoleInAgreement] ([id])
GO
ALTER TABLE [dbo].[PARTYROLEINAGREEMENT_PARTYROLERELATIONSHIP] CHECK CONSTRAINT [FK8E58E55E4A713FA6]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_UserSelection]  WITH CHECK ADD  CONSTRAINT [FK1561F88B7B9D8138] FOREIGN KEY([PartyRoleInAgreement_id])
REFERENCES [dbo].[PartyRoleInAgreement] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_UserSelection] CHECK CONSTRAINT [FK1561F88B7B9D8138]
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_UserSelection]  WITH CHECK ADD  CONSTRAINT [FK1561F88BF68A4322] FOREIGN KEY([userSelectedOptions_id])
REFERENCES [dbo].[UserSelection] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInAgreement_UserSelection] CHECK CONSTRAINT [FK1561F88BF68A4322]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FK5B5D8F7922D23111] FOREIGN KEY([baseSalaryAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship] CHECK CONSTRAINT [FK5B5D8F7922D23111]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FK5B5D8F7933239203] FOREIGN KEY([bonusSalaryAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship] CHECK CONSTRAINT [FK5B5D8F7933239203]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FK5B5D8F7952B768F9] FOREIGN KEY([proposer_id])
REFERENCES [dbo].[PartyRoleInAgreement] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship] CHECK CONSTRAINT [FK5B5D8F7952B768F9]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FK5B5D8F79664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship] CHECK CONSTRAINT [FK5B5D8F79664F1F4C]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FK5B5D8F7966FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship] CHECK CONSTRAINT [FK5B5D8F7966FE5661]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FK5B5D8F796AC99C95] FOREIGN KEY([playerParty_id])
REFERENCES [dbo].[Party] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship] CHECK CONSTRAINT [FK5B5D8F796AC99C95]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FK5B5D8F7989823872] FOREIGN KEY([rolePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship] CHECK CONSTRAINT [FK5B5D8F7989823872]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FK5B5D8F798B835B4E] FOREIGN KEY([customerRelationship_id])
REFERENCES [dbo].[PartyRoleRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship] CHECK CONSTRAINT [FK5B5D8F798B835B4E]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FK5B5D8F79F923009B] FOREIGN KEY([annualTaxableBenefitAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship] CHECK CONSTRAINT [FK5B5D8F79F923009B]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Communication]  WITH CHECK ADD  CONSTRAINT [FK80E459709E27650E] FOREIGN KEY([PartyRoleInRelationship_id])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Communication] CHECK CONSTRAINT [FK80E459709E27650E]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Communication]  WITH CHECK ADD  CONSTRAINT [FK80E45970C8A0BA34] FOREIGN KEY([receivesCommunication_id])
REFERENCES [dbo].[Communication] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Communication] CHECK CONSTRAINT [FK80E45970C8A0BA34]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Extension]  WITH CHECK ADD  CONSTRAINT [FKA53F82D99FEBB22E] FOREIGN KEY([PartyRoleInRelationship_id])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Extension] CHECK CONSTRAINT [FKA53F82D99FEBB22E]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Extension]  WITH CHECK ADD  CONSTRAINT [FKA53F82D9A8636878] FOREIGN KEY([includesExtension_id])
REFERENCES [dbo].[Extension] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Extension] CHECK CONSTRAINT [FKA53F82D9A8636878]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FKAA2688B39FEBB22E] FOREIGN KEY([PartyRoleInRelationship_id])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleInRelationship] CHECK CONSTRAINT [FKAA2688B39FEBB22E]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleInRelationship]  WITH CHECK ADD  CONSTRAINT [FKAA2688B3C6CDF3D] FOREIGN KEY([relatedCustomers_id])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleInRelationship] CHECK CONSTRAINT [FKAA2688B3C6CDF3D]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK89FB6C6EC2ED18FE] FOREIGN KEY([PartyRoleInRelationship_id])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleRelationship] CHECK CONSTRAINT [FK89FB6C6EC2ED18FE]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK89FB6C6EC2ED190B] FOREIGN KEY([PartyRoleInRelationship_id])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleRelationship] CHECK CONSTRAINT [FK89FB6C6EC2ED190B]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK89FB6C6ED679DAC6] FOREIGN KEY([employment_id])
REFERENCES [dbo].[PartyRoleRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PartyRoleRelationship] CHECK CONSTRAINT [FK89FB6C6ED679DAC6]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PurchasedProduct]  WITH CHECK ADD  CONSTRAINT [FK8B6838129FEBB22E] FOREIGN KEY([PartyRoleInRelationship_id])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PurchasedProduct] CHECK CONSTRAINT [FK8B6838129FEBB22E]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PurchasedProduct]  WITH CHECK ADD  CONSTRAINT [FK8B683812DC0770FC] FOREIGN KEY([purchasedProducts_id])
REFERENCES [dbo].[PurchasedProduct] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_PurchasedProduct] CHECK CONSTRAINT [FK8B683812DC0770FC]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Specification]  WITH CHECK ADD  CONSTRAINT [FKC83CA9D4A37E3AF] FOREIGN KEY([questionnaire_id])
REFERENCES [dbo].[Specification] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Specification] CHECK CONSTRAINT [FKC83CA9D4A37E3AF]
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Specification]  WITH CHECK ADD  CONSTRAINT [FKC83CA9D9FEBB22E] FOREIGN KEY([PartyRoleInRelationship_id])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleInRelationship_Specification] CHECK CONSTRAINT [FKC83CA9D9FEBB22E]
GO
ALTER TABLE [dbo].[PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK10B16CB422D23111] FOREIGN KEY([baseSalaryAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PartyRoleRelationship] CHECK CONSTRAINT [FK10B16CB422D23111]
GO
ALTER TABLE [dbo].[PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK10B16CB433239203] FOREIGN KEY([bonusSalaryAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PartyRoleRelationship] CHECK CONSTRAINT [FK10B16CB433239203]
GO
ALTER TABLE [dbo].[PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK10B16CB44622A03F] FOREIGN KEY([grossIncomeAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PartyRoleRelationship] CHECK CONSTRAINT [FK10B16CB44622A03F]
GO
ALTER TABLE [dbo].[PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK10B16CB4664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[PartyRoleRelationship] CHECK CONSTRAINT [FK10B16CB4664F1F4C]
GO
ALTER TABLE [dbo].[PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK10B16CB466FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[PartyRoleRelationship] CHECK CONSTRAINT [FK10B16CB466FE5661]
GO
ALTER TABLE [dbo].[PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK10B16CB497FE1DA3] FOREIGN KEY([disposableIncomeAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PartyRoleRelationship] CHECK CONSTRAINT [FK10B16CB497FE1DA3]
GO
ALTER TABLE [dbo].[PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK10B16CB4CFDB3A5E] FOREIGN KEY([relatedCustomer_id])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[PartyRoleRelationship] CHECK CONSTRAINT [FK10B16CB4CFDB3A5E]
GO
ALTER TABLE [dbo].[PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FK10B16CB4F923009B] FOREIGN KEY([annualTaxableBenefitAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PartyRoleRelationship] CHECK CONSTRAINT [FK10B16CB4F923009B]
GO
ALTER TABLE [dbo].[PaymentMethod]  WITH CHECK ADD  CONSTRAINT [FK79DE02675DA0EFA1] FOREIGN KEY([cardholderName_id])
REFERENCES [dbo].[PartyName] ([id])
GO
ALTER TABLE [dbo].[PaymentMethod] CHECK CONSTRAINT [FK79DE02675DA0EFA1]
GO
ALTER TABLE [dbo].[PaymentMethod]  WITH CHECK ADD  CONSTRAINT [FK79DE0267664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[PaymentMethod] CHECK CONSTRAINT [FK79DE0267664F1F4C]
GO
ALTER TABLE [dbo].[PaymentMethod]  WITH CHECK ADD  CONSTRAINT [FK79DE02678E8C3699] FOREIGN KEY([fromAccount_id])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[PaymentMethod] CHECK CONSTRAINT [FK79DE02678E8C3699]
GO
ALTER TABLE [dbo].[PaymentMethod]  WITH CHECK ADD  CONSTRAINT [FK79DE0267E2400B96] FOREIGN KEY([relatedBankAccount_id])
REFERENCES [dbo].[Account] ([id])
GO
ALTER TABLE [dbo].[PaymentMethod] CHECK CONSTRAINT [FK79DE0267E2400B96]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK8E4887751BBDABB] FOREIGN KEY([id])
REFERENCES [dbo].[Party] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK8E4887751BBDABB]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK8E48877575539F3D] FOREIGN KEY([weight_id])
REFERENCES [dbo].[Measurement] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK8E48877575539F3D]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK8E4887758C391565] FOREIGN KEY([birthAddress_id])
REFERENCES [dbo].[PostalAddressContact] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK8E4887758C391565]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK8E488775C0DB8450] FOREIGN KEY([spouseOrParentName_id])
REFERENCES [dbo].[PartyName] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK8E488775C0DB8450]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK8E488775C5511E2E] FOREIGN KEY([height_id])
REFERENCES [dbo].[Measurement] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK8E488775C5511E2E]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK8E488775E792110C] FOREIGN KEY([primaryLanguageCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK8E488775E792110C]
GO
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FK8E488775EC44D956] FOREIGN KEY([residenceCountry_id])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FK8E488775EC44D956]
GO
ALTER TABLE [dbo].[Person_CitizenshipCountry]  WITH CHECK ADD  CONSTRAINT [FK2816C27C68D0DA20] FOREIGN KEY([person_Id])
REFERENCES [dbo].[Person] ([id])
GO
ALTER TABLE [dbo].[Person_CitizenshipCountry] CHECK CONSTRAINT [FK2816C27C68D0DA20]
GO
ALTER TABLE [dbo].[Person_CitizenshipCountry]  WITH CHECK ADD  CONSTRAINT [FK2816C27C9D440DD0] FOREIGN KEY([Country_Id])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[Person_CitizenshipCountry] CHECK CONSTRAINT [FK2816C27C9D440DD0]
GO
ALTER TABLE [dbo].[Person_NationalityCountry]  WITH CHECK ADD  CONSTRAINT [FK46FE9B6468D0DA20] FOREIGN KEY([person_Id])
REFERENCES [dbo].[Person] ([id])
GO
ALTER TABLE [dbo].[Person_NationalityCountry] CHECK CONSTRAINT [FK46FE9B6468D0DA20]
GO
ALTER TABLE [dbo].[Person_NationalityCountry]  WITH CHECK ADD  CONSTRAINT [FK46FE9B649D440DD0] FOREIGN KEY([Country_Id])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[Person_NationalityCountry] CHECK CONSTRAINT [FK46FE9B649D440DD0]
GO
ALTER TABLE [dbo].[Person_PartyName]  WITH CHECK ADD  CONSTRAINT [FKAF4DBA273E24D472] FOREIGN KEY([name_id])
REFERENCES [dbo].[PartyName] ([id])
GO
ALTER TABLE [dbo].[Person_PartyName] CHECK CONSTRAINT [FKAF4DBA273E24D472]
GO
ALTER TABLE [dbo].[Person_PartyName]  WITH CHECK ADD  CONSTRAINT [FKAF4DBA2768D0DA20] FOREIGN KEY([Person_id])
REFERENCES [dbo].[Person] ([id])
GO
ALTER TABLE [dbo].[Person_PartyName] CHECK CONSTRAINT [FKAF4DBA2768D0DA20]
GO
ALTER TABLE [dbo].[Person_PersonDetail]  WITH CHECK ADD  CONSTRAINT [FK6196C8B068D0DA20] FOREIGN KEY([Person_id])
REFERENCES [dbo].[Person] ([id])
GO
ALTER TABLE [dbo].[Person_PersonDetail] CHECK CONSTRAINT [FK6196C8B068D0DA20]
GO
ALTER TABLE [dbo].[Person_PersonDetail]  WITH CHECK ADD  CONSTRAINT [FK6196C8B0986BF813] FOREIGN KEY([detail_id])
REFERENCES [dbo].[PersonDetail] ([id])
GO
ALTER TABLE [dbo].[Person_PersonDetail] CHECK CONSTRAINT [FK6196C8B0986BF813]
GO
ALTER TABLE [dbo].[PersonDetail]  WITH CHECK ADD  CONSTRAINT [FKC88F60262399609C] FOREIGN KEY([providingHousehold_id])
REFERENCES [dbo].[PartyRoleRelationship] ([id])
GO
ALTER TABLE [dbo].[PersonDetail] CHECK CONSTRAINT [FKC88F60262399609C]
GO
ALTER TABLE [dbo].[PersonDetail]  WITH CHECK ADD  CONSTRAINT [FKC88F60262E8F118F] FOREIGN KEY([netAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PersonDetail] CHECK CONSTRAINT [FKC88F60262E8F118F]
GO
ALTER TABLE [dbo].[PersonDetail]  WITH CHECK ADD  CONSTRAINT [FKC88F6026431C692C] FOREIGN KEY([disposableAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PersonDetail] CHECK CONSTRAINT [FKC88F6026431C692C]
GO
ALTER TABLE [dbo].[PersonDetail]  WITH CHECK ADD  CONSTRAINT [FKC88F60264E21D9F0] FOREIGN KEY([annualIncomeOfSpouse_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PersonDetail] CHECK CONSTRAINT [FKC88F60264E21D9F0]
GO
ALTER TABLE [dbo].[PersonDetail]  WITH CHECK ADD  CONSTRAINT [FKC88F6026664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[PersonDetail] CHECK CONSTRAINT [FKC88F6026664F1F4C]
GO
ALTER TABLE [dbo].[PersonDetail]  WITH CHECK ADD  CONSTRAINT [FKC88F602666FE5661] FOREIGN KEY([effectivePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[PersonDetail] CHECK CONSTRAINT [FKC88F602666FE5661]
GO
ALTER TABLE [dbo].[PersonDetail]  WITH CHECK ADD  CONSTRAINT [FKC88F60267D0E10C8] FOREIGN KEY([grossAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PersonDetail] CHECK CONSTRAINT [FKC88F60267D0E10C8]
GO
ALTER TABLE [dbo].[PersonDetail]  WITH CHECK ADD  CONSTRAINT [FKC88F6026BABA93A8] FOREIGN KEY([estGrossAnnualOtherIncome_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PersonDetail] CHECK CONSTRAINT [FKC88F6026BABA93A8]
GO
ALTER TABLE [dbo].[PersonDetail_PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FKFC75CD5B9F64F279] FOREIGN KEY([PersonDetail_id])
REFERENCES [dbo].[PersonDetail] ([id])
GO
ALTER TABLE [dbo].[PersonDetail_PartyRoleRelationship] CHECK CONSTRAINT [FKFC75CD5B9F64F279]
GO
ALTER TABLE [dbo].[PersonDetail_PartyRoleRelationship]  WITH CHECK ADD  CONSTRAINT [FKFC75CD5BAC561468] FOREIGN KEY([providingEmployment_id])
REFERENCES [dbo].[PartyRoleRelationship] ([id])
GO
ALTER TABLE [dbo].[PersonDetail_PartyRoleRelationship] CHECK CONSTRAINT [FKFC75CD5BAC561468]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK499E8E749151818] FOREIGN KEY([assignedCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK499E8E749151818]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK499E8E74ADAB03B] FOREIGN KEY([relatedCountry_id])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK499E8E74ADAB03B]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK499E8E7664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK499E8E7664F1F4C]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK499E8E795311B7F] FOREIGN KEY([availablePeriod_id])
REFERENCES [dbo].[TimePeriod] ([id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK499E8E795311B7F]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK499E8E79869755F] FOREIGN KEY([alphaISOCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK499E8E79869755F]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK499E8E7A884A8B2] FOREIGN KEY([postaladdresscontactid])
REFERENCES [dbo].[PostalAddressContact] ([id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK499E8E7A884A8B2]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK499E8E7D04E5B7A] FOREIGN KEY([extendedISOCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK499E8E7D04E5B7A]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK499E8E7DCD703BB] FOREIGN KEY([relatedid])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK499E8E7DCD703BB]
GO
ALTER TABLE [dbo].[Place]  WITH CHECK ADD  CONSTRAINT [FK499E8E7F7E74150] FOREIGN KEY([telephonePrefixCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Place] CHECK CONSTRAINT [FK499E8E7F7E74150]
GO
ALTER TABLE [dbo].[PostalAddressContact]  WITH CHECK ADD  CONSTRAINT [FKE053C3571947B0B9] FOREIGN KEY([postalCountrySubdivision_id])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[PostalAddressContact] CHECK CONSTRAINT [FKE053C3571947B0B9]
GO
ALTER TABLE [dbo].[PostalAddressContact]  WITH CHECK ADD  CONSTRAINT [FKE053C3571B07EDF9] FOREIGN KEY([postalMunicipality_id])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[PostalAddressContact] CHECK CONSTRAINT [FKE053C3571B07EDF9]
GO
ALTER TABLE [dbo].[PostalAddressContact]  WITH CHECK ADD  CONSTRAINT [FKE053C35777FC0549] FOREIGN KEY([streetTypeCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[PostalAddressContact] CHECK CONSTRAINT [FKE053C35777FC0549]
GO
ALTER TABLE [dbo].[PostalAddressContact]  WITH CHECK ADD  CONSTRAINT [FKE053C3578803C8DB] FOREIGN KEY([postalCountry_id])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[PostalAddressContact] CHECK CONSTRAINT [FKE053C3578803C8DB]
GO
ALTER TABLE [dbo].[PostalAddressContact]  WITH CHECK ADD  CONSTRAINT [FKE053C35788693F9] FOREIGN KEY([postalPostCode_id])
REFERENCES [dbo].[Place] ([id])
GO
ALTER TABLE [dbo].[PostalAddressContact] CHECK CONSTRAINT [FKE053C35788693F9]
GO
ALTER TABLE [dbo].[PostalAddressContact]  WITH CHECK ADD  CONSTRAINT [FKE053C357AAE64E67] FOREIGN KEY([id])
REFERENCES [dbo].[ContactPoint] ([id])
GO
ALTER TABLE [dbo].[PostalAddressContact] CHECK CONSTRAINT [FKE053C357AAE64E67]
GO
ALTER TABLE [dbo].[Premium]  WITH CHECK ADD  CONSTRAINT [FK503D663770B83A8C] FOREIGN KEY([amount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Premium] CHECK CONSTRAINT [FK503D663770B83A8C]
GO
ALTER TABLE [dbo].[Premium]  WITH CHECK ADD  CONSTRAINT [FK503D6637901A3DF9] FOREIGN KEY([id])
REFERENCES [dbo].[FinancialProvision] ([id])
GO
ALTER TABLE [dbo].[Premium] CHECK CONSTRAINT [FK503D6637901A3DF9]
GO
ALTER TABLE [dbo].[Premium_Installment]  WITH CHECK ADD  CONSTRAINT [FKD9FC7513F96E2AD] FOREIGN KEY([resultingInstallment_id])
REFERENCES [dbo].[Installment] ([id])
GO
ALTER TABLE [dbo].[Premium_Installment] CHECK CONSTRAINT [FKD9FC7513F96E2AD]
GO
ALTER TABLE [dbo].[Premium_Installment]  WITH CHECK ADD  CONSTRAINT [FKD9FC7517C15EB9B] FOREIGN KEY([Premium_id])
REFERENCES [dbo].[Premium] ([id])
GO
ALTER TABLE [dbo].[Premium_Installment] CHECK CONSTRAINT [FKD9FC7517C15EB9B]
GO
ALTER TABLE [dbo].[PurchasedProduct]  WITH CHECK ADD  CONSTRAINT [FKBD8A40C664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[PurchasedProduct] CHECK CONSTRAINT [FKBD8A40C664F1F4C]
GO
ALTER TABLE [dbo].[PurchasedProduct]  WITH CHECK ADD  CONSTRAINT [FKBD8A40C6C74882D] FOREIGN KEY([premium_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[PurchasedProduct] CHECK CONSTRAINT [FKBD8A40C6C74882D]
GO
ALTER TABLE [dbo].[Specification]  WITH CHECK ADD  CONSTRAINT [FK7D8970E3664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[Specification] CHECK CONSTRAINT [FK7D8970E3664F1F4C]
GO
ALTER TABLE [dbo].[Specification]  WITH CHECK ADD  CONSTRAINT [FK7D8970E395142A14] FOREIGN KEY([estimatedMarketAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Specification] CHECK CONSTRAINT [FK7D8970E395142A14]
GO
ALTER TABLE [dbo].[Specification]  WITH CHECK ADD  CONSTRAINT [FK7D8970E3D929234E] FOREIGN KEY([languageCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[Specification] CHECK CONSTRAINT [FK7D8970E3D929234E]
GO
ALTER TABLE [dbo].[Specification]  WITH CHECK ADD  CONSTRAINT [FK7D8970E3DDB34FD7] FOREIGN KEY([listPriceNewAmount_id])
REFERENCES [dbo].[CurrencyAmount] ([id])
GO
ALTER TABLE [dbo].[Specification] CHECK CONSTRAINT [FK7D8970E3DDB34FD7]
GO
ALTER TABLE [dbo].[SPOUSE_CIVILRELATIONSHIP]  WITH CHECK ADD  CONSTRAINT [FK6101F06153474FC9] FOREIGN KEY([SPOUSE_ID])
REFERENCES [dbo].[PartyRoleInRelationship] ([id])
GO
ALTER TABLE [dbo].[SPOUSE_CIVILRELATIONSHIP] CHECK CONSTRAINT [FK6101F06153474FC9]
GO
ALTER TABLE [dbo].[SPOUSE_CIVILRELATIONSHIP]  WITH CHECK ADD  CONSTRAINT [FK6101F0616AB89946] FOREIGN KEY([CIVILRELATIONSHIP_ID])
REFERENCES [dbo].[PartyRoleRelationship] ([id])
GO
ALTER TABLE [dbo].[SPOUSE_CIVILRELATIONSHIP] CHECK CONSTRAINT [FK6101F0616AB89946]
GO
ALTER TABLE [dbo].[TaxRegistration]  WITH CHECK ADD  CONSTRAINT [FK97C28BE4664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[TaxRegistration] CHECK CONSTRAINT [FK97C28BE4664F1F4C]
GO
ALTER TABLE [dbo].[TelephoneCallContact]  WITH CHECK ADD  CONSTRAINT [FKEE0B95E95BD9079] FOREIGN KEY([areaCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[TelephoneCallContact] CHECK CONSTRAINT [FKEE0B95E95BD9079]
GO
ALTER TABLE [dbo].[TelephoneCallContact]  WITH CHECK ADD  CONSTRAINT [FKEE0B95E9A48BD50] FOREIGN KEY([countryCode_id])
REFERENCES [dbo].[ExternalCode] ([id])
GO
ALTER TABLE [dbo].[TelephoneCallContact] CHECK CONSTRAINT [FKEE0B95E9A48BD50]
GO
ALTER TABLE [dbo].[TelephoneCallContact]  WITH CHECK ADD  CONSTRAINT [FKEE0B95EAAE64E67] FOREIGN KEY([id])
REFERENCES [dbo].[ContactPoint] ([id])
GO
ALTER TABLE [dbo].[TelephoneCallContact] CHECK CONSTRAINT [FKEE0B95EAAE64E67]
GO
ALTER TABLE [dbo].[TopUp]  WITH CHECK ADD  CONSTRAINT [FK4D3DAB0664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[TopUp] CHECK CONSTRAINT [FK4D3DAB0664F1F4C]
GO
ALTER TABLE [dbo].[UserSelection]  WITH CHECK ADD  CONSTRAINT [FK3E67E9E1664F1F4C] FOREIGN KEY([transactionKeys_id])
REFERENCES [dbo].[TransactionKeys] ([id])
GO
ALTER TABLE [dbo].[UserSelection] CHECK CONSTRAINT [FK3E67E9E1664F1F4C]
GO
ALTER TABLE [dbo].[UserSelection_SelectedOption]  WITH CHECK ADD  CONSTRAINT [FKCF6DF92E92D380D9] FOREIGN KEY([UserSelection_id])
REFERENCES [dbo].[UserSelection] ([id])
GO
ALTER TABLE [dbo].[UserSelection_SelectedOption] CHECK CONSTRAINT [FKCF6DF92E92D380D9]
GO
ALTER TABLE [dbo].[UserSelection_SelectedOption]  WITH CHECK ADD  CONSTRAINT [FKCF6DF92E9AD91F28] FOREIGN KEY([selectedOptions_id])
REFERENCES [dbo].[SelectedOption] ([id])
GO
ALTER TABLE [dbo].[UserSelection_SelectedOption] CHECK CONSTRAINT [FKCF6DF92E9AD91F28]

GO


--Agent Insert

INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68019570', N'Business Director', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68002619', N'Business Director', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68000614', N'Field consultant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68000382', N'Business Director', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68000042', N'Field consultant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68000022', N'Business Director', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68000002', N'Business Manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68006299', N'Business Manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68001551', N'Business Manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68005610', N'Field consultant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AGENT] ( [agentCode], [briefWriteUp], [business_sourced], [email_id], [emp_type], [fullname], [agent_group], [license_exp_date], [license_issue_date], [license_num], [mob_num], [name_of_excelsheet], [num_of_service], [office], [role], [supervisor_code], [unit], [uploaded_timestamp], [userid], [yoe], [trustFactor], [go], [employeeId], [advisorCategory], [admCode]) VALUES ( N'68005800', N'Field consultant', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)

--Login Credentials

INSERT [dbo].[AGENT_APP_LOGIN] ([userid], [password], [active], [agentcode]) VALUES (N'admin', N'21232f297a57a5a743894a0e4a801fc3', N'active', NULL)
INSERT [dbo].[AGENT_APP_LOGIN] ([userid], [password], [active], [agentcode]) VALUES (N'admin1', N'*4ACFE3202A5FF5CF467', N'active', NULL)
INSERT [dbo].[AGENT_APP_LOGIN] ([userid], [password], [active], [agentcode]) VALUES (N'admin2', N'*4ACFE3202A5FF5CF467898FC58AAB1D615029441', N'active', NULL)

--Content Admin

INSERT [dbo].[APPLICATION_CONTEXTS] ([Id], [Context]) VALUES (1, N'common')
INSERT [dbo].[APPLICATION_CONTEXTS] ([Id], [Context]) VALUES (2, N'generali')

INSERT [dbo].[CONTENT_LANGUAGES] ([id], [name]) VALUES (1, N'English')
INSERT [dbo].[CONTENT_LANGUAGES] ([id], [name]) VALUES (2, N'Thai')
INSERT [dbo].[CONTENT_LANGUAGES] ([id], [name]) VALUES (3, N'Malayalam')
INSERT [dbo].[CONTENT_LANGUAGES] ([id], [name]) VALUES (4, N'Hindi')
INSERT [dbo].[CONTENT_LANGUAGES] ([id], [name]) VALUES (5, N'Tamil')
INSERT [dbo].[CONTENT_LANGUAGES] ([id], [name]) VALUES (6, N'Telugu')
INSERT [dbo].[CONTENT_LANGUAGES] ([id], [name]) VALUES (7, N'Kannada')
INSERT [dbo].[CONTENT_LANGUAGES] ([id], [name]) VALUES (8, N'Marathi')


INSERT [dbo].[CONTENT_TYPES] ([Id], [Type], [priority], [extensions], [optionality], [previewable]) VALUES (1, N'PDF', 3, N'pdf', 1, 0)
INSERT [dbo].[CONTENT_TYPES] ([Id], [Type], [priority], [extensions], [optionality], [previewable]) VALUES (2, N'Image', 2, N'jpg,jpeg,png', 1, 0)
INSERT [dbo].[CONTENT_TYPES] ([Id], [Type], [priority], [extensions], [optionality], [previewable]) VALUES (3, N'Offline DB', 1, N'db', 1, 0)
INSERT [dbo].[CONTENT_TYPES] ([Id], [Type], [priority], [extensions], [optionality], [previewable]) VALUES (4, N'Validation Rule', 1, N'js', 1, 0)
INSERT [dbo].[CONTENT_TYPES] ([Id], [Type], [priority], [extensions], [optionality], [previewable]) VALUES (5, N'Illustration Rule', 1, N'js', 1, 0)
INSERT [dbo].[CONTENT_TYPES] ([Id], [Type], [priority], [extensions], [optionality], [previewable]) VALUES (6, N'Video', 4, N'avi,wmv,mpg,mpeg,mkv,3gp,mp4', 1, 0)
INSERT [dbo].[CONTENT_TYPES] ([Id], [Type], [priority], [extensions], [optionality], [previewable]) VALUES (7, N'Config Files', 1, N'properties,json', 1, 0)
INSERT [dbo].[CONTENT_TYPES] ([Id], [Type], [priority], [extensions], [optionality], [previewable]) VALUES (8, N'HTML', 1, N'html,htm', 1, 0)
INSERT [dbo].[CONTENT_TYPES] ([Id], [Type], [priority], [extensions], [optionality], [previewable]) VALUES (9, N'dummy', 5, N'xls', 1, 0)


