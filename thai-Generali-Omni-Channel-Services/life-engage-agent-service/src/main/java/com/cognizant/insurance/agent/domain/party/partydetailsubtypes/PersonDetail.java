/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partydetailsubtypes;


import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.cognizant.insurance.agent.domain.party.PartyDetail;

/**
 * This concept is a generalizing concept providing further details about a
 * person. Some details regarding a party are not likely to be necessary to
 * perform basic processes about parties. As such, they have been separated from
 * basic party information and identified as details. 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PERSON_DETAIL_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class PersonDetail extends PartyDetail {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4221488282969382130L;
}
