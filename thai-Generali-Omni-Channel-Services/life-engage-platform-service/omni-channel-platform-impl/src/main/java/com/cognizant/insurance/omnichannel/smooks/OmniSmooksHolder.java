package com.cognizant.insurance.omnichannel.smooks;

import java.io.IOException;

import org.milyn.Smooks;
import org.milyn.container.ExecutionContext;
import org.milyn.payload.JavaResult;
import org.milyn.payload.JavaSource;
import org.xml.sax.SAXException;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;

public class OmniSmooksHolder extends LifeEngageSmooksHolder {
	
	 private Smooks boToBPMSmooks;

	public Smooks getBoToBPMSmooks() {
		return boToBPMSmooks;
	}

	
	public final void setBoToBPMFile(final String jsonToBoMappingFile) {
        try {
        	boToBPMSmooks = new Smooks(jsonToBoMappingFile);
        } catch (IOException e) {
            throw new SystemException(e);
        } catch (SAXException e) {
            throw new SystemException(e);
        }
    }
	public Object parseToDM(Object srcObject) {
		
		 ExecutionContext executionContext = boToBPMSmooks.createExecutionContext();
		 
	        // Transform the source Order to the target LineOrder via a
	        // JavaSource and JavaResult instance...
	        JavaSource source = new JavaSource(srcObject);
	        JavaResult result = new JavaResult();
	 
	 /*       // Configure the execution context to generate a report...
	        try {
				executionContext.setEventListener(new HtmlReportGenerator("target/report/report.html"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
	 
	        boToBPMSmooks.filterSource(executionContext, source, result);
	 
	        return  result.getBean(Constants.RESULT);
		
	}
	 

}
