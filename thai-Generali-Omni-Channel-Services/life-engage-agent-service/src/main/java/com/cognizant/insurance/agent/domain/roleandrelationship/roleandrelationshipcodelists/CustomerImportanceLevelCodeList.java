/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 19, 2015
 */

package com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists;

/**
 * Identifies a classification of customers according to their importance level.
 * 
 * @author 300797
 * 
 * 
 */
public enum CustomerImportanceLevelCodeList {
    /**
     * Identifies a Customer with importance level '1: highest priority (VIP)'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    HighestPriority,
    /**
     * Identifies a Customer with importance level '2: high priority'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    HighPriority,
    /**
     * Identifies a Customer with importance level '3: normal priority (VIP)'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    NormalPriority,
    /**
     * Identifies a Customer with importance level '4: low priority'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    LowPriority,
    
    /** Not Mentioned. */
    NotMentioned,
    
    /** Warm. */ 
    Warm,
    
    /** Hot. */
    Hot,
    
    /** Cold. */
    Cold,
    
    /** RedHot. */
    RedHot,
    
    //Added for Generali Thailand
    VeryHot
}
