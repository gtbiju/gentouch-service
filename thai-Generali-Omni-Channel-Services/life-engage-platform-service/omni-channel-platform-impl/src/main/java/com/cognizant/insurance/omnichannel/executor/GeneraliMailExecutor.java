package com.cognizant.insurance.omnichannel.executor;

import java.util.Date;

import org.json.JSONObject;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.component.LifeEngageEmailComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.EmailRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.eMail.LifeEngageEmail;
import com.cognizant.insurance.executor.LifeEngageMailExecutor;
import com.cognizant.insurance.request.vo.StatusData;

/**
 * @Class GeneraliMailExecutor
 * @Desc To execute email notification in an async manner
 * 
 */

public class GeneraliMailExecutor extends LifeEngageMailExecutor {

    /** The id. */
    private Long id;

    /** The email json. */
    private String emailJson = null;

    /** The attempt Number. */
    private String attemptNo;

    /** The currentDate. */
    private Date currentDate;

    /** The Constant EMAIL_TEMPLATE. */
    private static final String EMAIL_TEMPLATE = "emailTemplate";

    /** The Constant AGENT ID. */
    private static final String AGENT_ID = "agentId";

    @Autowired
    private LifeEngageEmailComponent lifeEngageEmailComponent;

    @Autowired
    private EmailRepository emailRepository;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailJson() {
        return emailJson;
    }

    public void setEmailJson(String emailJson) {
        this.emailJson = emailJson;
    }

    public String getAttemptNo() {
        return attemptNo;
    }

    public void setAttemptNo(String attemptNo) {
        this.attemptNo = attemptNo;
    }

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {

    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }

    /**
     * executes the batch job
     * 
     * @param arg0
     *            the Step Contribution
     * @param chunkContext
     *            the chunk Context
     */
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        String statusFlag;
        try {
            JobExecution jobExecution = chunkContext.getStepContext().getStepExecution().getJobExecution();
            Long jobId = jobExecution.getJobId();
            String templateName = "";
            String pdfName = "";
            final JSONObject emailJSONObj = new JSONObject(emailJson);
            final JSONObject templateObj = emailJSONObj.getJSONObject(EMAIL_TEMPLATE);
            final String type = emailJSONObj.getString(Constants.TYPE);
            String agentId = new JSONObject(emailJson).getString(AGENT_ID);

            StatusData statusData =
                    lifeEngageEmailComponent.sendEmail(emailJson, templateName, templateObj.toString(), pdfName);
            statusFlag = statusData.getStatus();
            final LifeEngageEmail lifeEngageEmail = new LifeEngageEmail();
            lifeEngageEmail.setSendTime(LifeEngageComponentHelper.getCurrentdate());
            lifeEngageEmail.setId(id);
            lifeEngageEmail.setEmailValues(emailJson);
            lifeEngageEmail.setStatus(statusFlag);
            lifeEngageEmail.setAttemptNumber(attemptNo);
            lifeEngageEmail.setStatusMessage(statusData.getStatusMessage());
            lifeEngageEmail.setBatchJobId(jobId);
            lifeEngageEmail.setType(type);
            lifeEngageEmail.setAgentId(agentId);
            if (Constants.SUCCESS.equals(statusFlag)) {
                statusFlag = Constants.SUCCESS;
                emailRepository.updatelifeEngageEmail(lifeEngageEmail);
            } else {
                statusFlag = Constants.FAILURE;
                emailRepository.updatelifeEngageEmail(lifeEngageEmail);
            }
        } catch (Exception e) {
        }
        return null;
    }

}
