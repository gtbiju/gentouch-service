/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291446
 * @version       : 0.1, Nov 4, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Class ProductTemplates.
 * 
 * 
 */

@Entity
@Table(name = "CORE_PROD_TEMPLATE_MAPPING")
public class ProductTemplateMapping {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7136100978621736164L;

    /** The id. */
    @Id
    @Column(name = "ID", columnDefinition = "int DEFAULT 0")
    private Integer id;

    /** The product specification. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "PROD_ID")
    private ProductSpecification productSpecification;

    /** The product templates. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "TEMPLATE_ID")
    private ProductTemplates productTemplates;

    /** The template type. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "TEMPLATE_TYPE")
    private TemplateType templateType;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the product specification.
     * 
     * @return the productSpecification
     */
    public ProductSpecification getProductSpecification() {
        return productSpecification;
    }

    /**
     * Sets the product specification.
     * 
     * @param productSpecification
     *            the productSpecification to set
     */
    public void setProductSpecification(ProductSpecification productSpecification) {
        this.productSpecification = productSpecification;
    }

    /**
     * Gets the product templates.
     * 
     * @return the productTemplates
     */
    public ProductTemplates getProductTemplates() {
        return productTemplates;
    }

    /**
     * Sets the product templates.
     * 
     * @param productTemplates
     *            the productTemplates to set
     */
    public void setProductTemplates(ProductTemplates productTemplates) {
        this.productTemplates = productTemplates;
    }

    /**
     * Gets the template type.
     * 
     * @return the templateType
     */
    public TemplateType getTemplateType() {
        return templateType;
    }

    /**
     * Sets the template type.
     * 
     * @param templateType
     *            the templateType to set
     */
    public void setTemplateType(TemplateType templateType) {
        this.templateType = templateType;
    }

    /**
     * Gets the template context.
     * 
     * @return the templateContext
     */
    public String getTemplateContext() {
        return templateContext;
    }

    /**
     * Sets the template context.
     * 
     * @param templateContext
     *            the templateContext to set
     */
    public void setTemplateContext(String templateContext) {
        this.templateContext = templateContext;
    }

    /** The template_context. */
    @Column(name = "TEMPLATE_CONTEXT")
    private String templateContext;

}
