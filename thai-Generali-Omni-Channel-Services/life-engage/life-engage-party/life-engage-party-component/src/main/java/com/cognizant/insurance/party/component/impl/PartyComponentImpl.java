/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 31, 2013
 */
package com.cognizant.insurance.party.component.impl;

import java.util.List;
import java.util.Set;

import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Appointee;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.domain.party.Person;
import com.cognizant.insurance.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInAgreement;
import com.cognizant.insurance.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
import com.cognizant.insurance.party.component.PartyComponent;
import com.cognizant.insurance.party.dao.PartyDao;
import com.cognizant.insurance.party.dao.impl.PartyDaoImpl;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

/**
 * The Class class PartyComponentImpl.
 * 
 * @author 300797
 */
public class PartyComponentImpl implements PartyComponent {

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.insuranec.party.component.PartyComponent#savePerson(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Long> savePerson(Request<Person> request) {
        final PartyDao partyDao = new PartyDaoImpl();
        partyDao.save(request);

        final Response<Long> partyId = new ResponseImpl<Long>();
        partyId.setType(Long.valueOf(request.getType().getId()));

        return partyId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insuranec.party.component.PartyComponent#savePartyRoleInAgreement(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public void savePartyRoleInAgreement(Request<PartyRoleInAgreement> request) {
        final PartyDao partyDao = new PartyDaoImpl();
        partyDao.save(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveInsured(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Insured> retrieveInsured(Request<Agreement> request) {
        final PartyDao partyDao = new PartyDaoImpl();
        return partyDao.retrieveInsured(request);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrievePayer(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<PremiumPayer> retrievePayer(Request<Agreement> agreementRequest) {
        final PartyDao partyDao = new PartyDaoImpl();
        return partyDao.retrievePayer(agreementRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveProposer(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<AgreementHolder> retrieveProposer(Request<Agreement> agreementRequest) {
        final PartyDao partyDao = new PartyDaoImpl();
        return partyDao.retrieveProposer(agreementRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveBeneficiaries(com.cognizant.insurance.request.
     * Request)
     */
    @Override
    public Response<Set<Beneficiary>> retrieveBeneficiaries(Request<Agreement> agreementRequest) {
        final PartyDao partyDao = new PartyDaoImpl();
        return partyDao.retrieveBeneficiaries(agreementRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveAppointee(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Appointee> retrieveAppointee(Request<Agreement> agreementRequest) {
        final PartyDao partyDao = new PartyDaoImpl();
        return partyDao.retrieveAppointee(agreementRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveInsured(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Insured> retrieveInsuredPreviousPolicyAgreement(Request<Agreement> agreementRequest) {
        final PartyDao partyDao = new PartyDaoImpl();
        return partyDao.retrieveInsured(agreementRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#savePartyRoleInRelationship(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public void savePartyRoleInRelationship(Request<PartyRoleInRelationship> request) {
        final PartyDao partyDao = new PartyDaoImpl();
        partyDao.save(request);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveCustomer(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Customer> retrieveCustomer(Request<Customer> identifierRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        final PartyDao partyDao = new PartyDaoImpl();
        return partyDao.retrieveCustomer(identifierRequest, statusRequest);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveCustomers(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public  Response<List<Customer>> retrieveCustomers(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest, Request<Integer> limitRequest) {
        final PartyDao partyDao = new PartyDaoImpl();
        final List<Customer> customers = partyDao.retrieveCustomers(customerRequest, statusRequest, limitRequest);

        final Response<List<Customer>> response = new ResponseImpl<List<Customer>>();
        if (!customers.isEmpty()) {
            response.setType(customers);
        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveCustomerForIdentifier(com.cognizant.insurance.
     * request.Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Customer> retrieveCustomerForIdentifier(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        final PartyDao partDao = new PartyDaoImpl();
        final List<Customer> customers = partDao.retrieveCustomerForIdentifier(customerRequest, statusRequest);
        final Response<Customer> response = new ResponseImpl<Customer>();
        if (!customers.isEmpty()) {
            response.setType(customers.get(0));
        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#updateCustomer(com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<Customer> updateCustomer(final Request<Customer> customerRequest) {
        final PartyDao partDao = new PartyDaoImpl();
        partDao.merge(customerRequest);
        final Response<Customer> customerResponse = new ResponseImpl<Customer>();
        customerResponse.setType(customerRequest.getType());
        return customerResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveDuplicateLeadList(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.vo.Transactions, com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<Customer>> retrieveDuplicateLeadList(final Request<PartyRoleInRelationship> customerRequest,
            final Transactions transactions, final Request<List<CustomerStatusCodeList>> statusRequest,
            final Request<List<TelephoneTypeCodeList>> telephoneTypeRequest) {
        // TODO Auto-generated method stub
        final PartyDao partyDao = new PartyDaoImpl();
        final List<Customer> customers =
                partyDao.retrieveLMSDuplicateCustomers(customerRequest, transactions, statusRequest,
                        telephoneTypeRequest);
        final Response<List<Customer>> response = new ResponseImpl<List<Customer>>();
        if (!customers.isEmpty()) {
            response.setType(customers);
        }
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#getCustomerCount(com.cognizant.insurance.request.Request,
     * com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<SearchCountResult> getCustomerCount(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        final PartyDao partyDao = new PartyDaoImpl();
        final Response<SearchCountResult> searchCountResponse =
                partyDao.getCustomerCount(searchCriteriatRequest, statusRequest);
        return searchCountResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.party.component.PartyComponent#retrieveFilterCustomers(com.cognizant.insurance.request
     * .Request, com.cognizant.insurance.request.Request)
     */
    @Override
    public Response<List<Customer>> retrieveFilterCustomers(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest) {
        final PartyDao partyDao = new PartyDaoImpl();
        final List<Customer> customers = partyDao.retrieveFilterCustomers(searchCriteriatRequest, statusRequest);

        final Response<List<Customer>> response = new ResponseImpl<List<Customer>>();
        if (!customers.isEmpty()) {
            response.setType(customers);
        }
        return response;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insuranec.party.component.PartyComponent#savePartyRoleInAgreement(com.cognizant.insurance.request
     * .Request)
     */
    @Override
    public void mergePartyRoleInAgreement(Request<PartyRoleInAgreement> request) {
        final PartyDao partyDao = new PartyDaoImpl();
        partyDao.merge(request);
    }


}
