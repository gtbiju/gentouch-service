/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Payment Form.
 * 
 * @author 301350
 * 
 * 
 */
public enum FinancialMediumTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Unknown,
    /**
     * 
     * 
     * 
     * 
     */
    Cash,
    /**
     * Check is drawn from a corporately owned account.
     * 
     * 
     * 
     * 
     */
    CorporateCheck,
    /**
     * Money Settlement via a third Party Clearing firm i.e.. NSCC
     * 
     * 
     * 
     * 
     * 
     */
    Clearinghouse,
    /**
     * Funds are electronically Wired.
     * 
     * 
     * 
     * 
     * 
     */
    Wire,
    /**
     * The funds will follow, payment method will define whether the funds have
     * been requested by the distributor or must be requested by the carrier.
     * 
     * 
     * 
     */
    Fundstofollow,
    /**
     * Money is moving within the existing policy; I.e. fund to fund transfers.
     * 
     * 
     * 
     * 
     * 
     */
    PaymentfromwithinthisAgreement,
    /**
     * 
     * 
     * 
     * 
     */
    CreditCard,
    /**
     * 
     * 
     * 
     * 
     */
    DebitCard,
    /**
     * Is a check drawn by the US Treasury. It is identified as such on the top
     * of the instrument
     * 
     * 
     * 
     */
    TreasuryCheck,
    /**
     * The terms "cashier's check," "treasurer's check," and "official check"
     * are often used interchangeably. What you need to look at is whether the
     * check is drawn by a bank on itself (Cashier's Check). If it is, and if it
     * is not captioned "expense check" or "dividend check" or "payroll check"
     * or in any other way that makes it a special-purpose payment, consider it
     * as fitting the definition of cashier's check. The term "official check"
     * is nebulous because it is not a term defined in the UCC and institutions
     * use it to describe a variety of instruments that, in many cases, would be
     * classified as teller's checks or cashier's checks.
     * 
     * 
     * 
     * 
     * 
     */
    OfficialCheck,
    /**
     * 
     * 
     * 
     * 
     */
    Other,
    /**
     * A check drawn by a depository institution on an account maintained at a
     * second depository institution and signed by a teller at the originating
     * institution. Teller's checks are often used in payment of withdrawal
     * orders.
     * 
     * 
     * 
     */
    TellerCheck,
    /**
     * 
     * 
     * 
     * 
     */
    RetainedAssetAccount,
    /**
     * 
     * 
     * 
     * 
     */
    MoneyOrder,
    /**
     * 
     * 
     * 
     * 
     */
    CashiersCheck,
    /**
     * 
     * 
     * 
     * 
     */
    CertifiedCheck,
    /**
     * 
     * 
     * 
     * 
     */
    PersonalCheck,
    /**
     * Carrier initiated transfer of funds using methodologies such as ACH or
     * PAC (pre authorized checking).
     * 
     * 
     * 
     * 
     * 
     */
    ElectronicFundsTransfer,
    /**
     * An authorized debit to a bank account, initiated by the payee
     * 
     * Notes: Required to differentiate between various forms of premium
     * collection. The South African banking industry have different fee
     * structures for different transaction types which in some cases can lead
     * to a discount on policy premiums. "Electronic Funds Transfer" (t c = 7)
     * is too general for South African purposes.
     * 
     * 
     * 
     * 
     * 
     * 
     */
    DebitOrder,
    /**
     * An Internal transfer of funds, whereby the transfer may be by means of a
     * Payroll Deduction or Commission Statement Deduction for an employee or
     * Producer of the company
     * 
     * Notes: An EFT that has been prearranged and will continue until a stop
     * order is requested.
     * 
     * 
     * 
     * 
     * 
     * 
     */
    StopOrder,
    
    DebitAccount,
    TransferOrVirtualAccount,
    Online,
    Offline,
   //Added for generali vietnam
    POS,
    Cards,
    ManualBankDeposit
    
    
    
    
}
