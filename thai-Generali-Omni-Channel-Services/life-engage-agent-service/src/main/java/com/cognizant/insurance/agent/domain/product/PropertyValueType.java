/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Class PropertyValueType.
 *
 * @author 304007
 */

@Entity
@DiscriminatorValue("Property_Value")
public class PropertyValueType extends Type {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8816366800058566649L;

    /**
     * The Constructor.
     */
    public PropertyValueType() {
        super();
    }

    /**
     * The Constructor.
     *
     * @param id the id
     */
    public PropertyValueType(final String id) {
        super(id);
    }
}
