package com.cognizant.insurance.omnichannel.component;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agreement.component.AgreementComponent;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.RiderSequence;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public interface GeneraliAgreementComponent extends AgreementComponent {

    /**
     * Gets the agreement count.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the agreement count
     */
    public Response<SearchCountResult> getAgreementCountForLMS(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);
    

    Response<List<Agreement>> getAgreementByFilterRelatedTransactions(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);


    Response<Set<AgreementDocument>> getBase64DocumentDetils(final Request<Agreement> agreementRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);


	Agreement updateInsuranceAgreementWithRequirementDocUpdates(
			Request<Agreement> agreementRequest,
			Request<List<AgreementStatusCodeList>> statusRequest);

	Response<List<Agreement>> getRelatedAgreementsForLMS(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);


    Response<List<Object>> getCountForSalesActivity(Request<SearchCriteria> searchCriteriatRequest);
    
    Response<GeneraliAgent>retrieveGeneraliAgent(Request<GeneraliAgent> agentRequest,String agentID);


    public Map<String, Integer> retrieveMonthwiseCount(Request<SearchCriteria> searchCriteriatRequest);


    public Map<String, Integer> retrieveWeeklyCount(Request<SearchCriteria> searchCriteriatRequest);


    public Response<List<Agreement>> getMonthlyData(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest);
    
    public Response<SearchCountResult> getAgreementCountForIllustration(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest, final Request<List<AgreementStatusCodeList>> statusRequestForDraft);


    public Response<InsuranceAgreement> getEAppAgreementByAppNo(Request<InsuranceAgreement> aggrementRequest, Request<List<AgreementStatusCodeList>> statusRequest);
    
    public Response<SearchCountResult> getAgreementCountforEapp(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest);


    public List<Agreement> getAggrementsWithExpiredGAOLocks(Request<Agreement> agreementRequest);


    public Response<List<Agreement>> getAgreementsList(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest, Request<Integer> limitRequest);


    Map<String, String> getEAppIllustrationCountForReport(Request<SearchCriteria> searchCriteriatRequest);


	public List<RiderSequence> retrieveRiderSequence();
	
	
}
