package com.cognizant.insurance.omnichannel.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agreement.dao.impl.AgreementDaoImpl;
import com.cognizant.insurance.dao.exception.DaoException;
import com.cognizant.insurance.domain.agreement.Agreement;
import com.cognizant.insurance.domain.agreement.RiderSequence;
import com.cognizant.insurance.domain.agreement.agreementcodelists.AgreementStatusCodeList;
import com.cognizant.insurance.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.documentandcommunication.Document;
import com.cognizant.insurance.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.omnichannel.GeneraliConstants;
import com.cognizant.insurance.omnichannel.dao.GeneraliAgreementDao;
import com.cognizant.insurance.omnichannel.vo.PaymentBO;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.response.impl.ResponseImpl;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;

public class GeneraliAgreementDaoImpl extends AgreementDaoImpl implements
		GeneraliAgreementDao {

	private static final String DELETEDSTATUS = "Deleted";
	private static final String SAVEDSTATUS = "Saved";
	private static final String RESUBMITTEDSTATUS = "Resubmitted";
	private static final String OPEN_MEMO="Open Memo";
	
	@PersistenceContext(unitName = "LE_Platform")
    private EntityManager entityManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cognizant.insurance.omnichannel.dao.impl.GeneraliAgreementDaoImpl
	 * #getAgreementCount(com.cognizant.insurance .request.Request,
	 * com.cognizant.insurance.request.Request)
	 */

	@Override
	public Response<SearchCountResult> getAgreementCountForLMS(
			Request<SearchCriteria> searchCriteriatRequest,
			Request<List<AgreementStatusCodeList>> statusRequest) {

		final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
		List<Object> searchCriteriaList = null;
		String type = null;
		String query;

		if (searchCriteriatRequest != null
				&& searchCriteriatRequest.getType() != null) {
			SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest
					.getType();
			type = searchCriteria.getType();
			if (searchCriteria.getValue() != null
					&& !("".equals(searchCriteria.getValue()))) {
				query = "select count(insAgr.identifier), insAgr.statusCode from InsuranceAgreement insAgr where insAgr.agentId =?1 "
						+ "and insAgr.statusCode in ?2 and insAgr.contextId =?3 and "
						+ "insAgr.transactionKeys.key1 like ?4 "
						+ "group by insAgr.statusCode";
				searchCriteriaList = findUsingQuery(searchCriteriatRequest,
						query, searchCriteria.getAgentId(),
						statusRequest.getType(),
						searchCriteriatRequest.getContextId(), "%"
								+ searchCriteria.getValue() + "%");

			}
		}

		final SearchCountResult searchCountResult = new SearchCountResult();

		Long totalModCount = 0l;
		if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
			for (Object searchCriteria : searchCriteriaList) {
				Object[] values = (Object[]) searchCriteria;
				Long count = (Long) values[0];
				totalModCount += count;
			}

		}
		searchCountResult.setCount(String.valueOf(totalModCount));
		searchCountResult.setMode(type);

		searchCountResponse.setType(searchCountResult);
		return searchCountResponse;
	}

	@Override
	public List<Agreement> getAgreementByFilterRelatedTransactions(
			Request<SearchCriteria> searchCriteriatRequest,
			Request<List<AgreementStatusCodeList>> statusRequest) {
		String query;
		List<Object> searchCriteriaList = null;

		if (searchCriteriatRequest != null
				&& searchCriteriatRequest.getType() != null) {
			SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest
					.getType();
			if (searchCriteria.getValue() != null
					&& !("".equals(searchCriteria.getValue()))) {
				query = "select insAgr from InsuranceAgreement insAgr where insAgr.agentId =?1 "
						+ "and insAgr.statusCode in ?2 and insAgr.contextId =?3 "
						+ "and insAgr.transactionKeys.key3 like ?4";

				searchCriteriaList = findUsingQuery(searchCriteriatRequest,
						query, searchCriteria.getAgentId(),
						statusRequest.getType(),
						searchCriteriatRequest.getContextId(), "%"
								+ searchCriteria.getValue() + "%");

			}
		}

		final List<Agreement> agreements = new ArrayList<Agreement>();
		if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
			for (Object agreement : searchCriteriaList) {
				agreements.add((InsuranceAgreement) agreement);
			}
		}
		return agreements;
	}

	@Override
	public Response<Set<AgreementDocument>> getBase64DocumentDetils(
			Request<Agreement> agreementRequest,
			Request<List<AgreementStatusCodeList>> statusRequest) {

		final Response<Set<AgreementDocument>> response = new ResponseImpl<Set<AgreementDocument>>();
		String query = "select agreement "
				+ "from AgreementDocument agreementDocument,InsuranceAgreement agreement "
				+ "join fetch agreement.relatedDocument relatedDocument "
				+ "where agreementDocument.typeCode = relatedDocument.typeCode and "
				+ "agreement.identifier =?1 and agreement.statusCode in ?2 and "
				+ "agreementDocument.typeCode=?3";
		final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest
				.getType();

		for (AgreementDocument agreementDocument : insuranceAgreement
				.getRelatedDocument()) {
			List<Agreement> agreementList = findByQuery(agreementRequest,
					query, insuranceAgreement.getIdentifier(),
					statusRequest.getType(), agreementDocument.getTypeCode());
			if (CollectionUtils.isNotEmpty(agreementList)
					&& agreementList.get(0) != null
					&& CollectionUtils.isNotEmpty(agreementList.get(0)
							.getRelatedDocument())) {

				response.setType(agreementList.get(0).getRelatedDocument());

			}
		}

		return response;
	}

	@Override
	public Agreement getInsuranceAgreementForReqDocUpdates(
			Request<Agreement> agreementRequest,
			Request<List<AgreementStatusCodeList>> statusRequest) {
		List<Agreement> agreementWithRequirement = null;
		// final Response<AgreementDocument> response = new
		// ResponseImpl<AgreementDocument>();
		String query = "select agreement from InsuranceAgreement agreement "
				+ "left join agreement.requirements relatedRequirement "
				+ "left join  relatedRequirement.requirementDocuments requirementDocument "
				+ "where requirementDocument.name =  ?4 and "
				+ "agreement.identifier =?1 and agreement.statusCode in ?2 and "
				+ "relatedRequirement.requirementName=?3";
		final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest
				.getType();

		for (Requirement agreementRequirementObj : insuranceAgreement
				.getRequirements()) {
			for (AgreementDocument document : agreementRequirementObj
					.getRequirementDocuments()) {
				agreementWithRequirement = findByQuery(agreementRequest, query,
						insuranceAgreement.getIdentifier(),
						statusRequest.getType(),
						agreementRequirementObj.getRequirementName(),
						document.getName());
				if (CollectionUtils.isNotEmpty(agreementWithRequirement)
						&& agreementWithRequirement.get(0) != null
						&& CollectionUtils.isNotEmpty(agreementWithRequirement
								.get(0).getRequirements())) {
					for (Requirement agreementRequirementObjResult : agreementWithRequirement
							.get(0).getRequirements())
						for (AgreementDocument documentResult : agreementRequirementObjResult
								.getRequirementDocuments()) {
							if (document.getName().equals(
									documentResult.getName())) {
								// add page of this doc
								for (Document page : document.getPages()) {
									if (page.getDocumentStatus().equals(
											SAVEDSTATUS)
											|| page.getDocumentStatus().equals(
													RESUBMITTEDSTATUS)) {
										// Comment Save and Signature save on
										// second time confirm click
										if (documentResult.getPages().contains(
												page)) {
											for (Document pageToCopy : documentResult
													.getPages()) {
												if (pageToCopy
														.getFileNames()
														.equals(page
																.getFileNames())) {
													pageToCopy
															.setBase64string(page
																	.getBase64string());
													pageToCopy
															.setDescription(page
																	.getDescription());

												}
											}

										} else { // New page
											documentResult.getPages().add(page);
											break;
										}
									} else if (page.getDocumentStatus().equals(
											DELETEDSTATUS)) {
										for (Document targetPage : documentResult
												.getPages()) {
											if (targetPage
													.getFileNames()
													.equals(page.getFileNames())) {
												targetPage
														.setDocumentStatus(DELETEDSTATUS);
												break;
											}
										}
									}
								}
							}
						}
				} else {
					throw new DaoException("No agreement Document matching : "
							+ insuranceAgreement.getIdentifier()+", Req Name :"+ agreementRequirementObj.getRequirementName()+
							", Doc Name :"+document.getName());
				}
			}
		}

		return agreementWithRequirement.get(0);
	}

	@Override
	public List<Agreement> getRelatedAgreementsForLMS(
			Request<SearchCriteria> searchCriteriatRequest,
			Request<List<AgreementStatusCodeList>> statusRequest) {
		String query;
		List<Object> searchCriteriaList = null;

		if (searchCriteriatRequest != null
				&& searchCriteriatRequest.getType() != null) {
			SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest
					.getType();
			if (searchCriteria.getValue() != null
					&& !("".equals(searchCriteria.getValue()))) {
				query = "select insAgr from InsuranceAgreement insAgr where insAgr.agentId =?1 "
						+ "and insAgr.statusCode NOT in ?2 and insAgr.contextId =?3 "
						+ "and insAgr.transactionKeys.key1 = ?4";

				searchCriteriaList = findUsingQuery(searchCriteriatRequest,
						query, searchCriteria.getAgentId(),
						statusRequest.getType(),
						searchCriteriatRequest.getContextId(),
						searchCriteria.getValue());

			}
		}
		final List<Agreement> agreements = new ArrayList<Agreement>();
		if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
			for (Object agreement : searchCriteriaList) {
				agreements.add((InsuranceAgreement) agreement);
			}
		}
		return agreements;
	}

	@Override
	public Boolean isSpajExisting(Request<InsuranceAgreement> spajRequest,
			String spajNo,
			Request<List<AgreementStatusCodeList>> statusRequest,
			String identifier) {
		Boolean result = false;
		// final Response<SearchCountResult> searchCountResponse = new
		// ResponseImpl<SearchCountResult>();
		List<Object> searchCriteriaList = null;
		String query;
		if(!identifier.equals("")){
			query = "select  insAgr from InsuranceAgreement insAgr where "
				+ "insAgr.statusCode in ?1 and insAgr.contextId =?2 and "
				+ "insAgr.transactionKeys.key21 = ?3 and insAgr.identifier not in ?4";
			searchCriteriaList = findUsingQuery(spajRequest, query,
					statusRequest.getType(), spajRequest.getType().getContextId(),
					spajNo, identifier);
		}else{
			query = "select  insAgr from InsuranceAgreement insAgr where "
					+ "insAgr.statusCode in ?1 and insAgr.contextId =?2 and "
					+ "insAgr.transactionKeys.key21 = ?3";
			searchCriteriaList = findUsingQuery(spajRequest, query,
					statusRequest.getType(), spajRequest.getType().getContextId(),
					spajNo);
		}
		
		
		
		if (searchCriteriaList != null && searchCriteriaList.size() > 0) {
			result = true;
		}
		return result;

	}
	
	@Override
	public Boolean isRecieptExisting(Request<PaymentBO> paymentReciptCheckRequest) {
		Boolean result = false;
		PaymentBO paymentObj =  new PaymentBO();
		paymentObj = paymentReciptCheckRequest.getType();
		List<Object> searchCriteriaList = null;
		int countInt =0;
		String query;
		query = "select count(fsh.id) from FinancialScheduler fsh " +
				"where fsh.description in ?1 and " +
				"fsh.transactionRefNumber = ?2";
		searchCriteriaList = findUsingQuery(paymentReciptCheckRequest,query,paymentObj.getPaymentType(),paymentObj.getRecieptNo());
		if (searchCriteriaList != null && searchCriteriaList.size() > 0) {			
			for(Object recieptNoCheck : searchCriteriaList) {				
				Long count = (Long) recieptNoCheck;				
				if(count > 0) {
					result = true;
				}				           	            
			}
		}		
		return result;

	}

    @Override
    public List<Object> getCountForSalesActivity(Request<SearchCriteria> searchCriteriatRequest) {

        String query = null;

        List<Object> searchCriteriaList = null;

        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            ArrayList<String> agentList = searchCriteria.getSelectedIds();

            String statusCode = null;
            if (searchCriteriatRequest.getContextId().equals(ContextTypeCodeList.EAPP)) {
                statusCode = "15";
            } else {
                statusCode = "10,11";
            }

            StringBuilder agentCode = new StringBuilder();

            if (null != agentList) {
                for (String agentId : agentList) {
                    agentCode.append("'").append(agentId).append("'").append(",");

                }
                agentCode.deleteCharAt(agentCode.length() - 1);

                query =
                        "select count(agr.id),CONVERT(VARCHAR(10),agr.creationDateTime,126) as creationDate "
                                + "from Agreement agr inner join InsuranceAgreement ig on (agr.id=ig.id) "
                                + "where ig.agentId in (" + agentCode.toString() + ") and agr.statusCode in ("
                                + statusCode
                                + ") and agr.contextId=?1  group by CONVERT(VARCHAR(10),agr.creationDateTime,126)";

                searchCriteriaList =
                        findByNativeQuery(searchCriteriatRequest, query, searchCriteriatRequest.getContextId()
                            .ordinal());

            }

        }

        return searchCriteriaList;

    }
    
    	public GeneraliAgent getGeneraliAgent(Request<GeneraliAgent> agentRequest,String agentID){
    		GeneraliAgent retrievedAgent = null;
    		String query = "SELECT agent FROM GeneraliAgent agent WHERE agent.agentCode = ?1";
    		List<GeneraliAgent> agentsList = findByQuery(agentRequest, query,agentID);
    		if (agentsList.size() > 0) {
    			retrievedAgent = agentsList.get(0);
    		}
    		return retrievedAgent;
    	}

    @Override
    public Map<String, Integer> retrieveMonthwiseCount(Request<SearchCriteria> searchCriteriatRequest) {
        List<Object[]> countList = null;
        String query;
        final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
        List<String> agentIDList = searchCriteria.getSelectedIds();
        String statusCode = null;
        if (searchCriteriatRequest.getContextId().equals(ContextTypeCodeList.EAPP)) {
            statusCode = "15";
        } else {
            statusCode = "10,11";
        }

        StringBuilder agentCode = new StringBuilder();
        for (String agentId : agentIDList) {
            agentCode.append("'").append(agentId).append("'").append(",");

        }
        agentCode.deleteCharAt(agentCode.length() - 1);
        query =
                "select DATEPART(MONTH, CONVERT(VARCHAR(10),agr.creationDateTime,126)) as redDate,count(agr.id) as reqCount  "
                        + "from Agreement agr inner join InsuranceAgreement ig on (agr.id=ig.id) where ig.agentId in (" + agentCode.toString() + ")  and agr.statusCode in ("
                        + statusCode
                        + ") and agr.contextId=?1 and agr.creationDateTime between ?2 and ?3 group by DATEPART(MONTH, CONVERT(VARCHAR(10),agr.creationDateTime,126)) ";

        countList =
                findUsingNativeQuery(searchCriteriatRequest, query, searchCriteriatRequest.getContextId().ordinal(),
                        searchCriteria.getStartDate(), searchCriteria.getCurrentDate());

        Map<String, Integer> monthwiseCount = new HashMap<String, Integer>();
        if (CollectionUtils.isNotEmpty(countList)) {
            for (Object[] customerObj : countList) {
                monthwiseCount.put(customerObj[0].toString(), (Integer) customerObj[1]);
            }
        }
        return monthwiseCount;
    }
    
    @Override
    public Map<String, Integer> retrieveWeeklyCount(Request<SearchCriteria> searchCriteriatRequest) {
        List<Object[]> countList = null;
        String query;
        final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
        List<String> agentIDList = searchCriteria.getSelectedIds();
        String statusCode = null;
        if (searchCriteriatRequest.getContextId().equals(ContextTypeCodeList.EAPP)) {
            statusCode = "15";
        } else {
            statusCode = "10,11";
        }

        StringBuilder agentCode = new StringBuilder();
        for (String agentId : agentIDList) {
            agentCode.append("'").append(agentId).append("'").append(",");

        }
        agentCode.deleteCharAt(agentCode.length() - 1);
        query =
                "select CONVERT(VARCHAR(10),agr.creationDateTime,126) as redDate,count(agr.id) as reqCount  "
                        + "from Agreement agr inner join InsuranceAgreement ig on (agr.id=ig.id) where ig.agentId in ("
                        + agentCode.toString()
                        + ")  and agr.statusCode in ("
                        + statusCode
                        + ") and agr.contextId=?1 and agr.creationDateTime between ?2 and ?3 group by CONVERT(VARCHAR(10),agr.creationDateTime,126) ";

        countList =
                findUsingNativeQuery(searchCriteriatRequest, query, searchCriteriatRequest.getContextId().ordinal(),
                        searchCriteria.getStartDate(), searchCriteria.getCurrentDate());

        Map<String, Integer> weeklyCount = new HashMap<String, Integer>();
        if (CollectionUtils.isNotEmpty(countList)) {
            for (Object[] customerObj : countList) {
                weeklyCount.put((String) customerObj[0], (Integer) customerObj[1]);
            }
        }
        return weeklyCount;

    }
       
    protected List<Object[]> findUsingNativeQuery(Request request, String queryString, Object... params) {
        try {
            final JPARequestImpl jpaRequestImpl = (JPARequestImpl) request;
            final Query query = jpaRequestImpl.getEntityManager().createNativeQuery(queryString);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    query.setParameter(i + 1, params[i]);
                }
            }

            return (List<Object[]>) query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        }
    }

    @Override
    public List<Agreement> getMonthlyData(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        String query;
        List<Object> searchCriteriaList = null;
        query =
                "select insAgr from InsuranceAgreement insAgr where insAgr.agentId in ?1 "
                        + "and insAgr.statusCode in ?2 and insAgr.creationDateTime between ?3 and ?4 and insAgr.contextId =?5 "
                        + "order by insAgr.creationDateTime asc";
        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();

            searchCriteriaList =
                    findUsingQuery(searchCriteriatRequest, query, searchCriteria.getSelectedIds(),
                            statusRequest.getType(), searchCriteria.getStartDate(), searchCriteria.getCurrentDate(),
                            searchCriteriatRequest.getContextId());

        }
        final List<Agreement> agreements = new ArrayList<Agreement>();
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object agreement : searchCriteriaList) {
                agreements.add((InsuranceAgreement) agreement);
            }
        }
        return agreements;

    }
    
    @Override
    public Response<SearchCountResult> getAgreementCountForIllustration(Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest, final Request<List<AgreementStatusCodeList>> statusRequestForDraft) {
        final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
        List<Object> searchCriteriaList = null;
        List<Object> searchCriteriaDraftList = null;
        String type = null;
        String query, queryForDraft;

        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            type = searchCriteria.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                query =
                        "select count(insAgr.identifier), insAgr.statusCode from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 and "
                                + "(insAgr.transactionKeys.key6 like ?4 or insAgr.transactionKeys.key8 like ?4 or insAgr.transactionKeys.key26 like ?4) "
                                + "group by insAgr.statusCode";
                
                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                statusRequest.getType(), searchCriteriatRequest.getContextId(),
                                "%" + searchCriteria.getValue() + "%");

            } else {
                query =
                        "select count(insAgr.identifier), insAgr.statusCode from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                + "and insAgr.contextId =?2 and (insAgr.statusCode in ?3 and MONTH(insAgr.modifiedDateTime) = MONTH(GETDATE()) "
                                + "and YEAR(insAgr.modifiedDateTime) = YEAR(GETDATE())) "
                                + "group by insAgr.statusCode";
                
                queryForDraft =
                        "select count(insAgr.identifier), insAgr.statusCode from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                + "and insAgr.contextId =?2 and (insAgr.statusCode in ?3 and MONTH(insAgr.creationDateTime) = MONTH(GETDATE()) "
                                + "and YEAR(insAgr.creationDateTime) = YEAR(GETDATE())) "
                                + "group by insAgr.statusCode";

                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                searchCriteriatRequest.getContextId(), statusRequest.getType());
                
                searchCriteriaDraftList = findUsingQuery(searchCriteriatRequest, queryForDraft, searchCriteria.getAgentId(),
                        searchCriteriatRequest.getContextId(), statusRequestForDraft.getType());
                        
            }
        }
       
        final List<SearchCountResult> subModeResults = new ArrayList<SearchCountResult>();
        final SearchCountResult searchCountResult = new SearchCountResult();
        SearchCountResult submode;
        Integer totalModCount = 0;
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object searchCriteria : searchCriteriaList) {
                Object[] values = (Object[]) searchCriteria;
                Long count = (Long) values[0];
                AgreementStatusCodeList mode = (AgreementStatusCodeList) values[1];
                submode = new SearchCountResult();
                submode.setCount(count.toString());
                submode.setMode(mode.toString());

                subModeResults.add(submode);
                totalModCount += Integer.valueOf(submode.getCount());
            }
        }
        if (CollectionUtils.isNotEmpty(searchCriteriaDraftList)) {
            for (Object searchCriteriaForDraft : searchCriteriaDraftList) {
                Object[] values = (Object[]) searchCriteriaForDraft;
                Long count = (Long) values[0];
                AgreementStatusCodeList mode = (AgreementStatusCodeList) values[1];
                submode = new SearchCountResult();
                submode.setCount(count.toString());
                submode.setMode(mode.toString());

                subModeResults.add(submode);
                totalModCount += Integer.valueOf(submode.getCount());
            }
        }
        
        searchCountResult.setSubModeResults(subModeResults);
        searchCountResult.setCount(String.valueOf(totalModCount));
        searchCountResult.setMode(type);

        searchCountResponse.setType(searchCountResult);
        return searchCountResponse;
    }

    @Override
    public InsuranceAgreement getEAppAgreementByAppNo(Request<InsuranceAgreement> aggrementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        List<Object> searchCriteriaList = null;
        String query;
        query = "select  insAgr from InsuranceAgreement insAgr where "
                + "insAgr.transactionKeys.key21 = ?1 and insAgr.statusCode in ?2 order by id desc";
        searchCriteriaList = findUsingQuery(aggrementRequest, query,
                aggrementRequest.getType().getTransactionKeys().getKey21(),statusRequest.getType());
        if(CollectionUtils.isNotEmpty(searchCriteriaList)){
            return  (InsuranceAgreement) searchCriteriaList.get(0);
        }else{
            return null;
        }
    }
    
    @Override
    public Response<SearchCountResult> getAgreementCountForEApp(Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<AgreementStatusCodeList>> statusRequest) {
        final Response<SearchCountResult> searchCountResponse = new ResponseImpl<SearchCountResult>();
        List<Object> searchCriteriaList = null;
        List<Object> memoCountList=null;
        String type = null;
        String query;
        String queryForMemoCount;

        if (searchCriteriatRequest != null
                && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest
                    .getType();
            type = searchCriteria.getType();
            if (searchCriteria.getValue() != null
                    && !("".equals(searchCriteria.getValue()))) {
                query = "select insAgr.identifier, insAgr.statusCode from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                        + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 and "
                        + "(insAgr.transactionKeys.key6 like ?4 or insAgr.transactionKeys.key8 like ?4 or insAgr.transactionKeys.key26 like ?4) and insAgr.transactionKeys.key37 <>?5  "
                        + "order by insAgr.id desc";
                
                queryForMemoCount = "select count(insAgr.id) from InsuranceAgreement insAgr,AppianMemoDetails memo where insAgr.agentId =?1 "
                        + "and insAgr.statusCode in (15) and insAgr.contextId =?2 and insAgr.transactionKeys.key21=memo.appNumber and memo.memoStatus='Open' and "
                        + "(insAgr.transactionKeys.key6 like ?3 or insAgr.transactionKeys.key8 like ?3 or insAgr.transactionKeys.key26 like ?3)";

                searchCriteriaList = findUsingQuery(searchCriteriatRequest,
                        query, searchCriteria.getAgentId(),
                        statusRequest.getType(),
                        searchCriteriatRequest.getContextId(), "%"
                                + searchCriteria.getValue() + "%" ,GeneraliConstants.BRANCH_ADMIN);
                
                memoCountList = findUsingQuery(searchCriteriatRequest,
                        queryForMemoCount, searchCriteria.getAgentId(),                       
                        searchCriteriatRequest.getContextId(), "%"
                                + searchCriteria.getValue() + "%");

            } else {
                query = "select insAgr.identifier, insAgr.statusCode from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                        + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 "
                        + "and MONTH(insAgr.creationDateTime) = MONTH(GETDATE()) and YEAR(insAgr.creationDateTime) = YEAR(GETDATE()) and insAgr.transactionKeys.key37 <>?4 "
                        + " order by insAgr.id desc";

                queryForMemoCount = "select count(lifeReq.id) from LifeAsiaRequest lifeReq,AppianMemoDetails memo where lifeReq.agentId =?1 and"
                        + " lifeReq.spajNo=memo.appNumber and memo.memoStatus='Open'";
                        

                searchCriteriaList = findUsingQuery(searchCriteriatRequest,
                        query, searchCriteria.getAgentId(),
                        statusRequest.getType(),
                        searchCriteriatRequest.getContextId(),GeneraliConstants.BRANCH_ADMIN);
                
                memoCountList = findUsingQuery(searchCriteriatRequest,
                        queryForMemoCount, searchCriteria.getAgentId());
            }
        }
        final List<SearchCountResult> subModeResults = new ArrayList<SearchCountResult>();
        final SearchCountResult searchCountResult = new SearchCountResult();
        List<String> identifier = new ArrayList<String>();
        Map<String,Long> countMap = new HashMap<String, Long>();
        SearchCountResult submode;
        Integer totalModCount = 0;
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object searchCriteria : searchCriteriaList) {
                Object[] values = (Object[]) searchCriteria;
                String eAppidentifier = (String) values[0];
                AgreementStatusCodeList mode = (AgreementStatusCodeList) values[1];
                
				if (!identifier.contains(eAppidentifier)) {
					identifier.add(eAppidentifier);

					if (!countMap.containsKey(mode.toString())) {

						countMap.put(mode.toString(), (long) 1);
					} else {
						Long internalCount = countMap.get(mode.toString());
						countMap.put(mode.toString(), internalCount + 1);
					}

				}               
                
              
            }
            for (Map.Entry<String, Long> entry : countMap.entrySet()) {
            	submode = new SearchCountResult();
            	submode.setCount(entry.getValue().toString());
                submode.setMode(entry.getKey());
                subModeResults.add(submode);
                totalModCount += entry.getValue().intValue();
    		}
            
            
            searchCountResult.setSubModeResults(subModeResults);
        }
        
        if (CollectionUtils.isNotEmpty(memoCountList)) {
            if(memoCountList.get(0) instanceof Long){
                Long count =  (Long) memoCountList.get(0);
                submode = new SearchCountResult();
                submode.setCount(count.toString());
                submode.setMode(OPEN_MEMO);
                subModeResults.add(submode);
                searchCountResult.setSubModeResults(subModeResults);
                }
            }
        
        searchCountResult.setCount(String.valueOf(totalModCount));
        searchCountResult.setMode(type);

        searchCountResponse.setType(searchCountResult);
        return searchCountResponse;
    }
    
    @Override
    public List<Agreement> getAgreementByFilter(Request<SearchCriteria> searchCriteriatRequest,
            Request<List<AgreementStatusCodeList>> statusRequest) {
        String query;
        List<Object> searchCriteriaList = null;

        if (searchCriteriatRequest != null && searchCriteriatRequest.getType() != null) {
            SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
            if (searchCriteria.getValue() != null && !("".equals(searchCriteria.getValue()))) {
                if(searchCriteria.getAgentType().equals(GeneraliConstants.BRANCH_ADMIN)){
                    query =
                
                        "select insAgr from InsuranceAgreement insAgr where insAgr.transactionKeys.key36 =?1 "
                                + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 and"
                                + "(insAgr.transactionKeys.key6 like ?4 or insAgr.transactionKeys.key8 like ?4) order by id desc";
                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getBranchAdminId(),
                                statusRequest.getType(), searchCriteriatRequest.getContextId(),
                                "%" + searchCriteria.getValue() + "%");
                }else if(searchCriteria.getAgentType().equals(GeneraliConstants.GAO)){
                    query =
                            
                            "select insAgr from InsuranceAgreement insAgr where (insAgr.transactionKeys.key35 =?1 or insAgr.transactionKeys.key36 =?2) "
                                    + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 and"
                                    + "(insAgr.transactionKeys.key6 like ?4 or insAgr.transactionKeys.key8 like ?4) order by id desc";
                    searchCriteriaList =
                            findUsingQuery(searchCriteriatRequest, query, searchCriteria.getGaoOfficeCode(),searchCriteria.getGaoId(),
                                    statusRequest.getType(), searchCriteriatRequest.getContextId(),
                                    "%" + searchCriteria.getValue() + "%");
                } else {
                    query =
                            
                            "select insAgr from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                    + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 and"
                                    + "(insAgr.transactionKeys.key6 like ?4 or insAgr.transactionKeys.key8 like ?4) " 
                                    + "and ( insAgr.transactionKeys.key37 is null or insAgr.transactionKeys.key37 !=?5) order by id desc";
                    searchCriteriaList =
                            findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                    statusRequest.getType(), searchCriteriatRequest.getContextId(),
                                    "%" + searchCriteria.getValue() + "%",GeneraliConstants.BRANCH_ADMIN);
                    }
            } else  {
                if(searchCriteria.getAgentType().equals(GeneraliConstants.BRANCH_ADMIN)){
                    query =
                        "select insAgr from InsuranceAgreement insAgr where insAgr.transactionKeys.key36 =?1 "
                                + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 order by id desc";

                searchCriteriaList =
                        findUsingQuery(searchCriteriatRequest, query, searchCriteria.getBranchAdminId(),
                                statusRequest.getType(), searchCriteriatRequest.getContextId());
                } else if(searchCriteria.getAgentType().equals(GeneraliConstants.GAO)){
                    query =
                            "select insAgr from InsuranceAgreement insAgr where (insAgr.transactionKeys.key35 =?1 or insAgr.transactionKeys.key36 =?2) "
                                    + "and insAgr.statusCode in ?3 and insAgr.contextId =?4 order by id desc";

                    searchCriteriaList =
                            findUsingQuery(searchCriteriatRequest, query, searchCriteria.getGaoOfficeCode(),searchCriteria.getGaoId(),
                                    statusRequest.getType(), searchCriteriatRequest.getContextId());
                } else {
                    query =
                            "select insAgr from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                    + "and insAgr.statusCode in ?2 and insAgr.contextId =?3"
                                    + " and  insAgr.transactionKeys.key37 <>?4 order by id desc";
                                    
                                    
                        searchCriteriaList =
                            findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                    statusRequest.getType(), searchCriteriatRequest.getContextId(),GeneraliConstants.BRANCH_ADMIN); 
                	
                /*	query =
                            "select insAgr from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                                    + "and insAgr.statusCode in ?2 and insAgr.contextId =?3 order by id desc";
                	
                	searchCriteriaList =
                            findUsingQuery(searchCriteriatRequest, query, searchCriteria.getAgentId(),
                                    statusRequest.getType(), searchCriteriatRequest.getContextId());*/

                   
                    }
            }
        }

        final List<Agreement> agreements = new ArrayList<Agreement>();
        List<String> identifier = new ArrayList<String>();
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object agreement : searchCriteriaList) {
				InsuranceAgreement agr = (InsuranceAgreement) agreement;
            	
            	if(!identifier.contains(agr.getIdentifier())){
            		identifier.add(agr.getIdentifier());
            		agreements.add(agr);
            	}
            	
                
            }
            identifier=null;
        }
        return agreements;
    }
    
    @Override
    public List<Agreement> getAgreements(Request<Agreement> agreementRequest,
            Request<List<AgreementStatusCodeList>> statusRequest, Request<Integer> limitRequest) {
        String query;
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
        query =
                "select insAgr from InsuranceAgreement insAgr where insAgr.agentId =?1 "
                        + "and insAgr.statusCode in ?2 and insAgr.modifiedDateTime>=?3 and insAgr.contextId =?4 "
                        + " and (insAgr.transactionKeys.key37 !=?5 or insAgr.transactionKeys.key37 is null)"
                        + " order by insAgr.modifiedDateTime asc";
        List<Object> insuranceAgreementIdList;
        if (limitRequest != null && limitRequest.getType() != null && limitRequest.getType() > 0) {
            insuranceAgreementIdList =
                    findUsingQueryWithLimit(agreementRequest, query, limitRequest.getType(),
                            insuranceAgreement.getAgentId(), statusRequest.getType(),
                            insuranceAgreement.getLastSyncDate(), agreementRequest.getContextId(),GeneraliConstants.BRANCH_ADMIN);
        } else {
            insuranceAgreementIdList =
                    findUsingQuery(agreementRequest, query, insuranceAgreement.getAgentId(), statusRequest.getType(),
                            insuranceAgreement.getLastSyncDate(), agreementRequest.getContextId());
        }
        final List<Agreement> agreements = new ArrayList<Agreement>();
        if (CollectionUtils.isNotEmpty(insuranceAgreementIdList)) {
            for (Object agreement : insuranceAgreementIdList) {
                agreements.add((InsuranceAgreement) agreement);
            }
        }
        return agreements;
    }
    
    public void updateAgreementStatusForLeadReassign(Request<Agreement> request) {
        if (request.getType() != null) {
           
		   Agreement agreement = (Agreement)request.getType();
        /*String query =
                "select insAgr from InsuranceAgreement insAgr where insAgr.contextId =?1 and insAgr.transactionKeys.key1 =?2 and insAgr.statusCode != ?3";

            final List<Object> insuranceAgreementList =
                    findUsingQuery(request, query, request.getContextId(),
                    		agreement.getTransactionKeys().getKey28(),agreement.getStatusCode());*/
            /*if (CollectionUtils.isNotEmpty(insuranceAgreementList)) {
                for (Object insAgr : insuranceAgreementList) {*/
                    InsuranceAgreement insrAgreement = ((InsuranceAgreement) agreement);
                    insrAgreement.setStatusCode(AgreementStatusCodeList.Cancelled);
                    request.setType(insrAgreement);
                    merge(request);

            /*    }
            }*/
        }
    }

    @Override
    public List<Agreement> getAggrementsWithExpiredGAOLocks(Request<Agreement> agreementRequest) {
        List<Object> searchCriteriaList = null;
        String query;
        final InsuranceAgreement insuranceAgreement = (InsuranceAgreement) agreementRequest.getType();
        query = "select  insAgr from InsuranceAgreement insAgr where "
                + " insAgr.modifiedDateTime<=?1 and insAgr.contextId =?2 and insAgr.statusCode not in (4,7)  and"
                + " insAgr.transactionKeys.key36 is not null and insAgr.transactionKeys.key36 <>'' and"
                + " (insAgr.transactionKeys.key37 <>?3 or insAgr.transactionKeys.key37 is null)";
        searchCriteriaList = findUsingQuery(agreementRequest, query,
                insuranceAgreement.getModifiedDateTime(),ContextTypeCodeList.EAPP,GeneraliConstants.BRANCH_ADMIN);
        final List<Agreement> agreements = new ArrayList<Agreement>();
        if (CollectionUtils.isNotEmpty(searchCriteriaList)) {
            for (Object agreement : searchCriteriaList) {
                agreements.add((InsuranceAgreement) agreement);
            }
        }
        return agreements;
    }

    @Override
    public Map<String, String> getEAppIllustrationCountForReport(Request<SearchCriteria> searchCriteriatRequest) {
        List<Object> countList = null;
        Map<String, String> agreementMap = new HashMap<String, String>();
        String query;
        final SearchCriteria searchCriteria = (SearchCriteria) searchCriteriatRequest.getType();
        List<String> agentIDList = searchCriteria.getAgentCodes();
        String statusCode = null;
        if (searchCriteriatRequest.getContextId().equals(ContextTypeCodeList.EAPP)) {
            statusCode = "15,21";
        } else {
            statusCode = "10,11";
        }
        query =
                "select insAgr.agentId,count(insAgr.identifier) from InsuranceAgreement insAgr where insAgr.agentId in ?1 "
                        + " and insAgr.statusCode in (" + statusCode + ") and insAgr.contextId =?2 "
                        + " and insAgr.creationDateTime between ?3 and ?4" + " group by insAgr.agentId";

        countList =
                findUsingQuery(searchCriteriatRequest, query, agentIDList, searchCriteriatRequest.getContextId(),
                        searchCriteria.getStartDate(), searchCriteria.getCurrentDate());

        if (CollectionUtils.isNotEmpty(countList)) {
            for (Object customerObj : countList) {
                Object[] values = (Object[]) customerObj;
                String agentCode = values[0].toString();
                String count = values[1].toString();
                agreementMap.put(agentCode, count);
            }

        }
        return agreementMap;
    }
    
    @Override
    public List<RiderSequence> retrieveRiderSequence() {
    	String query;
    	final Request<RiderSequence> riderSequenceRequest =
                new JPARequestImpl<RiderSequence>(entityManager, ContextTypeCodeList.EAPP, "");
        query = "select riderSeq from RiderSequence riderSeq";
        List<Object> riderObjectList = findUsingQuery(riderSequenceRequest, query);
        List<RiderSequence> riderSequenceList = new ArrayList<RiderSequence>();
        for(Object riderSequence: riderObjectList) {
        	riderSequenceList.add((RiderSequence)riderSequence);
        }
    	return riderSequenceList;
    }
   
}
