/**
 * 
 */
package com.cognizant.insurance.domain.documentandcommunication;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.ExternalCode;
import com.cognizant.insurance.domain.commonelements.complexdatatypes.TimePeriod;
import com.cognizant.insurance.domain.documentandcommunication.documentandcommunicationcodelists.DocumentTypeCodeList;

/**
 * This concept is a generalizing concept for all documents (for example : an
 * expert report, a letter with free text, passport...).
 * 
 * 
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DOCUMENT_TYPE", discriminatorType = DiscriminatorType.STRING)
public class Document extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -870863549174990899L;

    /**
     * An Integer assigned to a document to uniquely identify it (e.g. police
     * report number, fire department report number, etc.).
     * 
     * 
     * 
     */
    private Integer identifier;

    /**
     * It is possible to give a name to a document. This name can be used to
     * give a short description of the document. Example: Mr Smith's passport
     * 
     * 
     * 
     */
    private String name;

    /**
     * Validity period.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    private TimePeriod effectivePeriod;

    /**
     * Defines the type or the nature of a document in simple cases. See also
     * the DocumentCategory to handle specific issues.
     * 
     * 
     * 
     * 
     */
    @Enumerated
    private DocumentTypeCodeList typeCode;

    /**
     * The date on which the document was received.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date receptionDate;

    /**
     * The date on which the document was emitted or sent/discharged.
     * 
     * 
     * 
     */
    @Temporal(TemporalType.DATE)
    private Date documentEmissionDate;

    /**
     * Define if the document is modifiable or not.
     * 
     * 
     * 
     * 
     * 
     */
    private Boolean modifiableIndicator;

    /**
     * Defines the document version number.
     * 
     * 
     * 
     * 
     * 
     */
    private Integer versionIdentifier;

    /**
     * Define where the user stocked the letters (ex: paper letter stored in the
     * file, email in the data processing).
     * 
     * 
     * 
     * 
     * 
     */
    private String storageLocation;

    /**
     * Define if yes or no the signature is required.
     * 
     * 
     * 
     */
    private Boolean signatureRequiredIndicator;

    /**
     * Define if the document has been signed.
     * 
     * 
     * 
     */
    private Boolean signedIndicator;

    /**
     * A code representing the language used in this document (see ISO 639).
     * 
     * REFERENCE:
     * http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail
     * .htm?csnumber=22109
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
    private ExternalCode languageCode;

    /**
     * This indicates the document is a free content document.
     * 
     * 
     * 
     */
    private Boolean freeContentIndicator;

    /******Newly added for EApp - Start ********/
    
    /** 
     * Defines the description of the document  
     **/
    private String description;

    /** 
     * Identifies the document submitted as proof 
     **/
    @Column(columnDefinition="nvarchar(300)")
    private String documentProofSubmitted;
    
    /** 
     * Defines the document Status
     **/
    private String documentStatus;
    
    /**
     * Mandatory Indicator
     */
    private Boolean isMandatory;
    
   
  
    /** 
     * Defines the pages Filenames
     **/
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    private Set<Document> pages;   
    
    /** 
     * Defines the Filenames
     **/
    private String fileNames;       
    
    /** 
     * Defines the base64string
     **/
    //@Column(columnDefinition="NVARCHAR(MAX)")
    @Transient
    private String base64string;   
    
    /** The file content. */
    @Transient
    private String fileContent;
    
    
    /**
     * The code of the document
     */
    private String documentTypeCode;
    
    private String contentType;

    /**
     * Added for Generali
     */
    private String documentUploadStatus;
    
    private String signatureType;

    public String getDocumentUploadStatus() {
        return documentUploadStatus;
    }

    public void setDocumentUploadStatus(String documentUploadStatus) {
        this.documentUploadStatus = documentUploadStatus;
    }

    public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
     * Defines the pages Filenames
     **/
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE })
    private Set<DocumentOption> documentOptions;
    
    /******Newly added for EApp - End ********/
     
    /**
     * @return the documentOptions
     */
    public Set<DocumentOption> getDocumentOptions() {
        return documentOptions;
    }

    /**
     * @param documentOptions
     *            the documentOptions to set
     */
    public void setDocumentOptions(Set<DocumentOption> documentOptions) {
        this.documentOptions = documentOptions;
    }
    
    public Boolean getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(Boolean isMandatory) {
        this.isMandatory = isMandatory;
    }
    /**
     * Gets the identifier.
     * 
     * @return Returns the identifier.
     */
    public Integer getIdentifier() {
        return identifier;
    }

    /**
     * Gets the name.
     * 
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the effectivePeriod.
     * 
     * @return Returns the effectivePeriod.
     */
    public TimePeriod getEffectivePeriod() {
        return effectivePeriod;
    }

    /**
     * Gets the typeCode.
     * 
     * @return Returns the typeCode.
     */
    public DocumentTypeCodeList getTypeCode() {
        return typeCode;
    }

    /**
     * Gets the receptionDate.
     * 
     * @return Returns the receptionDate.
     */
    public Date getReceptionDate() {
        return receptionDate;
    }

    /**
     * Gets the documentEmissionDate.
     * 
     * @return Returns the documentEmissionDate.
     */
    public Date getDocumentEmissionDate() {
        return documentEmissionDate;
    }

    /**
     * Gets the modifiableIndicator.
     * 
     * @return Returns the modifiableIndicator.
     */
    public Boolean getModifiableIndicator() {
        return modifiableIndicator;
    }

    /**
     * Gets the versionIdentifier.
     * 
     * @return Returns the versionIdentifier.
     */
    public Integer getVersionIdentifier() {
        return versionIdentifier;
    }

    /**
     * Gets the storageLocation.
     * 
     * @return Returns the storageLocation.
     */
    public String getStorageLocation() {
        return storageLocation;
    }

    /**
     * Gets the signatureRequiredIndicator.
     * 
     * @return Returns the signatureRequiredIndicator.
     */
    public Boolean getSignatureRequiredIndicator() {
        return signatureRequiredIndicator;
    }

    /**
     * Gets the signedIndicator.
     * 
     * @return Returns the signedIndicator.
     */
    public Boolean getSignedIndicator() {
        return signedIndicator;
    }

    /**
     * Gets the languageCode.
     * 
     * @return Returns the languageCode.
     */
    public ExternalCode getLanguageCode() {
        return languageCode;
    }

    /**
     * Gets the freeContentIndicator.
     * 
     * @return Returns the freeContentIndicator.
     */
    public Boolean getFreeContentIndicator() {
        return freeContentIndicator;
    }

    /**
     * Sets The identifier.
     * 
     * @param identifier
     *            The Integer to set.
     */
    public void setIdentifier(final Integer identifier) {
        this.identifier = identifier;
    }

    /**
     * Sets The name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets The effectivePeriod.
     * 
     * @param effectivePeriod
     *            The effectivePeriod to set.
     */
    public void setEffectivePeriod(final TimePeriod effectivePeriod) {
        this.effectivePeriod = effectivePeriod;
    }

    /**
     * Sets The typeCode.
     * 
     * @param typeCode
     *            The typeCode to set.
     */
    public void setTypeCode(final DocumentTypeCodeList typeCode) {
        this.typeCode = typeCode;
    }

    /**
     * Sets The receptionDate.
     * 
     * @param receptionDate
     *            The receptionDate to set.
     */
    public void setReceptionDate(final Date receptionDate) {
        this.receptionDate = receptionDate;
    }

    /**
     * Sets The documentEmissionDate.
     * 
     * @param documentEmissionDate
     *            The documentEmissionDate to set.
     */
    public void
            setDocumentEmissionDate(final Date documentEmissionDate) {
        this.documentEmissionDate = documentEmissionDate;
    }

    /**
     * Sets The modifiableIndicator.
     * 
     * @param modifiableIndicator
     *            The modifiableIndicator to set.
     */
    public void setModifiableIndicator(final Boolean modifiableIndicator) {
        this.modifiableIndicator = modifiableIndicator;
    }

    /**
     * Sets The versionIdentifier.
     * 
     * @param versionIdentifier
     *            The versionIdentifier to set.
     */
    public void setVersionIdentifier(final Integer versionIdentifier) {
        this.versionIdentifier = versionIdentifier;
    }

    /**
     * Sets The storageLocation.
     * 
     * @param storageLocation
     *            The storageLocation to set.
     */
    public void setStorageLocation(final String storageLocation) {
        this.storageLocation = storageLocation;
    }

    /**
     * Sets The signatureRequiredIndicator.
     * 
     * @param signatureRequiredIndicator
     *            The signatureRequiredIndicator to set.
     */
    public void setSignatureRequiredIndicator(
            final Boolean signatureRequiredIndicator) {
        this.signatureRequiredIndicator = signatureRequiredIndicator;
    }

    /**
     * Sets The signedIndicator.
     * 
     * @param signedIndicator
     *            The signedIndicator to set.
     */
    public void setSignedIndicator(final Boolean signedIndicator) {
        this.signedIndicator = signedIndicator;
    }

    /**
     * Sets The languageCode.
     * 
     * @param languageCode
     *            The languageCode to set.
     */
    public void setLanguageCode(final ExternalCode languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * Sets The freeContentIndicator.
     * 
     * @param freeContentIndicator
     *            The freeContentIndicator to set.
     */
    public void
            setFreeContentIndicator(final Boolean freeContentIndicator) {
        this.freeContentIndicator = freeContentIndicator;
    }

    /**
     * Gets the documentProofSubmitted.
     * 
     * @return Returns the documentProofSubmitted.
     */    
	public String getDocumentProofSubmitted() {
		return documentProofSubmitted;
	}

	/**
     * Sets The documentProofSubmitted.
     * 
     * @param documentProofSubmitted
     *            The documentProofSubmitted to set.
     */
	public void setDocumentProofSubmitted(String documentProofSubmitted) {
		this.documentProofSubmitted = documentProofSubmitted;
	}

	/**
     * Gets the documentStatus.
     * 
     * @return Returns the documentStatus.
     */ 
	public String getDocumentStatus() {
		return documentStatus;
	}

	/**
     * Sets The documentStatus.
     * 
     * @param documentStatus
     *            The documentStatus to set.
     */
	public void setDocumentStatus(String documentStatus) {
		this.documentStatus = documentStatus;
	}

	/**
     * Sets The pagesFilenames.
     * 
     * @param pagesFilenames
     *            The pagesFilenames to set.
     */	
	public void setPages(Set<Document> pages) {
		this.pages = pages;
	}

	/**
     * Gets the pagesFilenames.
     * 
     * @return Returns the pagesFilenames.
     */ 
	public Set<Document> getPages() {
		return pages;
	}
	 
    /**
     * Sets The pagesFilenames.
     * 
     * @param pagesFilenames
     *            The pagesFilenames to set.
     */
	public void setFileNames(String fileNames) {
		this.fileNames = fileNames;
	}

	/**
     * Gets the pagesFilenames.
     * 
     * @return Returns the pagesFilenames.
     */ 
	public String getFileNames() {
		return fileNames;
	}

	/**
     * Sets The base64string.
     * 
     * @param base64string
     *            The base64string to set.
     */
	public void setBase64string(String base64string) {
		this.base64string = base64string;
	}

	/**
     * Gets the base64string.
     * 
     * @return Returns the base64string.
     */
	public String getBase64string() {
		return base64string;
	}

    /**
     * @return the fileContent
     */
    public final String getFileContent() {
        return fileContent;
    }

    /**
     * @param fileContent
     *            the fileContent to set
     */
    public final void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    /**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public String getDocumentTypeCode() {
		return documentTypeCode;
	}

	public void setDocumentTypeCode(String documentTypeCode) {
		this.documentTypeCode = documentTypeCode;
	}

	public String getSignatureType() {
		return signatureType;
	}

	public void setSignatureType(String signatureType) {
		this.signatureType = signatureType;
	}
    
    

}
