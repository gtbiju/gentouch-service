/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304007
 * @version       : 0.1, Dec 14, 2012
 */
package com.cognizant.insurance.domain.party.partycodelists;

/**
 * This class corresponding to all the list values against a code list name.
 */
public class PartyCodeListValues {
    /** The id. */
    private Long id;
    
    /** The key. */
    private PartyCodeList key;
    
    /** The code name. */
    private String codeName;
    
    /** The code description. */
    private String codeDescription;

    /**
     * Gets the codeDescription.
     *
     * @return Returns the codeDescription.
     */
    public String getCodeDescription() {
        return codeDescription;
    }

    /**
     * Gets the codeName.
     *
     * @return Returns the codeName.
     */
    public String getCodeName() {
        return codeName;
    }

    /**
     * Gets the id.
     *
     * @return Returns the id.
     */
    public Long getId() {
        return id;
    }
    
    /**
     * Sets The codeDescription.
     *
     * @param codeDescription The codeDescription to set.
     */
    public void setCodeDescription(final String codeDescription) {
        this.codeDescription = codeDescription;
    }

    /**
     * Sets The codeName.
     *
     * @param codeName The codeName to set.
     */
    public void setCodeName(final String codeName) {
        this.codeName = codeName;
    }

    /**
     * Sets The id.
     *
     * @param id The id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the key.
     *
     * @return Returns the key.
     */
    public PartyCodeList getKey() {
        return key;
    }

    /**
     * Sets The key.
     *
     * @param key The key to set.
     */
    public void setKey(final PartyCodeList key) {
        this.key = key;
    }
}
