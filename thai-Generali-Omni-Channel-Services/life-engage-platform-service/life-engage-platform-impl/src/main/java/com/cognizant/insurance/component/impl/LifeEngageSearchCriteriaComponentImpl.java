/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Jul 31, 2015
 */
package com.cognizant.insurance.component.impl;

import static com.cognizant.insurance.core.helper.ExceptionHelper.throwSystemException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cognizant.insurance.component.LifeEngageSearchCriteriaComponent;
import com.cognizant.insurance.component.helper.LifeEngageComponentHelper;
import com.cognizant.insurance.component.repository.EAppRepository;
import com.cognizant.insurance.component.repository.FNARepository;
import com.cognizant.insurance.component.repository.IllustrationRepository;
import com.cognizant.insurance.component.repository.LMSRepository;
import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.constants.ErrorConstants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.core.exception.SystemException;
import com.cognizant.insurance.core.smooks.LifeEngageSmooksHolder;
import com.cognizant.insurance.request.vo.RequestInfo;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;
import com.cognizant.insurance.searchcriteria.SearchCriteriaResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class class LifeEngageSearchCriteriaComponentImpl.
 */
@Service("lifeEngageSearchCriteriaComponent")
public class LifeEngageSearchCriteriaComponentImpl implements LifeEngageSearchCriteriaComponent {

    /** The lms repository. */
    @Autowired
    private LMSRepository lmsRepository;

    /** The fna repository. */
    @Autowired
    private FNARepository fnaRepository;

    /** The illustration repository. */
    @Autowired
    private IllustrationRepository illustrationRepository;

    /** The e app repository. */
    @Autowired
    private EAppRepository eAppRepository;

    /** The retrieve by count holder. */
    @Autowired
    @Qualifier("retrieveByCountMapping")
    private LifeEngageSmooksHolder retrieveByCountHolder;

    /** The retrieve by filter holder. */
    @Autowired
    @Qualifier("retrieveByFilterMapping")
    private LifeEngageSmooksHolder retrieveByFilterHolder;
    
    /** The retrieve by Ids holder. */
    @Autowired
    @Qualifier("retrieveByIdsMapping")
    private LifeEngageSmooksHolder retrieveByIdsHolder;

    /** The Constant FNA. */
    private static final String FNA = "FNA";

    /** The Constant LMS. */
    private static final String LMS = "LMS";

    /** The Constant E_APP. */
    private static final String E_APP = "eApp";

    /** The Constant illustration. */
    private static final String ILLUSTRATION = "illustration";

    /** The Constant TransactionData. */
    private static final String TRANSACTIONDATA = "TransactionData";

    /** The Constant LOGGER. */
    public static final Logger LOGGER = LoggerFactory.getLogger(LifeEngageSearchCriteriaComponentImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageSearchCriteriaComponent#retrieveByFilterCount(com.cognizant.insurance
     * .request.vo.RequestInfo, org.json.JSONArray)
     */
    @Override
    public String retrieveByFilterCount(final RequestInfo requestInfo, final JSONArray jsonRqArray) {
        Transactions transactions = new Transactions();
        SearchCriteriaResponse searchCriteriaResponse = new SearchCriteriaResponse();
        String response = null;
        String agentId = null;
        try {
            if (jsonRqArray != null) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);
                    JSONObject jsonTransactionData = null;
                    SearchCriteria searchCriteria = new SearchCriteria();
                    agentId = jsonTransactionsObj.getString(Constants.KEY11);
                    if (agentId != null && !("").equals(agentId)) {
                        ArrayList<String> modesList = new ArrayList<String>();
                        if (!(jsonTransactionsObj.isNull(TRANSACTIONDATA))) {
                            jsonTransactionData = jsonTransactionsObj.getJSONObject(TRANSACTIONDATA);
                            final JSONObject searchCriteriaRqJson =
                                    jsonTransactionData.getJSONObject(Constants.SEARCHCRITERIAREQUEST);
                            JSONArray jsonModesArray = searchCriteriaRqJson.getJSONArray(Constants.MODES);
                            for (int j = 0; j < jsonModesArray.length(); j++) {
                                modesList.add((String) jsonModesArray.get(j));

                            }
                            String command = searchCriteriaRqJson.getString(Constants.COMMAND);
                            searchCriteria.setCommand(command);
                            searchCriteria.setValue(searchCriteriaRqJson.getString(Constants.VALUE));
                            searchCriteria.setAgentId(agentId);
                            searchCriteria.setModes(modesList);
                            if ((Constants.LANDING_DASHBOARD_COUNT).equals(command)) {
                                searchCriteriaResponse = retrieveByCount(requestInfo, searchCriteria);
                            }
                        }
                    } else {
                        throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, Constants.AGENTID_MISSING);
                    }
                }
            }
            transactions.setAgentId(agentId);
            transactions.setSearchCriteriaResponse(searchCriteriaResponse);
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (Exception e) {
            LOGGER.error("Unable to retrieve count by each module :" + e.getMessage());
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

        } finally {
            response = retrieveByCountHolder.parseBO(transactions);

        }

        return response;
    }

    /**
     * Retrieve by count.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the string
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    protected SearchCriteriaResponse
            retrieveByCount(final RequestInfo requestInfo, final SearchCriteria searchCriteria)
                    throws BusinessException, ParseException {
        LOGGER.trace("Inside LifeEngageSearchCriteriaComponentImpl retrieveByCount : requestInfo" + requestInfo);
        SearchCountResult searchCountResult = null;
        SearchCriteriaResponse searchCriteriaResponse = new SearchCriteriaResponse();
        List<SearchCountResult> searchCountResultList = new ArrayList<SearchCountResult>();
        ArrayList<String> modeList = searchCriteria.getModes();

        if (modeList.size() > 0) {
            for (String mode : modeList) {
                if (E_APP.equals(mode)) {
                    searchCriteria.setType(mode);
                    searchCountResult = eAppRepository.retrieveByCountEApp(requestInfo, searchCriteria);
                    searchCountResultList.add(searchCountResult);
                } else if (ILLUSTRATION.equals(mode)) {
                    searchCriteria.setType(mode);
                    searchCountResult = illustrationRepository.retrieveByCountIllustration(requestInfo, searchCriteria);
                    searchCountResultList.add(searchCountResult);
                } else if (FNA.equals(mode)) {
                    searchCriteria.setType(mode);
                    searchCountResult = fnaRepository.retrieveByCountFNA(requestInfo, searchCriteria);
                    searchCountResultList.add(searchCountResult);
                } else if (LMS.equals(mode)) {
                    searchCriteria.setType(mode);
                    searchCountResult = lmsRepository.retrieveByCountLMS(requestInfo, searchCriteria);
                    searchCountResultList.add(searchCountResult);
                }
            }
        }
        searchCriteriaResponse.setModeResults(searchCountResultList);
        return searchCriteriaResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageSearchCriteriaComponent#retrieveByFilter(com.cognizant.insurance.
     * request.vo.RequestInfo, org.json.JSONArray)
     */
    @Override
    public String retrieveByFilter(final RequestInfo requestInfo, final JSONArray jsonRqArray) {
        Transactions transactions = new Transactions();
        String response = null;
        String agentId = null;
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        try {
            if (jsonRqArray != null) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);
                    JSONObject jsonTransactionData = null;
                    SearchCriteria searchCriteria = new SearchCriteria();
                    agentId = jsonTransactionsObj.getString(Constants.KEY11);
                    if (agentId != null && !("").equals(agentId)) {
                        ArrayList<String> modesList = new ArrayList<String>();
                        if (!(jsonTransactionsObj.isNull(TRANSACTIONDATA))) {
                            jsonTransactionData = jsonTransactionsObj.getJSONObject(TRANSACTIONDATA);
                            final JSONObject searchCriteriaRqJson =
                                    jsonTransactionData.getJSONObject(Constants.SEARCHCRITERIAREQUEST);
                            JSONArray jsonModesArray = searchCriteriaRqJson.getJSONArray(Constants.MODES);
                            for (int j = 0; j < jsonModesArray.length(); j++) {
                                modesList.add((String) jsonModesArray.get(j));

                            }
                            String command = searchCriteriaRqJson.getString(Constants.COMMAND);
                            searchCriteria.setCommand(command);
                            searchCriteria.setValue(searchCriteriaRqJson.getString(Constants.VALUE));
                            searchCriteria.setAgentId(agentId);
                            searchCriteria.setModes(modesList);
                            if (Constants.LISTING_DASHBOARD.equals(command)) {
                                transactionsList = retrieveFilter(requestInfo, searchCriteria);
                            }
                        }
                    } else {
                        throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, Constants.AGENTID_MISSING);
                    }
                }
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (Exception e) {
            LOGGER.error("Unable to retrieve listing details for retrieve filter :" + e.getMessage());
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

        } finally {
            response = retrieveByFilterHolder.parseBO(transactionsList);

        }

        return response;
    }

    /**
     * Retrieve filter.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    protected List<Transactions> retrieveFilter(final RequestInfo requestInfo, final SearchCriteria searchCriteria)
            throws BusinessException, ParseException {
        LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter : requestInfo" + requestInfo);
        ArrayList<String> modeList = searchCriteria.getModes();
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        if (modeList.size() > 0) {
            String mode = modeList.get(0);
            if (E_APP.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = eAppRepository.retrieveByFilterEApp(requestInfo, searchCriteria);
            } else if (ILLUSTRATION.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = illustrationRepository.retrieveByFilterIllustration(requestInfo, searchCriteria);
            } else if (FNA.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = fnaRepository.retrieveByFilterFNA(requestInfo, searchCriteria);
            } else if (LMS.equals(mode)) {
                searchCriteria.setType(mode);
                transactionsList = lmsRepository.retrieveByFilterLMS(requestInfo, searchCriteria);
            }
            LOGGER.trace("Inside LifeEngageSyncImpl retrieveFilter ");
        }
        return transactionsList;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.insurance.component.LifeEngageSearchCriteriaComponent#retrieveByIds(com.cognizant.insurance.request
     * .vo.RequestInfo, org.json.JSONArray)
     */
    @Override
    public String retrieveByIds(RequestInfo requestInfo, JSONArray jsonRqArray) {
        Transactions transactions = new Transactions();
        String response = null;
        String agentId = null;
        List<Transactions> transactionsList = new ArrayList<Transactions>();
        try {
            if (jsonRqArray != null) {
                for (int i = 0; i < jsonRqArray.length(); i++) {
                    final JSONObject jsonTransactionsObj = jsonRqArray.getJSONObject(i);
                    JSONObject jsonTransactionData = null;
                    SearchCriteria searchCriteria = new SearchCriteria();
                    agentId = jsonTransactionsObj.getString(Constants.KEY11);
                    if (agentId != null && !("").equals(agentId)) {
                        ArrayList<String> idsList = new ArrayList<String>();
                        if (!(jsonTransactionsObj.isNull(TRANSACTIONDATA))) {
                            jsonTransactionData = jsonTransactionsObj.getJSONObject(TRANSACTIONDATA);
                            final String typemode = jsonTransactionsObj.getString(Constants.TYPE);
                            final JSONObject searchCriteriaRqJson =
                                    jsonTransactionData.getJSONObject(Constants.SEARCHCRITERIAREQUEST);
                            JSONArray jsonIdsArray = searchCriteriaRqJson.getJSONArray(Constants.IDS);
                            for (int k = 0; k < jsonIdsArray.length(); k++) {
                                idsList.add((String) jsonIdsArray.get(k));
                            }
                            String command = searchCriteriaRqJson.getString(Constants.COMMAND);
                            searchCriteria.setCommand(command);
                            searchCriteria.setAgentId(agentId);

                            searchCriteria.setSelectedIds(idsList);
                            searchCriteria.setType(typemode);
                            if (Constants.EMAIL_TRIGGERING.equals(command)) {
                                transactionsList = retrieveIdsByEmail(requestInfo, searchCriteria);
                            }
                        }
                    } else {
                        throwSystemException(true, ErrorConstants.LE_SYNC_ERR_105, Constants.AGENTID_MISSING);
                    }
                }
            }
        } catch (SystemException e) {
            LOGGER.error("SystemException", e);
            if (e.getErrorCode() == null || ("").equals(e.getErrorCode().trim())) {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        ErrorConstants.LE_SYNC_ERR_105);
            } else {
                LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage(),
                        e.getErrorCode());
            }
        } catch (Exception e) {
            LOGGER.error("Unable to retrieve listing details for retrieve ids for email triggering :" + e.getMessage());
            LifeEngageComponentHelper.createResponseStatus(transactions, Constants.FAILURE, e.getMessage() + " : "
                    + getExceptionMessage(e), ErrorConstants.LE_SYNC_ERR_100);

        } finally {
            response = retrieveByIdsHolder.parseBO(transactionsList);

        }

        return response;
    }

    /**
     * Retrieve ids by email.
     * 
     * @param requestInfo
     *            the request info
     * @param searchCriteria
     *            the search criteria
     * @return the list
     * @throws BusinessException
     *             the business exception
     * @throws ParseException
     *             the parse exception
     */
    protected List<Transactions> retrieveIdsByEmail(final RequestInfo requestInfo, final SearchCriteria searchCriteria)
            throws BusinessException, ParseException {
        LOGGER.trace("Inside LifeEngageSearchCriteriaComponentImpl retrieveIdsByEmail : requestInfo" + requestInfo);

        List<Transactions> transactionsList = new ArrayList<Transactions>();
        if (E_APP.equals(searchCriteria.getType())) {
            transactionsList = eAppRepository.retrieveIdsByEmailEApp(requestInfo, searchCriteria);
        } else if (ILLUSTRATION.equals(searchCriteria.getType())) {
            transactionsList = illustrationRepository.retrieveIdsByEmailIllustration(requestInfo, searchCriteria);
        } else if (FNA.equals(searchCriteria.getType())) {
            transactionsList = fnaRepository.retrieveIdsByEmailFNA(requestInfo, searchCriteria);
        } else if (LMS.equals(searchCriteria.getType())) {
            transactionsList = lmsRepository.retrieveIdsByEmailLMS(requestInfo, searchCriteria);
        }
        LOGGER.trace("Inside LifeEngageSearchCriteriaComponentImpl retrieveIdsByEmail ");

        return transactionsList;
    }

    /**
     * Gets the exception message.
     * 
     * @param exception
     *            the exception
     * @return the exception message
     */
    protected String getExceptionMessage(final Exception exception) {
        String message = "";
        if (exception.getCause() != null) {
            message = exception.getCause().toString();
        }

        return message;
    }
}
