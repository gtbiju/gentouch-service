package com.cognizant.insurance.omnichannel.component;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import com.cognizant.insurance.core.exception.BusinessException;

public interface LifeAsiaRequestSubmitter {
	
	public String submit(String request) throws BusinessException;
	public String fetchStatusChanges(String boday) throws BusinessException;
	public String submit(Source object) throws BusinessException;
	public String getLAStatus() throws BusinessException;
	public String postJSONToBPM(String jsonString, String url) throws BusinessException;

}
