package com.cognizant.insurance.response.vo;


import java.util.List;
import java.util.Map;

import com.cognizant.insurance.agent.Agent;
import com.cognizant.insurance.agent.AgentAchievement;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliAgentAddress;
import com.cognizant.insurance.agent.GeneraliGAOAgent;

/**
 * The Class AgentResponse.
 */
public class AgentProfileResponse {
	
	/** The response info. */
	private ResponseInfo responseInfo;
	
	/** The agent. */
	private Agent agent;
	
	/** The Generali Agent. */
	private GeneraliAgent generaliAgent;
	
	private GeneraliGAOAgent gaoAgent;
	
	private GeneraliAgentAddress address;
	
	private  Map<String, String> agentProductList;
	
	private List<AgentAchievement> agentAchievementList;

	

    /**
	 * Gets the response info.
	 *
	 * @return the response info
	 */
	public ResponseInfo getResponseInfo() {
		return responseInfo;
	}

	/**
	 * Sets the response info.
	 *
	 * @param responseInfo the new response info
	 */
	public void setResponseInfo(ResponseInfo responseInfo) {
		this.responseInfo = responseInfo;
	}

	/**
	 * Gets the agent.
	 *
	 * @return the agent
	 */
	public Agent getAgent() {
		return agent;
	}

	/**
	 * Sets the agent.
	 *
	 * @param agent the new agent
	 */
	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public GeneraliAgent getGeneraliAgent() {
		return generaliAgent;
	}

	public void setGeneraliAgent(GeneraliAgent generaliAgent) {
		this.generaliAgent = generaliAgent;
	}

    public Map<String, String> getAgentProductList() {
        return agentProductList;
    }

    public void setAgentProductList(Map<String, String> agentProductList) {
        this.agentProductList = agentProductList;
    }

	public GeneraliGAOAgent getGaoAgent() {
		return gaoAgent;
	}

	public void setGaoAgent(GeneraliGAOAgent gaoAgent) {
		this.gaoAgent = gaoAgent;
	}

	public GeneraliAgentAddress getAddress() {
		return address;
	}

	public void setAddress(GeneraliAgentAddress address) {
		this.address = address;
	}
	public List<AgentAchievement> getAgentAchievementList() {
		return agentAchievementList;
	}

	public void setAgentAchievementList(List<AgentAchievement> agentAchievementList) {
		this.agentAchievementList = agentAchievementList;
	}

}
