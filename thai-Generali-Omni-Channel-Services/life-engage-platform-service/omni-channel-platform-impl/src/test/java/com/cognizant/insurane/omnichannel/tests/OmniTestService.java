/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 356551
 * @version       : 0.1, Apr 18, 2018
 */
package com.cognizant.insurane.omnichannel.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.omnichannel.component.repository.GeneraliEAppRepository;
import com.cognizant.insurance.omnichannel.service.GeneraliService;

@ContextConfiguration("/service-context-test.xml")
public class OmniTestService extends AbstractJUnit4SpringContextTests{
    
    @Autowired
    GeneraliService generaliService;
    
    @Autowired
    GeneraliEAppRepository eAppRepo;
    
    @Test
    public void updateMemoServiceTest(){
        String responseJson = null;
        try {
            responseJson = generaliService.updateMemoDetails(getRequest("appianMemoRequest/appianMemoRq.json"));
        } catch (BusinessException e) {
        } catch (IOException e) {
        }
        assertNotNull(responseJson);
        assertEquals("Success", responseJson);
    }
    
    @Test
    public void unlockIdleTest(){
        eAppRepo.updateGAOLockStatus();
    }
    /**
     * Gets the request.
     * 
     * @param requestFileName
     *            the request file name
     * @return the request
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getRequest(String requestFileName) throws IOException {
        FileReader reader =
                new FileReader(Thread.currentThread().getContextClassLoader().getResource(requestFileName).getFile());
        BufferedReader buffer = new BufferedReader(reader);

        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = buffer.readLine()) != null) {
            sb.append(new String(line.getBytes(), "UTF-8"));
        }

        return sb.toString();
    }

}
