/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Nov 13, 2012
 */
package com.cognizant.insurance.agent.party.dao;

import java.util.List;
import java.util.Set;

import com.cognizant.insurance.agent.dao.Dao;
import com.cognizant.insurance.agent.domain.agreement.Agreement;
import com.cognizant.insurance.agent.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Appointee;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.agent.domain.contactandplace.contactcodelists.TelephoneTypeCodeList;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyrolesubtypes.PartyRoleInRelationship;
import com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists.CustomerStatusCodeList;
import com.cognizant.insurance.agent.request.Request;
import com.cognizant.insurance.agent.request.vo.Transactions;
import com.cognizant.insurance.agent.response.Response;
import com.cognizant.insurance.agent.searchcriteria.SearchCountResult;
/*import com.cognizant.insurance.searchcriteria.SearchCountResult;
import com.cognizant.insurance.searchcriteria.SearchCriteria;*/
import com.cognizant.insurance.agent.searchcriteria.SearchCriteria;

/**
 * The Interface interface PartyDao.
 */
public interface PartyDao extends Dao {

    /**
     * Retrieve.
     * 
     * @param request
     *            the request
     * @return the response
     */
    Response<Transactions> retrieve(Request<InsuranceAgreement> request);

    /**
     * Retrieve basic details.
     * 
     * @param request
     *            the request
     * @return the response
     */
    Response<List<Transactions>> retrieveBasicDetails(Request<InsuranceAgreement> request);

    /**
     * Retrieve all details.
     * 
     * @param request
     *            the request
     * @return the response
     */
    Response<List<Transactions>> retrieveAll(Request<InsuranceAgreement> request);

    /**
     * Retrieve insured.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<Insured> retrieveInsured(Request<Agreement> agreementRequest);

    /**
     * Retrieve payer.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<PremiumPayer> retrievePayer(Request<Agreement> agreementRequest);

    /**
     * Retrieve proposer.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<AgreementHolder> retrieveProposer(Request<Agreement> agreementRequest);

    /**
     * Retrieve beneficiaries.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<Set<Beneficiary>> retrieveBeneficiaries(Request<Agreement> agreementRequest);

    /**
     * Retrieve appointee.
     * 
     * @param agreementRequest
     *            the agreement request
     * @return the response
     */
    Response<Appointee> retrieveAppointee(Request<Agreement> agreementRequest);

    /**
     * Retrieve Customer.
     * 
     * @param agreementRequest
     *            the agreement request
     * @param statusRequest
     *            the status request
     * @return the response
     */
    Response<Customer> retrieveCustomer(Request<Customer> identifierRequest,
			Request<List<CustomerStatusCodeList>> statusRequest);

    /**
     * Retrieve Customers.
     * 
     * @param customerRequest
     *            the customer request
     * @param statusRequest
     *            the status request
     * @param limitRequest
     *            the limit request
     * @return the list
     */
    List<Customer> retrieveCustomers(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest, Request<Integer> limitRequest);

    /**
     * Retrieve Customer for an identifier.
     * 
     * @param customerRequest
     *            the customer request
     * @param statusRequest
     *            the status request
     * @return the list
     */
    List<Customer> retrieveCustomerForIdentifier(final Request<Customer> customerRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest);

    /**
     * Retrieve lms duplicate customers.
     * 
     * @param customerRequest
     *            the customer request
     * @param transactions
     *            the transactions
     * @param statusRequest
     *            the status request
     * @param telephoneTypeRequest
     *            the telephone type request
     * @return the list
     */
    List<Customer> retrieveLMSDuplicateCustomers(final Request<PartyRoleInRelationship> customerRequest,
            final Transactions transactions, final Request<List<CustomerStatusCodeList>> statusRequest,
            final Request<List<TelephoneTypeCodeList>> telephoneTypeRequest);

    /**
     * Gets the customer count.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the customer count
     */
    Response<SearchCountResult> getCustomerCount(final Request<SearchCriteria> searchCriteriatRequest,final Request<List<CustomerStatusCodeList>> statusRequest);

    /**
     * Retrieve filter customers.
     * 
     * @param searchCriteriatRequest
     *            the search criteriat request
     * @param statusRequest
     *            the status request
     * @return the list
     */
    List<Customer> retrieveFilterCustomers(final Request<SearchCriteria> searchCriteriatRequest,
            final Request<List<CustomerStatusCodeList>> statusRequest);

	

}
