/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 304007
 * @version       : 0.1, Jan 30, 2013
 */
package com.cognizant.insurance.domain.product;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * The Class Type.
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.STRING)
@NamedQueries({@NamedQuery(name="type.getProductType",query="select p from ProductType p join fetch p.carrier carrier where carrier.id = :carrierCode "), 
			   @NamedQuery(name="type.getCategoryType",query="select c from CategoryType c join fetch c.carrier carrier where carrier.id = :carrierCode"),	
			   @NamedQuery(name="type.getLobType",query="select lob from LobType lob join fetch lob.carrier carrier where carrier.id = :carrierCode"),
			   @NamedQuery(name="type.getType",query="select t from Type t where (:carrierCode is null or t.carrier.id = :carrierCode)"),
			   @NamedQuery(name="type.getAllNonTypes",query="select t from Type t where t.type != :filterType and (:carrierCode is null or t.carrier.id = :carrierCode)"),
			   @NamedQuery(name="type.getTypeByFilter",query="select t from Type t where t.type = :filterType and (:carrierCode is null or t.carrier.id = :carrierCode)"),
			   @NamedQuery(name="type.getTypeById",query="select t from Type t where t.id = :id and (:carrierCode is null or t.carrier.id = :carrierCode)"),
			   @NamedQuery(name="type.multiLookupById",query="select t from Type t where t.id = :id and (:carrierCode is null or t.carrier.id = :carrierCode)"),
			   @NamedQuery(name="type.getTypeByIdAndType",query="select t from Type t where t.id = :id and t.type = :type and (:carrierCode is null or t.carrier.id = :carrierCode)")
			   })     
@Table(name = "CORE_PROD_CODE_TYPE_LOOK_UP")

public class Type implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6143475804546781147L;

    /** The id. */
    @Id
    @Column(name="ID")
    @GenericGenerator(name = "type_seq", strategy = "com.cognizant.insurance.domain.product.TypeIdGenerator")
    @GeneratedValue(generator = "type_seq")
    private String id;
    
    /** The description. */
    @Column(name="DESCRIPTION")
    private String description;

    /** The CODE_MAPPING_KEY. */
    @Column(name="CODE_MAPPING_KEY")
    private String codeMappingKey;   
    
    /** The type. */
    @Column(name="TYPE", insertable = false, updatable = false)
    private String type; 
    
    /** The carriers. */
    @OneToOne(cascade = { CascadeType.REFRESH })
    @JoinColumn(name="CARRIER_ID")
    private Carrier carrier;
    
    /** The parent type. */
    @OneToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name="PARENT_TYPE_ID")
    private Type parentType;

    /**
     * The Constructor.
     */
    public Type() {
        super();
    }
    
    /**
     * The Constructor.
     *
     * @param id the id
     */
    public Type(final String id) {
        this.id = id;
    }
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the description
     */
    public void setDescription(final String description) {
        this.description = description;
    }


    /**
	 * Gets the code mapping key.
	 *
	 * @return the codeMappingKey
	 */
	public String getCodeMappingKey() {
		return codeMappingKey;
	}

	/**
	 * Sets the code mapping key.
	 *
	 * @param codeMappingKey the codeMappingKey to set
	 */
	public void setCodeMappingKey(String codeMappingKey) {
		this.codeMappingKey = codeMappingKey;
	}

	/**
     * Sets The carrier.
     *
     * @param carrier The carrier to set.
     */
    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    /**
     * Gets the carrier.
     *
     * @return Returns the carrier.
     */
    public Carrier getCarrier() {
        return carrier;
    }

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the parent type.
	 *
	 * @return the parent type
	 */
	public Type getParentType() {
		return parentType;
	}

	/**
	 * Sets the parent type.
	 *
	 * @param parentType the new parent type
	 */
	public void setParentType(Type parentType) {
		this.parentType = parentType;
	}
}
