/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 30, 2013
 */
package com.cognizant.insurance.agent.request.vo;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.agent.generali.entity.GeneraliAgent;
import com.cognizant.insurance.agent.audit.LEDataWipeAudit;
import com.cognizant.insurance.agent.domain.agreement.financialservicesagreementsubtypes.InsuranceAgreement;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.AgreementHolder;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.AgreementProducer;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Appointee;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Beneficiary;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.Insured;
import com.cognizant.insurance.agent.domain.agreement.partyroleinagreement.PremiumPayer;
import com.cognizant.insurance.agent.domain.documentandcommunication.Requirement;
import com.cognizant.insurance.agent.domain.documentandcommunication.documentsubtypes.AgreementDocument;
import com.cognizant.insurance.agent.domain.finance.agreementloan.AgreementLoan;
import com.cognizant.insurance.agent.domain.finance.agreementloan.AgreementWithdrawal;
import com.cognizant.insurance.agent.domain.goalandneed.GoalAndNeed;
import com.cognizant.insurance.agent.domain.roleandrelationship.partyroleinrelationshipsubtypes.Customer;
//import com.cognizant.insurance.agent.eMail.LifeEngageEmail;
//import com.cognizant.insurance.agent.pushnotification.DeviceRegister;
//import com.cognizant.insurance.agent.pushnotification.SNSDetails;
//import com.cognizant.insurance.agent.response.ResponseCount;
//import com.cognizant.insurance.searchcriteria.SearchCriteriaResponse;

/**
 * The Class class Transactions.
 * 
 * @author 345166
 * 
 *         Class Transactions - Created for EApp. Used to map RequestPayload details.
 */
public class Transactions {

    /** The customer id. */
    private String customerId;

    /** The agent id. Key 11 */
    private String agentId;

    /** The product id. */
    private String productId;

    /** The creation date time. Key 13 */
    private Date creationDateTime;

    /** The modified time stamp. Key 14 */
    private Date modifiedTimeStamp;

    /** The type. */
    private String type;

    /** The proposal number. */
    private String proposalNumber;

    /** The proposal status. */
    private String status;

    /** The trans tracking id. */
    private String transTrackingId;

    /** The insured. */
    private Insured insured;

    /** The proposer. */
    private AgreementHolder proposer;

    /** The beneficiaries. */
    private Set<Beneficiary> beneficiaries;

    /** The appointee. */
    private Appointee appointee;

    /** The payer. */
    private PremiumPayer payer;

    /** The insured previous policy list. */
    private Set<Insured> insuredPreviousPolicyList;
    
    /** The insured previous policy list. */
    private Set<Insured> additionalInsuredes;

    //private LifeEngageEmail lifeEngageEmail;
    
    /** The Mode. */
    private String mode;

    /** The proposal. */
    private InsuranceAgreement proposal;

    /** The STP status. */
    private String STPStatus;

    /** The Validation results. */
    private String validationResults;

    /** The e app. */
    private Map<String, Object> eapp;

    /** The status data. */
    private StatusData statusData;

    /** key 1. */
    private String key1;

    /** key 2. */
    private String key2;

    /** The key 3. */
    private String key3;

    /** The key 4. */
    private String key4;

    /** key 5. */
    private String key5;

    /** key 6. */
    private String key6;

    /** The key 7. */
    private String key7;

    /** The key 8. */
    private String key8;

    /** The FnaID. */
    private String fnaId;

    /** The IllustrationID. */
    private String illustrationId;

    /** The RetrieveType. */
    private String retrieveType;

    /** The Last Sync Date. */
    private Date lastSyncDate;

    /** The Sync Status. */
    private String syncStatus;

    /** The Sync Error. */
    private String syncError;

    /** The leadFirstName. */
    private String leadFirstName;

    /** The leadLastName. */
    private String leadLastName;

    /** The contactNumber. */
    private String contactNumber;

    /** The source. */
    private String source;

    /** The subSource. */
    private String subSource;

    /** The dateOfBirth. */
    private Date dateOfBirth;

    /** The Key18. */
    private String key18;

    /** The Key19. */
    private String key19;

    /** The Key20. */
    private String key20;

    /** The Key21. */
    private String key21;

    /** The Key22. */
    private String key22;

    /** The Key23. */
    private String key23;

    /** The Key24. */
    private String key24;

    /** The Key25. */
    private String key25;

    /** The offlineIdentifier. */
    private String offlineIdentifier;

    /** The previous agreements. */
    private Set<InsuranceAgreement> previousAgreements;

    /** The agreement loan. */
    private AgreementLoan agreementLoan;

    /** The agreement loans. */
    private Set<AgreementLoan> agreementLoans;

    /** The agreement withdrawals. */
    private Set<AgreementWithdrawal> agreementWithdrawals;

    /** The document. */
    private AgreementDocument document;

    /** The GoalAndNeed. */
    private GoalAndNeed goalAndNeed;

    //** The customer. *//*
    private Customer customer;

    /** The GoalAndNeed Basic Dtls String to retrieve/map only the basic details. */
    private String goalAndNeedBasicDtls;

    /** The rule output. */
    private String ruleOutput;

    /** The fna Count. */
    private String fnaCount;

    /** The illustration Count. */
    private String illustrationCount;

    /** The eApp Count. */
    private String eAppCount;

    /** The search criteria response. */
    //private SearchCriteriaResponse searchCriteriaResponse;

    /** The response count of each modes. */
   // private ResponseCount responseCount;

    /** The agent. */
    private GeneraliAgent agent;
    
    /** The agent. */
    private GeneraliTHUser generaliThUser;

	/** The requirements */
    private Set<Requirement> requirements;
    
    /** The agent */
    private AgreementProducer agreementProducer;
    
    /** The Key26. */
    private String key26;
    
    /** The Key27. */
    private String key27;
    
    /** The Key28. */
    private String key28;
    
    /** The Key29. */
    private String key29;
    
    /** The Key30. */
    private String key30;
    
    /** The device register. *//*
    private DeviceRegister deviceRegister;

    *//** The sns services. *//*
    private SNSDetails snsServices;*/
    
    /** The encryption key. */
    private String encryptionKey;
   
    /**Added for generali Vietnam
    
    /** The branch id. */
    private String branchId;
    
    private String rmIdentityProof;
      
    private String allocationType;
    
    /*To use only for additional insured pdf generation*/
    //private Insured additionalInsured;
    
    /** The data wipe audit. */
    private LEDataWipeAudit dataWipeAudit;
    
    /** The user ids. */
    private List<String> userIds;
	
	/** The business creation date. */
    private Date businessCreationDate; 
    
    /** The business modified date. */
    private Date businessModifiedDate;
    
    /** The isUnlockedByAgent. */
    private String isUnlockedByAgent;
    
    /**End**/

    
    public Set<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(Set<Requirement> requirements) {
        this.requirements = requirements;
    }

    /**
     * Gets the fna count.
     * 
     * @return the fna count
     */
    public String getFnaCount() {
        return fnaCount;
    }

    /**
     * Sets the fna count.
     * 
     * @param fnaCount
     *            the fna count to set.
     */
    public void setFnaCount(String fnaCount) {
        this.fnaCount = fnaCount;
    }

    /**
     * Gets the illustration count.
     * 
     * @return the illustration count
     */
    public String getIllustrationCount() {
        return illustrationCount;
    }

    /**
     * Sets the illustration count.
     * 
     * @param illustrationCount
     *            the illustration count to set.
     */
    public void setIllustrationCount(String illustrationCount) {
        this.illustrationCount = illustrationCount;
    }

    /**
     * Gets the e app count.
     * 
     * @return the e app count
     */
    public String geteAppCount() {
        return eAppCount;
    }

    /**
     * Sets the e app count.
     * 
     * @param eAppCount
     *            the e app count to set.
     */
    public void seteAppCount(String eAppCount) {
        this.eAppCount = eAppCount;
    }

    /**
     * Gets the response count.
     * 
     * @return the response count
     */
    /*public ResponseCount getResponseCount() {
        return responseCount;
    }*/

    /**
     * Sets the response count.
     * 
     * @param responseCount
     *            the response count to set.
     */
    /*public void setResponseCount(ResponseCount responseCount) {
        this.responseCount = responseCount;
    }*/

    /**
     * Gets the customer id.
     * 
     * @return the customer id
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customer id.
     * 
     * @param customerId
     *            the new customer id
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the agent id.
     * 
     * @return the agent id
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * Sets the agent id.
     * 
     * @param agentId
     *            the new agent id
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * Gets the product id.
     * 
     * @return the product id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the product id.
     * 
     * @param productId
     *            the new product id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Gets the creation date time.
     * 
     * @return the creation date time
     */
    public Date getCreationDateTime() {
        return creationDateTime;
    }

    /**
     * Sets the creation date time.
     * 
     * @param creationDateTime
     *            the new creation date time
     */
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    /**
     * Gets the modified time stamp.
     * 
     * @return the modified time stamp
     */
    public Date getModifiedTimeStamp() {
        return modifiedTimeStamp;
    }

    /**
     * Sets the modified time stamp.
     * 
     * @param modifiedTimeStamp
     *            the new modified time stamp
     */
    public void setModifiedTimeStamp(Date modifiedTimeStamp) {
        this.modifiedTimeStamp = modifiedTimeStamp;
    }

    /**
     * Gets the type.
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     * 
     * @param type
     *            the new type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the proposal number.
     * 
     * @return the proposal number
     */
    public String getProposalNumber() {
        return proposalNumber;
    }

    /**
     * Sets the proposal number.
     * 
     * @param proposalNumber
     *            the new proposal number
     */
    public void setProposalNumber(String proposalNumber) {
        this.proposalNumber = proposalNumber;
    }

    /**
     * Gets the proposal status.
     * 
     * @return the proposal status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the proposal status.
     * 
     * @param status
     *            the status to set.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets the trans tracking id.
     * 
     * @return the trans tracking id
     */
    public String getTransTrackingId() {
        return transTrackingId;
    }

    /**
     * Sets the trans tracking id.
     * 
     * @param transTrackingId
     *            the new trans tracking id
     */
    public void setTransTrackingId(String transTrackingId) {
        this.transTrackingId = transTrackingId;
    }

    /**
     * Gets the status data.
     * 
     * @return the statusData
     */
    public StatusData getStatusData() {
        return statusData;
    }

    /**
     * Sets the status data.
     * 
     * @param statusData
     *            the statusData to set
     */
    public void setStatusData(StatusData statusData) {
        this.statusData = statusData;
    }

    /**
     * Gets the key5.
     * 
     * @return the key5
     */
    public String getKey5() {
        return key5;
    }

    /**
     * Sets the key5.
     * 
     * @param key5
     *            the key5 to set
     */
    public void setKey5(String key5) {
        this.key5 = key5;
    }

    /**
     * Gets the key6.
     * 
     * @return the key6
     */
    public String getKey6() {
        return key6;
    }

    /**
     * Sets the key6.
     * 
     * @param key6
     *            the key6 to set
     */
    public void setKey6(String key6) {
        this.key6 = key6;
    }

    /**
     * Gets the key7.
     * 
     * @return the key7
     */
    public String getKey7() {
        return key7;
    }

    /**
     * Sets the key7.
     * 
     * @param key7
     *            the key7 to set
     */
    public void setKey7(String key7) {
        this.key7 = key7;
    }

    /**
     * Gets the eapp.
     * 
     * @return the eapp
     */
    public Map<String, Object> getEapp() {
        return eapp;
    }

    /**
     * Sets the eapp.
     * 
     * @param eapp
     *            the eapp
     */
    public void setEapp(Map<String, Object> eapp) {
        this.eapp = eapp;
    }

    /**
     * Gets the offlineIdentifier.
     * 
     * @return the offlineIdentifier
     */
    public String getOfflineIdentifier() {
        return offlineIdentifier;
    }

    /**
     * Sets the offlineIdentifier.
     * 
     * @param offlineIdentifier
     *            the offlineIdentifier to set
     */
    public void setOfflineIdentifier(String offlineIdentifier) {
        this.offlineIdentifier = offlineIdentifier;
    }

    /**
     * Gets the insured.
     * 
     * @return the insured
     */
    public Insured getInsured() {
        return insured;
    }

    /**
     * Sets the insured.
     * 
     * @param insured
     *            the insured to set
     */
    public void setInsured(Insured insured) {
        this.insured = insured;
    }

    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode
     *            the mode to set.
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the sTP status.
     * 
     * @return the sTP status
     */
    public String getSTPStatus() {
        return STPStatus;
    }

    /**
     * Sets the sTP status.
     * 
     * @param sTPStatus
     *            the sTP status to set.
     */
    public void setSTPStatus(String sTPStatus) {
        STPStatus = sTPStatus;
    }

    /**
     * Gets the validation results.
     * 
     * @return the validation results
     */
    public String getValidationResults() {
        return validationResults;
    }

    /**
     * Sets the validation results.
     * 
     * @param validationResults
     *            the validation results to set.
     */
    public void setValidationResults(String validationResults) {
        this.validationResults = validationResults;
    }

    /**
     * Gets the proposer.
     * 
     * @return the proposer
     */
    public AgreementHolder getProposer() {
        return proposer;
    }

    /**
     * Sets the proposer.
     * 
     * @param proposer
     *            the proposer to set
     */
    public void setProposer(AgreementHolder proposer) {
        this.proposer = proposer;
    }

    /**
     * Gets the beneficiaries.
     * 
     * @return the beneficiaries
     */
    public Set<Beneficiary> getBeneficiaries() {
        return beneficiaries;
    }

    /**
     * Sets the beneficiaries.
     * 
     * @param beneficiaries
     *            the beneficiaries to set
     */
    public void setBeneficiaries(Set<Beneficiary> beneficiaries) {
        this.beneficiaries = beneficiaries;
    }

    /**
     * Gets the appointee.
     * 
     * @return the appointee
     */
    public Appointee getAppointee() {
        return appointee;
    }

    /**
     * Sets the appointee.
     * 
     * @param appointee
     *            the appointee to set
     */
    public void setAppointee(Appointee appointee) {
        this.appointee = appointee;
    }

    /**
     * Gets the payer.
     * 
     * @return the payer
     */
    public PremiumPayer getPayer() {
        return payer;
    }

    /**
     * Sets the payer.
     * 
     * @param payer
     *            the payer to set
     */
    public void setPayer(PremiumPayer payer) {
        this.payer = payer;
    }

    /**
     * Gets the insured previous policy list.
     * 
     * @return the insuredPreviousPolicyList
     */
    public Set<Insured> getInsuredPreviousPolicyList() {
        return insuredPreviousPolicyList;
    }

    /**
     * Sets the insured previous policy list.
     * 
     * @param insuredPreviousPolicyList
     *            the insuredPreviousPolicyList to set
     */
    public void setInsuredPreviousPolicyList(Set<Insured> insuredPreviousPolicyList) {
        this.insuredPreviousPolicyList = insuredPreviousPolicyList;
    }

    /**
     * Gets the proposal.
     * 
     * @return the proposal
     */
    public InsuranceAgreement getProposal() {
        return proposal;
    }

    /**
     * Sets the proposal.
     * 
     * @param proposal
     *            the proposal to set
     */
    public void setProposal(InsuranceAgreement proposal) {
        this.proposal = proposal;
    }

    /**
     * Sets the previous agreements.
     * 
     * @param previousAgreements
     *            the previousAgreements to set
     */
    public void setPreviousAgreements(Set<InsuranceAgreement> previousAgreements) {
        this.previousAgreements = previousAgreements;
    }

    /**
     * Gets the previous agreements.
     * 
     * @return the previousAgreements
     *//*
    public Set<InsuranceAgreement> getPreviousAgreements() {
        return previousAgreements;
    }

    *//**
     * Gets the agreement loan.
     * 
     * @return the agreementLoan
     *//*
    public AgreementLoan getAgreementLoan() {
        return agreementLoan;
    }

    *//**
     * Sets the agreement loan.
     * 
     * @param agreementLoan
     *            the agreementLoan to set
     *//*
    public void setAgreementLoan(AgreementLoan agreementLoan) {
        this.agreementLoan = agreementLoan;
    }

    *//**
     * Sets the agreement withdrawals.
     * 
     * @param agreementWithdrawals
     *            the agreementWithdrawals to set
     *//*
    public void setAgreementWithdrawals(Set<AgreementWithdrawal> agreementWithdrawals) {
        this.agreementWithdrawals = agreementWithdrawals;
    }

    *//**
     * Gets the agreement withdrawals.
     * 
     * @return the agreementWithdrawals
     *//*
    public Set<AgreementWithdrawal> getAgreementWithdrawals() {
        return agreementWithdrawals;
    }

    *//**
     * Gets the agreement loans.
     * 
     * @return the agreementLoans
     *//*
    public Set<AgreementLoan> getAgreementLoans() {
        return agreementLoans;
    }

    *//**
     * Sets the agreement loans.
     * 
     * @param agreementLoans
     *            the agreementLoans to set
     *//*
    public void setAgreementLoans(Set<AgreementLoan> agreementLoans) {
        this.agreementLoans = agreementLoans;
    }

    *//**
     * Gets the document.
     * 
     * @return the document
     *//*
    public AgreementDocument getDocument() {
        return document;
    }

    *//**
     * Sets the document.
     * 
     * @param document
     *            the document to set
     *//*
    public void setDocument(AgreementDocument document) {
        this.document = document;
    }

    *//**
     * Gets the goalAndNeed.
     * 
     * @return Returns the goalAndNeed.
     *//*
    public GoalAndNeed getGoalAndNeed() {
        return goalAndNeed;
    }

    *//**
     * Sets The goalAndNeed.
     * 
     * @param goalAndNeed
     *            The goalAndNeed to set.
     *//*
    public void setGoalAndNeed(GoalAndNeed goalAndNeed) {
        this.goalAndNeed = goalAndNeed;
    }*/

    /**
     * Gets the goal and need basic dtls.
     * 
     * @return the goalAndNeedBasicDtls
     */
    public String getGoalAndNeedBasicDtls() {
        return goalAndNeedBasicDtls;
    }

    /**
     * Sets the goal and need basic dtls.
     * 
     * @param goalAndNeedBasicDtls
     *            the goalAndNeedBasicDtls to set
     */
    public void setGoalAndNeedBasicDtls(String goalAndNeedBasicDtls) {
        this.goalAndNeedBasicDtls = goalAndNeedBasicDtls;
    }

    /**
     * Gets the fna id.
     * 
     * @return the fna id
     */
    public String getFnaId() {
        return fnaId;
    }

    /**
     * Sets the fna id.
     * 
     * @param fnaId
     *            the fna id to set.
     */
    public void setFnaId(String fnaId) {
        this.fnaId = fnaId;
    }

    /**
     * Gets the illustration id.
     * 
     * @return the illustration id
     */
    public String getIllustrationId() {
        return illustrationId;
    }

    /**
     * Sets the illustration id.
     * 
     * @param illustrationId
     *            the illustration id to set.
     */
    public void setIllustrationId(String illustrationId) {
        this.illustrationId = illustrationId;
    }

    /**
     * Gets the key8.
     * 
     * @return the key8
     */
    public String getKey8() {
        return key8;
    }

    /**
     * Sets the key8.
     * 
     * @param key8
     *            the key8 to set.
     */
    public void setKey8(String key8) {
        this.key8 = key8;
    }

    /**
     * Gets the retrieve type.
     * 
     * @return the retrieve type
     */
    public String getRetrieveType() {
        return retrieveType;
    }

    /**
     * Sets the retrieve type.
     * 
     * @param retrieveType
     *            the retrieve type to set.
     */
    public void setRetrieveType(String retrieveType) {
        this.retrieveType = retrieveType;
    }

    /**
     * Gets the last sync date.
     * 
     * @return the last sync date
     */
    public Date getLastSyncDate() {
        return lastSyncDate;
    }

    /**
     * Sets the last sync date.
     * 
     * @param lastSyncDate
     *            the last sync date to set.
     */
    public void setLastSyncDate(Date lastSyncDate) {
        this.lastSyncDate = lastSyncDate;
    }

    /**
     * Gets the sync status.
     * 
     * @return the sync status
     */
    public String getSyncStatus() {
        return syncStatus;
    }

    /**
     * Sets the sync status.
     * 
     * @param syncStatus
     *            the sync status to set.
     */
    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    /**
     * Gets the sync error.
     * 
     * @return the sync error
     */
    public String getSyncError() {
        return syncError;
    }

    /**
     * Sets the sync error.
     * 
     * @param syncError
     *            the sync error to set.
     */
    public void setSyncError(String syncError) {
        this.syncError = syncError;
    }

    /**
     * Gets the key18.
     * 
     * @return the key18
     */
    public String getKey18() {
        return key18;
    }

    /**
     * Sets the key18.
     * 
     * @param key18
     *            the key18 to set.
     */
    public void setKey18(String key18) {
        this.key18 = key18;
    }

    /**
     * Gets the key19.
     * 
     * @return the key19
     */
    public String getKey19() {
        return key19;
    }

    /**
     * Sets the key19.
     * 
     * @param key19
     *            the key19 to set.
     */
    public void setKey19(String key19) {
        this.key19 = key19;
    }

    /**
     * Gets the key20.
     * 
     * @return the key20
     */
    public String getKey20() {
        return key20;
    }

    /**
     * Sets the key20.
     * 
     * @param key20
     *            the key20 to set.
     */
    public void setKey20(String key20) {
        this.key20 = key20;
    }

    /**
     * Gets the key21.
     * 
     * @return the key21
     */
    public String getKey21() {
        return key21;
    }

    /**
     * Sets the key21.
     * 
     * @param key21
     *            the key21 to set.
     */
    public void setKey21(String key21) {
        this.key21 = key21;
    }

    /**
     * Gets the key23.
     * 
     * @return the key23
     */
    public String getKey23() {
        return key23;
    }

    /**
     * Sets the key23.
     * 
     * @param key23
     *            the key23 to set.
     */
    public void setKey23(String key23) {
        this.key23 = key23;
    }

    /**
     * Gets the key24.
     * 
     * @return the key24
     */
    public String getKey24() {
        return key24;
    }

    /**
     * Sets the key24.
     * 
     * @param key24
     *            the key24 to set.
     */
    public void setKey24(String key24) {
        this.key24 = key24;
    }

    /**
     * Gets the key25.
     * 
     * @return the key25
     */
    public String getKey25() {
        return key25;
    }

    /**
     * Sets the key25.
     * 
     * @param key25
     *            the key25 to set.
     */
    public void setKey25(String key25) {
        this.key25 = key25;
    }

    /**
     * Sets The lifeEngageEmail.
     *
     * @param lifeEngageEmail The lifeEngageEmail to set.
     *//*
    public void setLifeEngageEmail(LifeEngageEmail lifeEngageEmail) {
        this.lifeEngageEmail = lifeEngageEmail;
    }*/
    
    /**
     * Gets the lead first name.
     * 
     * @return the lead first name
     */
    public String getLeadFirstName() {
        return leadFirstName;
    }

    /**
     * Sets the lead first name.
     * 
     * @param leadFirstName
     *            the lead first name to set.
     */
    public void setLeadFirstName(String leadFirstName) {
        this.leadFirstName = leadFirstName;
    }

    /**
     * Gets the lead last name.
     * 
     * @return the lead last name
     */
    public String getLeadLastName() {
        return leadLastName;
    }

    /**
     * Sets the lead last name.
     * 
     * @param leadLastName
     *            the lead last name to set.
     */
    public void setLeadLastName(String leadLastName) {
        this.leadLastName = leadLastName;
    }

    /**
     * Gets the contact number.
     * 
     * @return the contact number
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     * Sets the contact number.
     * 
     * @param contactNumber
     *            the contact number to set.
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    /**
     * Gets the source.
     * 
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the source to set.
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Gets the sub source.
     * 
     * @return the sub source
     */
    public String getSubSource() {
        return subSource;
    }

    /**
     * Sets the sub source.
     * 
     * @param subSource
     *            the sub source to set.
     */
    public void setSubSource(String subSource) {
        this.subSource = subSource;
    }

    /**
     * Gets the date of birth.
     * 
     * @return the date of birth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the date of birth.
     * 
     * @param dateOfBirth
     *            the date of birth to set.
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Gets the key22.
     * 
     * @return the key22
     */
    public String getKey22() {
        return key22;
    }

    /**
     * Sets the key22.
     * 
     * @param key22
     *            the key22 to set.
     */
    public void setKey22(String key22) {
        this.key22 = key22;
    }

    /**
     * Gets the customer.
     * 
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the customer.
     * 
     * @param customer
     *            the customer to set
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Gets the key1.
     * 
     * @return the key1
     */
    public String getKey1() {
        return key1;
    }

    /**
     * Sets the key1.
     * 
     * @param key1
     *            the key1 to set
     */
    public void setKey1(String key1) {
        this.key1 = key1;
    }

    /**
     * Gets the key2.
     * 
     * @return the key2
     */
    public String getKey2() {
        return key2;
    }

    /**
     * Sets the key2.
     * 
     * @param key2
     *            the key2 to set
     */
    public void setKey2(String key2) {
        this.key2 = key2;
    }

    /**
     * Gets the key3.
     * 
     * @return the key3
     */
    public String getKey3() {
        return key3;
    }

    /**
     * Sets the key3.
     * 
     * @param key3
     *            the key3 to set
     */
    public void setKey3(String key3) {
        this.key3 = key3;
    }

    /**
     * Gets the key4.
     * 
     * @return the key4
     */
    public String getKey4() {
        return key4;
    }

    /**
     * Sets the key4.
     * 
     * @param key4
     *            the key4 to set
     */
    public void setKey4(String key4) {
        this.key4 = key4;
    }

    /**
     * Sets the rule output.
     * 
     * @param ruleOutput
     *            the new rule output
     */
    public void setRuleOutput(String ruleOutput) {
        this.ruleOutput = ruleOutput;
    }

    /**
     * Gets the rule output.
     * 
     * @return the rule output
     */
    public String getRuleOutput() {
        return ruleOutput;
    }

    /**
     * Gets the search criteria response.
     * 
     * @return the search criteria response
     *//*
    public SearchCriteriaResponse getSearchCriteriaResponse() {
        return searchCriteriaResponse;
    }

    *//**
     * Sets the search criteria response.
     * 
     * @param searchCriteriaResponse
     *            the search criteria response to set.
     *//*
    public void setSearchCriteriaResponse(SearchCriteriaResponse searchCriteriaResponse) {
        this.searchCriteriaResponse = searchCriteriaResponse;
    }*/
	
	
	/*public void setAdditionalInsuredes(Set<Insured> additionalInsuredes) {
		this.additionalInsuredes = additionalInsuredes;
	}

	public Set<Insured> getAdditionalInsuredes() {
		return additionalInsuredes;
	}*/
	   /**
     * Gets the agent.
     * 
     * @return the agent
     */
    public GeneraliAgent getAgent() {
        return agent;
    }

    /**
     * Sets the agent.
     * 
     * @param agent
     *            the agent to set.
     */
    public void setAgent(GeneraliAgent agent) {
        this.agent = agent;
    }

	/*public void setAgreementProducer(AgreementProducer agreementProducer) {
		this.agreementProducer = agreementProducer;
	}

	public AgreementProducer getAgreementProducer() {
		return agreementProducer;
	}*/

	public GeneraliTHUser getGeneraliThUser() {
		return generaliThUser;
	}

	public void setGeneraliThUser(GeneraliTHUser generaliThUser) {
		this.generaliThUser = generaliThUser;
	}

	public void setKey26(String key26) {
		this.key26 = key26;
	}

	public String getKey26() {
		return key26;
	}

	public void setKey27(String key27) {
		this.key27 = key27;
	}

	public String getKey27() {
		return key27;
	}

	public void setKey28(String key28) {
		this.key28 = key28;
	}

	public String getKey28() {
		return key28;
	}

	public void setKey29(String key29) {
		this.key29 = key29;
	}

	public String getKey29() {
		return key29;
	}

	public void setKey30(String key30) {
		this.key30 = key30;
	}

	public String getKey30() {
		return key30;
	}

	/*public void setDeviceRegister(DeviceRegister deviceRegister) {
		this.deviceRegister = deviceRegister;
	}

	public DeviceRegister getDeviceRegister() {
		return deviceRegister;
	}

	public void setSnsServices(SNSDetails snsServices) {
		this.snsServices = snsServices;
	}

	public SNSDetails getSnsServices() {
		return snsServices;
	}*/
	
	/**
	 * @return the encryptionKey
	 */
	public String getEncryptionKey() {
		return encryptionKey;
	}

	/**
	 * @param encryptionKey the encryptionKey to set
	 */
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getBranchId() {
		return branchId;
	}

	public String getRmIdentityProof() {
		return rmIdentityProof;
	}

	public void setRmIdentityProof(String rmIdentityProof) {
		this.rmIdentityProof = rmIdentityProof;
	}

	/**
	 * @param allocationType the allocationType to set
	 */
	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}

	/**
	 * @return the allocationType
	 */
	public String getAllocationType() {
		return allocationType;
	}

	/*public Insured getAdditionalInsured() {
		return additionalInsured;
	}

	public void setAdditionalInsured(Insured additionalInsured) {
		this.additionalInsured = additionalInsured;
	}*/
	/* Added for LE_datawipe functionality*/
	public LEDataWipeAudit getDataWipeAudit() {
		return dataWipeAudit;
	}

	public void setDataWipeAudit(LEDataWipeAudit dataWipeAudit) {
		this.dataWipeAudit = dataWipeAudit;
	}

	public List<String> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}
	
	
	public Date getBusinessCreationDate() {
		return businessCreationDate;
	}

	public void setBusinessCreationDate(Date businessCreationDate) {
		this.businessCreationDate = businessCreationDate;
	}

	public Date getBusinessModifiedDate() {
		return businessModifiedDate;
	}

	public void setBusinessModifiedDate(Date businessModifiedDate) {
		this.businessModifiedDate = businessModifiedDate;
	}
	
	/**
	 * Gets the checks if is unlocked by agent.
	 *
	 * @return the checks if is unlocked by agent
	 */
	public String getIsUnlockedByAgent() {
		return isUnlockedByAgent;
	}

	/**
	 * Sets the checks if is unlocked by agent.
	 *
	 * @param isUnlockedByAgent the new checks if is unlocked by agent
	 */
	public void setIsUnlockedByAgent(String isUnlockedByAgent) {
		this.isUnlockedByAgent = isUnlockedByAgent;
	}

	public AgreementProducer getAgreementProducer() {
		return agreementProducer;
	}

	public void setAgreementProducer(AgreementProducer agreementProducer) {
		this.agreementProducer = agreementProducer;
	}
	

}
