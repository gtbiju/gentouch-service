/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 331701
 * @version       : 0.1, Feb 25, 2013
 */
package com.cognizant.insurance.domain.commonelements.commonclasses;

import javax.persistence.Entity;

import com.cognizant.insurance.domain.commonelements.InformationModelObject;

/**
 * The Class FamilyHealthHistory.
 * Newly added for EApp - 301350, 01Aug2013. Not part of ACORD. 
 */

@Entity
public class FamilyHealthHistory extends InformationModelObject {
   
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8254159605449610889L;

    /** The family member. */
    private String familyMember;
    
    /** The present age if alive. */
    private Integer presentAgeIfAlive;
    
    /** The age at death. */
    private Integer ageAtDeath;
    
    /** The cause of death or illness. */
    private String causeOfDeathOrIllness;
    
    private Integer orderId;


    /**
     * Gets the familyMember.
     *
     * @return Returns the familyMember.
     */
    public final String getFamilyMember() {
        return familyMember;
    }

    /**
     * Gets the presentAgeIfAlive.
     *
     * @return Returns the presentAgeIfAlive.
     */
    public final Integer getPresentAgeIfAlive() {
        return presentAgeIfAlive;
    }

    /**
     * Gets the ageAtDeath.
     *
     * @return Returns the ageAtDeath.
     */
    public final Integer getAgeAtDeath() {
        return ageAtDeath;
    }

    /**
     * Gets the causeOfDeathOrIllness.
     *
     * @return Returns the causeOfDeathOrIllness.
     */
    public final String getCauseOfDeathOrIllness() {
        return causeOfDeathOrIllness;
    }

    /**
     * Sets The familyMember.
     *
     * @param familyMember The familyMember to set.
     */
    public final void setFamilyMember(String familyMember) {
        this.familyMember = familyMember;
    }

    /**
     * Sets The presentAgeIfAlive.
     *
     * @param presentAgeIfAlive The presentAgeIfAlive to set.
     */
    public final void setPresentAgeIfAlive(Integer presentAgeIfAlive) {
        this.presentAgeIfAlive = presentAgeIfAlive;
    }

    /**
     * Sets The ageAtDeath.
     *
     * @param ageAtDeath The ageAtDeath to set.
     */
    public final void setAgeAtDeath(Integer ageAtDeath) {
        this.ageAtDeath = ageAtDeath;
    }

    /**
     * Sets The causeOfDeathOrIllness.
     *
     * @param causeOfDeathOrIllness The causeOfDeathOrIllness to set.
     */
    public final void setCauseOfDeathOrIllness(String causeOfDeathOrIllness) {
        this.causeOfDeathOrIllness = causeOfDeathOrIllness;
    }

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	
    	
    
    
    }
