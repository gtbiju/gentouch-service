package com.cognizant.insurance.omnichannel.lifeasia.searchcriteria;

import java.util.Date;
import java.util.List;




public class LifeAsiaSearchCriteria {

	private String spajNo;
	private List<String> statuses;
	private Date fromDate;
	private int limit=-1;
	private String documentId;
	private String eAppId;

	public LifeAsiaSearchCriteria() {

	}

	public LifeAsiaSearchCriteria(String spajNo, List<String> statuses, Date fromDate,String documentId,
			int limit) {

		this.spajNo = spajNo;
		this.statuses = statuses;
		this.fromDate = fromDate;
		this.limit = limit;
		this.documentId=documentId;
	}
	

	public String geteAppId() {
		return eAppId;
	}

	public void seteAppId(String eAppId) {
		this.eAppId = eAppId;
	}

	public String getSpajNo() {
		return spajNo;
	}

	public void setSpajNo(String spajNo) {
		this.spajNo = spajNo;
	}
	public List<String> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	
}
