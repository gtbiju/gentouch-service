package com.cognizant.generali.security;

import javax.servlet.http.HttpServletResponse;

import com.cognizant.le.security.constants.ExceptionCodes;

public final class GeneraliAuthResponse extends ExceptionCodes{
	
	/*
     * Auth SUCCESS
     */

    public static final int SUCCESS = HttpServletResponse.SC_OK;
	
	/*
     * USER_SUSPENDED 
     */
   
    public static final int USER_SUSPENDED = 220;
    
    public static final int USER_NOT_FOUND = 471;
    
    /*
     * USER_TERMINATED 
     */

    public static final int USER_TERMINATED = 480;
    /*
     * USER_LICENCE Expired 
     */

    public static final int USER_LICENCE_EXPD = 481;
    
    /*
     * USER_UNAUTHORIZED
     */
    public static final int USER_UNAUTHORIZED = 482;
    
	

}
