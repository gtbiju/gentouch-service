/**
 * 
 */
package com.cognizant.insurance.omnichannel.service;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * @author 397850
 *
 */
public interface CheckinService {
	
	public String checkinAgentDetails(String requestJson) throws BusinessException;

	public String retrieveDetails(String json) throws BusinessException;

}
