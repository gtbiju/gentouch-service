package com.cognizant.insurance.omnichannel.domain;



import java.io.Serializable;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.insurance.omnichannel.GeneraliConstants;

// TODO: Auto-generated Javadoc
/**
 * @author 444703
 * The Class SPAJGenerator.
 * MAXIMUM Possible value for the Prefix Will be 'ZZZZZZZ'
 */
public class SPAJGenerator implements IdentifierGenerator,Configurable {
	
	/** The prefix. */
	private String prefix;
	
	/** The jump on. */
	private long jumpOn;
	
	/** The increment by. */
	private int incrementBy;
	
	/** The table name. */
	private String tableName;
	 
 	/** The has table. */
 	boolean hasTable=false;
	 
 	/** The seq id. */
 	private String seqId;
	 
 	/** The mask length. */
 	private int maskLength;
 	
 	private static final Logger LOGGER = LoggerFactory
	.getLogger(SPAJGenerator.class);

	/* (non-Javadoc)
	 * @see org.hibernate.id.Configurable#configure(org.hibernate.type.Type, java.util.Properties, org.hibernate.dialect.Dialect)
	 */
	@Override
	public void configure(Type type, Properties params, Dialect d)
			throws MappingException {
		prefix=params.getProperty(CONFIG_PREFIX);
		String _jumpOn=params.getProperty(CONFIG_JUMP_ON,"-1");
		jumpOn=Long.parseLong(_jumpOn);
		incrementBy= Integer.parseInt(params.getProperty(CONFIG_INCREMENT_BY,"1"));
		tableName=params.getProperty(CONFIG_TABLE_NAME);
		seqId=params.getProperty(CONFIG_SEQ_ID);
		maskLength= Integer.parseInt(params.getProperty(CONFIG_MASK_LENGTH,_jumpOn.length()+""));
	}

	/* (non-Javadoc)
	 * @see org.hibernate.id.IdentifierGenerator#generate(org.hibernate.engine.spi.SessionImplementor, java.lang.Object)
	 */
	@Override
	public Serializable generate(SessionImplementor session, Object object)
			throws HibernateException {
 		long currentId=0;
		if(prefix==null){
			prefix="";
		}
		Calendar todayCal = Calendar.getInstance();
		String dateString = new Integer(todayCal.get(1)+543).toString();
		String prefixWithYear =  dateString.substring(dateString.length()-2);
		prefix = GeneraliConstants.SALES_AGENCY_CODE + prefixWithYear; 
	    Connection connection = session.connection();
	    Statement statement=null;
	   
	    try {
	    	 statement=connection.createStatement();
	    	if(!hasTable){
	    		DatabaseMetaData meta = connection.getMetaData();
		    	ResultSet res = meta.getTables(null, null, tableName, 
		    		     new String[] {"TABLE"});
		    	 while (res.next()) {
		    	    String _tableName=res.getString("TABLE_NAME");
		    	       if(tableName.equals(_tableName)){
		    	    	   hasTable=true;
		    	    	   break;
		    	       }
		    	  }
		    	 if(!hasTable){
		    		 initMetatable(connection);
		 			insertMetaTble(connection, prefix, 0);
		 			connection.commit();
		    	 }
		    	 
	    	}
	    	 
	       
	       // String query= "CREATE TABLE ["+tableName+"]([prefix] [nvarchar](50) NULL,[id] [numeric](18, 0) NULL)";
	        String query= "Select  * from "+tableName +"  where seqId='"+seqId+"'";
	        ResultSet rs=statement.executeQuery(query);
	        
	        if(rs.next())
	        {
	            currentId=rs.getInt(3);
	            prefix=rs.getString(2); 
	            rs.close();
	            
	            if(prefix.length()>3 && !prefix.equalsIgnoreCase(null)) {
		        	char digit1 = prefix.charAt(2);
			        char digit2 = prefix.charAt(3);
			        String yearofId = Character.toString(digit1) + Character.toString(digit2);
			        if(!yearofId.equalsIgnoreCase(prefixWithYear)) {
			        	currentId=0;
			        	prefix = GeneraliConstants.SALES_AGENCY_CODE + prefixWithYear;
			        }
		        }
	             
	        }else{
	        	insertMetaTble(connection, prefix, 0);
	        }
	        currentId=currentId+incrementBy;
	        updateMetaTble(connection, seqId, prefix, currentId);
			statement.close();
			connection.close();
	        return prefix+ String.format("%0"+maskLength+"d",currentId);
	        
	    } catch (SQLException e) {
	    	
	    }finally{
	    	try {
	    			statement.close();
	    			connection.close();
	    			
			} catch (SQLException e) {
				LOGGER.error("Failed to close the connection");
			}
			
	    }
	    return null;
	}
	
	/**
	 * Next prefix.
	 *
	 * @param prefix2 the prefix2
	 * @return the string
	 */
	private String _nextPrefix(String prefix2) {
		int charValue = prefix2.charAt(0);
    	String next = String.valueOf( (char) (charValue + 1));
    	return (next);
	}
	 public String nextPrefix(String s){
	    	
	    	int Letters=0;
	    	boolean addNewFiled=false;
	    	for(int i=0;i<s.length();i++){
	    		Letters = (int) (Letters+ (value1(s, i) * Math.pow(26, ((s.length()-1)-i)))) ;
	    	}	    	
	        Letters += 1;	        
	        addNewFiled=isPrefixEnd(s);
	        	
	        char[] letters = new char[s.length()];
	        int n = Letters;
	        for (int i = 0; i < s.length(); i++) {
	                letters[(s.length()-1)-i] = (char)('A' + (n%26));
	                n /= 26;
	        }
	       
	        s=new String(letters);
	        s=addNewFiled?"A"+s:s;
	        return s;
	        
	    }
	    private  int value1(String s, int index) {
	        return Character.toUpperCase(s.charAt(index)) - 'A';
	    }
	    
	    private  boolean isPrefixEnd(String s){
	    	Pattern pattern=Pattern.compile("[Z]*");
	    	Matcher matcher= pattern.matcher(s);
	    	return matcher.matches();
	    	
	    }
	
	
	private void updateMetaTble(Connection  connection,String key,String prefix,long id) throws SQLException {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("UPDATE ["+tableName+"]  SET prefix=? ,id=?  where seqId=?");
			preparedStatement.setString(1, prefix);
			preparedStatement.setLong(2, id);
			preparedStatement.setString(3, seqId);
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			throw new SQLException();
		}finally{
			
				preparedStatement.close();
			
		}
		
	}
	
	private void insertMetaTble(Connection  connection,String prefix,long id) throws SQLException {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("INSERT INTO ["+tableName+"]  ([seqId]  ,[prefix]   ,[id])   VALUES    (?,? ,?)");
			preparedStatement.setString(1, seqId);
			preparedStatement.setString(2, prefix);
			preparedStatement.setLong(3, id);			
			preparedStatement.executeUpdate();
			//preparedStatement.close();
		} catch (SQLException e) {
			throw new SQLException();
		}finally{
			
				preparedStatement.close();
			
		}
		
	}
	private void initMetatable(Connection connection) throws SQLException{
		Statement statement=null;
	
			statement=connection.createStatement();
			String query= "CREATE TABLE ["+tableName+"] ([seqId] varchar(50) NOT NULL PRIMARY KEY,[prefix] varchar(50) NULL,[id] numeric(18, 0) NULL)";
			statement.executeUpdate(query);
		
	}

	/** The Constant CONFIG_TABLE_NAME. */
	private final static String CONFIG_TABLE_NAME="tableName";
	
	/** The Constant CONFIG_SEQ_ID. */
	private final static String CONFIG_SEQ_ID="seqId";
	
	/** The Constant CONFIG_PREFIX. */
	private final static String CONFIG_PREFIX="prefix";
	
	/** The Constant CONFIG_INCREMENT_BY. */
	private final static String CONFIG_INCREMENT_BY="incrementBy";
	
	/** The Constant CONFIG_JUMP_ON. */
	private final static String CONFIG_JUMP_ON="jumpOn";
	
	/** The Constant CONFIG_MASK_LENGTH. */
	private final static String CONFIG_MASK_LENGTH="maskLength";

}
