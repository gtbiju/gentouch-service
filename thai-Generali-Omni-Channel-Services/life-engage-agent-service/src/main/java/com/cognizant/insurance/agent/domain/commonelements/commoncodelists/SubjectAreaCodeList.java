/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Identifies a classification of EducationCertificates according to their
 * subject area.
 * 
 * @author 301350
 * 
 * 
 */
public enum SubjectAreaCodeList {
    /**
     * Identifies a EducationCertificate with subject area 'Information
     * Technology'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    InformationTechnology,
    /**
     * Identifies an education certificate with subject area 'Accountancy'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Accountancy,
    /**
     * Identifies an education certificate with subject area 'Actuarial'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Actuarial,
    /**
     * Identifies an education certificate with subject area 'Insurance'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Insurance
}
