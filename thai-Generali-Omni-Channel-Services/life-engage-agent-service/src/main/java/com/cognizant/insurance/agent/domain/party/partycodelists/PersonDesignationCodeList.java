/**
 * 
 */
package com.cognizant.insurance.agent.domain.party.partycodelists;

/**
 * Person professional title.
 * 
 * @author 301350
 * 
 * 
 */
public enum PersonDesignationCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    CPCU,
    /**
     * 
     * 
     * 
     * 
     */
    MBA,
    /**
     * 
     * 
     * 
     * 
     */
    RN,
    /**
     * Physicians Assistant.
     * 
     * 
     * 
     */
    PA,
    /**
     * Physicians Assistant Certified.
     * 
     * 
     * 
     */
    PAC,
    /**
     * Registered Physicians Assistant.
     * 
     * 
     * 
     */
    RPA,
    /**
     * Registered Physicians Assistant Certified.
     * 
     * 
     * 
     */
    RPAC,
    /**
     * Professional Engineer.
     * 
     * 
     * 
     */
    PEng,
    /**
     * 
     * 
     * 
     * 
     */
    PhD,
    /**
     * Doctor of Dental Surgery.
     * 
     * 
     * 
     */
    DDS,
    /**
     * Doctor of Dental Medicine.
     * 
     * 
     * 
     */
    DMD,
    /**
     * 
     * 
     * 
     * 
     */
    EurChem,
    /**
     * 
     * 
     * 
     * 
     */
    EurIng,
    /**
     * 
     * 
     * 
     * 
     */
    MD
}
