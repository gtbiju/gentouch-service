package com.cognizant.generali.security.exception;

import org.springframework.security.authentication.BadCredentialsException;

public class UserLicenseException extends BadCredentialsException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1072551998339871172L;
	private String errorCode;

	public UserLicenseException(String msg) {
		super(msg);		
	}
	
	public UserLicenseException(final String errorCode, final String msg) {
        super(msg);
        this.setErrorCode(errorCode);
    }

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
