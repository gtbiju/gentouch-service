/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Dec 6, 2013
 */
package com.cognizant.insurance.component.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.domain.product.ProductSpecification;
import com.cognizant.insurance.product.component.ProductComponent;
import com.cognizant.insurance.domain.product.ProductTemplateMapping;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.impl.JPARequestImpl;
import com.cognizant.insurance.response.Response;
import com.cognizant.insurance.searchcriteria.ProductSearchCriteria;

/**
 * The Class class ProductRepository.
 */
//@Repository
public class ProductRepository {

    /** The entity manager. **/
    @PersistenceContext(unitName="LE_Platform")
    private EntityManager entityManager;

    /** The agreement component. */
    @Autowired
    private ProductComponent productComponent;

    /**
     * Gets the product.
     * 
     * @param criteria
     *            the criteria
     * @return the product
     */
    public final ProductSpecification getProduct(final ProductSearchCriteria criteria, final String version) {
    	Response<ProductSpecification> response = null;
        if (criteria.getId() == null) {
            final Request<ProductSearchCriteria> request =
                    new JPARequestImpl<ProductSearchCriteria>(entityManager, ContextTypeCodeList.PRODUCT, null);
            request.setType(criteria);
            response = productComponent.getProduct(request, version);
        } else {
            final Request<ProductSearchCriteria> request = new JPARequestImpl<ProductSearchCriteria>(entityManager, ContextTypeCodeList.PRODUCT, null);
            request.setType(criteria);
            response = productComponent.getProductById(request, version);
        }

        return response.getType();
    }

    /**
     * Used to check whether a product is active or not
     *
     * @param criteria
     * @return
     */
    public final Boolean isProductActive(final ProductSearchCriteria criteria, final String version) {
        Response<Boolean> response = null;
        final Request<ProductSearchCriteria> request = new JPARequestImpl<ProductSearchCriteria>(entityManager, ContextTypeCodeList.PRODUCT, null);
        request.setType(criteria);
        response = productComponent.isProductActive(request, version);      

        return response.getType();
    }

   
    /**
     * Gets the products.
     * 
     * @param criteria
     *            the criteria
     * @param activeProducts
     *            the active products
     * @return the products
     */
    public final Response<List<ProductSpecification>> getProducts(final ProductSearchCriteria criteria,
            final Boolean activeProducts) {
        Response<List<ProductSpecification>> response = null;

        final Request<ProductSearchCriteria> request =
                new JPARequestImpl<ProductSearchCriteria>(entityManager, ContextTypeCodeList.PRODUCT, null);
        request.setType(criteria);
        if (activeProducts) {
            response = productComponent.getActiveProducts(request);
        } else {
            response = productComponent.getProducts(request);

        }

        return response;
    }
    
    /**
     * Get all products by filter : carrierId & ChannelId
     * 
     * @param criteria
     *            the criteria
     * @param activeProducts
     *            the active products
     * @return the products
     */
    public final Response<List<ProductSpecification>> getAllOrActiveProductsByFilter(final ProductSearchCriteria criteria,
            final Boolean activeProducts, final String version) {
        Response<List<ProductSpecification>> response = null;

        final Request<ProductSearchCriteria> request =
                new JPARequestImpl<ProductSearchCriteria>(entityManager, ContextTypeCodeList.PRODUCT, null);
        request.setType(criteria);
        if (activeProducts) {
            response = productComponent.getActiveProductsByFilter(request,version);
        } else {
            response = productComponent.getAllProductsByFilter(request,version);

        }

        return response;
    }

    /**
     * Gets the products by id.
     * 
     * @param criteria
     *            the criteria
     * @return the products by id
     */
    public final Response<List<ProductSpecification>> getProductsByFilter(final ProductSearchCriteria criteria, String version) {
        final Request<ProductSearchCriteria> request =
                new JPARequestImpl<ProductSearchCriteria>(entityManager, ContextTypeCodeList.PRODUCT, null);
        request.setType(criteria);
        return productComponent.getProductsByFilter(request, version);
    }
    /**
     * Gets the product templates by filter.
     * 
     * @param criteria
     *            the criteria
     * @return the product templates by filter
     */
    public final Response<List<ProductTemplateMapping>> getProductTemplatesByFilter(final ProductSearchCriteria criteria) {
        final Request<ProductSearchCriteria> request =
                new JPARequestImpl<ProductSearchCriteria>(entityManager, ContextTypeCodeList.PRODUCT, null);
        request.setType(criteria);
        return productComponent.getProductTemplatesByFilter(request);
    }

    /**
     * Gets the entity manager.
     * 
     * @return the entityManager
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Sets the entity manager.
     * 
     * @param entityManager
     *            the entityManager to set
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Gets the product component.
     * 
     * @return the productComponent
     */
    public final ProductComponent getProductComponent() {
        return productComponent;
    }

    /**
     * Sets the product component.
     * 
     * @param productComponent
     *            the productComponent to set
     */
    public final void setProductComponent(final ProductComponent productComponent) {
        this.productComponent = productComponent;
    }

}
