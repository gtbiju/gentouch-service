/**
 * 
 */
package com.cognizant.insurance.domain.party.partycodelists;

/**
 * Identifies a classification of human bodies according to their blood type.
 * 
 * @author 301350
 * 
 * 
 */
public enum BloodTypeCodeList {
    /**
     * Identifies a HumanBody with blood type 'AB+'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    AbPlus,
    /**
     * Identifies a human body with blood type 'A+'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    APlus,
    /**
     * Identifies a human body with blood type 'AB'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    Ab,
    /**
     * Identifies a human body with blood type 'B-'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    BMinus,
    /**
     * Identifies a human body with blood type 'B+'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    BPlus,
    /**
     * Identifies a human body with blood type 'O'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    O,
    /**
     * Identifies a human body with blood type 'O+'.
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    OPlus
}
