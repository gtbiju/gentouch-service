/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Feb 4, 2013
 */

package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

// TODO: Auto-generated Javadoc
/**
 * Identifies a classification of Context.
 *
 * @author 301350
 */
public enum ContextTypeCodeList {
    
    /** Identifies the context as FNA. */
    FNA,
    
    /** Identifies the context as Quote. */
    QUOTE,
    
    /** Identifies the context as Illustration. */
    ILLUSTRATION,
    
    /** The product. */
    PRODUCT,

    /** Identifies the context as EAPP. */
    EAPP,
    
    /** The life engage. */
    LIFE_ENGAGE, 
    
    /** LMS */
    LMS,
   
    /** Email */
    EMAIL,
    
    /** The agent. */
    AGENT,
    
    /** The push notification. */
    PUSH_NOTIFICATION
}
