/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 291433
 * @version       : 0.1, Nov 12, 2012
 */
package com.cognizant.insurance.request.impl;

import javax.persistence.EntityManager;

import com.cognizant.insurance.domain.commonelements.commoncodelists.ContextTypeCodeList;
import com.cognizant.insurance.request.impl.RequestImpl;

/**
 * The Class class JPARequestImpl.
 * 
 * @param <T>
 *            the generic type
 */
public class JPARequestImpl<T> extends RequestImpl<T> {

    
    /**
     * Instantiates a new jPA request impl.
     *
     * @param entityManager the entity manager
     * @param contextId the context id
     * @param transactionId the transaction id
     */
    public JPARequestImpl(final EntityManager entityManager, ContextTypeCodeList contextId, String transactionId) {
        super();
        this.entityManager = entityManager;
        this.setContextId(contextId);
        this.setTransactionId(transactionId);        
    }

    /** The manager. */
    private EntityManager entityManager;

    /**
     * Gets the entityManager.
     * 
     * @return Returns the entityManager.
     */
    public final EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Sets The entityManager.
     * 
     * @param entityManager
     *            The entityManager to set.
     */
    public final void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}
