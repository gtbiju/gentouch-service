/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.domain.product;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


// TODO: Auto-generated Javadoc
/**
 * The Class CalculationSpecification.
 * 
 * @author 304007,420118
 */

@Entity
@Table(name = "CORE_PROD_CALC_SPEC")
public class CalculationSpecification extends Component {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8799760309347458010L;

    /** The name. */
    @Column(name = "NAME")
    private String name;

    /** The version. */
/*    @Column(name = "VERSION")
    private String version;*/

    /** The input parameters. */
    @Column(name = "INPUT_PARAMETERS", columnDefinition = "varchar(max)")
    private String inputParameters;

    /** The output parameters. */
    @Column(name = "OUTPUT_PARAMETERS", columnDefinition = "varchar(max)")
    private String outputParameters;

    /** The actual rule ref. */
    @Column(name = "ACTUAL_RULE_REF")
    private String actualRuleRef;

    /** The calculation type. */
    @ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "calculationType_ID")
    private CalculationType calculationType;

    /** The parent product. */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinColumn(name = "parentProduct_id")
    private ProductSpecification parentProductId;
    
    /** Group Name*. */
    @Column(name = "GROUP_NAME")
    private String groupName;
    
    /** The basic data complete code. */
    @Column(name= "basicDataCompleteCode")
    private Integer basicDataCompleteCode;
    
    /** The context id. */
    @Column(name= "contextId")
    private Integer contextId;
    
    /** The creation date time. */
    @Column(name= "creationDateTime")
    private Date creationDateTime;
    
    /** The modified date time. */
    @Column(name= "modifiedDateTime")
    private Date modifiedDateTime;
    
    /** The trans tracking id. */
    @Column(name= "transTrackingId")
    private String transTrackingId;
    
    /** The transaction id. */
    @Column(name= "transactionId")
    private String transactionId;
    
    /** The type name. */
    @Column(name= "typeName")
    private String typeName;
    
    /** The transaction keys_id. */
    @Column(name= "transactionKeys_id")
    private Long transactionKeys_id;

    /**
     * Gets the name.
     *
     * @return the name
     */
    public final String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the name to set
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the input parameters.
     *
     * @return the version
     */
/*    public final String getVersion() {
        return version;
    }*/

    /**
     * @param version
     *            the version to set
     */
/*    public final void setVersion(String version) {
        this.version = version;
    }*/

    /**
     * @return the inputParameters
     */
    public final String getInputParameters() {
        return inputParameters;
    }

    /**
     * Sets the input parameters.
     *
     * @param inputParameters the inputParameters to set
     */
    public final void setInputParameters(String inputParameters) {
        this.inputParameters = inputParameters;
    }

    /**
     * Gets the output parameters.
     *
     * @return the outputParameters
     */
    public final String getOutputParameters() {
        return outputParameters;
    }

    /**
     * Sets the output parameters.
     *
     * @param outputParameters the outputParameters to set
     */
    public final void setOutputParameters(String outputParameters) {
        this.outputParameters = outputParameters;
    }

    /**
     * Gets the actual rule ref.
     *
     * @return the actualRuleRef
     */
    public final String getActualRuleRef() {
        return actualRuleRef;
    }

    /**
     * Sets the actual rule ref.
     *
     * @param actualRuleRef the actualRuleRef to set
     */
    public final void setActualRuleRef(String actualRuleRef) {
        this.actualRuleRef = actualRuleRef;
    }

    /**
     * Gets the calculation type.
     *
     * @return the calculationType
     */
    public CalculationType getCalculationType() {
        return calculationType;
    }

    /**
     * Sets the calculation type.
     *
     * @param calculationType the calculationType to set
     */
    public void setCalculationType(CalculationType calculationType) {
        this.calculationType = calculationType;
    }

    /**
     * Gets the parent product id.
     *
     * @return the parentProductId
     */
    public final ProductSpecification getParentProductId() {
        return parentProductId;
    }

    /**
     * Sets the parent product id.
     *
     * @param parentProductId the parentProductId to set
     */
    public final void setParentProductId(ProductSpecification parentProductId) {
        this.parentProductId = parentProductId;
    }

	/**
	 * Gets the group name.
	 *
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * Sets the group name.
	 *
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

    /**
     * Gets the basic data complete code.
     *
     * @return the basic data complete code
     */
    public Integer getBasicDataCompleteCode() {
        return basicDataCompleteCode;
    }

    /**
     * Sets the basic data complete code.
     *
     * @param basicDataCompleteCode the basic data complete code to set.
     */
    public void setBasicDataCompleteCode(Integer basicDataCompleteCode) {
        this.basicDataCompleteCode = basicDataCompleteCode;
    }

    /**
     * Gets the context id.
     *
     * @return the context id
     */
    public Integer getContextId() {
        return contextId;
    }

    /**
     * Sets the context id.
     *
     * @param contextId the context id to set.
     */
    public void setContextId(Integer contextId) {
        this.contextId = contextId;
    }

    /**
     * Gets the creation date time.
     *
     * @return the creation date time
     */
    public Date getCreationDateTime() {
        return creationDateTime;
    }

    /**
     * Sets the creation date time.
     *
     * @param creationDateTime the creation date time to set.
     */
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    /**
     * Gets the modified date time.
     *
     * @return the modified date time
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    /**
     * Sets the modified date time.
     *
     * @param modifiedDateTime the modified date time to set.
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    /**
     * Gets the trans tracking id.
     *
     * @return the trans tracking id
     */
    public String getTransTrackingId() {
        return transTrackingId;
    }

    /**
     * Sets the trans tracking id.
     *
     * @param transTrackingId the trans tracking id to set.
     */
    public void setTransTrackingId(String transTrackingId) {
        this.transTrackingId = transTrackingId;
    }

    /**
     * Gets the transaction id.
     *
     * @return the transaction id
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the transaction id.
     *
     * @param transactionId the transaction id to set.
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Gets the type name.
     *
     * @return the type name
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * Sets the type name.
     *
     * @param typeName the type name to set.
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * Gets the transaction keys_id.
     *
     * @return the transaction keys_id
     */
    public Long getTransactionKeys_id() {
        return transactionKeys_id;
    }

    /**
     * Sets the transaction keys_id.
     *
     * @param transactionKeys_id the transaction keys_id to set.
     */
    public void setTransactionKeys_id(Long transactionKeys_id) {
        this.transactionKeys_id = transactionKeys_id;
    }
    
	
}
