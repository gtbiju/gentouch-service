package com.cognizant.insurance.agent.domain.agent;

/**
 * The Enum AgentStatusCodeList.
 */
public enum AgentStatusCodeList {
    
    /** The Terminated. */
	Terminated,
    
    /** The Active. */
	Active,
    
    /** The Suspended. */
    Suspended

}
