<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page import = "java.util.ResourceBundle" %>
<% ResourceBundle resource = ResourceBundle.getBundle("resource");%>
<%@ page import="com.cognizant.le.model.dto.LoginStatus" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Page</title>

<link type="text/css" rel="stylesheet" href="css/styles_workbench.css" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">

window.history.forward(1);
function noBack() 
{ 
window.history.forward(); 
}

function submit(){
	 document.LoginForm.submit();
	
}
	

$(document).ready(function() {  
	var msg = "<%=request.getAttribute("requestMsg")%>";
	if (msg.length > 4) {
		document.getElementById('loginmsg').innerHTML=msg;
	}
	 <%
            				if(null!=request.getAttribute("status")){
            				LoginStatus loginStatus =(LoginStatus)request.getAttribute("status");
            			    Boolean isEmpty = loginStatus.isEmpty();
                            Boolean isInvalidUser = loginStatus.isInvalidUser();
							Boolean isUnauthorizedUser = loginStatus.isUnauthorized();
							if(isEmpty){%>
									if(document.getElementById('password').value == ""){
										document.getElementById('password').className += ' highlight';
									}else{
										document.getElementById('password').className = 'date span12';
									}
									if(document.getElementById('username').value == ""){
										document.getElementById('username').className += ' highlight';
									}else{
										document.getElementById('username').className = 'date span12';
									}
									
									$('#LoginErrorPopup').show();
									$('#LoginErrorPopup #errorMessage').html("<%=new String(resource.getString("userIdPasswordMandatoryMessageEN").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									
							<%} else if(isUnauthorizedUser){ %>
									$('#LoginErrorPopup').show();
									$('#LoginErrorPopup #errorMessage').html("<%=new String(resource.getString("unauthorizedUserMessageEN").getBytes( "ISO-8859-1" ),"UTF-8")%>");
									
							<%} else {
								if(isInvalidUser){%>
									$('#LoginErrorPopup').show();
									$('#LoginErrorPopup #errorMessage').html("<%=new String(resource.getString("userIdPasswordValidationMessageEN").getBytes( "ISO-8859-1" ),"UTF-8")%>");
							<% } } }%>							
							 
	$("#password").keyup(function(event){
	    if(event.keyCode == 13){
	        $("#loginButton").click();
	    }
	});
	
	$('#errorOKbtn').click(function() {		
		$('#LoginErrorPopup').hide();		
	});

	
});
</script>

</head>

<body class="loginBody">
	<div class="headerLogin headerStyle">
	<h1 class="text-center subheading welcome"><span class="red-text header"><%=new String(resource.getString("welcomeEN").getBytes( "ISO-8859-1" ),"UTF-8") %></span></h1>
	</div>    
   
	<div class="image-login push">
   	<div class="login">
		    <form action="Login" method="post" name="LoginForm">
                <div class="text-center"><img class="Logo_TH" src="img/login_logo.png"></div>
				
				<div class="row-fluid ">
						<h1 class="text-center subheading welcometxt"><span class="red-text"><%=new String(resource.getString("welcomeEN").getBytes( "ISO-8859-1" ),"UTF-8") %></span></h1>	
						<div class="input-area"><span class="user"></span><input type="text" class=" date span12"  placeholder="<%=new String(resource.getString("userNameEN").getBytes( "ISO-8859-1" ),"UTF-8") %>" name="username" id="username" required>
						
						</div>
						<div class="input-area" ng-show="showpassword"><span class="lock"></span><input type="password" class=" date span12" placeholder="<%=new String(resource.getString("passwordEN").getBytes( "ISO-8859-1" ),"UTF-8") %>" id="password" name="password"> </div>
						<button type="button" class="btn-primary btn-block btn-large"  onclick="submit()" id="loginButton"><%=new String(resource.getString("loginEN").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
                </div>
			</form>
	</div>  
				<!-- CONTENT CONTAINER -->

    <div class="modal_pop_up agtupd_content_pop_up error_popup" id="LoginErrorPopup">
		<div class="pop_up_img">
		    <img src="img/infored.png" width="25">
		</div>		
			<div class="center-align msgfont">
				<span id="errorMessage"></span>
			</div>
			<div class="btn-wrapper closeBtn">
				<button class="pop_up_upload_btn ok-btn close_btn" id="errorOKbtn"><%=new String(resource.getString("ok").getBytes( "ISO-8859-1" ),"UTF-8") %></button>
			</div>		
	</div>

	<div class="footer_part">
	    <p class="copyright_label">©2017 Generali Life Assurance (Thailand) Plc</p>
		<span class="version">Version No:1.0</span>
	</div>

</div>
	

</body>


</html>
