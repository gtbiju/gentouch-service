package com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists;

public enum ActivityCodeList {
	Non_Contactable,
	
	Meeting_Fixed,
	
	Successful_Sale,
	
	ToBeCalled,
	
	ContactedAndNotInterested,
	
	Interested,
	
	App_Submitted,
	
	Pending_Documents,
	
	Others,
	
	NotMentioned
}
