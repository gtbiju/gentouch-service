/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Jul 30, 2015
 */
package com.cognizant.insurance.searchcriteria;

import java.util.List;


/**
 * The Class class Submodes.
 */
public class SearchCountResult {

    /** The mode. */
    private String mode;

    /** The count. */
    private String count;
    
    private String newCount;
    
    private String closedCount;
    
    private String followUpCount;

  	private String closed_YTD;		
	
	private String closed_MTD;
	
	private String leadAssigned_YTD;		
	
	private String leadAssigned_MTD;
	
	private String APE_YTD;
	private String APE_MTD;
	
	private String activeAgents_MTD;
	private String activeAgents_YTD;
	
	private String totalAgents_YTD;
	private String totalAgents_MTD;
	
	public String noOfAppointments;
	
    /** The sub mode results. */
    private List<SearchCountResult> subModeResults;

    /** The transTrackingId. */
    private String transTrackingId;

    public String getTransTrackingId() {
        return transTrackingId;
    }

    public void setTransTrackingId(String transTrackingId) {
        this.transTrackingId = transTrackingId;
    }

    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode
     *            the mode to set.
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the count.
     * 
     * @return the count
     */
    public String getCount() {
        return count;
    }

    /**
     * Sets the count.
     * 
     * @param count
     *            the count to set.
     */
    public void setCount(String count) {
        this.count = count;
    }

    /**
     * Gets the sub mode results.
     * 
     * @return the sub mode results
     */
    public List<SearchCountResult> getSubModeResults() {
        return subModeResults;
    }

    /**
     * Sets the sub mode results.
     * 
     * @param subModeResults
     *            the sub mode results to set.
     */
    public void setSubModeResults(List<SearchCountResult> subModeResults) {
        this.subModeResults = subModeResults;
    }

	/**
	 * @return the newCount
	 */
	public String getNewCount() {
		return newCount;
	}

	/**
	 * @param newCount the newCount to set
	 */
	public void setNewCount(String newCount) {
		this.newCount = newCount;
	}

	/**
	 * @return the closedCount
	 */
	public String getClosedCount() {
		return closedCount;
	}

	/**
	 * @param closedCount the closedCount to set
	 */
	public void setClosedCount(String closedCount) {
		this.closedCount = closedCount;
	}

	/**
	 * @return the followUpCount
	 */
	public String getFollowUpCount() {
		return followUpCount;
	}

	/**
	 * @param followUpCount the followUpCount to set
	 */
	public void setFollowUpCount(String followUpCount) {
		this.followUpCount = followUpCount;
	}

	/**
	 * @return the closed_YTD
	 */
	public String getClosed_YTD() {
		return closed_YTD;
	}

	/**
	 * @param closed_YTD the closed_YTD to set
	 */
	public void setClosed_YTD(String closed_YTD) {
		this.closed_YTD = closed_YTD;
	}

	/**
	 * @return the closed_MTD
	 */
	public String getClosed_MTD() {
		return closed_MTD;
	}

	/**
	 * @param closed_MTD the closed_MTD to set
	 */
	public void setClosed_MTD(String closed_MTD) {
		this.closed_MTD = closed_MTD;
	}

	/**
	 * @return the leadAssigned_YTD
	 */
	public String getLeadAssigned_YTD() {
		return leadAssigned_YTD;
	}

	/**
	 * @param leadAssigned_YTD the leadAssigned_YTD to set
	 */
	public void setLeadAssigned_YTD(String leadAssigned_YTD) {
		this.leadAssigned_YTD = leadAssigned_YTD;
	}

	/**
	 * @return the leadAssigned_MTD
	 */
	public String getLeadAssigned_MTD() {
		return leadAssigned_MTD;
	}

	/**
	 * @param leadAssigned_MTD the leadAssigned_MTD to set
	 */
	public void setLeadAssigned_MTD(String leadAssigned_MTD) {
		this.leadAssigned_MTD = leadAssigned_MTD;
	}

	/**
	 * @return the aPE_YTD
	 */
	public String getAPE_YTD() {
		return APE_YTD;
	}

	/**
	 * @param aPE_YTD the aPE_YTD to set
	 */
	public void setAPE_YTD(String aPE_YTD) {
		APE_YTD = aPE_YTD;
	}

	/**
	 * @return the aPE_MTD
	 */
	public String getAPE_MTD() {
		return APE_MTD;
	}

	/**
	 * @param aPE_MTD the aPE_MTD to set
	 */
	public void setAPE_MTD(String aPE_MTD) {
		APE_MTD = aPE_MTD;
	}

	/**
	 * @return the activeAgents_MTD
	 */
	public String getActiveAgents_MTD() {
		return activeAgents_MTD;
	}

	/**
	 * @param activeAgents_MTD the activeAgents_MTD to set
	 */
	public void setActiveAgents_MTD(String activeAgents_MTD) {
		this.activeAgents_MTD = activeAgents_MTD;
	}

	/**
	 * @return the activeAgents_YTD
	 */
	public String getActiveAgents_YTD() {
		return activeAgents_YTD;
	}

	/**
	 * @param activeAgents_YTD the activeAgents_YTD to set
	 */
	public void setActiveAgents_YTD(String activeAgents_YTD) {
		this.activeAgents_YTD = activeAgents_YTD;
	}

	/**
	 * @return the totalAgents_YTD
	 */
	public String getTotalAgents_YTD() {
		return totalAgents_YTD;
	}

	/**
	 * @param totalAgents_YTD the totalAgents_YTD to set
	 */
	public void setTotalAgents_YTD(String totalAgents_YTD) {
		this.totalAgents_YTD = totalAgents_YTD;
	}

	/**
	 * @return the totalAgents_MTD
	 */
	public String getTotalAgents_MTD() {
		return totalAgents_MTD;
	}

	/**
	 * @param totalAgents_MTD the totalAgents_MTD to set
	 */
	public void setTotalAgents_MTD(String totalAgents_MTD) {
		this.totalAgents_MTD = totalAgents_MTD;
	}

	public String getNoOfAppointments() {
		return noOfAppointments;
	}

	public void setNoOfAppointments(String noOfAppointments) {
		this.noOfAppointments = noOfAppointments;
	}

}
