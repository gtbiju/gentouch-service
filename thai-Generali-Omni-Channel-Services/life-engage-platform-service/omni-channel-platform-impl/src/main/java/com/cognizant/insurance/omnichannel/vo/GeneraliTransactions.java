/**
 *
 * © Copyright 2012, Cognizant 
 *
 * @author        : 356551
 * @version       : 0.1, Mar 13, 2018
 */
package com.cognizant.insurance.omnichannel.vo;

import java.util.Date;
import java.util.List;

import com.cognizant.insurance.omnichannel.domain.AppianMemoDetails;
import com.cognizant.insurance.request.vo.Transactions;

// TODO: Auto-generated Javadoc
/**
 * The Class GeneraliTransactions.
 */
public class GeneraliTransactions extends Transactions {
    
    /** The appian memo details. */
    private List<AppianMemoDetails> appianMemoDetails;
    
    /** The memo date. */
    private Date memoDate;
    
    /** The lock or unlock mode. */
    private String lockOrUnlockMode;

    private String dateMonthInThai;
    
    /**
     * Gets the appianMemoDetails.
     *
     * @return Returns the appianMemoDetails.
     */
    public List<AppianMemoDetails> getAppianMemoDetails() {
        return appianMemoDetails;
    }

    /**
     * Sets The appianMemoDetails.
     *
     * @param appianMemoDetails The appianMemoDetails to set.
     */
    public void setAppianMemoDetails(List<AppianMemoDetails> appianMemoDetails) {
        this.appianMemoDetails = appianMemoDetails;
    }

    /**
     * Gets the memoDate.
     *
     * @return Returns the memoDate.
     */
    public Date getMemoDate() {
        return memoDate;
    }

    /**
     * Sets The memoDate.
     *
     * @param memoDate The memoDate to set.
     */
    public void setMemoDate(Date memoDate) {
        this.memoDate = memoDate;
    }

    /**
     * Gets the lockOrUnlockMode.
     *
     * @return Returns the lockOrUnlockMode.
     */
    public String getLockOrUnlockMode() {
        return lockOrUnlockMode;
    }

    /**
     * Sets The lockOrUnlockMode.
     *
     * @param lockOrUnlockMode The lockOrUnlockMode to set.
     */
    public void setLockOrUnlockMode(String lockOrUnlockMode) {
        this.lockOrUnlockMode = lockOrUnlockMode;
    }

	public String getDateMonthInThai() {
		return dateMonthInThai;
	}

	public void setDateMonthInThai(String dateMonthInThai) {
		this.dateMonthInThai = dateMonthInThai;
	}


}
