package com.cognizant.insurance.agent;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * Agent details will be stored.
 * 
 * 
 */
@Entity
@Table(name = "User_GAO")
public class GeneraliGAOAgent {	
	
	@Id
	@Column(name = "Username")
	private String agentCode;
	
	@Column(name = "Name", columnDefinition = "NVARCHAR(MAX)")
	private String agentName;
	
	@Column(name = "APPOINT_DATE")
	private String appointmentDate;
	
	@Column(name = "SO_LIC_EXPIRE_DATE")
	private String licenseExpireDate;
	
	@Column(name = "STATUS")
	private String agentStatus;
	
	@Column(name = "TERMINATE_DATE")
	private String terminateDate;
	
    @Column(name = "TSALESUNT")
	private String gaoCode;
	
	@Column(name = "SECURITY_NO")
	private String identificationId;
	
	@Column(name = "SALES_OFFICER_LICENSE")
	private String licenseNumber;
	
	@Column(name = "CLTDOB")
	private String agentDob;
	
	@Column(name = "Level")
	private String level;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getLicenseExpireDate() {
		return licenseExpireDate;
	}

	public void setLicenseExpireDate(String licenseExpireDate) {
		this.licenseExpireDate = licenseExpireDate;
	}

	public String getAgentStatus() {
		return agentStatus;
	}

	public void setAgentStatus(String agentStatus) {
		this.agentStatus = agentStatus;
	}

	public String getTerminateDate() {
		return terminateDate;
	}

	public void setTerminateDate(String terminateDate) {
		this.terminateDate = terminateDate;
	}

	public String getIdentificationId() {
		return identificationId;
	}

	public void setIdentificationId(String identificationId) {
		this.identificationId = identificationId;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getAgentDob() {
		return agentDob;
	}

	public void setAgentDob(String agentDob) {
		this.agentDob = agentDob;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getGaoCode() {
		return gaoCode;
	}

	public void setGaoCode(String gaoCode) {
		this.gaoCode = gaoCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	
}
