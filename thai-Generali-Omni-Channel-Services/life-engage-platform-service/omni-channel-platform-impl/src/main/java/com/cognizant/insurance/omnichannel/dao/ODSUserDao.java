package com.cognizant.insurance.omnichannel.dao;

import java.util.List;

import com.cognizant.generali.security.entity.GeneraliTHUser;
import com.cognizant.insurance.agent.GeneraliAgent;
import com.cognizant.insurance.agent.GeneraliGAOAgent;
import com.cognizant.insurance.agent.PasswordHistory;
import com.cognizant.insurance.dao.Dao;
import com.cognizant.insurance.omnichannel.dataresults.dto.TeamDetailsDto;
import com.cognizant.insurance.request.Request;
import com.cognizant.insurance.request.vo.Transactions;
import com.cognizant.insurance.response.Response;

public interface ODSUserDao extends Dao {
	
	public Response<GeneraliAgent> validateAgentProfile(Request<Transactions> request);
	
	public Response<GeneraliTHUser> validateLoggedInUser(Request<Transactions> request);
	
	public Response<List<PasswordHistory>> getRecentPasswords(Request<String> userRequest, Request<Integer> historyRequest);

	public Response<PasswordHistory> getOldPasswordHistory(Request<String> userRequest);
	
	public Response<List<TeamDetailsDto>> getTeamDetails(Request<Transactions> request);
	
	public Response<GeneraliGAOAgent> validateGAOAgentProfile(Request<Transactions> request);
	
	public Response<List<GeneraliAgent>> getAllAgentDetails(Request<Transactions> request);
}
