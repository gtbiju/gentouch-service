/**
 * 
 */
package com.cognizant.insurance.agent.domain.finance;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.cognizant.insurance.agent.domain.commonelements.InformationModelObject;
import com.cognizant.insurance.agent.domain.commonelements.complexdatatypes.TimePeriod;

/**
 * A definition of a set of real or potential cash-flows that belong together.
 * This can be used for all amounts of money relevant to the modeled
 * organization, including premiums, insurance benefits, invoices, claims,
 * commissions, salaries, and many others.
 * 
 * e.g: A monthly amount of EUR 500 due on the first of each month from 1 April
 * 2001.
 * 
 * e.g: A yearly amount of BEF 15,000 due on the first of May from 1 May 1998
 * until 1 May 2003 as premium for a car insurance policy.
 * 
 * e.g: An amount of 200 Euro due as a 'reinstatement premium' for the
 * reinstatement of car insurance policy ABC2255 after suspension.
 * 
 * @author 301350
 * 
 * 
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class FinancialProvision extends InformationModelObject {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6659613503926438093L;

    /**
     * A textual statement used to explain the meaning of this money provision.
     * The description may contain the reason for creating the money provision.
     * 
     * 
     * 
     */
    private String description;

    /**
     * The time period at which this money provision is active.
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    private TimePeriod effectivePeriod;

    /**
     * The identification of a particular money provision of which the value is
     * based on the value of another particular money provision.
     * 
     * e.g: The particular money provision initial commission based on the
     * particular money provision coverage premium.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinTable(name = "FINANCIALPROVISION_ISBASEDONFINANCIALPROVISION",
            joinColumns = { @JoinColumn(name = "FINANCIALPROVISION_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "ISBASEDONFINANCIALPROVISION_ID", referencedColumnName = "Id") })
    private Set<FinancialProvision> isBasedOn;

    /**
     * The identification of a particular money provision of which the value is
     * based on the value of another particular money provision.
     * 
     * e.g: The particular money provision initial commission based on the
     * particular money provision coverage premium.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    @JoinTable(name = "FINANCIALPROVISION_ISBASEDFORFINANCIALPROVISION",
            joinColumns = { @JoinColumn(name = "FINANCIALPROVISION_ID", referencedColumnName = "Id") },
            inverseJoinColumns = { @JoinColumn(name = "ISBASEDFORFINANCIALPROVISION_ID", referencedColumnName = "Id") })
    private Set<FinancialProvision> isBaseFor;

    /**
     * The identification of a particular money provision defined from a generic
     * money provision.
     * 
     * e.g: The particular money provision insurance benefit granted for a claim
     * is defined from the generic money provision insurance benefit provided in
     * the insurance policy.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "isDefinedFrom")
    private Set<FinancialProvision> defines;

    /**
     * The identification of a particular money provision defined from a generic
     * money provision.
     * 
     * e.g: The particular money provision insurance benefit granted for a claim
     * is defined from the generic money provision insurance benefit provided in
     * the insurance policy.
     * 
     * 
     * 
     * 
     * 
     */
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private FinancialProvision isDefinedFrom;

    /**
     * Indicates that the particular money provision is an estimate.
     * 
     * 
     * 
     */
    private Boolean estimateIndicator;
 
    /**
     * This relationship provides traceability from the scheduled financial
     * exchange to the exact Financial Provision used.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private Set<FinancialScheduler> attachedFinancialScheduler;

    /**
     * This relationship links a financial provision to a related installment.
     * 
     * 
     * 
     * 
     * 
     */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE })
    private Set<Installment> basedOn;

    /**
     * Gets the description.
     * 
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the effectivePeriod.
     * 
     * @return Returns the effectivePeriod.
     */
    public TimePeriod getEffectivePeriod() {
        return effectivePeriod;
    }

    /**
     * Gets the isBasedOn.
     * 
     * @return Returns the isBasedOn.
     */
    public Set<FinancialProvision> getIsBasedOn() {
        return isBasedOn;
    }

    /**
     * Gets the isBaseFor.
     * 
     * @return Returns the isBaseFor.
     */
    public Set<FinancialProvision> getIsBaseFor() {
        return isBaseFor;
    }

    /**
     * Gets the defines.
     * 
     * @return Returns the defines.
     */
    public Set<FinancialProvision> getDefines() {
        return defines;
    }

    /**
     * Gets the isDefinedFrom.
     * 
     * @return Returns the isDefinedFrom.
     */
    public FinancialProvision getIsDefinedFrom() {
        return isDefinedFrom;
    }

    /**
     * Gets the estimateIndicator.
     * 
     * @return Returns the estimateIndicator.
     */
    public Boolean getEstimateIndicator() {
        return estimateIndicator;
    }

    /**
     * Gets the attachedFinancialScheduler.
     * 
     * @return Returns the attachedFinancialScheduler.
     */
    public Set<FinancialScheduler> getAttachedFinancialScheduler() {
        return attachedFinancialScheduler;
    }

    /**
     * Gets the basedOn.
     * 
     * @return Returns the basedOn.
     */
    public Set<Installment> getBasedOn() {
        return basedOn;
    }

    /**
     * Sets The description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets The effectivePeriod.
     * 
     * @param effectivePeriod
     *            The effectivePeriod to set.
     */
    public void setEffectivePeriod(final TimePeriod effectivePeriod) {
        this.effectivePeriod = effectivePeriod;
    }

    /**
     * Sets The isBasedOn.
     * 
     * @param isBasedOn
     *            The isBasedOn to set.
     */
    public void setIsBasedOn(final Set<FinancialProvision> isBasedOn) {
        this.isBasedOn = isBasedOn;
    }

    /**
     * Sets The isBaseFor.
     * 
     * @param isBaseFor
     *            The isBaseFor to set.
     */
    public void setIsBaseFor(final Set<FinancialProvision> isBaseFor) {
        this.isBaseFor = isBaseFor;
    }

    /**
     * Sets The defines.
     * 
     * @param defines
     *            The defines to set.
     */
    public void setDefines(final Set<FinancialProvision> defines) {
        this.defines = defines;
    }

    /**
     * Sets The isDefinedFrom.
     * 
     * @param isDefinedFrom
     *            The isDefinedFrom to set.
     */
    public void setIsDefinedFrom(final FinancialProvision isDefinedFrom) {
        this.isDefinedFrom = isDefinedFrom;
    }

    /**
     * Sets The estimateIndicator.
     * 
     * @param estimateIndicator
     *            The estimateIndicator to set.
     */
    public void setEstimateIndicator(final Boolean estimateIndicator) {
        this.estimateIndicator = estimateIndicator;
    }

    /**
     * Sets The attachedFinancialScheduler.
     * 
     * @param attachedFinancialScheduler
     *            The attachedFinancialScheduler to set.
     */
    public void setAttachedFinancialScheduler(
            final Set<FinancialScheduler> attachedFinancialScheduler) {
        this.attachedFinancialScheduler = attachedFinancialScheduler;
    }

    /**
     * Sets The basedOn.
     * 
     * @param basedOn
     *            The basedOn to set.
     */
    public void setBasedOn(final Set<Installment> basedOn) {
        this.basedOn = basedOn;
    }


}
