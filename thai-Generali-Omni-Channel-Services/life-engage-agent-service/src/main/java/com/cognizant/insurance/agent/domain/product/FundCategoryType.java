/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Jan 31, 2013
 */

package com.cognizant.insurance.agent.domain.product;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * The Class class CategoryType.
 */

@Entity
@DiscriminatorValue("Fund_Category")
public class FundCategoryType extends Type {

    /** The product funds1. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "fundCategoryType1")
    private Set<ProductFunds> productFunds1;

    /** The product funds2. */
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE }, mappedBy = "fundCategoryType2")
    private Set<ProductFunds> productFunds2;

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2182594528943276604L;

    /**
     * The Constructor.
     */
    public FundCategoryType() {
        super();
    }

    /**
     * The Constructor.
     * 
     * @param id
     *            the id
     */
    public FundCategoryType(final String id) {
        super(id);
    }

    /**
     * Gets the product funds1.
     * 
     * @return the productFunds1
     */
    public final Set<ProductFunds> getProductFunds1() {
        return productFunds1;
    }

    /**
     * Sets the product funds1.
     * 
     * @param productFunds1
     *            the productFunds1 to set
     */
    public final void setProductFunds1(Set<ProductFunds> productFunds1) {
        this.productFunds1 = productFunds1;
    }

    /**
     * Gets the product funds2.
     * 
     * @return the productFunds2
     */
    public final Set<ProductFunds> getProductFunds2() {
        return productFunds2;
    }

    /**
     * Sets the product funds2.
     * 
     * @param productFunds2
     *            the productFunds2 to set
     */
    public final void setProductFunds2(Set<ProductFunds> productFunds2) {
        this.productFunds2 = productFunds2;
    }
}
