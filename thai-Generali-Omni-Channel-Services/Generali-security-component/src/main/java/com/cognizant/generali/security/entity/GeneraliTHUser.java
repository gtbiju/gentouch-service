package com.cognizant.generali.security.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cognizant.le.security.entity.User;

/**
 * 
 * @author 594889
 *
 */
@Entity
public class GeneraliTHUser extends User {
	
	private static final long serialVersionUID = 1L;
	
	private static final String dateTimeDefaultNull = "datetime DEFAULT NULL";
	
	@Column(name="ATTEMPTS",columnDefinition = "int default 0")
	private int attempts;

	@Column(name="PASSWORDLOCKED",columnDefinition = "bit default 0")
	private boolean passwordLocked;	
	
	@Temporal(TemporalType.DATE)
	@Column(name="PASSWORDEXPIRYDATE", columnDefinition = dateTimeDefaultNull)
	private Date passwordExpiryDate;
	
	@Column(name="ORGIN",columnDefinition = "nvarchar(50) default NULL")
    private String orgin;

	public int getAttempts() {
		return attempts;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	public boolean isPasswordLocked() {
		return passwordLocked;
	}

	public void setPasswordLocked(boolean passwordLocked) {
		this.passwordLocked = passwordLocked;
	}

	public Date getPasswordExpiryDate() {
		return passwordExpiryDate;
	}

	public void setPasswordExpiryDate(Date passwordExpiryDate) {
		this.passwordExpiryDate = passwordExpiryDate;
	}

    /**
     * Gets the orgin.
     *
     * @return Returns the orgin.
     */
    public String getOrgin() {
        return orgin;
    }

    /**
     * Sets The orgin.
     *
     * @param passwordOrgin The orgin to set.
     */
    public void setOrgin(String orgin) {
        this.orgin = orgin;
    }
	
}
