-- MySQL dump 10.13  Distrib 5.1.51, for Win32 (ia32)
--
-- Host: localhost    Database: le_contentadmin
-- ------------------------------------------------------
-- Server version	5.1.51-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for table `AGENT_APP_LOGIN`
--

DROP TABLE IF EXISTS `AGENT_APP_LOGIN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AGENT_APP_LOGIN` (
  `userid` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(256) DEFAULT NULL,
  `active` varchar(20) DEFAULT NULL,
  `agentcode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `APPLICATION_CONTEXTS`
--

DROP TABLE IF EXISTS `APPLICATION_CONTEXTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `APPLICATION_CONTEXTS` (
  `Id` int(11) NOT NULL,
  `Context` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CONTENT_TYPES`
--

DROP TABLE IF EXISTS `CONTENT_TYPES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTENT_TYPES` (
  `Id` int(11) NOT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `extensions` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CONTENTFILES`
--

DROP TABLE IF EXISTS `CONTENTFILES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTENTFILES` (
  `fileName` varchar(255) NOT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `publicURL` varchar(255) DEFAULT NULL,
  `tabletFullPath` varchar(255) DEFAULT NULL,
  `Type_Id` int(11) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fileName`),
  KEY `FKBC11519E360674B9` (`Type_Id`),
  CONSTRAINT `FKBC11519E360674B9` FOREIGN KEY (`Type_Id`) REFERENCES `CONTENT_TYPES` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `CONTENTFILES_APPCONTEXT_X`
--

DROP TABLE IF EXISTS `CONTENTFILES_APPCONTEXT_X`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTENTFILES_APPCONTEXT_X` (
  `contentFile` varchar(255) NOT NULL,
  `appContextId` int(11) NOT NULL,
  KEY `FK9E3B01C826F5859` (`contentFile`),
  KEY `FK9E3B01C8227E8B28` (`appContextId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NWS_USER`
--

DROP TABLE IF EXISTS `NWS_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NWS_USER` (
  `UID` varchar(255) NOT NULL,
  `givenName` varchar(255) DEFAULT NULL,
  `mnylAgentCode` varchar(255) DEFAULT NULL,
  `mnylAgentEmail` varchar(255) DEFAULT NULL,
  `mnylBossID` varchar(255) DEFAULT NULL,
  `mnylMyAgentRoleCode` varchar(255) DEFAULT NULL,
  `mnylStatus` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `UserMarkedAsDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-24 15:08:06

-- New table content languages added, updated foreign key dependancy in content file 

CREATE TABLE `CONTENT_LANGUAGES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE CONTENTFILES
 ADD lang_id INT;


ALTER TABLE CONTENTFILES
 ADD CONSTRAINT FK_CONFILE_CONLANG FOREIGN KEY (lang_id) REFERENCES CONTENT_LANGUAGES (id) ON UPDATE RESTRICT ON DELETE RESTRICT;

--End Content_Language Changes

  ALTER TABLE CONTENT_TYPES
 ADD optionality TINYINT AFTER extensions;
 
 
 ALTER TABLE CONTENTFILES
 ADD is_mandatory TINYINT(1) DEFAULT '1' AFTER lang_id;
 
 
 ALTER TABLE CONTENT_TYPES
 ADD previewable TINYINT;
 
 
 
 
 ALTER TABLE LE_LOGIN_USER ADD INDEX (USER_ID);

CREATE TABLE `AGENT` (
  `agentCode` varchar(255) NOT NULL,
  `briefWriteUp` varchar(255) DEFAULT NULL,
  `employeeId` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `name_of_excelsheet` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `uploaded_timestamp` datetime DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `emp_type` varchar(50) DEFAULT NULL,
  `supervisor_code` varchar(255) DEFAULT NULL,
  `office` varchar(20) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `agent_group` varchar(20) DEFAULT NULL,
  `yoe` decimal(3,0) DEFAULT NULL,
  `business_sourced` decimal(20,0) DEFAULT NULL,
  `num_of_service` decimal(20,0) DEFAULT NULL,
  `license_num` varchar(30) DEFAULT NULL,
  `license_issue_date` date DEFAULT NULL,
  `license_exp_date` date DEFAULT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `mob_num` decimal(20,0) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `agentCode_2` (`agentCode`),
  KEY `supervisor_code` (`supervisor_code`),
  KEY `agentCode` (`agentCode`),
  KEY `FK_AGENT_CODES` (`userid`),
  CONSTRAINT `FK_AGENT_CODES` FOREIGN KEY (`userid`) REFERENCES `LE_LOGIN_USER` (`USER_ID`),
  CONSTRAINT `FK_AGENT_SUPER_CODES` FOREIGN KEY (`supervisor_code`) REFERENCES `AGENT` (`agentCode`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;


CREATE TABLE `ACHIEVEMENT` (
  `agent_id` int(11) NOT NULL,
  `achievement_code` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `id` int(11) DEFAULT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `agent_id` (`agent_id`),
  CONSTRAINT `FK_ACHIEVEMENT_AGENT` FOREIGN KEY (`agent_id`) REFERENCES `AGENT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 