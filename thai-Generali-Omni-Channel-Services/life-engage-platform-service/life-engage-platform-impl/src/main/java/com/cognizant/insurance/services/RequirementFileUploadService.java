/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 325754
 * @version       : 0.1, Oct 2, 2015
 */
package com.cognizant.insurance.services;

import java.io.IOException;

import com.cognizant.insurance.core.exception.BusinessException;

/**
 * 
 * The Interface RequirementService.
 * 
 */
public interface RequirementFileUploadService {

    /**
     * 
     * @param inputJsonObj
     * @return
     * @throws BusinessException
     */
    String uploadRequirementDocFile(String inputJsonObj) throws BusinessException;

    /**
     * 
     * @param inputJsonObj
     * @return
     * @throws BusinessException
     * @throws IOException
     */
    String getRequirementDocumentFile(String inputJsonObj) throws BusinessException;

    /**
     * @param inputJsonObj
     * @return
     */
    String getRequirementDocFilesList(String inputJsonObj) throws BusinessException;    
    
    /**
     * @param inputJsonObj
     * @return
     */
    String saveRequirement(String inputJsonObj) throws BusinessException;

}
