package com.cognizant.insurance.generic.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.cognizant.insurance.dao.Dao;
import com.cognizant.insurance.domain.AutoEtrGenerateAudit;
import com.cognizant.insurance.request.Request;


public interface AutoEtrGenerateAuditDao extends Dao {

	List<AutoEtrGenerateAudit> retrieveAutoEtrGenerateAudit(String eappNo, EntityManager entityManager);

	void updateAutoETROnSave(
			Request<AutoEtrGenerateAudit> autoEtrGenerateAuditReq);

}
