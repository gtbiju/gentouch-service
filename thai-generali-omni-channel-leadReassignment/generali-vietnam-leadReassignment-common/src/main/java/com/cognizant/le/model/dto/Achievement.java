package com.cognizant.le.model.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



// TODO: Auto-generated Javadoc
/**
 * The Class Achievement.
 */
@Entity
@Table(name = "ACHIEVEMENT")
public class Achievement implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	/** The achievement id. */
	@Column(name = "achievement_code")
	private String achievementCode;
	
	@ManyToOne
	@JoinColumn(name = "agent_id", nullable=false)
	private Agent agent;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "description")
	private String description;


	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAchievementCode() {
		return achievementCode;
	}

	public void setAchievementCode(String achievementCode) {
		this.achievementCode = achievementCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
