package com.cognizant.insurance.omnichannel.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class SalesActivityDataVo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The columnHeader **/
    private String columnHeader;

    /** The total **/
    private String total;

    /** The call **/
    private String call;

    /** The appointment **/
    private String appointment;

    /** The visiting **/
    private String visiting;

    /** The presentation **/
    private String presentation;

    /** The newProspect **/
    private String newProspect;

    /** The closeCase **/
    private String closeCase;
    
    Map<String,Integer> appointmentWeekCount = new LinkedHashMap<String,Integer>(); 
        
    Map<String,Integer> callWeekCount = new LinkedHashMap<String,Integer>(); 
        
    Map<String,Integer> visitWeekCount = new LinkedHashMap<String,Integer>(); 
    
    Map<String,Integer> closeCaseWeekCount = new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> presentationWeekCount = new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> monthwiseAppointmentCount = new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> monthwiseCallCount = new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> monthwiseVisitCount = new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> monthwisePresentationCount =  new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> monthwiseCloseCount =  new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> weekwiseAppointmentCount = new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> weekwiseCallCount = new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> weekwiseVisitCount = new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> weekwiseCloseCount = new LinkedHashMap<String,Integer>();
    
    Map<String,Integer> weekwisePresentationCount = new LinkedHashMap<String,Integer>();
    
    Map<String,String> callColorMap = new LinkedHashMap<String,String>();
    
    Map<String,String> visitColorMap = new LinkedHashMap<String,String>();
    
    Map<String,String> appointmentColorMap = new LinkedHashMap<String,String>();
    
    Map<String,String> newProspectColorMap = new LinkedHashMap<String,String>();
    
    Map<String,String> presentationColorMap = new LinkedHashMap<String,String>();
    
    Map<String,String> closeCaseColorMap = new LinkedHashMap<String,String>();
    
    Map<String,String> totalColorMap = new LinkedHashMap<String,String>();
    
    Map<String,String> achieveAcitivity = new HashMap<String,String>();

    Map<String,Integer> totalMap = new LinkedHashMap<String,Integer>();

    Map<String,Integer> newProspectDetails = new LinkedHashMap<String,Integer>();
     
       

	public Map<String, Integer> getCloseCaseWeekCount() {
        return closeCaseWeekCount;
    }

    public void setCloseCaseWeekCount(Map<String, Integer> closeCaseWeekCount) {
        this.closeCaseWeekCount = closeCaseWeekCount;
    }

    public Map<String, Integer> getPresentationWeekCount() {
        return presentationWeekCount;
    }

    public void setPresentationWeekCount(Map<String, Integer> presentationWeekCount) {
        this.presentationWeekCount = presentationWeekCount;
    }

    public Map<String, Integer> getWeekwiseCloseCount() {
        return weekwiseCloseCount;
    }

    public void setWeekwiseCloseCount(Map<String, Integer> weekwiseCloseCount) {
        this.weekwiseCloseCount = weekwiseCloseCount;
    }

    public Map<String, Integer> getWeekwisePresentationCount() {
        return weekwisePresentationCount;
    }

    public void setWeekwisePresentationCount(Map<String, Integer> weekwisePresentationCount) {
        this.weekwisePresentationCount = weekwisePresentationCount;
    }

    public Map<String, Integer> getMonthwisePresentationCount() {
        return monthwisePresentationCount;
    }

    public void setMonthwisePresentationCount(Map<String, Integer> monthwisePresentationCount) {
        this.monthwisePresentationCount = monthwisePresentationCount;
    }

    public Map<String, Integer> getMonthwiseCloseCount() {
        return monthwiseCloseCount;
    }

    public void setMonthwiseCloseCount(Map<String, Integer> monthwiseCloseCount) {
        this.monthwiseCloseCount = monthwiseCloseCount;
    }

    public String getColumnHeader() {
        return columnHeader;
    }

    public void setColumnHeader(String columnHeader) {
        this.columnHeader = columnHeader;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    public String getVisiting() {
        return visiting;
    }

    public void setVisiting(String visiting) {
        this.visiting = visiting;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getNewProspect() {
        return newProspect;
    }

    public void setNewProspect(String newProspect) {
        this.newProspect = newProspect;
    }

    public String getCloseCase() {
        return closeCase;
    }

    public void setCloseCase(String closeCase) {
        this.closeCase = closeCase;
    }

	public Map<String, Integer> getAppointmentWeekCount() {
		return appointmentWeekCount;
	}

	public void setAppointmentWeekCount(Map<String, Integer> appointmentWeekCount) {
		this.appointmentWeekCount = appointmentWeekCount;
	}

	public Map<String, Integer> getCallWeekCount() {
		return callWeekCount;
	}

	public void setCallWeekCount(Map<String, Integer> callWeekCount) {
		this.callWeekCount = callWeekCount;
	}

	public Map<String, Integer> getVisitWeekCount() {
		return visitWeekCount;
	}

	public void setVisitWeekCount(Map<String, Integer> visitWeekCount) {
		this.visitWeekCount = visitWeekCount;
	}

	public Map<String, Integer> getMonthwiseAppointmentCount() {
		return monthwiseAppointmentCount;
	}

	public void setMonthwiseAppointmentCount(Map<String, Integer> monthwiseAppointmentCount) {
		this.monthwiseAppointmentCount = monthwiseAppointmentCount;
	}

	public Map<String, Integer> getMonthwiseCallCount() {
		return monthwiseCallCount;
	}

	public void setMonthwiseCallCount(Map<String, Integer> monthwiseCallCount) {
		this.monthwiseCallCount = monthwiseCallCount;
	}

	public Map<String, Integer> getMonthwiseVisitCount() {
		return monthwiseVisitCount;
	}

	public void setMonthwiseVisitCount(Map<String, Integer> monthwiseVisitCount) {
		this.monthwiseVisitCount = monthwiseVisitCount;
	}

	public Map<String, Integer> getWeekwiseAppointmentCount() {
		return weekwiseAppointmentCount;
	}

	public void setWeekwiseAppointmentCount(Map<String, Integer> weekwiseAppointmentCount) {
		this.weekwiseAppointmentCount = weekwiseAppointmentCount;
	}

	public Map<String, Integer> getWeekwiseCallCount() {
		return weekwiseCallCount;
	}

	public void setWeekwiseCallCount(Map<String, Integer> weekwiseCallCount) {
		this.weekwiseCallCount = weekwiseCallCount;
	}

	public Map<String, Integer> getWeekwiseVisitCount() {
		return weekwiseVisitCount;
	}

	public void setWeekwiseVisitCount(Map<String, Integer> weekwiseVisitCount) {
		this.weekwiseVisitCount = weekwiseVisitCount;
	}

	public Map<String, String> getCallColorMap() {
		return callColorMap;
	}

	public void setCallColorMap(Map<String, String> callColorMap) {
		this.callColorMap = callColorMap;
	}

	public Map<String, String> getVisitColorMap() {
		return visitColorMap;
	}

	public void setVisitColorMap(Map<String, String> visitColorMap) {
		this.visitColorMap = visitColorMap;
	}

	public Map<String, String> getAppointmentColorMap() {
		return appointmentColorMap;
	}

	public void setAppointmentColorMap(Map<String, String> appointmentColorMap) {
		this.appointmentColorMap = appointmentColorMap;
	}

	public Map<String, String> getAchieveAcitivity() {
		return achieveAcitivity;
	}

	public void setAchieveAcitivity(Map<String, String> achieveAcitivity) {
		this.achieveAcitivity = achieveAcitivity;
	}

	public Map<String, Integer> getTotalMap() {
		return totalMap;
	}

	public void setTotalMap(Map<String, Integer> totalMap) {
		this.totalMap = totalMap;
	}

	public Map<String, Integer> getNewProspectDetails() {
		return newProspectDetails;
	}

	public void setNewProspectDetails(Map<String, Integer> newProspectDetails) {
		this.newProspectDetails = newProspectDetails;
	}

	public Map<String, String> getNewProspectColorMap() {
		return newProspectColorMap;
	}

	public void setNewProspectColorMap(Map<String, String> newProspectColorMap) {
		this.newProspectColorMap = newProspectColorMap;
	}

	public Map<String, String> getTotalColorMap() {
		return totalColorMap;
	}

	public void setTotalColorMap(Map<String, String> totalColorMap) {
		this.totalColorMap = totalColorMap;
	}

	public Map<String, String> getPresentationColorMap() {
		return presentationColorMap;
	}

	public void setPresentationColorMap(Map<String, String> presentationColorMap) {
		this.presentationColorMap = presentationColorMap;
	}

	public Map<String, String> getCloseCaseColorMap() {
		return closeCaseColorMap;
	}

	public void setCloseCaseColorMap(Map<String, String> closeCaseColorMap) {
		this.closeCaseColorMap = closeCaseColorMap;
	}

}
