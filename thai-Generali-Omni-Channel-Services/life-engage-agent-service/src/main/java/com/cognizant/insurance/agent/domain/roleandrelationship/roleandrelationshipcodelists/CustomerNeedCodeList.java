package com.cognizant.insurance.agent.domain.roleandrelationship.roleandrelationshipcodelists;

public enum CustomerNeedCodeList {
	
	Education,
	
	Retirement,
	
	Investment,
	
	Health,
	
	Other,
	
	NotMentioned
}
