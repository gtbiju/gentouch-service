package com.cognizant.insurance.omnichannel.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.icr.pdf.util.PropertyFileReader;

public final class GeneraliPDFContentReader {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyFileReader.class);

    /**
     * Fetch property.
     *
     * @param key
     *            , the key to the property file.
     * @param fileName
     *            the file name
     * @return the property value.
     */
    public static String getPDFContent(final String fileName) {
        LOGGER.debug("Method Entry : PropertyFileReader-> fetchProperty");
        String theString = null;
        try {

            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream stream = classLoader.getResourceAsStream("/" + fileName);
            if (null == stream) {
                stream = classLoader.getResourceAsStream(fileName);
            }
            if(stream != null){
            	int i = stream.available();
            	StringWriter writer = new StringWriter();
            	IOUtils.copy(stream, writer, "UTF-8");
            	theString = writer.toString();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.debug("Method Exit : PropertyFileReader-> fetchProperty");
        return theString;
    }

    /**
     * Instantiates a new property file reader.
     */
    private GeneraliPDFContentReader() {

    }
}
