/**
 *
 * Copyright 2013, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Jul 29, 2013
 */
package com.cognizant.insurance.endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.insurance.constants.Constants;
import com.cognizant.insurance.core.exception.BusinessException;
import com.cognizant.insurance.services.PaymentService;

/**
 * @author 301350
 * 
 */
@Controller
@RequestMapping("/paymentService")
public class PaymentServiceEndPoint {

    /** The logger. */
    public static final Logger LOGGER =
            LoggerFactory.getLogger(LookUpServiceEndPoint.class);

    /** The payment service. */
    @Autowired
    private PaymentService paymentService;

    /**
     * Make payment.
     *
     * @param json the json
     * @return the string
     */
    @RequestMapping(value = "/makePayment", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String makePayment(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return paymentService.makePayment(json);
            }
        }.execute(json);
    }
    
    /**
     * Gets the payment options.
     *
     * @param json the json
     * @return the payment options
     */
    @RequestMapping(value = "/paymentOptions", method = RequestMethod.POST, consumes = Constants.TYPE_APPLICATION_JSON, produces = Constants.TYPE_APPLICATION_JSON)
    @ResponseBody
    public final String getPaymentOptions(@RequestBody final String json) {
        return new ServiceTemplate() {
            @Override
            public String doExecute(final String json) throws BusinessException {
                return paymentService.getPaymentOptions(json);
            }
        }.execute(json);
    }

    /**
     * @return the paymentService
     */
    public final PaymentService getPaymentService() {
        return paymentService;
    }

    /**
     * @param paymentService the paymentService to set
     */
    public final void setPaymentService(final PaymentService paymentService) {
        this.paymentService = paymentService;
    }
    
}
