package com.cognizant.insurance.agent.service;


public interface UserService {

	public String register(String inputJSON);

	public String forgetPassword(String inputJSON);	

	public String createPassword(String inputJSON);
	
	public String resetPassword(String inputJSON);
	
	public String retrieveLeadList(String inputJSON);
	
}
