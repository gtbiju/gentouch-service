/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Feb 18, 2015
 */
package com.cognizant.generali.security.component.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.generali.security.dao.impl.GeneraliValidationDaoImpl;
import com.cognizant.generali.security.entity.GeneraliUserToken;
import com.cognizant.le.security.component.LifeEngageValidationComponent;
import com.cognizant.le.security.dao.ValidationDao;
import com.cognizant.le.security.dao.impl.ValidationDaoImpl;
import com.cognizant.le.security.entity.UserToken;
import com.cognizant.le.security.request.Request;
import com.cognizant.le.security.response.Response;
import com.cognizant.le.security.response.impl.ResponseImpl;
import com.cognizant.le.security.response.vo.TokenInfo;

/**
 * The Class class LifeEngageValidationComponentImpl.
 */
public class GeneraliValidationComponentImpl implements LifeEngageValidationComponent {

    /** The logger. */
    public static final Logger LOGGER = LoggerFactory.getLogger(GeneraliValidationComponentImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.login.component.LifeEngageValidationComponent#saveToken(com.cognizant.login.request.Request)
     */
    
    @Transactional(rollbackFor = { Exception.class })
    public final void saveToken(final Request<TokenInfo> tokenInfoRq) {
        LOGGER.debug("METHOD ENTRY : LifeEngageValidationComponentImpl.saveToken ");

        ValidationDao validationDao = new GeneraliValidationDaoImpl();
        validationDao.merge(tokenInfoRq);
        LOGGER.debug("METHOD EXIT : LifeEngageValidationComponentImpl.saveToken ");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.login.component.LifeEngageValidationComponent#getTokenDetails(com.cognizant.login.request.Request)
     */
    
    @Transactional(rollbackFor = { Exception.class })
    public final Response<TokenInfo> getTokenDetails(final Request<String> seriesRq) {
        LOGGER.debug("METHOD ENTRY : LifeEngageValidationComponentImpl.getTokenDetails series" + seriesRq.getType());
        GeneraliUserToken userToken = null;
        Response<TokenInfo> response = new ResponseImpl<TokenInfo>();
        final TokenInfo responseTokenInfo = new TokenInfo();

        ValidationDao validationDao = new ValidationDaoImpl();
        userToken = (GeneraliUserToken) validationDao.getTokenDetails(seriesRq);
        if (userToken != null) {
            responseTokenInfo.setToken(userToken.getToken());
            responseTokenInfo.setSeries(seriesRq.getType());
            responseTokenInfo.setLastUsedAt(userToken.getTimeStamp());
            responseTokenInfo.setUsername(userToken.getUserId());
            LOGGER.debug("getTokenDetails responseTokenInfo>>" + responseTokenInfo);
        }

        response.setType(responseTokenInfo);
        return response;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.cognizant.le.security.component.LifeEngageValidationComponent#updateToken(com.cognizant.le.security.request
     * .Request)
     */
    
    @Transactional(rollbackFor = { Exception.class })
    public void updateToken(Request<TokenInfo> tokenInfo) {
        LOGGER.debug("METHOD ENTRY : LifeEngageValidationComponentImpl.getTokenDetails series" + tokenInfo.getType());
        ValidationDao validationDao = new ValidationDaoImpl();
        validationDao.updateToken(tokenInfo);

    }

}
