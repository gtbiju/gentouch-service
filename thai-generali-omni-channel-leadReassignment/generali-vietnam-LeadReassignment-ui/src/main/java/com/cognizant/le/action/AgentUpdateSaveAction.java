package com.cognizant.le.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.cognizant.le.manager.LeadManager;

@WebServlet("/AgentUpdateSaveAction")
public class AgentUpdateSaveAction extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6833439222439684636L;
	private static Logger LOGGER = Logger.getLogger(LoginAction.class);
	
	
	private WebApplicationContext springContext;	
	
	@Autowired
	LeadManager leadListingService;
	
	
	
   public AgentUpdateSaveAction() {
	        super();
	        // TODO Auto-generated constructor stub
	    }
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.debug("Method Entry : AgentRetrieveAction.doPost");
				
		String leadSaveUpdateResponseString = "";
		JSONObject leadSaveUpdateJSON = new JSONObject();
		JSONObject statusArrayJSONObj = new JSONObject();
		JSONObject statusObject = new JSONObject();
		Boolean statusFlag = false;
		Boolean isValidSession = false;	
		/*Properties properties = new Properties();
		String propertiesFile = getServletContext().getInitParameter("CONFIG_PROPERTIES");
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream stream = classLoader.getResourceAsStream("/" + propertiesFile);
		if(null == stream) {
			stream = classLoader.getResourceAsStream(propertiesFile);
		}
		properties.load(stream);*/
		
		HttpSession session = request.getSession(true);
		String agentId = (String) session.getAttribute("username");
		/*String defaultLanguage = properties.getProperty("DEFAULT_LANGUAGE");
		if((null==defaultLanguage) || "".equals(defaultLanguage.trim())) {
			LOGGER.error("Property DEFAULT_LANGUAGE is not defined");
		} 
		session.setAttribute("DEFAULT_LANGUAGE", defaultLanguage);*/
		
		String reAssignedAgentName = request.getParameter("reassignAgentName");		
		String[] reAssignAgentCodeParts = reAssignedAgentName.split("-");
		String reAssignAgentCode = reAssignAgentCodeParts[0];
		String[] selectedLeadNames = request.getParameterValues("recordsSelected[]");
		
		try {			
			
			//for(int i = 0; i < selectedLeadNames.length; i++)
			//{
				
				//String selectedLeadId = selectedLeadNames[i];				
				
				//MockData
		    	/*String url= "/agentProfile/Agent_UpdateSaveResponse.json";
				String responseJSON = new Scanner(this.getClass().getResourceAsStream(url)).useDelimiter("\\Z").next();
				JSONObject jsonObject = new JSONObject(responseJSON);
				leadSaveUpdateJSON = jsonObject.toString();*/				
				
				leadSaveUpdateResponseString = leadListingService.getLeadsAndUpdate(selectedLeadNames, reAssignAgentCode, agentId);	
				
				if("FAILURE".equals(leadSaveUpdateResponseString)) {
					if(session!= null)
					{
						isValidSession = true;
					}
					statusFlag = false;
					JSONObject leadResponseMessage = new JSONObject();
					leadResponseMessage.put("isValidSession",isValidSession);
					leadResponseMessage.put("status", leadSaveUpdateResponseString);
					leadSaveUpdateJSON = leadResponseMessage;
				} else {
					JSONObject updateResponse = new JSONObject(leadSaveUpdateResponseString);			
					JSONArray updateResponseArray = updateResponse.getJSONObject("Response")
		         	.getJSONObject("ResponsePayload").getJSONArray("Transactions");
					for(int i = 0; i < updateResponseArray.length(); i++) {
						statusArrayJSONObj = updateResponseArray.getJSONObject(i);
						statusObject = statusArrayJSONObj.getJSONObject("StatusData");
						if(("SUCCESS").equalsIgnoreCase(statusObject.getString("Status"))) {					
							statusFlag = true;
							
							break;
						}
					}
					
					if(session!= null)
					{
						isValidSession = true;
					} 										
					statusArrayJSONObj.put("isValidSession", isValidSession);
					if(statusFlag) {
						leadSaveUpdateJSON = statusArrayJSONObj;
					}
				}
									
			//}
			
		    if(statusFlag) {
		    	response.getWriter().print(leadSaveUpdateJSON);	
		    } else {
		    	response.getWriter().print(leadSaveUpdateJSON);	
		    }
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		LOGGER.debug("Method Exit : LoginAction.doPost");		
	}
	
	public void init(ServletConfig config)throws ServletException {
        super.init(config);
         springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
            final AutowireCapableBeanFactory beanFactory = springContext.getAutowireCapableBeanFactory();
            beanFactory.autowireBean(this);    
  }

	
	

	
	
	
}
