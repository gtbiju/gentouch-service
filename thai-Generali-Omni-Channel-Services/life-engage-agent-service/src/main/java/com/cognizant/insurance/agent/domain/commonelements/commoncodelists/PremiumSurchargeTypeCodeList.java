/**
 * 
 */
package com.cognizant.insurance.agent.domain.commonelements.commoncodelists;

/**
 * Premium Surcharge Type.
 * 
 * @author 301350
 * 
 * 
 */
public enum PremiumSurchargeTypeCodeList {
    /**
     * 
     * 
     * 
     * 
     */
    Rate,
    /**
     * 
     * 
     * 
     * 
     */
    Amount
}
