/**
 * 
 */
package com.cognizant.insurance.omnichannel.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author 420118
 *
 */
@Component
public class GeneraliAsymmetricEncryptionUtil {
	
	@Value("${le.privateKey.template.location}")
    private String privateKeyLocation;
	
	public String encrypt(String agentId)throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, InvalidKeyException, SignatureException{
		
		final KeyFactory keyFactory=KeyFactory.getInstance("RSA");
        final PrivateKey privateKey=keyFactory.generatePrivate(new PKCS8EncodedKeySpec(Files.readAllBytes(Paths.get(privateKeyLocation))));
        final String stringToSign=agentId;
        final Signature signature=Signature.getInstance("SHA256WithRSA");
        signature.initSign(privateKey);
        signature.update(stringToSign.getBytes());
        final byte[] signatureBytes =signature.sign();
        final String base64Signature = new String(DatatypeConverter.printBase64Binary(signatureBytes));
		return base64Signature;
	}
}
